﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using DevExpress.XtraEditors;
using Smartmatic.SmartCad.Telephony;
using SmartCadCore.Common;
using SmartCadCore.Core;



namespace SmartCadServerConfiguration.Gui
{
    public partial class ServerTelephonyConfigureForm : XtraForm
    {
        IConfigurationControl TelephonyControl;

        internal string ServerType { get; set; }

        public ServerTelephonyConfigureForm()
        {
            InitializeComponent();
            LoadLanguage();

            comboBoxEditServerType.Properties.Items.AddRange(TelephonyFactory.GetAvailableTypes());
            comboBoxEditServerType.SelectedItem = SmartCadConfiguration.SmartCadSection.ServerElement.TelephonyServerCommunicationElement.ServerType;
        }

        void LoadLanguage()
        {
            Text = ResourceLoader.GetString2(this.Name);
            layoutControlItemType.Text ="  " + ResourceLoader.GetString2("TelephonyServerType") + " :*";
            buttonExAccept.Text = ResourceLoader.GetString2("Accept");
            buttonExCancel.Text = ResourceLoader.GetString2("Cancel");
        }

        private void comboBoxEditServerType_SelectedIndexChanged(object sender, EventArgs e)
        {
            FormUtil.InvokeRequired(this,
            delegate
            {
                ServerType = comboBoxEditServerType.SelectedItem as string;
                if (ServerType != null)
                {
                    try
                    {
                        panelConfigurationControl.Controls.Clear();

                        TelephonyControl = TelephonyFactory.GetConfigurationControl(ServerType);
                        TelephonyControl.FulFilled_Change += new Parameters_ChangeEvent(TelephonyControl_FulFilled_Change);
                        TelephonyControl.SetProperties(LoadProperties());
                        TelephonyControl.SetStatisticProperties(LoadStatisticProperties());

                        panelConfigurationControl.Controls.Add(TelephonyControl as Control);
                        this.Width = ((Control)TelephonyControl).Width + 22;
                        this.Height = ((Control)TelephonyControl).Height + 105;
                    }
                    catch
                    {
                        this.Width = 275;
                        this.Height = 125;
                    }
                }
            });
        }

        Dictionary<string, string> LoadProperties()
        {
            Dictionary<string, string> properties = new Dictionary<string, string>();
            foreach (PropertyElement prop in SmartCadConfiguration.SmartCadSection.ServerElement.TelephonyServerCommunicationElement.Properties)
            {
                properties.Add(prop.Name, prop.Value);
            }
            return properties;
        }

        private string[] LoadStatisticProperties()
        {
            string[] props = new string[8];
            TelephonyServerStatisticElement statisticsElement = SmartCadConfiguration.SmartCadSection.ServerElement.TelephonyServerStatisticElement;

            props[0] = statisticsElement.Server;
            props[1] = statisticsElement.ServerLogin;
            props[2] = statisticsElement.ServerPassword;
            //NORTEL
            var baseScript = "Driver={};" +
                "Host=;" +
                "Server=;" +
                "Service=;" +
                "Protocol=;" +
                "Database=;" +
                "Uid=;" +
                "Pwd=;" +
                "CLIENT_LOCALE=;" +
                "DB_LOCALE=;";

            //var baseScriptSplited = baseScript.Split(';');

            //var propertiesFromFile = statisticsElement.Script.Split(';');

            


            props[3] = GetScriptFieldFormatted(baseScript.Split(';'), statisticsElement.Script.Split(';'));
            //GENESYS
            props[4] = statisticsElement.Port;
            props[5] = statisticsElement.ServiceName;

            return props;
        }

        private string GetScriptFieldFormatted(string[] BaseScript, string[] FileScript)
        {
            var result = "";
            foreach (var property in BaseScript)
            {
                var propertyComplete = property;
                foreach (var propertyFile in FileScript)
                {
                    var propertyFileDictionary = propertyFile.Split('=');
                    if (!string.IsNullOrEmpty(property) && !string.IsNullOrEmpty(propertyFile) && property.StartsWith(propertyFileDictionary[0]))
                    {
                        propertyComplete = property + propertyFileDictionary[1];
                        break;
                    }
                    
                }
                result = result + propertyComplete + ";";
            }
            return result;
        }

        void TelephonyControl_FulFilled_Change(bool isfullFilled)
        {
            buttonExAccept.Enabled = isfullFilled;
        }

        private void buttonExAccept_Click(object sender, EventArgs e)
        {
            //TELEPHONY 
            SmartCadConfiguration.SmartCadSection.ServerElement.TelephonyServerCommunicationElement.ServerType = comboBoxEditServerType.Text;

            PropertiesCollection pc = SmartCadConfiguration.SmartCadSection.ServerElement.TelephonyServerCommunicationElement.Properties;
            pc.Clear();
            foreach (KeyValuePair<string,string> prop in TelephonyControl.GetProperties())
            {
                pc.Add(new PropertyElement() { Name = prop.Key, Value = prop.Value });
            }
            //STATISTICS
            string[] statsProperties = TelephonyControl.GetStatisticProperties();
            TelephonyServerStatisticElement statisticsElement = SmartCadConfiguration.SmartCadSection.ServerElement.TelephonyServerStatisticElement;
            statisticsElement.ServerType = comboBoxEditServerType.Text;
            statisticsElement.Server = statsProperties[0];
            statisticsElement.ServerLogin = statsProperties[1];
            statisticsElement.ServerPassword = statsProperties[2];
            //NORTEL
            statisticsElement.Script = statsProperties[3];
            //GENESYS
            statisticsElement.Port = statsProperties[4];
            statisticsElement.ServiceName = statsProperties[5];
            //AVAYA

           // SmartCadConfiguration.Save();
        }
    }
}
