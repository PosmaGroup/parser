using System;
using SmartCadControls.Controls;


namespace SmartCadServerConfiguration.Gui
{
    partial class ConnectionServerConfigurationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private new System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConnectionServerConfigurationForm));
            this.buttonExAccept = new SmartCadControls.Controls.SimpleButtonEx();
            this.buttonExCancel = new SmartCadControls.Controls.SimpleButtonEx();
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.xtraTabPageVMS = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabPageVMSVA = new DevExpress.XtraTab.XtraTabPage();
            this.layoutControlMainVMS = new DevExpress.XtraLayout.LayoutControl();
            this.layoutControlMainVMSVA = new DevExpress.XtraLayout.LayoutControl();
            this.textEditPort = new DevExpress.XtraEditors.TextEdit();
            this.textEditPassword = new DevExpress.XtraEditors.TextEdit();
            this.textEditServer = new DevExpress.XtraEditors.TextEdit();
            this.textEditLogin = new DevExpress.XtraEditors.TextEdit();
            this.textEditPortVA = new DevExpress.XtraEditors.TextEdit();
            this.textEditPasswordVA = new DevExpress.XtraEditors.TextEdit();
            this.textEditServerVA = new DevExpress.XtraEditors.TextEdit();
            this.textEditLoginVA = new DevExpress.XtraEditors.TextEdit();
            this.checkbuttonVA = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControlGroupVMS = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroupVMSVA = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemServer = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemLogin = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemServerVA = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemLoginVA = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemCheckButtonVA = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItemVMS = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItemPassword = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItemVMSVA = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2VMSVA = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItemPasswordVA = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1VA = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabPageConnection = new DevExpress.XtraTab.XtraTabPage();
            this.dbConnectionControl = new SmartCadServerConfiguration.Gui.DbConnectionControl();
            this.tabControl = new DevExpress.XtraTab.XtraTabControl();
            this.tabPagePreferences = new DevExpress.XtraTab.XtraTabPage();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.textBoxLongitud = new System.Windows.Forms.TextBox();
            this.textBoxLatitud = new System.Windows.Forms.TextBox();
            this.comboBoxLanguages = new System.Windows.Forms.ComboBox();
            this.comboBoxMapType = new System.Windows.Forms.ComboBox();
            this.layoutControlLocation = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlMapType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlLanguages = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlLatitud = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlLongitud = new DevExpress.XtraLayout.LayoutControlItem();
            this.xtraTabPageVMS.SuspendLayout();
            this.xtraTabPageVMSVA.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlMainVMS)).BeginInit();
            this.layoutControlMainVMS.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlMainVMSVA)).BeginInit();
            this.layoutControlMainVMSVA.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPort.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPassword.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditServer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditLogin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPortVA.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPasswordVA.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditServerVA.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkbuttonVA.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditLoginVA.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupVMS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupVMSVA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemServer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLogin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemServerVA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLoginVA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCheckButtonVA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItemVMS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItemVMSVA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2VMSVA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPasswordVA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1VA)).BeginInit();
            this.tabPageConnection.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl)).BeginInit();
            this.tabControl.SuspendLayout();
            this.tabPagePreferences.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlMapType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlLanguages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlLatitud)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlLongitud)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonExAccept
            // 
            this.buttonExAccept.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonExAccept.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExAccept.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonExAccept.Appearance.Options.UseFont = true;
            this.buttonExAccept.Appearance.Options.UseForeColor = true;
            this.buttonExAccept.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonExAccept.FontFormat = SmartCadControls.Controls.FontFormat.GetFormat("ButtonFormat");
            this.buttonExAccept.Location = new System.Drawing.Point(270, 233);
            this.buttonExAccept.Name = "buttonExAccept";
            this.buttonExAccept.Size = new System.Drawing.Size(75, 23);
            this.buttonExAccept.TabIndex = 0;
            this.buttonExAccept.Text = "Accept";
            this.buttonExAccept.Click += new System.EventHandler(this.buttonExAccept_Click);
            // 
            // buttonExCancel
            // 
            this.buttonExCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonExCancel.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExCancel.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonExCancel.Appearance.Options.UseFont = true;
            this.buttonExCancel.Appearance.Options.UseForeColor = true;
            this.buttonExCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonExCancel.FontFormat = SmartCadControls.Controls.FontFormat.GetFormat("ButtonFormat");
            this.buttonExCancel.Location = new System.Drawing.Point(351, 233);
            this.buttonExCancel.Name = "buttonExCancel";
            this.buttonExCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonExCancel.TabIndex = 1;
            this.buttonExCancel.Text = "Cancelar";
            // 
            // defaultLookAndFeel1
            // 
            this.defaultLookAndFeel1.LookAndFeel.SkinName = "Blue";
            // 
            // xtraTabPageVMS
            // 
            this.xtraTabPageVMS.Controls.Add(this.layoutControlMainVMS);
            this.xtraTabPageVMS.Name = "xtraTabPageVMS";
            this.xtraTabPageVMS.Size = new System.Drawing.Size(413, 194);
            // 
            // xtraTabPageVMSVA
            // 
            this.xtraTabPageVMSVA.Controls.Add(this.layoutControlMainVMSVA);
            this.xtraTabPageVMSVA.Name = "xtraTabPageVMSVA";
            this.xtraTabPageVMSVA.Size = new System.Drawing.Size(413, 194);
            // 
            // layoutControlMainVMS
            // 
            this.layoutControlMainVMS.Controls.Add(this.textEditPort);
            this.layoutControlMainVMS.Controls.Add(this.textEditPassword);
            this.layoutControlMainVMS.Controls.Add(this.textEditServer);
            this.layoutControlMainVMS.Controls.Add(this.textEditLogin);
            this.layoutControlMainVMS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlMainVMS.Location = new System.Drawing.Point(0, 0);
            this.layoutControlMainVMS.Name = "layoutControlMainVMS";
            this.layoutControlMainVMS.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(452, 21, 250, 350);
            this.layoutControlMainVMS.Root = this.layoutControlGroupVMS;
            this.layoutControlMainVMS.Size = new System.Drawing.Size(413, 194);
            this.layoutControlMainVMS.TabIndex = 0;
            this.layoutControlMainVMS.Text = "layoutControl1";
            // 
            // layoutControlMainVMSVA
            // 
            this.layoutControlMainVMSVA.Controls.Add(this.textEditPortVA);
            this.layoutControlMainVMSVA.Controls.Add(this.textEditPasswordVA);
            this.layoutControlMainVMSVA.Controls.Add(this.textEditServerVA);
            this.layoutControlMainVMSVA.Controls.Add(this.textEditLoginVA);
            this.layoutControlMainVMSVA.Controls.Add(this.checkbuttonVA);
            this.layoutControlMainVMSVA.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlMainVMSVA.Location = new System.Drawing.Point(0, 0);
            this.layoutControlMainVMSVA.Name = "layoutControlMainVMSVA";
            this.layoutControlMainVMSVA.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(452, 21, 250, 350);
            this.layoutControlMainVMSVA.Root = this.layoutControlGroupVMSVA;
            this.layoutControlMainVMSVA.Size = new System.Drawing.Size(413, 194);
            this.layoutControlMainVMSVA.TabIndex = 0;
            this.layoutControlMainVMSVA.Text = "layoutControlVA1";
            // 
            // textEditPort
            // 
            this.textEditPort.Location = new System.Drawing.Point(148, 84);
            this.textEditPort.Name = "textEditPort";
            this.textEditPort.Properties.PasswordChar = '*';
            this.textEditPort.Size = new System.Drawing.Size(253, 20);
            this.textEditPort.StyleController = this.layoutControlMainVMS;
            this.textEditPort.TabIndex = 7;
            // 
            // textEditPassword
            // 
            this.textEditPassword.Location = new System.Drawing.Point(148, 60);
            this.textEditPassword.Name = "textEditPassword";
            this.textEditPassword.Properties.PasswordChar = '*';
            this.textEditPassword.Size = new System.Drawing.Size(253, 20);
            this.textEditPassword.StyleController = this.layoutControlMainVMS;
            this.textEditPassword.TabIndex = 6;
            this.textEditPassword.TextChanged += new System.EventHandler(this.dbConnectionControl_ParametersChange);
            // 
            // textEditServer
            // 
            this.textEditServer.Location = new System.Drawing.Point(148, 12);
            this.textEditServer.Name = "textEditServer";
            this.textEditServer.Size = new System.Drawing.Size(253, 20);
            this.textEditServer.StyleController = this.layoutControlMainVMS;
            this.textEditServer.TabIndex = 4;
            this.textEditServer.TextChanged += new System.EventHandler(this.dbConnectionControl_ParametersChange);
            // 
            // textEditLogin
            // 
            this.textEditLogin.Location = new System.Drawing.Point(148, 36);
            this.textEditLogin.Name = "textEditLogin";
            this.textEditLogin.Size = new System.Drawing.Size(253, 20);
            this.textEditLogin.StyleController = this.layoutControlMainVMS;
            this.textEditLogin.TabIndex = 5;
            this.textEditLogin.TextChanged += new System.EventHandler(this.dbConnectionControl_ParametersChange);
            // 
            // textEditPortVA
            // 
            this.textEditPortVA.Location = new System.Drawing.Point(148, 108);
            this.textEditPortVA.Name = "textEditPort";
            this.textEditPortVA.Properties.PasswordChar = '*';
            this.textEditPortVA.Size = new System.Drawing.Size(253, 20);
            this.textEditPortVA.StyleController = this.layoutControlMainVMSVA;
            this.textEditPortVA.TabIndex = 7;
            // 
            // textEditPasswordVA
            // 
            this.textEditPasswordVA.Location = new System.Drawing.Point(148, 84);
            this.textEditPasswordVA.Name = "textEditPassword";
            this.textEditPasswordVA.Properties.PasswordChar = '*';
            this.textEditPasswordVA.Size = new System.Drawing.Size(253, 20);
            this.textEditPasswordVA.StyleController = this.layoutControlMainVMSVA;
            this.textEditPasswordVA.TabIndex = 6;
            this.textEditPasswordVA.TextChanged += new System.EventHandler(this.dbConnectionControl_ParametersChange);
            //
            // checkbuttonVA
            //
            this.checkbuttonVA.Location = new System.Drawing.Point(148,12);
            this.checkbuttonVA.Name = "checkbuttonVA";
            this.checkbuttonVA.Size = new System.Drawing.Size(253, 20);
            this.checkbuttonVA.StyleController = this.layoutControlMainVMSVA;
            this.checkbuttonVA.TabIndex = 4;
            this.checkbuttonVA.Text = "";
            this.checkbuttonVA.CheckedChanged += new System.EventHandler(this.analitycsActivate);
            // 
            // textEditServerVA
            // 
            this.textEditServerVA.Location = new System.Drawing.Point(148, 36);
            this.textEditServerVA.Name = "textEditServer";
            this.textEditServerVA.Size = new System.Drawing.Size(253, 20);
            this.textEditServerVA.StyleController = this.layoutControlMainVMSVA;
            this.textEditServerVA.TabIndex = 4;
            this.textEditServerVA.TextChanged += new System.EventHandler(this.dbConnectionControl_ParametersChange);
            // 
            // textEditLoginVA
            // 
            this.textEditLoginVA.Location = new System.Drawing.Point(148, 60);
            this.textEditLoginVA.Name = "textEditLogin";
            this.textEditLoginVA.Size = new System.Drawing.Size(253, 20);
            this.textEditLoginVA.StyleController = this.layoutControlMainVMSVA;
            this.textEditLoginVA.TabIndex = 5;
            this.textEditLoginVA.TextChanged += new System.EventHandler(this.dbConnectionControl_ParametersChange);
            // 
            // layoutControlGroupVMS
            // 
            this.layoutControlGroupVMS.CustomizationFormText = "layoutControlGroupVMS";
            this.layoutControlGroupVMS.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroupVMS.GroupBordersVisible = false;
            this.layoutControlGroupVMS.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemServer,
            this.layoutControlItemLogin,
            this.emptySpaceItemVMS,
            this.layoutControlItemPassword,
            this.layoutControlItem1});
            this.layoutControlGroupVMS.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupVMS.Name = "layoutControlGroupVMS";
            this.layoutControlGroupVMS.Size = new System.Drawing.Size(413, 194);
            this.layoutControlGroupVMS.Text = "layoutControlGroupVMS";
            this.layoutControlGroupVMS.TextVisible = false;
            // 
            // layoutControlGroupVMSVA
            // 
            this.layoutControlGroupVMSVA.CustomizationFormText = "layoutControlGroupVMSVA";
            this.layoutControlGroupVMSVA.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroupVMSVA.GroupBordersVisible = false;
            this.layoutControlGroupVMSVA.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemCheckButtonVA,    
            this.layoutControlItemServerVA,
            this.layoutControlItemLoginVA,
            this.emptySpaceItemVMSVA,
            this.emptySpaceItem2VMSVA,
            this.layoutControlItemPasswordVA,
            this.layoutControlItem1VA});
            this.layoutControlGroupVMSVA.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupVMSVA.Name = "layoutControlGroupVMSVA";
            this.layoutControlGroupVMSVA.Size = new System.Drawing.Size(413, 194);
            this.layoutControlGroupVMSVA.Text = "layoutControlGroupVMSVA";
            this.layoutControlGroupVMSVA.TextVisible = false;
            // 
            // layoutControlItemServer
            // 
            this.layoutControlItemServer.Control = this.textEditServer;
            this.layoutControlItemServer.CustomizationFormText = "layoutControlItemIP";
            this.layoutControlItemServer.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemServer.Name = "layoutControlItemIP";
            this.layoutControlItemServer.Size = new System.Drawing.Size(393, 24);
            this.layoutControlItemServer.Text = "layoutControlItemIP";
            this.layoutControlItemServer.TextSize = new System.Drawing.Size(133, 13);
            // 
            // layoutControlItemLogin
            // 
            this.layoutControlItemLogin.Control = this.textEditLogin;
            this.layoutControlItemLogin.CustomizationFormText = "layoutControlItemUser";
            this.layoutControlItemLogin.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItemLogin.Name = "layoutControlItemUser";
            this.layoutControlItemLogin.Size = new System.Drawing.Size(393, 24);
            this.layoutControlItemLogin.Text = "layoutControlItemUser";
            this.layoutControlItemLogin.TextSize = new System.Drawing.Size(133, 13);
            // 
            // layoutControlItemServerVA
            // 
            this.layoutControlItemServerVA.Control = this.textEditServerVA;
            this.layoutControlItemServerVA.CustomizationFormText = "layoutControlItemIP";
            this.layoutControlItemServerVA.Location = new System.Drawing.Point(0, 35);
            this.layoutControlItemServerVA.Name = "layoutControlItemIP";
            this.layoutControlItemServerVA.Size = new System.Drawing.Size(393, 24);
            this.layoutControlItemServerVA.Text = "layoutControlItemIP";
            this.layoutControlItemServerVA.TextSize = new System.Drawing.Size(133, 13);
            // 
            // layoutControlItemCheckButtonVA
            // 
            this.layoutControlItemCheckButtonVA.Control = this.checkbuttonVA;
            this.layoutControlItemCheckButtonVA.CustomizationFormText = "layoutControlItemIP";
            this.layoutControlItemCheckButtonVA.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemCheckButtonVA.Name = "layoutControlItemIP";
            this.layoutControlItemCheckButtonVA.Size = new System.Drawing.Size(393, 24);
            this.layoutControlItemCheckButtonVA.Text = "layoutControlItemIP";
            this.layoutControlItemCheckButtonVA.TextSize = new System.Drawing.Size(133, 13);
            // 
            // layoutControlItemLoginVA
            // 
            this.layoutControlItemLoginVA.Control = this.textEditLoginVA;
            this.layoutControlItemLoginVA.CustomizationFormText = "layoutControlItemUser";
            this.layoutControlItemLoginVA.Location = new System.Drawing.Point(0, 59);
            this.layoutControlItemLoginVA.Name = "layoutControlItemUser";
            this.layoutControlItemLoginVA.Size = new System.Drawing.Size(393, 24);
            this.layoutControlItemLoginVA.Text = "layoutControlItemUser";
            this.layoutControlItemLoginVA.TextSize = new System.Drawing.Size(133, 13);
            // 
            // emptySpaceItemVMS
            // 
            this.emptySpaceItemVMS.AllowHotTrack = false;
            this.emptySpaceItemVMS.CustomizationFormText = "emptySpaceItemVMS";
            this.emptySpaceItemVMS.Location = new System.Drawing.Point(0, 96);
            this.emptySpaceItemVMS.Name = "emptySpaceItemVMS";
            this.emptySpaceItemVMS.Size = new System.Drawing.Size(393, 78);
            this.emptySpaceItemVMS.Text = "emptySpaceItemVMS";
            this.emptySpaceItemVMS.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItemPassword
            // 
            this.layoutControlItemPassword.Control = this.textEditPassword;
            this.layoutControlItemPassword.CustomizationFormText = "layoutControlItemPassword";
            this.layoutControlItemPassword.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItemPassword.Name = "layoutControlItemPassword";
            this.layoutControlItemPassword.Size = new System.Drawing.Size(393, 24);
            this.layoutControlItemPassword.Text = "layoutControlItemPassword";
            this.layoutControlItemPassword.TextSize = new System.Drawing.Size(133, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.textEditPort;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItemPort";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(393, 24);
            this.layoutControlItem1.Text = "Port";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(133, 13);
            // 
            // emptySpaceItemVMSVA
            // 
            this.emptySpaceItemVMSVA.AllowHotTrack = false;
            this.emptySpaceItemVMSVA.CustomizationFormText = "emptySpaceItemVMS";
            this.emptySpaceItemVMSVA.Location = new System.Drawing.Point(0, 131);
            this.emptySpaceItemVMSVA.Name = "emptySpaceItemVMS";
            this.emptySpaceItemVMSVA.Size = new System.Drawing.Size(393, 78);
            this.emptySpaceItemVMSVA.Text = "emptySpaceItemVMS";
            this.emptySpaceItemVMSVA.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2VMSVA
            // 
            this.emptySpaceItem2VMSVA.AllowHotTrack = false;
            this.emptySpaceItem2VMSVA.CustomizationFormText = "emptySpaceItem2VMSV";
            this.emptySpaceItem2VMSVA.Location = new System.Drawing.Point(0, 24);
            this.emptySpaceItem2VMSVA.Name = "emptySpaceItem2VMSVA";
            this.emptySpaceItem2VMSVA.Size = new System.Drawing.Size(393, 24);
            this.emptySpaceItem2VMSVA.Text = "emptySpaceItem2VMSVA";
            this.emptySpaceItem2VMSVA.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItemPasswordVA
            // 
            this.layoutControlItemPasswordVA.Control = this.textEditPasswordVA;
            this.layoutControlItemPasswordVA.CustomizationFormText = "layoutControlItemPassword";
            this.layoutControlItemPasswordVA.Location = new System.Drawing.Point(0, 83);
            this.layoutControlItemPasswordVA.Name = "layoutControlItemPassword";
            this.layoutControlItemPasswordVA.Size = new System.Drawing.Size(393, 24);
            this.layoutControlItemPasswordVA.Text = "layoutControlItemPassword";
            this.layoutControlItemPasswordVA.TextSize = new System.Drawing.Size(133, 13);
            // 
            // layoutControlItem1VA
            // 
            this.layoutControlItem1VA.Control = this.textEditPortVA;
            this.layoutControlItem1VA.CustomizationFormText = "layoutControlItemPort";
            this.layoutControlItem1VA.Location = new System.Drawing.Point(0, 107);
            this.layoutControlItem1VA.Name = "layoutControlItem1";
            this.layoutControlItem1VA.Size = new System.Drawing.Size(393, 24);
            this.layoutControlItem1VA.Text = "Port";
            this.layoutControlItem1VA.TextSize = new System.Drawing.Size(133, 13);
            // 
            // tabPageConnection
            // 
            this.tabPageConnection.Controls.Add(this.dbConnectionControl);
            this.tabPageConnection.Name = "tabPageConnection";
            this.tabPageConnection.Padding = new System.Windows.Forms.Padding(2);
            this.tabPageConnection.Size = new System.Drawing.Size(413, 370);
            this.tabPageConnection.Text = "Conexion";
            // 
            // dbConnectionControl
            // 
            this.dbConnectionControl.AVLServer = "";
            this.dbConnectionControl.DataBaseName = "";
            this.dbConnectionControl.DataBaseServer = "";
            this.dbConnectionControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dbConnectionControl.Location = new System.Drawing.Point(2, 2);
            this.dbConnectionControl.MinimumSize = new System.Drawing.Size(269, 154);
            this.dbConnectionControl.Name = "dbConnectionControl";
            this.dbConnectionControl.Size = new System.Drawing.Size(409, 380);
            this.dbConnectionControl.TabIndex = 0;
            this.dbConnectionControl.Load += new System.EventHandler(this.dbConnectionControl_Load);
            // 
            // tabControl
            // 
            this.tabControl.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.tabControl.Location = new System.Drawing.Point(8, 5);
            this.tabControl.Name = "tabControl";
            this.tabControl.Padding = new System.Windows.Forms.Padding(2);
            this.tabControl.SelectedTabPage = this.tabPageConnection;
            this.tabControl.Size = new System.Drawing.Size(418, 320);
            this.tabControl.TabIndex = 2;
            this.tabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tabPageConnection,
            this.xtraTabPageVMS,
            this.xtraTabPageVMSVA,
            this.tabPagePreferences});
            this.tabControl.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tabControl_KeyDown);
            this.tabControl.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tabControl_KeyUp);
            // 
            // tabPagePreferences
            // 
            this.tabPagePreferences.Controls.Add(this.layoutControl1);
            this.tabPagePreferences.Name = "tabPagePreferences";
            this.tabPagePreferences.Size = new System.Drawing.Size(413, 194);
            this.tabPagePreferences.Text = "Preferences";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.textBoxLongitud);
            this.layoutControl1.Controls.Add(this.textBoxLatitud);
            this.layoutControl1.Controls.Add(this.comboBoxLanguages);
            this.layoutControl1.Controls.Add(this.comboBoxMapType);
            this.layoutControl1.Location = new System.Drawing.Point(4, 3);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlLocation;
            this.layoutControl1.Size = new System.Drawing.Size(407, 370);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // textBoxLongitud
            // 
            this.textBoxLongitud.Location = new System.Drawing.Point(92, 118);
            this.textBoxLongitud.Name = "textBoxLongitud";
            this.textBoxLongitud.Size = new System.Drawing.Size(291, 20);
            this.textBoxLongitud.TabIndex = 7;
            // 
            // textBoxLatitud
            // 
            this.textBoxLatitud.Location = new System.Drawing.Point(92, 94);
            this.textBoxLatitud.Name = "textBoxLatitud";
            this.textBoxLatitud.Size = new System.Drawing.Size(291, 20);
            this.textBoxLatitud.TabIndex = 6;
            // 
            // comboBoxLanguages
            // 
            this.comboBoxLanguages.FormattingEnabled = true;
            this.comboBoxLanguages.Location = new System.Drawing.Point(80, 12);
            this.comboBoxLanguages.Name = "comboBoxLanguages";
            this.comboBoxLanguages.Size = new System.Drawing.Size(315, 21);
            this.comboBoxLanguages.TabIndex = 5;
            // 
            // comboBoxMapType
            // 
            this.comboBoxMapType.FormattingEnabled = true;
            this.comboBoxMapType.Location = new System.Drawing.Point(80, 37);
            this.comboBoxMapType.Name = "comboBoxMapType";
            this.comboBoxMapType.Size = new System.Drawing.Size(315, 21);
            this.comboBoxMapType.TabIndex = 4;
            // 
            // layoutControlLocation
            // 
            this.layoutControlLocation.CustomizationFormText = "layoutControlLocation";
            this.layoutControlLocation.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlLocation.GroupBordersVisible = false;
            this.layoutControlLocation.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlMapType,
            this.layoutControlLanguages,
            this.layoutControlGroup3});
            this.layoutControlLocation.Location = new System.Drawing.Point(0, 0);
            this.layoutControlLocation.Name = "layoutControlLocation";
            this.layoutControlLocation.Size = new System.Drawing.Size(407, 189);
            this.layoutControlLocation.Text = "layoutControlLocation";
            this.layoutControlLocation.TextVisible = false;
            // 
            // layoutControlMapType
            // 
            this.layoutControlMapType.Control = this.comboBoxMapType;
            this.layoutControlMapType.CustomizationFormText = "layoutControlMapType";
            this.layoutControlMapType.Location = new System.Drawing.Point(0, 25);
            this.layoutControlMapType.Name = "layoutControlMapType";
            this.layoutControlMapType.Size = new System.Drawing.Size(387, 25);
            this.layoutControlMapType.Text = "Map Type: *";
            this.layoutControlMapType.TextSize = new System.Drawing.Size(65, 13);
            // 
            // layoutControlLanguages
            // 
            this.layoutControlLanguages.Control = this.comboBoxLanguages;
            this.layoutControlLanguages.CustomizationFormText = "layoutControlLanguages";
            this.layoutControlLanguages.Location = new System.Drawing.Point(0, 0);
            this.layoutControlLanguages.Name = "layoutControlLanguages";
            this.layoutControlLanguages.Size = new System.Drawing.Size(387, 25);
            this.layoutControlLanguages.Text = "Languages: *";
            this.layoutControlLanguages.TextSize = new System.Drawing.Size(65, 13);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlLatitud,
            this.layoutControlLongitud});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 50);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(387, 119);
            this.layoutControlGroup3.Text = "Location";
            // 
            // layoutControlLatitud
            // 
            this.layoutControlLatitud.Control = this.textBoxLatitud;
            this.layoutControlLatitud.CustomizationFormText = "layoutControlLatitud";
            this.layoutControlLatitud.Location = new System.Drawing.Point(0, 0);
            this.layoutControlLatitud.Name = "layoutControlLatitud";
            this.layoutControlLatitud.Size = new System.Drawing.Size(363, 24);
            this.layoutControlLatitud.Text = "Latitud: *";
            this.layoutControlLatitud.TextSize = new System.Drawing.Size(65, 13);
            // 
            // layoutControlLongitud
            // 
            this.layoutControlLongitud.Control = this.textBoxLongitud;
            this.layoutControlLongitud.CustomizationFormText = "layoutControlLongitud";
            this.layoutControlLongitud.Location = new System.Drawing.Point(0, 24);
            this.layoutControlLongitud.Name = "layoutControlLongitud";
            this.layoutControlLongitud.Size = new System.Drawing.Size(363, 51);
            this.layoutControlLongitud.Text = "Longitud: *";
            this.layoutControlLongitud.TextSize = new System.Drawing.Size(65, 13);
            // 
            // ConnectionServerConfigurationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(434, 262);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.buttonExCancel);
            this.Controls.Add(this.buttonExAccept);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ConnectionServerConfigurationForm";
            this.Padding = new System.Windows.Forms.Padding(5);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.ConnectionServerConfigurationForm_Load);
            this.xtraTabPageVMS.ResumeLayout(false);
            this.xtraTabPageVMSVA.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlMainVMS)).EndInit();
            this.layoutControlMainVMS.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlMainVMSVA)).EndInit();
            this.layoutControlMainVMSVA.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEditPort.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPassword.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditServer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditLogin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPortVA.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPasswordVA.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditServerVA.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditLoginVA.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkbuttonVA.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupVMS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupVMSVA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemServer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLogin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemServerVA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLoginVA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCheckButtonVA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItemVMS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItemVMSVA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2VMSVA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPasswordVA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1VA)).EndInit();
            this.tabPageConnection.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabControl)).EndInit();
            this.tabControl.ResumeLayout(false);
            this.tabPagePreferences.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlMapType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlLanguages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlLatitud)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlLongitud)).EndInit();
            this.ResumeLayout(false);
            this.Size = new System.Drawing.Size(440,410);
        }

        #endregion

        private SimpleButtonEx buttonExAccept;
        private SimpleButtonEx buttonExCancel;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageVMS;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageVMSVA;
        private DevExpress.XtraLayout.LayoutControl layoutControlMainVMS;
        private DevExpress.XtraLayout.LayoutControl layoutControlMainVMSVA;
        private DevExpress.XtraEditors.TextEdit textEditPassword;
        private DevExpress.XtraEditors.TextEdit textEditServer;
        private DevExpress.XtraEditors.TextEdit textEditLogin;
        private DevExpress.XtraEditors.TextEdit textEditPasswordVA;
        private DevExpress.XtraEditors.TextEdit textEditServerVA;
        private DevExpress.XtraEditors.TextEdit textEditLoginVA;
        private DevExpress.XtraEditors.CheckEdit checkbuttonVA;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupVMS;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupVMSVA;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemServer;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemLogin;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemServerVA;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemLoginVA;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemCheckButtonVA;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItemVMS;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItemVMSVA;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2VMSVA;
        private DevExpress.XtraTab.XtraTabPage tabPageConnection;
        private DbConnectionControl dbConnectionControl;
        private DevExpress.XtraTab.XtraTabControl tabControl;
        private DevExpress.XtraEditors.TextEdit textEditPort;
        private DevExpress.XtraEditors.TextEdit textEditPortVA;
        private DevExpress.XtraTab.XtraTabPage tabPagePreferences;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemPassword;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemPasswordVA;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1VA;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private System.Windows.Forms.TextBox textBoxLongitud;
        private System.Windows.Forms.TextBox textBoxLatitud;
        private System.Windows.Forms.ComboBox comboBoxLanguages;
        private System.Windows.Forms.ComboBox comboBoxMapType;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlLocation;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlMapType;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlLanguages;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlLatitud;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlLongitud;
    }
}