using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Reflection;
using SmartCadserverConfiguration.Gui;
using SmartCadCore.Common;
using SmartCadCore.Core;

namespace SmartCadServerConfiguration.Gui
{
    public partial class DbConnectionControl : DevExpress.XtraEditors.XtraUserControl
    {
        public event EventHandler ParametersChange;
        public bool HasNeededFilds
        {
            get
            {
                return DataBaseServer.Length != 0 &&
                   DataBaseName.Length != 0; //&&
                                             //DataBaseLogin.Length != 0;
            }
        }

        public DbConnectionControl()
        {
            InitializeComponent();
        }

        private void DbConnectionControl_Load(object sender, EventArgs e)
        {
            DbConnectionControlParameters_Change(null, null);
            this.toolTipMain.SetToolTip(this.buttonTelephonyData, ResourceLoader.GetString2("ToolTipButtonTelephonyConfiguration"));
            this.toolTipMain.SetToolTip(this.buttonConfigurateDB, ResourceLoader.GetString2("ToolTipbuttonConfigurateDB"));
            this.toolTipMain.SetToolTip(this.buttonNewDB, ResourceLoader.GetString2("ToolTipButtonNewDB"));
            LoadLenguage();
        }

        private void DbConnectionControlParameters_Change(object sender, System.EventArgs e)
        {

            buttonConfigurateDB.Enabled = HasNeededFilds;
            //buttonNewDB.Enabled = HasNeededFilds;
            if (ParametersChange != null)
                ParametersChange(new object(), new EventArgs());
        }

        private void LoadLenguage()
        {
            layoutControlGroupAVL.Text = ResourceLoader.GetString2("Parser");
            layoutControlGroupReport.Text = ResourceLoader.GetString2("ReportDataBase");
            layoutControlGroupServer.Text = ResourceLoader.GetString2("ApplicationDB");
            labelDBName.Text = ResourceLoader.GetString2("$Message.Name") + ": *"; ;
            labelServer.Text = ResourceLoader.GetString2("Server") + ": *";
            labelReport.Text = ResourceLoader.GetString2("ReportServer") + ": *";
            labelReportName.Text = ResourceLoader.GetString2("ReportName") + ": *";
            labelControlAVLServerText.Text = ResourceLoader.GetString2("Server") + ": *";
            labelControlPortText.Text = ResourceLoader.GetString2("Port") + ": *";
        }

        private void buttonNewDB_Click(object sender, EventArgs e)
        {
            NewDBLogin newDBLogin = new NewDBLogin();
            newDBLogin.ShowDialog();

            if (newDBLogin.DialogResult == DialogResult.OK)
            {
                NewDBForm newDBForm = new NewDBForm(newDBLogin.connection);
                newDBForm.ShowDialog();

                if (newDBForm.DialogResult == DialogResult.OK)
                {
                    DataBaseServer = newDBLogin.Server;
                    DataBaseName = newDBForm.DataBaseName;
                    //DataBaseLogin = newDBLogin.Login;
                    //DataBasePassword = newDBLogin.Password;
                }
            }
            this.toolTipMain.SetToolTip(this.buttonNewDB, ResourceLoader.GetString2("ToolTipButtonNewDB"));
        }

        private void buttonConfigurateDB_Click(object sender, EventArgs e)
        {
            string oldDataBaseServer = SmartCadConfiguration.DataBaseServer;
            string oldDataBaseName = SmartCadConfiguration.DataBaseName;

            try
            {
#if !DEBUG
                if (ApplicationUtil.IsServiceUp("ServerConsole"))
                {
                    throw new Exception("ServiceUpException");
                }
#endif
                SmartCadConfiguration.DataBaseServer = DataBaseServer;
                SmartCadConfiguration.DataBaseName = DataBaseName;
                SmartCadConfiguration.ReportServer = ReportServer;
                SmartCadConfiguration.Save();

                BackgroundProcessForm processForm = new BackgroundProcessForm(ResourceLoader.GetString2("ChekingConnectionDBServer"), this, new MethodInfo[1] { GetType().GetMethod("CreateConnection", BindingFlags.NonPublic | BindingFlags.Instance) }, new object[1][] { new object[0] });
                processForm.CanThrowError = true;
                processForm.ShowDialog();

                SqlConnection connection = (SqlConnection)processForm.Results[0];

                DBConfigurationForm ConfigurateDB = new DBConfigurationForm(connection);

                if (ConfigurateDB.ShowDialog() == DialogResult.Cancel)
                {
                    SmartCadConfiguration.DataBaseServer = oldDataBaseServer;
                    SmartCadConfiguration.DataBaseName = oldDataBaseName;
                    //SmartCadConfiguration.DataBaseLogin = oldDataBaseLogin;
                    //SmartCadConfiguration.DataBasePassword = oldDataBasePassword;

                    SmartCadConfiguration.Save();
                    this.toolTipMain.SetToolTip(this.buttonConfigurateDB, ResourceLoader.GetString2("ToolTipbuttonConfigurateDB"));
                }
            }
            catch (SqlException ex)
            {
                MessageForm.Show(ResourceLoader.GetString2("ErrorConnectionDBServer"), ex);
                SmartCadConfiguration.DataBaseServer = oldDataBaseServer;
                SmartCadConfiguration.DataBaseName = oldDataBaseName;

                SmartCadConfiguration.Save();
            }
            catch (Exception ex)
            {
                if (ex.Message == "ServiceUpException")
                {
                    MessageForm.Show(
                        string.Format(
                            ResourceLoader.GetString2("NoChangesWithServiceUp"),
                            ResourceLoader.GetString2("ServerServiceSmartCad")//+ SmartCadConfiguration.Version
                        ),
                        MessageFormType.Error
                    );
                }
                else
                {
                    MessageForm.Show(ex.Message, MessageFormType.Error);
                }
            }
        }

        public string DataBaseServer
        {
            get
            {
                return textBoxServer.Text.Trim();
            }
            set
            {
                if (value != null)
                    textBoxServer.Text = value.Trim();
            }
        }

        public string DataBaseName
        {
            get
            {
                return textBoxName.Text.Trim();
            }
            set
            {
                textBoxName.Text = value.Trim();
            }
        }

        public string ReportName
        {
            get
            {
                return textBoxReportName.Text.Trim();
            }
            set
            {
                textBoxReportName.Text = value.Trim();
            }
        }

        public string ReportServer
        {
            get
            {
                return textBoxReport.Text.Trim();
            }
            set
            {
                textBoxReport.Text = value.Trim();
            }
        }

        public string AVLServer
		{
			get
			{
				return textEditAVLServer.Text.Trim();
			}
			set
			{
				textEditAVLServer.Text = value.Trim();
			}
		}

		public int AVLPort 
		{
			get
			{
				return int.Parse(textEditPort.Text.Trim());
			}
			set
			{
				textEditPort.Text = value.ToString();
			}
		}

        private SqlConnection CreateConnection()
        {
            string server = "Data Source=";
            string catalog = ";Initial Catalog=";
            string user = ";uid=";
            string pwd = ";pwd=";
            string security = "; Persist Security Info=True";

            server += DataBaseServer;
            catalog += DataBaseName;
            //user += DataBaseLogin;
            //pwd += DataBasePassword;

            string connectionString = server + catalog + user + pwd + security + ";Pooling = false";
            
            //SqlConnection connection = new SqlConnection(connectionString);
            SqlConnection connection = new SqlConnection(SmartCadConfiguration.DataBaseConnectionString);
            connection.Open();
            System.Threading.Thread.Sleep(2000);
            return connection;
        }

        private void buttonTelephonyData_Click(object sender, EventArgs e)
        {
            ServerTelephonyConfigureForm stcf = new ServerTelephonyConfigureForm();
            stcf.ShowDialog();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
