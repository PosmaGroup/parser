using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Net;using SmartCadCore.Common;
using SmartCadCore.Core;




namespace SmartCadServerConfiguration.Gui
{
    public partial class ConnectionServerConfigurationForm : DevExpress.XtraEditors.XtraForm
    {

        public ConnectionServerConfigurationForm()
        {
            InitializeComponent();
            
        }

        private void ConnectionServerConfigurationForm_Load(object sender, EventArgs e)
        {
            dbConnectionControl.DataBaseServer = SmartCadConfiguration.DataBaseServer;
            dbConnectionControl.DataBaseName = SmartCadConfiguration.DataBaseName;
			dbConnectionControl.AVLServer = SmartCadConfiguration.AVLServer;
			dbConnectionControl.AVLPort = SmartCadConfiguration.AVLPort;
            dbConnectionControl.ReportServer = SmartCadConfiguration.ReportServer;
            dbConnectionControl.ReportName = SmartCadConfiguration.ReportName;

            textEditServer.Text = SmartCadConfiguration.SmartCadSection.ServerElement.VmsServerCommunicationElement.Server;
            textEditLogin.Text = SmartCadConfiguration.SmartCadSection.ServerElement.VmsServerCommunicationElement.Login;
            textEditPassword.Text = SmartCadConfiguration.SmartCadSection.ServerElement.VmsServerCommunicationElement.Password;
            textEditPort.Text = SmartCadConfiguration.SmartCadSection.ServerElement.VmsServerCommunicationElement.PortVms;

            checkbuttonVA.Checked = SmartCadConfiguration.SmartCadSection.ServerElement.VideoAnaliticaCommunicationElement.activate;

            if (!checkbuttonVA.Checked)
            {
                textEditServerVA.Enabled = false;
                textEditLoginVA.Enabled = false;
                textEditPasswordVA.Enabled = false;
                textEditPortVA.Enabled = false;
            }

            textEditServerVA.Text = SmartCadConfiguration.SmartCadSection.ServerElement.VideoAnaliticaCommunicationElement.ServerIP;
            textEditLoginVA.Text = SmartCadConfiguration.SmartCadSection.ServerElement.VideoAnaliticaCommunicationElement.ServerUser;
            textEditPasswordVA.Text = SmartCadConfiguration.SmartCadSection.ServerElement.VideoAnaliticaCommunicationElement.ServerPassword;
            textEditPortVA.Text = SmartCadConfiguration.SmartCadSection.ServerElement.VideoAnaliticaCommunicationElement.ServerPort;

            buttonExAccept.Enabled = dbConnectionControl.HasNeededFilds;
            dbConnectionControl.ParametersChange += new EventHandler(dbConnectionControl_ParametersChange);

            string nameLanguage= ApplicationUtil.GetNameLanguageByKey( SmartCadConfiguration.SmartCadSection.CultureElement.CultureLanguageElement.Name +"-"+ SmartCadConfiguration.SmartCadSection.CultureElement.CultureCountryElement.Name);
            string nameMaps = SmartCadConfiguration.SmartCadSection.MapName.Name;

            comboBoxLanguages.Text = nameLanguage;
            if (SmartCadConfiguration.SmartCadSection.ServerElement.Map.Dll.Equals("Smartmatic.SmartCad.Map.MapXtreme"))
            {
                comboBoxMapType.Text = "MapXtreme";
            }
            else if (SmartCadConfiguration.SmartCadSection.ServerElement.Map.Dll.Equals("Smartmatic.SmartCad.Map.Google"))
            {
                comboBoxMapType.Text = "Google Map";
            }

           // LoadLanguage();

            if ((SmartCadConfiguration.SmartCadSection.Maps[nameMaps].CenterLon).Contains(","))
            {
                textBoxLongitud.Text = (SmartCadConfiguration.SmartCadSection.Maps[nameMaps].CenterLon).Replace(",", ".");
            }
            else
            {
                textBoxLongitud.Text = (SmartCadConfiguration.SmartCadSection.Maps[nameMaps].CenterLon);
            }

            if ((SmartCadConfiguration.SmartCadSection.Maps[nameMaps].CenterLon).Contains(","))
            {
                textBoxLatitud.Text = (SmartCadConfiguration.SmartCadSection.Maps[nameMaps].CenterLat).Replace(",", ".");
            }
            else
            {
                textBoxLatitud.Text = (SmartCadConfiguration.SmartCadSection.Maps[nameMaps].CenterLat);
            }

            comboBoxLanguages.Items.Clear();
            comboBoxMapType.Items.Clear();

            comboBoxMapType.Items.Add("Google Map");
            comboBoxMapType.Items.Add("MapXtreme");

            List <string> listLanguagesNames = new List<string>();
            listLanguagesNames = ApplicationUtil.GetLanguages();

            foreach (var item in listLanguagesNames)
            {
                comboBoxLanguages.Items.Add(item);
            }
            
            LoadLanguage();

        }

        private void LoadLanguage()
        {
            buttonExAccept.Text = ResourceLoader.GetString2("$Message.Accept");
            buttonExCancel.Text = ResourceLoader.GetString2("$Message.Cancel");
            tabPageConnection.Text = ResourceLoader.GetString2("Connection");
            this.Text = ResourceLoader.GetString2("ConnectionServerConfiguration");

            xtraTabPageVMS.Text = ResourceLoader.GetString2("VMS");
            layoutControlItemServer.Text = ResourceLoader.GetString2("Server");
            layoutControlItemLogin.Text = ResourceLoader.GetString2("Login");
            layoutControlItemPassword.Text = ResourceLoader.GetString2("Password");
            layoutControlItem1.Text = ResourceLoader.GetString2("Port");

            xtraTabPageVMSVA.Text = ResourceLoader.GetString2("VA");
            layoutControlItemServerVA.Text = ResourceLoader.GetString2("Server");
            layoutControlItemLoginVA.Text = ResourceLoader.GetString2("Login");
            layoutControlItemPasswordVA.Text = ResourceLoader.GetString2("Password");
            layoutControlItem1VA.Text = ResourceLoader.GetString2("Port");
            layoutControlItemCheckButtonVA.Text = ResourceLoader.GetString2("CheckButtonVa");


            layoutControlMapType.Text = ResourceLoader.GetString2("MapType");
            layoutControlLanguages.Text = ResourceLoader.GetString2("Languages");
            layoutControlLatitud.Text = ResourceLoader.GetString2("Latitud");
            layoutControlLongitud.Text = ResourceLoader.GetString2("Longitud");
            layoutControlLocation.Text = ResourceLoader.GetString2("LocationConfigurationServer");
        }

        void dbConnectionControl_ParametersChange(object sender, EventArgs e)
        {
            EvalEnableOk();
        }

        private void buttonExAccept_Click(object sender, EventArgs e)
        {
            SmartCadConfiguration.DataBaseServer = dbConnectionControl.DataBaseServer;
            SmartCadConfiguration.DataBaseName = dbConnectionControl.DataBaseName;
            SmartCadConfiguration.ReportServer = dbConnectionControl.ReportServer;
            SmartCadConfiguration.ReportName = dbConnectionControl.ReportName;
            SmartCadConfiguration.TrustedConnection = "True";
			SmartCadConfiguration.AVLServer = dbConnectionControl.AVLServer;
			SmartCadConfiguration.AVLPort = dbConnectionControl.AVLPort;
            SmartCadConfiguration.ServerAddress = System.Net.Dns.GetHostEntry(Environment.MachineName).AddressList[0].ToString();

           // if (string.IsNullOrEmpty(textEditServer.Text) == false)
          //  {
            SmartCadConfiguration.SmartCadSection.ServerElement.VmsServerCommunicationElement.dllName = "Smartmatic.SmartCad.Vms.Milestone";
            SmartCadConfiguration.SmartCadSection.ServerElement.VmsServerCommunicationElement.Server = textEditServer.Text;
            SmartCadConfiguration.SmartCadSection.ServerElement.VmsServerCommunicationElement.Login = textEditLogin.Text;
            SmartCadConfiguration.SmartCadSection.ServerElement.VmsServerCommunicationElement.Password = textEditPassword.Text;
            SmartCadConfiguration.SmartCadSection.ServerElement.VmsServerCommunicationElement.PortVms = textEditPort.Text;

            SmartCadConfiguration.SmartCadSection.ServerElement.VideoAnaliticaCommunicationElement.activate = checkbuttonVA.Checked;
            SmartCadConfiguration.SmartCadSection.ServerElement.VideoAnaliticaCommunicationElement.ServerIP = textEditServerVA.Text;
            SmartCadConfiguration.SmartCadSection.ServerElement.VideoAnaliticaCommunicationElement.ServerUser = textEditLoginVA.Text;
            SmartCadConfiguration.SmartCadSection.ServerElement.VideoAnaliticaCommunicationElement.ServerPassword = textEditPasswordVA.Text;
            SmartCadConfiguration.SmartCadSection.ServerElement.VideoAnaliticaCommunicationElement.ServerPort = textEditPortVA.Text;

            if (comboBoxMapType.Text.Equals("Google Map"))
            {
                SmartCadConfiguration.SmartCadSection.ServerElement.Map.Dll = "Smartmatic.SmartCad.Map.Google";
            }
            else if (comboBoxMapType.Text.Equals("MapXtreme"))
            {
                SmartCadConfiguration.SmartCadSection.ServerElement.Map.Dll = "Smartmatic.SmartCad.Map.MapXtreme";
            }

            string language = ApplicationUtil.GetKeyLanguagesByName((string) comboBoxLanguages.Text);
            var finalLanguage = language.Split('-');
            SmartCadConfiguration.SmartCadSection.CultureElement.CultureLanguageElement.Name = finalLanguage[0];
            SmartCadConfiguration.SmartCadSection.CultureElement.CultureCountryElement.Name = finalLanguage[1];

            string nameMaps = SmartCadConfiguration.SmartCadSection.MapName.Name;

            if (textBoxLongitud.Text.Contains(","))
            {
                SmartCadConfiguration.SmartCadSection.Maps[nameMaps].CenterLon = (textBoxLongitud.Text.Replace(",", "."));
            }
            else
            {
                SmartCadConfiguration.SmartCadSection.Maps[nameMaps].CenterLon = (textBoxLongitud.Text);
            }

            if (textBoxLatitud.Text.Contains(","))
            {
                SmartCadConfiguration.SmartCadSection.Maps[nameMaps].CenterLat = (textBoxLatitud.Text.Replace(",", "."));
            }
            else
            {
                SmartCadConfiguration.SmartCadSection.Maps[nameMaps].CenterLat = (textBoxLatitud.Text);
            }


            try
            {
#if !DEBUG
                if (ApplicationUtil.IsServiceUp("SmartCad Server Modularized"))
                {
                    throw new Exception("ServiceUpException");
                }
#endif
                SmartCadConfiguration.Save();

                MessageForm.Show(ResourceLoader.GetString2("ChangesSavedSuccessfully"), MessageFormType.Information);

                DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                if(ex.Message.Equals("ServiceUpException"))
                {
                    MessageForm.Show(ResourceLoader.GetString2("NoChangesWithServiceUp", ResourceLoader.GetString2("ServiceSmartCAD")), MessageFormType.Error);
                    DialogResult = DialogResult.None;
                }
                else 
                {                                   
                    MessageForm.Show(ResourceLoader.GetString2("SavingConfigurationError"), ex);
                    DialogResult = DialogResult.None;
                }
            }
        }
                    

        private void tabControl_KeyDown(object sender, KeyEventArgs e)
        {
            EvalEnableOk();
        }

        private void tabControl_KeyUp(object sender, KeyEventArgs e)
        {
            EvalEnableOk();
        }

        private void EvalEnableOk()
        {
            bool enabled =  dbConnectionControl.HasNeededFilds;
            if (string.IsNullOrEmpty(textEditServer.Text) == false)
                enabled &= string.IsNullOrEmpty(textEditServer.Text) == false && string.IsNullOrEmpty(textEditPassword.Text) == false && string.IsNullOrEmpty(textEditLogin.Text) == false;
            if (string.IsNullOrEmpty(textEditServerVA.Text) == false)
                enabled &= string.IsNullOrEmpty(textEditServerVA.Text) == false && string.IsNullOrEmpty(textEditPasswordVA.Text) == false && string.IsNullOrEmpty(textEditLoginVA.Text) == false;
            if (checkbuttonVA.Checked)
                enabled &= string.IsNullOrEmpty(textEditServerVA.Text) == false && string.IsNullOrEmpty(textEditPasswordVA.Text) == false && string.IsNullOrEmpty(textEditLoginVA.Text) == false;
            buttonExAccept.Enabled = enabled;
        }

        private void dbConnectionControl_Load(object sender, EventArgs e)
        {

        }

        private void analitycsActivate(object sender, EventArgs e)
        {
            if (checkbuttonVA.Checked) { 
                textEditServerVA.Enabled = true;
                textEditLoginVA.Enabled = true;
                textEditPasswordVA.Enabled = true;
                textEditPortVA.Enabled = true;
            }else
            {
                textEditServerVA.Enabled = false;
                textEditLoginVA.Enabled = false;
                textEditPasswordVA.Enabled = false;
                textEditPortVA.Enabled = false;
            }
        }
    }
}