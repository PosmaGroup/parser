using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Diagnostics;
using System.Reflection;
using System.Xml;
using System.Xml.Schema;
using System.Xml.XPath;
using SmartCadGuiCommon.Controls;
using SmartCadControls.Controls;
using SmartCadCore.Common;
using SmartCadCore.Core;
using SmartCadGuiCommon;



namespace SmartCadServerConfiguration.Gui
{
    #region Class DBConfigurationForm Documentation
    /// <summary>
    /// TODO:
    /// </summary>
    /// <className>DBConfigurationForm</className>
    /// <author email="atorres@smartmatic.com">Alden Torres</author>
    /// <date>2006/06/16</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
	public class DBConfigurationForm : DevExpress.XtraEditors.XtraForm
	{
		private SimpleButtonEx buttonNewDB;
        private SimpleButtonEx buttonDeleteDB;
		private Separator separator1;
        private SimpleButtonEx buttonCancel;
        private LabelEx labelNewDB;
        private LabelEx labelDeleteDB;
        private LabelEx labelUpgradeDB;
        private Separator separator2;
        private SimpleButtonEx buttonUpgradeDB;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private new System.ComponentModel.Container components = null;

		public DBConfigurationForm(SqlConnection myConnection)
		{
			InitializeComponent();
			this.Icon = ResourceLoader.GetIcon("appIcon");
            this.labelNewDB.Text = ResourceLoader.GetString2("CreateDataBase");
            this.labelUpgradeDB.Text = ResourceLoader.GetString2("UpdateDataBase");
            this.labelDeleteDB.Text = ResourceLoader.GetString2("DeleteDataBase");
            this.Text = ResourceLoader.GetString2("ConfigureDataBase");
			this.buttonCancel.Text = ResourceLoader.GetString2("Close");
			connection = myConnection;

            BackgroundProcessForm processForm = new BackgroundProcessForm(ResourceLoader.GetString2("CheckSquemeDataBase"), this, new MethodInfo[1] { GetType().GetMethod("hasDataObjects", BindingFlags.NonPublic | BindingFlags.Instance) }, new object[1][] { new object[0] });
            processForm.CanThrowError = true;
            processForm.ShowDialog();

            int resultCheck = (int)processForm.Results[0];

            if (resultCheck == 0)
            {
                buttonNewDB.Enabled = false;
                buttonUpgradeDB.Enabled = false;
                buttonDeleteDB.Enabled = true;
            }
            else if (resultCheck == 1)
            {
                buttonNewDB.Enabled = true;
                buttonUpgradeDB.Enabled = false;
                buttonDeleteDB.Enabled = false;
            }
            else if (resultCheck == 2)
            {
                buttonNewDB.Enabled = false;
                buttonUpgradeDB.Enabled = true;
                buttonDeleteDB.Enabled = true;
            }
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DBConfigurationForm));
            this.buttonNewDB = new SimpleButtonEx();
            this.buttonDeleteDB = new SimpleButtonEx();
            this.separator1 = new Separator();
            this.labelNewDB = new LabelEx();
            this.labelDeleteDB = new LabelEx();
            this.buttonCancel = new SimpleButtonEx();
            this.labelUpgradeDB = new LabelEx();
            this.separator2 = new Separator();
            this.buttonUpgradeDB = new SimpleButtonEx();
            this.SuspendLayout();
            // 
            // buttonNewDB
            // 
            this.buttonNewDB.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNewDB.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonNewDB.Appearance.Options.UseFont = true;
            this.buttonNewDB.Appearance.Options.UseForeColor = true;
            this.buttonNewDB.FontFormat = FontFormat.GetFormat("ButtonFormat");
            this.buttonNewDB.Image = ((System.Drawing.Image)(resources.GetObject("buttonNewDB.Image")));
            this.buttonNewDB.Location = new System.Drawing.Point(16, 17);
            this.buttonNewDB.Name = "buttonNewDB";
            this.buttonNewDB.Size = new System.Drawing.Size(48, 52);
            this.buttonNewDB.TabIndex = 4;
            this.buttonNewDB.Click += new System.EventHandler(this.buttonNewDB_Click);
            // 
            // buttonDeleteDB
            // 
            this.buttonDeleteDB.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDeleteDB.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonDeleteDB.Appearance.Options.UseFont = true;
            this.buttonDeleteDB.Appearance.Options.UseForeColor = true;
            this.buttonDeleteDB.Enabled = false;
            this.buttonDeleteDB.FontFormat = FontFormat.GetFormat("ButtonFormat");
            this.buttonDeleteDB.Image = ((System.Drawing.Image)(resources.GetObject("buttonDeleteDB.Image")));
            this.buttonDeleteDB.Location = new System.Drawing.Point(16, 193);
            this.buttonDeleteDB.Name = "buttonDeleteDB";
            this.buttonDeleteDB.Size = new System.Drawing.Size(44, 51);
            this.buttonDeleteDB.TabIndex = 5;
            this.buttonDeleteDB.Click += new System.EventHandler(this.buttonDeleteDB_Click);
            // 
            // separator1
            // 
            this.separator1.BackColor = System.Drawing.Color.DarkGray;
            this.separator1.Location = new System.Drawing.Point(7, 176);
            this.separator1.Name = "separator1";
            this.separator1.Size = new System.Drawing.Size(268, 2);
            this.separator1.TabIndex = 6;
            this.separator1.TransparentColor = System.Drawing.Color.WhiteSmoke;
            // 
            // labelNewDB
            // 
            this.labelNewDB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewDB.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelNewDB.Location = new System.Drawing.Point(80, 22);
            this.labelNewDB.Name = "labelNewDB";
            this.labelNewDB.Size = new System.Drawing.Size(186, 60);
            this.labelNewDB.TabIndex = 7;
            // 
            // labelDeleteDB
            // 
            this.labelDeleteDB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDeleteDB.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelDeleteDB.Location = new System.Drawing.Point(80, 197);
            this.labelDeleteDB.Name = "labelDeleteDB";
            this.labelDeleteDB.Size = new System.Drawing.Size(174, 52);
            this.labelDeleteDB.TabIndex = 8;
            // 
            // buttonCancel
            // 
            this.buttonCancel.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCancel.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonCancel.Appearance.Options.UseFont = true;
            this.buttonCancel.Appearance.Options.UseForeColor = true;
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.FontFormat = FontFormat.GetFormat("ButtonFormat");
            this.buttonCancel.Location = new System.Drawing.Point(203, 252);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(72, 25);
            this.buttonCancel.TabIndex = 30;
            // 
            // labelUpgradeDB
            // 
            this.labelUpgradeDB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUpgradeDB.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelUpgradeDB.Location = new System.Drawing.Point(80, 107);
            this.labelUpgradeDB.Name = "labelUpgradeDB";
            this.labelUpgradeDB.Size = new System.Drawing.Size(186, 56);
            this.labelUpgradeDB.TabIndex = 33;
            // 
            // separator2
            // 
            this.separator2.BackColor = System.Drawing.Color.DarkGray;
            this.separator2.Location = new System.Drawing.Point(7, 85);
            this.separator2.Name = "separator2";
            this.separator2.Size = new System.Drawing.Size(268, 2);
            this.separator2.TabIndex = 32;
            this.separator2.TransparentColor = System.Drawing.Color.WhiteSmoke;
            // 
            // buttonUpgradeDB
            // 
            this.buttonUpgradeDB.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonUpgradeDB.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonUpgradeDB.Appearance.Options.UseFont = true;
            this.buttonUpgradeDB.Appearance.Options.UseForeColor = true;
            this.buttonUpgradeDB.Enabled = false;
            this.buttonUpgradeDB.FontFormat = FontFormat.GetFormat("ButtonFormat");
            this.buttonUpgradeDB.Image = ((System.Drawing.Image)(resources.GetObject("buttonUpgradeDB.Image")));
            this.buttonUpgradeDB.Location = new System.Drawing.Point(16, 102);
            this.buttonUpgradeDB.Name = "buttonUpgradeDB";
            this.buttonUpgradeDB.Size = new System.Drawing.Size(44, 52);
            this.buttonUpgradeDB.TabIndex = 31;
            this.buttonUpgradeDB.Click += new System.EventHandler(this.buttonUpgradeDB_Click);
            // 
            // DBConfigurationForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(287, 289);
            this.Controls.Add(this.labelUpgradeDB);
            this.Controls.Add(this.separator2);
            this.Controls.Add(this.buttonUpgradeDB);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.labelDeleteDB);
            this.Controls.Add(this.labelNewDB);
            this.Controls.Add(this.separator1);
            this.Controls.Add(this.buttonNewDB);
            this.Controls.Add(this.buttonDeleteDB);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.LookAndFeel.SkinName = "Blue";
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DBConfigurationForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.TopMost = true;
            this.ResumeLayout(false);

		}
		#endregion
        
        
        private void CreateDataObjects(ChangeTaskDelegate changeTaskFunction, ChangeValueDelegate changeValueFunction, object [] arguments)
		{
            try
            {
                changeValueFunction(10);
                changeTaskFunction(ResourceLoader.GetString2("InitializeService"));

                SmartCadDatabase.ReInit();
                
                changeValueFunction(50);
                changeTaskFunction(ResourceLoader.GetString2("CreateSquemeDataBase"));
                SmartCadDatabase.CreateDataBaseSchema();

                changeValueFunction(90);
                changeTaskFunction(ResourceLoader.GetString2("LoadingInitialData"));
                SmartCadInitialData.CreateInitialData(changeTaskFunction, changeValueFunction);

                changeValueFunction(100);
            }
            catch (Exception ex)
            {
                throw ex;
            }
		}

		private void buttonNewDB_Click(object sender, System.EventArgs e)
		{
            try
			{
				ProgressForm progressForm = new ProgressForm (new TaskFunctionsDelegate (CreateDataObjects),
                    new object[0], ResourceLoader.GetString2("ConfiguringDataBase"));
				progressForm.CanThrowError = true;
				progressForm.ShowDialog();	
				buttonNewDB.Enabled = false;
                buttonUpgradeDB.Enabled = false;
				buttonDeleteDB.Enabled = true;
                MessageForm.Show(ResourceLoader.GetString2("CreateDataRepository"), MessageFormType.Information);
			}
			catch (Exception ex)
			{
				MessageForm.Show(ex.Message,ex);
			}
	 	}

		private void buttonDeleteDB_Click(object sender, System.EventArgs e)
		{
            try
			{
				ProgressForm processForm = new ProgressForm (new TaskFunctionsDelegate (DeleteDB),
                    new object[0], ResourceLoader.GetString2("DeleteDataBase"));
				processForm.CanThrowError = true;
				processForm.ShowDialog();	
				buttonNewDB.Enabled = true;
                buttonUpgradeDB.Enabled = false;
				buttonDeleteDB.Enabled = false;
                MessageForm.Show(ResourceLoader.GetString2("DeleteDataRepository"), MessageFormType.Information);
			}
			catch (Exception ex)
			{
				MessageForm.Show(ex.Message,ex);
			}
		}

        private void DeleteDB(ChangeTaskDelegate changeTaskFunction, ChangeValueDelegate changeValueFunction, object[] arguments)
        {
            changeValueFunction(40);
            changeTaskFunction(ResourceLoader.GetString2("InitializeService"));
            SmartCadDatabase.ReInit();

            changeTaskFunction(ResourceLoader.GetString2("DeletingDataBase"));
            changeValueFunction(80);
            SmartCadDatabase.DeleteDataBaseSchema();

            changeValueFunction(100);
        }

        private int hasDataObjects()
        {
            ArrayList tableNames = new ArrayList();
			int resultCheck = 0;
            SqlCommand myCommand = null;
			SqlDataReader myReader = null;
            string sql = string.Empty;
            try
            {
                sql = File.ReadAllText(SmartCadConfiguration.CheckSchemaDataDB);
            }
            catch
            {
                MessageForm.Show("CanNotReadCheckFile", MessageFormType.Warning);
                resultCheck = 1;
            }
			try
			{
				myCommand = new SqlCommand(sql,connection);
				myReader = myCommand.ExecuteReader();
                if (myReader.Read())
                {
                    if (myReader[0].ToString().Length > 0)
                    {
                        if (myReader[0].ToString() == "0")
                        {
                            resultCheck = 1;
                        }
                        else
                        {
                            SmartLogger.Print(myReader[0].ToString());
                            resultCheck = 2;
                        }
                    }
                }
			}
			catch (Exception ex)
			{
				MessageForm.Show(ex.Message, ex);
			}
			finally
			{
                if (myReader != null)
                {
                    myReader.Close();
                    myReader.Dispose();
                }

                if (myCommand != null)
                    myCommand.Dispose();
			}
            System.Threading.Thread.Sleep(2000);
            return resultCheck;
        }

        private SqlConnection connection = null;

        private void buttonUpgradeDB_Click(object sender, EventArgs e)
        {
            try
            {
                ProgressForm processForm = new ProgressForm(new TaskFunctionsDelegate(UpgradeDB),
                    new object[0], ResourceLoader.GetString2("UpdateStructureDataBase"));
                processForm.CanThrowError = true;
                processForm.ShowDialog();
                buttonNewDB.Enabled = false;
                buttonUpgradeDB.Enabled = false;
                buttonDeleteDB.Enabled = true;
                MessageForm.Show(ResourceLoader.GetString2("UpdatedDataBaseSuccessfully"), MessageFormType.Information);
            }
            catch (Exception ex)
            {
                MessageForm.Show(ex.Message, ex);
            }
        }

        private void UpgradeDB(ChangeTaskDelegate changeTaskFunction, ChangeValueDelegate changeValueFunction, object[] arguments)
        {
            changeValueFunction(10);
            changeTaskFunction(ResourceLoader.GetString2("InitializeService"));
            SmartCadDatabase.ReInit();

            changeTaskFunction(ResourceLoader.GetString2("UpdateSquemeDataBase"));
            changeValueFunction(50);
            SmartCadDatabase.UpgradeDatabaseSchema();

            changeValueFunction(100);
        }
	}
}
