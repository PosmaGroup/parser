using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Reflection;
using System.Threading;
using DevExpress.XtraEditors;
using SmartCadCore.Common;


namespace SmartCadServerConfiguration.Gui
{
	public enum BackgoundProcessFormDisplayType
	{
		HourGlass = 0,
		ProgressBar = 1
	}
	/// <summary>
	/// Summary description for BackgroundProcessForm.
	/// </summary>
	public class BackgroundProcessForm : XtraForm
	{
        private Thread backgroundThread;
		private bool canThrowError = false;
		private bool canShowError = true;
		private bool showError = false;
		private Exception error = null;
		private object targetObject = null;
		private MethodInfo[] methods = null;
		private object[][] arguments = null;
		private object[] results = null;
		private System.Windows.Forms.Panel panelHourglass;
		private System.Windows.Forms.ProgressBar progressBar;
		private System.Windows.Forms.Label labelProgressBar;		
		private System.Windows.Forms.Label label;
		private System.Windows.Forms.PictureBox pictureBox;
		private System.Windows.Forms.ToolTip toolTip;
		private System.Windows.Forms.Panel panelProgressBar;
		private new System.ComponentModel.IContainer components;

        private delegate void SimpleDelegate();

		private BackgroundProcessForm()
		{
			FontFix();
			InitializeComponent();
            LoadLanguage();
		}
		private BackgoundProcessFormDisplayType backgoundProcessFormDisplayType = BackgoundProcessFormDisplayType.HourGlass;
		private BackgoundProcessFormDisplayType BackgoundProcessFormDisplayType
		{
			get{return backgoundProcessFormDisplayType;}
			set 
			{
				backgoundProcessFormDisplayType = value;
				if(value == BackgoundProcessFormDisplayType.HourGlass)
				{
					panelHourglass.Visible = true;
					panelHourglass.BringToFront();
					panelHourglass.Dock = DockStyle.Fill; 					
					this.Size = new Size(220, 90);
				}
				else
				{
					panelHourglass.Visible = false;
					panelHourglass.Dock = DockStyle.None; 
				}
				if(value == BackgoundProcessFormDisplayType.ProgressBar)
				{
					panelProgressBar.Visible = true;
					panelProgressBar.BringToFront();
					panelProgressBar.Dock = DockStyle.Fill; 
					this.Size = new Size(312, 92);					
				}
				else
				{
					panelProgressBar.Dock = DockStyle.None; 
					panelProgressBar.Visible = false;
				}
			}
		}

		private string[] displayedText = new string[0]{};
		public string[] DisplayedText
		{
			get
			{
				return displayedText;
			}
			set
			{
				displayedText = value;
			}
		}
		public BackgroundProcessForm(object targetObjectI, MethodInfo[] methodsI, object[][] argumentsI, string[] displayedTextP) :
			this(targetObjectI, methodsI, argumentsI)
		{
			this.BackgoundProcessFormDisplayType = BackgoundProcessFormDisplayType.ProgressBar;			
			if(methodsI.Length != displayedTextP.Length)
                throw new Exception(ResourceLoader.GetString2("InvalidArguments"));
			else
			{				
				this.progressBar.Maximum = 10 * methods.Length;
				DisplayedText = displayedTextP;				
			}
		}
		public BackgroundProcessForm(object targetObjectI, MethodInfo[] methodsI, object[][] argumentsI) :
			this()
		{
			this.BackgoundProcessFormDisplayType = BackgoundProcessFormDisplayType.HourGlass;
			targetObject = targetObjectI;
			methods = methodsI;
			arguments = argumentsI;
			if((methods.Length!=arguments.Length))
                throw new Exception(ResourceLoader.GetString2("InvalidArguments"));
			else
			{
				if(targetObject!=null)
				{
					Type targetType = targetObject.GetType();
					foreach(MethodInfo method in methods)
					{
                        
						if(targetType!=method.DeclaringType && !targetType.IsSubclassOf(method.DeclaringType) && !method.IsStatic)
                            throw new Exception(ResourceLoader.GetString2("InvalidArguments"));
					}
				}
				else
				{
					foreach(MethodInfo method in methods)
					{
						if(!method.IsStatic)
                            throw new Exception(ResourceLoader.GetString2("InvalidArguments"));
					}
				}
			}
		}
		public BackgroundProcessForm(string messageI, Image imageI, object targetObjectI, MethodInfo[] methodsI, object[][] argumentsI) :
			this(targetObjectI,methodsI,argumentsI)
		{
			label.Text = messageI;
			pictureBox.Image = imageI;
		}

        public BackgroundProcessForm(string messageI, object targetObjectI, MethodInfo[] methodsI, object[][] argumentsI)
            :this(targetObjectI, methodsI, argumentsI)
        {
            label.Text = messageI;
        }	

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		protected override void OnVisibleChanged( EventArgs e )
		{
			base.OnVisibleChanged( e );
			if(Visible==true)
			{
				backgroundThread = new Thread(new ThreadStart(backgroundThreadMethod));
				backgroundThread.IsBackground = true;
				backgroundThread.Start();
			}
		}
		private bool validClose = false;
		private new void Close()
		{
			validClose = true;
			base.Close();
		}
		protected override void OnClosing( CancelEventArgs e )
		{
			e.Cancel = (!validClose);
			base.OnClosing(e);
		}
		protected override void OnClosed( EventArgs e )
		{
			base.OnClosed(e);
		}
		protected override void OnPaint(PaintEventArgs e)
		{
			base.OnPaint(e);
		}
		public new void ShowDialog()
		{
			base.ShowDialog();
			if(showError && canShowError)
			{
				Opacity = 0;
				MessageForm.Show(error.Message,error);
			}
			else if(showError && canThrowError)
			{
				Opacity = 0;
				throw error;
			}
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BackgroundProcessForm));
            this.panelHourglass = new System.Windows.Forms.Panel();
            this.label = new System.Windows.Forms.Label();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.labelProgressBar = new System.Windows.Forms.Label();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.panelProgressBar = new System.Windows.Forms.Panel();
            this.panelHourglass.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.panelProgressBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelHourglass
            // 
            this.panelHourglass.Controls.Add(this.label);
            this.panelHourglass.Controls.Add(this.pictureBox);
            this.panelHourglass.Location = new System.Drawing.Point(60, 284);
            this.panelHourglass.Name = "panelHourglass";
            this.panelHourglass.Size = new System.Drawing.Size(214, 74);
            this.panelHourglass.TabIndex = 5;
            // 
            // label
            // 
            this.label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label.Location = new System.Drawing.Point(72, 0);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(142, 74);
            this.label.TabIndex = 6;
            this.label.Text = "";
            this.label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pictureBox
            // 
            this.pictureBox.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox.Image")));
            this.pictureBox.Location = new System.Drawing.Point(0, 0);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(72, 74);
            this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox.TabIndex = 5;
            this.pictureBox.TabStop = false;
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(12, 13);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(284, 21);
            this.progressBar.TabIndex = 0;
            // 
            // labelProgressBar
            // 
            this.labelProgressBar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelProgressBar.Location = new System.Drawing.Point(8, 52);
            this.labelProgressBar.Name = "labelProgressBar";
            this.labelProgressBar.Size = new System.Drawing.Size(292, 17);
            this.labelProgressBar.TabIndex = 7;
            this.labelProgressBar.Text = "";
            this.labelProgressBar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panelProgressBar
            // 
            this.panelProgressBar.Controls.Add(this.progressBar);
            this.panelProgressBar.Controls.Add(this.labelProgressBar);
            this.panelProgressBar.Location = new System.Drawing.Point(168, 43);
            this.panelProgressBar.Name = "panelProgressBar";
            this.panelProgressBar.Size = new System.Drawing.Size(308, 86);
            this.panelProgressBar.TabIndex = 8;
            // 
            // BackgroundProcessForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.ClientSize = new System.Drawing.Size(750, 462);
            this.Controls.Add(this.panelProgressBar);
            this.Controls.Add(this.panelHourglass);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BackgroundProcessForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.BackgroundProcessForm_FormClosing);
            this.panelHourglass.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.panelProgressBar.ResumeLayout(false);
            this.ResumeLayout(false);

		}
		#endregion

        private void LoadLanguage()
        {
            this.label.Text = ResourceLoader.GetString2("WaitAMoment");
            this.labelProgressBar.Text = ResourceLoader.GetString2("WaitAMoment");
        }
		public object[] Results
		{
			get{ return results;}
		}
		private delegate void toolTip_SetToolTip(Control control, string s);
		private void backgroundThreadMethod()
		{
			try 
			{
				results = new object[methods.Length];
				for(int i = 0; i < methods.Length; i++)
				{
					if(BackgoundProcessFormDisplayType == BackgoundProcessFormDisplayType.ProgressBar)
					{
                        if (progressBar.InvokeRequired == true)
                        {
                            progressBar.Invoke(new SimpleDelegate(delegate
                            {
                                progressBar.PerformStep();
                            }));
                        }
                        else
                            progressBar.PerformStep();						
						string[] strArr = DisplayedText[i].Split(new char[1]{' '});
						string str = "";
						string lastStr = "";
                        bool exit = false;
                        for (int j = 0; j < strArr.Length && (exit == false); j++ )
                        {
                            string s = strArr[j];
                            str += " " + s;
                            if (labelProgressBar.InvokeRequired == true)
                            {
                                labelProgressBar.Invoke(new SimpleDelegate(delegate
                                {
                                    if (Graphics.FromHwnd(labelProgressBar.Handle).MeasureString(str, labelProgressBar.Font).Width >= labelProgressBar.Size.Width - 10)
                                    {
                                        str = lastStr + "...";
                                        exit = true;
                                    }
                                    lastStr = str;
                                }));
                            }
                            else
                            {
                                if (Graphics.FromHwnd(labelProgressBar.Handle).MeasureString(str, labelProgressBar.Font).Width >= labelProgressBar.Size.Width - 10)
                                {
                                    str = lastStr + "...";
                                    exit = true;
                                }
                                lastStr = str;
                            }
                        }

						if(this.InvokeRequired)
							BeginInvoke(new toolTip_SetToolTip(toolTip.SetToolTip), new object[2]{labelProgressBar, DisplayedText[i]});
						else
							toolTip.SetToolTip(labelProgressBar, DisplayedText[i]);
						if(str.IndexOf("...")==-1)
							str = str + "...";
                        if (labelProgressBar.InvokeRequired == true)
                        {
                            labelProgressBar.Invoke(new SimpleDelegate(delegate
                            {
                                labelProgressBar.Text = str;
                            }));
                        }
                        else
                            labelProgressBar.Text = str;

						
					}
					results[i] = methods[i].Invoke(targetObject,arguments[i]);
				}
			}
			catch(Exception ex)
			{
//				if(ex.InnerException.InnerException!=null)
//					Error = ex.InnerException.InnerException;
//				else
//					Error = ex.InnerException;
                if (ex.InnerException != null)
                {
                    if (ex.InnerException.InnerException != null)
                        Error = ex.InnerException.InnerException;
                    else
                        Error = ex.InnerException;
                }
                else
                    Error = ex;
			}
			finally
			{
                try
                {
                    BeginInvoke(new MethodInvoker(Close));
                }
                catch
                { }
			}
		}

		private Exception Error
		{
			set
			{
				error = value;
				showError = true;
			}
		}
		public bool WasAnError
		{
			get{ return showError;}
		}

		public bool CanShowError
		{
			get{return canShowError;}
			set
			{
				canShowError=value;
				if(canShowError)
					canThrowError = false;
			}
		}
		public bool CanThrowError
		{
			get{return canThrowError;}
			set
			{
				canThrowError=value;
				if(canThrowError)
					canShowError = false;
			}
		}

        public bool CanCancel { get; set; }

		#region Font Fix

		[System.Runtime.InteropServices.DllImport("msvcr120.dll")]
		public static extern uint _controlfp(uint controlword, uint mask);

		public static void FontFix()
		{
			const uint controlword = 0x0008001F; // _MCW_EM
			const uint mask = 0x00000010; // _EM_INVALID
			_controlfp(controlword, mask);
		}

		#endregion

        private void BackgroundProcessForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (CanCancel)
            {
                try
                {
                    if (backgroundThread != null && backgroundThread.IsAlive)
                    {
                        CanShowError = false;
                        CanThrowError = false;
                        backgroundThread.Abort();
                    }
                }
                catch
                { }
                validClose = true;
            }
        }

        private const int CP_NOCLOSE_BUTTON = 0x200;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        } 
	}
}
