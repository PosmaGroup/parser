namespace SmartCadServerConfiguration.Gui
{
    partial class DbConnectionControl
    {
        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DbConnectionControl));
            this.buttonTelephonyData = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.textEditPort = new DevExpress.XtraEditors.TextEdit();
            this.labelControlPortText = new DevExpress.XtraEditors.LabelControl();
            this.textEditAVLServer = new DevExpress.XtraEditors.TextEdit();
            this.labelControlAVLServerText = new DevExpress.XtraEditors.LabelControl();
            this.buttonConfigurateDB = new DevExpress.XtraEditors.SimpleButton();
            this.labelDBName = new DevExpress.XtraEditors.LabelControl();
            this.textBoxName = new DevExpress.XtraEditors.TextEdit();
            this.labelReport = new DevExpress.XtraEditors.LabelControl();
            this.textBoxReport = new DevExpress.XtraEditors.TextEdit();
            this.labelReportName = new DevExpress.XtraEditors.LabelControl();
            this.textBoxReportName = new DevExpress.XtraEditors.TextEdit();
            this.labelServer = new DevExpress.XtraEditors.LabelControl();
            this.textBoxServer = new DevExpress.XtraEditors.TextEdit();
            this.buttonNewDB = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroupServer = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemDBText = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemServerText = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemServer = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemDBName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemNewDB = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemConfigurateDB = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemTelephony = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupAVL = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemAVLName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemAVLText = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItemPortText = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemPort = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroupReport = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemReport = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemReportText = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemReportNameText = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemReportName = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.toolTipMain = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPort.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditAVLServer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxReport.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxReportName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxServer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupServer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDBText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemServerText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemServer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDBName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemNewDB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemConfigurateDB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTelephony)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupAVL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAVLName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAVLText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPortText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPort)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemReportText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemReportNameText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemReportName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonTelephonyData
            // 
            this.buttonTelephonyData.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonTelephonyData.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonTelephonyData.Appearance.Options.UseFont = true;
            this.buttonTelephonyData.Appearance.Options.UseForeColor = true;
            this.buttonTelephonyData.Image = ((System.Drawing.Image)(resources.GetObject("buttonTelephonyData.Image")));
            this.buttonTelephonyData.Location = new System.Drawing.Point(316, 27);
            this.buttonTelephonyData.Name = "buttonTelephonyData";
            this.buttonTelephonyData.Size = new System.Drawing.Size(35, 31);
            this.buttonTelephonyData.StyleController = this.layoutControl1;
            this.buttonTelephonyData.TabIndex = 20;
            this.toolTipMain.SetToolTip(this.buttonTelephonyData, "Configurar parametros de servidor de tel");
            this.buttonTelephonyData.Click += new System.EventHandler(this.buttonTelephonyData_Click);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.textEditPort);
            this.layoutControl1.Controls.Add(this.labelControlPortText);
            this.layoutControl1.Controls.Add(this.textEditAVLServer);
            this.layoutControl1.Controls.Add(this.labelControlAVLServerText);
            this.layoutControl1.Controls.Add(this.buttonTelephonyData);
            this.layoutControl1.Controls.Add(this.buttonConfigurateDB);
            this.layoutControl1.Controls.Add(this.labelDBName);
            this.layoutControl1.Controls.Add(this.textBoxName);
            this.layoutControl1.Controls.Add(this.labelReport);
            this.layoutControl1.Controls.Add(this.textBoxReport);
            this.layoutControl1.Controls.Add(this.labelReportName);
            this.layoutControl1.Controls.Add(this.textBoxReportName);
            this.layoutControl1.Controls.Add(this.labelServer);
            this.layoutControl1.Controls.Add(this.textBoxServer);
            this.layoutControl1.Controls.Add(this.buttonNewDB);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(375, 190);
            this.layoutControl1.TabIndex = 2;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // textEditPort
            // 
            this.textEditPort.Location = new System.Drawing.Point(104, 246);
            this.textEditPort.Name = "textEditPort";
            this.textEditPort.Size = new System.Drawing.Size(168, 20);
            this.textEditPort.StyleController = this.layoutControl1;
            this.textEditPort.TabIndex = 24;
            // 
            // labelControlPortText
            // 
            this.labelControlPortText.Location = new System.Drawing.Point(12, 246);
            this.labelControlPortText.Name = "labelControlPortText";
            this.labelControlPortText.Size = new System.Drawing.Size(42, 13);
            this.labelControlPortText.StyleController = this.layoutControl1;
            this.labelControlPortText.TabIndex = 23;
            this.labelControlPortText.Text = "PortText";
            // 
            // textEditAVLServer
            // 
            this.textEditAVLServer.Location = new System.Drawing.Point(104, 222);
            this.textEditAVLServer.Name = "textEditAVLServer";
            this.textEditAVLServer.Size = new System.Drawing.Size(168, 20);
            this.textEditAVLServer.StyleController = this.layoutControl1;
            this.textEditAVLServer.TabIndex = 22;
            // 
            // labelControlAVLServerText
            // 
            this.labelControlAVLServerText.Location = new System.Drawing.Point(12, 222);
            this.labelControlAVLServerText.Name = "labelControlAVLServerText";
            this.labelControlAVLServerText.Size = new System.Drawing.Size(72, 13);
            this.labelControlAVLServerText.StyleController = this.layoutControl1;
            this.labelControlAVLServerText.TabIndex = 21;
            this.labelControlAVLServerText.Text = "AVLServerText";
            // 
            // buttonConfigurateDB
            // 
            this.buttonConfigurateDB.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonConfigurateDB.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonConfigurateDB.Appearance.Options.UseFont = true;
            this.buttonConfigurateDB.Appearance.Options.UseForeColor = true;
            this.buttonConfigurateDB.Enabled = false;
            this.buttonConfigurateDB.Image = ((System.Drawing.Image)(resources.GetObject("buttonConfigurateDB.Image")));
            this.buttonConfigurateDB.Location = new System.Drawing.Point(316, 62);
            this.buttonConfigurateDB.Name = "buttonConfigurateDB";
            this.buttonConfigurateDB.Size = new System.Drawing.Size(35, 31);
            this.buttonConfigurateDB.StyleController = this.layoutControl1;
            this.buttonConfigurateDB.TabIndex = 13;
            this.toolTipMain.SetToolTip(this.buttonConfigurateDB, "Configurar base de datos");
            this.buttonConfigurateDB.Click += new System.EventHandler(this.buttonConfigurateDB_Click);
            // 
            // labelDBName
            // 
            this.labelDBName.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDBName.Location = new System.Drawing.Point(7, 62);
            this.labelDBName.Name = "labelDBName";
            this.labelDBName.Size = new System.Drawing.Size(88, 31);
            this.labelDBName.StyleController = this.layoutControl1;
            this.labelDBName.TabIndex = 17;
            this.labelDBName.Text = "DataBase";
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(99, 62);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(174, 20);
            this.textBoxName.StyleController = this.layoutControl1;
            this.textBoxName.TabIndex = 11;
            this.textBoxName.TextChanged += new System.EventHandler(this.DbConnectionControlParameters_Change);
            // 
            // labelReport
            // 
            this.labelReport.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelReport.Location = new System.Drawing.Point(12, 122);
            this.labelReport.Name = "labelReport";
            this.labelReport.Size = new System.Drawing.Size(88, 31);
            this.labelReport.StyleController = this.layoutControl1;
            this.labelReport.TabIndex = 17;
            this.labelReport.Text = "DataBase";
            // 
            // textBoxReport
            // 
            this.textBoxReport.Location = new System.Drawing.Point(104, 122);
            this.textBoxReport.Name = "textBoxReport";
            this.textBoxReport.Size = new System.Drawing.Size(242, 20);
            this.textBoxReport.StyleController = this.layoutControl1;
            this.textBoxReport.TabIndex = 11;
            this.textBoxReport.TextChanged += new System.EventHandler(this.DbConnectionControlParameters_Change);
            // 
            // labelReportName
            // 
            this.labelReportName.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelReportName.Location = new System.Drawing.Point(12, 157);
            this.labelReportName.Name = "labelReportName";
            this.labelReportName.Size = new System.Drawing.Size(88, 31);
            this.labelReportName.StyleController = this.layoutControl1;
            this.labelReportName.TabIndex = 17;
            this.labelReportName.Text = "DataBase";
            // 
            // textBoxReportName
            // 
            this.textBoxReportName.Location = new System.Drawing.Point(104, 157);
            this.textBoxReportName.Name = "textBoxReportName";
            this.textBoxReportName.Size = new System.Drawing.Size(242, 20);
            this.textBoxReportName.StyleController = this.layoutControl1;
            this.textBoxReportName.TabIndex = 11;
            this.textBoxReportName.TextChanged += new System.EventHandler(this.DbConnectionControlParameters_Change);
            // 
            // labelServer
            // 
            this.labelServer.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelServer.Location = new System.Drawing.Point(7, 27);
            this.labelServer.Name = "labelServer";
            this.labelServer.Size = new System.Drawing.Size(59, 31);
            this.labelServer.StyleController = this.layoutControl1;
            this.labelServer.TabIndex = 16;
            this.labelServer.Text = "ServerText";
            // 
            // textBoxServer
            // 
            this.textBoxServer.Location = new System.Drawing.Point(99, 27);
            this.textBoxServer.Name = "textBoxServer";
            this.textBoxServer.Size = new System.Drawing.Size(174, 20);
            this.textBoxServer.StyleController = this.layoutControl1;
            this.textBoxServer.TabIndex = 10;
            this.textBoxServer.TextChanged += new System.EventHandler(this.DbConnectionControlParameters_Change);
            // 
            // buttonNewDB
            // 
            this.buttonNewDB.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNewDB.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonNewDB.Appearance.Options.UseFont = true;
            this.buttonNewDB.Appearance.Options.UseForeColor = true;
            this.buttonNewDB.Image = ((System.Drawing.Image)(resources.GetObject("buttonNewDB.Image")));
            this.buttonNewDB.Location = new System.Drawing.Point(277, 62);
            this.buttonNewDB.Name = "buttonNewDB";
            this.buttonNewDB.Size = new System.Drawing.Size(35, 31);
            this.buttonNewDB.StyleController = this.layoutControl1;
            this.buttonNewDB.TabIndex = 12;
            this.toolTipMain.SetToolTip(this.buttonNewDB, "Crear nueva base de datos");
            this.buttonNewDB.Click += new System.EventHandler(this.buttonNewDB_Click);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupServer});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(358, 278);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroupServer
            // 
            this.layoutControlGroupServer.CustomizationFormText = "layoutControlGroupServer";
            this.layoutControlGroupServer.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemDBText,
            this.layoutControlItemServerText,
            this.layoutControlItemServer,
            this.layoutControlItemDBName,
            this.layoutControlItemNewDB,
            this.layoutControlItemConfigurateDB,
            this.layoutControlItemTelephony,
            this.layoutControlGroupAVL,
            this.layoutControlGroupReport,
            this.emptySpaceItem1,
            this.emptySpaceItem2});
            this.layoutControlGroupServer.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupServer.Name = "layoutControlGroupServer";
            this.layoutControlGroupServer.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupServer.Size = new System.Drawing.Size(358, 278);
            this.layoutControlGroupServer.Text = "layoutControlGroupServer";
            // 
            // layoutControlItemDBText
            // 
            this.layoutControlItemDBText.Control = this.labelDBName;
            this.layoutControlItemDBText.CustomizationFormText = "layoutControlItemDBText";
            this.layoutControlItemDBText.Location = new System.Drawing.Point(0, 35);
            this.layoutControlItemDBText.MaxSize = new System.Drawing.Size(92, 35);
            this.layoutControlItemDBText.MinSize = new System.Drawing.Size(92, 35);
            this.layoutControlItemDBText.Name = "layoutControlItemDBText";
            this.layoutControlItemDBText.Size = new System.Drawing.Size(92, 35);
            this.layoutControlItemDBText.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemDBText.Text = "layoutControlItemDBText";
            this.layoutControlItemDBText.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemDBText.TextToControlDistance = 0;
            this.layoutControlItemDBText.TextVisible = false;
            // 
            // layoutControlItemServerText
            // 
            this.layoutControlItemServerText.Control = this.labelServer;
            this.layoutControlItemServerText.CustomizationFormText = "layoutControlItemServerText";
            this.layoutControlItemServerText.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemServerText.MaxSize = new System.Drawing.Size(63, 35);
            this.layoutControlItemServerText.MinSize = new System.Drawing.Size(63, 35);
            this.layoutControlItemServerText.Name = "layoutControlItemServerText";
            this.layoutControlItemServerText.Size = new System.Drawing.Size(63, 35);
            this.layoutControlItemServerText.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemServerText.Text = "layoutControlItemServerText";
            this.layoutControlItemServerText.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemServerText.TextToControlDistance = 0;
            this.layoutControlItemServerText.TextVisible = false;
            // 
            // layoutControlItemServer
            // 
            this.layoutControlItemServer.Control = this.textBoxServer;
            this.layoutControlItemServer.CustomizationFormText = "layoutControlItemServer";
            this.layoutControlItemServer.Location = new System.Drawing.Point(92, 0);
            this.layoutControlItemServer.Name = "layoutControlItemServer";
            this.layoutControlItemServer.Size = new System.Drawing.Size(178, 35);
            this.layoutControlItemServer.Text = "layoutControlItemServer";
            this.layoutControlItemServer.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemServer.TextToControlDistance = 0;
            this.layoutControlItemServer.TextVisible = false;
            // 
            // layoutControlItemDBName
            // 
            this.layoutControlItemDBName.Control = this.textBoxName;
            this.layoutControlItemDBName.CustomizationFormText = "layoutControlItemDBName";
            this.layoutControlItemDBName.Location = new System.Drawing.Point(92, 35);
            this.layoutControlItemDBName.Name = "layoutControlItemDBName";
            this.layoutControlItemDBName.Size = new System.Drawing.Size(178, 35);
            this.layoutControlItemDBName.Text = "layoutControlItemDBName";
            this.layoutControlItemDBName.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemDBName.TextToControlDistance = 0;
            this.layoutControlItemDBName.TextVisible = false;
            // 
            // layoutControlItemNewDB
            // 
            this.layoutControlItemNewDB.Control = this.buttonNewDB;
            this.layoutControlItemNewDB.CustomizationFormText = "layoutControlItemNewDB";
            this.layoutControlItemNewDB.Location = new System.Drawing.Point(270, 35);
            this.layoutControlItemNewDB.MaxSize = new System.Drawing.Size(39, 35);
            this.layoutControlItemNewDB.MinSize = new System.Drawing.Size(39, 35);
            this.layoutControlItemNewDB.Name = "layoutControlItemNewDB";
            this.layoutControlItemNewDB.Size = new System.Drawing.Size(39, 35);
            this.layoutControlItemNewDB.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemNewDB.Text = "layoutControlItemNewDB";
            this.layoutControlItemNewDB.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemNewDB.TextToControlDistance = 0;
            this.layoutControlItemNewDB.TextVisible = false;
            // 
            // layoutControlItemConfigurateDB
            // 
            this.layoutControlItemConfigurateDB.Control = this.buttonConfigurateDB;
            this.layoutControlItemConfigurateDB.CustomizationFormText = "layoutControlItemConfigurateDB";
            this.layoutControlItemConfigurateDB.Location = new System.Drawing.Point(309, 35);
            this.layoutControlItemConfigurateDB.MaxSize = new System.Drawing.Size(39, 35);
            this.layoutControlItemConfigurateDB.MinSize = new System.Drawing.Size(39, 35);
            this.layoutControlItemConfigurateDB.Name = "layoutControlItemConfigurateDB";
            this.layoutControlItemConfigurateDB.Size = new System.Drawing.Size(39, 35);
            this.layoutControlItemConfigurateDB.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemConfigurateDB.Text = "layoutControlItemConfigurateDB";
            this.layoutControlItemConfigurateDB.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemConfigurateDB.TextToControlDistance = 0;
            this.layoutControlItemConfigurateDB.TextVisible = false;
            // 
            // layoutControlItemTelephony
            // 
            this.layoutControlItemTelephony.Control = this.buttonTelephonyData;
            this.layoutControlItemTelephony.CustomizationFormText = "layoutControlItemTelephony";
            this.layoutControlItemTelephony.Location = new System.Drawing.Point(309, 0);
            this.layoutControlItemTelephony.MaxSize = new System.Drawing.Size(39, 35);
            this.layoutControlItemTelephony.MinSize = new System.Drawing.Size(39, 35);
            this.layoutControlItemTelephony.Name = "layoutControlItemTelephony";
            this.layoutControlItemTelephony.Size = new System.Drawing.Size(39, 35);
            this.layoutControlItemTelephony.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemTelephony.Text = "layoutControlItemTelephony";
            this.layoutControlItemTelephony.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemTelephony.TextToControlDistance = 0;
            this.layoutControlItemTelephony.TextVisible = false;
            // 
            // layoutControlGroupAVL
            // 
            this.layoutControlGroupAVL.CustomizationFormText = "layoutControlGroupAVL";
            this.layoutControlGroupAVL.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemAVLName,
            this.layoutControlItemAVLText,
            this.emptySpaceItem3,
            this.emptySpaceItem4,
            this.layoutControlItemPortText,
            this.layoutControlItemPort,
            this.emptySpaceItem5,
            this.emptySpaceItem6});
            this.layoutControlGroupAVL.Location = new System.Drawing.Point(0, 170);
            this.layoutControlGroupAVL.Name = "layoutControlGroupAVL";
            this.layoutControlGroupAVL.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupAVL.Size = new System.Drawing.Size(348, 78);
            this.layoutControlGroupAVL.Text = "layoutControlGroupAVL";
            // 
            // layoutControlItemAVLName
            // 
            this.layoutControlItemAVLName.Control = this.textEditAVLServer;
            this.layoutControlItemAVLName.CustomizationFormText = "layoutControlItemAVLName";
            this.layoutControlItemAVLName.Location = new System.Drawing.Point(92, 0);
            this.layoutControlItemAVLName.Name = "layoutControlItemAVLName";
            this.layoutControlItemAVLName.Size = new System.Drawing.Size(172, 24);
            this.layoutControlItemAVLName.Text = "layoutControlItemAVLName";
            this.layoutControlItemAVLName.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemAVLName.TextToControlDistance = 0;
            this.layoutControlItemAVLName.TextVisible = false;
            // 
            // layoutControlItemAVLText
            // 
            this.layoutControlItemAVLText.Control = this.labelControlAVLServerText;
            this.layoutControlItemAVLText.CustomizationFormText = "layoutControlItemAVLText";
            this.layoutControlItemAVLText.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemAVLText.Name = "layoutControlItemAVLText";
            this.layoutControlItemAVLText.Size = new System.Drawing.Size(76, 24);
            this.layoutControlItemAVLText.Text = "layoutControlItemAVLText";
            this.layoutControlItemAVLText.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemAVLText.TextToControlDistance = 0;
            this.layoutControlItemAVLText.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(264, 0);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(74, 0);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(74, 10);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(74, 24);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(76, 0);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(16, 0);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(16, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(16, 24);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItemPortText
            // 
            this.layoutControlItemPortText.Control = this.labelControlPortText;
            this.layoutControlItemPortText.CustomizationFormText = "layoutControlItemPortText";
            this.layoutControlItemPortText.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItemPortText.Name = "layoutControlItemPortText";
            this.layoutControlItemPortText.Size = new System.Drawing.Size(46, 24);
            this.layoutControlItemPortText.Text = "layoutControlItemPortText";
            this.layoutControlItemPortText.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemPortText.TextToControlDistance = 0;
            this.layoutControlItemPortText.TextVisible = false;
            // 
            // layoutControlItemPort
            // 
            this.layoutControlItemPort.Control = this.textEditPort;
            this.layoutControlItemPort.CustomizationFormText = "layoutControlItemPort";
            this.layoutControlItemPort.Location = new System.Drawing.Point(92, 24);
            this.layoutControlItemPort.Name = "layoutControlItemPort";
            this.layoutControlItemPort.Size = new System.Drawing.Size(172, 24);
            this.layoutControlItemPort.Text = "layoutControlItemPort";
            this.layoutControlItemPort.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemPort.TextToControlDistance = 0;
            this.layoutControlItemPort.TextVisible = false;
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(46, 24);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(46, 0);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(46, 10);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(46, 24);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.Text = "emptySpaceItem5";
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(264, 24);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(74, 0);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(74, 10);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(74, 24);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.Text = "emptySpaceItem6";
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroupReport
            // 
            this.layoutControlGroupReport.CustomizationFormText = "layoutControlGroupReport";
            this.layoutControlGroupReport.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemReport,
            this.layoutControlItemReportText,
            this.layoutControlItemReportNameText,
            this.layoutControlItemReportName});
            this.layoutControlGroupReport.Location = new System.Drawing.Point(0, 70);
            this.layoutControlGroupReport.Name = "layoutControlGroupReport";
            this.layoutControlGroupReport.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupReport.Size = new System.Drawing.Size(348, 100);
            this.layoutControlGroupReport.Text = "layoutControlGroupReport";
            // 
            // layoutControlItemReport
            // 
            this.layoutControlItemReport.Control = this.textBoxReport;
            this.layoutControlItemReport.CustomizationFormText = "layoutControlItemReport";
            this.layoutControlItemReport.Location = new System.Drawing.Point(92, 0);
            this.layoutControlItemReport.Name = "layoutControlItemReport";
            this.layoutControlItemReport.Size = new System.Drawing.Size(246, 35);
            this.layoutControlItemReport.Text = "layoutControlItemReport";
            this.layoutControlItemReport.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemReport.TextToControlDistance = 0;
            this.layoutControlItemReport.TextVisible = false;
            // 
            // layoutControlItemReportText
            // 
            this.layoutControlItemReportText.Control = this.labelReport;
            this.layoutControlItemReportText.CustomizationFormText = "layoutControlItemReportText";
            this.layoutControlItemReportText.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemReportText.MaxSize = new System.Drawing.Size(92, 35);
            this.layoutControlItemReportText.MinSize = new System.Drawing.Size(92, 35);
            this.layoutControlItemReportText.Name = "layoutControlItemReportText";
            this.layoutControlItemReportText.Size = new System.Drawing.Size(92, 35);
            this.layoutControlItemReportText.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemReportText.Text = "layoutControlItemReportText";
            this.layoutControlItemReportText.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemReportText.TextToControlDistance = 0;
            this.layoutControlItemReportText.TextVisible = false;
            // 
            // layoutControlItemReportNameText
            // 
            this.layoutControlItemReportNameText.Control = this.labelReportName;
            this.layoutControlItemReportNameText.CustomizationFormText = "layoutControlItemReportText";
            this.layoutControlItemReportNameText.Location = new System.Drawing.Point(0, 35);
            this.layoutControlItemReportNameText.MaxSize = new System.Drawing.Size(92, 35);
            this.layoutControlItemReportNameText.MinSize = new System.Drawing.Size(92, 35);
            this.layoutControlItemReportNameText.Name = "layoutControlItemReportNameText";
            this.layoutControlItemReportNameText.Size = new System.Drawing.Size(92, 35);
            this.layoutControlItemReportNameText.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemReportNameText.Text = "layoutControlItemReportNameText";
            this.layoutControlItemReportNameText.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemReportNameText.TextToControlDistance = 0;
            this.layoutControlItemReportNameText.TextVisible = false;
            // 
            // layoutControlItemReportName
            // 
            this.layoutControlItemReportName.Control = this.textBoxReportName;
            this.layoutControlItemReportName.CustomizationFormText = "layoutControlItemReportName";
            this.layoutControlItemReportName.Location = new System.Drawing.Point(92, 35);
            this.layoutControlItemReportName.Name = "layoutControlItemReportName";
            this.layoutControlItemReportName.Size = new System.Drawing.Size(246, 35);
            this.layoutControlItemReportName.Text = "layoutControlItemReportName";
            this.layoutControlItemReportName.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemReportName.TextToControlDistance = 0;
            this.layoutControlItemReportName.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(63, 0);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(29, 35);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(29, 35);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(29, 35);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(270, 0);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(39, 35);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(39, 35);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(39, 35);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // DbConnectionControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.MinimumSize = new System.Drawing.Size(292, 190);
            this.Name = "DbConnectionControl";
            this.Size = new System.Drawing.Size(375, 190);
            this.Load += new System.EventHandler(this.DbConnectionControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEditPort.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditAVLServer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxReport.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxReportName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxServer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupServer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDBText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemServerText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemServer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDBName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemNewDB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemConfigurateDB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTelephony)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupAVL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAVLName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAVLText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPortText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPort)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemReportText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemReportNameText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemReportName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

		private DevExpress.XtraEditors.TextEdit textBoxServer;
        private DevExpress.XtraEditors.LabelControl labelDBName;
        private DevExpress.XtraEditors.SimpleButton buttonNewDB;
        private DevExpress.XtraEditors.TextEdit textBoxName;
        private DevExpress.XtraEditors.LabelControl labelServer;
		private DevExpress.XtraEditors.SimpleButton buttonConfigurateDB;
        private DevExpress.XtraEditors.LabelControl labelReport;
        private DevExpress.XtraEditors.TextEdit textBoxReport;
        private DevExpress.XtraEditors.LabelControl labelReportName;
        private DevExpress.XtraEditors.TextEdit textBoxReportName;
        private System.Windows.Forms.ToolTip toolTipMain;
        private System.ComponentModel.IContainer components;
        private DevExpress.XtraEditors.SimpleButton buttonTelephonyData;
		private DevExpress.XtraLayout.LayoutControl layoutControl1;
		private DevExpress.XtraEditors.TextEdit textEditAVLServer;
		private DevExpress.XtraEditors.LabelControl labelControlAVLServerText;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupServer;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDBText;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemServerText;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemServer;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemReportText;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemReport;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemReportNameText;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemReportName;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDBName;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemNewDB;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemConfigurateDB;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTelephony;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupAVL;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupReport;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemAVLName;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemAVLText;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
		private DevExpress.XtraEditors.LabelControl labelControlPortText;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemPortText;
		private DevExpress.XtraEditors.TextEdit textEditPort;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemPort;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
    }
}
