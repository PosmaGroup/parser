using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using SmartCadServerConfiguration.Gui;
using SmartCadGuiCommon.Controls;
using SmartCadControls.Controls;
using SmartCadCore.Common;
using SmartCadCore.Core;
using SmartCadGuiCommon;


namespace SmartCadserverConfiguration.Gui
{
    #region Class NewDBForm Documentation
    /// <summary>
    /// TODO:
    /// </summary>
    /// <className>NewDBForm</className>
    /// <author email="atorres@smartmatic.com">Alden Torres</author>
    /// <date>2006/03/31</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    public class NewDBForm : DevExpress.XtraEditors.XtraForm
    {
        private Separator separator1;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.LabelControl labelServer;
        private DevExpress.XtraEditors.LabelControl labelPassword;
        private DevExpress.XtraEditors.LabelControl labelExLogin;
        private SimpleButtonEx buttonOk;
        private SimpleButtonEx buttonCancel;
        private DevExpress.XtraEditors.TextEdit dataBaseNameTextBox;
        private DevExpress.XtraEditors.TextEdit textBoxDirectory;
        private ComboBoxEx comboBoxExSize;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private new System.ComponentModel.Container components = null;

        public NewDBForm(SqlConnection myConnection)
        {
            InitializeComponent();
            comboBoxExSize.SelectedIndex = 1;
            connection = myConnection;
            LoadLanguage();
        }

        public string DataBaseName
        {
            get
            {
                return dataBaseNameTextBox.Text.Trim();
            }
        }

        public string Path
        {
            set
            {
                textBoxDirectory.Text = value;
            }
            get
            {
                return textBoxDirectory.Text.Trim();
            }
        }

        public int DBSize
        {
            get
            {
                if (comboBoxExSize.SelectedIndex == 0)
                    return 256;
                else if (comboBoxExSize.SelectedIndex == 1)
                    return 512;
                else if (comboBoxExSize.SelectedIndex == 2)
                    return 1024;
                return 0;
            }
        }

        public SqlConnection connection = null;

        private void ButtonAcceptActivation(object sender, System.EventArgs e)
        {
            if (dataBaseNameTextBox.Text == "" || textBoxDirectory.Text == "" || comboBoxExSize.SelectedIndex == -1)
                buttonOk.Enabled = false;
            else
                buttonOk.Enabled = true;
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.separator1 = new Separator();
            this.label1 = new System.Windows.Forms.Label();
            this.dataBaseNameTextBox = new DevExpress.XtraEditors.TextEdit();
            this.labelServer = new DevExpress.XtraEditors.LabelControl();
            this.textBoxDirectory = new DevExpress.XtraEditors.TextEdit();
            this.labelPassword = new DevExpress.XtraEditors.LabelControl();
            this.labelExLogin = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxExSize = new ComboBoxEx();
            this.buttonOk = new SimpleButtonEx();
            this.buttonCancel = new SimpleButtonEx();
            ((System.ComponentModel.ISupportInitialize)(this.dataBaseNameTextBox.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxDirectory.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // separator1
            // 
            this.separator1.BackColor = System.Drawing.Color.Gray;
            this.separator1.Dock = System.Windows.Forms.DockStyle.Top;
            this.separator1.Location = new System.Drawing.Point(0, 34);
            this.separator1.Name = "separator1";
            this.separator1.Size = new System.Drawing.Size(292, 2);
            this.separator1.TabIndex = 18;
            this.separator1.TransparentColor = System.Drawing.Color.WhiteSmoke;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(292, 34);
            this.label1.TabIndex = 17;
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dataBaseNameTextBox
            // 
            this.dataBaseNameTextBox.Location = new System.Drawing.Point(98, 52);
            this.dataBaseNameTextBox.Name = "dataBaseNameTextBox";
            this.dataBaseNameTextBox.Size = new System.Drawing.Size(182, 20);
            this.dataBaseNameTextBox.TabIndex = 1;
            this.dataBaseNameTextBox.TextChanged += new System.EventHandler(this.ButtonAcceptActivation);
            // 
            // labelServer
            // 
            this.labelServer.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelServer.Location = new System.Drawing.Point(8, 56);
            this.labelServer.Name = "labelServer";
            this.labelServer.Size = new System.Drawing.Size(0, 13);
            this.labelServer.TabIndex = 26;
            // 
            // textBoxDirectory
            // 
            this.textBoxDirectory.EditValue = "c:\\program files\\microsoft sql server\\mssql\\data\\ ";
            this.textBoxDirectory.Location = new System.Drawing.Point(98, 80);
            this.textBoxDirectory.Name = "textBoxDirectory";
            this.textBoxDirectory.Size = new System.Drawing.Size(182, 20);
            this.textBoxDirectory.TabIndex = 2;
            this.textBoxDirectory.TextChanged += new System.EventHandler(this.ButtonAcceptActivation);
            // 
            // labelPassword
            // 
            this.labelPassword.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPassword.Location = new System.Drawing.Point(8, 87);
            this.labelPassword.Name = "labelPassword";
            this.labelPassword.Size = new System.Drawing.Size(0, 13);
            this.labelPassword.TabIndex = 24;
            // 
            // labelExLogin
            // 
            this.labelExLogin.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExLogin.Location = new System.Drawing.Point(8, 80);
            this.labelExLogin.Name = "labelExLogin";
            this.labelExLogin.Size = new System.Drawing.Size(51, 13);
            this.labelExLogin.TabIndex = 23;
            this.labelExLogin.Text = "Directorio :";
            // 
            // comboBoxExSize
            // 
            this.comboBoxExSize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxExSize.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.comboBoxExSize.FormattingEnabled = true;
            this.comboBoxExSize.Items.AddRange(new object[] {
            "Pequena (256 MB)",
            "Mediana (512 MB)",
            "Grande (1 GB)"});
            this.comboBoxExSize.Location = new System.Drawing.Point(98, 86);
            this.comboBoxExSize.Name = "comboBoxExSize";
            this.comboBoxExSize.Size = new System.Drawing.Size(182, 21);
            this.comboBoxExSize.TabIndex = 3;
            // 
            // buttonOk
            // 
            this.buttonOk.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOk.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonOk.Appearance.Options.UseFont = true;
            this.buttonOk.Appearance.Options.UseForeColor = true;
            this.buttonOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOk.Enabled = false;
            this.buttonOk.FontFormat = FontFormat.GetFormat("ButtonFormat");
            this.buttonOk.Location = new System.Drawing.Point(130, 123);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(72, 25);
            this.buttonOk.TabIndex = 4;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCancel.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonCancel.Appearance.Options.UseFont = true;
            this.buttonCancel.Appearance.Options.UseForeColor = true;
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.FontFormat = FontFormat.GetFormat("ButtonFormat");
            this.buttonCancel.Location = new System.Drawing.Point(208, 123);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(72, 25);
            this.buttonCancel.TabIndex = 5;
            // 
            // NewDBForm
            // 
            this.AcceptButton = this.buttonOk;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(292, 157);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.comboBoxExSize);
            this.Controls.Add(this.dataBaseNameTextBox);
            this.Controls.Add(this.labelServer);
            this.Controls.Add(this.labelPassword);
            this.Controls.Add(this.separator1);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.LookAndFeel.SkinName = "Blue";
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "NewDBForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)(this.dataBaseNameTextBox.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxDirectory.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private void LoadLanguage()
        {
            this.Text = ResourceLoader.GetString2("DatabaseWizardCreation");
            this.label1.Text = ResourceLoader.GetString2("DatabaseCreationStepTwo");
            this.labelServer.Text = ResourceLoader.GetString2("NameTwoPoints");
            this.labelPassword.Text = ResourceLoader.GetString2("InitialSizeTwoPoints");
            this.buttonOk.Text = ResourceLoader.GetString2("Create");
            this.buttonCancel.Text = ResourceLoader.GetString2("ProfileFormEditButtonCancelText");
            
        }
        private void CreateDB(ChangeTaskDelegate changeTaskFunction, ChangeValueDelegate changeValueFunction, object[] arguments)
        {
            try
            {
                changeTaskFunction("ValidatingDatabaseName");
                changeValueFunction(10);
                connection.ChangeDatabase("master");
                connection.ChangeDatabase(DataBaseName);
                throw new Exception("DatabaseAlreadyExists");
            }
            catch (SqlException)
            {
                changeTaskFunction(ResourceLoader.GetString2("CreatingDatabase"));
                changeValueFunction(50);
                String CreateQuery = "DECLARE @data_path nvarchar(256); " +
                                     "SET @data_path = (SELECT SUBSTRING(filename, 1, CHARINDEX(N'master.mdf', LOWER(filename)) - 1) " +
                                                        "FROM sysfiles " +
                                                        "WHERE name = 'master' AND fileid = 1); "; 

                //string CreateQuery = "CREATE DATABASE "+ DataBaseName +" ON (NAME = "+DataBaseName+@"_dat, FILENAME = '" + Path + DataBaseName+"01.mdf', SIZE = " +DBSize+"MB) LOG ON(NAME = "+DataBaseName+@"_log1, FILENAME = '" + Path + DataBaseName+"01.ldf', SIZE = 25MB),(NAME = "+DataBaseName+@"_log2, FILENAME = '"+ Path + DataBaseName+"02.ldf', SIZE= 25MB),(NAME = "+DataBaseName+@"_log3, FILENAME = '"+ Path + DataBaseName+"03.ldf', SIZE =25MB) COLLATE Modern_Spanish_CI_AI";
                FormUtil.InvokeRequired(comboBoxExSize, delegate
                {
                    CreateQuery = CreateQuery + "EXEC('CREATE DATABASE " + "\"" + DataBaseName + "\"" + " ON (NAME = " + "''" + DataBaseName + "_dat" + "''" + ", FILENAME = '''+ @data_path + '" + DataBaseName + "01.mdf'', SIZE = " + DBSize + "MB) COLLATE Modern_Spanish_CI_AI')";
                
                });
                
                SqlCommand myCommand = new SqlCommand(CreateQuery, connection);
                myCommand.CommandTimeout = 400;
                myCommand.ExecuteNonQuery();
                connection.ChangeDatabase(DataBaseName);
                changeValueFunction(100);
            }
        }

        private void buttonOk_Click(object sender, System.EventArgs e)
        {
            try
            {
                ProgressForm processForm = new ProgressForm(new TaskFunctionsDelegate(CreateDB), new object[0], ResourceLoader.GetString2("NewDatabase"));
                processForm.CanThrowError = true;
                processForm.ShowDialog();
                MessageForm.Show(ResourceLoader.GetString2("DatabaseSuccesfulyBuilded"), MessageFormType.Information);
            }
            catch (Exception ex)
            {
                if (ex.Message.IndexOf("MODIFY FILE") != -1)
                    MessageForm.Show(ResourceLoader.GetString2("ServerWithoutSpaceOnDisk"), ex);
                else if (ex.Message.IndexOf("permission") != -1)
                    MessageForm.Show(ResourceLoader.GetString2("UserWithoutAccesForOperation"), ex);
                else
                    MessageForm.Show(ex.Message, ex);

                DialogResult = DialogResult.None;
            }
        }
    }
}
