﻿
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using System.Windows.Forms;
using SmartCadCore.Core;
using SmartCadCore.Common;
using SmartCadGuiCommon;
using SmartCadServerConfiguration.Gui;


namespace SmartCadServerConfiguration
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                ApplicationUtil.ActivateProcess(Process.GetCurrentProcess());

                AppDomain.CurrentDomain.SetPrincipalPolicy(PrincipalPolicy.WindowsPrincipal);
                WindowsPrincipal myPrincipal = (WindowsPrincipal)Thread.CurrentPrincipal;



                SmartCadConfiguration.Load();

                ResourceLoader.UpdatedLanguage(new CultureInfo(
                   SmartCadConfiguration.SmartCadSection.CultureElement.CultureLanguageElement.Name
                   + "-" +
                 SmartCadConfiguration.SmartCadSection.CultureElement.CultureCountryElement.Name));

                SplashForm.ShowSplash();

                Thread.Sleep(TimeSpan.FromSeconds(3));

                SplashForm.CloseSplash();

                if (myPrincipal.IsInRole(WindowsBuiltInRole.Administrator) == true)
                {
                    ConnectionServerConfigurationForm form = new ConnectionServerConfigurationForm();
                    form.ShowDialog();
                }
                else
                {
                    MessageForm.Show(ResourceLoader.GetString2("MustHaveAdministratorAccess"), MessageFormType.Error);
                }

            }
            catch (System.Security.SecurityException se)
            {
                SplashForm.CloseSplash();
                MessageForm.Show(ResourceLoader.GetString2("MustHaveAdministratorAccess"), se);
            }
            catch (Exception ex)
            {
                SplashForm.CloseSplash();

                MessageForm.Show(ResourceLoader.GetString2("GenericErrorApplication"), ex);
            }
        }
    }
}
