﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace Smartmatic.SmartCad.Vms
{
    public partial class AlarmManager
    {
        #region fields
        private static Type AlarmManagerType = null;
        #endregion

        /// <summary>
        /// This create a new instance of the control with the current dll
        /// </summary>
        /// <param name="assemblyName">dll name</param>
        /// <returns>the control fully loaded</returns>
        public static AlarmManager GetInstance(string assemblyName)
        {
            if (AlarmManagerType == null)
            {

                Assembly assembly = Assembly.Load(assemblyName);

                Type[] types = assembly.GetTypes();

                foreach (Type type in types)
                {
                    if (type.IsClass == true
                        && type.IsAbstract == false
                        && type.IsSubclassOf(typeof(AlarmManager)))
                    {
                        AlarmManagerType = type;
                        break;
                    }
                }
            }
            return (AlarmManager)AlarmManagerType.GetConstructor(new Type[0] { }).Invoke(new object[0] { });
        }  

        /// <summary>
        /// Connect to the server of events
        /// </summary>
        /// <returns>0 Not connected, 1 Connected</returns>
        public virtual int Connect()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Initialize the Class
        /// </summary>
        /// <param name="serverIp">Ip for the VMS server</param>
        /// <param name="login">User Login</param>
        /// <param name="password">User Password</param>
        public virtual void Initialize(string serverIp, string login, string password)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Start the alarm manager
        /// </summary>
        public virtual void Start()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Stop the alarm manager
        /// </summary>
        public virtual void Stop()
        {
            throw new NotImplementedException();
        }

        public delegate void AlarmEvent(object sender, AlarmArgs args);

        public virtual event AlarmEvent AlarmEventManager;

        public class Alarm
        {
            private string cameraId;
            private string deviceEventName;
            private DateTime eventTime;

            public string CameraId
            {
                get { return cameraId; }
                set { cameraId = value; }
            }

            public string DeviceEventName
            {
                get { return deviceEventName; }
                set { deviceEventName = value; }
            }

            public DateTime EventTime
            {
                get { return eventTime; }
                set { eventTime = value; }
            }
        }

        public class AlarmArgs : System.EventArgs
        {
            private Alarm alarm;

            public AlarmArgs(Alarm Data)
            {
                this.alarm = Data;
            }

            public Alarm AlarmData
            {
                get { return alarm; }
                set { alarm = value; }
            }
        }

    }
}
