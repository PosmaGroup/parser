﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using SmartCadCore.ClientData;

namespace Smartmatic.SmartCad.Vms
{
    public delegate void AlarmsReceived(List<VmsAlarm> newAlarms, List<VmsAlarm> closedAlarms);
    public delegate void StatsReceived();

    public class VmsAlarmManager
    {
        public virtual event AlarmsReceived AlarmsReceived;
        public virtual event StatsReceived StatsReceived;
        private static Type VmsType = null;

        public VmsAlarmManager()
        {
        }

        /// <summary>
        /// Connects to the Alarms Server
        /// </summary>
        /// <param name="serverIp"></param>
        public virtual void Connect(string serverIp)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Disconnects from the Alarms Server
        /// </summary>
        public virtual void Disconnect()
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Updates the alarm
        /// </summary>
        /// <param name="vmsAlarm"></param>
        /// <param name="newStatus"> 1 new, 2 InProgress, 3 Hold, 4 Closed</param>
        public virtual void UpdateAlarm(VmsAlarm vmsAlarm, int newStatus)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This create a new instance of the control with the current dll
        /// </summary>
        /// <param name="assemblyName">dll name</param>
        /// <returns>the control fully loaded</returns>
        public static VmsAlarmManager GetInstance(string assemblyName)
        {
            if (VmsType == null)
            {

                Assembly assembly = Assembly.Load(assemblyName);

                Type[] types = assembly.GetTypes();

                foreach (Type type in types)
                {
                    if (type.IsClass == true
                        && type.IsAbstract == false
                        && type.IsSubclassOf(typeof(VmsAlarmManager)))
                    {
                        VmsType = type;
                        break;
                    }
                }
            }
            return (VmsAlarmManager)VmsType.GetConstructor(new Type[0] { }).Invoke(new object[0] { });
        }  
    }

    public enum VmsAlarmPriority
	{
        High,
        Medium,
        Low
	}

    public enum VmsAlarmStatus
	{
        New,
        InProgress,
        Closed
	}

    public class VmsAlarm : ClientData
    {
        public Guid AlarmId { get; set; }
        public string CameraId { get; set; }
        public string Description { get; set; }
        public VmsAlarmStatus Status { get; set; }
        public VmsAlarmPriority Priority { get; set; }
        public DateTime StartDate { get; set; }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is VmsAlarm)
            {
                result = this.AlarmId == ((VmsAlarm)obj).AlarmId;
            }
            return result; 
        }
    }
}
