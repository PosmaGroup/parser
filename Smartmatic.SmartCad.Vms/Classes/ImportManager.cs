﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Reflection;

namespace Smartmatic.SmartCad.Vms
{
    public partial class ImportManager
    {
        #region fields
        private static Type ImportManagerType = null;
        #endregion

        #region properties
        public IList Cameras
        {   get;
            set;
        }
        #endregion

        /// <summary>
        /// This create a new instance of the control with the current dll
        /// </summary>
        /// <param name="assemblyName">dll name</param>
        /// <returns>the control fully loaded</returns>
        public static ImportManager GetInstance(string assemblyName)
        {
            if (ImportManagerType == null)
            {

                Assembly assembly = Assembly.Load(assemblyName);

                Type[] types = assembly.GetTypes();

                foreach (Type type in types)
                {
                    if (type.IsClass == true
                        && type.IsAbstract == false
                        && type.IsSubclassOf(typeof(ImportManager)))
                    {
                        ImportManagerType = type;
                        break;
                    }
                }
            }
            return (ImportManager)ImportManagerType.GetConstructor(new Type[0] { }).Invoke(new object[0] { });
        }

        /// <summary>
        ///  Connect to the server to import the cameras
        /// </summary>
        /// <returns>0 Not connected, 1 Connected</returns>
        public virtual int Export()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Initialize the Class
        /// </summary>
        /// <param name="serverIp">Ip for the VMS server</param>
        /// <param name="login">User Login</param>
        /// <param name="password">User Password</param>
        public virtual void Initialize(string serverIp, string login, string password)
        {
            throw new NotImplementedException();
        }


        public virtual void Disconnect()
        {
            throw new NotImplementedException();
        }

    }
}
