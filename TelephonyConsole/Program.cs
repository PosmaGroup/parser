﻿using SmartCadCore.Common;
using SmartCadCore.Core;
using SmartCadGuiCommon.Util;
using Smartmatic.SmartCad.Service;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace TelephonyConsole
{
    class Program
    {
        private static SmartCadFrontClientManager smartCadFrontClientManager;

        static void Main(string[] args)
        {
            try
            {
                //MediaManager test = new MediaManager();
                ActivateProcess(Process.GetCurrentProcess());
                SmartLogger.Print(ResourceLoader.GetString2("Iniciando servidor..."));
                SmartCadConfiguration.Load();

                //SmartCadConfiguration.Load();

                ServiceHost telephonyService = null;

                telephonyService = TelephonyService.Host();

                telephonyService.Open();

                if (args.Length > 0)
                {
                    smartCadFrontClientManager = new SmartCadFrontClientManager();
                    smartCadFrontClientManager.StartMonitor(int.Parse(args[0]));
                }
                else
                {
                    SmartLogger.Print("No parent process");
                }

                SmartLogger.Print(ResourceLoader.GetString2("Servidor iniciado."));

                SmartLogger.Print("Presionar cualquier tecla para finalizar...");

                Console.ReadLine();

                telephonyService.Close();
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        static void ActivateProcess(Process process)
        {
            Process[] processes = Process.GetProcessesByName(process.ProcessName);

            if (processes.Length > 1)
            {
                foreach (Process p in processes)
                {
                    if (p.Handle != process.Handle)
                    {
                        Win32.ShowWindow(p.MainWindowHandle, Win32.SW_RESTORE);
                        Win32.SetForegroundWindow(p.MainWindowHandle);
                    }
                }

                process.Kill();
            }
        }
    }
}
