using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony.NortelLibrary
{
    public enum NortelReason
    {
        Default,
        BargeIn,
        CallSupervisor,
        ConferenceComplete,
        ConferenceInitiated,
        ConsultComplete,
        ConsultInitiated,
        Emergency,
        Observe,
        Park,
        Redirect,
        Reject,
        Route,
        Shutdown,
        TransferComplete,
        TransferInitiated,
        Whisper
    }
}
