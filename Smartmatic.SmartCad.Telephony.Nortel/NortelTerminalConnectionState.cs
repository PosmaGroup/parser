using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony.NortelLibrary
{
    public enum NortelTerminalConnectionState
    {
        Active,
        Bridged,
        Dropped,
        Held,
        Idle,
        InUse,
        Ringing,
        Unknown
    }
}
