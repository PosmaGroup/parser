using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony
{
    [Serializable]
    public abstract class TerminalStateEventArgs : CCTEventArgs
    {
        // Methods
        // Properties
        public abstract ResourceState NewState { get; }
        public abstract ITerminal Terminal { get; }
        public abstract string TerminalName { get; }

        // Fields
        protected readonly string _name;
        protected readonly ResourceState _newState;
        protected readonly ITerminal _terminal;
    }
}
