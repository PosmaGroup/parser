using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony
{
    public interface ITerminalConnection
    {
        // Methods
        void Answer();
        void BlindTransfer(string destAddress);
        void CompleteConference(IContact consultContact);
        void CompleteSupervisedTransfer(IContact consultContact);
        IContact Consult(string destAddress);
        void GenerateDTMF(string tones);
        void Hold();
        IContact InitiateConference(string destAddress);
        IContact InitiateSupervisedTransfer(string destAddress);
        void Unhold();

        // Properties
        ITerminalConnectionCapabilities Capabilities { get; }
        IContact ConferenceConsultContact { get; }
        IConnection Connection { get; }
        IContact ConsultContact { get; }
        IContact Contact { get; }
        string ContactID { get; }
        TerminalConnectionState CurrentState { get; }
        TerminalConnectionState PreviousState { get; }
        Reason StateReason { get; }
        ITerminal Terminal { get; }
        IContact TransferConsultContact { get; }
    }
}
