using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony
{
    public interface IAddress
    {
        // Methods
        IConnection GetConnection(string ContactID);
        IContact Originate(ITerminal origTerminal, string destAddress);

        // Properties
        IAddressCapabilities Capabilities { get; }
        IConnection[] Connections { get; }
        bool DoNotDisturb { get; set; }
        bool IsForwarded { get; }
        bool IsMessageWaiting { get; }
        string Name { get; }
        ITerminal[] RelatedTerminals { get; }
        ResourceState State { get; }

        //Events
        event ConnectionStateEventHandler ConnectionStateChanged;
    }
}
