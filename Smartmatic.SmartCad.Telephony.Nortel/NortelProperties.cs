﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Smartmatic.SmartCad.Telephony
{
    public static class NortelProperties
    {
        public const string SIP_SERVER_IP = "SERVER_IP";
        public const string SIP_SERVER_PORT = "SERVER_PORT";

        public const string STATS_SERVER_IP = "STATISTICS_SERVER_IP";
        public const string STATS_USER_NAME = "STATISTICS_USER_NAME";
        public const string STATS_PASSWORD = "STATISTICS_PASSWORD";
        public const string STATS_SCRIPT = "STATISTICS_SCRIPT";
    }
}
