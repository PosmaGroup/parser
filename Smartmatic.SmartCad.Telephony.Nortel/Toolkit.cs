using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony
{
    public abstract class Toolkit
    {
        //public event SessionDisconnectedEventHandler SessionDisconnected;

        // Methods        
        protected abstract void ChannelDisconnectionHandler(ChannelDisconnectedEventArgs args);
        public abstract ISession Connect();
        public abstract AddressPropertyEventHandler CreateEventHandler(AddressPropertyEventHandler clientEventHandler);
        public abstract AddressStateEventHandler CreateEventHandler(AddressStateEventHandler clientEventHandler);
        public abstract ConnectionPropertyEventHandler CreateEventHandler(ConnectionPropertyEventHandler clientEventHandler);
        public abstract ConnectionStateEventHandler CreateEventHandler(ConnectionStateEventHandler clientEventHandler);
        public abstract ContactPropertyEventHandler CreateEventHandler(ContactPropertyEventHandler clientEventHandler);
        public abstract ContactScopeEventHandler CreateEventHandler(ContactScopeEventHandler clientEventHandler);
        public abstract TermConnStateEventHandler CreateEventHandler(TermConnStateEventHandler clientEventHandler);
        public abstract TerminalPropertyEventHandler CreateEventHandler(TerminalPropertyEventHandler clientEventHandler);
        public abstract TerminalStateEventHandler CreateEventHandler(TerminalStateEventHandler clientEventHandler);
        protected abstract void CreateSession(string server, int port, SecuritySettings sec, string workstation);
        public abstract void Disconnect();

        // Properties
        public abstract CCTCredentials Credentials { get; set; }
        public abstract CCTSecurityLevel NegotiatedSecurityLevel { get; }
        public abstract int Port { get; set; }
        public abstract CCTSecurityLevel RequestedSecurityLevel { get; set; }
        public abstract string Server { get; set; }
        public abstract ISession Session { get; }
        protected abstract SecuritySettings TransportSecuritySettings { get; }
        public abstract string Workstation { get; set; }

        // Fields
        protected CCTCredentials _credentials;
        protected EventDeliveryThread _defaultEventThread;
        protected bool _localDisconnect;
        protected int _port;
        protected CCTSecurityLevel _requestedSecLevel;
        protected string _server;
        protected ISessionFactory _serverFactory;
        protected ISession _serverSession;
        protected Guid _sessionGUID;
        protected string _workstation;
    }
}
