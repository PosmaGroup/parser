using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony
{
    [Serializable]
    public class ConnectionProperty : Property
    {
        public ConnectionProperty(int type)
            :base(type)
        {}

        // Methods
        // Nested Types
        protected enum Type
        {
        }
    }
}
