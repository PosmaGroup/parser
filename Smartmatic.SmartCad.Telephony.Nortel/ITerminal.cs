using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony
{
    public interface ITerminal
    {
        // Methods
        IContact Originate(IAddress origAddress, string destAddress);
        ITerminalConnection Pickup(IAddress origAddress, IAddress destAddress);
        ITerminalConnection Pickup(ITerminalConnection origTermConn, IAddress destAddress);
        ITerminalConnection PickupFromGroup(IAddress destAddress);
        ITerminalConnection PickupFromGroup(string pickupGroup, IAddress destAddress);

        // Properties
        ITerminalCapabilities Capabilities { get; }
        bool DoNotDisturb { get; set; }
        bool IsForwarded { get; }
        string Name { get; }
        IAddress[] RelatedAddresses { get; }
        ResourceState State { get; }
        ITerminalConnection[] TerminalConnections { get; }

        //Events
        event TermConnStateEventHandler TermConnStateChanged;
        event TerminalPropertyEventHandler TerminalPropertyChanged;
    }
}
