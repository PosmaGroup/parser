using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony
{
    public enum ConnectionState
    {
        Alerting,
        Dialing,
        Disconnected,
        Established,
        Failed,
        Idle,
        Initiated,
        NetworkAlerting,
        NetworkReached,
        Offered,
        Queued,
        Unknown
    }
}
