using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony
{
    [Serializable]
    public abstract class TermConnStateEventArgs : CCTEventArgs
    {
        // Methods
        // Properties
        public abstract IConnection Connection { get; }
        public abstract IContact Contact { get; }
        public abstract TerminalConnectionState NewState { get; }
        public abstract TerminalConnectionState PreviousState { get; }
        public abstract Reason Reason { get; }
        public abstract ITerminal Sender { get; }
        public abstract ITerminalConnection TerminalConnection { get; }

        // Fields
        protected readonly IConnection _connection;
        protected readonly IContact _contact;
        protected readonly TerminalConnectionState _newState;
        protected readonly TerminalConnectionState _prevState;
        protected readonly Reason _reason;
        protected readonly ITerminal _sender;
        protected readonly ITerminalConnection _termConn;
    }
}
