using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony
{
    public class RemotingException : ApplicationException
    {
        public RemotingException()
            : base()
        {
        }

        public RemotingException(string message)
            : base(message)
        {
        }

        public RemotingException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
