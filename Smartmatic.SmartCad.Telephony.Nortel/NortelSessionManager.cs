using SmartCadCore.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony.NortelLibrary
{
    public static class NortelSessionManager
    {
        public static void Close()
        {
            toolkit = null;
            nortelToolkit = null;
        }
        static Dictionary<string, NortelContact> contacts = new Dictionary<string, NortelContact>();

        public static Dictionary<string, NortelContact> Contacts
        {
            get { return contacts; }
        }

        
        static Nortel.CCT.Toolkit toolkit;
        static NortelToolkit nortelToolkit;

        static NortelSessionManager()
        {
            toolkit = new Nortel.CCT.Toolkit();
            nortelToolkit = new NortelToolkit(toolkit);
        }

        public static Nortel.CCT.Toolkit CurrentNativeToolkit
        {
            get { return toolkit; }
        }

        public static NortelToolkit CurrentNortelToolkit
        {
            get { return nortelToolkit; }
            set
            {
                toolkit = value.NativeToolkit;
                nortelToolkit = value;
            }
        }

        private static NortelAgentTerminal GetTerminal(NortelSession nortelSession)
        {
            NortelAgentTerminal nortelAgentTerminal = null;
            if (nortelSession.Terminals.Length > 0)
            {
                //Nortel.CCT.ITerminal nt = (nortelSession.Terminals[0] as NortelTerminal).NativeTerminal;
                Nortel.CCT.ITerminal nt = nortelSession.SelectedTerminal;
                //Nortel.CCT.ITerminalCapabilities ntc = nt.Capabilities;
                Nortel.CCT.IAgentTerminal nativeAgentTerminal = nt as Nortel.CCT.IAgentTerminal;
                if (nativeAgentTerminal != null)
                {
                    nortelAgentTerminal = new NortelAgentTerminal(nativeAgentTerminal);                    
                }
                else
                {
                    throw new NotSupportedException("The terminal connected is not an Agent Terminal");
                }
            }
            else
            {
                throw new NotSupportedException("There is no terminal associated to this user");
            }
            return nortelAgentTerminal;
        }


        public static void Originate(NortelSession nortelSession, string fromExt, string destExt)
        {
            NortelAgentTerminal nortelAgentTerminal = GetTerminal(nortelSession);
            NortelAddress address = GetAddress(nortelAgentTerminal, fromExt);
            try
            {
                nortelAgentTerminal.Originate(address, destExt);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private static NortelAddress GetAddress(NortelAgentTerminal terminal, string fromExt)
        {
            IAddress[] addresses = (terminal as ITerminal).RelatedAddresses;
            NortelAddress resultAddress = null;
            for (int i = 0; i < addresses.Length; i++)
			{
                NortelAddress address = addresses[i] as NortelAddress;
                if (address.Name == fromExt)
                {
                    resultAddress = address;
                    i = addresses.Length;
                }
			}
            return resultAddress;
        }

        public static void Login(NortelSession nortelSession, string agentId)
        {
            NortelAgentTerminal nortelAgentTerminal = GetTerminal(nortelSession);
            try
            {
                IAgentTerminalCapabilities capabilities;
                ITerminal[] terminals = nortelSession.Terminals;

                if ((nortelAgentTerminal.Capabilities as IAgentTerminalCapabilities).CanLogout == true &&
                    (nortelAgentTerminal.LoginID != string.Empty) &&
                    (nortelAgentTerminal.LoginID != agentId))
                {
                    throw new Nortel.CCT.InvalidTerminalConnectionStateException(ResourceLoader.GetString2(""), Nortel.CCT.TerminalConnectionState.InUse);
                }

                foreach (ITerminal terminal in terminals)
                {
                    if (terminal is IAgentTerminal)
                    {
                        if ((terminal as IAgentTerminal).LoginID == agentId)
                        {
                            capabilities = terminal.Capabilities as IAgentTerminalCapabilities;
                            if (capabilities != null &&
                                capabilities.CanLogin == false &&
                                capabilities.CanLogout == true)
                                (terminal as IAgentTerminal).Logout();
                        }
                    }
                }

                nortelSession.AgentId = agentId;

                capabilities = nortelAgentTerminal.Capabilities as IAgentTerminalCapabilities;

                if (capabilities != null &&
                    capabilities.CanLogin == false &&
                    capabilities.CanLogout == true)
                    nortelAgentTerminal.Logout();

                if (capabilities != null &&
                    capabilities.CanLogin == true)
                    nortelAgentTerminal.Login(agentId);
                else
                    throw new ApplicationException("Impossible to login in nortel.");
            }/*
            catch (Nortel.CCT.AgentAlreadyLoggedInException)
            {
                try
                {
                    nortelAgentTerminal.Logout();
                    nortelSession.AgentId = agentId;
                    nortelAgentTerminal.Login(agentId);
                }
                catch { }

            }*/
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static void Logout(NortelSession nortelSession)
        {
            NortelAgentTerminal nortelAgentTerminal = GetTerminal(nortelSession);
            try
            {
                nortelAgentTerminal.Logout();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static bool GetReadyStatus(NortelSession nortelSession)
        {
            bool retVal = false;
            NortelAgentTerminal nortelAgentTerminal = GetTerminal(nortelSession);
            try
            {
                retVal = nortelAgentTerminal.IsReady;
            }
            catch (Exception e)
            {
                throw e;
            }
            return retVal;
        }

        public static void SetReadyStatus(NortelSession nortelSession, bool isReady)
        {
            NortelAgentTerminal nortelAgentTerminal = GetTerminal(nortelSession);

            try
            {
                if (nortelAgentTerminal.IsReady != !isReady)
                {
                    nortelAgentTerminal.IsReady = !isReady;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void DropCall(NortelSession nortelSession)
        {
            try
            {
                if (nortelSession.CurrentContacts.Length > 0)
                {
                    nortelSession.CurrentContacts[0].Connections[0].Disconnect();
                }
            }

            catch
            {
                throw;
            }        
        }

        public static void Reject(string callId)
        {
            NortelSessionManager.Contacts[callId].Connections[0].Reject();
        }

        public static void PickUp(NortelSession nortelSession, string callID)
        {
            IConnection[] connections = NortelSessionManager.Contacts[callID].Connections;

            foreach (IConnection connection in connections)
            {
                foreach (ITerminalConnection terminalConnection in connection.TerminalConnections)
                {
                    if (terminalConnection.CurrentState == TerminalConnectionState.Ringing)
                        terminalConnection.Answer();
                }
            }
        }
    }
}