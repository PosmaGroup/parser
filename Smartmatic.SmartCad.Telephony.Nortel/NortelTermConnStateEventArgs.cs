using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony.NortelLibrary
{
    public class NortelTermConnStateEventArgs : TermConnStateEventArgs
    {
        private Nortel.CCT.TermConnStateEventArgs termConnStateEventArgs;

        public NortelTermConnStateEventArgs(Nortel.CCT.TermConnStateEventArgs termConnStateEventArgs)
        {
            this.termConnStateEventArgs = termConnStateEventArgs;
        }

        public override IConnection Connection
        {
            get { return new NortelConnection(termConnStateEventArgs.Connection); }
        }

        public override IContact Contact
        {
            get { return new NortelContact(termConnStateEventArgs.Contact); }
        }

        public override TerminalConnectionState NewState
        {
            get { return (TerminalConnectionState)termConnStateEventArgs.NewState; }
        }

        public override TerminalConnectionState PreviousState
        {
            get { return (TerminalConnectionState)termConnStateEventArgs.PreviousState; }
        }

        public override Reason Reason
        {
            get { return (Reason)termConnStateEventArgs.Reason; }
        }

        public override ITerminal Sender
        {
            get { return new NortelTerminal(termConnStateEventArgs.Sender); }
        }

        public override ITerminalConnection TerminalConnection
        {
            get { return new NortelTerminalConnection(termConnStateEventArgs.TerminalConnection); }
        }

        public override long Sequence
        {
            get { return termConnStateEventArgs.Sequence; }
        }
    }
}
