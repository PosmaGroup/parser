using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony
{
    public enum ImpersonationLevel
    {
        Identify,
        Impersonate,
        Delegate
    }

}
