using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony
{
    public enum CCTSecurityLevel
    {
        None,
        Clear,
        Signed,
        Encrypted
    }
}
