using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony
{
    [Serializable]
    public class TerminalProperty : Property
    {
        // Methods
        // Fields
        public static readonly TerminalProperty DoNotDisturb;
        public static readonly TerminalProperty Forwarding;
        public static readonly TerminalProperty Name;

        static TerminalProperty()
        {
            DoNotDisturb = new TerminalProperty(0);
            Forwarding = new TerminalProperty(1);
            Name = new TerminalProperty(2);
        }

        protected TerminalProperty(int type)
            :base(type)
        { }

        // Nested Types
        protected enum Type
        {
            DoNotDisturb = 0,
            Forwarding = 1,
            Name = 2
        }
    }
}
