using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony.NortelLibrary
{
    public class NortelTerminalConnectionCapabilities : ITerminalConnectionCapabilities
    {
        private Nortel.CCT.ITerminalConnectionCapabilities iterminalConnectionCapabilities;

        public NortelTerminalConnectionCapabilities(Nortel.CCT.ITerminalConnectionCapabilities iterminalConnectionCapabilities)
        {
            this.iterminalConnectionCapabilities = iterminalConnectionCapabilities;
        }

        #region ITerminalConnectionCapabilities Members

        public bool CanAnswer
        {
            get { return this.iterminalConnectionCapabilities.CanAnswer; }
        }

        public bool CanBlindTransfer
        {
            get { return this.iterminalConnectionCapabilities.CanBlindTransfer; }
        }

        public bool CanCompleteConference
        {
            get { return this.iterminalConnectionCapabilities.CanCompleteConference; }
        }

        public bool CanCompleteTransfer
        {
            get { return this.iterminalConnectionCapabilities.CanCompleteTransfer; }
        }

        public bool CanConsult
        {
            get { return this.iterminalConnectionCapabilities.CanConsult; }
        }

        public bool CanGenerateDTMF
        {
            get { return this.iterminalConnectionCapabilities.CanGenerateDTMF; }
        }

        public bool CanHold
        {
            get { return this.iterminalConnectionCapabilities.CanHold; }
        }

        public bool CanInitiateConference
        {
            get { return this.iterminalConnectionCapabilities.CanInitiateConference; }
        }

        public bool CanInitiateTransfer
        {
            get { return this.iterminalConnectionCapabilities.CanInitiateTransfer; }
        }

        public bool CanUnhold
        {
            get { return this.iterminalConnectionCapabilities.CanUnhold; }
        }

        #endregion
    }
}
