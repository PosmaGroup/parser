using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony
{
    public abstract class SecuritySettings
    {
        // Methods
        // Properties
        public abstract string Domain { get; }
        internal abstract ImpersonationLevel ImpersonationLevel { get; set; }
        public abstract string Password { get; }
        public abstract SecurityScheme Scheme { get; }
        public abstract SecurityLevel SecurityLevel { get; set; }
        public abstract string User { get; }

        // Fields
        protected string _domain;
        protected ImpersonationLevel _impLevel;
        protected string _password;
        protected SecurityScheme _scheme;
        protected SecurityLevel _secLevel;
        protected string _user;
    }
}
