using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony
{
    [Serializable]
    public abstract class AddressStateEventArgs : CCTEventArgs
    {
        // Methods
        // Properties
        public abstract IAddress Address { get; }
        public abstract string AddressName { get; }
        public abstract ResourceState NewState { get; }

        // Fields
        protected readonly IAddress _address;
        protected readonly string _name;
        protected readonly ResourceState _newState;
    }
}
