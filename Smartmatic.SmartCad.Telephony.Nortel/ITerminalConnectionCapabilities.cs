using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony
{
    public interface ITerminalConnectionCapabilities
    {
        // Properties
        bool CanAnswer { get; }
        bool CanBlindTransfer { get; }
        bool CanCompleteConference { get; }
        bool CanCompleteTransfer { get; }
        bool CanConsult { get; }
        bool CanGenerateDTMF { get; }
        bool CanHold { get; }
        bool CanInitiateConference { get; }
        bool CanInitiateTransfer { get; }
        bool CanUnhold { get; }
    }
}
