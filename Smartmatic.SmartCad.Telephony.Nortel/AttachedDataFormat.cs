using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony
{
    public enum AttachedDataFormat
    {
        Str,
        Bin,
        KVP
    }
}
