using SmartCadCore.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony.NortelLibrary
{
    public class NortelTerminal : ITerminal
    {
        private Nortel.CCT.ITerminal terminal;

        public NortelTerminal(Nortel.CCT.ITerminal terminal)
        {
            this.terminal = terminal;
            try
            {
                this.terminal.TermConnStateChanged += NortelToolkit.LastToolkit.CreateEventHandler(terminal_TermConnStateChanged);
                this.terminal.PropertyChanged += NortelToolkit.LastToolkit.CreateEventHandler(terminal_TerminalPropertyChanged);
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        public Nortel.CCT.ITerminal NativeTerminal
        {
            get { return terminal; }
        }

        #region ITerminal Members

        public IContact Originate(IAddress origAddress, string destAddress)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public ITerminalConnection Pickup(IAddress origAddress, IAddress destAddress)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public ITerminalConnection Pickup(ITerminalConnection origTermConn, IAddress destAddress)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public ITerminalConnection PickupFromGroup(IAddress destAddress)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public ITerminalConnection PickupFromGroup(string pickupGroup, IAddress destAddress)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public ITerminalCapabilities Capabilities
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public bool DoNotDisturb
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        public bool IsForwarded
        {
            get
            {
                return terminal.IsForwarded;
            }
        }

        public string Name
        {
            get 
            {
                return terminal.Name;
            }
        }

        public IAddress[] RelatedAddresses
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public ResourceState State
        {
            get 
            {
                return (ResourceState)terminal.State;
            }
        }

        public ITerminalConnection[] TerminalConnections
        {
            get
            {
                Nortel.CCT.ITerminalConnection[] cons = this.terminal.TerminalConnections;
                NortelTerminalConnection[] retVal = new NortelTerminalConnection[cons.Length];
                for (int i = 0; i < cons.Length; i++)
                {
                    retVal[i] = new NortelTerminalConnection(cons[i]);
                }
                return retVal;
            }
        }

        public event TermConnStateEventHandler TermConnStateChanged;

        private void terminal_TermConnStateChanged(Nortel.CCT.TermConnStateEventArgs e)
        {
            if (TermConnStateChanged != null)
            {
                TermConnStateChanged(new NortelTermConnStateEventArgs(e));
            }
        }

        public event TerminalPropertyEventHandler TerminalPropertyChanged;

        private void terminal_TerminalPropertyChanged(Nortel.CCT.TerminalPropertyEventArgs e)
        {
            if (TerminalPropertyChanged != null)
            {
                TerminalPropertyChanged(new NortelTerminalPropertyEventArgs(e));
            }
        }

        #endregion
    }
}
