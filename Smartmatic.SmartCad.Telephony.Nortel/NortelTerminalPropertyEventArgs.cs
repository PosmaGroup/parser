using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony.NortelLibrary
{
    [Serializable]
    public class NortelTerminalPropertyEventArgs : TerminalPropertyEventArgs
    {
        private Nortel.CCT.TerminalPropertyEventArgs terminalPropertyEventArgs;

        public NortelTerminalPropertyEventArgs(Nortel.CCT.TerminalPropertyEventArgs terminalPropertyEventArgs)
        {
            this.terminalPropertyEventArgs = terminalPropertyEventArgs;
        }

        public override TerminalProperty ChangedProperty
        {
            get 
            {
                TerminalProperty terminalProperty = null;
                if (this.terminalPropertyEventArgs.ChangedProperty is Nortel.CCT.ReadyStatusAgentTerminalProperty)
                {
                    Nortel.CCT.ReadyStatusAgentTerminalProperty property = (Nortel.CCT.ReadyStatusAgentTerminalProperty)this.terminalPropertyEventArgs.ChangedProperty;
                    terminalProperty = new NortelReadyStatusAgentTerminalProperty(property.IsReady, property.NotReadyReasonCode);
                }
                else if (this.terminalPropertyEventArgs.ChangedProperty is Nortel.CCT.ActivityCodeAgentTerminalProperty)
                {
                    Nortel.CCT.ActivityCodeAgentTerminalProperty property = (Nortel.CCT.ActivityCodeAgentTerminalProperty)this.terminalPropertyEventArgs.ChangedProperty;
                    terminalProperty = new NortelActivityCodeAgentTerminalProperty(property.Code);
                }
                else if (this.terminalPropertyEventArgs.ChangedProperty is Nortel.CCT.DoNotDisturbTerminalProperty)
                {
                    Nortel.CCT.DoNotDisturbTerminalProperty property = (Nortel.CCT.DoNotDisturbTerminalProperty)this.terminalPropertyEventArgs.ChangedProperty;
                    terminalProperty = new NortelDoNotDisturbTerminalProperty(property.DoNotDisturbValue);
                }
                else if (this.terminalPropertyEventArgs.ChangedProperty is Nortel.CCT.AgentTerminalProperty)
                    terminalProperty = new NortelAgentTerminalProperty(((Nortel.CCT.AgentTerminalProperty)this.terminalPropertyEventArgs.ChangedProperty).ToString());
                return terminalProperty;

            }
        }

        public override ITerminalCapabilities CurrentCapabilities
        {
            get 
            {
                if (this.terminalPropertyEventArgs.CurrentCapabilities is Nortel.CCT.IAgentTerminalCapabilities)
                    return new NortelAgentTerminalCapabilities(this.terminalPropertyEventArgs.CurrentCapabilities as Nortel.CCT.IAgentTerminalCapabilities);
                else
                    throw new Exception("The method or operation is not implemented.");
            }
        }

        public override ITerminal Sender
        {
            get 
            { 
                ITerminal terminal = null;
                if (this.terminalPropertyEventArgs.Sender is Nortel.CCT.IAgentTerminal)
                {
                    terminal = new NortelAgentTerminal((Nortel.CCT.IAgentTerminal)this.terminalPropertyEventArgs.Sender);
                }
                else
                {
                    terminal = new NortelTerminal(this.terminalPropertyEventArgs.Sender);
                }
                return terminal;
            }
        }

        public override long Sequence
        {
            get 
            {
                return this.terminalPropertyEventArgs.Sequence;
            }
        }

    }
}
