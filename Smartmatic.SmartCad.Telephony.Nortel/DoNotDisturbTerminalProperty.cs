using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony
{
    public class DoNotDisturbTerminalProperty : TerminalProperty
    {
         // Fields
        public readonly bool DoNotDisturbValue;

        // Methods
        public DoNotDisturbTerminalProperty(bool dnd)
            : base(0)
        {
            this.DoNotDisturbValue = dnd;
        }
    }
}
