using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony
{
    [Serializable]
    public abstract class SessionDisconnectedEventArgs : EventArgs
    {
        // Properties
        public abstract bool IsLocalDisconnection { get; }

        // Fields
        protected bool _localDisconnect;
    }
}
