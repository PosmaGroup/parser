using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony
{
    public interface ISessionFactory
    {
        // Methods
        void DisconnectSession(Guid clientGUID);
        ISession GetSession(Guid clientGUID);
        ISession GetSession(Guid clientGUID, string workstation);
    }

}
