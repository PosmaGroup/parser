using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Threading;

namespace Smartmatic.SmartCad.Telephony
{
    public abstract class EventDeliveryThread
    {
        // Methods
        public abstract void Dispose();
        public abstract void Queue(EventHandlerProxy proxy, CCTEventArgs args);
        protected abstract void Run();

        // Properties
        public abstract string Name { get; }
        protected abstract QueuedEvent NextEvent { get; }

        // Fields
        protected bool _disposed;
        protected Queue _eventQueue;
        protected AutoResetEvent _eventSignal;
        protected Thread _eventThread;
        protected SortedList _outOfSeqEvents;
        protected long _prevSequence;

        // Nested Types
        protected abstract class QueuedEvent
        {
            // Methods
            public QueuedEvent(EventHandlerProxy proxy, CCTEventArgs args) { }
            public abstract void Raise();

            // Properties
            public abstract CCTEventArgs Args { get; }

            // Fields
            protected CCTEventArgs _args;
            protected EventHandlerProxy _proxy;
        }
    }
}
