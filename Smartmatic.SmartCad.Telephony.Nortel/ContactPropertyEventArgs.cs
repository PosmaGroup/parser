using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony
{
    [Serializable]
    public abstract class ContactPropertyEventArgs : CCTEventArgs
    {
        // Methods
        // Properties
        public abstract ContactProperty ChangedProperty { get; }
        public abstract IContact Sender { get; }

        // Fields
        protected readonly ContactProperty _property;
        protected readonly IContact _sender;
    }
}
