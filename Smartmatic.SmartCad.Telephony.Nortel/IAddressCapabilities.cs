using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony
{
    public interface IAddressCapabilities
    {
        // Properties
        bool CanConference { get; }
        bool CanConsult { get; }
        bool CanDoNotDisturb { get; }
        bool CanForward { get; }
        bool CanGetMessageWaiting { get; }
        bool CanOriginate { get; }
        bool CanSetMessageWaiting { get; }
        bool CanTransfer { get; }
    }

}
