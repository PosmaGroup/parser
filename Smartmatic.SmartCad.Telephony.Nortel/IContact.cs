using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony
{
    public interface IContact
    {
        // Methods
        IConnection AddParty(string address);
        IConnection BargeIn(ITerminal terminal, IAddress address);
        void Drop();
        IConnection Observe(ITerminal terminal, IAddress address);
        IConnection Whisper(ITerminal spvTerminal, IAddress spvAddress, ITerminal agtTerminal, IAddress agtAddress);

        // Properties
        IAddress CalledAddress { get; }
        IAddress CallingAddress { get; }
        ITerminal CallingTerminal { get; }
        IContactCapabilities Capabilities { get; }
        IConnection[] Connections { get; }
        AttachedData Data { get; set; }
        string ID { get; }
        IAddress LastRedirectedAddress { get; }
        string OriginalDestination { get; }
        UserUserInfo UUI { get; set; }
    }
}
