using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony.NortelLibrary
{
    [Serializable]
    public class NortelActivityCodeAgentTerminalProperty : ActivityCodeAgentTerminalProperty
    {
        public NortelActivityCodeAgentTerminalProperty(string code)
            : base(code)
        {
        }
    }
}
