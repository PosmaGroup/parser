using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony.NortelLibrary
{
    public class NortelAddressStateEventArgs : AddressStateEventArgs
    {
        private Nortel.CCT.AddressStateEventArgs args;

        public NortelAddressStateEventArgs(Nortel.CCT.AddressStateEventArgs args)
        {
            this.args = args;
        }

        public override IAddress Address
        {
            get { return new NortelAddress(this.args.Address); }
        }

        public override string AddressName
        {
            get { return this.args.AddressName; }
        }

        public override ResourceState NewState
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public override long Sequence
        {
            get { return this.args.Sequence; }
        }
    }
}
