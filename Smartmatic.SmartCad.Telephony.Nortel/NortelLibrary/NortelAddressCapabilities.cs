using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony.NortelLibrary
{
    public class NortelAddressCapabilities : IAddressCapabilities
    {
        private Nortel.CCT.IAddressCapabilities addressCapabilities;

        public NortelAddressCapabilities(Nortel.CCT.IAddressCapabilities addressCapabilities)
        {
            this.addressCapabilities = addressCapabilities;
        }

        #region IAddressCapabilities Members

        public bool CanConference
        {
            get { return this.addressCapabilities.CanConference; }
        }

        public bool CanConsult
        {
            get { return this.addressCapabilities.CanConsult; }
        }

        public bool CanDoNotDisturb
        {
            get { return this.addressCapabilities.CanDoNotDisturb; }
        }

        public bool CanForward
        {
            get { return this.addressCapabilities.CanForward; }
        }

        public bool CanGetMessageWaiting
        {
            get { return this.addressCapabilities.CanGetMessageWaiting; }
        }

        public bool CanOriginate
        {
            get { return this.addressCapabilities.CanOriginate; }
        }

        public bool CanSetMessageWaiting
        {
            get { return this.addressCapabilities.CanSetMessageWaiting; }
        }

        public bool CanTransfer
        {
            get { return this.addressCapabilities.CanTransfer; }
        }

        #endregion
    }
}
