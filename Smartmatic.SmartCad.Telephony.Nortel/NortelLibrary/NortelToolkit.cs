using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony.NortelLibrary
{
    public class NortelToolkit : Toolkit
    {
        private Nortel.CCT.Toolkit toolkit;

        internal NortelToolkit(Nortel.CCT.Toolkit toolkit)
        {
            this.toolkit = toolkit;

            lastToolkit = this.toolkit;
        }

        public NortelToolkit()
        {
            this.toolkit = new Nortel.CCT.Toolkit();
            lastToolkit = this.toolkit;
        }

        public Nortel.CCT.Toolkit NativeToolkit
        {
            get { return toolkit; }
        }

        private static Nortel.CCT.Toolkit lastToolkit;

        public static Nortel.CCT.Toolkit LastToolkit
        {
            get
            {
                return lastToolkit;
            }
        }

        protected override void ChannelDisconnectionHandler(ChannelDisconnectedEventArgs args)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override ISession Connect()
        {
            NortelSession session = new NortelSession(toolkit.Connect());
            return session;
        }

        public override AddressPropertyEventHandler CreateEventHandler(AddressPropertyEventHandler clientEventHandler)
        {
            return (AddressPropertyEventHandler)toolkit.CreateEventHandler((Nortel.CCT.AddressPropertyEventHandler)(clientEventHandler.GetInvocationList()[0])).GetInvocationList()[0];
        }

        public override AddressStateEventHandler CreateEventHandler(AddressStateEventHandler clientEventHandler)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override ConnectionPropertyEventHandler CreateEventHandler(ConnectionPropertyEventHandler clientEventHandler)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override ContactPropertyEventHandler CreateEventHandler(ContactPropertyEventHandler clientEventHandler)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override ContactScopeEventHandler CreateEventHandler(ContactScopeEventHandler clientEventHandler)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override TermConnStateEventHandler CreateEventHandler(TermConnStateEventHandler clientEventHandler)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override TerminalPropertyEventHandler CreateEventHandler(TerminalPropertyEventHandler clientEventHandler)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override TerminalStateEventHandler CreateEventHandler(TerminalStateEventHandler clientEventHandler)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        protected override void CreateSession(string server, int port, SecuritySettings sec, string workstation)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override void Disconnect()
        {
            this.toolkit.Disconnect();
        }

        public override CCTCredentials Credentials
        {
            get
            {
                return new NortelCCTCredentials(toolkit.Credentials);
            }
            set
            {
                toolkit.Credentials = ((NortelCCTCredentials)value).NativeCredentials;
            }
        }

        public override CCTSecurityLevel NegotiatedSecurityLevel
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public override int Port
        {
            get
            {
                return this.toolkit.Port;
            }
            set
            {
                this.toolkit.Port = value;
            }
        }

        public override CCTSecurityLevel RequestedSecurityLevel
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        public override string Server
        {
            get
            {
                return this.toolkit.Server;
            }
            set
            {
                this.toolkit.Server = value;
            }
        }

        public override ISession Session
        {
            get { return new NortelSession(this.toolkit.Session); }
        }

        protected override SecuritySettings TransportSecuritySettings
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public override string Workstation
        {
            get
            {
                return this.toolkit.Workstation;
            }
            set
            {
                this.toolkit.Workstation = value;
            }
        }

        public override ConnectionStateEventHandler CreateEventHandler(ConnectionStateEventHandler clientEventHandler)
        {
            throw new Exception("The method or operation is not implemented.");
        }
    }
}
