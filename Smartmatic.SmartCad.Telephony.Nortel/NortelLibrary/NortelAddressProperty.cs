using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony.NortelLibrary
{
    public class NortelAddressProperty : AddressProperty
    {
        public static explicit operator NortelAddressProperty(Nortel.CCT.AddressProperty p)
        {
            Console.WriteLine("NortelAddressProperty " + p.ToString());
            NortelAddressProperty retVal = new NortelAddressProperty(0);
            return retVal;
        }

        public NortelAddressProperty(int type)
            : base(type)
        { }
    }
}
