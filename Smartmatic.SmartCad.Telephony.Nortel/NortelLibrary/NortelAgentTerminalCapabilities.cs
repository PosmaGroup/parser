using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony.NortelLibrary
{
    #region Class Program Documentation
    /// <summary>
    /// 
    /// </summary>
    /// <className>NortelTerminalCapabilities</className>
    /// <author email="atorres@smartmatic.com">Alden Torres</author>
    /// <date>2007/05/09</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    public class NortelAgentTerminalCapabilities : IAgentTerminalCapabilities
    {
        private Nortel.CCT.IAgentTerminalCapabilities agentTerminalCapabilities;

        public NortelAgentTerminalCapabilities(Nortel.CCT.IAgentTerminalCapabilities agentTerminalCapabilities)
        {
            this.agentTerminalCapabilities = agentTerminalCapabilities;
        }

        #region IAgentTerminalCapabilities Members

        public bool CanLogin
        {
            get
            {
                return agentTerminalCapabilities.CanLogin;
            }
        }

        public bool CanLogout
        {
            get
            {
                return agentTerminalCapabilities.CanLogout;
            }
        }

        public bool CanGetReadyStatus
        {
            get
            {
                return agentTerminalCapabilities.CanGetReadyStatus;
            }
        }

        public bool CanSetReadyStatus
        {
            get
            {
                return agentTerminalCapabilities.CanSetReadyStatus;
            }
        }

        #endregion

        #region ITerminalCapabilities Members

        public bool CanConference
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public bool CanConsult
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public bool CanDoNotDisturb
        {
            get 
            { 
                return agentTerminalCapabilities.CanDoNotDisturb; 
            }
        }

        public bool CanForward
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public bool CanOriginate
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public bool CanPickup
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public bool CanPickupFromGroup
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public bool CanTransfer
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        #endregion
    }
}
