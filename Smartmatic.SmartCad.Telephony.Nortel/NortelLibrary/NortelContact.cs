using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony.NortelLibrary
{
    public class NortelContact : IContact
    {
        private Nortel.CCT.IContact contact;

        public NortelContact(Nortel.CCT.IContact contact)
        {
            this.contact = contact;
        }

        public Nortel.CCT.IContact NativeContact
        {
            get { return contact; }
        }

        #region IContact Members

        public IConnection AddParty(string address)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public IConnection BargeIn(ITerminal terminal, IAddress address)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void Drop()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public IConnection Observe(ITerminal terminal, IAddress address)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public IConnection Whisper(ITerminal spvTerminal, IAddress spvAddress, ITerminal agtTerminal, IAddress agtAddress)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public IAddress CalledAddress
        {
            get
            {
                return new NortelAddress(this.contact.CalledAddress, false);
            }
        }

        public IAddress CallingAddress
        {
            get
            {
                return new NortelAddress(this.contact.CallingAddress, false);
            }
        }

        public ITerminal CallingTerminal
        {
            /*get
            {
                //TODO: Revisar este metodo.
                Nortel.CCT.IContactCapabilities cap = this.contact.Capabilities;

                Nortel.CCT.IConnection[] cons = this.contact.Connections;
                Nortel.CCT.ITerminalConnection[] tcs = ((cons[0] as Nortel.CCT.IConnection).TerminalConnections);
                (cons[0] as Nortel.CCT.IConnection).TerminalConnections[0].Answer();
                if (this.contact.CallingTerminal != null)
                {
                    return new NortelTerminal(this.contact.CallingTerminal);
                }
                else
                {
                    return null;
                }
            }*/
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public IContactCapabilities Capabilities
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public IConnection[] Connections
        {
            get 
            {
                Nortel.CCT.IConnection []cons = this.contact.Connections;
                NortelConnection[] retVal = new NortelConnection[cons.Length];
                for (int i = 0; i < cons.Length; i++)
                {
                    retVal[i] = new NortelConnection(cons[i]);
                }
                return retVal;
            
            }
        }

            public AttachedData Data
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        public string ID
        {
            get { return this.contact.ID; }
        }

        public IAddress LastRedirectedAddress
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public string OriginalDestination
        {
            get { return this.contact.OriginalDestination; }
        }

        public UserUserInfo UUI
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        #endregion
    }
}
