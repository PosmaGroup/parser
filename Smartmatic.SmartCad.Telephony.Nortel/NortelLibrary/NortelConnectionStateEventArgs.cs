using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony.NortelLibrary
{
    public class NortelConnectionStateEventArgs : ConnectionStateEventArgs
    {
        private Nortel.CCT.ConnectionStateEventArgs connectionStateEventArgs;

        public NortelConnectionStateEventArgs(Nortel.CCT.ConnectionStateEventArgs connectionStateEventArgs)
        {
            this.connectionStateEventArgs = connectionStateEventArgs;
        }

        public override IAddress Address
        {
            get
            {
                return new NortelAddress(connectionStateEventArgs.Address);
            }
        }

        public override IConnection Connection
        {
            get 
            {
                return new NortelConnection(connectionStateEventArgs.Connection); 
            }
        }

        public override IContact Contact
        {
            get 
            {
                return new NortelContact(connectionStateEventArgs.Contact);
            }
        }

        public override bool IsRemote
        {
            get 
            {
                return connectionStateEventArgs.IsRemote;
            }
        }

        public override ConnectionState NewState
        {
            get 
            {
                return (ConnectionState)connectionStateEventArgs.NewState;
            }
        }

        public override ConnectionState PreviousState
        {
            get 
            {
                return (ConnectionState)connectionStateEventArgs.PreviousState;
            }
        }

        public override Reason Reason
        {
            get 
            {
                return (Reason)connectionStateEventArgs.Reason;
            }
        }

        public override IAddress Sender
        {
            get 
            {
                return new NortelAddress(connectionStateEventArgs.Sender);
            }
        }

        public override long Sequence
        {
            get 
            {
                return connectionStateEventArgs.Sequence;
            }
        }
    }
}
