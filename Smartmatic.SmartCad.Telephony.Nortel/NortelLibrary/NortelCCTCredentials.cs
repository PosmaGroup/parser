using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony.NortelLibrary
{
    public class NortelCCTCredentials : CCTCredentials
    {
        private Nortel.CCT.CCTCredentials credentials = new Nortel.CCT.CCTCredentials();

        public Nortel.CCT.CCTCredentials NativeCredentials
        {
            get { return credentials; }
        }

        public NortelCCTCredentials(Nortel.CCT.CCTCredentials credentials)
        {
            this.credentials = credentials;
        }

        public NortelCCTCredentials()
        {
        }

        public override void SetSingleSignOn()
        {
            credentials.SetSingleSignOn();
        }

        public override void SetWindowsUser(string userid, string domain)
        {
            credentials.SetWindowsUser(userid, domain);
        }

        public override string Domain
        {
            get { return credentials.Domain; }
        }

        public override bool IsSingleSignOn
        {
            get
            {
                return credentials.IsSingleSignOn;
            }
        }

        public override string Password
        {
            get
            {
                return credentials.Password;
            }
            set
            {
                credentials.Password = value;
            }
        }

        public override string UserID
        {
            get { return credentials.UserID; }
        }
    }
}
