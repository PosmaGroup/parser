using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony.NortelLibrary
{
    [Serializable]
    public class NortelAgentTerminalProperty : AgentTerminalProperty
    {
        public NortelAgentTerminalProperty(string propertyName)
            : base(AgentTerminalProperty.GetTypeCode(propertyName))
        {
        }
    }
}
