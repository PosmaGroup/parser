using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony.NortelLibrary
{
    public class NortelAdressPropertyEventArgs : AddressPropertyEventArgs
    {
        private Nortel.CCT.AddressPropertyEventArgs args;
        public NortelAdressPropertyEventArgs(Nortel.CCT.AddressPropertyEventArgs args)
        {
            this.args = args;
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public override AddressProperty ChangedProperty
        {
            get { return (NortelAddressProperty)args.ChangedProperty; }
        }

        public override IAddress Sender
        {
            get { return new NortelAddress(args.Sender); }
        }

        public override long Sequence
        {
            get { return args.Sequence; }
        }
    }
}
