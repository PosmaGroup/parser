using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony.NortelLibrary
{
    [Serializable]
    public class NortelDoNotDisturbTerminalProperty : DoNotDisturbTerminalProperty
    {
        public NortelDoNotDisturbTerminalProperty(bool dnd)
            : base(dnd)
        {}
    }
}
