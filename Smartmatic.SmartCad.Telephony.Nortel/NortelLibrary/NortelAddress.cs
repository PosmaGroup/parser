using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony.NortelLibrary
{
    public class NortelAddress : IAddress
    {
        private Nortel.CCT.IAddress address;

        public NortelAddress(Nortel.CCT.IAddress address)
            : this(address, true)
        {
        }

        public NortelAddress(Nortel.CCT.IAddress address, bool handleConnectionStateChanged)
        {
            this.address = address;

            if (handleConnectionStateChanged)
            {
                this.address.ConnectionStateChanged += NortelToolkit.LastToolkit.CreateEventHandler(address_ConnectionStateChanged);
            }
        }

        private void address_ConnectionStateChanged(Nortel.CCT.ConnectionStateEventArgs e)
        {
            if (ConnectionStateChanged != null)
            {
                ConnectionStateChanged(new NortelConnectionStateEventArgs(e));
            }
        }

        public Nortel.CCT.IAddress NativeAddress
        {
            get { return address; }
        }

        #region IAddress Members

        public IConnection GetConnection(string ContactID)
        {
            return new NortelConnection(this.address.GetConnection(ContactID));
        }

        public IContact Originate(ITerminal origTerminal, string destAddress)
        {
            return new NortelContact(this.address.Originate(((NortelTerminal)origTerminal).NativeTerminal, destAddress));
        }

        public IAddressCapabilities Capabilities
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public IConnection[] Connections
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public bool DoNotDisturb
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        public bool IsControllable
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public bool IsForwarded
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public bool IsMessageWaiting
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public string Name
        {
            get
            {
                return this.address.Name;
            }
        }

        public ITerminal[] RelatedTerminals
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public ResourceState State
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public event ConnectionStateEventHandler ConnectionStateChanged;
        #endregion
    }
}
