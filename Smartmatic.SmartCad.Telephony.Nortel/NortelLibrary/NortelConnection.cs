using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony.NortelLibrary
{
    public class NortelConnection : IConnection
    {
        Nortel.CCT.IConnection connection;

        public NortelConnection(Nortel.CCT.IConnection connection)
        {
            this.connection = connection;
        }

        #region IConnection Members

        public void Accept()
        {
            this.connection.Accept();
        }

        public void Disconnect()
        {
            try
            {
                connection.Disconnect();
            }
            catch(Exception)
            {}
        }

        public IConnection Park(string destAddress)
        {
            return new NortelConnection(this.connection.Park(destAddress));
        }

        public IConnection Redirect(string destAddress)
        {
            return new NortelConnection(this.connection.Redirect(destAddress));
        }

        public void Reject()
        {
            this.connection.Reject();
        }

        public IAddress Address
        {
            get
            {
                return new NortelAddress(this.connection.Address);
            }
        }

        public IConnectionCapabilities Capabilities
        {
            get
            {
                return new NortelConnectionCapabilities(this.connection.Capabilities);
            }
        }

        public IContact Contact
        {
            get { return new NortelContact(this.connection.Contact); }
        }

        public string ContactID
        {
            get { return this.connection.ContactID; }
        }

        public ConnectionState CurrentState
        {
            get { return (ConnectionState)this.connection.CurrentState; }
        }

        public ConnectionState PreviousState
        {
            get { return (ConnectionState)this.connection.PreviousState; }
        }

        public Reason StateReason
        {
            get { return (Reason)this.connection.StateReason; }
        }

        public ITerminalConnection[] TerminalConnections
        {
            get
            {
                Nortel.CCT.ITerminalConnection[] connections = this.connection.TerminalConnections;
                NortelTerminalConnection[] retVal = new NortelTerminalConnection[connections.Length];
                int i = 0;
                foreach (Nortel.CCT.ITerminalConnection con in connections)
                {
                    retVal[i++] = new NortelTerminalConnection(con);
                }
                return retVal;
            }
        }

        #endregion
    }
}
