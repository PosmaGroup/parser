using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony.NortelLibrary
{
    public class NortelConnectionCapabilities : IConnectionCapabilities
    {
        private Nortel.CCT.IConnectionCapabilities connectionCapabilities;

        public NortelConnectionCapabilities(Nortel.CCT.IConnectionCapabilities connectionCapabilities)
        {
            this.connectionCapabilities = connectionCapabilities;
        }

        #region IConnectionCapabilities Members

        public bool CanAccept
        {
            get 
            {
                return connectionCapabilities.CanAccept;
            }
        }

        public bool CanDisconnect
        {
            get 
            {
                return connectionCapabilities.CanDisconnect;
            }
        }

        public bool CanPark
        {
            get 
            {
                return connectionCapabilities.CanPark;
            }
        }

        public bool CanRedirect
        {
            get 
            {
                return connectionCapabilities.CanRedirect;
            }
        }

        public bool CanReject
        {
            get 
            {
                return connectionCapabilities.CanReject;
            }
        }

        #endregion
    }
}
