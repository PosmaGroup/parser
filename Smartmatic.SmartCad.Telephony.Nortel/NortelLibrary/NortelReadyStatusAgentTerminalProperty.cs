using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony.NortelLibrary
{
    [Serializable]
    public class NortelReadyStatusAgentTerminalProperty : ReadyStatusAgentTerminalProperty
    {
        public NortelReadyStatusAgentTerminalProperty(bool ready, string reasoncode)
            : base(ready, reasoncode)
        {}
    }
}
