using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony.NortelLibrary
{
    public class NortelContactScopeEventArgs : ContactScopeEventArgs
    {
        private Nortel.CCT.ContactScopeEventArgs contactScopeEventargs;

        public NortelContactScopeEventArgs(Nortel.CCT.ContactScopeEventArgs contactScopeEventargs)
        {
            this.contactScopeEventargs = contactScopeEventargs;
        }

        public override IContact Contact
        {
            get { return new NortelContact(contactScopeEventargs.Contact); }
        }

        public override bool EnteringScope
        {
            get { return contactScopeEventargs.EnteringScope; }
        }

        public override bool LeavingScope
        {
            get { return contactScopeEventargs.LeavingScope; }
        }

        public override ISession Session
        {
            get { return new NortelSession(contactScopeEventargs.Session); }
        }

        public override long Sequence
        {
            get { return contactScopeEventargs.Sequence; }
        }
    }
}
