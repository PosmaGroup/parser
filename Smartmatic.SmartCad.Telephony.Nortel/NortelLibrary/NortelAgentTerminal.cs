using SmartCadCore.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony.NortelLibrary
{
    public class NortelAgentTerminal : IAgentTerminal
    {
        private Nortel.CCT.IAgentTerminal agentTerminal;

        public NortelAgentTerminal(Nortel.CCT.IAgentTerminal agentTerminal)
        {
            this.agentTerminal = agentTerminal;
            try
            {
                this.agentTerminal.TermConnStateChanged += NortelToolkit.LastToolkit.CreateEventHandler(terminal_TermConnStateChanged);
                this.agentTerminal.PropertyChanged += NortelToolkit.LastToolkit.CreateEventHandler(terminal_TerminalPropertyChanged);
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        public Nortel.CCT.IAgentTerminal NativeAgentTerminal
        {
            get
            {
                return agentTerminal;
            }
        }

        #region IAgentTerminal Members

        public void CallSupervisor(IAddress address)
        {
            agentTerminal.CallSupervisor(((NortelAddress)address).NativeAddress);
        }

        public void Login(string loginID)
        {
            agentTerminal.Login(loginID);
        }

        public void Login(string loginID, AgentState initialState)
        {
            agentTerminal.Login(loginID, (Nortel.CCT.AgentState)initialState);
        }

        public void Login(string loginID, string password)
        {
            agentTerminal.Login(loginID, password);
        }

        public void Login(string loginID, string password, AgentState initialState)
        {
            agentTerminal.Login(loginID, password, (Nortel.CCT.AgentState)initialState);
        }

        public void Logout()
        {
            try
            {
                agentTerminal.Logout();
            }
            catch (Nortel.Security.Remoting.RemotingException ex)
            {
                throw new RemotingException(ex.Message, ex.InnerException);
            }
            catch (Exception ex)
            {
                throw new RemotingException(ex.Message, ex.InnerException);
            }
        }

        public string ActivityCode
        {
            get
            {
                return agentTerminal.ActivityCode;
            }
            set
            {
                agentTerminal.ActivityCode = value;
            }
        }

        public bool IsReady
        {
            get
            {
                return agentTerminal.IsReady;
            }
            set
            {
                agentTerminal.IsReady = value;
            }
        }

        public string LoginID
        {
            get 
            {
                return agentTerminal.LoginID;
            }
        }

        public string NotReadyReasonCode
        {
            get
            {
                return agentTerminal.NotReadyReasonCode;
            }
            set
            {
                agentTerminal.NotReadyReasonCode = value;
            }
        }

        #endregion

        #region ITerminal Members

        public ITerminalConnection Pickup(IAddress origAddress, IAddress destAddress)
        {
            NortelAddress origAddressAux = origAddress as NortelAddress;
            NortelAddress destAddressAux = destAddress as NortelAddress;
            return new NortelTerminalConnection(agentTerminal.Pickup(origAddressAux.NativeAddress, destAddressAux.NativeAddress));
        }

        public ITerminalConnection Pickup(ITerminalConnection origTermConn, IAddress destAddress)
        {
            NortelTerminalConnection origTermConnAux = origTermConn as NortelTerminalConnection;
            NortelAddress destAddressAux = destAddress as NortelAddress;
            return new NortelTerminalConnection(agentTerminal.Pickup(origTermConnAux.NativeTerminalConnection, destAddressAux.NativeAddress));
        }

        public ITerminalConnection PickupFromGroup(IAddress destAddress)
        {
            NortelAddress destAddressAux = destAddress as NortelAddress;
            return new NortelTerminalConnection(agentTerminal.PickupFromGroup(destAddressAux.NativeAddress));
        }

        public ITerminalConnection PickupFromGroup(string pickupGroup, IAddress destAddress)
        {
            NortelAddress destAddressAux = destAddress as NortelAddress;
            return new NortelTerminalConnection(agentTerminal.PickupFromGroup(pickupGroup, destAddressAux.NativeAddress));
        }

        public ITerminalCapabilities Capabilities
        {
            get
            {
                if (agentTerminal.Capabilities is Nortel.CCT.IAgentTerminalCapabilities)
                    return new NortelAgentTerminalCapabilities(agentTerminal.Capabilities as Nortel.CCT.IAgentTerminalCapabilities);

                return null;
            }
        }

        public bool DoNotDisturb
        {
            get
            {
                return agentTerminal.DoNotDisturb;
            }
            set
            {
                agentTerminal.DoNotDisturb = value;
            }
        }

        public bool IsForwarded
        {
            get 
            {
                return agentTerminal.IsForwarded;
            }
        }

        public string Name
        {
            get 
            {
                return agentTerminal.Name; 
            }
        }

        public IAddress[] RelatedAddresses
        {
            get
            {
                IAddress[] retVal = null;
                Nortel.CCT.IAddress[] address = agentTerminal.RelatedAddresses;
                retVal = new IAddress[address.Length];
                int i = 0;
                foreach (Nortel.CCT.IAddress add in address)
                {
                    if (add is Nortel.CCT.IAgentAddress)
                    {
                        retVal[i++] = new NortelAgentAddress(add as Nortel.CCT.IAgentAddress);
                    }
                    else
                    {
                        retVal[i++] = new NortelAddress(add);
                    }
                }
                return retVal;
            }
        }

        public ResourceState State
        {
            get 
            {
                return (ResourceState)agentTerminal.State;
            }
        }

        public ITerminalConnection[] TerminalConnections
        {
            get 
            {
                ITerminalConnection[] retVal = null;
                Nortel.CCT.ITerminalConnection[] terminalConnections = agentTerminal.TerminalConnections;
                retVal = new ITerminalConnection[terminalConnections.Length];
                int i = 0;
                foreach (Nortel.CCT.ITerminalConnection terminalConnection in terminalConnections)
                {
                    retVal[i++] = new NortelTerminalConnection(terminalConnection);
                }
                return retVal;
            }
        }

        public IContact Originate(IAddress origAddress, string destAddress)
        {
            Nortel.CCT.IAddress originateAddress = (origAddress as NortelAddress).NativeAddress;
            return new NortelContact(agentTerminal.Originate(originateAddress, destAddress));
        }

        public event TermConnStateEventHandler TermConnStateChanged;

        private void terminal_TermConnStateChanged(Nortel.CCT.TermConnStateEventArgs e)
        {
            if (TermConnStateChanged != null)
            {
                TermConnStateChanged(new NortelTermConnStateEventArgs(e));
            }
        }

        public event TerminalPropertyEventHandler TerminalPropertyChanged;

        private void terminal_TerminalPropertyChanged(Nortel.CCT.TerminalPropertyEventArgs e)
        {
            if (TerminalPropertyChanged != null)
            {
                TerminalPropertyChanged(new NortelTerminalPropertyEventArgs(e));
            }
        }

        #endregion
    }
}
