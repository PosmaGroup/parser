using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony.NortelLibrary
{
    public class NortelAgentAddress : IAgentAddress
    {
        private Nortel.CCT.IAgentAddress agentAddress;

        public NortelAgentAddress(Nortel.CCT.IAgentAddress agentAddress)
        {
            this.agentAddress = agentAddress;
            this.agentAddress.ConnectionStateChanged += NortelToolkit.LastToolkit.CreateEventHandler(agentAddress_ConnectionStateChanged);
        }

        private void agentAddress_ConnectionStateChanged(Nortel.CCT.ConnectionStateEventArgs e)
        {
            if (ConnectionStateChanged != null)
            {
                ConnectionStateChanged(new NortelConnectionStateEventArgs(e));
            }
        }

        #region IAddress Members

        public IConnection GetConnection(string ContactID)
        {
            return new NortelConnection(agentAddress.GetConnection(ContactID));
        }

        public IContact Originate(ITerminal origTerminal, string destAddress)
        {
            return new NortelContact(this.agentAddress.Originate(((NortelTerminal)origTerminal).NativeTerminal, destAddress));
        }

        public IAddressCapabilities Capabilities
        {
            get 
            {
                return new NortelAddressCapabilities(agentAddress.Capabilities);
            }
        }

        public IConnection[] Connections
        {
            get 
            {
                NortelConnection[] retVal = null;
                Nortel.CCT.IConnection[] connections = agentAddress.Connections;
                retVal = new NortelConnection[connections.Length];
                int i = 0;
                foreach (Nortel.CCT.IConnection connection in connections)
                {
                    retVal[i++] = new NortelConnection(connection);
                }
                return retVal;
            }
        }

        public bool DoNotDisturb
        {
            get
            {
                return agentAddress.DoNotDisturb;
            }
            set
            {
                agentAddress.DoNotDisturb = value;
            }
        }

        public bool IsForwarded
        {
            get 
            {
                return agentAddress.IsForwarded;
            }
        }

        public bool IsMessageWaiting
        {
            get 
            {
                return agentAddress.IsMessageWaiting;
            }
        }

        public string Name
        {
            get 
            {
                return agentAddress.Name;
            }
        }

        public ITerminal[] RelatedTerminals
        {
            get 
            {
                ITerminal[] retVal = null;
                Nortel.CCT.ITerminal[] terminals = agentAddress.RelatedTerminals;
                retVal = new ITerminal[terminals.Length];
                int i = 0;
                foreach (Nortel.CCT.ITerminal terminal in terminals)
                {
                    if (terminal is Nortel.CCT.IAgentTerminal)
                    {
                        retVal[i++] = new NortelAgentTerminal(terminal as Nortel.CCT.IAgentTerminal) as ITerminal;
                    }
                    else
                    {
                        retVal[i++] = new NortelTerminal(terminal);
                    }
                }
                return retVal;
            }
        }

        public ResourceState State
        {
            get 
            {
                return (ResourceState)agentAddress.State;
            }
        }

        public event ConnectionStateEventHandler ConnectionStateChanged;

        #endregion
    }
}
