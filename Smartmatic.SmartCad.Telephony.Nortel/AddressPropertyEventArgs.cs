using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony
{
    [Serializable]
    public abstract class AddressPropertyEventArgs : CCTEventArgs
    {
        // Methods
        // Properties
        public abstract AddressProperty ChangedProperty { get; }
        public abstract IAddress Sender { get; }

        // Fields
        protected readonly AddressProperty _property;
        protected readonly IAddress _sender;
    }
}
