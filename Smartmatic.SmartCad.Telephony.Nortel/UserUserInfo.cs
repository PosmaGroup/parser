using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony
{
    [Serializable]
    public abstract class UserUserInfo
    {
        // Properties
        public abstract byte[] BinaryData { get; set; }

        // Fields
        protected byte[] _data;
    }
}
