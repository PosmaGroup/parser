using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony.NortelLibrary
{
    public class NortelTerminalConnection : ITerminalConnection
    {

        private Nortel.CCT.ITerminalConnection iTerminalConnection;

        public NortelTerminalConnection(Nortel.CCT.ITerminalConnection iTerminalConnection)
        {
            this.iTerminalConnection = iTerminalConnection;
        }

        public Nortel.CCT.ITerminalConnection NativeTerminalConnection
        {
            get
            {
                return iTerminalConnection;
            }
        }

        #region ITerminalConnection Members

        public void Answer()
        {
            this.iTerminalConnection.Answer();
        }

        public void BlindTransfer(string destAddress)
        {
            this.iTerminalConnection.BlindTransfer(destAddress);
        }

        public void CompleteConference(IContact consultContact)
        {
            this.iTerminalConnection.CompleteConference(((NortelContact)consultContact).NativeContact);
        }


        public void CompleteSupervisedTransfer(IContact consultContact)
        {
            this.iTerminalConnection.CompleteSupervisedTransfer(((NortelContact)consultContact).NativeContact);
        }

        public IContact Consult(string destAddress)
        {
            return new NortelContact(this.iTerminalConnection.Consult(destAddress));
        }

        public void GenerateDTMF(string tones)
        {
            this.iTerminalConnection.GenerateDTMF(tones);
        }

        public void Hold()
        {
            this.iTerminalConnection.Hold();
        }

        public IContact InitiateConference(string destAddress)
        {
            return new NortelContact(this.iTerminalConnection.InitiateConference(destAddress));
        }

        public IContact InitiateSupervisedTransfer(string destAddress)
        {
            return new NortelContact(this.iTerminalConnection.InitiateSupervisedTransfer(destAddress));
        }

        public void Unhold()
        {
            this.iTerminalConnection.Unhold();
        }

        public ITerminalConnectionCapabilities Capabilities
        {
            get
            {
                return new NortelTerminalConnectionCapabilities(this.iTerminalConnection.Capabilities);
            }
        }

        public IContact ConferenceConsultContact
        {
            get { return new NortelContact(this.iTerminalConnection.ConferenceConsultContact); }
        }

        public IConnection Connection
        {
            get { return new NortelConnection(this.iTerminalConnection.Connection); }
        }

        public IContact ConsultContact
        {
            get { return new NortelContact(this.iTerminalConnection.ConsultContact); }
        }

        public IContact Contact
        {
            get { return new NortelContact(this.iTerminalConnection.Contact); }
        }

        public string ContactID
        {
            get { return this.iTerminalConnection.ContactID; }
        }

        public TerminalConnectionState CurrentState
        {
            get { return (TerminalConnectionState)(NortelTerminalConnectionState)(this.iTerminalConnection.CurrentState); }
        }

        public TerminalConnectionState PreviousState
        {
            get { return (TerminalConnectionState)(NortelTerminalConnectionState)this.iTerminalConnection.PreviousState; }
        }

        public Reason StateReason
        {
            get { return (Reason)(NortelReason)this.iTerminalConnection.StateReason; }
        }

        public ITerminal Terminal
        {
            get { return new NortelTerminal(this.iTerminalConnection.Terminal); }
        }

        public IContact TransferConsultContact
        {
            get { return new NortelContact(this.iTerminalConnection.TransferConsultContact); }
        }

        #endregion
    }
}
