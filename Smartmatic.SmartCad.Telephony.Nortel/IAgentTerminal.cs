using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony
{
    public interface IAgentTerminal : ITerminal
    {
        // Methods
        void CallSupervisor(IAddress address);
        void Login(string loginID);
        void Login(string loginID, AgentState initialState);
        void Login(string loginID, string password);
        void Login(string loginID, string password, AgentState initialState);
        void Logout();

        // Properties
        string ActivityCode { get; set; }
        bool IsReady { get; set; }
        string LoginID { get; }
        string NotReadyReasonCode { get; set; }
    }
 

}
