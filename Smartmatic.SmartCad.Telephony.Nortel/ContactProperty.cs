using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony
{
    [Serializable]
    public class ContactProperty : Property
    {
        // Fields
        public static readonly ContactProperty AttachedData;
        public static readonly ContactProperty CalledAddress;
        public static readonly ContactProperty CallingAddress;
        public static readonly ContactProperty LastRedirectedAddress;
        public static readonly ContactProperty UserUserInfo;

        static ContactProperty()
        {
            AttachedData = new ContactProperty(0);
            CalledAddress = new ContactProperty(1);
            CallingAddress = new ContactProperty(2);
            LastRedirectedAddress = new ContactProperty(3);
            UserUserInfo = new ContactProperty(4); 
        }

        protected ContactProperty(int type)
            :base(type)
        {}

        // Nested Types
        protected enum Type
        {
            AttachedData,
            CalledAddress,
            CallingAddress,
            BlindTransferCompleted,
            ConferenceCompleted,
            ConferenceInitiated,
            Consulted,
            LastRedirectedAddress,
            SupervisedTransferCompleted,
            SupervisedTransferInitiated,
            UserUserInfo
        }
    }
}
