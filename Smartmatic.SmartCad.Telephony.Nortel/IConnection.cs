using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony
{
    public interface IConnection
    {
        // Methods
        void Accept();
        void Disconnect();
        IConnection Park(string destAddress);
        IConnection Redirect(string destAddress);
        void Reject();

        // Properties
        IAddress Address { get; }
        IConnectionCapabilities Capabilities { get; }
        IContact Contact { get; }
        string ContactID { get; }
        ConnectionState CurrentState { get; }
        ConnectionState PreviousState { get; }
        Reason StateReason { get; }
        ITerminalConnection[] TerminalConnections { get; }
    }
}
