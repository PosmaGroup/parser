﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SmartCadCore.Common;

namespace Smartmatic.SmartCad.Telephony
{
    public partial class NortelConfigurationControl : UserControl, IConfigurationControl
    {
        public string ServerStat
        {
            get
            {
                return textBoxServer.Text;
            }
            private set
            {
                textBoxServer.Text = value;
            }
        }
        public string UserNameStat
        {
            get
            {
                return textBoxUserName.Text;
            }
            private set
            {
                textBoxUserName.Text = value;
            }
        }
        public string PasswordStat
        {
            get
            {
                return textBoxPassword.Text;
            }
            private set
            {
                textBoxPassword.Text = value;
            }
        }
        public string ScriptStat
        {
            get
            {
                return textBoxExScript.Text;
            }
            private set
            {
                textBoxExScript.Text = value;
            }
        }
        public string SipServerPort
        {
            get
            {
                return textBoxExSIPServerPort.Text;
            }
            private set
            {
                textBoxExSIPServerPort.Text = value;
            }
        }
        public string SipServerIp
        {
            get
            {
                return textBoxExSipServerIp.Text;
            }
            private set
            {
                textBoxExSipServerIp.Text = value;
            }
        }


        public NortelConfigurationControl()
        {
            InitializeComponent();
        }

        private void LoadLanguage()
        {
            Text = ResourceLoader.GetString2(this.Name);
            this.groupControlStatistic.Text = ResourceLoader.GetString2("StatisticConfiguration");
            labelServer.Text = ResourceLoader.GetString2("Server") + " :*";

            labelUserName.Text = ResourceLoader.GetString2("UserName") + " :*";
            labelPassword.Text = ResourceLoader.GetString2("Password") + " :";
            labelScript.Text = ResourceLoader.GetString2("Script") + " :*";
            this.labelExSipServerIp.Text = ResourceLoader.GetString2("SipServerIpOrName") + " :*";
            this.labelExSipServerPort.Text = ResourceLoader.GetString2("SipServerPort") + ":*";
            this.groupControlTelephonyConnection.Text = ResourceLoader.GetString2("TelephonyConfiguration");
        }

        #region IConfigurationControl Members

        public event Parameters_ChangeEvent FulFilled_Change;

        public void SetProperties(Dictionary<string, string> properties)
        {
            foreach (string propName in properties.Keys)
            {
                switch (propName)
                {
                    case NortelProperties.SIP_SERVER_IP:
                        SipServerIp = properties[propName];
                        break;
                    case NortelProperties.SIP_SERVER_PORT:
                        SipServerPort = properties[propName];
                        break;
                    default:
                        break;
                }
            }
        }

        public Dictionary<string, string> GetProperties()
        {
            Dictionary<string, string> props = new Dictionary<string, string>();

            props.Add(NortelProperties.SIP_SERVER_IP, SipServerIp);
            props.Add(NortelProperties.SIP_SERVER_PORT, SipServerPort);

            return props;
        }

        public void SetStatisticProperties(string[] statisticProperties)
        {
            ServerStat = statisticProperties[0];
            UserNameStat = statisticProperties[1];
            PasswordStat = statisticProperties[2];
            ScriptStat = statisticProperties[3];
        }

        public string[] GetStatisticProperties()
        {
            string[] props = new string[8];

            props[0] = ServerStat;
            props[1] = UserNameStat;
            props[2] = PasswordStat;
            props[3] = ScriptStat;

            return props;
        }

        #endregion

        bool isFullFilled = false;
        private void textBoxEx_TextChanged(object sender, EventArgs e)
        {
            if (((string.IsNullOrEmpty(ServerStat) == true &&
                     string.IsNullOrEmpty(UserNameStat) == true &&
                //string.IsNullOrEmpty(PasswordStat) == true &&
                     string.IsNullOrEmpty(ScriptStat) == true) ||
                     (string.IsNullOrEmpty(ServerStat) == false &&
                     string.IsNullOrEmpty(UserNameStat) == false &&
                //string.IsNullOrEmpty(PasswordStat) == false &&
                     string.IsNullOrEmpty(ScriptStat) == false)) &&
                     (string.IsNullOrEmpty(SipServerIp) == false &&
                     string.IsNullOrEmpty(SipServerPort) == false))
            {
                if (!isFullFilled && FulFilled_Change != null)
                {
                    FulFilled_Change(true);
                }
                isFullFilled = true;
            }
            else
            {
                if (isFullFilled && FulFilled_Change != null)
                {
                    FulFilled_Change(false);
                }
                isFullFilled = false;
            }
        }
    }
}
