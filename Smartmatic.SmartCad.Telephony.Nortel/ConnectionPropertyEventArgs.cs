using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony
{
    [Serializable]
    public abstract class ConnectionPropertyEventArgs : CCTEventArgs
    {
        // Properties
        public abstract ConnectionProperty ChangedProperty { get; }
        public abstract IConnection Sender { get; }

        // Fields
        protected readonly ConnectionProperty _property;
        protected readonly IConnection _sender;
    }
}
