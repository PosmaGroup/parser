using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony
{
    public class ActivityCodeAgentTerminalProperty : AgentTerminalProperty
    {
        // Fields
        public readonly string Code;

        public ActivityCodeAgentTerminalProperty(string code)
            :base(6)
        {
            this.Code = code;
        }
    }
}
