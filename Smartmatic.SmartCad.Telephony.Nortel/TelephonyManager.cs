using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Collections;
using SmartCadCore.Common;
using SmartCadCore.Core;

namespace Smartmatic.SmartCad.Telephony
{
    public class TelephonyManager : TelephonyManagerAbstract
    {   
        private Toolkit toolkit;
        private ISession session;
        private IAgentTerminal terminal;
        private IAddress address;
        private ITerminalConnection terminalConnection;
        private IConnection connection;
        private bool answerCall = false;

        private string extensionNumber;
        private string agentId;
        private string server;
        private int port;
        private CCTCredentials credentials;
        private Timer doNotDisturbTimer;
        //In case the disconnect event is activate but the CCT ot CCM is down
        private Timer disconnectTimer;
        private Timer endCallTimer;

        public TelephonyManager(Hashtable properties)
            : base(properties)
        {
            doNotDisturbTimer = new Timer(new TimerCallback(doNotDisturb_TimerCallBack));
            disconnectTimer = new Timer(new TimerCallback(DisconnectAction));
            endCallTimer = new Timer(new TimerCallback(CheckServer));
            Init();
            credentials = new NortelLibrary.NortelCCTCredentials();
            credentials.SetWindowsUser((string)properties[CommonProperties.UserLogin],
                (string)properties[CommonProperties.Domain]);
            credentials.Password = (string)properties[CommonProperties.UserPassword];
            try
            {
                this.Port = Convert.ToInt32(properties[NortelProperties.SIP_SERVER_PORT]);
                this.Server = (string)properties[NortelProperties.SIP_SERVER_IP];
            }
            catch(Exception ex)
            {
                throw new TelephonyException(ResourceLoader.GetString2("TelphonyServerNotConfigured"), ex);
            }
            TakeControl((string)properties[CommonProperties.Extension]);
            Login((string)properties[CommonProperties.AgentId],
                (string)properties[CommonProperties.AgentPassword],
                (bool)properties[CommonProperties.Ready]);            
        }

        public override string TestCTIState()
        {
            string str = string.Empty;
            str = Terminal.Name;
            return str;
        }

        internal Toolkit Toolkit
        {
            get 
            { 
                return toolkit; 
            }
        }

        internal ISession Session
        {
            get
            {
                return session;
            }
        }

        public IAddress Address
        {
            get
            {
                return address;
            }
        }

        public ITerminal Terminal
        {
            get
            {
                return terminal;
            }
        }

        internal string ExtensionNumber
        {
            get
            {
                return extensionNumber;
            }
        }

        internal string AgentId
        {
            get
            {
                return agentId;
            }
        }

        internal string Server
        {
            get
            {
                return server;
            }
            set
            {
                server = value;
                toolkit.Server = server;
            }
        }

        internal int Port
        {
            get
            {
                return port;
            }
            set
            {
                port = value;
                toolkit.Port = port;
            }
        }

        public CCTCredentials Credentials
        {
            get
            {
                return credentials;
            }
            set 
            {
                credentials = value;
                toolkit.Credentials = credentials;
            }
        }

        private void LogoutFromOtherTerminals(string agentId)
        {
            int i = 0;
            ITerminal currentTerminal;
            ITerminal[] terminals;

            terminals = session.Terminals;

            while (i < terminals.Length)
            {
                currentTerminal = terminals[i];

                if (currentTerminal is IAgentTerminal)
                {
                    IAgentTerminal temp = currentTerminal as IAgentTerminal;

                    IAgentTerminalCapabilities capabilities = temp.Capabilities as IAgentTerminalCapabilities;
                    
                    if (capabilities.CanLogout == true &&
                        temp.LoginID != null &&
                        temp.LoginID.Trim() != string.Empty &&
                        temp.LoginID.Trim() == agentId)
                    {
                        temp.Logout();
                    }
                }

                i++;
            }
        }

        public override void Call(string number)
        {
            throw new NotImplementedException();
        }

        public override void Login(string agentId, string password, bool isReady)
        {
            try
            {
                IAgentTerminalCapabilities capabilities = terminal.Capabilities as IAgentTerminalCapabilities;
                
                if (capabilities.CanLogout == true &&
                    terminal.LoginID != null &&
                    terminal.LoginID.Trim() != string.Empty &&
                    terminal.LoginID.Trim() != agentId)
                {
                    throw new ApplicationException(ResourceLoader.GetString2("TerminalInUse"));
                }
                
                if (terminal.TerminalConnections.Length > 0)
                {
                    foreach(ITerminalConnection terminalConnection in terminal.TerminalConnections)
                    {
                        if (terminalConnection.Connection != null &&
                            terminalConnection.Connection.Capabilities != null &&
                            terminalConnection.Connection.Capabilities.CanDisconnect == true)
                        {
                            terminalConnection.Connection.Disconnect();
                        }
                    }
                }

                if (capabilities != null &&
                    capabilities.CanDoNotDisturb == true &&
                    terminal.DoNotDisturb == true)
                    terminal.DoNotDisturb = false;

                if (capabilities != null &&
                    capabilities.CanLogin == false &&
                    capabilities.CanLogout == true)
                {
                    try
                    {
                        terminal.Logout();
                    }
                    catch (Exception ex)
                    {
                        throw new TelephonyException("No se puede tomar control del tel�fono, revise el servidor de tel�fonia.", ex);
                    }
                }

                LogoutFromOtherTerminals(agentId);

                if (capabilities != null && capabilities.CanLogin == true)
                {
                    try
                    {
                        AgentState agentState = AgentState.NotReady;
                        if (isReady == true)
                            agentState = AgentState.Ready;
                        terminal.Login(agentId, agentState);
                        this.agentId = agentId;
                    }
                    catch (Exception ex)
                    {
                        throw new TelephonyException(ResourceLoader.GetString2("UnableLoginInTerminal"), ex);
                    }
                }
                else
                    throw new TelephonyException(ResourceLoader.GetString2("UnableLoginInTerminal"), null);
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
                throw ex;
            }
        }


        public override void Logout()
        {
            try
            {
                IAgentTerminalCapabilities capabilities = terminal.Capabilities as IAgentTerminalCapabilities;

                if (capabilities != null &&
                    capabilities.CanLogin == false &&
                    capabilities.CanLogout == true)
                    terminal.Logout();

                try
                {
                    if (capabilities != null &&
                        capabilities.CanDoNotDisturb == true)
                        terminal.DoNotDisturb = false;
                }
                catch(Exception ex)
                {
                    SmartLogger.Print(ex);
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
                throw new TelephonyException(ex.Message, ex);
            }
        }

        public void TakeControl(string extensionName)
        {
            try
            {
                session = toolkit.Connect();
                SelectTerminalAndAddress(extensionName);
                this.extensionNumber = extensionName;
                address.ConnectionStateChanged += new ConnectionStateEventHandler(address_ConnectionStateChanged);
                terminal.TermConnStateChanged += new TermConnStateEventHandler(terminal_TermConnStateChanged);
                terminal.TerminalPropertyChanged += new TerminalPropertyEventHandler(terminal_TerminalPropertyChanged);
            }
            catch(System.Net.Sockets.SocketException)
            {
                throw new ApplicationException(ResourceLoader.GetString2("UnavailableTelephonyServer"));
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
                throw ex;
            }
        }

        private void CheckServer(object data)
        {
            bool serverRunning = false;
            try
            {
                IAgentTerminalCapabilities capabilities = terminal.Capabilities as IAgentTerminalCapabilities;
                serverRunning = true;
            }
            catch { }

            if (serverRunning == false)
            {
                DisconnectAction(null);
            }
        }

        private void DisconnectAction(object data)
        {
            disconnectTimer.Change(-1, -1);
            endCallTimer.Change(-1, -1);

            SendTelephonyEventArgs(new CallReleasedEventArgs(null, null, null, TimeSpan.Zero));
            
            if (answerCall == true)
                SetReadyStatus(false);

            connection = null;
            terminalConnection = null;
            answerCall = false;
        }

        private void doNotDisturb_TimerCallBack(object data)
        {
            try
            {
                IAgentTerminalCapabilities capabilities = terminal.Capabilities as IAgentTerminalCapabilities;
                if (capabilities.CanDoNotDisturb == true && terminal.DoNotDisturb == true)
                {
                    terminal.DoNotDisturb = false;
                    return;
                }
                else
                {
                    SmartLogger.Print("DoNotDisturb to default value false (DoNotDisturb == false)");
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
                return;
            }

            SendTelephonyEventArgs(new DoNotDisturbChangedEventArgs(false));
            
            try
            {
                if (answerCall == false)
                {
                    IAgentTerminalCapabilities capabilities = terminal.Capabilities as IAgentTerminalCapabilities;
                    if (capabilities != null && capabilities.CanLogin == true)
                    {
                        try
                        {
                            AgentState agentState = AgentState.NotReady;
                            //if (IsReady == true)
                            //    agentState = AgentState.Ready;

                            terminal.Login(AgentId, agentState);
                        }
                        catch (Exception ex)
                        {
                            SmartLogger.Print(ex);
                        }
                    }
                    else
                    {
                        SmartLogger.Print("No se puede hacer login sobre la terminal.");
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }

            /*if (DoNotDisturbChanged != null)
            {
                DoNotDisturbChanged(this, args);
            }*/
        }

        void terminal_TerminalPropertyChanged(TerminalPropertyEventArgs e)
        {
            try
            {
                if (e.ChangedProperty is ReadyStatusAgentTerminalProperty)
                {
                    ReadyStatusAgentTerminalProperty property = (ReadyStatusAgentTerminalProperty)e.ChangedProperty;
                    ReadyStatusChangedEventArgs args = new ReadyStatusChangedEventArgs(property.IsReady, property.NotReadyReasonCode);
                    Console.WriteLine("Test terminal ready " + property.IsReady.ToString());

                    SendTelephonyEventArgs(args);                   
                }
                else if (e.ChangedProperty is DoNotDisturbTerminalProperty)
                {
                    DoNotDisturbTerminalProperty property = (DoNotDisturbTerminalProperty)e.ChangedProperty;
                    DoNotDisturbChangedEventArgs args = new DoNotDisturbChangedEventArgs(property.DoNotDisturbValue);
                    Console.WriteLine("Test terminal do not disturb " + property.DoNotDisturbValue.ToString());

                    doNotDisturbTimer.Change(1000, -1);
                }
                else if (e.ChangedProperty is ActivityCodeAgentTerminalProperty)
                {
                    ActivityCodeAgentTerminalProperty property = (ActivityCodeAgentTerminalProperty)e.ChangedProperty;
                    Console.WriteLine("test activity code " + property.Code);
                }
                else if (e.ChangedProperty == AgentTerminalProperty.AgentLoggedIn)
                {
                    Console.WriteLine("test agent logged in");
                }
                else if (e.ChangedProperty == AgentTerminalProperty.AgentLoggedOut)
                {
                    Console.WriteLine("test agent logged out");
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        private void SelectTerminalAndAddress(string extensionName)
        {
            bool found = false;
            int i = 0;
            ITerminal currentTerminal;
            ITerminal[] terminals;

            terminals = session.Terminals;

            while (found == false && i < terminals.Length)
            {
                currentTerminal = terminals[i];

                foreach (IAddress currentAddress in currentTerminal.RelatedAddresses)
                {
                    if (currentAddress is IAgentAddress)
                    {
                        address = currentAddress;
                    }

                    if (currentAddress.Name == extensionName)
                    {
                        if (currentTerminal is IAgentTerminal)
                        {
                            terminal = currentTerminal as IAgentTerminal;
                        }
                        else
                        {
                            throw new ApplicationException(ResourceLoader.GetString2("NoCapableTerminal"));
                        }
                        found = true;
                    }
                }

                i++;
            }

            if (terminal == null)
                throw new ApplicationException(ResourceLoader.GetString2("ExtensionNotProperlyConfigured"));
        }

        private void terminal_TermConnStateChanged(TermConnStateEventArgs e)
        {
        }

        private void address_ConnectionStateChanged(ConnectionStateEventArgs e)
        {
            try
            {
                SmartLogger.Print("address_ConnectionStateChanged(Begin)");
                SmartLogger.Print("State transition: " + e.PreviousState + " -> " + e.NewState);

                if (e.NewState == ConnectionState.Alerting)
                {
                    IConnection connectionTemp = e.Connection;
                    IContact contact = connectionTemp.Contact;
                    IAddress callingAddress = contact.CallingAddress;

                    if (answerCall == false)
                    {
                        CallRingingEventArgs args = new CallRingingEventArgs(callingAddress.Name, null, null, TimeSpan.Zero);
                        
                        connection = connectionTemp;
                        
                        foreach (ITerminalConnection currentTerminalConnection in connection.TerminalConnections)
                        {
                            if (currentTerminalConnection.CurrentState == TerminalConnectionState.Ringing)
                            {
                                terminalConnection = currentTerminalConnection;
                                break;
                            }
                        }

                        if (terminalConnection == null)
                            SmartLogger.Print("Error no se consiguio la conexi�n");
                        SmartLogger.Print("Invoking IncomingCall");
                        SendTelephonyEventArgs(args);                        
                    }
                }
                else if (e.NewState == ConnectionState.Established)
                {
                    answerCall = true;
                    endCallTimer.Change(5000, 5000);
                    SendTelephonyEventArgs(new CallEstablishedEventArgs(null, null, null, TimeSpan.Zero));
                }
                else if (e.NewState == ConnectionState.Disconnected)
                {
                    DisconnectAction(null);
                }

                SmartLogger.Print("address_ConnectionStateChanged(End)");
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        public override void RecoverConnection()
        {
            try
            {
                Close();
                Init();
                toolkit.Server = Server;
                toolkit.Port = Port;
                toolkit.Credentials = Credentials;
                TakeControl(ExtensionNumber);
                Login(AgentId, "", true);
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
                throw ex;
            }
        }

        public override void Answer()
        {
            try
            {
                if (terminalConnection != null && terminalConnection.CurrentState == TerminalConnectionState.Ringing)
                {
                    terminalConnection.Answer();
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
                //throw ex;
            }
        }

        public override void Close()
        {
            try
            {
                toolkit.Disconnect();
                doNotDisturbTimer.Dispose();
                disconnectTimer.Dispose();  
                endCallTimer.Dispose();
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        protected override void Init()
        {
            toolkit = new NortelLibrary.NortelToolkit();
        }

        public override void Hold()
        {
            
        }

        public override void SetReadyStatus(bool ready)
        {
            try
            {
                IAgentTerminalCapabilities capabilities = terminal.Capabilities as IAgentTerminalCapabilities;
                if (capabilities.CanGetReadyStatus == true && terminal.IsReady != ready &&
                    capabilities.CanSetReadyStatus == true)
                    terminal.IsReady = ready;
                else
                {
                    SmartLogger.Print("IsReady to default value false (CanSetReadyStatus == false)");
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        public override bool GetReadyStatus()
        {
            try
            {
                bool result = false;
                if ((terminal.Capabilities as IAgentTerminalCapabilities).CanGetReadyStatus == true)
                    result = terminal.IsReady;
                else
                {
                    SmartLogger.Print("IsReady to default value false (CanGetReadyStatus == false)");
                }
                return result;
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
                return false;
            }
        }

        public override void Release()
        {
            try
            {
                if (connection != null && connection.CurrentState == ConnectionState.Established)
                {
                    connection.Disconnect();
                }
                disconnectTimer.Change(5000, -1);
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
                RecoverConnection();
                //throw ex;
            }
        }

        public override void Transfer()
        {
            
        }

        public override void Conference()
        {
            
        }

        public override void ParkCall(string extensionParking, string parkId)
        {
            
        }

        public override void SetPhoneReport(string customCode)
        {
            throw new NotImplementedException();
        }
    }
}
