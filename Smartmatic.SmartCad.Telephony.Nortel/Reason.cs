using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony
{
    public enum Reason
    {
        Default,
        BargeIn,
        CallSupervisor,
        ConferenceComplete,
        ConferenceInitiated,
        ConsultComplete,
        ConsultInitiated,
        Emergency,
        Observe,
        Park,
        Redirect,
        Reject,
        Route,
        Shutdown,
        TransferComplete,
        TransferInitiated,
        Whisper
    }
}
