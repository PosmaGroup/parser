using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony
{
    public enum TerminalConnectionState
    {
        Active,
        Bridged,
        Dropped,
        Held,
        Idle,
        InUse,
        Ringing,
        Unknown
    }
}
