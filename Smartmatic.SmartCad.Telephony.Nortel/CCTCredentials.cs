using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony
{
    public abstract class CCTCredentials
    {
        // Methods
        public abstract void SetSingleSignOn();
        public abstract void SetWindowsUser(string userid, string domain);

        // Properties
        public abstract string Domain { get; }
        public abstract bool IsSingleSignOn { get; }
        public abstract string Password { get; set; }
        public abstract string UserID { get; }

        // Fields
        protected string _domain;
        protected string _password;
        protected bool _sso;
        protected string _userid;
    }
}
