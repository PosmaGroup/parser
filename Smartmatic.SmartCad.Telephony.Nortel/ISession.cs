using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony
{
    public interface ISession
    {
        // Events
        event AddressStateEventHandler AddressStateChanged;
        event ContactScopeEventHandler ContactEnteringScope;
        
        // Methods
        IAddress GetAddress(string addressName);
        ITerminal GetTerminal(string terminalName);

        // Properties
        IAddress[] Addresses { get; }
        IContact[] CurrentContacts { get; }
        Guid ID { get; }
        ITerminal[] Terminals { get; }
    }
}
