using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony
{
    [Serializable]
    public class AgentTerminalProperty : TerminalProperty
    {
        public static readonly AgentTerminalProperty ActivityCode;
        public static readonly AgentTerminalProperty AgentLoggedIn;
        public static readonly AgentTerminalProperty AgentLoggedOut;
        public static readonly AgentTerminalProperty ReadyStatus;

        static AgentTerminalProperty()
        {
            ActivityCode = new AgentTerminalProperty(4);
            AgentLoggedIn = new AgentTerminalProperty(5);
            AgentLoggedOut = new AgentTerminalProperty(6);
            ReadyStatus = new AgentTerminalProperty(7);
        }

        protected AgentTerminalProperty(int type)
            :base(type)
        {}

        protected enum Type
        {
            AgentLoggedIn = 4,
            AgentLoggedOut = 5,
            ActivityCode = 6,
            ReadyStatus = 7,
        }

        public static int GetTypeCode(string typeName)
        {
            foreach (string name in Enum.GetNames(typeof(Type)))
            {
                if (typeName.Contains(name) == true)
                {
                    return (int)Enum.Parse(typeof(Type), name);
                }
            }
            return 0;
        }
    }
}
