using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony
{
    [Serializable]
    public abstract class CCTEventArgs : EventArgs
    {
        // Methods
        public abstract long Sequence { get; }

        // Fields
        protected readonly long _sequence;
    }
}
