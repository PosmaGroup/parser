using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony
{
    public interface IContactCapabilities
    {
        // Properties
        bool CanAddParty { get; }
        bool CanBargeIn { get; }
        bool CanDrop { get; }
        bool CanGetAttachedData { get; }
        bool CanGetUUI { get; }
        bool CanObserve { get; }
        bool CanSetAttachedData { get; }
        bool CanSetUUI { get; }
        bool CanWhisper { get; }
    }
}
