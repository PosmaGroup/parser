using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony
{
    public class ReadyStatusAgentTerminalProperty : AgentTerminalProperty
    {
        // Fields
        public readonly bool IsReady;
        public readonly string NotReadyReasonCode;

        // Methods
        public ReadyStatusAgentTerminalProperty(bool ready, string reasoncode)
            : base(7)
        {
            this.IsReady = ready;
            this.NotReadyReasonCode = reasoncode;
        }
    }
}
