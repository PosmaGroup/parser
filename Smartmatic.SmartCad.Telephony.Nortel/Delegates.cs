using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony
{
    public delegate void AddressStateEventHandler(AddressStateEventArgs e);
    public delegate void ConnectionPropertyEventHandler(ConnectionPropertyEventArgs e);
    public delegate void ConnectionStateEventHandler(ConnectionStateEventArgs e);
    public delegate void ContactPropertyEventHandler(ContactPropertyEventArgs e);
    public delegate void ContactScopeEventHandler(ContactScopeEventArgs e);
    public delegate void TermConnStateEventHandler(TermConnStateEventArgs e);
    public delegate void TerminalStateEventHandler(TerminalStateEventArgs e);
    public delegate void TerminalPropertyEventHandler(TerminalPropertyEventArgs e);
    public delegate void SessionDisconnectedEventHandler(SessionDisconnectedEventArgs args);


}
