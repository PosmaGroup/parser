using SmartCadCore.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony.NortelLibrary
{   
    [Serializable]
    public class NortelSession : ISession
    {
        public event EventHandler<CallRingingEventArgs> IncomingCall;
        public event EventHandler<CallReleasedEventArgs> EndingCall;
        public event EventHandler<NewNotReadyStatusEventArgs> NewNotReadyStatus;
        private string agentId;

        private Nortel.CCT.ISession session;

        public NortelSession(Nortel.CCT.ISession session)
        {
            this.session = session;
        }

        public string AgentId
        {
            get
            {
                return agentId;
            }
            set
            {
                agentId = value;
            }
        }

        private Nortel.CCT.ITerminal selectedTerminal;

        public Nortel.CCT.ITerminal SelectedTerminal
        {
            get
            {
                return selectedTerminal;
            }
        }

        private Nortel.CCT.ITerminal SelectTerminal(string extNumber)
        {
            bool found = false;
            int i = 0;
            Nortel.CCT.ITerminal terminal = null;

            while (found == false && i < session.Terminals.Length)
            {
                terminal = session.Terminals[i];

                foreach (Nortel.CCT.IAddress address in terminal.RelatedAddresses)
                {
                    if (address.Name == extNumber)
                    {
                        found = true;
                    }
                }

                i++;
            }
            
            if (found == false)
                terminal = null;

            return terminal;
        }

        private string TermConnStateEventArgsToString(Nortel.CCT.TermConnStateEventArgs e)
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("Connection address name: " + e.Connection.Address.Name);
            sb.AppendLine("Connection contact calling address name: " + e.Connection.Contact.CallingAddress.Name);
            sb.AppendLine("State transition: " + e.PreviousState + " -> " + e.NewState);

            return sb.ToString();
        }
        
        private void NortelSession_TermConnStateChanged(Nortel.CCT.TermConnStateEventArgs e)
        {
            Console.WriteLine(TermConnStateEventArgsToString(e));

            if (e.NewState == Nortel.CCT.TerminalConnectionState.Ringing)
            {
                if (this.IncomingCall != null)
                {
                    IContact contact = new NortelContact(e.Contact);
                    CallRingingEventArgs args = new CallRingingEventArgs(contact.CallingAddress.Name, "", "", TimeSpan.Zero);
                    IncomingCall(this, args);
                }
            }
            if (e.NewState == Nortel.CCT.TerminalConnectionState.Dropped)
            {
                if (EndingCall != null)
                {
                    EndingCall(this, new CallReleasedEventArgs("", "", "", TimeSpan.Zero));
                }
            }
        }

        private void AddressStateHandler(Nortel.CCT.AddressStateEventArgs args)
        {
            this.AddressStateChanged(new NortelAddressStateEventArgs(args));
        }

        #region ISession Members

        public event AddressStateEventHandler AddressStateChanged;

        public event ContactScopeEventHandler ContactEnteringScope;

        public IAddress GetAddress(string addressName)
        {
            return new NortelAddress(this.session.GetAddress(addressName));
        }

        public ITerminal GetTerminal(string terminalName)
        {
            return new NortelTerminal(this.session.GetTerminal(terminalName));
        }

        public IAddress[] Addresses
        {
            get
            {
                Nortel.CCT.IAddress[] nativeAddress = this.session.Addresses;
                NortelAddress[] retVal = new NortelAddress[nativeAddress.Length];
                for (int i = 0; i < nativeAddress.Length; i++)
                {
                    retVal[i] = new NortelAddress(nativeAddress[i]);
                }
                return retVal;
            }
        }

        public IContact[] CurrentContacts
        {
            get
            {
                Nortel.CCT.IContact[] nativeContacts = this.session.CurrentContacts;
                NortelContact[] retVal = new NortelContact[nativeContacts.Length];
                for (int i = 0; i < nativeContacts.Length; i++)
                {
                    retVal[i] = new NortelContact(nativeContacts[i]);
                }
                return retVal;
            }
        }

        public Guid ID
        {
            get { return this.session.ID; }
        }

        public ITerminal[] Terminals
        {
            get
            {
                Nortel.CCT.ITerminal[] nativeTerminals = this.session.Terminals;
                ITerminal[] retVal = new ITerminal[nativeTerminals.Length];
                for (int i = 0; i < nativeTerminals.Length; i++)
                {
                    if (nativeTerminals[i] is Nortel.CCT.IAgentTerminal)
                        retVal[i] = new NortelAgentTerminal(nativeTerminals[i] as Nortel.CCT.IAgentTerminal);
                    else
                        retVal[i] = new NortelTerminal(nativeTerminals[i]);
                }
                return retVal;
            }
        }

        #endregion
    }

}
