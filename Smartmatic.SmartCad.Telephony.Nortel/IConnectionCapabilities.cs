using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony
{
    public interface IConnectionCapabilities
    {
        // Properties
        bool CanAccept { get; }
        bool CanDisconnect { get; }
        bool CanPark { get; }
        bool CanRedirect { get; }
        bool CanReject { get; }
    }
}
