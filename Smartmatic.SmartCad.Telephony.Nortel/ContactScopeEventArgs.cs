using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony
{
    [Serializable]
    public abstract class ContactScopeEventArgs : CCTEventArgs
    {
        // Methods
        // Properties
        public abstract IContact Contact { get; }
        public abstract bool EnteringScope { get; }
        public abstract bool LeavingScope { get; }
        public abstract ISession Session { get; }

        // Fields
        protected readonly IContact _contact;
        protected readonly bool _entering;
        protected readonly ISession _session;
    }
}
