using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony
{
    public abstract class ChannelDisconnectedEventArgs : EventArgs
    {
        // Methods
        // Properties
        public abstract Guid ClientID { get; }

        // Fields
        protected readonly Guid _clientID;
    }
}
