using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony
{
    [Serializable]
    public class AddressProperty : Property
    {
        // Methods
        // Fields
        public static readonly AddressProperty AddressTerminalAssociation;
        public static readonly AddressProperty DoNotDisturb;
        public static readonly AddressProperty Forwarding;
        public static readonly AddressProperty MessageWaiting;
        public static readonly AddressProperty Name;

        static AddressProperty()
        {
            AddressTerminalAssociation = new AddressProperty(0);
            DoNotDisturb = new AddressProperty(1);
            Forwarding = new AddressProperty(2);
            MessageWaiting = new AddressProperty(3);
            Name = new AddressProperty(4);
        }

        protected AddressProperty(int type)
            : base(type)
        { }

        // Nested Types
        protected enum Type
        {
            AddressTerminalAssociation,
            DoNotDisturb,
            Forwarding,
            MessageWaiting,
            Name
        }
    }
}
