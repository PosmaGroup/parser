using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony
{
    public interface ITerminalCapabilities
    {
        bool CanConference { get; }
        bool CanConsult { get; }
        bool CanDoNotDisturb { get; }
        bool CanForward { get; }
        bool CanOriginate { get; }
        bool CanPickup { get; }
        bool CanPickupFromGroup { get; }
        bool CanTransfer { get; }
    }
}
