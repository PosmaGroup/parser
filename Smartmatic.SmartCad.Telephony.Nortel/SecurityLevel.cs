using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony
{
    public enum SecurityLevel
    {
        None,
        Clear,
        Signed,
        Encrypted
    }
}
