using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony
{
    #region Class Program Documentation
    /// <summary>
    /// 
    /// </summary>
    /// <className>NortelTerminalCapabilities</className>
    /// <author email="atorres@smartmatic.com">Alden Torres</author>
    /// <date>2007/05/09</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    public interface IAgentTerminalCapabilities : ITerminalCapabilities
    {
        bool CanLogin
        {
            get;
        }

        bool CanLogout
        {
            get;
        }

        bool CanGetReadyStatus
        {
            get;
        }

        bool CanSetReadyStatus
        {
            get;
        }
    }
}
