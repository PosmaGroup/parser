using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony
{
    [Serializable]
    public abstract class TerminalPropertyEventArgs : CCTEventArgs
    {
        // Methods
        // Properties
        public abstract TerminalProperty ChangedProperty { get; }
        public abstract ITerminalCapabilities CurrentCapabilities { get; }
        public abstract ITerminal Sender { get; }

        // Fields
        protected readonly TerminalProperty evtProperty;
        protected readonly ITerminal evtSender;
    }
}
