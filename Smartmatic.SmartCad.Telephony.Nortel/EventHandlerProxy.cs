using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony
{
    public abstract class EventHandlerProxy : MarshalByRefObject
    {
        // Methods

        protected abstract void HandleServerEvent(CCTEventArgs args);
        public abstract void InvokeClientHandler(object args);

        // Fields
        protected EventDeliveryThread _eventThread;
    }
}
