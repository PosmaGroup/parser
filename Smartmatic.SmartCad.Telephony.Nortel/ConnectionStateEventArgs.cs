using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony
{
    [Serializable]
    public abstract class ConnectionStateEventArgs : CCTEventArgs
    {
        // Methods
        // Properties
        public abstract IAddress Address { get; }
        public abstract IConnection Connection { get; }
        public abstract IContact Contact { get; }
        public abstract bool IsRemote { get; }
        public abstract ConnectionState NewState { get; }
        public abstract ConnectionState PreviousState { get; }
        public abstract Reason Reason { get; }
        public abstract IAddress Sender { get; }

        // Fields
        protected readonly IAddress _address;
        protected readonly IConnection _connection;
        protected readonly IContact _contact;
        protected readonly ConnectionState _newState;
        protected readonly ConnectionState _prevState;
        protected readonly Reason _reason;
        protected readonly bool _remote;
        protected readonly IAddress _sender;
    }
}
