﻿using SmartCadControls.Controls;

namespace Smartmatic.SmartCad.Telephony
{
    partial class NortelConfigurationControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupControlStatistic = new DevExpress.XtraEditors.GroupControl();
            this.textBoxUserName = new TextBoxEx();
            this.textBoxPassword = new TextBoxEx();
            this.labelServer = new LabelEx();
            this.labelScript = new LabelEx();
            this.labelUserName = new LabelEx();
            this.textBoxExScript = new TextBoxEx();
            this.textBoxServer = new TextBoxEx();
            this.labelPassword = new LabelEx();
            this.groupControlTelephonyConnection = new DevExpress.XtraEditors.GroupControl();
            this.textBoxExSIPServerPort = new TextBoxEx();
            this.labelExSipServerIp = new LabelEx();
            this.labelExSipServerPort = new LabelEx();
            this.textBoxExSipServerIp = new TextBoxEx();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlStatistic)).BeginInit();
            this.groupControlStatistic.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlTelephonyConnection)).BeginInit();
            this.groupControlTelephonyConnection.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupControlStatistic
            // 
            this.groupControlStatistic.Controls.Add(this.textBoxUserName);
            this.groupControlStatistic.Controls.Add(this.textBoxPassword);
            this.groupControlStatistic.Controls.Add(this.labelServer);
            this.groupControlStatistic.Controls.Add(this.labelScript);
            this.groupControlStatistic.Controls.Add(this.labelUserName);
            this.groupControlStatistic.Controls.Add(this.textBoxExScript);
            this.groupControlStatistic.Controls.Add(this.textBoxServer);
            this.groupControlStatistic.Controls.Add(this.labelPassword);
            this.groupControlStatistic.Location = new System.Drawing.Point(3, 3);
            this.groupControlStatistic.Name = "groupControlStatistic";
            this.groupControlStatistic.Size = new System.Drawing.Size(375, 143);
            this.groupControlStatistic.TabIndex = 8;
            this.groupControlStatistic.Text = "groupControl1";
            // 
            // textBoxUserName
            // 
            this.textBoxUserName.AllowsLetters = true;
            this.textBoxUserName.AllowsNumbers = true;
            this.textBoxUserName.AllowsPunctuation = true;
            this.textBoxUserName.AllowsSeparators = true;
            this.textBoxUserName.AllowsSymbols = true;
            this.textBoxUserName.AllowsWhiteSpaces = true;
            this.textBoxUserName.ExtraAllowedChars = "";
            this.textBoxUserName.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxUserName.Location = new System.Drawing.Point(130, 53);
            this.textBoxUserName.MaxLength = 100;
            this.textBoxUserName.Name = "textBoxUserName";
            this.textBoxUserName.NonAllowedCharacters = "\'";
            this.textBoxUserName.RegularExpresion = "";
            this.textBoxUserName.Size = new System.Drawing.Size(240, 20);
            this.textBoxUserName.TabIndex = 3;
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.AllowsLetters = true;
            this.textBoxPassword.AllowsNumbers = true;
            this.textBoxPassword.AllowsPunctuation = true;
            this.textBoxPassword.AllowsSeparators = true;
            this.textBoxPassword.AllowsSymbols = true;
            this.textBoxPassword.AllowsWhiteSpaces = true;
            this.textBoxPassword.ExtraAllowedChars = "";
            this.textBoxPassword.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxPassword.Location = new System.Drawing.Point(130, 82);
            this.textBoxPassword.MaxLength = 63;
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.NonAllowedCharacters = "\'";
            this.textBoxPassword.PasswordChar = '*';
            this.textBoxPassword.RegularExpresion = "";
            this.textBoxPassword.Size = new System.Drawing.Size(240, 20);
            this.textBoxPassword.TabIndex = 5;
            // 
            // labelServer
            // 
            this.labelServer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelServer.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelServer.Location = new System.Drawing.Point(8, 28);
            this.labelServer.Name = "labelServer";
            this.labelServer.Size = new System.Drawing.Size(64, 16);
            this.labelServer.TabIndex = 0;
            this.labelServer.Text = "ServerIp";
            // 
            // labelScript
            // 
            this.labelScript.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelScript.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelScript.Location = new System.Drawing.Point(8, 116);
            this.labelScript.Name = "labelScript";
            this.labelScript.Size = new System.Drawing.Size(88, 16);
            this.labelScript.TabIndex = 6;
            this.labelScript.Text = "LabelScript";
            // 
            // labelUserName
            // 
            this.labelUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUserName.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelUserName.Location = new System.Drawing.Point(8, 56);
            this.labelUserName.Name = "labelUserName";
            this.labelUserName.Size = new System.Drawing.Size(88, 16);
            this.labelUserName.TabIndex = 2;
            this.labelUserName.Text = "LabelUser";
            // 
            // textBoxExScript
            // 
            this.textBoxExScript.AllowsLetters = false;
            this.textBoxExScript.AllowsNumbers = true;
            this.textBoxExScript.AllowsPunctuation = false;
            this.textBoxExScript.AllowsSeparators = false;
            this.textBoxExScript.AllowsSymbols = false;
            this.textBoxExScript.AllowsWhiteSpaces = false;
            this.textBoxExScript.ExtraAllowedChars = "";
            this.textBoxExScript.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExScript.Location = new System.Drawing.Point(130, 111);
            this.textBoxExScript.MaxLength = 2;
            this.textBoxExScript.Name = "textBoxExScript";
            this.textBoxExScript.NonAllowedCharacters = "\'";
            this.textBoxExScript.RegularExpresion = "";
            this.textBoxExScript.Size = new System.Drawing.Size(240, 20);
            this.textBoxExScript.TabIndex = 7;
            // 
            // textBoxServer
            // 
            this.textBoxServer.AllowsLetters = true;
            this.textBoxServer.AllowsNumbers = true;
            this.textBoxServer.AllowsPunctuation = true;
            this.textBoxServer.AllowsSeparators = false;
            this.textBoxServer.AllowsSymbols = false;
            this.textBoxServer.AllowsWhiteSpaces = false;
            this.textBoxServer.ExtraAllowedChars = "";
            this.textBoxServer.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxServer.Location = new System.Drawing.Point(130, 24);
            this.textBoxServer.MaxLength = 15;
            this.textBoxServer.Name = "textBoxServer";
            this.textBoxServer.NonAllowedCharacters = "\'";
            this.textBoxServer.RegularExpresion = "";
            this.textBoxServer.Size = new System.Drawing.Size(240, 20);
            this.textBoxServer.TabIndex = 1;
            // 
            // labelPassword
            // 
            this.labelPassword.AutoSize = true;
            this.labelPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPassword.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelPassword.Location = new System.Drawing.Point(8, 87);
            this.labelPassword.Name = "labelPassword";
            this.labelPassword.Size = new System.Drawing.Size(53, 13);
            this.labelPassword.TabIndex = 4;
            this.labelPassword.Text = "LabelPsw";
            // 
            // groupControlTelephonyConnection
            // 
            this.groupControlTelephonyConnection.Controls.Add(this.textBoxExSIPServerPort);
            this.groupControlTelephonyConnection.Controls.Add(this.labelExSipServerIp);
            this.groupControlTelephonyConnection.Controls.Add(this.labelExSipServerPort);
            this.groupControlTelephonyConnection.Controls.Add(this.textBoxExSipServerIp);
            this.groupControlTelephonyConnection.Location = new System.Drawing.Point(3, 152);
            this.groupControlTelephonyConnection.Name = "groupControlTelephonyConnection";
            this.groupControlTelephonyConnection.Size = new System.Drawing.Size(375, 81);
            this.groupControlTelephonyConnection.TabIndex = 9;
            this.groupControlTelephonyConnection.Text = "groupControl1";
            // 
            // textBoxExSIPServerPort
            // 
            this.textBoxExSIPServerPort.AllowsLetters = false;
            this.textBoxExSIPServerPort.AllowsNumbers = true;
            this.textBoxExSIPServerPort.AllowsPunctuation = true;
            this.textBoxExSIPServerPort.AllowsSeparators = true;
            this.textBoxExSIPServerPort.AllowsSymbols = true;
            this.textBoxExSIPServerPort.AllowsWhiteSpaces = true;
            this.textBoxExSIPServerPort.ExtraAllowedChars = "";
            this.textBoxExSIPServerPort.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExSIPServerPort.Location = new System.Drawing.Point(130, 52);
            this.textBoxExSIPServerPort.MaxLength = 100;
            this.textBoxExSIPServerPort.Name = "textBoxExSIPServerPort";
            this.textBoxExSIPServerPort.NonAllowedCharacters = "\'";
            this.textBoxExSIPServerPort.RegularExpresion = "";
            this.textBoxExSIPServerPort.Size = new System.Drawing.Size(240, 20);
            this.textBoxExSIPServerPort.TabIndex = 3;
            // 
            // labelExSipServerIp
            // 
            this.labelExSipServerIp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExSipServerIp.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExSipServerIp.Location = new System.Drawing.Point(8, 28);
            this.labelExSipServerIp.Name = "labelExSipServerIp";
            this.labelExSipServerIp.Size = new System.Drawing.Size(64, 16);
            this.labelExSipServerIp.TabIndex = 0;
            this.labelExSipServerIp.Text = "SIPServerIp";
            // 
            // labelExSipServerPort
            // 
            this.labelExSipServerPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExSipServerPort.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExSipServerPort.Location = new System.Drawing.Point(8, 56);
            this.labelExSipServerPort.Name = "labelExSipServerPort";
            this.labelExSipServerPort.Size = new System.Drawing.Size(88, 16);
            this.labelExSipServerPort.TabIndex = 2;
            this.labelExSipServerPort.Text = "SIPServerPort";
            // 
            // textBoxExSipServerIp
            // 
            this.textBoxExSipServerIp.AllowsLetters = true;
            this.textBoxExSipServerIp.AllowsNumbers = true;
            this.textBoxExSipServerIp.AllowsPunctuation = true;
            this.textBoxExSipServerIp.AllowsSeparators = false;
            this.textBoxExSipServerIp.AllowsSymbols = false;
            this.textBoxExSipServerIp.AllowsWhiteSpaces = false;
            this.textBoxExSipServerIp.ExtraAllowedChars = "";
            this.textBoxExSipServerIp.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExSipServerIp.Location = new System.Drawing.Point(130, 24);
            this.textBoxExSipServerIp.MaxLength = 15;
            this.textBoxExSipServerIp.Name = "textBoxExSipServerIp";
            this.textBoxExSipServerIp.NonAllowedCharacters = "\'";
            this.textBoxExSipServerIp.RegularExpresion = "";
            this.textBoxExSipServerIp.Size = new System.Drawing.Size(240, 20);
            this.textBoxExSipServerIp.TabIndex = 1;
            // 
            // NortelConfigurationControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupControlTelephonyConnection);
            this.Controls.Add(this.groupControlStatistic);
            this.Name = "NortelConfigurationControl";
            this.Size = new System.Drawing.Size(381, 236);
            ((System.ComponentModel.ISupportInitialize)(this.groupControlStatistic)).EndInit();
            this.groupControlStatistic.ResumeLayout(false);
            this.groupControlStatistic.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlTelephonyConnection)).EndInit();
            this.groupControlTelephonyConnection.ResumeLayout(false);
            this.groupControlTelephonyConnection.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControlStatistic;
        private TextBoxEx textBoxUserName;
        private TextBoxEx textBoxPassword;
        private LabelEx labelServer;
        private LabelEx labelScript;
        private LabelEx labelUserName;
        private TextBoxEx textBoxExScript;
        private TextBoxEx textBoxServer;
        private LabelEx labelPassword;
        private DevExpress.XtraEditors.GroupControl groupControlTelephonyConnection;
        private TextBoxEx textBoxExSIPServerPort;
        private LabelEx labelExSipServerIp;
        private LabelEx labelExSipServerPort;
        private TextBoxEx textBoxExSipServerIp;
    }
}
