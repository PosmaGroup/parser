using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Telephony
{
    [Serializable]
    public class Property
    {
        // Methods
        // Fields
        protected readonly int propType;

        protected Property(int type)
        {
            this.propType = type;
        }

        public override bool Equals(object obj)
        {
            if (System.Object.ReferenceEquals(this, obj))
            {
                return true;
            }
            if (System.Object.Equals(this, null) || System.Object.Equals(obj, null))
            {
                return false;
            }
            if (obj is Property)
            {
                return (this.propType == ((Property)obj).propType);
            }
            return false;
        }

        public static bool operator ==(Property p1, Property p2)
        {
            if (System.Object.ReferenceEquals(p1, p2))
            {
                return true;
            }

            if (System.Object.Equals(p1, null))
            {
                return false;
            }

            return p1.Equals(p2);
        }

        public static bool operator !=(Property p1, Property p2)
        {
            return !(p1 == p2);
        }
    }
}
