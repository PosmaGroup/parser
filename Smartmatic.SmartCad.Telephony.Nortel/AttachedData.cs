using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
namespace Smartmatic.SmartCad.Telephony
{
    [Serializable]
    public abstract class AttachedData
    {
        // Methods
        // Properties
        public abstract byte[] BinaryData { get; set; }
        public abstract AttachedDataFormat Format { get; set; }
        public abstract string this[string key] { get; set; }
        public abstract IDictionaryEnumerator KeyValuePairs { get; }
        public abstract string StringData { get; set; }
        public abstract string XMLData { get; }

        // Fields
        protected byte[] _bindata;
        protected AttachedDataFormat _format;
        protected Hashtable _kvpdata;
        protected string _strdata;
    }

}
