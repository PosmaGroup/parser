﻿using System;

namespace SmartCadGuiCommon
{
    /// <summary>
    /// Interface to expose properties related to the loading of resources associated to
    /// windows form
    /// </summary>
    /// <remarks>Author: Cornelio R Carrillo, March 2018</remarks>

    public interface IIntegration
    {
        
        public string Camara
        {
            get
            {
                return "";
            }
        }

        /// <summary>
        /// When LoadFromResources is true, this property is used to load text and images resources from Smartmatic.Usp.Resources
        /// </summary>		
        
        public string CargarData();
    }
}
