using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;
using Smartmatic.SmartCad.Service;
using SmartCadCore.Core;
using SmartCadCore.Model;
using System.ServiceModel;
using System.Collections;
using SmartCadCore.ClientData;
using System.Net.Sockets;
using SmartCadCore.Common;
using System.Globalization;
using SmartCadGuiCommon;
using SmartCadGuiCommon.Util;
using System.IO;

namespace SmartCadAdministration
{     
    static class Program
    {
        private static string ApplicationName = "Administration";

        static System.Threading.Timer checkServer;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
#if ! DEBUG
                DevExpress.Data.CurrencyDataController.DisableThreadingProblemsDetection = true; 
#endif
                Application.ThreadException += new ThreadExceptionEventHandler(ApplicationGuiUtil.Application_ThreadException);
                ApplicationUtil.ActivateProcess(Process.GetCurrentProcess());

                SmartCadConfiguration.Load();
                
                ResourceLoader.UpdatedLanguage(new CultureInfo(SmartCadConfiguration.SmartCadSection.CultureElement.CultureLanguageElement.Name
                    + "-" + SmartCadConfiguration.SmartCadSection.CultureElement.CultureCountryElement.Name));

                object result = ServerServiceClient.GetServerService();

                if (result == null)
                {
                    MessageForm.Show(ResourceLoader.GetString2("UnableConnectToServer"), MessageFormType.Error);
                    return;
                }

                ConfigurationClientData cfg = ServerServiceClient.GetInstance().GetConfiguration();
                SmartCadConfiguration.SaveConfigurationInClient(cfg);
                if (!ApplicationUtil.VerifyLanguage(cfg))
                {
                    byte[] dll= ServerServiceClient.GetInstance().CheckLanguages(cfg.Language);
                    DirectoryInfo directory = Directory.CreateDirectory(SmartCadConfiguration.LanguageDirectoryClient + "\\" + cfg.Language);
                    using (FileStream fileDll = new FileStream(directory.FullName + "\\Smartmatic.SmartCad.Resources.resources.dll", FileMode.Create, FileAccess.Write))
                    {
                        fileDll.Write(dll, 0, dll.Length);
                    }
                }

                ResourceLoader.UpdatedLanguage(new CultureInfo(cfg.Language));
                ServerServiceClient.GetInstance().SetApplication(UserApplicationClientData.UserApplications.Administration.ToString());
                SmartCadLoadInitialData.LoadInitialClientData();

                System.Threading.Timer checkServer = ApplicationGuiUtil.StartCheckServer();

                ServerServiceClient.GetServerService().ActivateApplication(ApplicationName, ApplicationUtil.GetMACAddress());
                ApplicationGuiUtil.CheckErrorDetailButton();

                bool success = false;

                do
                {
                    LoginForm login = new LoginForm(UserApplicationClientData.Administration);

                    if (login.ShowDialog() == DialogResult.OK)
                    {
                        SplashForm.ShowSplash();
                        
                        ServerServiceClient serverServiceClient = login.ServerService;

                        success = true;

                        AdministrationRibbonForm administrationForm = new AdministrationRibbonForm();
                        administrationForm.UserPassword = login.Password;
                        administrationForm.ServerServiceClient = serverServiceClient;
                        AdministrationRibbonForm.Password = login.Password;
                        SplashForm.CloseSplash();
                        Application.Run(administrationForm);
                    }
                    else
                    {
                        ServerServiceClient.GetInstance().CloseSession();
                        success = true;
                    }
                }
                while (!success);

            }
            catch (EndpointNotFoundException ex)
            {
                SplashForm.CloseSplash();

                MessageForm.Show(ResourceLoader.GetString2("UnableConnectToServer"), ex);
            }
            catch (Exception ex)
            {
                SplashForm.CloseSplash();

                MessageForm.Show(ResourceLoader.GetString2("GenericErrorApplication"), ex);
            }
        }

    }
}
