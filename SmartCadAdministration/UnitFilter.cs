using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Data;
using SmartCadCore.Core;
using SmartCadCore.ClientData;
using SmartCadCore.Enums;
using Smartmatic.SmartCad.Service;


namespace SmartCadControls.Filters
{
    public class UnitFilter : FilterBase
    {
        private Dictionary<UnitButtonFilter, IList<string>> mappedStatus;

        private IncidentNotificationClientData selectedIncidentNotification;

        public UnitFilter(DataGridEx dataGrid, IList parameters)
            : base(dataGrid, parameters)
        {
            mappedStatus = new Dictionary<UnitButtonFilter, IList<string>>();

            IList<string> availableStatusList = new List<string>();
            availableStatusList.Add(UnitStatusClientData.Available.CustomCode);
            mappedStatus.Add(UnitButtonFilter.AVAILABLE, availableStatusList);

            IList<string> assignedStatusList = new List<string>();
            assignedStatusList.Add(UnitStatusClientData.Assigned.CustomCode);
            assignedStatusList.Add(UnitStatusClientData.OnScene.CustomCode);
            assignedStatusList.Add(UnitStatusClientData.Delayed.CustomCode);
            mappedStatus.Add(UnitButtonFilter.ASSIGNED, assignedStatusList);

            IList<string> attentionStatusList = new List<string>();
            attentionStatusList.Add(UnitStatusClientData.Delayed.CustomCode);
            mappedStatus.Add(UnitButtonFilter.ATTENTION, attentionStatusList);
        }

        public override bool Filter(object obj)
        {
            bool visible = false;

            OperatorClientData oper = null;

            if (parameters.Count > 0)
                oper = parameters[parameters.Count - 1] as OperatorClientData;

            if (oper != null)
            {
                UnitClientData unit = obj as UnitClientData;

                if (oper.DepartmentTypes.Contains(unit.DepartmentStation.DepartmentZone.DepartmentType) &&
                    (unit.DispatchOrder == null 
                        || unit.DispatchOrder.DispatchOperatorCode == ServerServiceClient.GetInstance().OperatorClient.Code))
                {
                    UnitButtonFilter unitButtonFilter = (UnitButtonFilter)parameters[0];
                    if (unitButtonFilter.Equals(UnitButtonFilter.ALL) == false)
                    {
                        if (mappedStatus[unitButtonFilter].Contains(unit.Status.CustomCode))
                        {
                            visible = true;
                        }
                    }
                    else
                    {
                        visible = true;
                    }

                    if (visible == true)
                    {
                        UnitRadioButtonFilter unitRadioButtonFilter = (UnitRadioButtonFilter)parameters[1];
                        if (unitRadioButtonFilter == UnitRadioButtonFilter.ALL)
                        {
                            visible = true;
                        }
                        else if (unitRadioButtonFilter == UnitRadioButtonFilter.ZONE)
                        {
                            if (SelectedIncidentNotification != null &&
                                SelectedIncidentNotification.DepartmentStation.DepartmentZone.Code == unit.DepartmentStation.DepartmentZone.Code)
                            {
                                visible = true;
                            }
                            else
                            {
                                visible = false;
                            }
                        }
                        else if (unitRadioButtonFilter == UnitRadioButtonFilter.STATION)
                        {
                            if (SelectedIncidentNotification != null &&
                                SelectedIncidentNotification.DepartmentStation.Code == unit.DepartmentStation.Code)
                            {
                                visible = true;
                            }
                            else
                            {
                                visible = false;
                            }
                        }
                        else
                        {
                            visible = false;
                        }
                    }
                }
            }

            return visible;
        }

        public bool VisibleAfterFilter(IList<UnitClientData> units, UnitButtonFilter unitFilter)
        {
            bool result = true;//Esta variable esta iniciada en true porque antes de llamar a este metodo se asegura que la lista de unidades tenga al menos un elemento
            if (unitFilter != UnitButtonFilter.ALL && unitFilter != UnitButtonFilter.TEXT)
            {
                for (int i = 0; i < units.Count && result == true; i++)
                {
                    UnitClientData unit = units[i];
                    if (mappedStatus[unitFilter].Contains(unit.Status.CustomCode) == false)
                    {
                        result = false;
                    }
                }
            }
            return result;
        }

        public override bool FilterByText(object obj, string text)
        {
            bool result = false;

            UnitClientData unit = obj as UnitClientData;
            string newTextToSearch = ApplicationUtil.GetStringWithoutAccent(text).ToLower();
            string newUnitTypeName = ApplicationUtil.GetStringWithoutAccent(unit.Type.Name).ToLower();
            string newCustomCode = ApplicationUtil.GetStringWithoutAccent(unit.CustomCode).ToLower();
            string newIdRadio = ApplicationUtil.GetStringWithoutAccent(unit.IdRadio).ToLower();

            if ((newUnitTypeName.Contains(newTextToSearch)) ||
                (newCustomCode.Contains(newTextToSearch)) ||
                (newIdRadio.Contains(newTextToSearch)))
            {
                result = true;
            }

            return result;
        }

        public IncidentNotificationClientData SelectedIncidentNotification
        {
            get
            {
                return selectedIncidentNotification;
            }
            set
            {
                selectedIncidentNotification = value;
            }
        }
    }
}
