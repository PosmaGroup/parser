using System;
using System.Collections.Generic;
using System.Text;
using System.ServiceModel;
using System.Collections;
using System.IO;
using System.Net.Security;

namespace Smartmatic.SmartCad.Service
{
    [ServiceContract(ProtectionLevel = ProtectionLevel.None)]
    public interface IFileServerService
    {
        [OperationContract]
        Stream GetFile(string fileName);

        [OperationContract]
        void SendFile(FileSend data);
    }

    [MessageContract]
    public class FileSend
    {
        [MessageHeader(MustUnderstand = true)]
        public string Filename { get; set; }
        [MessageHeader(MustUnderstand = true)]
        public int Length { get; set; }
        [MessageBodyMember(Order = 1)]
        public Stream Data { get; set; }
    }
}
