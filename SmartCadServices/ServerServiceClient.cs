using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Security;
using System.Security.Cryptography.X509Certificates;
using System.IdentityModel.Selectors;
using System.IdentityModel.Policy;
using System.Threading;
using System.Reflection;
using NHibernate.Collection;
using System.ComponentModel;
using System.Diagnostics;
using System.Net.Sockets;
using System.Net;
using SmartCadCore.ClientData;
using SmartCadCore.Core;
using SmartCadCore.Common;
using SmartCadCore.Model;

namespace Smartmatic.SmartCad.Service
{
    public class ServerServiceClient : IServerService
    {
        #region Fields
        private const int MAX_ROWS_PAGING = 500;
        private static ServerServiceClient serverServiceClient;
        private SessionHistoryClientData sessionHistoryClientData;
        private OperatorClientData operatorClient;
        private UserApplicationClientData application;
        private TimeSpan serverTimedifference;

        private string applicationName;
        private ApplicationPreferenceClientData maxRowPaging = new ApplicationPreferenceClientData() { Value = MAX_ROWS_PAGING.ToString() };
        private static SessionState sessionState = new SessionState();
        private static SessionStateClientBehavior sessionStateClientBehaviour = new SessionStateClientBehavior();
        private ApplicationServerService clientServiceClient = new ApplicationServerService();

        private static ChannelFactory<IServerService> factory = null;
        #endregion

        #region Enums
        public enum Device
        {
            GPS,
            LPR
        }
        #endregion

        #region Constructors

        private ServerServiceClient()
        {
        }


        #endregion

        #region Properties

        public OperatorClientData OperatorClient
        {
            get
            {
                return operatorClient;
            }
        }

        public UserApplicationClientData Application
        {
            get
            {
                return application;
            }
        }

        public bool IsLoggedIn
        {
            get
            {
                return OperatorClient != null;
            }
        }

        public ConfigurationClientData Configuration { get; set; }
        public NetworkCredential NetworkCredential { get; set; }
        
        #endregion

        #region Members

        public static ServerServiceClient GetInstance()
        {
            if (serverServiceClient == null)
                throw new Exception(ResourceLoader.GetString2("ClientInstanceNotCreated"));

            return serverServiceClient;
        }

        public static ServerServiceClient GetServerService()
        {
            try
            {
                if (string.IsNullOrEmpty(SmartCadConfiguration.SmartCadSection.ClientElement.ServerAddressElement.IP) == true)
                    return null;

                string url = ApplicationUtil.BuildServiceUrl(
                    "net.tcp",
                    SmartCadConfiguration.SmartCadSection.ClientElement.ServerAddressElement.IP,
                    SmartCadConfiguration.SERVER_SERVICE_PORT,
                    SmartCadConfiguration.SERVER_SERVICE_ADDRESS);

                EndpointAddress address = new EndpointAddress(url);

                CustomBinding binding = BindingBuilder.GetClientNetTcpBinding();

                factory = new ChannelFactory<IServerService>(binding, address);

                factory.Endpoint.Behaviors.Add(new SessionStateClientBehavior());

                foreach (OperationDescription op in factory.Endpoint.Contract.Operations)
                {
                    DataContractSerializerOperationBehavior dataContractBehavior =
                       op.Behaviors.Remove<DataContractSerializerOperationBehavior>()
                            as DataContractSerializerOperationBehavior;

                    op.Behaviors.Add(new NetDataContractSerializerOperationBehavior(op));
                }

                factory.Open();

                serverServiceClient = new ServerServiceClient();

                sessionState.Computer = Environment.MachineName;

                ApplicationPreferenceClientData preference = serverServiceClient.SearchClientObject("select data from ApplicationPreferenceData data where data.Name = 'SearchClientObjectsMaxRows'") as ApplicationPreferenceClientData;
                if (preference != null)
                    serverServiceClient.maxRowPaging = preference;

                return serverServiceClient;
            }
            catch (Exception ex) 
            {
                return null;
            }
        }

        public SessionHistoryClientData CurrentSession
        {
            get
            {
                if (sessionHistoryClientData == null)
                {
                    sessionHistoryClientData = GetLastSession(Application.Name);
                }
                return sessionHistoryClientData;
            }
        }

        public SessionHistoryClientData GetLastSession(string applicationName)
        {
            SessionHistoryClientData session = null;
            bool found = false;
            IList sessions = OperatorClient.LastSessions;
            for (int i = 0; i < sessions.Count && found == false; i++)
            {
                SessionHistoryClientData temp = (SessionHistoryClientData)sessions[i];
                if (temp.UserApplication == applicationName && temp.IsLoggedIn == true)
                {
                    session = temp;
                    found = true;
                }
            }
            return session;
        }

        public void SetApplication(string application)
        {
            applicationName = application;
            sessionState.Application = application;
        }

        public void SetApplication(UserApplicationClientData application)
        {
            this.application = application;
            sessionState.Application = application.Name;
        }
        #endregion

        #region IServerService Members

        #region Session
        public OperatorClientData OpenSession(string login, string password, string extNumber, string computerName, UserApplicationClientData application)
        {
            this.operatorClient = CallChannel<OperatorClientData>("OpenSession", login, password, extNumber, computerName, application);

            clientServiceClient.OpenHost(application.Name);

            sessionState.User = new OperatorSessionState();
            sessionState.User.Code = operatorClient.Code;
            sessionState.User.Login = operatorClient.Login;
            sessionState.User.Role = operatorClient.RoleCode;

            this.application = application;

            serverTimedifference = GetTimeFromDB().Subtract(DateTime.Now);

            this.operatorClient = ServerServiceClient.GetInstance().SearchClientObject(
                SmartCadHqls.GetCustomHql(SmartCadHqls.GetOperatorByCode, operatorClient.Code)) as OperatorClientData;

            return this.operatorClient;
        }

        public void CloseSession()
        {
            CloseSession(true);
        }

        public void CloseSession(bool closeServer)
        {
            try
            {
                if (IsLoggedIn == true)
                {
                    if (closeServer == true)
                        CallChannel<object>("CloseSession");
                    clientServiceClient.CloseHost();
                }
                factory.Close();
            }
            catch (ProtocolException ex)
            {
                SmartLogger.Print(ex);
            }
        }
        #endregion

        #region SaveOrUpdate
        public void SaveOrUpdateClientData(ClientData obj)
        {
            CallChannel<object>("SaveOrUpdateClientData", obj, false);
        }

        public object SaveOrUpdateClientDataWithReturn(ClientData obj)
        {
            return CallChannel<object>("SaveOrUpdateClientData", obj, true);
        }

        /// <summary>
        /// This method is not intended to be used or called directly from client app.
        /// Instead you must use SaveOrUpdateClientData or SaveOrUpdateClientDataWithReturn.
        /// It is only public because it is an implementation of a method from an interface
        /// </summary>
        /// <param name="obj">Object to save or uodate</param>
        /// <param name="withReturn">Indicates if a value is expected</param>
        /// <returns>Persisted object or null if nothing is expected</returns>
        public object SaveOrUpdateClientData(ClientData obj, bool withReturn)
        {
            return CallChannel<object>("SaveOrUpdateClientData", obj, withReturn);
        }

        public void SaveOrUpdateClientData(IList list)
        {
            CallChannel<object>("SaveOrUpdateClientData", list);
        }
        #endregion

        public event EventHandler<CommittedObjectDataCollectionEventArgs> CommittedChanges
        {
            add
            {
                ApplicationServerService.CommittedChanges += value;
            }
            remove
            {
                ApplicationServerService.CommittedChanges -= value;
            }
        }

        public bool CheckVirtualCall()
        {
            return CallChannel<bool>("CheckVirtualCall");
        }

        //public event EventHandler<GisActionEventArgs> GisAction
        //{
        //    add
        //    {
        //        ClientServiceClient.GisAction += value;
        //    }
        //    remove
        //    {
        //        ClientServiceClient.GisAction -= value;
        //    }
        //}

        #region Languages
        public byte[] CheckLanguages(string language)
        {
            return CallChannel<byte[]>("CheckLanguages", language);
        }
        #endregion

        public void SetSupervisedApplicationName(string supervisedAppName)
        {
            CallChannel<object>("SetSupervisedApplicationName", supervisedAppName);
        }

        public bool CheckAccessClient(UserResourceClientData userResource,
            UserActionClientData userAction,
            UserApplicationClientData userApplication, bool throwException)
        {
            return CallChannel<bool>("CheckAccessClient", userResource, userAction, userApplication, throwException);
        }

        public void DeleteClientObject(ClientData clientData)
        {
            CallChannel<object>("DeleteClientObject", clientData);
        }

        public void DeleteAlarmObject(ClientData clientData)
        {
            CallChannel<object>("DeleteAlarmObject", clientData);
        }

        public void DeleteClientObjectCollection(IList list)
        {
            CallChannel<object>("DeleteClientObjectCollection", list);
        }

        public void FinalizeCall(PhoneReportClientData phoneReport)
        {
            CallChannel<object>("FinalizeCall", phoneReport);
        }


        

        public void FinalizeCctvIncident(CctvReportClientData cctvReport)
        {
            CallChannel<object>("FinalizeCctvIncident", cctvReport);
        }

        public void FinalizeAlarmIncident(AlarmReportClientData alarmReport)
        {
            CallChannel<object>("FinalizeAlarmIncident", alarmReport);
        }
        public void FinalizeAlarmLprIncident(AlarmLprReportClientData alarmReport)
        {
            CallChannel<object>("FinalizeAlarmLprIncident", alarmReport);
        }
        public void FinalizeTwitterIncident(TwitterReportClientData alarmReport)
        {
            CallChannel<object>("FinalizeTwitterIncident", alarmReport);
        }

       

        ICollection IServerService.InitializeLazy(ObjectData objectData, string property)
        {
            return CallChannel<ICollection>("InitializeLazy", objectData, property);
        }

        public void InitializeLazy(ObjectData objectData, object objectList)
        {
            if (((objectList != null) &&
                (objectList is PersistentBag) &&
                (((PersistentBag)objectList).WasInitialized == false)) ||
                ((objectList is PersistentList) &&
                (((PersistentList)objectList).WasInitialized == false)) ||
                ((objectList is PersistentSet) &&
                (((PersistentSet)objectList).WasInitialized == false)))
            {
                string propertyName = ClassUtil.GetPropertyName(objectData, objectList);
                ICollection list = (this as IServerService).InitializeLazy(objectData, propertyName);
                ClassUtil.SetValue(objectData, propertyName, list);
            }
        }

        public DateTime GetTime()
        {
            return DateTime.Now.Add(serverTimedifference);
        }

        public DateTime GetTimeFromDB()
        {
            return CallChannel<DateTime>("GetTime");
        }

        public IList RunTasks(IList tasks)
        {
            return CallChannel<IList>("RunTasks", tasks);
        }

        public void SetOperatorStatus(OperatorStatusClientData status)
        {
            CallChannel<object>("SetOperatorStatus", status);
        }

        public void AssignIncidentNotificationToManualSupervisor(IncidentNotificationClientData notification)
        {
            CallChannel<object>("AssignIncidentNotificationToManualSupervisor", notification);
        }

        public void RequestReassignIncidentNotifications()
        {
            CallChannel<object>("RequestReassignIncidentNotifications");
        }

        public void AskUpdatedObjectToServer(ClientData clientObjectData, EventHandler<CommittedObjectDataCollectionEventArgs> eventHandler)
        {
            if (clientObjectData != null && clientObjectData.Code > 0)
            {
                try
                {
                    ClientData objectData;
                    ConstructorInfo constructor = clientObjectData.GetType().GetConstructor(Type.EmptyTypes);
                    objectData = constructor.Invoke(null) as ClientData;
                    objectData.Code = clientObjectData.Code;
                    objectData = ServerServiceClient.GetInstance().RefreshClient(objectData, true);
                    CommittedObjectDataCollectionEventArgs args = null;
                    if (objectData != null)
                    {
                        args = new CommittedObjectDataCollectionEventArgs(
                            objectData, CommittedDataAction.Update, null);
                    }
                    else
                    {
                        args = new CommittedObjectDataCollectionEventArgs(
                            objectData, CommittedDataAction.Delete, null);
                    }
                    if (clientServiceClient != null)
                        clientServiceClient.OnCommittedChanges(null, args);
                }
                catch (Exception ex)
                {
                    SmartLogger.Print(ex);
                }
            }
        }

        public bool UserHasApplicationMapOpen()
        {
            bool result = false;
            try
            {
                IList list = (IList)SearchBasicObjects(
                                SmartCadHqls.GetCustomHql(SmartCadHqls.GetUserSessionCountByApplication,
                                    "Map", OperatorClient.Login));
                if (list.Count > 0)
                {
                    long sessionCount = (long)list[0];
                    if (sessionCount > 0)
                    {
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }

            return result;
        }

        public void Ping()
        {
            CallChannel<object>("Ping");
        }

        public ConfigurationClientData GetConfiguration()
        {
            ConfigurationClientData configuration = CallChannel<ConfigurationClientData>("GetConfiguration");
            Configuration = configuration;
            return configuration;
        }

        public bool SetGPSOutputs(string idGPS, IList<int> outputs, bool state)
        {
            return CallChannel<bool>("SetGPSOutputs", idGPS, outputs, state);
        }

        public bool SetGPSOutputs(string idGPS, IList<int> outputs, bool state, int seconds, int times)
        {
            return CallChannel<bool>("SetGPSOutputs", idGPS, outputs, state, seconds, times);
        }

        public bool DevicePing(Device deviceToPing)
        {
            return CallChannel<bool>("DevicePing", deviceToPing);
        }

        public IList SearchClientObjects(Type clientDataType)
        {
            
            return CallChannel<IList>("SearchClientObjects", clientDataType);
        }

        public IList SearchClientObjects(string hql)
        {
            
            return CallChannel<IList>("SearchClientObjects", hql);
        }

        public IList SearchClientObjects(string hql, bool InitializeCollections)
        {
            
            return CallChannel<IList>("SearchClientObjects", hql, InitializeCollections);
        }

        public IList SearchClientObjectsMaxRows(string HQL, int startIndex, int maxRow)
        {
            
            return CallChannel<IList>("SearchClientObjectsMaxRows", HQL, startIndex, int.Parse(maxRowPaging.Value));
        }

        public IList SearchClientObjectsMaxRows(string hql, bool sleep)
        {
            ArrayList completeResults = new ArrayList();
            int rows = 0;
            int index = 0;
            do
            {
                IList results = SearchClientObjectsMaxRows(hql, index, int.Parse(maxRowPaging.Value));
                if (sleep == true)
                {
                    Thread.Sleep(100);
                }
                rows = results.Count;
                completeResults.AddRange(results);
                index += int.Parse(maxRowPaging.Value);
            }
            while (rows == int.Parse(maxRowPaging.Value));
            return completeResults;
        }

        

		public Dictionary<string, IList> SearchClientObjectsMaxRowsMultiQuery(int startIndex, int maxRow, Dictionary<string, IList> querys)
		{
			return CallChannel<Dictionary<string, IList>>("SearchClientObjectsMaxRowsMultiQuery", startIndex, maxRow, querys);
		}

		public Dictionary<string, IList> SearchClientObjectsMaxRowsMultiQuery(bool sleep, Dictionary<string, IList> querys)
		{
			Dictionary<string, IList> completeResults = new Dictionary<string, IList>();
			int index = 0;
			int rows = 0;
			do
			{
				rows = 0;
				Dictionary<string, IList> results = SearchClientObjectsMaxRowsMultiQuery(index, int.Parse(maxRowPaging.Value) / querys.Count, querys);
				if (sleep == true)
				{
					Thread.Sleep(20);
				}
				foreach (string key in results.Keys)
				{
					rows = rows < results[key].Count ? results[key].Count : rows;
					if (completeResults.ContainsKey(key))
						((ArrayList)completeResults[key]).AddRange(results[key]);
					else
						completeResults.Add(key, new ArrayList(results[key]));
				}
				index += int.Parse(maxRowPaging.Value) / querys.Count;
			}
			while (rows == int.Parse(maxRowPaging.Value) / querys.Count);
			return completeResults;
		}

        public ClientData SearchClientObject(string hql)
        {
            
            return CallChannel<ClientData>("SearchClientObject", hql);
        }

        public ClientData SearchClientObjectByBytes(string hql, int offset, int length)
        {
            return CallChannel<ClientData>("SearchClientObjectByBytes", hql, offset, length);
        }

        public ClientData SearchClientObject(ClientData obj)
        {
            
            return CallChannel<ClientData>("SearchClientObject", obj);
        }

        public void SendClientData(ClientData objectClient)
        {
            CallChannel<object>("SendClientData", objectClient);
        }

        public void SendClientDataList(IList list)
        {
            CallChannel<object>("SendClientDataList", list);
        }

		public List<ClientData> GetHistory(DateTime startDate, DateTime endDate, string deviceType, string deviceID, int min, int max)
        {
			return CallChannel<List<ClientData>>("GetHistory", startDate,endDate, deviceType, deviceID, min, max);
        }

        public void UpdatePhoneReportClient(PhoneReportClientData phoneReportClient)
        {
            CallChannel<object>("UpdatePhoneReportClient", phoneReportClient);
        }

        public void UpdateCctvReportClient(CctvReportClientData cctvReportClient)
        {
            CallChannel<object>("UpdateCctvReportClient", cctvReportClient);
        }

        public void UpdateAlarmReportClient(AlarmReportClientData alarmReportClient)
        {
            CallChannel<object>("UpdateAlarmReportClient", alarmReportClient);
        }

        public void UpdateAlarmLprReportClient(AlarmLprReportClientData alarmReportClient)
        {
            CallChannel<object>("UpdateAlarmLprReportClient", alarmReportClient);
        }

        private void PrintStackTrace(int numberOfMethods) 
        {
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(System.Diagnostics.StackTrace.METHODS_TO_SKIP + 1);

            if (numberOfMethods > trace.GetFrames().Length)
                numberOfMethods = trace.GetFrames().Length - 1;


            Console.WriteLine(" ");
            Console.WriteLine("***********************************************************************");
            for (int i = 0; i < numberOfMethods; i++)
            {
                System.Diagnostics.StackFrame frame = trace.GetFrames()[i];

                System.Reflection.MethodBase caller = frame.GetMethod();
                if (caller != null)
                    Console.WriteLine(i + " " + caller.Name);
            }
            Console.WriteLine("***********************************************************************");
            Console.WriteLine(" ");
            
        }

        public object SearchBasicObject(string hql)
        {
            
            return CallChannel<object>("SearchBasicObject", hql);
        }

        public object SearchBasicObjects(string hql)
        {
            
            return CallChannel<object>("SearchBasicObjects", hql);
        }

        public ClientData RefreshClient(ClientData clientData)
        {
            return CallChannel<ClientData>("RefreshClient", clientData);
        }

        public ClientData RefreshClient(ClientData clientData, bool initializeCollections)
        {
            return CallChannel<ClientData>("RefreshClient", clientData, initializeCollections);
        }

        public bool AssignIncidentNotificationToOperator(IncidentNotificationClientData selectedNotification, OperatorClientData selectedOperator)
        {
            return CallChannel<bool>("AssignIncidentNotificationToOperator", selectedNotification, selectedOperator);
        }

        public bool ActivateApplication(string application, string macAddress) 
        {
            return CallChannel<bool>("ActivateApplication", application, macAddress);
        }

        public bool RemoveInstalledApplication(string application, string macAddress) 
        {
            return CallChannel<bool>("RemoveInstalledApplication", application, macAddress);
        }

        public IList GetActiveApplications() 
        {
            return CallChannel<IList>("GetActiveApplications");
        }

        public IList GetInstalledApplicationsForWorkStation(string macAddress) 
        {
            return CallChannel<IList>("GetInstalledApplicationsForWorkStation", macAddress);
        }

        public IList GetSupervisedOperators(string applicationName, int departmentCode)
        {
            return CallChannel<IList>("GetSupervisedOperators", applicationName, departmentCode);
        }

        public IList GetOperatorsWorkingNowByAppByWorkShift(string applicationName, int workshifCode)
        {
            return CallChannel<IList>("GetOperatorsWorkingNowByAppByWorkShift", applicationName, workshifCode);
        }

        /// <summary>
        /// Calculates remaining time (in seconds) in selected status with pause
        /// </summary>
        /// <param name="operatorStatusCustomCode"></param>
        /// <returns>remaining time (in seconds) in selected status with pause</returns>
        public double CalculateAmountTimeAvailableByStatusWithPause(string operatorStatusCustomCode)
        {
            return CallChannel<double>("CalculateAmountTimeAvailableByStatusWithPause", operatorStatusCustomCode);
        }

        public bool CheckOperatorClientAccess(OperatorClientData oper, string supervisedApplicationName)
        {
            return CallChannel<bool>("CheckOperatorClientAccess", oper, supervisedApplicationName);
        }

        public object OperatorScheduleManagerMethod(string methodName, IList args)
        {
            return CallChannel<object>("OperatorScheduleManagerMethod", methodName, args);
        }

        public bool CheckGeneralSupervisor(int roleCode)
        {
            return CallChannel<bool>("CheckGeneralSupervisor", roleCode);
        }

        public bool CheckSupervisor(int roleCode)
        {
            return CallChannel<bool>("CheckSupervisor", roleCode);
        }

        public bool CheckApplicationSupervisor(int roleCode, string app)
        {
            return CallChannel<bool>("CheckApplicationSupervisor", roleCode, app);
        }

        public bool CheckFirstLevelOperator(int roleCode)
        {
            return CallChannel<bool>("CheckFirstLevelOperator", roleCode);
        }

        public bool CheckDispatchOperator(int roleCode)
        {
            return CallChannel<bool>("CheckDispatchOperator", roleCode);
        }

        public bool CheckUserAccess(int roleCode, string accessName)
        {
            return CallChannel<bool>("CheckUserAccess", roleCode, accessName);
        }

        public IList<string> GetIncidentCodeInRatio(GeoPoint point, double ratio, DistanceUnit distanceUnit)
        {
            return CallChannel<IList<string>>("GetIncidentCodeInRatio", point, ratio, distanceUnit);
        }

        public DepartmentStationClientData GetDepartmentStationForIncidentNotification(IncidentNotificationClientData notification)
        {
            return CallChannel<DepartmentStationClientData>("GetDepartmentStationForIncidentNotification", notification);
        }

        public RouteAddressClientData GetLastStop(UnitClientData unit, RouteClientData route)
        {
            return CallChannel<RouteAddressClientData>("GetLastStop", unit, route);
        }
		
		public TimeSpan GetAverageStopTime(UnitClientData selectedPosition, WorkShiftRouteClientData wsr)
		{
			return CallChannel<TimeSpan>("GetAverageStopTime", selectedPosition, wsr);
		}
       
		public double GetAverageSpeed(UnitClientData selectedPosition, WorkShiftScheduleVariationClientData schedule)
		{
			return CallChannel<double>("GetAverageSpeed", selectedPosition, schedule);
		}

		public IList GetRunTimePoints(UnitClientData selectedPosition, WorkShiftScheduleVariationClientData schedule)
		{
			return CallChannel<IList>("GetRunTimePoints", selectedPosition, schedule);
		} 
		#endregion

        #region Custom Server Call
        interface ICustomServer : IServerService, IClientChannel, IContextChannel
        { }

        /// <summary>
        /// Method that fix the server if one of them is not responding.
        /// </summary>
        /// <typeparam name="T">Type to return, object if void.</typeparam>
        /// <param name="args">Arguments from the method that has to be called.</param>
        /// <returns>The result of the method called.</returns>
        private T CallChannel<T>(string operation, params object[] args)
        {
            T result = default(T);
            bool successfull = false;
            int serverTries = 0;
            while (successfull == false)
            {
                IServerService channel = null;
                try
                {
                    channel = factory.CreateChannel();

                    Type contract = typeof(IServerService);
                    MethodInfo methodInfo = null;

                    //Create the Type array to get the method.
                    Type[] methodTypes = new Type[args.Length];

                    //Fill the arrays with the parameter types.
                    for (int parametersIndex = 0; parametersIndex < args.Length; parametersIndex++)
                    {
                        object parameterInfo = args[parametersIndex];
                        if (parameterInfo != null)
                            methodTypes[parametersIndex] = parameterInfo.GetType();
                        else
                            methodTypes[parametersIndex] = typeof(object);
                    }
                    //We obtain the method that we want to call.
                    //MethodInfo methodInfo = channel.GetType().GetMethod(method.Name, methodTypes);

                    methodInfo = contract.GetMethod(operation, methodTypes);


                    ((IContextChannel)channel).AddAutoHeader("Session", "http://Session", sessionState);
#if DEBUG
                    ((IContextChannel)channel).OperationTimeout = TimeSpan.FromMinutes(10);
#endif

                    if (args.Length > 0 && methodInfo.ReturnType != typeof(void))
                        result = (T)methodInfo.Invoke(channel, args);
                    else if (args.Length > 0 && methodInfo.ReturnType == typeof(void))
                        methodInfo.Invoke(channel, args);
                    else if (args.Length == 0 && methodInfo.ReturnType != typeof(void))
                        result = (T)methodInfo.Invoke(channel, null);
                    else if (args.Length == 0 && methodInfo.ReturnType == typeof(void))
                        methodInfo.Invoke(channel, null);
                    successfull = true;

                    ((IClientChannel)channel).Close();
                }
                catch (EndpointNotFoundException)
                {
                    serverTries++;
                    if (channel != null)
                        ((IClientChannel)channel).Abort();
                    if (serverTries >= 2)
                        throw;
                }
                catch (SocketException)
                {
                    serverTries++;
                    if (channel != null)
                        ((IClientChannel)channel).Abort();
                    if (serverTries >= 2)
                        throw;
                }
                catch (Exception ex)
                {
                    if (ex.InnerException is EndpointNotFoundException ||
                        ex is EndpointNotFoundException ||
                        ex.InnerException is SocketException ||
                        ex is SocketException)
                    {
                        serverTries++;
                        if (channel != null)
                            ((IClientChannel)channel).Abort();
                        if (serverTries >= 2)
                        {
                            throw new EndpointNotFoundException();
                        }
                    }
                    else if (ex.InnerException != null)
                        throw ex.InnerException;
                    else
                        throw;
                }
                finally
                {
                    if (channel != null)
                    {
                        ((IClientChannel)channel).Dispose();
                    }
                }
                Thread.Sleep(100);
            }
            return result;
        }
        #endregion				
	}

    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class ApplicationServerService : IApplicationService
    {
        #region Events
        public static event EventHandler<CommittedObjectDataCollectionEventArgs> CommittedChanges;
        public static event EventHandler<GisActionEventArgs> GisAction;
        #endregion

        #region Fields
        private ServiceHost host;
        #endregion

        #region Constructor
        public ApplicationServerService()
        { }
        #endregion

        #region Methods
        public void OpenHost(string application)
        {
            if (host == null)
            {
                String address = ApplicationUtil.BuildServiceUrl(
                    "net.tcp",
                    "localhost",
                    UserApplicationClientData.GetUserApplicationPortByName(application),
                    SmartCadConfiguration.CLIENT_SERVICE_ADDRESS);

                Binding binding = BindingBuilder.GetNetTcpBinding();

                string address2 = "net.pipe://localhost/" + application;

                Binding binding2 = new NetNamedPipeBinding(NetNamedPipeSecurityMode.None);
                binding2.ReceiveTimeout = TimeSpan.MaxValue;

                host = ServiceHostBuilder.ConfigureHost(
                    typeof(ApplicationServerService), 
                    typeof(IApplicationService), new Binding[] {binding, binding2}, new string[] {address, address2});
                host.Open();
            }
        }

        public void CloseHost()
        {
            try
            {
                if (host != null)
                    host.Abort();
            }
            catch { }
        }
        #endregion

        #region IServerServiceCallback Members

        public void OnPing()
        {

        }

        public void OnCommittedChanges(object sender, CommittedObjectDataCollectionEventArgs committedObjectDataCollectionEventArgs)
        {
            if (CommittedChanges != null)
            {
                Delegate[] list = CommittedChanges.GetInvocationList();
                foreach(Delegate del in list)
                {
                    try
                    {
                        del.DynamicInvoke(sender, committedObjectDataCollectionEventArgs);
                    }
                    catch (Exception ex)
                    {
                        SmartLogger.Print(ex);
                    }
                }
            }
        }

        public void OnGisAction(GisActionEventArgs e)
        {
            if (GisAction != null)
            {
                Delegate[] list = GisAction.GetInvocationList();
                foreach (Delegate del in list)
                {
                    try
                    {
                        del.DynamicInvoke(null, e);
                    }
                    catch (Exception ex)
                    {
                        SmartLogger.Print(ex);
                    }
                }
            }
        }

        #endregion
    }
}

