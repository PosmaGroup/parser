using System;
using System.Collections.Generic;
using System.Text;
using System.ServiceModel;
using System.Net.Security;
using SmartCadCore.Core;

namespace Smartmatic.SmartCad.Service
{
    [ServiceContract(ProtectionLevel = ProtectionLevel.None)]
    public interface IApplicationService
    {
        [OperationContract(IsOneWay = true)]
        void OnPing();

        [OperationContract(IsOneWay = true)]
        void OnCommittedChanges(object sender, CommittedObjectDataCollectionEventArgs committedObjectDataCollectionEventArgs);

        [OperationContract(IsOneWay = true)]
        void OnGisAction(GisActionEventArgs e);
    }
}
