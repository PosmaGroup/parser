using System;
using System.Collections.Generic;
using System.Text;
using System.ServiceModel;
using SmartCadCore.Core;

using System.ServiceModel.Channels;
using SmartCadCore.ClientData;
using System.Collections;
using SmartCadCore.Common;


namespace SmartCadServices
{
#if DEBUG
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple, IncludeExceptionDetailInFaults = true)]
#else
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
#endif
    public class TelephonyService : ITelephonyService, IDisposable
    {
        private ITelephonyServiceCallback callback;
        private TelephonyManagerAbstract telephonyManager;
        private ConfigurationClientData serverConfiguration;
        private bool virtualMode = true;
        private bool virtualCall = false;
        private object locker = new object();
        Hashtable properties = new Hashtable();
        
        public TelephonyService()
        {
            //callback = OperationContext.Current.GetCallbackChannel<ITelephonyServiceCallback>();            
        }

        #region IDisposable

        private bool disposed = false;

        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            Logout();
            if (!this.disposed)
            {
                if (disposing)
                {                    
                    // Dispose managed resources.
                }

                // Call the appropriate methods to clean up
                // unmanaged resources here.
                disposed = true;
            }
        }

        ~TelephonyService()
        {
            Dispose(false);
        }

        #endregion

        #region Host

        public static ServiceHost Host()
        {
            String address = ApplicationUtil.BuildServiceUrl(
                "net.tcp",
                "localhost",
                SmartCadConfiguration.TELEPHONY_SERVICE_PORT,
                SmartCadConfiguration.TELEPHONY_SERVICE_ADDRESS);

            Binding binding = BindingBuilder.GetNetTcpBinding();

            ServiceHost host = ServiceHostBuilder.ConfigureHost(typeof(TelephonyService), typeof(ITelephonyService), binding, address);

            return host;
        }

        #endregion

        #region ITelephonyService Members

        public void SetConfiguration(ConfigurationClientData configurationProperties)
        {
            this.serverConfiguration = configurationProperties;
        }

        public void Login(string login, string password, string agentId, string agentPassword, bool recordCalls, string extNumber, bool isReady)
        {
            try
            {
                if (properties != null)
                {
                    properties.Clear();
                }
                if (properties.Count == 0)
                {
                    properties.Add(CommonProperties.ServerType, serverConfiguration.TelephonyServerType);
                    Console.WriteLine("ServerType - {0}", serverConfiguration.TelephonyServerType);
                    properties.Add(CommonProperties.AgentId, agentId);
                    Console.WriteLine("AgentId - {0}", agentId);
                    properties.Add(CommonProperties.AgentPassword, agentPassword);
                    Console.WriteLine("AgentPassword - {0}", agentPassword);
                    properties.Add(CommonProperties.Domain, Environment.UserDomainName);
                    Console.WriteLine("UserName - {0}", Environment.UserDomainName);
                    properties.Add(CommonProperties.Extension, serverConfiguration.Extension);
                    Console.WriteLine("Extension - {0}", serverConfiguration.Extension);
                    properties.Add(CommonProperties.ExtensionPassword, serverConfiguration.ExtensionPassword);
                    Console.WriteLine("Extension Password - {0}", serverConfiguration.ExtensionPassword);
                    properties.Add(CommonProperties.UserLogin, login);
                    Console.WriteLine("Login - {0}", login);
                    properties.Add(CommonProperties.UserPassword, password);
                    Console.WriteLine("Password - {0}", password);
                    properties.Add(CommonProperties.Ready, isReady);
                    Console.WriteLine("Ready - {0}", isReady);
                    properties.Add(CommonProperties.VirtualMode, serverConfiguration.VirtualMode);
                    Console.WriteLine("Virtual - {0}", serverConfiguration.VirtualMode);
                    properties.Add(CommonProperties.RecordCalls, recordCalls);
                    Console.WriteLine("Record - {0}", recordCalls);
                    
                    //TODO: check if properties = telephonyServerProperties works
                    foreach (KeyValuePair<string,string> prop in serverConfiguration.TelephonyServerProperties)
                    {
                        properties.Add(prop.Key, prop.Value);
                        Console.WriteLine("Name - {0}, Value - {1}", prop.Key, prop.Value);
                    }
                }

                if (serverConfiguration.VirtualMode)
                    SetVirtualCall(true);

                Console.WriteLine("Telephony callback");
                callback = OperationContext.Current.GetCallbackChannel<ITelephonyServiceCallback>();

                if (/*string.IsNullOrEmpty(agentId) == false &&*/
                    string.IsNullOrEmpty(extNumber) == false )
                {
                    TelephonyFactory.SetConfiguration(properties);
                    try
                    {
                        if (serverConfiguration.VirtualMode == false)
                        {
                            lock (locker)
                            {
                                //Checks the extension exists and there is connection to server
                                Console.WriteLine("Initializing designated DLL");
                                telephonyManager = TelephonyFactory.GetTelephonyManager();
                                telephonyManager.CallRinging += new EventHandler<CallRingingEventArgs>(telephonyManager_CallRinging);
                                telephonyManager.CallReleased += new EventHandler<CallReleasedEventArgs>(telephonyManager_CallReleased);
                                telephonyManager.CallAbandoned += new EventHandler<CallAbandonedEventArgs>(telephonyManager_CallAbandoned);
                                telephonyManager.CallEstablished += new EventHandler<CallEstablishedEventArgs>(telephonyManager_CallEstablished);
                                telephonyManager.DigitsReceive += new EventHandler<DigitsReceivedEventArgs>(telephonyManager_DigitsReceive);
                                telephonyManager.CallMade += new EventHandler<CallMadeEventArgs>(telephonyManager_CallMade);
                                telephonyManager.ReadyStatusChanged += new EventHandler<ReadyStatusChangedEventArgs>(telephonyManager_ReadyStatusChanged);
                                telephonyManager.DoNotDisturbChanged += new EventHandler<DoNotDisturbChangedEventArgs>(telephonyManager_DoNotDisturbChanged);
                                telephonyManager.TerminalUnregistered += telephonyManager_TerminalUnregistered;
                                telephonyManager.OnError += new EventHandler<TelephonyErrorEventArgs>(telephonyManager_OnError);
                                telephonyManager.CallRecorded += new EventHandler<CallRecordedEventArgs>(telephonyManager_CallRecorded);
                                virtualMode = false;
                                telephonyManager.TestCTIState();
                            }
                        }
                    }
                    catch(Exception ex)
                    {
                        virtualMode = true;
                        throw ex;
                    }
                }
                else
                {
                    virtualMode = true;
                }
                
                OnConnected();
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void telephonyManager_CallRecorded(object sender, CallRecordedEventArgs e)
        {
            try
            {
                this.callback.OnTelephonyAction(e);
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        void telephonyManager_TerminalUnregistered(object sender, TerminalUnRegisteredEventArgs e)
        {
            try
            {
                this.callback.OnTelephonyAction(e);
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        void telephonyManager_CallMade(object sender, CallMadeEventArgs e)
        {
            try
            {
                this.callback.OnTelephonyAction(e);
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        void telephonyManager_DigitsReceive(object sender, DigitsReceivedEventArgs e)
        {
            try
            {
                this.callback.OnTelephonyAction(e);
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        void telephonyManager_OnError(object sender, TelephonyErrorEventArgs e)
        {
            if (e.ErrorMessage == "DN is not configured in CME" ||
                e.ErrorMessage == "NoConnectionToCTI")
            {
                if (virtualMode == false)
                    telephonyManager_CallReleased(null, new CallReleasedEventArgs("", "", "", TimeSpan.Zero));
                System.Threading.Thread.Sleep(100);
                virtualMode = true;
                OnConnected();
            }
            else
            {
                try
                {
                    this.callback.OnTelephonyAction(e);
                }
                catch (Exception ex)
                {
                    SmartLogger.Print(ex);
                }
            }
        }


        public void Answer()
        {
            try
            {
                //It only works for Nortel. In genesys is not implemented yet
                if (virtualMode == false)
                {
                    telephonyManager.Answer();
                }
                else
                {
                    telephonyManager_CallEstablished(null, new CallEstablishedEventArgs("", "", "", TimeSpan.Zero));
                }
            }
            catch (Exception ex)
            {
                if (telephonyManager is TelephonyManagerAbstract)
                {
                    try
                    {
                        telephonyManager.TestCTIState();//If server is down, a manual released is performed
                    }
                    catch
                    {
                        telephonyManager_CallReleased(null, new CallReleasedEventArgs("", "", "", TimeSpan.Zero));
                    }
                }
                HandleException(ex);
            }
        }

        public void Disconnect()
        {
            try
            {
                if (virtualMode == false)
                    telephonyManager.Release();
                else
                    telephonyManager_CallReleased(null, new CallReleasedEventArgs("", "", "", TimeSpan.Zero));
            }
            catch (Exception ex)
            {
                telephonyManager_CallReleased(null, new CallReleasedEventArgs("", "", "", TimeSpan.Zero));
                HandleException(ex);
            }
        }

        public void SetIsReady(bool isReady)
        {
            try
            {
                if (virtualMode == false)
                    telephonyManager.SetReadyStatus(isReady);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        public void RecoverConnection()
        {
            try
            {
                if (virtualMode == false)
                    telephonyManager.RecoverConnection();
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }


        public void Logout()
        {
            try
            {
                if (virtualMode == false)
                {
                    telephonyManager.CallRinging -= new EventHandler<CallRingingEventArgs>(telephonyManager_CallRinging);
                    telephonyManager.CallReleased -= new EventHandler<CallReleasedEventArgs>(telephonyManager_CallReleased);
                    telephonyManager.CallAbandoned -= new EventHandler<CallAbandonedEventArgs>(telephonyManager_CallAbandoned);
                    telephonyManager.CallEstablished -= new EventHandler<CallEstablishedEventArgs>(telephonyManager_CallEstablished);
                    telephonyManager.DigitsReceive -= new EventHandler<DigitsReceivedEventArgs>(telephonyManager_DigitsReceive);
                    telephonyManager.CallMade -= new EventHandler<CallMadeEventArgs>(telephonyManager_CallMade);
                    telephonyManager.ReadyStatusChanged -= new EventHandler<ReadyStatusChangedEventArgs>(telephonyManager_ReadyStatusChanged);
                    telephonyManager.DoNotDisturbChanged -= new EventHandler<DoNotDisturbChangedEventArgs>(telephonyManager_DoNotDisturbChanged);
                    telephonyManager.OnError -= new EventHandler<TelephonyErrorEventArgs>(telephonyManager_OnError);
                    telephonyManager.CallRecorded -= new EventHandler<CallRecordedEventArgs>(telephonyManager_CallRecorded);

                    telephonyManager.Logout();
                    telephonyManager.Close();
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

       
        public void Ping()
        {
            try
            {
                lock (locker)
                {
                    if (virtualMode == false)
                    {
                        if (telephonyManager != null)
                        {
                            if (telephonyManager.GetType() == typeof(TelephonyManagerAbstract) && !virtualCall)
                            {
                                try
                                {
                                    telephonyManager.TestCTIState();
                                }
                                catch (Exception ex)
                                {
                                    SmartLogger.Print(ex);
                                    telephonyManager_CallReleased(null, new CallReleasedEventArgs("", "", "", TimeSpan.Zero));
                                    throw ex;
                                }
                            }
                        }
                        else
                            throw new Exception("NoConnectionToCTI");
                    }
                    else
                    {
                        if (properties != null && properties.Count > 0 && !virtualCall)
                        {
                            Login((string)properties[CommonProperties.UserLogin],
                                (string)properties[CommonProperties.UserPassword],
                                (string)properties[CommonProperties.AgentId],
                                (string)properties[CommonProperties.AgentPassword],
                                (bool)properties[CommonProperties.RecordCalls],
                                (string)properties[CommonProperties.Extension],
                                (bool)properties[CommonProperties.Ready]);
                            SetIsReady(false);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        public void GenerateCall(string number)
        {
            if (virtualMode == false)
            {
                try
                {
                    telephonyManager.Call(number);
                }
                catch (Exception ex)
                {
                    telephonyManager_CallReleased(null, new CallReleasedEventArgs("", "", "", TimeSpan.Zero));
                    HandleException(ex);
                }
            }
            else
            {
                virtualCall = true;
                CallMadeEventArgs args1 = new CallMadeEventArgs();
                telephonyManager_CallMade(null, args1);
                telephonyManager_CallRinging(null, new CallRingingEventArgs(number, string.Empty, string.Empty, TimeSpan.Zero));
            }
        }

        public bool IsVirtual()
        {
            return virtualMode;
        }

        public void SetVirtualCall(bool virtualCall)
        {
            this.virtualCall = virtualCall;
        }

        public bool InVirtualCall()
        {
            return this.virtualCall;
        }

        /// <summary>
        /// Puts the current call in parking state, waiting for a dispatcher to re-take it.
        /// </summary>
        /// <param name="extensionParking">Extension number where call will be parked(queue number)</param>
        /// <param name="parkId">call id used to park and unpark the call</param>
        public void ParkCall(string extensionParking, string parkId)
        {
            if (virtualMode == false)
            {
                telephonyManager.ParkCall(extensionParking, parkId);
            }
        }

        #endregion

        private void HandleException(Exception ex)
        {
            SmartLogger.Print(ex);
            virtualMode = true;
            OnConnected();
            throw ApplicationUtil.NewFaultException(ex);
        }

        private void telephonyManager_CallRinging(object sender, CallRingingEventArgs args)
        {
            try
            {
                this.callback.OnTelephonyAction(args);
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        private void telephonyManager_CallReleased(object sender, CallReleasedEventArgs e)
        {
            try
            {
                this.callback.OnTelephonyAction(e);
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        void telephonyManager_CallAbandoned(object sender, CallAbandonedEventArgs e)
        {
            try
            {
                virtualCall = false;
                this.callback.OnTelephonyAction(e);
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        private void telephonyManager_CallEstablished(object sender, CallEstablishedEventArgs e)
        {
            try
            {
                this.callback.OnTelephonyAction(e);
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        private void telephonyManager_ReadyStatusChanged(object sender, ReadyStatusChangedEventArgs e)
        {
            try
            {
                ReadyStatusChangedEventArgs rsc = new ReadyStatusChangedEventArgs(e.IsReady, e.NotReadyReasonCode);
                this.callback.OnTelephonyAction(rsc);
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        private void telephonyManager_DoNotDisturbChanged(object sender, DoNotDisturbChangedEventArgs e)
        {
            try
            {
                DoNotDisturbChangedEventArgs dnd = new DoNotDisturbChangedEventArgs(e.DoNotDisturb);
                this.callback.OnTelephonyAction(dnd);
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        /// <summary>
        /// Indicate the status of the server.
        /// </summary>
        private void OnConnected()
        {
            try
            {
                if (this.callback != null)
                {
                    TelephonyServerConnectionEventArgs connection = new TelephonyServerConnectionEventArgs();
                    connection.Connected = virtualMode == false;

                    this.callback.OnTelephonyAction(connection);
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }



        public void DivertCall()
        {
            if (virtualMode == false)
            {
                telephonyManager.Release();
            }
        }

        public void SetPhoneReportCustomCode(string customCode)
        {
            telephonyManager.SetPhoneReport(customCode);
        }
    }
}
