using System;
using System.Collections.Generic;
using System.Text;
using System.ServiceModel;
using System.Reflection;
using System.Collections;
using System.Security.Principal;
using System.Threading;
using System.Collections.Specialized;
using System.IO;
using System.Net.Sockets;
using System.ServiceModel.Channels;

using NHibernate;
using NHibernate.Collection;

using System.Diagnostics;
using Iesi.Collections;
using System.Data.SqlClient;
using System.ComponentModel;

using SmartCadCore.ClientData;
using SmartCadCore.Core;
using SmartCadCore.Model;
using SmartCadCore.Common;
using SmartCadCore.Model.Util;

namespace Smartmatic.SmartCad.Service
{
#if DEBUG
    [SessionStateContextDispatchBehavior]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall, ConcurrencyMode = ConcurrencyMode.Multiple, IncludeExceptionDetailInFaults = true)]
#else
    [SessionStateContextDispatchBehavior]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall, ConcurrencyMode = ConcurrencyMode.Multiple)]
#endif
    public class ServerService : IServerService
    {
        #region Fields
        
        //Fer: This variable is used to initialize and use by the map events, this should be deleted
        //when the client can communicate with each other without going throught the server.
        //This means that the clients should use a WCF internally.
        public static ServiceHelper helper;

        #endregion

        #region Constructors

        static ServerService()
        {
            try
            {
                helper = new ServiceHelper();
                helper.Start();
            }
            catch (Exception ex)
            {
                SmartLogger.Print("********");
                SmartLogger.Print(ex);
            }
        }

        public ServerService()
        {
        }

        #endregion

        private UserApplicationData Application
        {
            get
            {
                return UserApplicationData.GetUserApplicationByName(SessionStateContextExtension.Current.Session.Application);
            }
        }

        private OperatorSessionState Operator
        {
            get
            {
                if (SessionStateContextExtension.Current.Session.User != null)
                    return SessionStateContextExtension.Current.Session.User;
                else
                    return new OperatorSessionState() { Login = string.Empty, Code = 0, Role = 0 };
            }
        }

        private OperatorData OperatorDataInfo
        {
            get
            {
                return new OperatorData() { Code = Operator.Code, Login = Operator.Login };
            }
        }

        #region Private Methods

        private SmartCadContext GetCurrentContext
        {
            get
            {
                return ServiceUtil.CreateContext(Operator.Login, Application.Name);
            }
        }

        private void AuditAction(ObjectData objectData)
        {
            if (objectData.Code == 0)
                SmartCadAudit.Audit(Application, Operator.Login, Operator.Code, objectData.GetType().Name, objectData, OperatorActions.Save);
            else
                SmartCadAudit.Audit(Application, Operator.Login, Operator.Code, objectData.GetType().Name, objectData, OperatorActions.Update);
        }

        private void AuditAction(ObjectData objectData, OperatorActions action)
        {
            SmartCadAudit.Audit(Application, Operator.Login, Operator.Code, objectData.GetType().Name, objectData, action);
        }

        #endregion

        #region IServerService Methods

        public OperatorClientData OpenSession(string login, string password, string extNumber, string computerName, UserApplicationClientData app)
        {
            try
            {
                OperatorData user = new OperatorData();
                user.Login = login;

                SessionHistoryData uaad = SmartCadDatabase.SearchObject<SessionHistoryData>(
                    SmartCadHqls.GetCustomHql(
                    SmartCadHqls.GetUserSessionByApplication,
                    Application.Name,
                    login));

                if (uaad != null)
                {
                    SmartCadAudit.Audit(Application, user.Login, user.Code, ResourceLoader.GetString2("AuditUserAlreadyLoggedIn"));
                    throw new InvalidLoginException(ResourceLoader.GetString2("UserAlreadyLoggedIn"));
                }
                else
                {
                    SessionHistoryData shOtherMachine = SmartCadDatabase.SearchObject<SessionHistoryData>(
                    SmartCadHqls.GetCustomHql(
                        SmartCadHqls.GetUserSessionInOtherMachine,
                        computerName,
                        login));

                    if (shOtherMachine != null)
                    {
                        SmartCadAudit.Audit(Application, user.Login, user.Code, ResourceLoader.GetString2("AuditUserAlreadyLoggedInOtherMachine"));
                        throw new InvalidLoginException(ResourceLoader.GetString2("UserAlreadyLoggedInOtherMachine",shOtherMachine.ComputerName));
                    }
                    else
                    {
                        bool authenticated = false;
                        user = SmartCadDatabase.SearchObject<OperatorData>(user);

                        if (user != null)
                        {
                            if (user.Windows.HasValue == false || (user.Windows.HasValue == true && user.Windows == false))
                                authenticated = user.Password == CryptoUtil.Hash(password);
                            else
                                authenticated = Domain.ValidateUser(login, password);
                        }

                        if (user == null || user.Login != login || authenticated == false)
                        {
                            SmartCadAudit.Audit(Application, login, 0, ResourceLoader.GetString2("AuditInvalidLogin"));
                            throw new InvalidLoginException(ResourceLoader.GetString2("InvalidLoginOrPassword"));
                        }

                        SessionStateContextExtension.Current.Session.User = new OperatorSessionState() { Code = user.Code, Login = user.Login, Role = user.Role.Code };
                        if (CheckAccess(UserResourceData.Application, UserActionData.Basic, Application, false) == true)
                        {
                            if (Application == UserApplicationData.FirstLevel || Application == UserApplicationData.Dispatch)
                            {
                                bool checkLoginMode = true;

                                try
                                {
                                    ApplicationPreferenceData preference = SmartCadDatabase.SearchBasicObject(
                                        SmartCadHqls.GetCustomHql(SmartCadHqls.GetApplicationPreferenceByName, "CheckLogInMode"))
                                        as ApplicationPreferenceData;
                                    checkLoginMode = Convert.ToBoolean(preference.Value);
                                }
                                catch (Exception ex1)
                                {
                                    SmartLogger.Print(ex1);
                                }
                                if (checkLoginMode == true)
                                {
                                    int maxTimeToEnter = 5;

                                    ApplicationPreferenceData preference = SmartCadDatabase.SearchBasicObject(
                                        SmartCadHqls.GetCustomHql(
                                            SmartCadHqls.GetApplicationPreferenceByName, "ExtraTimeToEnterApplication")) 
                                                as ApplicationPreferenceData;

                                    if (preference != null)
                                        maxTimeToEnter = Convert.ToInt32(preference.Value);

                                    bool workingNow = OperatorScheduleManager.IsWorkingNow(user.Code, SmartCadDatabase.GetTimeFromBD().AddMinutes(maxTimeToEnter));
                                    
                                    if (workingNow == false)
                                    {

                                        bool vacation = OperatorScheduleManager.CheckVacationByOperator(user.Code, DateTime.Now);
                                        if (vacation == true)
                                        {
                                            SmartCadAudit.Audit(Application, user.Login, user.Code, ResourceLoader.GetString2("OperatorOnVacation"));
                                            throw new InvalidLoginException(ResourceLoader.GetString2("OperatorOnVacation"));
                                        }
                                        else
                                        { 
                                            bool userHasWorkshift = OperatorScheduleManager.GetCurrentWorkShiftSchedulesByOperator(user.Code, DateTime.Now) != null;

                                            if (userHasWorkshift == false)
                                            {
                                                SmartCadAudit.Audit(Application, user.Login, user.Code, ResourceLoader.GetString2("UserDoesNotHaveWorkShift"));
                                                throw new InvalidLoginException(ResourceLoader.GetString2("UserDoesNotHaveWorkShift"));
                                            }
                                        
                                        }
       
                                    }
                                    else
                                    {
                                        int supervisor = OperatorScheduleManager.GetSupervisorCode(user.Code, Application, SmartCadDatabase.GetTimeFromBD().AddMinutes(maxTimeToEnter));
                                        if (supervisor == 0)
                                        {
                                            SmartCadAudit.Audit(Application, user.Login, user.Code, ResourceLoader.GetString2("OperatorDoesNotHaveSupervisorNow"));
                                            throw new InvalidLoginException(ResourceLoader.GetString2("OperatorDoesNotHaveSupervisorNow"));
                                        }
                                        else
                                        {
                                            bool vacation = OperatorScheduleManager.CheckVacationByOperator(supervisor, DateTime.Now);
                                            if (vacation == true)
                                            {
                                                SmartCadAudit.Audit(Application, user.Login, user.Code, ResourceLoader.GetString2("OperatorSupervisorOnVacation"));
                                                throw new InvalidLoginException(ResourceLoader.GetString2("OperatorSupervisorOnVacation"));
                                            }
                                        }
                                    }
                                }
                            }
                            try
                            {
                                user.Password = password;
                                uaad = ServiceUtil.LoginOperator(Application, user, computerName);

                                SmartCadAudit.Audit(Application, user.Login, user.Code, ResourceLoader.GetString2("AuditValidLogin"));
                                
                                user.Password = CryptoUtil.Hash(password);
                            }
                            catch
                            {
                                SmartCadAudit.Audit(Application, user.Login, user.Code, ResourceLoader.GetString2("AuditUserAlreadyLoggedIn"));
                                throw new InvalidLoginException(ResourceLoader.GetString2("UserAlreadyLoggedIn"));
                            }
                        }
                        else
                        {
                            SmartCadAudit.Audit(Application, user.Login, user.Code, ResourceLoader.GetString2("AuditInvalidAccess"));
                            throw new InvalidLoginException(ResourceLoader.GetString2("NoAccessToEnterApplication"));
                        }
                    }
                }

                return OperatorConversion.ToClient(user,Application);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return null;
            }
        }

        public void RequestReassignIncidentNotifications()
        {
            try
            {
                OperatorData oper = new OperatorData();
                oper.Code = this.OperatorDataInfo.Code;
                oper = SmartCadDatabase.RefreshObject(oper) as OperatorData;

                ApplicationPreferenceData preferenceMaxNotificationsToReassign = SmartCadDatabase.SearchObject<ApplicationPreferenceData>(
                            SmartCadHqls.GetCustomHql(SmartCadHqls.GetApplicationPreferenceByName, "MaxNotificationsToReassign"));
                ServiceUtil.AssignIncidentNotificationToOperator(oper, int.Parse(preferenceMaxNotificationsToReassign.Value));
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        public void CloseSession()
        {
            try
            {
                CheckSecurity();
                if (Application != null)
                {
                    SmartCadAudit.Audit(Application, Operator.Login, Operator.Code, ResourceLoader.GetString2("CloseSession"), null, OperatorActions.Logout);

                    this.EndCloseReport();
                    if (Application != null && Operator != null)
                    {
                        SessionHistoryData uaad = SmartCadDatabase.SearchObject<SessionHistoryData>(
                            SmartCadHqls.GetCustomHql(
                            SmartCadHqls.GetUserSessionByApplication,
                            Application.Name,
                            Operator.Login));
                        if (uaad != null && uaad.IsLoggedIn == true)
                        {
                            helper.CloseSession(uaad);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        public void EndCloseReport()
        {
            try
            {
                if (Application != null && Operator != null && ServiceUtil.CheckSupervisor(Operator.Role) == true)
                {
                    SessionHistoryData session = SmartCadDatabase.SearchObject<SessionHistoryData>(
                           SmartCadHqls.GetCustomHql(SmartCadHqls.GetUserSessionByApplication, Application.Name, Operator.Login));

                    if (session != null)
                    {
                        helper.EndCloseReport(session);
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        public bool CheckAccess(UserResourceData userResource, UserActionData userAction,
            UserApplicationData userApplication, bool throwException)
        {
            bool accessFound = false;
            try
            {
                IList accessList = SmartCadDatabase.SearchObjects(
                    SmartCadHqls.GetCustomHql(
                    SmartCadHqls.GetAllAccessByOperatorAndApplication, 
                    Operator.Code, 
                    userApplication.Code));

                //SmartCadDatabase.InitializeLazy(Operator.Role, Operator.Role.Profiles);
                //IList profiles = Operator.Role.Profiles;
                //int currentProfileIndex = 0;

                //while ((accessFound == false)
                //    && (currentProfileIndex < profiles.Count))
                //{
                //    UserProfileData userProfileData = profiles[currentProfileIndex] as UserProfileData;
                //    SmartCadDatabase.InitializeLazy(userProfileData, userProfileData.Accesses);
                //    ISet accessList = userProfileData.Accesses;
                    foreach (UserAccessData userAccessData in accessList)
                    {
                        if ((userAccessData.UserApplication.Equals(userApplication)) &&
                               ((userAction.Equals(UserActionData.Basic)
                                 && CheckBasicAccessApplication(userAccessData, userApplication)
                                 && userResource.Equals(UserApplicationData.Resource)) ||
                                (userAccessData.UserPermission.UserResource.Equals(userResource)) &&
                                (userAccessData.UserPermission.UserAction.Equals(userAction))))
                        {
                            accessFound = true;
                        }
                        if (accessFound == true)
                            break;
                    }

                //    currentProfileIndex++;
                //}
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return accessFound;
            }

            return accessFound;
        }

        private bool CheckBasicAccessApplication(UserAccessData access, UserApplicationData application)
        {
            bool result = false;
            SmartCadDatabase.InitializeLazy(application, application.Accesses);
            foreach (UserAccessData appAccess in application.Accesses)
            {
                if (appAccess.Code == access.Code)
                {
                    result = true;
                    break;
                }
            }
            return result;
        }

        public bool CheckOperatorClientAccess(OperatorClientData oper, string supervisedApplicationName)
        {
            bool result = false;
            try
            {
                result = ServiceUtil.CheckUserApplication(oper.RoleCode, supervisedApplicationName);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return result;
            }
            return result;
        }

        public bool CheckAccessClient(UserResourceClientData userResource, UserActionClientData userAction,
            UserApplicationClientData userApplication, bool throwException)
        {
            bool result = false;
            try
            {
                UserResourceData resource = UserResourceConversion.ToObject(userResource, this.Application);
                UserActionData action = null;
                UserApplicationData userAppData = null;
                if (userAction.Name.Equals(UserActionData.Basic.Name))
                {
                    action = UserActionData.Basic;
                }
                else if (userAction.Name.Equals(UserActionData.Delete.Name))
                {
                    action = UserActionData.Delete;
                }
                else if (userAction.Name.Equals(UserActionData.Insert.Name))
                {
                    action = UserActionData.Insert;
                }
                else if (userAction.Name.Equals(UserActionData.Search.Name))
                {
                    action = UserActionData.Search;
                }
                else if (userAction.Name.Equals(UserActionData.Update.Name))
                {
                    action = UserActionData.Update;
                }
                else if (userAction.Name.Equals(UserActionData.Duplicate.Name))
                {
                    action = UserActionData.Duplicate;
                }

                if (userApplication.Name.Equals(UserApplicationData.Administration.Name))
                {
                    userAppData = UserApplicationData.Administration;
                }
                else if (userApplication.Name.Equals(UserApplicationData.Dispatch.Name))
                {
                    userAppData = UserApplicationData.Dispatch;
                }
                else if (userApplication.Name.Equals(UserApplicationData.FirstLevel.Name))
                {
                    userAppData = UserApplicationData.FirstLevel;
                }
                else if (userApplication.Name.Equals(UserApplicationData.Map.Name))
                {
                    userAppData = UserApplicationData.Map;
                }
                else if (userApplication.Name.Equals(UserApplicationData.Report.Name))
                {
                    userAppData = UserApplicationData.Report;
                }
                else if (userApplication.Name.Equals(UserApplicationData.Server.Name))
                {
                    userAppData = UserApplicationData.Server;
                }
                else if (userApplication.Name.Equals(UserApplicationData.Cctv.Name))
                {
                    userAppData = UserApplicationData.Cctv;
                }
                else if (userApplication.Name.Equals(UserApplicationData.Supervision.Name))
                {
                    userAppData = UserApplicationData.Supervision;
                }
                result = CheckAccess(resource, action, userAppData, throwException);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return result;
            }
            return result;
        }

        public IList SearchObjects(ObjectData obj)
        {
            try
            {
                if (Operator != null)
                {
                    SmartCadAudit.Audit(Application, Operator.Login, Operator.Code, obj.GetType().Name, "");
                }
                IList result = SmartCadDatabase.SearchObjects(obj);

                return result;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return null;
            }
        }

        public IList SearchObjects(string hql)
        {
            IList result = null;

            try
            {
                if (Operator != null)
                {
                    SmartCadAudit.Audit(Application, Operator.Login, Operator.Code, hql);
                }
                result = SmartCadDatabase.SearchObjects(hql);
                return result;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return null;
            }
        }

        public IList MaxSearchObjects(string hql, int maxMessageSize)
        {
            maxMessageSize = maxMessageSize / 2;
            // try
            //{
            IList result = SearchObjects(hql);

            System.Runtime.Serialization.Formatters.Binary.BinaryFormatter formatter
                = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

            MemoryStream memory = new MemoryStream();

            object[] resultObj = new object[maxMessageSize];
            result.CopyTo(resultObj, 0);
            formatter.Serialize(memory, resultObj);
            int resulSize = memory.GetBuffer().Length;
            if (resulSize >= maxMessageSize)
            {
                FaultException exception = new FaultException(ResourceLoader.GetString2("FilterSearch"));

                result = null;

                throw exception;
            }
            return result;
            //}
            //catch (Exception ex)
            //{
            //    HandleException(ex);
            //    return null;
            //}
        }

        public ObjectData SearchObject(ObjectData obj)
        {
            try
            {
                SmartCadAudit.Audit(Application, Operator.Login, Operator.Code, obj.GetType().Name, "");

                ObjectData result = SmartCadDatabase.SearchObject<ObjectData>(obj);

                return result;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return null;
            }
        }

        public void DeleteObject(ObjectData objectData)
        {
            try
            {
                CheckSecurity();
                SmartCadDatabase.DeleteObject(objectData, GetCurrentContext);
                SmartCadAudit.Audit(Application, Operator.Login, Operator.Code, objectData.GetType().Name, objectData, OperatorActions.Delete);
            }
            catch (FaultException fault)
            {
                SmartLogger.Print(fault);
                throw fault;
            }
            catch (DatabaseObjectException ex)
            {
                SmartLogger.Print(ex);
                HandleException(ex);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        public void DeleteClientObject(ClientData clientData)
        {
            try
            {
                string queryHql = "SELECT DATA FROM " + clientData.GetType().Name.Replace("Client", "") + " DATA where DATA.Code = " + clientData.Code;
                ObjectData objectData = SmartCadDatabase.SearchObjectData(queryHql);
                if (objectData != null && (objectData.Version == clientData.Version || Application == UserApplicationData.Administration))
                {
                    DeleteObject(objectData);
                }
                else if (objectData != null)
                {
                    StaleObjectStateException staleObjectException =
                            new StaleObjectStateException(objectData.GetType().Name, objectData.Code);
                    throw new DatabaseStaleObjectException(objectData,
                        SmartCadDatabase.GetConcurrencyErrorByObject(objectData), staleObjectException);
                }
                else
                {
                    throw new DatabaseDeletedObjectException();
                }
            }
            catch (FaultException fault)
            {
                SmartLogger.Print(fault);
                throw fault;
            }
            catch (DatabaseObjectException ex)
            {
                SmartLogger.Print(ex);
                HandleException(ex);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        public void DeleteAlarmObject(ClientData clientData)
        {
            try
            {
                string queryHql = "SELECT DATA FROM " + clientData.GetType().Name.Replace("Client", "") + " DATA where DATA.Code = " + clientData.Code;
                ObjectData objectData = SmartCadDatabase.SearchObjectData(queryHql);
                if (objectData != null)
                {
                    DeleteObject(objectData);
                }
                else if (objectData != null)
                {
                    StaleObjectStateException staleObjectException =
                            new StaleObjectStateException(objectData.GetType().Name, objectData.Code);
                    throw new DatabaseStaleObjectException(objectData,
                        SmartCadDatabase.GetConcurrencyErrorByObject(objectData), staleObjectException);
                }
                else
                {
                    throw new DatabaseDeletedObjectException();
                }
            }
            catch (FaultException fault)
            {
                SmartLogger.Print(fault);
                throw fault;
            }
            catch (DatabaseObjectException ex)
            {
                SmartLogger.Print(ex);
                HandleException(ex);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        public void DeleteClientObjectCollection(IList list)
        {
            try
            {
                ArrayList collection = new ArrayList();
                foreach (ClientData clientData in list)
                {
                    string queryHql = "SELECT DATA FROM " + clientData.GetType().Name.Replace("Client", "") + " DATA where DATA.Code = " + clientData.Code;
                    ObjectData objectData = SmartCadDatabase.SearchObjectData(queryHql);
                    if (objectData.Version == clientData.Version)
                    {
                        collection.Add(Conversion.ToObject(clientData, Application));
                    }
                    else
                    {
                        StaleObjectStateException staleObjectException =
                                new StaleObjectStateException(objectData.GetType().Name, objectData.Code);
                        throw new DatabaseStaleObjectException(objectData,
                            SmartCadDatabase.GetConcurrencyErrorByObject(objectData), staleObjectException);
                    }
                    //collection.Add(Conversion.ToObject(clientData, Application));
                }
                SmartCadDatabase.DeleteObjectCollection(collection, null);
            }
            catch (FaultException fault)
            {
                SmartLogger.Print(fault);
                throw fault;
            }
            catch (DatabaseObjectException ex)
            {
                SmartLogger.Print(ex);
                HandleException(ex);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void SaveOrUpdate(ObjectData objectData)
        {
            try
            {
                CheckSecurity();
                if (objectData is UnitData)
                {
                    (objectData as UnitData).DispatchOperator = OperatorDataInfo;
                }
                SmartCadDatabase.SaveOrUpdate(objectData, GetCurrentContext);
                AuditAction(objectData);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        public ICollection InitializeLazy(ObjectData obj, string property)
        {
            try
            {
                //SmartLogger.Print("begin");
                if (Application != null)
                {
                    SmartCadAudit.Audit(Application, Operator.Login, Operator.Code, obj.GetType().Name, property);
                }
                PropertyInfo propInf = obj.GetType().GetProperty(property);
                object list = propInf.GetValue(obj, null);
                SmartCadDatabase.InitializeLazy(obj, list);

                //SmartLogger.Print("end");

                return (ICollection)list;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return null;
            }
        }

        public DateTime GetTime()
        {
            try
            {
                return SmartCadDatabase.GetTimeFromBD();
            }
            catch
            {
                return DateTime.Now;
            }
        }

        public IList RunTasks(IList tasks)
        {
            try
            {
                CheckSecurity();

                //SmartLogger.Print("Begin");

                IList result = new ArrayList(tasks.Count);

                foreach (TaskBase task in tasks)
                {
                    try
                    {
                        IList taskResult = task.Run();

                        result.Add(taskResult);
                    }
                    catch (Exception ex)
                    {
                        result.Add(null);

                        SmartLogger.Print(ex);
                    }
                }

                //SmartLogger.Print("End");

                return result;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return null;
            }
        }

        public void SetOperatorStatus(OperatorStatusClientData status)
        {
            try
            {
                //SmartLogger.Print("Begin");

                CheckSecurity();

                SmartCadAudit.Audit(Application, Operator.Login, Operator.Code, status.GetType().Name, status.FriendlyName);

                ServiceUtil.UpdateSessionStatusHistory(Application, Operator.Login, OperatorStatusConversion.ToObject(status, Application));

                //SmartLogger.Print("End");
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        public double CalculateAmountTimeAvailableByStatusWithPause(string operatorStatusCustomCode)
        {
            try
            {
                return ServiceUtil.CalculateAmountTimeAvailableByStatusWithPause(this.Operator.Code,
                    operatorStatusCustomCode, this.Application.Code);
            }
            catch(Exception ex)
            {
                HandleException(ex);
                return 0;
            }
        }

        public void FinalizeCall(PhoneReportClientData phoneReport)
        {
            try
            {
                CheckSecurity();

                //SmartLogger.Print("Begin");
                phoneReport.FinishedCallTime = SmartCadDatabase.GetTimeFromBD();
                UpdatePhoneReportClient(phoneReport);

                //SmartLogger.Print("End");
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        public void FinalizeCctvIncident(CctvReportClientData cctvReport)
        {
            try
            {
                CheckSecurity();

                UpdateCctvReportClient(cctvReport);

            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        public void FinalizeAlarmIncident(AlarmReportClientData alarmReport)
        {
            try
            {
                CheckSecurity();

                UpdateAlarmReportClient(alarmReport);

            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        public void FinalizeAlarmLprIncident(AlarmLprReportClientData alarmReport)
        {
            try
            {
                CheckSecurity();

                UpdateAlarmLprReportClient(alarmReport);

            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        public void FinalizeTwitterIncident(TwitterReportClientData twitterReport)
        {
            try
            {
                 CheckSecurity();

                UpdateTwitterReportClient(twitterReport);

            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        public object SearchBasicObject(string hql)
        {
            try
            {
                return SmartCadDatabase.SearchBasicObject(hql);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return null;
            }
        }

        public object SearchBasicObjects(string hql)
        {
            object result = null;
            try
            {
                if (Operator != null)
                {
                    SmartCadAudit.Audit(Application, Operator.Login, Operator.Code, hql);
                }
                result = SmartCadDatabase.SearchBasicObjects(hql);
                return result;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return null;
            }
        }

        public object SearchBasicObjects(string hql, bool isHql)
        {
            object result = null;
            try
            {
                if (Operator != null)
                {
                    SmartCadAudit.Audit(Application, Operator.Login, Operator.Code, hql);
                }
                result = SmartCadDatabase.SearchBasicObjects(hql, isHql);
                return result;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return null;
            }
        }

        public ObjectData Refresh(ObjectData objectData)
        {
            try
            {
                CheckSecurity();
                objectData = SmartCadDatabase.RefreshObject(objectData);
                return objectData;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return null;
            }
        }

        public void Ping()
        { }

        public object OperatorScheduleManagerMethod(string methodName, IList args)
        {
            try
            {
                switch (methodName)
                {
                    case "IsWorkingNow":
                        return OperatorScheduleManager.IsWorkingNow(Int32.Parse(args[0].ToString()));
                    case "GetTimeOffByOperator":
                        return OperatorScheduleManager.GetTimeOffByOperator(Int32.Parse(args[0].ToString()));
                    case "GetWorkTimeByOperator":
                        return OperatorScheduleManager.GetWorkTimeByOperator(Int32.Parse(args[0].ToString()));
                    case "GetOperatorsWorkingNowByApp":
                        return OperatorScheduleManager.GetOperatorsWorkingNowByApp(args[0].ToString());
                    case "GetOperatorsWorkingNowByAppByWorkShift":
                        return OperatorScheduleManager.GetOperatorsWorkingNowByAppByWorkShift(args[0].ToString(), Int32.Parse(args[1].ToString()));
                    case "GetOperatorsWorkingNowByDepartament":
                        return OperatorScheduleManager.GetOperatorsWorkingNowByDepartament(Int32.Parse(args[0].ToString()));
                    case "GetOperatorTodaysSchedules":
                        return OperatorScheduleManager.GetOperatorTodaysSchedules(args);
                    case "GetOperatorAssignsTotalTime":
                        if (args.Count == 1)
                            return OperatorScheduleManager.GetOperatorAssignsTotalTime(Int32.Parse(args[0].ToString()));
                        else 
                            return OperatorScheduleManager.GetOperatorAssignsTotalTime(Int32.Parse(args[0].ToString()), (OperatorClientData)args[1]);
                    case "GetCurrentWorkShiftName":
                        string wsv = (string)OperatorScheduleManager.GetCurrentWorkShiftName((int)args[0]);

                        if (wsv != null)
                            return wsv;

                        return null;
                    case "CollideWSRoute":
                        return OperatorScheduleManager.CollideWSRoute((int)args[0], (IList<int>)args[1]);
                    default:
                        return null;
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return null;
            }
        }

        #endregion

        #region Host

        public static ServiceHost Host()
        {
            try
            {
                String address = ApplicationUtil.BuildServiceUrl(
                    "net.tcp",
                    "localhost",
                    SmartCadConfiguration.SERVER_SERVICE_PORT,
                    SmartCadConfiguration.SERVER_SERVICE_ADDRESS);

                NetTcpBinding binding = new NetTcpBinding(SecurityMode.None);

                binding.ReaderQuotas.MaxDepth = 1024;
                binding.ReaderQuotas.MaxArrayLength = 524288;
                binding.MaxReceivedMessageSize = 5242880;
                binding.ReaderQuotas.MaxStringContentLength = 1972864;
                binding.MaxConnections = 1000;
                binding.ListenBacklog = 1000;
                binding.ReceiveTimeout = new TimeSpan(0, 3, 0);

                ServiceHost host = ServiceHostBuilder.ConfigureHost(typeof(ServerService), typeof(IServerService), binding, address);
                return host;
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
            return null;
        }

        #endregion

        #region IServerService Members

        public ConfigurationClientData GetConfiguration()
        {
            try
            {
                ConfigurationClientData configuration = new ConfigurationClientData();

                configuration.NHibernateProperties = SmartCadConfiguration.NHibernateProperties;
                configuration.DatabaseServer = SmartCadConfiguration.DataBaseServer;
                configuration.DatabaseName = SmartCadConfiguration.DataBaseName;
                configuration.MaxUpdateMaps = SmartCadConfiguration.SmartCadSection.ServerElement.SpatialwareElement.MaxUpdates;
                configuration.MaxTimeSleepMaps = SmartCadConfiguration.SmartCadSection.ServerElement.SpatialwareElement.MaxSleepTime;

                //if (SmartCadConfiguration.SmartCadSection.ServerElement.ReportsElement.DatabaseServer == "" &&
                //    SmartCadConfiguration.SmartCadSection.ServerElement.ReportsElement.DatabaseName == "")
                //{
                //    configuration.ReportsDatabaseServer = configuration.DatabaseServer;
                //    configuration.ReportsDatabaseName = configuration.DatabaseName;
                //}
                //else
                //{
                    configuration.ReportsDatabaseServer = SmartCadConfiguration.SmartCadSection.ServerElement.ReportsElement.DatabaseServer;
                    configuration.ReportsDatabaseName = SmartCadConfiguration.SmartCadSection.ServerElement.ReportsElement.DatabaseName;
                //}

                configuration.TelephonyServerType = SmartCadConfiguration.SmartCadSection.ServerElement.TelephonyServerCommunicationElement.ServerType;
                configuration.TelephonyServerProperties = new Dictionary<string, string>();
                foreach (PropertyElement prop in SmartCadConfiguration.SmartCadSection.ServerElement.TelephonyServerCommunicationElement.Properties)
                {
                    configuration.TelephonyServerProperties.Add(prop.Name, prop.Value);
                }

                //string query = "select data from TelephonyConfigurationData data where data.Computer = '" + SessionStateContextExtension.Current.Session.Computer + "'";
                string query = string.Format(SmartCadHqls.GetTelephonyConfigurationByComputer, SessionStateContextExtension.Current.Session.Computer);
                TelephonyConfigurationData telephony = SmartCadDatabase.SearchObjectData(query) as TelephonyConfigurationData;
                if (telephony != null)
                {

                    configuration.Extension = telephony.Extension.ToString();
                    configuration.ExtensionPassword = telephony.Password;
                    configuration.VirtualMode = CheckVirtualCall();
                }

                configuration.VmsDllName = SmartCadConfiguration.SmartCadSection.ServerElement.VmsServerCommunicationElement.dllName;
                configuration.VmsServerIp = SmartCadConfiguration.SmartCadSection.ServerElement.VmsServerCommunicationElement.Server;
                configuration.VmsEventsServerIp = SmartCadConfiguration.SmartCadSection.ServerElement.VmsServerCommunicationElement.EventsServer;
                configuration.VmsLogin = SmartCadConfiguration.SmartCadSection.ServerElement.VmsServerCommunicationElement.Login;
                configuration.VmsPassword = SmartCadConfiguration.SmartCadSection.ServerElement.VmsServerCommunicationElement.Password;
                configuration.VmsPort = SmartCadConfiguration.SmartCadSection.ServerElement.VmsServerCommunicationElement.PortVms;
                configuration.MapDLL = SmartCadConfiguration.SmartCadSection.ServerElement.Map.Dll;
                configuration.MapName = SmartCadConfiguration.SmartCadSection.MapName.Name;
                configuration.MapLat = SmartCadConfiguration.SmartCadSection.DefaultMapLat;
                configuration.MapLon = SmartCadConfiguration.SmartCadSection.DefaultMapLon;

                string language = SmartCadConfiguration.SmartCadSection.CultureElement.CultureLanguageElement.Name+ "-"+
                    SmartCadConfiguration.SmartCadSection.CultureElement.CultureCountryElement.Name;
                configuration.Language = language;
                configuration.DevicePort = SmartCadConfiguration.SmartCadSection.DeviceServer.Port.ToString();
                    
                return configuration;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return null;
            }
        }

		public List<ClientData> GetHistory(DateTime startDate, DateTime endDate, string deviceType, string deviceID, int min, int max)
        {
            try
            {
                List<string> values = ParserServiceClient.Current.GetHistory(startDate, endDate, deviceType, deviceID, min, max);

                if (deviceType == "GPS")
                {
                    return values.ConvertAll<ClientData>(str => new GPSClientData(str));
                }
                else if (deviceType == "Datalogger")
                {
                    //return values.ConvertAll<ClientData>(str => new DataloggerClientData(str));

                    IList dataloggerListDB = SmartCadDatabase.SearchObjects(SmartCadHqls.GetAllDataloggers);
                    List<DataloggerData> dataLoggerList = new List<DataloggerData>();
                    DataloggerClientData dataloggerClientData = new DataloggerClientData();
                    DataloggerData dataloggerData = new DataloggerData();


                    foreach (DataloggerData datalogger in dataloggerListDB)
                    {
                        if (datalogger.CustomCode.ToString() == deviceID)
                        {
                            dataloggerData = datalogger;
                            break;
                        }
                    }

                    dataloggerClientData = DeviceConversion.ConvertToDataloggerClientData(dataloggerData);


                    return values.ConvertAll<ClientData>(str => new DataloggerClientData(str, dataloggerClientData));
                }
            }
            catch 
            {
                return null;
            }
            
            return new List<ClientData>();
        }

        public bool SetGPSOutputs(string idGPS, IList<int> outputs, bool state)
        {
            return ParserServiceClient.Current.SetOutputs(idGPS, outputs, state);
        }

        public bool SetGPSOutputs(string idGPS, IList<int> outputs, bool state, int seconds, int times)
        {
            return ParserServiceClient.Current.SetOutputs(idGPS, outputs, state, seconds, times);
        }

        public bool DevicePing(ServerServiceClient.Device deviceToPing)
        {
            switch (deviceToPing)
            {
                case ServerServiceClient.Device.GPS:
                    if (ParserServiceClient.Current != null)
                        return ParserServiceClient.Current.Ping();
                    else
                        return false;
                case ServerServiceClient.Device.LPR:
                    break;
                default:
                    break;
            }
            return false;
        }

        public IList SearchClientObjects(Type clientDataType)
        {
            try
            {
                ArrayList result = new ArrayList();
                string queryHql = "SELECT DATA FROM " + clientDataType.Name.Replace("Client", "") + " DATA";
                IList list = SmartCadDatabase.SearchObjects(queryHql);
                foreach (ObjectData objData in list)
                {
                    objData.InitializeCollections = false;
                    ClientData clientObject = Conversion.ToClient(objData, Application);
                    if (clientObject != null)
                    {
                        result.Add(clientObject);
                    }
                    else
                    {
                        throw new FaultException(objData.ToString() + " - No conversion found for application: " + Application.FriendlyName);
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return null;
            }
        }


        public bool RemoveInstalledApplication(string application, string macAddress) 
        {
            try
            {
                var hql = "select work from WorkStationData work where work.Name='" + macAddress + "'";
                var hql2 = "select app from InstalledApplicationData app where app.Name='"+application+"'";
            //    var workStation = SmartCadDatabase.SearchObject<WorkStationData>(hql);
            //    var applications = workStation.InstalledApps as List<InstalledApplicationData>;
            //    var Application = SmartCadDatabase.SearchObject<InstalledApplicationData>(hql2);
            //    applications.Remove(Application);
            //    workStation.InstalledApps = applications;
            //    SmartCadDatabase.UpdateObject<WorkStationData>(workStation);
                SmartCadDatabase.RemoveApplicationFromWorkStation(hql, hql2);
                return true;

            }
            catch (Exception ex)
            {
                HandleException(ex);
                return false;
            }
        }

        public bool ActivateApplication(string application, string macAddress) 
        {
            try 
            {
                var hqlInstalledApplication = "Select app From InstalledApplicationData app Where app.Name = '"+application+"'";
                var hqlWorkStation = "Select app From WorkStationData app Where app.Name = '" + macAddress + "'";
                //var Application = SmartCadDatabase.SearchObject<InstalledApplicationData>(hqlInstalledApplication);
                var ApplicationWorkStation = SmartCadDatabase.SearchObject<WorkStationData>(hqlWorkStation);

                if (ApplicationWorkStation == null)
                {   
                    //ApplicationWorkStation = new WorkStationData(){ Name=macAddress,DateCreation=DateTime.Now};
                    //ApplicationWorkStation.InstalledApps = new List<InstalledApplicationData>();
                    //ApplicationWorkStation.InstalledApps.Add(Application);
                    //SmartCadDatabase.SaveObject<WorkStationData>(ApplicationWorkStation);
                    SmartCadDatabase.AddApplicationFromWorkStation(hqlWorkStation, hqlInstalledApplication, macAddress);
                }
                else
                {
                    SmartCadDatabase.UpdateApplicationFromWorkStation(hqlWorkStation, hqlInstalledApplication,macAddress);
                    //if (ApplicationWorkStation.InstalledApps.Contains(Application))
                    //{
                    //    ApplicationWorkStation.InstalledApps.Add(Application);
                    //    SmartCadDatabase.UpdateObject<WorkStationData>(ApplicationWorkStation);
                    //}
                }

                return true;
                            
            }
            catch (Exception ex) 
            {
                HandleException(ex);
                return false;
            }
        
        }

        public IList GetInstalledApplicationsForWorkStation(string macAddress) 
        {
            try
            {
                var hql = "select work.InstalledApps from WorkStationData work where work.Name='"+macAddress+"'";
                var Applications = SmartCadDatabase.SearchObjects(hql);
                var result = new ArrayList();
                foreach (InstalledApplicationData application in Applications)
                {
                    result.Add(application.Name);
                }
                return result;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return null;
            }
        
        }

        public IList GetActiveApplications() 
        {
            try 
            {
               // var hql = SmartCadHqls.ActiveApplicationList;
                var Applications = SmartCadDatabase.SearchObjects(SmartCadHqls.ActiveApplicationList);
                var result = new ArrayList();
                foreach (InstalledApplicationData application in Applications)
                {
                    result.Add(application.Name);
                }
                return result;
            }
            catch (Exception ex) 
            {
                HandleException(ex);
                return null;
            }
        }

		public IList SearchClientObjectsMaxRows(string hql, int startIndex, int maxRows)
		{
			try
			{
				IList list = SmartCadDatabase.SearchObjects(hql, startIndex, maxRows);
				ArrayList result = new ArrayList();
				foreach (ObjectData var in list)
				{
					result.Add(Conversion.ToClient(var, Application));
				}
				return result;
			}
			catch (Exception ex)
			{
				HandleException(ex);
				return null;
			}
		}

		public Dictionary<string, IList> SearchClientObjectsMaxRowsMultiQuery(int startIndex, int maxRows, Dictionary<string, IList> querys)
		{
			try
			{
				return SmartCadDatabase.SearchObjects(startIndex, maxRows, querys, Application,true);
			}
			catch (Exception ex)
			{
				HandleException(ex);
				return null;
			}
		}

		public ClientData SearchClientObject(string hql)
        {
            try
            {
                ObjectData objectData = SmartCadDatabase.SearchObjectData(hql);
                return Conversion.ToClient(objectData, Application);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return null;
            }
        }

        public ClientData SearchClientObjectByBytes(string hql, int offset, int length)
        {
            ClientData result = SearchClientObject(hql);
            
            byte[] maximumData = new byte[1];
            ////DEPRECATED. REMOVE AA
            //if (result is PhoneReportAudioClientData)
            //{
            //    PhoneReportAudioClientData classType = (PhoneReportAudioClientData)result;
            //    if (classType.Audio.Length - offset > length)
            //    {
            //        maximumData = new byte[length];
            //        Buffer.BlockCopy(classType.Audio, offset, maximumData, 0, length);
            //    }
            //    else
            //    {
            //        maximumData = new byte[classType.Audio.Length - offset];
            //        Buffer.BlockCopy(classType.Audio, offset, maximumData, 0, classType.Audio.Length - offset);
            //    }
            //    classType.Audio = maximumData;
            //}
            return result;
        }

        public IList SearchClientObjects(string hql)
        {
            return SearchClientObjects(hql, false);
        }

        public IList SearchClientObjects(string hql, bool InitializeCollections)
        {
            try
            {
                ArrayList result = new ArrayList();
                IList objectList = SmartCadDatabase.SearchObjects(hql);
                foreach (ObjectData data in objectList)
                {
                    if (data != null)
                    {
                        data.InitializeCollections = InitializeCollections;
                        ClientData client = Conversion.ToClient(data, Application);
                        if (client != null)
                        {
                            result.Add(client);
                        }
                        else
                        {
                            throw new FaultException(data.ToString() + " - No conversion found for application: " + Application.FriendlyName);
                        }
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return null;
            }
        }

        public ClientData SearchClientObject(ClientData obj)
        {
            ObjectData objectData = Conversion.ToObject(obj, Application);
            return Conversion.ToClient(objectData, Application);
        }

		public void SendClientData(ClientData objectClient)
		{
			try
			{
				CommittedDataAction action = CommittedDataAction.None;
				SmartCadDatabase.InvokeCommitedChangedEvent(objectClient, action, null);
			}
			catch (Exception ex)
			{
				HandleException(ex);
			}
		}

		public void SendClientDataList(IList list)
		{
			try
			{
				CommittedDataAction action = CommittedDataAction.None;
				SmartCadDatabase.InvokeCommitedChangedEvent(null, action, list);
			}
			catch (Exception ex)
			{
				HandleException(ex);
			}
		}

		public RouteAddressClientData GetLastStop(UnitClientData unit, RouteClientData route)
		{
			RouteAddressClientData stop = null;
			ApplicationPreferenceData distTol = SmartCadDatabase.SearchObject<ApplicationPreferenceData>("Select pref From ApplicationPreferenceData pref Where pref.Name = 'ToleranceForStopApproximation'");

            GPSUnitData point = null;
			try
			{
				point = ParserServiceClient.Current.GetLastStop(unit.IdGPS, unit.CoordinatesDate, route.StopTime * 60, (route.StopTime + route.StopTimeTol) * 60);
			}
			catch
			{

			}
			if (point != null)
			{
				stop = ServiceUtil.GetNearestStopClient(point.lat, point.lon, route.RouteAddress);
                if (GeoCodeCalc.CalcDistance(point.lat, point.lon, stop.Lat, stop.Lon) > double.Parse(distTol.Value) / 1000.0)
					return null;
                stop.StopTime = new TimeSpan(0, (int)point.stop / 60, (int)point.stop % 60);
			}
			return stop;
		}

		public TimeSpan GetAverageStopTime(UnitClientData selectedPosition, WorkShiftRouteClientData wsr)
		{
			string unit = selectedPosition.IdGPS;
			DateTime date = selectedPosition.CoordinatesDate;
			int minTime = wsr.Route.StopTime * 60;
			int maxTime = (wsr.Route.StopTime + wsr.Route.StopTimeTol) * 60;

            try
            {
                return ParserServiceClient.Current.GetAverageStopTime(unit, date, minTime, maxTime);
            }
            catch
            {
                return TimeSpan.Zero;
            }
		}

		public IList GetRunTimePoints(UnitClientData selectedPosition, WorkShiftScheduleVariationClientData schedule)
		{
			string unit = selectedPosition.IdGPS;
			DateTime start = schedule.Start;
			DateTime end = selectedPosition.CoordinatesDate;

            try
            {
                return ParserServiceClient.Current.GetRunTimePoints(unit, start, end);
            }
            catch
            {
                return new ArrayList();
            }
		}

		public double GetAverageSpeed(UnitClientData selectedPosition, WorkShiftScheduleVariationClientData schedule)
		{
			string unit = selectedPosition.IdGPS;
			DateTime start = schedule.Start;
			DateTime end = selectedPosition.CoordinatesDate;

            try
            {
                return ParserServiceClient.Current.GetAverageSpeed(unit, start, end);
            }
            catch
            {
                return 0;
            }
		}

		public void SaveOrUpdateClientData(IList list)
        {
            try
            {
                CheckSecurity();
                ArrayList objectDataList = new ArrayList();
                object data = null;
              
                foreach (ClientData clientData in list)
                {
                    ObjectData objectData = Conversion.ToObject(clientData, Application);
                    if (objectData != null)
                    {
                        objectDataList.Add(objectData);
                    }
                    else
                        throw new Exception("Missing " + clientData.GetType().Name + " in Conversion Class");
                }
              
                SmartCadDatabase.SaveOrUpdateCollection((IList)objectDataList, GetCurrentContext);
            
                //TODO: Crear metodo AuditAction List
                //AuditAction(objectData);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
        }

        public object SaveOrUpdateClientData(ClientData objectClient, bool withReturn)
        {
            try
            {
               ObjectData objectData = null;
                objectData = Conversion.ToObject(objectClient, Application);
				if (objectClient is UnitClientData)
				{
					((UnitData)objectData).DispatchOperator = OperatorDataInfo;
				}
				if (objectData == null)
                {
                    throw new FaultException("No conversion for this clientType: " + objectClient.GetType().Name);
                }
                else if (objectData != null)
                {
					objectData = SmartCadDatabase.SaveOrUpdate(objectData, GetCurrentContext);
					if (objectData == null)
					{
						throw new FaultException("No conversion for this clientType: " + objectClient.GetType().Name);
					}
                    if (withReturn == true)
                    {
                        return Conversion.ToClient(objectData, Application);
                    }
                    else
                        return null;
                }
				return null;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return null;
            }
        }

        public void UpdatePhoneReportClient(PhoneReportClientData phoneReportClient)
        {
            try
            {
                bool foundChange = false;
                PhoneReportData SavedPhoneReport = new PhoneReportData();
                SavedPhoneReport.CustomCode = phoneReportClient.CustomCode;
                if (phoneReportClient.Answers != null)
                {
                    SavedPhoneReport = SmartCadDatabase.SearchObjects("select phoneReport from PhoneReportData phoneReport left join fetch phoneReport.SetAnswers where phoneReport.CustomCode = '" + phoneReportClient.CustomCode + "'")[0] as PhoneReportData;
                }
                else
                {
                    SavedPhoneReport = SmartCadDatabase.SearchObjects("select phoneReport from PhoneReportData phoneReport where phoneReport.CustomCode = '" + phoneReportClient.CustomCode + "'")[0] as PhoneReportData;
                }

                if (phoneReportClient.FinishedCallTime != DateTime.MinValue)
                {
                    SavedPhoneReport.FinishedReportTime = phoneReportClient.FinishedCallTime;

                    if (SavedPhoneReport.RegisteredCallTime.HasValue == true &&
                        SavedPhoneReport.FinishedReportTime < SavedPhoneReport.RegisteredCallTime)
                        SavedPhoneReport.FinishedReportTime = SavedPhoneReport.RegisteredCallTime.Value.AddSeconds(1);
                }

                if (phoneReportClient.HangedUpCallTime != DateTime.MinValue)
                {
                    SavedPhoneReport.HangedUpCallTime = phoneReportClient.HangedUpCallTime;

                    if (SavedPhoneReport.HangedUpCallTime.HasValue == true &&
                        SavedPhoneReport.FinishedReportTime < SavedPhoneReport.HangedUpCallTime)
                        SavedPhoneReport.HangedUpCallTime = SavedPhoneReport.FinishedReportTime.Value.AddSeconds(-1);
                }

                if (phoneReportClient.Answers != null)
                {
                    //SmartCadDatabase.InitializeLazy(SavedPhoneReport, SavedPhoneReport.SetAnswers);
                    IList answerList = new ArrayList(SavedPhoneReport.SetAnswers);
                    foreach (ReportAnswerClientData pracd in phoneReportClient.Answers)
                    {
                        bool found = false;
                        for (int index = 0; found == false && index < answerList.Count; index++)
                        {
                            PhoneReportAnswerData prad = answerList[index] as PhoneReportAnswerData;
                            if (pracd.QuestionCode == prad.Question.Code)
                            {
                                found = true;

                            }
                        }

                        if (found == false)
                        {
                            foundChange = true;
                            PhoneReportAnswerData phoneReportAnswerData = PhoneReportAnswerConversion.ToObject(pracd);
                            if (phoneReportAnswerData.Question != null)
                            {
                                phoneReportAnswerData.PhoneReport = SavedPhoneReport;
                                SavedPhoneReport.SetAnswers.Add(phoneReportAnswerData);
                            }
                        }
                    }
                }
                if (foundChange)
                {
                    SaveOrUpdate(SavedPhoneReport);
                }
                else
                {
                    if (SavedPhoneReport.HangedUpCallTime.HasValue && SavedPhoneReport.FinishedReportTime.HasValue)
                    {
                        SmartCadDatabase.UpdateSqlWithOutValidator(
                           SmartCadSQL.GetCustomSQL(
                               SmartCadSQL.UpdateHangedUpCallAndFinishedReportTime,
                               SavedPhoneReport.Code,
                               ApplicationUtil.GetDataBaseFormattedDate(SavedPhoneReport.HangedUpCallTime.Value),
                               ApplicationUtil.GetDataBaseFormattedDate(SavedPhoneReport.FinishedReportTime.Value)
                           )
                       );
                    }
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        public void UpdateCctvReportClient(CctvReportClientData cctvReportClient)
        {
            try
            {
                bool foundChange = false;
                CctvReportData SavedCctvReport = new CctvReportData();
                SavedCctvReport.CustomCode = cctvReportClient.CustomCode;
                if (cctvReportClient.Answers != null)
                {
                    SavedCctvReport = SmartCadDatabase.SearchObjects("select cctvReport from CctvReportData cctvReport left join fetch cctvReport.SetAnswers where cctvReport.CustomCode = '" + cctvReportClient.CustomCode + "'")[0] as CctvReportData;
                }
                else
                {
                    SavedCctvReport = SmartCadDatabase.SearchObjects("select cctvReport from cctvReportData cctvReport where cctvReport.CustomCode = '" + cctvReportClient.CustomCode + "'")[0] as CctvReportData;
                }

                //if (cctvReportClient.StatrtIncidentCameraTime != DateTime.MinValue)
                //    SavedCctvReport.StatrtIncidentCameraTime = cctvReportClient.StatrtIncidentCameraTime;

                //if (cctvReportClient.FinishedIncidentCameraTime != DateTime.MinValue)
                //    SavedCctvReport.FinishedIncidentCameraTime = cctvReportClient.FinishedIncidentCameraTime;

                if (cctvReportClient.Answers != null)
                {
                    IList answerList = new ArrayList(SavedCctvReport.SetAnswers);
                    foreach (ReportAnswerClientData pracd in cctvReportClient.Answers)
                    {
                        bool found = false;
                        for (int index = 0; found == false && index < answerList.Count; index++)
                        {
                            CctvReportAnswerData prad = answerList[index] as CctvReportAnswerData;
                            if (pracd.QuestionCode == prad.Question.Code)
                            {
                                found = true;

                            }
                        }

                        if (found == false)
                        {
                            foundChange = true;
                            CctvReportAnswerData cctvReportAnswerData = CctvReportAnswerConversion.ToObject(pracd);
                            cctvReportAnswerData.CctvReport = SavedCctvReport;
                            SavedCctvReport.SetAnswers.Add(cctvReportAnswerData);
                        }
                    }
                }
                if (foundChange)
                {
                    SaveOrUpdate(SavedCctvReport);
                }
                else
                {
                    //if (SavedCctvReport.StatrtIncidentCameraTime.HasValue && 
                    //    SavedCctvReport.FinishedIncidentCameraTime.HasValue)
                    //{
                    //    SmartCadDatabase.UpdateSqlWithOutValidator(
                    //       SmartCadSQL.GetCustomSQL(
                    //           SmartCadSQL.UpdateHangedUpCallAndFinishedReportTime,
                    //           SavedPhoneReport.Code,
                    //           ApplicationUtil.GetDataBaseFormattedDate(SavedPhoneReport.HangedUpCallTime.Value),
                    //           ApplicationUtil.GetDataBaseFormattedDate(SavedPhoneReport.FinishedReportTime.Value)
                    //       )
                    //   );
                    //}
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

       

        public void UpdateAlarmReportClient(AlarmReportClientData alarmReportClient)
        {
            try
            {
                bool foundChange = false;
                AlarmReportData SavedAlarmReport = new AlarmReportData();
                SavedAlarmReport.CustomCode = alarmReportClient.CustomCode;
                if (alarmReportClient.Answers != null)
                {
                    SavedAlarmReport = SmartCadDatabase.SearchObjects("select alarmReport from AlarmReportData alarmReport left join fetch alarmReport.SetAnswers where alarmReport.CustomCode = '" + alarmReportClient.CustomCode + "'")[0] as AlarmReportData;
                }
                else
                {
                    SavedAlarmReport = SmartCadDatabase.SearchObjects("select alarmReport from AlarmReportData alarmReport where alarmReport.CustomCode = '" + alarmReportClient.CustomCode + "'")[0] as AlarmReportData;
                }

                if (alarmReportClient.Answers != null)
                {
                    IList answerList = new ArrayList(SavedAlarmReport.SetAnswers);
                    foreach (ReportAnswerClientData pracd in alarmReportClient.Answers)
                    {
                        bool found = false;
                        for (int index = 0; found == false && index < answerList.Count; index++)
                        {
                            AlarmReportAnswerData prad = answerList[index] as AlarmReportAnswerData;
                            if (pracd.QuestionCode == prad.Question.Code)
                            {
                                found = true;

                            }
                        }

                        if (found == false)
                        {
                            foundChange = true;
                            AlarmReportAnswerData alarmReportAnswerData = AlarmReportAnswerConversion.ToObject(pracd);
                            alarmReportAnswerData.AlarmReport = SavedAlarmReport;
                            SavedAlarmReport.SetAnswers.Add(alarmReportAnswerData);
                        }
                    }
                }
                if (foundChange)
                {
                    SaveOrUpdate(SavedAlarmReport);
                }
                else
                {                    
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }


        public void UpdateAlarmLprReportClient(AlarmLprReportClientData alarmReportClient)
        {
            try
            {
                bool foundChange = false;
                AlarmLprReportData SavedAlarmReport = new AlarmLprReportData();
                SavedAlarmReport.CustomCode = alarmReportClient.CustomCode;
                if (alarmReportClient.Answers != null)
                {
                    SavedAlarmReport = SmartCadDatabase.SearchObjects("select alarmReport from AlarmLprReportData alarmReport left join fetch alarmReport.SetAnswers where alarmReport.CustomCode = '" + alarmReportClient.CustomCode + "'")[0] as AlarmLprReportData;
                }
                else
                {
                    SavedAlarmReport = SmartCadDatabase.SearchObjects("select alarmReport from AlarmLprReportData alarmReport where alarmReport.CustomCode = '" + alarmReportClient.CustomCode + "'")[0] as AlarmLprReportData;
                }

                if (alarmReportClient.FinishedTime != DateTime.MinValue)
                    SavedAlarmReport.FinishedIncidentLprTime = alarmReportClient.FinishedTime;

                if (alarmReportClient.Answers != null)
                {
                    IList answerList = new ArrayList(SavedAlarmReport.SetAnswers);
                    foreach (ReportAnswerClientData pracd in alarmReportClient.Answers)
                    {
                        bool found = false;
                        for (int index = 0; found == false && index < answerList.Count; index++)
                        {
                            AlarmLprReportAnswerData prad = answerList[index] as AlarmLprReportAnswerData;
                            if (pracd.QuestionCode == prad.Question.Code)
                            {
                                found = true;

                            }
                        }

                        if (found == false)
                        {
                            foundChange = true;
                            AlarmLprReportAnswerData alarmReportAnswerData = AlarmLprReportAnswerConversion.ToObject(pracd);
                            alarmReportAnswerData.AlarmLprReport = SavedAlarmReport;
                            SavedAlarmReport.SetAnswers.Add(alarmReportAnswerData);
                        }
                    }
                }
                if (foundChange)
                {
                    SaveOrUpdate(SavedAlarmReport);
                }
                else
                {
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        public void UpdateTwitterReportClient(TwitterReportClientData twitterReportClient)
        {
            try
            {
                bool foundChange = false;
                TwitterReportData SavedAlarmReport = new TwitterReportData();
                SavedAlarmReport.CustomCode = twitterReportClient.CustomCode;
                if (twitterReportClient.Answers != null)
                {
                    SavedAlarmReport = SmartCadDatabase.SearchObjects("select alarmReport from TwitterReportData alarmReport left join fetch alarmReport.SetAnswers where alarmReport.CustomCode = '" + twitterReportClient.CustomCode + "'")[0] as TwitterReportData;
                }
                else
                {
                    SavedAlarmReport = SmartCadDatabase.SearchObjects("select alarmReport from TwitterReportData alarmReport where alarmReport.CustomCode = '" + twitterReportClient.CustomCode + "'")[0] as TwitterReportData;
                }

                if (twitterReportClient.FinishedTime != DateTime.MinValue)
                    SavedAlarmReport.FinishedIncidentTime = twitterReportClient.FinishedTime;

                if (twitterReportClient.Answers != null)
                {
                    IList answerList = new ArrayList(SavedAlarmReport.SetAnswers);
                    foreach (ReportAnswerClientData pracd in twitterReportClient.Answers)
                    {
                        bool found = false;
                        for (int index = 0; found == false && index < answerList.Count; index++)
                        {
                            TwitterReportAnswerData prad = answerList[index] as TwitterReportAnswerData;
                            if (pracd.QuestionCode == prad.Question.Code)
                            {
                                found = true;

                            }
                        }

                        if (found == false)
                        {
                            foundChange = true;
                            TwitterReportAnswerData alarmReportAnswerData = TwitterReportAnswerConversion.ToObject(pracd);
                            alarmReportAnswerData.TwitterReport = SavedAlarmReport;
                            SavedAlarmReport.SetAnswers.Add(alarmReportAnswerData);
                        }
                    }
                }
                if (foundChange)
                {
                    SaveOrUpdate(SavedAlarmReport);
                }
                else
                {
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        public ClientData RefreshClient(ClientData clientData)
        {
            return RefreshClient(clientData, false);
        }

        public ClientData RefreshClient(ClientData clientData, bool initializeCollections)
        {
            try
            {
                CheckSecurity();

                if (clientData != null)
                {
                    ObjectData objectData = (ObjectData)ServiceUtil.CreateInstanceFromData("Model."+clientData.GetType().Name.Replace("Client", ""));

                    objectData.Code = clientData.Code;
                    objectData = SmartCadDatabase.RefreshObject(objectData);

                    if (objectData != null)
                    {
                        objectData.InitializeCollections = initializeCollections;
                        return Conversion.ToClient(objectData, Application);
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return null;
            }
        }

        public void AssignIncidentNotificationToManualSupervisor(IncidentNotificationClientData notification)
        {
            try
            {
                IncidentNotificationData incidentNotification = new IncidentNotificationData();
                incidentNotification.Code = notification.Code;
                incidentNotification = SmartCadDatabase.RefreshObject(incidentNotification) as IncidentNotificationData;
                incidentNotification.Version = notification.Version;
                ServiceUtil.AssignIncidentNotificationToManualSupervisor(incidentNotification, OperatorDataInfo);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        public bool AssignIncidentNotificationToOperator(IncidentNotificationClientData selectedNotification, OperatorClientData selectedOperator)
        {
            bool result = false;
            try
            {
                IncidentNotificationData incidentNotification = new IncidentNotificationData();
                incidentNotification.Code = selectedNotification.Code;
                incidentNotification = SmartCadDatabase.RefreshObject(incidentNotification) as IncidentNotificationData;
                incidentNotification.Version = selectedNotification.Version;
                OperatorData oper = new OperatorData();
                oper.Code = selectedOperator.Code;
                oper = SmartCadDatabase.RefreshObject(oper) as OperatorData;
                ServiceUtil.AssignIncidentNotificationToOperator(incidentNotification, oper, OperatorDataInfo);
                result = true;
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
            return result;
        }

        public IList GetSupervisedOperators(string applicationName, int departmentCode)
        {
            IList result = new ArrayList();
            try
            {
                bool generalSupervisor = ServiceUtil.CheckGeneralSupervisor(this.Operator.Role);
                bool checkLoginMode = ServiceUtil.GetLogInMode();
                IList operators = new ArrayList();
                if (generalSupervisor == true)
                {                    
                    if (applicationName == UserApplicationData.FirstLevel.Name)
                    {
                        if (checkLoginMode == true)
                        {
                            operators = OperatorScheduleManager.GetOperatorsWorkingNowByApp(applicationName);
                        }
                        else
                        {
                            operators = SmartCadDatabase.SearchObjects(
                                SmartCadHqls.GetCustomHql(
                                SmartCadHqls.GetOperatorsByApplication, UserApplicationData.FirstLevel.Name));
                        }
                    }
                    else if (applicationName == UserApplicationData.Dispatch.Name)
                    {
                        if (checkLoginMode == true)
                        {
                            operators = OperatorScheduleManager.GetOperatorsWorkingNowByDepartament(departmentCode);
                        }
                        else
                        {
                            operators = SmartCadDatabase.SearchObjects(
                                SmartCadHqls.GetCustomHql(
                                SmartCadHqls.GetOperatorsByApplication, UserApplicationData.Dispatch.Name));
                        }
                    }
                }
                else
                {
                    if (checkLoginMode == true)
                    {
                        operators = OperatorScheduleManager.GetOperatorsWorkingNowBySupervisor(this.Operator.Code);
                    }
                    else
                    {
                        operators = new ArrayList();
                    }
                }
                foreach (OperatorData oper in operators)
                {
                    SmartCadDatabase.InitializeLazy(oper, oper.DepartmentTypes);
                    if (applicationName == UserApplicationData.FirstLevel.Name ||
                        (applicationName == UserApplicationData.Dispatch.Name &&
                         ContainsDepartment(oper.DepartmentTypes, departmentCode)))
                    {
                        OperatorClientData operatorClient = OperatorConversion.ToClient(oper, this.Application);
                        if (operatorClient.IsSupervisor == false)
                        {
                            result.Add(operatorClient);
                        }
                    }
                }

                int length = result.Count;
                for (int i = 0; i < length; i++)
                {
                    OperatorClientData oper = (OperatorClientData)result[i];

                    IList lastSessionHistories = SmartCadDatabase.SearchObjects(
                        SmartCadHqls.GetCustomHql(SmartCadHqls.GetLastSessionHistory, oper.Code, applicationName));
                    SessionHistoryData sh = GetLastSessionHistory(lastSessionHistories, applicationName);
                    oper.LastSessions = new ArrayList();
                    if (sh != null)
                    {
                        oper.LastSessions.Add(SessionHistoryConversion.ToClient(sh, this.Application));
                    }
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
            return result;
        }

        public IList GetOperatorsWorkingNowByAppByWorkShift(string applicationName, int wsCode)
        {
            IList result = new ArrayList();
            try
            {
                IList operatorList = OperatorScheduleManager.GetOperatorsWorkingNowByAppByWorkShift(applicationName, wsCode);
                foreach (OperatorData operData in operatorList)
                {
                    OperatorClientData operClient = OperatorConversion.ToClient(operData, this.Application);
                    IList lastSessionHistories = SmartCadDatabase.SearchObjects(
                        SmartCadHqls.GetCustomHql(SmartCadHqls.GetLastSessionHistory, operClient.Code, applicationName));
                    SessionHistoryData sh = GetLastSessionHistory(lastSessionHistories, applicationName);
                    operClient.LastSessions = new ArrayList();
                    if (sh != null)
                    {
                        operClient.LastSessions.Add(SessionHistoryConversion.ToClient(sh, this.Application));
                    }
                    result.Add(operClient);
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
            return result;
        }

        private bool ContainsDepartment(IList departmentList, int departmentCode)
        {
            bool found = false;
            for (int i = 0; i < departmentList.Count && !found; i++)
            {
                DepartmentTypeData dept = departmentList[i] as DepartmentTypeData;
                if (dept.Code == departmentCode)
                {
                    found = true;
                }
            }
            return found;
        }

        private SessionHistoryData GetLastSessionHistory(IList lastSessionHistories, string applicationName)
        {
            SessionHistoryData sessionHistory = null;
            foreach (SessionHistoryData sess in lastSessionHistories)
            {
                if (sess.UserApplication.Name == applicationName)
                {
                    sessionHistory = sess;
                    break;
                }
            }
            return sessionHistory;
        }

        #endregion

        private void CheckSecurity()
        {
            long exists = (long)SmartCadDatabase.SearchBasicObject(
                            SmartCadHqls.GetCustomHql(
                            SmartCadHqls.GetUserSessionCountByApplication, Application.Name, Operator.Login));
            if (exists == 0)
            {
                string login = "login en null";
                if (this.Operator != null && this.Operator.Login != null)
                {
                    login = this.Operator.Login;
                }
                throw new SecurityException(ResourceLoader.GetString2("InvalidLogin", login));
            }
        }


        #region Languages
        public byte[] CheckLanguages(string language)
        {
            string filePath = SmartCadConfiguration.LanguageDirectory;//@"C:\Users\Posma-developer\Documents\Posma\server\SmartCadServer\Smartmatic.SmartCad.Resources\bin\Debug";
            string[] fileEntries = Directory.GetDirectories(filePath);
            string[] fileDll;
            string pathDll="";
            byte[] dll=null;
            List<string> listDirectories = new List<string>();
            foreach (var item in fileEntries)
            {
                var finalFolder = item.Split('\\');
                if (((string)finalFolder.GetValue(finalFolder.Length - 1)).Equals(language))
                {
                    fileDll = Directory.GetFiles(item);

                            pathDll = item + "\\Smartmatic.SmartCad.Resources.resources.dll";
                            FileStream stream = new FileStream(pathDll, FileMode.Open, FileAccess.Read);
                            BinaryReader reader = new BinaryReader(stream);
                            dll = reader.ReadBytes((int)stream.Length);
                            reader.Close();
                            stream.Close();
                            return dll;

                }
            }
            return dll;
        }
        #endregion



        public bool CheckGeneralSupervisor(int roleCode)
		{
			try
			{
				return ServiceUtil.CheckGeneralSupervisor(roleCode);
			}
			catch (Exception ex)
			{
				HandleException(ex);
				return false;
			}
		}

		public bool CheckSupervisor(int roleCode)
		{
			try
			{
				return ServiceUtil.CheckSupervisor(roleCode);
			}
			catch (Exception ex)
			{
				HandleException(ex);
				return false;
			}
		}

		public bool CheckApplicationSupervisor(int roleCode, string app)
        {
            try
            {
                return ServiceUtil.CheckApplicationSupervisor(roleCode,app);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return false;
            }
        }


        
        public bool CheckVirtualCall() 
        {
            try 
            {
                var dbResult = SmartCadDatabase.SearchObject<ApplicationPreferenceData>("select preference from ApplicationPreferenceData preference where preference.Name='VirtualCallActive'");
                var result = bool.Parse(dbResult.Value.ToLower());
                return result;
            }
            catch (Exception ex) 
            {
                HandleException(ex);
                return false;
            }
        }

        public bool CheckFirstLevelOperator(int roleCode)
        {
            try
            {
                return ServiceUtil.CheckFirstLevelOperator(roleCode);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return false;
            }
        }

        public bool CheckDispatchOperator(int roleCode)
        {
            try
            {
                return ServiceUtil.CheckDispatchOperator(roleCode);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return false;
            }
        }

        public bool CheckUserAccess(int roleCode, string accessName)
        {
            try
            {
                return ServiceUtil.CheckUserAccess(roleCode, accessName);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return false;
            }
        }



        public IList<string> GetIncidentCodeInRatio(GeoPoint point, double ratio, DistanceUnit distanceUnit)
        {
            return ServiceUtil.GetIncidentCodeInRatio(point, ratio, distanceUnit);
        }

        private void HandleException(Exception ex)
        {
            if (ex.Message != ResourceLoader.GetString2("La solicitud ya esta asignada"))
                SmartLogger.Print(ex);
            throw ApplicationUtil.NewFaultException(ex);
        }

		public DepartmentStationClientData GetDepartmentStationForIncidentNotification(IncidentNotificationClientData notification)
		{
			DepartmentStationData station = ServiceUtil.GetDepartmentStationForThisPoint(notification.IncidentAddress, notification.DepartmentType.Code);
			return station != null ? DepartmentStationConversion.ToClient(station, UserApplicationData.Dispatch) : null;
		}

        /// <summary>
        /// Sets the supervised application name into the current session.
        /// This method is used for supervisor.
        /// </summary>
        /// <param name="supervisedAppName">Supervised application name</param>
        public void SetSupervisedApplicationName(string supervisedAppName)
        {
            try
            {
                CheckSecurity();
                SessionHistoryData session = ServiceUtil.GetCurrentSessionHistory(Application.Name, Operator.Login);
                session.SupervisedApplication = supervisedAppName;

                SaveOrUpdate(session);
                helper.AddOrUpdateServerServiceLogIn(session);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }
    }
}

