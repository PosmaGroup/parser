﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ComponentModel;
using System.Collections;
using System.Threading;
using SmartCadCore.Core;
using SmartCadCore.Common;

namespace Smartmatic.SmartCad.Service
{
    public class ParserServiceClient : ClientBase<IParserService>, IParserService
    {

        
        #region Constructors

        private ParserServiceClient(Binding binding, EndpointAddress remoteAddress)
            : base(binding, remoteAddress)
        {
        }

        
        #endregion

        private static ParserServiceClient current = null;

        public static ParserServiceClient Current {
            get
            {
                return (current == null || current.State == CommunicationState.Faulted) ? GetParserService() : current;
            }
        }

        private static ParserServiceClient GetParserService()
        {
            if (string.IsNullOrEmpty(SmartCadConfiguration.SmartCadSection.AvlClient.IP) == true)
                return null;

            string url = ApplicationUtil.BuildServiceUrl(
                "net.tcp",
                SmartCadConfiguration.SmartCadSection.AvlClient.IP,
                SmartCadConfiguration.SmartCadSection.AvlClient.Port,
                "Parser");

            EndpointAddress address = new EndpointAddress(url);

            NetTcpBinding binding = new NetTcpBinding(SecurityMode.None);
			
			binding.ReaderQuotas.MaxDepth = 1024;
			binding.ReaderQuotas.MaxArrayLength = 524288;
			binding.MaxReceivedMessageSize = 5242880;
			binding.ReaderQuotas.MaxStringContentLength = 1972864;

            ParserServiceClient client = new ParserServiceClient(binding, address);

            foreach (OperationDescription op in client.Endpoint.Contract.Operations)
            {
                DataContractSerializerOperationBehavior dataContractBehavior =
                   op.Behaviors.Remove<DataContractSerializerOperationBehavior>()
                        as DataContractSerializerOperationBehavior;

                op.Behaviors.Add(new NetDataContractSerializerOperationBehavior(op));
            }

            client.InnerChannel.OperationTimeout = TimeSpan.FromMinutes(3);

            current = client;

            return client;
        }

        #region IParserService Members

        public bool SetOutputs(string idGPS, IList<int> outputs, bool state)
        {
            return base.Channel.SetOutputs(idGPS, outputs, state);
        }

        public bool SetOutputs(string idGPS, IList<int> outputs, bool state, int seconds, int times)
        {
            return base.Channel.SetOutputs(idGPS, outputs, state, seconds, times);
        }

        public bool SendCommand(string idGPS, string command)
        {
            return base.Channel.SendCommand(idGPS,command);
        }

		public List<string> GetHistory(DateTime startDate, DateTime endDate, string deviceType, string deviceID, int min, int max)
        {
            return base.Channel.GetHistory(startDate, endDate, deviceType, deviceID, min, max);
        }

        public GPSUnitData GetLastStop(string unit, DateTime date, int minTime, int maxTime)
		{
			return base.Channel.GetLastStop(unit, date, minTime, maxTime);
		}

		public TimeSpan GetAverageStopTime(string unit, DateTime date, int minTime, int maxTime)
		{
			return base.Channel.GetAverageStopTime(unit,date,minTime,maxTime);
		}

        public GPSUnitData CheckPointForStop(GPSUnitData gpsUnit, int minTime, int maxTime)
		{
			return base.Channel.CheckPointForStop(gpsUnit, minTime, maxTime);
		}

		public IList GetRunTimePoints(string unit, DateTime start, DateTime end)
		{
			return base.Channel.GetRunTimePoints(unit, start, end);
		}

		public double GetAverageSpeed(string unit, DateTime start, DateTime end)
		{
			return base.Channel.GetAverageSpeed(unit, start, end);
		}

		public bool Ping()
        {
            for (int i = 0; i < 3; i++)
            {
                try
                {
                    return base.Channel.Ping();
                }
                catch (Exception e) { SmartLogger.Print(e); }
                Thread.Sleep(100);
            }
            return false;
        }

        #endregion		
	}
}
