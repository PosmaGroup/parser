using System;
using System.Collections.Generic;
using System.Text;

using System.ServiceModel;

using System.Web.Services.Description;

using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using SmartCadCore.Core;
using SmartCadCore.Model;

namespace Smartmatic.SmartCad.Service
{
    public class ClientService : ClientBase<IApplicationService>, IApplicationService
    {
        private ClientService(System.ServiceModel.Channels.Binding binding, EndpointAddress remoteAddress)
            : base(binding, remoteAddress)
        {
        }

        public static ClientService GetClientService(SessionHistoryData data)
        {
            string url = ApplicationUtil.BuildServiceUrl(
                "net.tcp",
                data.ComputerName,
                UserApplicationData.GetUserApplicationPortByName(data.UserApplication.Name),
                SmartCadConfiguration.CLIENT_SERVICE_ADDRESS);

            EndpointAddress address = new EndpointAddress(url);

            CustomBinding binding = BindingBuilder.GetClientNetTcpBinding();

            ClientService client = new ClientService(binding, address);

            foreach (OperationDescription op in client.Endpoint.Contract.Operations)
            {
                DataContractSerializerOperationBehavior dataContractBehavior =
                   op.Behaviors.Remove<DataContractSerializerOperationBehavior>()
                        as DataContractSerializerOperationBehavior;

                op.Behaviors.Add(new NetDataContractSerializerOperationBehavior(op));
            }

            client.InnerChannel.OperationTimeout = TimeSpan.FromMinutes(10);

            client.OnPing();

            return client;
        }

        #region IServerServiceCallback Members

        public void OnPing()
        {
            base.Channel.OnPing();
        }

        public void OnCommittedChanges(object sender, CommittedObjectDataCollectionEventArgs committedObjectDataCollectionEventArgs)
        {
            base.Channel.OnCommittedChanges(sender, committedObjectDataCollectionEventArgs);
        }

        public void OnGisAction(GisActionEventArgs e)
        {
            base.Channel.OnGisAction(e);
        }

        #endregion
    }
}
