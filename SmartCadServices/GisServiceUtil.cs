﻿using SmartCadCore.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadServices
{
    public class GisServiceUtil
    {
        /*
        public static IList Distance(GeoPoint centralPoint, IList points, DistanceUnit distanceUnit)
        {
            IList result = new ArrayList(points.Count);

            CoordSys coorSys = Session.Current.CoordSysFactory.CreateLongLat(DatumID.WGS84);

            DPoint centralDPoint = new DPoint(centralPoint.Lon, centralPoint.Lat);

            foreach (GeoPoint point in points)
            {
                double distance = 0;
                if (point.Lat != 0 && point.Lon != 0)
                {
                    DPoint dpoint = new DPoint(point.Lon, point.Lat);
                    distance = coorSys.Distance(DistanceType.Cartesian, (MapInfo.Geometry.DistanceUnit)distanceUnit, centralDPoint, dpoint);
                }
                else
                    distance = 0;
                result.Add(distance);
            }

            return result;
        }  */

        private static readonly double EARTH_RATIO = 6371; //In Km
        private static readonly double DEG_TO_RAD = 2 * Math.PI / 360;
        private static readonly double RAD_TO_DEG = 360 / (2 * Math.PI);
        public static readonly double DISTANCE_PER_DEGREE = EARTH_RATIO * 2 * Math.PI / 360;

        /// <summary>
        /// Calculates the distances between a given point(incident coordinates) and many other points(unit coordinates).
        /// This method can be used to obtain recommended units, taking in consideration their distances to 
        /// the incident.
        /// </summary>
        /// <param name="point"></param>
        /// <param name="unitPoints"></param>
        /// <returns></returns>
        public static IList<double> GetDistances(GeoPoint point, IList<GeoPoint> unitPoints)
        {
            IList<double> result = new List<double>();
            foreach (GeoPoint pu in unitPoints)
            {
                double res = CalculateDistance(point, pu);
                result.Add(res);
            }
            return result;
        }

        /// <summary>
        /// Calculates the distance between two points in the earth surface
        /// </summary>
        /// <param name="incidentPoint"></param>
        /// <param name="unitPoint"></param>
        /// <returns></returns>
        public static double CalculateDistance(GeoPoint incidentPoint, GeoPoint unitPoint)
        {
            double distance = 0;
            if (incidentPoint.Lat != 0 && incidentPoint.Lon != 0 &&
                unitPoint.Lat != 0 && unitPoint.Lon != 0)
            {
                distance = DISTANCE_PER_DEGREE * Math.Acos(
                    Math.Sin(incidentPoint.Lat * DEG_TO_RAD) * Math.Sin(unitPoint.Lat * DEG_TO_RAD) +
                    Math.Cos(incidentPoint.Lat * DEG_TO_RAD) * Math.Cos(unitPoint.Lat * DEG_TO_RAD) *
                    Math.Cos((incidentPoint.Lon - unitPoint.Lon) * DEG_TO_RAD)) * RAD_TO_DEG;
            }
            return distance;
        }
    }
}
