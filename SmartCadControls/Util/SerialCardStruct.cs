using System.ComponentModel;

namespace SmartCadControls.Util
{
	/// <summary>
	/// Summary description for SerialCardStruct.
	/// </summary>
	[TypeConverter(typeof (SerialCardStructConverter))]
	public class SerialCardStruct
	{
		private byte byte1;
		private byte byte2;
		private byte byte3;
		private byte byte4;
		private byte byte5;
		private byte byte6;

		public byte Byte1
		{
			get { return byte1; }
			set { byte1 = value; }
		}

		public byte Byte2
		{
			get { return byte2; }
			set { byte2 = value; }
		}

		public byte Byte3
		{
			get { return byte3; }
			set { byte3 = value; }
		}

		public byte Byte4
		{
			get { return byte4; }
			set { byte4 = value; }
		}

		public byte Byte5
		{
			get { return byte5; }
			set { byte5 = value; }
		}

		public byte Byte6
		{
			get { return byte6; }
			set { byte6 = value; }
		}

		public bool IsEmpty()
		{
			return ((byte1 == 0) && (byte2 == 0) && (byte3 == 0) && (byte4 == 0) && (byte5 == 0) && (byte6 == 0));
		}

		public ulong Serial
		{
			get
			{
				ulong res = (((ulong) Byte6) << 40);
				res += (((ulong) Byte5) << 32);
				res += (((ulong) Byte4) << 24);
				res += (((ulong) Byte3) << 16);
				res += (((ulong) Byte2) << 8);
				res += (((ulong) Byte1));
				return res;
			}
			set
			{
				Byte1 = (byte) (value & 255);
				Byte2 = (byte) ((value >> 8) & 255);
				Byte3 = (byte) ((value >> 16) & 255);
				Byte4 = (byte) ((value >> 24) & 255);
				Byte5 = (byte) ((value >> 32) & 255);
				Byte6 = (byte) ((value >> 40) & 255);
			}
		}

		public static SerialCardStruct Parse(string s)
		{
			string[] v = s.Split(new char[] {'.'});
			return new SerialCardStruct(byte.Parse(v[0]), byte.Parse(v[1]), byte.Parse(v[2]), byte.Parse(v[3]), byte.Parse(v[4]), byte.Parse(v[5]));
		}

		public override bool Equals(object obj)
		{
			SerialCardStruct objStr = obj as SerialCardStruct;
			if ((!object.Equals(objStr, null)) && (objStr.Serial == Serial))
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public static bool Equals(SerialCardStruct a, SerialCardStruct b)
		{
			if (!object.Equals(a, null))
			{
				return a.Equals(b);
			}
			else if (object.Equals(b, null))
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public static bool operator ==(SerialCardStruct a, SerialCardStruct b)
		{
			return Equals(a, b);
		}

		public static bool operator !=(SerialCardStruct a, SerialCardStruct b)
		{
			return !Equals(a, b);
		}

		public override int GetHashCode()
		{
			return Serial.GetHashCode();
		}

		public override string ToString()
		{
			return (Byte1 + "." + Byte2 + "." + Byte3 + "." + Byte4 + "." + Byte5 + "." + Byte6);
		}

		public string ToString(bool omitLastZeros)
		{
			string res = ToString();
			if (byte6 == 0 && omitLastZeros)
			{
				res = res.Remove(res.Length - 2, 2);
				if (byte5 == 0)
				{
					res = res.Remove(res.Length - 2, 2);
					if (byte4 == 0)
					{
						res = res.Remove(res.Length - 2, 2);
						if (byte3 == 0)
						{
							res = res.Remove(res.Length - 2, 2);
							if (byte2 == 0)
							{
								res = res.Remove(res.Length - 2, 2);
								if (byte1 == 0)
								{
									res = "";
								}
							}
						}
					}
				}
			}
			return res;
		}

		public SerialCardStruct(byte byte1I, byte byte2I, byte byte3I, byte byte4I, byte byte5I, byte byte6I)
		{
			byte1 = byte1I;
			byte2 = byte2I;
			byte3 = byte3I;
			byte4 = byte4I;
			byte5 = byte5I;
			byte6 = byte6I;
		}

		public SerialCardStruct(ulong serialI)
		{
			Serial = serialI;
		}

		public SerialCardStruct() :
			this(0, 0, 0, 0, 0, 0)
		{
		}
	}
}