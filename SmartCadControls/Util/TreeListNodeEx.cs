﻿using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;

namespace SmartCadControls.Util
{
    public class TreeListNodeEx : TreeListNode
    {
        public string MapLayerName { get; set; }

        public TreeListNodeExType TypeNodeLeaf { get; set; }

        public object TypeValue { get; set; }

        public string LayerName { get; set; }

        public TreeListNodeEx(int id, TreeListNodes owner)
            : base(id, owner)
        {
        }

        private string NodeParameters()
        {
            return LayerName;
        }
        
        

        public override object this[object columnID]
        {
            get 
            {
                   return LayerName;
            }
            set
            {
                if (columnID.Equals(this.TreeList.Columns[0]))
                {
                    this.Tag = value;
                    this.TreeList.LayoutChanged();
                }
            }
        }

  
    }

    public enum TreeListNodeExType 
    {
        None,
        UnitsRoot,
        DepartamentType,
        Zone,
        Area,
        LabelLayer,
        LabelRoot,
        DepartamentTypeZone,
        DepartamentStation,
        Root,
        UbicationRoot,
        UbicationLayer,
        DepartamentZoneRoot,
        DepartamentZoneUbicationRoot,
        DepartamentZoneUbicationLayer,
        DepartamentZoneLayer,
        DepartamentStationRoot,
        DepartamentStationUbicationRoot,
        DepartamentStationUbicationLayer,
        CCTVRoot,
        CCTVLayer,
        ALARMRoot,
        ALARMLayer,
        LPRRoot,
        LPRLayer,
        MovilSecurityRoot,
        MovilSecurityLayer,
        AreaLayer,
        AreaRoot,
        PostRoot,
        PostLayer,
        PoligonRoot,
        PoligonLayer,
        IncidentRoot,
        IncidentLayer,
        RouteRoot,
        RouteDepartmetTypeRoot,
        RouteLayer,
        Layer
    }
}
