﻿namespace SmartCadControls.Controls
{
    using Microsoft.DirectX.DirectInput;
    using System;
    using System.Runtime.CompilerServices;

    public abstract class JoystickInputBase
    {
        protected string _displayName;
        protected DeviceObjectInstance _inputObjectInstance;
        protected int _inputValue;
        protected Device _joystickDevice;
        protected object ressourceLockObejct = new object();

        public event InputValueChangedEventHandler InputValueChangedEvent;

        internal JoystickInputBase(Device joystickDevice, DeviceObjectInstance inputObjectInstance, string displayName)
        {
            this._inputObjectInstance = inputObjectInstance;
            this._joystickDevice = joystickDevice;
            this._displayName = displayName;
        }

        protected virtual void FireInputValueChangedEvent(InputValueChangedEventArgs e)
        {
            if (this.InputValueChangedEvent != null)
            {
                this.InputValueChangedEvent(this, e);
            }
        }

        internal virtual void SetValue(int newValue)
        {
            bool flag = false;
            lock (this.ressourceLockObejct)
            {
                if (newValue != this._inputValue)
                {
                    this._inputValue = newValue;
                    flag = true;
                }
            }
            if (flag)
            {
                this.FireInputValueChangedEvent(new InputValueChangedEventArgs(this._inputValue));
            }
        }

        public override string ToString()
        {
            return this._displayName;
        }

        public int Id
        {
            get
            {
                lock (this.ressourceLockObejct)
                {
                    return this._inputObjectInstance.ObjectId;
                }
            }
        }

        public Guid JoystickDeviceId
        {
            get
            {
                lock (this.ressourceLockObejct)
                {
                    return this._joystickDevice.DeviceInformation.InstanceGuid;
                }
            }
        }

        public virtual int Value
        {
            get
            {
                lock (this.ressourceLockObejct)
                {
                    return this._inputValue;
                }
            }
        }

        public class InputValueChangedEventArgs : EventArgs
        {
            protected int _inputValue;

            public InputValueChangedEventArgs(int inputValue)
            {
                this._inputValue = inputValue;
            }

            public virtual int InputValue
            {
                get
                {
                    return this._inputValue;
                }
                set
                {
                    this._inputValue = value;
                }
            }
        }

        public delegate void InputValueChangedEventHandler(object sender, JoystickInputBase.InputValueChangedEventArgs e);
    }
}

