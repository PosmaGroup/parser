﻿namespace SmartCadControls.Controls
{
    using Microsoft.DirectX.DirectInput;
    using System;

    public class JoystickButtonInput : JoystickInputBase
    {
        internal JoystickButtonInput(Device joystickDevice, DeviceObjectInstance inputObjectInstance, string displayName) : base(joystickDevice, inputObjectInstance, displayName)
        {
        }

        public enum ButtonStates
        {
            Activated = 0x80,
            Deactivated = 0
        }
    }
}

