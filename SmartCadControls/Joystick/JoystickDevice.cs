﻿namespace SmartCadControls.Controls
{
    using Microsoft.DirectX.DirectInput;
    using Microsoft.DirectX;
    using System;
    using System.Collections;
    using System.Reflection;
    using System.Resources;
    using System.Threading;

    public class JoystickDevice
    {
        protected ArrayList _joystickAxisList = new ArrayList();
        protected ArrayList _joystickButtonList = new ArrayList();
        protected Device _joystickDevice;
        protected Thread _readThread;
        private ResourceManager _stringResourceManager;
        protected object ressourceLockObejct = new object();

        public JoystickDevice(Device joystickDevice)
        {
            lock (this.ressourceLockObejct)
            {
                Assembly executingAssembly = Assembly.GetExecutingAssembly();
                string name = executingAssembly.GetName().Name;
                this._stringResourceManager = new ResourceManager(name + ".resources.Strings", executingAssembly);
                this._joystickDevice = joystickDevice;
                this._joystickDevice.Acquire();
                this.InitJoystickAxis();
                this.InitJoystickButtons();
                this.StartJoystickReadThread();
            }
        }

        protected static void AssignXYValues(int x, int y, JoystickAxisInput xInput, JoystickAxisInput yInput)
        {
            if ((x != -2147483648) || (y != -2147483648))
            {
                int num = (int) Normalization.NormalizeValue((double) xInput.DeadZone, 0.0, (double) xInput.HARDWARE_AXIS_RANGE, 0.0, (double) xInput.Range);
                int newValue = xInput.HARDWARE_AXIS_RANGE / 2;
                double normalizedValueMax = xInput.HARDWARE_AXIS_RANGE / 2;
                int deadZone = xInput.DeadZone;
                int num5 = yInput.DeadZone;
                xInput.DeadZone = 0;
                yInput.DeadZone = 0;
                if ((x != -2147483648) && (y != -2147483648))
                {
                    x -= newValue;
                    y = (y - newValue) * -1;
                    double num6 = Math.Sqrt(Math.Pow((double) x, 2.0) + Math.Pow((double) y, 2.0));
                    if (num6 > num)
                    {
                        double num7 = (((((double) x) / num6) * (num6 - num)) * normalizedValueMax) / (normalizedValueMax - num);
                        double num8 = (((((double) y) / num6) * (num6 - num)) * normalizedValueMax) / (normalizedValueMax - num);
                        if (num7 < -normalizedValueMax)
                        {
                            num7 = -normalizedValueMax;
                        }
                        if (num7 > normalizedValueMax)
                        {
                            num7 = normalizedValueMax;
                        }
                        if (num8 < -normalizedValueMax)
                        {
                            num8 = -normalizedValueMax;
                        }
                        if (num8 > normalizedValueMax)
                        {
                            num8 = normalizedValueMax;
                        }
                        xInput.SetValue(newValue + ((int) num7));
                        yInput.SetValue((((int) num8) * -1) + newValue);
                    }
                    else
                    {
                        xInput.SetValue(newValue);
                        yInput.SetValue(newValue);
                    }
                }
                else if (x != -2147483648)
                {
                    if (x < num)
                    {
                        xInput.SetValue(newValue);
                    }
                    else
                    {
                        double num9 = Normalization.NormalizeValue((double) x, 0.0, normalizedValueMax, (double) num, normalizedValueMax);
                        double num10 = (((double) x) / normalizedValueMax) * num9;
                        xInput.SetValue((int) num10);
                    }
                }
                else if (y != -2147483648)
                {
                    if (y < num)
                    {
                        yInput.SetValue(newValue);
                    }
                    else
                    {
                        double num11 = Normalization.NormalizeValue((double) y, 0.0, normalizedValueMax, (double) num, normalizedValueMax);
                        double num12 = (((double) y) / normalizedValueMax) * num11;
                        yInput.SetValue((int) num12);
                    }
                }
                xInput.DeadZone = deadZone;
                yInput.DeadZone = num5;
            }
        }

        public void Close()
        {
            this.StopJoystickReadThread();
        }

        public JoystickAxisInput GetJoystickAxis(int axisId)
        {
            ArrayList joystickAxisList = this.JoystickAxisList as ArrayList;
            foreach (JoystickAxisInput input in joystickAxisList)
            {
                if (input.Id == axisId)
                {
                    return input;
                }
            }
            return null;
        }

        public JoystickButtonInput GetJoystickButton(int buttonId)
        {
            lock (this.ressourceLockObejct)
            {
                ArrayList joystickButtonList = this.JoystickButtonList as ArrayList;
                foreach (JoystickButtonInput input in joystickButtonList)
                {
                    if (input.Id == buttonId)
                    {
                        return input;
                    }
                }
                return null;
            }
        }

        public JoystickState GetJoystickState()
        {
            lock (this.ressourceLockObejct)
            {
                return this._joystickDevice.CurrentJoystickState;
            }
        }

        protected int GetState(JoystickAxisInput.AxisTypes axisType, JoystickState joystickState)
        {
            switch (axisType)
            {
                case JoystickAxisInput.AxisTypes.X:
                    return joystickState.X;

                case JoystickAxisInput.AxisTypes.Y:
                    return joystickState.Y;

                case JoystickAxisInput.AxisTypes.Z:
                    return joystickState.Z;

                case JoystickAxisInput.AxisTypes.RotationX:
                    return joystickState.Rx;

                case JoystickAxisInput.AxisTypes.RotationY:
                    return joystickState.Ry;

                case JoystickAxisInput.AxisTypes.RotationZ:
                    return joystickState.Rz;

                case JoystickAxisInput.AxisTypes.SliderA:
                    return joystickState.GetSlider()[0];

                case JoystickAxisInput.AxisTypes.SliderB:
                    return joystickState.GetSlider()[1];
            }
            return 0;
        }

        protected void InitJoystickAxis()
        {
            lock (this.ressourceLockObejct)
            {
                JoystickAxisInput input;
                ArrayList list = new ArrayList();
                int minimumRange = 5;
                foreach (DeviceObjectInstance instance in this._joystickDevice.Objects)
                {
                    if ((instance.ObjectId & 3) != 0)
                    {
                        this._joystickDevice.Properties.SetRange(ParameterHow.ById, instance.ObjectId, new InputRange(minimumRange, minimumRange));
                        list.Add(instance);
                        minimumRange++;
                    }
                }
                if (this._joystickDevice.CurrentJoystickState.X > 0)
                {
                    DeviceObjectInstance inputObjectInstance = (DeviceObjectInstance) list[this._joystickDevice.CurrentJoystickState.X - 5];
                    string displayName = string.Format("AxisX", new object[0]);
                    input = new JoystickAxisInput(this._joystickDevice, inputObjectInstance, displayName, JoystickAxisInput.AxisTypes.X);
                    ((ArrayList) this.JoystickAxisList).Add(input);
                }
                if (this._joystickDevice.CurrentJoystickState.Y > 0)
                {
                    DeviceObjectInstance instance3 = (DeviceObjectInstance) list[this._joystickDevice.CurrentJoystickState.Y - 5];
                    string str2 = string.Format("AxisY", new object[0]);
                    input = new JoystickAxisInput(this._joystickDevice, instance3, str2, JoystickAxisInput.AxisTypes.Y);
                    ((ArrayList) this.JoystickAxisList).Add(input);
                }
                if (this._joystickDevice.CurrentJoystickState.Z > 0)
                {
                    DeviceObjectInstance instance4 = (DeviceObjectInstance) list[this._joystickDevice.CurrentJoystickState.Z - 5];
                    string str3 = string.Format("AxisZ", new object[0]);
                    input = new JoystickAxisInput(this._joystickDevice, instance4, str3, JoystickAxisInput.AxisTypes.Z);
                    ((ArrayList) this.JoystickAxisList).Add(input);
                }
                if (this._joystickDevice.CurrentJoystickState.Rx > 0)
                {
                    DeviceObjectInstance instance5 = (DeviceObjectInstance) list[this._joystickDevice.CurrentJoystickState.Rx - 5];
                    string str4 = string.Format("AxisRx", new object[0]);
                    input = new JoystickAxisInput(this._joystickDevice, instance5, str4, JoystickAxisInput.AxisTypes.RotationX);
                    ((ArrayList) this.JoystickAxisList).Add(input);
                }
                if (this._joystickDevice.CurrentJoystickState.Ry > 0)
                {
                    DeviceObjectInstance instance6 = (DeviceObjectInstance) list[this._joystickDevice.CurrentJoystickState.Ry - 5];
                    string str5 = string.Format("AxisRy", new object[0]);
                    input = new JoystickAxisInput(this._joystickDevice, instance6, str5, JoystickAxisInput.AxisTypes.RotationY);
                    ((ArrayList) this.JoystickAxisList).Add(input);
                }
                if (this._joystickDevice.CurrentJoystickState.Rz > 0)
                {
                    DeviceObjectInstance instance7 = (DeviceObjectInstance) list[this._joystickDevice.CurrentJoystickState.Rz - 5];
                    string str6 = string.Format("AxisRz", new object[0]);
                    input = new JoystickAxisInput(this._joystickDevice, instance7, str6, JoystickAxisInput.AxisTypes.RotationZ);
                    ((ArrayList) this.JoystickAxisList).Add(input);
                }
                if (this._joystickDevice.CurrentJoystickState.GetSlider()[0] > 0)
                {
                    DeviceObjectInstance instance8 = (DeviceObjectInstance) list[this._joystickDevice.CurrentJoystickState.GetSlider()[0] - 5];
                    string str7 = string.Format("AxisSliderA", new object[0]);
                    input = new JoystickAxisInput(this._joystickDevice, instance8, str7, JoystickAxisInput.AxisTypes.SliderA);
                    ((ArrayList) this.JoystickAxisList).Add(input);
                }
                if (this._joystickDevice.CurrentJoystickState.GetSlider()[1] > 0)
                {
                    DeviceObjectInstance instance9 = (DeviceObjectInstance) list[this._joystickDevice.CurrentJoystickState.GetSlider()[1] - 5];
                    string str8 = string.Format("AxisSliderB", new object[0]);
                    input = new JoystickAxisInput(this._joystickDevice, instance9, str8, JoystickAxisInput.AxisTypes.SliderB);
                    ((ArrayList) this.JoystickAxisList).Add(input);
                }
            }
        }

        protected void InitJoystickButtons()
        {
            lock (this.ressourceLockObejct)
            {
                int num = 0;
                foreach (DeviceObjectInstance instance in this._joystickDevice.Objects)
                {
                    if ((instance.ObjectId & 12) != 0)
                    {
                        string displayName = string.Format("ButtonName", num + 1);
                        JoystickButtonInput input = new JoystickButtonInput(this._joystickDevice, instance, displayName);
                        ((ArrayList) this.JoystickButtonList).Add(input);
                        num++;
                    }
                }
            }
        }

        protected void ReadJoystickThread()
        {
            while (true)
            {
                JoystickState currentJoystickState;
                try 
                {
                    currentJoystickState = this._joystickDevice.CurrentJoystickState;
                }
                catch (InputLostException)
                {
                    return;
                }
                ArrayList joystickAxisList = this.JoystickAxisList as ArrayList;
                int x = -2147483648;
                int y = -2147483648;
                JoystickAxisInput xInput = null;
                JoystickAxisInput yInput = null;
                foreach (JoystickAxisInput input3 in joystickAxisList)
                {
                    if (input3.AxisAction == JoystickAxisInput.AxisActions.Pan)
                    {
                        x = this.GetState(input3.AxisType, currentJoystickState);
                        xInput = input3;
                    }
                    else
                    {
                        if (input3.AxisAction == JoystickAxisInput.AxisActions.Tilt)
                        {
                            y = this.GetState(input3.AxisType, currentJoystickState);
                            yInput = input3;
                            continue;
                        }
                        input3.SetValue(this.GetState(input3.AxisType, currentJoystickState));
                    }
                }
                if ((xInput != null) && (yInput != null))
                {
                    AssignXYValues(x, y, xInput, yInput);
                }
                byte[] buttons = currentJoystickState.GetButtons();
                int count = this.JoystickButtonList.Count;
                for (int i = 0; i < count; i++)
                {
                    if (buttons.Length < i)
                    {
                        break;
                    }
                    ((JoystickButtonInput) ((ArrayList) this.JoystickButtonList)[i]).SetValue(buttons[i]);
                }
                Thread.Sleep(100);
            }
        }

        protected void StartJoystickReadThread()
        {
            if (this._readThread == null)
            {
                this._readThread = new Thread(new ThreadStart(this.ReadJoystickThread));
                this._readThread.Name = "Joystick Read Thread. Joystick ID: " + this.Id;
                this._readThread.IsBackground = true;
                this._readThread.Start();
            }
        }

        protected void StopJoystickReadThread()
        {
            if (this._readThread != null)
            {
                this._readThread.Abort();
                this._readThread.Join();
                this._readThread = null;
            }
        }

        public override string ToString()
        {
            lock (this.ressourceLockObejct)
            {
                return this._joystickDevice.DeviceInformation.ProductName;
            }
        }

        public Guid Id
        {
            get
            {
                lock (this.ressourceLockObejct)
                {
                    return this._joystickDevice.DeviceInformation.InstanceGuid;
                }
            }
        }

        public ICollection JoystickAxisList
        {
            get
            {
                lock (this.ressourceLockObejct)
                {
                    return this._joystickAxisList;
                }
            }
        }

        public ICollection JoystickButtonList
        {
            get
            {
                lock (this.ressourceLockObejct)
                {
                    return this._joystickButtonList;
                }
            }
        }
    }
}

