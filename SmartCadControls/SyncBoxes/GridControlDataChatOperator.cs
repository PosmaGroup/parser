﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Core;
using Smartmatic.SmartCad.Service;
using System.Collections;
using SmartCadControls.Controls;

namespace SmartCadControls.SyncBoxes
{
	public class GridControlDataChatOperator: GridControlData
	{
		private OperatorClientData chatOperator;
        
		public OperatorClientData Operator
        {
            get
            {
                return chatOperator;
            }
            set
            {
                chatOperator = value;
            }
        }

		public string applicationName;

        [GridControlRowInfoData(Visible = true, Width = 100, Searchable = true)]
        public string WorkShift
        {
            get
            {
                string scheduleName = string.Empty;
                if (chatOperator.WorkShifts != null && chatOperator.WorkShifts.Count > 0)
                {
                    DateTime now = ServerServiceClient.GetInstance().GetTime();
                    bool flag = false;
                    try
                    {
                        foreach (WorkShiftVariationClientData WorkS in chatOperator.WorkShifts)
                        {
                            foreach (WorkShiftScheduleVariationClientData Schedule in WorkS.Schedules)
                                if (Schedule.Start <= now && now <= Schedule.End)
                                {
                                    scheduleName = (string)WorkS.Name;
                                    flag = true;
                                    break;
                                }
                            if (flag)
                                break;
                        }
                    }
                    catch
                    {
                        scheduleName = string.Empty;
                    }

                    if (string.IsNullOrEmpty(scheduleName) == true)
                        scheduleName = string.Empty;

                    return scheduleName;
                                        
                }
                return scheduleName;
            }
        }
        
        public GridControlDataChatOperator(OperatorClientData chatOperator)
            : base(chatOperator)
        {
            this.chatOperator = chatOperator;
        }

   
		[GridControlRowInfoData(Visible = true, Width = 100, Searchable = true)]
		public string FirstName
        {
            get
            {
                return chatOperator.FirstName;
            }
        }

		[GridControlRowInfoData(Visible = true, Width = 100, Searchable = true)]
		public string LastName
        {
            get
            {
                return chatOperator.LastName;
            }
        }

		//private string status;

        [GridControlRowInfoData(Visible = true, Width = 100, Searchable = true)]
		public string Status {
			get
			{
				return ApplicationUtil.GetOperatorStatus(chatOperator, applicationName).StatusFriendlyName;
			}
		}

		[GridControlRowInfoData(Visible = true, Width = 120, Searchable = true)]
		public string Role
		{
			get
			{
				return chatOperator.RoleFriendlyName;
			}
		}

		
		public override bool Equals(object obj)
		{
			if ((obj is GridControlDataChatOperator) == false)
				return false;

			if (((GridControlDataChatOperator)obj).chatOperator.Code == this.chatOperator.Code)
				return true;
			return false;
		}
	}
}
