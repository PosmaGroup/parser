﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.ClientData;
using System.Drawing;
using DevExpress.XtraEditors.Repository;
using SmartCadCore.Core;
using SmartCadCore.Common;
using SmartCadControls.Controls;

namespace SmartCadControls.SyncBoxes
{
    public class GridControlDataCreateOrChangeDispatchOrder : GridControlData
    {
        public enum ActionOnData
        {
            ToBeCreated,
            TimeChangedInDelayedUnit,
            TimeChangedInAssignedUnit,
            Cancelled,
            Closed
        }

        private ActionOnData action;
        private bool hasEndingReport;
        private string expectedTime;
        private string finalUnitStatus;
        private static readonly Image finishedImg = ResourceLoader.GetImage("$Image.FinishedDispatch");
        private static readonly Image unFinishedImg = ResourceLoader.GetImage("$Image.Transparent");
        private bool incidentIsSynchronized;


        public GridControlDataCreateOrChangeDispatchOrder(DispatchOrderClientData dispatchOrder,
            ActionOnData action)
            : base(dispatchOrder)
        {
            this.hasEndingReport = false;
            this.action = action;
        }

        
        public DispatchOrderClientData DispatchOrder
        {
            get
            {
                return this.Tag as DispatchOrderClientData;
            }
        }

        public ActionOnData Action
        {
            get
            {
                return action;
            }
            set
            {
                if (action != value)
                {
                    action = value;
                }
            }
        }

        public bool HasEndingReport
        {
            get
            {
                return this.hasEndingReport;
            }
            set
            {
                this.hasEndingReport = value;
            }
        }

        public bool IncidentIsSynchronized
        {
            get
            {
                return incidentIsSynchronized;
            }
            set
            {
                incidentIsSynchronized = value;
            }
        }

        [GridControlRowInfoData(Searchable = false)]
        public string DepartmentType
        {
            get
            {
                return (Tag as DispatchOrderClientData).DepartmentType.Name;
            }
        }

        [GridControlRowInfoData(Searchable = false)]
        public string UnitCustomCode
        {
            get
            {
                return (Tag as DispatchOrderClientData).Unit.CustomCode;
            }
        }

        [GridControlRowInfoData(Searchable = false)]
        public string UnitType
        {
            get
            {
                return (Tag as DispatchOrderClientData).Unit.Type.Name;
            }
        }

        [GridControlRowInfoData(Searchable = false)]
        public string ExpectedTime
        {
            get
            {
                return expectedTime;
            }
            set
            {
                expectedTime = value;
            }
        }

        [GridControlRowInfoData(Searchable = false, Visible = false)]
        public string FinalUnitStatus
        {
            set
            {
                finalUnitStatus = value;
            }
            get
            {
                return finalUnitStatus;
            }
        }

        [GridControlRowInfoData(Visible = true, Width = 80,
            RepositoryType = typeof(RepositoryItemPictureEdit), Searchable = false)]
        public Image Finished
        {
            get
            {
                return GetImageOnAction(this.action);
            }            
        }

        [GridControlRowInfoData(Searchable = false)]
        public string Calculable
        {
            get
            {
                string result = ResourceLoader.GetString2("$Message.Yes");
                bool calculable = DispatchOrder.Unit.Lon != 0 && DispatchOrder.Unit.Lat != 0 && IncidentIsSynchronized == true;
                if (calculable == false)
                    result = ResourceLoader.GetString2("$Message.No");
                return result;
            }
        }

        private Image GetImageOnAction(ActionOnData actionOnData)
        {
            Image imageToDisplay = unFinishedImg;
            switch (action)
            {
                case ActionOnData.TimeChangedInAssignedUnit:
                    if (string.IsNullOrEmpty((Tag as DispatchOrderClientData).NoteDetail) == false &&
                        (Tag as DispatchOrderClientData).TemporalExpectedTime > TimeSpan.Zero)
                        imageToDisplay = finishedImg;
                    break;
                case ActionOnData.TimeChangedInDelayedUnit:
                    if (string.IsNullOrEmpty((Tag as DispatchOrderClientData).NoteDetail) == false &&
                        (Tag as DispatchOrderClientData).TemporalExpectedTime > TimeSpan.Zero)
                        imageToDisplay = finishedImg;
                    break;
                case ActionOnData.Cancelled:
                    if (string.IsNullOrEmpty((Tag as DispatchOrderClientData).NoteDetail) == false)
                        imageToDisplay = finishedImg;
                    break;
                case ActionOnData.Closed:
                    if (hasEndingReport == true)
                        imageToDisplay = finishedImg;
                    break;
                case ActionOnData.ToBeCreated:
                    imageToDisplay = unFinishedImg;
                    break;
                default:
                    imageToDisplay = unFinishedImg;
                    break;
            }
            return imageToDisplay;
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is GridControlDataCreateOrChangeDispatchOrder)
            {
                result = DispatchOrder.CustomCode == (obj as GridControlDataCreateOrChangeDispatchOrder).DispatchOrder.CustomCode;
            }
            return result;
        }
    }
}
