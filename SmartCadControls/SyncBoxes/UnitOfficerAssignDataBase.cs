using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;

using System.Data;
using System.IO;
using SmartCadCore.Core;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadCore.Core.Util;

namespace SmartCadControls.SyncBoxes
{
    public class UnitOfficerAssignDataBase
    {

        private const int DEPARTAMENT_TYPE_ROW = 4;
        private const int DEPARTAMENT_ZONE_ROW = 5;
        private const int DEPARTAMENT_STATION_ROW = 6;
        private const int DEPARTAMENT_COLUNM = 2;

        private const int DATE_START_ROW = 8;
        private const int DATE_COLUMN_A = 3;
        private const int DATE_COLUMN_B = 4;
        private const int DATE_END_ROW = 9;

        private const int BEGIN_ASSIGNS_ROWS = 14;

        private const int LAST_NAME_COLUMN = 3;
        private const int FIRST_NAME_COLUMN = 4;
        private const int PERSONID_COLUMN = 5;
        private const int UNIT_CUSTOM_CODE_COLUMN = 6;

        private static void foo() { }

        public static DepartmentStationAssignHistoryClientData GetAssignHistoryDataFromExcelFile(string path)
        {
            string ExceptionError = "";
            try
            {
                ExcelReader reader = new ExcelReader();

                DataSet dataBase = reader.ReadExcel(path);

                DataTable table = dataBase.Tables[0];

                string departament;
                string zone;
                string station;

                try
                {
                    departament = (string)table.Rows[DEPARTAMENT_TYPE_ROW][DEPARTAMENT_COLUNM];
                    zone = (string)table.Rows[DEPARTAMENT_ZONE_ROW][DEPARTAMENT_COLUNM];
                    station = (string)table.Rows[DEPARTAMENT_STATION_ROW][DEPARTAMENT_COLUNM];
                }
                catch (Exception)
                {
                    ExceptionError = ResourceLoader.GetString2("InvalidFormatTable");
                    throw new Exception();
                }

                DepartmentStationAssignHistoryClientData retval = new DepartmentStationAssignHistoryClientData();
                retval.DepartmentStation = new DepartmentStationClientData();
                retval.DepartmentStation.Name = station;
                retval.DepartmentStation.DepartmentZone = new DepartmentZoneClientData();
                retval.DepartmentStation.DepartmentZone.Name = zone;
                retval.DepartmentStation.DepartmentZone.DepartmentType = new DepartmentTypeClientData();
                retval.DepartmentStation.DepartmentZone.DepartmentType.Name = departament;

                DateTime start;
                DateTime end;
                try
                {

                    string aux = (string)table.Rows[DATE_START_ROW][DATE_COLUMN_A] + " " + (string)table.Rows[DATE_START_ROW][DATE_COLUMN_B];
                    string aux2 = (string)table.Rows[DATE_END_ROW][DATE_COLUMN_A] + " " + (string)table.Rows[DATE_END_ROW][DATE_COLUMN_B];
                    start = DateTime.Parse(aux);

                    end = DateTime.Parse(aux2);
                }
                catch (Exception)
                {
                    ExceptionError = "Invalid Date formating";
                    throw new Exception();
                }

                ArrayList list = new ArrayList();

                retval.StartDate = start;
                retval.EndDate = end;
                for (int i = BEGIN_ASSIGNS_ROWS; i < table.Rows.Count; i++)
                {
                    UnitOfficerAssignHistoryClientData var = new UnitOfficerAssignHistoryClientData();
                    var.Start = start;
                    var.End = end;
                    OfficerClientData officer = new OfficerClientData();
                    UnitClientData unit = new UnitClientData();
                    if (table.Rows[i][3].GetType() == typeof(System.DBNull))
                        break;
                    try
                    {
                        officer.LastName = (string)table.Rows[i][LAST_NAME_COLUMN];
                        officer.FirstName = (string)table.Rows[i][FIRST_NAME_COLUMN];
                        officer.PersonID = (string)table.Rows[i][PERSONID_COLUMN];
                        unit.CustomCode = (string)table.Rows[i][UNIT_CUSTOM_CODE_COLUMN];
                    }
                    catch (Exception)
                    {
                        ExceptionError = ResourceLoader.GetString2("InvalidFile");
                        throw new Exception();
                    }
                    var.Officer = officer;
                    var.Unit = unit;
                    var.Code = i;
                    list.Add(var);
                }
                //            GetInfoExcel(table);

                retval.UnitOfficerAssign = list;
                return retval;
            }
            catch (Exception)
            {
                if (ExceptionError == "")
                    throw new Exception(ResourceLoader.GetString2("InvalidFormatTable"));
                else
                    throw new Exception(ExceptionError);
            }
        }

        private static void GetInfoExcel(DataTable table)
        {

            StringBuilder sb = new StringBuilder();
            StringBuilder sb2 = new StringBuilder();


            foreach (DataRow row in table.Rows)
            {
                foreach (object obj in row.ItemArray)
                {
                    if (obj.GetType() == typeof(System.String))
                    {
                        string s = (string)obj;
                        sb.Append(s + "\n ");
                    }
                    if (obj.GetType() == typeof(System.DBNull))
                        sb.Append("   ");
                    sb2.Append(obj.GetType().ToString() + " ");
                }
                sb.AppendLine("");
                sb2.AppendLine("");
            }
          //  File.WriteAllText("C:\\Resultados.txt", sb.ToString(), Encoding.UTF8);
          //  File.WriteAllText("C:\\Resultados2.txt", sb2.ToString(), Encoding.UTF8);
        }
    }
}
