﻿using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.ClientData;

namespace SmartCadControls.SyncBoxes
{

    public class GridSearchBasicMapData 
    {
        public AddressClientData Address
        {
            get;
            set;
        }
    }

    public class GridSearchMapData : GridSearchBasicMapData
    {

        public GridSearchMapData(AddressClientData address)
        {
            this.Address = address;
        }

        public GridSearchMapData()
        {
         
        }

        [GridControlRowInfoData(Visible = true, Width = 120, Searchable = true)]
        public string ZoneSearchMaps
        {
            get { return Address.Zone; }
        }

        [GridControlRowInfoData(Visible = true, Width = 90, Searchable = true)]
        public string StreetSearchMaps
        {
            get { return Address.Street; }
        }

        [GridControlRowInfoData(Visible = false, Width = 60, Searchable = true)]
        public string MoreSearchMaps
        {
            get { return Address.More;}
        }

        [GridControlRowInfoData(Visible = true,RealFieldName = "Otros" ,Width = 50, Searchable = true)]
        public string FullTextSearchMaps
        {
            get { return Address.FullText; }
        }

        public override bool Equals(object obj)
        {
            GridSearchMapData gridObj = obj as GridSearchMapData;
            if (gridObj != null)
                return gridObj.Address.Lat == this.Address.Lat && gridObj.Address.Lon == this.Address.Lon;
            return false;
        }
    }

    public class GridSearchUnitMapData : GridSearchBasicMapData
    {

        public UnitClientData Unit { get; set; }


        public GridSearchUnitMapData(UnitClientData unit, AddressClientData address)
        {
            this.Address = address;
            this.Unit = unit;
        }

        public GridSearchUnitMapData()
        {
         
        }

        [GridControlRowInfoData(Visible = true, Width = 50, Searchable = true)]
        public string Plate
        {
            get { return Unit.CustomCode; }
        }

        [GridControlRowInfoData(Visible = true, Width = 50, Searchable = true)]
        public string UnitType
        {
            get { return Unit.Type.Name; }
        }

        [GridControlRowInfoData(Visible = true, Width = 50, Searchable = true)]
        public string Department
        {
            get { return Unit.DepartmentType.Name; }
        }

        public override bool Equals(object obj)
        {
            GridSearchUnitMapData gridObj = obj as GridSearchUnitMapData;
            if (gridObj != null)
                return gridObj.Unit.Code == this.Unit.Code;
            return false;
        }
    }

    public class GridSearchIncidentMapData : GridSearchBasicMapData
    {



        public IncidentClientData IncidentClientData { get; set; }

        public GridSearchIncidentMapData(IncidentClientData inc)
        {
            this.IncidentClientData = inc;
            this.Address = inc.Address;
        }

        [GridControlRowInfoData(Visible = true, Searchable = true)]
        public string Incident
        {
            get { return IncidentClientData.CustomCode; }
        }


        [GridControlRowInfoData(Visible = true, Searchable = true)]
        public string IncidentType
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                foreach (IncidentTypeClientData it in IncidentClientData.IncidentTypes)
                {
                    if (sb.Length > 0)
                        sb.Append(", " + it.FriendlyName);
                    else
                        sb.Append(it.FriendlyName);
                }
                return sb.ToString();
            }
        }
    }

    public class GridSearchStructMapData : GridSearchBasicMapData
    {
        public StructClientData Struct { get; set; }

        public GridSearchStructMapData(StructClientData st)
        {
            this.Struct = st;
            this.Address = new AddressClientData(st.Lon, st.Lat);
        }

        [GridControlRowInfoData(Visible = true, Searchable = true)]
        public string Name
        {
            get { return Struct.Name; }
        }

        [GridControlRowInfoData(Visible = true, Searchable = true)]
        public string Zone
        {
            get { return Struct.CctvZone.Name; }
        }
    }

}
