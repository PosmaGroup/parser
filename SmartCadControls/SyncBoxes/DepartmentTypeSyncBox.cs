using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.ClientData;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraEditors.Repository;
using SmartCadControls.Controls;

namespace SmartCadControls.SyncBoxes
{

    public class GridDepartmentTypeData : GridControlData
    {
        private DepartmentTypeClientData departmentType;
        private bool check;
		private IncidentNotificationPriorityClientData priority;
        
        public DepartmentTypeClientData DepartmentType
        {
            get
            {
                return departmentType;
            }
            set
            {
                departmentType = value;
            }
        }

        public GridDepartmentTypeData(DepartmentTypeClientData departmentType)
            : base(departmentType)
        {
            this.departmentType = departmentType;
        }

       [GridControlRowInfoData(RepositoryType = typeof(RepositoryItemCheckEdit), Width = 15, Searchable = false)]
        public bool DepartmentTypeCheck
        {
            get
            {
                return check;
            }
            set
            {
                check = value;
            }
        }

		[GridControlRowInfoData(Searchable = true)]
        public string Name
        {
            get
            {
                return departmentType.Name;
            }
        }

		[GridControlRowInfoData(RepositoryType = typeof(RepositoryItemComboBox), Searchable = true)]
		public IncidentNotificationPriorityClientData Priority
        {
            get
            {
                return priority;
            }
            set 
            {
                priority = value;
            
            }
        }

        public int Code
        {
            get
            {
                return departmentType.Code;
            }
        }
		public override bool Equals(object obj)
		{
			bool result = false;
            if (obj is GridDepartmentTypeData)
            {
                result = DepartmentType.Code == (obj as GridDepartmentTypeData).DepartmentType.Code;
            }
            return result;
		}
    }
}
