﻿using SmartCadControls.Controls;
using SmartCadCore.ClientData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadControls.SyncBoxes
{
    public class GridControlDataUnit : GridControlData
    {

        private UnitClientData unit;
        private string recommended;

        public UnitClientData Unit
        {
            get
            {
                return unit;
            }
            set
            {
                unit = value;
            }
        }

        public GridControlDataUnit(UnitClientData unit)
            : base(unit)
        {
            if (unit.Status != null)
                this.BackColor = unit.Status.Color;
            this.unit = unit;
            this.unit.Distance = 0;
        }


        public int Code
        {
            get
            {
                return unit.Code;
            }
            set
            {
            }
        }

        [GridControlRowInfoData(Visible = true)]
        public string DepartmentType
        {
            get
            {
                return unit.DepartmentStation.DepartmentZone.DepartmentType.Name;
            }
        }

        [GridControlRowInfoData(Visible = true)]
        public string UnitCustomCode
        {
            get
            {
                return unit.CustomCode;
            }
        }

        [GridControlRowInfoData(Visible = true)]
        public string UnitType
        {
            get
            {
                return unit.Type.Name;
            }
        }

        [GridControlRowInfoData(Visible = true)]
        public string DepartmentZone
        {
            get
            {
                return unit.DepartmentStation.DepartmentZone.Name;
            }
        }

        [GridControlRowInfoData(Visible = true)]
        public string DepartmentStation
        {
            get
            {
                return unit.DepartmentStation.Name;
            }
        }

        [GridControlRowInfoData(Visible = true)]
        public string Recommended
        {
            get
            {
                return recommended;
            }
            set
            {
                recommended = value;
            }
        }

        [GridControlRowInfoData(Visible = true)]
        public string Distance
        {
            get
            {
                if (unit.Distance != 0)
                {
                    return ((double)Math.Round(unit.Distance, 2, MidpointRounding.ToEven)).ToString("0.00");
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        [GridControlRowInfoData(Visible = true)]
        public string Status
        {
            get
            {
                return unit.Status.FriendlyName;
            }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is GridControlDataUnit)
            {
                result = Unit.Code.Equals((obj as GridControlDataUnit).Code) || Unit.CustomCode.Equals((obj as GridControlDataUnit).UnitCustomCode);
            }
            return result;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

    }
}
