﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadControls.Controls;

namespace SmartCadControls.SyncBoxes
{
    public class GridControlDataDepartmentStationCoord : GridControlData
    {
        [GridControlRowInfoData]
        public string PointNumber
        {
            get
            {
                return Address.PointNumber;
            }
        }

        [GridControlRowInfoData]
        public double Lat
        {
            get
            {
                return Address.Lat;
            }
        }

        [GridControlRowInfoData]
        public double Lon
        {
            get
            {
                return Address.Lon;
            }
        }

        public DepartmentStationAddressClientData Address
        {
            get
            {
                return this.Tag as DepartmentStationAddressClientData;
            }
        }

        public GridControlDataDepartmentStationCoord(DepartmentStationAddressClientData address)
            : base(address)
        {
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is GridControlDataDepartmentStationCoord)
            {
                GridControlDataDepartmentStationCoord address = (GridControlDataDepartmentStationCoord)obj;
                if ((this.Lon == address.Lon) && (this.Lat == address.Lat))
                {
                    result = true;
                }
            }
            return result;
        }
    }
}
