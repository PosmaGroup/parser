using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.Core;
using System.Drawing;
using System.Windows.Forms;
using SmartCadCore.ClientData;
using SmartCadCore.Common;

namespace SmartCadControls.SyncBoxes
{
    public class GridUnitOfficerAsignData : DataGridExData
    {

        #region Fields


        private UnitOfficerAssignHistoryClientData unitOfficerAsign;
        public static DataGridEx DataGrid = null;
        private Image icon;
        private DataGridViewExButtonColumn buttonAddOfficer;
        private Color backColor;
        private DataGridViewExButtonColumn buttonAddUnit;
        private ErrorsInRow error;
        #endregion

        #region Properties

        public ErrorsInRow Error
        {
            get { return error; }
            set { error = value; }
        }
	

        public override Color BackColor
        {
            get
            {
                return backColor;
            }
            set
            {
                backColor = value;
            }
        }

        public override string Key
        {
            get
            {
                return unitOfficerAsign.Code.ToString();
            }
            set
            {
                throw new Exception(ResourceLoader.GetString2("NotImplementedMethod"));
            }
        }

        public DataGridViewExButtonColumn AddOfficer
        {
            get { return buttonAddOfficer; }
            set { buttonAddOfficer = value; }
        }

        public Image Icon
        {
            get { return icon; }
            set { icon = value; }
        }

        public DataGridViewExButtonColumn AddUnit
        {
            get { return buttonAddUnit; }
            set { buttonAddUnit = value; }
        }

        public UnitOfficerAssignHistoryClientData UnitOfficerAssing
        {
            get 
            {
                return unitOfficerAsign;
            }
            set
            {
                unitOfficerAsign = value;
            }
        }

        #endregion

        public GridUnitOfficerAsignData(UnitOfficerAssignHistoryClientData unitOfficerAsign)
        {
            buttonAddOfficer = new DataGridViewExButtonColumn();
            buttonAddUnit = new DataGridViewExButtonColumn();
            this.unitOfficerAsign = unitOfficerAsign;
            this.icon = ResourceLoader.GetImage("IconTreeIncidentType");
        }

        [DataGridExColumn(HeaderText = "!", Type = typeof(Image), Column = typeof(DataGridViewImageColumn),Width = 25,HeaderAlignment = DataGridViewContentAlignment.MiddleCenter)]
        public Image IconGrid
        {
            get
            {
                return this.icon;
            }
        }

        
        [DataGridExColumn(HeaderText = "FullLastName",Width = 100)]
        public string LastName
        {
            get
            {
                string retval = "";
                if (unitOfficerAsign != null && unitOfficerAsign.Officer != null)
                    retval = unitOfficerAsign.Officer.LastName;
                return retval;
              
            }
        }

        [DataGridExColumn(HeaderText = "FullFirstName", Width = 100)]
        public string Name
        {
            get
            {
                string retval = "";
                if (unitOfficerAsign != null && unitOfficerAsign.Officer != null)
                    retval = unitOfficerAsign.Officer.FirstName;
                return retval;
            }
        }

        [DataGridExColumn(HeaderText = "OfficerID", Width = 90)]
        public string PersonID 
        {
            get 
            {
                string retval = "";
                if (unitOfficerAsign != null && unitOfficerAsign.Officer != null)
                    retval = unitOfficerAsign.Officer.PersonID;
                return retval;
            }
        }

        [DataGridExColumn(HeaderText = "AddOfficial", Column = typeof(DataGridViewExButtonColumn), Width = 90)]
        public string ButtonAddOfficer
        {
            get
            {
                return "...";
            }
        }

        [DataGridExColumn(HeaderText = "PlateUnit", Width = 110)]
        public string UnitCustomCode
        {
            get
            {
                string retval = "";
                if (unitOfficerAsign != null && unitOfficerAsign.Unit != null)
                    retval = unitOfficerAsign.Unit.CustomCode;
                return retval;
            }
        }

        [DataGridExColumn(HeaderText = "AddUnit", Column = typeof(DataGridViewExButtonColumn), Width = 90)]
        public string ButtonAddUnit
        {
            get
            {
                return "...";
            }
        }

    }

    public enum ErrorsInRow
    {
        BadDepartamentType,
        BadDepartamentZone,
        BadDepartamentStation,
        OfficerNotInDepartamentType,
        OfficerNotInDepartamentZone,
        OfficerNotInDepartamentStation,
        UnitNotInDepartamentType,
        UnitNotInDepartamentZone,
        UnitNotInDepartamentStation,
        UnitNotInSystem,
        OfficerNotInSystem,
        OfficerRepeted,
        UnitOveloaded,
        OfficerUnitNotInDepartamentStation,
        AssignStarted,
        UnitRepeted,
        AssignOk
    }

    public class UnitOfficerAsignSyncBox2 : SyncBox2 
    {
        public override DataGridExData BuildGridData(object objectData)
        {
            UnitOfficerAssignHistoryClientData obj =  objectData as UnitOfficerAssignHistoryClientData;
            GridUnitOfficerAsignData retval = new GridUnitOfficerAsignData(obj);

            return retval;
        }
    }
}
