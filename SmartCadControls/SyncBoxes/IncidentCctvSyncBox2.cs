using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Collections;
using Smartmatic.SmartCad.Service;
using SmartCadControls.Controls;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadControls;

namespace SmartCadFirstLevel.Gui.SyncBoxes
{
    public class GridIncidentCctvData : GridControlData
    {
        private CameraClientData camera;
        private Color backColor;
        private IncidentClientData incident;
        public GridIncidentCctvData(IncidentClientData incident)
            : base(incident)
        {
            this.incident = incident;
            backColor = Color.White;
            if (incident.CctvReports != null)
            {
                CctvReportClientData cctvReport = (CctvReportClientData)incident.CctvReports[0];
                this.camera = cctvReport.Camera;
            }
        }

        
        public string Key
        {
            get
            {
                return incident.CustomCode;
            }
            set
            {
                throw new Exception(ResourceLoader.GetString2("NotImplementedMethod"));
            }
        }

		[GridControlRowInfoData(Searchable = true)]
        public string State
        {
            get
            {
                if (camera.StructClientData != null)
                    return camera.StructClientData.State;
                else
                    return string.Empty;
            }
        }
		[GridControlRowInfoData(Searchable = true)]
        public string Town
        {
            get
            {
                if (camera.StructClientData != null)
                    return camera.StructClientData.Town;
                else
                    return string.Empty;
            }
        }

		[GridControlRowInfoData(Searchable = true)]
        public string Zone
        {
            get
            {
                if (camera.StructClientData != null)
                    return camera.StructClientData.ZoneSecRef;
                else
                    return string.Empty;
            }
        }
		[GridControlRowInfoData(Searchable = true)]
        public string Street
        {
            get
            {
                if (camera.StructClientData != null)
                    return camera.StructClientData.Street;
                else
                    return string.Empty;
            }
        }
		[GridControlRowInfoData(Searchable = true)]
        public string CctvZone
        {
            get
            {
                if (camera.StructClientData != null && camera.StructClientData.CctvZone != null)
                    return camera.StructClientData.CctvZone.Name;
                else
                    return string.Empty;
            }
        }
		[GridControlRowInfoData(Searchable = true)]
        public string Struct
        {
            get
            {
                if (camera.StructClientData != null)
                    return camera.StructClientData.Name;
                else
                    return string.Empty;
            }
        }
		[GridControlRowInfoData(Searchable = true)]
        public string Camera
        {
            get
            {
                return camera.Name;
            }
        }

		[GridControlRowInfoData(Searchable = true)]
        public string IncidentTypes
        {
            get
            {
                int count = 0;
                StringBuilder sb = new StringBuilder();
                if (incident.IncidentTypes != null)
                {
                    foreach (IncidentTypeClientData temp in incident.IncidentTypes)
                    {
                        sb.Append(temp.CustomCode);
                        if (++count < incident.IncidentTypes.Count)
                        {
                            sb.Append(", ");
                        }
                        else
                        {
                            sb.Append(".");
                        }
                    }
                }
                return sb.ToString();
            }
        }

		[GridControlRowInfoData(Searchable = true)]
        public string CallDate
        {
            get
            {

                return (incident.CctvReports[0] as CctvReportClientData).StartDate.ToString();

            }
        }

		public override bool Equals(object obj)
		{
			if (obj is GridIncidentCctvData == false)
				return false;
			GridIncidentCctvData gridData = obj as GridIncidentCctvData;
			if (gridData.incident.Code == this.incident.Code)
				return true;
			return false;
		}
    }
}