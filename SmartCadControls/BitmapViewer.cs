using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.IO;
using SmartCadCore.Core;

namespace SmartCadControls.Controls
{
    public partial class BitmapViewer : UserControl
    {
        // The directory that will be scanned for image.
        private string directory = "";
        // Each picture box will be a square of dimension X dimension pixels.
        private int dimension;
        // The space between the images and the top, left, and right sides.
        private int border = 5;
        // The space between each image.
        private int spacing;
        // The images that were found in the selected
        public ArrayList images = new ArrayList();

        public BitmapViewer()
        {
            InitializeComponent();
        }

        #region Propierties
        
        public string Directory 
        {
            get 
            {
                return directory;
            }
            set 
            {
                directory = value;
                GetImages();
                UpdateDisplay();
            }
        }

        public int Dimension 
        {
            get 
            {
                return dimension;
            }
            set 
            {
                dimension = value;
                UpdateDisplay();
            }
        }

        public int Spacing 
        {
            get
            {
                return spacing;
            }
            set
            {
                spacing = value;
                UpdateDisplay();
            }
        }

        public Panel PnlPictures
        {
            get { return pnlPictures; }
            set { pnlPictures = value; }
        }

        #endregion

        #region NamedImageClass

        public class NamedImage 
        {
            public Image Image;
            public string FileName;

            public NamedImage(Image image,string fileName)
            {
                this.Image = image;
                this.FileName = fileName;
            }
        }

        #endregion

        private void GetImages()
        {
            images.Clear();
            if (this.Directory != "")
            {
                DirectoryInfo dir = new DirectoryInfo(Directory);
                foreach (FileInfo file in dir.GetFiles("*.*"))
                {
                    if(SmartCadConfiguration.SmartCadSection.ServerElement.Map.Dll.ToLower().Contains("mapxtreme") && file.Extension.ToLower() == ".bmp" ||
                       SmartCadConfiguration.SmartCadSection.ServerElement.Map.Dll.ToLower().Contains("google") && file.Extension.ToLower() == ".png")
                        images.Add(new NamedImage(Bitmap.FromFile(file.FullName), file.Name));
                }
            }
        }

        private PictureBox picSelected;
        public void pic_Click(object sender, System.EventArgs e)
        {            
            // Clear the border style from the last selected picture box.
            if (picSelected != null)
            {
                picSelected.BorderStyle = BorderStyle.FixedSingle;
                picSelected.BackColor = SystemColors.Window;
                if (picFirst != picSelected && done == false)
                    PnlPictures.Controls[0].BackColor = SystemColors.Window;
            }
            // Get the new selection.
            picSelected = (PictureBox)sender;
            picSelected.BorderStyle = BorderStyle.Fixed3D;
            picSelected.DisplayRectangle.Inflate(15, 15);
            picSelected.BackColor = SystemColors.GradientActiveCaption;
            // Fire the selection event.
            PictureSelectedEventArgs args = new
            PictureSelectedEventArgs((string)picSelected.Tag, picSelected.Image);
            if (PictureSelected != null)
            {
                PictureSelected(this, args);
            }
        }

        private PictureBox picFirst; bool done;
        public void pic_Click(object sender, System.EventArgs e,int first)
        {
            // Clear the border style from the last selected picture box.
            if (picFirst != null)
            {
                picFirst.BorderStyle = BorderStyle.FixedSingle;
                picFirst.BackColor = SystemColors.Window;
            }
            // Get the new selection.
            picFirst = (PictureBox)sender;
            picFirst.BorderStyle = BorderStyle.Fixed3D;
            picFirst.DisplayRectangle.Inflate(15, 15);
            picFirst.BackColor = SystemColors.GradientActiveCaption;
            // Fire the selection event.
            PictureSelectedEventArgs args = new
            PictureSelectedEventArgs((string)picFirst.Tag, picFirst.Image);
            if (PictureSelected != null)
            {
                PictureSelected(this, args);
            }
        }

        public event PictureSelectedDelegate PictureSelected;
        public delegate void PictureSelectedDelegate(object sender,PictureSelectedEventArgs e);
        
        private void UpdateDisplay()
        {
            // Clear the current display.
            pnlPictures.Controls.Clear();
            
            // Row and Col will track the current position where pictures are
            // being inserted. They begin at the top-left corner.
            int row = border, col = border;
            // Iterate through the Images collection, and create PictureBox controls.
            foreach (NamedImage image in images)
            {
                PictureBox pic = new PictureBox();
                pic.Name = image.FileName;
                pic.Image = image.Image;
                pic.Tag = image.FileName;
                pic.Size = new Size(dimension, dimension);
                pic.Location = new Point(col, row);
                pic.BorderStyle = BorderStyle.FixedSingle;
                // StretchImage mode gives us the "thumbnail" ability.
                pic.SizeMode = PictureBoxSizeMode.CenterImage;
                pic.Click += new EventHandler(this.pic_Click);
                // Display the picture.
                
                pnlPictures.Controls.Add(pic);
                // Move to the next column.
                col += dimension + spacing;
                // Move to next line if no more pictures will fit.
                if ((col + dimension + spacing + border) > this.Width)
                {
                    col = border;
                    row += dimension + spacing;
                }
            }
        }

        public void RefreshImages()
        {
            GetImages();
            UpdateDisplay();
        }

        protected override void OnSizeChanged(System.EventArgs e)
        {
            UpdateDisplay();
            base.OnSizeChanged(e);
        }


    }

    public class PictureSelectedEventArgs : EventArgs
    {
        public string FileName;
        public Image Image;
        public PictureSelectedEventArgs(String fileName, Image image)
        {
            this.FileName = fileName;
            this.Image = image;
        }
    }
}
