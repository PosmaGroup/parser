﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SmartCadControls
{
    public class DataGridViewColorCell : DataGridViewTextBoxCell
    {

        public DataGridViewColorCell()
            : base()
        {

        }

        protected override void Paint(Graphics graphics, Rectangle clipBounds, Rectangle cellBounds, int rowIndex, DataGridViewElementStates cellState, object value, object formattedValue, string errorText, DataGridViewCellStyle cellStyle, DataGridViewAdvancedBorderStyle advancedBorderStyle, DataGridViewPaintParts paintParts)
        {
            base.Paint(graphics, clipBounds, cellBounds, rowIndex, cellState, value, "", errorText, cellStyle, advancedBorderStyle, paintParts);

            System.Drawing.Drawing2D.GraphicsContainer container =
                    graphics.BeginContainer();

            graphics.SetClip(cellBounds);
            graphics.FillRectangle(new SolidBrush((Color)value), cellBounds.X + 3, cellBounds.Y + 3, cellBounds.Width / 2, cellBounds.Height - 6);

            graphics.EndContainer(container);
        }


        public override Type ValueType
        {
            get
            {
                // Return the type of the value that DataGridViewColorCell contains.
                return typeof(Color);
            }
        }

    }
}
