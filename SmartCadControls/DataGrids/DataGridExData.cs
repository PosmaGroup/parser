﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadControls
{
    public abstract class DataGridExData
    {
        public DataGridExData()
        {
        }

        public DataGridExData(object tag)
        {
            this.tag = tag;
        }

        public abstract Color BackColor
        {
            get;
            set;
        }

        public abstract string Key
        {
            get;
            set;
        }

        private object tag;

        public object Tag
        {
            get
            {
                return tag;
            }
            set
            {
                tag = value;
            }
        }

        public override bool Equals(object obj)
        {
            bool result = false;

            DataGridExData data = obj as DataGridExData;

            if (data != null)
            {
                if (this.Tag != null && data.Tag != null)
                {
                    result = this.Tag.Equals(data.Tag);
                }
            }
            return result;
        }

        public override int GetHashCode()
        {
            int hashCode = this.GetHashCode();

            if (this.Tag != null)
                hashCode = this.Tag.GetHashCode();

            return hashCode;
        }
    }
}
