namespace SmartCadControls.Controls
{
    partial class SearchFilterResultsBar
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControl = new DevExpress.XtraLayout.LayoutControl();
            this.gridControlExResults = new GridControlEx();
            this.gridViewExPreviousIncidents = new GridViewEx();
            this.gridColumnIncidentType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl)).BeginInit();
            this.layoutControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExResults)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExPreviousIncidents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl
            // 
            this.layoutControl.Controls.Add(this.gridControlExResults);
            this.layoutControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl.Location = new System.Drawing.Point(0, 0);
            this.layoutControl.Name = "layoutControl";
            this.layoutControl.Root = this.layoutControlGroup1;
            this.layoutControl.Size = new System.Drawing.Size(233, 394);
            this.layoutControl.TabIndex = 4;
            this.layoutControl.Text = "layoutControl1";
            // 
            // gridControlExResults
            // 
            this.gridControlExResults.EnableAutoFilter = true;
            this.gridControlExResults.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExResults.Location = new System.Drawing.Point(5, 5);
            this.gridControlExResults.MainView = this.gridViewExPreviousIncidents;
            this.gridControlExResults.Name = "gridControlExResults";
            this.gridControlExResults.Size = new System.Drawing.Size(223, 384);
            this.gridControlExResults.TabIndex = 5;
            this.gridControlExResults.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExPreviousIncidents});
            this.gridControlExResults.ViewTotalRows = true;
            this.gridControlExResults.Click += new System.EventHandler(this.buttonFilter_Click);
            this.gridControlExResults.MouseMove += new System.Windows.Forms.MouseEventHandler(this.gridControlExResults_MouseMove);
            // 
            // gridViewExPreviousIncidents
            // 
            this.gridViewExPreviousIncidents.AllowFocusedRowChanged = true;
            this.gridViewExPreviousIncidents.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewExPreviousIncidents.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExPreviousIncidents.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewExPreviousIncidents.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewExPreviousIncidents.Appearance.FooterPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewExPreviousIncidents.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewExPreviousIncidents.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewExPreviousIncidents.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExPreviousIncidents.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewExPreviousIncidents.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewExPreviousIncidents.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnIncidentType,
            this.gridColumnCount});
            this.gridViewExPreviousIncidents.EnablePreviewLineForFocusedRow = false;
            this.gridViewExPreviousIncidents.GridControl = this.gridControlExResults;
            this.gridViewExPreviousIncidents.Name = "gridViewExPreviousIncidents";
            this.gridViewExPreviousIncidents.OptionsMenu.EnableFooterMenu = false;
            this.gridViewExPreviousIncidents.OptionsNavigation.UseTabKey = false;
            this.gridViewExPreviousIncidents.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExPreviousIncidents.OptionsView.ShowDetailButtons = false;
            this.gridViewExPreviousIncidents.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewExPreviousIncidents.OptionsView.ShowFooter = true;
            this.gridViewExPreviousIncidents.OptionsView.ShowGroupPanel = false;
            this.gridViewExPreviousIncidents.OptionsView.ShowIndicator = false;
            this.gridViewExPreviousIncidents.ViewTotalRows = true;
            // 
            // gridColumnIncidentType
            // 
            this.gridColumnIncidentType.Caption = "gridColumnIncidentType";
            this.gridColumnIncidentType.FieldName = "Name";
            this.gridColumnIncidentType.MinWidth = 140;
            this.gridColumnIncidentType.Name = "gridColumnIncidentType";
            this.gridColumnIncidentType.OptionsColumn.AllowEdit = false;
            this.gridColumnIncidentType.OptionsFilter.AllowAutoFilter = false;
            this.gridColumnIncidentType.OptionsFilter.AllowFilter = false;
            this.gridColumnIncidentType.Visible = true;
            this.gridColumnIncidentType.VisibleIndex = 0;
            this.gridColumnIncidentType.Width = 160;
            // 
            // gridColumnCount
            // 
            this.gridColumnCount.Caption = "gridColumnCount";
            this.gridColumnCount.FieldName = "Count";
            this.gridColumnCount.Name = "gridColumnCount";
            this.gridColumnCount.OptionsColumn.AllowEdit = false;
            this.gridColumnCount.OptionsFilter.AllowAutoFilter = false;
            this.gridColumnCount.OptionsFilter.AllowFilter = false;
            this.gridColumnCount.Visible = true;
            this.gridColumnCount.VisibleIndex = 1;
            this.gridColumnCount.Width = 39;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(233, 394);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridControlExResults;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(227, 388);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // SearchFilterResultsBar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl);
            this.Name = "SearchFilterResultsBar";
            this.Size = new System.Drawing.Size(233, 394);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl)).EndInit();
            this.layoutControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExResults)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExPreviousIncidents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private GridControlEx gridControlExResults;
        private GridViewEx gridViewExPreviousIncidents;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnIncidentType;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnCount;

    }
}
