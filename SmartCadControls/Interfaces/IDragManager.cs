﻿using DevExpress.XtraLayout;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SmartCadControls.Interfaces
{
    public interface IDragManager
    {
        LayoutControlItem DragItem { get; set; }
        void SetDragCursor(DragDropEffects effect);
    }
}
