using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Globalization;

namespace SmartCadControls.Controls
{
    public class DateTimePicker30MinutesChangeEx : DateTimePicker
    {
        private DateTime oldValue = DateTime.MinValue;
        private DateTime value = DateTime.MinValue;

        public DateTimePicker30MinutesChangeEx()
            : this(DateTime.Today)
        { }

        
        public DateTimePicker30MinutesChangeEx(DateTime setDate)
        {
            Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            value = Value;
            DateTimePicker dtp = new DateTimePicker();
            
          
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
             return true;
        }

        protected override void OnValueChanged(EventArgs eventargs)
        {
            //if (oldValue < value)
            //{
            //    if (value.Minute > 0 && value.Minute < 30)
            //        Value = value.AddMinutes(30 - value.Minute);
            //    else if (value.Minute > 30 && value.Minute <= 59)
            //        Value = value.AddMinutes(60 - value.Minute);
            //}
            //else
            //{
            //    if (value.Minute > 0 && value.Minute < 30)
            //        Value = value.AddMinutes(-value.Minute);
            //    else if (value.Minute > 30 && value.Minute <= 59)
            //    {
            //        Value = value.AddMinutes(-(value.Minute - 30));
            //    }
            //}
            if (oldValue != DateTime.MinValue)
                Value = oldValue;

            oldValue = DateTime.MinValue;
           // value = oldValue;
            base.OnValueChanged(new EventArgs());
        }

        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case 0x204e:
                    this.WmReflectCommand(ref m);
                    base.WndProc(ref m);
                    return;

                default:
                    base.WndProc(ref m);
                    break;
            }
        }

        private void WmReflectCommand(ref Message m)
        {
            if (m.HWnd == base.Handle)
            {
                NMHDR lParam = (NMHDR)m.GetLParam(typeof(NMHDR));
                switch (lParam.code)
                {
                    case -759:
                        this.WmDateTimeChange(ref m);
                        return;

                    default:
                        return;
                }
            }
        }

        private void WmDateTimeChange(ref Message m)
        {
            NMDATETIMECHANGE lParam = (NMDATETIMECHANGE)m.GetLParam(typeof(NMDATETIMECHANGE));
            DateTime tempValue = DateTime.MinValue;
            if (lParam.dwFlags != 1)
            {
                tempValue = SysTimeToDateTime(lParam.st);
                if (tempValue.Minute != value.Minute)
                {
                    //if (tempValue > value && (tempValue.Minute == 59 && tempValue.Hour == 0))
                    //{
                    //    oldValue = oldValue.AddDays(1);
                    //    value = oldValue.AddMinutes(-1);
                    //}
                    //else if (tempValue > value && (tempValue.Minute == 31 && tempValue.Hour == 23))
                    //{
                    //    oldValue = oldValue.AddDays(-1);
                    //    value = oldValue.AddMinutes(1);
                    //}
                    //else if (tempValue > value && tempValue.Minute == 59)
                    //{
                    //    value = tempValue.AddHours(-1);
                    //}
                    //else
                    //    value = tempValue;
                    if (tempValue.Minute > value.Minute && value.Minute == 0)
                    {
                        if (tempValue.Minute > 0 && tempValue.Minute < 30)
                            value = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, tempValue.Hour, 30, 0);
                        else if (tempValue.Minute > 30 && tempValue.Minute <= 59)
                            value = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, tempValue.Hour, 30, 0);
                        else
                            value = tempValue;
                    }else if (tempValue.Minute > value.Minute) 
                    {
                        if (tempValue.Minute > 0 && tempValue.Minute < 30)
                            value = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, tempValue.Hour, 30, 0);
                        else if (tempValue.Minute > 30 && tempValue.Minute <= 59)
                            value = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, tempValue.Hour, 0, 0);
                        else
                            value = tempValue;
                    }
                    else if (tempValue.Minute < value.Minute)
                    {
                        if (tempValue.Minute > 0 && tempValue.Minute < 30)
                            value = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, tempValue.Hour, 0, 0);
                        else if (tempValue.Minute > 30 && tempValue.Minute <= 59)
                            value = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, tempValue.Hour, 30, 0);
                        else
                            value = tempValue;
                    }


                        //if (tempValue.Minute > 0 && tempValue.Minute < 30)
                        //{
                        //    if (tempValue.Minute > oldValue.Minute || oldValue.Minute == 59)
                        //        tempValue = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, tempValue.Hour, 0, 0);
                        //    else
                        //        tempValue = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, tempValue.Hour, 30, 0);


                        //}
                        //else if (tempValue.Minute > 30 && tempValue.Minute <= 59)
                        //{
                        //    if (tempValue.Minute > oldValue.Minute && oldValue.Minute != 0)
                        //        tempValue = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, tempValue.Hour, 0, 0);
                        //    else
                        //        tempValue = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, tempValue.Hour, 30, 0);


                        //}

                }
                else
                    value = tempValue;
            }
            oldValue = value;
          
        }

        internal static DateTime SysTimeToDateTime(SYSTEMTIME s)
        {
            return new DateTime(s.wYear, s.wMonth, s.wDay, s.wHour, s.wMinute, s.wSecond);
        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        public class NMDATETIMECHANGE
        {
            public NMHDR nmhdr;
            public int dwFlags;
            public SYSTEMTIME st;
        }

        [StructLayout(LayoutKind.Sequential)]
        public class SYSTEMTIME
        {
            public short wYear;
            public short wMonth;
            public short wDayOfWeek;
            public short wDay;
            public short wHour;
            public short wMinute;
            public short wSecond;
            public short wMilliseconds;
            public override string ToString()
            {
                return ("[SYSTEMTIME: " + this.wDay.ToString(CultureInfo.InvariantCulture) + "/" + this.wMonth.ToString(CultureInfo.InvariantCulture) + "/" + this.wYear.ToString(CultureInfo.InvariantCulture) + " " + this.wHour.ToString(CultureInfo.InvariantCulture) + ":" + this.wMinute.ToString(CultureInfo.InvariantCulture) + ":" + this.wSecond.ToString(CultureInfo.InvariantCulture) + "]");
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct NMHDR
        {
            public IntPtr hwndFrom;
            public IntPtr idFrom;
            public int code;
        }
    }

}
