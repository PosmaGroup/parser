﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using SmartCadControls.Util;

namespace SmartCadControls.Controls
{
    public partial class TreeListEx : TreeList
    {

        protected override TreeListNode CreateNode(int nodeID, TreeListNodes owner, object Tag)
        {
            this.OptionsBehavior.AllowIndeterminateCheckState = true;
            
            return new TreeListNodeEx(nodeID, owner);
        }
        protected override void RaiseCellValueChanged(CellValueChangedEventArgs e)
        {
            TreeListNodeEx node = e.Node as TreeListNodeEx;
            if (e.Column == Columns[0])
            {
                node[e.Column] = e.Value;
                RefreshRowsInfo();
            }
            base.RaiseCellValueChanged(e);
        }
        protected override DevExpress.XtraEditors.Container.EditorContainerHelper CreateHelper()
        {
            return new CustomTreeListContainerHelper(this);
        }

    }

    public class CustomTreeListContainerHelper : TreeListContainerHelper
    {
        public CustomTreeListContainerHelper(TreeList owner) : base(owner) { }
        public override void ActivateEditor(DevExpress.XtraEditors.Repository.RepositoryItem ritem, DevExpress.XtraEditors.Container.UpdateEditorInfoArgs args)
        {
            args = new DevExpress.XtraEditors.Container.UpdateEditorInfoArgs(args.MakeReadOnly, args.Bounds, args.Appearance, Owner.FocusedNode.Tag, args.LookAndFeel, args.ErrorText);
            base.ActivateEditor(ritem, args);
        }
    }

}
