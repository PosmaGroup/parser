using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel;

namespace SmartCadControls.Controls
{
    public class WebBrowserEx : WebBrowser
    {
        int scrollTop = 0;
        int scrollLeft = 0;

        [Category("Key"), Description("This event is fired when a command key is pressed.")]
        public event KeyEventHandler KeyDownEx;
        
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (KeyDownEx != null)
            {
                const int WM_KEYDOWN = 0x100;
                const int WM_SYSKEYDOWN = 0x104;

                if ((msg.Msg == WM_KEYDOWN) || (msg.Msg == WM_SYSKEYDOWN))
                {
                    KeyDownEx(null, new KeyEventArgs(keyData));
                    return true;
                }
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        public WebBrowserEx()
        {
            this.DocumentText = "";
        }

        protected override void OnEnter(EventArgs e)
        {
            base.OnEnter(e);

            this.Document.Window.Focus();
        }

        protected override void OnDocumentCompleted(WebBrowserDocumentCompletedEventArgs e)
        {
            if (scrollLeft > 0)
                Document.Body.ScrollLeft = scrollLeft;
            if (scrollTop > 0)
                Document.Body.ScrollTop = scrollTop;
            scrollLeft = 0;
            scrollTop = 0;

            base.OnDocumentCompleted(e);

            this.Document.ContextMenuShowing += new HtmlElementEventHandler(Document_ContextMenuShowing);
        }
        
        private void Document_ContextMenuShowing(object sender, HtmlElementEventArgs e)
        {
            if ((e.ClientMousePosition.X > this.Document.Body.ScrollRectangle.Width ||
                e.ClientMousePosition.Y > this.Document.Body.ScrollRectangle.Height) &&
                this.Document.Url.IsFile == true)
            
                e.ReturnValue = true;
            else
                e.ReturnValue = false;
        }

        public void Navigate(string urlString, bool keepScrollIndex)
        {
            if (keepScrollIndex == true)
            {
                if (Document != null){ 
                    scrollLeft = Document.Body.ScrollLeft;
                    scrollTop = Document.Body.ScrollTop;
                }
            }
            Navigate(urlString);
        }

        public string XmlText
        {
            get
            {
                return this.DocumentText;
            }
            set
            {
                this.DocumentText = value;
            }
        }

        public void Clear()
        {
            try
            {
                this.DocumentText = "";
            }
            catch { }
        }

        
    }
}
