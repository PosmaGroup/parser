using System;
using System.Windows.Forms;

namespace SmartCadControls.Controls
{
    public class CheckedListBoxEx : CheckedListBox
    {
        public event ItemCheckEventHandler CheckedItem;
        private bool keyPress = false;

        public CheckedListBoxEx()
        {
            this.ItemCheck += new ItemCheckEventHandler(CheckedListBoxEx_ItemCheck);
            this.MouseClick += new MouseEventHandler(CheckedListBoxEx_MouseClick);
            this.Enter += new EventHandler(CheckedListBoxEx_Enter);
            this.LostFocus += new EventHandler(CheckedListBoxEx_LostFocus);

        }

        public void ClearCheckedItems() {

            if (this.CheckedItems.Count > 0)
            {
                for (int i = 0; i < this.Items.Count; i++)
                {
                    this.SetItemChecked(i, false);
                }
            }
        }
        void CheckedListBoxEx_Enter(object sender, EventArgs e)
        {
            if (this.Items.Count > 0)
                this.SelectedIndex = 0;
        }

        void CheckedListBoxEx_LostFocus(object sender, EventArgs e)
        {
            this.SelectedIndex = -1;
        }

        void CheckedListBoxEx_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if (keyPress == true)
            {
                e.NewValue = e.CurrentValue;
            }
            else
            {
                if (CheckedItem != null)
                    CheckedItem(sender, e);
            }
        }

        void CheckedListBoxEx_MouseClick(object sender, MouseEventArgs e)
        {
            this.keyPress = false;
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData != Keys.Space)
            {
                keyPress = true;
            }
            else
            {
                keyPress = false;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }


    }
}
