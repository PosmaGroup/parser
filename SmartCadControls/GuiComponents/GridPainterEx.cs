﻿using DevExpress.XtraGrid.Views.Grid.Drawing;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using SmartCadCore.Common;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadControls
{
    public class GridPainterEx : GridPainter
    {
        public GridPainterEx(GridViewEx gridView)
            : base(gridView)
        { }

        protected override void DrawRegularRowCell(GridViewDrawArgs e, GridCellInfo ci)
        {
            try
            {
                if (ci.CellValue == null && ci.Column != null &&
                    ci.RowHandle == DevExpress.XtraGrid.GridControl.AutoFilterRowHandle)
                {
                    Type gridType = (this.View.GridControl as GridControlEx).Type;
                    if (gridType != null)
                    {
                        object[] attributes = gridType.GetProperty(ci.Column.FieldName).GetCustomAttributes(typeof(GridControlRowInfoDataAttribute), false);
                        bool searchable = false;
                        if (attributes.Length > 0)
                        {
                            GridControlRowInfoDataAttribute attribute = (GridControlRowInfoDataAttribute)attributes[0];
                            searchable = attribute.Searchable;
                        }

                        int Y = Convert.ToInt32(ci.Bounds.Y * 1.2);
                        int X = Convert.ToInt32(ci.Bounds.X * 1);
                        Rectangle rect = new Rectangle(X, Y, ci.Bounds.Width, ci.Bounds.Height);
                        e.Graphics.FillRectangle(new SolidBrush(GridViewEx.FilterRowBackColor), ci.Bounds);
                        if (searchable && gridType.GetProperty(ci.Column.FieldName).PropertyType != typeof(bool))
                        {
                            Font newFont = new Font(ci.Appearance.Font.Name, 8.25f);
                            e.Graphics.DrawString(ResourceLoader.GetString2("Search") + "...", newFont, new SolidBrush(Color.Black), rect);
                        }
                    }
                    else
                    {
                        base.DrawRegularRowCell(e, ci);
                    }
                }
                else
                {
                    if (ci.CellValue != null && ci.Column != null &&
                    ci.RowHandle == DevExpress.XtraGrid.GridControl.AutoFilterRowHandle)
                    {
                        int Y = Convert.ToInt32(ci.Bounds.Y * 1.2);
                        int X = Convert.ToInt32(ci.Bounds.X * 1);
                        Rectangle rect = new Rectangle(X, Y, ci.Bounds.Width, ci.Bounds.Height);
                        e.Graphics.FillRectangle(new SolidBrush(GridViewEx.FilterRowBackColorWhenSomethingWritten), ci.Bounds);
                        if (ci.Column.ColumnType != typeof(bool))
                        {
                            Font newFont = new Font(ci.Appearance.Font.Name, 8.25f);
                            e.Graphics.DrawString(ci.CellValue as string, newFont, new SolidBrush(Color.Black), rect);
                        }
                    }
                    else
                    {
                        base.DrawRegularRowCell(e, ci);
                    }
                }
            }
            catch
            {
                base.DrawRegularRowCell(e, ci);
            }
        }
    }
}
