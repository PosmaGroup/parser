﻿namespace SmartCadControls.Controls
{
    partial class VmsControlEx
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.timerplayback = new System.Windows.Forms.Timer();
            this.SuspendLayout();
            // 
            // timerplayback
            // 
            this.timerplayback.Interval = 1000;
            this.timerplayback.Tick += new System.EventHandler(this.timerplayback_Tick);
            // 
            // VmsControlEx
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.MinimumSize = new System.Drawing.Size(100, 100);
            this.Name = "VmsControlEx";
            this.Size = new System.Drawing.Size(647, 552);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timerplayback;
    }
}
