using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using MyTimer = System.Threading.Timer;
using DevExpress.XtraEditors;
namespace SmartCadControls.Controls
{
    public partial class BlinkingSimpleButtonEx : SimpleButton
    {
        private bool blinking = false;
        private bool flag = true;
        private MyTimer myTimer;
        private int resolution = 500;

		public BlinkingSimpleButtonEx()
        {
            InitializeComponent();
        }        

        public bool Blinking
        {
            get
            {
                return blinking;
            }
            set
            {
                blinking = value;
                if (blinking == true)
                {
                    this.LookAndFeel.UseDefaultLookAndFeel = false;
                    if (myTimer == null)
                    {
                        myTimer = new MyTimer(new TimerCallback(delegate(object state)
                        {
                            if (blinking == true)
                            {
                                if (flag == true)
                                {
                                    flag = false;
                                    this.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Skin;
                                   
                                }
                                else
                                {
                                    flag = true;
									this.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
                                }
                            }
                        }
                        ), null, 0, resolution);
                    }
                    else
                        myTimer.Change(0, resolution);
                }
                else
                {
                    if (myTimer != null)
                        myTimer.Change(Timeout.Infinite, Timeout.Infinite);

					this.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Skin;
                }
                this.Invalidate();
            }
        }                
    }
}
