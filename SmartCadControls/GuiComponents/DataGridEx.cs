﻿using SmartCadControls.Controls;
using SmartCadControls.SyncBoxes;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadCore.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SmartCadControls
{
    public class DataGridEx : DataGridView
    {
        #region Const

        private const string KEY_COLUMN_NAME = "Key";
        private const string VISIBLE_COLUMN_NAME = "Visible";
        private const string VISIBLE_BY_TEXT_COLUMN_NAME = "VisibleByText";
        public const string GROUP_COLUMN_NAME = "Group";

        private const Keys CONTROL_TAB_KEYS = Keys.Control | Keys.Tab;
        private const Keys CONTROL_A_KEYS = Keys.Control | Keys.A;
        private const Keys CONTROL_C_KEYS = Keys.Control | Keys.C;
        private const Keys CONTROL_SHIFT_C_KEYS = (Keys)(Keys.Control | Keys.Shift | Keys.C);
        private const Keys CONTROL_INSERT = Keys.Control | Keys.Insert;

        private const int WM_VSCROLL = 0x115;
        private const int SB_ENDSCROLL = 0x8;
        private const int WM_CONTEXTMENU = 0x007B;

        private readonly int MINIMUM_COLUMN_WIDTH;
        #endregion

        #region Fields

        private DataTable table;
        private DataView view;
        private Type type;
        private Dictionary<string, DataGridExData> hash;
        private List<ListSortDirection> directions;
        private bool editing;

        private int selectedColumnIndex;

        private Color selectedItemBackColor;
        private Color selectedItemForeColor;
        private Color sortedColumnColor;
        private Color columnHeadersBackColor;

        private System.Threading.Timer timer;
        private EventArgs timerEventArgs;
        private int previousHorizontalScrollOffset;
        private int lastKeyRowIndex = -1;
        private bool enableSystemShortCuts = false;
        private bool enabledSelectionHandler;
        private bool comboBoxEventAdded = false;
        //private bool dateTimePickerEventAdded = false;
        private bool mgroping;
        private bool allowEditing = false;
        private bool allowUserToSortColumns = true;

        private ToolStripMenuItem groupMenuItem = new ToolStripMenuItem(ResourceLoader.GetString2("GroupingName"));
        private ToolStripMenuItem collapseAllMenuItem = new ToolStripMenuItem(ResourceLoader.GetString2("CollapseAllName"));
        private ToolStripMenuItem expandAllMenuItem = new ToolStripMenuItem(ResourceLoader.GetString2("ExpandAllName"));
        private ContextMenuStrip popUpMenu = new ContextMenuStrip();
        private Point actualPostion = new Point();


        #endregion

        public DataGridEx()
        {
            //Things of OutLook Look and Feel
            base.RowTemplate = new DataGridRowEx();
            this.groupTemplate = new DatagGridDefaultGroup();

            AllowUserToDeleteRows = false;
            AllowUserToOrderColumns = true;
            AllowUserToResizeRows = false;
            AllowUserToResizeColumns = true;
            AllowUserToAddRows = false;
            MultiSelect = false;
            SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            ColumnHeadersHeight = 25;
            DoubleBuffered = true;
            selectedColumnIndex = -1;
            VirtualMode = true;
            AllowDrop = true;
            BackgroundColor = Color.White;
            EnableHeadersVisualStyles = false;
            EditMode = DataGridViewEditMode.EditOnEnter;
            timer = new System.Threading.Timer(new TimerCallback(OnTimer));
            timerEventArgs = null;

            MINIMUM_COLUMN_WIDTH = 30;
            EnabledSelectionHandler = true;
            ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;

            directions = new List<ListSortDirection>();


            groupMenuItem.CheckOnClick = true;
            groupMenuItem.Checked = this.Grouping;
            groupMenuItem.Click += new EventHandler(groupMenuItem_Click);

            collapseAllMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            collapseAllMenuItem.ImageScaling = ToolStripItemImageScaling.None;
            collapseAllMenuItem.Image = this.ExpandIcon;
            collapseAllMenuItem.Click += new EventHandler(collapseAllMenuItem_Click);

            expandAllMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            expandAllMenuItem.ImageScaling = ToolStripItemImageScaling.None;
            expandAllMenuItem.Image = this.CollapseIcon;
            expandAllMenuItem.Click += new EventHandler(expandAllMenuItem_Click);

            popUpMenu.Items.Add(groupMenuItem);
            popUpMenu.Items.Add(collapseAllMenuItem);
            popUpMenu.Items.Add(expandAllMenuItem);

        }



        #region Properties


        public new DataGridViewRow RowTemplate
        {
            get { return base.RowTemplate; }
        }

        private IDataGridGroup groupTemplate;

        public IDataGridGroup GroupTemplate
        {
            get
            {
                return groupTemplate;
            }
            set
            {
                groupTemplate = value;
            }
        }

        private Image iconCollapse = ResourceLoader.GetImage("CollapseIcon");

        public Image CollapseIcon
        {
            get { return iconCollapse; }
        }

        private Image iconExpand = ResourceLoader.GetImage("ExpandIcon");

        public Image ExpandIcon
        {
            get
            {
                return iconExpand;
            }
        }

        public bool EnableSystemShortCuts
        {
            get
            {
                return enableSystemShortCuts;
            }
            set
            {
                enableSystemShortCuts = value;
            }
        }

        public DataTable Table
        {
            get
            {
                return table;
            }
        }

        public IDictionary<string, DataGridExData> Hash
        {
            get
            {
                return hash;
            }
        }

        public DataView View
        {
            get
            {
                return view;
            }
        }

        public Type Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value;
                if (type != null)
                {
                    ResetColumns();
                }
            }
        }

        public Color SelectedItemBackColor
        {
            get
            {
                return selectedItemBackColor;
            }
            set
            {
                if (value.IsEmpty == false)
                {
                    selectedItemBackColor = value;

                    this.DefaultCellStyle.SelectionBackColor = selectedItemBackColor;
                }
            }
        }

        public bool EnabledSelectionHandler
        {
            get
            {
                return enabledSelectionHandler;
            }
            set
            {
                enabledSelectionHandler = value;
            }
        }

        private void AcceptChanges()
        {
            try
            {
                this.table.BeginLoadData();
                try
                {
                    for (int i = 0; i < this.table.Rows.Count; i++)
                    {
                        FieldInfo fi = this.table.Rows[i].GetType().GetField("_rowID", BindingFlags.NonPublic | BindingFlags.Instance);
                        int fiValue = (int)fi.GetValue(this.table.Rows[i]);
                        if (fiValue != -1)
                        {
                            this.table.Rows[i].AcceptChanges();
                        }
                    }
                }
                finally
                {
                    this.table.EndLoadData();
                }
            }
            catch
            {

            }
        }

        public Color SelectedItemForeColor
        {
            get
            {
                return selectedItemForeColor;
            }
            set
            {
                if (value.IsEmpty == false)
                {
                    selectedItemForeColor = value;

                    this.DefaultCellStyle.SelectionForeColor = selectedItemForeColor;
                }
            }
        }

        public Color SortedColumnColor
        {
            get
            {
                return sortedColumnColor;
            }
            set
            {
                sortedColumnColor = value;
            }
        }

        public Color ColumnHeadersBackColor
        {
            get
            {
                return columnHeadersBackColor;
            }
            set
            {
                columnHeadersBackColor = value;

                this.ColumnHeadersDefaultCellStyle.BackColor = columnHeadersBackColor;
            }
        }

        public bool Editing
        {
            get
            {
                return editing;
            }
            set
            {
                editing = value;
            }
        }

        public bool Grouping
        {
            get { return mgroping; }
            set
            {
                if (value == true)
                {
                    groupMenuItem.Checked = true;
                }
                else
                {
                    groupMenuItem.Enabled = false;
                    groupMenuItem.Visible = false;
                    collapseAllMenuItem.Enabled = false;
                    collapseAllMenuItem.Visible = false;
                    expandAllMenuItem.Enabled = false;
                    expandAllMenuItem.Visible = false;
                }
                mgroping = value;
            }
        }

        #endregion

        #region Private util methods

        private void expandAllMenuItem_Click(object sender, EventArgs e)
        {
            if (this.Grouping)
                ExpandAll();
        }

        private void collapseAllMenuItem_Click(object sender, EventArgs e)
        {
            if (this.Grouping)
                CollapseAll();
        }

        private void groupMenuItem_Click(object sender, EventArgs e)
        {
            this.Grouping = !this.Grouping;
            if (this.mgroping == false)
            {
                ClearRowsCategory();
                SortByColumn(selectedColumnIndex, directions[selectedColumnIndex]);
                this.PerformMouseDownEmptyArea();
            }
        }

        private void OnTimer(object state)
        {
            timer.Change(Timeout.Infinite, Timeout.Infinite);

            if (timerEventArgs != null)
            {
                if (this.InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(delegate
                    {
                        OnSelectionChanged_Help(timerEventArgs);
                    }));
                }
                else
                {
                    OnSelectionChanged_Help(timerEventArgs);
                }
            }
        }


        private void OnSelectionChanged_Help(EventArgs e)
        {
            base.OnSelectionChanged(e);
        }

        protected override void OnColumnWidthChanged(DataGridViewColumnEventArgs e)
        {
            base.OnColumnWidthChanged(e);
            if (e.Column.Width < MINIMUM_COLUMN_WIDTH)
            {
                e.Column.Width = MINIMUM_COLUMN_WIDTH;
            }
        }

        private void AddColumn(string propertyName, string headerText, int width, Type valueType, bool visible, Type columnType, DataGridViewContentAlignment headerStyle, string toolTip, bool readOnly)
        {
            DataGridViewColumn gridColumn = null;
            if (columnType == typeof(DataGridViewImageColumn))
            {
                gridColumn = new DataGridViewImageColumn();
            }
            else if (columnType == typeof(DataGridViewExButtonColumn))
            {
                gridColumn = new DataGridViewExButtonColumn();
            }
            else if (columnType == typeof(DataGridViewCheckBoxColumn))
            {
                gridColumn = new DataGridViewCheckBoxColumn();
            }
            else if (columnType == typeof(DataGridViewLinkColumn))
            {
                gridColumn = new DataGridViewLinkColumn();
            }
            else if (columnType == typeof(DataGridViewComboBoxColumn))
            {
                gridColumn = new DataGridViewComboBoxColumn();
            }
            else if (columnType == typeof(DataGridViewCheckBoxColumn))
            {
                gridColumn = new DataGridViewCheckBoxColumn();
                ((DataGridViewCheckBoxColumn)gridColumn).FalseValue = "false";
                ((DataGridViewCheckBoxColumn)gridColumn).TrueValue = "true";
            }
            else if (columnType == typeof(TextAndImageColumn))
            {
                gridColumn = new TextAndImageColumn();
            }
            else if (columnType == typeof(IntColumn))
            {
                int len = GetMaxLength(propertyName);
                gridColumn = new IntColumn(len);
            }
            else if (columnType == typeof(NumericColumn))
            {
                int len = GetMaxLength(propertyName);
                gridColumn = new NumericColumn(len);
            }
            else if (columnType == typeof(CalendarTimeColumn))
            {
                gridColumn = new CalendarTimeColumn();

            }
            else if (columnType == typeof(CalendarDateColumn))
            {
                gridColumn = new CalendarDateColumn();
            }
            else if (columnType == typeof(DataGridViewColorColumn))
            {

                gridColumn = new DataGridViewColorColumn();
            }
            else
            {
                int len = GetMaxLength(propertyName);
                gridColumn = new TextColumn(len);
            }

            gridColumn.DefaultHeaderCellType = typeof(DataGridViewImageColumnHeaderCell);

            gridColumn.Name = propertyName;
            gridColumn.DataPropertyName = propertyName;
            gridColumn.HeaderText = headerText;
            gridColumn.ReadOnly = readOnly;
            gridColumn.Visible = visible;
            //things of outlook
            gridColumn.SortMode = DataGridViewColumnSortMode.Programmatic;

            gridColumn.Resizable = DataGridViewTriState.True;
            gridColumn.ToolTipText = toolTip;
            gridColumn.HeaderCell.Style.Alignment = headerStyle;

            gridColumn.ValueType = valueType;

            if (width > 0)
                gridColumn.Width = width;

            Columns.Add(gridColumn);

            DataColumn dataColumn = null;
            if (valueType != null)
                dataColumn = new DataColumn(propertyName, valueType);
            else
                dataColumn = new DataColumn();

            dataColumn.ColumnName = propertyName;
            dataColumn.Caption = headerText;

            table.Columns.Add(dataColumn);
        }

        void Control_EventHandler(object sender, EventArgs e)
        {
            object value = null;
            if (sender is ComboBox)
            {
                ComboBox cb = sender as ComboBox;
                value = cb.SelectedItem;

            }
            else if (sender is CheckBox)
            {
                CheckBox cb = sender as CheckBox;
                value = cb.Checked;
            }
            else if (sender is DateTimePicker30MinutesChangeEx)
            {
                DateTimePicker30MinutesChangeEx dt = sender as DateTimePicker30MinutesChangeEx;
                value = dt.Value;
            }

            UpdateDataValue(value);
        }

        private void UpdateDataValue(object value)
        {
            DataGridExData row = GetData(CurrentCell.RowIndex);

            Type typeDataGridExData = row.GetType();

            typeDataGridExData.GetProperty(Columns[CurrentCell.ColumnIndex].Name).SetValue(row, value, null);

            UpdateData(row);
        }

        private void UpdateDataValueCell(object value, DataGridViewCell cell)
        {
            DataGridExData row = GetData(cell.RowIndex);

            Type typeDataGridExData = row.GetType();

            typeDataGridExData.GetProperty(Columns[cell.ColumnIndex].Name).SetValue(row, value, null);

            UpdateData(row);
        }

        private void ResetColumns()
        {
            Columns.Clear();

            table = new DataTable();
            hash = new Dictionary<string, DataGridExData>();

            PropertyInfo[] properties = type.GetProperties();

            foreach (PropertyInfo propertyInfo in properties)
            {
                object[] attributes = propertyInfo.GetCustomAttributes(typeof(DataGridExColumnAttribute), false);

                if (attributes.Length > 0)
                {
                    DataGridExColumnAttribute attribute = (DataGridExColumnAttribute)attributes[0];
                    AddColumn(propertyInfo.Name, ResourceLoader.GetString2(attribute.HeaderText), attribute.Width, attribute.Type, attribute.Visible, attribute.Column, attribute.HeaderAlignment, attribute.ToolTip, attribute.ReadOnly);
                }
            }

            AddColumn(KEY_COLUMN_NAME, KEY_COLUMN_NAME, 0, null, false, null, DataGridViewContentAlignment.NotSet, null, true);
            AddColumn(VISIBLE_COLUMN_NAME, VISIBLE_COLUMN_NAME, 0, null, false, null, DataGridViewContentAlignment.NotSet, null, true);
            AddColumn(VISIBLE_BY_TEXT_COLUMN_NAME, VISIBLE_BY_TEXT_COLUMN_NAME, 0, null, false, null, DataGridViewContentAlignment.NotSet, null, true);
            AddColumn(GROUP_COLUMN_NAME, GROUP_COLUMN_NAME, 0, null, false, null, DataGridViewContentAlignment.NotSet, null, true);

            table.BeginLoadData();
            view = table.DefaultView;

            view.RowFilter = "(" + VISIBLE_COLUMN_NAME + "= 'true') AND (" + VISIBLE_BY_TEXT_COLUMN_NAME + " = 'true')";

            DataSource = view;
            table.EndLoadData();

            RowHeadersVisible = false;
        }

        private int GetMaxLength(string columnName)
        {
            PropertyInfo[] properties = type.GetProperties();

            foreach (PropertyInfo propertyInfo in properties)
            {
                if (propertyInfo.Name == columnName)
                {

                    object[] attributes = propertyInfo.GetCustomAttributes(typeof(DataGridExColumnAttribute), false);

                    if (attributes.Length > 0)
                    {
                        DataGridExColumnAttribute attribute = (DataGridExColumnAttribute)attributes[0];
                        return attribute.MaxLength;
                    }
                }
            }
            return 1000;
        }


        /// <summary>
        /// Creates an object containing date of dragged item.
        /// </summary>
        /// <returns>DragItemData object</returns>
        private GridDragItemData GetDataForDragDrop()
        {
            // create a drag item data object that will be used to pass along with the drag and drop
            GridDragItemData data = new GridDragItemData(this);

            // go through each of the selected items and 
            // add them to the drag items collection
            // by creating a clone of the list item
            foreach (DataGridExData item in this.SelectedItems)
            {
                data.DragItems.Add(item);
            }

            return data;
        }

        private void SelectFirstLetterInAColumn(int keyValue)
        {
            if (Char.IsLetterOrDigit(Convert.ToChar(keyValue)))
            {
                ClearSelection();

                if (0 <= selectedColumnIndex && selectedColumnIndex < Columns.Count)
                {

                    if (SearchFirstLetterInAColumn(keyValue, lastKeyRowIndex + 1, Rows.Count) == false)
                    {
                        SearchFirstLetterInAColumn(keyValue, 0, lastKeyRowIndex + 1);
                    }
                }
                else
                {
                    lastKeyRowIndex = -1;
                }
            }
        }

        private bool SearchFirstLetterInAColumn(int keyValue, int start, int end)
        {
            for (int i = start; (i < end && Rows.Count > 0); i++)
            {
                DataGridViewRow row = Rows[i];

                string val = row.Cells[selectedColumnIndex].Value.ToString();

                if (val.Length > 0 && ((Char.ToUpper(val[0]) == keyValue || Char.ToLower(val[0]) == keyValue) ||
                                      ((Keys)keyValue == Keys.Oemtilde && (Char.ToUpper(val[0]) == 'n' || Char.ToLower(val[0]) == 'n'))))
                {
                    this.CurrentCell = this[this.CurrentCell.ColumnIndex, row.Index];
                    this.Rows[row.Index].Selected = true;
                    lastKeyRowIndex = row.Index;
                    return true;
                }
            }
            return false;
        }
        #endregion

        #region Protected methods

        protected override void OnMouseClick(MouseEventArgs e)
        {
            actualPostion.X = e.X;
            actualPostion.Y = e.Y;
            base.OnMouseClick(e);
        }



        protected override void OnCellBeginEdit(DataGridViewCellCancelEventArgs e)
        {
            DataRow row = ((DataRowView)Rows[e.RowIndex].DataBoundItem).Row;
            if (row.RowState == DataRowState.Deleted)
            {
                return;
            }
            if (IsGroupRow(row) == true)
                e.Cancel = true;
            else
                base.OnCellBeginEdit(e);
        }

        protected override void OnCellDoubleClick(DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataRow row = ((DataRowView)Rows[e.RowIndex].DataBoundItem).Row;
                DataGridRowEx gridRow = (DataGridRowEx)base.Rows[e.RowIndex];
                if (IsGroupRow(row) == true)
                {
                    GroupRows(row[GROUP_COLUMN_NAME].ToString(), gridRow);
                }
            }
            base.OnCellClick(e);
        }

        private void PrintMatix()
        {
            foreach (DataColumn column in table.Rows[0].Table.Columns)
            {
                Console.Write(column.ColumnName + " ");
            }
            Console.WriteLine();
            for (int i = 0; i < table.Rows.Count; i++)
            {
                foreach (object obj in table.Rows[i].ItemArray)
                {
                    Console.Write(obj.ToString() + " ");
                }
                Console.WriteLine();
            }
        }

        protected override void OnCellMouseDown(DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex < 0)
            {
                base.OnCellMouseDown(e);
            }
            else
            {
                DataRow row = ((DataRowView)Rows[e.RowIndex].DataBoundItem).Row;
                DataGridRowEx gridRow = (DataGridRowEx)base.Rows[e.RowIndex];
                bool isgroup = IsGroupRow(row);
                if (isgroup == true && gridRow.IsIconHit(e))
                {
                    GroupRows(row[GROUP_COLUMN_NAME].ToString(), gridRow);
                }
                else if (isgroup == false)
                {
                    base.OnCellMouseDown(e);
                }
            }
        }

        private void GroupRows(string groupName, DataGridRowEx gridRow)
        {
            gridRow.Group.Collapsed = !gridRow.Group.Collapsed;

            DataRow[] rows = GetRows(GROUP_COLUMN_NAME + " = '" + groupName.Substring(1) + "'");

            for (int i = 0; i < rows.Length; i++)
            {
                DataRow row = rows[i];

                UpdateRow(row[KEY_COLUMN_NAME].ToString(), VISIBLE_COLUMN_NAME, !IsVisibleRow(row[KEY_COLUMN_NAME].ToString()));
                UpdateRow(row[KEY_COLUMN_NAME].ToString(), VISIBLE_BY_TEXT_COLUMN_NAME, row[VISIBLE_COLUMN_NAME]);
            }
        }

        protected override void OnCurrentCellDirtyStateChanged(EventArgs e)
        {
            if (this.IsCurrentCellDirty)
            {
                this.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        protected override void OnCellValueChanged(DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                DataGridViewCell cell = this.Rows[e.RowIndex].Cells[e.ColumnIndex] as DataGridViewCell;

                if (cell is DataGridViewCheckBoxCell)
                {
                    bool check = false;
                    bool.TryParse(cell.Value.ToString(), out check);
                    UpdateDataValueCell(check, cell);
                }
                else if (cell is DataGridViewComboBoxCell)
                {
                    UpdateDataValueCell(cell.Value.ToString(), cell);
                }
                else if (cell is NumericCell)
                {
                    string temp = cell.Value.ToString();
                    if (string.IsNullOrEmpty(temp.Trim(',', '.', ' ')) == false)
                    {
                        UpdateDataValueCell(temp, cell);
                    }
                    else
                    {
                        UpdateDataValueCell("0", cell);
                    }

                }
                else if (cell is IntCell)
                {
                    if (string.IsNullOrEmpty(cell.Value.ToString()) == false)
                    {
                        UpdateDataValueCell(cell.Value.ToString(), cell);
                    }
                    else
                    {
                        UpdateDataValueCell("0", cell);
                    }

                }
                else if (cell is CalendarDateCell || cell is CalendarTimeCell)
                {
                    UpdateDataValue(cell.Value);

                }
                else
                {
                    UpdateDataValueCell(cell.Value.ToString(), cell);
                }
            }

            base.OnCellValueChanged(e);

        }

        protected override void OnEditingControlShowing(DataGridViewEditingControlShowingEventArgs e)
        {
            base.OnEditingControlShowing(e);
            if (e.Control is ComboBox && comboBoxEventAdded == false)
            {
                ComboBox cb = e.Control as ComboBox;
                cb.SelectionChangeCommitted += new EventHandler(Control_EventHandler);
                comboBoxEventAdded = true;
            }
            //else if (e.Control is DateTimePicker30MinutesChangeEx)
            //{
            //    DateTimePicker30MinutesChangeEx dt = e.Control as DateTimePicker30MinutesChangeEx;
            //    dt.ValueChanged += new EventHandler(Control_EventHandler);
            //    dateTimePickerEventAdded = true;
            //}



        }

        protected override void OnDragEnter(DragEventArgs drgevent)
        {
            base.OnDragEnter(drgevent);

            drgevent.Effect = DragDropEffects.Move;
        }

        public event MouseEventHandler CustomMouseDoubleClick;

        public void PerformMouseDownEmptyArea()
        {
            MouseEventArgs e = new MouseEventArgs(MouseButtons.Left, 1, this.Right, this.Bottom, 0);
            OnMouseDown(e);
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);

            if (e.Button == MouseButtons.Left && e.Clicks == 2)
            {
                if (this.CustomMouseDoubleClick != null)
                    this.CustomMouseDoubleClick(this, e);
            }
            else if (e.Button == MouseButtons.Left)
            {
                DataGridView.HitTestInfo info = HitTest(e.X, e.Y);
                if (info.RowIndex >= 0)
                {
                    //Rows[info.RowIndex].Selected = true;

                    DataRowView view = (DataRowView)Rows[info.RowIndex].DataBoundItem;

                    bool columnDragDrop = ColumnAllowDoDragDrop(info.ColumnIndex);
                    if (view != null && columnDragDrop)
                    {
                        DoDragDrop(GetDataForDragDrop(), DragDropEffects.Move);
                    }
                    try
                    {
                        OnCellMouseClick(new DataGridViewCellMouseEventArgs(info.ColumnIndex, info.RowIndex, e.X, e.Y, e));
                    }
                    catch { }

                    base.OnMouseLeave(e);
                }
                else if (info.Type != DataGridViewHitTestType.ColumnHeader)
                {
                    ClearSelection();
                }
            }
            else if (e.Button == MouseButtons.Right)
            {
                DataGridView.HitTestInfo info = HitTest(e.X, e.Y);

                if (info.RowIndex >= 0)
                {
                    if (Rows[info.RowIndex].Selected == false)
                    {
                        ClearSelection();
                        Rows[info.RowIndex].Selected = true;
                    }
                }
                else
                {
                    ClearSelection();
                }
            }
        }

        private bool ColumnAllowDoDragDrop(int columnIndex)
        {
            if (this.Columns[columnIndex] is DataGridViewComboBoxColumn)
                return false;
            if (this.Columns[columnIndex] is DataGridViewCheckBoxColumn)
                return false;
            return true;
        }

        private bool ColumnAllowSorting(int columnIndex)
        {
            return ColumnAllowDoDragDrop(columnIndex);
        }


        protected override void OnRowPrePaint(DataGridViewRowPrePaintEventArgs e)
        {
            base.OnRowPrePaint(e);

            try
            {
                int rowIndex = e.RowIndex;

                if (0 <= rowIndex && rowIndex < Rows.Count)
                {
                    DataGridViewRow row = Rows[rowIndex];
                    string key = row.Cells[KEY_COLUMN_NAME].Value.ToString();

                    if (row.Selected)
                    {
                        row.DefaultCellStyle.BackColor = selectedItemBackColor;
                    }
                    else
                    {
                        row.DefaultCellStyle.BackColor = hash[key].BackColor;
                    }

                    if (0 <= selectedColumnIndex && selectedColumnIndex < Columns.Count)
                        row.Cells[selectedColumnIndex].Style.BackColor = sortedColumnColor;
                }
            }
            catch
            {
                // TODO: Ver que hacer con la exception.
            }
        }

        protected override void OnColumnHeaderMouseClick(DataGridViewCellMouseEventArgs e)
        {

            if (e.Button == MouseButtons.Left)
            {
                int horizontalOffset = HorizontalScrollingOffset;

                base.OnColumnHeaderMouseClick(e);

                HorizontalScrollingOffset = horizontalOffset;

                ClearSelection();

                DataGridViewColumn column;

                if (0 <= selectedColumnIndex && selectedColumnIndex < Columns.Count && (AllowUserToSortColumns == true))
                {
                    column = Columns[selectedColumnIndex];

                    column.DefaultCellStyle.BackColor = Color.White;
                }

                selectedColumnIndex = e.ColumnIndex;
                if (ColumnAllowSorting(e.ColumnIndex) && (AllowUserToSortColumns == true))
                {
                    ClearRowsCategory();
                    GetSetColumnSortDirection(e.ColumnIndex);
                    SortByColumn(e.ColumnIndex, directions[e.ColumnIndex]);
                }
                ClearSelection();
            }
            else if (e.Button == MouseButtons.Right)
            {
                selectedColumnIndex = e.ColumnIndex;
                popUpMenu.Show(this, actualPostion.X, actualPostion.Y);
            }
        }

        private void GetSetColumnSortDirection(int columnIndex)
        {
            if (directions[columnIndex] == ListSortDirection.Ascending)
                directions[columnIndex] = ListSortDirection.Descending;
            else
                directions[columnIndex] = ListSortDirection.Ascending;
        }

        private void ResetStyleColumnHeader(int excludedColumn)
        {
            if (this.Columns != null)
            {
                DataGridViewColumn column = null;
                for (int i = 0; i < this.ColumnCount; i++)
                {
                    if (i != excludedColumn)
                    {
                        column = Columns[i];
                        this.InvalidateCell(column.HeaderCell);
                    }
                }
            }
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            DataGridViewColumn column;
            HitTestInfo info = HitTest(e.X, e.Y);
            if (info.ColumnIndex > -1)
            {
                ResetStyleColumnHeader(info.ColumnIndex);
                column = Columns[info.ColumnIndex];

                if (info.RowIndex == -1)
                {
                    if (e.Button == MouseButtons.None)
                    {
                        Graphics g = Graphics.FromHwnd(this.Handle);
                        SolidBrush brush = new SolidBrush(Color.Orange);
                        Rectangle rect = new Rectangle(info.ColumnX, column.HeaderCell.ContentBounds.Bottom + 2, column.HeaderCell.Size.Width, 2);
                        g.FillRectangle(brush, rect);
                        g.Dispose();
                    }
                }
                else
                {
                    this.InvalidateCell(column.HeaderCell);
                }
            }
            else
            {
                ResetStyleColumnHeader(-1);
            }
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            base.OnMouseLeave(e);
            ResetStyleColumnHeader(-1);
        }

        public event EventHandler SelectionWillChange;

        protected virtual void OnSelectionWillChange(EventArgs e)
        {
            if (EnabledSelectionHandler == true && SelectionWillChange != null)
            {
                SelectionWillChange(this, e);
            }
        }

        public void DelayTimerToInfinite()
        {
            timer.Change(Timeout.Infinite, Timeout.Infinite);
        }

        protected override void OnSelectionChanged(EventArgs e)
        {
            if (EnabledSelectionHandler == true && editing == false && this.timer != null && this.Disposing == false)
            {
                OnSelectionWillChange(EventArgs.Empty);

                timerEventArgs = e;
                timer.Change(1000, Timeout.Infinite);
            }
        }

        protected override void OnDataError(bool displayErrorDialogIfNoHandler, DataGridViewDataErrorEventArgs e)
        {
            base.OnDataError(false, e);
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            Keys keys = e.KeyData & Keys.KeyCode;
            if ((Keys)e.KeyValue >= Keys.NumPad0 && (Keys)e.KeyValue <= Keys.NumPad9)
            {
                e = new KeyEventArgs((Keys)(e.KeyValue - (int)Keys.D0));
            }
            if (keys == Keys.Enter || (e.KeyData == CONTROL_TAB_KEYS) || (e.KeyData == CONTROL_A_KEYS) ||
                (e.KeyData == CONTROL_C_KEYS) || (e.KeyData == CONTROL_INSERT) ||
                (e.KeyData == CONTROL_SHIFT_C_KEYS) || (keys == Keys.F5))
            //Falta Filtrar Ctrl + Shift + C (Redefinir constante CONTROL_SHIFT_C_KEYS). No funciona en primer nivel
            {
                FieldInfo fi = typeof(System.Windows.Forms.Control).GetField("EventKeyDown", BindingFlags.NonPublic | BindingFlags.Static);
                object fiValue = fi.GetValue(null);
                KeyEventHandler handler = (KeyEventHandler)base.Events[fiValue];
                if (handler != null)
                {
                    handler(this, e);
                }
            }
            else
            {
                base.OnKeyDown(e);
            }

            //if ((keys != Keys.F1) && (keys != Keys.F2) && (keys != Keys.F3) && (keys != Keys.F4) &&
            //    (keys != Keys.F5) && (keys != Keys.F6) && (keys != Keys.F6) && (keys != Keys.F7) &&
            //    (keys != Keys.F8) && (keys != Keys.F9) && (keys != Keys.F10) && (e.Alt == false) && 
            //    (e.Control == false) && (e.Shift == false) && (keys != Keys.Home) && (keys != Keys.LWin))
            if (e.Control == false && e.Alt == false && e.Shift == false)
            {
                if (((int)keys >= (int)Keys.D0 && (int)keys <= (int)Keys.D9) ||
                    ((int)keys >= (int)Keys.NumPad0 && (int)keys <= (int)Keys.NumPad9) ||
                    ((int)keys >= (int)Keys.A && (int)keys <= (int)Keys.Z) ||
                    ((int)keys == (int)Keys.Oemtilde))
                {
                    SelectFirstLetterInAColumn(e.KeyValue);
                }
            }
        }

        protected override void OnRowEnter(DataGridViewCellEventArgs e)
        {
            lastKeyRowIndex = e.RowIndex;
            base.OnRowEnter(e);
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            const int WM_KEYDOWN = 0x100;
            const int WM_SYSKEYDOWN = 0x104;

            if ((msg.Msg == WM_KEYDOWN) || (msg.Msg == WM_SYSKEYDOWN))
            {
                switch (keyData)
                {
                    case (CONTROL_TAB_KEYS):
                        {
                            OnKeyDown(new KeyEventArgs(keyData));
                            return true;
                        }
                    default:
                        break;
                }
                if (keyData != Keys.Tab)
                {
                    if (AllowEditing == true)
                    {
                        if (this.CurrentCell is IntCell)
                        {
                            IntCell cell = (IntCell)this.CurrentCell;
                            // OnKeyDown(new KeyEventArgs(keyData));

                            if (cell.ProcessKeyDown(keyData) == false)
                            {
                                return true;
                            }
                        }
                        if (this.CurrentCell is NumericCell)
                        {
                            NumericCell cell = (NumericCell)this.CurrentCell;
                            // OnKeyDown(new KeyEventArgs(keyData));

                            if (cell.ProcessKeyDown(keyData) == false)
                            {
                                return true;
                            }

                        }
                    }
                    else if (EnableSystemShortCuts == false)
                    {
                        OnKeyDown(new KeyEventArgs(keyData));
                        return true;
                    }

                }

            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        protected override void Dispose(bool disposing)
        {
            if (base.IsDisposed != false && base.Disposing != false)
            {
                base.Dispose(disposing);
                if (timer != null)
                    timer.Dispose();
            }
        }

        protected override void OnColumnAdded(DataGridViewColumnEventArgs e)
        {
            directions.Add(ListSortDirection.Ascending);
            base.OnColumnAdded(e);
        }

        #endregion

        #region Public methods

        public void CollapseAll()
        {
            SetGroupCollapse(true);
        }

        public void ExpandAll()
        {
            SetGroupCollapse(false);
        }

        public void ClearGroups()
        {
            groupTemplate.Column = null; //reset
            FillGrid(null);
        }

        private void SetGroupCollapse(bool collapsed)
        {
            if (Rows.Count == 0) return;
            if (groupTemplate == null) return;

            bool existGroup = false;
            foreach (DataRow row in table.Rows)
            {
                if (IsGroupRow(row) == true)
                {
                    existGroup = true;
                    break;
                }
            }

            if (!existGroup)
                return;

            BeginChanges();
            foreach (DataRow row in table.Rows)
            {
                if (IsGroupRow(row) == true)
                {
                    string groupName = row[DataGridEx.GROUP_COLUMN_NAME].ToString().ToLower();
                    groups[groupName.Substring(1)].Collapsed = collapsed;
                }
                else
                {
                    UpdateRow(row[KEY_COLUMN_NAME].ToString(), VISIBLE_COLUMN_NAME, !collapsed);
                    UpdateRow(row[KEY_COLUMN_NAME].ToString(), VISIBLE_BY_TEXT_COLUMN_NAME, !collapsed);
                }
            }
            EndChanges(false);
        }

        public void SortByColumn(int columnIndex, ListSortDirection direction)
        {
            //Sort(Columns[columnIndex], direction);
            string order = "ASC";
            if (direction == ListSortDirection.Descending)
                order = "DESC";

            if (Grouping)
            {
                table.DefaultView.Sort = Columns[columnIndex].Name + " " + order + ", " + Columns[GROUP_COLUMN_NAME].Name + " ASC";
                FillGrid(groupTemplate);
            }
            else
            {
                table.DefaultView.Sort = Columns[columnIndex].Name + " " + order;
            }
        }

        public void SortByColumns(string sortText)
        {
            if (Grouping == true)
            {
                table.DefaultView.Sort = sortText + ", " + Columns[GROUP_COLUMN_NAME].Name + " ASC";
                FillGrid(GroupTemplate);
            }
            else
            {
                table.DefaultView.Sort = sortText;
            }
            //TODO:
            //Update direction.
        }

        public Dictionary<object, IDataGridGroup> groups = new Dictionary<object, IDataGridGroup>();

        private void FillGrid(IDataGridGroup groupingStyle)
        {
            if (Rows.Count <= 0) return;

            DataRow row;
            if (groupingStyle != null && selectedColumnIndex >= 0)
            {
                IDataGridGroup groupCur = null;
                object result = null;
                int counter = 0; // counts number of items in the group

                List<DataRow> list = new List<DataRow>();

                foreach (DataGridViewRow var in Rows)
                {
                    DataRowView drv = var.DataBoundItem as DataRowView;
                    list.Add(drv.Row);
                }

                Dictionary<object, IDataGridGroup> auxGroups = new Dictionary<object, IDataGridGroup>(groups);

                groups.Clear();

                int listIndex = 0;

                BeginChanges();
                foreach (DataRow r in list)
                {
                    result = r[selectedColumnIndex];
                    if (groupCur != null && groupCur.CompareTo(result) == 0) // item is part of the group
                    {
                        UpdateRow(r[KEY_COLUMN_NAME].ToString(), GROUP_COLUMN_NAME, groupCur.Value);
                        UpdateRow(r[KEY_COLUMN_NAME].ToString(), VISIBLE_COLUMN_NAME, !groupCur.Collapsed);
                        UpdateRow(r[KEY_COLUMN_NAME].ToString(), VISIBLE_BY_TEXT_COLUMN_NAME, !groupCur.Collapsed);
                        counter++;
                        listIndex += 1;
                    }
                    else
                    {
                        row = table.NewRow();

                        if (groupCur != null)
                            groupCur.ItemCount = counter;

                        groupCur = (IDataGridGroup)groupingStyle.Clone(); // init
                        groupCur.Value = result;
                        groupCur.Column = Columns[selectedColumnIndex];
                        if (auxGroups.ContainsKey(result.ToString()) == true)
                        {
                            groupCur.Collapsed = auxGroups[result.ToString()].Collapsed;
                        }
                        groups.Add(result.ToString(), groupCur);
                        row[selectedColumnIndex] = result;
                        row[GROUP_COLUMN_NAME] = "$" + result.ToString();
                        row[VISIBLE_COLUMN_NAME] = true;
                        row[VISIBLE_BY_TEXT_COLUMN_NAME] = true;

                        table.Rows.InsertAt(row, listIndex);

                        UpdateRow(r[KEY_COLUMN_NAME].ToString(), GROUP_COLUMN_NAME, groupCur.Value.ToString());
                        UpdateRow(r[KEY_COLUMN_NAME].ToString(), VISIBLE_COLUMN_NAME, !groupCur.Collapsed);
                        UpdateRow(r[KEY_COLUMN_NAME].ToString(), VISIBLE_BY_TEXT_COLUMN_NAME, !groupCur.Collapsed);

                        counter = 1; // reset counter for next group
                        listIndex += 2;
                    }
                    groupCur.ItemCount = counter;
                }
                EndChanges(true);

                //updating the rows, faster than the other methods, that had errors.
                foreach (DataGridViewRow var in Rows)
                {
                    var.Visible = var.Visible;
                }
            }
        }

        private void ClearRowsCategory()
        {
            for (int i = 0; i < table.Rows.Count; i++)
            {
                DataRow var = table.Rows[i] as DataRow;
                if (IsGroupRow(var) == true)
                {
                    table.Rows.RemoveAt(i);
                    i--;
                }
                else
                {
                    table.Rows[i][GROUP_COLUMN_NAME] = string.Empty;
                    //table.Rows[i][VISIBLE_COLUMN_NAME] = true;
                    table.Rows[i][VISIBLE_BY_TEXT_COLUMN_NAME] = true;
                }
            }
        }

        private bool IsGroupRow(DataRow row)
        {
            bool result = false;
            if (row != null && row[GROUP_COLUMN_NAME].ToString().ToLower().StartsWith("$") == true)
            {
                result = true;
            }
            return result;
        }

        public List<DataGridExData> SelectedItems
        {
            get
            {
                List<DataGridExData> result = new List<DataGridExData>(this.SelectedRows.Count);
                SortedList<int, DataGridExData> sortedSelectedRows = new SortedList<int, DataGridExData>();

                foreach (DataGridViewRow row in SelectedRows)
                {
                    string groupName = row.Cells[DataGridEx.GROUP_COLUMN_NAME].Value.ToString().ToLower();
                    if (sortedSelectedRows.ContainsKey(row.Index) == false && groupName.StartsWith("$") == false)
                        sortedSelectedRows.Add(row.Index, hash[row.Cells[KEY_COLUMN_NAME].Value.ToString()]);
                }
                result.AddRange(sortedSelectedRows.Values);
                return result;
            }
        }

        public int SelectedItemsCount
        {
            get
            {
                return this.SelectedRows.Count;
            }
        }

        public DataGridExData GetData(string key)
        {
            return hash[key];
        }

        public DataGridExData GetData(int rowIndex)
        {
            return hash[Rows[rowIndex].Cells[KEY_COLUMN_NAME].Value.ToString()];
        }

        public bool ContainsData(DataGridExData gridData)
        {
            return hash.ContainsKey(gridData.Key);
        }

        private DataRow GetRow(string key)
        {
            DataRow[] rows = table.Select(KEY_COLUMN_NAME + " = '" + key + "'");
            if (rows.Length > 0)
                return rows[0];
            else
                return null;
            //return table.Select(KEY_COLUMN_NAME + " = '" + key + "'")[0];
        }

        public DataGridViewRow GetViewRow(string key)
        {
            foreach (DataGridViewRow row in Rows)
            {
                if (GetData(row.Index).Key == key)
                    return row;
            }
            return null;
        }


        public bool IsVisibleRow(string key)
        {
            bool result = false;
            DataRow row = GetRow(key);
            if (row != null)
            {
                result = row[VISIBLE_COLUMN_NAME].ToString().ToLower() == "true";
            }
            return result;
        }

        private void SetRowValue(DataRow row, DataGridExData gridData, string propertyName)
        {
            if (propertyName != KEY_COLUMN_NAME &&
                propertyName != VISIBLE_COLUMN_NAME &&
                propertyName != VISIBLE_BY_TEXT_COLUMN_NAME &&
                propertyName != GROUP_COLUMN_NAME)
            {
                row[propertyName] = type.GetProperty(propertyName).GetValue(gridData, null);
            }
        }

        public void ClearData()
        {
            hash.Clear();
            table.Clear();
        }

        public void BeginChanges()
        {
            BeginChanges(true);
        }

        public void BeginChanges(bool isEditing)
        {
            previousHorizontalScrollOffset = this.HorizontalScrollingOffset;
            editing = isEditing;
            table.BeginLoadData();
        }

        //public new void ClearSelection()
        //{
        //    base.ClearSelection();
        //    this.lastKeyRowIndex = -1;
        //    this.AcceptChanges();
        //}

        public void EndChanges()
        {
            EndChanges(true);
        }

        public void EndChanges(bool clearSelection)
        {
            EndChanges(clearSelection, true);
        }

        public void EndChanges(bool clearSelection, bool putSelectedRowFirst)
        {
            bool rowSelected = this.SelectedItemsCount > 0;
            table.EndLoadData();
            int rowIndex = FirstDisplayedScrollingRowIndex;
            AcceptChanges();
            if (rowIndex > -1 && rowIndex < Rows.Count)
                FirstDisplayedScrollingRowIndex = rowIndex;

            if (clearSelection || rowSelected == false)
            {
                ClearSelection();
            }
            if (this.HorizontalScrollingOffset != previousHorizontalScrollOffset)
            {
                this.HorizontalScrollingOffset = previousHorizontalScrollOffset;
            }

            if (!clearSelection && this.SelectedRows.Count > 0 && putSelectedRowFirst)
                FirstDisplayedScrollingRowIndex = this.SelectedRows[0].Index;
            editing = false;
        }

        public void AddData(DataGridExData gridData)
        {
            AddData(gridData, true);
        }

        public void AddRange(DataGridExData[] gridData)
        {

            for (int i = 0; i < gridData.Length; i++)
            {
                AddData(gridData[i], true);
            }

        }

        public void AddData(DataGridExData gridData, bool visible)
        {
            if (hash.ContainsKey(gridData.Key))
                return;

            hash.Add(gridData.Key, gridData);
            table.BeginLoadData();
            DataRow row = table.NewRow();

            foreach (DataColumn column in table.Columns)
            {
                SetRowValue(row, gridData, column.ColumnName);
            }

            row[KEY_COLUMN_NAME] = gridData.Key;
            row[VISIBLE_COLUMN_NAME] = visible;
            row[VISIBLE_BY_TEXT_COLUMN_NAME] = true;

            table.Rows.Add(row);
            table.EndLoadData();
        }

        public void UpdateData(DataGridExData gridData)
        {
            DataRow row = GetRow(gridData.Key);
            if (hash.ContainsKey(gridData.Key))
            {
                DataGridExData oldGridData = hash[gridData.Key];
                bool applyUpdate = false;
                if (oldGridData == null)
                {
                    applyUpdate = true;
                }
                else if ((oldGridData.Tag is ClientData) &&
                     (oldGridData.Tag as ClientData).Version <= (gridData.Tag as ClientData).Version)
                {
                    applyUpdate = true;
                }
                else if (oldGridData is GridUnitOfficerAsignData)
                {
                    applyUpdate = true;
                }
                /*else if ((oldGridData.Tag is Smartmatic.SmartCad.Data.ObjectData) &&
                     (oldGridData.Tag as Smartmatic.SmartCad.Data.ObjectData).Version <= (gridData.Tag as Smartmatic.SmartCad.Data.ObjectData).Version)
                {
                    applyUpdate = true;
                }*/
                if (row != null && applyUpdate)
                {
                    row.BeginEdit();

                    foreach (DataColumn column in table.Columns)
                    {
                        SetRowValue(row, gridData, column.ColumnName);
                    }

                    if (editing == false && AllowEditing == false)
                        row.EndEdit();

                    hash[gridData.Key] = gridData;
                }
            }
        }

        public void UpdateData(DataGridExData gridData, bool visible)
        {
            DataRow row = GetRow(gridData.Key);

            if (row != null)
            {
                row.BeginEdit();

                foreach (DataColumn column in table.Columns)
                {
                    SetRowValue(row, gridData, column.ColumnName);
                }

                row[VISIBLE_COLUMN_NAME] = visible;

                if (editing == false)
                    row.EndEdit();

                hash[gridData.Key] = gridData;
            }
        }

        public void UpdateData(DataGridExData gridData, string propertyName)
        {
            DataRow row = GetRow(gridData.Key);

            if (row != null)
            {
                row.BeginEdit();

                SetRowValue(row, gridData, propertyName);

                if (editing == false)
                    row.EndEdit();
            }
        }

        private void UpdateRow(string key, string columnName, object value)
        {
            DataRow row = GetRow(key);

            if (row != null)
            {
                row.BeginEdit();

                row[columnName] = value;

                if (editing == false)
                    row.EndEdit();
            }
        }

        public void RemoveData(DataGridExData gridData)
        {
            DataRow row = GetRow(gridData.Key);

            if (row != null)
            {
                table.Rows.Remove(row);
                hash.Remove(gridData.Key);
            }
        }

        public DataRow[] GetVisibleRows()
        {
            return table.Select(VISIBLE_COLUMN_NAME + " = 'true'");
        }

        public IList GetVisibleAreaRows()
        {
            ArrayList list = new ArrayList();
            try
            {
                int count = this.DisplayedRowCount(true);
                int startRow = this.FirstDisplayedCell.RowIndex;

                for (int i = 0; i <= count; i++)
                {
                    DataGridViewRow row = Rows[i + startRow];
                    string hashKey = row.Cells[KEY_COLUMN_NAME].Value.ToString();
                    if (hash.ContainsKey(hashKey) == true)
                    {
                        DataGridExData dataGridExData = hash[hashKey] as DataGridExData;
                        list.Add(dataGridExData);
                    }
                }
            }
            catch
            { }
            return list;
        }

        public IList GetVisibleColumns()
        {
            ArrayList list = new ArrayList();

            foreach (DataGridViewColumn col in this.Columns)
            {
                if (col.Visible == true)
                    list.Add(col);
            }

            return list;
        }



        public void SetDataVisible(DataRow row, bool visible)
        {
            row.BeginEdit();

            row[VISIBLE_COLUMN_NAME] = visible;
            row[VISIBLE_BY_TEXT_COLUMN_NAME] = true;

            if (editing == false)
                row.EndEdit();
        }

        public void SetDataVisibleByText(DataRow row, bool visible)
        {
            row.BeginEdit();

            row[VISIBLE_BY_TEXT_COLUMN_NAME] = visible;

            if (editing == false)
                row.EndEdit();
        }

        public IList GetObjects(string filter)
        {
            ArrayList list = new ArrayList();
            foreach (DataRow row in table.Select(filter))
            {
                DataGridExData dataGridExData = hash[row[KEY_COLUMN_NAME].ToString()] as DataGridExData;
                list.Add(dataGridExData.Tag);
            }
            return list;
        }

        public IList GetDataGridObjects(string filter)
        {
            ArrayList list = new ArrayList();
            foreach (DataRow row in table.Select(filter))
            {
                DataGridExData dataGridExData = hash[row[KEY_COLUMN_NAME].ToString()] as DataGridExData;
                list.Add(dataGridExData);
            }
            return list;
        }

        public IList GetDataGridObjects()
        {
            ArrayList list = new ArrayList();

            foreach (DataRow row in table.Rows)
            {
                if (row.RowState != DataRowState.Deleted && row[GROUP_COLUMN_NAME].ToString().ToLower().StartsWith("$") == false)
                {
                    DataGridExData dataGridExData = hash[row[KEY_COLUMN_NAME].ToString()] as DataGridExData;
                    list.Add(dataGridExData);
                }
            }
            return list;
        }

        public void SetColumnImage(string propertyName, Image image)
        {
            DataGridViewColumn column = this.Columns[propertyName];
            column.HeaderText = " ";

            DataGridViewImageColumnHeaderCell cell = (DataGridViewImageColumnHeaderCell)column.HeaderCell;

            cell.Image = image;
        }

        public void SelectData(DataGridExData gridData)
        {
            foreach (DataGridViewRow row in this.Rows)
            {
                if (row.Cells[KEY_COLUMN_NAME].Value.ToString() == gridData.Key &&
                    row.Selected == false)
                {
                    row.Selected = true;
                    this.CurrentCell = this[this.CurrentCell.ColumnIndex, row.Index];
                    if (this.CurrentCell != this.FirstDisplayedCell)
                    {
                        this.FirstDisplayedCell = this.CurrentCell;
                    }
                    break;
                }
            }
        }

        private DataRow[] GetRows(string filter)
        {
            return table.Select(filter);
        }

        #endregion

        public void PerformClickOnRow(int rowIndexAdded)
        {
            if (rowIndexAdded > -1 &&
                this.RowCount > rowIndexAdded)
            {
                FormUtil.InvokeRequired(this,
                    delegate
                    {
                        this.Rows[rowIndexAdded].Selected = true;
                        this.Rows[rowIndexAdded].Visible = true;
                        this.CurrentCell = this[this.CurrentCell.ColumnIndex, rowIndexAdded];
                        OnSelectionChanged(new EventArgs());
                    });
            }
        }

        /// <summary> 
        /// Gets or sets a value indicating whether the control can edit a text row. 
        /// </summary>
        public bool AllowEditing
        {
            get
            {
                return allowEditing;
            }
            set
            {
                allowEditing = value;
            }
        }

        public bool AllowUserToSortColumns
        {
            get
            {
                return allowUserToSortColumns;
            }
            set
            {
                allowUserToSortColumns = value;
            }
        }


    }
}
