﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SmartCadControls
{
    public class CalendarDateCell : DataGridViewTextBoxCell
    {

        public CalendarDateCell()
            : base()
        {
            // Use the short date format.
            this.Style.Format = "d";

        }

        public override void InitializeEditingControl(int rowIndex, object
            initialFormattedValue, DataGridViewCellStyle dataGridViewCellStyle)
        {
            // Set the value of the editing control to the current cell value.
            base.InitializeEditingControl(rowIndex, initialFormattedValue,
                dataGridViewCellStyle);

            CalendarDateEditingControl ctl = DataGridView.EditingControl as CalendarDateEditingControl;
            ctl.Format = DateTimePickerFormat.Short;
            if (this.Value != System.DBNull.Value)
            {
                ctl.Value = (DateTime)this.Value;
            }

        }

        public override Type EditType
        {
            get
            {
                // Return the type of the editing contol that CalendarCell uses.
                return typeof(CalendarDateEditingControl);
            }
        }

        public override Type ValueType
        {
            get
            {
                // Return the type of the value that CalendarCell contains.
                return typeof(DateTime);
            }
        }

        public override object DefaultNewRowValue
        {
            get
            {
                // Use the current date and time as the default value.
                return DateTime.Now;
            }
        }
    }
}
