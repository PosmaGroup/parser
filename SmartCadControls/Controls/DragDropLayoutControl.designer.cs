﻿namespace SmartCadControls.Controls
{
    partial class DragDropLayoutControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControl = new DevExpress.XtraLayout.LayoutControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl
            // 
            this.layoutControl.AllowCustomizationMenu = false;
            this.layoutControl.AllowDrop = true;
            this.layoutControl.Appearance.DisabledLayoutGroupCaption.ForeColor = System.Drawing.SystemColors.GrayText;
            this.layoutControl.Appearance.DisabledLayoutGroupCaption.Options.UseForeColor = true;
            this.layoutControl.Appearance.DisabledLayoutItem.ForeColor = System.Drawing.SystemColors.GrayText;
            this.layoutControl.Appearance.DisabledLayoutItem.Options.UseForeColor = true;
            this.layoutControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl.Location = new System.Drawing.Point(0, 0);
            this.layoutControl.MinimumSize = new System.Drawing.Size(624, 100);
            this.layoutControl.Padding = new System.Windows.Forms.Padding(2);
            this.layoutControl.Name = "layoutControl";
            this.layoutControl.OptionsView.DrawItemBorders = true;
            this.layoutControl.Root = this.layoutControlGroup1;
            this.layoutControl.Size = new System.Drawing.Size(624, 100);
            this.layoutControl.TabIndex = 0;
            this.layoutControl.Text = "layoutControl1";
            this.layoutControl.DragLeave += new System.EventHandler(this.layoutControl_DragLeave);
            this.layoutControl.DragOver += new System.Windows.Forms.DragEventHandler(this.layoutControl_DragOver);
            this.layoutControl.DragDrop += new System.Windows.Forms.DragEventHandler(this.layoutControl_DragDrop);
            this.layoutControl.MouseDown += new System.Windows.Forms.MouseEventHandler(this.layoutControl_MouseDown);
            this.layoutControl.DragEnter += new System.Windows.Forms.DragEventHandler(this.layoutControl_DragEnter);
            this.layoutControl.GiveFeedback += new System.Windows.Forms.GiveFeedbackEventHandler(this.layoutControl_GiveFeedback);
            this.layoutControl.MouseMove += new System.Windows.Forms.MouseEventHandler(this.layoutControl_MouseMove);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(624, 100);
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // DragDropLayoutControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl);
            this.Name = "DragDropLayoutControl";
            this.Size = new System.Drawing.Size(624, 100);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        public DevExpress.XtraLayout.LayoutControl layoutControl;
    }
}
