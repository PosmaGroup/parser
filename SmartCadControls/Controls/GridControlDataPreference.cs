﻿using SmartCadCore.ClientData;
using SmartCadCore.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadControls.Controls
{
    public class GridControlDataPreference : GridControlData
    {
        public GridControlDataPreference(ApplicationPreferenceClientData preference)
            : base(preference)
        {
        }

        public ApplicationPreferenceClientData Preference
        {
            get
            {
                return this.Tag as ApplicationPreferenceClientData;
            }
        }

        [GridControlRowInfoData(Searchable = false)]
        public string Preference_Name
        {
            get
            {
                return ResourceLoader.GetString2((this.Tag as ApplicationPreferenceClientData).Name);
            }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is GridControlDataPreference)
            {
                result = Preference.Equals((obj as GridControlDataPreference).Preference);
            }
            return result;
        }

        public override int GetHashCode()
        {
            return Preference.GetHashCode();
        }
    }
}
