using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using SmartCadCore.ClientData;
using System.IO;
using Smartmatic.SmartCad.Service;
using SmartCadCore.Core;
using System.Collections;
using SmartCadCore.Common;

namespace SmartCadControls.Controls
{
	public partial class OperatorChatControl : DevExpress.XtraEditors.XtraUserControl
	{
		public OperatorChatControl()
		{
			InitializeComponent();
			OperatorClientData oper = (OperatorClientData)ServerServiceClient.GetInstance().RefreshClient(ServerServiceClient.GetInstance().OperatorClient);
			if(oper.Picture != null)
				OperatorPicture.Image = Image.FromStream(new MemoryStream(ServerServiceClient.GetInstance().OperatorClient.Picture));
			else
				OperatorPicture.Image = ResourceLoader.GetImage("$Image.DefaultPhoto");
			this.sendSimpleButton.Text = ResourceLoader.GetString2("Send");
		}

		private OperatorClientData supervisor;

		public OperatorClientData Supervisor
		{
			get
			{
				return supervisor;
			}
			set
			{
				supervisor = value;
				if(supervisor.Picture != null)
					SupervisorPicture.Image = Image.FromStream(new MemoryStream(supervisor.Picture));
				else
					SupervisorPicture.Image = ResourceLoader.GetImage("$Image.DefaultPhoto");
			}
		}

		public void IncomingMessage(ApplicationMessageClientData amcd)
		{
			int textLength = richTextEditReader.Text.Length;
			richTextEditReader.AppendText(String.Format("{0} {1} {2} {3}: ",
										  amcd.Date.ToString("HH:mm:ss"),
										  ResourceLoader.GetString2(amcd.Date.DayOfWeek.ToString()),
										  amcd.Operator.FirstName,
										  amcd.Operator.LastName));
			richTextEditReader.Select(textLength, richTextEditReader.Text.Length);
			richTextEditReader.SelectionFont = new Font(richTextEditReader.Font.FontFamily, richTextEditReader.Font.Size, FontStyle.Bold);
			richTextEditReader.AppendText(Environment.NewLine);
			richTextEditReader.AppendText(amcd.Message);															 
			richTextEditReader.AppendText(Environment.NewLine);
			richTextEditReader.AppendText(Environment.NewLine);
			richTextEditReader.ScrollToCaret();
		}

		private void sendSimpleButton_Click(object sender, EventArgs e)
		{
			ApplicationMessageClientData amcd = new ApplicationMessageClientData();
			amcd.Date = ServerServiceClient.GetInstance().GetTime();
			amcd.Message = richTextEditWriter.Text;
			amcd.Operator = ServerServiceClient.GetInstance().OperatorClient;
			amcd.ToOperators = new ArrayList();
			amcd.ToOperators.Add(supervisor);

			richTextEditWriter.Text = "";

			IncomingMessage(amcd);

			ServerServiceClient.GetInstance().SendClientData(amcd);
		}

		void richTextEditWriter_PreviewKeyDown(object sender, System.Windows.Forms.PreviewKeyDownEventArgs e)
		{
			if (e.KeyValue.Equals(Keys.Enter) == true)
			{
				sendSimpleButton.PerformClick();
			}
		}

		private void richTextEditWriter_TextChanged(object sender, EventArgs e)
		{
			if (richTextEditWriter.Text.Length > 0)
				this.sendSimpleButton.Enabled = true;
			else
				this.sendSimpleButton.Enabled = false;
		}
	}
}
