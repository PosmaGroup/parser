﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Core;
using SmartCadCore.Common;

namespace SmartCadControls.Controls
{
	public class GridControlDataCommands:GridControlData
	{
		#region Fields
		private string status;
		#endregion

		#region Constructor
		public GridControlDataCommands(GPSPinSensorClientData sensor) :
			base(sensor)
		{
			status = ResourceLoader.GetString2("Unactive");
		}
		#endregion

		#region Properties
		[GridControlRowInfoData(Visible = true, Searchable = true)]
		public string Name
		{
			get
			{
				return (Tag as GPSPinSensorClientData).SensorName;
			}
		}

		[GridControlRowInfoData(Visible = true, Searchable = true)]
		public string Status
		{
			get
			{
				return status;
			}
			set
			{
				status = value;
			}
		}
		#endregion

		public override bool Equals(object obj)
		{
			if (obj is GPSPinSensorClientData == false)
				return false;
			GPSPinSensorClientData sensor = obj as GPSPinSensorClientData;
			if (sensor.Code == (Tag as GPSPinSensorClientData).Code)
				return true;
			return false;
		}
	}
}
