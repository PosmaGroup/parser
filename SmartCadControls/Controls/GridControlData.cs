﻿using DevExpress.XtraGrid;
using SmartCadCore.Common;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SmartCadControls.Controls
{
    public abstract class GridControlData
    {
        #region Fields

        private SmartCadCore.ClientData.ClientData tag;
        private Color backColor;
        private GridControl parentGridControl;
        //private SuperToolTip toolTip;

        private static bool isInitiated;
        private static ImageList imageListSemaphores;
        private static object sync = new object();
        private string previewFieldValue;

        static void InitRepositories()
        {
            lock (sync)
            {
                if (isInitiated == false)
                {
                    imageListSemaphores = new ImageList();
                    imageListSemaphores.ColorDepth = ColorDepth.Depth32Bit;
                    imageListSemaphores.ImageSize = new Size(16, 16);
                    imageListSemaphores.TransparentColor = Color.Transparent;
                    imageListSemaphores.Images.Add("Undefined", ResourceLoader.GetImage("$Image.Transparent"));
                    imageListSemaphores.Images.Add("Green", ResourceLoader.GetImage("IMAGE_GreenSemaphoreLightSmall"));
                    imageListSemaphores.Images.Add("Yellow", ResourceLoader.GetImage("IMAGE_YellowSemaphoreLightSmall"));
                    imageListSemaphores.Images.Add("Red", ResourceLoader.GetImage("IMAGE_RedSemaphoreLightSmall"));
                    isInitiated = true;
                }
            }
        }

        internal static ImageList ImageListSemaphores
        {
            get
            {
                return imageListSemaphores;
            }
        }
        static GridControlData()
        {
            InitRepositories();
        }

        #endregion

        #region Properties

        public SmartCadCore.ClientData.ClientData Tag
        {
            get
            {
                return tag;
            }
            set
            {
                tag = value;
            }
        }

        public Color BackColor
        {
            get
            {
                return backColor;
            }
            set
            {
                backColor = value;
            }
        }

        public GridControl ParentGridControl
        {
            get
            {
                return parentGridControl;
            }
            set
            {
                parentGridControl = value;
            }
        }

        public virtual string PreviewFieldValue
        {
            get
            {
                return previewFieldValue;
            }
            set
            {
                previewFieldValue = value;
            }
        }
        #endregion

        #region Constructors

        public GridControlData(SmartCadCore.ClientData.ClientData tagArg)
        {
            this.tag = tagArg;
        }

        #endregion

        #region Methods to override

        public abstract override bool Equals(object obj);


        public override int GetHashCode()
        {
            return this.Tag.GetHashCode();
        }

        #endregion
    }
}
