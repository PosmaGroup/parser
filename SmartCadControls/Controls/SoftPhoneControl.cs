﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Smartmatic.SmartCad.Service;
using System.Threading;
using System.IO;
using NAudio.Wave;
using SmartCadCore.Core;
using SmartCadCore.Common;
using SmartCadCore.Enums;

namespace SmartCadControls.Controls
{
    public partial class SoftPhoneControl : UserControl
    {
        private string incomingPhoneNumber;
        private bool pickedUpCall;
        private object syncTelephonyAction = new object();
        System.Media.SoundPlayer sp = new System.Media.SoundPlayer(SmartCadConfiguration.RingingWav);
        private bool isVirtual = true;
        private bool recording = false;
        public bool ActiveCall { get; set; }

        public TelephoneStatusType TelephoneStatus { get; set; }
        public string ParentCustomCode { get; set; }
        public bool Saving { get{return pickedUpCall;} }

        public SoftPhoneControl()
        {
            InitializeComponent();
            TelephoneStatus = TelephoneStatusType.None;

            isVirtual = TelephonyServiceClient.GetInstance().IsVirtual();
            if (isVirtual == false)
                TelephonyServiceClient.GetInstance().TelephonyAction += SoftPhoneControl_TelephonyAction;

            bCall.Image = ResourceLoader.GetImage("TelephoneIncoming1");
        }

        public void Initialize(string parentCustomCode, string phoneToCall = "")
        {
            ParentCustomCode = parentCustomCode;

            if (isVirtual == false)
            {
                TelephonyServiceClient.GetInstance().SetPhoneReportCustomCode(ParentCustomCode);
            }
            cbPhoneEntry.Items.Clear();
            cbPhoneEntry.Text = "";
            if (string.IsNullOrEmpty(phoneToCall) == false)
            {
                char separatorType = (char)(phoneToCall.IndexOf('|') > -1 ? '|' :
                                           (phoneToCall.IndexOf('/') > -1 ? '/' : 0));
                if (separatorType != 0)
                {
                    string[] pArr = phoneToCall.Split(separatorType);
                    foreach (string pStr in pArr)
                    {
                        if (string.IsNullOrEmpty(pStr) == false)
                            cbPhoneEntry.Items.Add(/*"9" + */pStr);  //Removed 9 Prefix
                    }
                }
                else
                {
                    if (phoneToCall.Split(':').Count() > 0)
                        cbPhoneEntry.Items.Add(/*"9" + */phoneToCall.Split(':')[0]); //Removed 9 Prefix
                    else
                        cbPhoneEntry.Items.Add(/*"9" + */phoneToCall); //Removed 9 Prefix
                }
            }
            else
            {
                cbPhoneEntry.Items.Add("");
            }
            if( cbPhoneEntry.Items.Count>0)
                cbPhoneEntry.SelectedIndex = 0;
        }

        private void SoftPhoneControl_Load(object sender, EventArgs e)
        {
            
        }

        private void bCall_Click(object sender, EventArgs e)
        {
            switch (TelephoneStatus)
            {
                case TelephoneStatusType.None:
                    TelephoneStatus = TelephoneStatusType.Outgoing;
                    bCall.Image = ResourceLoader.GetImage("TelephoneEnded");
                    bCall.Text = ResourceLoader.GetString2("HangUp");
                    if (isVirtual == false)
                    {
                        TelephonyServiceClient.GetInstance().GenerateCall(cbPhoneEntry.Text.Trim());
                    }
                    EnableKeyPad(false);
                    break;
                case TelephoneStatusType.Connected:
                    TelephoneStatus = TelephoneStatusType.Ended;
                    TelephonyServiceClient.GetInstance().Disconnect();
                    break;
                case TelephoneStatusType.Incoming:
                    TelephoneStatus = TelephoneStatusType.Connected;
                    TelephonyServiceClient.GetInstance().Answer();
                    break;
                case TelephoneStatusType.Outgoing:
                    if (isVirtual == false)
                    {
                        TelephonyServiceClient.GetInstance().Disconnect();
                    }
                    TelephoneStatus = TelephoneStatusType.None;
                    bCall.Image = ResourceLoader.GetImage("TelephoneIncoming1");
                    bCall.Text = ResourceLoader.GetString2("Call");
                    EnableKeyPad(true);
                    
                    break;
                default:
                    break;
            }
        }

        private void buttonDialer_Click(object sender, EventArgs e)
        {
            cbPhoneEntry.Text += ((SimpleButtonEx)sender).Text;
            ((SimpleButtonEx)sender).Focus();
        }

        private void tbPhoneEntry_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter && bCall.Enabled == true)
            {
                bCall.PerformClick();
            }
        }

        private void buttonDialer_KeyPress(object sender, KeyPressEventArgs e)
        {
            switch (e.KeyChar)
            {
                case (char)Keys.D1:
                    button1.PerformClick();
                    break;
                case (char)Keys.D2:
                    button2.PerformClick();
                    break;
                case (char)Keys.D3:
                    button3.PerformClick();
                    break;
                case (char)Keys.D4:
                    button4.PerformClick();
                    break;
                case (char)Keys.D5:
                    button5.PerformClick();
                    break;
                case (char)Keys.D6:
                    button6.PerformClick();
                    break;
                case (char)Keys.D7:
                    button7.PerformClick();
                    break;
                case (char)Keys.D8:
                    button8.PerformClick();
                    break;
                case (char)Keys.D9:
                    button9.PerformClick();
                    break;
                case (char)Keys.D0:
                    button0.PerformClick();
                    break;
                case '#':
                    bHash.PerformClick();
                    break;
                case '*':
                    bAsterisk.PerformClick();
                    break;
                case (char)Keys.Back:
                    cbPhoneEntry.Focus();
                    SendKeys.SendWait(e.KeyChar.ToString());
                    break;
                default:
                    break;
            }
        }

        private void tbPhoneEntry_TextChanged(object sender, EventArgs e)
        {
            bCall.Enabled = (TelephoneStatus == TelephoneStatusType.Outgoing) || !string.IsNullOrEmpty(cbPhoneEntry.Text.Trim());
            if (bCall.Enabled == false) groupBox1.Focus();
        }

        private void EnableKeyPad(bool enable)
        {
            groupBox1.Enabled = enable;
            cbPhoneEntry.Enabled = enable;
        }


        private void SoftPhoneControl_TelephonyAction(object sender, TelephonyActionEventArgs e)
        {
            lock (syncTelephonyAction)
            {
                FormUtil.InvokeRequired(this, delegate
                {
                    if (e is TelephonyServerConnectionEventArgs)
                    {
                        #region TelephonyServerConnectionEventArgs
                        #endregion
                    }
                    else if (e is CallDialingEventArgs)
                    {
                        ActiveCall = true;
                        sp.PlayLooping();
                    }
                    else if (e is CallRingingEventArgs)
                    {
                        #region CallRingingEventArgs
                        sp.PlayLooping();
                        this.incomingPhoneNumber = ((CallRingingEventArgs)e).ANI;
                        EnableKeyPad(false);
                        #endregion
                    }
                    else if (e is CallEstablishedEventArgs)
                    {
                        #region CallEstablishedEventArgs
                        ActiveCall = true;
                        sp.Stop();
                        pickedUpCall = true;
                        #endregion
                    }
                    else if (e is CallReleasedEventArgs || e is CallAbandonedEventArgs)
                    {
                        #region CallReleasedEventArgs || CallAbandonedEventArgs
                        ActiveCall = false;
                        TelephoneStatus = TelephoneStatusType.None;
                        sp.Stop();
                        bCall.Image = ResourceLoader.GetImage("TelephoneIncoming1");
                        bCall.Text = ResourceLoader.GetString2("Call");
                        EnableKeyPad(true);
                        #endregion
                    }
                    else if (e is ReadyStatusChangedEventArgs)
                    {
                        #region ReadyStatusChangedEventArgs

                        #endregion
                    }
                    else if (e is TerminalUnRegisteredEventArgs)
                    {
                        #region TerminalUnRegisteredEventArgs

                        if (pickedUpCall == false)
                        {
                        }

                        #endregion
                    }
                    else if (e is TelephonyErrorEventArgs)
                    {
                        ActiveCall = false;
                        MessageForm.Show(((TelephonyErrorEventArgs)e).ErrorMessage, new Exception());
                        TelephoneStatus = TelephoneStatusType.None;
                        bCall.Image = ResourceLoader.GetImage("TelephoneIncoming1");
                        bCall.Text = ResourceLoader.GetString2("Call");
                        EnableKeyPad(true);
                    }
                    else if (e is CallRecordedEventArgs)
                    {
                        #region CallRecorded
                        //SEND THE AUDIO FILE (IF EXISTS) TO THE SERVER.
                        new Thread(new ThreadStart(delegate
                        {
                            recording = true;
                            FileInfo filePath = FindAudioFile();
                            if (filePath != null)
                            {
                                WaveFileReader audioFile = null;
                                try
                                {
                                    audioFile = new WaveFileReader(filePath.FullName);
                                    FileServerServiceClient fileServerServiceClient = FileServerServiceClient.CreateInstance();
                                    fileServerServiceClient.SendFile(audioFile, ((CallRecordedEventArgs)e).PhoneReportCustomCode + ".wav");
                                    fileServerServiceClient.Close();
                                    audioFile.Close();
                                    audioFile.Dispose();
                                    filePath.Delete();
                                    filePath = null;
                                }
                                catch (Exception ex)
                                {
                                    SmartLogger.Print(ex);
                                    if(audioFile!=null){
                                        audioFile.Close();
                                        audioFile.Dispose();
                                    }
                                    filePath = null;
                                }
                            }
                            recording = false;

                        })).Start();
                        #endregion
                    }
                });
            }
        }

        private FileInfo FindAudioFile()
        {
            try
            {
                if (Directory.Exists(SmartCadConfiguration.AudioRecordingFolder) == false)
                    Directory.CreateDirectory(SmartCadConfiguration.AudioRecordingFolder);
                var directory = new DirectoryInfo(SmartCadConfiguration.AudioRecordingFolder);
                var myFile = directory.GetFiles()
                             .Where<FileInfo>(f => f.Extension == ".wav" && f.Name.Contains("mixed"))
                             .OrderByDescending(f => f.LastWriteTime)
                             .FirstOrDefault();
                return myFile;
            }
            catch
            {
            }
            return null;
        }

        public void Close()
        {
            Stop();
            while (recording) Thread.Sleep(1000);
        }

        public void Stop()
        {
            //if (TelephoneStatus == TelephoneStatusType.Outgoing)
            //{
            //    TelephonyServiceClient.GetInstance().Disconnect();
            //}
            if (bCall.Text == ResourceLoader.GetString2("HangUp"))
            {
                bCall.PerformClick();
                Thread.Sleep(100);
            }
        }
    }
}
