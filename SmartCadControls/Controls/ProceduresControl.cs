using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using SmartCadCore.Core;
using SmartCadCore.Common;

namespace SmartCadControls.Controls
{
    public partial class ProceduresControl : HeaderPanelControl
    {
        public event PreviewKeyDownEventHandler ProceduresControlPreviewKeyDown;

        public ProceduresControl()
        {
            InitializeComponent();
        }

        private void ProceduresControl_Load(object sender, EventArgs e)
        {
            this.xtraTabControlProcedures.Click += new EventHandler(internalControl_Click);
			LoadLanguage();
        }

		private void LoadLanguage()
		{
            this.groupControlBody.Text = ResourceLoader.GetString2("FirstLevelProcedures");
			this.xtraTabPage1.Text = ResourceLoader.GetString2("AnswersProcedure");
			this.xtraTabPage2.Text = ResourceLoader.GetString2("GenericProcedures");
		}

        private void internalControl_Click(object sender, EventArgs e)
        {
            OnInternalControlClick(EventArgs.Empty);
        }

        public event EventHandler InternalControlClick;

        protected virtual void OnInternalControlClick(EventArgs e)
        {
            if (InternalControlClick != null)
                InternalControlClick(this, e);
        }

        public void browserProcedures_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (ProceduresControlPreviewKeyDown != null)
                ProceduresControlPreviewKeyDown(this, e);
        }

        public void browserGenericProcedure_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (ProceduresControlPreviewKeyDown != null)
                ProceduresControlPreviewKeyDown(this, e);
        }

		public override bool Active
		{
			get
			{
				return base.Active;
			}
			set
			{
				base.Active = value;

				if (Active)
					this.groupControlBody.LookAndFeel.Style = GetStyle(ActiveColor);
				else
				{
					this.groupControlBody.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Skin;
					this.groupControlBody.LookAndFeel.SkinName = InactiveColor;
				}
			}
		}
    }
}
