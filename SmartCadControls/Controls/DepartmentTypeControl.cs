using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Reflection;

using SmartCadCore.ClientData;
using Smartmatic.SmartCad.Service;
using SmartCadCore.Core;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraEditors.Controls;
using SmartCadCore.Common;
using SmartCadCore.Enums;
using SmartCadControls.SyncBoxes;

namespace SmartCadControls.Controls
{
    #region Class DepartmentTypesControl Documentation
    /// <summary>
    /// 
    /// </summary>
    /// <className>DepartmentTypesControl</className>
    /// <author email="armando.torres@smartmatic.com">Armando Torres</author>
    /// <refactoredBy email="atorres@smartmatic.com">Alden Torres</refactoredBy>
    /// <date>2007/05/16</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    public partial class DepartmentTypesControl : UserControl
    {
        #region Fields

        private IList selectedDepartmentTypesWithPriority;
        private FrontClientStateEnum frontClientState;
        private Dictionary<string, IncidentNotificationPriorityClientData> globalNotificationPrioritiesClient;
        private Dictionary<int, DepartmentTypeClientData> globalDepartmentTypesClient;
        private bool OneSubItemChange = false;
        public event EventHandler<EventArgs> DepartmentTypesSelected;
		private RepositoryItemComboBox priorityCombo = new RepositoryItemComboBox();
		private CctvClientStateEnum cctvClientState = CctvClientStateEnum.WaitingForCctvIncident;
        private int departmentChecksCount = 0;

		#endregion

        #region Properties
                

        public IList SelectedDepartmentTypesWithPriority
        {
            get
            {
                if (this.selectedDepartmentTypesWithPriority == null)
                    this.selectedDepartmentTypesWithPriority = new ArrayList();

                return this.selectedDepartmentTypesWithPriority;
            }
            set
            {
                this.selectedDepartmentTypesWithPriority = value;
            }
        }

        public Dictionary<int, DepartmentTypeClientData> GlobalDepartmentTypes
        {
            get
            {
                return globalDepartmentTypesClient;
            }
            set
            {
                this.globalDepartmentTypesClient = value;
            }
        }

        public Dictionary<string, IncidentNotificationPriorityClientData> GlobalNotificationPriorities
        {
            get
            {
                return this.globalNotificationPrioritiesClient;
            }
            set
            {
                this.globalNotificationPrioritiesClient = value;
            }
        }

        public IList IncidentTypes
        {
            set
            {
                ReportBaseDepartmentTypeClientData departmentWithPriority = null;
				
                gridViewExDepartmentType.BeginUpdate();
                foreach (GridDepartmentTypeData gridData in gridControlExDepartmentType.Items)
                {
                    if (gridData.DepartmentTypeCheck == true)
                    {
                        if (gridData.Priority == null || gridData.Priority.ToString() == string.Empty)
                        {
                            DeleteDepartmentWithPriority(gridData.DepartmentType);
                            gridData.DepartmentTypeCheck = false;
                            departmentChecksCount--;
                        }
                    }
                    
                }         

                foreach (IncidentTypeClientData incidentType in value) 
                {
                    foreach (IncidentTypeDepartmentTypeClientData incidentTypeDepartmentType in incidentType.IncidentTypeDepartmentTypeClients)
                    {
                        if (GlobalDepartmentTypes.ContainsKey(incidentTypeDepartmentType.DepartmentType.Code) == true)
                        {
                            DepartmentTypeClientData departmentType = GlobalDepartmentTypes[incidentTypeDepartmentType.DepartmentType.Code];

                            GridDepartmentTypeData data = (GridDepartmentTypeData)gridControlExDepartmentType.GetGridControlData(departmentType);//new GridDepartmentTypeData(departmentType);

                            if (data.DepartmentTypeCheck == false)
                            {
                                departmentWithPriority = new ReportBaseDepartmentTypeClientData();
                                departmentWithPriority.DepartmentTypeCode = departmentType.Code;
                                data.DepartmentTypeCheck = true;
                                AddOrUpdateDepartmentWithPriority(departmentWithPriority);
                                departmentChecksCount++;
                            }
                        }
                    }                                
                }

                gridViewExDepartmentType.EndUpdate();

                UpdateCheckDepartment();
			}
        }

        #endregion

        #region Constructors

        public DepartmentTypesControl()
        {
            InitializeComponent();
            gridControlExDepartmentType.Type = typeof(GridDepartmentTypeData);
			gridControlExDepartmentType.DataSource = new ArrayList();
			gridViewExDepartmentType.Columns["Priority"].ColumnEdit = priorityCombo;
			gridViewExDepartmentType.Columns["DepartmentTypeCheck"].OptionsColumn.AllowEdit = true;
            gridViewExDepartmentType.Columns["Priority"].OptionsColumn.AllowEdit = true;
			gridViewExDepartmentType.Columns["DepartmentTypeCheck"].OptionsColumn.ReadOnly = false;			
			gridViewExDepartmentType.Columns["Name"].OptionsColumn.AllowEdit = false;
			gridViewExDepartmentType.OptionsView.ColumnAutoWidth = true;
			gridViewExDepartmentType.OptionsBehavior.Editable = true;

			priorityCombo.TextEditStyle = TextEditStyles.DisableTextEditor;

			priorityCombo.SelectedIndexChanged += new EventHandler(priorityCombo_SelectedIndexChanged);
			LoadLanguage();
        }

        void priorityCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int rowIndex = gridViewExDepartmentType.FocusedRowHandle;
            DevExpress.XtraEditors.ComboBoxEdit combo = sender as DevExpress.XtraEditors.ComboBoxEdit;
            if (combo != null)
            {
                string priority = combo.EditValue as string;
                
                if (priority != null)
                {
                    this.radioButtonExPriority1.CheckedChanged -= new System.EventHandler(this.radioButtonExPriority_CheckedChanged);
                    this.radioButtonExPriority2.CheckedChanged -= new System.EventHandler(this.radioButtonExPriority_CheckedChanged);
                    this.radioButtonExPriority3.CheckedChanged -= new System.EventHandler(this.radioButtonExPriority_CheckedChanged);
                    //LO BUSCO Y LE SETEO LA PRIORIDAD AL VALOR SELECCIONADO.
                    gridViewExDepartmentType.SetRowCellValue(rowIndex, "Priority", (IncidentNotificationPriorityClientData)GlobalNotificationPriorities[priority]);
                    DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e1 =
                        new DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs(rowIndex, gridViewExDepartmentType.Columns["Priority"], (IncidentNotificationPriorityClientData)GlobalNotificationPriorities[priority]);
                    gridViewExDepartmentType_CellValueChanging(sender, e1);
                    OneSubItemChange = true;
                    this.radioButtonExPriority1.Checked = false;
                    this.radioButtonExPriority2.Checked = false;
                    this.radioButtonExPriority3.Checked = false;
                    this.radioButtonExPriority1.CheckedChanged += new System.EventHandler(this.radioButtonExPriority_CheckedChanged);
                    this.radioButtonExPriority2.CheckedChanged += new System.EventHandler(this.radioButtonExPriority_CheckedChanged);
                    this.radioButtonExPriority3.CheckedChanged += new System.EventHandler(this.radioButtonExPriority_CheckedChanged);
                }
                OneSubItemChange = false;
            }
        }

        private void LoadLanguage()
		{
			layoutControlItemDepartmentCheckBox.Text = ResourceLoader.GetString2("AllDepartments");
			radioButtonExPriority1.Text = ResourceLoader.GetString2("High_priority");
            radioButtonExPriority2.Text = ResourceLoader.GetString2("Medium_priority");
            radioButtonExPriority3.Text = ResourceLoader.GetString2("Low_priority");
            toolTipMain.SetToolTip(radioButtonExPriority1, ResourceLoader.GetString2("High_priority"));
            toolTipMain.SetToolTip(radioButtonExPriority2, ResourceLoader.GetString2("Medium_priority"));
            toolTipMain.SetToolTip(radioButtonExPriority3, ResourceLoader.GetString2("Low_priority"));
		}

		#endregion
      
        public void FillPriorityComboBox()
        {
            if (globalNotificationPrioritiesClient != null)
            {
				priorityCombo.Items.Clear();
				
                foreach (IncidentNotificationPriorityClientData priority in this.globalNotificationPrioritiesClient.Values)
                {
                    priorityCombo.Items.Add(priority.CustomCode);      
                    if (priority.CustomCode.Substring(0, 1).Equals("1") == true)
                        this.radioButtonExPriority1.Tag = priority;
                    else if (priority.CustomCode.Substring(0, 1).Equals("2") == true)
                        this.radioButtonExPriority2.Tag = priority;
                    else if (priority.CustomCode.Substring(0, 1).Equals("3") == true)
                        this.radioButtonExPriority3.Tag = priority;
                }
            }
        }

        public void FillDepartmentTypes()
        {
            if (globalDepartmentTypesClient != null)
            {
                GridDepartmentTypeData data = null;
                ArrayList list = new ArrayList(globalDepartmentTypesClient.Values);

                if (gridControlExDepartmentType.DataSource != null && gridControlExDepartmentType.DataSource is IList)
                    ((IList)gridControlExDepartmentType.DataSource).Clear();

                list.Sort(new DepartmentTypeNameComparer());
				gridControlExDepartmentType.BeginUpdate();
				foreach (DepartmentTypeClientData departmentType in list) 
				{
					data = new GridDepartmentTypeData(departmentType);              
                    ((IList)gridControlExDepartmentType.DataSource).Add(data);                                  
                }
				gridControlExDepartmentType.EndUpdate();
			}
        }

        private void DepartmentTypesControl_Load(object sender, EventArgs e)
        {
            if (this.DesignMode == false)
            {
                this.FillDepartmentTypes();
                this.FillPriorityComboBox();
                this.selectedDepartmentTypesWithPriority = new ArrayList();
            }
        }

        public void SetFocus(String ctrl)
        {
            RadioButtonEx rd;

            rd = this.Controls.Find(ctrl, true)[0] as RadioButtonEx;
            if (rd != null)
            {
                rd.AutoCheck = false;
                rd.Focus();
                rd.AutoCheck = true;
            }
        }

        public void CleanControl()
        {
			FormUtil.InvokeRequired(this, delegate
			{
				checkBoxDepartmentType.Checked = false;
				CheckDepartmentTypes(false);
				if (radioButtonExPriority1.Checked == true)
					radioButtonExPriority1.Checked = false;
				else if (radioButtonExPriority2.Checked == true)
					radioButtonExPriority2.Checked = false;
				else if (radioButtonExPriority3.Checked == true)
					radioButtonExPriority3.Checked = false;

                this.SelectedDepartmentTypesWithPriority = new ArrayList();
			});
        }        

        public void DeleteDepartmentWithPriority(DepartmentTypeClientData departmentType)
        {
            int index = 0;
            bool found = false;

            while (found != true && index < this.selectedDepartmentTypesWithPriority.Count)
            {
                ReportBaseDepartmentTypeClientData existentDepartmentWithPriority = 
                    (ReportBaseDepartmentTypeClientData)this.selectedDepartmentTypesWithPriority[index];

                if (existentDepartmentWithPriority.DepartmentTypeCode == departmentType.Code)
                {
                    found = true;
                }

                index = index + 1;
            }
            if (found == true)
            {
                selectedDepartmentTypesWithPriority.RemoveAt(index - 1);

                DepartmentTypesSelected(this, EventArgs.Empty);
            }
            if (selectedDepartmentTypesWithPriority.Count == 0)
                gridViewExDepartmentType.Columns["Priority"].Caption = ResourceLoader.GetString2("Priority");
        }        

        private void AddOrUpdateDepartmentWithPriority(ReportBaseDepartmentTypeClientData departmentWithPriority)
        {
            int index = 0;
            bool found = false;

            while (found != true && index < this.selectedDepartmentTypesWithPriority.Count)
            {
                ReportBaseDepartmentTypeClientData existentDepartmentWithPriority = 
                    (ReportBaseDepartmentTypeClientData)this.selectedDepartmentTypesWithPriority[index];

                if (existentDepartmentWithPriority.DepartmentTypeCode == departmentWithPriority.DepartmentTypeCode)
                {
                    found = true;
					existentDepartmentWithPriority.PriorityCode = departmentWithPriority.PriorityCode;
                }
                index = index + 1;
            }           
            if (found != true)
                selectedDepartmentTypesWithPriority.Add(departmentWithPriority);

            DepartmentTypesSelected(this, EventArgs.Empty);

            if (selectedDepartmentTypesWithPriority.Count > 0)
                gridViewExDepartmentType.Columns["Priority"].Caption = ResourceLoader.GetString2("Priority") + " *";
        }

		private void CheckDepartmentTypes(bool check)
		{
            gridViewExDepartmentType.BeginUpdate();
            foreach (GridDepartmentTypeData item in gridControlExDepartmentType.Items)
            {
                item.DepartmentTypeCheck = check;
                if (check == false)
                {
                    DeleteDepartmentWithPriority(item.DepartmentType);
                    item.Priority = null;
                }
                else 
                {
                    ReportBaseDepartmentTypeClientData departmentWithPriority = new ReportBaseDepartmentTypeClientData();
                    departmentWithPriority.DepartmentTypeCode = item.DepartmentType.Code;
                    AddOrUpdateDepartmentWithPriority(departmentWithPriority);
                }
            }

            if (check == true)
                departmentChecksCount = gridControlExDepartmentType.Items.Count;
            else
                departmentChecksCount = 0;

            gridViewExDepartmentType.EndUpdate();
		}        

        private void checkBoxDepartmentType_CheckedChanged(object sender, EventArgs e)
        {
			if (checkBoxDepartmentType.CheckState != CheckState.Indeterminate)
			{
				CheckDepartmentTypes(checkBoxDepartmentType.Checked);
				EnableRadioButtons(checkBoxDepartmentType.Checked);
			}			
        }
        
        private void radioButtonExPriority_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton currentRadioButton = (RadioButton)sender;
            if ((currentRadioButton.Checked == true)&&(OneSubItemChange == false))
            {
               for (int i = 0;i < this.gridViewExDepartmentType.RowCount; i++)
				{
					if(gridViewExDepartmentType.GetRowCellValue(i,gridViewExDepartmentType.Columns["DepartmentTypeCheck"]).Equals(true))
                    {
                        DepartmentTypesSelected(this, EventArgs.Empty);
                        gridViewExDepartmentType_CellValueChanging(
							gridViewExDepartmentType,
							new DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs(
								i,
								gridViewExDepartmentType.Columns["Priority"],
								(((IncidentNotificationPriorityClientData)currentRadioButton.Tag).ToString())));                       
                    }
                }
            }
            else if (OneSubItemChange == true)
            {
                currentRadioButton.Checked = false;
            }
            OneSubItemChange = false;
        }

        public new bool Focus()
        {
            return this.checkBoxDepartmentType.Focus();
        }        	

		private void EnableRadioButtons(bool enable)
		{
			FormUtil.InvokeRequired(this, delegate
			{
				radioButtonExPriority1.Enabled = enable;
				radioButtonExPriority1.TabStop = enable;
				radioButtonExPriority2.Enabled = enable;
				radioButtonExPriority2.TabStop = enable;
				radioButtonExPriority3.Enabled = enable;
				radioButtonExPriority3.TabStop = enable;
			});
		}

        private void gridViewExDepartmentType_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            this.gridViewExDepartmentType.CellValueChanging -= new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewExDepartmentType_CellValueChanging);

            GridDepartmentTypeData gridData = gridViewExDepartmentType.GetRow(e.RowHandle) as GridDepartmentTypeData;

            if (gridData != null)
            {
                DepartmentTypeClientData departmentType = new DepartmentTypeClientData();

                departmentType = gridData.DepartmentType as DepartmentTypeClientData;
                gridViewExDepartmentType.BeginUpdate();
                if (e.Column.FieldName.Equals("DepartmentTypeCheck") && e.Value.Equals(true))
                {
                    if (e.Value.Equals(true))
                    {
                        ReportBaseDepartmentTypeClientData departmentWithPriority = new ReportBaseDepartmentTypeClientData();
                        departmentWithPriority.DepartmentTypeCode = departmentType.Code;
                        gridData.DepartmentTypeCheck = true;
                        AddOrUpdateDepartmentWithPriority(departmentWithPriority);
                        departmentChecksCount++;
                    }
                }
                else if (e.Column.FieldName.Equals("Priority") && e.Value != DBNull.Value && e.Value.ToString() != string.Empty)
                {
                    ReportBaseDepartmentTypeClientData departmentWithPriority = new ReportBaseDepartmentTypeClientData();
                    departmentWithPriority.DepartmentTypeCode = departmentType.Code;

                    gridData.Priority = GlobalNotificationPriorities[(string)e.Value];
                    try
                    {
                        IncidentNotificationPriorityClientData priority = gridData.Priority as IncidentNotificationPriorityClientData;
                        if (priority == null)
                            departmentWithPriority.PriorityCode = GlobalNotificationPriorities[(string)e.Value].Code;
                        else
                            departmentWithPriority.PriorityCode = priority.Code;
                    }
                    catch
                    {
                        departmentWithPriority.PriorityCode = GlobalNotificationPriorities[(string)e.Value].Code;
                    }

                    if (gridData.DepartmentTypeCheck == false)
                    {
                        gridData.DepartmentTypeCheck = true;
                        departmentChecksCount++;
                    }
                    AddOrUpdateDepartmentWithPriority(departmentWithPriority);

                }
                else
                {
                    gridData.Priority = null;
                    if (gridData.DepartmentTypeCheck == true)
                    {
                        gridData.DepartmentTypeCheck = false;
                        departmentChecksCount--;
                    }
                    DeleteDepartmentWithPriority(departmentType);
                }
                gridViewExDepartmentType.EndUpdate();

                UpdateCheckDepartment();
            }

            UpdateRadioButtons();

            this.gridViewExDepartmentType.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewExDepartmentType_CellValueChanging);
        }

        private void UpdateCheckDepartment()
        {
            if (departmentChecksCount == gridControlExDepartmentType.Items.Count)
            {
                checkBoxDepartmentType.CheckState = CheckState.Checked;
                EnableRadioButtons(true);
            }
            else if (departmentChecksCount == 0)
            {
                checkBoxDepartmentType.CheckState = CheckState.Unchecked;
                EnableRadioButtons(false);
            }
            else
            {
                checkBoxDepartmentType.CheckState = CheckState.Indeterminate;
                EnableRadioButtons(true);
            }
        }

        private void ResetRadioButtons()
        {
            radioButtonExPriority1.Checked = false;
            radioButtonExPriority2.Checked = false;
            radioButtonExPriority3.Checked = false;
        }

        private void UpdateRadioButtons()
        {
            if (checkBoxDepartmentType.CheckState == CheckState.Indeterminate || checkBoxDepartmentType.CheckState == CheckState.Checked)
            {
                IncidentNotificationPriorityClientData priorityIterator, priority = null;
                bool mixedPriority = false;
                for (int i = 0; i < gridViewExDepartmentType.RowCount; i++)
                {
                    int rowHandle = gridViewExDepartmentType.GetRowHandle(i);
                    priorityIterator = (IncidentNotificationPriorityClientData)gridViewExDepartmentType.GetRowCellValue(rowHandle, "Priority");
                    if (priorityIterator == null)
                    {
                        continue;
                    }
                    else
                    {
                        if (priority == null)
                        {
                            priority = priorityIterator;
                        }
                        else
                        {
                            if (priority.Code != priorityIterator.Code)
                            {
                                ResetRadioButtons();
                                mixedPriority = true;
                                break;
                            }
                        }
                    }
                }
                if (!mixedPriority && (priority != null))
                {
                    switch (priority.Code)
                    {
                        case 1:
                            radioButtonExPriority1.Checked = true;
                            break;
                        case 2:
                            radioButtonExPriority2.Checked = true;
                            break;
                        case 3:
                            radioButtonExPriority3.Checked = true;
                            break;
                        default:
                            ResetRadioButtons();
                            break;
                    }
                }
                else
                {
                    ResetRadioButtons();
                }
            }
        }
          

        //private void gridViewExDepartmentType_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        //{
        //    DepartmentTypeClientData departmentType = new DepartmentTypeClientData();
        //    departmentType.Name = gridViewExDepartmentType.GetRowCellValue(e.RowHandle, "Name") as string;
        //    object obj = gridViewExDepartmentType.GetRowCellValue(e.RowHandle, "Code");

        //    if (obj != null)
        //    {
        //        departmentType.Code = (int)obj;
        //    }
        //    if (departmentType.Name == null || departmentType.Code == 0)
        //    {
        //        return;
        //    }
        //    int index = ((IList)gridViewExDepartmentType.DataSource).IndexOf(new GridDepartmentTypeData(departmentType));
        //    if (index >= 0)
        //    {
        //        departmentType = (((IList)gridViewExDepartmentType.DataSource)[index] as GridDepartmentTypeData).DepartmentType;

        //        this.gridViewExDepartmentType.CellValueChanging -= new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewExDepartmentType_CellValueChanging);
        //        if (rightClick == true)
        //        {
        //            if (gridViewExDepartmentType.GetRowCellValue(e.RowHandle, "DepartmentTypeCheck").Equals(true))
        //            {
        //                gridViewExDepartmentType.SetRowCellValue(e.RowHandle, "DepartmentTypeCheck", false);
        //            }
        //            else
        //            {
        //                gridViewExDepartmentType.SetRowCellValue(e.RowHandle, "DepartmentTypeCheck", true);
        //            }
        //        }
        //        else
        //        {
        //            if (e.Column.FieldName.Equals("DepartmentTypeCheck") && e.Value.Equals(true))
        //            {
        //                ReportBaseDepartmentTypeClientData departmentWithPriority = new ReportBaseDepartmentTypeClientData();
        //                departmentWithPriority.DepartmentTypeCode = departmentType.Code;
        //                gridViewExDepartmentType.SetRowCellValue(e.RowHandle, "DepartmentTypeCheck", true);

        //                AddOrUpdateDepartmentWithPriority(departmentWithPriority);
        //            }
        //            else if (e.Column.FieldName.Equals("Priority") &&
        //                e.Value != DBNull.Value &&
        //                    e.Value != string.Empty)
        //            {
        //                ReportBaseDepartmentTypeClientData departmentWithPriority = new ReportBaseDepartmentTypeClientData();
        //                departmentWithPriority.DepartmentTypeCode = departmentType.Code;

        //                gridViewExDepartmentType.SetRowCellValue(e.RowHandle, e.Column, (e.Value as IncidentNotificationPriorityClientData));
        //                try
        //                {
        //                    IncidentNotificationPriorityClientData priority = gridViewExDepartmentType.GetRowCellValue(e.RowHandle, "Priority") as IncidentNotificationPriorityClientData;
        //                    if (priority == null)
        //                    {
        //                        departmentWithPriority.PriorityCode = GlobalNotificationPriorities[(string)e.Value].Code;
        //                    }
        //                    else
        //                    {
        //                        departmentWithPriority.PriorityCode = priority.Code;
        //                    }
        //                }
        //                catch
        //                {
        //                    departmentWithPriority.PriorityCode = GlobalNotificationPriorities[(string)e.Value].Code;
        //                }
        //                gridViewExDepartmentType.SetRowCellValue(e.RowHandle, "DepartmentTypeCheck", true);
        //                AddOrUpdateDepartmentWithPriority(departmentWithPriority);
        //            }
        //            else
        //            {
        //                gridViewExDepartmentType.SetRowCellValue(e.RowHandle, "Priority", null);
        //                gridViewExDepartmentType.SetRowCellValue(e.RowHandle, "DepartmentTypeCheck", false);
        //                DeleteDepartmentWithPriority(departmentType);
        //            }
        //        }
        //        this.gridViewExDepartmentType.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewExDepartmentType_CellValueChanging);
        //    }
        //}      
	}
}
