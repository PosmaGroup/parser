using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace SmartCadControls.Controls
{
    public partial class TimeSelectorControl : UserControl
    {
        private bool showSeconds = false;        
        public event EventHandler TimeChanged;

        [Browsable(false)]
        public int Hours
        {
            get
            {
                return (int)this.numericUpDownHours.Value;
            }
        }

        [Browsable(false)]
        public int Minutes
        {
            get
            {
                return (int)this.numericUpDownMinutes.Value;
            }
        }

        [Browsable(false)]
        public int Seconds
        {
            get
            {
                return (int)this.numericUpDownSeconds.Value;
            }
        }

        [Browsable(false)]
        public TimeSpan TimeSpan
        {
            set
            {
                this.numericUpDownHours.Value = new Decimal(Math.Floor(value.TotalHours));
                this.numericUpDownMinutes.Value = new Decimal(value.Minutes);
                this.numericUpDownSeconds.Value = new Decimal(value.Seconds);                
            }
            get
            {
                TimeSpan timeSpan = new TimeSpan((int)this.numericUpDownHours.Value,
                    (int)this.numericUpDownMinutes.Value,
                    (int)this.numericUpDownSeconds.Value);
                return timeSpan;
            }
        }

        [Category("Appearance")]
        public bool ShowSeconds
        {
            get
            {
                return showSeconds;
            }
            set
            {
                showSeconds = value;
                if (showSeconds == true)
                    this.Width = 264;
                else
                    this.Width = 165;
            }
        }

        public void Reset()
        {
            TimeSpan = TimeSpan.Zero;
        }

        public TimeSelectorControl()
        {
            InitializeComponent();
            this.numericUpDownSeconds.TabStop = false;
            this.numericUpDownHours.TextChanged += new EventHandler(numericUpDown_TextChanged);
            this.numericUpDownMinutes.TextChanged += new EventHandler(numericUpDown_TextChanged);
            this.numericUpDownSeconds.TextChanged += new EventHandler(numericUpDown_TextChanged);
        }

        void numericUpDown_TextChanged(object sender, EventArgs e)
        {
            NumericUpDown numericUpDown = sender as NumericUpDown;
            numericUpDown.TextChanged -= new EventHandler(numericUpDown_TextChanged);
            if (numericUpDown.Maximum < numericUpDown.Value)
                numericUpDown.Value = numericUpDown.Maximum;
            else if (numericUpDown.Minimum > numericUpDown.Value)
                numericUpDown.Value = numericUpDown.Minimum;
            numericUpDown.TextChanged += new EventHandler(numericUpDown_TextChanged);
        }

        private void numericUpDown_ValueChanged(object sender, EventArgs e)
        {
            if (TimeChanged != null)
            {
                TimeChanged(this, e);
            }
        }

        private void TimeSelectorControl_Load(object sender, EventArgs e)
        {
            this.FontHeight = 7;
        }
        protected override void OnFontChanged(EventArgs e)
        {
            base.OnFontChanged(e);
        }

        private void numericUpDown_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 189 || e.KeyValue == 109)            
            {
                e.SuppressKeyPress = true;
            }
      
        }
    }
}
