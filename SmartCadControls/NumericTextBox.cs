using SmartCadControls.Controls;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
using FontClass = System.Drawing.Font;
using FontFormatClass = SmartCadControls.Controls.FontFormat;

namespace SmartCadControls
{
	/// <summary>
	/// Summary description for NumericTextBoxType.
	/// </summary>
	public enum NumericTextBoxType
	{
		SByte,
		Byte,
		Short,
		UShort,
		Int,
		UInt,
		Long,
		ULong,
		Float,
		Double,
		Decimal
	}

	/// <summary>
	/// Summary description for NumericTextBox.
	/// </summary>
	public class NumericTextBox : TextBox, IResourceLoadable
	{
		private MenuItem menuItemUndo;
		private MenuItem menuItemCut;
		private MenuItem menuItemCopy;
		private MenuItem menuItemPaste;
		private MenuItem menuItemDelete;
		private MenuItem menuItemSelectAll;
		private double maxValue = (double) int.MaxValue;
		private double minValue = (double) int.MinValue;
		private NumericTextBoxType type = NumericTextBoxType.Int;
		private ContextMenu contextMenu;

		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private Container components = null;

		public NumericTextBox()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitForm call
			menuItemUndo = new MenuItem("Undo", new EventHandler(contextMenu_undoItem_Click), Shortcut.CtrlZ);
			menuItemCut = new MenuItem("Cut", new EventHandler(contextMenu_cutItem_Click), Shortcut.CtrlX);
			menuItemCopy = new MenuItem("Copy", new EventHandler(contextMenu_copyItem_Click), Shortcut.CtrlC);
			menuItemPaste = new MenuItem("Paste", new EventHandler(contextMenu_pasteItem_Click), Shortcut.CtrlV);
			menuItemDelete = new MenuItem("Delete", new EventHandler(contextMenu_deleteItem_Click), Shortcut.Del);
			menuItemSelectAll = new MenuItem("Select all", new EventHandler(contextMenu_selectAllItem_Click));
			contextMenu.MenuItems.Add(menuItemUndo);
			contextMenu.MenuItems.Add(new MenuItem("-"));
			contextMenu.MenuItems.Add(menuItemCut);
			contextMenu.MenuItems.Add(menuItemCopy);
			contextMenu.MenuItems.Add(menuItemPaste);
			contextMenu.MenuItems.Add(menuItemDelete);
			contextMenu.MenuItems.Add(new MenuItem("-"));
			contextMenu.MenuItems.Add(menuItemSelectAll);
			this.TextAlign = HorizontalAlignment.Right;
		}


        protected override void WndProc(ref Message m)
        {
            const int WM_CONTEXTMENU = 0x007b;
            switch (m.Msg)
            {
                case WM_CONTEXTMENU:
                    break;
                default:
                    base.WndProc(ref m);
                    break;
            }
        }

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.contextMenu = new System.Windows.Forms.ContextMenu();
			// 
			// contextMenu
			// 
			this.contextMenu.Popup += new System.EventHandler(this.contextMenu_Popup);
			// 
			// NumericTextBox
			// 
			this.ContextMenu = this.contextMenu;

		}

		#endregion

		private bool showInHexadecimalFormat = false;

		public bool ShowInHexadecimalFormat
		{
			get { return showInHexadecimalFormat; }
			set
			{
				showInHexadecimalFormat = value;
				double val = Value;
				Value = val;
			}
		}

		[Description("Specifies the numeric value of the text"),
			Category("Data")]
		public double Value
		{
			get
			{
				double ret = 0;
				try
				{
                    if (!string.IsNullOrEmpty(Text))
                    {
                        if (!showInHexadecimalFormat)
                        {
                            switch (type)
                            {
                                case NumericTextBoxType.SByte:
                                    ret = (double)sbyte.Parse(Text);
                                    break;
                                case NumericTextBoxType.Byte:
                                    ret = (double)byte.Parse(Text);
                                    break;
                                case NumericTextBoxType.Short:
                                    ret = (double)short.Parse(Text);
                                    break;
                                case NumericTextBoxType.UShort:
                                    ret = (double)ushort.Parse(Text);
                                    break;
                                case NumericTextBoxType.Int:
                                    ret = (double)int.Parse(Text);
                                    break;
                                case NumericTextBoxType.UInt:
                                    ret = (double)uint.Parse(Text);
                                    break;
                                case NumericTextBoxType.Long:
                                    ret = (double)long.Parse(Text);
                                    break;
                                case NumericTextBoxType.ULong:
                                    ret = (double)ulong.Parse(Text);
                                    break;
                                case NumericTextBoxType.Float:
                                    ret = (double)float.Parse(Text);
                                    break;
                                case NumericTextBoxType.Double:
                                    ret = double.Parse(Text);
                                    break;
                                case NumericTextBoxType.Decimal:
                                    ret = (double)decimal.Parse(Text);
                                    break;
                            }
                        }
                        else
                        {
                            ret = (double)long.Parse(Text, NumberStyles.HexNumber);
                        }
                    }
				}
				catch
				{
				}
				return ret;
			}
			set
			{
				if ((value >= minValue) && (value <= maxValue))
				{
					if (!showInHexadecimalFormat)
					{
						switch (type)
						{
							case NumericTextBoxType.SByte:
								Text = ((sbyte) value).ToString();
								break;
							case NumericTextBoxType.Byte:
								Text = ((byte) value).ToString();
								break;
							case NumericTextBoxType.Short:
								Text = ((short) value).ToString();
								break;
							case NumericTextBoxType.UShort:
								Text = ((ushort) value).ToString();
								break;
							case NumericTextBoxType.Int:
								Text = ((int) value).ToString();
								break;
							case NumericTextBoxType.UInt:
								Text = ((uint) value).ToString();
								break;
							case NumericTextBoxType.Long:
								Text = ((long) value).ToString();
								break;
							case NumericTextBoxType.ULong:
								Text = ((ulong) value).ToString();
								break;
							case NumericTextBoxType.Float:
								Text = ((float) value).ToString();
								break;
							case NumericTextBoxType.Double:
								Text = ((double) value).ToString();
								break;
							case NumericTextBoxType.Decimal:
								Text = ((decimal) value).ToString();
								break;
						}
					}
					else
					{
						long x = (long) value;
						Text = x.ToString("X2");
					}
				}
			}
		}

		[Description("Sets the numeric type allowed"),
			Category("Behavior"),
			RefreshProperties(RefreshProperties.All)]
		public NumericTextBoxType InputType
		{
			get { return type; }
			set
			{
				type = value;
				double te = maxValue;
				MaxValue = te;
				te = minValue;
				MinValue = te;
			}
		}


		[Description("Specifies the maximun value that can be entered into the edit control"),
			Category("Behavior")]
		public double MaxValue
		{
			get { return maxValue; }
			set
			{
				switch (type)
				{
					case NumericTextBoxType.SByte:
						maxValue = (value > ((double) sbyte.MaxValue)) ? ((double) sbyte.MaxValue) : ((double) value);
						break;
					case NumericTextBoxType.Byte:
						maxValue = (value > ((double) byte.MaxValue)) ? ((double) byte.MaxValue) : ((double) value);
						break;
					case NumericTextBoxType.Short:
						maxValue = (value > ((double) short.MaxValue)) ? ((double) short.MaxValue) : ((double) value);
						break;
					case NumericTextBoxType.UShort:
						maxValue = (value > ((double) ushort.MaxValue)) ? ((double) ushort.MaxValue) : ((double) value);
						break;
					case NumericTextBoxType.Int:
						maxValue = (value > ((double) int.MaxValue)) ? ((double) int.MaxValue) : ((double) value);
						break;
					case NumericTextBoxType.UInt:
						maxValue = (value > ((double) uint.MaxValue)) ? ((double) uint.MaxValue) : ((double) value);
						break;
					case NumericTextBoxType.Long:
						maxValue = (value > ((double) long.MaxValue)) ? ((double) long.MaxValue) : ((double) value);
						break;
					case NumericTextBoxType.ULong:
						maxValue = (value > ((double) ulong.MaxValue)) ? ((double) ulong.MaxValue) : ((double) value);
						break;
					case NumericTextBoxType.Float:
						maxValue = (value > ((double) float.MaxValue)) ? ((double) float.MaxValue) : ((double) value);
						break;
					case NumericTextBoxType.Double:
						maxValue = (value > (double.MaxValue)) ? (double.MaxValue) : ((double) value);
						break;
					case NumericTextBoxType.Decimal:
						maxValue = (value > ((double) decimal.MaxValue)) ? ((double) decimal.MaxValue) : ((double) value);
						break;
				}
			}
		}


		[Description("Specifies the manimun value that can be entered into the edit control"),
			Category("Behavior")]
		public double MinValue
		{
			get { return minValue; }
			set
			{
				switch (type)
				{
					case NumericTextBoxType.SByte:
						minValue = (value < ((double) sbyte.MinValue)) ? ((double) sbyte.MinValue) : ((double) value);
						break;
					case NumericTextBoxType.Byte:
						minValue = (value < ((double) byte.MinValue)) ? ((double) byte.MinValue) : ((double) value);
						break;
					case NumericTextBoxType.Short:
						minValue = (value < ((double) short.MinValue)) ? ((double) short.MinValue) : ((double) value);
						break;
					case NumericTextBoxType.UShort:
						minValue = (value < ((double) ushort.MinValue)) ? ((double) ushort.MinValue) : ((double) value);
						break;
					case NumericTextBoxType.Int:
						minValue = (value < ((double) int.MinValue)) ? ((double) int.MinValue) : ((double) value);
						break;
					case NumericTextBoxType.UInt:
						minValue = (value < ((double) uint.MinValue)) ? ((double) uint.MinValue) : ((double) value);
						break;
					case NumericTextBoxType.Long:
						minValue = (value < ((double) long.MinValue)) ? ((double) long.MinValue) : ((double) value);
						break;
					case NumericTextBoxType.ULong:
						minValue = (value < ((double) ulong.MinValue)) ? ((double) ulong.MinValue) : ((double) value);
						break;
					case NumericTextBoxType.Float:
						minValue = (value < ((double) float.MinValue)) ? ((double) float.MinValue) : ((double) value);
						break;
					case NumericTextBoxType.Double:
						minValue = (value < (double.MinValue)) ? (double.MinValue) : ((double) value);
						break;
					case NumericTextBoxType.Decimal:
						minValue = (value < ((double) decimal.MinValue)) ? ((double) decimal.MinValue) : ((double) value);
						break;
				}
			}
		}

		private bool IsUnsignedNumber
		{
			get { return (type == NumericTextBoxType.Byte || type == NumericTextBoxType.UInt || type == NumericTextBoxType.ULong || type == NumericTextBoxType.UShort); }
		}

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
			Browsable(false)]
		public override string Text
		{
			get { return base.Text; }
			set
			{
				if (IsValid(value))
				{
					base.Text = value;
				}
			}
		}

		private bool IsValid(string val)
		{
			bool ret = true;

            //if (val.Equals("-") && (!IsUnsignedNumber))
            //{
            //    return ret;
            //}
			/*else*/ if (val.Equals("-"))
			{
				return false;
			}
            else if (val.Length > 1 && 
                (val[0].ToString().Equals("-") || (val[0].ToString().Equals("+"))))
            {
                return false;
            }
			if (val == "")
			{
				return true;
			}
            
			try
			{
				double value = 0;
				if (!showInHexadecimalFormat)
				{
					switch (type)
					{
						case NumericTextBoxType.SByte:
							value = (double) sbyte.Parse(val);
							break;
						case NumericTextBoxType.Byte:
							value = (double) byte.Parse(val);
							break;
						case NumericTextBoxType.Short:
							value = (double) short.Parse(val);
							break;
						case NumericTextBoxType.UShort:
							value = (double) ushort.Parse(val);
							break;
						case NumericTextBoxType.Int:
							value = (double) int.Parse(val);
							break;
						case NumericTextBoxType.UInt:
							value = (double) uint.Parse(val);
							break;
						case NumericTextBoxType.Long:
							value = (double) long.Parse(val);
							break;
						case NumericTextBoxType.ULong:
							value = (double) ulong.Parse(val);
							break;
						case NumericTextBoxType.Float:
							value = (double) float.Parse(val);
							break;
						case NumericTextBoxType.Double:
							value = double.Parse(val);
							break;
						case NumericTextBoxType.Decimal:
							value = (double) decimal.Parse(val);
							break;
						default:
							throw new Exception();
					}
				}
				else
				{
					value = (double) long.Parse(val, NumberStyles.HexNumber);
				}
				if (value > maxValue)
				{
					if (val[0] != '-')
					{
						ret = false;
					}
					else if (val.Length == MaxLength)
					{
						ret = false;
					}
				}
				if (value < minValue)
				{
					if (val[0] == '-')
					{
						ret = false;
					}
					else if (val.Length == MaxLength)
					{
						ret = false;
					}
				}
			}
			catch
			{
				ret = false;
			}
			return ret;
		}

		protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
		{
			IDataObject iData = Clipboard.GetDataObject();
			if (keyData == (Keys) Shortcut.CtrlV && iData.GetDataPresent(DataFormats.Text))
			{
				string newText;
				newText = base.Text.Substring(0, base.SelectionStart)
					+ (string) iData.GetData(DataFormats.Text)
					+ base.Text.Substring(base.SelectionStart + base.SelectionLength);
				if (!IsValid(newText))
				{
					return true;
				}
            }
            else if (keyData == (Keys)Shortcut.ShiftIns)
            {
                return true;
            }
			return base.ProcessCmdKey(ref msg, keyData);
		}

		protected override void OnKeyPress(KeyPressEventArgs e)
		{
			char c = e.KeyChar;
			if (!Char.IsControl(c))
			{
				if (c == ' ')
				{
					e.Handled = true;
				}
				else
				{
					string newText = base.Text.Substring(0, base.SelectionStart)
						+ c.ToString() + base.Text.Substring(base.SelectionStart + base.SelectionLength);

					if (!IsValid(newText))
					{
						e.Handled = true;
					}
				}
			}
			base.OnKeyPress(e);
		}

		protected override bool IsInputChar(char charCode)
		{
			if (Char.IsDigit(charCode) || (charCode == '-') || (charCode == '.' && !showInHexadecimalFormat) || (Char.IsLetter(charCode) && showInHexadecimalFormat))
			{
				return true;
			}
			else
			{
				return base.IsInputChar(charCode);
			}
		}

		public event CancelEventHandler ValidatingEx;

		public virtual void OnValidatingEx(CancelEventArgs e)
		{
			if (ValidatingEx != null)
			{
				ValidatingEx(this, e);
			}
		}

		protected override void OnLostFocus(EventArgs e)
		{
			CancelEventArgs evn = new CancelEventArgs(false);

			OnValidatingEx(evn);

			if (evn.Cancel)
			{
				return;
			}

			if (((Value < minValue) || (Value > maxValue)) && Parent.ContainsFocus && CausesValidation)
			{
				Form teForm = Parent as Form;
				if (teForm != null)
				{
					Button teButton = teForm.CancelButton as Button;
					if (!teButton.Focused)
					{
						MessageBox.Show("Please enter a number between " + maxValue + " and " + minValue,
						                "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
						Focus();
						SelectAll();
					}
				}
				else
				{
					MessageBox.Show("Please enter a number between " + maxValue + " and " + minValue,
					                "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
					Focus();
					SelectAll();
				}
			}
			else
			{
				base.OnLostFocus(e);
			}
		}

		protected virtual void contextMenu_undoItem_Click(object sender, EventArgs e)
		{
			Undo();
		}

		protected virtual void contextMenu_cutItem_Click(object sender, EventArgs e)
		{
			Cut();
		}

		protected virtual void contextMenu_copyItem_Click(object sender, EventArgs e)
		{
			Copy();
		}

		protected virtual void contextMenu_pasteItem_Click(object sender, EventArgs e)
		{
			IDataObject iData = Clipboard.GetDataObject();
			if (iData.GetDataPresent(DataFormats.Text))
			{
				string newText;
				newText = base.Text.Substring(0, base.SelectionStart)
					+ (string) iData.GetData(DataFormats.Text)
					+ base.Text.Substring(base.SelectionStart + base.SelectionLength);
				if (IsValid(newText))
				{
					Paste();
				}
			}
		}

		protected virtual void contextMenu_deleteItem_Click(object sender, EventArgs e)
		{
			if (SelectionStart < Text.Length && !this.ReadOnly)
			{
				int selectionStarT = SelectionStart;
				int count = (SelectionLength == 0 ? 1 : SelectionLength);
				Text = Text.Remove(SelectionStart, count);
				SelectionStart = selectionStarT;
			}
		}

		protected virtual void contextMenu_selectAllItem_Click(object sender, EventArgs e)
		{
			SelectAll();
		}

		private void contextMenu_Popup(object sender, EventArgs e)
		{
			if (!Focused)
			{
				Focus();
			}
			if (SelectionLength == 0)
			{
				menuItemCopy.Enabled = false;
				menuItemCut.Enabled = false;
				menuItemDelete.Enabled = false;
			}
			else
			{
				menuItemCopy.Enabled = true;
				menuItemCut.Enabled = true;
				menuItemDelete.Enabled = true;
			}
			if (Text.Length == SelectionLength)
			{
				menuItemSelectAll.Enabled = false;
			}
			else
			{
				menuItemSelectAll.Enabled = true;
			}
			if (!Modified)
			{
				menuItemUndo.Enabled = false;
			}
			else
			{
				menuItemUndo.Enabled = true;
			}
			IDataObject iData = Clipboard.GetDataObject();
			if (!iData.GetDataPresent(DataFormats.Text))
			{
				menuItemPaste.Enabled = false;
			}
			else
			{
				string newText;
				newText = base.Text.Substring(0, base.SelectionStart)
					+ (string) iData.GetData(DataFormats.Text)
					+ base.Text.Substring(base.SelectionStart + base.SelectionLength);
				if (!IsValid(newText))
				{
					menuItemPaste.Enabled = false;
				}
				else
				{
					menuItemPaste.Enabled = true;
				}
			}
			if (this.ReadOnly)
			{
				menuItemDelete.Enabled = false;
			}
		}

		[RefreshProperties(RefreshProperties.All),
			DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
			Browsable(false)]
		public override FontClass Font
		{
			get { return base.Font; }
			set { base.Font = value; }
		}

		[RefreshProperties(RefreshProperties.All),
			DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
			Browsable(false)]
		public override Color ForeColor
		{
			get { return base.ForeColor; }
			set { base.ForeColor = value; }
		}

		private FontFormatClass fontFormat = FontFormatClass.EditBoxFormat;

		[EditorBrowsable(EditorBrowsableState.Never),
			Browsable(true),
			Description("Specified The font and foreground color used to display text and graphics in the control"),
			Category("Appearance")]
		public FontFormatClass FontFormat
		{
			get { return fontFormat; }
			set
			{
				fontFormat = value;
				ForeColor = fontFormat.Color;
				Font = fontFormat.Font;
			}
		}
		
		private bool loadFromResources = true;
		#region IResourceLoadable Members

		[Description("This property is used to load text and images resources from Smartmatic.Usp.Resources when it is true."
			 +" When false, resources are the same at design time"),
		Category("Behavior"), Browsable(true), DefaultValue(true)]
		public bool LoadFromResources
		{
			get
			{
				// TODO:  Add AlphabeticalTextBox.LoadFromResources getter implementation
				return loadFromResources;
			}
			set
			{
				// TODO:  Add AlphabeticalTextBox.LoadFromResources setter implementation
				loadFromResources = value;
			}
		}

		#endregion
	}
}