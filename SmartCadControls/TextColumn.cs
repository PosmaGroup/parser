﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SmartCadControls
{
    public class TextColumn : DataGridViewTextBoxColumn
    {
        public TextColumn()
        {
            this.CellTemplate = new TextCell();
        }

        public TextColumn(int len)
        {
            this.CellTemplate = new TextCell(len);
        }
    }
}
