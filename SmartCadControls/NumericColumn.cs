﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SmartCadControls
{
    public class NumericColumn : DataGridViewColumn
    {
        public NumericColumn()
            : base(new NumericCell())
        {
        }

        public NumericColumn(int len)
            : base(new NumericCell(len))
        {
        }

        public override DataGridViewCell CellTemplate
        {
            get
            {
                return base.CellTemplate;
            }
            set
            {
                if (value != null &&
                    !value.GetType().IsAssignableFrom(typeof(NumericCell)))
                {
                    throw new InvalidCastException("Must be a IntCell");
                }
                base.CellTemplate = value;
            }
        }
    }
}
