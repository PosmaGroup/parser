using System;
using System.ComponentModel;
using System.Drawing.Design;
using System.Windows.Forms;
using System.Windows.Forms.Design;


namespace SmartCadControls.Controls
{
	/// <summary>
	/// Summary description for FontFormatEditor.
	/// </summary>
	public class FontFormatEditor : UITypeEditor
	{
		private IWindowsFormsEditorService edSvc = null;

		public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
		{
			if (context != null && context.Instance != null)
			{
				return UITypeEditorEditStyle.DropDown;
			}
			return base.GetEditStyle(context);
		}

		public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
		{
			if (context != null && context.Instance != null && provider != null)
			{
				edSvc = (IWindowsFormsEditorService) provider.GetService(typeof (IWindowsFormsEditorService));
				FontFormat valueCast = value as FontFormat;
				if (edSvc != null && valueCast != null)
				{
					ListBox listBox = new ListBox();
					int selectedIndex = 0;
					foreach (string name in FontFormat.ValueNames)
					{
						int i = listBox.Items.Add(name);
						if (name == valueCast.Name)
						{
							selectedIndex = i;
						}
					}
					listBox.SelectedIndex = selectedIndex;
					listBox.SelectedIndexChanged += new EventHandler(this.ValueChanged);
					edSvc.DropDownControl(listBox);
					string selectedName = (string) listBox.SelectedItem;
					value = FontFormat.GetFormat(selectedName);
				}
			}
			return value;
		}

		private void ValueChanged(object sender, EventArgs e)
		{
			if (edSvc != null)
			{
				edSvc.CloseDropDown();
			}
		}

/*		public override bool GetPaintValueSupported(ITypeDescriptorContext context)
		{
			return true;
		}
		public override void PaintValue(PaintValueEventArgs e)
		{
//			String drawString = "este no es el valor";
			String drawString = (string)e.Value;
			Font drawFont = new Font("Arial", 10);
			SolidBrush drawBrush = new SolidBrush(Color.Black);
			float x = 150.0F;
			float y =  50.0F;
//			StringFormat drawFormat = new StringFormat();
//			drawFormat.FormatFlags = StringFormatFlags.DirectionVertical;
//			e.Graphics.DrawString(drawString, drawFont, drawBrush, x, y, drawFormat);
			e.Graphics.DrawString(drawString, drawFont, drawBrush, e.Bounds.Left, e.Bounds.Top);
//			base.PaintValue(e);
		}*/

		public FontFormatEditor()
		{
		}
	}
}