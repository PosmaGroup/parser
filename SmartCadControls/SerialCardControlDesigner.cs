using System.Windows.Forms.Design;
using SelectionRulesEnum = System.Windows.Forms.Design.SelectionRules;

namespace SmartCadControls
{
	/// <summary>
	/// Summary description for SerialCardControlDesigner.
	/// </summary>
	public class SerialCardControlDesigner : ControlDesigner
	{
		public override SelectionRulesEnum SelectionRules
		{
			get { return (base.SelectionRules & (~SelectionRulesEnum.BottomSizeable) & (~SelectionRulesEnum.TopSizeable)); }
		}
	}
}