﻿using SmartCadCore.Common;
using SmartCadCore.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smartmatic.SmartCad.Map.Util
{
    public class Polygon : IEnumerable
    {
        private List<GeoPoint> m_points = null;
        private List<Line> m_lines = null;

        public Polygon()
        {
            m_points = new List<GeoPoint>();
            m_lines = new List<Line>();
        }

        public Polygon(List<GeoPoint> m_points)
            : this()
        {
            GeoPoint point1 = new GeoPoint(m_points[0].Lon, m_points[0].Lat);
            this.AddPoint(point1);
            for (int i = 1; i < m_points.Count; i++)
            {
                GeoPoint point2 = new GeoPoint(m_points[i].Lon, m_points[i].Lat);
                m_lines.Add(new Line(point1, point2));
                point1 = point2;
                this.AddPoint(point1);
            }
        }

        public bool AddPoint(GeoPoint pt)
        {
            if (pt != null && this.ContainsPoint(pt) == false)
            {
                this.m_points.Add(pt);
                return false;
            }
            return true;
        }

        public List<GeoPoint> Points
        {
            get { return this.m_points; }
        }

        public List<Line> Lines
        {
            get { return this.m_lines; }
        }

        public IEnumerator GetEnumerator()
        {
            return this.m_points.GetEnumerator();
        }

        public bool ContainsPoint(GeoPoint pt)
        {
            foreach (GeoPoint p in this.m_points)
                if (p.Lon == pt.Lon && p.Lat == pt.Lat)
                    return true;

            return false;
        }

        public static bool Intersects(Polygon pol1, Polygon pol2)
        {
            foreach (Line l1 in pol1.Lines)
            {
                foreach (Line l2 in pol2.Lines)
                {
                    GeoPoint point = Line.GetIntersection(l1, l2);
                    if (point != null &&
                        (ApplicationUtil.PointInPolygon(point, pol1.Points) ||
                        ApplicationUtil.PointInPolygon(point, pol2.Points)))
                        return true;
                }
            }
            return false;
        }
    }
}
