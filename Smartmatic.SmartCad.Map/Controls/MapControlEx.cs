﻿using System.Data;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Collections;
using System.Drawing;
using System;
using System.Linq;
using System.Reflection;
using System.IO;

using DevExpress.Utils;

using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using SmartCadCore.Common;
using SmartCadCore.Core;

namespace Smartmatic.SmartCad.Map
{
    #region Enums
    /// <summary>
    /// Este enumerado fue creado para realizar acciones sobre
    /// las tablas dinamicas. Agregar si hace falta alguna.
    /// </summary>
    public enum MapActions
    {
        ClearDistanceTable,
        ClearTempTable,
        Highlight
    }

    public enum ToolStatus
    {
        Starting,
        InProgress,
        Ending,
    }

    #endregion

    public partial class MapControlEx : UserControl
    {
        #region Constants
        public const string ZOOM_IN_CODE = "ZoomIn";
        public const string ZOOM_OUT_CODE = "ZoomOut";
        public const string PAN_CODE = "Pan";
        public const string CENTER_CODE = "Center";
        public const string ADD_POLYGON_CODE = "AddPolygon";
        public const string SELECT_CODE = "Select";
        public const string ADD_POSITION_CODE = "AddPoint";
        public const string SELECT_RECT_CODE = "SelectRect";
        public const string DISTANCE_CODE = "Distance";
        public const string SELECT_RADIUS_CODE = "SelectRadius";
        public const string ADD_POLYLINE_CODE = "AddPolyline";
        public const string ADD_POINT = "AddPoint";
        private string postFixL;

        public string PostFixTables { get { return postFixL; } set { postFixL = value; } }

        public string DynTablePostName { get { return "Postes" + PostFixTables; } }

        public string DynTableDistance { get { return "DistanceTable" + PostFixTables; } }

        public string DynTableRouteTemp { get { return "RouteTableTemp" + PostFixTables; } }

        public string DynTablePolygonZoneName { get { return "PoligonosZonas" + PostFixTables; } }

        public string DynTablePolygonStationName { get { return "PoligonosEstaciones" + PostFixTables; } }

        public string DynTableUnitsName { get { return "UnitsVisibleTable" + PostFixTables; } }

        public string DynTableIncidentsName { get { return "Incidentes" + PostFixTables; } }

        public string DynTableRoute { get { return "RouteTable" + PostFixTables; } }

        public string DynTableTracksName { get { return "TracksTable" + PostFixTables; } }

        public string DynTableUnitTracksName { get { return "UnitTracksName" + PostFixTables; } }

        public string DynTableIconsTemp { get { return "IconsTemp" + PostFixTables; } }

        public string DynTableTemp { get { return "Temp" + PostFixTables; } }

        public string DynTableTelemetryAlarm { get { return "TelemetryAlarmTable" + PostFixTables; } }
        
        public string CurrentMap { get; set; }
        public double CenterLat { get; set; }
        public double CenterLon { get; set; }

        #endregion

        #region Fields
        public bool ShowDistance { get; set; }

        public string DistanceUnit { get; set; }
        
        public bool ShowingHeatMap { get; set; }

        internal double distance = 0;
        public double Distance
        {
            get
            {
                return distance;
            }

            set
            {
                if (value < 1)
                {
                    DistanceUnit = "mts";
                    distance = (double)((int)(value * 1000));
                }
                else
                {
                    DistanceUnit = "Km";
                    distance = value;
                }
            }
        }
        public double totalDistance = 0;
        public string ToolInUse { get; set; }

        //Tamaño inicial para el zoom mas afuera.
        private static double bitmapIconSize = 8;
        public static double BitmapIconSize
        {
            get
            {
                return bitmapIconSize;
            }
            set
            {
                bitmapIconSize = value;
            }
        }

        private static Type MapType = null;

        public string mapsFolder = "";
        public string fileName = null;

        #endregion

        #region Constructor

        public MapControlEx()
        {
            InitializeComponent();

            if (DesignMode == false)
            {
                //Esto servirá para tener varios mapas cargados. AA
                foreach (MapElement element in SmartCadConfiguration.SmartCadSection.Maps)
                {
                    CurrentMap = element.Id;
                    CenterLat = double.Parse(element.CenterLat);
                    CenterLon = double.Parse(element.CenterLon);

                    break;
                }
                try
                {
                    this.mapsFolder = SmartCadConfiguration.MapsFolder;
                    //if (SmartCadConfiguration.SmartCadSection.ServerElement.Map.Local.ToLower() == bool.TrueString.ToLower())
                    //{
                    this.fileName = SmartCadConfiguration.MapsFolder + SmartCadConfiguration.SmartCadSection.ServerElement.Map.InformationFileName;
                    //}
                }
                catch { }
            }
        }

        public static MapControlEx GetInstance(string assemblyName)
        {
            try
            {
                if (MapType == null)
                {

                    Assembly assembly = Assembly.Load(assemblyName);

                    Type[] types = assembly.GetTypes();

                    foreach (Type type in types)
                    {
                        if (type.IsClass == true
                            && type.IsAbstract == false
                            && type.IsSubclassOf(typeof(MapControlEx)))
                        {
                            MapType = type;
                            break;
                        }
                    }
                }
                return (MapControlEx)MapType.GetConstructor(new Type[0] { }).Invoke(new object[0] { });
            }
            catch (Exception e)
            {
                SmartLogger.Print(e);
                return null;
            }
        }

        public static MapControlEx GetNewInstance(string assemblyName)
        {
            try
            {
                Type NewInstance = null;
                Assembly assembly = Assembly.Load(assemblyName);

                Type[] types = assembly.GetTypes();

                foreach (Type type in types)
                {
                    if (type.IsClass == true
                        && type.IsAbstract == false
                        && type.IsSubclassOf(typeof(MapControlEx)))
                    {
                        NewInstance = type;
                        break;
                    }
                }
                return (MapControlEx)NewInstance.GetConstructor(new Type[0] { }).Invoke(new object[0] { });
            }
            catch (Exception e)
            {
                SmartLogger.Print(e);
                return null;
            }
        }

        #endregion

        #region Methods

        public virtual void InsertObject(MapObject obj)
        {
            throw new NotImplementedException();
        }

        public virtual void UpdateObject(MapObject obj)
        {
            throw new NotImplementedException();
        }

        public virtual void DeleteObject(MapObject obj)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Return the point on the map.
        /// </summary>
        /// <param name="originalPoint"></param>
        /// <returns></returns>
        public virtual Point GetRelativePoint(GeoPoint originalPoint)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Return true if the point is in the visible area of the map.
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public virtual bool IsVisible(Point point)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Make visible the layer with name "layerName".
        /// </summary>
        /// <param name="layerName"></param>
        /// <param name="enable"></param>
        public virtual void EnableLayer(string layerName, bool enable)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Return true if the layer with name "layerName" is summary.
        /// </visible>
        /// <param name="layerName"></param>
        /// <returns></returns>
        public virtual bool IsLayerEnable(string layerName)
        {
            throw new NotImplementedException();
        }

        public virtual Dictionary<string, string> GetLayersName()
        {
            throw new NotImplementedException();
        }

        public virtual void EnableAddPositionTool(bool enable)
        {
            throw new NotImplementedException();
        }

        public virtual void ClearTables()
        {
            throw new NotImplementedException();
        }

        public virtual void ClearTable(string tableName)
        {
            throw new NotImplementedException();
        }

        public virtual void RecalculateAll()
        {
            throw new NotImplementedException();
        }

        public virtual bool CheckOverlaps(List<GeoPoint> points, List<GeoPoint> points2, string tableName)
        {
            throw new NotImplementedException();
        }

        public virtual bool CheckContains(List<GeoPoint> points, List<GeoPoint> points2, string tableName)
        {
            throw new NotImplementedException();
        }

        public virtual void InsertDistanceSegment(List<GeoPoint> points, bool isDistance)
        {
            throw new NotImplementedException();
        }

        public virtual void CreateLineTable(string type)
        {
            throw new NotImplementedException();
        }

        public virtual void CreateTable(MapObject emptyObject, string tableName, bool objEnable, bool lblEnable, int objZoom, int lblZoom)
        {
            throw new NotImplementedException();
        }

        public virtual void ClearActions(MapActions clearAction)
        {
            throw new NotImplementedException();
        }

        public virtual void HighLightListObjects(List<HLGisObject> list)
        {
            throw new NotImplementedException();
        }

        public virtual void HighLightIncident(MapObjectIncident obj)
        {
            throw new NotImplementedException();
        }

        public virtual void HighLightUnit(MapObjectUnit obj)
        {
            throw new NotImplementedException();
        }

        public virtual void HighLightSingle(GeoPoint point)
        {
            throw new NotImplementedException();
        }

        public virtual void HighLightUnits(MapObjectIncident inc, List<HLGisObject> list, Color color)
        {
            throw new NotImplementedException();
        }

        public virtual void HighLightUnits(List<HLGisObject> list)
        {
            throw new NotImplementedException();
        }

        public virtual void CenterMapInPoint(GeoPoint point)
        {
            throw new NotImplementedException();
        }

        public virtual void CenterMap(int viewDistance)
        {
            throw new NotImplementedException();
        }

        public virtual void CenterMap()
        {
            throw new NotImplementedException();
        }

        public virtual void RefreshZoomTrackBar()
        {
            throw new NotImplementedException();
        }

        //TODO: ELIMINAR--> debería usarse IsertTempObject
        public virtual void InsertPointInTempLayer(GeoPoint geoPoint)
        {
            throw new NotImplementedException();
        }

        //TODO: ELIMINAR--> debería usarse IsertTempObject
        public virtual void InsertTempPolygonInLayer(List<GeoPoint> points, string p)
        {
            throw new NotImplementedException();
        }

        //NUEVO METODO PARA RESUMIR LOS DOS ANTERIORES
        public virtual void InsertTempObject(MapObject obj, string tableName)
        {
            throw new NotImplementedException();
        }

        public virtual void SetToolUseDefaultCursor(bool p, Cursor cursor)
        {
            throw new NotImplementedException();
        }

        public virtual void MakeLayerSelectable(string layerName)
        {
            throw new NotImplementedException();
        }

        public virtual void ReloadMap()
        {
            throw new NotImplementedException();
        }

        public virtual void ChangeAddPositionToolState(AddPositionToolState newState)
        {
            throw new NotImplementedException();
        }

        public virtual void RefreshMap()
        {
            throw new NotImplementedException();
        }

        public virtual void DeleteAllTracksUnless(int unitCode)
        {
            throw new NotImplementedException();
        }

        public virtual void SetLeftButtonTool(string p)
        {
            throw new NotImplementedException();
        }

        public virtual void ShowMessageBallon()
        {
            throw new NotImplementedException();
        }

        public virtual void MapDraw(GeoPoint geoPoint, ImageSearchState setVisibleImageSearch)
        {
            throw new NotImplementedException();
        }

        public virtual void ToolFeatureSelected(EventHandler toolstripmenuitem_Click, FeatureSelectedEventArgs e, ref ToolTipController toolTipController1)
        {
            throw new NotImplementedException();
        }

        public BallonData CreateBallonData(string code, string type, GeoPoint centroid, double lon, double lat)
        {
            System.Drawing.Point point;
            point = GetRelativePoint(centroid);

            BallonData bd = new BallonData()
            {
                Code = code,
                Type = type,
                Position = point,
                Lat = lat,
                Lon = lon
            };

            return bd;
        }

        public virtual void DrawPing(GeoPoint selectedPosition)
        {
            throw new NotImplementedException();
        }

        public virtual bool ReproduceTrack(MapPoint mapPoint)
        {
            throw new NotImplementedException();
        }

        public virtual void RestoreAllTracksUnless(int unitCode)
        {
            throw new NotImplementedException();
        }

        public virtual void DeleteAllUnitTracks()
        {
            throw new NotImplementedException();
        }

        public virtual void InitializeMaps()
        {
            throw new NotImplementedException();
        }

        public virtual IList<GeoAddress> Search(GeoAddress geoAddress)
        {
            throw new NotImplementedException();
        }

        public virtual void BuildPreQuerys(Dictionary<string, IList> layersInfo)
        {
            throw new NotImplementedException();
        }

        public virtual void InitializeMouseWheelSupport()
        {
            throw new NotImplementedException();
        }

        public virtual void ShowCrimeMap(List<GeoPoint> source, int zoom, int clusterSize)
        {
            throw new NotImplementedException();
        }

        public virtual void ShowHeatMap(List<GeoPoint> source, int zoom)
        {
            HeatPoints = source;
            ShowingHeatMap = true;
        }

        public List<GeoPoint> HeatPoints { get; set; }


        /// <summary>
        /// mapControl_MouseMove helper Method 
        /// </summary>
        /// <param name="degfloat"></param>
        /// <returns></returns>
        public string degToDMS(double degfloat)
        {
            degfloat = Math.Abs(degfloat);
            double deg = Math.Truncate(degfloat);
            double minfloat = 60 * (degfloat - deg);
            double min = Math.Truncate(minfloat);
            double secfloat = 60 * (minfloat - min);
            secfloat = Math.Round(secfloat, 3);

            if (secfloat >= 60)
            {
                min = min + 1;
                secfloat = 0;
            }

            if (min == 60)
            {
                deg++;
                min = 0;
            }
            return deg + "°" + min + "'" + secfloat + "\"";
        }

        #endregion

        #region Events

        public virtual event ToolUsedEventHandler ToolUsed;
        public virtual event ToolEndingEventHandler ToolEnding;
        public virtual event ToolActivatedEventHandler ToolActivated;
        public virtual event ViewChangedEventHandler ViewChanged;
        public virtual event FeatureAddingEventHandler FeatureAdding;
        public virtual event FeatureSelectedEventHandler FeatureSelected;
        public virtual event DrawEventHandler DrawEvent;
        public virtual event ShowBallonEventHandler ShowBallonEvent;
        public virtual event ShowSuperToolTipEventHandler ShowSuperToolTipEvent;
        public virtual event EventHandler Initialized;


        public delegate void ToolUsedEventHandler(object sender, ToolUsedEventArgs e);
        public delegate void ToolEndingEventHandler(object sender, ToolEndingEventArgs e);
        public delegate void ToolActivatedEventHandler(object sender, ToolActivatedEventArgs e);
        public delegate void ViewChangedEventHandler(object sender, ViewChangedEventArgs e);
        public delegate void FeatureAddingEventHandler(object sender, FeatureAddingEventArgs e);
        public delegate void FeatureSelectedEventHandler(object sender, FeatureSelectedEventArgs e);
        public delegate void DrawEventHandler(object sender, DrawEventArgs e);
        public delegate void ShowBallonEventHandler(object sender, BallonData ballon);
        public delegate void ShowSuperToolTipEventHandler(object sender, ShowSuperToolTipEventArgs e);




        #endregion
    }

    public class BallonData
    {
        public string Code { get; set; }
        public string Type { get; set; }
        public System.Drawing.Point Position { get; set; }
        public double Lat { get; set; }
        public double Lon { get; set; }
    }

    public class ToolUsedEventArgs
    {
        public ToolStatus ToolStatus { get; set; }

        public GeoPoint Position { get; set; }

        public string ToolName { get; set; }

        public ToolUsedEventArgs(ToolStatus status, GeoPoint pos, string name)
        {
            ToolStatus = status;
            Position = pos;
            ToolName = name;
        }
    }

    public class ToolEndingEventArgs
    {
        public String ToolName { get; set; }

        public ToolEndingEventArgs(string name)
        {
            ToolName = name;
        }
    }

    public class FeatureAddingEventArgs
    {
        public string ToolName { get; set; }

        public FeatureAddingEventArgs(string name)
        {
            ToolName = name;
        }
    }

    public class ToolActivatedEventArgs
    {
        public String ToolName { get; set; }

        public ToolActivatedEventArgs(string name)
        {
            ToolName = name;
        }
    }

    public class ViewChangedEventArgs
    {
        public ViewChangedEventArgs()
        {
        }
    }

    public class FeatureSelectedEventArgs
    {
        public object Selection { get; set; }

        public GeoPoint Position { get; set; }

        public FeatureSelectedEventArgs(object selection, GeoPoint geoPoint)
        {
            Selection = selection;
            Position = geoPoint;
        }
    }

    public class DrawEventArgs
    {
        public DrawEventArgs()
        {
        }
    }

    public class ShowSuperToolTipEventArgs
    {
        public List<string> Fields { get; set; }

        public Type ObjectType { get; set; }

        public int ObjectCode { get; set; }

        public GeoPoint Position { get; set; }

        public ShowSuperToolTipEventArgs(Type objectType, int code, GeoPoint geoPoint)
        {
            ObjectType = objectType;
            ObjectCode = code;
            Position = geoPoint;
            Fields = new List<string>();
        }

        public ShowSuperToolTipEventArgs(Type objectType, int code, GeoPoint geoPoint, List<string> data)
        {
            ObjectType = objectType;
            ObjectCode = code;
            Position = geoPoint;
            Fields = data;
        }
    }

    public class HeatMap
    {
        public List<HeatPoint> HeatPoints { get; set; }

        public HeatMap()
        {
            HeatPoints = new List<HeatPoint>();
        }

        public Bitmap CreateIntensityMask(Bitmap bSurface)
        {
            // Create new graphics surface from memory bitmap
            Graphics DrawSurface = Graphics.FromImage(bSurface);

            // Set background color to white so that pixels can be correctly colorized
            DrawSurface.Clear(Color.White);

            //DrawHeatPoint(DrawSurface, new HeatPoint(0,0,(byte)20), 650);
            // Traverse heat point data and draw masks for each heat point
            foreach (HeatPoint DataPoint in HeatPoints)
            {
                // Render current heat point on draw surface
                DrawHeatPoint(DrawSurface, DataPoint, 25);
            }

            return bSurface;
        }

        private void DrawHeatPoint(Graphics Canvas, HeatPoint HeatPoint, int Radius)
        {
            // Create points generic list of points to hold circumference points
            List<Point> CircumferencePointsList = new List<Point>();

            // Create an empty point to predefine the point struct used in the circumference loop
            Point CircumferencePoint;

            // Create an empty array that will be populated with points from the generic list
            Point[] CircumferencePointsArray;

            // Calculate ratio to scale byte intensity range from 0-255 to 0-1
            float fRatio = 1F / Byte.MaxValue;
            // Precalulate half of byte max value
            byte bHalf = Byte.MaxValue / 2;
            // Flip intensity on it's center value from low-high to high-low
            int iIntensity = (byte)(HeatPoint.Intensity - ((HeatPoint.Intensity - bHalf) * 2));
            // Store scaled and flipped intensity value for use with gradient center location
            float fIntensity = iIntensity * fRatio;

            // Loop through all angles of a circle
            // Define loop variable as a double to prevent casting in each iteration
            // Iterate through loop on 10 degree deltas, this can change to improve performance
            for (double i = 0; i <= 360; i += 5)
            {
                // Replace last iteration point with new empty point struct
                CircumferencePoint = new Point();

                // Plot new point on the circumference of a circle of the defined radius
                // Using the point coordinates, radius, and angle
                // Calculate the position of this iterations point on the circle
                CircumferencePoint.X = Convert.ToInt32(HeatPoint.X + Radius * Math.Cos(ConvertDegreesToRadians(i)));
                CircumferencePoint.Y = Convert.ToInt32(HeatPoint.Y + Radius * Math.Sin(ConvertDegreesToRadians(i)));

                // Add newly plotted circumference point to generic point list
                CircumferencePointsList.Add(CircumferencePoint);
            }

            // Populate empty points system array from generic points array list
            // Do this to satisfy the datatype of the PathGradientBrush and FillPolygon methods
            CircumferencePointsArray = CircumferencePointsList.ToArray();

            // Create new PathGradientBrush to create a radial gradient using the circumference points
            PathGradientBrush GradientShaper = new PathGradientBrush(CircumferencePointsArray);

            // Create new color blend to tell the PathGradientBrush what colors to use and where to put them
            ColorBlend GradientSpecifications = new ColorBlend(3);

            // Define positions of gradient colors, use intesity to adjust the middle color to
            // show more mask or less mask
            GradientSpecifications.Positions = new float[3] { 0, fIntensity, 1 };
            // Define gradient colors and their alpha values, adjust alpha of gradient colors to match intensity
            GradientSpecifications.Colors = new Color[3]
            {
                Color.FromArgb(0, Color.White),
                Color.FromArgb(HeatPoint.Intensity, Color.Black),
                Color.FromArgb(HeatPoint.Intensity, Color.Black)
            };

            // Pass off color blend to PathGradientBrush to instruct it how to generate the gradient
            GradientShaper.InterpolationColors = GradientSpecifications;

            // Draw polygon (circle) using our point array and gradient brush
            Canvas.FillPolygon(GradientShaper, CircumferencePointsArray);
        }

        private double ConvertDegreesToRadians(double degrees)
        {
            double radians = (Math.PI / 180) * degrees;
            return (radians);
        }

        public Bitmap Colorize(Bitmap Mask, byte Alpha)
        {
            Mask.MakeTransparent(Color.Transparent);

            // Create new bitmap to act as a work surface for the colorization process
            Bitmap Output = new Bitmap(Mask.Width, Mask.Height, PixelFormat.Format32bppArgb);

            // Create a graphics object from our memory bitmap so we can draw on it and clear it's drawing surface
            Graphics Surface = Graphics.FromImage(Output);
            Surface.Clear(Color.Transparent);

            // Build an array of color mappings to remap our greyscale mask to full color
            // Accept an alpha byte to specify the transparancy of the output image
            ColorMap[] Colors = CreatePaletteIndex(Alpha);

            // Create new image attributes class to handle the color remappings
            // Inject our color map array to instruct the image attributes class how to do the colorization
            ImageAttributes Remapper = new ImageAttributes();
            Remapper.SetRemapTable(Colors);

            // Draw our mask onto our memory bitmap work surface using the new color mapping scheme
            Surface.DrawImage(Mask, new Rectangle(0, 0, Mask.Width, Mask.Height), 0, 0, Mask.Width, Mask.Height, GraphicsUnit.Pixel, Remapper);

           // Output.MakeTransparent(Color.Transparent);

            // Send back newly colorized memory bitmap
            return Output;
        }


        private ColorMap[] CreatePaletteIndex(byte Alpha)
        {
            ColorMap[] OutputMap = new ColorMap[256];

            // Change this path to wherever you saved the palette image.
            Bitmap Palette = (Bitmap)ResourceLoader.GetImage("HeatPaletteImage");

            // Loop through each pixel and create a new color mapping
            for (int X = 0; X <= 255; X++)
            {
                OutputMap[X] = new ColorMap();
                OutputMap[X].OldColor = Color.FromArgb(X, X, X);
                OutputMap[X].NewColor = Color.FromArgb(Alpha, Palette.GetPixel(X, 0));
            }

            return OutputMap;
        }
    }

    public struct HeatPoint
    {
        public int X;
        public int Y;
        public byte Intensity;

        public HeatPoint(int iX, int iY, byte bIntensity)
        {
            X = iX;
            Y = iY;
            Intensity = bIntensity;
        }
    }
}
