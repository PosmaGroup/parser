using System;
using System.Collections.Generic;
using System.Text;

namespace Smartmatic.SmartCad.Map
{
    public class GisException : ApplicationException
    {
        public GisException(string message)
            : base(message)
        {
        }

        public GisException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
