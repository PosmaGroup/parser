using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Smartmatic.SmartCad.Map
{
    public class DelegateThreadMap
    {
        private bool running;
        private Thread thread;
        private AutoResetEvent beginEvent;
        private AutoResetEvent endEvent;
        private Delegate del;
        private object[] args;
        private object result;
        private Exception exception;

        public DelegateThreadMap()
        {
            beginEvent = new AutoResetEvent(false);
            endEvent = new AutoResetEvent(false);
            thread = new Thread(new ThreadStart(RunThread));
        }

        public void Start()
        {
            thread.Start();
        }

        private void RunThread()
        {
            running = true;

            while (running)
            {
                beginEvent.WaitOne();

                // For minimize the probability of dead in endEvent.WaitOne();
                Thread.Sleep(50);

                result = null;
                exception = null;

                if (del != null)
                {
                    try
                    {
                        result = del.DynamicInvoke(args);
                    }
                    catch (Exception ex)
                    {
                        exception = ex.InnerException;
                    }
                }
                endEvent.Set();

                del = null;
                args = null;
            }
        }

        public void Stop()
        {
            running = false;
            
            CallDelegate(null, null);
            
            try
            {
                thread.Abort();
            }
            catch
            {
            }
        }

        public object CallDelegate(Delegate del, params object[] args)
        {
            this.del = del;
            this.args = args;

            beginEvent.Set();

            endEvent.WaitOne();

            if (exception != null)
            {
                throw exception;
            }

            return result;
        }
    }
}
