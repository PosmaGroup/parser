﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Smartmatic.SmartCad.Telephony.Genesys
{
    public static class GenesysProperties
    {
        public const string MAIN_SERVER_IP = "SERVER_IP";
        public const string MAIN_SERVER_PORT = "SERVER_PORT";
        public const string BACKUP_SERVER_IP = "BACKUP_SERVER_IP";
        public const string BACKUP_SERVER_PORT = "BACKUP_SERVER_PORT";
        public const string SERVICE_NAME = "SERVICE_NAME";
        public const string SERVER_LOGIN = "SERVER_LOGIN";
        public const string SERVER_PASSWORD = "SERVER_PASSWORD";

        public const string STATS_SERVER_IP = "STADISTICS_SERVER_IP";
        public const string STATS_SERVER_PORT = "STADISTICS_SERVER_PORT";
        public const string STATS_SERVICE_NAME = "STADISTICS_SERVICE_NAME";
        public const string STATS_SERVER_LOGIN = "STADISTICS_SERVER_LOGIN";
        public const string STATS_SERVER_PASSWORD = "STADISTICS_SERVER_PASSWORD";
    }
}
