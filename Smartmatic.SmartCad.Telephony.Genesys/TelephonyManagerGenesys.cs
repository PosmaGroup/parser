﻿using Genesyslab.Platform.ApplicationBlocks.Commons.Protocols;
using Genesyslab.Platform.ApplicationBlocks.WarmStandby;
using Genesyslab.Platform.ApplicationBlocks.Commons.Broker;
using Genesyslab.Platform.Commons.Protocols;
using Genesyslab.Platform.Voice.Protocols.TServer.Events;
using Genesyslab.Platform.Voice.Protocols.TServer.Requests.Agent;
using Genesyslab.Platform.Voice.Protocols.TServer;
using Genesyslab.Platform.Voice.Protocols.TServer.Requests.Dn;
using Genesyslab.Platform.Voice.Protocols.TServer.Requests.Party;
using Genesyslab.Platform.Voice.Protocols;
using Genesyslab.Platform.Voice.Protocols.TServer.Requests.Queries;
using Genesyslab.Platform.Commons.Collections;
using Genesyslab.Platform.Voice.Protocols.TServer.Requests.Userdata;
using SmartCadCore.Common;
using SmartCadCore.Core;
using System;
using System.Collections;
//using System.Runtime.Remoting.Messaging;

namespace Smartmatic.SmartCad.Telephony.Genesys
{
    public class TelephonyManagerGenesys : TelephonyManagerAbstract
    {
        // Enter configuration information here:
        private string tServerName;
        private string tServerHost;
        private string tServerPort;
        private string tServerHostBackup;
        private string tServerPortBackup;
        private string thisDn;
        // Your switch may not require a user name and password:
        private string sipServerLogin;
        private string sipServerPassword;
        private string agentLogin;
        private string agentPassword;
        private string thisQueue;
        private TServerConfiguration tServerConfiguration;
        private Uri connectionURI;

        private ProtocolManagementService mProtocolManager;
        private EventBrokerService mEventBroker;
        private static int requestId = 1;
        static Random rnd = new Random((int)DateTime.Now.Ticks);
        private Hashtable RequestHash = new Hashtable();
        private bool isReady = false;
        // Data to handle calls.
        private ConnectionId callID;
        //private ConnectionId RelatedConnID;
        //private bool InConsultation = false;
        //private bool isConsultationTransfer = false;


        public TelephonyManagerGenesys(Hashtable configProperties)
            : base(configProperties)
        {
            tServerName = configurationProperties[GenesysProperties.SERVICE_NAME] as string;
            tServerHost = configurationProperties[GenesysProperties.MAIN_SERVER_IP] as string;
            tServerPort = configurationProperties[GenesysProperties.MAIN_SERVER_PORT].ToString();
            tServerHostBackup = configurationProperties[GenesysProperties.BACKUP_SERVER_IP] as string;
            tServerPortBackup = configurationProperties[GenesysProperties.BACKUP_SERVER_PORT] as string;
            thisDn = configurationProperties[CommonProperties.Extension] as string;
            sipServerLogin = configurationProperties[GenesysProperties.SERVER_LOGIN] as string;
            sipServerPassword = configurationProperties[GenesysProperties.SERVER_PASSWORD] as string;
            if (sipServerPassword.Trim() != "")
                sipServerPassword = CryptoUtil.DecryptWithRijndael(sipServerPassword).TrimEnd('\0');
            agentLogin = configurationProperties[CommonProperties.UserLogin] as string;
            agentPassword = configurationProperties[CommonProperties.UserPassword] as string;
            thisQueue = configurationProperties[CommonProperties.Queue] as string;
            Init();
            Login(agentLogin, agentPassword, true);
        }

        public override string TestCTIState()
        {
            RequestQueryAddress request = RequestQueryAddress.Create(thisDn, AddressType.DN, AddressInfoType.AddressStatus);
            int uniqueId = GenerateUniqueId();
            request.ReferenceID = uniqueId;
            RequestHash.Add(uniqueId, "RequestQueryAddress");
            try
            {
                IMessage response = mProtocolManager[tServerName].Request(request);
                if (response is Genesyslab.Platform.Configuration.Protocols.ConfServer.Events.EventError)
                {
                    //SendTelephonyEventArgs(new TelephonyErrorEventArgs("DN is not configured in CME", thisDn, thisDn, thisQueue, TimeSpan.Zero));
                    throw new Exception("DN is not configured in CME");
                }
            }
            catch
            {
                throw;
            }
            return "CONNECTION_OK";
        }

        //<summary> Initializes the protocols required to connect your application.
        //This method uses the <code>TServerConfiguration</code> class to describe
        //the connection to the T-Server, and to connect, it reads parameters specified in the AgentData instance.
        //</summary>
        private void InitializeProtocol()
        {
            tServerConfiguration = new TServerConfiguration(tServerName);
            tServerConfiguration.ClientName = sipServerLogin;
            tServerConfiguration.ClientPassword = sipServerPassword;
            if (!string.IsNullOrEmpty(tServerHostBackup) && !string.IsNullOrEmpty(tServerPortBackup))
            {
                try
                {
                    tServerConfiguration.WarmStandbyUri = new Uri("tcp://" + tServerHostBackup + ":" + tServerPortBackup);
                    tServerConfiguration.FaultTolerance = FaultToleranceMode.HotStandby;
                    tServerConfiguration.WarmStandbyAttempts = 5;
                    tServerConfiguration.WarmStandbyTimeout = 10;
                }
                catch (Exception ex)
                {
                    SmartLogger.Print(ex);
                }
            }
            connectionURI = new Uri("tcp://" + tServerHost + ":" + tServerPort);
            tServerConfiguration.Uri = connectionURI;

            mProtocolManager = new ProtocolManagementService();
            IProtocol p1 = mProtocolManager.Register(tServerConfiguration);
            try
            {
                mProtocolManager[tServerName].Open();

            }
            catch (Exception e)
            {
                throw e;
            }
            // Creates the configuration object used to connect to the T-Server
            tServerConfiguration = new TServerConfiguration(tServerName);
            // User information for the T-Server            

            try
            {
                tServerConfiguration.Uri = new Uri("tcp://" + tServerHost + ":" + tServerPort);
            }
            catch (Exception e)
            {
                // adds error handling
                throw e;
            }
        }

        //<summary> Initializes the event broker application block, then registers for each event 
        // that this PSDK application is listening.<BR>
        // A new class <code>OnEvent_myEvent_</code> is registered for each event <code>_myEvent_</code> 
        // and implements the <code>Action<Message></code> interface. 
        //</summary>

        private void InitializeEventBroker()
        {
            // Set up event broker service
            mEventBroker = new EventBrokerService(mProtocolManager.Receiver);
            // register event handlers with the broker
            mEventBroker.Register(this.OnEventAgentLogin);
            mEventBroker.Register(this.OnEventAgentLogout);
            mEventBroker.Register(this.OnEventAgentNotReady);
            mEventBroker.Register(this.OnEventAbandoned);
            mEventBroker.Register(this.OnEventAgentReady);
            mEventBroker.Register(this.OnEventDialing);
            mEventBroker.Register(this.OnEventError);
            mEventBroker.Register(this.OnEventEstablished);
            mEventBroker.Register(this.OnEventHeld);
            mEventBroker.Register(this.OnEventRegistered);
            mEventBroker.Register(this.OnEventReleased);
            mEventBroker.Register(this.OnEventRetrieved);
            mEventBroker.Register(this.OnEventRinging);
            mEventBroker.Register(this.OnEventAttachedData);
        }

        private void DisconnectEventBroker()
        {
            // unregister event handlers with the broker
            mEventBroker.Unregister(this.OnEventAgentLogin);
            mEventBroker.Unregister(this.OnEventAgentLogout);
            mEventBroker.Unregister(this.OnEventAgentNotReady);
            mEventBroker.Unregister(this.OnEventAbandoned);
            mEventBroker.Unregister(this.OnEventAgentReady);
            mEventBroker.Unregister(this.OnEventDialing);
            mEventBroker.Unregister(this.OnEventError);
            mEventBroker.Unregister(this.OnEventEstablished);
            mEventBroker.Unregister(this.OnEventHeld);
            mEventBroker.Unregister(this.OnEventRegistered);
            mEventBroker.Unregister(this.OnEventReleased);
            mEventBroker.Unregister(this.OnEventRetrieved);
            mEventBroker.Unregister(this.OnEventRinging);
            mEventBroker.Unregister(this.OnEventAttachedData);
            mEventBroker.Dispose();
        }

        private int GenerateUniqueId()
        {
            // Create a unique ID number
            int currentId = ++requestId;
            currentId = rnd.Next(10000000);
            // Return the ID number
            return currentId;
        }

        private String CreateTimeStamp()
        {
            return System.DateTime.UtcNow.ToString();

        }

        #region Telephony Events
        [MessageIdFilter(EventAgentLogin.MessageId, ProtocolName = "TServer", SdkName = "Voice")]
        private void OnEventAgentLogin(IMessage obj)
        {
            if (obj == null)
            {
                SmartLogger.Print("Message is null");
            }
            else
            {
                EventAgentLogin evnt = (EventAgentLogin)obj;
                OnAgentLogin(new AgentLoginEventArgs(evnt.ThisDN, evnt.AgentID, evnt.ThisQueue, TimeSpan.FromSeconds((double)evnt.Time.TimeinSecs)));
            }            
        }

        [MessageIdFilter(EventAgentLogout.MessageId, ProtocolName = "TServer", SdkName = "Voice")]
        private void OnEventAgentLogout(IMessage obj)
        {
            if (obj == null)
            {
                SmartLogger.Print("Message is null");
            }
            else
            {
                EventAgentLogout evnt = (EventAgentLogout)obj;
                OnAgentLogout(new AgentLogoutEventArgs(evnt.ThisDN, evnt.AgentID, evnt.ThisQueue, TimeSpan.FromSeconds((double)evnt.Time.TimeinSecs)));
            }
        }

        [MessageIdFilter(EventAbandoned.MessageId, ProtocolName = "TServer", SdkName = "Voice")]
        private void OnEventAbandoned(IMessage obj)
        {
            if (obj == null)
            {
                SmartLogger.Print("Message is null");
            }
            else
            {
                EventAbandoned evnt = (EventAbandoned)obj;
                string ani = string.IsNullOrEmpty(evnt.ANI) ? (string.IsNullOrEmpty(evnt.OtherDN) ? "9999" : evnt.OtherDN) : evnt.ANI;
                OnCallAbandoned(new CallAbandonedEventArgs(ani, evnt.AgentID, evnt.ThisQueue, TimeSpan.FromSeconds((double)evnt.Time.TimeinSecs)));
            }
        }

        [MessageIdFilter(EventAgentNotReady.MessageId, ProtocolName = "TServer", SdkName = "Voice")]
        private void OnEventAgentNotReady(IMessage obj)
        {
            if (obj == null)
            {
                SmartLogger.Print("Message is null");
            }
            else
            {
                EventAgentNotReady evnt = (EventAgentNotReady)obj;
                string reason = string.Empty;
                if (evnt.AgentWorkMode == AgentWorkMode.Unknown)
                    reason = "CALL_NOT_ANSWERED";
                OnAgentNotReady(new AgentNotReadyEventArgs(evnt.ThisDN, evnt.AgentID, evnt.ThisQueue, TimeSpan.FromSeconds((double)evnt.Time.TimeinSecs), reason));
            }
        }

        [MessageIdFilter(EventAgentReady.MessageId, ProtocolName = "TServer", SdkName = "Voice")]
        private void OnEventAgentReady(IMessage obj)
        {
            if (obj == null)
            {
                SmartLogger.Print("Message is null");
            }
            else
            {
                EventAgentReady evnt = (EventAgentReady)obj;
                OnAgentReady(new AgentReadyEventArgs(evnt.ThisDN, evnt.AgentID, evnt.ThisQueue, TimeSpan.FromSeconds((double)evnt.Time.TimeinSecs), string.Empty));
            }
        }

        [MessageIdFilter(EventDialing.MessageId, ProtocolName = "TServer", SdkName = "Voice")]
        private void OnEventDialing(IMessage obj)
        {
            if (obj == null)
            {
                SmartLogger.Print("Message is null");
            }
            else
            {
                EventDialing evnt = (EventDialing)obj;
                callID = evnt.ConnID;
                string ani = string.IsNullOrEmpty(evnt.ANI) ? (string.IsNullOrEmpty(evnt.OtherDN) ? "9999" : evnt.OtherDN) : evnt.ANI;
                OnCallDialing(new CallDialingEventArgs(ani, evnt.AgentID, evnt.ThisQueue, evnt.ThisDN, TimeSpan.FromSeconds((double)evnt.Time.TimeinSecs)));
            }
        }

        [MessageIdFilter(EventError.MessageId, ProtocolName = "TServer", SdkName = "Voice")]
        private void OnEventError(IMessage obj)
        {
            if (obj == null)
            {
                SmartLogger.Print("Message is null");
            }
            else
            {
                EventError evnt = (EventError)obj;
                callID = null;
                SendTelephonyEventArgs(new TelephonyErrorEventArgs(evnt.ErrorMessage, evnt.AgentID, evnt.ThisDN, evnt.ThisQueue, TimeSpan.FromSeconds((double)evnt.Time.TimeinSecs)));
            }
        }

        [MessageIdFilter(EventEstablished.MessageId, ProtocolName = "TServer", SdkName = "Voice")]
        private void OnEventEstablished(IMessage obj)
        {
            if (obj == null)
            {
                SmartLogger.Print("Message is null");
            }
            else
            {
                EventEstablished evnt = (EventEstablished)obj;
                string ani = string.IsNullOrEmpty(evnt.ANI) ? (string.IsNullOrEmpty(evnt.OtherDN) ? "9999" : evnt.OtherDN) : evnt.ANI;
                OnCallEstablished(new CallEstablishedEventArgs(ani, evnt.AgentID, evnt.ThisQueue, TimeSpan.FromSeconds((double)evnt.Time.TimeinSecs)));
                SetReadyStatus(false);
            }
        }

        [MessageIdFilter(EventHeld.MessageId, ProtocolName = "TServer", SdkName = "Voice")]
        private void OnEventHeld(IMessage obj)
        {
            if (obj == null)
            {
                SmartLogger.Print("Message is null");
            }
            else
            {
                EventHeld evnt = (EventHeld)obj;
                string ani = string.IsNullOrEmpty(evnt.ANI) ? (string.IsNullOrEmpty(evnt.OtherDN) ? "9999" : evnt.OtherDN) : evnt.ANI;
                OnCallHeld(new CallHeldEventArgs(ani, evnt.AgentID, evnt.ThisQueue, TimeSpan.FromSeconds((double)evnt.Time.TimeinSecs)));
            }
        }

        [MessageIdFilter(EventRegistered.MessageId, ProtocolName = "TServer", SdkName = "Voice")]
        private void OnEventRegistered(IMessage obj)
        {
            if (obj == null)
            {
                SmartLogger.Print("Message received is null");
            }
            else if (obj is EventRegistered)
            {
                int uniqueId = GenerateUniqueId();
                //writeToLogArea("\nRequest: Registered (Id " + uniqueId + ")");
                RequestAgentLogin requestAgentLogin = RequestAgentLogin.Create(thisDn, AgentWorkMode.AfterCallWork);
                requestAgentLogin.ThisQueue = thisQueue;
                // Note: your switch may not need a user name and password:
                requestAgentLogin.AgentID = agentLogin;
                requestAgentLogin.Password = agentPassword;
                requestAgentLogin.ReferenceID = uniqueId;
                try
                {
                    //mProtocolManager[tServerName].Send(requestAgentLogin);
                    IMessage mess = mProtocolManager[tServerName].Request(requestAgentLogin);
                    if (mess is EventError)
                    {
                        OnEventError(obj);
                        try
                        {
                            RequestAgentLogout requestAgentLogout =
                                RequestAgentLogout.Create(thisDn);
                        }
                        catch
                        { }
                    }
                    else if (mess is EventAgentLogin)
                    {
                        OnEventAgentLogin(mess);
                    }
                    else
                    {
                        try
                        {
                            RequestAgentLogout requestAgentLogout =
                                RequestAgentLogout.Create(thisDn);
                        }
                        catch
                        { }
                    }
                    RequestHash.Add(uniqueId, "RequestAgentLogin");
                    /* Expected events
                     * 1. EventAgentLogin 
                     * 2. EventAgentReady
                     */
                }
                catch (ProtocolException protocolException)
                {
                    SmartLogger.Print(protocolException);
                    try
                    {
                        RequestAgentLogout requestAgentLogout = 
                            RequestAgentLogout.Create(thisDn);
                    }
                    catch
                    { }
                }               
            }
            else if (obj is EventError)
            {
                OnEventError(obj);
            }
        }

        [MessageIdFilter(EventReleased.MessageId, ProtocolName = "TServer", SdkName = "Voice")]
        private void OnEventReleased(IMessage obj)
        {
            if (obj == null)
            {
                SmartLogger.Print("Message is null");
            }
            else if (obj is EventError)
            {
                OnEventError(obj);
            }
            else
            {
                EventReleased evnt = (EventReleased)obj;
                if (callID != null && callID.ToString().CompareTo(evnt.ConnID.ToString()) == 0)
                {
                    callID = null;
                }
                string ani = string.IsNullOrEmpty(evnt.ANI) ? (string.IsNullOrEmpty(evnt.OtherDN) ? "9999" : evnt.OtherDN) : evnt.ANI;
                OnCallReleased(new CallReleasedEventArgs(ani, evnt.AgentID, evnt.ThisQueue, TimeSpan.FromSeconds((double)evnt.Time.TimeinSecs)));                
            }
        }

        [MessageIdFilter(EventRetrieved.MessageId, ProtocolName = "TServer", SdkName = "Voice")]
        private void OnEventRetrieved(IMessage obj)
        {
            if (obj == null)
            {
                SmartLogger.Print("Message is null");
            }
            else
            {
                EventRetrieved evnt = (EventRetrieved)obj;
                string ani = string.IsNullOrEmpty(evnt.ANI) ? (string.IsNullOrEmpty(evnt.OtherDN) ? "9999" : evnt.OtherDN) : evnt.ANI;
                OnCallRetrieved(new CallRetrievedEventArgs(ani, evnt.AgentID, evnt.ThisQueue, TimeSpan.FromSeconds(evnt.Time.TimeinSecs)));
            }
        }

        [MessageIdFilter(EventRinging.MessageId, ProtocolName = "TServer", SdkName = "Voice")]
        private void OnEventRinging(IMessage obj)
        {
            if (obj == null)
            {
                SmartLogger.Print("Message is null");
            }
            else
            {
                EventRinging evnt = (EventRinging)obj;
                if (evnt.ThisDN.CompareTo(thisDn) == 0 && callID == null)
                {
                    callID = evnt.ConnID;
                }
                string ani = string.IsNullOrEmpty(evnt.ANI) ? (string.IsNullOrEmpty(evnt.OtherDN) ? "9999": evnt.OtherDN) : evnt.ANI;

                OnCallRinging(new CallRingingEventArgs(ani, evnt.AgentID, evnt.ThisQueue, TimeSpan.FromSeconds(evnt.Time.TimeinSecs)));
            }
        }

        [MessageIdFilter(EventAttachedDataChanged.MessageId, ProtocolName = "TServer", SdkName = "Voice")]
        private void OnEventAttachedData(IMessage obj)
        {
            if (obj == null)
            {
                SmartLogger.Print("Message is null");
            }
            else if (obj is EventError)
            {
                OnEventError(obj);
            }
            else
            {
                SmartLogger.Print("Data attached in call");
            }
        }
        #endregion

        protected override void Init()
        {
            if (string.IsNullOrEmpty(tServerName) == false)
            {
                InitializeProtocol();
                InitializeEventBroker();
            }
            else
            {
                //TODO: JY manejar correctamente la validación
                throw new Exception("Faltan parámetros por suministrar");
            }
        }

        public override void Call(string number)
        {
            throw new NotImplementedException();
        }

        public override void Login(string agentId, string agentPassword, bool isReady)
        {
            try
            {
                RequestRegisterAddress requestRegisterAddress =
                    RequestRegisterAddress.Create(
                        thisDn,
                        RegisterMode.ModeShare,
                        ControlMode.RegisterDefault,
                        AddressType.DN);
                int uniqueId = GenerateUniqueId();
                requestRegisterAddress.ReferenceID = uniqueId;

                IMessage message = mProtocolManager[tServerName].Request(requestRegisterAddress);
                OnEventRegistered(message);
                RequestHash.Add(uniqueId, "RequestRegisterAddress");

            }
            catch (ProtocolException protocolException)
            {
                //TODO: JY Manejar la excepcion y propagarla a algo no dependiente del app block
                throw protocolException;
            }
        }

        public override void Logout()
        {
            RequestAgentLogout requestAgentLogout =
                RequestAgentLogout.Create(
                    thisDn);
            int uniqueId = GenerateUniqueId();
            requestAgentLogout.ReferenceID = uniqueId;
            try
            {
                mProtocolManager[tServerName].Send(requestAgentLogout);
                RequestHash.Add(uniqueId, "RequestAgentLogout");
            }
            catch (ProtocolException protocolException)
            {
                //TODO: JY Manejar la excepcion y propagarla a algo no dependiente del app block
                throw protocolException;
            }
        }

        public override void  SetReadyStatus(bool ready)
        {
            if (ready)
            {
                RequestAgentReady requestAgentReady =
                    RequestAgentReady.Create(
                        thisDn,
                        AgentWorkMode.ManualIn);
                int uniqueId = GenerateUniqueId();
                requestAgentReady.ReferenceID = uniqueId;
                try
                {
                    mProtocolManager[tServerName].Send(requestAgentReady);
                    RequestHash.Add(uniqueId, "RequestAgentReady");
                    isReady = true;
                }
                catch (ProtocolException protocolException)
                {
                    //TODO: JY Manejar la excepcion y propagarla a algo no dependiente del app block
                    throw protocolException;
                }
            }
            else
            {
                RequestAgentNotReady requestAgentNotReady =
                RequestAgentNotReady.Create(
                    thisDn,
                    AgentWorkMode.ManualIn);
                int uniqueId = GenerateUniqueId();
                requestAgentNotReady.ReferenceID = uniqueId;
                RequestHash.Add(uniqueId, "RequestAgentNotReady");

                try
                {
                    mProtocolManager[tServerName].Send(requestAgentNotReady);
                    isReady = false;
                }
                catch (ProtocolException protocolException)
                {
                    //TODO: JY Manejar la excepcion y propagarla a algo no dependiente del app block
                    throw protocolException;
                }
            }
        }

        public override void Answer()
        {
            RequestQueryAddress request = RequestQueryAddress.Create(thisDn, AddressType.DN, AddressInfoType.DNStatus);
            int uniqueId = GenerateUniqueId();
            request.ReferenceID = uniqueId;
            RequestHash.Add(uniqueId, "RequestQueryAddress");
            try
            {
                IMessage message = mProtocolManager[tServerName].Request(request);
                if (message is EventError)
                {
                    OnEventError(message);
                }
            }
            catch
            {
                EventError eventError = EventError.Create();
                eventError.ErrorMessage = "NoConnectionToCTI";
                eventError.AgentID = (string)configurationProperties[CommonProperties.AgentId];
                eventError.ThisDN = (string)configurationProperties[CommonProperties.Extension];
                eventError.ThisQueue = (string)configurationProperties[CommonProperties.Queue];
                eventError.Time = TimeStamp.Create();
                OnEventError(eventError);
            }
            #region Code to uncomment when sdk can answer
            //This code will be enabled when genesys sdk works fine
            //RequestAnswerCall requestAnswerCall =
            //    RequestAnswerCall.Create(
            //        thisDn,
            //        callID);
            //int uniqueId = GenerateUniqueId();
            //requestAnswerCall.ReferenceID = uniqueId;
            //RequestHash.Add(uniqueId, "RequestAnswerCall");
            //try
            //{
            //    //mProtocolManager[tServerName].Send(requestAnswerCall);
            //    IMessage message = mProtocolManager[tServerName].Request(requestAnswerCall);
            //    if (message is EventEstablished)
            //    {
            //        OnEventEstablished(message);
            //    }
            //    else if (message is EventError)
            //    {
            //        OnEventError(message);
            //    }

            //}
            //catch (ProtocolException protocolException)
            //{
            //    //TODO: JY Manejar la excepcion y propagarla a algo no dependiente del app block
            //    throw protocolException;
            //}
            #endregion
        }

        public override void Release()
        {
            RequestReleaseCall requestReleaseCall
                = RequestReleaseCall.Create(
                    thisDn,
                    callID);
            int uniqueId = GenerateUniqueId();
            requestReleaseCall.ReferenceID = uniqueId;
            RequestHash.Add(uniqueId, "RequestReleaseCall");
            try
            {
                IMessage message = mProtocolManager[tServerName].Request(requestReleaseCall);
                if (message == null)
                {
                    System.Threading.Thread.Sleep(200);
                    message = mProtocolManager[tServerName].Request(requestReleaseCall);
                }
                OnEventReleased(message);
            }
            catch (ProtocolException protocolException)
            {
                //TODO: JY Manejar la excepcion y propagarla a algo no dependiente del app block
                throw protocolException;
            }
        }

        public override void Hold()
        {
            RequestHoldCall requestHoldCall =
                RequestHoldCall.Create(
                    thisDn,
                    callID);
            int uniqueId = GenerateUniqueId();
            requestHoldCall.ReferenceID = uniqueId;
            try
            {
                mProtocolManager[tServerName].Send(requestHoldCall);
                RequestHash.Add(uniqueId, "RequestHoldCall");
            }
            catch (ProtocolException protocolException)
            {
                //TODO: JY Manejar la excepcion y propagarla a algo no dependiente del app block
                throw protocolException;
            }
        }

        public override void Transfer()
        {
            
        }

        /// <summary>
        /// Puts the current call in parking state, waiting for a dispatcher to re-take it.
        /// </summary>
        /// <param name="extensionParking">Extension number where call will be parked(queue number)</param>
        /// <param name="parkId">call id used to park and unpark the call</param>
        public override void ParkCall(string extensionParking, string parkId)
        {
            KeyValueCollection coll = new KeyValueCollection();
            coll.Add("TicketNumber", parkId);
            RequestUpdateUserData request = RequestUpdateUserData.Create(thisDn, callID, coll);
            int uniqueId = GenerateUniqueId();
            request.ReferenceID = uniqueId;
            try
            {
                IMessage message = mProtocolManager[tServerName].Request(request);
                OnEventAttachedData(message);
            }
            catch (ProtocolException protocolException)
            {
                throw protocolException;
            }
            uniqueId = GenerateUniqueId();
            RequestSingleStepTransfer requestSingleStepTransfer = RequestSingleStepTransfer.Create(
                    thisDn,
                    callID,
                    extensionParking);
            requestSingleStepTransfer.ReferenceID = uniqueId;
            try
            {
                mProtocolManager[tServerName].Send(requestSingleStepTransfer);
                RequestHash.Add(uniqueId, "RequestSingleStepTransfer");
            }
            catch (ProtocolException protocolException)
            {
                throw protocolException;
            }
        }

        public override void Conference()
        {

        }

        public override void RecoverConnection()
        {
            try
            {
                Close();
                Init();
                Login(agentLogin, agentPassword, true);
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
                throw ex;
            }
        }

        public override void Close()
        {
            DisconnectEventBroker();
            mProtocolManager[tServerName].Close(); 
        }

        public override bool GetReadyStatus()
        {
            return isReady;
        }

        public override void SetPhoneReport(string customCode)
        {
            throw new NotImplementedException();
        }
    }
}
