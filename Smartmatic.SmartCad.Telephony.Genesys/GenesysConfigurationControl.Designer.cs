﻿using SmartCadControls.Controls;

namespace Smartmatic.SmartCad.Telephony.Genesys
{
    partial class GenesysConfigurationControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxExServerPortStatGen = new TextBoxEx();
            this.labelExQueueTelGen = new LabelEx();
            this.labelExServiceTelGenMain = new LabelEx();
            this.textBoxExQueueTelGen = new TextBoxEx();
            this.textBoxExUserTelGen = new TextBoxEx();
            this.textBoxExServerIpTelGenBackup = new TextBoxEx();
            this.textBoxExPswTelGen = new TextBoxEx();
            this.textBoxExServerPortTelGenBackup = new TextBoxEx();
            this.labelExServerIpTelGenBackup = new LabelEx();
            this.labelExUserTelGen = new LabelEx();
            this.labelExPswTelGen = new LabelEx();
            this.labelExServerIpTelGenMain = new LabelEx();
            this.groupControlBackupServer = new DevExpress.XtraEditors.GroupControl();
            this.labelControlSeparatorBackup = new DevExpress.XtraEditors.LabelControl();
            this.labelExServiceNameStatGen = new LabelEx();
            this.textBoxExServiceStatGen = new TextBoxEx();
            this.textBoxExUserStatGen = new TextBoxEx();
            this.textBoxExPswStatGen = new TextBoxEx();
            this.groupControlMainServer = new DevExpress.XtraEditors.GroupControl();
            this.labelControlSeparatorMain = new DevExpress.XtraEditors.LabelControl();
            this.textBoxExServerIpTelGenMain = new TextBoxEx();
            this.textBoxExServerPortTelGenMain = new TextBoxEx();
            this.labelExServerIpStatGen = new LabelEx();
            this.labelExUserStatGen = new LabelEx();
            this.textBoxExServerIpStatGen = new TextBoxEx();
            this.labelExPswStatGen = new LabelEx();
            this.groupControlTelephonyGenesys = new DevExpress.XtraEditors.GroupControl();
            this.textBoxExServiceTelGenMain = new TextBoxEx();
            this.groupControlStatisticsGenesys = new DevExpress.XtraEditors.GroupControl();
            this.labelControlSeparator = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlBackupServer)).BeginInit();
            this.groupControlBackupServer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlMainServer)).BeginInit();
            this.groupControlMainServer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlTelephonyGenesys)).BeginInit();
            this.groupControlTelephonyGenesys.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlStatisticsGenesys)).BeginInit();
            this.groupControlStatisticsGenesys.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxExServerPortStatGen
            // 
            this.textBoxExServerPortStatGen.AllowsLetters = false;
            this.textBoxExServerPortStatGen.AllowsNumbers = true;
            this.textBoxExServerPortStatGen.AllowsPunctuation = true;
            this.textBoxExServerPortStatGen.AllowsSeparators = false;
            this.textBoxExServerPortStatGen.AllowsSymbols = false;
            this.textBoxExServerPortStatGen.AllowsWhiteSpaces = false;
            this.textBoxExServerPortStatGen.ExtraAllowedChars = "";
            this.textBoxExServerPortStatGen.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExServerPortStatGen.Location = new System.Drawing.Point(294, 24);
            this.textBoxExServerPortStatGen.MaxLength = 15;
            this.textBoxExServerPortStatGen.Name = "textBoxExServerPortStatGen";
            this.textBoxExServerPortStatGen.NonAllowedCharacters = "\'";
            this.textBoxExServerPortStatGen.RegularExpresion = "";
            this.textBoxExServerPortStatGen.Size = new System.Drawing.Size(76, 20);
            this.textBoxExServerPortStatGen.TabIndex = 3;
            this.textBoxExServerPortStatGen.TextChanged += new System.EventHandler(this.textBoxEx_TextChanged);
            // 
            // labelExQueueTelGen
            // 
            this.labelExQueueTelGen.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExQueueTelGen.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExQueueTelGen.Location = new System.Drawing.Point(8, 163);
            this.labelExQueueTelGen.Name = "labelExQueueTelGen";
            this.labelExQueueTelGen.Size = new System.Drawing.Size(88, 16);
            this.labelExQueueTelGen.TabIndex = 4;
            this.labelExQueueTelGen.Text = "Queue";
            // 
            // labelExServiceTelGenMain
            // 
            this.labelExServiceTelGenMain.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExServiceTelGenMain.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExServiceTelGenMain.Location = new System.Drawing.Point(8, 137);
            this.labelExServiceTelGenMain.Name = "labelExServiceTelGenMain";
            this.labelExServiceTelGenMain.Size = new System.Drawing.Size(88, 16);
            this.labelExServiceTelGenMain.TabIndex = 2;
            this.labelExServiceTelGenMain.Text = "ServiceName";
            // 
            // textBoxExQueueTelGen
            // 
            this.textBoxExQueueTelGen.AllowsLetters = false;
            this.textBoxExQueueTelGen.AllowsNumbers = true;
            this.textBoxExQueueTelGen.AllowsPunctuation = true;
            this.textBoxExQueueTelGen.AllowsSeparators = false;
            this.textBoxExQueueTelGen.AllowsSymbols = false;
            this.textBoxExQueueTelGen.AllowsWhiteSpaces = false;
            this.textBoxExQueueTelGen.ExtraAllowedChars = "";
            this.textBoxExQueueTelGen.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExQueueTelGen.Location = new System.Drawing.Point(130, 160);
            this.textBoxExQueueTelGen.MaxLength = 15;
            this.textBoxExQueueTelGen.Name = "textBoxExQueueTelGen";
            this.textBoxExQueueTelGen.NonAllowedCharacters = "\'";
            this.textBoxExQueueTelGen.RegularExpresion = "";
            this.textBoxExQueueTelGen.Size = new System.Drawing.Size(240, 20);
            this.textBoxExQueueTelGen.TabIndex = 5;
            this.textBoxExQueueTelGen.TextChanged += new System.EventHandler(this.textBoxEx_TextChanged);
            // 
            // textBoxExUserTelGen
            // 
            this.textBoxExUserTelGen.AllowsLetters = true;
            this.textBoxExUserTelGen.AllowsNumbers = true;
            this.textBoxExUserTelGen.AllowsPunctuation = true;
            this.textBoxExUserTelGen.AllowsSeparators = true;
            this.textBoxExUserTelGen.AllowsSymbols = true;
            this.textBoxExUserTelGen.AllowsWhiteSpaces = true;
            this.textBoxExUserTelGen.ExtraAllowedChars = "";
            this.textBoxExUserTelGen.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExUserTelGen.Location = new System.Drawing.Point(130, 187);
            this.textBoxExUserTelGen.MaxLength = 100;
            this.textBoxExUserTelGen.Name = "textBoxExUserTelGen";
            this.textBoxExUserTelGen.NonAllowedCharacters = "\'";
            this.textBoxExUserTelGen.RegularExpresion = "";
            this.textBoxExUserTelGen.Size = new System.Drawing.Size(240, 20);
            this.textBoxExUserTelGen.TabIndex = 7;
            this.textBoxExUserTelGen.TextChanged += new System.EventHandler(this.textBoxEx_TextChanged);
            // 
            // textBoxExServerIpTelGenBackup
            // 
            this.textBoxExServerIpTelGenBackup.AllowsLetters = true;
            this.textBoxExServerIpTelGenBackup.AllowsNumbers = true;
            this.textBoxExServerIpTelGenBackup.AllowsPunctuation = true;
            this.textBoxExServerIpTelGenBackup.AllowsSeparators = false;
            this.textBoxExServerIpTelGenBackup.AllowsSymbols = false;
            this.textBoxExServerIpTelGenBackup.AllowsWhiteSpaces = false;
            this.textBoxExServerIpTelGenBackup.ExtraAllowedChars = "";
            this.textBoxExServerIpTelGenBackup.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExServerIpTelGenBackup.Location = new System.Drawing.Point(125, 25);
            this.textBoxExServerIpTelGenBackup.MaxLength = 15;
            this.textBoxExServerIpTelGenBackup.Name = "textBoxExServerIpTelGenBackup";
            this.textBoxExServerIpTelGenBackup.NonAllowedCharacters = "\'";
            this.textBoxExServerIpTelGenBackup.RegularExpresion = "";
            this.textBoxExServerIpTelGenBackup.Size = new System.Drawing.Size(148, 20);
            this.textBoxExServerIpTelGenBackup.TabIndex = 1;
            this.textBoxExServerIpTelGenBackup.TextChanged += new System.EventHandler(this.textBoxEx_TextChanged);
            // 
            // textBoxExPswTelGen
            // 
            this.textBoxExPswTelGen.AllowsLetters = true;
            this.textBoxExPswTelGen.AllowsNumbers = true;
            this.textBoxExPswTelGen.AllowsPunctuation = true;
            this.textBoxExPswTelGen.AllowsSeparators = true;
            this.textBoxExPswTelGen.AllowsSymbols = true;
            this.textBoxExPswTelGen.AllowsWhiteSpaces = true;
            this.textBoxExPswTelGen.ExtraAllowedChars = "";
            this.textBoxExPswTelGen.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExPswTelGen.Location = new System.Drawing.Point(130, 216);
            this.textBoxExPswTelGen.MaxLength = 63;
            this.textBoxExPswTelGen.Name = "textBoxExPswTelGen";
            this.textBoxExPswTelGen.NonAllowedCharacters = "\'";
            this.textBoxExPswTelGen.PasswordChar = '*';
            this.textBoxExPswTelGen.RegularExpresion = "";
            this.textBoxExPswTelGen.Size = new System.Drawing.Size(240, 20);
            this.textBoxExPswTelGen.TabIndex = 9;
            this.textBoxExPswTelGen.TextChanged += new System.EventHandler(this.textBoxEx_TextChanged);
            // 
            // textBoxExServerPortTelGenBackup
            // 
            this.textBoxExServerPortTelGenBackup.AllowsLetters = false;
            this.textBoxExServerPortTelGenBackup.AllowsNumbers = true;
            this.textBoxExServerPortTelGenBackup.AllowsPunctuation = true;
            this.textBoxExServerPortTelGenBackup.AllowsSeparators = false;
            this.textBoxExServerPortTelGenBackup.AllowsSymbols = false;
            this.textBoxExServerPortTelGenBackup.AllowsWhiteSpaces = false;
            this.textBoxExServerPortTelGenBackup.ExtraAllowedChars = "";
            this.textBoxExServerPortTelGenBackup.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExServerPortTelGenBackup.Location = new System.Drawing.Point(289, 25);
            this.textBoxExServerPortTelGenBackup.MaxLength = 15;
            this.textBoxExServerPortTelGenBackup.Name = "textBoxExServerPortTelGenBackup";
            this.textBoxExServerPortTelGenBackup.NonAllowedCharacters = "\'";
            this.textBoxExServerPortTelGenBackup.RegularExpresion = "";
            this.textBoxExServerPortTelGenBackup.Size = new System.Drawing.Size(68, 20);
            this.textBoxExServerPortTelGenBackup.TabIndex = 3;
            this.textBoxExServerPortTelGenBackup.TextChanged += new System.EventHandler(this.textBoxEx_TextChanged);
            // 
            // labelExServerIpTelGenBackup
            // 
            this.labelExServerIpTelGenBackup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExServerIpTelGenBackup.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExServerIpTelGenBackup.Location = new System.Drawing.Point(5, 28);
            this.labelExServerIpTelGenBackup.Name = "labelExServerIpTelGenBackup";
            this.labelExServerIpTelGenBackup.Size = new System.Drawing.Size(64, 16);
            this.labelExServerIpTelGenBackup.TabIndex = 0;
            this.labelExServerIpTelGenBackup.Text = "ServerIp";
            // 
            // labelExUserTelGen
            // 
            this.labelExUserTelGen.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExUserTelGen.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExUserTelGen.Location = new System.Drawing.Point(8, 190);
            this.labelExUserTelGen.Name = "labelExUserTelGen";
            this.labelExUserTelGen.Size = new System.Drawing.Size(88, 16);
            this.labelExUserTelGen.TabIndex = 6;
            this.labelExUserTelGen.Text = "LabelUser";
            // 
            // labelExPswTelGen
            // 
            this.labelExPswTelGen.AutoSize = true;
            this.labelExPswTelGen.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExPswTelGen.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExPswTelGen.Location = new System.Drawing.Point(8, 219);
            this.labelExPswTelGen.Name = "labelExPswTelGen";
            this.labelExPswTelGen.Size = new System.Drawing.Size(53, 13);
            this.labelExPswTelGen.TabIndex = 8;
            this.labelExPswTelGen.Text = "LabelPsw";
            // 
            // labelExServerIpTelGenMain
            // 
            this.labelExServerIpTelGenMain.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExServerIpTelGenMain.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExServerIpTelGenMain.Location = new System.Drawing.Point(5, 28);
            this.labelExServerIpTelGenMain.Name = "labelExServerIpTelGenMain";
            this.labelExServerIpTelGenMain.Size = new System.Drawing.Size(64, 16);
            this.labelExServerIpTelGenMain.TabIndex = 0;
            this.labelExServerIpTelGenMain.Text = "ServerIp";
            // 
            // groupControlBackupServer
            // 
            this.groupControlBackupServer.Controls.Add(this.labelControlSeparatorBackup);
            this.groupControlBackupServer.Controls.Add(this.labelExServerIpTelGenBackup);
            this.groupControlBackupServer.Controls.Add(this.textBoxExServerIpTelGenBackup);
            this.groupControlBackupServer.Controls.Add(this.textBoxExServerPortTelGenBackup);
            this.groupControlBackupServer.Location = new System.Drawing.Point(6, 80);
            this.groupControlBackupServer.Name = "groupControlBackupServer";
            this.groupControlBackupServer.Size = new System.Drawing.Size(365, 49);
            this.groupControlBackupServer.TabIndex = 1;
            this.groupControlBackupServer.Text = "Backup Server";
            // 
            // labelControlSeparatorBackup
            // 
            this.labelControlSeparatorBackup.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControlSeparatorBackup.Location = new System.Drawing.Point(279, 26);
            this.labelControlSeparatorBackup.Name = "labelControlSeparatorBackup";
            this.labelControlSeparatorBackup.Size = new System.Drawing.Size(5, 16);
            this.labelControlSeparatorBackup.TabIndex = 2;
            this.labelControlSeparatorBackup.Text = ":";
            // 
            // labelExServiceNameStatGen
            // 
            this.labelExServiceNameStatGen.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExServiceNameStatGen.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExServiceNameStatGen.Location = new System.Drawing.Point(8, 55);
            this.labelExServiceNameStatGen.Name = "labelExServiceNameStatGen";
            this.labelExServiceNameStatGen.Size = new System.Drawing.Size(88, 16);
            this.labelExServiceNameStatGen.TabIndex = 4;
            this.labelExServiceNameStatGen.Text = "ServiceName";
            // 
            // textBoxExServiceStatGen
            // 
            this.textBoxExServiceStatGen.AllowsLetters = true;
            this.textBoxExServiceStatGen.AllowsNumbers = true;
            this.textBoxExServiceStatGen.AllowsPunctuation = true;
            this.textBoxExServiceStatGen.AllowsSeparators = false;
            this.textBoxExServiceStatGen.AllowsSymbols = false;
            this.textBoxExServiceStatGen.AllowsWhiteSpaces = false;
            this.textBoxExServiceStatGen.ExtraAllowedChars = "";
            this.textBoxExServiceStatGen.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExServiceStatGen.Location = new System.Drawing.Point(130, 53);
            this.textBoxExServiceStatGen.MaxLength = 15;
            this.textBoxExServiceStatGen.Name = "textBoxExServiceStatGen";
            this.textBoxExServiceStatGen.NonAllowedCharacters = "\'";
            this.textBoxExServiceStatGen.RegularExpresion = "";
            this.textBoxExServiceStatGen.Size = new System.Drawing.Size(240, 20);
            this.textBoxExServiceStatGen.TabIndex = 5;
            this.textBoxExServiceStatGen.TextChanged += new System.EventHandler(this.textBoxEx_TextChanged);
            // 
            // textBoxExUserStatGen
            // 
            this.textBoxExUserStatGen.AllowsLetters = true;
            this.textBoxExUserStatGen.AllowsNumbers = true;
            this.textBoxExUserStatGen.AllowsPunctuation = true;
            this.textBoxExUserStatGen.AllowsSeparators = true;
            this.textBoxExUserStatGen.AllowsSymbols = true;
            this.textBoxExUserStatGen.AllowsWhiteSpaces = true;
            this.textBoxExUserStatGen.ExtraAllowedChars = "";
            this.textBoxExUserStatGen.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExUserStatGen.Location = new System.Drawing.Point(130, 82);
            this.textBoxExUserStatGen.MaxLength = 100;
            this.textBoxExUserStatGen.Name = "textBoxExUserStatGen";
            this.textBoxExUserStatGen.NonAllowedCharacters = "\'";
            this.textBoxExUserStatGen.RegularExpresion = "";
            this.textBoxExUserStatGen.Size = new System.Drawing.Size(240, 20);
            this.textBoxExUserStatGen.TabIndex = 7;
            this.textBoxExUserStatGen.TextChanged += new System.EventHandler(this.textBoxEx_TextChanged);
            // 
            // textBoxExPswStatGen
            // 
            this.textBoxExPswStatGen.AllowsLetters = true;
            this.textBoxExPswStatGen.AllowsNumbers = true;
            this.textBoxExPswStatGen.AllowsPunctuation = true;
            this.textBoxExPswStatGen.AllowsSeparators = true;
            this.textBoxExPswStatGen.AllowsSymbols = true;
            this.textBoxExPswStatGen.AllowsWhiteSpaces = true;
            this.textBoxExPswStatGen.ExtraAllowedChars = "";
            this.textBoxExPswStatGen.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExPswStatGen.Location = new System.Drawing.Point(130, 110);
            this.textBoxExPswStatGen.MaxLength = 63;
            this.textBoxExPswStatGen.Name = "textBoxExPswStatGen";
            this.textBoxExPswStatGen.NonAllowedCharacters = "\'";
            this.textBoxExPswStatGen.PasswordChar = '*';
            this.textBoxExPswStatGen.RegularExpresion = "";
            this.textBoxExPswStatGen.Size = new System.Drawing.Size(240, 20);
            this.textBoxExPswStatGen.TabIndex = 9;
            this.textBoxExPswStatGen.TextChanged += new System.EventHandler(this.textBoxEx_TextChanged);
            // 
            // groupControlMainServer
            // 
            this.groupControlMainServer.Controls.Add(this.labelControlSeparatorMain);
            this.groupControlMainServer.Controls.Add(this.labelExServerIpTelGenMain);
            this.groupControlMainServer.Controls.Add(this.textBoxExServerIpTelGenMain);
            this.groupControlMainServer.Controls.Add(this.textBoxExServerPortTelGenMain);
            this.groupControlMainServer.Location = new System.Drawing.Point(5, 26);
            this.groupControlMainServer.Name = "groupControlMainServer";
            this.groupControlMainServer.Size = new System.Drawing.Size(365, 51);
            this.groupControlMainServer.TabIndex = 0;
            this.groupControlMainServer.Text = "Primary Server";
            // 
            // labelControlSeparatorMain
            // 
            this.labelControlSeparatorMain.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControlSeparatorMain.Location = new System.Drawing.Point(279, 26);
            this.labelControlSeparatorMain.Name = "labelControlSeparatorMain";
            this.labelControlSeparatorMain.Size = new System.Drawing.Size(5, 16);
            this.labelControlSeparatorMain.TabIndex = 2;
            this.labelControlSeparatorMain.Text = ":";
            // 
            // textBoxExServerIpTelGenMain
            // 
            this.textBoxExServerIpTelGenMain.AllowsLetters = true;
            this.textBoxExServerIpTelGenMain.AllowsNumbers = true;
            this.textBoxExServerIpTelGenMain.AllowsPunctuation = true;
            this.textBoxExServerIpTelGenMain.AllowsSeparators = false;
            this.textBoxExServerIpTelGenMain.AllowsSymbols = false;
            this.textBoxExServerIpTelGenMain.AllowsWhiteSpaces = false;
            this.textBoxExServerIpTelGenMain.ExtraAllowedChars = "";
            this.textBoxExServerIpTelGenMain.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExServerIpTelGenMain.Location = new System.Drawing.Point(125, 25);
            this.textBoxExServerIpTelGenMain.MaxLength = 15;
            this.textBoxExServerIpTelGenMain.Name = "textBoxExServerIpTelGenMain";
            this.textBoxExServerIpTelGenMain.NonAllowedCharacters = "\'";
            this.textBoxExServerIpTelGenMain.RegularExpresion = "";
            this.textBoxExServerIpTelGenMain.Size = new System.Drawing.Size(148, 20);
            this.textBoxExServerIpTelGenMain.TabIndex = 1;
            this.textBoxExServerIpTelGenMain.TextChanged += new System.EventHandler(this.textBoxEx_TextChanged);
            // 
            // textBoxExServerPortTelGenMain
            // 
            this.textBoxExServerPortTelGenMain.AllowsLetters = false;
            this.textBoxExServerPortTelGenMain.AllowsNumbers = true;
            this.textBoxExServerPortTelGenMain.AllowsPunctuation = true;
            this.textBoxExServerPortTelGenMain.AllowsSeparators = false;
            this.textBoxExServerPortTelGenMain.AllowsSymbols = false;
            this.textBoxExServerPortTelGenMain.AllowsWhiteSpaces = false;
            this.textBoxExServerPortTelGenMain.ExtraAllowedChars = "";
            this.textBoxExServerPortTelGenMain.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExServerPortTelGenMain.Location = new System.Drawing.Point(289, 25);
            this.textBoxExServerPortTelGenMain.MaxLength = 15;
            this.textBoxExServerPortTelGenMain.Name = "textBoxExServerPortTelGenMain";
            this.textBoxExServerPortTelGenMain.NonAllowedCharacters = "\'";
            this.textBoxExServerPortTelGenMain.RegularExpresion = "";
            this.textBoxExServerPortTelGenMain.Size = new System.Drawing.Size(68, 20);
            this.textBoxExServerPortTelGenMain.TabIndex = 3;
            this.textBoxExServerPortTelGenMain.TextChanged += new System.EventHandler(this.textBoxEx_TextChanged);
            // 
            // labelExServerIpStatGen
            // 
            this.labelExServerIpStatGen.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExServerIpStatGen.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExServerIpStatGen.Location = new System.Drawing.Point(8, 28);
            this.labelExServerIpStatGen.Name = "labelExServerIpStatGen";
            this.labelExServerIpStatGen.Size = new System.Drawing.Size(64, 16);
            this.labelExServerIpStatGen.TabIndex = 0;
            this.labelExServerIpStatGen.Text = "ServerIp";
            // 
            // labelExUserStatGen
            // 
            this.labelExUserStatGen.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExUserStatGen.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExUserStatGen.Location = new System.Drawing.Point(8, 85);
            this.labelExUserStatGen.Name = "labelExUserStatGen";
            this.labelExUserStatGen.Size = new System.Drawing.Size(88, 16);
            this.labelExUserStatGen.TabIndex = 6;
            this.labelExUserStatGen.Text = "LabelUser";
            // 
            // textBoxExServerIpStatGen
            // 
            this.textBoxExServerIpStatGen.AllowsLetters = true;
            this.textBoxExServerIpStatGen.AllowsNumbers = true;
            this.textBoxExServerIpStatGen.AllowsPunctuation = true;
            this.textBoxExServerIpStatGen.AllowsSeparators = false;
            this.textBoxExServerIpStatGen.AllowsSymbols = false;
            this.textBoxExServerIpStatGen.AllowsWhiteSpaces = false;
            this.textBoxExServerIpStatGen.ExtraAllowedChars = "";
            this.textBoxExServerIpStatGen.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExServerIpStatGen.Location = new System.Drawing.Point(130, 24);
            this.textBoxExServerIpStatGen.MaxLength = 15;
            this.textBoxExServerIpStatGen.Name = "textBoxExServerIpStatGen";
            this.textBoxExServerIpStatGen.NonAllowedCharacters = "\'";
            this.textBoxExServerIpStatGen.RegularExpresion = "";
            this.textBoxExServerIpStatGen.Size = new System.Drawing.Size(148, 20);
            this.textBoxExServerIpStatGen.TabIndex = 1;
            this.textBoxExServerIpStatGen.TextChanged += new System.EventHandler(this.textBoxEx_TextChanged);
            // 
            // labelExPswStatGen
            // 
            this.labelExPswStatGen.AutoSize = true;
            this.labelExPswStatGen.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExPswStatGen.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExPswStatGen.Location = new System.Drawing.Point(8, 113);
            this.labelExPswStatGen.Name = "labelExPswStatGen";
            this.labelExPswStatGen.Size = new System.Drawing.Size(53, 13);
            this.labelExPswStatGen.TabIndex = 8;
            this.labelExPswStatGen.Text = "LabelPsw";
            // 
            // groupControlTelephonyGenesys
            // 
            this.groupControlTelephonyGenesys.Controls.Add(this.groupControlBackupServer);
            this.groupControlTelephonyGenesys.Controls.Add(this.groupControlMainServer);
            this.groupControlTelephonyGenesys.Controls.Add(this.labelExQueueTelGen);
            this.groupControlTelephonyGenesys.Controls.Add(this.labelExServiceTelGenMain);
            this.groupControlTelephonyGenesys.Controls.Add(this.textBoxExServiceTelGenMain);
            this.groupControlTelephonyGenesys.Controls.Add(this.textBoxExQueueTelGen);
            this.groupControlTelephonyGenesys.Controls.Add(this.textBoxExUserTelGen);
            this.groupControlTelephonyGenesys.Controls.Add(this.textBoxExPswTelGen);
            this.groupControlTelephonyGenesys.Controls.Add(this.labelExUserTelGen);
            this.groupControlTelephonyGenesys.Controls.Add(this.labelExPswTelGen);
            this.groupControlTelephonyGenesys.Location = new System.Drawing.Point(3, 149);
            this.groupControlTelephonyGenesys.Name = "groupControlTelephonyGenesys";
            this.groupControlTelephonyGenesys.Size = new System.Drawing.Size(375, 245);
            this.groupControlTelephonyGenesys.TabIndex = 7;
            this.groupControlTelephonyGenesys.Text = "groupControlTelephonyGenesys";
            // 
            // textBoxExServiceTelGenMain
            // 
            this.textBoxExServiceTelGenMain.AllowsLetters = true;
            this.textBoxExServiceTelGenMain.AllowsNumbers = true;
            this.textBoxExServiceTelGenMain.AllowsPunctuation = true;
            this.textBoxExServiceTelGenMain.AllowsSeparators = false;
            this.textBoxExServiceTelGenMain.AllowsSymbols = false;
            this.textBoxExServiceTelGenMain.AllowsWhiteSpaces = false;
            this.textBoxExServiceTelGenMain.ExtraAllowedChars = "";
            this.textBoxExServiceTelGenMain.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExServiceTelGenMain.Location = new System.Drawing.Point(130, 134);
            this.textBoxExServiceTelGenMain.MaxLength = 15;
            this.textBoxExServiceTelGenMain.Name = "textBoxExServiceTelGenMain";
            this.textBoxExServiceTelGenMain.NonAllowedCharacters = "\'";
            this.textBoxExServiceTelGenMain.RegularExpresion = "";
            this.textBoxExServiceTelGenMain.Size = new System.Drawing.Size(240, 20);
            this.textBoxExServiceTelGenMain.TabIndex = 3;
            this.textBoxExServiceTelGenMain.TextChanged += new System.EventHandler(this.textBoxEx_TextChanged);
            // 
            // groupControlStatisticsGenesys
            // 
            this.groupControlStatisticsGenesys.Controls.Add(this.labelControlSeparator);
            this.groupControlStatisticsGenesys.Controls.Add(this.labelExServiceNameStatGen);
            this.groupControlStatisticsGenesys.Controls.Add(this.textBoxExServiceStatGen);
            this.groupControlStatisticsGenesys.Controls.Add(this.textBoxExServerPortStatGen);
            this.groupControlStatisticsGenesys.Controls.Add(this.textBoxExUserStatGen);
            this.groupControlStatisticsGenesys.Controls.Add(this.textBoxExPswStatGen);
            this.groupControlStatisticsGenesys.Controls.Add(this.labelExServerIpStatGen);
            this.groupControlStatisticsGenesys.Controls.Add(this.labelExUserStatGen);
            this.groupControlStatisticsGenesys.Controls.Add(this.textBoxExServerIpStatGen);
            this.groupControlStatisticsGenesys.Controls.Add(this.labelExPswStatGen);
            this.groupControlStatisticsGenesys.Location = new System.Drawing.Point(3, 3);
            this.groupControlStatisticsGenesys.Name = "groupControlStatisticsGenesys";
            this.groupControlStatisticsGenesys.Size = new System.Drawing.Size(375, 138);
            this.groupControlStatisticsGenesys.TabIndex = 6;
            this.groupControlStatisticsGenesys.Text = "groupControlStatisticGenesys";
            // 
            // labelControlSeparator
            // 
            this.labelControlSeparator.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControlSeparator.Location = new System.Drawing.Point(283, 25);
            this.labelControlSeparator.Name = "labelControlSeparator";
            this.labelControlSeparator.Size = new System.Drawing.Size(5, 16);
            this.labelControlSeparator.TabIndex = 2;
            this.labelControlSeparator.Text = ":";
            // 
            // GenesysConfigurationControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupControlTelephonyGenesys);
            this.Controls.Add(this.groupControlStatisticsGenesys);
            this.Name = "GenesysConfigurationControl";
            this.Size = new System.Drawing.Size(381, 398);
            ((System.ComponentModel.ISupportInitialize)(this.groupControlBackupServer)).EndInit();
            this.groupControlBackupServer.ResumeLayout(false);
            this.groupControlBackupServer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlMainServer)).EndInit();
            this.groupControlMainServer.ResumeLayout(false);
            this.groupControlMainServer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlTelephonyGenesys)).EndInit();
            this.groupControlTelephonyGenesys.ResumeLayout(false);
            this.groupControlTelephonyGenesys.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlStatisticsGenesys)).EndInit();
            this.groupControlStatisticsGenesys.ResumeLayout(false);
            this.groupControlStatisticsGenesys.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal TextBoxEx textBoxExServerPortStatGen;
        private LabelEx labelExQueueTelGen;
        private LabelEx labelExServiceTelGenMain;
        internal TextBoxEx textBoxExQueueTelGen;
        internal TextBoxEx textBoxExUserTelGen;
        internal TextBoxEx textBoxExServerIpTelGenBackup;
        internal TextBoxEx textBoxExPswTelGen;
        internal TextBoxEx textBoxExServerPortTelGenBackup;
        private LabelEx labelExServerIpTelGenBackup;
        private LabelEx labelExUserTelGen;
        private LabelEx labelExPswTelGen;
        private LabelEx labelExServerIpTelGenMain;
        private DevExpress.XtraEditors.GroupControl groupControlBackupServer;
        private DevExpress.XtraEditors.LabelControl labelControlSeparatorBackup;
        private LabelEx labelExServiceNameStatGen;
        internal TextBoxEx textBoxExServiceStatGen;
        internal TextBoxEx textBoxExUserStatGen;
        internal TextBoxEx textBoxExPswStatGen;
        private DevExpress.XtraEditors.GroupControl groupControlMainServer;
        private DevExpress.XtraEditors.LabelControl labelControlSeparatorMain;
        internal TextBoxEx textBoxExServerIpTelGenMain;
        internal TextBoxEx textBoxExServerPortTelGenMain;
        private LabelEx labelExServerIpStatGen;
        private LabelEx labelExUserStatGen;
        internal TextBoxEx textBoxExServerIpStatGen;
        private LabelEx labelExPswStatGen;
        private DevExpress.XtraEditors.GroupControl groupControlTelephonyGenesys;
        internal TextBoxEx textBoxExServiceTelGenMain;
        private DevExpress.XtraEditors.GroupControl groupControlStatisticsGenesys;
        private DevExpress.XtraEditors.LabelControl labelControlSeparator;

    }
}
