﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SmartCadCore.Common;
using SmartCadCore.Core;

namespace Smartmatic.SmartCad.Telephony.Genesys
{
    public partial class GenesysConfigurationControl : UserControl, IConfigurationControl
    {
        public GenesysConfigurationControl()
        {
            InitializeComponent();
            LoadLanguage();
        }

        private void LoadLanguage()
        {
            Text = ResourceLoader.GetString2(this.Name);
            this.groupControlMainServer.Text = ResourceLoader.GetString2("MainServerTitle");
            this.groupControlBackupServer.Text = ResourceLoader.GetString2("BackupServerTitle");
            this.labelExServerIpTelGenBackup.Text = ResourceLoader.GetString2("SipServerIpOrName") + " :";

            this.groupControlStatisticsGenesys.Text = ResourceLoader.GetString2("StatisticConfiguration");

            this.labelExServerIpStatGen.Text = ResourceLoader.GetString2("Server") + " :*";
            this.labelExServiceNameStatGen.Text = ResourceLoader.GetString2("ServiceName") + " :*";
            this.labelExUserStatGen.Text = ResourceLoader.GetString2("UserName") + " :*";
            this.labelExPswStatGen.Text = ResourceLoader.GetString2("Password") + " :";

            this.labelExServerIpTelGenMain.Text = ResourceLoader.GetString2("SipServerIpOrName") + " :*";

            this.labelExServiceTelGenMain.Text = ResourceLoader.GetString2("ServiceName") + " :*";
            this.labelExQueueTelGen.Text = ResourceLoader.GetString2("QueueId") + " :*";
            this.labelExUserTelGen.Text = ResourceLoader.GetString2("UserName") + " :*";
            this.labelExPswTelGen.Text = ResourceLoader.GetString2("Password") + " :";

            this.groupControlTelephonyGenesys.Text = ResourceLoader.GetString2("TelephonyConfiguration");
        }

        #region IConfigurationControl Members

        public event Parameters_ChangeEvent FulFilled_Change;

        public void SetProperties(Dictionary<string, string> properties)
        {
            foreach (string propName in properties.Keys)
            {
                switch (propName)
                {
                    case GenesysProperties.MAIN_SERVER_IP:
                        textBoxExServerIpTelGenMain.Text = properties[propName];
                        break;
                    case GenesysProperties.MAIN_SERVER_PORT:
                        textBoxExServerPortTelGenMain.Text = properties[propName];
                        break;
                    case GenesysProperties.BACKUP_SERVER_IP:
                        textBoxExServerIpTelGenBackup.Text = properties[propName];
                        break;
                    case GenesysProperties.BACKUP_SERVER_PORT:
                        textBoxExServerPortTelGenBackup.Text = properties[propName];
                        break;
                    case CommonProperties.Queue:
                        textBoxExQueueTelGen.Text = properties[propName];
                        break;
                    case GenesysProperties.SERVICE_NAME:
                        textBoxExServiceTelGenMain.Text = properties[propName];
                        break;
                    case GenesysProperties.SERVER_LOGIN:
                        textBoxExUserTelGen.Text = properties[propName];
                        break;
                    case GenesysProperties.SERVER_PASSWORD:
                        if (string.IsNullOrEmpty(properties[propName].Trim()))
                            textBoxExPswTelGen.Text = CryptoUtil.DecryptWithRijndael(properties[propName]).TrimEnd('\0');
                        break;
                    default:
                        break;
                }
            }
        }

        public Dictionary<string, string> GetProperties()
        {
            Dictionary<string, string> props = new Dictionary<string, string>();

            props.Add(GenesysProperties.MAIN_SERVER_IP, textBoxExServerIpTelGenMain.Text);
            props.Add(GenesysProperties.MAIN_SERVER_PORT, textBoxExServerPortTelGenMain.Text);
            props.Add(GenesysProperties.BACKUP_SERVER_IP, textBoxExServerIpTelGenBackup.Text);
            props.Add(GenesysProperties.BACKUP_SERVER_PORT, textBoxExServerPortTelGenBackup.Text);
            props.Add(CommonProperties.Queue, textBoxExQueueTelGen.Text);
            props.Add(GenesysProperties.SERVICE_NAME, textBoxExServiceTelGenMain.Text);
            props.Add(GenesysProperties.SERVER_LOGIN, textBoxExUserTelGen.Text);

            string encryptedPass = "";
            if (string.IsNullOrEmpty(textBoxExPswTelGen.Text.Trim()) == false)
                encryptedPass = CryptoUtil.EncryptWithRijndael(textBoxExPswTelGen.Text);
            props.Add(GenesysProperties.SERVER_PASSWORD, encryptedPass);

            return props;
        }

        public void SetStatisticProperties(string[] statisticProperties)
        {
            textBoxExServerIpStatGen.Text = statisticProperties[0];
            textBoxExUserStatGen.Text = statisticProperties[1];
            textBoxExPswStatGen.Text = statisticProperties[2];
            //Ignore [3]
            textBoxExServerPortStatGen.Text = statisticProperties[4];
            textBoxExServiceStatGen.Text = statisticProperties[5];
        }

        public string[] GetStatisticProperties()
        {
            string[] props = new string[8];

            props[0] = textBoxExServerIpStatGen.Text;
            props[1] = textBoxExUserStatGen.Text;
            props[2] = textBoxExPswStatGen.Text;
            props[3] =""; //IGNORE [3]
            props[4] = textBoxExServerPortStatGen.Text;
            props[5] = textBoxExServiceStatGen.Text;

            return props;
        }
        #endregion

        bool isFullFilled = false;
        private void textBoxEx_TextChanged(object sender, EventArgs e)
        {
            if (((string.IsNullOrEmpty(textBoxExServerIpStatGen.Text) == true &&
                    string.IsNullOrEmpty(textBoxExServerPortStatGen.Text) == true &&
                    string.IsNullOrEmpty(textBoxExUserStatGen.Text) == true &&
                //string.IsNullOrEmpty(textBoxExPswStatGen.Text) == true &&
                    string.IsNullOrEmpty(textBoxExServiceStatGen.Text) == true) ||
                //Validating all not null, excepting password that can be empty
                    (string.IsNullOrEmpty(textBoxExServerIpStatGen.Text) == false &&
                    string.IsNullOrEmpty(textBoxExServerPortStatGen.Text) == false &&
                    string.IsNullOrEmpty(textBoxExUserStatGen.Text) == false &&
                    string.IsNullOrEmpty(textBoxExServiceStatGen.Text) == false)) &&
                //Now, validating telephony parameters, all of the must be set, password can be empty
                    (string.IsNullOrEmpty(textBoxExServerIpTelGenMain.Text) == false &&
                     string.IsNullOrEmpty(textBoxExServerPortTelGenMain.Text) == false &&
                     string.IsNullOrEmpty(textBoxExServiceTelGenMain.Text) == false &&
                     string.IsNullOrEmpty(textBoxExQueueTelGen.Text) == false &&
                     string.IsNullOrEmpty(textBoxExUserTelGen.Text) == false))
                //string.IsNullOrEmpty(textBoxExPswTelGen.Text) == false)))
            {
                if (!isFullFilled && FulFilled_Change != null)
                {
                    FulFilled_Change(true);
                }
                isFullFilled = true;
            }
            else
            {
                if (isFullFilled && FulFilled_Change != null)
                {
                    FulFilled_Change(false);
                }
                isFullFilled = false;
            }
        }
    }
}
