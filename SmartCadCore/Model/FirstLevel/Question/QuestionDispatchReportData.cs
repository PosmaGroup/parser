﻿using System;
using System.Linq;
using System.Text;
using NHibernate.Mapping.Attributes;

namespace SmartCadCore.Model
{
    #region Class QuestionData Documentation
    /// <summary>
    /// This class represents a possible question of a dispatch report.
    /// </summary>
    /// <className>QuestionData</className>
    /// <author email="fsilva@smartmatic.com">Fernando Silva</author>
    /// <date>2007/01/30</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    [Serializable]
    [Class(NameType = typeof(QuestionDispatchReportData), Table = "QUESTION_DISPATCH_REPORT", Lazy = false, BatchSize = 100, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_QUESTION_DISPATCH_REPORT_TEXT")]
    public class QuestionDispatchReportData : ObjectData
    {
        /// <summary>
        /// A posible question of the phone report
        /// </summary>
        [Property(0, Unique = true)]
        [Column(1, Name = "TEXT", NotNull = true, UniqueKey = "UK_QUESTION_DISPATCH_REPORT_TEXT")]
        public virtual string Text { get; set; }
        
        /// <summary>
        /// XML that contains the way the question would be painted.
        /// </summary>
        [Property(0)]
        [Column(1, Name = "RENDER", Length = 65535)]
        public virtual string Render { get; set; }

        [Property(Column = "REQUIRED", NotNull = true)]
        public virtual bool Required { get; set; }



        [InitialData(PropertyName = "Name", PropertyValue = "QuestionDispatchReport")]
        public static readonly UserResourceData Resource;   
    }
}
