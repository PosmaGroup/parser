﻿using NHibernate.Mapping.Attributes;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(PossibleAnswerAnswerData), Table = "POSSIBLE_ANSWER_ANSWER", Lazy = false, Where = "DELETED_ID IS NULL")]
    public class PossibleAnswerAnswerData : ObjectData
    {
        private string textAnswer;
        private QuestionPossibleAnswerData possibleAnswer;
        private PhoneReportAnswerData phoneReportAnswer;
        private CctvReportAnswerData cctvReportAnswer;
        private AlarmReportAnswerData alarmReportAnswer;
        private AlarmLprReportAnswerData alarmLprReportAnswer;
        private TwitterReportAnswerData twitterReportAnswer;



        [Property(0)]
        [Column(1, Name = "TEXT_ANSWER", Length = 16384)]
        public virtual string TextAnswer
        {
            get
            {
                return this.textAnswer;
            }
            set
            {
                this.textAnswer = value;
            }
        }

        [ManyToOne(ClassType = typeof(QuestionPossibleAnswerData), Column = "INCIDENT_TYPE_POSSIBLE_ANSWER_CODE",
                  ForeignKey = "FK_POSSIBLE_ANSWER_ANSWER_INCIDENT_TYPE_POSSIBLE_ANSWER", Cascade = "none")]
        public virtual QuestionPossibleAnswerData PossibleAnswer
        {
            get
            {
                return possibleAnswer;
            }
            set
            {
                possibleAnswer = value;
            }
        }

        [ManyToOne(0, ClassType = typeof(PhoneReportAnswerData), ForeignKey = "FK_POSSIBLE_ANSWER_ANSWER_PHONE_REPORT_ANSWER", Cascade = "none")]
        [Column(1, Name = "PHONE_REPORT_ANSWER_CODE", NotNull = false, Index = "IX_POSSIBLE_ANSWER_ANSWER_PHONE_REPORT_ANSWER")]
        public virtual PhoneReportAnswerData PhoneReportAnswer
        {
            get
            {
                return phoneReportAnswer;
            }
            set
            {
                phoneReportAnswer = value;
            }
        }


        [ManyToOne(0, ClassType = typeof(CctvReportAnswerData), ForeignKey = "FK_POSSIBLE_ANSWER_ANSWER_CCTV_REPORT_ANSWER", Cascade = "none")]
        [Column(1, Name = "CCTV_REPORT_ANSWER_CODE", NotNull = false, Index = "IX_POSSIBLE_ANSWER_ANSWER_CCTV_REPORT_ANSWER")]
        public virtual CctvReportAnswerData CctvReportAnswer
        {
            get
            {
                return cctvReportAnswer;
            }
            set
            {
                cctvReportAnswer = value;
            }
        }

        [ManyToOne(0, ClassType = typeof(AlarmReportAnswerData), ForeignKey = "FK_POSSIBLE_ANSWER_ANSWER_ALARM_REPORT_ANSWER", Cascade = "none")]
        [Column(1, Name = "ALARM_REPORT_ANSWER_CODE", NotNull = false, Index = "IX_POSSIBLE_ANSWER_ANSWER_ALARM_REPORT_ANSWER")]
        public virtual AlarmReportAnswerData AlarmReportAnswer
        {
            get
            {
                return alarmReportAnswer;
            }
            set
            {
                alarmReportAnswer = value;
            }
        }

        [ManyToOne(0, ClassType = typeof(AlarmLprReportAnswerData), ForeignKey = "FK_POSSIBLE_ANSWER_ANSWER_ALARM_LPR_REPORT_ANSWER", Cascade = "none")]
        [Column(1, Name = "ALARM_LPR_REPORT_ANSWER_CODE", NotNull = false, Index = "IX_POSSIBLE_ANSWER_ANSWER_ALARM_LPR_REPORT_ANSWER")]
        public virtual AlarmLprReportAnswerData AlarmLprReportAnswer
        {
            get
            {
                return alarmLprReportAnswer;
            }
            set
            {
                alarmLprReportAnswer = value;
            }
        }

        [ManyToOne(0, ClassType = typeof(TwitterReportAnswerData), ForeignKey = "FK_POSSIBLE_ANSWER_ANSWER_TWITTER_REPORT_ANSWER", Cascade = "none")]
        [Column(1, Name = "TWITTER_REPORT_ANSWER_CODE", NotNull = false, Index = "IX_POSSIBLE_ANSWER_ANSWER_TWITTER_REPORT_ANSWER")]
        public virtual TwitterReportAnswerData TwitterReportAnswer
        {
            get
            {
                return twitterReportAnswer;
            }
            set
            {
                twitterReportAnswer = value;
            }
        }


    }
}
