﻿using NHibernate.Mapping.Attributes;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(QuestionPossibleAnswerData), Table = "QUESTION_POSSIBLE_ANSWER", Lazy = false, BatchSize = 100, Where = "DELETED_ID IS NULL")]
    public class QuestionPossibleAnswerData : ObjectData
    {
        #region Fields

        private QuestionData question;
        private string description;
        private string procedure;
        private string controlName;

        #endregion

        #region Constructors
        public QuestionPossibleAnswerData()
        {
            //customCode = Guid.NewGuid().ToString();
        }
        #endregion

        [ManyToOne(ClassType = typeof(QuestionData), Column = "INCIDENT_TYPE_QUESTION_CODE",
          ForeignKey = "FK_INCIDENT_TYPE_POSSIBLE_ANSWER_QUESTION", Cascade = "none")]
        public virtual QuestionData Question
        {
            get
            {
                return question;
            }
            set
            {
                question = value;
            }
        }

        [Property(0)]
        [Column(1, Name = "DESCRIPTION", NotNull = true)]
        public virtual string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }

        [Property(0)]
        [Column(1, Name = "ANSWER_PROCEDURE", Length = 16384)]
        public virtual string Procedure
        {
            get
            {
                return procedure;
            }
            set
            {
                procedure = value;
            }
        }

        [Property(0)]
        [Column(1, Name = "CONTROL_NAME")]
        public virtual string ControlName
        {
            get
            {
                return controlName;
            }
            set
            {
                controlName = value;
            }
        }
    }
}
