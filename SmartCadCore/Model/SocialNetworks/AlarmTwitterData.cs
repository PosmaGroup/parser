﻿using Iesi.Collections;
using NHibernate.Mapping.Attributes;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(AlarmTwitterData), Table = "ALARM_TWITTER", Lazy = false, BatchSize = 1000, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_ALARM_TWITTER")]
    public class AlarmTwitterData : ObjectData
    {
        public enum AlarmStatus
        {
            NotTaken = 0,
            InProcess,
            Ended
        }

        private ISet tweets;

        [ManyToOne(0, ClassType = typeof(AlarmQueryTwitterData), ForeignKey = "FK_ALARM_QUERY_ALARM_CODE", Cascade = "none")]
        [Column(1, Name = "ALARM_QUERY_TWITTER_CODE", UniqueKey = "UK_ALARM_TWITTER", NotNull = true)]
        public AlarmQueryTwitterData AlarmQuery
        {
            get;
            set;
        }

        [Set(0, Name = "Tweets", Table = "TWEET", Cascade = "all-delete-orphan",
          Lazy = CollectionLazy.False, Fetch = CollectionFetchMode.Subselect)]
        [Key(1, Column = "ALARM_TWITTER_CODE")]
        [OneToMany(2, ClassType = typeof(TweetData))]
        public ISet Tweets
        {
            get
            {
                return tweets;
            }
            set
            {
                tweets = value;
            }
        }


        [Property(0, Unique = true, TypeType = typeof(DateTime))]
        [Column(1, Name = "START_DATE", UniqueKey = "UK_ALARM_TWITTER", NotNull = true)]
        public virtual DateTime StartDate
        {
            get;
            set;
        }


        [Property(0, TypeType = typeof(DateTime))]
        [Column(1, Name = "END_DATE")]
        public virtual DateTime? EndDate
        {
            get;
            set;
        }

        [Property(0, TypeType = typeof(AlarmStatus))]
        [Column(1, Name = "ALARM_STATUS", NotNull = true)]
        public virtual AlarmStatus Status
        {
            get;
            set;
        }

        [InitialData(PropertyName = "Name", PropertyValue = "AlarmTwitterData")]
        public static readonly UserResourceData Resource;


        public override string ToString()
        {
            return AlarmQuery.ToString();
        }

        public override bool Equals(object obj)
        {
            try
            {
                if (obj.GetType() == typeof(AlarmTwitterData))
                {
                    AlarmTwitterData alarm = (AlarmTwitterData)obj;
                    if (this.Code == alarm.Code)
                    {
                        return true;
                    }
                }
            }
            catch { }
            return false;
        }
    }
}
