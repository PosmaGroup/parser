﻿using NHibernate.Mapping.Attributes;
using System;

using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(SensorData), Table = "SENSOR", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_SENSOR_CUSTOM_CODE")]
    [AttributeIdentifier("DELETED_ID_2", Value = "UK_SENSOR_NAME")]
    public class SensorData : ObjectData, INamedObjectData
    {
        public enum SensorType
        {
            Input = 0,
            Output = 1
        }

        public SensorData()
        {
        }

        [Property(0, Unique = true)]
        [Column(1, Name = "CUSTOM_CODE", UniqueKey = "UK_SENSOR_CUSTOM_CODE", NotNull = true)]
        public string CustomCode
        {
            get;
            set;
        }


        [Property(0, Unique = true)]
        [Column(1, Name = "NAME", UniqueKey = "UK_SENSOR_NAME", NotNull = true)]
        public string Name
        {
            get;
            set;
        }


        [Property(Column = "TYPE")]
        public SensorType Type
        {
            get;
            set;
        }

        [InitialData(PropertyName = "Name", PropertyValue = "SensorData")]
        public static readonly UserResourceData Resource;

        #region Overrides

        public override string ToString()
        {
            return this.Name;
        }

        #endregion

    }
}
