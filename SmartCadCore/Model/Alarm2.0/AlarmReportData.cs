using System;
using System.Collections;
using System.Text;

using NHibernate.Mapping.Attributes;
using Iesi.Collections;

namespace SmartCadCore.Model
{
    #region Class AlarmReportData Documentation
    /// <summary>
    /// This class represents the cctv report.
    /// </summary>
    /// <className>AlarmReportData</className>
    /// <author email="fsilva@smartmatic.com">Fernando Silva</author>
    /// <date>2007/01/30</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    [Serializable]
    [JoinedSubclass(Table = "ALARM_REPORT", NameType = typeof(AlarmReportData), ExtendsType = typeof(ReportBaseData), Lazy = false)]
    public class AlarmReportData : ReportBaseData
    {
        #region Fields

        private int parentCode;       
        private ISet setAnswers;
        #endregion

        #region Constructors
        
        public AlarmReportData()
        {
        }

        public AlarmReportData(
            AlarmSensorTelemetryData alarmSensorTelemetry,           
            ISet answers)
        {
            this.AlarmSensorTelemetry = alarmSensorTelemetry;            
            this.setAnswers = answers;
        }

        #endregion

        #region Properties

        [Key(0, Column = "PARENT_CODE", ForeignKey = "FK_ALARM_REPORT_REPORT_BASE_CODE")]
        public virtual int ParentCode
        {
            get
            {
                return parentCode;
            }
            set
            {
                parentCode = value;
            }
        }

        [ManyToOne(ClassType = typeof(AlarmSensorTelemetryData), Column = "ALARM_SENSOR_TELEMETRY_CODE",
          ForeignKey = "FK_SENSOR_TELEMETRY_ALARM_REPORT_CODE", Cascade = "save-update")]
        public virtual AlarmSensorTelemetryData AlarmSensorTelemetry
        {
            get;
            set;
        }
       
        [Set(0, Name = "SetAnswers", Table = "ALARM_REPORT_ANSWER", Cascade = "all-delete-orphan",
            Lazy = CollectionLazy.True/*, Inverse = true*/, Fetch = CollectionFetchMode.Subselect)]
        [Key(1, Column = "ALARM_REPORT_CODE"/*, ForeignKey = "FK_ALARM_REPORT_ALARM_REPORT_ANSWER_CODE"*/)]
        [OneToMany(2, ClassType = typeof(AlarmReportAnswerData))]
        public ISet SetAnswers
        {
            get
            {
                return setAnswers;
            }
            set
            {
                setAnswers = value;
            }
        }

        #endregion

        #region doc
        //private bool? incomplete;
        //private PhoneReportCallerData caller;
        //private DateTime? registeredCallTime;
        //private DateTime? receivedCallTime;
        //private DateTime? pickedUpCallTime;
        //[Property(Column = "REGISTERED_CALL_TIME", TypeType = typeof(DateTime))]
        //public virtual DateTime? RegisteredCallTime
        //{
        //    get
        //    {
        //        return registeredCallTime;
        //    }
        //    set
        //    {
        //        registeredCallTime = value;
        //    }
        //}

        //[Property(Column = "RECEIVED_CALL_TIME", TypeType = typeof(DateTime))]
        //public virtual DateTime? ReceivedCallTime
        //{
        //    get
        //    {
        //        return receivedCallTime;
        //    }
        //    set
        //    {
        //        receivedCallTime = value;
        //    }
        //}

        //[Property(Column = "HANGED_UP_CALL_TIME", TypeType = typeof(DateTime))]
        //public virtual DateTime? HangedUpCallTime
        //{
        //    get
        //    {
        //        return hangedUpCallTime;
        //    }
        //    set
        //    {
        //        hangedUpCallTime = value;
        //    }
        //}

        //[Property(Column = "INCOMPLETE", TypeType = typeof(bool))]
        //public virtual bool? Incomplete
        //{
        //    get
        //    {
        //        return incomplete;
        //    }
        //    set
        //    {
        //        incomplete = value;
        //    }
        //}
        //[ComponentProperty(ComponentType = typeof(PhoneReportCallerData))]
        //public virtual PhoneReportCallerData Caller
        //{
        //    get
        //    {
        //        return caller;
        //    }
        //    set
        //    {
        //        caller = value;
        //    }
        //}
        #endregion doc
      
    }

   
}
