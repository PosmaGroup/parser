using System;
using System.Text;

using NHibernate.Mapping.Attributes;

namespace SmartCadCore.Model
{
    #region Class AlarmReportSensorTelemetryData Documentation
    /// <summary>
    /// This class represents the cctv camera of a cctv report
    /// </summary>
    /// <className>AlarmReportSensorTelemetryData</className>
    /// <author email="fsilva@smartmatic.com">Fernando Silva</author>
    /// <date>2007/01/30</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    [Serializable]
    //[Class(Table = "PHONE_REPORT_LINE", Lazy = false, Where = "DELETED_ID IS NULL")]
    [Component(/*Insert = true, Update = true*/)]
    public class AlarmReportSensorTelemetryData
    {
        #region Fields

        private string name;
        private StructAddressData address;

        #endregion

        #region Constructors

        public AlarmReportSensorTelemetryData()
        {
        }

        public AlarmReportSensorTelemetryData(string name, StructAddressData address)
        {
            this.name = name;            
            this.address = address;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Owner's name of the alarm line calling
        /// </summary>
        [Property(0)]
        [Column(1, Name = "SENSOR_TELEMETRY_NAME"/*, NotNull = true*/)]
        public virtual string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

      

        /// <summary>
        /// Address of the alarm line calling
        /// </summary>
        [ComponentProperty(ComponentType = typeof(StructAddressData))]
        public virtual StructAddressData Address
        {
            get
            {
                return address;
            }
            set
            {
                address = value;
            }
        }

        #endregion

        #region doc
        //private string telephone;
        ///// <summary>
        ///// Telephone number of the phone line calling
        ///// </summary>
        //[Property(0)]
        //[Column(1, Name = "LINE_TELEPHONE")]
        //public virtual string Telephone
        //{
        //    get
        //    {
        //        return telephone;
        //    }
        //    set
        //    {
        //        telephone = value;
        //    }
        //}
        #endregion doc
    }
}
