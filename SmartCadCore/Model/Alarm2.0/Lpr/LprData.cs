using System;
using System.Text;
using NHibernate.Mapping.Attributes;
using Iesi.Collections;
using System.Collections;

namespace SmartCadCore.Model
{
    [Serializable]
    //[Class(Table = "LPR", Lazy = false, Where = "DELETED_ID IS NULL")]
    [JoinedSubclass(Table = "LPR", NameType = typeof(LprData), ExtendsType = typeof(VideoDeviceData), Lazy = false)]
    //[AttributeIdentifier("DELETED_ID_1", Value = "UK_LPR_CUSTOM_CODE")]    
    public class LprData : VideoDeviceData, INamedObjectData
    {
        private LprTypeData type;        
        private int parentCode;
       
        private LprWayData way;
        private string serial;


        [ManyToOne(Column = "LPR_TYPE", ClassType = typeof(LprTypeData),
          ForeignKey = "FK_STRUCT_LPR_TYPE", NotNull = true, Cascade = "none")]
        public LprTypeData Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value;
            }
        }

        [Property(Column = "SERIAL", NotNull = false)]
        public string Serial
        {
            get
            {
                return serial;
            }
            set
            {
                if (value != "")
                    serial = value;
            }
        }

        [ManyToOne(Column = "LPR_WAY", ClassType = typeof(LprWayData),
          ForeignKey = "FK_STRUCT_LPR_WAY", NotNull = true, Cascade = "none")]
        public LprWayData Way
        {
            get
            {
                return way;
            }
            set
            {
                way = value;
            }
        }

       

        [Key(0, Column = "PARENT_CODE", ForeignKey = "FK_LPR_VIDEO_DEVICE_CODE")]
        public virtual int ParentCode
        {
            get
            {
                return parentCode;
            }
            set
            {
                parentCode = value;
            }
        }

        [InitialData(PropertyName = "Name", PropertyValue = "LprData")]
        public static readonly UserResourceData Resource;
        public override string ToString()
        {
            return this.Name;
        }
        public override bool Equals(object obj)
        {
            try
            {
                if (obj.GetType() == typeof(LprData))
                {
                    LprData str = (LprData)obj;
                    if (this.Code == str.Code)
                    {
                        return true;
                    }
                }
            }
            catch { }
            return false;
        }
    }
}