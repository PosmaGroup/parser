using System;
using System.Collections;
using System.Text;

using NHibernate.Mapping.Attributes;
using Iesi.Collections;

namespace SmartCadCore.Model
{
    #region Class AlarmLprReportAnswerData Documentation
    /// <summary>
    /// This class represents the setAnswers of a cctv report
    /// </summary>
    /// <className>AlarmLprReportAnswerData</className>
    /// <author email="fsilva@smartmatic.com">Fernando Silva</author>
    /// <date>2007/01/30</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
 
    [Serializable]
    [Class(NameType = typeof(AlarmLprReportAnswerData), Table = "ALARM_LPR_REPORT_ANSWER", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_ALARM_LPR_REPORT_ANSWER_QUESTION_ALARM_LPR_REPORT_CODE")]
    public class AlarmLprReportAnswerData : ObjectData
    {
        #region Fields

        private QuestionData question;
        private ISet setAnswers;
        private AlarmLprReportData alarmLprReport;

        #endregion       
       
        #region Constructor

        public AlarmLprReportAnswerData()
        {
        }

       

        public AlarmLprReportAnswerData(QuestionData question, QuestionPossibleAnswerData answer, 
            string answerText)
        {
            this.question = question;
            PossibleAnswerAnswerData paad = new PossibleAnswerAnswerData();
            paad.AlarmLprReportAnswer = this;
            paad.PossibleAnswer = answer;
            paad.TextAnswer = answerText;
            this.SetAnswers.Add(paad);
            //this.answer = answer;
        }

        public AlarmLprReportAnswerData(QuestionData question, string answer)
        {
            this.question = question;
            PossibleAnswerAnswerData paad = new PossibleAnswerAnswerData();
            //this.answer = answer;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Alarm report question
        /// </summary>
        [ManyToOne(0, ClassType = typeof(QuestionData), ForeignKey = "FK_ALARM_LPR_REPORT_ANSWER_QUESTION_CODE", Unique = true, Cascade = "none")]
        [Column(1, Name = "ALARM_LPR_REPORT_ANSWER_QUESTION_CODE", UniqueKey = "UK_ALARM_LPR_REPORT_ANSWER_QUESTION_ALARM_LPR_REPORT_CODE", NotNull = true, Index = "IX_ALARM_LPR_REPORT_ANSWER_QUESTION_ALARM_LPR_REPORT")]
        public virtual QuestionData Question
        {
            get
            {
                return question;
            }
            set
            {
                question = value;
            }
        }

        [Set(0, Name = "SetAnswers", Table = "POSSIBLE_ANSWER_ANSWER_DATA", Cascade = "save-update",
            Lazy = CollectionLazy.True/*, Inverse = true*/, Fetch = CollectionFetchMode.Subselect)]
        [Key(1, Column = "ALARM_LPR_REPORT_ANSWER_CODE"/*, ForeignKey = "FK_POSSIBLE_ANSWER_ANSWER_ALARM_LPR_REPORT_ANSWER"*/)]
        [OneToMany(2, ClassType = typeof(PossibleAnswerAnswerData))]
        public ISet SetAnswers
        {
            get
            {
                return setAnswers;
            }
            set
            {
                setAnswers = value;
            }
        } 

        /// <summary>
        /// ALARM report parent of this answer
        /// </summary>
        [ManyToOne(0, ClassType = typeof(AlarmLprReportData), ForeignKey = "FK_ALARM_LPR_REPORT_ALARM_LPR_REPORT_ANSWER_CODE", Unique = true, Cascade = "none")]
        [Column(1, Name = "ALARM_LPR_REPORT_CODE", UniqueKey = "UK_ALARM_LPR_REPORT_ANSWER_QUESTION_ALARM_LPR_REPORT_CODE", NotNull = true, Index = "IX_ALARM_LPR_REPORT_ANSWER_QUESTION_ALARM_LPR_REPORT")]
        public virtual AlarmLprReportData AlarmLprReport
        {
            get
            {
                return alarmLprReport;
            }
            set
            {
                alarmLprReport = value;
            }
        }
        #endregion

        public override bool Equals(object obj)
        {

            if ((question.Code != (obj as AlarmLprReportAnswerData).Question.Code)&&
                (question.Text != (obj as AlarmLprReportAnswerData).Question.Text) &&
                (question.ShortText != (obj as AlarmLprReportAnswerData).Question.ShortText))
                return false;
            else
                return true;
        }
    }
}
