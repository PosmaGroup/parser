using System;
using System.Collections;
using System.Text;

using NHibernate.Mapping.Attributes;
using Iesi.Collections;

namespace SmartCadCore.Model
{
    #region Class AlarmReportAnswerData Documentation
    /// <summary>
    /// This class represents the setAnswers of a cctv report
    /// </summary>
    /// <className>AlarmReportAnswerData</className>
    /// <author email="fsilva@smartmatic.com">Fernando Silva</author>
    /// <date>2007/01/30</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
 
    [Serializable]
    [Class(NameType = typeof(AlarmReportAnswerData), Table = "ALARM_REPORT_ANSWER", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_ALARM_REPORT_ANSWER_QUESTION_ALARM_REPORT_CODE")]
    public class AlarmReportAnswerData : ObjectData
    {
        #region Fields

        private QuestionData question;
        private ISet setAnswers;
        private AlarmReportData alarmReport;

        #endregion       
       
        #region Constructor

        public AlarmReportAnswerData()
        {
        }

       

        public AlarmReportAnswerData(QuestionData question, QuestionPossibleAnswerData answer, 
            string answerText)
        {
            this.question = question;
            PossibleAnswerAnswerData paad = new PossibleAnswerAnswerData();
            paad.AlarmReportAnswer = this;
            paad.PossibleAnswer = answer;
            paad.TextAnswer = answerText;
            this.SetAnswers.Add(paad);
            //this.answer = answer;
        }

        public AlarmReportAnswerData(QuestionData question, string answer)
        {
            this.question = question;
            PossibleAnswerAnswerData paad = new PossibleAnswerAnswerData();
            //this.answer = answer;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Alarm report question
        /// </summary>
        [ManyToOne(0, ClassType = typeof(QuestionData), ForeignKey = "FK_ALARM_REPORT_ANSWER_QUESTION_CODE", Unique = true, Cascade = "none")]
        [Column(1, Name = "ALARM_REPORT_ANSWER_QUESTION_CODE", UniqueKey = "UK_ALARM_REPORT_ANSWER_QUESTION_ALARM_REPORT_CODE", NotNull = true, Index = "IX_ALARM_REPORT_ANSWER_QUESTION_ALARM_REPORT")]
        public virtual QuestionData Question
        {
            get
            {
                return question;
            }
            set
            {
                question = value;
            }
        }

        [Set(0, Name = "SetAnswers", Table = "POSSIBLE_ANSWER_ANSWER_DATA", Cascade = "save-update",
            Lazy = CollectionLazy.True/*, Inverse = true*/, Fetch = CollectionFetchMode.Subselect)]
        [Key(1, Column = "ALARM_REPORT_ANSWER_CODE"/*, ForeignKey = "FK_POSSIBLE_ANSWER_ANSWER_ALARM_REPORT_ANSWER"*/)]
        [OneToMany(2, ClassType = typeof(PossibleAnswerAnswerData))]
        public ISet SetAnswers
        {
            get
            {
                return setAnswers;
            }
            set
            {
                setAnswers = value;
            }
        } 

        /// <summary>
        /// ALARM report parent of this answer
        /// </summary>
        [ManyToOne(0, ClassType = typeof(AlarmReportData), ForeignKey = "FK_ALARM_REPORT_ALARM_REPORT_ANSWER_CODE", Unique = true, Cascade = "none")]
        [Column(1, Name = "ALARM_REPORT_CODE", UniqueKey = "UK_ALARM_REPORT_ANSWER_QUESTION_ALARM_REPORT_CODE", NotNull = true, Index = "IX_ALARM_REPORT_ANSWER_QUESTION_ALARM_REPORT")]
        public virtual AlarmReportData AlarmReport
        {
            get
            {
                return alarmReport;
            }
            set
            {
                alarmReport = value;
            }
        }
        #endregion

        public override bool Equals(object obj)
        {

            if ((question.Code != (obj as AlarmReportAnswerData).Question.Code)&&
                (question.Text != (obj as AlarmReportAnswerData).Question.Text) &&
                (question.ShortText != (obj as AlarmReportAnswerData).Question.ShortText))
                return false;
            else
                return true;
        }
    }
}
