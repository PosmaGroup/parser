﻿using System;
using System.Linq;
using System.Text;
using NHibernate.Mapping.Attributes;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(TelephonyStatsHistoryData), Table = "TELEPHONY_STATS_HISTORY", Lazy = false, Where = "DELETED_ID IS NULL")]
    public class TelephonyStatsHistoryData : ObjectData
    {
        private DateTime timeStamp;
        private string name;
        private double _value;
        private double _valueRecovery;

        [Property(Column = "TIME_STAMP", NotNull = true)]
        public DateTime TimeStamp 
        {
            get 
            {
                return timeStamp;
            }
            set
            {
                timeStamp = value;
            }
        }

        [Property(Column = "NAME", NotNull = true)]
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        [Property(Column = "VALUE", NotNull = true)]
        public double Value
        {
            get
            {
                return _value;
            }
            set
            {
                _value = value;
            }
        }

        [Property(Column = "VALUE_RECOVERY", NotNull = false)]
        public double ValueRecovery
        {
            get
            {
                return _valueRecovery;
            }
            set
            {
                _valueRecovery = value;
            }
        }
    }
}
