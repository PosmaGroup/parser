using System;
using System.Text;

using NHibernate.Mapping.Attributes;

namespace SmartCadCore.Model
{
    [Serializable]
    public class BaseSessionHistory : ObjectData
    {
        #region Properties
		[Property(0, TypeType = typeof(DateTime))]
		[Column(1, Name = "{{START_DATE}}", NotNull = false, UniqueKey = "{{UK_START}}", Index = "{{IX_START}}")]
		[Column(2, Name = "{{START_DATE}}", NotNull = false, UniqueKey = "{{UK_START1}}")]
		[AttributeIdentifier("START_DATE", Value = "START_DATE")]
		[AttributeIdentifier("{{UK_START}}", Value = "UK_START")]
		[AttributeIdentifier("UK_START1", Value = "")]
		[AttributeIdentifier("IX_START", Value = "IX_START")]
		public DateTime? Start { get; set; }

        [Property(0, TypeType = typeof(DateTime))]
        [Column(1, Name = "END_DATE", NotNull = false, UniqueKey = "{{UK_END}}")]
        [Column(2, Name = "END_DATE", NotNull = false, UniqueKey = "{{UK_END1}}")]
        [AttributeIdentifier("END_DATE", Value = "END_DATE")]
        [AttributeIdentifier("{{UK_END}}", Value = "UK_END")]
        [AttributeIdentifier("UK_END1", Value = "")]
        public DateTime? End { get; set; }
        #endregion
    }
}
