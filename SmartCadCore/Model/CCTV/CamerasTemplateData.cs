﻿using System;
using System.Linq;
using System.Text;
using NHibernate.Mapping.Attributes;
using Iesi.Collections;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(CamerasTemplateData), Table = "CAMERAS_TEMPLATE", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_CAMERAS_TEMPLATE_NAME")]
    public class CamerasTemplateData : ObjectData, INamedObjectData
    {
        public CamerasTemplateData()
        {
        }

        [Set(0, Name = "SetCameras", Table = "CAMERAS_TEMPLATE_CAMERA", Cascade = "all-delete-orphan", Lazy = CollectionLazy.True, Fetch = CollectionFetchMode.Subselect, BatchSize = 5)]
        [Key(1, Column = "CAMERAS_TEMPLATE_CODE", ForeignKey = "FK_CAMERAS_TEMPLATE_CAMERA_CODE")]
        [ManyToMany(2, ClassType = typeof(CameraData), Column = "CAMERA_CODE", ForeignKey = "FK_CAMERA_CAMERAS_TEMPLATE_CODE")]
        public ISet SetCameras
        {
            get;
            set;
        }

        [Property(0, Unique = true)]
        [Column(1, Name = "NAME", UniqueKey = "UK_CAMERAS_TEMPLATE_NAME", NotNull = true)]
        public string Name
        {
            get;
            set;
        }


        [InitialData(PropertyName = "Name", PropertyValue = "CamerasTemplateData")]
        public static readonly UserResourceData Resource;

        #region Overrides

        public override string ToString()
        {
            return this.Name;
        }

        #endregion

    }
}
