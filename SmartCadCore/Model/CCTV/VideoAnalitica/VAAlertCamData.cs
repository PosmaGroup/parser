﻿using NHibernate.Mapping.Attributes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Iesi.Collections;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(VAAlertCamData), Table = "VA_ALERT_CAM", Lazy = false, BatchSize = 1000, Where = "DELETED_ID IS NULL")]
    public class VAAlertCamData : ObjectData 
    {

        #region Fields

        private CameraData camera;

        private DateTime alertDate;

        private int status;

        private int externalId;

        private VARuleData rule;

        private VAAlertVideoData videoAlert;

        #endregion


        #region Properties
        
        [ManyToOne(0, ClassType = typeof(CameraData), ForeignKey = "FK_VA_ALERT_CAM_CAMERA", Cascade = "none", Column = "CAMERA_CODE")]
        [Column(1, Name = "CAMERA_CODE", NotNull = true)]
        public virtual CameraData Camera
        {
            get { return camera;}
            set { camera = value; }
        }
        
        [Property(0, TypeType = typeof(DateTime))]
        [Column(1, Name = "ALERT_DATE")]
        public virtual DateTime AlertDate
        {
            get { return alertDate;}
            set { alertDate = value; }
        }

        [Property(0)]
        [Column(1, Name = "STATUS", NotNull = false)]
        public virtual int Status
        {
            get { return status;}
            set { status = value; }
        }

        [Property(0)]
        [Column(1, Name = "EXTERNAL_ID", NotNull = true)]
        public virtual int ExternalId
        {
            get { return externalId;}
            set { externalId = value; }
        }

        [ManyToOne(0, ClassType = typeof(VARuleData), ForeignKey = "FK_VA_ALERT_CAM_RULE", Cascade = "none")]
        [Column(1, Name = "RULE_CODE", NotNull = true)]
        public virtual VARuleData Rule
        {
            get { return rule;}
            set { rule = value; }
        }
        

        
        [ComponentProperty(ComponentType = typeof(VAAlertVideoData))]
        public virtual VAAlertVideoData VideoAlert
        {
            get { return videoAlert;}
            set { videoAlert = value; }
        }
        
        #endregion

        #region InitialData

        [InitialData(PropertyName = "Name", PropertyValue = "VAAlertCamData")]
        public static readonly UserResourceData Resource;
        #endregion

    }
}