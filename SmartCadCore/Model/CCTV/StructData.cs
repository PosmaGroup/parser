using System;
using System.Text;
using NHibernate.Mapping.Attributes;
using Iesi.Collections;


namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(StructData), Table = "STRUCT", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_STRUCT_CUSTOM_CODE")]
    public class StructData: ObjectData, INamedObjectData
    {
        private ISet devices;
        private CctvZoneData zone; 
        private StructAddressData addres;
        private StructTypeData type;
        private string name;

        [ComponentProperty(ComponentType = typeof(StructAddressData))]
        public StructAddressData Addres
        {
            get
            {
                return addres;
            }
            set
            {
                addres = value;
            }
        }

        [Set(0, Name = "Devices", Table = "VIDEO_DEVICE", Cascade = "none",
            Lazy = CollectionLazy.True, Fetch = CollectionFetchMode.Subselect)]
        [Key(1, Column = "STRUCT_CODE"/*, ForeignKey = "FK_STRUCT_VIDEO_DEVICE_CODE"*/)]
        [OneToMany(2, ClassType = typeof(VideoDeviceData))]
        public ISet Devices
        {
            get
            {
                return devices;
            }
            set
            {
                devices = value;
            }
        }

        [ManyToOne(Column = "ZONE_CODE", ClassType = typeof(CctvZoneData), ForeignKey = "FK_ZONE_STRUCT_CODE", Cascade = "none")]
        public CctvZoneData Zone
        {
            get
            {
                return zone;
            }
            set
            {
                zone = value;
            }
        }

        [ManyToOne(Column = "STRUCT_TYPE", ClassType = typeof(StructTypeData), ForeignKey = "FK_ZONE_STRUCT_TYPE", Cascade = "none")]
        public StructTypeData Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value;
            }
        }

        [Property(0, Unique = true)]
        [Column(1, Name = "NAME", UniqueKey = "UK_STRUCT_CUSTOM_CODE", NotNull = true)]
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }       

        [InitialData(PropertyName = "Name", PropertyValue = "StructData")]
        public static readonly UserResourceData Resource;

        public override string ToString()
        {
            if (this.type != null)
                return this.name + "  (" + this.Type + ")";
            else
                return this.name;
        }

        public override bool Equals(object obj)
        {
            try
            {
                if (obj.GetType() == typeof(StructData))
                {
                    StructData str = (StructData)obj;
                    if (this.Code == str.Code)
                    {
                        return true;
                    }
                }
            }
            catch { }
            return false;
        }
    }
}
