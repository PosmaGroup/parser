using System;

using System.Text;
using NHibernate.Mapping.Attributes;
using Iesi.Collections;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(CctvZoneData), Table = "CCTV_ZONE", Lazy = false, Where = "DELETED_ID IS NULL")]   
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_CCTV_ZONE_CUSTOM_CODE")]
    public class CctvZoneData: ObjectData
    {        
        //private string customCode;
        private string name = string.Empty;
        private ISet structs;

        [Set(0, Name = "Structs", Table = "STRUCT", Cascade = "all-delete-orphan",
          Lazy = CollectionLazy.True, Fetch = CollectionFetchMode.Subselect, Where = "DELETED_ID IS NULL")]
        [Key(1, Column = "ZONE_CODE"/*, ForeignKey = "FK_ZONE_STRUCT_CODE"*/)]
        [OneToMany(2, ClassType = typeof(StructData))]
        public ISet Structs
        {
            get
            {
                return structs;
            }
            set
            {
                structs = value;
            }
        }

        [Property(0, Unique = true)]
        [Column(1, Name = "NAME", UniqueKey = "UK_CCTV_ZONE_CUSTOM_CODE", NotNull = true)]
        public virtual string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public override string ToString()
        {
            return this.Name;           
        }

        [InitialData(PropertyName = "Name", PropertyValue = "CctvZoneData")]
        public static readonly UserResourceData Resource;
    }
}
