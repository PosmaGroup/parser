﻿using NHibernate.Mapping.Attributes;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(DepartmentZoneAddressData), Table = "DEPARTMENT_ZONE_ADDRESS", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_DEPARTMENTZONEADDRESS_CUSTOM_CODE")]
    public class DepartmentZoneAddressData : ObjectData
    {
        private DepartmentZoneData zone;
        private MultipleAddressData addres;
        private string customCode;
        private string pointNumber;


        [ComponentProperty(ComponentType = typeof(MultipleAddressData))]
        public MultipleAddressData Address
        {
            get
            {
                return addres;
            }
            set
            {
                addres = value;
            }
        }

        [ManyToOne(Column = "ZONE_CODE", ClassType = typeof(DepartmentZoneData), ForeignKey = "FK_DEPARTMENT_ZONE_ADDRESS_CODE", Cascade = "none")]
        public DepartmentZoneData Zone
        {
            get
            {
                return zone;
            }
            set
            {
                zone = value;
            }
        }

        [Property(0, Unique = true)]
        [Column(1, Name = "CUSTOM_CODE", UniqueKey = "UK_DEPARTMENTZONEADDRESS_CUSTOM_CODE", NotNull = true)]
        public string CustomCode
        {
            get
            {
                return customCode;
            }
            set
            {
                customCode = value;
            }
        }


        [Property(0, Unique = false)]
        [Column(1, Name = "POINT_NUMBER", NotNull = true)]
        public string PointNumber
        {
            get
            {
                return pointNumber;
            }
            set
            {
                pointNumber = value;
            }
        }

        [InitialData(PropertyName = "Name", PropertyValue = "DepartmentZoneAddressData")]
        public static readonly UserResourceData Resource;


        public override bool Equals(object obj)
        {
            try
            {
                if (obj.GetType() == typeof(DepartmentZoneAddressData))
                {
                    DepartmentZoneAddressData str = (DepartmentZoneAddressData)obj;
                    if ((this.addres.Lon == str.addres.Lon) &&
                        (this.addres.Lat == str.addres.Lat))
                    {
                        return true;
                    }
                }
                return false;
            }
            catch { }
            return false;
        }
    }
}
