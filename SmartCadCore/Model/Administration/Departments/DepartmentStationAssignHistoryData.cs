﻿using NHibernate.Mapping.Attributes;
using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(DepartmentStationAssignHistoryData), Table = "DEPARTMENT_STATION_ASSIGN_HISTORY", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("UK_START", Value = "UK_DEPARTMENT_STATION_ASSIGN")]
    [AttributeIdentifier("UK_END", Value = "UK_DEPARTMENT_STATION_ASSIGN")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_DEPARTMENT_STATION_ASSIGN")]
    [AttributeIdentifier("IX_START", Value = "IX_DEPARTMENT_STATION_ASSIGN_HISTORY_START")]
    public class DepartmentStationAssignHistoryData : BaseSessionHistory
    {
        #region Enums

        /// <summary>
        /// Enumerates the properties that this class has.
        /// </summary>
        public enum Properties
        {
            DepartmentStation,
            Start,
            End
        }

        #endregion

        #region Fields
        private DepartmentStationData departmentStation;
        private IList unitOfficerAssign;
        #endregion

        #region Constructors
        public DepartmentStationAssignHistoryData()
        {
        }
        #endregion

        #region Properties
        [ManyToOne(0, ClassType = typeof(DepartmentStationData), ForeignKey = "FK_DEPARTMENT_STATION_ASSIGN_DEPARTMENT_STATION_CODE", Unique = true, Cascade = "none")]
        [Column(1, Name = "DEPARTMENT_STATION_CODE", NotNull = true, UniqueKey = "UK_DEPARTMENT_STATION_ASSIGN")]
        public virtual DepartmentStationData DepartmentStation
        {
            get
            {
                return departmentStation;
            }
            set
            {
                departmentStation = value;
            }
        }

        [Bag(0, Cascade = "all-delete-orphan", Lazy = CollectionLazy.True, Fetch = CollectionFetchMode.Subselect, Where = "DELETED_ID IS NULL")]
        [Key(1, Column = "DEPARTMENT_STATION_ASSIGN_HISTORY_CODE",
           ForeignKey = "FK_DEPARTMENT_STATION_ASSIGN_HISTORY_UNIT_OFFICER_ASSIGN_HISTORY_CODE")]
        [OneToMany(2, ClassType = typeof(UnitOfficerAssignHistoryData))]
        public virtual IList UnitOfficerAssign
        {
            get
            {
                return unitOfficerAssign;
            }
            set
            {
                unitOfficerAssign = value;
            }
        }
        #endregion

        #region Resource
        [InitialData(PropertyName = "Name", PropertyValue = "DepartmentStationAssignHistoryData")]
        public static readonly UserResourceData Resource;
        #endregion
    }
}
