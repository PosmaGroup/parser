﻿using System;
using System.Linq;
using System.Text;
using NHibernate.Mapping.Attributes;

namespace SmartCadCore.Model
{   

    [Serializable]
    [Class(NameType = typeof(DepartmentTypeAlertData), Table = "DEPARTMENT_TYPE_ALERT", Lazy = false, Where = "DELETED_ID IS NULL")]
    public class DepartmentTypeAlertData: ObjectData
    {
        public enum AlertPriority
        {
            Low = 0,
            Medium = 1,
            High = 2
        }

        #region Fields
        private AlertData alert;
        private DepartmentTypeData department;
        private AlertPriority priority;
        private int maxValue;

        #endregion
        
        #region Properties

        [ManyToOne(ClassType = typeof(AlertData), Column = "ALERT_CODE",
            Cascade = "none", ForeignKey = "FK_DEPARTMENT_TYPE_ALERT_ALERT_CODE")]
        public AlertData Alert
        {
            get
            {
                return alert;
            }
            set
            {
                alert = value;
            }
        }

        [ManyToOne(Column = "DEPARTMENT_TYPE_CODE", ClassType = typeof(DepartmentTypeData),
            ForeignKey = "FK_DEPARTMENT_TYPE_ALERT_DEPARTMENT_TYPE_CODE", Cascade = "none")]
        public DepartmentTypeData DepartmentType
        {
            get
            {
                return department;
            }
            set
            {
                department = value;
            }
        }

        [Property(0, Type = "int", Unique = false)]
        [Column(1, Name = "PRIORITY", NotNull = true)]
        public AlertPriority Priority
        {
            get
            {
                return priority;
            }
            set
            {
                priority = value;
            }
        }

        [Property(Column = "MAX_VALUE", NotNull = false)]
        public int MaxValue
        {
            get
            {
                return maxValue;
            }
            set
            {
                maxValue = value;
            }
        }

        #endregion


        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is DepartmentTypeAlertData)
            {
                result = this.Code == ((DepartmentTypeAlertData)obj).Code;
            }
            return result;
        }
    }
}
