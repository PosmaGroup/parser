using System;
using System.Collections;
using System.Text;

using NHibernate.Mapping.Attributes;

using Iesi.Collections;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(DepartmentZoneData), Table = "DEPARTMENT_ZONE", Lazy = false, BatchSize = 1000, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_DEPARTMENT_ZONE_NAME")]
    public class DepartmentZoneData : ObjectData, INamedObjectData
    {
        #region Fields

        private string customCode;
        private string name;
        private ISet setDepartmentStations;
        private DepartmentTypeData departmentType;
        private ISet departmentZoneAddres;

        #endregion

        #region Constructors

        public DepartmentZoneData()
        {
        }

        #endregion

        #region Properties

        [Property(Column = "CUSTOM_CODE")]
        public virtual string CustomCode
        {
            get
            {
                return customCode;
            }
            set
            {
                customCode = value;
            }
        }

        [Property(0, Unique = true)]
        [Column(1, Name = "NAME", UniqueKey = "UK_DEPARTMENT_ZONE_NAME", NotNull = true)]
        public virtual string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        [Set(0, Name = "SetDepartmentStations", Table = "DEPARTMENT_STATION", Cascade = "all-delete-orphan",
            Lazy = CollectionLazy.True, /*Inverse = true,*/ Fetch = CollectionFetchMode.Subselect, Where = "DELETED_ID IS NULL")]
        [Key(1, Column = "DEPARTMENT_ZONE_CODE"/*, ForeignKey = "FK_DEPARTMENT_ZONE_DEPARTMENT_STATION_CODE"*/)]
        [OneToMany(2, ClassType = typeof(DepartmentStationData))]
        public ISet SetDepartmentStations
        {
            get
            {
                return setDepartmentStations;
            }
            set
            {
                setDepartmentStations = value;
            }
        }

        [ManyToOne(0, ClassType = typeof(DepartmentTypeData),
			ForeignKey = "FK_DEPARTMENT_ZONE_DEPARTMENT_TYPE_CODE", Cascade = "none", Fetch = FetchMode.Join)]
        [Column(1, Name = "DEPARTMENT_TYPE_CODE", NotNull = true,
            UniqueKey = "UK_DEPARTMENT_ZONE_NAME")]
        public virtual DepartmentTypeData DepartmentType
        {
            get
            {
                return departmentType;
            }
            set
            {
                departmentType = value;
            }
        }

     


        [Set(0,  Name = "DepartmentZoneAddres", Table = "DEPARTMENT_ZONE_ADDRESS", Cascade = "save-update",
           Lazy = CollectionLazy.True, Fetch = CollectionFetchMode.Subselect, Inverse = true, Where = "DELETED_ID IS NULL")]
        [Key(1, Column = "ZONE_CODE"/*, ForeignKey = "FK_DEPARTMENT_ZONE_ADDRESS_CODE"*/)]
        [OneToMany(2, ClassType = typeof(DepartmentZoneAddressData))]
        public ISet DepartmentZoneAddres
        {
            get
            {
                return departmentZoneAddres;
            }
            set
            {
                departmentZoneAddres = value;
            }
        }




        #endregion

        public override string ToString()
        {
            return this.Name;
        }
        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is DepartmentZoneData)
            {
                result = this.Code == ((DepartmentZoneData)obj).Code;
            }
            return result;
        }

        #region Initial data

        [InitialData(PropertyName = "Name", PropertyValue = "DepartmentZoneData")]
        public static readonly UserResourceData Resource;

        #endregion
    }
}
