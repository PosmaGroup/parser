﻿using NHibernate.Mapping.Attributes;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(DepartmentStationAddressData), Table = "DEPARTMENT_STATION_ADDRESS", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_DEPARTMENTSTATIONADDRESS_CUSTOM_CODE")]
    public class DepartmentStationAddressData : ObjectData
    {
        public DepartmentStationAddressData(MultipleAddressData address)
        {
            this.addres = address;
        }

        public DepartmentStationAddressData()
        {
        }

        private DepartmentStationData station;
        private MultipleAddressData addres;
        private string customCode;
        private string pointNumber;


        [ComponentProperty(ComponentType = typeof(MultipleAddressData))]
        public MultipleAddressData Address
        {
            get
            {
                return addres;
            }
            set
            {
                addres = value;
            }
        }

        [ManyToOne(Column = "STATION_CODE", ClassType = typeof(DepartmentStationData), ForeignKey = "FK_DEPARTMENT_STATION_ADDRESS_CODE", Cascade = "none")]
        public DepartmentStationData Station
        {
            get
            {
                return station;
            }
            set
            {
                station = value;
            }
        }

        [Property(0, Unique = true)]
        [Column(1, Name = "CUSTOM_CODE", UniqueKey = "UK_DEPARTMENTSTATIONADDRESS_CUSTOM_CODE", NotNull = true)]
        public string CustomCode
        {
            get
            {
                return customCode;
            }
            set
            {
                customCode = value;
            }
        }


        [Property(0, Unique = false)]
        [Column(1, Name = "POINT_NUMBER", NotNull = true)]
        public string PointNumber
        {
            get
            {
                return pointNumber;
            }
            set
            {
                pointNumber = value;
            }
        }

        [InitialData(PropertyName = "Name", PropertyValue = "DepartmentStationAddressData")]
        public static readonly UserResourceData Resource;


        public override bool Equals(object obj)
        {
            try
            {
                if (obj.GetType() == typeof(DepartmentStationAddressData))
                {
                    DepartmentStationAddressData str = (DepartmentStationAddressData)obj;
                    if ((this.addres.Lon == str.addres.Lon) &&
                        (this.addres.Lat == str.addres.Lat))
                    {
                        return true;
                    }
                }
                return false;
            }
            catch { }
            return false;
        }

        public override string ToString()
        {
            return PointNumber;
        }
    }
}
