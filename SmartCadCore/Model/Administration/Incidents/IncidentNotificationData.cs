﻿using Iesi.Collections;
using NHibernate.Mapping.Attributes;
using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{

    [Serializable]
    [Class(NameType = typeof(IncidentNotificationData), Table = "INCIDENT_NOTIFICATION", Lazy = false, Where = "DELETED_ID IS NULL", BatchSize = 5)]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_INCIDENT_NOTIFICATION_CUSTOM_CODE")]
    public class IncidentNotificationData : ObjectData
    {
        #region Fields

        private ReportBaseData reportBase;
        private DepartmentTypeData departmentType;
        private IncidentNotificationPriorityData priority;
        private DateTime? creationDate;
        private DateTime? startDate;
        private DateTime? endDate;
        private string comment;
        private ISet setDispatchOrders;
        private OperatorData dispatchOperator;
        private IncidentNotificationStatusData status;
        private bool? availableRecommendedUnits;
        private string customCode;
        private string friendlyCustomCode;
        private IList statusHistoryList;
        private DepartmentStationData departmentStation;
        private string cancelDetail;
        private bool recalculateStatus;

        #endregion

        #region Constructors

        public IncidentNotificationData()
        {
        }

        public IncidentNotificationData(
            ReportBaseData reportBase,
            DepartmentTypeData departmentType,
            IncidentNotificationPriorityData priority,
            DateTime creationDate,
            DateTime startDate,
            DateTime endDate,
            string comment,
            ISet dispatchOrders,
            IncidentNotificationStatusData status,
            string customCode)
        {
            this.reportBase = reportBase;
            this.departmentType = departmentType;
            this.priority = priority;
            this.CreationDate = startDate;
            this.StartDate = startDate;
            this.EndDate = endDate;
            this.comment = comment;
            this.setDispatchOrders = dispatchOrders;
            this.Status = status;
            this.customCode = customCode;
        }

        #endregion

        #region Properties

        [ManyToOne(0, ClassType = typeof(ReportBaseData), ForeignKey = "FK_INCIDENT_NOTIFICATION_REPORT_BASE_CODE", Cascade = "none")]
        [Column(1, Name = "REPORT_BASE_CODE", NotNull = true, Index = "IX_INCIDENT_NOTIFICATION_REPORT_BASE")]
        public virtual ReportBaseData ReportBase
        {
            get
            {
                return reportBase;
            }
            set
            {
                reportBase = value;
            }
        }

        [ManyToOne(ClassType = typeof(DepartmentTypeData), Column = "DEPARTMENT_TYPE_CODE",
            ForeignKey = "FK_INCIDENT_NOTIFICATION_DEPARTMENT_TYPE_CODE", Cascade = "none")]
        public virtual DepartmentTypeData DepartmentType
        {
            get
            {
                return departmentType;
            }
            set
            {
                departmentType = value;
            }
        }

        [ManyToOne(ClassType = typeof(IncidentNotificationPriorityData), Column = "INCIDENT_NOTIFICATION_PRIORITY_CODE",
            ForeignKey = "FK_INCIDENT_NOTIFICATION_INCIDENT_NOTIFICATION_PRIORITY_CODE", Cascade = "none")]
        public virtual IncidentNotificationPriorityData Priority
        {
            get
            {
                return priority;
            }
            set
            {
                priority = value;
            }
        }

        [Property(0, TypeType = typeof(DateTime))]
        [Column(1, Name = "CREATION_DATE", NotNull = true)]
        public virtual DateTime? CreationDate
        {
            get
            {
                return creationDate;
            }
            set
            {
                creationDate = value;
            }
        }

        [Property(0, TypeType = typeof(DateTime))]
        [Column(1, Name = "START_DATE")]
        public virtual DateTime? StartDate
        {
            get
            {
                return startDate;
            }
            set
            {
                startDate = value;
            }
        }

        [Property(0, TypeType = typeof(DateTime))]
        [Column(1, Name = "END_DATE")]
        public virtual DateTime? EndDate
        {
            get
            {
                return endDate;
            }
            set
            {
                endDate = value;
            }
        }

        [Property(0)]
        [Column(1, Name = "COMMENT", Length = 1000)]
        public virtual string Comment
        {
            get
            {
                return comment;
            }
            set
            {
                comment = value;
            }
        }

        [Set(0, Name = "SetDispatchOrders", Table = "DISPATCH_ORDER", Cascade = "none",
            Lazy = CollectionLazy.True, Inverse = true, Fetch = CollectionFetchMode.Subselect, BatchSize = 10)]
        [Key(1, Column = "INCIDENT_NOTIFICATION_CODE"/*, ForeignKey = "FK_INCIDENT_NOTIFICATION_DISPATCH_ORDERS_CODE"*/)]
        [OneToMany(2, ClassType = typeof(DispatchOrderData))]
        public ISet SetDispatchOrders
        {
            get
            {
                return setDispatchOrders;
            }
            set
            {
                setDispatchOrders = value;
            }
        }

        [ManyToOne(ClassType = typeof(OperatorData), Column = "USER_ACCOUNT_CODE",
            ForeignKey = "FK_USER_ACCOUNT_INCIDENT_NOTIFICATION_CODE", Cascade = "none")]
        public virtual OperatorData DispatchOperator
        {
            get
            {
                return dispatchOperator;
            }
            set
            {
                dispatchOperator = value;
            }
        }

        [ManyToOne(ClassType = typeof(IncidentNotificationStatusData), Column = "INCIDENT_NOTIFICATION_STATUS_CODE",
            ForeignKey = "FK_INCIDENT_NOTIFICATION_INCIDENT_NOTIFICATION_STATUS_CODE", Cascade = "none")]
        public virtual IncidentNotificationStatusData Status
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
            }
        }

        [Property(0, Unique = true)]
        [Column(1, Name = "CUSTOM_CODE", UniqueKey = "UK_INCIDENT_NOTIFICATION_CUSTOM_CODE", NotNull = true)]
        public virtual string CustomCode
        {
            get
            {
                return customCode;
            }
            set
            {
                customCode = value;
            }
        }

        [Property(0, Column = "FRIENDLY_CUSTOM_CODE", NotNull = true)]
        public virtual string FriendlyCustomCode
        {
            get
            {
                return friendlyCustomCode;
            }
            set
            {
                friendlyCustomCode = value;
            }
        }

        public virtual bool? AvailableRecommendedUnits
        {
            get
            {
                return availableRecommendedUnits;
            }
            set
            {
                availableRecommendedUnits = value;
            }
        }

        [ManyToOne(ClassType = typeof(DepartmentStationData), Column = "DEPARTMENT_STATION_CODE",
           ForeignKey = "FK_DEPARTMENT_STATION_INCIDENT_NOTIFICATION_CODE", Cascade = "none")]
        public DepartmentStationData DepartmentStation
        {
            get
            {
                return departmentStation;
            }
            set
            {
                departmentStation = value;
            }
        }

        [Bag(0, Name = "StatusHistoryList", Table = "INCIDENT_NOTIFICATION_STATUS_HISTORY",
            Cascade = "save-update", Lazy = CollectionLazy.True, Fetch = CollectionFetchMode.Subselect)]
        [Key(1, Column = "INCIDENT_NOTIFICATION_CODE"/*, 
            ForeignKey = "FK_INCIDENT_NOTIFICATION_STATUS_HISTORY_INCIDENT_NOTIFICATION_CODE"*/)]
        [OneToMany(3, ClassType = typeof(IncidentNotificationStatusHistoryData))]
        public virtual IList StatusHistoryList
        {
            get
            {
                return statusHistoryList;
            }
            set
            {
                statusHistoryList = value;
            }
        }

        public string CancelDetail
        {
            get
            {
                return cancelDetail;
            }
            set
            {
                cancelDetail = value;
            }
        }

        public bool RecalculateStatus
        {
            get
            {
                return recalculateStatus;
            }
            set
            {
                recalculateStatus = value;
            }
        }

        #endregion
    }
}
