﻿using NHibernate.Mapping.Attributes;
using System;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ColorBase = System.Drawing.Color;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(IncidentNotificationPriorityData), Table = "INCIDENT_NOTIFICATION_PRIORITY", BatchSize = 10, Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_INCIDENT_NOTIFICATION_PRIORITY_NAME")]
    [AttributeIdentifier("DELETED_ID_2", Value = "UK_INCIDENT_NOTIFICATION_PRIORITY_CUSTOM_CODE")]
    public class IncidentNotificationPriorityData : ObjectData
    {
        #region Fields

        private string name;
        private Color color;
        private byte[] image;
        private string customCode;
        private ComponentColorData componentColor;

        #endregion

        #region Constructors

        public IncidentNotificationPriorityData()
        {
        }

        public IncidentNotificationPriorityData(string name, Color color, byte[] image)
        {
            this.name = name;
            this.color = color;
            this.image = image;
        }

        #endregion

        #region Properties

        [Property(0, Unique = true)]
        [Column(1, Name = "NAME", UniqueKey = "UK_INCIDENT_NOTIFICATION_PRIORITY_NAME", NotNull = true)]
        public virtual string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        //[Property(Column = "COLOR", TypeType = typeof(Color))]
        public virtual Color Color
        {
            get
            {
                if (ComponentColor != null)
                    return ColorBase.FromArgb(ComponentColor.ColorA.Value, ComponentColor.ColorR.Value, ComponentColor.ColorG.Value, ComponentColor.ColorB.Value);
                else
                    return Color.White;
            }
            set
            {
                componentColor = new ComponentColorData(value);
            }
        }

        [ComponentProperty(ComponentType = typeof(ComponentColorData))]
        public ComponentColorData ComponentColor
        {
            get
            {
                return componentColor;
            }
            set
            {
                componentColor = value;
                color = ColorBase.FromArgb(value.ColorA.Value, value.ColorR.Value, value.ColorG.Value, value.ColorB.Value);
            }
        }

        [Property(Column = "IMAGE", Type = "BinaryBlob", Length = int.MaxValue)]
        public virtual byte[] Image
        {
            get
            {
                return image;
            }
            set
            {
                image = value;
            }
        }

        [Property(0, Unique = true)]
        [Column(1, Name = "CUSTOM_CODE", UniqueKey = "UK_INCIDENT_NOTIFICATION_PRIORITY_CUSTOM_CODE", NotNull = true)]
        public virtual string CustomCode
        {
            get
            {
                return customCode;
            }
            set
            {
                customCode = value;
            }
        }

        #endregion

        #region Overrides

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is IncidentNotificationPriorityData)
            {
                IncidentNotificationPriorityData inpd = (IncidentNotificationPriorityData)obj;
                result = this.Name == inpd.Name;
            }
            return result;
        }

        public override string ToString()
        {
            return this.CustomCode.ToString();
        }

        #endregion
    }
}
