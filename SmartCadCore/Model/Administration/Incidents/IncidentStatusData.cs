﻿using NHibernate.Mapping.Attributes;
using System;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ColorBase = System.Drawing.Color;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(IncidentStatusData), Table = "INCIDENT_STATUS", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_INCIDENT_STATUS_NAME")]
    [AttributeIdentifier("DELETED_ID_2", Value = "UK_INCIDENT_STATUS_CUSTOM_CODE")]
    [AttributeIdentifier("DELETED_ID_3", Value = "UK_INCIDENT_STATUS_FRIENDLY_NAME")]
    public class IncidentStatusData : ObjectData, INamedObjectData
    {
        #region Fields

        private string name;
        private string friendlyName;
        private Color color;
        private byte[] image;
        private string customCode;
        private bool? allowDelete;
        private ComponentColorData componentColor;

        #endregion

        #region Constructors

        public IncidentStatusData()
        {
        }

        public IncidentStatusData(string name, Color color, byte[] image, bool allowDelete)
        {
            this.name = name;
            this.color = color;
            this.image = image;
            this.allowDelete = allowDelete;
        }

        #endregion

        #region Properties

        [Property(0, Unique = true)]
        [Column(1, Name = "NAME", UniqueKey = "UK_INCIDENT_STATUS_NAME", NotNull = true)]
        public virtual string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        [Property(0, Unique = true)]
        [Column(1, Name = "FRIENDLY_NAME", UniqueKey = "UK_INCIDENT_STATUS_FRIENDLY_NAME", NotNull = true)]
        public virtual string FriendlyName
        {
            get
            {
                return friendlyName;
            }
            set
            {
                friendlyName = value;
            }
        }

        //[Property(Column = "COLOR", TypeType = typeof(Color))]
        public virtual Color Color
        {
            get
            {
                if (ComponentColor != null)
                    return ColorBase.FromArgb(ComponentColor.ColorA.Value, ComponentColor.ColorR.Value, ComponentColor.ColorG.Value, ComponentColor.ColorB.Value);
                else
                    return Color.White;
            }
            set
            {
                componentColor = new ComponentColorData(value);
            }
        }

        [ComponentProperty(ComponentType = typeof(ComponentColorData))]
        public ComponentColorData ComponentColor
        {
            get
            {
                return componentColor;
            }
            set
            {
                componentColor = value;
                color = ColorBase.FromArgb(value.ColorA.Value, value.ColorR.Value, value.ColorG.Value, value.ColorB.Value);
            }
        }
        [Property(Column = "IMAGE", Type = "BinaryBlob", Length = int.MaxValue)]
        public virtual byte[] Image
        {
            get
            {
                return image;
            }
            set
            {
                image = value;
            }
        }


        [Property(0, Unique = true)]
        [Column(1, Name = "CUSTOM_CODE", UniqueKey = "UK_INCIDENT_STATUS_CUSTOM_CODE", NotNull = true)]
        public virtual string CustomCode
        {
            get
            {
                return customCode;
            }
            set
            {
                customCode = value;
            }
        }

        [Property(Column = "MANDATORY", NotNull = false, TypeType = typeof(bool))]
        public virtual bool? AllowDelete
        {
            get
            {
                return allowDelete;
            }
            set
            {
                allowDelete = value;
            }
        }
        #endregion

        [InitialData(PropertyName = "CustomCode", PropertyValue = "CLOSED")]
        public static IncidentStatusData Closed;

        [InitialData(PropertyName = "CustomCode", PropertyValue = "OPEN")]
        public static IncidentStatusData Open;

        [InitialData(PropertyName = "CustomCode", PropertyValue = "DISPATCHED")]
        public static IncidentStatusData Dispatched;
    }
}
