﻿using NHibernate.Mapping.Attributes;
using System;

using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    #region Class RankData Documentation
    /// <summary>
    /// Clase que representa una dependencia
    /// </summary>
    /// <className>RankData</className>
    /// <author email="dchang@smartmatic.com">Daniel Chang</author>
    /// <date>2007/04/04</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    [Serializable]
    [Class(NameType = typeof(RankData), Table = "RANK", Lazy = false, BatchSize = 1000, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_RANK_NAME")]

    public class RankData : ObjectData
    {
        #region Constructors

        public RankData()
        {

        }

        public RankData(string name, string description, DepartmentTypeData deparmentType)
        {
            this.Name = name;
            this.Description = description;
            this.DepartmentType = deparmentType;
        }

        #endregion

        #region Properties

        [Property(0, Unique = true)]
        [Column(1, Name = "NAME", UniqueKey = "UK_RANK_NAME", NotNull = true)]
        public virtual string Name { get; set; }

        [Property(Column = "DESCRIPTION", Length = 1024)]
        public virtual string Description { get; set; }

        [ManyToOne(0, ClassType = typeof(DepartmentTypeData),
            ForeignKey = "FK_RANK_DEPARTMENT_TYPE_CODE", Cascade = "none", Fetch = FetchMode.Join)]
        [Column(1, Name = "DEPARTMENT_TYPE_CODE", NotNull = true, UniqueKey = "UK_RANK_NAME")]
        public virtual DepartmentTypeData DepartmentType { get; set; }
        #endregion

        #region Initial Data
        [InitialData(PropertyName = "Name", PropertyValue = "RankData")]
        public static readonly UserResourceData Resource;
        #endregion

        #region Overrides
        public override string ToString()
        {
            return this.Name;
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is RankData)
            {
                result = this.Code == ((RankData)obj).Code;
            }
            return result;
        }
        #endregion
    }
}
