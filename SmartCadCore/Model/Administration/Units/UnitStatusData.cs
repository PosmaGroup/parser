﻿using NHibernate.Mapping.Attributes;
using System;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ColorBase = System.Drawing.Color;


namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(UnitStatusData), Table = "UNIT_STATUS", Lazy = false, BatchSize = 1000, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_UNIT_STATUS_CUSTOM_CODE")]
    [AttributeIdentifier("DELETED_ID_2", Value = "UK_UNIT_STATUS_FRIENDLY_NAME")]
    public class UnitStatusData : ObjectData, INamedObjectData, IImmutableObjectData
    {
        #region Fields

        private string name;
        private string friendlyName;
        private Color color;
        private byte[] image;
        private string customCode;
        private bool? active;
        private bool? immutable;
        private ComponentColorData componentColor;

        #endregion

        #region Constructors

        public UnitStatusData()
        {
        }

        public UnitStatusData(string name, Color color, byte[] image, bool allowDelete)
        {
            this.name = name;
            this.color = color;
            this.image = image;
            this.immutable = allowDelete;
        }

        #endregion

        #region Properties

        [Property(Column = "NAME")]
        public virtual string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        [Property(0, Unique = true)]
        [Column(1, Name = "FRIENDLY_NAME", UniqueKey = "UK_UNIT_STATUS_FRIENDLY_NAME", NotNull = true)]
        public virtual string FriendlyName
        {
            get
            {
                return friendlyName;
            }
            set
            {
                friendlyName = value;
            }
        }

        //[Property(Column = "COLOR", TypeType = typeof(Color))]
        public virtual Color Color
        {
            get
            {
                if (ComponentColor != null)
                    return ColorBase.FromArgb(ComponentColor.ColorA.Value, ComponentColor.ColorR.Value, ComponentColor.ColorG.Value, ComponentColor.ColorB.Value);
                else
                    return Color.White;
            }
            set
            {
                componentColor = new ComponentColorData(value);
            }
        }

        [ComponentProperty(ComponentType = typeof(ComponentColorData))]
        public ComponentColorData ComponentColor
        {
            get
            {
                return componentColor;
            }
            set
            {
                componentColor = value;
                color = ColorBase.FromArgb(value.ColorA.Value, value.ColorR.Value, value.ColorG.Value, value.ColorB.Value);
            }
        }

        [Property(Column = "IMAGE", Type = "BinaryBlob", Length = int.MaxValue)]
        public virtual byte[] Image
        {
            get
            {
                return image;
            }
            set
            {
                image = value;
            }
        }

        [Property(0, Unique = true)]
        [Column(1, Name = "CUSTOM_CODE", UniqueKey = "UK_UNIT_STATUS_CUSTOM_CODE", NotNull = true)]
        public virtual string CustomCode
        {
            get
            {
                return customCode;
            }
            set
            {
                customCode = value;
            }
        }

        [Property(Column = "ACTIVE", NotNull = true, TypeType = typeof(bool))]
        public virtual bool? Active
        {
            get
            {
                return active;
            }
            set
            {
                active = value;
            }
        }

        public override string ToString()
        {
            string text = "";
            if (this.Name != null)
            {
                text = this.FriendlyName;
            }
            return text;
        }

        #region IImmutableObjectData Members

        [Property(Column = "IMMUTABLE", NotNull = false, TypeType = typeof(bool))]
        public virtual bool? Immutable
        {
            get
            {
                return immutable;
            }
            set
            {
                immutable = value;
            }
        }

        #endregion

        #endregion

        #region InitialData

        [InitialData(PropertyName = "CustomCode", PropertyValue = "AVAILABLE")]
        public static UnitStatusData Available;

        [InitialData(PropertyName = "CustomCode", PropertyValue = "OUTOFORDER")]
        public static UnitStatusData OutOfOrder;

        [InitialData(PropertyName = "CustomCode", PropertyValue = "ASSIGNED")]
        public static UnitStatusData Assigned;

        [InitialData(PropertyName = "CustomCode", PropertyValue = "ONSCENE")]
        public static UnitStatusData OnScene;

        [InitialData(PropertyName = "CustomCode", PropertyValue = "DELAYED")]
        public static UnitStatusData Delayed;

        [InitialData(PropertyName = "CustomCode", PropertyValue = "NOTAVAILABLE")]
        public static UnitStatusData NotAvailable;

        #endregion
    }
}
