﻿using NHibernate.Mapping.Attributes;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(UnitOfficerAssignHistoryData), Table = "UNIT_OFFICER_ASSIGN_HISTORY", Lazy = false, BatchSize = 1000, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("UK_START", Value = "UK_UNIT_OFFICER_ASSIGN")]
    [AttributeIdentifier("UK_END", Value = "UK_UNIT_OFFICER_ASSIGN")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_UNIT_OFFICER_ASSIGN")]
    [AttributeIdentifier("IX_START", Value = "IX_UNIT_OFFICER_ASSIGN_HISTORY_START")]
    public class UnitOfficerAssignHistoryData : BaseSessionHistory
    {
        #region Enums

        /// <summary>
        /// Enumerates the properties that this class has.
        /// </summary>
        public enum Properties
        {
            Officer,
            Unit,
            Start,
            End
        }

        #endregion

        #region Constructors
        public UnitOfficerAssignHistoryData()
        {
        }
        #endregion

        #region Properties
        [ManyToOne(0, ClassType = typeof(OfficerData), ForeignKey = "FK_OFFICER_OFFICER_ASSIGN_HISTORY_CODE", Cascade = "none", Fetch = FetchMode.Join)]
        [Column(1, Name = "OFFICER_CODE", NotNull = true, UniqueKey = "UK_UNIT_OFFICER_ASSIGN")]
        public virtual OfficerData Officer { get; set; }

        [ManyToOne(0, ClassType = typeof(UnitData), ForeignKey = "FK_UNIT_OFFICER_ASSIGN_UNIT_CODE", Cascade = "none", Fetch = FetchMode.Join)]
        [Column(1, Name = "UNIT_CODE", NotNull = true, UniqueKey = "UK_UNIT_OFFICER_ASSIGN")]
        public virtual UnitData Unit { get; set; }
        #endregion

        #region Resource
        [InitialData(PropertyName = "Name", PropertyValue = "UnitOfficerAssignHistoryData")]
        public static readonly UserResourceData Resource;
        #endregion

    }
}
