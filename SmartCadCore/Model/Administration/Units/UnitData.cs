﻿using Iesi.Collections;
using NHibernate.Mapping.Attributes;
using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(UnitData), Table = "UNIT", Lazy = false, BatchSize = 1000, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_UNIT_CUSTOM_CODE")]
    public class UnitData : ObjectData
    {
        #region Enums

        /// <summary>
        /// Enumerates the properties that this class has.
        /// </summary>
        public enum Properties
        {
            DepartmentType,
            Brand,
            Model,
            Year,
            Number,
            Type,
            Status,
            Description,
            Changed,
            Capacity,
            CustomCode,
            IncidentTypes,
            OfficerAssigns,
            IdRadio
        }

        public enum DEVICE_TYPE
        {
            GPS = 0,
            DVR = 1
        }
        #endregion

        #region Fields

        private DepartmentTypeData departmentType;
        private string brand;
        private string model;
        private int? year;
        private int? number;
        private UnitTypeData type;
        private UnitStatusData status;
        private string description;
        private bool? changed;
        private int? capacity;
        private string customCode;
        private ISet setIncidentTypes;
        private IList statusHistoryList;
        private ISet setOfficerAssigns;
        private string idRadio;
        private DispatchOrderData dispatchOrder;
        private TimeSpan eta;
        private double distance;
        private double? velocity;
        private DepartmentStationData departmentStation;
        private double lon;
        private double lat;
        private bool isSynchronized;
        private GPSData gps;
        private OperatorData dispatchOperator;
        private IList notes;
        private DateTime coordinatesDate;
        private IList alerts;
        private string idUnit;
        private string serial;
        private DEVICE_TYPE deviceType;

        #endregion

        #region Constructors

        public UnitData()
        {
        }

        public UnitData(
            DepartmentTypeData departmentType,
            int number,
            UnitTypeData type,
            UnitStatusData status,
            string description)
        {
            this.departmentType = departmentType;
            this.number = number;
            this.type = type;
            this.status = status;
            this.description = description;
        }

        #endregion

        #region Properties


        public virtual DepartmentTypeData DepartmentType
        {
            get
            {
                return departmentStation.DepartmentZone.DepartmentType;
            }
        }

        [Property(Column = "BRAND")]
        public virtual string Brand
        {
            get
            {
                return brand;
            }
            set
            {
                brand = value;
            }
        }

        [Property(Column = "MODEL")]
        public virtual string Model
        {
            get
            {
                return model;
            }
            set
            {
                model = value;
            }
        }

        [Property(Column = "YEAR", TypeType = typeof(int))]
        public virtual int? Year
        {
            get
            {
                return year;
            }
            set
            {
                year = value;
            }
        }

        [Property(Column = "NUMBER", TypeType = typeof(int))]
        public virtual int? Number
        {
            get
            {
                return number;
            }
            set
            {
                number = value;
            }
        }

        [ManyToOne(ClassType = typeof(UnitTypeData), Column = "UNIT_TYPE_CODE",
            ForeignKey = "FK_UNIT_UNIT_TYPE_CODE", Cascade = "none", Fetch = FetchMode.Join)]
        public virtual UnitTypeData Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value;
            }
        }

        [ManyToOne(ClassType = typeof(UnitStatusData), Column = "UNIT_STATUS_CODE",
            ForeignKey = "FK_UNIT_UNIT_STATUS_CODE", Cascade = "none", Fetch = FetchMode.Join)]
        public virtual UnitStatusData Status
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
            }
        }

        [Property(Column = "DESCRIPTION", NotNull = true)]
        public virtual string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }

        [Property(Column = "CAPACITY", NotNull = true, TypeType = typeof(int))]
        public virtual int? Capacity
        {
            get
            {
                return capacity;
            }
            set
            {
                capacity = value;
            }
        }

        [Property(0, Unique = true)]
        [Column(1, Name = "CUSTOM_CODE", UniqueKey = "UK_UNIT_CUSTOM_CODE", NotNull = true)]
        public virtual string CustomCode
        {
            get
            {
                return customCode;
            }
            set
            {
                customCode = value;
            }
        }

        [Set(0, Name = "SetIncidentTypes", Table = "UNIT_INCIDENT_TYPE", Cascade = "none", Lazy = CollectionLazy.True, Fetch = CollectionFetchMode.Subselect, BatchSize = 5)]
        [Key(1, Column = "UNIT_CODE", ForeignKey = "FK_UNIT_INCIDENT_TYPE_CODE")]
        [ManyToMany(2, ClassType = typeof(IncidentTypeData), Column = "INCIDENT_TYPE_CODE", ForeignKey = "FK_INCIDENT_TYPE_UNIT_CODE")]
        public ISet SetIncidentTypes
        {
            get
            {
                return setIncidentTypes;
            }
            set
            {
                setIncidentTypes = value;
            }
        }

        [Set(0, Name = "SetOfficerAssigns", Table = "UNIT_OFFICER_ASSIGN_HISTORY", Lazy = CollectionLazy.True,
           Cascade = "all-delete-orphan", Inverse = true, Where = "START_DATE <= getdate() and END_DATE > getdate() and DELETED_ID IS NULL",
           Fetch = CollectionFetchMode.Subselect)]
        [Key(1, Column = "UNIT_CODE"/*, ForeignKey = "FK_UNIT_OFFICER_ASSIGN_HISTORY_CODE"*/)]
        [OneToMany(2, ClassType = typeof(UnitOfficerAssignHistoryData))]
        public ISet SetOfficerAssigns
        {
            get
            {
                return setOfficerAssigns;
            }
            set
            {
                setOfficerAssigns = value;
            }
        }

        [Bag(0, Name = "StatusHistoryList", Table = "UNIT_STATUS_HISTORY",
            Cascade = "save-update", Lazy = CollectionLazy.True, Fetch = CollectionFetchMode.Subselect)]
        [Key(1, Column = "UNIT_CODE"/*, ForeignKey = "FK_UNIT_STATUS_HISTORY_UNIT_CODE"*/)]
        [OneToMany(3, ClassType = typeof(UnitStatusHistoryData))]
        public virtual IList StatusHistoryList
        {
            get
            {
                return statusHistoryList;
            }
            set
            {
                statusHistoryList = value;
            }
        }

        [Property(Column = "ID_RADIO")]
        public virtual string IdRadio
        {
            get
            {
                return idRadio;
            }
            set
            {
                idRadio = value;
            }
        }


        public DispatchOrderData DispatchOrder
        {
            get
            {
                return dispatchOrder;
            }
            set
            {
                dispatchOrder = value;
            }
        }

        public TimeSpan ETA
        {
            get
            {
                return eta;
            }

            set
            {
                eta = value;
            }
        }

        /// <summary>
        /// Velocidad promedio en Km/h
        /// </summary>
        [Property(Column = "VELOCITY", NotNull = true, TypeType = typeof(double))]
        public double? Velocity
        {
            get
            {
                return velocity;
            }
            set
            {
                velocity = value;
            }
        }

        public double Distance
        {
            get
            {
                return distance;
            }
            set
            {
                distance = value;
            }
        }

        [ManyToOne(ClassType = typeof(DepartmentStationData), Column = "DEPARTMENT_STATION_CODE",
           ForeignKey = "FK_DEPARTMENT_STATION_UNIT_CODE", Cascade = "none", Fetch = FetchMode.Join)]
        public virtual DepartmentStationData DepartmentStation
        {
            get
            {
                return departmentStation;
            }
            set
            {
                departmentStation = value;
            }
        }

        [ManyToOne(ForeignKey = "FK_UNIT_GPS_CODE", ClassType = typeof(GPSData),
            Cascade = "none", Column = "GPS_CODE",
            NotNull = false, Fetch = FetchMode.Join)]
        public GPSData GPS
        {
            get
            {
                return gps;
            }
            set
            {
                gps = value;
            }
        }


        [Property(Column = "DEVICE_TYPE")]
        public DEVICE_TYPE DeviceType
        {
            get
            {
                return deviceType;
            }
            set
            {
                deviceType = value;
            }
        }

        [Property(Column = "ID_UNIT")]
        public string IdUnit
        {
            get
            {
                return idUnit;
            }
            set
            {
                idUnit = value;
            }
        }

        [Property(Column = "SERIAL")]
        public string Serial
        {
            get
            {
                return serial;
            }
            set
            {
                serial = value;
            }
        }

        public OperatorData DispatchOperator
        {
            get
            {
                return dispatchOperator;
            }
            set
            {
                dispatchOperator = value;
            }
        }

        [Bag(0, Name = "Notes", Table = "UNIT_NOTE", Cascade = "save-update",
           Lazy = CollectionLazy.True, Fetch = CollectionFetchMode.Subselect)]
        [Key(1, Column = "UNIT_CODE"/*, ForeignKey = "FK_UNIT_UNIT_NOTE_CODE"*/)]
        [OneToMany(2, ClassType = typeof(UnitNoteData))]
        public virtual IList Notes
        {
            get
            {
                return notes;
            }
            set
            {
                notes = value;
            }
        }

        [Bag(0, Name = "Alerts", Table = "UNIT_ALERT", Cascade = "none",
                 Lazy = CollectionLazy.True, Fetch = CollectionFetchMode.Subselect, Where = "DELETED_ID IS NULL")]
        [Key(1, Column = "UNIT_CODE"/*, ForeignKey = "FK_UNIT_ALERT_UNIT_CODE"*/)]
        [OneToMany(2, ClassType = typeof(UnitAlertData))]
        public IList Alerts
        {
            get
            {
                return alerts;
            }
            set
            {
                alerts = value;
            }
        }

        [Bag(0, Name = "WorkShiftRoutes", Table = "WORK_SHIFT_ROUTE", Cascade = "save-update", Lazy = CollectionLazy.True, Fetch = CollectionFetchMode.Subselect)]
        [Key(1, Column = "UNIT_CODE"/*, ForeignKey = "FK_WORK_SHIFT_ROUTE_UNIT_CODE"*/)]
        [OneToMany(2, ClassType = typeof(WorkShiftRouteData))]
        public IList WorkShiftRoutes
        {
            get;
            set;
        }

        #endregion

        #region Overrides section
        public override string ToString()
        {
            string text = "";
            text += this.CustomCode + " - " + this.Description;
            return text;
        }

        #endregion

        #region Initial data

        [InitialData(PropertyName = "Name", PropertyValue = "UnitData")]
        public static readonly UserResourceData Resource;

        #endregion

        public string ts()
        {
            string s = customCode;

            s += "\nInType\n";
            foreach (IncidentTypeData item in setIncidentTypes)
            {
                s += item.CustomCode.ToString() + ", ";
            }
            s += "\nOfficers\n";
            foreach (UnitOfficerAssignHistoryData item in setOfficerAssigns)
            {
                s += item.Officer.ToString() + ", ";
            }
            return s;
        }
    }
}
