﻿using Iesi.Collections;
using NHibernate.Mapping.Attributes;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(RouteData), Table = "ROUTE", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_ROUTE_NAME")]
    public class RouteData : ObjectData, INamedObjectData
    {
        public enum RouteType
        {
            Track = 0,
            Area = 1
        }

        #region Constructors

        public RouteData()
        {
        }

        #endregion

        #region Properties

        [Property(Column = "CUSTOM_CODE")]
        public virtual string CustomCode { get; set; }

        [Property(0, Unique = true)]
        [Column(1, Name = "NAME", UniqueKey = "UK_ROUTE_NAME", NotNull = true)]
        public virtual string Name { get; set; }

        [Property(0, Type = "int", Unique = false)]
        [Column(1, Name = "TYPE", NotNull = true)]
        public virtual RouteType Type { get; set; }

        [Property(0, Unique = false)]
        [Column(1, Name = "IMAGE")]
        public virtual string Image { get; set; }

        [Property(0, Unique = false)]
        [Column(1, Name = "STOP_TIME", NotNull = true)]
        public virtual int StopTime { get; set; }

        [Property(0, Unique = false)]
        [Column(1, Name = "STOP_TIME_TOL", NotNull = true)]
        public virtual int StopTimeTol { get; set; }

        [ManyToOne(0, ClassType = typeof(DepartmentTypeData),
            ForeignKey = "FK_ROUTE_DEPARTMENT_TYPE_CODE", Cascade = "none")]
        [Column(1, Name = "DEPARTMENT_TYPE_CODE", NotNull = true,
            UniqueKey = "UK_ROUTE_NAME")]
        public virtual DepartmentTypeData DepartmentType { get; set; }

        [Set(0, Name = "RouteAddress", Table = "ROUTE_ADDRESS", Cascade = "save-update",
           Lazy = CollectionLazy.True, Fetch = CollectionFetchMode.Subselect, Inverse = true, Where = "DELETED_ID IS NULL", OrderBy = "POINT_NUMBER")]
        [Key(1, Column = "ROUTE_CODE"/*, ForeignKey = "FK_ROUTE_ADDRESS_CODE"*/)]
        [OneToMany(2, ClassType = typeof(RouteAddressData))]
        public ISet RouteAddress { get; set; }

        #endregion

        public override string ToString()
        {
            return this.Name;
        }
        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is RouteData)
            {
                result = this.Code == ((RouteData)obj).Code;
            }
            return result;
        }

        #region Initial data

        [InitialData(PropertyName = "Name", PropertyValue = "RouteData")]
        public static readonly UserResourceData Resource;

        #endregion
    }
}
