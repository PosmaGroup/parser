﻿using NHibernate.Mapping.Attributes;
using System;
using System.Collections;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ColorBase = System.Drawing.Color;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(UnitTypeData), Table = "UNIT_TYPE", Lazy = false, BatchSize = 1000, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_UNIT_TYPE_NAME")]
    public class UnitTypeData : ObjectData
    {

        #region Enums
        /// <summary>
        /// Enumerates the properties that this class has.
        /// </summary>
        public enum Properties
        {
            Name,
            Color,
            Image,
            Capacity,
            Description,
            DepartmentType,
            IncidentTypes
        }
        #endregion

        #region Fields

        private string name;
        private Color color;
        private string image;
        private int? capacity;
        private string description;
        private DepartmentTypeData departmentType;
        private IList incidentTypes;
        private double? velocity;


        private ComponentColorData componentColor;

        #endregion

        #region Constructors

        public UnitTypeData()
        {
        }

        public UnitTypeData(string name, Color color, string image)
        {
            this.name = name;
            this.color = color;
            this.image = image;
        }

        #endregion

        #region Properties

        [Property(0, Unique = true)]
        [Column(1, Name = "NAME", NotNull = true, UniqueKey = "UK_UNIT_TYPE_NAME")]
        public virtual string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        //[Property(Column = "COLOR", TypeType = typeof(Color))]
        public virtual Color Color
        {
            get
            {
                if (ComponentColor != null)
                    return ColorBase.FromArgb(ComponentColor.ColorA.Value, ComponentColor.ColorR.Value, ComponentColor.ColorG.Value, ComponentColor.ColorB.Value);
                else
                    //return ColorBase.FromArgb(255, 255, 255, 255);
                    return Color.White;
            }
            set
            {
                componentColor = new ComponentColorData(value);
            }
        }

        [ComponentProperty(ComponentType = typeof(ComponentColorData))]
        public ComponentColorData ComponentColor
        {
            get
            {
                return componentColor;
            }
            set
            {
                componentColor = value;
                color = ColorBase.FromArgb(value.ColorA.Value, value.ColorR.Value, value.ColorG.Value, value.ColorB.Value);
            }
        }

        [Property(Column = "IMAGE")]
        public virtual string Image
        {
            get
            {
                return image;
            }
            set
            {
                image = value;
            }
        }

        [Property(Column = "CAPACITY", TypeType = typeof(int))]
        public virtual int? Capacity
        {
            get
            {
                return capacity;
            }
            set
            {
                capacity = value;
            }
        }

        [Property(Column = "DESCRIPTION")]
        public virtual string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }

        [Bag(0, Name = "IncidentTypes", Table = "UNIT_TYPE_INCIDENT_TYPE", Cascade = "none", Lazy = CollectionLazy.True, Fetch = CollectionFetchMode.Subselect)]
        [Key(1, Column = "UNIT_TYPE_CODE", ForeignKey = "FK_UNIT_TYPE_CODE")]
        [ManyToMany(2, ClassType = typeof(IncidentTypeData), Column = "UNIT_TYPE_INCIDENT_CODE", ForeignKey = "FK_UNIT_TYPE_INCIDENT_TYPE_CODE")]
        public virtual IList IncidentTypes
        {
            get
            {
                return incidentTypes;
            }
            set
            {
                incidentTypes = value;
            }
        }

        [ManyToOne(0, ClassType = typeof(DepartmentTypeData), Cascade = "none", ForeignKey = "FK_UNIT_TYPE_DEPARTMENT_TYPE_CODE", Fetch = FetchMode.Join)]
        [Column(1, Name = "DEPARTMENT_TYPE_CODE", UniqueKey = "UK_UNIT_TYPE_NAME")]
        public virtual DepartmentTypeData DepartmentType
        {
            get
            {
                return departmentType;
            }
            set
            {
                departmentType = value;
            }
        }

        /// <summary>
        /// Velocidad promedio en Km/h
        /// </summary>
        [Property(Column = "VELOCITY", NotNull = true, TypeType = typeof(double))]
        public double? Velocity
        {
            get
            {
                return velocity;
            }
            set
            {
                velocity = value;
            }
        }
        #endregion

        [InitialData(PropertyName = "Name", PropertyValue = "UnitTypeData")]
        public static readonly UserResourceData Resource;

        public override string ToString()
        {
            string text = "";
            if (this.Name != null)
            {
                text = this.Name;
            }
            return text;
        }
    }
}
