﻿using NHibernate.Mapping.Attributes;
using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(OfficerData), Table = "OFFICER", Lazy = false, BatchSize = 1000, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_OFFICER_PERSON_ID")]
    [AttributeIdentifier("DELETED_ID_2", Value = "UK_OFFICER_BADGE")]
    public class OfficerData : ObjectData
    {
        #region Enums

        /// <summary>
        /// Enumerates the properties that this class has.
        /// </summary>
        public enum Properties
        {
            FirstName,
            LastName,
            PersonId,
            Birthday,
            Badge,
            Telephone,
            Rank,
            Position
        }

        #endregion

        public OfficerData()
        {
        }

        #region Properties

        [Property(Column = "FIRSTNAME")]
        public virtual string FirstName { get; set; }

        [Property(Column = "LASTNAME")]
        public virtual string LastName { get; set; }

        [Property(0, Unique = true)]
        [Column(1, Name = "PERSON_ID", UniqueKey = "UK_OFFICER_PERSON_ID", NotNull = true)]
        public virtual string PersonId { get; set; }

        [Property(0, TypeType = typeof(DateTime))]
        [Column(1, Name = "BIRTHDAY", NotNull = true)]
        public virtual DateTime? Birthday { get; set; }

        [Property(0, Unique = true)]
        [Column(1, Name = "BADGE", UniqueKey = "UK_OFFICER_BADGE", NotNull = true)]
        public virtual string Badge { get; set; }

        [Property(Column = "TELEPHONE")]
        public virtual string Telephone { get; set; }

        [ManyToOne(ClassType = typeof(RankData), Column = "RANK_CODE",
            ForeignKey = "FK_OFFICER_RANK_CODE", Cascade = "none", Fetch = FetchMode.Join)]
        public virtual RankData Rank { get; set; }

        public virtual DepartmentTypeData Department
        {
            get
            {
                if (DepartmentStation != null && DepartmentStation.DepartmentZone != null)
                    return DepartmentStation.DepartmentZone.DepartmentType;
                else
                    return null;
            }
        }

        [ManyToOne(ClassType = typeof(PositionData), Column = "POSITION_CODE",
            ForeignKey = "FK_OFFICER_POSITION_CODE", Cascade = "none", Fetch = FetchMode.Join)]
        public virtual PositionData Position { get; set; }

        [Bag(0, Name = "UnitsAssigned", Table = "UNIT_OFFICER_ASSIGN_HISTORY",
           Cascade = "all-delete-orphan", Lazy = CollectionLazy.True, Inverse = true, Fetch = CollectionFetchMode.Subselect)]
        [Key(1, Column = "OFFICER_CODE"/*, ForeignKey = "FK_OFFICER_OFFICER_ASSIGN_HISTORY_CODE"*/)]
        [OneToMany(2, ClassType = typeof(UnitOfficerAssignHistoryData))]
        public virtual IList UnitsAssigned { get; set; }

        [ManyToOne(ClassType = typeof(DepartmentStationData), Column = "DEPARTMENT_STATION_CODE",
           ForeignKey = "FK_DEPARTMENT_STATION_OFFICER_CODE", Cascade = "none", Fetch = FetchMode.Join)]
        public virtual DepartmentStationData DepartmentStation { get; set; }


        #endregion

        [InitialData(PropertyName = "Name", PropertyValue = "OfficerData")]
        public static readonly UserResourceData Resource;

        public override string ToString()
        {
            string text = "";
            if (this.FirstName != null)
                text += this.FirstName;
            if (this.LastName != null)
            {
                if (this.FirstName != null)
                    text += " ";
                text += this.LastName;
            }
            return text;
        }
    }
}
