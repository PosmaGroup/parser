﻿using NHibernate.Mapping.Attributes;
using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    [Serializable]
    [JoinedSubclass(Table = "GPS", NameType = typeof(GPSData), BatchSize = 1000, ExtendsType = typeof(DeviceData), Lazy = false)]
    public class GPSData : DeviceData
    {
        [Key(0, Column = "PARENT_CODE", ForeignKey = "FK_GPS_DEVICE_CODE")]
        public int ParentCode
        {
            get;
            set;
        }

        [ManyToOne(Column = "GPS_TYPE", ClassType = typeof(GPSTypeData),
         ForeignKey = "FK_GPS_GPS_TYPE", NotNull = true, Cascade = "none", Fetch = FetchMode.Join)]
        public GPSTypeData Type
        {
            get;
            set;
        }

        [Bag(0, Name = "Sensors", Table = "GPS_PIN_SENSOR", Cascade = "save-update",
                 Lazy = CollectionLazy.True, Fetch = CollectionFetchMode.Subselect, Where = "DELETED_ID IS NULL")]
        [Key(1, Column = "PARENT_CODE", ForeignKey = "FK_GPS_GPS_PIN_SENSOR")]
        [OneToMany(2, ClassType = typeof(GPSPinSensorData))]
        public IList Sensors
        {
            get;
            set;
        }

        public UnitData Unit
        {
            get;
            set;
        }

        [Property(Column = "LONGITUDE", Index = "IX_GPS_LONGITUDE")]
        public double Longitude
        {
            get;
            set;
        }

        [Property(Column = "LATITUDE", Index = "IX_GPS_LATITUDE")]
        public double Latitude
        {
            get;
            set;
        }

        [Property(Column = "ALTITUDE")]
        public int Altitude
        {
            get;
            set;
        }

        [Property(Column = "HEADING")]
        public double Heading
        {
            get;
            set;
        }

        [Property(Column = "SPEED")]
        public double Speed
        {
            get;
            set;
        }

        [Property(Column = "SATELLITES")]
        public int Satellites
        {
            get;
            set;
        }

        [Property(Column = "COORDINATES_DATE", NotNull = true)]
        public DateTime CoordinatesDate
        {
            get;
            set;
        }

        [Property(Column = "SYNCHRONIZED")]
        public bool IsSynchronized
        {
            get;
            set;
        }


        [InitialData(PropertyName = "Name", PropertyValue = "GPSData")]
        public static readonly UserResourceData Resource;

        public override string ToString()
        {
            return this.Name;
        }

        public override bool Equals(object obj)
        {
            try
            {
                if (obj.GetType() == typeof(GPSData))
                {
                    GPSData gps = (GPSData)obj;
                    if (this.Name == gps.Name)
                    {
                        return true;
                    }
                }
            }
            catch { }
            return false;
        }
    }
}
