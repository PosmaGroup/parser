﻿using NHibernate.Mapping.Attributes;
using System;

using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(GPSPinData), Table = "GPS_PIN", Lazy = false, Where = "DELETED_ID IS NULL")]
    public class GPSPinData : ObjectData
    {
        private int number;
        private PinType type;
        private GPSTypeData gpsType;


        public enum PinType
        {
            Input = 0,
            Output = 1,
            Input_Output = 2
        }

        [Property(0, Column = "NUMBER", NotNull = true)]
        public int Number
        {
            get
            {
                return number;
            }
            set
            {
                number = value;
            }
        }

        [Property(1, Column = "TYPE", NotNull = true)]
        public PinType Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value;
            }
        }

        [ManyToOne(ClassType = typeof(GPSTypeData), Column = "GPS_TYPE_CODE",
          Cascade = "none", ForeignKey = "FK_GPS_PIN_GPS_TYPE_CODE")]
        public GPSTypeData GPSType
        {
            get
            {
                return gpsType;
            }
            set
            {
                gpsType = value;
            }

        }

    }
}
