﻿using System;
using System.Linq;
using System.Text;
using NHibernate.Mapping.Attributes;

namespace SmartCadCore.Model
{
    /// <summary>
    /// This class is used to store information about all parked calls put from First Level application
    /// </summary>
    [Serializable]
    [Class(NameType = typeof(ParkedCallData), Table = "PARKED_CALL", Lazy = false, BatchSize = 1000, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_PARKED_CALL_PHONE_REPORT_CUSTOM_CODE")]
    public class ParkedCallData : ObjectData
    {
        #region Properties
        
        /// <summary>
        /// First Level Operator who put in parked state the received call
        /// </summary>
        [ManyToOne(0, ClassType = typeof(OperatorData),
            ForeignKey = "FK_PARKED_CALL_OPERATOR_WHO_PARKED_CODE", Cascade = "none", Fetch = FetchMode.Join)]
        [Column(1, Name = "OPERATOR_WHO_PARKED_CODE", NotNull = true)]
        public OperatorData OperatorWhoParked { get; set; }
        /// <summary>
        /// Date when the received call in First Level App was parked
        /// </summary>
        [Property(Column = "DATE_WHEN_PARKED", TypeType = typeof(DateTime))]
        public DateTime DateWhenParked { get; set; }
        /// <summary>
        /// Dispatch Operator who put in unparked the call
        /// </summary>
        [ManyToOne(0, ClassType = typeof(OperatorData),
            ForeignKey = "FK_PARKED_CALL_OPERATOR_WHO_UNPARKED_CODE", Cascade = "none", Fetch = FetchMode.Join)]
        [Column(1, Name = "OPERATOR_WHO_UNPARKED_CODE", NotNull = false)]
        public OperatorData OperatorWhoUnParked { get; set; }
        /// <summary>
        /// Login of operator who unparked the call. This is needed for workaround in Genesys to
        /// realize parking operation
        /// </summary>
        [Property(Column = "LOGIN_TO_UNPARK")]
        public string AgentLoginToUnPark { get; set; }
        /// <summary>
        /// Date when tha call was unparked by a dispatcher
        /// </summary>
        [Property(Column = "DATE_WHEN_UNPARKED", TypeType = typeof(DateTime))]
        public DateTime DateWhenUnParked { get; set; }
        /// <summary>
        /// This code correspond to Phone report code and is needed for workaround in Genesys to
        /// realize parking operation
        /// </summary>
        [Property(0, Unique = true)]
        [Column(1, Name = "PHONE_REPORT_CUSTOM_CODE", UniqueKey = "UK_PARKED_CALL_PHONE_REPORT_CUSTOM_CODE", NotNull = true)]
        public string PhoneReportCustomCode { get; set; }
        /// <summary>
        /// ANI received in incoming call
        /// </summary>
        [Property(Column = "ANI_RECEIVED")]
        public string ANIReceived { get; set; }

        #endregion
        
        
        #region Overrides

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(ANIReceived).Append(" ").Append(PhoneReportCustomCode);
            return sb.ToString();
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is ParkedCallData)
            {
                result = this.Code == ((ParkedCallData)obj).Code;
            }
            return result;
        }

        #endregion
    }
}
