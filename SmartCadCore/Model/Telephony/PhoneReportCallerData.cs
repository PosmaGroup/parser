using System;
using System.Text;

using NHibernate.Mapping.Attributes;

namespace SmartCadCore.Model
{
    #region Class PhoneReportCallerData Documentation
    /// <summary>
    /// This class represents the caller information of the phone report
    /// </summary>
    /// <className>PhoneReportCallerData</className>
    /// <author email="fsilva@smartmatic.com">Fernando Silva</author>
    /// <date>2007/01/30</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    [Serializable]
    //[Class(Table = "PHONE_REPORT_CALLER", Lazy = false, Where = "DELETED_ID IS NULL")]
    [Component(/*Insert = true, Update = true*/)]
    public class PhoneReportCallerData
    {
        #region Fields

        private string name;
        
        private string telephone;

        private string additionalNumber;

        private PhoneReportCallerAddressData address;

        private bool? anonymous;
        
        private PhoneReportData phoneReport;

        #endregion

        #region Constructors

        public PhoneReportCallerData()
        {
        }

        public PhoneReportCallerData(
            string name,
            string telephone,
            PhoneReportCallerAddressData address)
        {
            this.name = name;
            this.telephone = telephone;
            this.address = address;
        }

        public PhoneReportCallerData(
            string name, 
            string telephone,
            PhoneReportCallerAddressData address,
            bool anonymous) : this(name, telephone, address)
        {
            this.anonymous = anonymous;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Name of the caller
        /// </summary>
        [Property(0)]
        [Column(1, Name = "CALLER_NAME"/*, NotNull = true*/)]
        public virtual string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        /// <summary>
        /// Address of the caller
        /// </summary>
        [ComponentProperty(ComponentType = typeof(PhoneReportCallerAddressData))]
        public virtual PhoneReportCallerAddressData Address
        {
            get
            {
                return address;
            }
            set
            {
                address = value;
            }
        }

        /// <summary>
        /// Telephone of the caller
        /// </summary>
        [Property(Column = "CALLER_TELEPHONE")]
        public virtual string Telephone
        {
            get
            {
                return telephone;
            }
            set
            {
                telephone = value;
            }
        }

        [Property(Column = "CALLER_ANONYMOUS", TypeType = typeof(bool))]
        public virtual bool? Anonymous
        {
            get
            {
                return anonymous;
            }
            set
            {
                anonymous = value;
            }
        }

        /// <summary>
        /// Additional Telephone number of the caller
        /// </summary>
        [Property(Column = "CALLER_ADDITIONAL_TELEPHONE")]
        public virtual string AdditionalNumber
        {
            get
            {
                return this.additionalNumber;
            }
            set
            {
                additionalNumber = value;
            }
        }

        #endregion    
    }
}
