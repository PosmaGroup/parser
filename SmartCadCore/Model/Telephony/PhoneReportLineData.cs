using System;
using System.Text;

using NHibernate.Mapping.Attributes;

namespace SmartCadCore.Model
{
    #region Class PhoneReportLineData Documentation
    /// <summary>
    /// This class represents the phone line of a phone report
    /// </summary>
    /// <className>PhoneReportLineData</className>
    /// <author email="fsilva@smartmatic.com">Fernando Silva</author>
    /// <date>2007/01/30</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    [Serializable]
    //[Class(Table = "PHONE_REPORT_LINE", Lazy = false, Where = "DELETED_ID IS NULL")]
    [Component(/*Insert = true, Update = true*/)]
    public class PhoneReportLineData
    {
        #region Fields

        private string name;
        private string telephone;
        private PhoneReportLineAddressData address;

        #endregion

        #region Constructors

        public PhoneReportLineData()
        {
        }

        public PhoneReportLineData(string name, string telephone, PhoneReportLineAddressData address)
        {
            this.name = name;
            this.telephone = telephone;
            this.address = address;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Owner's name of the phone line calling
        /// </summary>
        [Property(0)]
        [Column(1, Name = "LINE_NAME"/*, NotNull = true*/)]
        public virtual string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        /// <summary>
        /// Telephone number of the phone line calling
        /// </summary>
        [Property(0)]
        [Column(1, Name = "LINE_TELEPHONE")]
        public virtual string Telephone
        {
            get
            {
                return telephone;
            }
            set
            {
                telephone = value;
            }
        }

        /// <summary>
        /// Address of the phone line calling
        /// </summary>
        [ComponentProperty(ComponentType = typeof(PhoneReportLineAddressData))]
        public virtual PhoneReportLineAddressData Address
        {
            get
            {
                return address;
            }
            set
            {
                address = value;
            }
        }

        #endregion
    }
}
