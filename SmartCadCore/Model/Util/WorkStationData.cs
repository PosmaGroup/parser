﻿using NHibernate.Mapping.Attributes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model.Util
{
    [Serializable]
    [Class(NameType = typeof(WorkStationData), Table = "WORKSTATION", Lazy = false, BatchSize = 1000)]
    public class WorkStationData : ObjectData, INamedObjectData
    {

        private IList installedApps;
        
        public WorkStationData()
        {
        }

        #region Properties
        [Property(Column = "NAME")]
        public string Name { get; set; }

        [Property(Column = "DATE")]
        public virtual DateTime DateCreation { get; set; }
        #endregion

        [InitialData(PropertyName = "Name", PropertyValue = "WorkStationData")]
        public static readonly UserResourceData Resource;

        public override string ToString()
        {
            string text = "";

            if (this.Name != null)
                text += this.Name;

            return text;
        }


        [Bag(0, Name = "InstalledApps", Table = "WORKSTATION_APPLICATION", Cascade = "none", Lazy = CollectionLazy.True, Fetch = CollectionFetchMode.Subselect)]
        [Key(1, Column = "WORKSTATION_CODE", ForeignKey = "FK_WORKSTATION")]
        [ManyToMany(2, ClassType = typeof(InstalledApplicationData), Column = "INSTALLEDAPPLICACTION_CODE", ForeignKey = "FK_INSTALLEDAPPLICACTION")]
        public virtual IList InstalledApps
        {
            get
            {
                return installedApps;
            }
            set
            {
                installedApps = value;
            }
        }

    }

}
