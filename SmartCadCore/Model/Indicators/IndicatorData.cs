using System;
using System.Text;
using NHibernate.Mapping.Attributes;
using Iesi.Collections;

namespace SmartCadCore.Model
{
    public enum IndicatorPosition
    {
        Up,
        Down
    }

    [Serializable]
    [Class(NameType = typeof(IndicatorData), Table = "INDICATOR", Lazy = false, Where = "DELETED_ID IS NULL")]
    //[AttributeIdentifier("DELETED_ID_1", Value = "UK_INDICATOR_NAME")]
    public class IndicatorData : ObjectData, INamedObjectData
    {
        private string name;
        private string mnemonic;
        private string description;
        private string measureUnit;
        private bool dashboard;
        private ISet classes;
        private IndicatorTypeData type;
        private int frecuency;
        private IndicatorForecastData forecast;
        private IndicatorForecastData general;
        private string customCode;
        private IndicatorPosition indicatorLocation;
        
        [Property(0, TypeType=typeof(int), Column  = "INDICATOR_LOCATION", NotNull = true)]
        public IndicatorPosition IndicatorLocation
        {
            get { return indicatorLocation; }
            set { indicatorLocation = value; }
        }

        [Property(0, Unique = true)]
        [Column(1, Name = "NAME", NotNull = true)]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        [Property(0, Unique = true)]
        [Column(1, Name = "CUSTOM_CODE", NotNull = true)]//, UniqueKey = "UK_INDICATOR_CUSTOM_CODE")]
        public string CustomCode
        {
            get { return customCode; }
            set { customCode = value; }
        }

        [Property(0, Unique = true)]
        [Column(1, Name = "MNEMONIC")]
        public string Mnemonic
        {
            get { return mnemonic; }
            set { mnemonic = value; }
        }

        [Property(0, Column = "DESCRIPTION")]
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        [Property(0, Column = "MEASURE_UNIT")]
        public string MeasureUnit
        {
            get { return measureUnit; }
            set { measureUnit = value; }
        }

        [Property(0, Column = "DASHBOARD")]
        public bool Dashboard
        {
            get { return dashboard; }
            set { dashboard = value; }
        }

        [Set(0, Cascade = "all-delete-orphan", Lazy = CollectionLazy.True, Fetch = CollectionFetchMode.Subselect)]
        [Key(1, Column = "INDICATOR_CODE"/*, ForeignKey = "FK_INDICATOR_INDICATOR_CLASS_DASHBOARD_CODE"*/)]
        [OneToMany(2, ClassType = typeof(IndicatorClassDashboardData))]
        public ISet Classes
        {
            get { return classes; }
            set { classes = value; }
        }

        [ManyToOne(0, ClassType = typeof(IndicatorTypeData), ForeignKey = "FK_INDICATOR_INDICATOR_TYPE_CODE", Cascade = "none")]
        [Column(1, Name = "INDICATOR_TYPE_CODE", NotNull = true)]
        public IndicatorTypeData Type
        {
            get { return type; }
            set { type = value; }
        }

        public IndicatorForecastData General
        {
            get { return general; }
            set { general = value; }
        }

        public IndicatorForecastData Forecast
        {
            get { return forecast; }
            set { forecast = value; }
        }

        public bool Threshold()
        {
            return general != null || forecast != null;
        }

        [Property(0)]
        [Column(1, Name = "FRECUENCY", NotNull = true)]
        public int Frecuency
        {
            get { return frecuency; }
            set { frecuency = value; }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is IndicatorData)
            {
                result = this.Name == ((IndicatorData)obj).Name;
            }
            return result;
        }
    }
}
