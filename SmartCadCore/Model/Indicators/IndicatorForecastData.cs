using System;
using System.Text;
using NHibernate.Mapping.Attributes;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(IndicatorForecastData), Table = "INDICATOR_FORECAST", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_INDICATOR_FORECAST")]
    public class IndicatorForecastData : ObjectData, IIndicatorThreshold
    {
        /*private string name;
        private bool general;*/
        private int low;
        private int high;
        private PositivePattern pattern;
        private IndicatorData indicator;

        /*private DateTime start;
        private DateTime end;*/
        private DepartmentTypeData departmentType;
        private IndicatorGroupForecastData indicatorGroupForecast;

        /*[Property(0, Unique = true)]
        [Column(1, Name = "NAME", UniqueKey = "UK_INDICATOR_FORECAST")]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        [Property(0, Unique = true)]
        [Column(1, Name = "GENERAL", NotNull = true, UniqueKey = "UK_INDICATOR_FORECAST")]
        public bool General
        {
            get { return general; }
            set { general = value; }
        }*/

        [Property(0)]
        [Column(1, Name = "LOW")]
        public int Low
        {
            get { return low; }
            set { low = value; }
        }

        [Property(0)]
        [Column(1, Name = "HIGH")]
        public int High
        {
            get { return high; }
            set { high = value; }
        }

        [Property(0, TypeType = typeof(PositivePattern))]
        [Column(1, Name = "PATTERN")]
        public PositivePattern Pattern
        {
            get { return pattern; }
            set { pattern = value; }
        }

        [ManyToOne(0, Unique = true, ClassType = typeof(IndicatorData), ForeignKey = "FK_INDICATOR_INDICATOR_FORECAST_CODE", Cascade = "none")]
        [Column(1, Name = "INDICATOR_CODE", NotNull = true, UniqueKey = "UK_INDICATOR_FORECAST")]
        public IndicatorData Indicator
        {
            get { return indicator; }
            set { indicator = value; }
        }

        [ManyToOne(0, ClassType = typeof(IndicatorGroupForecastData), ForeignKey = "FK_INDICATOR_GROUP_FORECAST_INDICATOR_FORECAST_CODE", Cascade = "none")]
        [Column(1, Name = "INDICATOR_GROUP_FORECAST_CODE", UniqueKey = "UK_INDICATOR_FORECAST")]
        public IndicatorGroupForecastData IndicatorGroupForecast
        {
            get { return indicatorGroupForecast; }
            set { indicatorGroupForecast = value; }
        }

        /*[Property(0, Unique = true, TypeType = typeof(DateTime))]
        [Column(1, Name = "START_TIME", NotNull = true, UniqueKey = "UK_INDICATOR_FORECAST")]
        public DateTime Start
        {
            get { return start; }
            set { start = value; }
        }

        [Property(0, Unique = true, TypeType = typeof(DateTime))]
        [Column(1, Name = "END_TIME", UniqueKey = "UK_INDICATOR_FORECAST")]
        public DateTime End
        {
            get { return end; }
            set { end = value; }
        }*/

        [ManyToOne(0, ClassType = typeof(DepartmentTypeData), ForeignKey = "FK_INDICATOR_FORECAST_DEPARTMENT_TYPE_CODE", Cascade = "none")]
        [Column(1, Name = "DEPARTMENT_TYPE_CODE")]
        public DepartmentTypeData DepartmentType
        {
            get { return departmentType; }
            set { departmentType = value; }
        }

        public bool Threshold()
        {
            return Low == 0 && High == 0;
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            IndicatorForecastData compare = (IndicatorForecastData)obj;
            if (obj is IndicatorForecastData)
            {
                
                if (DepartmentType == null)
                {
                    result = this.Indicator.CustomCode == compare.Indicator.CustomCode &&
                             this.IndicatorGroupForecast.Equals(compare.IndicatorGroupForecast);
                }
                else
                {
                    result = this.Indicator == compare.Indicator &&
                             this.IndicatorGroupForecast == compare.IndicatorGroupForecast &&
                             this.DepartmentType == compare.DepartmentType;
                }
            }
            return result;
        }
    }

    public enum PositivePattern
    {
        Increase = 1,
        Maintain = 0,
        Decrease = -1
    }
}
