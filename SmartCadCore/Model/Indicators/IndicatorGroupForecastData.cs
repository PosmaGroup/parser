using System;
using System.Text;
using NHibernate.Mapping.Attributes;
using System.Collections;
using Iesi.Collections;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(IndicatorGroupForecastData), Table = "INDICATOR_GROUP_FORECAST", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_INDICATOR_GROUP_FORECAST")]
    public class IndicatorGroupForecastData : ObjectData, INamedObjectData
    {
        private string name;
        private string description;
        private bool general;
        private DateTime start;
        private DateTime end;
        private IList forecastValues;
        private bool factory; 

        [Property(0, Unique = true)]
        [Column(1, Name = "NAME", UniqueKey = "UK_INDICATOR_GROUP_FORECAST")]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        [Property(0)]
        [Column(1, Name = "FACTORY_DATA")]
        public bool FactoryData 
        {
            get
            {
                return factory;
            }
            set
            {
                factory = value;
            }
        }

        [Property(0)]
        [Column(1, Name = "DESCRIPTION")]
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        [Property(0, Unique = true)]
        [Column(1, Name = "GENERAL", NotNull = true, UniqueKey = "UK_INDICATOR_GROUP_FORECAST")]
        public bool General
        {
            get { return general; }
            set { general = value; }
        }

        [Property(0, Unique = true, TypeType = typeof(DateTime))]
        [Column(1, Name = "START_TIME", NotNull  = true, UniqueKey = "UK_INDICATOR_GROUP_FORECAST")]
        public DateTime Start
        {
            get { return start; }
            set { start = value; }
        }

        [Property(0, Unique = true, TypeType = typeof(DateTime))]
        [Column(1, Name = "END_TIME", UniqueKey = "UK_INDICATOR_GROUP_FORECAST")]
        public DateTime End
        {
            get { return end; }
            set { end = value; }
        }

        [Bag(0, Name = "ForecastValues", Table = "INDICATOR_FORECAST", Cascade = "save-update",
          Lazy = CollectionLazy.True, Fetch = CollectionFetchMode.Subselect, Where = "DELETED_ID IS NULL")]
        [Key(1, Column = "INDICATOR_GROUP_FORECAST_CODE"/*, ForeignKey = "FK_INDICATOR_GROUP_FORECAST_INDICATOR_FORECAST_CODE"*/)]
        [OneToMany(2, ClassType = typeof(IndicatorForecastData))]
        public IList ForecastValues
        {
            get
            {
                return forecastValues;
            }
            set
            {
                forecastValues = value;
            }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is IndicatorGroupForecastData)
            { 
                IndicatorGroupForecastData data = (IndicatorGroupForecastData)obj;
                result = this.Name == data.Name /*&&
                         this.General == data.General &&
                         this.Start == data.Start &&
                         this.End == data.End*/;
            }
            return result;
        }
    }
}
