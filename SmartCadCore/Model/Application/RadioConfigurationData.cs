﻿using System;
using System.Linq;
using System.Text;
using NHibernate.Mapping.Attributes;


namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(RadioConfigurationData), Table = "RADIO_CONFIGURATION", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_RADIO_CONFIGURATION_EXTENSION")]
    public class RadioConfigurationData : ObjectData
    {
        #region Properties
        [Property(0, Unique = true)]
        [Column(1, Name = "EXTENSION", UniqueKey = "UK_RADIO_CONFIGURATION_EXTENSION", NotNull = true)]
        public int Extension { get; set; }
        
        [Property(0)]
        [Column(1, Name = "TALKCODE")]
        public string TalkCode { get; set; }

        [Property(0)]
        [Column(1, Name = "FRECUENCYCODE")]
        public string FrecuencyCode { get; set; }

        [Property(0)]
        [Column(1, Name = "DESCRIPTION")]
        public string Description { get; set; }

        [ManyToOne(0, ClassType = typeof(DepartmentTypeData),
            ForeignKey = "FK_RADIO_CONFIGURATION_DEPARTMENT_TYPE_CODE", Cascade = "none", Fetch = FetchMode.Join)]
        [Column(1, Name = "RADIO_CONFIGURATION_CODE", NotNull = false)]
        public DepartmentTypeData Department { get; set; }
        #endregion

        [InitialData(PropertyName = "Name", PropertyValue = "RadioConfigurationData")]
        public static readonly UserResourceData Resource;

        #region Overrides

        public override string ToString()
        {
            return this.Extension.ToString();
        }

        #endregion
    }
}
