﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    #region Class SearchablePropertyAttribute Documentation
    ///<summary>
    /// Clase para indicar si una propiedad de un objeto puede ser usado dentro
    /// de un criterio de búsqueda.
    ///</summary>
    ///<className>SearchablePropertyAttribute</className>
    ///<author email="jynojosa@smartmatic.com">Julio Ynojosa</author>
    ///<date>2006/04/04</date>
    ///<copyRight>Smartmatic copyright</copyRight>
    #endregion
    public class SearchablePropertyAttribute : Attribute
    {
        private bool isSearchable = false;

        public bool IsSearchable
        {
            get
            {
                return isSearchable;
            }

            set
            {
                isSearchable = value;
            }
        }

        public SearchablePropertyAttribute(bool isSearchable)
        {
            this.IsSearchable = isSearchable;
        }
    }
}
