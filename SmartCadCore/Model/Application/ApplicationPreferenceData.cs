using System;
using System.Text;
using NHibernate.Mapping.Attributes;

namespace SmartCadCore.Model
{
    #region Class ApplicationPreferenceData Documentation
    /// <summary>
    /// This class represents the application preferences
    /// </summary>
    /// <className>ApplicationPreferenceData</className>
    /// <author email="armando.torres@smartmatic.com">Armando Torres</author>
    /// <date>2007/03/07</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    [Serializable]
    [Class(NameType = typeof(ApplicationPreferenceData), Table = "APPLICATION_PREFERENCE", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_APPLICATION_PREFERENCE_NAME")]
    public class ApplicationPreferenceData : ObjectData
    {
        public enum Unit
        {
            SecondsUnit,
            WeightUnit,
            LogicalValueUnit,
            MillisecondsUnit,
            QuantityUnit,
            MinutesUnit,
            HoursUnit,
            DurationUnit,
            TextUnit,
            MetersUnit,
        }
        #region Constructors

        public ApplicationPreferenceData(
            string name,
            string type,
            string value,
            string module,
            string description,
            Unit measure, 
            string rangeCustomCode, 
            string minValue, 
            string maxValue)
        {
            this.name = name;
            this.type = type;
            this.value = value;
            this.module = module;
            this.description = description;
            this.measure = measure.ToString();
            this.valueRangeCustomCode = rangeCustomCode;
            this.MinValue = minValue;
            this.MaxValue = maxValue;
        }

        public ApplicationPreferenceData(){ }

        #endregion

        #region Fields

        private string name;
        private string type;
        private string value;
        private string module;
        private string description;
        private string measure;
        private string valueRangeCustomCode;
        private string minValue;
        private string maxValue;

        #endregion

        #region Properties

        [Property(0, Unique = true)]
        [Column(1, Name = "NAME", UniqueKey = "UK_APPLICATION_PREFERENCE_NAME", NotNull = true)]
        public string Name
        {
            get 
            {
                return name; 
            }
            set 
            {
                name = value; 
            }
        }

        [Property(0)]
        [Column(1, Name = "TYPE", NotNull = true)]
        public string Type
        {
            get 
            {
                return type; 
            }
            set 
            {
                type = value; 
            }
        }

        [Property(0)]
        [Column(1, Name = "VALUE", NotNull = true)]
        public string Value
        {
            get 
            {
                return this.value;
            }
            set
            {
                this.value = value;
            }
        }

        [Property(0)]
        [Column(1, Name = "MODULE")]
        public string Module
        {
            get 
            {
                return module; 
            }
            set 
            {
                module = value; 
            }
        }

        [Property(Column = "DECRIPTION", Length = 1024)]
        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }

        [Property(Column = "MEASURE_UNIT")]
        public string Measure
        {
            get
            {
                return measure;
            }
            set
            {
                measure = value;
            }
        }

        [Property(Column = "VALUE_RANGE_CUSTOM_CODE")]
        public string ValueRangeCustomCode
        {
            get
            {
                return valueRangeCustomCode;
            }
            set
            {
                valueRangeCustomCode = value;
            }
        }

        [Property(Column = "MIN_VALUE")]
        public string MinValue
        {
            get
            {
                return minValue;
            }
            set
            {
                minValue = value;
            }
        }

        [Property(Column = "MAX_VALUE")]
        public string MaxValue
        {
            get
            {
                return maxValue;
            }
            set
            {
                maxValue = value;
            }
        }
        #endregion

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is ApplicationPreferenceData)
            {
                result = this.Code == ((ApplicationPreferenceData)obj).Code;
            }
            return result;
        }

        #region Initial data

        [InitialData(PropertyName = "Name", PropertyValue = "ApplicationPreferenceData")]
        public static readonly UserResourceData Resource;

        #endregion
    }
}
