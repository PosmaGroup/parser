using System;
using System.Text;

namespace SmartCadCore.Model
{
    #region Interface INamedObjectData Documentation
    /// <summary>
    /// This interface must be implemented for that classes that owns a custom code
    /// that allows to uniquely identify an instance of this class.
    /// </summary>
    /// <className>INamedObjectData</className>
    /// <author email="jynojosa@smartmatic.com">Julio Ynojosa</author>
    /// <date>2006/12/01</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion 
    public interface INamedObjectData
    {
        string Name
        {
            get;
            set;
        }
    }
}
