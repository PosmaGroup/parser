using System;
using System.Text;
using NHibernate.Mapping.Attributes;

namespace SmartCadCore.Model
{
    [Class(NameType = typeof(DummyData), Table = "DUMMY", Lazy = false, Where = "DELETED_ID IS NULL")]
    public class DummyData : ObjectData
    {
        #region Constructors

        public DummyData()
        { }

        #endregion

        #region fields

        private string name;

        #endregion

        #region Properties

        [Property(0)]
        [Column(1, Name = "NAME", UniqueKey = "UK_DUMMY_NAME_CONTROLLER", NotNull = true)]
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        #endregion
    }
}
