using System;
using System.Text;

using NHibernate.Mapping.Attributes;

namespace SmartCadCore.Model
{
    #region Class UserAccessData Documentation
    /// <summary>
    /// This class represents accesses availables for a determined profile. This accesses
    /// are represented in the form: UserApplication + UserPermission. In other words,
    /// permissions availables on an application.
    /// </summary>
    /// <className>UserAccessData</className>
    /// <author email="jynojosa@smartmatic.com">Julio Ynojosa</author>
    /// <date>2006/12/01</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    [Serializable]
    [Class(NameType = typeof(UserAccessData), Table = "USER_ACCESS", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_USER_ACCESS_NAME")]
    public class UserAccessData : ObjectData, INamedObjectData
    {
        #region Enums

        /// <summary>
        /// Enumerates the properties that this class has.
        /// </summary>
        public enum Properties
        {
            Name,
            UserPermission,
            UserApplication
        }

        #endregion

        #region Fields
        
        private string name;

        private UserPermissionData userPermission;

        private UserApplicationData userApplication;

        #endregion

        #region Constructors

        public UserAccessData()
        {
        }

        public UserAccessData(string name, UserPermissionData userPermission,
            UserApplicationData userApplication)
        {
            this.Name = name;
            this.UserPermission = userPermission;
            this.UserApplication = userApplication;
        }

        #endregion
        
        #region Properties

        /// <summary>
        /// User access name.
        /// </summary>
        [Property(0, Unique = true)]
        [Column(1, Name = "NAME", UniqueKey = "UK_USER_ACCESS_NAME", NotNull = true)]
        public virtual string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }
        
        /// <summary>
        /// Application where this action applies.
        /// </summary>
        [ManyToOne(ClassType = typeof(UserApplicationData), Column = "USER_APPLICATION_CODE",
            ForeignKey = "FK_USER_ACCESS_APPLICATION_CODE", Cascade = "none")]
        public virtual UserApplicationData UserApplication
        {
            get 
            {
                return userApplication; 
            }
            set 
            { 
                userApplication = value; 
            }
        }

        /// <summary>
        /// Permission(UserResource - USerAction)
        /// </summary>
        [ManyToOne(ClassType = typeof(UserPermissionData), Column = "USER_PERMISSION_CODE",
            ForeignKey = "FK_USER_ACCESS_PERMISSION_CODE", Cascade = "none")]
        public virtual UserPermissionData UserPermission
        {
            get
            {
                return userPermission;
            }
            set
            {
                userPermission = value;
            }
        }

        #endregion

        #region Override
        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is UserAccessData)
            {
                result = this.Name == ((UserAccessData)obj).Name;
            }
            return result;
        }

        #endregion
    }
}
