﻿using System;
using System.Linq;
using System.Text;
using NHibernate.Mapping.Attributes;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(EndingReportAnswerData), Table = "ENDING_REPORT_ANSWER", Lazy = false, Where = "DELETED_ID IS NULL")]
    public class EndingReportAnswerData: ObjectData
    {
        [Property(Column = "ANSWER_TEXT", NotNull=true)]
        public string AnswerText
        {
            get;
            set;
        }

        [Property(Column = "ANSWER_HEADER_TEXT", NotNull = true)]
        public string AnswerHeaderText
        {
            get;
            set;
        }

        [Property(Column = "QUESTION_TEXT", NotNull=true)]
        public string QuestionText
        {
            get;
            set;
        }

        [ManyToOne(ClassType = typeof(DispatchOrderData), Column = "DISPATCH_ORDER_CODE",
        Cascade = "none", ForeignKey = "FK_DISPATCH_ORDER_ENDING_REPORT_ANSWER_CODE")]
        public DispatchOrderData DispatchOrder
        {
            get;
            set;
        }
    }
}
