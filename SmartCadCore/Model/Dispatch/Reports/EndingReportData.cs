using System;
using System.Collections;
using System.Text;
using NHibernate.Mapping.Attributes;

namespace SmartCadCore.Model
{
    [Serializable]
    [Component()]
    public class EndingReportData
    {

        #region Properties

        [ManyToOne(ClassType = typeof(OfficerData), Column = "OFFICER_CODE",
            ForeignKey = "FK_OFFICER_ENDING_REPORT_CODE", NotNull = false, Cascade = "none")]
        public OfficerData Officer
        {
            get;
            set;
        }

        [Property(Column = "BY_UNIT")]
        public bool ByUnit
        {
            get;
            set;
        }

        [Bag(0, Name = "Answers", Table = "ENDING_REPORT_ANSWER", Cascade = "save-update",
          Lazy = CollectionLazy.True, Fetch = CollectionFetchMode.Subselect, Where = "DELETED_ID IS NULL")]
        [Key(1, Column = "DISPATCH_ORDER_CODE"/*, ForeignKey = "FK_DISPATCH_ORDER_ENDING_REPORT_ANSWER_CODE"*/)]
        [OneToMany(2, ClassType = typeof(EndingReportAnswerData))]
        public IList Answers
        {
            get; 
            set;
        }

        public bool Active
        {
            get
            {
                return Officer != null;
            }
        }

        #endregion
    }
}
