﻿using NHibernate.Mapping.Attributes;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    #region Class DepartmentData Documentation
    /// <summary>
    /// This class represents a department type for a phone report.
    /// </summary>
    /// <className>DepartmentData</className>
    /// <author email="fsilva@smartmatic.com">Fernando Silva</author>
    /// <date>2007/01/30</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    [Serializable]
    [Class(NameType = typeof(ReportBaseDepartmentTypeData), Table = "REPORT_BASE_DEPARTMENT_TYPE", Lazy = false, Where = "DELETED_ID IS NULL")]
    public class ReportBaseDepartmentTypeData : ObjectData
    {
        #region Fields

        DepartmentTypeData departmentType;
        IncidentNotificationPriorityData priority;
        ReportBaseData reportBase;

        #endregion

        #region Constructors

        public ReportBaseDepartmentTypeData()
        {
        }

        public ReportBaseDepartmentTypeData(DepartmentTypeData departmentType, IncidentNotificationPriorityData priority)
        {
            this.departmentType = departmentType;
            this.priority = priority;
        }

        public ReportBaseDepartmentTypeData(
            DepartmentTypeData departmentType,
            IncidentNotificationPriorityData priority,
            ReportBaseData reportBase)
            : this(departmentType, priority)
        {
            this.reportBase = reportBase;
        }

        #endregion

        #region Properties

        [ManyToOne(ClassType = typeof(DepartmentTypeData), Column = "DEPARTMENT_TYPE_CODE",
            ForeignKey = "FK_REPORT_BASE_DEPARTMENT_TYPE_DEPARTMENT_TYPE_CODE", Cascade = "none")]
        public virtual DepartmentTypeData DepartmentType
        {
            get
            {
                return this.departmentType;
            }
            set
            {
                this.departmentType = value;
            }
        }

        [ManyToOne(ClassType = typeof(IncidentNotificationPriorityData), Column = "INCIDENT_NOTIFICATION_PRIORITY_CODE",
            ForeignKey = "FK_REPORT_BASE_DEPARTMENT_TYPE_INCIDENT_NOTIFICATION_PRIORITY_CODE", Cascade = "none")]
        public virtual IncidentNotificationPriorityData Priority
        {
            get
            {
                return this.priority;
            }
            set
            {
                this.priority = value;
            }
        }

        [ManyToOne(ClassType = typeof(ReportBaseData), Column = "REPORT_BASE_CODE",
                  ForeignKey = "FK_REPORT_BASE_DEPARTMENT_TYPE_REPORT_BASE_CODE", Cascade = "none")]
        public virtual ReportBaseData ReportBase
        {
            get
            {
                return this.reportBase;
            }
            set
            {
                this.reportBase = value;
            }
        }
        #endregion
    }
}
