﻿using NHibernate.Mapping.Attributes;
using System;

using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    #region Class DispatchOrderNoteTypeData Documentation
    /// <summary>
    /// This class represents the types that a dispatch Order Note can be.
    /// </summary>
    /// <className>DispatchOrderNoteTypeData</className>
    /// <author email="jynojosa@smartmatic.com">Julio Ynojosa</author>
    /// <date>2007/03/28</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    [Serializable]
    [Class(NameType = typeof(DispatchOrderNoteTypeData), Table = "DISPATCH_ORDER_NOTE_TYPE", Lazy = false, Where = "DELETED_ID IS NULL")]
    public class DispatchOrderNoteTypeData : ObjectData
    {
        #region Fields
        private string customCode;
        private string friendlyName;
        private string name;
        #endregion

        #region Properties

        [Property(0)]
        [Column(1, Name = "NAME", NotNull = true)]
        public virtual string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        [Property(0)]
        [Column(1, Name = "FRIENDLY_NAME", NotNull = true)]
        public virtual string FriendlyName
        {
            get
            {
                return friendlyName;
            }
            set
            {
                friendlyName = value;
            }
        }

        [Property(0, Unique = true)]
        [Column(1, Name = "CUSTOM_CODE", UniqueKey = "UK_DISPATCH_ORDER_NOTE_CUSTOM_CODE", NotNull = true)]
        public virtual string CustomCode
        {
            get
            {
                return customCode;
            }
            set
            {
                customCode = value;
            }
        }

        #endregion

        #region InitialData

        [InitialData(PropertyName = "CustomCode", PropertyValue = "DELAY_NOTE")]
        public static DispatchOrderNoteTypeData TimeChanged;

        [InitialData(PropertyName = "CustomCode", PropertyValue = "CANCEL_NOTE")]
        public static DispatchOrderNoteTypeData Cancelled;

        [InitialData(PropertyName = "CustomCode", PropertyValue = "CLOSE_NOTE")]
        public static DispatchOrderNoteTypeData Closed;

        [InitialData(PropertyName = "CustomCode", PropertyValue = "CURRENT_PROCEDURE")]
        public static DispatchOrderNoteTypeData CurrentProcedure;

        [InitialData(PropertyName = "CustomCode", PropertyValue = "CREATION_NOTE")]
        public static DispatchOrderNoteTypeData Creation;

        #endregion
    }
}
