using System;
using System.Text;
using NHibernate.Mapping.Attributes;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(SupervisorCloseReportMessageData), Table = "CLOSE_REPORT_MESSAGE", Lazy = false, Where = "DELETED_ID IS NULL")]
    public class SupervisorCloseReportMessageData : ObjectData
    {
        private SupervisorCloseReportData report;
        private DateTime time;
        private string message;

        [Property(0)]
        [Column(1, Name = "MESSAGE", NotNull = true, Length = 16384)]
        public string Message
        {
            get { return message; }
            set { message = value; }
        }

        [Property(0, TypeType = typeof(DateTime))]
        [Column(1, Name = "TIME", NotNull = true)]
        public DateTime Time
        {
            get { return time; }
            set { time = value; }
        }

        [ManyToOne(0, ClassType = typeof(SupervisorCloseReportData), ForeignKey = "FK_CLOSE_REPORT_CLOSE_REPORT_MESSAGE_CODE", Cascade = "none")]
        [Column(1, Name = "CLOSE_REPORT_CODE", NotNull = true)]
        public SupervisorCloseReportData Report
        {
            get { return report; }
            set { report = value; }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is SupervisorCloseReportMessageData)
            {
                SupervisorCloseReportMessageData scrmd = (SupervisorCloseReportMessageData)obj;
                result = this.Report == scrmd.Report && this.Time == scrmd.Time;
            }
            return result;
        }
    }
}
