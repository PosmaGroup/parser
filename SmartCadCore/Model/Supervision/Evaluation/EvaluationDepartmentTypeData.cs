using System;
using System.Text;
using NHibernate.Mapping.Attributes;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(EvaluationDepartmentTypeData), Table = "EVALUATION_DEPARTAMENT_TYPE", Lazy = false, Where = "DELETED_ID IS NULL")]
    public class EvaluationDepartmentTypeData : ObjectData, INamedObjectData
    {
        #region Fields
        private EvaluationData evaluation;
        private DepartmentTypeData departament;
        #endregion

        #region Properties

        [ManyToOne(ClassType = typeof(EvaluationData), Column = "EVALUATION_CODE", 
            Cascade = "none", ForeignKey = "FK_EVALUATION_DEPARTAMENT_TYPE_EVALUATION_CODE")]
        public virtual EvaluationData Evaluation
        {
            get
            {
                return evaluation;
            }
            set
            {
                evaluation = value;
            }
        }

        [ManyToOne(Column = "DEPARTAMENT_TYPE_CODE", ClassType = typeof(DepartmentTypeData),
            ForeignKey = "FK_EVALUATION_DEPARTAMENT_TYPE_DEPARTAMENT_TYPE_CODE", Cascade = "none")]
        public virtual DepartmentTypeData Departament
        {
            get
            {
                return departament;
            }
            set
            {
                departament = value;
            }
        }
        #endregion

        public override bool Equals(object obj)
        {
            try
            {
                if (obj.GetType() == typeof(EvaluationDepartmentTypeData))
                {
                    EvaluationDepartmentTypeData deps = (EvaluationDepartmentTypeData)obj;

                    if (deps.Name == this.Name)
                    {
                        return true;
                    }
                }
            }
            catch { }
            return false;
        }

        public override string ToString()
        {
            string text = "";

            if (this.departament != null)
            {
                text = this.departament.Name;
            }

            return text;
        }

        #region INamedObjectData Members

        public string Name
        {
            get
            {
                string text = "";

                if (this.departament != null)
                {
                    text = this.departament.Name;
                }

                return text;
            }
            set
            {
                if (this.departament != null)
                {
                    departament.Name = value;
                }
            }
        }

        #endregion
    }
}
