using System;
using System.Text;
using NHibernate.Mapping.Attributes;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(EvaluationUserApplicationData), Table = "EVALUATION_USER_APPLICATION", Lazy = false, Where = "DELETED_ID IS NULL")]
    public class EvaluationUserApplicationData : ObjectData, INamedObjectData
    { 
        #region Fields
        private EvaluationData evaluation;
        private UserApplicationData application;
        #endregion

        #region Properties
      
        [ManyToOne(ClassType = typeof(EvaluationData), Column = "EVALUATION_CODE",
           Cascade = "none", ForeignKey = "FK_EVALUATION_USER_APPLICATION_EVALUATION_CODE")]
        public virtual EvaluationData Evaluation
        {
            get
            {
                return evaluation;
            }
            set
            {
                evaluation = value;
            }
        }


        [ManyToOne(ClassType = typeof(UserApplicationData), Column = "USER_APPLICATION_CODE", 
            Cascade = "none", ForeignKey = "FK_EVALUATION_USER_APPLICATION_USER_APPLICATION_CODE")]
        public virtual UserApplicationData Application
        {
            get
            {
                return application;
            }
            set
            {
                application = value;
            }
        }
        #endregion

        public override bool Equals(object obj)
        {
            try
            {
                if (obj.GetType() == typeof(EvaluationUserApplicationData))
                {
                    EvaluationUserApplicationData apps = (EvaluationUserApplicationData)obj;

                    if (apps.Name == this.Name)
                    {
                        return true;
                    }
                }
            }
            catch { }
            return false;
        }

        public override string ToString()
        {
            string text = "";

            if (this.application.FriendlyName != null)
            {
                text = this.application.FriendlyName;
            }

            return text;
        }

        #region INamedObjectData Members

        public string Name
        {
            get
            {
                return application.FriendlyName;
            }
            set
            {
                application.FriendlyName = value;
            }
        }

        #endregion
    }
}
