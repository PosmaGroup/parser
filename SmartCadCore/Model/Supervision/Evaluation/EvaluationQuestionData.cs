using System;
using System.Text;

using NHibernate.Mapping.Attributes;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(EvaluationQuestionData), Table = "EVALUATION_QUESTION", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_2", Value = "UK_EVALUATION_QUESTION")]

    public class EvaluationQuestionData : ObjectData, INamedObjectData
    {
        #region Fields

        private EvaluationData evaluation;
        private string name;
        private float weighting; 

        #endregion

        #region Constructors

        public EvaluationQuestionData()
        {
        }

        public EvaluationQuestionData(string name)
        {
            this.name = name;
        }

        #endregion

        #region Properties

        [Property(0, TypeType = typeof(string))]
        [Column(1, Name = "NAME", UniqueKey = "UK_EVALUATION_QUESTION", NotNull = true)]
        public virtual string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        [ManyToOne(ClassType = typeof(EvaluationData), ForeignKey = "FK_EVALUATION_QUESTION_EVALUATION_CODE", Cascade = "none")]
        [Column(1, Name = "EVALUATION_CODE", UniqueKey = "UK_EVALUATION_QUESTION")]        
        public EvaluationData Evaluation
        {
            get
            {
                return evaluation;
            }
            set
            {
                evaluation = value;
            }
        }

        [Property(0, Unique = false)]
        [Column(1, Name = "WEIGHTING", NotNull = false)]
        public virtual float Weighting
        {
            get
            {
                return weighting;
            }
            set
            {
                weighting = value;
            }
        }

        #endregion

        public override bool Equals(object obj)
        {
            try
            {
                EvaluationQuestionData ques = (EvaluationQuestionData)obj;

                if (ques.Name == this.Name)
                {
                    if (ques.evaluation != null && this.evaluation != null)
                    {
                        if (ques.evaluation.Code == this.evaluation.Code)
                        {
                            return true;
                        }
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            catch { }
            return false;
        }

        public override string ToString()
        {
            string text = "";

            if (this.Name != null)
            {
                text = this.Name;
            }

            return text;
        }
    }
}
