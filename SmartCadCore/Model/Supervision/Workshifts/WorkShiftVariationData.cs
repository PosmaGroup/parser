using System;
using System.Text;
using NHibernate.Mapping.Attributes;
using System.Collections;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(WorkShiftVariationData), Table = "WORK_SHIFT_VARIATION", Lazy = false, Where = "DELETED_ID IS NULL")]
	public class WorkShiftVariationData : ObjectData, INamedObjectData
	{
		#region Properties
		public enum ObjectRelatedType
		{
			Operators = 0,
			Units = 1
		}

		public enum WorkShiftType 
        {
            WorkShift = 0,
            ExtraTime = 1,
            TimeOff = 2
        }

        [Property(0, Unique = true)]
        [Column(1, Name = "NAME", NotNull = true)]
        public string Name { get; set; }

        [Property(Length = 305, Column = "DESCRIPTION" ,NotNull=true)]
        public string Description { get; set ; }
        
        [ManyToOne(0, ClassType = typeof(MotiveVariationData), ForeignKey = "FK_WORK_SHIFT_VARIATION_MOTIVE_VARIATION_CODE", Cascade = "none")]
        [Column(1, Name = "MOTIVE_VARIATION_CODE", NotNull = false)]
        public MotiveVariationData Motive { get; set; }

		[Property(0, Type = "int", Unique = false)]
		[Column(1, Name = "TYPE", NotNull = false)]
		public WorkShiftType Type { get; set; }

		[Property(0, Type = "int", Unique = true)]
		[Column(1, Name = "OBJECT_RELATED_TYPE", NotNull = false)]
		public ObjectRelatedType ObjectType { get; set; }

		[Bag(0, Name = "Schedules", Table = "WORK_SHIFT_SCHEDULE_VARIATION", Cascade = "save-update",
          Lazy = CollectionLazy.True, Fetch = CollectionFetchMode.Subselect, Where = "DELETED_ID IS NULL")]
        [Key(1, Column = "WORK_SHIFT_VARIATION_CODE"/*, ForeignKey = "FK_WORK_SHIFT_VARIATION_WORK_SHIFT_SCHEDULE_VARIATION_CODE"*/)]
        [OneToMany(2, ClassType = typeof(WorkShiftScheduleVariationData))]
        public IList Schedules { get; set; }

        [Bag(0, Name = "Operators", Table = "WORK_SHIFT_VARIATION_OPERATOR", Cascade = "save-update", 
            Lazy = CollectionLazy.True, Fetch = CollectionFetchMode.Subselect, Where = "DELETED_ID IS NULL")]
        [Key(1, Column = "WORK_SHIFT_VARIATION_CODE"/*, ForeignKey = "FK_WORK_SHIFT_VARIATION_CODE"*/)]
        [OneToMany(2, ClassType = typeof(WorkShiftOperatorData))]
        public virtual IList Operators { get; set; }

		#endregion

		#region Overrides

		public override bool Equals(object obj)
		{
			if (obj is WorkShiftVariationData== false)
				return false;
			
			WorkShiftVariationData wsvd = obj as WorkShiftVariationData;
			if (wsvd.Name == Name)
				return true;
			return false;
		}
		#endregion

		#region Initial data

		[InitialData(PropertyName = "Name", PropertyValue = "WorkShiftVariationData")]
		public static readonly UserResourceData Resource;

		#endregion
    }

    [Serializable]
    [Class(NameType = typeof(WorkShiftOperatorData), Table = "WORK_SHIFT_VARIATION_OPERATOR", Lazy = false, Where = "DELETED_ID IS NULL")]
    public class WorkShiftOperatorData : ObjectData
    {
        [ManyToOne(0, ClassType = typeof(WorkShiftVariationData), ForeignKey = "FK_WORK_SHIFT_OPERATOR_WORK_SHIFT_VARIATION_CODE")]
        [Column(1, Name = "WORK_SHIFT_VARIATION_CODE", NotNull = false)]
        public WorkShiftVariationData WorkShift { get; set; }

        [ManyToOne(0, ClassType = typeof(OperatorData), ForeignKey = "FK_WORK_SHIFT_OPERATOR_OPERATOR_CODE")]
        [Column(1, Name = "OPERATOR_CODE", NotNull = false)]
        public OperatorData Operator { get; set; }

        public override bool Equals(object obj)
        {
            if (obj is WorkShiftOperatorData == false)
                return false;

            WorkShiftOperatorData wsvd = obj as WorkShiftOperatorData;
            if (wsvd.WorkShift.Code == WorkShift.Code && 
                wsvd.Operator.Code == Operator.Code)
                return true;
            return false;
        }
    }
}
