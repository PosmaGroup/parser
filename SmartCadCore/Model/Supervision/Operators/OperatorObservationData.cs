using System;
using System.Text;
using NHibernate.Mapping.Attributes;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(OperatorObservationData), Table = "OPERATOR_OBSERVATION", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_OPERATOR_OBSERVATION")]
    public class OperatorObservationData : ObjectData
    {
        private ObservationTypeData observationType;
        private DateTime date;
        private string observation;
        private OperatorData operatorData;
        private OperatorData supervisor;

        [ManyToOne(0, ClassType = typeof(OperatorData),
            ForeignKey = "FK_OPERATOR_OBSERVATION_OPERATOR_CODE", Cascade = "none")]
        [Column(1, Name = "OPERATOR_CODE", NotNull = true,
           UniqueKey = "UK_OPERATOR_OBSERVATION")]
        public OperatorData Operator
        {
            get { return operatorData; }
            set { operatorData = value; }
        }

        [ManyToOne(0, ClassType = typeof(OperatorData),
            ForeignKey = "FK_SUPERVISOR_OBSERVATION_OPERATOR_CODE", Cascade = "none")]
        [Column(1, Name = "SUPERVISOR_CODE", NotNull = true)]
        public OperatorData Supervisor
        {
            get { return supervisor; }
            set { supervisor = value; }
        }

        [Property(0, Column = "OBSERVATION", NotNull = true, Length = 65535)]       
        public string Observation
        {
            get { return observation; }
            set { observation = value; }
        }

        [Property(0, Unique = true)]
        [Column(1, Name = "DATE", UniqueKey = "UK_OPERATOR_OBSERVATION", NotNull = true)]
        public DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        [ManyToOne(0, ClassType = typeof(ObservationTypeData),
            ForeignKey = "FK_OPERATOR_OBSERVATION_OBSERVATION_TYPE_CODE", Cascade = "none")]
        [Column(1, Name = "OBSERVATION_TYPE_CODE", NotNull = true,
           UniqueKey = "UK_OPERATOR_OBSERVATION")]
        public ObservationTypeData ObservationType
        {
            get { return observationType; }
            set { observationType = value; }
        }
    }
}
