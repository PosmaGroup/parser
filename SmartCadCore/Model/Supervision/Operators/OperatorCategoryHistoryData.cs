﻿using NHibernate.Mapping.Attributes;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(OperatorCategoryHistoryData), Table = "OPERATOR_CATEGORY_HISTORY", Lazy = false, Where = "DELETED_ID IS NULL")]
    public class OperatorCategoryHistoryData : ObjectData
    {
        #region Fields

        private OperatorData userAccount;

        private OperatorCategoryData category;

        private string detail;

        private DateTime? startDate;

        private DateTime? endDate;

        #endregion

        #region Properties

        /// <summary>
        /// User account.
        /// </summary>

        [ManyToOne(0, ClassType = typeof(OperatorData), Insert = true, Update = false,
          ForeignKey = "FK_OPERATOR_OPERATOR_CATEGORY_HISTORY_CODE", Cascade = "none")]
        [Column(1, Name = "USER_ACCOUNT_CODE", NotNull = true, UniqueKey = "UK_OPERATOR_OPERATOR_CATEGORY_HISTORY")]
        public virtual OperatorData Operator
        {
            get
            {
                return userAccount;
            }
            set
            {
                userAccount = value;
            }
        }

        [ManyToOne(0, ClassType = typeof(OperatorCategoryData), ForeignKey = "FK_OPER_CAT_HISTORY_OPER_CAT_CODE",
            Cascade = "none")]
        [Column(1, Name = "CATEGORY_CODE", NotNull = true, UniqueKey = "UK_OPERATOR_OPERATOR_CATEGORY_HISTORY")]
        public virtual OperatorCategoryData Category
        {
            get
            {
                return category;
            }
            set
            {
                category = value;
            }
        }

        [Property(Column = "DETAIL", Length = 1024, NotNull = false)]
        public virtual string Detail
        {
            get
            {
                return detail;
            }
            set
            {
                detail = value;
            }
        }

        /// <summary>
        /// Start date
        /// </summary>
        [Property(Name = "StartDate", Column = "START_DATE", TypeType = typeof(DateTime))]
        public virtual DateTime? StartDate
        {
            get
            {
                return startDate;
            }
            set
            {
                startDate = value;
            }
        }

        /// <summary>
        /// End date
        /// </summary>
        [Property(Name = "EndDate", Column = "END_DATE", TypeType = typeof(DateTime))]
        public virtual DateTime? EndDate
        {
            get
            {
                return endDate;
            }
            set
            {
                endDate = value;
            }
        }
        #endregion

        public override bool Equals(object obj)
        {
            if (obj is OperatorCategoryHistoryData == false)
                return false;
            OperatorCategoryHistoryData cat = obj as OperatorCategoryHistoryData;
            if (cat.Code == Code)
                return true;
            return false;
        }
    }
}
