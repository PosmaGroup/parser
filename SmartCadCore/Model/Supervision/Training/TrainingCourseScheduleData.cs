using System;
using System.Text;
using System.Collections;
using NHibernate.Mapping.Attributes;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(TrainingCourseScheduleData), Table = "TRAINING_COURSE_SCHEDULE", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("UK_START", Value = "UK_TRAINING_COURSE_SCHEDULE_NAME")]
    [AttributeIdentifier("UK_END", Value = "UK_TRAINING_COURSE_SCHEDULE_NAME")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_TRAINING_COURSE_SCHEDULE_NAME")]
    [AttributeIdentifier("IX_START", Value = "IX_TRAINING_COURSE_SCHEDULE_START")]
    public class TrainingCourseScheduleData : BaseSessionHistory
    {
        private string name;
        private string trainer;
        private int minimumAttendance;
        private IList parts;
        private TrainingCourseData trainingCourse;
        private IList operators;

        private DateTime realStart = DateTime.MaxValue;
        private DateTime realEnd = DateTime.MinValue;

        [ManyToOne(0, ClassType = typeof(TrainingCourseData),
            ForeignKey = "FK_TRAINING_COURSE_SCHEDULE_TRAINING_COURSE_CODE", Cascade = "none")]
        [Column(1, Name = "TRAINING_COURSE_CODE", NotNull = true,
           UniqueKey = "UK_TRAINING_COURSE_SCHEDULE_NAME")]
        public TrainingCourseData TrainingCourse
        {
            get { return trainingCourse; }
            set { trainingCourse = value; }
        }

        [Bag(0, Table = "TRAINING_COURSE_SCHEDULE_PARTS", Cascade = "save-update",
           Lazy = CollectionLazy.True, Inverse = true, Fetch = CollectionFetchMode.Subselect,  Where = "DELETED_ID IS NULL")]
        [Key(1, Column = "TRAINING_COURSE_SCHEDULE_CODE"/*, 
            ForeignKey = "FK_TRAINING_COURSE_SCHEDULE_TRAINING_COURSE_SCHEDULE_PARTS_CODE"*/)]
        [OneToMany(2, ClassType = typeof(TrainingCourseSchedulePartsData))]
        public IList Parts
        {
            get { return parts; }
            set { parts = value; }
        }

        [Property(0, Unique=true)]
        [Column(1, Name = "NAME", NotNull = true, UniqueKey = "UK_TRAINING_COURSE_SCHEDULE_NAME")]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        [Property(0)]
        [Column(1, Name = "TRAINER")]
        public string Trainer
        {
            get { return trainer; }
            set { trainer = value; }
        }

        [Bag(0, Table = "OPERATOR_TRAINING_COURSE", Cascade = "save-update", Inverse = true,
         Lazy = CollectionLazy.True, Fetch = CollectionFetchMode.Subselect, Where = "DELETED_ID IS NULL")]
        [Key(1, Column = "TRAINING_COURSE_SCHEDULE_CODE"/*, ForeignKey = "FK_TRAINING_COURSE_SCHEDULE_OPERATOR_TRAINING_COURSE_CODE"*/)]
        [OneToMany(2, ClassType = typeof(OperatorTrainingCourseData))]
        public IList Operators
        {
            get { return operators; }
            set { operators = value; }
        }

        public DateTime RealStart
        {
            get
            {
                return realStart;
            }
            set
            {
                realStart = value;
            }
        }

        public DateTime RealEnd
        {
            get
            {
                return realEnd;
            }
            set
            {
                realEnd = value;
            }
        }
    }
}
