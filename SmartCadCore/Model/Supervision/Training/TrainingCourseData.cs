using System;
using System.Text;
using System.Collections;
using NHibernate.Mapping.Attributes;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(TrainingCourseData), Table = "TRAINING_COURSE", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_TRAINING_COURSE")]
    public class TrainingCourseData : ObjectData
    {
        private string name;
        private string objective;
        private string content;
        private string contactPerson;
        private string phoneContactPerson;
        private string link;
        private IList schedules;

        public TrainingCourseData()
        {
        }

        [Bag(0, Table = "TRAINING_COURSE_SCHEDULE", Cascade = "none",
           Lazy = CollectionLazy.True, Inverse = true, Fetch = CollectionFetchMode.Subselect, Where = "DELETED_ID IS NULL")]
        [Key(1, Column = "TRAINING_COURSE_CODE"/*,
           ForeignKey = "FK_TRAINING_COURSE_SCHEDULE_TRAINING_COURSE_CODE"*/)]
        [OneToMany(2, ClassType = typeof(TrainingCourseScheduleData))]
        public IList Schedules
        {
            get { return schedules; }
            set { schedules = value; }
        }

        [Property(0)]
        [Column(1, Name = "LINK", NotNull = true)]
        public string Link
        {
            get { return link; }
            set { link = value; }
        }

        [Property(0)]
        [Column(1, Name = "PHONE_CONTACT_PERSON", NotNull = true)]
        public string PhoneContactPerson
        {
            get { return phoneContactPerson; }
            set { phoneContactPerson = value; }
        }

        [Property(0)]
        [Column(1, Name = "CONTACT_PERSON", NotNull = true)]
        public string ContactPerson
        {
            get { return contactPerson; }
            set { contactPerson = value; }
        }

        [Property(0)]
        [Column(1, Name = "CONTENT", NotNull = true)]
        public string Content
        {
            get { return content; }
            set { content = value; }
        }

        [Property(0)]
        [Column(1, Name = "OBJECTIVE", NotNull = true)]
        public string Objective
        {
            get { return objective; }
            set { objective = value; }
        }

        [Property(0, Unique = true)]
        [Column(1, Name = "NAME", UniqueKey = "UK_TRAINING_COURSE", NotNull = true)]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
    }
}
