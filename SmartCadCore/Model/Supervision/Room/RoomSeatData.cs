using System;
using System.Collections;
using System.Text;

using NHibernate.Mapping.Attributes;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(RoomSeatData), Table = "ROOM_SEAT", Lazy = false, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_ROOM_SEAT_COMPUTER_NAME")]
    public class RoomSeatData : ObjectData
    {


        #region Fields

        private int x;
        private int y;
        private int width;
        private int heigth;
        private string numberseat;
        private RoomData room;
        private string computerName;

        #endregion

        #region Properties


        [Property(Column = "X")]
        public int X
        {
            get { return x; }
            set { x = value; }
        }

        [Property(Column = "Y")]
        public int Y
        {
            get { return y; }
            set { y = value; }
        }

        [Property(Column = "WIDTH")]
        public int Width
        {
            get { return width; }
            set { width = value; }
        }


        [Property(Column = "HEIGHT")]
        public int Height
        {
            get { return heigth; }
            set { heigth = value; }
        }

        [Property(Column = "NUMBER_SEAT")]
        public string NumberSeat
        {
            get { return numberseat; }
            set { numberseat = value; }
        }

        [Property(0, Unique = true)]
        [Column(1, Name = "COMPUTER_NAME", NotNull = true, UniqueKey = "UK_ROOM_SEAT_COMPUTER_NAME")]
        public string ComputerName
        {
            get { return computerName; }
            set { computerName = value; }
        }

        [ManyToOne(0, ClassType = typeof(RoomData), ForeignKey = "FK_ROOM_DATA_CODE", Cascade = "none")]
        [Column(1, Name = "ROOM_CODE")]
        public virtual RoomData Room
        {
            get
            {
                return room;
            }
            set
            {
                room = value;
            }
        }
        
        #endregion


        #region Constructors
        
        #endregion

    }
}
