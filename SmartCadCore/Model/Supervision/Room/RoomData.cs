using System;
using System.Text;

using NHibernate.Mapping.Attributes;
using Iesi.Collections;
using System.Collections;
using System.Drawing;

namespace SmartCadCore.Model
{
    [Serializable]
    [Class(NameType = typeof(RoomData), Table = "ROOM", Lazy = false, BatchSize = 100, Where = "DELETED_ID IS NULL")]
    [AttributeIdentifier("DELETED_ID_1", Value = "UK_ROOM_NAME")]
    public class RoomData : ObjectData
    {
        #region Fields

        private string name;
        private string description;
        private int seats;
        private string renderMethod;
        private IList listSeats;
        private byte[] image;
        private string seatsImage;

        
        #endregion

        #region Properties

        [Property(0, Unique = true)]
        [Column(1, Name = "NAME", NotNull = true, UniqueKey = "UK_ROOM_NAME")]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        [Property(Name = "Description")]
        public string Description
        {
            get { return description; }
            set { description = value; }
        }


        [Property(Name = "Seats")]
        public int Seats
        {
            get { return seats; }
            set { seats = value; }
        }

        [Property(0)]
        [Column(1, Name = "RENDER_METHOD", Length = 65535)]
        public string RenderMethod
        {
            get { return renderMethod; }
            set { renderMethod = value; }
        }


        [Property(Column = "BACKGROUND_IMAGE", TypeType = typeof(byte[]), Length = 80000)]
        public byte[] Image 
        {
            get { return image; }
            set { image = value; }
        }

        [Property(Column = "SEATS_IMAGE")]
        public string SeatsImage
        {
            get { return seatsImage; }
            set { seatsImage = value; }
        }

        [Bag(0, Name = "ListSeats", Table = "ROOM_SEAT", Lazy = CollectionLazy.True,
           Cascade = "save-update", Inverse = true, Where = "DELETED_ID IS NULL")]
        [Key(1, Column = "ROOM_CODE"/*, ForeignKey = "FK_ROOM_CODE"*/)]
        [OneToMany(2, ClassType = typeof(RoomSeatData))]
        public IList ListSeats
        {
            get
            {
                return listSeats;
            }
            set
            {
                listSeats = value;
            }
        }
        
        #endregion

        #region Initial data

        [InitialData(PropertyName = "Name", PropertyValue = "RoomData")]
        public static readonly UserResourceData Resource; 

        #endregion

        #region Methods

        public override string ToString()
        {
            return this.name;
        }
        #endregion
    }
}
