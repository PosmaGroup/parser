﻿using System;
using System.Linq;
using System.Text;
using NHibernate.Mapping.Attributes;
using System.Collections;
using Iesi.Collections;

namespace SmartCadCore.Model
{
	[Serializable]
    [Class(NameType = typeof(AlertNotificationData), Table = "ALERT_NOTIFICATION", Lazy = false, Where = "DELETED_ID IS NULL")]
	public class AlertNotificationData : ObjectData
	{
		public enum AlertStatus
		{
			NotTaken = 0,
			InProcess,
			Ended
		}

		#region Constructor
		public AlertNotificationData()
		{

		}
		#endregion

		#region Properties
		[ManyToOne(Column = "OPERATOR_CODE", ClassType = typeof(OperatorData),
			ForeignKey = "FK_ALERT_NOTIFICATION_OPERATOR_CODE", Cascade = "none")]
		public OperatorData AttendingOperator { get; set; }

		[ManyToOne(Column = "UNIT_ALERT_CODE", ClassType = typeof(UnitAlertData),
			ForeignKey = "FK_ALERT_NOTIFICATION_UNIT_ALERT_CODE", Cascade = "none")]
		public UnitAlertData UnitAlert { get; set; }

		[Property(0,Type="DateTime")]
		[Column(1,Name = "DATE",NotNull = true)]
		public DateTime Date { get; set; }
		
		[Property(1,Type="int")]
		[Column(2,Name="STATUS",NotNull= true)]
		public AlertStatus Status { get; set; }

		[Set(0, Name = "Observations", Table = "ALERT_NOTIFICATION_OBSERVATION", Cascade = "save-update",
		   Lazy = CollectionLazy.True, Fetch = CollectionFetchMode.Subselect, Inverse = true, Where = "DELETED_ID IS NULL")]
		[Key(1, Column = "ALERT_NOTIFICATION_CODE"/*, ForeignKey = "FK_ALERT_NOTIFICATION_OBSERVATION_CODE"*/)]
		[OneToMany(2, ClassType = typeof(AlertNotificationObservationData))]
        public ISet Observations { get; set; }

		[ManyToOne(ClassType = typeof(SensorData), Column = "SENSOR_CODE",
			Cascade = "none", ForeignKey = "FK_ALERT_NOTIFICATION_SENSOR_CODE")]
		public SensorData Sensor
		{
			get;
			set;
		}

		#endregion

		#region Overrides
		public override bool Equals(object obj)
		{
			if (obj is AlertNotificationData == false)
				return false;
			AlertNotificationData and = obj as AlertNotificationData;
			if (and.UnitAlert.Code == UnitAlert.Code &&
				and.Date.Ticks == Date.Ticks)
				return true;
			return false;
		}
		#endregion
	}
}
