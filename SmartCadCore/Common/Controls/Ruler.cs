﻿using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace SmartCadCore.Common
{
    public partial class Ruler : DevExpress.XtraEditors.XtraUserControl
    {
        public Ruler()
        {
            InitializeComponent();
            this.pictureEdit1.Image = ResourceLoader.GetImage("$Image.RulerPNG");
        }
    }
}
