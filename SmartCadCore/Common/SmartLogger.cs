using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Reflection;

using log4net;

namespace SmartCadCore.Common
{
    #region Class SmartLogger Documentation
    /// <summary>
    /// TODO:
    /// </summary>
    /// <className>SmartLogger</className>
    /// <author email="atorres@smartmatic.com">Alden Torres</author>
    /// <author email="fsilva@smartmatic.com">Fernando Silva</author>
    /// <date>2007/01/16</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    public static class SmartLogger
    {
        private const string GENERIC_LOG_NAME = "GenericLog";

        private static readonly ILog log;
        private static bool printToConsole = true;

        static SmartLogger()
        {
            string logName;

            if (Assembly.GetEntryAssembly() != null)
                logName = Assembly.GetEntryAssembly().GetName().Name;
            else
                logName = GENERIC_LOG_NAME;

            log = LogManager.GetLogger(logName);
        }

        public static bool PrintToConsole
        {
            get
            {
                return printToConsole;
            }
            set
            {
                printToConsole = value;
            }
        }

        public static void Info(object message)
        {
            log.Info(message);
        }

        public static void Print(string message)
        {
            StringBuilder sb = new StringBuilder();

            if (log.IsInfoEnabled == true)
            {
                StackTrace stackTrace = new StackTrace();
                StackFrame stackFrame = stackTrace.GetFrame(1);

                MethodBase methodBase = stackFrame.GetMethod();

                string className = methodBase.DeclaringType.FullName;
                string methodName = methodBase.Name;

                sb.Append(className);
                sb.Append(":-");
                sb.Append(methodName);
                sb.Append(":-");
                sb.Append(message);

                log.Info(sb.ToString());
            }

            if (printToConsole == true)
                Console.WriteLine("[" + DateTime.Now.ToString("hh:mm:ss.fff") + "] " + sb.ToString());
        }

        public static void Print(Exception ex)
        {
            Print(ex.Message);
            Print(ex.StackTrace);
        }
    }
}
