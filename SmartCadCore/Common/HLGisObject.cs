﻿using System;
using System.Collections.Generic;
using System;
using System.Text;
using SmartCadCore.ClientData;

namespace SmartCadCore.Common
{
    [Serializable]
    public enum HLGisObjectType 
    { 
        Unit,
        Units,
        Incident,
        Incidets,
        Struct,
        Structs,
        Zone,
        Station,
        UnitsRecommended,
        UnitsAsigned,
        UnitRecommended,
        UnitAsigned,
        UnitsRecommendedAsigned
    }

    [Serializable]
    public class HLGisObject
    {
        public double Lat { get; set; }
        public double Lon { get; set; }
        public ClientData.ClientData Data { get; set; }
        public HLGisObjectType HLType { get; set; }

        public HLGisObject()
        {

        }

        public HLGisObject(double lat, double lon, ClientData.ClientData clientData, HLGisObjectType dtype)
        {
            this.Lat = lat;
            this.Lon = lon;
            this.Data = clientData;
            this.HLType = dtype;
        }
    }
}
