using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class TrainingCourseClientData : ClientData
    {
        private string name;
        private string objective;
        private string content;
        private string contactPerson;
        private string phoneContactPerson;
        private string link;
        private int qualification;
        private int approvalQualification;
        private IList schedules;

        public int ApprovalQualification
        {
            get { return approvalQualification; }
            set { approvalQualification = value; }
        }

        public int Qualification
        {
            get { return qualification; }
            set { qualification = value; }
        }

        public string Link
        {
            get { return link; }
            set { link = value; }
        }

        public string PhoneContactPerson
        {
            get { return phoneContactPerson; }
            set { phoneContactPerson = value; }
        }

        public string ContactPerson
        {
            get { return contactPerson; }
            set { contactPerson = value; }
        }

        public string Content
        {
            get { return content; }
            set { content = value; }
        }

        public string Objective
        {
            get { return objective; }
            set { objective = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public IList Schedules 
        {
            get { return schedules; }
            set { schedules = value; }
        }

        public object Clone()
        {
            TrainingCourseClientData clone = new TrainingCourseClientData();
            clone.ApprovalQualification = this.approvalQualification;
            clone.Code = this.Code;
            clone.ContactPerson = this.contactPerson;
            clone.Content = this.content;
            clone.Link = this.link;
            clone.Name = this.name;
            clone.Objective = this.objective;
            clone.PhoneContactPerson = this.phoneContactPerson;
            clone.Qualification = this.qualification;
            if (this.schedules != null)
                clone.Schedules = new ArrayList(this.schedules);
            clone.Version = this.Version;

            return clone;
        }
    }
}
