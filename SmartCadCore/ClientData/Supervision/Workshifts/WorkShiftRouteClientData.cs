﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class WorkShiftRouteClientData: ClientData
    {

        public int WorkShiftCode { get; set; }

		public string WorkShiftName { get; set; }

		public RouteClientData Route { get; set; }

		public int UnitCode { get; set; }

		public IList WorkShiftShedules { get; set; }

        public override bool Equals(object obj)
        {
            try
            {
                if (obj.GetType() == typeof(WorkShiftRouteClientData))
                {
                    WorkShiftRouteClientData wsRoute = (WorkShiftRouteClientData)obj;
                    if (this.Code == wsRoute.Code)
                    {
                        return true;
                    }
                }
            }
            catch { }
            return false;
        }

    }
}
