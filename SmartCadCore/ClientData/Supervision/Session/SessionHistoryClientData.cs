using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class SessionHistoryClientData : ClientData
    {
        #region Fields

        private string userApplication;

        private DateTime? startDateLogin;

        private DateTime? endDateLogin;

        private bool? isLoggedIn;

        private IList statusHistoryList;

        private string computerName;

        private string numberSeat;

        private string room;

        private int operatorCode;

        #endregion

        #region Properties

        /// <summary>
        /// User application that is or was logged
        /// </summary>        
        public string UserApplication
        {
            get
            {
                return userApplication;
            }
            set
            {
                userApplication = value;
            }
        }

        /// <summary>
        /// Start date of login
        /// </summary>
        public DateTime? StartDateLogin
        {
            get
            {
                return startDateLogin;
            }
            set
            {
                startDateLogin = value;
            }
        }

        /// <summary>
        /// End date of login
        /// </summary>
        public DateTime? EndDateLogin
        {
            get
            {
                return endDateLogin;
            }
            set
            {
                endDateLogin = value;
            }
        }

        /// <summary>
        /// Indicates if the user is logged
        /// </summary>
        public bool? IsLoggedIn
        {
            get
            {
                return isLoggedIn;
            }
            set
            {
                isLoggedIn = value;
            }
        }

        /// <summary>
        /// Contains all session status history entities related to a session.
        /// </summary>
        public IList StatusHistoryList
        {
            get
            {
                return statusHistoryList;
            }
            set
            {
                statusHistoryList = value;
            }
        }

        public string ComputerName
        {
            get
            {
                return computerName;
            }
            set
            {
                computerName = value;
            }
        }

        public string NumberSeat
        {
            get
            {
                return numberSeat;
            }
            set
            {
                numberSeat = value;
            }
        }

        public string Room
        {
            get
            {
                return room;
            }
            set
            {
                room = value;
            }
        }

        public int Operator
        {
            get
            {
                return operatorCode;
            }
            set
            {
                operatorCode = value;
            }
        }
        #endregion
    }
}
