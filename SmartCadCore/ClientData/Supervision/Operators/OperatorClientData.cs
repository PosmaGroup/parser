using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class OperatorClientData : ClientData
    {
        private string firstName;
        private string lastName;
        private string login;
        private string agentId;
        private string agentPassword;
        private IList departmentTypes;
        
        private IList operators;
        private IList supervisors;
        private string roleName;
        private string roleFriendlyName;
        private string personId;

        private string operatorCategory;
        private int operatorCategoryCode;
        private IList categoryList;
        private IList lastSession;
        
        private int roleCode;
        private IList workShifts;
        private string roomName;
        private int roomCode;
        private IList supervisorCodes;
        private bool isSupervisor;
        private byte[] picture;
        private DateTime? birthday;
        private string email;
        private string address;
        private string telephone;

        private bool? windows;
        private IList cameras = new ArrayList();
        private string password;

        public const int MinimunPasswordLength = 6;

        public string PersonID
        {
            get { return personId; }
            set { personId = value; }
        }
	

        public string FirstName
        {
            get
            {
                return firstName;
            }
            set
            {
                firstName = value;
            }
        }

        public string LastName
        {
            get
            {
                return lastName;
            }
            set
            {
                lastName = value;
            }
        }

        public string Login
        {
            get
            {
                return login;
            }
            set 
            {
                login = value;
            }
        }

        public string AgentId
        {
            get
            {
                return agentId;
            }
            set
            {
                agentId = value;
            }
        }

        public string AgentPassword
        {
            get
            {
                return agentPassword;
            }
            set
            {
                agentPassword = value;
            }
        }

        public DateTime? Birthday
        {
            get { return birthday; }
            set { birthday = value; }
        }

        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        public string Address
        {
            get { return address; }
            set { address = value; }
        }

        public string Telephone
        {
            get { return telephone; }
            set { telephone = value; }
        }

        public IList DepartmentTypes
        {
            get
            {
                return departmentTypes;
            }
            set
            {
                departmentTypes = value;
            }
        }

        public string RoleName
        {
            get
            {
                return roleName;
            }
            set
            {
                roleName = value;
            }
        }

        public string RoleFriendlyName
        {
            get
            {
                return roleFriendlyName;
            }
            set
            {
                roleFriendlyName = value;
            }
        }

        public IList Supervisors
        {
            get
            {
                return supervisors;
            }
            set
            {
                supervisors = value;
            }
        }

        public IList Operators
        {
            get
            {
                return operators;
            }
            set
            {
                operators = value;
            }
        }

        

        public IList SupervisorCodes
        {
            get
            {
                return supervisorCodes;
            }
            set
            {
                supervisorCodes = value;
            }
        }

        public string OperatorCategory
        {
            get
            {
                return operatorCategory;
            }
            set
            {
                operatorCategory = value;
            }
        }

        public int OperatorCategoryCode
        {
            get
            {
                return operatorCategoryCode;
            }
            set
            {
                operatorCategoryCode = value;
            }
        }

        public IList CategoryList
        {
            get
            {
                return categoryList;
            }
            set
            {
                categoryList = value;
            }
        }

        public IList LastSessions
        {
            get
            {
                return lastSession;
            }
            set
            {
                lastSession = value;
            }
        }

      
        public IList WorkShifts
        {
            get
            {
                return workShifts;
            }
            set
            {
                workShifts = value;
            }
        }

        public int RoleCode
        {
            get
            {
                return roleCode;
            }
            set
            {
                roleCode = value;
            }
        }

        public string RoomName
        {
            get
            {
                return roomName;
            }
            set
            {
                roomName = value;
            }
        }

        public int RoomCode
        {
            get
            {
                return roomCode;
            }
            set
            {
                roomCode = value;
            }
        }

        public bool IsSupervisor
        {
            get
            {
                return isSupervisor;
            }
            set
            {
                isSupervisor = value;
            }
        }

        public byte []Picture
        {
            get
            {
                return picture;
            }
            set
            {
                picture = value;
            }
        }

        public bool? Windows
        {
            get { return windows; }
            set { windows = value; }
        }

        public IList Cameras
        {
            get
            {
                return cameras;
            }
            set
            {
                cameras = value;
            }
        }

        public string Password
        {
            get
            {
                return password;
            }
            set
            {
                password = value;
            }
        }

        public override string ToString()
        {
            return this.LastName + ", " + this.FirstName;
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            ClientData clientData = obj as ClientData;
            if (clientData != null)
            {
                if (this.Code == clientData.Code)
                    result = true;
            }

            return result;
        }

        public override int GetHashCode()
        {
            return this.GetType().GetHashCode() * 10000000 + this.Code.GetHashCode();
        }

        [InitialDataClient(PropertyName = "Name", PropertyValue = "UserAccountData")]
        public static readonly UserResourceClientData Resource;

        [InitialDataClient(PropertyName = "Login", PropertyValue = "supervisor66666666666666666666666666666666666666666666666666")]
        public static readonly OperatorClientData InternalSupervisor;
    }
}
