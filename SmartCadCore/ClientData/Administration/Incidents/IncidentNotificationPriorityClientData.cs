using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class IncidentNotificationPriorityClientData : ClientData
    {
        #region Fields

        private string name;
        private Color color;
        private byte[] image;
        private string customCode;

        #endregion

        #region Properties
        public virtual string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public virtual Color Color
        {
            get
            {
                return color;
            }
            set
            {
                color = value;
            }
        }

        public virtual byte[] Image
        {
            get
            {
                return image;
            }
            set
            {
                image = value;
            }
        }

        public virtual string CustomCode
        {
            get
            {
                return customCode;
            }
            set
            {
                customCode = value;
            }
        }
	
        #endregion

        #region Overrides

        public override string ToString()
        {
            return this.CustomCode.ToString();
        }

        #endregion
    }
}
