﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.ClientData
{
    public interface IIncidentReference
    {
        int IncidentCode
        {
            get;
            set;
        }
    }
}
