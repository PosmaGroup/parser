﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class IncidentStatusClientData : ClientData
    {
        #region Fields

        private string name;
        private string friendlyName;
        private string customCode;

        #endregion

        #region Properties

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public string FriendlyName
        {
            get
            {
                return friendlyName;
            }
            set
            {
                friendlyName = value;
            }
        }

        public string CustomCode
        {
            get
            {
                return customCode;
            }
            set
            {
                customCode = value;
            }
        }

        #endregion

        public override string ToString()
        {
            string text = "";
            if (this.Name != null)
            {
                text = this.FriendlyName;
            }
            return text;
        }

        [InitialDataClient(PropertyName = "CustomCode", PropertyValue = "CLOSED")]
        public static IncidentStatusClientData Closed;

        [InitialDataClient(PropertyName = "CustomCode", PropertyValue = "OPEN")]
        public static IncidentStatusClientData Open;

        [InitialDataClient(PropertyName = "CustomCode", PropertyValue = "DISPATCHED")]
        public static IncidentStatusClientData Dispatched;
    }
}
