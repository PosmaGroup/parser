using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;


namespace SmartCadCore.ClientData
{
    [Serializable]
    public class DepartmentZoneClientData : ClientData, ICloneable, INamedObjectClientData
    {
        #region Fields
        private bool special;
        private DepartmentTypeClientData deparmentType;
        private IList departmentStations;
        private string customCode;
        private string name;
        private IList departmentZoneAddress;
        #endregion


        #region Properties
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public string CustomCode
        {
            get
            {
                return customCode;
            }
            set
            {
                customCode = value;
            }
        }

        public bool Special
        {
            get
            {
                return special;
            }
            set
            {
                special = value;
            }
        }

        public IList DepartmentStations
        {
            get
            {
                return departmentStations;
            }
            set
            {
                departmentStations = value;
            }
        }

        public IList DepartmentZoneAddress
        {
            get
            {
                return departmentZoneAddress;
            }
            set
            {
                departmentZoneAddress = value;
            }
        }

        public DepartmentTypeClientData DepartmentType
        {
            get
            {
                return deparmentType;
            }
            set
            {
                deparmentType = value;
            }
        }

        #endregion

        #region Overrides

        public override bool Equals(object obj)
        {
            bool result = false;
            DepartmentZoneClientData zone = obj as DepartmentZoneClientData;
            if (zone != null)
            {
                if (this.Code == zone.Code)
                    result = true;
            }

            return result;
        }

        public override int GetHashCode()
        {
            return this.GetType().GetHashCode() * 10000000 + this.Code.GetHashCode();
        }

        public override string ToString()
        {
            return this.Name;
        } 

        #endregion

        #region ICloneable Members

        public object Clone()
        {
            DepartmentZoneClientData zone = new DepartmentZoneClientData();
            zone.Code = this.Code;
            zone.Version = this.Version;
            zone.Name = this.Name;
            zone.Special = this.Special;
            zone.CustomCode = this.CustomCode;
            zone.DepartmentType = this.DepartmentType.Clone() as DepartmentTypeClientData;
            return zone;
        }

        #endregion

        #region Initial data

        [InitialDataClient(PropertyName = "Name", PropertyValue = "DepartmentZoneData")]
        public static readonly UserResourceClientData Resource;

        #endregion
    }
}
