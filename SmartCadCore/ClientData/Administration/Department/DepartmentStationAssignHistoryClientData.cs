using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class DepartmentStationAssignHistoryClientData : ClientData
    {
        private DepartmentStationClientData departmentStation;
        private DateTime startDate;
        private DateTime endDate;
        private IList unitOfficerAssign;

        public DepartmentStationClientData DepartmentStation
        {
            get
            {
                return departmentStation;
            }
            set
            {
                departmentStation = value;
            }
        }

        public DateTime StartDate
        {
            get
            {
                return startDate;

            }
            set
            {
                startDate = value;
            }
        }

        public DateTime EndDate
        {
            get
            {
                return endDate;
            }
            set
            {
                endDate = value;
            }
        }

        public IList UnitOfficerAssign
        {
            get
            {
                return unitOfficerAssign;
            }
            set
            {
                unitOfficerAssign = value;
            }
        }

        //Se usa s�lo para Despacho, es una version ligera de la lista UnitOfficerAssign
        private Dictionary<string, List<OfficerClientData>> units = new Dictionary<string,List<OfficerClientData>>();

        public Dictionary<string, List<OfficerClientData>> Units
        {
            get { return units; }
            set { units = value; }
        }

        [InitialDataClient(PropertyName = "Name", PropertyValue = "DepartmentStationAssignHistoryData")]
        public static readonly UserResourceClientData Resource;
    }
}
