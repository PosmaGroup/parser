using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;


namespace SmartCadCore.ClientData
{
    [Serializable]
    public class DepartmentStationClientData : ClientData, ICloneable, INamedObjectClientData
    {
        #region Properties        

        public string DepartamentTypeName { get; set; }

		public int DepartamentTypeCode { get; set; }

		public string Name { get; set; }

		public string CustomCode { get; set; }

		public DepartmentZoneClientData DepartmentZone { get; set; }

		public IList DepartmentStationAddress { get; set; }

		public bool Points { get; set; }

        public string Telephone { get; set; }

        #endregion

        #region Overrides

        public override bool Equals(object obj)
        {
            bool result = false;
            DepartmentStationClientData station = obj as DepartmentStationClientData;
            if (station != null)
            {
                if (this.Code == station.Code)
                    result = true;
            }

            return result;
        }

        public override int GetHashCode()
        {
            return this.GetType().GetHashCode() * 10000000 + this.Code.GetHashCode();
        }

        public override string ToString()
        {
            return this.Name;
        }

        #endregion

        #region Initial data

        [InitialDataClient(PropertyName = "Name", PropertyValue = "DepartmentStationData")]
        public static readonly UserResourceClientData Resource;

        #endregion

        #region ICloneable Members

        public object Clone()
        {
            DepartmentStationClientData station = new DepartmentStationClientData();
            station.Code = this.Code;
            station.Version = this.Version;
            station.CustomCode = this.CustomCode;
            station.Name = this.Name;
            station.DepartmentZone = this.DepartmentZone;
            station.Telephone = this.Telephone;
            return station;
        }

        #endregion
    }
}
