using System;
using System.Collections;
using System.Text;
using System.Drawing;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class UnitTypeClientData : ClientData
    {
        string name;
        string description;
        DepartmentTypeClientData departmentType;
        string iconName;
        IList units = new ArrayList();
        private IList incidentTypes;
        private int capacity;
        private Color color;
        private double velocity;
        private string image;

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public IList Units
        {
            get
            {
                return units;
            }
            set
            {
                units = value;
            }
        }

        public string IconName
        {
            get
            {
                return iconName;
            }
            set
            {
                iconName = value;
            }
        }

        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }

        public DepartmentTypeClientData DepartmentType
        {
            get
            {
                return departmentType;
            }
            set
            {
                departmentType = value;
            }
        }

        public int Capacity
        {
            get
            {
                return capacity;
            }
            set
            {
                capacity = value;
            }
        }

        public double Velocity
        {
            get
            {
                return velocity;
            }
            set
            {
                velocity = value;
            }
        }

        public Color Color
        {
            get
            {
                return color;
            }
            set
            {
                color = value;
            }
        }

        public IList IncidentTypes
        {
            get
            {
                return incidentTypes;
            }
            set
            {
                incidentTypes = value;
            }
        }

        public string Image
        {
            get
            {
                return image;
            }
            set
            {
                image = value;
            }
        }

        #region Overrides
        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is UnitTypeClientData)
            {
                result = this.Code == ((UnitTypeClientData)obj).Code;
            }
            return result;
        }

        public override string ToString()
        {
            return this.Name;
        }
        #endregion

        [InitialDataClient(PropertyName = "Name", PropertyValue = "UnitTypeData")]
        public static readonly UserResourceClientData Resource;
    }
}
