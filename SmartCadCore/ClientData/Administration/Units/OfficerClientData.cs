using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class OfficerClientData : ClientData
    {
        private string firstName;
        private string lastName;
        private int unitAssignCode = 0;
        private DepartmentTypeClientData departmentType;
        private string personID;
        private string badge;
        private PositionClientData position;
        private DateTime? birthday;
        private string telephone;
        private RankClientData rank;
        private DepartmentStationClientData departmentStation;
        private IList unitsAssigned;

        public virtual string FirstName
        {
            get
            {
                return firstName;
            }
            set
            {
                firstName = value;
            }
        }

        public virtual string LastName
        {
            get
            {
                return lastName;
            }
            set
            {
                lastName = value;
            }
        }

        public int UnitAssignCode
        {
            get
            {
                return unitAssignCode;
            }
            set
            {
                unitAssignCode = value;
            }
        }

        public DepartmentTypeClientData DepartmentType
        {
            get
            {
                return departmentType;
            }
            set
            {
                departmentType = value;
            }
        }

        public string PersonID
        {
            get
            {
                return personID;
            }
            set
            {
                personID = value;
            }
        }

        public string Badge
        {
            get
            {
                return badge;
            }
            set
            {
                badge = value;
            }
        }

        public PositionClientData Position
        {
            get
            {
                return position;
            }
            set
            {
                position = value;
            }
        }

        public DateTime? Birthday
        {
            get
            {
                return birthday;
            }
            set
            {
                birthday = value;
            }
        }

        public string Telephone
        {
            get
            {
                return telephone;
            }
            set
            {
                telephone = value;
            }
        }

        public RankClientData Rank
        {
            get
            {
                return rank;
            }
            set
            {
                rank = value;
            }
        }
        
        public IList UnitsAssigned
        {
            get
            {
                return unitsAssigned;
            }
            set
            {
                unitsAssigned = value;
            }
        }

        public DepartmentStationClientData DepartmentStation
        {
            get
            {
                return departmentStation;
            }
            set
            {
                departmentStation = value;
            }
        }

        public override string ToString()
        {
            string text = "";
            if (this.FirstName != null)
                text += this.firstName;
            if (this.LastName != null)
            {
                if (this.FirstName != null)
                    text += " ";
                text += this.LastName;
            }
            return text;
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is OfficerClientData)
            {
                result = this.Code == ((OfficerClientData)obj).Code;
            }
            return result;
        }

        [InitialDataClient(PropertyName = "Name", PropertyValue = "OfficerData")]
        public static readonly UserResourceClientData Resource;
    }
}
