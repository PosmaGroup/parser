using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class UnitClientData : ClientData, ICloneable
    {
		public enum DEVICE_TYPE
        {
            GPS = 0,
            DVR = 1
		}

		#region Properties

		public string CustomCode { get; set; }

		public string IconName { get; set; }

		public DepartmentStationClientData DepartmentStation { get; set; }

		public DepartmentTypeClientData DepartmentType { get; set; }

		public string Brand { get; set; }

		public string Model { get; set; }

		public int? Year { get; set; }

		public int Number { get; set; }

		public UnitTypeClientData Type { get; set; }

		public UnitStatusClientData Status { get; set; }

		public string Description { get; set; }

		public int Capacity { get; set; }

		public IList IncidentTypes { get; set; }

		public IList StatusHistoryList { get; set; }

		public IList OfficerAssigns { get; set; }

		public string IdRadio { get; set; }

		public int GPSCode { get; set; }

		public string IdGPS { get; set; }

		public double Lon { get; set; }

		public double Lat { get; set; }

		public double Speed { get; set; }

		public DateTime CoordinatesDate { get; set; }

		public int Satellites { get; set; }

		public double Heading { get; set; }

		public DEVICE_TYPE DeviceType { get; set; }

		public string IdUnit { get; set; }

		public string Serial { get; set; }

		public double Velocity { get; set; }

		public DispatchOrderClientData DispatchOrder { get; set; }

		public double Distance { get; set; }

		public bool IsSynchronized { get; set; }

		public IList WorkShiftsRoutes { get; set; }

		#endregion

		#region Overrides
		public override string ToString()
        {
            return this.CustomCode;
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            UnitClientData objectData = obj as UnitClientData;
            if (objectData != null && this.Code == objectData.Code)
				result = true;

            return result;
		}

		#endregion

		#region ICloneable Members

		public object Clone()
        {
            UnitClientData retval = new UnitClientData();
            retval.Code = this.Code;
            retval.CustomCode = this.CustomCode;
            retval.DepartmentStation = this.DepartmentStation.Clone() as DepartmentStationClientData;
            retval.DispatchOrder = this.DispatchOrder;
            retval.Distance = this.Distance;
            retval.IconName = this.IconName;
            retval.IdRadio = this.IdRadio;
            retval.IncidentTypes = this.IncidentTypes;
            retval.Lat = this.Lat;
            retval.Lon = this.Lon;
            retval.IsSynchronized = this.IsSynchronized;
            retval.OfficerAssigns = this.OfficerAssigns;
            retval.Status = this.Status;
            retval.Type = this.Type;
            retval.Velocity = this.Velocity;
			retval.Brand = this.Brand;
			retval.DepartmentType = this.DepartmentType;
			retval.Model = this.Model;
			retval.WorkShiftsRoutes = this.WorkShiftsRoutes;

            return retval;
        }

        #endregion

        #region Initial data

        [InitialDataClient(PropertyName = "Name", PropertyValue = "UnitData")]
        public static readonly UserResourceClientData Resource;

        #endregion
    }
}
