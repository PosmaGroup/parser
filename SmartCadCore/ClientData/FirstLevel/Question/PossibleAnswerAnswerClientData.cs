using System;
using System.Collections.Generic;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class PossibleAnswerAnswerClientData : ClientData
    {
        private string textAnswer;
        private QuestionPossibleAnswerClientData questionPossibleAnswer;
        private int reportAnswerCode;
        
        public string TextAnswer
        {
            get
            {
                return this.textAnswer;
            }
            set
            {
                this.textAnswer = value;
            }
        }

        public QuestionPossibleAnswerClientData QuestionPossibleAnswer
        {
            get
            {
                return this.questionPossibleAnswer;
            }
            set
            {
                this.questionPossibleAnswer = value;
            }
        }

        public int ReportAnswerCode
        {
            get
            {
                return this.reportAnswerCode;
            }
            set
            {
                this.reportAnswerCode = value;
            }
        }

		public override bool Equals(object obj)
		{
			if (obj is PossibleAnswerAnswerClientData == false)
				return false;
			PossibleAnswerAnswerClientData paacd = obj as PossibleAnswerAnswerClientData;
			if (paacd.QuestionPossibleAnswer.Code == this.QuestionPossibleAnswer.Code &&
				paacd.reportAnswerCode == this.reportAnswerCode)
				return true;
			return false;
		}
    }
}
