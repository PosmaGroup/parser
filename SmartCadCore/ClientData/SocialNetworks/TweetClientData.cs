﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class TweetClientData: ClientData
    {

        public TweetClientData()
        {
        }

        public string CustomCode
        {
            get;
            set;
        }

        public AlarmTwitterClientData Alarm
        {
            get;
            set;
        }

        public DateTime Published
        {
            get;
            set;
        }

        public virtual string Content
        {
            get;
            set;
        }

        public virtual string AuthorName
        {
            get;
            set;
        }

        public virtual string AuthorUserName
        {
            get;
            set;
        }

        public virtual string AuthorUri
        {
            get;
            set;
        }

        public virtual string ImageUrl
        {
            get;
            set;
        }

        public virtual string Link
        {
            get;
            set;
        }


        public override string ToString()
        {
            return Content;
        }

    }

}

