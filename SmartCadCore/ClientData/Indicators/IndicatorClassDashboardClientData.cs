using System;
using System.Collections.Generic;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class IndicatorClassDashboardClientData : ClientData
    {
        private string indicatorName;
        private string indicatorDescription;
        private string indicatorTypeFriendlyName;
        private string indicatorTypeName;
        private string className;
        private string classFriendlyName;
        private bool showDashboard;
        private string indicatorCustomCode;

        public string IndicatorName
        {
            get { return indicatorName; }
            set { indicatorName = value; }
        }

        public string IndicatorDescription
        {
            get { return indicatorDescription; }
            set { indicatorDescription= value; }
        }

        public string IndicatorTypeFriendlyName
        {
            get { return indicatorTypeFriendlyName; }
            set { indicatorTypeFriendlyName = value; }
        }

        public string IndicatorTypeName
        {
            get { return indicatorTypeName; }
            set { indicatorTypeName = value; }
        }

        public string ClassName
        {
            get { return className; }
            set { className = value; }
        }

        public string ClassFriendlyName
        {
            get { return classFriendlyName; }
            set { classFriendlyName = value; }
        }

        public bool ShowDashboard
        {
            get { return showDashboard; }
            set { showDashboard = value; }
        }

        public string IndicatorCustomCode
        {
            get
            {
                return indicatorCustomCode;
            }
            set
            {
                indicatorCustomCode = value;
            }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is IndicatorClassDashboardClientData)
            {
                IndicatorClassDashboardClientData icdcd = (IndicatorClassDashboardClientData)obj;
                result = this.IndicatorCustomCode == icdcd.IndicatorCustomCode && this.ClassName == icdcd.ClassName;
            }
            return result;
        }

        public override int GetHashCode()
        {
            return this.GetType().GetHashCode() * 10000000 + this.IndicatorCustomCode.GetHashCode() + this.Code.GetHashCode();
        }

        public override string ToString()
        {
            return this.IndicatorName;
        }
    }
}
