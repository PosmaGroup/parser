using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class IndicatorResultClientData : ClientData
    {
        #region Fields
        
        private string indicatorName;
        private string indicatorMnemonic;
        private DateTime time;
        //private string indicatorClassName;
        //private string indicatorClassFriendlyName;
        private IList resultValues;
        private int frecuency;
        private string measureUnit;
        private string indicatorTypeFriendlyName;
        //private int customCode;
        private string indicatorTypeName;
        private string indicatorCustomCode;
        private int indicatorCode;

        #endregion

        #region Constructors

        public IndicatorResultClientData()
        {
        }

        public IndicatorResultClientData(IndicatorResultClientData data, bool withValues)
        {
            this.Code = data.Code;
            this.IndicatorCustomCode = data.IndicatorCustomCode;
            this.IndicatorFrecuency = data.IndicatorFrecuency;
            this.IndicatorMeasureUnit = data.IndicatorMeasureUnit;
            this.IndicatorMnemonic = data.IndicatorMnemonic;
            this.IndicatorName = data.IndicatorName;
            this.IndicatorTypeFriendlyName = data.IndicatorTypeFriendlyName;
            this.IndicatorTypeName = data.IndicatorTypeName;
            this.Time = data.Time;
            if (withValues == true)
            {
                this.Values = data.Values;
            }
            else
            {
                this.Values = new ArrayList();
            }
            this.Version = data.Version;
        }

        #endregion

        #region Properties

        public string IndicatorName
        {
            get
            {
                return indicatorName;
            }
            set
            {
                indicatorName = value;
            }
        }

        //public string IndicatorClassFriendlyName
        //{
        //    get
        //    {
        //        return indicatorClassFriendlyName;
        //    }
        //    set
        //    {
        //        indicatorClassFriendlyName = value;
        //    }
        //}

        public string IndicatorMnemonic
        {
            get
            {
                return indicatorMnemonic;
            }
            set
            {
                indicatorMnemonic = value;
            }
        }

        //public string IndicatorClassName
        //{
        //    get
        //    {
        //        return indicatorClassName;
        //    }
        //    set
        //    {
        //        indicatorClassName = value;
        //    }
        //}

        public DateTime Time
        {
            get
            {
                return time;
            }
            set
            {
                time = value;
            }
        }

        public IList Values
        {
            get
            {
                return resultValues;
            }
            set
            {
                resultValues = value;
            }
        }

        public string IndicatorTypeFriendlyName
        {
            get
            {
                return indicatorTypeFriendlyName;
            }
            set
            {
                indicatorTypeFriendlyName = value;
            }
        }

        public string IndicatorTypeName
        {
            get
            {
                return indicatorTypeName;
            }
            set
            {
                indicatorTypeName = value;
            }
        }

        public int IndicatorFrecuency
        {
            get
            {
                return frecuency;
            }
            set
            {
                frecuency = value;
            }
        }
        public string IndicatorMeasureUnit
        {
            get
            {
                return measureUnit;
            }
            set
            {
                measureUnit = value;
            }
        }

        
        public string IndicatorCustomCode
        {
            get
            {
                return indicatorCustomCode;
            }
            set
            {
                indicatorCustomCode = value;
            }
        }
        public int IndicatorCode
        {
            get
            {
                return indicatorCode;
            }
            set
            {
                indicatorCode = value;
            }
        }
        //public int CustomCode
        //{
        //    get
        //    {
        //        return customCode;
        //    }
        //    set
        //    {
        //        customCode = value;
        //    }
        //}
        #endregion
    }
}
