using System;
using System.Collections.Generic;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class IndicatorResultValuesClientData : ClientData
    {
         #region Fields

        private int customCode;
        private int threshold;
        private string resultValue;
        private int trend;
        private int indicatorResultCode;
        private int indicatorResultClassCode;
        private string indicatorClassName;
        private string indicatorClassFriendlyName;
        private string indicatorName;
        private string indicatorDescription;
        private string indicatorTypeName;
        private string indicatorTypeFriendlyName;
        private DateTime date;
        private string information;
        private string measureUnit;
        private string description;
        private string indicatorCustomCode;
        private bool locationGrid;
        #endregion



        #region Properties
        public bool LocationGrid
        {
            get
            {
                return locationGrid;
            }
            set
            {
                locationGrid = value;
            }
        }

        public string IndicatorClassFriendlyName
        {
            get
            {
                return indicatorClassFriendlyName;
            }
            set
            {
                indicatorClassFriendlyName = value;
            }
        }
        public string IndicatorClassName
        {
            get
            {
                return indicatorClassName;
            }
            set
            {
                indicatorClassName = value;
            }
        }
        public string IndicatorTypeFriendlyName
        {
            get
            {
                return indicatorTypeFriendlyName;
            }
            set
            {
                indicatorTypeFriendlyName = value;
            }
        }
        public string IndicatorTypeName
        {
            get
            {
                return indicatorTypeName;
            }
            set
            {
                indicatorTypeName = value;
            }
        }
        public string IndicatorName
        {
            get
            {
                return indicatorName;
            }
            set
            {
                indicatorName = value;
            }
        }

        public string IndicatorDescription
        {
            get
            {
                return indicatorDescription;
            }
            set
            {
                indicatorDescription = value;
            }
        }


        public string IndicatorCustomCode
        {
            get
            {
                return indicatorCustomCode;
            }
            set
            {
                indicatorCustomCode = value;
            }
        }
        public int CustomCode
        {
            get
            {
                return customCode;
            }
            set
            {
                customCode = value;
            }
        }
        public int Threshold
        {
            get
            {
                return threshold;
            }
            set
            {
                threshold = value;
            }
        }
        public string ResultValue
        {
            get
            {
                return resultValue;
            }
            set
            {
                resultValue = value;
            }
        }
        public string MeasureUnit
        {
            get
            {
                return measureUnit;
            }
            set
            {
                measureUnit = value;
            }
        }
        public DateTime Date
        {
            get 
            {
                return date;
            }

            set 
            {
                date = value;
            }
        }
        public int Trend
        {
            get
            {
                return trend;
            }
            set
            {
                trend = value;
            }
        }        
        public int IndicatorResultCode
        {
            get { return indicatorResultCode; }
            set { indicatorResultCode = value; }
        }
        public int IndicatorResultClassCode
        {
            get { return indicatorResultClassCode; }
            set { indicatorResultClassCode = value; }
        }
        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }
        public string Information
        {
            get { return information; }
            set { information = value; }
        }
        #endregion
        
    }
}
