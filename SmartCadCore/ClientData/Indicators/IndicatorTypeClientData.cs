﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class IndicatorTypeClientData : ClientData
    {
        private string name;
        private string friendlyName;
        private string description;
        
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public virtual string FriendlyName
        {
            get
            {
                return friendlyName;
            }
            set
            {
                friendlyName = value;
            }
        }

        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is IndicatorTypeClientData)
            {
                result = this.Code == ((IndicatorTypeClientData)obj).Code;
            }
            return result;
        }

        [InitialDataClient(PropertyName = "Name", PropertyValue = "CALLS")]
        public static IndicatorTypeClientData Calls;

        [InitialDataClient(PropertyName = "Name", PropertyValue = "FIRSTLEVEL")]
        public static IndicatorTypeClientData FirstLevel;

        [InitialDataClient(PropertyName = "Name", PropertyValue = "DISPATCH")]
        public static IndicatorTypeClientData Dispatch;
    }
}
