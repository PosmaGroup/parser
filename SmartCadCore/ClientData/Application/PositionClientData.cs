﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class PositionClientData : ClientData, ICloneable
    {
        #region Fields
        private string name;

        private string description;

        private string departmentTypeName;

        private int departmentTypeCode;

        #endregion

        #region Properties
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }

        public string DepartmentTypeName
        {
            get
            {
                return departmentTypeName;
            }
            set
            {
                departmentTypeName = value;
            }
        }

        public int DepartmentTypeCode
        {
            get
            {
                return departmentTypeCode;
            }
            set
            {
                departmentTypeCode = value;
            }
        }
        
        #endregion

        #region Overrides
        public override bool Equals(object obj)
        {
            bool result = false;
            PositionClientData position = obj as PositionClientData;
            if (position != null)
            {
                if (this.Code == position.Code)
                    result = true;
            }

            return result;
        }

        public override int GetHashCode()
        {
            return this.GetType().GetHashCode() * 10000000 + this.Code.GetHashCode();
        }

        public override string ToString()
        {
            return this.Name;
        } 
        #endregion

        #region ICloneable Members

        public object Clone()
        {
            PositionClientData position = new PositionClientData();
            position.Code = this.Code;
            position.Version = this.Version;
            position.Name = this.Name;
            position.Description = this.Description;
            position.DepartmentTypeName = this.DepartmentTypeName;
            position.DepartmentTypeCode = this.DepartmentTypeCode;
            return position;
        }

        #endregion

        #region Initial Data
        [InitialDataClient(PropertyName = "Name", PropertyValue = "PositionData")]
        public static readonly UserResourceClientData Resource;
        #endregion
    }
}
