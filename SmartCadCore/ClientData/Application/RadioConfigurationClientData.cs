﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace SmartCadCore.ClientData
{
    [Serializable]
    public class RadioConfigurationClientData : ClientData
    {
        #region Properties
        public int Extension { get; set; }
        public string FrecuencyCode { get; set; }
        public string TalkCode { get; set;} 
        public DepartmentTypeClientData Department { get; set; }
        public string Description { get; set; }
        #endregion

        [InitialDataClient(PropertyName = "Name", PropertyValue = "RadioConfigurationData")]
        public static readonly UserResourceClientData Resource;
    }
}
