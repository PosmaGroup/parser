using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class PhoneReportAudioClientData : ClientData
    {
        public string Custom_Code { get; set; }
        public string FileName { get; set; }
        public string StorageFolder { get; set; }
    }
}
