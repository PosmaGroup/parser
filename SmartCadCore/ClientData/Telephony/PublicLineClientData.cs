﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class PublicLineClientData : ClientData
    {
        #region Fields

        private string telephone;

        private string name;

        private AddressClientData address;

        private bool _public;

        private string callerName;

        private AddressClientData callerAddress;

        private DateTime? updated = null;

        #endregion

        #region Constructors

        public PublicLineClientData()
        {
        }

        public PublicLineClientData(string name, string telephone, AddressClientData address)
        {
            this.name = name;
            this.telephone = telephone;
            this.address = address;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Telephone number of the phone line calling
        /// </summary>
        public string Telephone
        {
            get
            {
                return telephone;
            }
            set
            {
                telephone = value;
            }
        }

        /// <summary>
        /// Owner's name of the phone line calling
        /// </summary>
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        /// <summary>
        /// Address of the number calling.
        /// </summary>
        public AddressClientData Address
        {
            get
            {
                return address;
            }
            set
            {
                address = value;
            }
        }

        /// <summary>
        /// Indicates if the number is a public phone.
        /// </summary>
        public bool Public
        {
            get
            {
                return _public;
            }
            set
            {
                _public = value;
            }
        }

        /// <summary>
        /// The name of the caller or the owner of the line.
        /// </summary>
        public virtual string CallerName
        {
            get
            {
                return callerName;
            }
            set
            {
                callerName = value;
            }
        }

        /// <summary>
        /// The address of the person calling or the address of the line.
        /// </summary>
        public virtual AddressClientData CallerAddress
        {
            get
            {
                return callerAddress;
            }
            set
            {
                callerAddress = value;
            }
        }

        /// <summary>
        /// Last time the record was updated.
        /// </summary>
        public DateTime? Updated
        {
            get
            {
                return updated;
            }
            set
            {
                updated = value;
            }
        }
        #endregion
    }
}
