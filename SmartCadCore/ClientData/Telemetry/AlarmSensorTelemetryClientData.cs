using System;
using System.Collections.Generic;
using System.Text;

namespace SmartCadCore.ClientData
{

    public class AlarmSensorTelemetryClientData : ClientData
    {
        public virtual DateTime? StartAlarm
        {
            get;
            set;
        }

        public virtual int IdSensorTelemetry
        {
            get;
            set;
        }

        public virtual SensorTelemetryClientData SensorTelemetry
        {
            get;
            set;
        }

        public double Value
        {
            get;
            set;
        }

        public virtual DateTime? EndAlarm
        {
            get;
            set;
        }

        public virtual string Observation
        {
            get;
            set;
        }

        public OperatorClientData Operator
        {
            get;
            set;
        }


        [InitialDataClient(PropertyName = "Name", PropertyValue = "AlarmSensorTelemetryClientData")]
        public static readonly UserResourceClientData Resource;

        public override string ToString()
        {
            return IdSensorTelemetry.ToString();
        }

        public override bool Equals(object obj)
        {
            try
            {
                if (obj.GetType() == typeof(AlarmSensorTelemetryClientData))
                {
                    AlarmSensorTelemetryClientData cam = (AlarmSensorTelemetryClientData)obj;
                    if (this.IdSensorTelemetry == cam.IdSensorTelemetry)
                    {
                        return true;
                    }
                }
            }
            catch { }
            return false;
        }

    }
}