using System;
using System.Collections.Generic;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class SensorTelemetryClientData : ClientData
    {
        private string name;
        private string variable;
        private DataloggerClientData myDatalogger;

        public SensorTelemetryClientData()
        { }
        
        
        public DataloggerClientData Datalogger
        {
            get
            {
                return myDatalogger;
            }
            set
            {
                myDatalogger = value;
            }
        }

       
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                if (value != "")
                    name = value;
            }
        }

        public string Variable
        {
            get
            {
                return variable;
            }
            set
            {
                if (value != "")
                    variable = value;
            }
        }

        public DateTime? ValueDate { get; set; }
        public double? Value { get; set; }


        public List<SensorTelemetryThresholdClientData> Threshold
        {
            get;
            set;
        }

        [InitialDataClient(PropertyName = "Name", PropertyValue = "SensorTelemetryData")]
        public static readonly UserResourceClientData Resource;

        public override string ToString()
        {
            return Name;
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() == typeof(SensorTelemetryClientData))
            {
                SensorTelemetryClientData sen = (SensorTelemetryClientData)obj;
                if (this.Name == sen.Name && this.Variable == sen.Variable)
                {
                    return true;
                }
            }
            return false;
        }
    }

    [Serializable]
    public class SensorTelemetryThresholdClientData : ClientData
    {
        public SensorTelemetryClientData SensorTelemetry { get; set; }
        public double? High { get; set; }
        public double? Low { get; set; }

        [InitialDataClient(PropertyName = "Name", PropertyValue = "SensorTelemetryThresholdData")]
        public static readonly UserResourceClientData Resource;

        public override string ToString()
        {
            return Low + "-" + High;
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() == typeof(SensorTelemetryThresholdClientData))
            {
                SensorTelemetryThresholdClientData sen = (SensorTelemetryThresholdClientData)obj;
                if (this.Code == sen.Code)
                {
                    return true;
                }
            }
            return false;
        }
    }


}
