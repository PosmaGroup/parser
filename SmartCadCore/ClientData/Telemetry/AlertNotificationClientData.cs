﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace SmartCadCore.ClientData
{
	[Serializable]
	public class AlertNotificationClientData : ClientData
	{
		public enum AlertStatus
		{
			NotTaken = 0,
			InProcess,
			Ended
		}
		
		#region Constructor
		public AlertNotificationClientData()
		{

		}
		#endregion

		#region Properties
		public OperatorClientData AttendingOperator { get; set; }

		public UnitAlertClientData UnitAlert { get; set; }

		public DateTime Date { get; set; }

		public AlertStatus Status{ get; set; }

		public IList Observations { get; set; }

		public string SensorName { get; set; }

		public int SensorCode { get; set; }
		#endregion

		#region Overrides
		public override bool Equals(object obj)
		{
			if (obj is AlertNotificationClientData == false)
				return false;
			AlertNotificationClientData and = obj as AlertNotificationClientData;
			if (and.UnitAlert.Code == UnitAlert.Code &&
				and.Date == Date)
				return true;
			return false;
		}
		#endregion

		public DateTime GetFinishDate()
		{
			if (Observations != null && Status == AlertStatus.Ended && Observations.Count > 0)
				Observations.Cast<AlertNotificationObservationClientData>().OrderByDescending(obs => obs.Date).First();
			return DateTime.MaxValue;
		}
	}
}
