using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class DataloggerClientData : VideoDeviceClientData
    {

        public DataloggerClientData()
        { }

        public DataloggerClientData(string frame)
        {
            SensorTelemetrys = new List<SensorTelemetryClientData>();
            string[] data = frame.Split('|');
            
            CustomCode = int.Parse(data[1]);
            DateTime datetime = DateTime.ParseExact(data[2], "yyyyMMdd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

            for (int k = 3; k < data.Length; k++)
            {
                string[] varValuePair = data[k].Split('=');

                SensorTelemetryClientData stcd = new SensorTelemetryClientData();
                stcd.Variable = varValuePair[0];
                stcd.Value = double.Parse(varValuePair[1]);
                stcd.ValueDate = datetime;

                SensorTelemetrys.Add(stcd);
            }
            ValuesUpdate = true;
        }

        public DataloggerClientData(string frame, DataloggerClientData datalogger)
        {
            SensorTelemetrys = new List<SensorTelemetryClientData>();
            string[] data = frame.Split('|');

            CustomCode = int.Parse(data[1]);
            DateTime datetime = DateTime.ParseExact(data[2], "yyyyMMdd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

            for (int k = 3; k < data.Length; k++)
            {

                string[] varValuePair = data[k].Split('=');

                SensorTelemetryClientData stcd = new SensorTelemetryClientData();
                //Validación para la fecha contra los sensores dates.
                foreach (SensorTelemetryClientData sensorDb in datalogger.SensorTelemetrys)
                {
                    if (sensorDb.Variable == varValuePair[0])
                    {
                        if (datetime >= sensorDb.ValueDate)
                        {
                            stcd.Variable = varValuePair[0];
                            stcd.Value = double.Parse(varValuePair[1].Split(';')[0]);
                            stcd.ValueDate = datetime;

                            SensorTelemetrys.Add(stcd);
                        }
                    };
                }

            }
            ValuesUpdate = true;
        }


        public bool ValuesUpdate { get; set; }
        public int CustomCode { get; set; }
       
        public List<SensorTelemetryClientData> SensorTelemetrys
        {
            get;
            set;
        }
       
        [InitialDataClient(PropertyName = "Name", PropertyValue = "DataloggerData")]
        public static readonly UserResourceClientData Resource;

        public override string ToString()
        {
            return Name;
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() == typeof(DataloggerClientData))
            {
                DataloggerClientData sen = (DataloggerClientData)obj;
                if (this.Code == sen.Code)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
