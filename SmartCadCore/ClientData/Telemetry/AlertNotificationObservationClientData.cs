﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartCadCore.ClientData
{
	[Serializable]
	public class AlertNotificationObservationClientData : ClientData
	{
		#region Constructor

		public AlertNotificationObservationClientData()
		{

		}

		#endregion

		#region Properties

		public virtual AlertNotificationClientData AlertNotification { get; set; }

		public string Text { get; set; }

		public DateTime Date { get; set; }

		public OperatorClientData Operator { get; set; }
		#endregion
	}
}
