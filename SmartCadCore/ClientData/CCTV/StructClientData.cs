using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class StructClientData : ClientData
    {
        private string name;
        private double lat;
        private double lon;
        private string zone;
        private string street;
        private string town;
        private string state;
        private CctvZoneClientData cctvZone;
        private StructTypeClientData type;
        private int camerasNumber;
        private IList devices;
        private bool isSynchronized;

        public double Lat
        {
            get { return lat; }
            set { lat = value; }
        }
        public double Lon
        {
            get { return lon; }
            set { lon = value; }
        }
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }
        public string ZoneSecRef
        {
            get
            {
                return zone;
            }
            set
            {
                zone = value;
            }
        }
        public string Street
        {
            get
            {
                return street;
            }
            set
            {
                street = value;
            }
        }
        public string State
        {
            get
            {
                return state;
            }
            set
            {
                state = value;
            }
        }
        public string Town
        {
            get
            {
                return town;
            }
            set
            {
                town = value;
            }
        }
        public CctvZoneClientData CctvZone
        {
            get
            {
                return cctvZone;
            }
            set
            {
                cctvZone = value;
            }
        }

        public StructTypeClientData Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value;
            }
        }

        public int CamerasNumber
        {
            get
            {
                return camerasNumber;
            }
            set
            {
                camerasNumber = value;
            }
        }
        public IList Devices
        {
            get
            {
                return devices;
            }
            set
            {
                devices = value;
            }
        }

        public bool IsSynchronized
        {
            get
            {
                return isSynchronized;
            }
            set
            {
                isSynchronized = value;
            }
        }
            
        public override string ToString()
        {
            return name;
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() == typeof(StructClientData))
            {
                StructClientData stc = (StructClientData)obj;
                if (this.Code == stc.Code)
                {
                    return true;
                }
            }
            return false;
        }

        [InitialDataClient(PropertyName = "Name", PropertyValue = "StructData")]
        public static readonly UserResourceClientData Resource;
    }
}
