using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class StructTypeClientData : ClientData
    {
        private string name;
       

      
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }
       
        public override string ToString()
        {
            return name;
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() == typeof(StructTypeClientData))
            {
                StructTypeClientData stc = (StructTypeClientData)obj;
                if (this.Name == stc.Name)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
