using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class CctvReportClientData : ClientData, IIncidentReference
    {
        #region Fields

        private IList incidentTypesCodes;
        private string incidentTypesText;
        private int incidentCode;
        private IList incidentNotifications;       
        private string incidentCustomCode;
        private DateTime eventTime;    
        private string xml;
        private string customCode;                       
        private IList answers;
        private CameraClientData camera;
        private DateTime startDate;
        private IList reportBaseDepartmentTypesClient;
        private string operatorLogin;
        private bool multipleOrganisms;
       


        #endregion

        #region Properties
       
        public IList ReportBaseDepartmentTypesClient
        {
            get
            {
                return this.reportBaseDepartmentTypesClient;
            }
            set
            {
                this.reportBaseDepartmentTypesClient = value;
            }
        }
        public CameraClientData Camera
        {
            get
            {
                return this.camera;
            }
            set
            {
                this.camera = value;
            }
        }
        public DateTime StartDate
        {
            get
            {
                return this.startDate;
            }
            set
            {
                this.startDate = value;
            }
        }
        public string IncidentTypesText
        {
            get
            {
                return this.incidentTypesText;
            }
            set
            {
                this.incidentTypesText = value;
            }
        }
        public IList Answers
        {
            get
            {
                return this.answers;
            }
            set
            {
                this.answers = value;
            }
        }            
        #region IIncidentReference Members

        public int IncidentCode
        {
            get
            {
                return this.incidentCode;
            }
            set
            {
                this.incidentCode = value;
            }
        }

        #endregion        

        public IList IncidentTypesCodes
        {
            get
            {
                return incidentTypesCodes;
            }
            set
            {
                incidentTypesCodes = value;
            }
        }      
        public string Xml
        {
            get
            {
                return xml;
            }
            set
            {
                xml = value;
            }
        }
        public string CustomCode
        {
            get
            {
                return customCode;
            }
            set
            {
                customCode = value;
            }
        }
        public string OperatorLogin
        {
            get
            {
                return operatorLogin;
            }
            set
            {
                operatorLogin = value;
            }
        }
        public bool MultipleOrganisms
        {
            get
            {
                return multipleOrganisms;
            }
            set
            {
                multipleOrganisms = value;
            }
        }
        public IList IncidentNotifications
        {
            get
            {
                return incidentNotifications;
            }
            set
            {
                incidentNotifications = value;
            }
        }
        public string IncidentCustomCode
        {
            get
            {
                return this.incidentCustomCode;
            }
            set
            {
                this.incidentCustomCode = value;
            }
        }

        #endregion


        #region doc
        //private DateTime hangedUpCallTime;
        //private DateTime pickedUpCallTime;
        //private DateTime receivedCallTime;
        //private PhoneReportCallerClientData phoneReportCallerClient;
        //private bool incomplete;
        //public DateTime ReceivedCallTime
        //{
        //    get
        //    {
        //        return this.receivedCallTime;
        //    }
        //    set
        //    {
        //        this.receivedCallTime = value;
        //    }
        //}

        //public bool Incomplete
        //{
        //    get
        //    {
        //        return this.incomplete;
        //    }
        //    set
        //    {
        //        this.incomplete = value;
        //    }
        //}

        //public DateTime HangedUpCallTime
        //{
        //    get
        //    {
        //        return this.hangedUpCallTime;
        //    }
        //    set
        //    {
        //        this.hangedUpCallTime = value;
        //    }
        //}

        //public DateTime PickedUpCallTime
        //{
        //    get
        //    {
        //        return this.pickedUpCallTime;
        //    }
        //    set
        //    {
        //        this.pickedUpCallTime = value;
        //    }
        //}

        //public PhoneReportCallerClientData PhoneReportCallerClient
        //{
        //    get
        //    {
        //        return this.phoneReportCallerClient;
        //    }
        //    set
        //    {
        //        this.phoneReportCallerClient = value;
        //    }
        //}
        #endregion doc

    }
}
