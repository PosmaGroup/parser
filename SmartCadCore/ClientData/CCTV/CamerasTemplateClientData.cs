using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class CamerasTemplateClientData : ClientData
    {
        public string Name { get; set; }
        public IList Cameras { get; set; }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is CamerasTemplateClientData)
            {
                result = this.Name == ((CamerasTemplateClientData)obj).Name;
            }
            return result;
        }

        public override string ToString()
        {
            return this.Name;
        }

        #region Initial data

        [InitialDataClient(PropertyName = "Name", PropertyValue = "CamerasTemplateData")]
        public static readonly UserResourceClientData Resource;

        #endregion
    }
}
