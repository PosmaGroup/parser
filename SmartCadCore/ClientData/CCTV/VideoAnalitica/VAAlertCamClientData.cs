﻿using NHibernate.Mapping.Attributes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Iesi.Collections;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class VAAlertCamClientData : ClientData
    {

        #region Fields

        private CameraClientData camera;

        private DateTime alertDate;

        private int status;

        private int externalId;


        private VARuleClientData rule;

        private VAAlertVideoClientData videoAlert;

        #endregion


        #region Properties

        
        public virtual CameraClientData Camera
        {
            get { return camera;}
            set { camera = value; }
        }

        public virtual DateTime AlertDate
        {
            get { return alertDate;}
            set { alertDate = value; }
        }

        public virtual int Status
        {
            get { return status;}
            set { status = value; }
        }

        public virtual int ExternalId
        {
            get { return externalId;}
            set { externalId = value; }
        }

        public virtual VARuleClientData Rule
        {
            get { return rule;}
            set { rule = value; }
        }

        public virtual VAAlertVideoClientData VideoAlert
        {
            get { return videoAlert;}
            set { videoAlert = value; }
        }

        #endregion

        #region InitialData

        [InitialDataClient(PropertyName = "Name", PropertyValue = "VAAlertCamData")]
        public static readonly UserResourceClientData Resource;
        #endregion

    }
}