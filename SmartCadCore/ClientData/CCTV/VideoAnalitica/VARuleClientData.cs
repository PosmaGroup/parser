using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using NHibernate.Mapping.Attributes;

namespace SmartCadCore.ClientData
{
    [Serializable]

    public class VARuleClientData : ClientData
    {

        #region Fields

        private string name;
        private string saviCode;
        #endregion

        #region Properties

        public virtual string Name
        {
            get { return name; }
            set { name = value; }
        }


        public virtual string SaviCode
        {
            get { return saviCode; }
            set { saviCode = value; }
        }
        

        public override string ToString()
        {
            return name;
        }
     
        public override bool Equals(object obj)
        {
            if (obj.GetType() == typeof(VARuleClientData))
            {
                VARuleClientData stc = (VARuleClientData)obj;
                if (this.Code == stc.Code)
                {
                    return true;
                }
            }
            return false;
        }
        #endregion

        #region InitialData

        [InitialDataClient(PropertyName = "Name", PropertyValue = "VARuleData")]
        public static readonly UserResourceClientData Resource;
        #endregion
    }
}
