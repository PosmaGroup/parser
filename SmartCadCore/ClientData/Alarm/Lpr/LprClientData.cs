using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace SmartCadCore.ClientData
{    
    [Serializable]   
    public class LprClientData : VideoDeviceClientData
    {
        private LprTypeClientData type;
        private int laneOne;
        private int laneTwo;
        private string serial;
        private LprWayClientData way;

      
        public int LaneOne
        {
            get
            {
                return laneOne;
            }
            set
            {
                laneOne = value;
            }
        }        
        public int LaneTwo
        {
            get
            {
                return laneTwo;
            }
            set
            {
                laneTwo = value;
            }
        }

        public LprTypeClientData Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value;
            }
        }
              
        public LprWayClientData Way
        {
            get
            {
                return way;
            }
            set
            {
                way = value;
            }
        }


        public string Serial
        {
            get
            {
                return serial;
            }
            set
            {
                serial = value;
            }
        }

        [InitialDataClient(PropertyName = "Name", PropertyValue = "LprData")]
        public static readonly UserResourceClientData Resource;
        public override string ToString()
        {
            return this.Name;
        }
        public override bool Equals(object obj)
        {
            try
            {
                if (obj.GetType() == typeof(LprClientData))
                {
                    LprClientData str = (LprClientData)obj;
                    if (this.Code == str.Code)
                    {
                        return true;
                    }
                }
            }
            catch { }
            return false;
        }
    }
}
