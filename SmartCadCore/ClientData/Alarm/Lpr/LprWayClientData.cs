using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class LprWayClientData : ClientData
    {
        private string name = string.Empty;

       
        public virtual string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() == typeof(LprWayClientData))
            {
                LprWayClientData stc = (LprWayClientData)obj;
                if (this.Code == stc.Code)
                {
                    return true;
                }
            }
            return false;
        }


        public override string ToString()
        {
            return this.Name;
        }

        [InitialDataClient(PropertyName = "Name", PropertyValue = "LprWayData")]
        public static readonly UserResourceClientData Resource;
    }
}
