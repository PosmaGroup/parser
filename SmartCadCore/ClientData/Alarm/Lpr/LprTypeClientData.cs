using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class LprTypeClientData : ClientData
    {
        private string name = string.Empty;

       
        public virtual string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() == typeof(LprTypeClientData))
            {
                LprTypeClientData stc = (LprTypeClientData)obj;
                if (this.Code == stc.Code)
                {
                    return true;
                }
            }
            return false;
        }

        public override string ToString()
        {
            return this.Name;
        }

        [InitialDataClient(PropertyName = "Name", PropertyValue = "LprTypeData")]
        public static readonly UserResourceClientData Resource;
    }
}
