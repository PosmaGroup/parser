﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class TwitterReportClientData : ClientData, IIncidentReference
    {
        #region Fields

        private IList incidentTypesCodes;
        private DateTime registeredTime;
        private DateTime receivedTime;
        private string xml;
        private string customCode;
        private string incidentTypesText;
        private int incidentCode;
        //private PhoneReportCallerClientData phoneReportCallerClient;
        private IList answers;
        //private PhoneReportLineClientData phoneReportLineClient;
        private IList reportBaseDepartmentTypesClient;
        private string operatorLogin;
        private bool multipleOrganisms;
        private IList incidentNotifications;
        private bool incomplete;
        private string incidentCustomCode;

        #endregion

        #region Properties

        public DateTime ReceivedTime
        {
            get
            {
                return this.receivedTime;
            }
            set
            {
                this.receivedTime = value;
            }
        }

        public bool Incomplete
        {
            get
            {
                return this.incomplete;
            }
            set
            {
                this.incomplete = value;
            }
        }

        public IList ReportBaseDepartmentTypesClient
        {
            get
            {
                return this.reportBaseDepartmentTypesClient;
            }
            set
            {
                this.reportBaseDepartmentTypesClient = value;
            }
        }

        public AlarmTwitterClientData AlarmTwitter { get; set; }

        public string IncidentTypesText
        {
            get
            {
                return this.incidentTypesText;
            }
            set
            {
                this.incidentTypesText = value;
            }
        }

        public IList Answers
        {
            get
            {
                return this.answers;
            }
            set
            {
                this.answers = value;
            }
        }

        #region IIncidentReference Members

        public int IncidentCode
        {
            get
            {
                return this.incidentCode;
            }
            set
            {
                this.incidentCode = value;
            }
        }

        #endregion        

        public IList IncidentTypesCodes
        {
            get
            {
                return incidentTypesCodes;
            }
            set
            {
                incidentTypesCodes = value;
            }
        }

        public DateTime RegisteredTime
        {
            get
            {
                return registeredTime;
            }
            set
            {
                registeredTime = value;
            }
        }

        public DateTime FinishedTime { get; set; }

        public string Xml
        {
            get
            {
                return xml;
            }
            set
            {
                xml = value;
            }
        }

        public string CustomCode
        {
            get
            {
                return customCode;
            }
            set
            {
                customCode = value;
            }
        }

        public string OperatorLogin
        {
            get
            {
                return operatorLogin;
            }
            set
            {
                operatorLogin = value;
            }
        }

        public bool MultipleOrganisms
        {
            get
            {
                return multipleOrganisms;
            }
            set
            {
                multipleOrganisms = value;
            }
        }

        public IList IncidentNotifications
        {
            get
            {
                return incidentNotifications;
            }
            set
            {
                incidentNotifications = value;
            }
        }

        public string IncidentCustomCode
        {
            get
            {
                return this.incidentCustomCode;
            }
            set
            {
                this.incidentCustomCode = value;
            }
        }
        #endregion
        
    }
}
