using System;
using System.Collections.Generic;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class NoteClientData : ClientData
    {    
        #region Fields

        private string completeUserName;

        private string userLogin;

        private string detail;

        private DateTime creationDate;

        #endregion

        #region Properties

        public string CompleteUserName
        {
            get
            {
                return completeUserName;
            }
            set
            {
                completeUserName = value;
            }
        }

        public string UserLogin
        {
            get
            {
                return userLogin;
            }
            set
            {
                userLogin = value;
            }
        }

        public string Detail
        {
            get
            {
                return detail;
            }
            set
            {
                detail = value;
            }
        }

        public DateTime CreationDate
        {
            get
            {
                return creationDate;
            }
            set
            {
                creationDate = value;
            }
        }

        #endregion
    }

    [Serializable]
    public class IncidentNoteClientData : NoteClientData, IIncidentReference
    {
        private int incidentCode;

        #region IIncidentReference Members

        public int IncidentCode
        {
            get
            {
                return incidentCode;
            }
            set
            {
                incidentCode = value;
            }
        }

        #endregion
    }

    [Serializable]
    public class UnitNoteClientData : NoteClientData, IIncidentReference
    {
        private int unitCode;

        private int incidentCode;

        private int dispatchOrderCode;

        public int UnitCode
        {
            get
            {
                return unitCode;
            }
            set
            {
                unitCode = value;
            }
        }

        public int DispatchOrderCode
        {
            get
            {
                return dispatchOrderCode;
            }
            set
            {
                dispatchOrderCode = value;
            }
        }

        #region IIncidentReference Members

        public int IncidentCode
        {
            get
            {
                return incidentCode;
            }
            set
            {
                incidentCode = value;
            }
        }

        #endregion
    }

    [Serializable]
    public class DispatchOrderNoteClientData : NoteClientData
    {
        private int dispatchOrderCode;
        private DispatchOrderNoteTypeClientData type;
        private int incidentCode;

        public int DispatchOrderCode
        {
            get
            {
                return dispatchOrderCode;
            }
            set
            {
                dispatchOrderCode = value;
            }
        }

        public DispatchOrderNoteTypeClientData Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value;
            }
        }

        #region IIncidentReference Members

        public int IncidentCode
        {
            get
            {
                return incidentCode;
            }
            set
            {
                incidentCode = value;
            }
        }

        #endregion

    }
}
