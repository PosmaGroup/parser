using System;
using System.Collections.Generic;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class MotiveVariationClientData : ClientData
    {
        private string name;
        private bool type;

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public bool Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value;
            }
        }

        public override string ToString()
        {
            string text = "";

            if (this.Name != null)
            {
                text = this.Name;
            }

            return text;
        }



    }
}
