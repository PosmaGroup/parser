using System;
using System.Collections;
using System.Text;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class SupportRequestReportClientData : ClientData, IIncidentReference
    {
        public int UnitCode;
        public string Comment;
        public string CustomCode;
        private int incidentCode;
        public int OperatorCode;
        public IList ReportBaseDepartmentTypes;
        public IList IncidentTypes;
        public IList IncidentNotifications;
        public int SourceReportBaseCode;
        public Type SourceReportBaseType;

        #region IIncidentReference Members

        public int IncidentCode
        {
            get
            {
                return incidentCode;
            }
            set
            {
                incidentCode = value;
            }
        }

        #endregion
    }
}
