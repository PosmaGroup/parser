using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace SmartCadCore.ClientData
{
    [Serializable]
    public class DispatchOrderClientData : ClientData, IIncidentReference
    {
        #region Fields

        private Color backColor;
        private DepartmentTypeClientData departmentType;
        private string customCode;
        private DispatchOrderStatusClientData status;
        private DateTime startDate;
        private DateTime arrivalDate = DateTime.MinValue;
        private DateTime tempArrivalDate = DateTime.MinValue;
        private TimeSpan expectedTime = TimeSpan.Zero;
        private TimeSpan temporalExpectedTime = TimeSpan.Zero;
        private int dispatchOperatorCode;
        private string dispatchOperatorName;
        private int incidentNotificationCode;
        private string incidentNotificationCustomCode;
        private AddressClientData address;
        private UnitClientData unit;
        private EndingReportClientData endingReport;
        private string noteDetail;
        private int incidentCode;
        private string incidentCustomCode;
        private int incidentNotificationVersion;

        #endregion

        #region Properties

        public AddressClientData Address
        {
            get
            {
                return address;
            }
            set
            {
                address = value;
            }
        }

        public UnitClientData Unit
        {
            get
            {
                return unit;
            }
            set
            {
                unit = value;
            }
        }

        public Color BackColor
        {
            get
            {
                return backColor;
            }
            set
            {
                backColor = value;
            }
        }

        public DepartmentTypeClientData DepartmentType
        {
            get
            {
                return departmentType;
            }
            set
            {
                departmentType = value;
            }
        }

        public string CustomCode
        {
            get
            {
                return customCode;
            }
            set
            {
                customCode = value;
            }
        }

        public DispatchOrderStatusClientData Status
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
            }
        }

        public DateTime StartDate
        {
            get
            {
                return startDate;
            }
            set
            {
                startDate = value;
            }
        }

        public DateTime ArrivalDate
        {
            get
            {
                return arrivalDate;
            }
            set
            {
                arrivalDate = value;
            }
        }

        public DateTime TempArrivalDate
        {
            get
            {
                return tempArrivalDate;
            }
            set
            {
                tempArrivalDate = value;
            }
        }

        public TimeSpan ExpectedTime
        {
            get
            {
                return expectedTime;
            }
            set
            {
                expectedTime = value;
            }
        }

        public TimeSpan TemporalExpectedTime
        {
            get
            {
                return temporalExpectedTime;
            }
            set
            {
                temporalExpectedTime = value;
            }
        }

        public int DispatchOperatorCode
        {
            get
            {
                return dispatchOperatorCode;
            }
            set
            {
                dispatchOperatorCode = value;
            }
        }

        public string DispatchOperatorName
        {
            get
            {
                return dispatchOperatorName;
            }
            set
            {
                dispatchOperatorName = value;
            }
        }

        public int IncidentNotificationCode
        {
            get
            {
                return incidentNotificationCode;
            }
            set
            {
                incidentNotificationCode = value;
            }
        }

        public string IncidentNotificationCustomCode
        {
            get
            {
                return incidentNotificationCustomCode;
            }
            set
            {
                incidentNotificationCustomCode = value;
            }
        }

        public int IncidentNotificationVersion
        {
            get
            {
                return incidentNotificationVersion;
            }
            set
            {
                incidentNotificationVersion = value;
            }
        }

        public string TimeInAttention(DateTime endDate)
        {
            string result = "";
            if (this.ArrivalDate != null)
            {
                if (this.EndingReport != null && this.EndingReport.EndingTime != null)
                    endDate = this.EndingReport.EndingTime;
                TimeSpan timespan = TimeSpan.Zero;
                timespan = endDate - this.ArrivalDate;
                result = timespan.Hours.ToString().PadLeft(2, '0');
                result += ":" + timespan.Minutes.ToString().PadLeft(2, '0');
                result += ":" + timespan.Seconds.ToString().PadLeft(2, '0');
            }
            return result;
        }

        public EndingReportClientData EndingReport
        {
            get
            {
                return endingReport;
            }
            set
            {
                endingReport = value;
            }
        }

        public string NoteDetail
        {
            get
            {
                return noteDetail;
            }
            set
            {
                noteDetail = value;
            }
        }

        #region IIncidentReference Members

        public int IncidentCode
        {
            get
            {
                return incidentCode;
            }
            set
            {
                incidentCode = value;
            }
        }

        public string IncidentCustomCode
        {
            get
            {
                return incidentCustomCode;
            }
            set
            {
                incidentCustomCode = value;
            }
        }

        #endregion

        #region Overrides

        public override bool Equals(object obj)
        {
            bool result = false;
            DispatchOrderClientData objectData = obj as DispatchOrderClientData;
            if (objectData != null)
            {
                if (this.CustomCode == objectData.CustomCode)
                    result = true;
            }
            return result;
        }

        public override int GetHashCode()
        {
            return this.GetType().GetHashCode() * 10000000 + this.CustomCode.GetHashCode();
        }

        #endregion

        #endregion
    }
}
