﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace SmartCadCore.ClientData
{
    #region Class DispatchOrderNoteTypeData Documentation
    /// <summary>
    /// This class represents the client types that a dispatch Order Note can be.
    /// </summary>
    /// <className>DispatchOrderNoteTypeClientData</className>
    /// <author email="arturo.aguer@smartmatic.com">Arturo Agüero</author>
    /// <date>2007/13/05</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    [Serializable]
    public class DispatchOrderNoteTypeClientData : ClientData 
    {
        #region Fields
        private string customCode;
        private string friendlyName;
        private string name;
        #endregion

        #region Properties

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public string FriendlyName
        {
            get
            {
                return friendlyName;
            }
            set
            {
                friendlyName = value;
            }
        }

        public string CustomCode
        {
            get
            {
                return customCode;
            }
            set
            {
                customCode = value;
            }
        }

        #endregion

        #region InitialData

        [InitialDataClient(PropertyName = "CustomCode", PropertyValue = "DELAY_NOTE")]
        public static DispatchOrderNoteTypeClientData TimeChanged;

        [InitialDataClient(PropertyName = "CustomCode", PropertyValue = "CANCEL_NOTE")]
        public static DispatchOrderNoteTypeClientData Cancelled;

        [InitialDataClient(PropertyName = "CustomCode", PropertyValue = "CLOSE_NOTE")]
        public static DispatchOrderNoteTypeClientData Closed;

        [InitialDataClient(PropertyName = "CustomCode", PropertyValue = "CURRENT_PROCEDURE")]
        public static DispatchOrderNoteTypeClientData CurrentProcedure;

        [InitialDataClient(PropertyName = "CustomCode", PropertyValue = "CREATION_NOTE")]
        public static DispatchOrderNoteTypeClientData Creation;

        #endregion
    }
}
