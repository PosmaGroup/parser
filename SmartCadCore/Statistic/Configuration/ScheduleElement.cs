﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using Genesyslab.Desktop.ApplicationBlocks.StatConnection.StatProxy;

namespace SmartCadCore.Statistic
{
    public class ScheduleElement : ConfigurationElement
    {
        [ConfigurationProperty("notification-mode", IsRequired = true)]
        public scheduleMode NotificationMode
        {
            get
            {
                return (scheduleMode)this["notification-mode"];
            }
            set
            {
                this["notification-mode"] = value;
            }
        }

        [ConfigurationProperty("insensitivity", IsRequired = false)]
        public int Insensitivity
        {
            get
            {
                return (int)this["insensitivity"];
            }
            set
            {
                this["insensitivity"] = value;
            }
        }

        [ConfigurationProperty("timeout", IsRequired = false)]
        public int Timeout
        {
            get
            {
                return (int)this["timeout"];
            }
            set
            {
                this["timeout"] = value;
            }
        }        
    }
}
