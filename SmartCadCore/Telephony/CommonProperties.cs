﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartCadCore.Telephony
{
    public static class CommonProperties
    {
        public const string ServerType = "SERVER_TYPE";
        public const string UserLogin = "USER_LOGIN";
        public const string UserPassword = "USER_PASS";
        public const string AgentId = "AGENT_ID";
        public const string AgentPassword = "AGENT_PASSWORD";
        public const string Domain = "DOMAIN";
        public const string Extension = "EXTENSION";
        public const string ExtensionPassword = "EXTENSION_PASS";
        public const string Ready = "READY";
        public const string Queue = "QUEUE";
        public const string VirtualMode = "VIRTUAL";
        public const string RecordCalls = "RECORD_CALLS";
    }
}
