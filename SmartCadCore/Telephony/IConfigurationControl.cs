﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SmartCadCore.Telephony
{
    public delegate void Parameters_ChangeEvent(bool isfullFilled);

    public interface IConfigurationControl 
    {
        event Parameters_ChangeEvent FulFilled_Change;

        void SetProperties(Dictionary<string, string> properties);
        Dictionary<string,string> GetProperties();

        void SetStatisticProperties(string[] statisticProperties);
        string[] GetStatisticProperties();
    }
}
