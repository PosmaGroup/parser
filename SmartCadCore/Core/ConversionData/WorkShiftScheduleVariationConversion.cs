using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;
using System.Collections;

namespace SmartCadCore.Core
{
   public class WorkShiftScheduleVariationConversion 
    {
        public static WorkShiftScheduleVariationClientData ToClient(WorkShiftScheduleVariationData variationScheduleData, UserApplicationData app)
        {
            WorkShiftScheduleVariationClientData variationScheduleClientData = new WorkShiftScheduleVariationClientData();
            variationScheduleClientData.Code = variationScheduleData.Code;
            variationScheduleClientData.Version = variationScheduleData.Version;
            variationScheduleClientData.Start = variationScheduleData.Start.Value;
            variationScheduleClientData.End = variationScheduleData.End.Value;
            variationScheduleClientData.WorkShiftVariationName = variationScheduleData.WorkShiftVariation.Name;
            variationScheduleClientData.WorkShiftVariationType = (WorkShiftVariationClientData.WorkShiftType)((int)variationScheduleData.WorkShiftVariation.Type);
            variationScheduleClientData.WorkShiftVariationCode = variationScheduleData.WorkShiftVariation.Code;

            return variationScheduleClientData;
        }

       public static WorkShiftScheduleVariationData ToObject(WorkShiftScheduleVariationClientData variationScheduleClientData, UserApplicationData app)
       {
           WorkShiftScheduleVariationData variationScheduleData = new WorkShiftScheduleVariationData();
           variationScheduleData.Code = variationScheduleClientData.Code;
           variationScheduleData.Version = variationScheduleClientData.Version;

           variationScheduleData.Start = variationScheduleClientData.Start;
           variationScheduleData.End = variationScheduleClientData.End;
           WorkShiftVariationData wsv = new WorkShiftVariationData();
           wsv.Code = variationScheduleClientData.WorkShiftVariationCode;
         
           variationScheduleData.WorkShiftVariation = wsv;

           return variationScheduleData;
       }
    }
}
