using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;
using System.Drawing;

namespace SmartCadCore.Core
{
    public class IncidentNotificationStatusConversion
    {
        public static IncidentNotificationStatusClientData ToClient(IncidentNotificationStatusData incidentNotificationStatusData, UserApplicationData app)
        {
            IncidentNotificationStatusClientData incidentNotificationStatusClientData = new IncidentNotificationStatusClientData();
            incidentNotificationStatusClientData.Code = incidentNotificationStatusData.Code;
            incidentNotificationStatusClientData.Version = incidentNotificationStatusData.Version;
            incidentNotificationStatusClientData.Color = incidentNotificationStatusData.Color;
            incidentNotificationStatusClientData.CustomCode = incidentNotificationStatusData.CustomCode;
            incidentNotificationStatusClientData.FriendlyName = incidentNotificationStatusData.FriendlyName;
            incidentNotificationStatusClientData.Image = incidentNotificationStatusData.Image;
            incidentNotificationStatusClientData.Immutable = incidentNotificationStatusData.Immutable.HasValue == true ? incidentNotificationStatusData.Immutable.Value : true;
            incidentNotificationStatusClientData.Name = incidentNotificationStatusData.Name;
            incidentNotificationStatusClientData.Active = incidentNotificationStatusData.Active.Value;

            return incidentNotificationStatusClientData;
        }

        public static IncidentNotificationStatusData ToObject(IncidentNotificationStatusClientData client, UserApplicationData app)
        {
            IncidentNotificationStatusData incidentNotificationStatus = new IncidentNotificationStatusData();
            incidentNotificationStatus.Code = client.Code;
            incidentNotificationStatus.CustomCode = client.CustomCode;
            incidentNotificationStatus = SmartCadDatabase.SearchObject<IncidentNotificationStatusData>(incidentNotificationStatus);
            return incidentNotificationStatus;
        }
    }
}
