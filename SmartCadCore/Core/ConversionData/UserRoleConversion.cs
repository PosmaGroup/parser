using System;
using System.Collections;
using System.Text;

using SmartCadCore.Model;
using SmartCadCore.ClientData;

namespace SmartCadCore.Core
{
    public class UserRoleConversion
    {
        public static UserRoleClientData ToClient(UserRoleData roleData, UserApplicationData application)
        {
            UserRoleClientData roleClientData = new UserRoleClientData();
            roleClientData.Code = roleData.Code;
            roleClientData.Description = roleData.Description;
            roleClientData.FriendlyName = roleData.FriendlyName;
            roleClientData.Immutable = roleData.Immutable;
            roleClientData.Name = roleData.Name;
            roleClientData.Profiles = new ArrayList();
            if (SmartCadDatabase.IsInitialize(roleData.Profiles) == true)
            {
                foreach (UserProfileData profile in roleData.Profiles)
                {
                    roleClientData.Profiles.Add(UserProfileConversion.ToClient(profile, application));
                }
            }
            roleClientData.Version = roleData.Version;
            return roleClientData;
        }

        public static UserRoleData ToObject(UserRoleClientData roleClientData, UserApplicationData application)
        {
            UserRoleData roleData = new UserRoleData();
            roleData.Code = roleClientData.Code;
            roleData.Description = roleClientData.Description;
            roleData.FriendlyName = roleClientData.FriendlyName;
            roleData.Immutable = roleClientData.Immutable;
            roleData.Name = roleClientData.Name;
            roleData.Profiles = new ArrayList();
            if (roleClientData.Profiles != null)
            {
                foreach (UserProfileClientData profile in roleClientData.Profiles)
                {
                    roleData.Profiles.Add(UserProfileConversion.ToObject(profile, application));
                }
            }
            roleData.Version = roleClientData.Version;
            return roleData;
        }
    }
}
