using System;
using System.Collections;
using System.Text;

using SmartCadCore.ClientData;
using SmartCadCore.Model;
using Iesi.Collections;
using System.Windows.Forms;
using System.Collections.Generic;
using NHibernate;
using SmartCadCore.Common;

namespace SmartCadCore.Core
{
    public class UnitConversion
    {
		public static UnitClientData ToClient(ISession session, UnitData unit, UserApplicationData app)
		{
			UnitClientData unitClient = new UnitClientData();
			unitClient.Code = unit.Code;
			unitClient.Version = unit.Version;
			unitClient.CustomCode = unit.CustomCode;
			unitClient.Type = new UnitTypeClientData();
			unitClient.Type.Code = unit.Type.Code;
			unitClient.Type.Name = unit.Type.Name;
            unitClient.Capacity = (int)unit.Capacity;
            unitClient.Description = unit.Description;
            if (unit.GPS != null)
            {
                unitClient.DeviceType = (UnitClientData.DEVICE_TYPE)(int)unit.DeviceType;
                unitClient.IdGPS = unit.GPS.Name;
                unitClient.GPSCode = unit.GPS.Code;
            }


			if (app.Equals(UserApplicationData.Administration))
			{
				#region Administration
				unitClient.DepartmentType = new DepartmentTypeClientData();
				unitClient.DepartmentType.Code = unit.DepartmentType.Code;
				unitClient.DepartmentType.Name = unit.DepartmentType.Name;
				unitClient.DepartmentType.Dispatch = unit.DepartmentType.Dispatch.Value;
				unitClient.DepartmentType.Version = unit.DepartmentType.Version;


				if (unit.InitializeCollections == true)
				{
					if (unit.GPS != null)
					{
						unitClient.DeviceType = (UnitClientData.DEVICE_TYPE)(int)unit.DeviceType;
						unitClient.IdGPS = unit.GPS.Name;
						unitClient.GPSCode = unit.GPS.Code;
					}
					unitClient.IdRadio = unit.IdRadio;
					unitClient.Capacity = (unit.Capacity.HasValue ? unit.Capacity.Value : 0);
					unitClient.Description = unit.Description;
					unitClient.Velocity = (unit.Velocity.HasValue ? unit.Velocity.Value : 0);
					unitClient.Brand = unit.Brand;
					unitClient.Model = unit.Model;
					unitClient.Year = unit.Year;
					unitClient.Serial = unit.Serial;
					unitClient.IdUnit = unit.IdUnit;

					unitClient.DepartmentStation = new DepartmentStationClientData();
					unitClient.DepartmentStation.Code = unit.DepartmentStation.Code;
					unitClient.DepartmentStation.Name = unit.DepartmentStation.Name;

					unitClient.DepartmentStation.DepartmentZone = new DepartmentZoneClientData();
					unitClient.DepartmentStation.DepartmentZone.Code = unit.DepartmentStation.DepartmentZone.Code;
					unitClient.DepartmentStation.DepartmentZone.Version = unit.DepartmentStation.DepartmentZone.Version;
					unitClient.DepartmentStation.DepartmentZone.CustomCode = unit.DepartmentStation.DepartmentZone.CustomCode;
					unitClient.DepartmentStation.DepartmentZone.Name = unit.DepartmentStation.DepartmentZone.Name;

					if (unit.SetOfficerAssigns != null)
						unitClient.OfficerAssigns = GetOfficerAssigns(unit, app);

					if (SmartCadDatabase.IsInitialize(unit.SetIncidentTypes) == false)
						unit.SetIncidentTypes = new HashedSet(SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetIncidentTypesByUnitCode, unit.Code)));

					if (unit.SetIncidentTypes != null && unit.SetIncidentTypes.Count > 0)
					{
						unitClient.IncidentTypes = BuildIncidentTypes(unit.SetIncidentTypes);
					}

					if (SmartCadDatabase.IsInitialize(unit.WorkShiftRoutes) == false)
						unit.WorkShiftRoutes = SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(@"SELECT wsRoute FROM WorkShiftRouteData wsRoute WHERE wsRoute.Unit.Code = {0} AND wsRoute.WorkShift.Code in (SELECT var.Code FROM WorkShiftVariationData var left join var.Schedules schedules Where schedules.End > '{1}')", unit.Code, ApplicationUtil.GetDataBaseFormattedDate(DateTime.Now)));
					if (SmartCadDatabase.IsInitialize(unit.WorkShiftRoutes) == true)
					{
						unitClient.WorkShiftsRoutes = new ArrayList();
						foreach (WorkShiftRouteData wsRoute in unit.WorkShiftRoutes)
							unitClient.WorkShiftsRoutes.Add(WorkShiftRouteConversion.ToClient(wsRoute, app));

					}
				}
				#endregion
			}
			else if (app.Equals(UserApplicationData.FirstLevel))
			{
				#region FisrtLevel
				unitClient.DepartmentType = new DepartmentTypeClientData();
				unitClient.DepartmentType.Code = unit.DepartmentType.Code;
				unitClient.DepartmentType.Name = unit.DepartmentType.Name;
				unitClient.DepartmentType.Dispatch = unit.DepartmentType.Dispatch.Value;
				unitClient.DepartmentType.Version = unit.DepartmentType.Version;

				if (unit.DepartmentStation != null)
					unitClient.DepartmentStation = DepartmentStationConversion.ToClient(unit.DepartmentStation, app);
				unitClient.Distance = unit.Distance;
				unitClient.Status = UnitStatusConversion.ToClient(unit.Status, app);
				if (unit.GPS != null)
				{
					unitClient.Lon = unit.GPS.Longitude;
					unitClient.Lat = unit.GPS.Latitude;
					unitClient.IsSynchronized = unit.GPS.IsSynchronized;

				}
				unitClient.IdRadio = unit.IdRadio;
				unitClient.Velocity = unit.Velocity.Value;
				unitClient.IncidentTypes = new ArrayList();
				#endregion
			}
			else if (app.Equals(UserApplicationData.Dispatch))
			{
				#region Dispatch
				try
				{
					unitClient.DepartmentType = new DepartmentTypeClientData();
					unitClient.DepartmentType.Code = unit.DepartmentType.Code;
					unitClient.DepartmentType.Name = unit.DepartmentType.Name;
					unitClient.DepartmentType.Dispatch = unit.DepartmentType.Dispatch.Value;
					unitClient.DepartmentType.Version = unit.DepartmentType.Version;

					if (unit.DepartmentStation != null)
						unitClient.DepartmentStation = DepartmentStationConversion.ToClient(unit.DepartmentStation, app);
					unitClient.Distance = unit.Distance;
					unitClient.Status = UnitStatusConversion.ToClient(unit.Status, app);
					if (unit.GPS != null)
					{
						unitClient.IdGPS = unit.GPS.Name;
						unitClient.GPSCode = unit.GPS.Code;
						unitClient.Lon = unit.GPS.Longitude;
						unitClient.Lat = unit.GPS.Latitude;
						unitClient.IsSynchronized = unit.GPS.IsSynchronized;
						unitClient.CoordinatesDate = unit.GPS.CoordinatesDate;
					}

					unitClient.IdRadio = unit.IdRadio;
					unitClient.Velocity = unit.Velocity.Value;
					unitClient.IncidentTypes = new ArrayList();


					unitClient.OfficerAssigns = GetOfficerAssigns(session, unit, app);
					if (SmartCadDatabase.IsInitialize(unit.SetIncidentTypes) == false)
					{
						unit.SetIncidentTypes = GetUnitIncidentTypes(session,unit);
					}
					if (unit.SetIncidentTypes != null)
						unitClient.IncidentTypes = BuildIncidentTypes(unit.SetIncidentTypes);
					if (unit.DispatchOrder != null)
					{
						unit.DispatchOrder.Unit = null;
						unitClient.DispatchOrder = DispatchOrderConversion.ToClient(unit.DispatchOrder, app);
						unitClient.DispatchOrder.Unit = unitClient;
						unitClient.DispatchOrder.DepartmentType = unitClient.DepartmentType;
					}
				}
				catch (Exception ex)
				{
                    SmartLogger.Print(ex);
				}
				#endregion
			}
			else if (app.Equals(UserApplicationData.Map))
			{
				#region Map
				unitClient.DepartmentType = new DepartmentTypeClientData();
				unitClient.DepartmentType.Code = unit.DepartmentType.Code;
				unitClient.DepartmentType.Name = unit.DepartmentType.Name;
				unitClient.DepartmentType.Dispatch = unit.DepartmentType.Dispatch.Value;
				unitClient.DepartmentType.Version = unit.DepartmentType.Version;

				if (unit.DepartmentStation != null)
					unitClient.DepartmentStation = DepartmentStationConversion.ToClient(unit.DepartmentStation, app);
				unitClient.Distance = unit.Distance;
				unitClient.Status = UnitStatusConversion.ToClient(unit.Status, app);


				unitClient.IdRadio = unit.IdRadio;
				if (unit.GPS != null)
				{
					unitClient.IdGPS = unit.GPS.Name;
					unitClient.GPSCode = unit.GPS.Code;
					unitClient.Lon = unit.GPS.Longitude;
					unitClient.Lat = unit.GPS.Latitude;
					unitClient.Heading = unit.GPS.Heading;
					unitClient.Satellites = unit.GPS.Satellites;
					unitClient.IsSynchronized = unit.GPS.IsSynchronized;
					unitClient.CoordinatesDate = unit.GPS.CoordinatesDate;
				}
				unitClient.IncidentTypes = new ArrayList();

				try
				{
					if (unit.DispatchOrder != null)
					{
						unitClient.DispatchOrder = DispatchOrderConversion.ToClient(unit.DispatchOrder, app);
					}
				}
				catch (Exception exc)
				{
                    SmartLogger.Print(ResourceLoader.GetString2("FormattedException", "Map", "unit.DispatchOrder", exc.Message));
					throw exc;
				}
				if (string.IsNullOrEmpty(unit.Type.Image))
					unitClient.IconName = ResourceLoader.GetString("DefaultIconUnitType") + ".BMP";
				else
					unitClient.IconName = unit.Type.Image;
				#endregion
			}

			return unitClient;
		}

        public static UnitClientData ToClient(UnitData unit, UserApplicationData app)
        {
            UnitClientData unitClient = new UnitClientData();
            unitClient.Code = unit.Code;
            unitClient.Description = unit.Description;
            unitClient.Version = unit.Version;
            unitClient.CustomCode = unit.CustomCode;
            unitClient.Type = new UnitTypeClientData();
            unitClient.Type.Code = unit.Type.Code;
            unitClient.Capacity = (int)unit.Capacity;
            unitClient.Type.Name = unit.Type.Name;
            if (unit.GPS != null)
            {
                unitClient.DeviceType = (UnitClientData.DEVICE_TYPE)(int)unit.DeviceType;
                unitClient.IdGPS = unit.GPS.Name;
                unitClient.GPSCode = unit.GPS.Code;
            }
            
            if (app.Equals(UserApplicationData.Administration))
            {
                #region Administration
                unitClient.DepartmentType = new DepartmentTypeClientData();
                unitClient.DepartmentType.Code = unit.DepartmentType.Code;
                unitClient.DepartmentType.Name = unit.DepartmentType.Name;
                unitClient.DepartmentType.Dispatch = unit.DepartmentType.Dispatch.Value;
                unitClient.DepartmentType.Version = unit.DepartmentType.Version;
                

                if (unit.InitializeCollections == true)
                {                    
                    if (unit.GPS != null)
                    {
                        unitClient.DeviceType = (UnitClientData.DEVICE_TYPE)(int)unit.DeviceType;
                        unitClient.IdGPS = unit.GPS.Name;
                        unitClient.GPSCode = unit.GPS.Code;
                    }
                    unitClient.IdRadio = unit.IdRadio;
                    unitClient.Capacity = (unit.Capacity.HasValue ? unit.Capacity.Value : 0);
                    unitClient.Description = unit.Description;
                    unitClient.Velocity = (unit.Velocity.HasValue ? unit.Velocity.Value : 0);
                    unitClient.Brand = unit.Brand;
                    unitClient.Model = unit.Model;
                    unitClient.Year = unit.Year;
                    unitClient.Serial = unit.Serial;
                    unitClient.IdUnit = unit.IdUnit;

                    unitClient.DepartmentStation = new DepartmentStationClientData();
                    unitClient.DepartmentStation.Code = unit.DepartmentStation.Code;
                    unitClient.DepartmentStation.Name = unit.DepartmentStation.Name;

                    unitClient.DepartmentStation.DepartmentZone = new DepartmentZoneClientData();
                    unitClient.DepartmentStation.DepartmentZone.Code = unit.DepartmentStation.DepartmentZone.Code;
                    unitClient.DepartmentStation.DepartmentZone.Version = unit.DepartmentStation.DepartmentZone.Version;
                    unitClient.DepartmentStation.DepartmentZone.CustomCode = unit.DepartmentStation.DepartmentZone.CustomCode;
                    unitClient.DepartmentStation.DepartmentZone.Name = unit.DepartmentStation.DepartmentZone.Name;

                    if (unit.SetOfficerAssigns != null)
                        unitClient.OfficerAssigns = GetOfficerAssigns(unit, app);

					if(SmartCadDatabase.IsInitialize(unit.SetIncidentTypes) == false)
						unit.SetIncidentTypes = new HashedSet(SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetIncidentTypesByUnitCode, unit.Code)));

					if (unit.SetIncidentTypes != null && unit.SetIncidentTypes.Count > 0)
                    {
                        unitClient.IncidentTypes = BuildIncidentTypes(unit.SetIncidentTypes);
                    }

                    if (SmartCadDatabase.IsInitialize(unit.WorkShiftRoutes) == false)
						unit.WorkShiftRoutes = SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(@"SELECT wsRoute FROM WorkShiftRouteData wsRoute WHERE wsRoute.Unit.Code = {0} AND wsRoute.WorkShift.Code in (SELECT var.Code FROM WorkShiftVariationData var left join var.Schedules schedules Where schedules.End > '{1}')", unit.Code, ApplicationUtil.GetDataBaseFormattedDate(DateTime.Now)));
                    if (SmartCadDatabase.IsInitialize(unit.WorkShiftRoutes) == true)
                    {
                        unitClient.WorkShiftsRoutes = new ArrayList();
                        foreach (WorkShiftRouteData wsRoute in unit.WorkShiftRoutes)
                            unitClient.WorkShiftsRoutes.Add(WorkShiftRouteConversion.ToClient(wsRoute, app));

                    }
                }
                #endregion
            }
            else if (app.Equals(UserApplicationData.FirstLevel))
            {
                #region FisrtLevel
				unitClient.DepartmentType = new DepartmentTypeClientData();
				unitClient.DepartmentType.Code = unit.DepartmentType.Code;
				unitClient.DepartmentType.Name = unit.DepartmentType.Name;
                unitClient.DepartmentType.Dispatch = unit.DepartmentType.Dispatch.Value;
				unitClient.DepartmentType.Version = unit.DepartmentType.Version;

				if (unit.DepartmentStation != null)
					unitClient.DepartmentStation = DepartmentStationConversion.ToClient(unit.DepartmentStation, app);
				unitClient.Distance = unit.Distance;
                unitClient.Status = UnitStatusConversion.ToClient(unit.Status, app);
                if (unit.GPS != null)
                {
                    unitClient.Lon = unit.GPS.Longitude;
                    unitClient.Lat = unit.GPS.Latitude;
                    unitClient.IsSynchronized = unit.GPS.IsSynchronized;
            
                }
                unitClient.IdRadio = unit.IdRadio;
                unitClient.Velocity = unit.Velocity.Value;
                unitClient.IncidentTypes = new ArrayList();
                #endregion
            }
            else if (app.Equals(UserApplicationData.Dispatch))
            {
                #region Dispatch
                try
                {
                    unitClient.DepartmentType = new DepartmentTypeClientData();
                    unitClient.DepartmentType.Code = unit.DepartmentType.Code;
                    unitClient.DepartmentType.Name = unit.DepartmentType.Name;
                    unitClient.DepartmentType.Dispatch = unit.DepartmentType.Dispatch.Value;
                    unitClient.DepartmentType.Version = unit.DepartmentType.Version;

                    if(unit.DepartmentStation != null)
                        unitClient.DepartmentStation = DepartmentStationConversion.ToClient(unit.DepartmentStation,app);
			        unitClient.Distance = unit.Distance;
                    unitClient.Status = UnitStatusConversion.ToClient(unit.Status, app);
                    if (unit.GPS != null)
                    {
                        unitClient.IdGPS = unit.GPS.Name;
                        unitClient.GPSCode = unit.GPS.Code;
			            unitClient.Lon = unit.GPS.Longitude;
                        unitClient.Lat = unit.GPS.Latitude;
                        unitClient.IsSynchronized = unit.GPS.IsSynchronized;
                        unitClient.CoordinatesDate = unit.GPS.CoordinatesDate;
                    }
                 
                    unitClient.IdRadio = unit.IdRadio;
                    unitClient.Velocity = unit.Velocity.Value;
                    unitClient.IncidentTypes = new ArrayList();
    
                    
                    unitClient.OfficerAssigns = GetOfficerAssigns(unit, app);
                    if (SmartCadDatabase.IsInitialize(unit.SetIncidentTypes) == false)
                    {
                        unit.SetIncidentTypes = GetUnitIncidentTypes(unit);
                    }
                    if (unit.SetIncidentTypes != null)
                        unitClient.IncidentTypes = BuildIncidentTypes(unit.SetIncidentTypes);
                    if (unit.DispatchOrder != null)
                    {
                        unit.DispatchOrder.Unit = null;
                        unitClient.DispatchOrder = DispatchOrderConversion.ToClient(unit.DispatchOrder, app);
                        unitClient.DispatchOrder.Unit = unitClient;
                        unitClient.DispatchOrder.DepartmentType = unitClient.DepartmentType;
                    }
                }
                catch (Exception ex)
                {
                    SmartLogger.Print(ex);
                }
                #endregion
            }
            else if (app.Equals(UserApplicationData.Map))
            {
                #region Map
				unitClient.DepartmentType = new DepartmentTypeClientData();
				unitClient.DepartmentType.Code = unit.DepartmentType.Code;
				unitClient.DepartmentType.Name = unit.DepartmentType.Name;
                unitClient.DepartmentType.Dispatch = unit.DepartmentType.Dispatch.Value;
				unitClient.DepartmentType.Version = unit.DepartmentType.Version;

				if (unit.DepartmentStation != null)
					unitClient.DepartmentStation = DepartmentStationConversion.ToClient(unit.DepartmentStation, app);
				unitClient.Distance = unit.Distance;
                unitClient.Status = UnitStatusConversion.ToClient(unit.Status, app);
              
       
                unitClient.IdRadio = unit.IdRadio;
                if (unit.GPS != null)
                {
                    unitClient.IdGPS = unit.GPS.Name;
                    unitClient.GPSCode = unit.GPS.Code;
                    unitClient.Lon = unit.GPS.Longitude;
                    unitClient.Lat = unit.GPS.Latitude;
                    unitClient.Heading = unit.GPS.Heading;
                    unitClient.Satellites = unit.GPS.Satellites;
                    unitClient.IsSynchronized = unit.GPS.IsSynchronized;
                    unitClient.CoordinatesDate = unit.GPS.CoordinatesDate;
                }
                unitClient.IncidentTypes = new ArrayList();
              
                try
                {
                    if (unit.DispatchOrder != null)
                    {
                        unitClient.DispatchOrder = DispatchOrderConversion.ToClient(unit.DispatchOrder, app);
                    }
                }
                catch (Exception exc)
                {
                    SmartLogger.Print(ResourceLoader.GetString2("FormattedException", "Map", "unit.DispatchOrder", exc.Message));
                    throw exc;
                }
                if (string.IsNullOrEmpty(unit.Type.Image))
                    unitClient.IconName = ResourceLoader.GetString("DefaultIconUnitType") + ".BMP";
                else
                    unitClient.IconName = unit.Type.Image;
                #endregion
            }

            return unitClient;
        }

        private static HashedSet GetUnitIncidentTypes(UnitData unit)
        {
            ICollection result = null;
            HashedSet incidentTypes = null;
            result = (ICollection)SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(
                SmartCadHqls.GetIncidentTypeByUnit, unit.Code));
            if (result.Count > 0 && !IsNullCollection(result))
                incidentTypes = new HashedSet(result);
            else
                incidentTypes = new HashedSet();
            return incidentTypes;
        }

		private static HashedSet GetUnitIncidentTypes(ISession session, UnitData unit)
		{
			ICollection result = null;
			HashedSet incidentTypes = null;
			SmartCadDatabase.InitializeLazy(session, unit, unit.SetIncidentTypes);
			result = unit.SetIncidentTypes;
			if (result.Count > 0 && !IsNullCollection(result))
				incidentTypes = new HashedSet(result);
			else
				incidentTypes = new HashedSet();
			return incidentTypes;
		}

        private static bool IsNullCollection(ICollection coll)
        {
            bool isNull = false;
            foreach (object obj in coll)
            {
                if (obj == null)
                {
                    isNull = true;
                }
            }
            return isNull;
        }

        public static UnitData ToObject(UnitClientData unitClient, UserApplicationData app)
        {
            UnitData unit = new UnitData();
            unit.Code = unitClient.Code;
            if(unit.Code >0)
                unit = SmartCadDatabase.RefreshObject(unit) as UnitData;
            unit.Version = unitClient.Version;

            if (app.Name == UserApplicationData.Administration.Name)
            {
                unit.Brand = unitClient.Brand;
                unit.Capacity = unitClient.Capacity;
                unit.CustomCode = unitClient.CustomCode;
                unit.Description = unitClient.Description;
                unit.Distance = unitClient.Distance;

                if (unitClient.GPSCode != 0)
                {
                    unit.DeviceType = (UnitData.DEVICE_TYPE)(int)unitClient.DeviceType;             
                    GPSData gps = new GPSData();
                    gps.Code = unitClient.GPSCode;
                    gps.Name = unitClient.IdGPS;
                    gps.Unit = unit;
                    unit.GPS = gps;
                }
           
                unit.Serial = unitClient.Serial;
                unit.IdUnit = unitClient.IdUnit;
                unit.IdRadio = unitClient.IdRadio;
               
                unit.Model = unitClient.Model;
                unit.Year = unitClient.Year;
                unit.Velocity = unitClient.Velocity;
                unit.DepartmentStation = new DepartmentStationData();
                unit.DepartmentStation.Code = unitClient.DepartmentStation.Code;
                unit.DepartmentStation.Version = unitClient.DepartmentStation.Version;
                unit.DepartmentStation = (DepartmentStationData)SmartCadDatabase.RefreshObject(unit.DepartmentStation);
                unit.Type = new UnitTypeData();
                unit.Type.Code = unitClient.Type.Code;
                unit.Type = (UnitTypeData)SmartCadDatabase.RefreshObject(unit.Type);
                if (unit.Code == 0)
                    unit.Status = UnitStatusConversion.ToObject(unitClient.Status);
              

                unit.SetIncidentTypes = new HashedSet();
                if (unitClient.IncidentTypes != null && unitClient.IncidentTypes.Count > 0)
                {
                    foreach (IncidentTypeClientData type in unitClient.IncidentTypes)
                    {
                        IncidentTypeData typeData = new IncidentTypeData();
                        typeData.Code = type.Code;
                        typeData.Version = type.Version;
                        typeData.Name = type.Name;
                        typeData.FriendlyName = type.FriendlyName;
                        typeData.CustomCode = type.CustomCode;

                        unit.SetIncidentTypes.Add(typeData);
                    }
                }

                if (unitClient.WorkShiftsRoutes != null) 
                {
                    WorkShiftRouteData wsRouteData;
                    unit.WorkShiftRoutes = new ArrayList();
                    foreach (WorkShiftRouteClientData wsRoute in unitClient.WorkShiftsRoutes)
                    {
                        wsRouteData = WorkShiftRouteConversion.ToObject(wsRoute, app);
                        wsRouteData.Unit = unit;
                        unit.WorkShiftRoutes.Add(wsRouteData);
                    }
                }

            }
            else if (app.Name == UserApplicationData.Dispatch.Name)
            {
                if (unitClient.Status != null && (unit.Status == null || unit.Status.CustomCode != unitClient.Status.CustomCode))
                {
                    unit.Status = SmartCadDatabase.SearchObject<UnitStatusData>(
                        SmartCadHqls.GetCustomHql(
                            SmartCadHqls.GetUnitStatusByCustomCode, unitClient.Status.CustomCode));
                }

                if (unitClient.DispatchOrder != null)
                {
                    unit.DispatchOrder = DispatchOrderConversion.ToObject(unitClient.DispatchOrder);
                }
            }

            return unit;
        }

        private static IList BuildIncidentTypes(ICollection incidentTypesData)
        {
            IList incidentTypes = new ArrayList(incidentTypesData.Count);

            foreach (IncidentTypeData it in incidentTypesData)
            {
                if (it != null)
                {
                    IncidentTypeClientData inc = new IncidentTypeClientData();
                    inc.Code = it.Code;
                    inc.Name = it.Name;
                    inc.FriendlyName = it.FriendlyName;
                    inc.CustomCode = it.CustomCode;
                    incidentTypes.Add(inc);//(IncidentTypeConversion.ToClient(it,null));
                }
            }

            return incidentTypes;
        }

		private static IList GetOfficerAssigns(ISession session, UnitData unit, UserApplicationData app)
		{
			IList officerAssigns = new ArrayList();
			
			try
			{
				if (SmartCadDatabase.IsInitialize(unit.SetOfficerAssigns) == false)
					SmartCadDatabase.InitializeLazy(session, unit, unit.SetOfficerAssigns);

				if (unit.SetOfficerAssigns != null)
				{
					foreach (UnitOfficerAssignHistoryData officerAssign in unit.SetOfficerAssigns)
					{
						OfficerClientData officerClient = new OfficerClientData();
						officerClient.Code = officerAssign.Officer.Code;
						officerClient.Badge = officerAssign.Officer.Badge;

						officerClient.DepartmentType = new DepartmentTypeClientData();
						officerClient.DepartmentType.Code = officerAssign.Officer.Department.Code;
						officerClient.DepartmentType.Name = officerAssign.Officer.Department.Name;

						officerClient.FirstName = officerAssign.Officer.FirstName;
						officerClient.LastName = officerAssign.Officer.LastName;
                        officerClient.PersonID = officerAssign.Officer.PersonId;
                        officerClient.Telephone = officerAssign.Officer.Telephone;
						if (officerAssign.Officer.Position != null)
							officerClient.Position = PositionConversion.ToClient(officerAssign.Officer.Position, app);
						officerClient.UnitAssignCode = officerAssign.Unit.Code;
						officerClient.Version = officerAssign.Officer.Version;

						officerAssigns.Add(officerClient);
					}
				}
			}
			catch (Exception exc)
			{
                SmartLogger.Print(ResourceLoader.GetString2("FormattedException", "Dispatch", "unit.SetOfficerAssigns", exc.Message));
				throw exc;
			}
			return officerAssigns;
		}

		private static IList GetOfficerAssigns(UnitData unit, UserApplicationData app)
		{
			IList officerAssigns = new ArrayList();
			//TODO: CUANDO SE AADA LA ASIGNACIN DE OFICIALES
			try
			{
				if (SmartCadDatabase.IsInitialize(unit.SetOfficerAssigns) == false)
				{
					unit.SetOfficerAssigns = new HashedSet(SmartCadDatabase.SearchObjects(
								SmartCadHqls.GetCustomHql(SmartCadHqls.GetUnitOfficerAssignsByUnitCode, unit.Code, DateTime.Now.ToString(ApplicationUtil.DateNHibernateFormat))));
				}
				if (unit.SetOfficerAssigns != null)
				{
					foreach (UnitOfficerAssignHistoryData officerAssign in unit.SetOfficerAssigns)
					{
						OfficerClientData officerClient = new OfficerClientData();
						officerClient.Code = officerAssign.Officer.Code;
						officerClient.Badge = officerAssign.Officer.Badge;

						officerClient.DepartmentType = new DepartmentTypeClientData();
						officerClient.DepartmentType.Code = officerAssign.Officer.Department.Code;
						officerClient.DepartmentType.Name = officerAssign.Officer.Department.Name;

						officerClient.FirstName = officerAssign.Officer.FirstName;
						officerClient.LastName = officerAssign.Officer.LastName;
						officerClient.PersonID = officerAssign.Officer.PersonId;
						if (officerAssign.Officer.Position != null)
							officerClient.Position = PositionConversion.ToClient(officerAssign.Officer.Position, app);
						officerClient.UnitAssignCode = officerAssign.Unit.Code;
						officerClient.Version = officerAssign.Officer.Version;

						officerAssigns.Add(officerClient);
					}
				}
			}
			catch (Exception exc)
			{
                SmartLogger.Print(ResourceLoader.GetString2("FormattedException", "Dispatch", "unit.SetOfficerAssigns", exc.Message));
				throw exc;
			}
			return officerAssigns;
		}
    }
}