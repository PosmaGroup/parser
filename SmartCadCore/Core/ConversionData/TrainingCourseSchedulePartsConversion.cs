using System;
using System.Collections;
using System.Text;
using SmartCadCore.Model;
using SmartCadCore.ClientData;


namespace SmartCadCore.Core
{
    public class TrainingCourseSchedulePartsConversion
    {
        public static TrainingCourseSchedulePartsClientData ToClient(TrainingCourseSchedulePartsData trainingCourseScheduleParts, UserApplicationData application)
        {
            TrainingCourseSchedulePartsClientData trainingCourseSchedulePartsClient = new TrainingCourseSchedulePartsClientData();
            trainingCourseSchedulePartsClient.Code = trainingCourseScheduleParts.Code;
            if (trainingCourseScheduleParts.Start.HasValue == true)
                trainingCourseSchedulePartsClient.Start = trainingCourseScheduleParts.Start.Value;
            if (trainingCourseScheduleParts.End.HasValue == true)
                trainingCourseSchedulePartsClient.End = trainingCourseScheduleParts.End.Value;
            trainingCourseSchedulePartsClient.Room = trainingCourseScheduleParts.Room;
            trainingCourseSchedulePartsClient.Version = trainingCourseScheduleParts.Version;
            if (trainingCourseScheduleParts.TrainingCourseSchedule != null)
            {
                trainingCourseSchedulePartsClient.TrainingCourseSchedule = new TrainingCourseScheduleClientData();
                trainingCourseSchedulePartsClient.TrainingCourseSchedule.Code = trainingCourseScheduleParts.TrainingCourseSchedule.Code;
            }
            return trainingCourseSchedulePartsClient;
        }

        public static TrainingCourseSchedulePartsData ToObject(TrainingCourseSchedulePartsClientData trainingCourseSchedulePartsClient, UserApplicationData application)
        {
            TrainingCourseSchedulePartsData trainingCourseScheduleParts = new TrainingCourseSchedulePartsData();
            trainingCourseScheduleParts.Code = trainingCourseSchedulePartsClient.Code;
            trainingCourseScheduleParts.Start = trainingCourseSchedulePartsClient.Start;
            trainingCourseScheduleParts.End = trainingCourseSchedulePartsClient.End;
            trainingCourseScheduleParts.Room = trainingCourseSchedulePartsClient.Room;
            trainingCourseScheduleParts.Version = trainingCourseSchedulePartsClient.Version;
            if (trainingCourseSchedulePartsClient.TrainingCourseSchedule != null)
            {
                trainingCourseScheduleParts.TrainingCourseSchedule = new TrainingCourseScheduleData();
                trainingCourseScheduleParts.TrainingCourseSchedule.Code = trainingCourseSchedulePartsClient.TrainingCourseSchedule.Code;
            }
            return trainingCourseScheduleParts;
        }

    }
}