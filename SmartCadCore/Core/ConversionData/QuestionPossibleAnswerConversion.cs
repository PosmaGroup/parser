using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.ClientData;
using SmartCadCore.Model;

namespace SmartCadCore.Core
{
    public class QuestionPossibleAnswerConversion
    {
        public static QuestionPossibleAnswerClientData ToClient(QuestionPossibleAnswerData questionPossibleAnswer, UserApplicationData app)
        {
            QuestionPossibleAnswerClientData convertedClientObject = new QuestionPossibleAnswerClientData();

            //if (app.Equals(UserApplicationData.FirstLevel)
            //    || app.Equals(UserApplicationData.Cctv)
            //    || app.Equals(UserApplicationData.SocialNetworks) 
            //    || app.Equals(UserApplicationData.Alarm) 
            //    || app.Equals(UserApplicationData.Administration))
            //{
                convertedClientObject.Code = questionPossibleAnswer.Code;
                convertedClientObject.Version = questionPossibleAnswer.Version;
                convertedClientObject.Description = questionPossibleAnswer.Description;
                convertedClientObject.Procedure = questionPossibleAnswer.Procedure;
                convertedClientObject.QuestionCode = questionPossibleAnswer.Question.Code;
                convertedClientObject.ControlName = questionPossibleAnswer.ControlName;
            //}
            
            return convertedClientObject;
        }

        public static QuestionPossibleAnswerData ToObject(QuestionPossibleAnswerClientData var, UserApplicationData app)
        {
            QuestionPossibleAnswerData retval = new QuestionPossibleAnswerData();
            retval.Code = var.Code;
            retval.Description = var.Description;
            retval.Procedure = var.Procedure;
            retval.ControlName = var.ControlName;
            retval.Question = new QuestionData();
            retval.Question.Code = var.QuestionCode;
            return retval;
        }
    }
}
