﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;
using System.Collections;

namespace SmartCadCore.Core
{
    public class GPSTypeConversion
    {
        public static GPSTypeClientData ToClient(GPSTypeData gpsType, UserApplicationData app)
        {
            GPSTypeClientData client = new GPSTypeClientData();
            client.Code = gpsType.Code;
            client.Version = gpsType.Version;
            client.Brand = gpsType.Brand;
            client.Model = gpsType.Model;

            if (SmartCadDatabase.IsInitialize(gpsType.Pines) == true)
            {
                client.Pines = new ArrayList();
                foreach (GPSPinData pin in gpsType.Pines)
                    client.Pines.Add(GPSPinConversion.ToClient(pin, app));
            }

            return client;
        }

        public static GPSTypeData ToObject(GPSTypeClientData gpsType, UserApplicationData app)
        {
            GPSTypeData data = new GPSTypeData();
            data.Code = gpsType.Code;
            data.Model = gpsType.Model;
            data.Brand = gpsType.Brand;
            data.Version = gpsType.Version;

            if (gpsType.Pines != null)
            {
                data.Pines = new ArrayList();
                foreach (GPSPinClientData pin in gpsType.Pines)
                    data.Pines.Add(GPSPinConversion.ToObject(pin, app));

            }


            return data;
        }
    }
}
