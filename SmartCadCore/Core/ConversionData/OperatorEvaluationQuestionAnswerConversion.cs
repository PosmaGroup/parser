using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.Model;
using SmartCadCore.ClientData;

namespace SmartCadCore.Core
{
    public class OperatorEvaluationQuestionAnswerConversion
    {
        public static OperatorEvaluationQuestionAnswerClientData ToClient(OperatorEvaluationQuestionAnswerData data, UserApplicationData app) 
        {
            OperatorEvaluationQuestionAnswerClientData client = new OperatorEvaluationQuestionAnswerClientData();
			client.Code = data.Code;
            client.OperatorEvaluationCode = data.OperatorEvaluation.Code;
            client.Answer = data.Answer;
			client.QuestionName = data.EvaluationQuestion;
            return client;
        }

        public static OperatorEvaluationQuestionAnswerData ToObject(OperatorEvaluationQuestionAnswerClientData data, UserApplicationData app) 
        {
            OperatorEvaluationQuestionAnswerData obj = new OperatorEvaluationQuestionAnswerData();
            obj.Code = data.Code;
            obj.Answer = data.Answer;
            obj.EvaluationQuestion = data.QuestionName;
            return obj;
        }
    }
}
