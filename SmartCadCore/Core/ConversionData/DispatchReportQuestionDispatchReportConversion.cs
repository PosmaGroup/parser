﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;

namespace SmartCadCore.Core
{
   public class DispatchReportQuestionDispatchReportConversion
    {

       public static DispatchReportQuestionDispatchReportClientData ToClient(DispatchReportQuestionDispatchReportData data, UserApplicationData app)
        {
            DispatchReportQuestionDispatchReportClientData client = new DispatchReportQuestionDispatchReportClientData();

            client.Code = data.Code;
            client.Version = data.Version;

            client.ReportCode = data.Report.Code;
            client.QuestionCode = data.Question.Code;
            client.QuestionText = data.Question.Text;
            client.QuestionRender = data.Question.Render;
            client.QuestionRequired = data.Question.Required;
            client.Order = data.Order;

            return client;
        }

       public static DispatchReportQuestionDispatchReportData ToObject(DispatchReportQuestionDispatchReportClientData client, UserApplicationData app)
       {
           DispatchReportQuestionDispatchReportData data = new DispatchReportQuestionDispatchReportData();

           data.Code = client.Code;
           data.Version = client.Version;
           data.Order = client.Order;

           DispatchReportData report = new DispatchReportData();
           report.Code = client.ReportCode;
           data.Report = report;

           QuestionDispatchReportData question = new QuestionDispatchReportData();
           question.Code = client.QuestionCode;
           question.Text = client.QuestionText;
           question.Render = client.QuestionRender;
           question.Required = client.QuestionRequired;
           data.Question = question;

           return data;
        }
    }
}
