﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.Model;
using SmartCadCore.ClientData;

namespace SmartCadCore.Core
{
    public class EvaluationUserAppllicationConversion
    {
        public static EvaluationUserApplicationClientData ToClient(EvaluationUserApplicationData evalAppData, UserApplicationData app)
        {
            EvaluationUserApplicationClientData evalAppClientData = new EvaluationUserApplicationClientData();
            evalAppClientData.Code = evalAppData.Code;
            evalAppClientData.Version = evalAppData.Version;
            if (evalAppData.Application != null)
            {
                evalAppClientData.Application = UserApplicationConversion.ToClient(evalAppData.Application, app);
            }
            if (evalAppData.Evaluation != null)
            {
                evalAppClientData.Evaluation = new EvaluationClientData();
                evalAppClientData.Evaluation.Code = evalAppData.Evaluation.Code;
                evalAppClientData.Evaluation.Version = evalAppData.Evaluation.Version;
                evalAppClientData.Evaluation.Description = evalAppData.Evaluation.Description;
                evalAppClientData.Evaluation.Name = evalAppData.Evaluation.Name;
            }
            return evalAppClientData;
        }

        public static EvaluationUserApplicationData ToObject(EvaluationUserApplicationClientData evalAppClientData, UserApplicationData app)
        {
            EvaluationUserApplicationData evalData = new EvaluationUserApplicationData();
            evalData.Code = evalAppClientData.Code;
            evalData = (EvaluationUserApplicationData)SmartCadDatabase.RefreshObject(evalData);

            return evalData;
        }
    }
}
