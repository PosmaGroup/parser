﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;

namespace SmartCadCore.Core
{
    public class ApplicationPreferenceConversion
    {
        public static ApplicationPreferenceClientData ToClient(ApplicationPreferenceData preference, UserApplicationData app)
        {
            ApplicationPreferenceClientData preferenceClientData = new ApplicationPreferenceClientData();
            preferenceClientData.Code = preference.Code;
            preferenceClientData.Version = preference.Version;
            preferenceClientData.Name = preference.Name;
            preferenceClientData.Type = preference.Type;
            preferenceClientData.Module = preference.Module;
            preferenceClientData.Value = preference.Value;
            preferenceClientData.Description = preference.Description;
            preferenceClientData.MeasureUnit = preference.Measure;
            preferenceClientData.MinValue = preference.MinValue;
            preferenceClientData.MaxValue = preference.MaxValue;
            preferenceClientData.ValueRange = preference.ValueRangeCustomCode;
            return preferenceClientData;
        }

        public static ApplicationPreferenceData ToObject(ApplicationPreferenceClientData preferenceClient, UserApplicationData app)
        {
            ApplicationPreferenceData preference = new ApplicationPreferenceData();
            preference.Code = preferenceClient.Code;
            preference.Version = preferenceClient.Version;
            preference.Name = preferenceClient.Name;
            preference.Type = preferenceClient.Type;
            preference.Module = preferenceClient.Module;
            preference.Value = preferenceClient.Value;
            preference.Description = preferenceClient.Description;
            preference.Measure = preferenceClient.MeasureUnit;
            preference.MinValue = preferenceClient.MinValue;
            preference.MaxValue = preferenceClient.MaxValue;
            preference.ValueRangeCustomCode = preferenceClient.ValueRange;
            return preference;
        }
    }
}
