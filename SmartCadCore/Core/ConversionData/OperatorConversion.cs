using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;
using System.Collections;
using Iesi.Collections;

namespace SmartCadCore.Core
{
    public class OperatorConversion
    {
        public static OperatorClientData ToClient(OperatorData operatorData, UserApplicationData app)
        {
            OperatorClientData operatorClient = new OperatorClientData();

            operatorClient.Code = operatorData.Code;

            operatorClient.Version = operatorData.Version;
            operatorClient.FirstName = operatorData.FirstName;
            operatorClient.LastName = operatorData.LastName;
            operatorClient.Login = operatorData.Login;
            operatorClient.AgentId = operatorData.AgentID;
            operatorClient.AgentPassword = operatorData.AgentPassword;

            if (operatorData.Role != null)
            {
                operatorClient.RoleName = operatorData.Role.Name;
                operatorClient.RoleFriendlyName = operatorData.Role.FriendlyName;
                operatorClient.RoleCode = operatorData.Role.Code;
            }
            if (app.Equals(UserApplicationData.Supervision))
            {
                #region Supervision
                operatorClient.IsSupervisor = ServiceUtil.CheckSupervisor(operatorClient.RoleCode);
                operatorClient.Address = operatorData.Address;
                operatorClient.PersonID = operatorData.PersonId;
                operatorClient.Birthday = operatorData.Birthday;
                operatorClient.Email = operatorData.Email;
                operatorClient.Telephone = operatorData.Telephone;

                if (operatorData.DepartmentTypes != null)// && SmartCadDatabase.IsInitialize(operatorData.DepartmentTypes) == true)
                {
                    operatorData.DepartmentTypes = (IList)SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetDepartmentsTypeByOperatorCode, operatorData.Code));
                    operatorClient.DepartmentTypes = BuildDepartmentTypes(operatorData.DepartmentTypes, app);
                }

                if (operatorData.Operators != null && SmartCadDatabase.IsInitialize(operatorData.Operators) == true)
                {
                    operatorData.Operators = SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetOperatorsBySupervisorCode, operatorData.Code));
                    operatorClient.Operators = BuildOperators(operatorData.Operators);
                }

                operatorData.Supervisors = (IList)SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetSupervisorsByOperatorCode, operatorData.Code));
                if (operatorData.Supervisors != null)
                {
                    operatorClient.SupervisorCodes = BuildSupervisorCodes(operatorData.Supervisors);
                    operatorClient.Supervisors = BuildSupervisors(operatorData.Supervisors);
                }

                operatorClient.WorkShifts = new ArrayList();
                if (SmartCadDatabase.IsInitialize(operatorData.WorkShifts) == false)
                {
                    operatorData.WorkShifts = (IList)SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.WorkShiftOperatorWithSchedulesByOperatorCode, operatorData.Code));
                }

                if (operatorData.WorkShifts.Count > 0)
                {
                    if (operatorData.WorkShifts[0] is WorkShiftVariationData)
                    {
                        foreach (WorkShiftVariationData wsv in operatorData.WorkShifts)
                        {
                            operatorClient.WorkShifts.Add(WorkShiftVariationConversion.ToClient(wsv, UserApplicationData.Supervision));
                        }
                    }
                    else
                    {
                        foreach (WorkShiftOperatorData wsv in operatorData.WorkShifts)
                        {
                            if (wsv.WorkShift != null)
                                operatorClient.WorkShifts.Add(WorkShiftVariationConversion.ToClient(wsv.WorkShift, UserApplicationData.Supervision));
                        }
                    }
                }


                string computerName = (string)SmartCadDatabase.SearchBasicObject(SmartCadSQL.GetCustomSQL(SmartCadHqls.GetComputerNameByUserLogin, operatorData.Login));
                RoomSeatData seat = (RoomSeatData)SmartCadDatabase.SearchBasicObject(SmartCadSQL.GetCustomSQL(SmartCadHqls.GetRoomsSeatByComputerName, computerName));

                if (seat != null && seat.Room != null)
                {
                    operatorClient.RoomName = seat.Room.Name;
                    operatorClient.RoomCode = seat.Room.Code;
                }
                operatorClient.CategoryList = new ArrayList();
                try
                {
                    if (SmartCadDatabase.IsInitialize(operatorData.CategoryHistoryList) == false)
                    {
                        SmartCadDatabase.InitializeLazy(operatorData, operatorData.CategoryHistoryList);
                        int i = operatorData.CategoryHistoryList.Count;
                    }
                }
                catch
                {
                    try
                    {
                        operatorData.CategoryHistoryList = new ListSet(SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetOperatorCategoryHistoryByOperatorCode, operatorData.Code)));
                    }
                    catch
                    {
                        operatorData.CategoryHistoryList = null;
                    }
                }

                if (operatorData.CategoryHistoryList != null)
                {
                    foreach (OperatorCategoryHistoryData category in operatorData.CategoryHistoryList)
                    {
                        OperatorCategoryHistoryClientData ochcd = OperatorCategoryHistoryConversion.ToClient(category, app);
                        if (operatorData.Deleted == false)
                        {
                            ochcd.OperatorCode = operatorData.Code;
                            operatorClient.CategoryList.Add(ochcd);
                        }
                        if (category.EndDate == null)
                        {
                            operatorClient.OperatorCategory = category.Category.FriendlyName;
                            operatorClient.OperatorCategoryCode = category.Category.Code;
                        }
                    }
                }

                IList lastSessionHistories = SmartCadDatabase.SearchObjects(
                    SmartCadHqls.GetCustomHql(SmartCadHqls.GetLastSessionHistories, operatorClient.Code));
                operatorClient.LastSessions = new ArrayList();
                if (lastSessionHistories.Count > 0)
                {
                    foreach (SessionHistoryData shd in lastSessionHistories)
                    {
                        operatorClient.LastSessions.Add(SessionHistoryConversion.ToClient(shd, app));
                    }
                }
                #endregion
            }
            else if (app.Equals(UserApplicationData.FirstLevel) || app.Equals(UserApplicationData.Dispatch))
            {
                #region FirstLevel || Dispatch
                operatorData.DepartmentTypes = (IList)SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetDepartmentsTypeByOperatorCode, operatorData.Code));
                operatorClient.DepartmentTypes = BuildDepartmentTypes(operatorData.DepartmentTypes, app);

                IList lastSessionHistories = SmartCadDatabase.SearchObjects(
                    SmartCadHqls.GetCustomHql(SmartCadHqls.GetLastSessionHistories, operatorClient.Code));
                operatorClient.LastSessions = new ArrayList();
                if (lastSessionHistories.Count > 0)
                {
                    foreach (SessionHistoryData shd in lastSessionHistories)
                    {
                        SessionHistoryClientData sess = SessionHistoryConversion.ToClient(shd, app);
                        operatorClient.LastSessions.Add(sess);
                    }
                }
                if (operatorData.WorkShifts != null)
                {
					operatorData.WorkShifts = (IList)SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetWorkShiftVariationByOperatorCode, operatorData.Code));
                    operatorClient.WorkShifts = new ArrayList();
                    foreach (WorkShiftVariationData wsv in operatorData.WorkShifts)
                    {
                        operatorClient.WorkShifts.Add(WorkShiftVariationConversion.ToClient(wsv, UserApplicationData.Supervision));

                    }
                }
                #endregion
            }
            else if (app.Equals(UserApplicationData.Administration))
            {
                #region Administration
                operatorClient.Windows = operatorData.Windows;
                operatorClient.Password = operatorData.Password;
                operatorClient.PersonID = operatorData.PersonId;
                operatorClient.Birthday = operatorData.Birthday;
                operatorClient.Address = operatorData.Address;
                operatorClient.Telephone = operatorData.Telephone;
                operatorClient.Email = operatorData.Email;
                if (operatorData.InitializeCollections == true)
                {
                    operatorData.DepartmentTypes = (IList)SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetDepartmentsTypeByOperatorCode, operatorData.Code));
                    operatorClient.DepartmentTypes = BuildDepartmentTypes(operatorData.DepartmentTypes, app);

                    operatorData.Devices = SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetOperatorDevicesByOperatorCode, operatorData.Code));

                    if (operatorData.Devices != null)
                    {
                        foreach (OperatorDeviceData cameraData in operatorData.Devices)
                        {
                            cameraData.User = null;
                            OperatorDeviceClientData opercam = OperatorDeviceConversion.ToClient(cameraData, app);
                            opercam.User = operatorClient;
                            operatorClient.Cameras.Add(opercam);
                        }
                    }
                }
                #endregion
            }
            operatorClient.Picture = operatorData.Picture;
            return operatorClient;

        }

        private static IList BuildDepartmentTypes(IList departmentTypes,UserApplicationData app)
        {
            IList deparmentTypesClient = new ArrayList();

            foreach (DepartmentTypeData dt in departmentTypes)
            {
                if (dt != null)
                {
                    DepartmentTypeClientData departmentTypeClientData = new DepartmentTypeClientData();
                    departmentTypeClientData.Code = dt.Code;
                    departmentTypeClientData.Version = dt.Version;
                    departmentTypeClientData.Name = dt.Name;

                    deparmentTypesClient.Add(departmentTypeClientData);
                    //deparmentTypesClient.Add(DepartmentTypeConversion.ToClient(dt, app));
                }
            }

            return deparmentTypesClient;
        }

        public static OperatorData ToObject(OperatorClientData clientData, UserApplicationData application)
        {
            OperatorData operatorData = new OperatorData();
            operatorData.Code = clientData.Code;
            operatorData.Login = clientData.Login;

            if (operatorData.Code != 0 )
				operatorData = (OperatorData)SmartCadDatabase.RefreshObject(operatorData);
			else if (string.IsNullOrEmpty(operatorData.Login) == false && operatorData.Login == OperatorData.InternalSupervisor.Login)
                operatorData = SmartCadDatabase.SearchObject<OperatorData>(operatorData);
            operatorData.Version = clientData.Version;

            if (application.Name == UserApplicationData.Administration.Name)
            {
                operatorData.Login = clientData.Login;
                operatorData.Address = clientData.Address;
                operatorData.AgentID = clientData.AgentId;
                operatorData.AgentPassword = clientData.AgentPassword;
                operatorData.Birthday = clientData.Birthday;
                if (clientData.Cameras != null && clientData.Cameras.Count >0)
                {
                    operatorData.Devices = new ArrayList();
                    foreach (OperatorDeviceClientData camera in clientData.Cameras)
                    {
                        OperatorDeviceData cameraData = new OperatorDeviceData();
                        cameraData.Code = camera.Code;
                        cameraData.Version = camera.Version;
                        cameraData.Device = new CameraData();
                        cameraData.Device.Code = camera.Device.Code;
                        cameraData.Device.Version = camera.Device.Version;
                        cameraData.End = camera.End;
                        cameraData.Start = camera.Start;
                        cameraData.User = operatorData;
                        //if (camera.User != null)
                        //{
                        //    cameraData.User = new OperatorData();
                        //    cameraData.User.Code = camera.User.Code;
                        //    cameraData.User.Version = camera.User.Version;
                        //}

                        operatorData.Devices.Add(cameraData);
                    }
                }
                operatorData.DepartmentTypes = new ArrayList();
                if (clientData.DepartmentTypes != null && clientData.DepartmentTypes.Count >0)
                {
                    foreach (DepartmentTypeClientData dep in clientData.DepartmentTypes)
                    {
                        DepartmentTypeData depData = new DepartmentTypeData();
                        depData.Code = dep.Code;
                        depData.Version = dep.Version;
                        operatorData.DepartmentTypes.Add(depData);
                    }
                }
                operatorData.Email = clientData.Email;
                operatorData.FirstName = clientData.FirstName;
                operatorData.LastName = clientData.LastName;
                if (clientData.Operators != null && clientData.Operators.Count>0)
                {
                    operatorData.Operators = new ArrayList();
                    foreach (OperatorClientData oper in clientData.Operators)
                    {
                        OperatorData operData = new OperatorData();
                        operData.Code = oper.Code;
                        operData.Version = oper.Version;
                        operatorData.Operators.Add(operData);
                    }
                }
                operatorData.Password = clientData.Password;
                operatorData.PersonId = clientData.PersonID;
                operatorData.Picture = clientData.Picture;
                UserRoleData role = new UserRoleData();
                role.Code = clientData.RoleCode;
                operatorData.Role = (UserRoleData)SmartCadDatabase.RefreshObject(role);
                if (clientData.Supervisors != null && clientData.Supervisors.Count>0)
                {
                    operatorData.Supervisors = new ArrayList();
                    foreach (OperatorClientData oper in clientData.Supervisors)
                    {
                        OperatorData operData = new OperatorData();
                        operData.Code = oper.Code;
                        operData.Version = oper.Version;
                        operatorData.Supervisors.Add(operData);
                    }
                }
                operatorData.Telephone = clientData.Telephone;
                operatorData.Windows = clientData.Windows;
                if (clientData.WorkShifts != null)
                {
                    foreach (WorkShiftVariationClientData wsv in clientData.WorkShifts)
                    {
                        WorkShiftVariationData ws = new WorkShiftVariationData();
                        ws.Code = wsv.Code;
                        ws.Version = wsv.Version;
                        WorkShiftOperatorData wso = new WorkShiftOperatorData() { WorkShift = ws, Operator = operatorData };
                        operatorData.WorkShifts.Add(wso);
                    }                 
                }
				if (clientData.CategoryList != null && clientData.CategoryList.Count > 0)
				{
					operatorData.CategoryHistoryList = new HashedSet();
					foreach (OperatorCategoryHistoryClientData item in clientData.CategoryList)
					{
						OperatorCategoryHistoryData ochd = (OperatorCategoryHistoryData)Conversion.ToObject(item, application);
						ochd.Operator = operatorData;
						operatorData.CategoryHistoryList.Add(ochd);

					}
				}
			}
            return operatorData;
        }

        private static IList BuildOperators(IList operators)
        {
            IList operatorsCode = new ArrayList();
            foreach (OperatorAssignData oad in operators)
            {
                OperatorAssignClientData oacd = OperatorAssignConversion.ToClient(oad, UserApplicationData.Supervision);
                if (operatorsCode.Contains(oacd) == false)
                    operatorsCode.Add(oacd);
            }
            return operatorsCode;
        }

        private static IList BuildSupervisors(IList supervisors)
        {
            IList supervisorsCode = new ArrayList();
            foreach (OperatorAssignData oad in supervisors)
            {
                OperatorAssignClientData oacd = OperatorAssignConversion.ToClient(oad, UserApplicationData.Supervision);
                if (supervisorsCode.Contains(oacd) == false)
                    supervisorsCode.Add(oacd);
            }
            //foreach (OperatorAssignData oad in supervisors)
            //{
            //    if (supervisorsCode.Contains(oad.Code) == false)
            //        supervisorsCode.Add(oad.Code);
            //}
            return supervisorsCode;
        }

        private static IList BuildSupervisorCodes(IList supervisors)
        {
            IList supervisorsCodes = new ArrayList();
            foreach (OperatorAssignData oad in supervisors)
            {
                if (supervisorsCodes.Contains(oad.Supervisor.Code) == false)
                    supervisorsCodes.Add(oad.Supervisor.Code);
            }
            return supervisorsCodes;
        }
    }
}