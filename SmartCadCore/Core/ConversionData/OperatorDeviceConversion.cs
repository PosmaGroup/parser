﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;

namespace SmartCadCore.Core
{
    public class OperatorDeviceConversion
    {
        public static OperatorDeviceClientData ToClient(OperatorDeviceData operDevice, UserApplicationData userApplication)
        {
            OperatorDeviceClientData clientOperDevice = new OperatorDeviceClientData();
            clientOperDevice.Code = operDevice.Code;
            clientOperDevice.Version = operDevice.Version;
            if(operDevice.User != null)
                clientOperDevice.User = OperatorConversion.ToClient(operDevice.User,userApplication);
            clientOperDevice.Device = DeviceConversion.ToClient(operDevice.Device, userApplication);//CameraConversion.ToClient(operCamera.Device, userApplication);
            clientOperDevice.Start = operDevice.Start;
            clientOperDevice.End = operDevice.End;
            return clientOperDevice;
        }

        public static OperatorDeviceData ToObject(OperatorDeviceClientData clientOperDevice)
        {
            if (clientOperDevice == null)
                return null;
            OperatorDeviceData operDevice = new OperatorDeviceData();
            operDevice.Code = clientOperDevice.Code;
            if (operDevice.Code != 0)
                operDevice = (OperatorDeviceData)SmartCadDatabase.RefreshObject(operDevice);

            operDevice.Version = clientOperDevice.Version;
            if (clientOperDevice.User != null)
            {
                operDevice.User = new OperatorData();
                operDevice.User.Code = clientOperDevice.User.Code;
            }
            if (clientOperDevice.Device is CameraClientData)
            {
                operDevice.Device = new CameraData();
            }
            if (clientOperDevice.Device is DataloggerClientData)
            {
                operDevice.Device = new DataloggerData();
            }
            else if (clientOperDevice.Device is LprClientData)
            {
                operDevice.Device = new LprData();
            }
            operDevice.Device.Code = clientOperDevice.Device.Code;
            operDevice.Start = clientOperDevice.Start;
            operDevice.End = clientOperDevice.End;
            
            return operDevice;
        }
    }
}
