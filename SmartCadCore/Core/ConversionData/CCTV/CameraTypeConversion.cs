﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.Model;
using SmartCadCore.ClientData;

namespace SmartCadCore.Core
{
    public class CameraTypeConversion
    {
        public static CameraTypeClientData ToClient(CameraTypeData cameraType, UserApplicationData app)
        {
            CameraTypeClientData client = null;
            if (cameraType != null)
            {
                client = new CameraTypeClientData();
                client.Code = cameraType.Code;
                client.Version = cameraType.Version;
                client.Name = cameraType.Name;
            }
            return client;
        }

        public static CameraTypeData ToObject(CameraTypeClientData cameraType, UserApplicationData app)
        {
            CameraTypeData data = new CameraTypeData();
            data.Code = cameraType.Code;
            data.Name = cameraType.Name;

            return data;
        }
    }
}
