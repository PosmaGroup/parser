using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using SmartCadCore.Model;
using Iesi.Collections;
using SmartCadCore.ClientData;

namespace SmartCadCore.Core
{
    public class StructConversion
    {
        public static StructClientData ToClient(StructData structData, UserApplicationData app)
        {
            StructClientData structClientData = new StructClientData();
            structClientData.Code = structData.Code;
            structClientData.Version = structData.Version;
            structClientData.Name = structData.Name;
            structClientData.State = structData.Addres.State;
            structClientData.Town = structData.Addres.Town;
            structClientData.Street = structData.Addres.Street;
            structClientData.Type = StructTypeConversion.ToClient(structData.Type, app);

            if (app.Name == UserApplicationData.Administration.Name)
            {
                #region Administration
                if (structData.Zone != null)
                {
                    structClientData.CctvZone = new CctvZoneClientData();
                    structClientData.CctvZone.Code = structData.Zone.Code;
                    //structData.Zone = (CctvZoneData)SmartCadDatabase.RefreshObject(structData.Zone);
                    structClientData.CctvZone.Version = structData.Zone.Version;
                    structClientData.CctvZone.Name = structData.Zone.Name;
                }

                structClientData.CamerasNumber = Convert.ToInt32(SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(SmartCadHqls.GetAmountVideoDevicesByStructName, structData.Name)));

                if (structData.InitializeCollections == true)
                {
                    structClientData.Lat = structData.Addres.Lat;
                    structClientData.Lon = structData.Addres.Lon;
                    structClientData.ZoneSecRef = structData.Addres.Reference;
                    structClientData.IsSynchronized = structData.Addres.IsSynchronized;

                    structClientData.Devices = new ArrayList();
                    if (structData.Devices != null)
                    {
                        if (SmartCadDatabase.IsInitialize(structData.Devices) == false)
                        {
                            structData.Devices = new ListSet(SmartCadDatabase.SearchObjects(
                                    SmartCadHqls.GetCustomHql(SmartCadHqls.GetDevicesByStructCode,structData.Code)));
                        }

                        structClientData.CamerasNumber = structData.Devices.Count;
                        foreach (VideoDeviceData device in structData.Devices)
                        {
                            device.Struct = null;
                            DeviceClientData deviceClient = DeviceConversion.ToClient(device, app);
                            (deviceClient as VideoDeviceClientData).StructClientData = structClientData;
                            structClientData.Devices.Add(deviceClient);
                        }
                    }
                    if (structData.Zone != null)
                    {
                        structData.Zone.Structs = null;
                        structClientData.CctvZone = CctvZoneConversion.ToClient(structData.Zone, app);
                    }
                }
                #endregion
            }
            else
            {
                structClientData.Lat = structData.Addres.Lat;
                structClientData.Lon = structData.Addres.Lon;
                structClientData.ZoneSecRef = structData.Addres.Reference;
                structClientData.IsSynchronized = structData.Addres.IsSynchronized;

                structClientData.Devices = new ArrayList();
                structClientData.CamerasNumber = 0;
                if (structData.Devices != null)
                {
                    if (SmartCadDatabase.IsInitialize(structData.Devices) == false)
                        SmartCadDatabase.InitializeLazy(structData, structData.Devices);

                    if (SmartCadDatabase.IsInitialize(structData.Devices) == false)
                    {
                        structData.Devices = new ListSet(SmartCadDatabase.SearchObjects(
                                SmartCadHqls.GetCustomHql(SmartCadHqls.GetDevicesByStructCode, structData.Code)));
                    }
                    structClientData.CamerasNumber = structData.Devices.Count;
                    foreach (DeviceData cam in structData.Devices)
                    {
                        structClientData.Devices.Add(DeviceConversion.ToClient(cam, app));
                    }
                }
                if (structData.Zone != null)
                {
                    structData.Zone.Structs = null;
                    structClientData.CctvZone = CctvZoneConversion.ToClient(structData.Zone, app);
                }
            }
            return structClientData;
            
        }
        
        public static StructData ToObject(StructClientData structClientData, UserApplicationData app)
        {
            StructData structData = new StructData();
            structData.Code = structClientData.Code;
            if(structData.Code != 0)
                structData = (StructData)SmartCadDatabase.RefreshObject(structData);
            
            structData.Version = structClientData.Version;
            structData.Name = structClientData.Name;
            if (structData.Addres == null)
            {
                structData.Addres = new StructAddressData();
            }
            structData.Addres.Lat = structClientData.Lat;
            structData.Addres.Lon = structClientData.Lon;
            structData.Addres.Reference = structClientData.ZoneSecRef;
            structData.Addres.State = structClientData.State;
            structData.Addres.Town = structClientData.Town;
            structData.Addres.Street = structClientData.Street;
            structData.Addres.IsSynchronized = structClientData.IsSynchronized;

            structData.Type = StructTypeConversion.ToObject( structClientData.Type,app);

            if (structClientData.Devices != null)
            {
                structData.Devices = new HashedSet();
                foreach (DeviceClientData device in structClientData.Devices)
                {
                    VideoDeviceData deviceData = null;
                    if (device is CameraClientData)
                    {
                        deviceData = new CameraData();
                        deviceData.Code = device.Code;
                        deviceData = (CameraData)SmartCadDatabase.RefreshObject(deviceData);
                        //((CameraData)deviceData).ParentCode = deviceData.Code;
                        structData.Devices.Add(deviceData);
                    }
                    if (device is DataloggerClientData)
                    {
                        deviceData = new DataloggerData();
                        deviceData.Code = device.Code;
                        deviceData = (DataloggerData)SmartCadDatabase.RefreshObject(deviceData);
                        //((DataloggerData)deviceData).ParentCode = deviceData.Code;
                        structData.Devices.Add(deviceData);
                    }
                    else if (device is LprClientData)
                    {
                        deviceData = new LprData();
                        deviceData.Code = device.Code;
                        deviceData = (LprData)SmartCadDatabase.RefreshObject(deviceData);
                        //((LprData)deviceData).ParentCode = deviceData.Code;
                        structData.Devices.Add(deviceData);
                    }
                    //deviceData.ParentCode = deviceData.Code;
                }
            }
            if (structClientData.CctvZone != null)
            {
                structData.Zone = new CctvZoneData();
                structData.Zone.Code = structClientData.CctvZone.Code;
                structData.Zone = (CctvZoneData)SmartCadDatabase.RefreshObject(structData.Zone);
            }

            return structData;
        }
    }
}