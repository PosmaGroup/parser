using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using SmartCadCore.Model;
using SmartCadCore.ClientData;

namespace SmartCadCore.Core
{
    public class StructTypeConversion
    {
        public static StructTypeClientData ToClient(StructTypeData structData, UserApplicationData app)
        {
            StructTypeClientData structClientData = new StructTypeClientData();
            structClientData.Code = structData.Code;
            structClientData.Version = structData.Version;
            structClientData.Name = structData.Name;                
            return structClientData;
            
        }
        
        public static StructTypeData ToObject(StructTypeClientData structClientData, UserApplicationData app)
        {
            StructTypeData structData = new StructTypeData();
            structData.Code = structClientData.Code;
            structData.Version = structClientData.Version;
            structData.Name = structClientData.Name;           
            SmartCadDatabase.RefreshObject(structData);
            return structData;
        }
    }
}