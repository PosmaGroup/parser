﻿using SmartCadCore.ClientData;
using SmartCadCore.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Core
{
    class VARuleConversion
    {
        public static VARuleClientData ToClient(VARuleData data, UserApplicationData app)
        {
            VARuleClientData client = new VARuleClientData();
            client.Code = data.Code;
            client.Name = data.Name;
            client.SaviCode = data.SaviCode;
            return client;
        }

        public static VARuleData ToObject(VARuleClientData client, UserApplicationData app)
        {
            VARuleData data = new VARuleData();
            data.Code = client.Code;
            data.Name = client.Name;
            data.SaviCode = client.SaviCode;
            return data;
        }
    }
}
