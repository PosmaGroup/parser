﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.Model;
using SmartCadCore.ClientData;

namespace SmartCadCore.Core
{
    public class VAAlertVideoInstanceConversion
    {
        public static VAAlertVideoInstanceClientData ToClient(VAAlertVideoInstanceData VAAlertVideoInstance, UserApplicationData app)
        {
            VAAlertVideoInstanceClientData client = null;
            if (VAAlertVideoInstance != null)
            {
                client = new VAAlertVideoInstanceClientData();
         //       client.Alert  = VAAlertVideoInstance.Alert;
                client.BoundingBoxButtom = VAAlertVideoInstance.BoundingBoxButtom;
                client.BoundingBoxLeft = VAAlertVideoInstance.BoundingBoxLeft;
                client.BoundingBoxRight = VAAlertVideoInstance.BoundingBoxRight;
                client.BoundingBoxTop = VAAlertVideoInstance.BoundingBoxTop;
                client.Code = VAAlertVideoInstance.Code;
                client.FootX = VAAlertVideoInstance.FootX;
                client.FootY = VAAlertVideoInstance.FootY;
                client.ObjectId = VAAlertVideoInstance.ObjectId;
                client.TimestampMilliseconds = VAAlertVideoInstance.TimestampMilliseconds;
                client.TimestampValue = VAAlertVideoInstance.TimestampValue;
                //client.TypeObject = VAAlertVideoInstance.TypeObject;
                client.Version = VAAlertVideoInstance.Version;
                
            }
            return client;
        }

        public static VAAlertVideoInstanceData ToObject(VAAlertVideoInstanceClientData VAAlertVideoInstance, UserApplicationData app)
        {
            VAAlertVideoInstanceData data = new VAAlertVideoInstanceData();
            data.Code = VAAlertVideoInstance.Code;
  //          data.Alert  = VAAlertVideoInstance.Alert;
            data.BoundingBoxButtom = VAAlertVideoInstance.BoundingBoxButtom;
            data.BoundingBoxLeft = VAAlertVideoInstance.BoundingBoxLeft;
            data.BoundingBoxRight = VAAlertVideoInstance.BoundingBoxRight;
            data.BoundingBoxTop = VAAlertVideoInstance.BoundingBoxTop;
            data.Code = VAAlertVideoInstance.Code;
            data.FootX = VAAlertVideoInstance.FootX;
            data.FootY = VAAlertVideoInstance.FootY;
            data.ObjectId = VAAlertVideoInstance.ObjectId;
            data.TimestampMilliseconds = VAAlertVideoInstance.TimestampMilliseconds;
            data.TimestampValue = VAAlertVideoInstance.TimestampValue;
            //data.TypeObject = VAAlertVideoInstance.TypeObject;
            data.Version = VAAlertVideoInstance.Version;

            return data;
        }
    }
}
