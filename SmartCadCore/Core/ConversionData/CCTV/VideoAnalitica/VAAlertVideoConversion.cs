﻿using SmartCadCore.ClientData;
using SmartCadCore.Model;
using System;
using System.Collections;
using Iesi.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadCore.Core
{
    class VAAlertVideoConversion
    {
        public static VAAlertVideoClientData ToClient(VAAlertVideoData data, UserApplicationData app)
        {
            VAAlertVideoClientData client = new VAAlertVideoClientData();
       
            client.BirthTimeValue = data.BirthTimeValue;
            client.BirtTimeMilliseconds = data.BirtTimeMilliseconds;
            client.DeathTimeMilliseconds = data.DeathTimeMilliseconds;
            client.DeathTimeValue = data.DeathTimeValue;
            client.ObjectId = data.ObjectId;
            client.TotalObjectInstances = data.TotalObjectInstances;
/*            client.VideoInstances = new System.Collections.Generic.List<VAAlertVideoInstanceClientData>();
            int i = data.VideoInstances.Count;  
            foreach (VAAlertVideoInstanceData videoInstance in data.VideoInstances)
            {
                client.VideoInstances.Add(VAAlertVideoInstanceConversion.ToClient(videoInstance,app));
            }
           
       */     
            
            return client;
        }

        public static VAAlertVideoData ToObject(VAAlertVideoClientData client, UserApplicationData app)
        {
            VAAlertVideoData data = new VAAlertVideoData();
     
            data.BirthTimeValue = client.BirthTimeValue;
            data.BirtTimeMilliseconds = client.BirtTimeMilliseconds;
            data.DeathTimeMilliseconds = client.DeathTimeMilliseconds;
            data.DeathTimeValue = client.DeathTimeValue;
            data.ObjectId = client.ObjectId;
            data.TotalObjectInstances = client.TotalObjectInstances;

            data.VideoInstances = new HashedSet();

            foreach (VAAlertVideoInstanceClientData instance in client.VideoInstances)
            {
                data.VideoInstances.Add(VAAlertVideoInstanceConversion.ToObject(instance, app));
            }
            
            return data;
        }
    }
}