﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;

namespace SmartCadCore.Core
{
    public class ConnectionTypeConversion
    {
        public static ConnectionTypeClientData ToClient(ConnectionTypeData connectionType, UserApplicationData app)
        {
            ConnectionTypeClientData client = null;
            if (connectionType != null)
            {
                client = new ConnectionTypeClientData();
                client.Code = connectionType.Code;
                client.Name = connectionType.Name;
            }
            return client;
        }

        public static ConnectionTypeData ToObject(ConnectionTypeClientData connectionType, UserApplicationData app)
        {
            ConnectionTypeData data = new ConnectionTypeData();
            data.Code = connectionType.Code;
            data.Name = connectionType.Name;

            return data;
        }
    }
}
