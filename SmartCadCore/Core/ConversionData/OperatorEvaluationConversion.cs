using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.Model;
using SmartCadCore.ClientData;
using System.Collections;

namespace SmartCadCore.Core
{
    public class OperatorEvaluationConversion
    {
        public static OperatorEvaluationClientData ToClient(OperatorEvaluationData data, UserApplicationData app)
        {
            OperatorEvaluationClientData client = new OperatorEvaluationClientData();
            client.EvaluationName = data.EvaluationName;
            client.Date = data.Date;
            client.EvaluatorCode = data.Evaluator.Code;
            client.EvaluatorFirstName = data.Evaluator.FirstName;
            client.EvaluatorLastName = data.Evaluator.LastName;
            client.Operator = OperatorConversion.ToClient(data.Operator, app);
            client.Qualification = data.Qualification;
            client.Category = data.OperatorCategory;
            client.RoleOperator = data.RoleOperator;
            client.Code = data.Code;

            switch (data.Scale)
            {
                case EvaluationData.EvaluationScales.OneToThree:
                    client.Scale = ClientEvaluationScale.OneToThree;
                    break;
                case EvaluationData.EvaluationScales.OneToFive:
                    client.Scale = ClientEvaluationScale.OneToFive;
                    break;
                case EvaluationData.EvaluationScales.YesNo:
                    client.Scale = ClientEvaluationScale.YesNo;
                    break;
            }

            if (SmartCadDatabase.IsInitialize(data.QuestionAnswers) == true)
            {
                //SmartCadDatabase.InitializeLazy(data, data.QuestionAnswers);

                ArrayList list = new ArrayList();
                foreach (OperatorEvaluationQuestionAnswerData var in data.QuestionAnswers)
                {
                    list.Add(OperatorEvaluationQuestionAnswerConversion.ToClient(var, app));
                }
                client.Questions = list;
            }
			return client;
        }

        public static OperatorEvaluationData ToObject(OperatorEvaluationClientData data, UserApplicationData app) 
        {
            OperatorEvaluationData obj = new OperatorEvaluationData();
            obj.Code = data.Code;
            obj.Date = data.Date;
            obj.EvaluationName = data.EvaluationName;
            OperatorData evaluator = new OperatorData();
            evaluator.Code = data.EvaluatorCode;
            obj.Evaluator = (OperatorData)SmartCadDatabase.RefreshObject(evaluator);
            OperatorData ope = new OperatorData();
            ope.Code = data.Operator.Code;
            obj.Operator = (OperatorData)SmartCadDatabase.RefreshObject(ope);
            obj.Qualification = data.Qualification;
            obj.RoleOperator = data.RoleOperator;
            obj.OperatorCategory = data.Category;

            ArrayList list = new ArrayList();
            foreach (OperatorEvaluationQuestionAnswerClientData var in data.Questions)
            {
                OperatorEvaluationQuestionAnswerData item = OperatorEvaluationQuestionAnswerConversion.ToObject(var, app);
                item.OperatorEvaluation = obj;
                list.Add(item);
            }
            switch (data.Scale)
            {
                case ClientEvaluationScale.YesNo:
                    obj.Scale = EvaluationData.EvaluationScales.YesNo;
                    break;
                case ClientEvaluationScale.OneToThree:
                    obj.Scale = EvaluationData.EvaluationScales.OneToThree;
                    break;
                case ClientEvaluationScale.OneToFive:
                    obj.Scale = EvaluationData.EvaluationScales.OneToFive;
                    break;
            }
            obj.QuestionAnswers = list;
            return obj;
        }
    }
}
