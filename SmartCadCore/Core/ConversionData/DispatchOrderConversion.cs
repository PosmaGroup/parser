using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.ClientData;
using SmartCadCore.Model;
using System.Collections;

namespace SmartCadCore.Core
{
    public class DispatchOrderConversion
    {
        public static DispatchOrderClientData ToClient(DispatchOrderData dispatchOrder, UserApplicationData app)
        {
            DispatchOrderClientData dispatchOrderClientData = new DispatchOrderClientData();

            if(app.Equals(UserApplicationData.Report))
            {
                dispatchOrderClientData.Code = dispatchOrder.Code;
                dispatchOrderClientData.Version = dispatchOrder.Version;
                dispatchOrderClientData.CustomCode = dispatchOrder.CustomCode;
                if (dispatchOrder.Unit != null)
                {
                    dispatchOrderClientData.DepartmentType = DepartmentTypeConversion.ToClient(dispatchOrder.Unit.DepartmentType,app);
                    dispatchOrder.Unit.DispatchOrder = null;
                    dispatchOrderClientData.Unit = UnitConversion.ToClient(dispatchOrder.Unit, app);
                    dispatchOrderClientData.Unit.DispatchOrder = dispatchOrderClientData;                    
                }

                

                dispatchOrderClientData.Status = DispatchOrderStatusConversion.ToClient(dispatchOrder.Status, app);
            }
            else if (app.Equals(UserApplicationData.Dispatch))
            {
                dispatchOrderClientData.Code = dispatchOrder.Code;
                dispatchOrderClientData.Version = dispatchOrder.Version;
                dispatchOrderClientData.CustomCode = dispatchOrder.CustomCode;
                dispatchOrderClientData.Status = DispatchOrderStatusConversion.ToClient(dispatchOrder.Status, app);
                dispatchOrderClientData.StartDate = dispatchOrder.StartDate.Value;
                dispatchOrderClientData.ArrivalDate = dispatchOrder.ArrivalDate.HasValue == true ? dispatchOrder.ArrivalDate.Value : DateTime.MinValue;
                dispatchOrderClientData.ExpectedTime = dispatchOrder.ExpectedTime.Value;
                if (dispatchOrder.IncidentNotification.DispatchOperator != null)
                {
                    dispatchOrderClientData.DispatchOperatorCode = dispatchOrder.IncidentNotification.DispatchOperator.Code;
                    dispatchOrderClientData.DispatchOperatorName = dispatchOrder.IncidentNotification.DispatchOperator.FirstName + " " + dispatchOrder.IncidentNotification.DispatchOperator.LastName;
                }
                dispatchOrderClientData.IncidentNotificationCode = dispatchOrder.IncidentNotification.Code;
                dispatchOrderClientData.IncidentNotificationCustomCode = dispatchOrder.IncidentNotification.CustomCode;
                dispatchOrderClientData.IncidentNotificationVersion = dispatchOrder.IncidentNotification.Version;
                dispatchOrderClientData.IncidentCode = dispatchOrder.IncidentNotification.ReportBase.Incident.Code;
                dispatchOrderClientData.IncidentCustomCode = dispatchOrder.IncidentNotification.ReportBase.Incident.CustomCode;

                

                if (dispatchOrder.Unit != null)
                {
                    if(dispatchOrder.Unit.DepartmentType != null)
                        dispatchOrderClientData.DepartmentType = DepartmentTypeConversion.ToClient(dispatchOrder.Unit.DepartmentType, app);
                    
                    dispatchOrder.Unit.DispatchOrder = null;
                    dispatchOrderClientData.Unit = UnitConversion.ToClient(dispatchOrder.Unit, app);
                    dispatchOrderClientData.Unit.DispatchOrder = dispatchOrderClientData;
                }

                dispatchOrderClientData.Address = AddressConversion.ToClient(dispatchOrder.IncidentNotification.ReportBase.Incident.Address, app);

                if (dispatchOrder.EndingReport != null && dispatchOrder.EndingReport.Active == true)
                {
                    dispatchOrderClientData.EndingReport = EndingReportConversion.ToClient(dispatchOrder.EndingReport, app);
                }
            }
            else if (app.Equals(UserApplicationData.Map)) 
            {
                dispatchOrderClientData.IncidentNotificationCode = dispatchOrder.IncidentNotification.Code;
                dispatchOrderClientData.IncidentCode = dispatchOrder.IncidentNotification.ReportBase.Incident.Code;

                
            }
            else if (app.Equals(UserApplicationData.Supervision))
            {
                dispatchOrderClientData.Code = dispatchOrder.Code;
                dispatchOrderClientData.Version = dispatchOrder.Version;
                dispatchOrderClientData.CustomCode = dispatchOrder.CustomCode;
                dispatchOrderClientData.Status = DispatchOrderStatusConversion.ToClient(dispatchOrder.Status, app);
                dispatchOrderClientData.IncidentNotificationCode = dispatchOrder.IncidentNotification.Code;
                dispatchOrderClientData.IncidentNotificationCustomCode = dispatchOrder.IncidentNotification.CustomCode;
                dispatchOrderClientData.IncidentNotificationVersion = dispatchOrder.IncidentNotification.Version;

                
            }
            return dispatchOrderClientData;
        }

        public static DispatchOrderData ToObject(DispatchOrderClientData dispatchOrder)
        {
            DispatchOrderData dispatchOrderData = new DispatchOrderData();
            dispatchOrderData.Code = dispatchOrder.Code;
            dispatchOrderData.Version = dispatchOrder.Version;
            dispatchOrderData.CustomCode = dispatchOrder.CustomCode;
            dispatchOrderData.Status = DispatchOrderStatusConversion.ToObject(dispatchOrder.Status);
            dispatchOrderData.StartDate = dispatchOrder.StartDate;
            dispatchOrderData.ArrivalDate = dispatchOrder.ArrivalDate;
            dispatchOrderData.ExpectedTime = dispatchOrder.ExpectedTime;

            IncidentNotificationData incidentNotificationData = new IncidentNotificationData();
            incidentNotificationData.Code = dispatchOrder.IncidentNotificationCode;
            dispatchOrderData.IncidentNotification = SmartCadDatabase.RefreshObject(incidentNotificationData) as IncidentNotificationData;
            dispatchOrderData.IncidentNotification.Code = dispatchOrder.IncidentNotificationCode;
            dispatchOrderData.IncidentNotification.CustomCode = dispatchOrder.IncidentNotificationCustomCode;
            //dispatchOrderData.IncidentNotification.Version = dispatchOrder.IncidentNotificationVersion;  // No descomentar... da problemas en despacho. AA 21/01/2010

            UnitData unitData = new UnitData();
            unitData.Code = dispatchOrder.Unit.Code;
            unitData = SmartCadDatabase.RefreshObject(unitData) as UnitData;        
            unitData.Status = UnitStatusConversion.ToObject(dispatchOrder.Unit.Status);
            dispatchOrderData.Unit = unitData;
            if (dispatchOrder.Status.Name == DispatchOrderStatusData.Closed.Name)
            {
                dispatchOrderData.EndingReport = EndingReportConversion.ToObject(dispatchOrder.EndingReport);
            }
            else if (dispatchOrderData.Status.Equals(DispatchOrderStatusData.Cancelled) == true)
            {
                dispatchOrderData.Notes = new ArrayList();
                dispatchOrderData.Notes.Add(GetNote(dispatchOrderData, dispatchOrder.NoteDetail,
                    DispatchOrderNoteTypeData.Cancelled));
            }
            else if (dispatchOrderData.Code != 0 &&
                dispatchOrderData.Status.Equals(DispatchOrderStatusData.Dispatched) == true)
            {
                dispatchOrderData.Notes = new ArrayList();
                dispatchOrderData.Notes.Add(GetNote(dispatchOrderData, dispatchOrder.NoteDetail,
                    DispatchOrderNoteTypeData.TimeChanged));
            }
            else if (dispatchOrderData.Status.Equals(DispatchOrderStatusData.Delayed) == true)
            {
                dispatchOrderData.Notes = new ArrayList();
                dispatchOrderData.Notes.Add(GetNote(dispatchOrderData, dispatchOrder.NoteDetail,
                    DispatchOrderNoteTypeData.TimeChanged));
            }
            return dispatchOrderData;
        }

        private static DispatchOrderNoteData GetNote(DispatchOrderData dispatchOrderData,
            string detail, DispatchOrderNoteTypeData dispatchOrderNoteTypeData)
        {
            DispatchOrderNoteData note = new DispatchOrderNoteData();
            note.CompleteUserName = dispatchOrderData.IncidentNotification.DispatchOperator.FirstName
                + " " + dispatchOrderData.IncidentNotification.DispatchOperator.LastName;
            note.CreationDate = SmartCadDatabase.GetTimeFromBD();
            note.Detail = detail;
            note.DispatchOrder = dispatchOrderData;
            note.Type = dispatchOrderNoteTypeData;
            note.UserLogin = dispatchOrderData.IncidentNotification.DispatchOperator.Login;
            return note;
        }
    }
}
