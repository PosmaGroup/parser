using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;
using System.Collections;
using Iesi.Collections;

namespace SmartCadCore.Core
{
    public class IndicatorGroupForecastConversion
    {
        public static IndicatorGroupForecastClientData ToClient(IndicatorGroupForecastData data, UserApplicationData app)
        {
            IndicatorGroupForecastClientData client = new IndicatorGroupForecastClientData();
            client.Code = data.Code;
            client.Description = data.Description;
            client.End = data.End;
            client.FactoryData = data.FactoryData;
            //if (!SmartCadDatabase.IsInitialize(data.ForecastValues))
            //{
            //    SmartCadDatabase.InitializeLazy(data, data.ForecastValues);
            //}
            //else 
            //{
                string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetIndicatorGroupForecastById, client.Code);
                data.ForecastValues = new ArrayList();
                data.ForecastValues = SmartCadDatabase.SearchObjects(hql);
            //}
            if (SmartCadDatabase.IsInitialize(data.ForecastValues) == true)
            {
                client.ForecastValues = new ArrayList();
                foreach (IndicatorForecastData forecast in data.ForecastValues)
                {
                    client.ForecastValues.Add(IndicatorForecastConversion.ToClient(forecast, app));
                }
            }
            client.General = data.General;
            client.Name = data.Name;
            client.Start = data.Start;
            client.Version = data.Version;
            return client;
        }

        public static IndicatorGroupForecastData ToObject(IndicatorGroupForecastClientData data, UserApplicationData app)
        {
            IndicatorGroupForecastData result = new IndicatorGroupForecastData();
            result.Code = data.Code;
            if (result.Code != 0)
            {
                result = (IndicatorGroupForecastData)SmartCadDatabase.RefreshObject(result);
                //if (result.General == false)
                //    SmartCadDatabase.InitializeLazy(result, result.ForecastValues);
            }

            //if (result.Code == 0 || SmartCadDatabase.IsInitialize(result.ForecastValues) == true)
            if (result.Code == 0 || result.General == false)
            {
                ArrayList values = new ArrayList();
                foreach (IndicatorForecastClientData value in data.ForecastValues)
                {
                    IndicatorForecastData forecast = IndicatorForecastConversion.ToObject(value, app);
                    forecast.IndicatorGroupForecast = result;
                    //if (value.Code != 0)
                    //{
                    //    int index = result.ForecastValues.IndexOf(forecast);
                    //    if (index != -1)
                    //        result.ForecastValues[index] = forecast;
                    //}
                    //else
                    //{
                        values.Add(forecast);
                    //}

                }
                //if (result.Code == 0)
                    result.ForecastValues = values;
            }

            result.Description = data.Description;
            result.End = data.End;
            result.General = data.General;
            result.Name = data.Name;
            result.Start = data.Start;
            result.FactoryData = data.FactoryData;
            if (data.Code != 0)
                result.Version = data.Version;
            return result;
        }
    }
}
