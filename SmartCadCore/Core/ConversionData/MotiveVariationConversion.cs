using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;

namespace SmartCadCore.Core
{
    public class MotiveVariationConversion
    {
        public static MotiveVariationClientData ToClient(MotiveVariationData motiveData, UserApplicationData app)
        {
            MotiveVariationClientData motivenClientData = new MotiveVariationClientData();
            motivenClientData.Code = motiveData.Code;
            motivenClientData.Version = motiveData.Version;
            motivenClientData.Name = motiveData.Name;
            motivenClientData.Type = motiveData.Type;

            return motivenClientData;
        }

        public static MotiveVariationData ToObject(MotiveVariationClientData motiveClientData, UserApplicationData app)
        {
            MotiveVariationData motiveData = new MotiveVariationData();
            motiveData.Code = motiveClientData.Code;
            SmartCadDatabase.RefreshObject(motiveData);

            return motiveData;
        }
    }
}
