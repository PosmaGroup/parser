using System;
using System.Collections;
using System.Text;

using SmartCadCore.ClientData;
using SmartCadCore.Model;

namespace SmartCadCore.Core
{
    public class DepartmentTypeConversion
    {
        public static DepartmentTypeClientData ToClient(DepartmentTypeData departmentTypeData, UserApplicationData app)
        {
            DepartmentTypeClientData departmentTypeClientData = new DepartmentTypeClientData();
            departmentTypeClientData.Code = departmentTypeData.Code;
            departmentTypeClientData.Version = departmentTypeData.Version;
            departmentTypeClientData.Name = departmentTypeData.Name;
            departmentTypeClientData.Color = departmentTypeData.Color;
            departmentTypeClientData.Dispatch = departmentTypeData.Dispatch.Value;
            departmentTypeClientData.Pattern = departmentTypeData.Pattern;
            if (departmentTypeData.RestartFrequency != null)
            departmentTypeClientData.RestartFrequency = (DepartmentTypeClientData.Frequency)(int)departmentTypeData.RestartFrequency;
            departmentTypeClientData.DepartmentZones = new ArrayList();
            if (app.Name == UserApplicationData.Administration.Name || app.Name == UserApplicationData.Map.Name)
            {
                if (departmentTypeData.InitializeCollections == true)
                {
                    if (SmartCadDatabase.IsInitialize(departmentTypeData.DepartmentZones) == false)
                        SmartCadDatabase.InitializeLazy(departmentTypeData, departmentTypeData.DepartmentZones);
                    foreach (DepartmentZoneData zone in departmentTypeData.DepartmentZones)
                    {
                        DepartmentZoneClientData dzcd = DepartmentZoneConversion.ToClient(zone, app);
                        dzcd.DepartmentType = departmentTypeClientData;
                        departmentTypeClientData.DepartmentZones.Add(dzcd);
                    }
                }
            }
            else
            {
                if (SmartCadDatabase.IsInitialize(departmentTypeData.DepartmentZones) == true)
                {
                    //SmartCadDatabase.InitializeLazy(departmentTypeData, departmentTypeData.DepartmentZones);
                    foreach (DepartmentZoneData zone in departmentTypeData.DepartmentZones)
                    {
                        departmentTypeClientData.DepartmentZones.Add(DepartmentZoneConversion.ToClient(zone, app));
                    }
                }
            }

            if (app.Name == UserApplicationData.Administration.Name)
            {
                if (SmartCadDatabase.IsInitialize(departmentTypeData.Alerts) == false)
                    SmartCadDatabase.InitializeLazy(departmentTypeData, departmentTypeData.Alerts);
                if (SmartCadDatabase.IsInitialize(departmentTypeData.Alerts) == true)
                {
                    departmentTypeClientData.Alerts = new ArrayList();
                    foreach (DepartmentTypeAlertData depAlert in departmentTypeData.Alerts)
                    {                      
                        departmentTypeClientData.Alerts.Add(DepartmentTypeAlertConversion.ToClient(depAlert, app));
                    }                      
                }
            }

            return departmentTypeClientData;
        }

        public static DepartmentTypeData ToObject(DepartmentTypeClientData departmentTypeClientData, UserApplicationData app)
        {
            DepartmentTypeData departmentType = new DepartmentTypeData();
            departmentType.Code = departmentTypeClientData.Code;
            departmentType.Version = departmentTypeClientData.Version;
            departmentType.Name = departmentTypeClientData.Name;
            departmentType.Color = departmentTypeClientData.Color;
            departmentType.Dispatch = departmentTypeClientData.Dispatch;
            departmentType.Pattern = departmentTypeClientData.Pattern;
            DepartmentTypeData dep = ((DepartmentTypeData)SmartCadDatabase.SearchObjectData(SmartCadHqls.GetCustomHql(SmartCadHqls.GetDepartmentTypeCounter, departmentTypeClientData.Code)));
            if (dep != null)
                departmentType.Counter = dep.Counter;
            else
                departmentType.Counter = null;
            if (departmentTypeClientData.RestartFrequency != null)
            departmentType.RestartFrequency = (DepartmentTypeData.Frequency)(int)departmentTypeClientData.RestartFrequency;
         
            if (departmentTypeClientData.Alerts != null)
            {
                departmentType.Alerts = new ArrayList();
                foreach (DepartmentTypeAlertClientData depAlertClient in departmentTypeClientData.Alerts)
                {
                    DepartmentTypeAlertData depAlertData = DepartmentTypeAlertConversion.ToObject(depAlertClient, app);
                    depAlertData.DepartmentType = departmentType;
                    departmentType.Alerts.Add(depAlertData);
                }
            }

            return departmentType;
        }
    }
}

