﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using SmartCadCore.Model;
using SmartCadCore.ClientData;

namespace SmartCadCore.Core
{
    public class DepartmentZoneAddressConversion
    {
        public static DepartmentZoneAddressClientData ToClient(DepartmentZoneAddressData depData, UserApplicationData app)
        {
            DepartmentZoneAddressClientData depCLientData = new DepartmentZoneAddressClientData();
            depCLientData.Lon = depData.Address.Lon;
            depCLientData.Lat = depData.Address.Lat;
            depCLientData.Code = depData.Code;
            depCLientData.Zone = depData.Zone.CustomCode;
            depCLientData.PointNumber = depData.PointNumber;
            depCLientData.Version = depData.Version;
            depCLientData.DepartamentTypeCode = depData.Zone.DepartmentType.Code;
            depCLientData.DepartamentZoneCode = depData.Zone.Code;
            depCLientData.Name = depData.Zone.Name;
            return depCLientData;

        }

        public static DepartmentZoneAddressData ToObject(DepartmentZoneAddressClientData depClientData, UserApplicationData app)
        {
            DepartmentZoneAddressData depData = new DepartmentZoneAddressData();
            //depData.Code = depClientData.Code;
            //depData.Version = depClientData.Version;

            depData.Address = new MultipleAddressData();
            depData.Address.Lat = depClientData.Lat;
            depData.Address.Lon = depClientData.Lon;
            //depData.Zone.CustomCode = depClientData.Zone;

            //depData.Zone = SmartCadDatabase.SearchObject<DepartmentZoneData>(
            //   SmartCadHqls.GetCustomHql(SmartCadHqls.GetDepartmentZoneByCustomCode, depClientData.Zone));

            depData.CustomCode = depClientData.Lon.ToString() + depClientData.Lat.ToString();
            depData.PointNumber = depClientData.PointNumber;
            //SmartCadDatabase.RefreshObject(depData);
            return depData;
        }
    }
}