using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;
using System.Collections;

namespace SmartCadCore.Core
{
    public class WorkShiftVariationConversion
    {
		public static WorkShiftVariationClientData ToClient(WorkShiftVariationData variationWorkShiftData, UserApplicationData app)
		{
           
			WorkShiftVariationClientData variationWorkShiftClientData = new WorkShiftVariationClientData();
			variationWorkShiftClientData.Code = variationWorkShiftData.Code;
			variationWorkShiftClientData.Version = variationWorkShiftData.Version;
			variationWorkShiftClientData.Name = variationWorkShiftData.Name;
			if (variationWorkShiftData.Motive != null)
			{
				variationWorkShiftClientData.MotiveName = variationWorkShiftData.Motive.Name;
				variationWorkShiftClientData.MotiveCode = variationWorkShiftData.Motive.Code;
			}
			variationWorkShiftClientData.Type = (WorkShiftVariationClientData.WorkShiftType)((int)variationWorkShiftData.Type);
			variationWorkShiftClientData.ObjectType = (WorkShiftVariationClientData.ObjectRelatedType)((int)variationWorkShiftData.ObjectType);
			variationWorkShiftClientData.Description = variationWorkShiftData.Description;
            

			if (variationWorkShiftData.InitializeCollections == true && SmartCadDatabase.IsInitialize(variationWorkShiftData.Schedules)== false)
				SmartCadDatabase.InitializeLazy(variationWorkShiftData, variationWorkShiftData.Schedules);

            if (SmartCadDatabase.IsInitialize(variationWorkShiftData.Operators) == true)
            {
                variationWorkShiftClientData.Operators = new ArrayList();
                if (variationWorkShiftData.Operators != null)
                    foreach (WorkShiftOperatorData oper in variationWorkShiftData.Operators)
                    {
                        oper.Operator.WorkShifts = null;
                        OperatorClientData operClient = GetOperator(oper.Operator);
                        variationWorkShiftClientData.Operators.Add(operClient);
                    }
            }
			if (SmartCadDatabase.IsInitialize(variationWorkShiftData.Schedules) == true)
			{
				variationWorkShiftClientData.Schedules = new ArrayList();
				foreach (WorkShiftScheduleVariationData schedule in variationWorkShiftData.Schedules)
					variationWorkShiftClientData.Schedules.Add(WorkShiftScheduleVariationConversion.ToClient(schedule, app));
			}
            
			return variationWorkShiftClientData;
		}

        private static OperatorClientData GetOperator(OperatorData operatorData)
        {
            OperatorClientData operatorClient = new OperatorClientData();

            operatorClient.Code = operatorData.Code;
            operatorClient.Version = operatorData.Version;
            operatorClient.FirstName = operatorData.FirstName;
            operatorClient.LastName = operatorData.LastName;
            operatorClient.Login = operatorData.Login;
            operatorClient.AgentId = operatorData.AgentID;
            operatorClient.AgentPassword = operatorData.AgentPassword;
            operatorClient.RoleName = operatorData.Role.Name;
            operatorClient.RoleFriendlyName = operatorData.Role.FriendlyName;
            operatorClient.PersonID = operatorData.PersonId;
            operatorClient.IsSupervisor = ServiceUtil.CheckSupervisor(operatorData.Role.Code);
            operatorClient.DepartmentTypes = new ArrayList();
            
			if (SmartCadDatabase.IsInitialize(operatorData.DepartmentTypes) == false)
                SmartCadDatabase.InitializeLazy(operatorData, operatorData.DepartmentTypes);

            if (SmartCadDatabase.IsInitialize(operatorData.DepartmentTypes) == true)
                foreach (DepartmentTypeData dep in operatorData.DepartmentTypes)
                    operatorClient.DepartmentTypes.Add(DepartmentTypeConversion.ToClient(dep, UserApplicationData.Server));

            operatorClient.RoleCode = operatorData.Role.Code;
                    
            if (operatorData.Operators != null && SmartCadDatabase.IsInitialize(operatorData.Operators) == true)
            {
                operatorClient.Operators = new ArrayList();
                foreach (OperatorAssignData assign in operatorData.Operators)
                    operatorClient.Operators.Add(OperatorAssignConversion.ToClient(assign, UserApplicationData.Supervision));
            }


            operatorClient.SupervisorCodes = new ArrayList();
            if (operatorData.Supervisors != null && SmartCadDatabase.IsInitialize(operatorData.Supervisors) == true)
            {
                operatorClient.Supervisors = new ArrayList();
                foreach (OperatorAssignData assign in operatorData.Supervisors)
                {
                    operatorClient.Supervisors.Add(OperatorAssignConversion.ToClient(assign, UserApplicationData.Supervision));
                    if (operatorClient.SupervisorCodes.Contains(assign.Supervisor.Code) == false)
                        operatorClient.SupervisorCodes.Add(assign.Supervisor.Code);
                }
            }

			if (operatorData.WorkShifts != null && SmartCadDatabase.IsInitialize(operatorData.WorkShifts) == true)
            {
				operatorClient.WorkShifts = new ArrayList();
                foreach (WorkShiftOperatorData ws in operatorData.WorkShifts)
                    operatorClient.WorkShifts.Add(WorkShiftVariationConversion.ToClient(ws.WorkShift, UserApplicationData.Supervision));
            }
            
			if (SmartCadDatabase.IsInitialize(operatorData.CategoryHistoryList) == false)
                SmartCadDatabase.InitializeLazy(operatorData, operatorData.CategoryHistoryList);

            if (operatorData.CategoryHistoryList != null && SmartCadDatabase.IsInitialize(operatorData.CategoryHistoryList) == true)
            {
                operatorClient.CategoryList = new ArrayList();
                foreach (OperatorCategoryHistoryData cat in operatorData.CategoryHistoryList)
                    if (cat.EndDate == null)
                    {
                        operatorClient.OperatorCategory = cat.Category.FriendlyName;
                        operatorClient.OperatorCategoryCode = cat.Category.Code;
                    }
            }
            operatorClient.LastSessions = new ArrayList();
            return operatorClient;
        }

        public static WorkShiftVariationData ToObject(WorkShiftVariationClientData variationWorkShiftClientData, UserApplicationData app)
        {
            WorkShiftVariationData variationWorkShiftData = new WorkShiftVariationData();
            variationWorkShiftData.Code = variationWorkShiftClientData.Code;
          if (variationWorkShiftData.Code != 0)  
                variationWorkShiftData = (WorkShiftVariationData)SmartCadDatabase.RefreshObject(variationWorkShiftData);

            variationWorkShiftData.Version = variationWorkShiftClientData.Version;
            variationWorkShiftData.Name = variationWorkShiftClientData.Name;

            if (variationWorkShiftClientData.MotiveCode != 0)
            {
                MotiveVariationData motive = new MotiveVariationData();
                motive.Code = variationWorkShiftClientData.MotiveCode;
                SmartCadDatabase.RefreshObject(motive);
                variationWorkShiftData.Motive = motive;
            }
            else
                variationWorkShiftData.Motive = null;

            variationWorkShiftData.Type = (WorkShiftVariationData.WorkShiftType)((int)variationWorkShiftClientData.Type);
			variationWorkShiftData.ObjectType = (WorkShiftVariationData.ObjectRelatedType)((int)variationWorkShiftClientData.ObjectType);
            variationWorkShiftData.Description = variationWorkShiftClientData.Description;

			if (variationWorkShiftClientData.Operators != null)
            {
                variationWorkShiftData.Operators = new ArrayList();
                foreach (OperatorClientData oper in variationWorkShiftClientData.Operators)
                {
                    //NO hay uso para esta linea. 
                    //OperatorData operData = OperatorConversion.ToObject(oper, app);
                    OperatorData operData = new OperatorData() { Code = oper.Code };
                    string hql = "select data from WorkShiftOperatorData data where data.WorkShift.Code = {0} and data.Operator.Code = {1}";
                    WorkShiftOperatorData wso = SmartCadDatabase.SearchObjectData(SmartCadHqls.GetCustomHql(hql, variationWorkShiftData.Code, oper.Code)) as WorkShiftOperatorData;
                    if (wso == null)
                    {
                        operData = OperatorConversion.ToObject(oper, app);
                        wso = new WorkShiftOperatorData() { Operator = operData, WorkShift = variationWorkShiftData };
                    }
                    variationWorkShiftData.Operators.Add(wso);
                }
            }
			if (variationWorkShiftClientData.Schedules != null)
            {
                variationWorkShiftData.Schedules = new ArrayList();
                foreach (WorkShiftScheduleVariationClientData schedule in variationWorkShiftClientData.Schedules)
                {
                    WorkShiftScheduleVariationData wssvData = WorkShiftScheduleVariationConversion.ToObject(schedule, app);
                    wssvData.WorkShiftVariation = variationWorkShiftData;
                    variationWorkShiftData.Schedules.Add(wssvData);
                }
            }
            return variationWorkShiftData;
        }
    }
}
