using System;
using System.Collections;
using System.Text;
using SmartCadCore.Model;
using SmartCadCore.ClientData;


namespace SmartCadCore.Core
{
    public class TrainingCourseScheduleConversion
    {
        public static TrainingCourseScheduleClientData ToClient(TrainingCourseScheduleData trainingCourseSchedule, UserApplicationData application)
        {
            TrainingCourseScheduleClientData trainingCourseScheduleClient = new TrainingCourseScheduleClientData();
            trainingCourseScheduleClient.Code = trainingCourseSchedule.Code;
            trainingCourseScheduleClient.Name = trainingCourseSchedule.Name;
            trainingCourseScheduleClient.Start = trainingCourseSchedule.Start.Value;
            trainingCourseScheduleClient.End = trainingCourseSchedule.End.Value;
            trainingCourseScheduleClient.Trainer = trainingCourseSchedule.Trainer;
            trainingCourseScheduleClient.Version = trainingCourseSchedule.Version;
            trainingCourseScheduleClient.TrainingCourseCode = trainingCourseSchedule.TrainingCourse.Code;
            trainingCourseScheduleClient.TrainingCourseName = trainingCourseSchedule.TrainingCourse.Name;
            trainingCourseScheduleClient.RealStart = trainingCourseSchedule.RealStart;
            trainingCourseScheduleClient.RealEnd = trainingCourseSchedule.RealEnd;


            if (trainingCourseSchedule.Parts != null && SmartCadDatabase.IsInitialize(trainingCourseSchedule.Parts) == true)
            {
                trainingCourseScheduleClient.Parts = new ArrayList();

                foreach (TrainingCourseSchedulePartsData scheduleParts in trainingCourseSchedule.Parts)
                {
                    TrainingCourseSchedulePartsClientData trainingCourseSchedulePartsClient = TrainingCourseSchedulePartsConversion.ToClient(scheduleParts, application);
                    trainingCourseSchedulePartsClient.TrainingCourseSchedule = trainingCourseScheduleClient;
                    trainingCourseScheduleClient.Parts.Add(trainingCourseSchedulePartsClient);
                }
            }

         

            if (trainingCourseSchedule.Operators != null && SmartCadDatabase.IsInitialize(trainingCourseSchedule.Operators) == true)
            {
                trainingCourseScheduleClient.TrainingCourseOperators = new ArrayList();
                foreach (OperatorTrainingCourseData operatorCourse in trainingCourseSchedule.Operators)
                {
                    OperatorTrainingCourseClientData operatorCourseClient = OperatorTrainingCourseConversion.ToClient(operatorCourse, application);

                    trainingCourseScheduleClient.TrainingCourseOperators.Add(operatorCourseClient);
                }
            }
            return trainingCourseScheduleClient;
        }

        public static TrainingCourseScheduleData ToObject(TrainingCourseScheduleClientData trainingCourseScheduleClient, UserApplicationData application)
        {
            TrainingCourseScheduleData trainingCourseSchedule = new TrainingCourseScheduleData();
            trainingCourseSchedule.Code = trainingCourseScheduleClient.Code;
            trainingCourseSchedule.Name = trainingCourseScheduleClient.Name;
            trainingCourseSchedule.Start = trainingCourseScheduleClient.Start;
            trainingCourseSchedule.End = trainingCourseScheduleClient.End;
            trainingCourseSchedule.RealStart = trainingCourseScheduleClient.RealStart;
            trainingCourseSchedule.RealEnd = trainingCourseScheduleClient.RealEnd;
            if (trainingCourseScheduleClient.Trainer != null)
                 trainingCourseSchedule.Trainer = trainingCourseScheduleClient.Trainer;
            else
                    trainingCourseSchedule.Trainer = string.Empty;            
            trainingCourseSchedule.Version = trainingCourseScheduleClient.Version;
           
            TrainingCourseData course = new TrainingCourseData();
            course.Code = trainingCourseScheduleClient.TrainingCourseCode;
            course = (TrainingCourseData)SmartCadDatabase.RefreshObject(course);
            if (SmartCadDatabase.IsInitialize(course.Schedules) == false)
                SmartCadDatabase.InitializeLazy(course, course.Schedules);

            trainingCourseSchedule.TrainingCourse = course;
 
            trainingCourseSchedule.Parts = new ArrayList();

            foreach (TrainingCourseSchedulePartsClientData scheduleParts in trainingCourseScheduleClient.Parts)
            {
                TrainingCourseSchedulePartsData trainingCourseScheduleParts = TrainingCourseSchedulePartsConversion.ToObject(scheduleParts, application);
                trainingCourseScheduleParts.TrainingCourseSchedule = trainingCourseSchedule;
                trainingCourseSchedule.Parts.Add(trainingCourseScheduleParts);
            }

            if (trainingCourseScheduleClient.TrainingCourseOperators != null)
            {
                trainingCourseSchedule.Operators = new ArrayList();
                foreach (OperatorTrainingCourseClientData oper in trainingCourseScheduleClient.TrainingCourseOperators)
                {
                    OperatorTrainingCourseData operData = OperatorTrainingCourseConversion.ToObject(oper, application);
                    operData.TrainingCourseSchedule = trainingCourseSchedule;
                    trainingCourseSchedule.Operators.Add(operData);
                }
            }
            return trainingCourseSchedule;
        
        }

    }
}