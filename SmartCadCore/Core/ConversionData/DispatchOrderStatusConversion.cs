using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.ClientData;
using SmartCadCore.Model;
using System.Drawing;

namespace SmartCadCore.Core
{
    public class DispatchOrderStatusConversion
    {
        public static DispatchOrderStatusClientData ToClient(DispatchOrderStatusData dispatchOrderStatusData, UserApplicationData app)
        {
            DispatchOrderStatusClientData dispatchOrderStatusClientData = new DispatchOrderStatusClientData();
            if (app.Name == UserApplicationData.Dispatch.Name)
            {
                dispatchOrderStatusClientData.Code = dispatchOrderStatusData.Code;
                dispatchOrderStatusClientData.Version = dispatchOrderStatusData.Version;
                dispatchOrderStatusClientData.Color = dispatchOrderStatusData.Color;
                dispatchOrderStatusClientData.CustomCode = dispatchOrderStatusData.CustomCode;
                dispatchOrderStatusClientData.FriendlyName = dispatchOrderStatusData.FriendlyName;
                dispatchOrderStatusClientData.Image = dispatchOrderStatusData.Image;
                dispatchOrderStatusClientData.Immutable = dispatchOrderStatusData.Immutable.HasValue == true ? dispatchOrderStatusData.Immutable.Value : true;
                dispatchOrderStatusClientData.Name = dispatchOrderStatusData.Name;
            }
            else if (app.Name == UserApplicationData.Report.Name)
            {
                dispatchOrderStatusClientData.FriendlyName = dispatchOrderStatusData.FriendlyName;
            }

            return dispatchOrderStatusClientData;
        }

        public static DispatchOrderStatusData ToObject(DispatchOrderStatusClientData dispatchOrderStatusClientData)
        {
            DispatchOrderStatusData dispatchOrderStatusData = new DispatchOrderStatusData();
            dispatchOrderStatusData.Code = dispatchOrderStatusClientData.Code;
            dispatchOrderStatusData.CustomCode = dispatchOrderStatusClientData.CustomCode;
            dispatchOrderStatusData = SmartCadDatabase.SearchObject<DispatchOrderStatusData>(dispatchOrderStatusData);
            return dispatchOrderStatusData;
        }
    }
}
