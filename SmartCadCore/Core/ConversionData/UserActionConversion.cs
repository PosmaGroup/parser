﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;

namespace SmartCadCore.Core
{
    public class UserActionConversion
    {
        public static UserActionClientData ToClient(UserActionData action, UserApplicationData app)
        {
            UserActionClientData client = new UserActionClientData();
            client.Code = action.Code;
            client.Version = action.Version;
            client.Name = action.Name;
            client.FriendlyName = action.FriendlyName;
            return client;
        }

        public static UserActionData ToObject(UserActionClientData objData)
        {
            UserActionData data = new UserActionData();
            data.Code = objData.Code;
            data.Name = objData.Name;
            data = SmartCadDatabase.SearchObject<UserActionData>(data);
            return data;
        }
    }
}
