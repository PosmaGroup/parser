using System;
using System.Collections;
using System.Text;

using SmartCadCore.ClientData;
using SmartCadCore.Model;

namespace SmartCadCore.Core
{
    class UnitTypeConversion
    {
        internal static UnitTypeClientData ToClient(UnitTypeData unitTypeData, UserApplicationData application)
        {
            UnitTypeClientData unitTypeClientData = new UnitTypeClientData();
            unitTypeClientData.Code = unitTypeData.Code;
            unitTypeClientData.Name = unitTypeData.Name;
            unitTypeClientData.Version = unitTypeData.Version;

            unitTypeClientData.DepartmentType = new DepartmentTypeClientData();
            unitTypeClientData.DepartmentType.Code = unitTypeData.DepartmentType.Code;
            unitTypeClientData.DepartmentType.Version = unitTypeData.DepartmentType.Version;
            unitTypeClientData.DepartmentType.Name = unitTypeData.DepartmentType.Name;
            unitTypeClientData.DepartmentType.Dispatch = unitTypeData.DepartmentType.Dispatch.Value;

            if (application.Name == UserApplicationData.Dispatch.Name ||
                application.Name == UserApplicationData.FirstLevel.Name ||
                application.Name == UserApplicationData.Map.Name)
            {
                #region Dispatch || FisrtLevel || Map
                unitTypeClientData.Description = unitTypeData.Description;
                unitTypeClientData.Color = unitTypeData.Color;
                unitTypeClientData.Image = unitTypeData.Image;
                unitTypeClientData.Capacity = unitTypeData.Capacity.HasValue ? unitTypeData.Capacity.Value : 0;
                unitTypeClientData.Velocity = unitTypeData.Velocity.HasValue ? unitTypeData.Velocity.Value : 0;

                string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetUnitCodesByType, unitTypeData.Code);
                ArrayList list = (ArrayList)SmartCadDatabase.SearchBasicObjects(hql);
                
                if (list.Count > 0)
                {
                    unitTypeClientData.Units = list;
                }
                unitTypeClientData.IconName = unitTypeData.Image;
                #endregion
            }
            else if (application.Name == UserApplicationData.Administration.Name)
            {
                #region Administration
                unitTypeClientData.Description = unitTypeData.Description;
                unitTypeClientData.Color = unitTypeData.Color;
                unitTypeClientData.Image = unitTypeData.Image;
                unitTypeClientData.Capacity = unitTypeData.Capacity.HasValue ? unitTypeData.Capacity.Value : 0;
                unitTypeClientData.Velocity = unitTypeData.Velocity.HasValue ? unitTypeData.Velocity.Value : 0;

                if (unitTypeData.InitializeCollections == true)
                {

                    unitTypeClientData.IncidentTypes = new ArrayList();
                    if (SmartCadDatabase.IsInitialize(unitTypeData.IncidentTypes) == false)
                        SmartCadDatabase.InitializeLazy(unitTypeData, unitTypeData.IncidentTypes);

                    if (SmartCadDatabase.IsInitialize(unitTypeData.IncidentTypes) == true)
                    {
                        foreach (IncidentTypeData it in unitTypeData.IncidentTypes)
                        {
                            IncidentTypeClientData type = new IncidentTypeClientData();
                            type.Code = it.Code;
                            type.Version = it.Version;
                            type.Name = it.Name;
                            type.FriendlyName = it.FriendlyName;
                            type.CustomCode = it.CustomCode;
                            unitTypeClientData.IncidentTypes.Add(type);
                        }
                    }
                }
                #endregion
            }
            return unitTypeClientData;
        }

        internal static UnitTypeData ToObject(UnitTypeClientData unitTypeClientData, UserApplicationData application)
        {
            try
            {
                UnitTypeData unitTypeData = new UnitTypeData();
                unitTypeData.Code = unitTypeClientData.Code;
                if(unitTypeData.Code != 0)
                    unitTypeData = (UnitTypeData)SmartCadDatabase.RefreshObject(unitTypeData);

                unitTypeData.Version = unitTypeClientData.Version;
                unitTypeData.Image = unitTypeClientData.Image;
                unitTypeData.Name = unitTypeClientData.Name;
                unitTypeData.Capacity = unitTypeClientData.Capacity;
                unitTypeData.Velocity = unitTypeClientData.Velocity;
                unitTypeData.Description = unitTypeClientData.Description;
                unitTypeData.ComponentColor = new ComponentColorData(unitTypeClientData.Color);
                DepartmentTypeData dept = new DepartmentTypeData();
                dept.Code = unitTypeClientData.DepartmentType.Code;
                unitTypeData.DepartmentType = SmartCadDatabase.RefreshObject(dept) as DepartmentTypeData;
                SmartCadDatabase.InitializeLazy(unitTypeData, unitTypeData.IncidentTypes);
                ArrayList incidentTypeListOld = new ArrayList();
                if (unitTypeData.IncidentTypes != null && unitTypeData.IncidentTypes.Count > 0)
                {
                    incidentTypeListOld.AddRange(unitTypeData.IncidentTypes);
                }
                else if (unitTypeData.IncidentTypes == null)
                {
                    unitTypeData.IncidentTypes = new ArrayList();
                }
                ArrayList incidentTypeListNew = new ArrayList();
                foreach (IncidentTypeClientData itClient in unitTypeClientData.IncidentTypes)
                {
                    IncidentTypeData itData = new IncidentTypeData();
                    itData.Code = itClient.Code;
                    itData = (IncidentTypeData)SmartCadDatabase.RefreshObject(itData);
                    incidentTypeListNew.Add(itData);
                    if (unitTypeData.IncidentTypes.Contains(itData) == false)
                    {
                        unitTypeData.IncidentTypes.Add(itData);
                    }
                }

                foreach (IncidentTypeData itData in incidentTypeListOld)
                {
                    if (incidentTypeListNew.Contains(itData) == false)
                    {
                        unitTypeData.IncidentTypes.Remove(itData);
                    }
                }
                return unitTypeData;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
