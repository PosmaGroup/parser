using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.Model;
using SmartCadCore.ClientData;

namespace SmartCadCore.Core
{
    public class PossibleAnswerAnswerConversion
    {
        public static PossibleAnswerAnswerClientData ToClient(PossibleAnswerAnswerData paad, UserApplicationData app)
        {
            PossibleAnswerAnswerClientData convertedClientObject = new PossibleAnswerAnswerClientData();

            if (app.Equals(UserApplicationData.FirstLevel) || app.Equals(UserApplicationClientData.SocialNetworks))
            {
                convertedClientObject.Code = paad.Code;
                convertedClientObject.Version = paad.Version;
                convertedClientObject.ReportAnswerCode = paad.PhoneReportAnswer.Code;
                convertedClientObject.QuestionPossibleAnswer = QuestionPossibleAnswerConversion.ToClient(paad.PossibleAnswer, app);
                convertedClientObject.TextAnswer = paad.TextAnswer;
            }

            return convertedClientObject;
        }

        public static PossibleAnswerAnswerData ToObject(PossibleAnswerAnswerClientData paacd)
        {
            PossibleAnswerAnswerData convertedObject = new PossibleAnswerAnswerData();

            convertedObject.Code = paacd.Code;
            convertedObject.Version = paacd.Version;
            convertedObject.TextAnswer = paacd.TextAnswer;
            QuestionPossibleAnswerData qpad = new QuestionPossibleAnswerData();
            qpad.Code = paacd.QuestionPossibleAnswer.Code;
            convertedObject.PossibleAnswer = qpad;

            return convertedObject;
        }
    }
}
