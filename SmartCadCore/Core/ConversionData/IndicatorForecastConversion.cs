using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;
using SmartCadCore.Common;

namespace SmartCadCore.Core
{
    public class IndicatorForecastConversion
    {
        public static IndicatorForecastClientData ToClient(IndicatorForecastData data, UserApplicationData app)
        {
            IndicatorForecastClientData client = new IndicatorForecastClientData();
            client.Code = data.Code;
            if (data.DepartmentType != null)
                client.DepartmentType = DepartmentTypeConversion.ToClient(data.DepartmentType, app);
            client.High = data.High;
            client.Indicator = IndicatorConversion.ToClient(data.Indicator,app);
            client.Low = data.Low;
            client.Pattern = ResourceLoader.GetString2(data.Pattern.ToString());
            client.PositivePattern = (int)data.Pattern;
            client.Version = data.Version;
            client.IndicatorGroupForecastCode = data.IndicatorGroupForecast.Code;
            return client;
        }

        public static IndicatorForecastData ToObject(IndicatorForecastClientData data, UserApplicationData app)
        {
            IndicatorForecastData result = new IndicatorForecastData();
            result.Code = data.Code;
            if (result.Code != 0)
            {
                result = (IndicatorForecastData)SmartCadDatabase.RefreshObject(result);
            }
            if (data.DepartmentType != null)
            {
                DepartmentTypeData dtd = new DepartmentTypeData();
                dtd.Code = data.DepartmentType.Code;
                dtd.Name = data.DepartmentType.Name;
                dtd.Version = data.DepartmentType.Version;
                result.DepartmentType = dtd;
            }
            result.High = data.High;
            if (data.Indicator != null)
            {
                IndicatorData id = new IndicatorData();
                id.Code = data.Indicator.Code;
                id = (IndicatorData)SmartCadDatabase.RefreshObject(id);
                result.Indicator = id;
            }
            result.Low = data.Low;
            result.Pattern = (PositivePattern)data.PositivePattern;
            result.Version = data.Version;
            return result;
        }
    }
}
