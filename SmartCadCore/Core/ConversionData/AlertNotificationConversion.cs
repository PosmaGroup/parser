﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;
using System.Collections;
using Iesi.Collections;

namespace SmartCadCore.Core
{
	public static class AlertNotificationConversion
	{
		public static AlertNotificationClientData ToClient(AlertNotificationData data, UserApplicationData app)
		{
			AlertNotificationClientData ancd = new AlertNotificationClientData();
			ancd.AttendingOperator = data.AttendingOperator==null?null:OperatorConversion.ToClient(data.AttendingOperator, app);
			ancd.UnitAlert = data.UnitAlert == null ? null : UnitAlertConversion.ToClient(data.UnitAlert, app);
			ancd.Date = data.Date;
			ancd.Code = data.Code;
			ancd.Version = data.Version;
			ancd.Status = (AlertNotificationClientData.AlertStatus)(int)data.Status;
			ancd.Observations = new ArrayList();
			if (data.Sensor != null)
			{
				ancd.SensorName = data.Sensor.Name;
				ancd.SensorCode = data.Sensor.Code;
			}
			if (data.Observations != null && SmartCadDatabase.IsInitialize(data.Observations))
			{
				foreach (AlertNotificationObservationData obs in data.Observations)
				{
					AlertNotificationObservationClientData obsClient = AlertNotificationObservationConversion.ToClient(obs, app);
					obsClient.AlertNotification = ancd;
					ancd.Observations.Add(obsClient);
				}
			}
			return ancd;
		}

		public static AlertNotificationData ToObject(AlertNotificationClientData clientData, UserApplicationData app)
		{
			AlertNotificationData and = new AlertNotificationData();
			and.Code = clientData.Code;
			and.Version = clientData.Version;
			and.Date = clientData.Date;
			and.AttendingOperator = clientData.AttendingOperator == null ? null : OperatorConversion.ToObject(clientData.AttendingOperator, app);
			and.UnitAlert = clientData.UnitAlert == null ? null : UnitAlertConversion.ToObject(clientData.UnitAlert, app);
			and.Status = (AlertNotificationData.AlertStatus)(int)clientData.Status;
			and.Observations = new HashedSet();
            if (clientData.SensorCode != null && clientData.SensorCode > 0)
            {
                and.Sensor = new SensorData();
                and.Sensor.Code = clientData.SensorCode;
            }
			foreach (AlertNotificationObservationClientData obs in clientData.Observations)
			{
				AlertNotificationObservationData obsData = AlertNotificationObservationConversion.ToObject(obs, app);
				obsData.AlertNotification = and;
				and.Observations.Add(obsData);
			}			
			return and;
		}
	}
}
