using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;

using SmartCadCore.ClientData;
using SmartCadCore.Model;
using NHibernate.Collection;
using Iesi.Collections;

namespace SmartCadCore.Core
{
    public class AlarmReportAnswerConversion
    {
        //TODO: Borrar
        public static ReportAnswerClientData ToClient(AlarmReportAnswerData prad, UserApplicationData app)
        {
            ReportAnswerClientData convertedClientObject = new ReportAnswerClientData();

            if (app.Equals(UserApplicationData.FirstLevel))
            {
                convertedClientObject.Code = prad.Code;
                convertedClientObject.Version = prad.Version;
                convertedClientObject.ReportCode = prad.AlarmReport.Code;
                convertedClientObject.QuestionCode = prad.Question.Code;
                convertedClientObject.Answers = new ArrayList();

                //TODO: Chequear
                if (SmartCadDatabase.IsInitialize(prad.SetAnswers) == false)
                {
                    SmartCadDatabase.InitializeLazy(prad, prad.SetAnswers);
                }

                foreach (PossibleAnswerAnswerData paad in prad.SetAnswers)
                {
                    convertedClientObject.Answers.Add(PossibleAnswerAnswerConversion.ToClient(paad, app));
                }
            }

            return convertedClientObject;
        }

        public static AlarmReportAnswerData ToObject(ReportAnswerClientData pracd)
        {
            AlarmReportAnswerData convertedObject = new AlarmReportAnswerData();

            convertedObject.Code = pracd.Code;
            convertedObject.Version = pracd.Version;
            convertedObject.SetAnswers = new HashedSet();
            foreach (PossibleAnswerAnswerClientData paacd in pracd.Answers)
            {
                PossibleAnswerAnswerData paad = PossibleAnswerAnswerConversion.ToObject(paacd);
                paad.AlarmReportAnswer = convertedObject;
                convertedObject.SetAnswers.Add(paad);
            }
            QuestionData question = new QuestionData();
            question.Code = pracd.QuestionCode;
            question.Text = pracd.QuestionText;
       
            StringBuilder queryStr = new StringBuilder(SmartCadHqls.GetCustomHql(
               SmartCadHqls.GetQuestionShorTextHql, question.Code));

            object objectList = SmartCadDatabase.SearchBasicObjects(
                queryStr.ToString(), true);
            ArrayList list = (ArrayList)objectList;
            question.ShortText = (string)list[0];
            convertedObject.Question = question;

            return convertedObject;
        }
    }
}
