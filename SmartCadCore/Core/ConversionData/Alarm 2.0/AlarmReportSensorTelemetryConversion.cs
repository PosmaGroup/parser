using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.Model;
using SmartCadCore.ClientData;

namespace SmartCadCore.Core
{
    public class AlarmReportSensorTelemetryConversion
    {
        public static AlarmReportSensorTelemetryClientData ToClient(AlarmReportSensorTelemetryData alarmReportSensorTelemetryData, UserApplicationData userApplication)
        {
            AlarmReportSensorTelemetryClientData toConvert = new AlarmReportSensorTelemetryClientData(
                alarmReportSensorTelemetryData.Name,
                null);

            toConvert.Address = AddressConversion.ToClient(alarmReportSensorTelemetryData.Address, userApplication);

            return toConvert;
        }

        public static AlarmReportSensorTelemetryData ToObject(AlarmReportSensorTelemetryClientData alarmReportSensorTelemetryClient)
        {
            AlarmReportSensorTelemetryData toConvert = new AlarmReportSensorTelemetryData(
                alarmReportSensorTelemetryClient.Name,
                new StructAddressData(AddressConversion.ToObject(alarmReportSensorTelemetryClient.Address)));
            return toConvert;
        }
    }

}