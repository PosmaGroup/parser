using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

using SmartCadCore.Model;
using SmartCadCore.ClientData;
using NHibernate.Collection;
using Iesi.Collections;

namespace SmartCadCore.Core
{
    public class AlarmLprReportConversion
    {
        public static AlarmLprReportClientData ToClient(AlarmLprReportData alarmLprReportData, UserApplicationData app)
        {
            AlarmLprReportClientData convertedClientObject = new AlarmLprReportClientData();

            convertedClientObject.Code = alarmLprReportData.Code;
            convertedClientObject.Version = alarmLprReportData.Version;
            convertedClientObject.CustomCode = alarmLprReportData.CustomCode;

            if (alarmLprReportData.FinishedIncidentLprTime != null)
                convertedClientObject.FinishedTime = alarmLprReportData.FinishedIncidentLprTime.Value;
            //if (alarmLprReportData.StatrtIncidentLprTime != null)
            //    convertedClientObject.StatrtIncidentLprTime = alarmLprReportData.StatrtIncidentLprTime.Value;
            

            if (alarmLprReportData.Incident != null)
            {
                convertedClientObject.IncidentCode = alarmLprReportData.Incident.Code;
                convertedClientObject.IncidentCustomCode = alarmLprReportData.Incident.CustomCode;
            }


            if (alarmLprReportData.AlarmLpr != null)
            {
                convertedClientObject.Alarm = AlarmLprConversion.ToClient(alarmLprReportData.AlarmLpr,app);
            }

            convertedClientObject.OperatorLogin = (alarmLprReportData.Operator != null) ? alarmLprReportData.Operator.Login : "";

            if (SmartCadDatabase.IsInitialize(alarmLprReportData.SetIncidentTypes) == false)
            {
                convertedClientObject.IncidentTypesCodes = (IList)SmartCadDatabase.SearchBasicObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.IncidentTypeCodesByReportBaseCode, alarmLprReportData.Code));
            }
            else
            {
                convertedClientObject.IncidentTypesCodes = new ArrayList();
                foreach (IncidentTypeData incidentType in alarmLprReportData.SetIncidentTypes)
                {
                    convertedClientObject.IncidentTypesCodes.Add(incidentType.Code);
                }
            }
           
            //convertedClientObject.AlarmLprReportLprClient = AlarmLprReportLprConversion.ToClient(alarmLprReportData.Lpr, app);

            convertedClientObject.ReportBaseDepartmentTypesClient = new ArrayList();
            //}
            return convertedClientObject;
        }

        public static AlarmLprReportData ToObject(AlarmLprReportClientData alarmLprReportClient)
        {
            AlarmLprReportData convertedObject = new AlarmLprReportData();

            convertedObject.SetAnswers = new ListSet();
            if (alarmLprReportClient.Answers != null)
            {
                foreach (ReportAnswerClientData pracd in alarmLprReportClient.Answers)
                {
                    AlarmLprReportAnswerData tempAlarmLprReportAnswer = new AlarmLprReportAnswerData();
                    tempAlarmLprReportAnswer = AlarmLprReportAnswerConversion.ToObject(pracd);
                    tempAlarmLprReportAnswer.AlarmLprReport = convertedObject;
                    bool resutl = convertedObject.SetAnswers.Add(tempAlarmLprReportAnswer);
                }              
            }            
            convertedObject.Code = alarmLprReportClient.Code;
            convertedObject.Version = alarmLprReportClient.Version;
            convertedObject.CustomCode = alarmLprReportClient.CustomCode;
            convertedObject.AlarmLpr = AlarmLprConversion.ToObject(alarmLprReportClient.Alarm,null);
           

            if (alarmLprReportClient.IncidentCode != 0)
            {
                IncidentData incident = new IncidentData();
                incident.Code = alarmLprReportClient.IncidentCode;
                incident = SmartCadDatabase.RefreshObject(incident) as IncidentData;
                convertedObject.Incident = incident;
            }

            convertedObject.SetIncidentTypes = new HashedSet();

            StringBuilder sb = new StringBuilder();
            sb.Append("(");
            for (int i = 0; i < alarmLprReportClient.IncidentTypesCodes.Count; i++)
            {
                int itc = (int)alarmLprReportClient.IncidentTypesCodes[i];
                sb.Append(itc);
                if (i < alarmLprReportClient.IncidentTypesCodes.Count - 1)
                    sb.Append(",");
            }
            sb.Append(")");

            foreach (IncidentTypeData incidentType in SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.FindIncidentTypesInCodeList, sb.ToString())))
            {
                convertedObject.SetIncidentTypes.Add(incidentType);
            }
           
            convertedObject.MultipleOrganisms = alarmLprReportClient.MultipleOrganisms;

            OperatorData operatorData = new OperatorData();
            operatorData.Login = alarmLprReportClient.OperatorLogin;
            operatorData = SmartCadDatabase.SearchObjects(operatorData)[0] as OperatorData;
            convertedObject.Operator = operatorData;

            if(alarmLprReportClient.FinishedTime.Year!= 1)
                convertedObject.FinishedIncidentLprTime = alarmLprReportClient.FinishedTime;            
            convertedObject.ReportBaseDepartmentTypes = new ArrayList();

            foreach (ReportBaseDepartmentTypeClientData rbdtcd in alarmLprReportClient.ReportBaseDepartmentTypesClient)
            {
                ReportBaseDepartmentTypeData tempReportBaseDepartmentType = ReportBaseDepartmentTypeConversion.ToObject(rbdtcd);
                tempReportBaseDepartmentType.ReportBase = convertedObject as ReportBaseData;
                convertedObject.ReportBaseDepartmentTypes.Add(tempReportBaseDepartmentType);
            }

            return convertedObject;
        }
    }

   
}
