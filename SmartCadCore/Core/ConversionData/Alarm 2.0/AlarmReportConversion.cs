using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

using SmartCadCore.Model;
using SmartCadCore.ClientData;
using NHibernate.Collection;
using Iesi.Collections;

namespace SmartCadCore.Core
{
    public class AlarmReportConversion
    {
        public static AlarmReportClientData ToClient(AlarmReportData alarmReportData, UserApplicationData app)
        {
            AlarmReportClientData convertedClientObject = new AlarmReportClientData();

            convertedClientObject.Code = alarmReportData.Code;
            convertedClientObject.Version = alarmReportData.Version;
            convertedClientObject.CustomCode = alarmReportData.CustomCode;

            if (alarmReportData.Incident != null)
            {
                convertedClientObject.IncidentCode = alarmReportData.Incident.Code;
                convertedClientObject.IncidentCustomCode = alarmReportData.Incident.CustomCode;
            }

            convertedClientObject.OperatorLogin = (alarmReportData.Operator != null) ? alarmReportData.Operator.Login : "";

            if (SmartCadDatabase.IsInitialize(alarmReportData.SetIncidentTypes) == false)
            {
                convertedClientObject.IncidentTypesCodes = (IList)SmartCadDatabase.SearchBasicObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.IncidentTypeCodesByReportBaseCode, alarmReportData.Code));
            }
            else
            {
                convertedClientObject.IncidentTypesCodes = new ArrayList();
                foreach (IncidentTypeData incidentType in alarmReportData.SetIncidentTypes)
                {
                    convertedClientObject.IncidentTypesCodes.Add(incidentType.Code);
                }
            }
           
            convertedClientObject.AlarmSensorTelemetryClient = AlarmSensorTelemetryConversion.ToClient(alarmReportData.AlarmSensorTelemetry, app);

            convertedClientObject.ReportBaseDepartmentTypesClient = new ArrayList();
            //}
            return convertedClientObject;
        }

        public static AlarmReportData ToObject(AlarmReportClientData alarmReportClient)
        {
            AlarmReportData convertedObject = new AlarmReportData();

            convertedObject.SetAnswers = new ListSet();
            if (alarmReportClient.Answers != null)
            {
                foreach (ReportAnswerClientData pracd in alarmReportClient.Answers)
                {
                    AlarmReportAnswerData tempAlarmReportAnswer = new AlarmReportAnswerData();
                    tempAlarmReportAnswer = AlarmReportAnswerConversion.ToObject(pracd);
                    tempAlarmReportAnswer.AlarmReport = convertedObject;
                    bool resutl = convertedObject.SetAnswers.Add(tempAlarmReportAnswer);
                }              
            }            
            convertedObject.Code = alarmReportClient.Code;
            convertedObject.Version = alarmReportClient.Version;
            convertedObject.CustomCode = alarmReportClient.CustomCode;

           

            if (alarmReportClient.IncidentCode != 0)
            {
                IncidentData incident = new IncidentData();
                incident.Code = alarmReportClient.IncidentCode;
                incident = SmartCadDatabase.RefreshObject(incident) as IncidentData;
                convertedObject.Incident = incident;
            }

            convertedObject.SetIncidentTypes = new HashedSet();

            StringBuilder sb = new StringBuilder();
            sb.Append("(");
            for (int i = 0; i < alarmReportClient.IncidentTypesCodes.Count; i++)
            {
                int itc = (int)alarmReportClient.IncidentTypesCodes[i];
                sb.Append(itc);
                if (i < alarmReportClient.IncidentTypesCodes.Count - 1)
                    sb.Append(",");
            }
            sb.Append(")");

            foreach (IncidentTypeData incidentType in SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.FindIncidentTypesInCodeList, sb.ToString())))
            {
                convertedObject.SetIncidentTypes.Add(incidentType);
            }
           
            convertedObject.AlarmSensorTelemetry = AlarmSensorTelemetryConversion.ToObject(alarmReportClient.AlarmSensorTelemetryClient, null);
            convertedObject.MultipleOrganisms = alarmReportClient.MultipleOrganisms;

            OperatorData operatorData = new OperatorData();
            operatorData.Login = alarmReportClient.OperatorLogin;
            operatorData = SmartCadDatabase.SearchObjects(operatorData)[0] as OperatorData;
            convertedObject.Operator = operatorData;

            convertedObject.ReportBaseDepartmentTypes = new ArrayList();

            foreach (ReportBaseDepartmentTypeClientData rbdtcd in alarmReportClient.ReportBaseDepartmentTypesClient)
            {
                ReportBaseDepartmentTypeData tempReportBaseDepartmentType = ReportBaseDepartmentTypeConversion.ToObject(rbdtcd);
                tempReportBaseDepartmentType.ReportBase = convertedObject as ReportBaseData;
                convertedObject.ReportBaseDepartmentTypes.Add(tempReportBaseDepartmentType);
            }

            return convertedObject;
        }
    }

   
}
