using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.Model;
using SmartCadCore.ClientData;

namespace SmartCadCore.Core
{
    public class AlarmLprConversion
    {
        public static AlarmLprClientData ToClient(AlarmLprData alarmReportLprData, 
            UserApplicationData userApplication)
        {
            AlarmLprClientData toConvert = new AlarmLprClientData();

            toConvert.VehicleRequest = VehicleRequestConversion.ToClient(alarmReportLprData.VehicleRequest, userApplication);
            toConvert.Lpr = (LprClientData)DeviceConversion.ToClient((DeviceData)alarmReportLprData.Lpr, userApplication);
            toConvert.StartDate = alarmReportLprData.StartDate;
            toConvert.EndDate = alarmReportLprData.EndDate;
            toConvert.ColorImage = alarmReportLprData.ColorImage;
            toConvert.BWImage = alarmReportLprData.BWImage;
            toConvert.plate = alarmReportLprData.Plate;
            toConvert.Status = (AlarmLprClientData.AlarmStatus)alarmReportLprData.Status;
            
            toConvert.Code = alarmReportLprData.Code;
            toConvert.Version = alarmReportLprData.Version;
            
            return toConvert;
        }

        public static AlarmLprData ToObject(AlarmLprClientData alarmReportLprClient,
            UserApplicationData userApplication)
        {
            AlarmLprData toConvert = new AlarmLprData();

            toConvert.Lpr = (LprData)DeviceConversion.ToObject((DeviceClientData)alarmReportLprClient.Lpr, userApplication);
            toConvert.VehicleRequest = VehicleRequestConversion.ToObject(alarmReportLprClient.VehicleRequest, userApplication);
            toConvert.StartDate = alarmReportLprClient.StartDate;
            toConvert.EndDate = alarmReportLprClient.EndDate;
            toConvert.ColorImage = alarmReportLprClient.ColorImage;
            toConvert.BWImage = alarmReportLprClient.BWImage;
            toConvert.Plate = alarmReportLprClient.plate;

            toConvert.Code = alarmReportLprClient.Code;
            toConvert.Version = alarmReportLprClient.Version;
            toConvert.Status = (AlarmLprData.AlarmStatus)alarmReportLprClient.Status;

            return toConvert;
        }
    }

}