using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.Model;
using SmartCadCore.ClientData;

namespace SmartCadCore.Core
{
    public class PhoneReportAudioConversion
    {
        public static PhoneReportAudioClientData ToClient(PhoneReportAudioData phoneReportAudioData, UserApplicationData userApplication)
        {
            PhoneReportAudioClientData toConvert = new PhoneReportAudioClientData()
            {
                Code = phoneReportAudioData.Code,
                Custom_Code = phoneReportAudioData.CustomCode,
                StorageFolder = phoneReportAudioData.StorageFolder,
                FileName = phoneReportAudioData.FileName
            };

            return toConvert;
        }

        public static PhoneReportAudioData ToObject(PhoneReportAudioClientData phoneReportAudioClient)
        {
            PhoneReportAudioData toConvert = new PhoneReportAudioData(){
                Code = phoneReportAudioClient.Code,
                CustomCode = phoneReportAudioClient.Custom_Code,
                StorageFolder = phoneReportAudioClient.StorageFolder,
                FileName = phoneReportAudioClient.FileName
            };

            return toConvert;
        }
    }
}
