using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

using SmartCadCore.ClientData;
using SmartCadCore.Model;

namespace SmartCadCore.Core
{
    public class OperatorCategoryHistoryConversion
    {
        public static OperatorCategoryHistoryClientData ToClient(OperatorCategoryHistoryData operatorCategoryHistory, UserApplicationData app)
        {
            OperatorCategoryHistoryClientData history = new OperatorCategoryHistoryClientData();
            history.Code = operatorCategoryHistory.Code;
            history.CategoryFriendlyName = operatorCategoryHistory.Category.FriendlyName;
            history.StartDate = operatorCategoryHistory.StartDate.Value;
            history.EndDate = operatorCategoryHistory.EndDate.HasValue ? operatorCategoryHistory.EndDate.Value : DateTime.MinValue;

            history.CategoryCode = operatorCategoryHistory.Category.Code;
            if (operatorCategoryHistory.Operator != null)
                history.OperatorCode = operatorCategoryHistory.Operator.Code;

            return history;
        }

        public static OperatorCategoryHistoryData ToObject(OperatorCategoryHistoryClientData data, UserApplicationData app) 
        {
            OperatorCategoryHistoryData obj = new OperatorCategoryHistoryData();
            obj.Code = data.Code;
            obj = (OperatorCategoryHistoryData)SmartCadDatabase.RefreshObject(obj);
            if (obj.Code == 0)
            {
                //Category
                string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetOperatorCategoriesByCode, data.CategoryCode);
                obj.Category = (OperatorCategoryData)SmartCadDatabase.SearchBasicObject(hql);
                //Operator
                hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetOperatorByCode, data.OperatorCode);
                obj.Operator = (OperatorData)SmartCadDatabase.SearchBasicObject(hql);
            }
            obj.StartDate = data.StartDate;
            obj.EndDate = data.EndDate;

            return obj;
        }
    }
}
