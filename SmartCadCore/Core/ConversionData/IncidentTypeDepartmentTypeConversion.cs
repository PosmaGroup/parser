using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;

using SmartCadCore.Model;
using SmartCadCore.ClientData;

namespace SmartCadCore.Core
{
    public class IncidentTypeDepartmentTypeConversion
    {
        public static IncidentTypeDepartmentTypeClientData ToClient(IncidentTypeDepartmentTypeData itdt, UserApplicationData app)
        {
            IncidentTypeDepartmentTypeClientData convertedClientObject = new IncidentTypeDepartmentTypeClientData();
            convertedClientObject.Code = itdt.Code;
            convertedClientObject.Version = itdt.Version;
            
            convertedClientObject.DepartmentType = new DepartmentTypeClientData();
            convertedClientObject.DepartmentType.Code = itdt.DepartmentType.Code;
            convertedClientObject.DepartmentType.Name = itdt.DepartmentType.Name;
            convertedClientObject.DepartmentType.Version = itdt.DepartmentType.Version;

            if(itdt.IncidentType != null)
                convertedClientObject.IncidentType = IncidentTypeConversion.ToClient(itdt.IncidentType,app);

            return convertedClientObject;
        }

        public static IncidentTypeDepartmentTypeData ToObject(IncidentTypeDepartmentTypeClientData itdt, UserApplicationData app)
        {
            IncidentTypeDepartmentTypeData typeObject = new IncidentTypeDepartmentTypeData();
            typeObject.Code = itdt.Code;
            typeObject.Version = itdt.Version;
            typeObject.DepartmentType = DepartmentTypeConversion.ToObject(itdt.DepartmentType, app);

            if (itdt.IncidentType != null)
            {
                typeObject.IncidentType = new IncidentTypeData(itdt.IncidentType.CustomCode, itdt.IncidentType.Name);
                typeObject.IncidentType.Code = itdt.IncidentType.Code;
                typeObject.IncidentType.Version = itdt.IncidentType.Version;
            }

            return typeObject;
        }
    }
}
