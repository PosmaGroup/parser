using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;

namespace SmartCadCore.Core
{
    public class ReportBaseDepartmentTypeConversion
    {
        public static ReportBaseDepartmentTypeClientData ToClient(ReportBaseDepartmentTypeData rbdtd, UserApplicationData app)
        {
            ReportBaseDepartmentTypeClientData clientObjectConverted = new ReportBaseDepartmentTypeClientData();

            if (app.Equals(UserApplicationData.FirstLevel))
            {
                clientObjectConverted.Code = rbdtd.Code;
                clientObjectConverted.Version = rbdtd.Version;
                clientObjectConverted.DepartmentTypeCode = rbdtd.DepartmentType.Code;
                clientObjectConverted.PriorityCode = rbdtd.Priority.Code;
                clientObjectConverted.ReportBaseCode = rbdtd.ReportBase.Code;
            }
            else if (app.Equals(UserApplicationData.Dispatch))
            {
                clientObjectConverted.Code = rbdtd.Code;
                clientObjectConverted.Version = rbdtd.Version;
                clientObjectConverted.DepartmentTypeCode = rbdtd.DepartmentType.Code;
                clientObjectConverted.PriorityCode = rbdtd.Priority.Code;
                clientObjectConverted.ReportBaseCode = rbdtd.ReportBase.Code;
            }

            return clientObjectConverted;
        }

        public static ReportBaseDepartmentTypeData ToObject(ReportBaseDepartmentTypeClientData rbdtcd)
        {
            ReportBaseDepartmentTypeData objectConverted = new ReportBaseDepartmentTypeData();

            objectConverted.Code = rbdtcd.Code;
            objectConverted.Version = rbdtcd.Version;
            
            DepartmentTypeData dtd = new DepartmentTypeData();
            dtd.Code = rbdtcd.DepartmentTypeCode;
            dtd = SmartCadDatabase.RefreshObject(dtd) as DepartmentTypeData;
            objectConverted.DepartmentType = dtd;

            IncidentNotificationPriorityData inpd = new IncidentNotificationPriorityData();
            inpd.Code = rbdtcd.PriorityCode;
            inpd = SmartCadDatabase.RefreshObject(inpd) as IncidentNotificationPriorityData;
            objectConverted.Priority = inpd;

            return objectConverted;
        }
    }
}
