﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;

namespace SmartCadCore.Core
{
    public class UserApplicationConversion
    {
        public static UserApplicationClientData ToClient(UserApplicationData applicationData, UserApplicationData app)
        {
            UserApplicationClientData client = new UserApplicationClientData();
            client.Code = applicationData.Code;
            client.Version = applicationData.Version;
            client.Name = applicationData.Name;
            client.FriendlyName = applicationData.FriendlyName;

            if (app.Name == UserApplicationData.Administration.Name)
            {
                if (applicationData.InitializeCollections == true)
                {
                    applicationData.Accesses = SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetUserAccessByApplication, applicationData.Name));
                    client.Accesses = new List<UserAccessClientData>();
                    foreach (UserAccessData access in applicationData.Accesses)
                    {
                        access.UserApplication = null;
                        client.Accesses.Add(UserAccessConversion.ToClient(access, app));
                    }
                }
            }
            return client;
        }

        public static UserApplicationData ToObject(UserApplicationClientData clientData, UserApplicationData app)
        {
            UserApplicationData applicationData = new UserApplicationData();
            applicationData.Code = clientData.Code;
            applicationData.Name = clientData.Name;
            applicationData = SmartCadDatabase.SearchObject <UserApplicationData>(applicationData);
            return applicationData;
        }
    }
}
