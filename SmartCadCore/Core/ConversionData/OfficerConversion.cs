using System;
using System.Collections;
using System.Text;

using SmartCadCore.Model;
using SmartCadCore.ClientData;
using Iesi.Collections;

namespace SmartCadCore.Core
{
    public class OfficerConversion
    {
        public static OfficerClientData ToClient(OfficerData officerData, UserApplicationData app)
        {
            OfficerClientData officerClientData = new OfficerClientData();
            officerClientData.Code = officerData.Code;
            officerClientData.Version = officerData.Version;
            officerClientData.FirstName = officerData.FirstName;
            officerClientData.LastName = officerData.LastName;
            officerClientData.PersonID = officerData.PersonId;
            officerClientData.Badge = officerData.Badge;
            officerClientData.Birthday = officerData.Birthday;
            officerClientData.Telephone = officerData.Telephone;
            if (officerData.Department != null)
            {
                officerClientData.DepartmentType = new DepartmentTypeClientData();
                officerClientData.DepartmentType.Code = officerData.Department.Code;
                officerClientData.DepartmentType.Name = officerData.Department.Name;
            }

            officerClientData.Position = new PositionClientData();
            if (officerData.Position != null)
            {
                officerClientData.Position.Code = officerData.Position.Code;
                officerClientData.Position.Name = officerData.Position.Name;
            }
            officerClientData.Rank = new RankClientData();
            if (officerData.Rank != null)
            {
                officerClientData.Rank.Code = officerData.Rank.Code;
                officerClientData.Rank.Name = officerData.Rank.Name;
            }
            if (app.Name == UserApplicationData.Administration.Name)
            {
                if (officerData.InitializeCollections == true)
                {
                    FillCollections(officerData, ref officerClientData, app);
                }
            }
            else
            {
                FillCollections(officerData, ref officerClientData, app);
            }

            if (app.Name == UserApplicationData.Dispatch.Name)
            { 
                string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetUnitAssignCodeByOfficerCode, officerData.Code, SmartCadDatabase.GetTimeFromBD().ToString(ApplicationUtil.DateNHibernateFormat));
                object result = SmartCadDatabase.SearchBasicObject(hql);
                if (result != null)
                    officerClientData.UnitAssignCode = (int)result;
            }

            return officerClientData;
        }

        public static OfficerData ToObject(OfficerClientData officerClientData, UserApplicationData app)
        {
            OfficerData officerData = new OfficerData();
            officerData.Code = officerClientData.Code;
            if (officerData.Code != 0)
                officerData = (OfficerData)SmartCadDatabase.RefreshObject(officerData);

            officerData.Badge = officerClientData.Badge;
            officerData.Birthday = officerClientData.Birthday;
            
            officerData.DepartmentStation = new DepartmentStationData();
            officerData.DepartmentStation.Code = officerClientData.DepartmentStation.Code;
            officerData.DepartmentStation = (DepartmentStationData)SmartCadDatabase.RefreshObject(officerData.DepartmentStation);

            officerData.FirstName = officerClientData.FirstName;
            officerData.LastName = officerClientData.LastName;
            officerData.PersonId = officerClientData.PersonID;
            officerData.Position = officerClientData.Position==null?null:(PositionData)PositionConversion.ToObject(officerClientData.Position,app);
            officerData.Rank = officerClientData.Rank == null ? null : (RankData)RankConversion.ToObject(officerClientData.Rank, app);
            officerData.Telephone = officerClientData.Telephone;
            officerData.Version = officerClientData.Version;

            return officerData;
        }

        private static void FillCollections(OfficerData officerData, ref OfficerClientData officerClientData, UserApplicationData app)
        {
            officerClientData.UnitsAssigned = new ArrayList();

            officerClientData.DepartmentStation = new DepartmentStationClientData();
            officerClientData.DepartmentStation.Code = officerData.DepartmentStation.Code;
            officerClientData.DepartmentStation.CustomCode = officerData.DepartmentStation.CustomCode;
            officerClientData.DepartmentStation.Name = officerData.DepartmentStation.Name;

            officerClientData.DepartmentStation.DepartmentZone = new DepartmentZoneClientData();
            officerClientData.DepartmentStation.DepartmentZone.Code = officerData.DepartmentStation.DepartmentZone.Code;
            officerClientData.DepartmentStation.DepartmentZone.CustomCode = officerData.DepartmentStation.DepartmentZone.CustomCode;
            officerClientData.DepartmentStation.DepartmentZone.Name = officerData.DepartmentStation.DepartmentZone.Name;

            if (officerData.Department != null)
            {
                officerClientData.DepartmentType.DepartmentZones = new ArrayList();
                if (officerData.Department.DepartmentZones != null)
                {
                    officerData.Department.DepartmentZones = new ListSet(SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetDepartmentZonesByDepartmentType, officerData.Department.Code)));
                    
                    foreach (DepartmentZoneData zone in officerData.Department.DepartmentZones)
                    {
                        DepartmentZoneClientData zoneClient = new DepartmentZoneClientData();
                        zoneClient.Code = zone.Code;
                        zoneClient.CustomCode = zone.CustomCode;
                        zoneClient.Name = zone.Name;
                        officerClientData.DepartmentType.DepartmentZones.Add(zoneClient);
                    }
                }
            }
        }

    }
}
