using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;

namespace SmartCadCore.Core
{
    public class OperatorObservationConversion
    {
        public static OperatorObservationClientData ToClient(OperatorObservationData data, UserApplicationData app) 
        {
            OperatorObservationClientData client = new OperatorObservationClientData();
            client.Code = data.Code;
            client.NameObservationType= data.ObservationType.Name;
            client.FriendlyNameObservationType = data.ObservationType.FriendlyName;
            client.Observation = data.Observation;
            client.Date = data.Date;
            client.SupervisorName = data.Supervisor.FirstName + " " + data.Supervisor.LastName;
            client.SupervisorCode = data.Supervisor.Code;
            client.OperatorCode = data.Operator.Code;
            return client;
        }

        public static OperatorObservationData ToObject(OperatorObservationClientData data,UserApplicationData app) 
        {
            OperatorObservationData obj = new OperatorObservationData();
            obj.Code = data.Code;
            obj.Observation = data.Observation;
            obj.Date = data.Date;

            string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetObservationTypeByName,data.NameObservationType);
            ObservationTypeData obType = (ObservationTypeData)SmartCadDatabase.SearchBasicObject(hql);
            obj.ObservationType = obType;
            //get operator
            hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetOperatorByCode,data.OperatorCode);
            OperatorData ope =(OperatorData) SmartCadDatabase.SearchBasicObject(hql);
            obj.Operator = ope;
            //get supervisor
            hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetOperatorByCode,data.SupervisorCode);
            OperatorData sup = (OperatorData)SmartCadDatabase.SearchBasicObject(hql);
            obj.Supervisor = sup;

            return obj;

        }
    }
}
