using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;

using SmartCadCore.ClientData;
using SmartCadCore.Model;
using NHibernate.Collection;
using Iesi.Collections;

namespace SmartCadCore.Core
{
    public class TwitterReportAnswerConversion
    {
        //TODO: Borrar
        public static TwitterReportAnswerClientData ToClient(TwitterReportAnswerData data, UserApplicationData app)
        {
            TwitterReportAnswerClientData client = new TwitterReportAnswerClientData();

            if (app.Equals(UserApplicationData.SocialNetworks))
            {
                client.Code = data.Code;
                client.Version = data.Version;
                client.ReportCode = data.TwitterReport.Code;
                client.QuestionCode = data.Question.Code;
                client.Answers = new ArrayList();

                //TODO: Chequear
                if (SmartCadDatabase.IsInitialize(data.SetAnswers) == false)
                {
                    SmartCadDatabase.InitializeLazy(data, data.SetAnswers);
                }

                foreach (PossibleAnswerAnswerData paad in data.SetAnswers)
                {
                    client.Answers.Add(PossibleAnswerAnswerConversion.ToClient(paad, app));
                }
            }

            return client;
        }

        public static TwitterReportAnswerData ToObject(ReportAnswerClientData client)
        {
            TwitterReportAnswerData data = new TwitterReportAnswerData();

            data.Code = client.Code;
            data.Version = client.Version;
            data.SetAnswers = new HashedSet();
            foreach (PossibleAnswerAnswerClientData paacd in client.Answers)
            {
                PossibleAnswerAnswerData paad = PossibleAnswerAnswerConversion.ToObject(paacd);
                paad.TwitterReportAnswer = data;
                data.SetAnswers.Add(paad);
            }
            QuestionData question = new QuestionData();
            question.Code = client.QuestionCode;
            question.Text = client.QuestionText;
       
            StringBuilder queryStr = new StringBuilder(SmartCadHqls.GetCustomHql(
               SmartCadHqls.GetQuestionShorTextHql, question.Code));

            object objectList = SmartCadDatabase.SearchBasicObjects(
                queryStr.ToString(), true);
            ArrayList list = (ArrayList)objectList;
            question.ShortText = (string)list[0];
            data.Question = question;

            return data;
        }
    }
}
