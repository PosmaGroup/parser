using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;
using System.Collections;
using SmartCadCore.Common;

namespace SmartCadCore.Core
{
    public class SupervisorCloseReportConversion
    {
        public static SupervisorCloseReportClientData ToClient(SupervisorCloseReportData supervisorCloseReportData, UserApplicationData app)
        {
            SupervisorCloseReportClientData client = new SupervisorCloseReportClientData();
            client.Code = supervisorCloseReportData.Code;
            client.Finished = supervisorCloseReportData.Finished;
            if (SmartCadDatabase.IsInitialize(supervisorCloseReportData.Messages) == false)
                SmartCadDatabase.InitializeLazy(supervisorCloseReportData, supervisorCloseReportData.Messages); 

                client.Messages = new ArrayList();
                foreach (SupervisorCloseReportMessageData scrmd in supervisorCloseReportData.Messages)
                {
                    client.Messages.Add(SupervisorCloseReportMessageConversion.ToClient(scrmd, app));
                }
            
            client.SessionCode = supervisorCloseReportData.Session.Code;
            if (supervisorCloseReportData.Session.UserAccount != null)
                client.SessionFullName = supervisorCloseReportData.Session.UserAccount.FirstName + " " + supervisorCloseReportData.Session.UserAccount.LastName;
            else {
                client.SessionFullName = "";
            }

            if (supervisorCloseReportData.Session.StartDateLogin.HasValue == true)
                client.Start = supervisorCloseReportData.Session.StartDateLogin.Value;
            
            if (supervisorCloseReportData.Session.EndDateLogin.HasValue == true)
                client.End = supervisorCloseReportData.Session.EndDateLogin.Value;

            if (supervisorCloseReportData.Session.UserAccount != null)
            {
                if (ServiceUtil.CheckApplicationSupervisor(supervisorCloseReportData.Session.UserAccount.Role,UserApplicationData.FirstLevel.Name) == true &&
					ServiceUtil.CheckApplicationSupervisor(supervisorCloseReportData.Session.UserAccount.Role, UserApplicationData.Dispatch.Name) == false)
                {
                    client.Supervisor = ResourceLoader.GetString2("FirstLevelSupervisor");
                    client.SupervisorType = (int)SupervisorType.FirstLevel;
                    client.SupervisorPersonId = supervisorCloseReportData.Session.UserAccount.PersonId;
                }
                else if (ServiceUtil.CheckApplicationSupervisor(supervisorCloseReportData.Session.UserAccount.Role,UserApplicationData.FirstLevel.Name) == false &&
                         ServiceUtil.CheckApplicationSupervisor(supervisorCloseReportData.Session.UserAccount.Role, UserApplicationData.Dispatch.Name) == true)
                {
                    client.Supervisor = ResourceLoader.GetString2("DispatchSupervisor");
                    client.SupervisorType = (int)SupervisorType.Dispatch;
                    client.SupervisorPersonId = supervisorCloseReportData.Session.UserAccount.PersonId;

                    IList departs = SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetOperatorDepartamentTypes, supervisorCloseReportData.Session.UserAccount.Code));
                    if (departs != null)
                        foreach (DepartmentTypeData departament in departs)
                        {
                            client.DepartamentTypes.Add(departament.Name);
                        }
                }
                else
                {
                    client.Supervisor = ResourceLoader.GetString2("GeneralSupervisor");
                    client.SupervisorType = (int)SupervisorType.General;
                }
            }
            client.Version = supervisorCloseReportData.Version;
            return client;
        }

        public static SupervisorCloseReportData ToObject(SupervisorCloseReportClientData supervisorCloseReportClientData, UserApplicationData app)
        {
            SupervisorCloseReportData objectData = new SupervisorCloseReportData();
            objectData.Code = supervisorCloseReportClientData.Code;
            if (objectData.Code == 0)
            {
                SessionHistoryData session = new SessionHistoryData();
                session.Code = supervisorCloseReportClientData.SessionCode;
                objectData.Session = session;
                objectData.Messages = new ArrayList();
            }
            else
            {
                objectData = (SupervisorCloseReportData)SmartCadDatabase.RefreshObject(objectData, true);
                if (supervisorCloseReportClientData.Finished == false)
                    SmartCadDatabase.InitializeLazy(objectData, objectData.Messages);
                objectData.Version = supervisorCloseReportClientData.Version;
            }
            if (supervisorCloseReportClientData.Finished == false)
            {
                foreach (SupervisorCloseReportMessageClientData clientMessage in supervisorCloseReportClientData.Messages)
                {
                    SupervisorCloseReportMessageData scrmd = SupervisorCloseReportMessageConversion.ToObject(clientMessage, app);
                    scrmd.Report = objectData;
                    if (objectData.Messages.Contains(scrmd) == false)
                        objectData.Messages.Add(scrmd);
                }
            }
            objectData.Finished = supervisorCloseReportClientData.Finished;
            return objectData;
        }
    }
}
