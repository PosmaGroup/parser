using System;
using System.Collections;
using System.Text;

using SmartCadCore.ClientData;
using SmartCadCore.Model;

namespace SmartCadCore.Core
{
    public class UnitEndingReportTypeConversion
    {
        public static UnitEndingReportTypeClientData ToClient(UnitEndingReportTypeData unitEndingReportTypeData, UserApplicationData app)
        {
            UnitEndingReportTypeClientData endingReportTypeClientData = new UnitEndingReportTypeClientData();
            if (app.Name == UserApplicationData.Dispatch.Name)
            {
                endingReportTypeClientData.Code = unitEndingReportTypeData.Code;
                endingReportTypeClientData.Version = unitEndingReportTypeData.Version;
                endingReportTypeClientData.CustomCode = unitEndingReportTypeData.CustomCode;
                endingReportTypeClientData.FriendlyName = unitEndingReportTypeData.FriendlyName;
                endingReportTypeClientData.Name = unitEndingReportTypeData.Name;
            }
            return endingReportTypeClientData;
        }

        public static UnitEndingReportTypeData ToObject(UnitEndingReportTypeClientData endingReportTypeClientData)
        {
            UnitEndingReportTypeData unitEndingReportTypeData = new UnitEndingReportTypeData();
            unitEndingReportTypeData.Code = endingReportTypeClientData.Code;
            unitEndingReportTypeData.CustomCode = endingReportTypeClientData.CustomCode;
            unitEndingReportTypeData = SmartCadDatabase.SearchObject<UnitEndingReportTypeData>(unitEndingReportTypeData);
            return unitEndingReportTypeData;
        }
    }
}
