using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;
using System.Collections;
using Iesi.Collections;
using System.Windows;
using System.Drawing;

namespace SmartCadCore.Core
{
    public class DepartmentZoneConversion
    {
        public static DepartmentZoneClientData ToClient(DepartmentZoneData departmentZone, UserApplicationData app)
        {
            DepartmentZoneClientData departmentZoneClient = new DepartmentZoneClientData();
            departmentZoneClient.Code = departmentZone.Code;
            departmentZoneClient.Version = departmentZone.Version;
            departmentZoneClient.CustomCode = departmentZone.CustomCode;
            departmentZoneClient.Name = departmentZone.Name;

            if (departmentZone.DepartmentType != null)
            {
                DepartmentTypeClientData departmentTypeClientData = new DepartmentTypeClientData();
                departmentTypeClientData.Code = departmentZone.DepartmentType.Code;
                departmentTypeClientData.Version = departmentZone.DepartmentType.Version;
                departmentTypeClientData.Name = departmentZone.DepartmentType.Name;
                departmentTypeClientData.Color = departmentZone.DepartmentType.Color;
                departmentTypeClientData.DepartmentZones = new ArrayList();
                departmentZoneClient.DepartmentType = departmentTypeClientData;
            }
                //departmentZoneClient.DepartmentType = DepartmentTypeConversion.ToClient(departmentZone.DepartmentType, app);

            if (app.Name == UserApplicationData.Supervision.Name)
            {
            }
            else if (app.Name == UserApplicationData.Dispatch.Name)
            {
                #region Dispatch
                departmentZoneClient.DepartmentStations = new ArrayList();
                if (SmartCadDatabase.IsInitialize(departmentZone.SetDepartmentStations) == true)
                {
                    //SmartCadDatabase.InitializeLazy(departmentZone, departmentZone.SetDepartmentStations);
                    if (departmentZone.SetDepartmentStations != null)
                    {
                        foreach (DepartmentStationData departmentStation in departmentZone.SetDepartmentStations)
                        {
                            departmentStation.DepartmentZone = null;
                            DepartmentStationClientData dep = DepartmentStationConversion.ToClient(departmentStation, app);
                            dep.DepartmentZone = departmentZoneClient;
                            departmentZoneClient.DepartmentStations.Add(dep);
                        }
                    }
                }
                #endregion
            }
            else if (app.Name == UserApplicationData.Administration.Name)
            {
                #region Administration
                if (SmartCadDatabase.IsInitialize(departmentZone.DepartmentZoneAddres) &&
                    SmartCadDatabase.IsInitialize(departmentZone.SetDepartmentStations))
                {
                    departmentZone.InitializeCollections = true;
                }
                if (departmentZone.InitializeCollections == true)
                {
                    departmentZoneClient.DepartmentZoneAddress = new ArrayList();
                    departmentZoneClient.DepartmentStations = new ArrayList();

                    if (SmartCadDatabase.IsInitialize(departmentZone.DepartmentZoneAddres) == false)
                        SmartCadDatabase.InitializeLazy(departmentZone, departmentZone.DepartmentZoneAddres);
                    if (departmentZone.DepartmentZoneAddres != null)
                    {
                        foreach (DepartmentZoneAddressData departmentAddress in departmentZone.DepartmentZoneAddres)
                        {
                            departmentZoneClient.DepartmentZoneAddress.Add(DepartmentZoneAddressConversion.ToClient(departmentAddress, app));
                        }
                    }

                    if (SmartCadDatabase.IsInitialize(departmentZone.SetDepartmentStations) == false)
                        SmartCadDatabase.InitializeLazy(departmentZone, departmentZone.SetDepartmentStations);
                    if (departmentZone.SetDepartmentStations != null)
                    {
                        foreach (DepartmentStationData departmentStation in departmentZone.SetDepartmentStations)
                        {
                            departmentZoneClient.DepartmentStations.Add(DepartmentStationConversion.ToClient(departmentStation, app));
                        }
                    }
                }
                else 
                {
                    if (SmartCadDatabase.IsInitialize(departmentZone.DepartmentZoneAddres))
                    {
                        departmentZoneClient.DepartmentZoneAddress = new ArrayList();
                        foreach (DepartmentZoneAddressData departmentAddress in departmentZone.DepartmentZoneAddres)
                        {
                            departmentZoneClient.DepartmentZoneAddress.Add(DepartmentZoneAddressConversion.ToClient(departmentAddress, app));
                        }
                    }
                    if (SmartCadDatabase.IsInitialize(departmentZone.SetDepartmentStations))
                    {
                        departmentZoneClient.DepartmentStations = new ArrayList();
                        foreach (DepartmentStationData departmentStation in departmentZone.SetDepartmentStations)
                        {
                            departmentZoneClient.DepartmentStations.Add(DepartmentStationConversion.ToClient(departmentStation, app));
                        }
                    }
                }
                
                #endregion
            }
            else if (app.Name == UserApplicationData.Map.Name)
            {
                #region Map
                departmentZoneClient.DepartmentZoneAddress = new ArrayList();
                if (SmartCadDatabase.IsInitialize(departmentZone.DepartmentZoneAddres) == true)
                {
                    //SmartCadDatabase.InitializeLazy(departmentZone, departmentZone.DepartmentZoneAddres);
                    foreach (DepartmentZoneAddressData departmentAddress in departmentZone.DepartmentZoneAddres)
                    {
                        departmentZoneClient.DepartmentZoneAddress.Add(DepartmentZoneAddressConversion.ToClient(departmentAddress, app));
                    }
                }

                if (SmartCadDatabase.IsInitialize(departmentZone.SetDepartmentStations) == true)
                {
                    //SmartCadDatabase.InitializeLazy(departmentZone, departmentZone.SetDepartmentStations);
                    if (departmentZone.SetDepartmentStations != null)
                    {
                        departmentZoneClient.DepartmentStations = new ArrayList();
                        foreach (DepartmentStationData departmentStation in departmentZone.SetDepartmentStations)
                        {
                            departmentZoneClient.DepartmentStations.Add(DepartmentStationConversion.ToClient(departmentStation, app));
                        }
                    }
                }
                #endregion
            }

            return departmentZoneClient;
        }

        public static DepartmentZoneData ToObject(DepartmentZoneClientData departmentZoneClient, UserApplicationData app)
        {
            DepartmentZoneData departmentZone = new DepartmentZoneData();
            departmentZone.Code = departmentZoneClient.Code;
            departmentZone.Version = departmentZoneClient.Version;
            
            if(departmentZone.Code != 0)
                departmentZone =
                  SmartCadDatabase.SearchObject<DepartmentZoneData>(SmartCadHqls.GetCustomHql(SmartCadHqls.GetDepartmentZoneWithAddressAndStationsByCode, departmentZoneClient.Code));

            
            departmentZone.Name = departmentZoneClient.Name;
            departmentZone.CustomCode = departmentZoneClient.CustomCode;
            departmentZone.DepartmentType = SmartCadDatabase.SearchObject<DepartmentTypeData>(
                SmartCadHqls.GetCustomHql(SmartCadHqls.GetDepartmentsTypeByCode, departmentZoneClient.DepartmentType.Code));

            departmentZone.DepartmentZoneAddres = new HashedSet();
            if (departmentZoneClient.DepartmentZoneAddress != null)
            {
                foreach (DepartmentZoneAddressClientData depClientData in departmentZoneClient.DepartmentZoneAddress)
                {
                    DepartmentZoneAddressData dzd = DepartmentZoneAddressConversion.ToObject(depClientData, app);
                    dzd.Zone = departmentZone;
                    departmentZone.DepartmentZoneAddres.Add(dzd);
                }
            }
            
            return departmentZone;
        }
    }
}
