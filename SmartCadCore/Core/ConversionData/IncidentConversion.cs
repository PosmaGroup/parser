using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.ClientData;
using SmartCadCore.Model;
using System.Collections;
using NHibernate.Collection;
using Iesi.Collections;
using SmartCadCore.Common;

namespace SmartCadCore.Core
{
    public class IncidentConversion
    {
        public static IncidentClientData ToClient(IncidentData incidentData, UserApplicationData app)
        {
            IncidentClientData clientConvertedObject = new IncidentClientData();

            try
            {
                clientConvertedObject.Code = incidentData.Code;
                clientConvertedObject.Version = incidentData.Version;
                clientConvertedObject.Address = AddressConversion.ToClient(incidentData.Address, app);
                clientConvertedObject.CustomCode = incidentData.CustomCode;
                clientConvertedObject.IsEmergency = incidentData.IsEmergency.Value;
                if (incidentData.StartDate.HasValue == true)
                {
                    clientConvertedObject.StartDate = incidentData.StartDate.Value;
                }
                if (incidentData.EndDate.HasValue == true)
                {
                    clientConvertedObject.EndDate = incidentData.EndDate.Value;
                }
                clientConvertedObject.PhoneReports = new ArrayList();
                clientConvertedObject.SupportRequestReports = new ArrayList();
                clientConvertedObject.Status = IncidentStatusConversion.ToClient(incidentData.Status, app);
                clientConvertedObject.Notes = new ArrayList();
                clientConvertedObject.IncidentNotifications = new ArrayList();
                clientConvertedObject.IncidentTypes = new ArrayList();

                if (app == UserApplicationData.FirstLevel || app == UserApplicationData.Report)
                {
                    #region FirstLevel

                    HashedSet setIncidentTypes = new HashedSet();
                    if (SmartCadDatabase.IsInitialize(incidentData.SetReportBaseList) == true)
                    {
                        //SmartCadDatabase.InitializeLazy(incidentData, incidentData.SetReportBaseList);

                        foreach (ReportBaseData reportBase in incidentData.SetReportBaseList)
                        {
                            if (reportBase is PhoneReportData)
                            {
                                clientConvertedObject.SourceIncidentApp = SourceIncidentAppEnum.FirstLevel;
                                PhoneReportData toConvert = (PhoneReportData)reportBase;
                                clientConvertedObject.PhoneReports.Add(PhoneReportConversion.ToClient(toConvert, app));
                            }
                            setIncidentTypes.AddAll(reportBase.SetIncidentTypes);
                        }
                        foreach (IncidentTypeData incidentType in setIncidentTypes)
                        {
                            clientConvertedObject.IncidentTypes.Add(IncidentTypeConversion.ToClient(incidentType, app));
                        }
                        setIncidentTypes.Clear();
                    }
                    #endregion
                }

                if (app == UserApplicationData.Dispatch)
                {
                    #region Dispatch
                    if (SmartCadDatabase.IsInitialize(incidentData.IncidentNotifications) == true)
                    {
                        //IList incidentNotifications = SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetIncidentNotificationsByIncidentCustomCode, incidentData.CustomCode));
                        if (incidentData.IncidentNotifications != null)
                        {
                            foreach (IncidentNotificationData incidentNotification in incidentData.IncidentNotifications)
                            {
                                clientConvertedObject.IncidentNotifications.Add(IncidentNotificationConversion.ToClient(incidentNotification, app));
                            }
                        }
                    }
                    #endregion
                }

                if (app == UserApplicationData.Map)
                {
                    #region Map
                    if (incidentData.Status.Name != IncidentStatusData.Closed.Name)
                    {
                        ArrayList setIncidentTypes = GetIncidentTypesFromIncident(incidentData, app, clientConvertedObject);
                        
                        if (setIncidentTypes == null || setIncidentTypes.Count > 1)
                        {
                            clientConvertedObject.IconName = ResourceLoader.GetString("DefaultIconIncidentType") + ".BMP";
                            if (setIncidentTypes.Count > 1)
                            {
                                clientConvertedObject.IncidentTypes = setIncidentTypes;
                            }
                        }
                        else
                        {
                            string icon = GetIconNameIncident(setIncidentTypes);
                            if (string.IsNullOrEmpty(icon))
                            {
                                clientConvertedObject.IconName = ResourceLoader.GetString("DefaultIconIncidentType") + ".BMP";
                            }
                            else
                            {
                                clientConvertedObject.IconName = icon;
                            }
                            clientConvertedObject.IncidentTypes = setIncidentTypes;
                        }
                    }
                    #endregion
                }
                else if (app == UserApplicationData.Supervision)
                {
                    #region Supervision
                    long count = (long)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(SmartCadHqls.PhoneReportCountIncident, incidentData.Code));
                    clientConvertedObject.PhoneReportCount = (int)count;
                    HashedSet setIncidentTypes = new HashedSet();
                    if (SmartCadDatabase.IsInitialize(incidentData.SetReportBaseList) == true)
                    {
                        //SmartCadDatabase.InitializeLazy(incidentData, incidentData.SetReportBaseList);
                        foreach (ReportBaseData reportBase in incidentData.SetReportBaseList)
                        {
                            if (reportBase is PhoneReportData)
                            {
                                PhoneReportData toConvert = (PhoneReportData)reportBase;
                                clientConvertedObject.PhoneReports.Add(PhoneReportConversion.ToClient(toConvert, app));
                            }

                            setIncidentTypes.AddAll(reportBase.SetIncidentTypes);
                        }

                        foreach (IncidentTypeData incidentType in setIncidentTypes)
                        {
                            clientConvertedObject.IncidentTypes.Add(IncidentTypeConversion.ToClient(incidentType, app));
                        }
                        setIncidentTypes.Clear();
                    }
                    #endregion
                }
                else if (app == UserApplicationData.Cctv)
                {
                    #region Cctv
                    long count = (long)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(SmartCadHqls.CctvReportCountIncident, incidentData.Code));
                    clientConvertedObject.CctvReportCount = (int)count;
                    HashedSet setIncidentTypes = new HashedSet();
                    if (SmartCadDatabase.IsInitialize(incidentData.SetReportBaseList) == true)
                    {
                        //    SmartCadDatabase.InitializeLazy(incidentData, incidentData.SetReportBaseList);
                        foreach (ReportBaseData reportBase in incidentData.SetReportBaseList)
                        {
                            if (reportBase is CctvReportData)
                            {
                                clientConvertedObject.SourceIncidentApp = SourceIncidentAppEnum.Cctv;
                                CctvReportData toConvert = (CctvReportData)reportBase;
                                if (clientConvertedObject.CctvReports == null)
                                {
                                    clientConvertedObject.CctvReports = new ArrayList();
                                }
                                clientConvertedObject.CctvReports.Add(CctvReportConversion.ToClient(toConvert, app));
                            }
                            if (SmartCadDatabase.IsInitialize(reportBase.SetIncidentTypes) == false)
                            {
                                SmartCadDatabase.InitializeLazy(reportBase, reportBase.SetIncidentTypes);
                            }
                            setIncidentTypes.AddAll(reportBase.SetIncidentTypes);
                        }

                        foreach (IncidentTypeData incidentType in setIncidentTypes)
                        {
                            clientConvertedObject.IncidentTypes.Add(IncidentTypeConversion.ToClient(incidentType, app));
                        }
                        setIncidentTypes.Clear();
                    }
                    #endregion
                }
                else if (app == UserApplicationData.Alarm)
                {
                    #region Alarm
                    long count = (long)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(SmartCadHqls.AlarmReportCountIncident, incidentData.Code));
                    clientConvertedObject.AlarmReportCount = (int)count;
                    HashedSet setIncidentTypes = new HashedSet();
                    if (SmartCadDatabase.IsInitialize(incidentData.SetReportBaseList) == true)
                    {                        
                        foreach (ReportBaseData reportBase in incidentData.SetReportBaseList)
                        {
                            if (reportBase is AlarmReportData)
                            {
                                clientConvertedObject.SourceIncidentApp = SourceIncidentAppEnum.Alarm;
                                AlarmReportData toConvert = (AlarmReportData)reportBase;
                                if (clientConvertedObject.AlarmReports == null)
                                {
                                    clientConvertedObject.AlarmReports = new ArrayList();
                                }
                                clientConvertedObject.AlarmReports.Add(AlarmReportConversion.ToClient(toConvert, app));
                            }
                            else if (reportBase is AlarmLprReportData)
                            {
                                clientConvertedObject.SourceIncidentApp = SourceIncidentAppEnum.Alarm;
                                AlarmLprReportData toConvert = (AlarmLprReportData)reportBase;
                                if (clientConvertedObject.AlarmReports == null)
                                {
                                    clientConvertedObject.AlarmReports = new ArrayList();
                                }
                                clientConvertedObject.AlarmReports.Add(AlarmLprReportConversion.ToClient(toConvert, app));
                            }
                            if (SmartCadDatabase.IsInitialize(reportBase.SetIncidentTypes) == false)
                            {
                                SmartCadDatabase.InitializeLazy(reportBase, reportBase.SetIncidentTypes);
                            }
                            setIncidentTypes.AddAll(reportBase.SetIncidentTypes);
                        }

                        foreach (IncidentTypeData incidentType in setIncidentTypes)
                        {
                            clientConvertedObject.IncidentTypes.Add(IncidentTypeConversion.ToClient(incidentType, app));
                        }
                        setIncidentTypes.Clear();
                    }
                    #endregion
                }
                else if (app == UserApplicationData.SocialNetworks)
                {
                    #region SocialNetworks
                    long count = (long)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(SmartCadHqls.TwitterReportCountIncident, incidentData.Code));
                    clientConvertedObject.TwitterReportCount = (int)count;
                    HashedSet setIncidentTypes = new HashedSet();
                    if (SmartCadDatabase.IsInitialize(incidentData.SetReportBaseList) == true)
                    {
                        foreach (ReportBaseData reportBase in incidentData.SetReportBaseList)
                        {
                            clientConvertedObject.SourceIncidentApp = SourceIncidentAppEnum.SocialNetworks;
                            TwitterReportData toConvert = (TwitterReportData)reportBase;
                            if (clientConvertedObject.TwitterReports == null)
                            {
                                clientConvertedObject.TwitterReports = new ArrayList();
                            }
                            clientConvertedObject.TwitterReports.Add(TwitterReportConversion.ToClient(toConvert, app));
                            
                            if (SmartCadDatabase.IsInitialize(reportBase.SetIncidentTypes) == false)
                            {
                                SmartCadDatabase.InitializeLazy(reportBase, reportBase.SetIncidentTypes);
                            }
                            setIncidentTypes.AddAll(reportBase.SetIncidentTypes);
                        }

                        foreach (IncidentTypeData incidentType in setIncidentTypes)
                        {
                            clientConvertedObject.IncidentTypes.Add(IncidentTypeConversion.ToClient(incidentType, app));
                        }
                        setIncidentTypes.Clear();
                    }
                    #endregion
                }
                else if (app == UserApplicationData.Lpr)
                {
                    #region Lpr
                    long count = (long)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(SmartCadHqls.LprReportCountIncident, incidentData.Code));
                    clientConvertedObject.LprReportCount = (int)count;
                    HashedSet setIncidentTypes = new HashedSet();
                    if (SmartCadDatabase.IsInitialize(incidentData.SetReportBaseList) == true)
                    {
                        foreach (ReportBaseData reportBase in incidentData.SetReportBaseList)
                        {
                            clientConvertedObject.SourceIncidentApp = SourceIncidentAppEnum.Lpr;
                            AlarmLprReportData toConvert = (AlarmLprReportData)reportBase;
                            if (clientConvertedObject.LprReports == null)
                            {
                                clientConvertedObject.LprReports = new ArrayList();
                            }
                            clientConvertedObject.LprReports.Add(AlarmLprReportConversion.ToClient(toConvert, app));

                            if (SmartCadDatabase.IsInitialize(reportBase.SetIncidentTypes) == false)
                            {
                                SmartCadDatabase.InitializeLazy(reportBase, reportBase.SetIncidentTypes);
                            }
                            setIncidentTypes.AddAll(reportBase.SetIncidentTypes);
                        }

                        foreach (IncidentTypeData incidentType in setIncidentTypes)
                        {
                            clientConvertedObject.IncidentTypes.Add(IncidentTypeConversion.ToClient(incidentType, app));
                        }
                        setIncidentTypes.Clear();
                    }
                    #endregion
                }

            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }

            return clientConvertedObject;
        }

        /// <summary>
        /// Get the icon name for the incident, if the incident had more than one incidentType
        /// then returns the icon generic for the incidents.
        /// </summary>
        /// <param name="setIncidentTypes">Set of incident types</param>
        /// <returns>Name of the icon</returns>
        private static string GetIconNameIncident(ArrayList setIncidentTypes)
        {
            IncidentTypeClientData current = null;
            foreach (IncidentTypeClientData incidentType in setIncidentTypes)
            {
                current = incidentType;
                break;
            }
            string icon = "";
            if (current != null)
                icon = current.IconName;
            return icon;
        }

        private static ArrayList GetIncidentTypesFromIncident(IncidentData incidentData, UserApplicationData app, IncidentClientData clientConvertedObject)
        {
            ArrayList setIncidentTypes = new ArrayList();
            Dictionary<int, bool> dic = new Dictionary<int, bool>();
            if (SmartCadDatabase.IsInitialize(incidentData.SetReportBaseList) == false)
            {
                SmartCadDatabase.InitializeLazy(incidentData, incidentData.SetReportBaseList);    
            }
            if (SmartCadDatabase.IsInitialize(incidentData.SetReportBaseList) == true)
            {
                foreach (ReportBaseData reportBase in incidentData.SetReportBaseList)
                {
                    /*if (reportBase is PhoneReportData)
                    {
                        PhoneReportData toConvert = (PhoneReportData)reportBase;
                        clientConvertedObject.PhoneReports.Add(PhoneReportConversion.ToClient(toConvert, app));
                    }*/
                    /*else if (reportBase is SupportRequestReportData)
                    {
                        SupportRequestReportData toConvert = (SupportRequestReportData)reportBase;
                        clientConvertedObject.SupportRequestReports.Add(SupportRequestReportConversion.ToClient(toConvert, app));
                    }*/

                    if (SmartCadDatabase.IsInitialize(reportBase.SetIncidentTypes) == false)
                    {
                        SmartCadDatabase.InitializeLazy(reportBase, reportBase.SetIncidentTypes);
                    }
                    foreach (IncidentTypeData inc in reportBase.SetIncidentTypes)
                    {
                        if (!dic.ContainsKey(inc.Code))
                        {
                            dic.Add(inc.Code, true);
                            setIncidentTypes.Add(IncidentTypeConversion.ToClient(inc, app));
                        }
                    }
                }
            }
            return setIncidentTypes;
        }

        public static IncidentData ToObject(IncidentClientData incidentClient)
        {
            IncidentData incidentObjectConverted = new IncidentData();
            incidentObjectConverted.Code = incidentClient.Code;
            if (incidentClient.SourceIncidentApp == SourceIncidentAppEnum.FirstLevel)
            {
                incidentObjectConverted.SourceIncidentApp = UserApplicationData.FirstLevel;
            }
            else if (incidentClient.SourceIncidentApp == SourceIncidentAppEnum.Cctv)
            {
                incidentObjectConverted.SourceIncidentApp = UserApplicationData.Cctv;
            }
            else if (incidentClient.SourceIncidentApp == SourceIncidentAppEnum.Alarm)
            {
                incidentObjectConverted.SourceIncidentApp = UserApplicationData.Alarm;
            }
            else if (incidentClient.SourceIncidentApp == SourceIncidentAppEnum.SocialNetworks)
            {
                incidentObjectConverted.SourceIncidentApp = UserApplicationData.SocialNetworks;
            }
            else if (incidentClient.SourceIncidentApp == SourceIncidentAppEnum.Lpr)
            {
                incidentObjectConverted.SourceIncidentApp = UserApplicationData.Lpr;
            }
            if (incidentObjectConverted.Code != 0)
            {
                incidentObjectConverted = SmartCadDatabase.RefreshObject(incidentObjectConverted) as IncidentData;                
            }
            else
            {
                incidentObjectConverted.Version = incidentClient.Version;
                incidentObjectConverted.CustomCode = incidentClient.CustomCode;
                incidentObjectConverted.Status = IncidentStatusConversion.ToObject(incidentClient.Status);
                incidentObjectConverted.SetReportBaseList = new HashedSet();
                incidentObjectConverted.StartDate = incidentClient.StartDate;
                incidentObjectConverted.EndDate = incidentClient.EndDate;
            }
            
            incidentObjectConverted.Address = new IncidentAddressData(AddressConversion.ToObject(incidentClient.Address));
            incidentObjectConverted.IsEmergency = incidentClient.IsEmergency;

            IList supportRequestReports = new ArrayList();
            foreach (ReportBaseData reportBase in incidentObjectConverted.SetReportBaseList)
            {
                if (reportBase is SupportRequestReportData)
                {
                    supportRequestReports.Add(reportBase as SupportRequestReportData);
                }
            }

            incidentObjectConverted.SetReportBaseList = new HashedSet();
            if (incidentClient.PhoneReports != null)
            {
                foreach (PhoneReportClientData phoneReportClient in incidentClient.PhoneReports)
                {
                    PhoneReportData phoneReport = PhoneReportConversion.ToObject(phoneReportClient);
                    phoneReport.Incident = incidentObjectConverted;
                    incidentObjectConverted.SetReportBaseList.Add(phoneReport);
                }
            }
            if (incidentClient.CctvReports != null)
            {
                foreach (CctvReportClientData cctvReportClient in incidentClient.CctvReports)
                {
                    CctvReportData cctvReport = CctvReportConversion.ToObject(cctvReportClient);
                    cctvReport.Incident = incidentObjectConverted;
                    incidentObjectConverted.SetReportBaseList.Add(cctvReport);
                }
            }
            if (incidentClient.AlarmReports != null)
            {
                if (incidentClient.AlarmReports[0] is AlarmReportClientData)
                {
                    foreach (AlarmReportClientData alarmReportClient in incidentClient.AlarmReports)
                    {
                        AlarmReportData alarmReport = AlarmReportConversion.ToObject(alarmReportClient);
                        alarmReport.Incident = incidentObjectConverted;
                        incidentObjectConverted.SetReportBaseList.Add(alarmReport);
                    }
                }
                else if (incidentClient.AlarmReports[0] is AlarmLprReportClientData)
                {
                    foreach (AlarmLprReportClientData alarmReportClient in incidentClient.AlarmReports)
                    {
                        AlarmLprReportData alarmReport = AlarmLprReportConversion.ToObject(alarmReportClient);
                        alarmReport.Incident = incidentObjectConverted;
                        incidentObjectConverted.SetReportBaseList.Add(alarmReport);
                    }
                }
            }
            if (incidentClient.TwitterReports != null)
            {
                foreach (TwitterReportClientData twitterReportClient in incidentClient.TwitterReports)
                {
                    TwitterReportData twitterReport = TwitterReportConversion.ToObject(twitterReportClient);
                    twitterReport.Incident = incidentObjectConverted;
                    incidentObjectConverted.SetReportBaseList.Add(twitterReport);
                }
            }
            if (incidentClient.LprReports != null)
            {
                foreach (AlarmLprReportClientData lprReportClient in incidentClient.LprReports)
                {
                    AlarmLprReportData lprReport = AlarmLprReportConversion.ToObject(lprReportClient);
                    lprReport.Incident = incidentObjectConverted;
                    incidentObjectConverted.SetReportBaseList.Add(lprReport);
                }
            }
            if (incidentObjectConverted.SetReportBaseList.Count > 1)
                CleanMultiplesIncidentTypes(incidentObjectConverted.SetReportBaseList);

            ((ISet)incidentObjectConverted.SetReportBaseList).AddAll(supportRequestReports);

            return incidentObjectConverted;
        }

        private static void CleanMultiplesIncidentTypes(ISet reportBaseList)
        {
            Dictionary<int, IncidentTypeData> globalIncidentTypes = new Dictionary<int, IncidentTypeData>();

            foreach (ReportBaseData rb in reportBaseList)
            {
                IList incidentTypeList = new ArrayList(rb.SetIncidentTypes);
                for (int i = 0; i < incidentTypeList.Count; i++)
                {
                    IncidentTypeData it = (IncidentTypeData)incidentTypeList[i];

                    if (globalIncidentTypes.ContainsKey(it.Code))
                    {
                        incidentTypeList[i] = globalIncidentTypes[it.Code];
                    }
                    else
                    {
                        globalIncidentTypes.Add(it.Code, it);
                    }
                }
                rb.SetIncidentTypes = new HashedSet(incidentTypeList);
            }
        }
    }
}
