//using System;
//using System.Collections.Generic;
//using System.Text;
//using SmartCadCore.ClientData;
//using SmartCadCore.Model;
//using System.Collections;

//namespace SmartCadCore.Core
//{
//    public class WorkShiftScheduleConversion
//    {
//        public static WorkShiftScheduleClientData ToClient(WorkShiftScheduleData workShiftSchedule, UserApplicationData application)
//        {
//            WorkShiftScheduleClientData workShiftScheduleClientData = new WorkShiftScheduleClientData();
//            workShiftScheduleClientData.Code = workShiftSchedule.Code;
//            workShiftScheduleClientData.Start = workShiftSchedule.Start;
//            workShiftScheduleClientData.End = workShiftSchedule.End;
//            workShiftScheduleClientData.Monday = workShiftSchedule.Monday;
//            workShiftScheduleClientData.Tuesday = workShiftSchedule.Tuesday;
//            workShiftScheduleClientData.Wednesday = workShiftSchedule.Wednesday;
//            workShiftScheduleClientData.Thursday = workShiftSchedule.Thursday;
//            workShiftScheduleClientData.Friday = workShiftSchedule.Friday;
//            workShiftScheduleClientData.Saturday = workShiftSchedule.Saturday;
//            workShiftScheduleClientData.Sunday = workShiftSchedule.Sunday;
//            workShiftScheduleClientData.Version = workShiftSchedule.Version;
            
//            workShiftScheduleClientData.WorkShift = new WorkShiftClientData();
//            workShiftScheduleClientData.WorkShift.Code = workShiftSchedule.WorkShift.Code;
//            workShiftScheduleClientData.WorkShift.Description = workShiftSchedule.WorkShift.Description;
//            workShiftScheduleClientData.WorkShift.Name = workShiftSchedule.WorkShift.Name;
//            return workShiftScheduleClientData;
//        }

//        public static WorkShiftScheduleData ToObject(WorkShiftScheduleClientData workShiftScheduleClientData, UserApplicationData application)
//        {
//            WorkShiftScheduleData workShiftSchedule = new WorkShiftScheduleData();
//            workShiftSchedule.Code = workShiftScheduleClientData.Code;
//            workShiftSchedule.Version = workShiftScheduleClientData.Version;
//            workShiftSchedule.Start = workShiftScheduleClientData.Start;
//            workShiftSchedule.End = workShiftScheduleClientData.End;
//            workShiftSchedule.Monday = workShiftScheduleClientData.Monday;
//            workShiftSchedule.Tuesday = workShiftScheduleClientData.Tuesday;
//            workShiftSchedule.Wednesday = workShiftScheduleClientData.Wednesday;
//            workShiftSchedule.Thursday = workShiftScheduleClientData.Thursday;
//            workShiftSchedule.Friday = workShiftScheduleClientData.Friday;
//            workShiftSchedule.Saturday = workShiftScheduleClientData.Saturday;
//            workShiftSchedule.Sunday = workShiftScheduleClientData.Sunday;
            
//            workShiftSchedule.WorkShift = new WorkShiftData();
//            workShiftSchedule.WorkShift.Code = workShiftScheduleClientData.WorkShift.Code;

//            return workShiftSchedule;
//        }

//    }
//}
