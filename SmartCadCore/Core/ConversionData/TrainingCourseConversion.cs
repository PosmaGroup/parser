using System;
using System.Collections;
using System.Text;
using SmartCadCore.Model;
using SmartCadCore.ClientData;


namespace SmartCadCore.Core
{
    public class TrainingCourseConversion
    {
        public static TrainingCourseClientData ToClient(TrainingCourseData trainingCourse, UserApplicationData application)
        {
            TrainingCourseClientData trainingCourseClient = new TrainingCourseClientData();
            trainingCourseClient.Code = trainingCourse.Code;
            trainingCourseClient.Name = trainingCourse.Name;
            trainingCourseClient.Content = trainingCourse.Content;
            trainingCourseClient.Objective = trainingCourse.Objective;
            trainingCourseClient.PhoneContactPerson = trainingCourse.PhoneContactPerson;
            trainingCourseClient.Link = trainingCourse.Link;
            trainingCourseClient.ContactPerson = trainingCourse.ContactPerson;
            trainingCourseClient.Version = trainingCourse.Version;
            trainingCourseClient.Schedules = new ArrayList();

            if (trainingCourse.Schedules != null && SmartCadDatabase.IsInitialize(trainingCourse.Schedules) == true)
            {
                foreach (TrainingCourseScheduleData schedule in trainingCourse.Schedules)
                {
                    TrainingCourseScheduleClientData trainigCourseSchedule = TrainingCourseScheduleConversion.ToClient(schedule, application);
                    trainigCourseSchedule.TrainingCourse = trainingCourseClient;
                    trainingCourseClient.Schedules.Add(trainigCourseSchedule);
                }
            }
            return trainingCourseClient;
        }

        public static TrainingCourseData ToObject(TrainingCourseClientData trainingCourseClient, UserApplicationData application)
        {
            TrainingCourseData trainingCourse = new TrainingCourseData();
            trainingCourse.Code = trainingCourseClient.Code;
            trainingCourse.Name = trainingCourseClient.Name;
            trainingCourse.Content = trainingCourseClient.Content;
            trainingCourse.Objective = trainingCourseClient.Objective;
            trainingCourse.PhoneContactPerson = trainingCourseClient.PhoneContactPerson;
            trainingCourse.Link = trainingCourseClient.Link;
            trainingCourse.ContactPerson = trainingCourseClient.ContactPerson;
            trainingCourse.Schedules = new ArrayList();
    
            if (trainingCourseClient.Schedules != null)
            {
                foreach (TrainingCourseScheduleClientData schedule in trainingCourseClient.Schedules)
                {
                    trainingCourse.Schedules.Add(TrainingCourseScheduleConversion.ToObject(schedule, application));
                }
            }

            trainingCourse.Version = trainingCourseClient.Version;


            return trainingCourse;
        
        }

    }
}