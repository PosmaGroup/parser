using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;

using SmartCadCore.ClientData;
using SmartCadCore.Model;

namespace SmartCadCore.Core
{
    class DepartmentStationAssignHistoryConversion
    {
        internal static DepartmentStationAssignHistoryClientData ToClient(DepartmentStationAssignHistoryData objectData, UserApplicationData app)
        {
            DepartmentStationAssignHistoryClientData departmentStationAssignHistoryClientData = new DepartmentStationAssignHistoryClientData();
            departmentStationAssignHistoryClientData.Code = objectData.Code;
            departmentStationAssignHistoryClientData.Version = objectData.Version;
            if (objectData.DepartmentStation != null)
            {
                departmentStationAssignHistoryClientData.DepartmentStation = DepartmentStationConversion.ToClient(objectData.DepartmentStation, app);                
            }
            departmentStationAssignHistoryClientData.StartDate = objectData.Start.HasValue ? objectData.Start.Value : DateTime.MinValue;
            departmentStationAssignHistoryClientData.EndDate = objectData.End.HasValue ? objectData.End.Value : DateTime.MinValue;
            if (app.Name == UserApplicationData.Dispatch.Name)
            {
                #region Dispatch
                departmentStationAssignHistoryClientData.UnitOfficerAssign = null;
                ArrayList totalDepartmentUnitsCustomCodes = new ArrayList(SmartCadDatabase.SearchBasicObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetUnitCustomCodesByDepartmentStationCode, objectData.DepartmentStation.Code)) as IList);
                foreach (string unitCustomCode in totalDepartmentUnitsCustomCodes)
                {
                    departmentStationAssignHistoryClientData.Units.Add(unitCustomCode, new List<OfficerClientData>());
                }

                if (SmartCadDatabase.IsInitialize(objectData.UnitOfficerAssign) == true)
                {
                    //SmartCadDatabase.InitializeLazy(objectData, objectData.UnitOfficerAssign);

                    foreach (UnitOfficerAssignHistoryData uoahd in objectData.UnitOfficerAssign)
                    {
                        if (departmentStationAssignHistoryClientData.Units.ContainsKey(uoahd.Unit.CustomCode) == true)
                        {
                            departmentStationAssignHistoryClientData.Units[uoahd.Unit.CustomCode].Add((OfficerClientData)Conversion.ToClient(uoahd.Officer, app));
                        }
                    }
                }
                #endregion
            }
            else if (app.Name == UserApplicationData.Administration.Name)
            {
                #region Administration
                if (objectData.InitializeCollections == true)
                {
                    departmentStationAssignHistoryClientData.UnitOfficerAssign = new ArrayList();

                    if (SmartCadDatabase.IsInitialize(objectData.UnitOfficerAssign) == false)
                        SmartCadDatabase.InitializeLazy(objectData, objectData.UnitOfficerAssign);

                    foreach (UnitOfficerAssignHistoryData uoahd in objectData.UnitOfficerAssign)
                    {
                        departmentStationAssignHistoryClientData.UnitOfficerAssign.Add(UnitOfficerAssignHistoryConversion.ToClient(uoahd, null));
                    }
                }
                #endregion
            }
            return departmentStationAssignHistoryClientData;
        }

        internal static DepartmentStationAssignHistoryData ToObject(DepartmentStationAssignHistoryClientData clientData, UserApplicationData app)
        {
            DepartmentStationAssignHistoryData data = new DepartmentStationAssignHistoryData();
            data.Code = clientData.Code;
            if(data.Code !=0)
                SmartCadDatabase.RefreshObject(data);

            data.DepartmentStation = new DepartmentStationData();
            data.DepartmentStation.Code = clientData.DepartmentStation.Code;
            data.DepartmentStation = (DepartmentStationData)SmartCadDatabase.RefreshObject(data.DepartmentStation);

            data.End = clientData.EndDate;
            data.Start = clientData.StartDate;

            if (clientData.UnitOfficerAssign != null)
            {
                data.UnitOfficerAssign = new ArrayList();
                foreach (UnitOfficerAssignHistoryClientData unitOfficer in clientData.UnitOfficerAssign)
                {
                    data.UnitOfficerAssign.Add(UnitOfficerAssignHistoryConversion.ToObject(unitOfficer,app));
                }
            }
            return data;
        }
    }
}
