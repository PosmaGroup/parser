using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.Model;
using SmartCadCore.ClientData;
using System.Collections;
using Iesi.Collections;

namespace SmartCadCore.Core
{
    public class DeviceConversion
    {
        public static DeviceClientData ToClient(DeviceData deviceData, UserApplicationData app)
        {
            DeviceClientData deviceClientData = null;
            if (deviceData is VideoDeviceData)
            {
                #region Camera Datalogger or Lpr
                if (deviceData is CameraData)
                {
                    #region Camera
                    deviceClientData = new CameraClientData();
                    (deviceClientData as CameraClientData).CustomId = (deviceData as CameraData).CustomId;
                    (deviceClientData as CameraClientData).Resolution = (deviceData as CameraData).Resolution;
                    (deviceClientData as CameraClientData).StreamType = (deviceData as CameraData).StreamType;
                    (deviceClientData as CameraClientData).FrameRate = (deviceData as CameraData).FrameRate;
                    (deviceClientData as CameraClientData).Type =
                        CameraTypeConversion.ToClient((deviceData as CameraData).Type, app);
                    (deviceClientData as CameraClientData).ConnectionType =
                        ConnectionTypeConversion.ToClient((deviceData as CameraData).ConnectionType, app);
                    #endregion
                }
                else if (deviceData is DataloggerData)
                {
                    #region Datalogger
                    deviceClientData = new DataloggerClientData();
                    (deviceClientData as DataloggerClientData).SensorTelemetrys = new List<SensorTelemetryClientData>();

                    (deviceClientData as DataloggerClientData).Name = (deviceData as DataloggerData).Name;
                    (deviceClientData as DataloggerClientData).CustomCode = (deviceData as DataloggerData).CustomCode;
                    (deviceClientData as DataloggerClientData).Ip = (deviceData as DataloggerData).Ip;
                    (deviceClientData as DataloggerClientData).IpServer = (deviceData as DataloggerData).IpServer;
                    (deviceClientData as DataloggerClientData).Login = (deviceData as DataloggerData).Login;
                    (deviceClientData as DataloggerClientData).Password = (deviceData as DataloggerData).Password;
                    (deviceClientData as DataloggerClientData).Port = (deviceData as DataloggerData).Port;
                    //(deviceClientData as DataloggerClientData).StructClientData = (deviceData as DataloggerData).Struct;
                    (deviceClientData as DataloggerClientData).Code = (deviceData as DataloggerData).Code;
                    (deviceClientData as DataloggerClientData).Version = (deviceData as DataloggerData).Version;

                  

                    IList sensors = SmartCadDatabase.SearchObjects(
                        SmartCadHqls.GetCustomHql(SmartCadHqls.GetSensorTelemetrysByDataloggerName, (deviceData as DataloggerData).Name));


                    if (SmartCadDatabase.IsInitialize((deviceData as DataloggerData).SensorTelemetrys) == false)
                        SmartCadDatabase.InitializeLazy(deviceData as DataloggerData, (deviceData as DataloggerData).SensorTelemetrys);
                    if (SmartCadDatabase.IsInitialize((deviceData as DataloggerData).SensorTelemetrys) == true)
                    {
                        foreach (SensorTelemetryData sensorData in sensors)
                        {
                            SensorTelemetryClientData sensorClientData = new SensorTelemetryClientData();
                            sensorClientData.Threshold = new List<SensorTelemetryThresholdClientData>();

                            if (SmartCadDatabase.IsInitialize((sensorData as SensorTelemetryData).Thresholds) == false)
                                SmartCadDatabase.InitializeLazy((sensorData as SensorTelemetryData), (sensorData as SensorTelemetryData).Thresholds);
                            foreach(SensorTelemetryThresholdData threshold in sensorData.Thresholds)
                            {
                                SensorTelemetryThresholdClientData thresholdClient = new SensorTelemetryThresholdClientData()
                                {
                                    Code = threshold.Code,
                                    Version = threshold.Version,
                                    SensorTelemetry = sensorClientData,
                                    High = threshold.High,
                                    Low = threshold.Low
                                };
                                sensorClientData.Threshold.Add(thresholdClient);
                            }

                            sensorClientData.Code = sensorData.Code;
                            sensorClientData.Version = sensorData.Version;
                            sensorClientData.Name = sensorData.Name;
                            sensorClientData.Variable = sensorData.Variable;
                            sensorClientData.Datalogger = (deviceClientData as DataloggerClientData);

                            (deviceClientData as DataloggerClientData).SensorTelemetrys.Add(sensorClientData);
                        }
                    }
                    #endregion
                }
                else if (deviceData is LprData)
                {
                    #region Lpr
                    deviceClientData = new LprClientData();
                    (deviceClientData as LprClientData).Serial = (deviceData as LprData).Serial;
                  
                    (deviceClientData as LprClientData).Type =
                       LprTypeConversion.ToClient((deviceData as LprData).Type, app);
                    (deviceClientData as LprClientData).Way =
                      LprWayConversion.ToClient((deviceData as LprData).Way, app);
                    #endregion
                }
                deviceClientData.Code = deviceData.Code;
                deviceClientData.Version = deviceData.Version;
                deviceClientData.Name = deviceData.Name;
                deviceClientData.Ip = deviceData.Ip;
                deviceClientData.Port = deviceData.Port;
                deviceClientData.IpServer = deviceData.IpServer;
                deviceClientData.Login = deviceData.Login;
                deviceClientData.Password = deviceData.Password;


                // if (deviceData is VideoDeviceData)
                //{
                if ((deviceData as VideoDeviceData).Struct != null)
                {
                    (deviceClientData as VideoDeviceClientData).StructClientData = new StructClientData();
                    (deviceClientData as VideoDeviceClientData).StructClientData.Code = (deviceData as VideoDeviceData).Struct.Code;
                    (deviceClientData as VideoDeviceClientData).StructClientData.Name = (deviceData as VideoDeviceData).Struct.Name;
                    (deviceClientData as VideoDeviceClientData).StructClientData.Type =
                        StructTypeConversion.ToClient((deviceData as VideoDeviceData).Struct.Type, app);
                    if ((deviceData as VideoDeviceData).Struct.Zone != null)
                    {
                        (deviceData as VideoDeviceData).Struct.Zone.Structs = null;
                        (deviceClientData as VideoDeviceClientData).StructClientData.CctvZone =
                            CctvZoneConversion.ToClient((deviceData as VideoDeviceData).Struct.Zone, app);

                        //if (deviceData is DataloggerData)
                        {
                            (deviceClientData as VideoDeviceClientData).StructClientData.State =
                                (deviceData as VideoDeviceData).Struct.Addres.State;
                            (deviceClientData as VideoDeviceClientData).StructClientData.Street =
                                (deviceData as VideoDeviceData).Struct.Addres.Street;
                            (deviceClientData as VideoDeviceClientData).StructClientData.Town =
                                (deviceData as VideoDeviceData).Struct.Addres.Town;
                            (deviceClientData as VideoDeviceClientData).StructClientData.Lat =
                                (deviceData as VideoDeviceData).Struct.Addres.Lat;
                            (deviceClientData as VideoDeviceClientData).StructClientData.Lon =
                                (deviceData as VideoDeviceData).Struct.Addres.Lon;
                            (deviceClientData as VideoDeviceClientData).StructClientData.ZoneSecRef =
                               (deviceData as VideoDeviceData).Struct.Addres.Reference;
                        }
                    }
                }
                #endregion
            }
            else if (deviceData is GPSData)
            {
                #region GPS
                deviceClientData = new GPSClientData();
                (deviceClientData as GPSClientData).Code = deviceData.Code;
                (deviceClientData as GPSClientData).Type = GPSTypeConversion.ToClient((deviceData as GPSData).Type, app);
                (deviceClientData as GPSClientData).Name = deviceData.Name;
                (deviceClientData as GPSClientData).Alt = (deviceData as GPSData).Altitude;
                (deviceClientData as GPSClientData).Lon = (deviceData as GPSData).Longitude;
                (deviceClientData as GPSClientData).Lat = (deviceData as GPSData).Latitude;
                (deviceClientData as GPSClientData).Satellites = (deviceData as GPSData).Satellites;
                (deviceClientData as GPSClientData).Speed = (deviceData as GPSData).Speed;
                (deviceClientData as GPSClientData).CoordinatesDate = (deviceData as GPSData).CoordinatesDate;
                (deviceClientData as GPSClientData).Heading = (deviceData as GPSData).Heading;
                (deviceClientData as GPSClientData).IsSynchronized = (deviceData as GPSData).IsSynchronized;
                if ((deviceData as GPSData).Unit != null)
                    (deviceClientData as GPSClientData).UnitCode = (deviceData as GPSData).Unit.Code;
                if (SmartCadDatabase.IsInitialize((deviceData as GPSData).Sensors) == false)
                    SmartCadDatabase.InitializeLazy(deviceData as GPSData, (deviceData as GPSData).Sensors);
                if (SmartCadDatabase.IsInitialize((deviceData as GPSData).Sensors) == true)
                {
                    (deviceClientData as GPSClientData).Sensors = new ArrayList();
                    foreach (GPSPinSensorData sensor in (deviceData as GPSData).Sensors)
                    {
						(deviceClientData as GPSClientData).Sensors.Add(GPSPinSensorConversion.ToClient(sensor, app));
                    }

                }

                (deviceClientData as GPSClientData).Version = deviceData.Version;
                #endregion
            }
            return deviceClientData;
        }

        public static DeviceData ToObject(DeviceClientData deviceClientData, UserApplicationData app)
        {
            DeviceData deviceData = null;
            if (deviceClientData is VideoDeviceClientData)
            {
                if (deviceClientData is CameraClientData)
                {
                    deviceData = new CameraData();

                    deviceData.Code = deviceClientData.Code;
                    if (deviceData.Code != 0)
                        deviceData = (CameraData)SmartCadDatabase.RefreshObject(deviceData);

                    (deviceData as CameraData).CustomId = (deviceClientData as CameraClientData).CustomId;
                    (deviceData as CameraData).Resolution = (deviceClientData as CameraClientData).Resolution;
                    (deviceData as CameraData).StreamType = (deviceClientData as CameraClientData).StreamType;
                    (deviceData as CameraData).FrameRate = (deviceClientData as CameraClientData).FrameRate;

                    (deviceData as CameraData).Type = new CameraTypeData();
                    (deviceData as CameraData).Type.Code = (deviceClientData as CameraClientData).Type.Code;
                    (deviceData as CameraData).Type = (CameraTypeData)SmartCadDatabase.RefreshObject((deviceData as CameraData).Type);

                    (deviceData as CameraData).ConnectionType = new ConnectionTypeData();
                    (deviceData as CameraData).ConnectionType.Code = (deviceClientData as CameraClientData).ConnectionType.Code;
                    (deviceData as CameraData).ConnectionType =
                        (ConnectionTypeData)SmartCadDatabase.RefreshObject((deviceData as CameraData).ConnectionType);

                }
                else if (deviceClientData is DataloggerClientData)
                {
                    deviceData = new DataloggerData();

                    deviceData.Code = deviceClientData.Code;
                    if (deviceData.Code != 0)
                    {
                        deviceData = (DataloggerData)SmartCadDatabase.RefreshObject(deviceData);
                    }

                    (deviceData as DataloggerData).Name = (deviceClientData as DataloggerClientData).Name;
                    (deviceData as DataloggerData).CustomCode = (deviceClientData as DataloggerClientData).CustomCode;
                    //(deviceData as DataloggerData).Ip = (deviceClientData as DataloggerClientData).Ip;
                    //(deviceData as DataloggerData).IpServer = (deviceClientData as DataloggerClientData).IpServer;
                    //(deviceData as DataloggerData).Login = (deviceClientData as DataloggerClientData).Login;
                    //(deviceData as DataloggerData).Password = (deviceClientData as DataloggerClientData).Password;
                    //(deviceData as DataloggerData).Port = (deviceClientData as DataloggerClientData).Port;
                    (deviceData as DataloggerData).Code = (deviceClientData as DataloggerClientData).Code;
                    (deviceData as DataloggerData).Version = (deviceClientData as DataloggerClientData).Version;

                    (deviceData as DataloggerData).SensorTelemetrys = new HashedSet();

                    if ((deviceClientData as VideoDeviceClientData).StructClientData != null)
                    {
                        (deviceData as VideoDeviceData).Struct = new StructData();
                        (deviceData as VideoDeviceData).Struct.Code = (deviceClientData as VideoDeviceClientData).StructClientData.Code;
                        (deviceData as VideoDeviceData).Struct.Name = (deviceClientData as VideoDeviceClientData).StructClientData.Name;
                        (deviceData as VideoDeviceData).Struct = (StructData)SmartCadDatabase.RefreshObject((deviceData as VideoDeviceData).Struct);
                    }
                    else
                        (deviceData as VideoDeviceData).Struct = null;

                    if ((deviceClientData as DataloggerClientData).SensorTelemetrys != null)
                    {
                        IList dataloggerListDB = SmartCadDatabase.SearchObjects(SmartCadHqls.GetAllDataloggers);
                        List<DataloggerData> dataLoggerList = new List<DataloggerData>();
                        bool dataloggerExist = false;
                        DataloggerData dataloggerDataBD = new DataloggerData();

                        //foreach (DataloggerData sensorClientData2 in dataloggerListDB)
                        //{
                        //    dataLoggerList.Add(sensorClientData2 as DataloggerData);
                        //}


                        foreach (DataloggerData datalogger in dataloggerListDB)
                        {
                            if (datalogger.Name == deviceData.Name)
                            {
                                dataloggerDataBD = datalogger;
                                dataloggerExist = true;
                                break;
                            }
                        }
                        if (dataloggerExist == true)
                        {
                            foreach (SensorTelemetryClientData sensorClientData in (deviceClientData as DataloggerClientData).SensorTelemetrys)
                            {
                                SensorTelemetryData sensorData = new SensorTelemetryData();
                                bool found = false;

                                foreach (SensorTelemetryData sensorsDB in dataloggerDataBD.SensorTelemetrys)
                                {
                                    if (sensorsDB.Name == sensorClientData.Name)
                                    {
                                        found = true;
                                        sensorData.ValueDate = sensorsDB.ValueDate;
                                    }
                                }

                                if (found == false) sensorData.ValueDate = DateTime.Now;
                                sensorData.Code = sensorClientData.Code;
                                sensorData.Version = sensorClientData.Version;
                                sensorData.Name = sensorClientData.Name;
                                sensorData.Datalogger = (deviceData as DataloggerData);
                                sensorData.Variable = sensorClientData.Variable;

                                sensorData.Thresholds = new HashedSet();
                                foreach (SensorTelemetryThresholdClientData thresholdClient in sensorClientData.Threshold)
                                {
                                    SensorTelemetryThresholdData threshold = new SensorTelemetryThresholdData()
                                    {
                                        Code = thresholdClient.Code,
                                        Version = thresholdClient.Version,
                                        Sensor = sensorData,
                                        High = thresholdClient.High,
                                        Low = thresholdClient.Low
                                    };
                                    sensorData.Thresholds.Add(threshold);
                                }
                                //if (SmartCadDatabase.IsInitialize((deviceData as DataloggerData).SensorTelemetrys) == false)
                                //    SmartCadDatabase.InitializeLazy(deviceData as DataloggerData, (deviceData as DataloggerData).SensorTelemetrys);
                                //if (SmartCadDatabase.IsInitialize((deviceData as DataloggerData).SensorTelemetrys) == true)
                                //{
                                //    (deviceData as DataloggerData).SensorTelemetrys.Add(sensorData);
                                //}

                                (deviceData as DataloggerData).SensorTelemetrys.Add(sensorData);
                            }

                            //  break;
                        }
                        else
                        {
                            foreach (SensorTelemetryClientData sensorClientData in (deviceClientData as DataloggerClientData).SensorTelemetrys)
                            {
                                SensorTelemetryData sensorData = new SensorTelemetryData();

                                sensorData.ValueDate = DateTime.Now;
                                sensorData.Code = sensorClientData.Code;
                                sensorData.Version = sensorClientData.Version;
                                sensorData.Name = sensorClientData.Name;
                                sensorData.Datalogger = (deviceData as DataloggerData);
                                sensorData.Variable = sensorClientData.Variable;

                                sensorData.Thresholds = new HashedSet();
                                foreach (SensorTelemetryThresholdClientData thresholdClient in sensorClientData.Threshold)
                                {
                                    SensorTelemetryThresholdData threshold = new SensorTelemetryThresholdData()
                                    {
                                        Code = thresholdClient.Code,
                                        Version = thresholdClient.Version,
                                        Sensor = sensorData,
                                        High = thresholdClient.High,
                                        Low = thresholdClient.Low
                                    };
                                    sensorData.Thresholds.Add(threshold);
                                }

                                (deviceData as DataloggerData).SensorTelemetrys.Add(sensorData);
                            }
                            //  break;
                        }

                   }
                    return deviceData;
                }
                else if (deviceClientData is LprClientData)
                {
                    deviceData = new LprData();
                    deviceData.Code = deviceClientData.Code;
                    if (deviceData.Code != 0)
                        deviceData = (LprData)SmartCadDatabase.RefreshObject(deviceData);

                    (deviceData as LprData).Serial = (deviceClientData as LprClientData).Serial;
                   
                    (deviceData as LprData).Type = new LprTypeData();
                    (deviceData as LprData).Type.Code = (deviceClientData as LprClientData).Type.Code;
                    (deviceData as LprData).Type = (LprTypeData)SmartCadDatabase.RefreshObject((deviceData as LprData).Type);
                    (deviceData as LprData).Way = new LprWayData();
                    (deviceData as LprData).Way.Code = (deviceClientData as LprClientData).Way.Code;
                    (deviceData as LprData).Way = (LprWayData)SmartCadDatabase.RefreshObject((deviceData as LprData).Way);
                }


                deviceData.Version = deviceClientData.Version;
                deviceData.Name = deviceClientData.Name;
                deviceData.Ip = deviceClientData.Ip;
                deviceData.Port = deviceClientData.Port;
                deviceData.IpServer = deviceClientData.IpServer;
                deviceData.Login = deviceClientData.Login;
                deviceData.Password = deviceClientData.Password;

                //if (deviceClientData is VideoDeviceClientData)
                //{

                if ((deviceClientData as VideoDeviceClientData).StructClientData != null)
                {
                    (deviceData as VideoDeviceData).Struct = new StructData();
                    (deviceData as VideoDeviceData).Struct.Code = (deviceClientData as VideoDeviceClientData).StructClientData.Code;
                    (deviceData as VideoDeviceData).Struct.Name = (deviceClientData as VideoDeviceClientData).StructClientData.Name;
                    (deviceData as VideoDeviceData).Struct = (StructData)SmartCadDatabase.RefreshObject((deviceData as VideoDeviceData).Struct);
                }
                else
                    (deviceData as VideoDeviceData).Struct = null;
            }
            else if (deviceClientData is GPSClientData)
            {
                deviceData = new GPSData();
                deviceData.Code = deviceClientData.Code;
                if (deviceData.Code != 0)
                    deviceData = (GPSData)SmartCadDatabase.RefreshObject(deviceData);
                else
                    (deviceData as GPSData).CoordinatesDate = DateTime.Now;

                 deviceData.Name = (deviceClientData as GPSClientData).Name;
                (deviceData as GPSData).Type = GPSTypeConversion.ToObject((deviceClientData as GPSClientData).Type, app);
                
                if ((deviceClientData as GPSClientData).Sensors != null)
                {
                    (deviceData as GPSData).Sensors = new ArrayList();
                    foreach (GPSPinSensorClientData sensor in (deviceClientData as GPSClientData).Sensors)
                    {
						GPSPinSensorData sensorData = GPSPinSensorConversion.ToObject(sensor, app);
                        sensorData.GPS = (deviceData as GPSData);
                        (deviceData as GPSData).Sensors.Add(sensorData);
                    }
                }
                deviceData.Version = deviceClientData.Version;

            }
            return deviceData;
        }

        public static DataloggerClientData ConvertToDataloggerClientData(DataloggerData dataloggerData)
        {
            DataloggerClientData dataloggerClientData = new DataloggerClientData();

            (dataloggerClientData as DataloggerClientData).Name = (dataloggerData as DataloggerData).Name;
            (dataloggerClientData as DataloggerClientData).CustomCode = (dataloggerData as DataloggerData).CustomCode;
            (dataloggerClientData as DataloggerClientData).Code = (dataloggerData as DataloggerData).Code;
            (dataloggerClientData as DataloggerClientData).Version = (dataloggerData as DataloggerData).Version;
            (dataloggerClientData as DataloggerClientData).SensorTelemetrys = new List<SensorTelemetryClientData>();



            foreach (SensorTelemetryData sensorsToClientData in (dataloggerData as DataloggerData).SensorTelemetrys)
            {
                SensorTelemetryClientData sensorData = new SensorTelemetryClientData();
                sensorData.Code = sensorsToClientData.Code;
                sensorData.Version = sensorsToClientData.Version;
                sensorData.Name = sensorsToClientData.Name;
                sensorData.Datalogger = (dataloggerClientData as DataloggerClientData);
                sensorData.Variable = sensorsToClientData.Variable;
                sensorData.ValueDate = sensorsToClientData.ValueDate;

                sensorData.Threshold = new List<SensorTelemetryThresholdClientData>();

                foreach (SensorTelemetryThresholdData thresholdClient in sensorsToClientData.Thresholds)
                {
                    SensorTelemetryThresholdClientData threshold = new SensorTelemetryThresholdClientData()
                    {
                        Code = thresholdClient.Code,
                        Version = thresholdClient.Version,
                        SensorTelemetry = sensorData,
                        High = thresholdClient.High,
                        Low = thresholdClient.Low
                    };
                    sensorData.Threshold.Add(threshold);
                }

                (dataloggerClientData as DataloggerClientData).SensorTelemetrys.Add(sensorData);
            }

            return dataloggerClientData;
        }

    }
}