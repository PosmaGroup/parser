﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Model;

namespace SmartCadCore.Core
{
    public class QuestionDispatchReportConversion
    {
        public static QuestionDispatchReportClientData ToClient(QuestionDispatchReportData question, UserApplicationData app)
        {
            QuestionDispatchReportClientData client = new QuestionDispatchReportClientData();
            
            client.Code = question.Code;
            client.Version = question.Version;
            client.Render = question.Render;
            client.Text = question.Text;
            client.Required = question.Required;

            return client;
        }

        public static QuestionDispatchReportData ToObject(QuestionDispatchReportClientData client, UserApplicationData app)
        {
            QuestionDispatchReportData data = new QuestionDispatchReportData();

            data.Code = client.Code;
            data.Version = client.Version;
            data.Text = client.Text;
            data.Render = client.Render;
            data.Required = client.Required;

            return data;
        }

    }
}
