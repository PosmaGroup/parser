using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.ClientData;
using SmartCadCore.Model;

namespace SmartCadCore.Core
{
    public class SessionStatusHistoryConversion
    {
        public static SessionStatusHistoryClientData ToClient(SessionStatusHistoryData sessionStatusHistoryData, UserApplicationData app)
        {
            SessionStatusHistoryClientData statusHistory = new SessionStatusHistoryClientData();
            statusHistory.Code = sessionStatusHistoryData.Code;
            statusHistory.SessionHistoryCode = sessionStatusHistoryData.Session.Code;
            statusHistory.StartDateStatus = sessionStatusHistoryData.StartDate.HasValue? sessionStatusHistoryData.StartDate.Value : DateTime.MinValue;
            statusHistory.EndDateStatus = sessionStatusHistoryData.EndDate.HasValue ? sessionStatusHistoryData.EndDate.Value : DateTime.MinValue;
            statusHistory.StatusFriendlyName = sessionStatusHistoryData.Status.FriendlyName;
            statusHistory.StatusNotReady = sessionStatusHistoryData.Status.NotReady.Value;
            statusHistory.StatusName = sessionStatusHistoryData.Status.Name;
            statusHistory.Version = sessionStatusHistoryData.Version;
            return statusHistory;
        }
    }
}
