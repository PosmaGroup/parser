using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace SmartCadCore.Core
{
    [Serializable]
    public class SelectedIncidentTaskResult
    {
        #region Fields
        private string xml;
        private string sourceApplication;
        private IList totalDispatchOrders;
        private IList totalPhoneReports;
        #endregion

        #region Properties
        public string Xml
        {
            get
            {
                return xml;
            }
            set
            {
                xml = value;
            }
        }

        public IList TotalDispatchOrders
        {
            get
            {
                return totalDispatchOrders;
            }
            set
            {
                totalDispatchOrders = value;
            }
        }

        public IList TotalPhoneReports
        {
            get
            {
                return totalPhoneReports;
            }
            set
            {
                totalPhoneReports = value;
            }
        }

        public string SourceApplication
        {
            get
            {
                return sourceApplication;
            }
            set
            {
                sourceApplication = value;
            }
        }
        #endregion
    }
}
