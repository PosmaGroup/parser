using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Windows.Forms;
using System.Reflection;
using System.ComponentModel;
using System.Drawing;

namespace SmartCadCore.Core
{
    public class SkinEngine
    {
        public SkinEngine(XmlDocument xmlDocument)
        {
            guiElements = new Dictionary<string, List<Control>>();

            Load(xmlDocument);
        }

        private Dictionary<string, Dictionary<string, string>> skinValues;

        private Dictionary<string, List<Control>> guiElements;

        private Dictionary<string, Color> colors;

        public Dictionary<string, Color> Colors
        {
            get
            {
                return colors;
            }
        }

        public static SkinEngine Load(string skinFile)
        {
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(skinFile);

            return new SkinEngine(xmlDocument);
        }

        public void Load(XmlDocument xmlDocument)
        {
            TypeConverter colorTypeConverter = TypeDescriptor.GetConverter(typeof(Color));

            skinValues = new Dictionary<string, Dictionary<string, string>>();
            colors = new Dictionary<string, Color>();

            XmlElement rootElement = xmlDocument.DocumentElement;

            if (rootElement.Name == "skin")
            {
                foreach (XmlNode xmlKey in rootElement.ChildNodes)
                {
                    if (xmlKey.Name == "set")
                    {
                        string setName = xmlKey.Attributes["name"].Value;

                        Dictionary<string, string> paramValues = new Dictionary<string, string>();

                        foreach (XmlNode xmlParam in xmlKey.ChildNodes)
                        {
                            if (xmlParam.Name == "param")
                            {
                                string param = xmlParam.Attributes["name"].Value;
                                string value = xmlParam.Attributes["value"].Value;

                                paramValues.Add(param, value);
                            }
                        }

                        skinValues.Add(setName, paramValues);
                    }
                    else if (xmlKey.Name == "color")
                    {
                        string name = xmlKey.Attributes["name"].Value;
                        string value = xmlKey.Attributes["value"].Value;
                        
                        //Dictionary<string, Color> colorstest = new Dictionary<string, Color>();

                        //ITypeDescriptorContext context = ;
                        //System.Globalization.CultureInfo culture;
                        

                        colors.Add(name, (Color)colorTypeConverter.ConvertFromInvariantString(value));
                        //try
                        //{
                        //    colors.Add(name, (Color)colorTypeConverter.ConvertFromString(value));
                        //}
                        //catch (Exception ex)
                        //{
                        //    ex = null;
                        //}
                        //colors.Add(name, (Color)colorTypeConverter.ConvertFrom(context,culture,value));
                        
                    }
                }
            }
        }

        public void ApplyChanges(Dictionary<string, string> paramValues, Control control)
        {
            Type controlType = control.GetType();

            foreach (string param in paramValues.Keys)
            {
                PropertyInfo propertyInfo = controlType.GetProperty(param);

                if (propertyInfo != null)
                {
                    Type propertyType = propertyInfo.PropertyType;
                    Type valueType = null;

                    if ((propertyType.IsClass == true) && ((valueType = Type.GetType(paramValues[param])) != null))
                    {
                        ConstructorInfo constructorInfo = valueType.GetConstructor(new Type[] { });
                        object newValue = constructorInfo.Invoke(new object[] { });
                        propertyInfo.SetValue(control, newValue, null);
                    }
                    else
                    {
                        TypeConverter typeConverter = TypeDescriptor.GetConverter(propertyType);

                        if (typeConverter != null)
                        {    
                            //object newValue = typeConverter.ConvertFromString(paramValues[param]);
                            object newValue = typeConverter.ConvertFromInvariantString(paramValues[param]);
                            propertyInfo.SetValue(control, newValue, null);
                        }
                    }
                }
            }
        }

        public void ApplyChanges(string set, Control control)
        {
            if (skinValues.ContainsKey(set))
            {
                Dictionary<string, string> paramValues = skinValues[set];

                ApplyChanges(paramValues, control);
            }
        }

        public void ApplyChanges(Dictionary<string, List<Control>> guiElements)
        {
            foreach (string set in guiElements.Keys)
            {
                if (skinValues.ContainsKey(set))
                {
                    List<Control> controls = guiElements[set];
                    Dictionary<string, string> paramValues = skinValues[set];

                    foreach (Control control in controls)
                    {
                        ApplyChanges(paramValues, control);
                    }
                }
            }
        }

        public void AddElement(string setName, Control control)
        {
            List<Control> controls;

            if (guiElements.ContainsKey(setName))
            {
                controls = guiElements[setName];
            }
            else
            {
                controls = new List<Control>();
                guiElements.Add(setName, controls);
            }

            controls.Add(control);
        }

        private void AddCompleteElement(string prefixName, Control control)
        {
            string setName = prefixName + "." + control.Name;

            AddElement(setName, control);

            foreach (Control child in control.Controls)
            {
                AddCompleteElement(setName, child);
            }
        }

        public void AddCompleteElement(Control control)
        {
            string setName = control.Name;

            AddElement(setName, control);

            foreach (Control child in control.Controls)
            {
                AddCompleteElement(setName, child);
            }
        }

        public void ApplyChanges()
        {
            ApplyChanges(this.guiElements);
        }
    }
}
