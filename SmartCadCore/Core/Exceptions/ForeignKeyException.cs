using System;
using System.Collections.Generic;
using System.Text;

namespace SmartCadCore.Core
{
    public class ForeingKeyException : ApplicationException
    {
        public ForeingKeyException()
            : base()
        {
        }

        public ForeingKeyException(string message)
            : base(message)
        {
        }

        public ForeingKeyException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}