using System;
using System.Collections.Generic;
using System.Text;

namespace SmartCadCore.Core
{
    public class SecurityException : ApplicationException
    {
        public SecurityException(string message)
            : base(message)
        {
        }
    }
}
