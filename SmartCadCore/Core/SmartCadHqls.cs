using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using SmartCadCore.Model;
using NHibernate;
using System.Reflection;
using System.Collections;
using Iesi.Collections;
using NHibernate.Criterion;
using NHibernate.Transform;

namespace SmartCadCore.Core
{
    public static class SmartCadHqls
    {
        public const string DeleteIncidentsOlderThan = @"Delete FROM IncidentData incident
                                                        WHERE incident.EndDate < '{0}'";

        public const string GetIncidentsOlderThan = @"Select incident FROM IncidentData incident
                                                        left join fetch incident.SetReportBaseList report
                                                        WHERE incident.EndDate < '{0}'";
        public const string GetINDepartmentStationHistoryDataByIncidentNotification = @"Select data FROM IncidentNotificationDepartmentStationHistoryData data
                                                        WHERE data.IncidentNotification.Code = {0}"; 
        

        public const string GetVariationMotives = @"SELECT vm
                                                    FROM MotiveVariationData vm";

        public const string GetCountGPSByName = @"SELECT count(gps.Code) FROM GPSData gps WHERE gps.Code != {0} AND gps.Name = '{1}'";
        public const string GetGPSWithoutUnit = @"SELECT gps 
                                                FROM GPSData gps 
                                                WHERE gps.Code not in 
                                                    (SELECT unit.GPS.Code 
                                                        FROM UnitData unit 
                                                        WHERE unit.GPS is not null AND unit.Code != {0}) 
                                                order by gps.Name";

        public const string GetVehicleRequestByPlate = @"SELECT request FROM VehicleRequestData request
                                                WHERE request.Plate = '{0}'";

        public const string GetLprAlarmsByStatus = @"SELECT alarm FROM AlarmLprData alarm
                                                WHERE alarm.AlarmStatus = '{0}' and alarm.DeletedId is null";

        public const string GetOperatorStatus = @"SELECT opsd
                                                    FROM OperatorStatusData opsd
                                                  ORDER BY opsd.Order";

        public const string GetOperatorStatusByFriendlyName = @"SELECT opsd
                                                    FROM OperatorStatusData opsd
                                                    WHERE opsd.FriendlyName = '{0}'
                                                    ORDER BY opsd.Order";

        public const string GetOperatorStatusByCustomCode = @"SELECT opsd
                                                    FROM OperatorStatusData opsd
                                                    WHERE opsd.CustomCode = '{0}'";

		public const string GetEvaluations = @"SELECT eval
                                                    FROM EvaluationData eval
                                                    ORDER BY eval.Name";

        public const string GetCamerasTemplates = @"SELECT template
                                                    FROM CamerasTemplateData template
                                                    ORDER BY template.Name";

        public const string GetDepartmentTypeCounter = @"SELECT counter FROM DepartmentTypeData counter WHERE counter.Code = {0}";

        public const string GetQuestionsByEvaluationCode = @"SELECT questions FROM EvaluationQuestionData questions 
                                                    WHERE questions.Evaluation.Code = {0}";

        public const string SearchStructByText = @"SELECT distinct obj FROM StructData obj WHERE obj.Name like '%{0}%' or obj.Zone.Name like '%{0}%'";

        public const string GetEvaluationByCode = @"SELECT eva FROM EvaluationData eva left join fetch eva.SetQuestions
                                                    WHERE eva.Code = {0}";

        public const string GetEvaluationUserAppByEvaluationCode = @"SELECT app FROM EvaluationUserApplicationData app 
                                                        WHERE app.Evaluation.Code = {0}";

        public const string NewIncidentRuleEvaluate = @"select incident
                                                            from IncidentData incident 
                                                                    left join fetch incident.SetReportBaseList child 
                                                                    left join fetch child.SetIncidentTypes
                                                            where incident.EndDate is null and
                                                                  incident.IsEmergency = true";
        public const string NewIncidentRule = @"select incident
                                                            from IncidentData incident 
                                                                    left join fetch incident.SetReportBaseList child 
                                                                    left join fetch child.SetIncidentTypes";
        
		public const string IncidentTypesWithDepartmentTypesQuestions = @"select child.Question, incidentType 
                                                                            from IncidentTypeData incidentType left join fetch
                                                                                    incidentType.SetIncidentTypeDepartmentTypes left join fetch
                                                                                        incidentType.SetIncidentTypeQuestions child left join fetch
                                                                                            child.Question.SetPossibleAnswers inner join
                                                                                            child.Question.App apps
                                                                            where child.Question.DeletedId is null and apps.Code = {0}";

        public const string NotCloseIncidentsSetIncidentTypes = @"select incident
                                                            from IncidentData incident 
                                                                    left join fetch incident.SetReportBaseList child 
                                                                    left join fetch child.SetIncidentTypes";

        public const string NotCloseIncidents = @"select incident
                                                            from IncidentData incident 
                                                            where incident.EndDate is null and
                                                                  incident.IsEmergency = true";

        public const string NewIncidentRuleEvaluateSearch = @"select incident
                                                            from IncidentData incident 
                                                                    left join fetch incident.SetReportBaseList child 
                                                                    left join fetch child.SetIncidentTypes incTypes
                                                            where incident.EndDate is null and
                                                                  incident.IsEmergency = true and (incident.CustomCode like '%{0}%' OR incTypes.FriendlyName like '%{0}%')";

        public const string SelectedIncidentRuleEvaluate = @"SELECT incident
                                                            FROM IncidentData incident
                                       
WHERE incident.Code = {0}";

        public const string AssignedIncidentNotification = @"SELECT incidentNotif 
                                    FROM IncidentNotificationData incidentNotif
                                    WHERE incidentNotif.DispatchOperator is not null";

        public const string GetUserSessionByApplication = @"Select uaad
                                                        from SessionHistoryData uaad
                                                        where uaad.UserApplication.Name = '{0}'
                                                        and uaad.UserAccount.Login = '{1}'
                                                        and uaad.IsLoggedIn = true
                                                        order by uaad.StartDateLogin desc";

        public const string GetUserSessionInOtherMachine = @"Select uaad
                                                        from SessionHistoryData uaad
                                                        where uaad.ComputerName != '{0}'
                                                        and uaad.UserAccount.Login = '{1}'
                                                        and uaad.IsLoggedIn = true
                                                        order by uaad.StartDateLogin desc";

        public const string GetUserSessionCountByApplication = @"Select count(*)
                                                        from SessionHistoryData uaad
                                                        where uaad.UserApplication.Name = '{0}'
                                                        and uaad.UserAccount.Login = '{1}'
                                                        and uaad.IsLoggedIn = true
                                                        ";

        public const string GetUserSessions = @"Select uaad
                                                from SessionHistoryData uaad
                                                where uaad.UserAccount.Code = {0}
                                                and uaad.IsLoggedIn = true
                                                order by uaad.StartDateLogin desc";

        public const string GetAllUserSessions = @"Select uaad
                                                from SessionHistoryData uaad
                                                where uaad.IsLoggedIn = true
                                                order by uaad.StartDateLogin desc";

        public const string GetComputerNameByUserLogin = @"Select uaad.ComputerName
                                                    from SessionHistoryData uaad
                                                    where uaad.UserAccount.Login = '{0}'
                                                    and uaad.IsLoggedIn = true
                                                    order by uaad.StartDateLogin desc";
        
        public const string GetOperatorDepartamentTypes = @"Select dep
                                                        from OperatorData oper
                                                             left join oper.DepartmentTypes dep
                                                        where oper.Code = '{0}'";

        public const string GetLastSessionHistory = @"Select sessionHistory
                                                            from SessionHistoryData sessionHistory
                                                            where sessionHistory.UserAccount.Code = {0}
                                                            and sessionHistory.UserApplication.Name = '{1}'
                                                            and (sessionHistory.EndDateLogin IS NULL OR sessionHistory.EndDateLogin = 
                                                                (Select max(sh.EndDateLogin) from SessionHistoryData sh where sh.UserAccount.Code = {0} and
                                                                 sessionHistory.UserApplication.Name = '{1}'))
                                                            order by sessionHistory.StartDateLogin desc";

        public const string GetLastSessionHistories = @"Select sessionHistory
                                                            from SessionHistoryData sessionHistory
                                                            where sessionHistory.UserAccount.Code = {0}
                                                            and (sessionHistory.EndDateLogin IS NULL OR sessionHistory.EndDateLogin = 
                                                                (Select max(sh.EndDateLogin) from SessionHistoryData sh where sh.UserAccount.Code = {0}))
                                                            order by sessionHistory.StartDateLogin desc";

        public const string GetIncidentNotificationPriorities = @"SELECT DATA FROM IncidentNotificationPriorityData DATA";

        public const string GetIncidentNotificationInSupervisorStatus = @"Select ind
                                                                        from IncidentNotificationData ind
                                                                        where ind.Status.CustomCode = 'AUTOMATIC_SUPERVISOR' OR
                                                                        ind.Status.CustomCode = 'MANUAL_SUPERVISOR'  ORDER BY ind.Priority.CustomCode ASC, ind.CreationDate ASC";


        public const string GetCountCurrentIncidentNotificationsByOperator = @"Select count(*)
                                                                        from IncidentNotificationData ind
                                                                        where ind.DispatchOperator IS NOT NULL AND ind.DispatchOperator.Code = {0} 
                                                                        AND ind.EndDate IS NULL";

        public const string GetIncidentNotificationInSupervisorStatusByDepartment = @"SELECT data from IncidentNotificationData data where data.Code in
                                                                        (Select distinct ind.Code
                                                                        from IncidentNotificationData ind
                                                                        where ind.DepartmentType.Code = {0}
                                                                        and ind.Status.CustomCode = 'AUTOMATIC_SUPERVISOR' OR
                                                                        ind.Status.CustomCode = 'MANUAL_SUPERVISOR') ORDER BY data.Priority.CustomCode ASC, data.CreationDate ASC";


        public const string GetDepartmentZonesWithAddress = @"SELECT data FROM DepartmentZoneData data left join fetch data.DepartmentZoneAddres";
        
        public const string GetDepartmentZoneAddressByDepartmentType = @"SELECT obj FROM DepartmentZoneAddressData obj WHERE obj.Zone.DepartmentType.Code = {0}";

        public const string GetDepartmentZoneAddressByZoneCode= @"SELECT obj FROM DepartmentZoneAddressData obj WHERE obj.Zone.Code = {0}";

        public const string GetDepartmentStationAddressByStationCode = @"SELECT obj FROM DepartmentStationAddressData obj WHERE obj.Station.Code = {0}";
        
        public const string GetIncidentNotificationByCustomCode = @"Select ind
                                                                    from IncidentNotificationData ind
                                                                    where ind.CustomCode = '{0}'";


        public const string GetDepartmentZoneAddressDataByZoneOrderByPointNumber = @"SELECT obj FROM DepartmentZoneAddressData obj WHERE obj.Zone.Code = {0} ORDER BY obj.PointNumber";

        public const string GetDepartmentStationAddressDataByZone = @"SELECT obj FROM DepartmentStationAddressData obj WHERE obj.Station.DepartmentZone.Code = {0}";

        public const string GetIncidentNotificationsByIncidentCustomCode = @"Select ind
                                                                 from IncidentNotificationData ind
                                                                 where ind.ReportBase.Incident.CustomCode = '{0}'";

        public const string GetIncidentNotificationsByReportBase = @"Select ind
                                                                 from IncidentNotificationData ind
                                                                 where ind.ReportBase.Code = '{0}'";

        public const string GetCctvReportDataByIncidentCode = @"Select report
                                                                 from CctvReportData report
                                                                 where report.Incident.Code = '{0}'";
        
        public const string GetOperatorByAgentID = "SELECT count(ObjectData.Code) FROM OperatorData ObjectData WHERE (ObjectData.AgentID is not null  AND ObjectData.AgentID = '{0}') AND (ObjectData.Code != '{1}') ";

        public const string GetCountWorkShiftByRange = "SELECT count(ws.Code) FROM WorkShiftVariationData ws inner join ws.Schedules schedules WHERE (schedules.Start BETWEEN '{0}' AND '{1}') OR schedules.End BETWEEN '{0}' AND '{1}')";

        public const string GetCountOperatorAssignByRange = @"SELECT count(assign.Code) 
                                                        FROM OperatorAssignData assign
                                                        WHERE (assign.StartDate BETWEEN '{0}' AND '{1}') OR assign.EndDate BETWEEN '{0}' AND '{1}')";
		
        public const string GetOperatorByCode = "Select data from OperatorData data where data.Code = {0}";

		public const string GetOperatorByCodeWithCategories = "Select data from OperatorData data left join fetch data.CategoryHistoryList where data.Code = {0}";
        
        public const string GetOperatorWithSupervisorsByCode = "Select data from OperatorData data left join fetch data.Supervisors where data.Code = {0}";

        public const string GetObservationTypeByName = "SELECT obj FROM ObservationTypeData obj WHERE obj.Name ='{0}'";

        public const string GetDispatchOrCctvOperators = @"Select operator from OperatorData operator where operator.Code in
                                                           (Select distinct ope.Code from OperatorData ope
                                                            left join ope.Role role
                                                            left join role.Profiles profile
                                                            inner join profile.Accesses access
                                                            where ope.PersonId != '6666666666666' AND (access.Name like '%DispatchSupervisorName+Basic+Supervision%' OR access.Name like '%UserApplicationData+Basic+Cctv%' OR access.Name like '%UserApplicationData+Basic+Dispatch%'))";

        public const string GetOperatorAccessByAdmCctvFirstLevel = @"Select count(ope) from OperatorData ope " +
                                                            "left join ope.Role role " +
                                                            "left join role.Profiles profile " +
                                                            "inner join profile.Accesses access " +
                                                            "where ope.Code = {0} AND (access.Name like '%StructTypeData+Insert+Administration%' OR " +
                                                            " access.Name like '%StructTypeData+Update+Administration%')";

        public const string GetIncidentNotificationsByDispatchOperator = @"Select notification
                                                                            from IncidentNotificationData notification
                                                                            where notification.DispatchOperator.Login = '{0}' and
                                                                                  notification.Status.Active = true";


        public const string GetOperatorEvaluationsByOperatorCode = "SELECT obj FROM OperatorEvaluationData obj WHERE obj.Operator.Code = {0}";

		public const string GetOperatorEvaluationWithAnswersByCode = "SELECT obj FROM OperatorEvaluationData obj left join fetch obj.QuestionAnswers WHERE obj.Code = {0}";

        public const string GetOperatorObservationByOperatorCode = "SELECT obj FROM OperatorObservationData obj WHERE obj.Operator.Code = {0}";

        public const string GetOperatorCategoryHistoryByOperatorCode = "SELECT obj FROM OperatorCategoryHistoryData obj WHERE obj.Operator.Code = {0}";

        public const string GetIncidentOpensByIncidentType = @"Select incident
                                                               from IncidentData incident 
                                                                where
                                                                incident.Status.Name = '{0}' AND incident.Code in 
                                                               ( select reportBase.Incident.Code
                                                                    from ReportBaseData reportBase 
                                                                    left join reportBase.SetIncidentTypes types
                                                                    where types.Code = {1})";

        public const string GetActiveIncidentNotificationsOrInManualSupervisor = 
                                                            @"Select notification
                                                               from IncidentNotificationData notification
                                                               where notification.Status.Active = true OR
                                                                    notification.Status.CustomCode = 'MANUAL_SUPERVISOR'";

        public const string GetLoggedInOperatorsByApplicationStatus = @"Select operator
                                                                        from OperatorData operator,
                                                                             SessionHistoryData session,
                                                                             SessionStatusHistoryData sessionStatus
                                                                        where operator.Code = session.UserAccount.Code and
                                                                              session.UserApplication.Name = '{1}' and
                                                                              session.IsLoggedIn = true and
                                                                              session.Code = sessionStatus.Session.Code and
                                                                              sessionStatus.Status.CustomCode IN {0} and
                                                                              sessionStatus.EndDate IS NULL";

        public const string GetIfOperatorCanReceiveNotifications = @"Select count(*)
                                                                        from SessionHistoryData session,
                                                                             SessionStatusHistoryData sessionStatus
                                                                        where session.UserAccount.Code = {0} and
                                                                              session.UserApplication.Name = '{1}' and
                                                                              session.IsLoggedIn = true and
                                                                              session.Code = sessionStatus.Session.Code and
                                                                              sessionStatus.Status.CustomCode IN {2} and
                                                                              sessionStatus.EndDate IS NULL";

        public const string GetUserAccessByApplication = @"Select distinct access 
                                                            from UserAccessData access 
                                                            where access.UserApplication.Name = '{0}'";

        public const string GetUserAccessByApplicationResource = @"Select access
                                                                        from UserAccessData access
                                                                        where access.UserApplication.Name = '{0}' and
                                                                            access.UserPermission.UserResource.Name = '{1}'";

        
        //Los siguientes HQLS son utilizados para cargar la información requerida al abrir el formulario de asignación de personal
        #region OperatorAssignForm 
        public const string GetWorkShiftWithNotExpiredSchedules = @"SELECT ws 
                                                                    FROM WorkShiftVariationData ws 
                                                                     inner join fetch ws.Schedules schedules
										                            WHERE ws.Type != {0} AND ws.ObjectType = 0 AND schedules.End >= '{1}'";

        public const string GetWorkShiftWithOperatorsAssigns = @"SELECT operator, ws  
                                                                    FROM WorkShiftVariationData ws
                                                                         inner join ws.Operators operators
                                                                         inner join operators.Operator operator
                                                                         inner join fetch operator.Operators assign  
										                            WHERE ws.Type != {0} AND ws.ObjectType = 0 AND assign.EndDate >= '{1}'";


        public const string GetWorkShiftWithSupervisorsAssigns = @"SELECT operator, ws  
                                                                    FROM WorkShiftVariationData ws
                                                                         inner join ws.Operators operators
                                                                         inner join operators.Operator operator
                                                                         inner join fetch operator.Supervisors assign  
										                            WHERE ws.Type != {0} AND ws.ObjectType = 0 AND assign.EndDate >= '{1}'";


        public const string GetWorkShiftWithOperatorsCategoryList = @"SELECT operator, ws  
                                                                    FROM WorkShiftVariationData ws
                                                                         inner join ws.Operators operators
                                                                         inner join operators.Operator operator
                                                                         inner join fetch operator.CategoryHistoryList categories  
										                            WHERE ws.Type != {0} AND ws.ObjectType = 0";

        public const string GetWorkShiftWithFirstLevelOperatorsAndSupervisors = @"SELECT ws FROM WorkShiftVariationData ws
                                                            inner join fetch ws.Operators operators 
                                                            WHERE ws.Type != {0} AND ws.ObjectType = 0 and
                                                                  operators.Operator.Code in (SELECT ope.Code 
                                                                                              FROM OperatorData ope
                                                                                                   left join ope.Role.Profiles profiles 
                                                                                                   inner join profiles.Accesses access 
                                                                                                   WHERE (access.Name like '%UserApplicationData+Basic+FirstLevel%' OR
                                                                                                          access.Name like '%FirstLevelSupervisor%')) AND
                                                                  operators.Operator.Code not in (SELECT distinct ope.Code 
                                                                                                  FROM OperatorData ope 
													                                                    left join ope.Role role  
													                                                    left join role.Profiles profile 
													                                                    inner join profile.Accesses access 
													                                              WHERE access.Name like '%GeneralSupervisorName%')";

        public const string GetWorkShiftWithDispatchOperatorsAndSupervisors = @"SELECT ws FROM WorkShiftVariationData ws
                                                            inner join fetch ws.Operators operators 
                                                            WHERE ws.Type != {0} AND ws.ObjectType = 0 and                                
                                                                  operators.Operator.Code in (SELECT ope.Code 
                                                                                              FROM OperatorData ope 
                                                                                                   left join ope.DepartmentTypes dep
                                                                                                   left join ope.Role.Profiles profiles 
                                                                                                   inner join profiles.Accesses access 
                                                                                                   WHERE (access.Name like '%UserApplicationData+Basic+Dispatch%' OR
                                                                                                          access.Name like '%DispatchSupervisor%') AND dep.Code = {1}) AND
                                                                  operators.Operator.Code not in (SELECT distinct ope.Code 
                                                                                                  FROM OperatorData ope 
													                                                    left join ope.Role role  
													                                                    left join role.Profiles profile 
													                                                    inner join profile.Accesses access 
													                                              WHERE access.Name like '%GeneralSupervisorName%')";

        public const string GetWorkShiftForOperatorAssignsFormCaseFirstLevel = @"SELECT ws FROM WorkShiftVariationData ws
                            inner join ws.Schedules schedules
                            inner join ws.Operators operators 
                            WHERE ws.Type != {0} AND ws.ObjectType = 0 and
                                  schedules.End >= '{1}' and
                                  operators.Operator.Code in (SELECT ope.Code 
                                                              FROM OperatorData ope
                                                                   left join ope.Role.Profiles profiles 
                                                                   inner join profiles.Accesses access 
                                                                   WHERE (access.Name like '%UserApplicationData+Basic+FirstLevel%' OR
                                                                          access.Name like '%FirstLevelSupervisor%')) AND
                                  operators.Operator.Code not in (SELECT distinct ope.Code 
                                                                  FROM OperatorData ope 
													                    left join ope.Role role  
													                    left join role.Profiles profile 
													                    inner join profile.Accesses access 
													              WHERE access.Name like '%GeneralSupervisorName%')";

        public const string GetWorkShiftForOperatorAssignsFormCaseDispatch = @"SELECT ws FROM WorkShiftVariationData ws
                            inner join ws.Schedules schedules
                            inner join ws.Operators operators 
                            WHERE ws.Type != {0} AND ws.ObjectType = 0 and
                                  schedules.End >= '{1}' and
                                  operators.Operator.Code in (SELECT ope.Code 
                                                              FROM OperatorData ope 
                                                                   left join ope.DepartmentTypes dep
                                                                   left join ope.Role.Profiles profiles 
                                                                   inner join profiles.Accesses access 
                                                                   WHERE (access.Name like '%UserApplicationData+Basic+Dispatch%' OR
                                                                          access.Name like '%DispatchSupervisor%') AND dep.Code = {2}) AND
                                  operators.Operator.Code not in (SELECT distinct ope.Code 
                                                                  FROM OperatorData ope 
													                    left join ope.Role role  
													                    left join role.Profiles profile 
													                    inner join profile.Accesses access 
													              WHERE access.Name like '%GeneralSupervisorName%')";

        #endregion


        #region EndDispatchReportForm

        public const string GetDispatchReportsWithQuestionsByIncTypeAndDepType = @"SELECT objectData
                                                                                   FROM DispatchReportData objectData 
                                                                                        inner join fetch objectData.Questions questions 
                                                                                        inner join objectData.IncidentTypes incidentTypes 
                                                                                        inner join objectData.DepartmentTypes depTypes 
                                                                                   WHERE depTypes.Code = {0} 
                                                                                         AND incidentTypes.Code IN (SELECT distinct incTypes.Code FROM ReportBaseData rb 
                                                                                                                                  inner join rb.SetIncidentTypes incTypes 
                                                                                                                             WHERE rb.Incident.Code = {1}) ORDER BY questions.Order";

        public const string GetDispatchReportsWithIncidentTypesByIncTypeAndDepType = @"SELECT objectData
                                                                                       FROM DispatchReportData objectData 
                                                                                            inner join fetch objectData.IncidentTypes incidentTypes 
                                                                                            inner join objectData.DepartmentTypes depTypes 
                                                                                       WHERE depTypes.Code = {0} 
                                                                                             AND incidentTypes.Code IN (SELECT distinct incTypes.Code 
                                                                                                                        FROM ReportBaseData rb 
                                                                                                                             inner join rb.SetIncidentTypes incTypes 
                                                                                                                        WHERE rb.Incident.Code = {1})";

        public const string GetDispatchReportsByIncTypeAndDepType = @"SELECT objectData
                                                                     FROM DispatchReportData objectData 
                                                                          inner join objectData.IncidentTypes incidentTypes 
                                                                          inner join objectData.DepartmentTypes depTypes 
                                                                     WHERE depTypes.Code = {0} 
                                                                           AND incidentTypes.Code IN (SELECT distinct incTypes.Code 
                                                                                                      FROM ReportBaseData rb 
                                                                                                           inner join rb.SetIncidentTypes incTypes 
                                                                                                      WHERE rb.Incident.Code = {1})";

#endregion


        public const string GetPositionByDepartmentName = @"Select position 
                                                            from PositionData position
                                                            where position.DepartmentType.Name = '{0}'";

        public const string GetPositionByDepartmentCode = @"Select position 
                                                            from PositionData position
                                                            where position.DepartmentType.Code = {0}";

        public const string GetPositions = @"Select DATA from PositionData DATA";

        public const string GetRanks = @"Select DATA from RankData DATA";

        public const string GetRankByDepartmentName = @"Select rank
                                                            from RankData rank
                                                            where rank.DepartmentType.Name = '{0}'";

        public const string GetRankByDepartmentCode = @"Select rank
                                                            from RankData rank
                                                            where rank.DepartmentType.Code = {0}";

        public const string GetUnitTypeByDepartmentTypeCode = @"Select unitType 
                                                            from UnitTypeData unitType
                                                            where unitType.DepartmentType.Code = '{0}'";

        public const string GetUnitByGPSCode = @"Select dataObject 
                                                            from UnitData dataObject
                                                            where dataObject.GPS.Code = '{0}'";

        public const string GetUnitByCustomCode = @"SELECT unit 
                                                      FROM UnitData unit 
                                                      WHERE unit.CustomCode = '{0}'";

        public const string GetCurrentUnitDispatchOrder = @"Select dispatchOrder
                                                            From DispatchOrderData dispatchOrder
                                                            Where dispatchOrder.Unit.CustomCode = '{0}' and
                                                                  dispatchOrder.Status IS NOT NULL and
                                                                  (dispatchOrder.Status.CustomCode = 'DISPATCHED' OR
                                                                   dispatchOrder.Status.CustomCode = 'DELAYED' OR
                                                                   dispatchOrder.Status.CustomCode = 'UNIT_ONSCENE')";

        public const string GetUnitByCode = @"Select unit
                                                From UnitData unit
                                                Where unit.Code = {0}";

        public const string GetIncidentByCode = @"Select incident
                                                    From IncidentData incident
                                                    Where incident.Code = {0}";

        public const string GetUnitOrderByCode = @"Select unit
                                                    From UnitData unit
                                                    ORDER BY unit.Code ASC";

           public const string GetCountDispatchReportByIncidentTypeCode = @"SELECT count(data.Code) 
                                                                            FROM DispatchReportData data
                                                                            inner join data.IncidentTypes it
                                                                            WHERE it.Code = {0}";

        public const string GetPhoneReportByCode = @"SELECT phoneReport
                                                            FROM PhoneReportData phoneReport
                                                            WHERE phoneReport.Code = '{0}'";

        public const string GetAudioByPhoneReportCustomCode = @"SELECT audio
                                                            FROM PhoneReportAudioData audio
                                                            WHERE audio.CustomCode = '{0}'";

        public const string GetSupportRequestReportByCode = @"SELECT supportRequestReport
                                                                FROM SupportRequestReportData supportRequestReport
                                                                WHERE supportRequestReport.Code = '{0}'";

        public const string GetCctvReportDataByCode = @"SELECT data
                                                                FROM CctvReportData data
                                                                WHERE data.Code = '{0}'";

        public const string GetAlarmReportDataByCode = @"SELECT data
                                                                FROM AlarmReportData data
                                                                WHERE data.Code = '{0}'";

        public const string GetAlarmLprByPlateRequestAndLpr = @"SELECT data 
                                                                FROM AlarmLprData data
                                                                WHERE data.Plate = '{0}' 
                                                                AND data.VehicleRequest.Code = {1}
                                                                AND data.Lpr.Code = {2}";

        public const string GetAlarmLprReportDataByCode = @"SELECT data
                                                                FROM AlarmLprReportData data
                                                                WHERE data.Code = '{0}'";

        public const string GetCurrentSessionStatusHistory = @"Select sessionStatusHistoryData
                                                                from SessionStatusHistoryData sessionStatusHistoryData
                                                                where sessionStatusHistoryData.Session.UserApplication.Code = {0}
                                                                and sessionStatusHistoryData.Session.UserAccount.Login = '{1}'
                                                                and sessionStatusHistoryData.Session.IsLoggedIn = true
                                                                and sessionStatusHistoryData.EndDate IS NULL
                                                                order by sessionStatusHistoryData.StartDate desc";

        public const string GetCurrentUnitStatusHistory = @"Select unitStatusHistoryData
                                                            from UnitStatusHistoryData unitStatusHistoryData
                                                                where unitStatusHistoryData.Unit.Code = {0}
                                                                and unitStatusHistoryData.End IS NULL
                                                                order by unitStatusHistoryData.Start desc";

        public const string GetCurrentIncidentNotificationStatusHistory =
                                                          @"Select incidentNotificationHistory
                                                            from IncidentNotificationStatusHistoryData incidentNotificationHistory
                                                                where incidentNotificationHistory.IncidentNotification.Code = {0}
                                                                and incidentNotificationHistory.End IS NULL
                                                                order by incidentNotificationHistory.Start desc";

        public const string GetCurrentIncidentNotificationDepartmentStationHistory =
                                                  @"Select INDSHD
                                                            from IncidentNotificationDepartmentStationHistoryData INDSHD
                                                                where INDSHD.IncidentNotification.Code = {0}
                                                                and INDSHD.End IS NULL
                                                                order by INDSHD.Start desc";

        public const string GetOperatorTrainingCourseByOperatorCode = "SELECT obj FROM OperatorTrainingCourseData obj WHERE obj.Operator.Code = {0}";

        public const string GetIndicatorGroupForecastFactoryWithValues = @"SELECT dat FROM IndicatorGroupForecastData dat inner join fetch dat.ForecastValues
                                                                WHERE dat.General = 1 AND dat.Start = ( SELECT min(data.Start) FROM IndicatorGroupForecastData data)";

        public const string GetIncidentsByStatus = @"SELECT incident
                                                          FROM IncidentData incident
                                                          WHERE incident.Status.CustomCode = '{0}'";

        public const string GetUnitsByOperatorLogin = @"SELECT unit 
                                                    FROM UnitData as unit JOIN unit.DepartmentStation.DepartmentZone.DepartmentType as departmentType 
                                                    WHERE departmentType.Name IN 
                                                    ( 
                                                        SELECT depType.Name 
                                                            FROM OperatorData oper JOIN oper.DepartmentTypes depType 
                                                            WHERE oper.Login ='{0}'
                                                    )";
        
        public const string GetCamerasByStructNameAndOperatorCode = @"SELECT camera 
                                                                        FROM CameraData camera 
                                                                        WHERE camera.Struct.Name = '{0}'
                                                                        AND camera.Code IN
                                                                        (
                                                                             SELECT objectData.Device.Code 
                                                                                FROM OperatorDeviceData objectData 
                                                                                WHERE objectData.User.Code = '{1}'
                                                                        )";


        public const string GetActiveIncidentNotificationsNotAssignedToSupervisor =
                                                                @"SELECT incidentNotification 
                                                                     FROM IncidentNotificationData incidentNotification
                                                                     WHERE incidentNotification.Status.Active = true AND
                                                                     (SELECT count(*) 
                                                                        FROM SessionHistoryData sh 
                                                                        WHERE sh.IsLoggedIn = true AND
                                                                              sh.UserAccount.Code = incidentNotification.DispatchOperator.Code AND
                                                                              sh.UserApplication.Name = 'Dispatch') = 0";

        public const string GetDepartmentTypeWithZonesStations = @"SELECT data FROM
                                                                    DepartmentTypeData data left join fetch 
                                                                    data.DepartmentZones zone left join fetch 
                                                                    zone.SetDepartmentStations";

        public const string GetDepartmentTypeWithZones = @"SELECT data FROM DepartmentTypeData data left join fetch data.DepartmentZones zone";

        public const string GetDepartmentStationsByDepartmentZoneCode = 
                                                            @"SELECT departmentStation 
                                                                FROM DepartmentStationData as departmentStation 
                                                                WHERE departmentStation.DepartmentZone.Code = {0}";

        public const string GetCountDepartmentStationsByDepartmentZoneCode =
                                                            @"SELECT count(*)  
                                                                FROM DepartmentStationData as departmentStation 
                                                                WHERE departmentStation.DepartmentZone.Code = {0}";

        public const string CheckAssociatedObjectsToDepartmentStation =
                                            @"SELECT station FROM DepartmentStationData station
                                                WHERE ((SELECT COUNT(inc.Code) FROM IncidentNotificationData inc WHERE inc.DepartmentStation.Code = station.Code AND inc.EndDate = null) > 0 OR
                                                      (SELECT COUNT(officer.Code) FROM OfficerData officer WHERE officer.DepartmentStation.Code = station.Code) > 0 OR
                                                      (SELECT COUNT(unit.Code) FROM UnitData unit WHERE unit.DepartmentStation.Code = station.Code) > 0) AND
                                                      station.Code = {0}";

        public const string CheckAssociatedObjectsToDepartmentType =
                                            @"SELECT dept FROM DepartmentTypeData dept
                                                WHERE ((SELECT COUNT(ut.Code) FROM UnitTypeData ut WHERE ut.DepartmentType.Code = dept.Code) > 0 OR
                                                       (SELECT COUNT(unit.Code) FROM UnitData unit WHERE unit.DepartmentStation.DepartmentZone.DepartmentType.Code = dept.Code) > 0 OR
                                                       (SELECT COUNT(dz.Code) FROM DepartmentZoneData dz WHERE dz.DepartmentType.Code = dept.Code) > 0 OR
                                                       (SELECT COUNT(inc.Code) FROM IncidentNotificationData inc WHERE inc.DepartmentType.Code = dept.Code) > 0 OR
                                                       (SELECT COUNT(rank.Code) FROM RankData rank WHERE rank.DepartmentType.Code = dept.Code) > 0 OR
                                                       (SELECT COUNT(officer.Code) FROM OfficerData officer WHERE officer.DepartmentStation.DepartmentZone.DepartmentType.Code = dept.Code) > 0 OR
                                                       (SELECT COUNT(position.Code) FROM PositionData position WHERE position.DepartmentType.Code = dept.Code) > 0 OR
                                                       (SELECT COUNT(itdt.Code) FROM IncidentTypeDepartmentTypeData itdt WHERE itdt.DepartmentType.Code = dept.Code) > 0 OR
                                                       (SELECT COUNT(edtd.Code) FROM EvaluationDepartmentTypeData edtd WHERE edtd.Departament.Code = dept.Code) > 0 OR
                                                       (SELECT COUNT(department.Code) FROM OperatorData oper left join oper.DepartmentTypes department WHERE department.Code = dept.Code) > 0 OR
                                                       (SELECT COUNT(route.Code) FROM RouteData route WHERE route.DepartmentType.Code = dept.Code) > 0 OR 
                                                       (SELECT count(data.Code) FROM DispatchReportData data inner join data.DepartmentTypes department WHERE department.Code = dept.Code) > 0) AND 
                                                        dept.Code = {0}";
       
        
        public const string GetNotificationOpenCountByDepartment = @"SELECT count(*) FROM IncidentNotificationData data
                                                                       WHERE data.DepartmentType.Code = {0} AND data.Status.Code != {1} AND data.Status.Code != {2}";


        public const string GetDispatchReportCountByDepartmentTypeCode = @"SELECT count(data.Code) 
                                                                            FROM DispatchReportData data
                                                                            inner join data.DepartmentTypes dep
                                                                            WHERE dep.Code = {0}";

        public const string GetOperatorsByDepartmentType = @"SELECT oper FROM OperatorData oper left join fetch oper.DepartmentTypes
                                                            WHERE oper.Code IN (SELECT op.Code FROM OperatorData op left join op.DepartmentTypes deptTypes WHERE deptTypes.Code = {0})";

        public const string GetIncidentTypesDepartmentByDept = @"SELECT incTypeDep FROM IncidentTypeDepartmentTypeData incTypeDep WHERE incTypeDep.DepartmentType.Code = {0})";

        public const string GetDepartmentStationCodeByDepartmentType = @"SELECT ds 
                                                                         FROM DepartmentStationData ds
                                                                         WHERE ds.DepartmentZone.DepartmentType.Code = {0}";

        public const string GetDepartmentZonesByDepartmentType = @"SELECT dz 
                                                                         FROM DepartmentZoneData dz
                                                                         WHERE dz.DepartmentType.Code = {0}";

        public const string GetDepartmentZoneByCustomCode = @"SELECT dz 
                                                                         FROM DepartmentZoneData dz
                                                                         WHERE dz.CustomCode = '{0}'";

		public const string GetDepartmentZoneWithAddressAndStationsByCode = @"SELECT dz 
                                                                         FROM DepartmentZoneData dz 
                                                                         left join fetch dz.DepartmentZoneAddres
                                                                         left join fetch dz.SetDepartmentStations   
                                                                         WHERE dz.Code = {0}";

		public const string GetRouteWithAddressByCode = @"SELECT route
                                                             FROM RouteData route 
                                                             left join fetch route.RouteAddress   
                                                             WHERE route.Code = {0}";

		public const string GetDepartmentStationByCode = @"SELECT ds 
                                                                         FROM DepartmentStationData ds 
                                                                         left join fetch ds.DepartmentStationAddres
                                                                         WHERE ds.Code = {0}";
        
        public const string GetAllDepartmentStationByZoneCode = @"SELECT data FROM DepartmentStationData data 
                                                                left join fetch data.DepartmentStationAddres where
                                                                data.DepartmentZone.Code = {0}";

        public const string GetDepartmentsStation = @"SELECT stat FROM DepartmentStationData stat LEFT JOIN FETCH stat.DepartmentStationAddres";

        public const string GetUnitStatus = @"SELECT ObjectData FROM UnitStatusData ObjectData";
        public const string GetIncidents = @"SELECT ObjectData FROM IncidentData ObjectData";
        public const string GetAllDepartmentStationAssignments = @"SELECT ObjectData 
                                                                    FROM DepartmentStationAssignHistoryData ObjectData 
                                                                    WHERE ObjectData.End > '{0}'";

        public const string GetUnitStatusByCustomCode = @"SELECT unitStatus 
                                                                FROM UnitStatusData as unitStatus 
                                                                WHERE unitStatus.CustomCode = '{0}'";

        public const string GetPublicLineByTelephoneNumber = @"SELECT publicLine
                                                                    FROM PublicLineData as publicLine
                                                                    WHERE publicLine.Telephone = '{0}'";

        public const string GetUnitsWithIncidentTypesOfficerAssigns = @"select unit from UnitData unit left join fetch unit.SetIncidentTypes left join fetch unit.SetOfficerAssigns";

        public const string OperatorHasAssignedDepartmentType = @"select count(*) from OperatorData ope inner join ope.DepartmentTypes dt where ope.Login = '{0}' and dt.Name = '{1}'";

        public const string IncidentNotificationByStatusByOperatorDepartmentType = @"SELECT incidentNotif 
                                                                                 FROM IncidentNotificationData incidentNotif 
                                                                                      inner join incidentNotif.ReportBase rb 
                                                                                 WHERE incidentNotif.Status.CustomCode = '{0}' 
                                                                                       AND incidentNotif.DepartmentType.Code IN (SELECT depTypes.Code 
                                                                                                                                 FROM OperatorData oper
                                                                                                                                      left join oper.DepartmentTypes depTypes
                                                                                                                                 WHERE oper.Code = {1})";


        public const string IncidentNotificationByStatusWithReportBaseIncidentTypes = @"SELECT rb.SetIncidentTypes, incidentNotif 
                                                                                                         FROM IncidentNotificationData incidentNotif 
                                                                                                         inner join incidentNotif.ReportBase rb 
                                                                                                         left join fetch incidentNotif.SetDispatchOrders
                                                                                                         WHERE incidentNotif.Status.CustomCode = '{0}'";

        public const string DepartmentZonesWithDepartmentStations = "select dz from DepartmentZoneData dz left join fetch dz.SetDepartmentStations";

		public const string DepartmentZonesWithDepartmentStationsByOperatorDepartmentType = @"SELECT dz 
																							  FROM DepartmentZoneData dz 
																								   inner join fetch dz.SetDepartmentStations
																							  WHERE dz.DepartmentType.Code IN (SELECT depTypes.Code 
																																 FROM OperatorData oper
																																	  left join oper.DepartmentTypes depTypes
																																 WHERE oper.Code = {0})";

		public const string DepartmentZonesByOperatorDepartmentType = @"SELECT dz 
                                                                      FROM DepartmentZoneData dz 
                                                                      WHERE dz.DepartmentType.Code IN (SELECT depTypes.Code 
                                                                                                         FROM OperatorData oper
                                                                                                              left join oper.DepartmentTypes depTypes
                                                                                                         WHERE oper.Code = {0})";


        public const string DepartmentZonesWithAddress = @"select data 
                                                            from DepartmentZoneData data 
                                                                inner join data.DepartmentType dt 
                                                                left join fetch data.DepartmentZoneAddres
                                                            where data.DeletedId is null and dt.DeletedId is null 
                                                            order by data.Name ASC";

		public const string IncidentTypesWithDepartmentTypes = @"SELECT ITQ.IncidentType
                                                                FROM IncidentTypeQuestionData ITQ inner join fetch ITQ.IncidentType.SetIncidentTypeDepartmentTypes";

		public const string QuestionsWithSetPossibleAnswers = @"SELECT ITQ.Question 
                                                                FROM IncidentTypeQuestionData ITQ inner join fetch ITQ.Question.SetPossibleAnswers";

		public const string QuestionsWithUserApp = @"SELECT ITQ.Question 
                                                    FROM IncidentTypeQuestionData ITQ inner join fetch ITQ.Question.App";

		public const string IncidentTypesForApp = @"SELECT IT 
                                                    FROM IncidentTypeData IT  
													WHERE IT.Code in (SELECT IT.Code
                                                    FROM IncidentTypeData IT  
															left join IT.SetIncidentTypeQuestions questions
                                                            left join questions.Question.App apps
                                                        WHERE apps.Code = {0} OR IT.NoEmergency = true)";

		public const string IncidentNotificationLastStatus = "select history.Status from IncidentNotificationStatusHistoryData history where history.Status.CustomCode <> 'AUTOMATIC_SUPERVISOR' and history.Status.CustomCode <> 'MANUAL_SUPERVISOR' and history.IncidentNotification.Code = {0} order by history.Start desc";

        public const string IncidentMultipleOrganisms = "select count(distinct rbdt.DepartmentType.Code) from ReportBaseDepartmentTypeData rbdt where rbdt.ReportBase.Incident.Code = {0}";
        
        public const string GetDispatchOrdersByIncidentCode =
                @"SELECT dispatchOrder 
                  FROM DispatchOrderData dispatchOrder
                  LEFT OUTER JOIN dispatchOrder.EndingReport.Officer as officer
                  WHERE (officer is null or officer.DeletedId is null) and
                  dispatchOrder.Unit.DeletedId is null and 
                  dispatchOrder.IncidentNotification.ReportBase.Incident.Code = {0}";

        public const string GetDispatchOrdersByIncidentNotification =
                @"SELECT dispatchOrder 
                  FROM DispatchOrderData dispatchOrder
                  WHERE dispatchOrder.IncidentNotification.Code = {0}";

        public const string GetDispatchOrderNotesByDispatchOrder =
                @"SELECT note 
                  FROM DispatchOrderNoteData note
                  WHERE note.DispatchOrder.Code = {0}";

        public const string GetCountAssignedUnitsToNotification =
                @"SELECT count(distinct dispatchOrder.Code) 
                  FROM DispatchOrderData dispatchOrder                  
                  WHERE dispatchOrder.EndDate is null and 
                        dispatchOrder.Status.CustomCode IN ('DISPATCHED', 'DELAYED', 'UNIT_ONSCENE') and
                        dispatchOrder.IncidentNotification.Code = {0}";

        public const string GetPhoneReportsByIncidentCode =
                @"SELECT phoneReport 
                  FROM PhoneReportData phoneReport 
                  left join fetch phoneReport.SetIncidentTypes incTypes
                  WHERE phoneReport.Incident.Code = {0}";

        public const string GetQuestionByCustomCode =
                @"SELECT Qdata
                  FROM QuestionData Qdata
                  WHERE Qdata.CustomCode = '{0}'";

        public const string GetQuestionByCode =
        @"SELECT Qdata
                  FROM QuestionData Qdata
                  WHERE Qdata.Code = {0}";

        public const string GetIncidentTypeByCode =
                @"SELECT Idata
                  FROM IncidentTypeData Idata
                  WHERE Idata.Code = {0}";

        public const string GetPhoneReportAnswerDataByQuestionCode =
                @"SELECT obj
                  FROM PhoneReportAnswerData obj
                  WHERE obj.Question.Code = {0}";

        public const string GetPhoneReportAnswerDataByPhoneReportCode =
                @"SELECT obj
                  FROM PhoneReportAnswerData obj
                  WHERE obj.PhoneReport.Code = {0}";


        public const string  GetOperatorsAssignByWorkShiftVariationSchedule = @" SELECT distinct obj FROM  OperatorAssignData obj WHERE obj.SupervisedScheduleVariation.Code = {0}";
        
        public const string GetAllRooms = "SELECT r FROM RoomData r order by r.Name";

        public const string GetRoomsByName = "SELECT obj FROM RoomData obj WHERE obj.Name LIKE '%{0}%'";

        public const string GetRoomByName = "SELECT obj FROM RoomData obj WHERE obj.Name = '{0}'";

        public const string GetRoomsSeatByComputerName = "SELECT obj FROM RoomSeatData obj WHERE obj.ComputerName = '{0}'";

        public const string GetAllSeats = "SELECT obj FROM RoomSeatData obj";

        public const string GetOperatorsByRoomCode = "Select objectData FROM OperatorData objectData WHERE objectData.Seat.Room.Code = {0}";

        public const string GetIncidentTypeQuestionDataByIncidentTypeCode =
                @"SELECT objectData FROM IncidentTypeQuestionData objectData WHERE objectData.IncidentType.Code = {0}";

        public const string GetIncidentTypeQuestionDataByQuestionCode =
                @"SELECT objectData FROM IncidentTypeQuestionData objectData WHERE objectData.Question.Code = {0}";

        public const string GetIncidentTypeQuestionData = @"SELECT objectData FROM IncidentTypeQuestionData objectData ";
        
        #region "Analitycal Video"
        public const string GetAllRules = "SELECT data FROM AV_RULES data";
        #endregion
        
        #region CCTV
        public const string GetAllUnits = @"SELECT data FROM UnitData data";

        public const string GetAllUnitsTypes = "SELECT data FROM UnitTypeData data";

        public const string GetAllCctvZone = "SELECT data FROM CctvZoneData data";

        public const string GetAllStructs = "SELECT data FROM StructData data";

        public const string GetAllStructTypes = "SELECT data FROM StructTypeData data";
        
        public const string GetAllFreeStructs = "SELECT struct FROM StructData struct WHERE struct.Zone is null";

        public const string GetFreeStructsByType = @"SELECT struct FROM StructData struct WHERE struct.Type.Name = '{0}' AND struct.Zone is null";

        public const string GetAllCameras = @"SELECT data FROM CameraData data";

        public const string GetAllSensorTelemetrys = @"SELECT data FROM SensorTelemetryData data";

        public const string GetAllSensorTelemetrysOfDataloggersWithStruct = @"SELECT data FROM SensorTelemetryData data Where data.Datalogger is not null AND data.Datalogger.Struct is not null";
        
        public const string GetAllAlarmSensorTelemetrys = @"SELECT data FROM AlarmSensorTelemetryData data where data.EndAlarm is null";

        public const string GetAllAlarmSensorTelemetrysStarted = @"SELECT data FROM AlarmSensorTelemetryData data where data.EndAlarm is null and data.StartAlarm is not null";

        public const string GetAlarmSensorTelemetryByCode = @"SELECT data FROM AlarmSensorTelemetryData data where data.Code = {0}";

        public const string GetAllDataloggers = @"SELECT data FROM DataloggerData data left join fetch data.SensorTelemetrys sensors 
left join fetch sensors.Thresholds thresholds";

        public const string GetAllLprs = @"SELECT data FROM LprData data";

        public const string GetAllVideoDevices = @"SELECT data FROM VideoDeviceData data";

        public const string GetOperatorDeviceByDeviceCode = @"SELECT ObjectData FROM OperatorDeviceData ObjectData WHERE ObjectData.Device.Code = {0}";

        public const string GetOperatorDevicesByOperatorCode = @"SELECT ObjectData FROM OperatorDeviceData ObjectData WHERE ObjectData.User.Code = {0}";
        
        public const string GetAmountVideoDevicesByStructName =
               @"SELECT count(*) FROM VideoDeviceData objectData WHERE objectData.Struct.Name = '{0}'";

        public const string GetCamerasByStructName =            
                @"SELECT objectData FROM CameraData objectData WHERE objectData.Struct.Name = '{0}'";

        public const string GetCameraByCode =
                @"SELECT objectData FROM CameraData objectData WHERE objectData.Code = '{0}'";

        public const string GetSensorTelemetrysByStructName =
                @"SELECT objectData FROM DataloggerData objectData WHERE objectData.Struct.Name = '{0}'";

        public const string GetSensorTelemetrysByDataloggerName =
                @"SELECT objectData FROM SensorTelemetryData objectData 
                                    LEFT JOIN FETCH objectData.Thresholds thresholds 
                                    WHERE objectData.Datalogger.Name = '{0}'";

        public const string GetSensorTelemetrysAndThresholdsByDataloggerName =
                @"SELECT objectData 
                  FROM SensorTelemetryData objectData left join fetch objectData.Thresholds 
                  WHERE objectData.Datalogger.Name = '{0}'";
            
        public const string GetStructsByCctvZoneName =            
                @"SELECT objectData FROM StructData objectData WHERE objectData.Zone.Name = '{0}'";

        public const string GetStructByStructTypeCodeZoneCode =
                @"SELECT objectData FROM StructData objectData WHERE objectData.Type.Code = '{0}' AND objectData.Zone.Code = '{1}'";

        public const string GetCamerasByStructTypeName =
                @"SELECT objectData FROM CameraData objectData WHERE objectData.Struct.Type.Name = '{0}'";

        public const string GetSensorTelemetryByStructTypeName =
                @"SELECT objectData FROM SensorTelemetryData objectData WHERE objectData.Datalogger.Struct.Type.Name = '{0}'";

        public const string GetCamerasByStructTypeNameZoneName =
                @"SELECT objectData FROM CameraData objectData WHERE objectData.Struct.Type.Name = '{0}'
                                                               AND objectData.Struct.Zone.Name = '{1}'";

        public const string GetSensorTelemetryByStructTypeNameZoneName =
               @"SELECT objectData FROM SensorTelemetryData objectData WHERE objectData.Datalogger.Struct.Type.Name = '{0}'
                                                               AND objectData.Struct.Zone.Name = '{1}'";

        public const string GetCamerasByName =
               @"SELECT objectData FROM CameraData objectData WHERE objectData.Name = '{0}'";

        public const string GetCamerasByCustomId =
               @"SELECT objectData FROM CameraData objectData WHERE objectData.CustomId = '{0}'";

        public const string GetDataloggersByName =
             @"SELECT objectData FROM DataloggerData objectData WHERE objectData.Name = '{0}'";

        public const string GetDataloggerByCode =
            @"SELECT objectData FROM DataloggerData objectData WHERE objectData.Code = '{0}'";

        public const string GetSensorTelemetryByName =
             @"SELECT objectData FROM SensorTelemetryData objectData 
left join fetch objectData.Thresholds thresholds WHERE objectData.Name = '{0}'";

        public const string GetSensorTelemetryByCode =
            @"SELECT objectData FROM SensorTelemetryData objectData 
left join fetch objectData.Thresholds thresholds WHERE objectData.Code = {0}";

        public const string GetLprByCode =
           @"SELECT objectData FROM LprData objectData WHERE objectData.Code = {0}";

        public const string GetLprByName =
           @"SELECT objectData FROM LprData objectData WHERE objectData.Name = '{0}'";


        public const string GetAlarmLprByCode =
             @"SELECT objectData FROM AlarmLprData objectData WHERE objectData.Code = {0}";

        public const string GetAlarmsLprByStatus =
            @"SELECT data FROM AlarmLprData data Where data.Status = {0} and data.DeletedId is null";

        public const string GetCamerasByOperatorCode = @"SELECT camera 
                                                            FROM CameraData camera
                                                            LEFT JOIN camera.Operators operators
                                                            WHERE operators.User.Code = '{0}'";

        public const string GetCamerasByZoneName =
               @"SELECT objectData FROM CameraData objectData WHERE objectData.Struct.Zone.Name = '{0}'";

        public const string GetSensorTelemetrysByZoneName =
               @"SELECT objectData FROM SensorTelemetryData objectData WHERE objectData.Datalogger.Struct.Zone.Name = '{0}'";

        public const string GetStructsByCode = @"Select struct
                                                From StructData struct
                                                Where struct.Code = {0}";
        
        #endregion CCTV

        #region new indicators    

        public const string GetAllIndicatorsResultByindicatorCode = @"SELECT data FROM IndicatorResultData data 
left join fetch data.Result result 
      where data.Indicator.Code = {0} and result.IndicatorClass.Code = {1} and data.Time >'{2}'";

        public const string GetIndicatorByForeCastTimeAndByIndicatorCode = @"SELECT data FROM IndicatorGroupForecastData data 
                                                                    left join fetch data.ForecastValues result 
                                                                    where '{0}'> data.Start and 
                                                                    '{1}' < data.End and
                                                                     {2} = result.Indicator.Code ";
        #endregion new indicators

        public const string IncidentLoadReporBaseList = @"select incident
                                                            from IncidentData incident 
                                                                    left join fetch incident.SetReportBaseList child 
                                                                    left join fetch child.SetIncidentTypes 
                                                                    left join fetch child.SetAnswers answers
                                                                    left join fetch answers.SetAnswers possibleAnswers
                                                            where incident.Code = {0} and
                                                                  answers.Question.DeletedId is null and
                                                                  possibleAnswers.PhoneReportAnswer.Question.DeletedId is null and
                                                                  possibleAnswers.PossibleAnswer.DeletedId is null";

        public const string IncidentTypesByIncidentCode = @"select its 
                                                                from ReportBaseData rb left join rb.SetIncidentTypes its
                                                                where rb.Incident.Code = {0}";

        public const string CountUserRolesByUserProfile = @" SELECT count(ObjectData.Code) FROM UserRoleData ObjectData  WHERE '{0}' = some ( SELECT Prof.Code FROM ObjectData.Profiles Prof )";

        public const string CountIncidentTypesCustomCode = @"SELECT count(ObjectData.Code) FROM IncidentTypeData ObjectData WHERE ObjectData.Code != '{0}' AND ObjectData.CustomCode = '{1}'";

        public const string CountOfficersWithPosition = @"SELECT Count(*) FROM OfficerData ObjectData WHERE ObjectData.Position.Code = {0}";

        public const string CountOfficersWithPositionAndDepartment = @"SELECT Count(*) FROM OfficerData ObjectData WHERE ObjectData.Position.Code = {0} AND ObjectData.DepartmentStation.DepartmentZone.DepartmentType.Code = {1}";

        public const string GetUnitsByType = @"SELECT ObjectData FROM UnitData ObjectData JOIN ObjectData.Type UnitType WHERE  UnitType.Code = '{0}'";

        public const string GetUnitsByDepartmentStationCode = @"SELECT obj FROM UnitData obj WHERE obj.DepartmentStation.Code = {0}";

        public const string GetUnitCodesByType = @"SELECT obj.Code FROM UnitData obj WHERE obj.Type.Code = '{0}'";

        public const string GetUnitCustomCodesByType = @"SELECT obj.CustomCode FROM UnitData obj WHERE obj.Type.Code = '{0}'";

        public const string GetUnitCustomCodesByDepartmentStationCode = @"SELECT obj.CustomCode FROM UnitData obj WHERE obj.DepartmentStation.Code = {0}";

        public const string GetUnitsIncidentType = @"SELECT ObjectData FROM UnitData ObjectData WHERE  '{0}' = some ( SELECT IncType.Code FROM ObjectData.SetIncidentTypes IncType )";

        public const string GetTypeUnitIncidentType = @"SELECT ObjectData FROM UnitTypeData ObjectData  WHERE '{0}' = some ( SELECT IncType.Code FROM ObjectData.IncidentTypes IncType )";

        public const string GetRank = @"SELECT ObjectData FROM RankData ObjectData WHERE ObjectData.Code = {0}";

        public const string GetPosition = @"SELECT ObjectData FROM PositionData ObjectData WHERE ObjectData.Code = {0}";

        public const string GetCodesIncidentsByIncidentType = @"SELECT obj.Incident.Code FROM ReportBaseData obj INNER JOIN obj.SetIncidentTypes incType WHERE incType.Code = '{0}' AND obj.Incident.Status.Name = 'OPEN' AND obj.Incident.Address.IsSynchronized = '1'";

        public const string CountFutureUnitAssigns = @"SELECT count(ObjectData.Code) FROM UnitOfficerAssignHistoryData ObjectData  WHERE  ObjectData.Unit.Code = '{0}'  AND ObjectData.Start >= '{1}'";

        public const string GetCurrentUnitOfficerAssignsByUnitCodeCount = @"SELECT count(ObjectData.Code) FROM UnitOfficerAssignHistoryData ObjectData  WHERE  ObjectData.Unit.Code = '{0}'  AND ( ObjectData.Start <= '{1}' AND '{1}' < ObjectData.End )";

        public const string GetAlertNotificationNotEndedByUnitCount = @"SELECT count(notif.Code) FROM AlertNotificationData notif 
                                                                   WHERE notif.UnitAlert.Unit.Code = {0} 
                                                                        AND notif.Status != 2";

        public const string GetUnitAlertsByDepartmentTypeCode = @"SELECT objectData FROM UnitAlertData objectData WHERE objectData.Unit.DepartmentStation.DepartmentZone.DepartmentType.Code = {0} AND objectData.DeletedId IS NULL";

        public const string GetUnitsByDepartmentTypeCode = @"SELECT objectData FROM UnitData objectData WHERE objectData.DepartmentStation.DepartmentZone.DepartmentType.Code = {0} AND objectData.DeletedId IS NULL";
        
        public const string GetUnitOfficerAssignsByUnitCode = @"SELECT ObjectData FROM UnitOfficerAssignHistoryData ObjectData  WHERE  ObjectData.Unit.Code = '{0}'  AND ( (ObjectData.Start <= '{1}' AND '{1}' < ObjectData.End) OR (  ObjectData.Start >= '{1}' ) )";

        public const string GetUnitAssignsByUnitCode = @"SELECT ObjectData FROM UnitOfficerAssignHistoryData ObjectData  WHERE  ObjectData.Unit.Code = '{0}' ";

        public const string GetUnitAssignCodeByOfficerCode = @"SELECT ObjectData.Unit.Code FROM UnitOfficerAssignHistoryData ObjectData  WHERE  ObjectData.Officer.Code = '{0}'  AND ( (ObjectData.Start <= '{1}' AND '{1}' < ObjectData.End) OR (  ObjectData.Start >= '{1}' ) )";

        public const string GetDepartmentStationHistoryAssing = @"SELECT ObjectData FROM DepartmentStationAssignHistoryData ObjectData  WHERE  ObjectData.DepartmentStation.Code = '{0}'  AND ( (ObjectData.Start <= '{1}' AND '{1}' < ObjectData.End) OR (  ObjectData.Start >= '{1}' ) )";

        public const string GetUnitOfficerAssignsByOfficerCode = @"SELECT ObjectData FROM UnitOfficerAssignHistoryData ObjectData  WHERE  ObjectData.Officer.Code = '{0}'";

        public const string CountUnitOfficerAssignsByOfficerCode = @"SELECT count(ObjectData.Code) FROM UnitOfficerAssignHistoryData ObjectData  WHERE  ObjectData.Officer.Code = '{0}'  AND ( (ObjectData.Start <= '{1}' AND '{1}' < ObjectData.End) OR (  ObjectData.Start >= '{1}' ) )";

        public const string CountUnitOfficerCurrentAssignsByOfficerCode = @"SELECT count(ObjectData.Code) FROM UnitOfficerAssignHistoryData ObjectData  WHERE  ObjectData.Officer.Code = '{0}'  AND  (ObjectData.Start <= '{1}' AND '{1}' < ObjectData.End)";

        public const string CountOfficersWithRank = @"SELECT Count(*) FROM OfficerData ObjectData WHERE ObjectData.Rank.Code = {0}";

        public const string CountOfficersWithRankAndDepartment = @"SELECT Count(*) FROM OfficerData ObjectData WHERE ObjectData.Rank.Code = {0} AND ObjectData.DepartmentStation.DepartmentZone.DepartmentType.Code = {1}";

        public const string CountUnitOfficerFutureAssignsByOfficerCode = @"SELECT count(ObjectData.Code) FROM UnitOfficerAssignHistoryData ObjectData  WHERE  ObjectData.Officer.Code = '{0}'  AND ObjectData.Start >= '{1}'";
   
        public const string CountUnitsByUnitTypeCode = @"SELECT count(ObjectData.Code) FROM UnitData ObjectData  WHERE  ObjectData.Type.Code = '{0}'";

        public const string CountIncidentTypeCustomCodeInIncidentOpen = @"select count(distinct its.CustomCode)
                                                                from ReportBaseData rb inner join rb.SetIncidentTypes its
                                                                where rb.Incident.Status.CustomCode = 'OPEN' and 
                                                                      its.CustomCode = '{0}'";


        public const string CheckIncidentTypeCustomCodeInIncidentOpen = @"select count(*)
                                                                from ReportBaseData rb inner join rb.SetIncidentTypes its
                                                                where rb.Incident.EndDate is null and 
                                                                      its.Code = {0}";

        public const string GetUserProfiles = @"SELECT data FROM UserProfileData data";

		public const string GetUnitWorkShift = @"SELECT var FROM WorkShiftVariationData var Where var.ObjectType = 1";

		public const string GetUnitNotExpiredWorkShift = @"SELECT var FROM WorkShiftVariationData var 
                                                              WHERE var.ObjectType = 1 AND var.Code in (
																	SELECT distinct schedule.WorkShiftVariation.Code FROM WorkShiftScheduleVariationData schedule 
																		WHERE schedule.End > '{0}')";
		
		public const string GetRoutes = @"SELECT data FROM RouteData data";

        public const string GetGPS = @"SELECT data FROM GPSData data";
        public const string GetRoutesByDepartmentType = @"SELECT route FROM RouteData route WHERE route.DepartmentType.Code = {0}";

        public const string GetSensors = @"SELECT sensor FROM SensorData sensor Order By sensor.Name";
        
        public const string GetUserProfilesByRole = @"SELECT p FROM UserRoleData r left join r.Profiles p WHERE r.Code = {0}";

        public const string GetUserRoles = @"SELECT data FROM UserRoleData data";

		public const string GetWorkShiftVariationWithSchedulesByCode = @"SELECT distinct var 
                                                                    FROM WorkShiftVariationData var 
                                                                        left join fetch var.Schedules schedules
                                                                    WHERE var.Code = {0} AND var.ObjectType = 0";

        public const string GetWorkShiftVariationByCode = @"SELECT distinct var 
                                                                    FROM WorkShiftVariationData var 
                                                                    WHERE var.Code = {0} AND var.ObjectType = 0";

        /// <summary>
        /// 28-01-10 Fue cambiado porque la lista de operadores cambio.
        /// </summary>
        public const string GetWorkShiftVariationWithSchedulesByOperatorCode = @"SELECT distinct var 
                                                                    FROM WorkShiftVariationData var 
                                                                        inner join var.Operators operators
                                                                        left join fetch var.Schedules schedules
                                                                    WHERE operators.Operator.Code = {0} AND var.ObjectType = 0";

        public const string WorkShiftOperatorWithSchedulesByOperatorCode = @"SELECT var, operators
                                                                    FROM WorkShiftVariationData var 
                                                                        left join var.Operators operators
                                                                        left join fetch var.Schedules schedules
                                                                    WHERE operators.Operator.Code = {0} AND var.ObjectType = 0";
    
        public const string GetWorkShiftVariationByType = @"SELECT distinct ObjectData 
                                                            FROM WorkShiftVariationData ObjectData 
                                                                left join fetch ObjectData.Schedules schedules 
                                                            WHERE ObjectData.Type = {0} and ObjectData.ObjectType = 0 and '{1}' < any (select schedules.End from ObjectData.Schedules schedules)";
               
        public const string GetCountWSVByNameTypeObjectType = @"SELECT count(obj.Code) 
                                                                    FROM WorkShiftVariationData obj 
                                                                    WHERE obj.Code != {0} AND obj.Name = '{1}' AND obj.ObjectType = {2} AND (obj.Type = {3} OR obj.Type = {4})";

        public const string GetTimeOff = @"SELECT distinct ObjectData 
                                                            FROM WorkShiftVariationData ObjectData 
                                                                left join fetch ObjectData.Schedules schedules
                                                            WHERE ObjectData.Type = {0} and ObjectData.ObjectType = 0 and '{1}' < any (select schedules.End from ObjectData.Schedules schedules)";

        public const string GetWorkShift = @"SELECT distinct ObjectData 
                                                            FROM WorkShiftVariationData ObjectData 
                                                                left join fetch ObjectData.Schedules schedules 
                                                            WHERE (ObjectData.Type = {0} OR ObjectData.Type = {1}) and ObjectData.ObjectType = 0 and '{2}' < any (select schedules.End from ObjectData.Schedules schedules)";       

        public const string GetWorkShiftScheduleVariationByType = @"SELECT ObjectData.Schedules FROM WorkShiftVariationData ObjectData WHERE ObjectData.Type = {0} and ObjectData.ObjectType = 0 ";

		public const string GetIncidentTypes = @"SELECT data FROM IncidentTypeData data";

        public const string GetEmergencyIncidentType = @"SELECT incType FROM IncidentTypeData incType WHERE incType.NoEmergency = false";

        public const string GetIncidentTypesByUnitCode = @"SELECT incidents FROM UnitData unit inner join unit.SetIncidentTypes incidents WHERE unit.Code = {0}";

        public const string GetIncidentTypesInitialData = @"SELECT incType 
                                                            FROM IncidentTypeData incType 
                                                                LEFT JOIN FETCH incType.SetIncidentTypeQuestions setIncType";

        public const string GetTrainingCourseByCode = @"SELECT ObjectData FROM TrainingCourseData ObjectData WHERE ObjectData.Code = {0}";

        public const string GetTrainingCourses = @"SELECT distinct ObjectData 
                                                    FROM TrainingCourseData ObjectData
                                                    LEFT JOIN FETCH ObjectData.Schedules";

        public const string GetTrainingCourseSchedulePartsByScheduleCode = @"SELECT ObjectData FROM TrainingCourseSchedulePartsData ObjectData WHERE ObjectData.TrainingCourseSchedule.Code = {0}";

        public const string GetTrainingCourseScheduleByCourseCode = @"SELECT ObjectData 
                                                                        FROM TrainingCourseScheduleData ObjectData 
                                                                        WHERE ObjectData.TrainingCourse.Code = {0}";

        public const string GetTrainingCourseScheduleWithOperators = @"SELECT ObjectData 
                                                                        FROM TrainingCourseScheduleData ObjectData 
                                                                            left join fetch ObjectData.Operators
                                                                            order by ObjectData.Code";

        public const string GetTrainingCourseScheduleWithParts = @"SELECT ObjectData 
                                                                    FROM TrainingCourseScheduleData ObjectData 
                                                                        left join fetch ObjectData.Parts
                                                                        order by ObjectData.Code";
        
        public const string GetTrainingCourseScheduleByCourseCodeWithScheduleParts = @"SELECT ObjectData 
                                                                                        FROM TrainingCourseScheduleData ObjectData 
                                                                                            left join fetch ObjectData.Parts 
                                                                                        WHERE ObjectData.TrainingCourse.Code = {0}";

        public const string GetTrainingCourseScheduleByOperatorCode = @"SELECT schedule.Parts, schedule
                                                                        FROM OperatorTrainingCourseData ObjectData left join
                                                                             ObjectData.TrainingCourseSchedule schedule
                                                                             left join fetch schedule.Parts
                                                                        WHERE ObjectData.Operator.Code = {0} and 
                                                                              '{1}' < any (select part.End from schedule.Parts part) and
                                                                              ((schedule.Start < '{2}' and '{2}' <= schedule.End) or
                                                                              (schedule.Start < '{3}' and '{3}' <= schedule.End))";

        public const string GetHistorySchedulesByCourseCode = @"SELECT ObjectData FROM TrainingCourseScheduleData ObjectData WHERE ObjectData.TrainingCourse.Code = {0} AND ObjectData.End < '{1}'";

        public const string GetChangeableSchedulesByCourseCode = @"SELECT ObjectData 
                                                                   FROM TrainingCourseScheduleData ObjectData
                                                                   WHERE ObjectData.TrainingCourse.Code = {0} AND 
                                                                         '{1}' < all (select part.Start from ObjectData.Parts part)";

        public const string GetRealStartDateTrainingCourseSchedule = @"select min(part.Start)
                                                                        from TrainingCourseScheduleData schedule left join
                                                                             schedule.Parts part
                                                                        where schedule.Code = {0}";

        public const string GetRealEndDateTrainingCourseSchedule = @"select max(part.End)
                                                                        from TrainingCourseScheduleData schedule left join
                                                                             schedule.Parts part
                                                                        where schedule.Code = {0}";

        public const string GetCurrentOrFinishedSchedulesByCourseCode = @"SELECT ObjectData FROM TrainingCourseScheduleData ObjectData WHERE ObjectData.TrainingCourse.Code = {0} AND (ObjectData.End < '{1}' OR (ObjectData.End >= '{1}' AND ObjectData.Start < '{1}'))";

        public const string GetCurrentSchedulesByCourseCode = @"SELECT ObjectData FROM TrainingCourseScheduleData ObjectData WHERE ObjectData.TrainingCourse.Code = {0} AND ObjectData.End >= '{1}' AND ObjectData.Start <= '{1}'";

        public const string GetOperatorsTrainingCourseByScheduleCode = @"SELECT ObjectData FROM OperatorTrainingCourseData ObjectData WHERE ObjectData.TrainingCourseSchedule.Code = {0}";
        
        public const string GetOperatorsTrainingCourseByCourseCode = @"SELECT ObjectData 
                                                                       FROM OperatorTrainingCourseData ObjectData 
                                                                       WHERE ObjectData.TrainingCourseSchedule.TrainingCourse.Code = {0} and
                                                                             ObjectData.TrainingCourseSchedule.DeletedId is null";

        public const string GetOperatorsTrainingCourseByOperatorCode = @"SELECT ObjectData FROM OperatorTrainingCourseData ObjectData WHERE ObjectData.Operator.Code = {0}";

        public const string GetQuestions = @"SELECT ObjectData FROM QuestionData ObjectData";

        public const string GetQuestionsDispatchReport = @"SELECT ObjectData FROM QuestionDispatchReportData ObjectData";

        public const string GetQuestionsDispatchReportWithoutRequired = @"SELECT ObjectData FROM QuestionDispatchReportData ObjectData WHERE ObjectData.Required = false";

        public const string GetQuestionsInitialData = @"SELECT ObjectData 
                                                        FROM QuestionData ObjectData 
                                                        left join fetch ObjectData.SetPossibleAnswers 
                                                        left join fetch ObjectData.IncidentTypes";

        public const string GetOfficers = @"SELECT ObjectData FROM OfficerData ObjectData";

        public const string GetOfficersWithRank = @"SELECT ObjectData FROM OfficerData ObjectData WHERE ObjectData.Rank.Code = {0}";

        public const string GetOfficersWithPosition = @"SELECT ObjectData FROM OfficerData ObjectData WHERE ObjectData.Position.Code = {0}";

        public const string GetOfficersByDepartmentStationCode = @"SELECT offic FROM OfficerData offic WHERE offic.DepartmentStation.Code = {0}";

        public const string GetOfficersByPersonID = @"SELECT ObjectData FROM OfficerData ObjectData WHERE ObjectData.PersonId = '{0}'";

        public const string GetOfficerByCode = @"SELECT officer FROM OfficerData officer WHERE officer.Code = {0}";

        public const string GetIncidentTypeDepartmentTypeDataByIncidentTypeCode = @" SELECT ObjectData FROM IncidentTypeDepartmentTypeData ObjectData JOIN ObjectData.IncidentType IncType WHERE IncType.Code = '{0}' ";

        public const string GetOperatorsWithoutSupervisor = @"SELECT ObjectData FROM OperatorData ObjectData WHERE ObjectData.PersonId != '6666666666666'";

        public const string GetFirstLevelOperatorsCodeFirstAndLastNameWithoutSupervisor = @"SELECT distinct ObjectData.Code, ObjectData.FirstName, ObjectData.LastName
                                                                                            FROM OperatorData ObjectData 
                                                                                            inner join ObjectData.Role role 
                                                                                            inner join role.Profiles profile 
                                                                                            inner join profile.Accesses access 
                                                                                            WHERE access.Name like '%UserApplicationData+Basic+FirstLevel%'"; 



        public const string GetDispatchOperatorsCodeFirstAndLastNameWithoutSupervisor = @"SELECT distinct ObjectData.Code, ObjectData.FirstName, ObjectData.LastName
                                                                                        FROM OperatorData ObjectData 
                                                                                        inner join ObjectData.DepartmentTypes dep 
                                                                                        inner join ObjectData.Role role 
                                                                                        inner join role.Profiles profile 
                                                                                        inner join profile.Accesses access 
                                                                                        WHERE access.Name like '%UserApplicationData+Basic+Dispatch%'AND dep.Code = {0}";

        public const string SeekOfficerByFirstNameBadgeLastNamePersonIdDepartamentRankPosition = 
									@"SELECT ObjectData 
									FROM OfficerData ObjectData LEFT JOIN ObjectData.Position Pos LEFT JOIN 
											ObjectData.DepartmentStation.DepartmentZone.DepartmentType Dep  
									WHERE (ObjectData.FirstName LIKE '%{0}%' OR ObjectData.Badge LIKE '%{0}%' OR ObjectData.LastName LIKE '%{0}%' OR
										ObjectData.PersonId LIKE '%{0}%' OR Dep.Name LIKE '%{0}%') OR ( (Pos IS NOT NULL) AND ( Pos.Name LIKE '%{0}%') ) ";

		public const string GetFirstLevelOperatorsWithWorkShifts = @"SELECT ObjectData 
                                                                                    FROM OperatorData ObjectData
                                                                                    left join fetch ObjectData.WorkShifts ws    
                                                                                    left join ObjectData.Role.Profiles profile
                                                                                    inner join profile.Accesses access
                                                                                    WHERE (access.Name like '%UserApplicationData+Basic+FirstLevel%' OR access.Name like '%FirstLevelSupervisor%') 
                                                                                               AND ObjectData.Code not in (SELECT oper.Code
                                                                                                           FROM OperatorData oper
                                                                                                           left join oper.Role.Profiles profileAux
                                                                                                           inner join profileAux.Accesses accessAux
                                                                                                           WHERE accessAux.Name like '%GeneralSupervisorName%')";


		public const string GetDispatchOperatorsWithWorkShifts = @"SELECT ObjectData 
                                                                                   FROM OperatorData ObjectData 
                                                                                       left join fetch ObjectData.WorkShifts ws 
                                                                                       inner join ObjectData.DepartmentTypes dep 
                                                                                       left join ObjectData.Role.Profiles profile 
                                                                                       inner join profile.Accesses access 
                                                                                   WHERE (access.Name like '%UserApplicationData+Basic+Dispatch%' OR access.Name like '%DispatchSupervisor%')
                                                                                       and dep.Code = {0}
                                                                                       AND ObjectData.Code not in (SELECT oper.Code
                                                                                                           FROM OperatorData oper
                                                                                                           left join oper.Role.Profiles profileAux
                                                                                                           inner join profileAux.Accesses accessAux
                                                                                                           WHERE accessAux.Name like '%GeneralSupervisorName%')";


		public const string GetFirstLevelOperators = @"SELECT ObjectData 
                                                        FROM OperatorData ObjectData
                                                        left join ObjectData.Role.Profiles profile
                                                        left join profile.Accesses access
                                                        WHERE access.Name like '%UserApplicationData+Basic+FirstLevel%'";



		public const string GetDispatchOperators = @"SELECT ObjectData 
													   FROM OperatorData ObjectData 
														   left join ObjectData.DepartmentTypes dep 
														   left join ObjectData.Role.Profiles profile 
														   left join profile.Accesses access 
													   WHERE (access.Name like '%UserApplicationData+Basic+Dispatch%')
														   and dep.Code = {0}";


		public const string GetFirstLevelSupervisors = @"SELECT ObjectData 
                                                            FROM OperatorData ObjectData
                                                             left join ObjectData.Role.Profiles profile
                                                            left join profile.Accesses access
                                                            WHERE (access.Name like '%FirstLevelSupervisor%') 
                                                                       AND ObjectData.Code not in (SELECT oper.Code
                                                                                   FROM OperatorData oper
																					   left join oper.Role.Profiles profileAux
																					   left join profileAux.Accesses accessAux
																				   WHERE accessAux.Name like '%GeneralSupervisorName%')";


		public const string GetDispatchSupervisors = @"SELECT ObjectData 
                                                       FROM OperatorData ObjectData 
                                                           left join ObjectData.DepartmentTypes dep 
                                                           left join ObjectData.Role.Profiles profile 
                                                           left join profile.Accesses access 
                                                       WHERE access.Name like '%DispatchSupervisor%'
                                                           and dep.Code = {0}
                                                           AND ObjectData.Code not in (SELECT oper.Code
                                                                               FROM OperatorData oper
                                                                               left join oper.Role.Profiles profileAux
                                                                               left join profileAux.Accesses accessAux
                                                                               WHERE accessAux.Name like '%GeneralSupervisorName%')";


		public const string GetRolesCodeByAccessWithoutGeneralSupervisor= @"SELECT role.Code 
                                                    FROM UserRoleData role
                                                    inner join role.Profiles profile
                                                    inner join profile.Accesses access
                                                    WHERE (access.Name like '%{0}%' OR access.Name like '%{1}%') 
                                                          AND role.Code not in (SELECT roleAux.Code
                                                               FROM UserRoleData roleAux
                                                               left join roleAux.Profiles profileAux
                                                               left join profileAux.Accesses accessAux
                                                               WHERE accessAux.Name like '%GeneralSupervisorName%')";

        public const string GetRolesCodeByAccess = @"SELECT role.Code 
                                                     FROM UserRoleData role
                                                          inner join role.Profiles profile
                                                          inner join profile.Accesses access
                                                     WHERE access.UserApplication.Name = '{0}'";

        public const string SeekQuestionsByTextShortText = @"SELECT ObjectData FROM QuestionData ObjectData WHERE (ObjectData.ShortText LIKE '%{0}%' OR ObjectData.Text LIKE '%{0}%' )";

        public const string SeekIncidentTypesByFriendlyNameCustomCode = @"SELECT ObjectData FROM IncidentTypeData ObjectData WHERE (ObjectData.FriendlyName LIKE '%{0}%' OR ObjectData.CustomCode LIKE '%{0}%')";

        public const string SeekUnitTypeByNameDepartamentType = @"SELECT ObjectData FROM UnitTypeData ObjectData JOIN ObjectData.DepartmentType DepType WHERE (ObjectData.Name LIKE '%{0}%' OR DepType.Name LIKE '%{0}%')";

        public const string SeekOperatorByFirstNameLastNamePersonIdWithoutSupervisor = @" SELECT ObjectData FROM OperatorData ObjectData  WHERE  ObjectData.PersonId != '6666666666666' AND (ObjectData.LastName LIKE '%{0}%' OR ObjectData.FirstName LIKE '%{0}%' OR ObjectData.PersonId LIKE '%{0}%')";
        
        public const string SeekEvaluationsByName = @" SELECT ObjectData FROM EvaluationData ObjectData WHERE ObjectData.Name LIKE '%{0}%'";    
		
		public const string SeekOperatorStatusByFriendlyName = @" SELECT ObjectData FROM OperatorStatusData ObjectData WHERE ObjectData.FriendlyName LIKE '%{0}%'";    

        public const string SeekUserRoleByFriendlyName = @"SELECT ObjectData FROM UserRoleData ObjectData WHERE ObjectData.FriendlyName LIKE '%{0}%' ";

 
        public const string SeekUserProfilesByFriendlyName = @"SELECT obj FROM UserProfileData obj WHERE obj.FriendlyName LIKE '%{0}%'";

        public const string SeekUnitsByCustomCodeDepartamentTypeUnitType = @"SELECT ObjectData FROM UnitData ObjectData JOIN ObjectData.DepartmentStation.DepartmentZone.DepartmentType DepTypeData JOIN ObjectData.Type UTypeData WHERE (ObjectData.CustomCode LIKE '%{0}%' OR DepTypeData.Name LIKE '%{0}%' OR UTypeData.Name LIKE '%{0}%')";

        public const string SeekCctvZonesByName = @"SELECT obj FROM CctvZoneData obj WHERE obj.Name LIKE '%{0}%'";

        public const string SeekStructsByNameTypeZoneStateTown = @"SELECT obj FROM StructData obj WHERE obj.Name LIKE '%{0}%' OR obj.Type.Name LIKE '%{0}%' OR obj.Zone.Name LIKE '{0}' OR obj.Addres.State LIKE '%{0}%' OR obj.Addres.Town LIKE '%{0}%'" ;

        
        public const string SeekCamerasByNameIPModeZoneStruct = @"SELECT obj FROM CameraData obj WHERE obj.Name LIKE '%{0}%' OR obj.Type.Name LIKE '%{0}%' OR obj.Ip LIKE '%{0}%' OR obj.IpServer LIKE '%{0}%' OR obj.Struct.Name LIKE '%{0}%' OR obj.Struct.Zone.Name LIKE '%{0}%'";        

        public const string GetDepartmentsTypeWithStationsAssociatedWithAlreadyNotifiedDepartmentsByIncidentCode =
                    @"SELECT obj FROM DepartmentTypeData obj
                                WHERE obj.Dispatch = true AND obj.DeletedId is null AND 
                                (SELECT count(unit.Code) FROM UnitData unit
                                    WHERE unit.DepartmentStation.DepartmentZone.DepartmentType.Code = obj.Code) > 0 AND
                                (SELECT count(rbdt) from ReportBaseDepartmentTypeData rbdt
                                 WHERE rbdt.ReportBase.Incident.Code = {0} and rbdt.DepartmentType.Code = obj.Code) = 0
                                ORDER BY obj.Name";

        public const string GetCustomCodeAndNameFromIncidentTypeByIncident = @"SELECT sit.CustomCode, sit.Name, sit.Code 
                                                                                FROM ReportBaseData rb left join rb.SetIncidentTypes sit 
                                                                                WHERE rb.Incident.Code = {0}";

        public const string GetIncidentTypeNamesFromUnit = @"SELECT sit.Name 
                                                                FROM UnitData unit left join unit.SetIncidentTypes sit 
                                                                WHERE unit.Code = {0}";
        
        public const string GetIncidentTypeFromUnit = @"SELECT sit.Name 
                                                                FROM UnitData unit left join unit.SetIncidentTypes sit 
                                                                WHERE unit.Code = {0}";

        public const string GetIncidentTypeByUnit = @"SELECT sit 
                                                                FROM UnitData unit inner join unit.SetIncidentTypes sit 
                                                                WHERE unit.Code = {0} and sit is not null";

        public const string IncidentTypeCodesByReportBaseCode = @"select sit.Code
                                                                  from ReportBaseData rb left join rb.SetIncidentTypes sit
                                                                  where rb.Code = {0}";

        public const string FindSpecificAnswerInAnIncident = @"select paad.PhoneReportAnswer.PhoneReport.Incident.Code, count(*) from PossibleAnswerAnswerData paad where paad.TextAnswer like '%{1}%' and paad.PhoneReportAnswer.PhoneReport.Incident.Code in {0} group by paad.PhoneReportAnswer.PhoneReport.Incident.Code";

        public const string FindSpecificAnswerInAnCctvIncident =@"select paad.CctvReportAnswer.CctvReport.Camera.Code, count(*) from PossibleAnswerAnswerData paad where paad.TextAnswer like '%{1}%' and paad.CctvReportAnswer.CctvReport.Camera.Code in {0} group by paad.CctvReportAnswer.CctvReport.Camera.Code";

        public const string GetLastIncidentNotificationRealStatus = @"SELECT inshd FROM IncidentNotificationStatusHistoryData inshd 
                                                                        WHERE inshd.Status.CustomCode != 'UPDATED' AND
                                                                        inshd.Status.CustomCode != 'AUTOMATIC_SUPERVISOR' AND
                                                                        inshd.Status.CustomCode != 'MANUAL_SUPERVISOR' AND
                                                                        inshd.Status.CustomCode != 'REASSIGNED' AND
                                                                        inshd.Status.CustomCode != 'NEW' AND
                                                                        inshd.End IS NOT NULL AND
                                                                        inshd.IncidentNotification.Code = {0}
                                                                        ORDER BY inshd.End DESC";

        public const string SeekRepetedAssings = @"SELECT ObjectData FROM DepartmentStationAssignHistoryData ObjectData
                              WHERE ObjectData.DepartmentStation.Code = {0}
                              AND ObjectData.Start = '{1}' 
                              AND ObjectData.End = '{2}' ";

        public const string IntersectDepartmentStationAssign = @"SELECT count(ObjectData) FROM DepartmentStationAssignHistoryData ObjectData  JOIN ObjectData.DepartmentStation Station
             WHERE Station.Code = '{0}' 
             AND  ObjectData.Code != {1}
             AND
                ( ( ObjectData.Start <= '{2}'  AND '{2}' < ObjectData.End )
                     OR 
                  (ObjectData.Start <= '{3}'  AND '{3}' < ObjectData.End )
                     OR 
                  ( '{2}' <= ObjectData.Start AND  ObjectData.Start  < '{3}')
                     OR 
                  ( '{2}' <= ObjectData.End AND  ObjectData.End  < '{3}'))";

        public const string FindIncidentTypesInCodeList = @"select itd from IncidentTypeData itd where itd.Code in {0}";

        public const string ReportBaseCountIncident = @"select count(*) from ReportBaseData rbd where rbd.Incident.Code = {0}";

        public const string PhoneReportCountIncident = @"select count(*) from PhoneReportData prd where prd.Incident.Code = {0}";

        public const string CctvReportCountIncident = @"select count(*) from CctvReportData prd where prd.Incident.Code = {0}";

        public const string AlarmReportCountIncident = @"select count(*) from AlarmReportData prd where prd.Incident.Code = {0}";

        public const string TwitterReportCountIncident = @"select count(*) from TwitterReportData prd where prd.Incident.Code = {0}";

        public const string LprReportCountIncident = @"select count(*) from AlarmLprReportData prd where prd.Incident.Code = {0}";

        public const string GetIncidentCodesInRatioHql = @" SELECT inc.Code FROM IncidentData inc WHERE
                                                         inc.Address.Lon BETWEEN {0} AND {1} AND inc.Address.Lat BETWEEN {2} AND {3} ";

        public const string GetQuestionShorTextHql = @" SELECT question.ShortText FROM QuestionData question WHERE
                                                         question.Code  = {0}";

        public const string GetCountLoggedInOperatorUsingOperatorStatus = @"SELECT count(*) FROM SessionHistoryData history, SessionStatusHistoryData historyStatus 
                                                                        where history.IsLoggedIn = true and 
                                                                        history.Code = historyStatus.Session.Code and 
                                                                        historyStatus.EndDate is null and 
                                                                        historyStatus.Status.Name = '{0}'";

        public const string GetLoggedInOperatorUsingOperatorStatus = @"SELECT historyStatus FROM SessionHistoryData history, SessionStatusHistoryData historyStatus 
                                                                        where history.IsLoggedIn = true and 
                                                                        history.Code = historyStatus.Session.Code and 
                                                                        historyStatus.EndDate is null and 
                                                                        historyStatus.Status.Name = '{0}'";

        public const string GetUnitsByCustomCodeTypeAndDepartmentTypeName = @"SELECT unit 
                                                                            FROM UnitData unit 
                                                                                LEFT JOIN unit.DepartmentStation station
                                                                                LEFT JOIN station.DepartmentZone zone
                                                                                LEFT JOIN zone.DepartmentType dt
                                                                            WHERE (unit.CustomCode like '%{0}%' OR dt.Name like '%{0}%' OR unit.Type.Name like '%{0}%') AND unit.GPS.IsSynchronized = true";


        public const string GetUnitByGps = "SELECT count(ObjectData.Code) " +
                    " FROM UnitData ObjectData " +
                    " WHERE " +
                    " (ObjectData.GPS is not null  AND ObjectData.GPS.Code = '{0}')" +
                    " AND (ObjectData.Code != '{1}') ";

        public const string GetCountDepartmentZoneByCustomCode = 
                    "SELECT count(ObjectData.Code) " +
                    " FROM DepartmentZoneData ObjectData " +
                    " WHERE " +
                    " (ObjectData.CustomCode is not null  AND ObjectData.CustomCode = '{0}')" +
                    " AND (ObjectData.Code != '{1}' AND ObjectData.DepartmentType.Code = {2}) ";

        public const string GetCountDepartmentStationByCustomCode =
                    "SELECT count(ObjectData.Code) " +
                    " FROM DepartmentStationData ObjectData " +
                    " WHERE " +
                    " (ObjectData.CustomCode is not null  AND ObjectData.CustomCode = '{0}')" +
                    " AND ObjectData.Code != {1} AND ObjectData.DepartmentZone.DepartmentType = {2} ";

        public const string GetCountDepartmentStationByNameByDepartmentType =
                    "SELECT count(ObjectData.Code) " +
                    " FROM DepartmentStationData ObjectData " +
                    " WHERE " +
                    " ObjectData.Name = '{0}' AND ObjectData.Code != {1} AND " +
                    " ObjectData.DepartmentZone.DepartmentType.Code = {2} ";

        public const string CheckDepartmentTypeChanged =
                    @"SELECT count(zone.Code) FROM DepartmentZoneData zone 
                        WHERE zone.Code = {0} AND zone.DepartmentType.Code != {1}";

        public const string CheckDepartmentZoneChanged =
                    @"SELECT count(station.Code) FROM DepartmentStationData station 
                        WHERE station.Code = {0} AND station.DepartmentZone.Code != {1}";

        public const string GetUnitByIdRadio = "SELECT count(ObjectData.Code) " +
                    " FROM UnitData ObjectData " +
                    " WHERE " +
                    " (ObjectData.IdRadio is not null  AND ObjectData.IdRadio = '{0}')" +
                    " AND (ObjectData.Code != '{1}') ";

		public const string IsAccessInRole = "select count(*) from UserRoleData role " +
												"left join role.Profiles profile " +
												"inner join profile.Accesses access " +
												"where role.Code = {0} and access.Name like '%{1}%'";

        public const string ActiveApplicationList = "select Distinct(work.InstalledApps) from WorkStationData work";
                                                


		public const string IsApplicationInRole = "select count(*) from UserRoleData role " +
												"left join role.Profiles profile " +
												"left join profile.Accesses access " +
												"where role.Code = {0} and access.UserApplication.Name like '%{1}%'";

		public const string IsApplicationAccessInRoleForSupervisor = "select count(*) from UserRoleData role " +
												"left join role.Profiles profile " +
												"inner join profile.Accesses access " +
												"where role.Code = {0} and access.Name like '%{1}%' and access.Name like '%{2}%'";


		public const string IsAccessInOperator = @"select count(*) 
                                             from OperatorData ope
                                                  left join ope.Role.Profiles profile 
                                                  inner join profile.Accesses access
                                             where ope.Code = {0} and access.Name like '%{1}%'";

      
		public const string IsSupervisorUser = "select count(*) from UserRoleData role " +
                                                "left join role.Profiles profile " +
                                                "inner join profile.Accesses access " +
                                                "where role.Code = {0} and (access.Name like '%{1}%' OR access.Name like '%{2}%')";

        public const string IsSupervisor = @"select count(*) 
                                             from OperatorData ope
                                                  left join ope.Role.Profiles profile 
                                                  inner join profile.Accesses access
                                             where ope.Code = {0} and (access.Name like '%{1}%' OR access.Name like '%{2}%')";

        public const string GetSupervisorsForOperator =
            @"SELECT ObjectData FROM OperatorData ObjectData inner join OperatorAssignData OperatorAssign WHERE ObjectData.Code = OperatorAssign.Supervisor.Code AND OperatorAssign.SupervisedOperator.Code = {0}";

        public const string GetOperatorProgram =
            @"SELECT ObjectData FROM OperatorAssignData ObjectData WHERE ObjectData.SupervisedOperator.Code = {0}";


        public const string GetOperatorsForSupervisor =
            @"SELECT oad.SupervisedOperator From OperatorAssignData oad  
                WHERE oad.Supervisor.Code = {0} AND
                '{1}' BETWEEN oad.StartDate AND oad.EndDate";

        public const string GetAllDayOperatorsForSupervisor =
            @"SELECT operator from OperatorData operator where operator.Code in
                (SELECT distinct oad.SupervisedOperator.Code From OperatorAssignData oad  
                WHERE oad.Supervisor.Code = {0} AND                
                oad.StartDate between '{1}' and '{2}')";

        public const string GetCurrentSupervisorsCodeByOperatorByApplication =
            @"SELECT oad.Supervisor.Code From OperatorAssignData oad
                left join oad.Supervisor.Role.Profiles profileSup
                inner join profileSup.Accesses accessSup  
                left join oad.SupervisedOperator.Role.Profiles profOper
                inner join profOper.Accesses accessOper
                WHERE oad.SupervisedOperator.Code = {0} AND
                '{1}' BETWEEN oad.StartDate AND oad.EndDate AND
                accessSup.Name like '%{2}%' AND
                accessOper.Name like '%{2}%'";

		public const string GetCurrentSupervisorsByOperatorByApplication =
			@"SELECT oad.Supervisor From OperatorAssignData oad
                left join oad.Supervisor.Role.Profiles profileSup
                inner join profileSup.Accesses accessSup  
                left join oad.SupervisedOperator.Role.Profiles profOper
                inner join profOper.Accesses accessOper
                WHERE oad.SupervisedOperator.Code = {0} AND
                '{1}' BETWEEN oad.StartDate AND oad.EndDate AND
                accessSup.Name like '%{2}%' AND
                accessOper.Name like '%{2}%'";

        public const string GetTodaysSupervisorsByOperatorByApplication =
             @"SELECT operator from OperatorData operator where operator.Code in
                (SELECT distinct oad.Supervisor.Code From OperatorAssignData oad
                left join oad.Supervisor.Role.Profiles profileSup
                inner join profileSup.Accesses accessSup  
                left join oad.SupervisedOperator.Role.Profiles profOper
                inner join profOper.Accesses accessOper
                WHERE oad.SupervisedOperator.Code = {0} AND
                '{1}' < oad.StartDate OR '{1}' < oad.EndDate AND
                accessSup.Name like '%{2}%' AND
                accessOper.Name like '%{2}%')";


        public const string GetLoggedInSupervisorByOperatorCode =
            @"SELECT operator 
              FROM OperatorData operator 
              WHERE operator.Code in
                   (SELECT distinct oad.Supervisor.Code 
                    FROM OperatorAssignData oad 
                    WHERE oad.SupervisedOperator.Code = {0} AND 
                          oad.EndDate > '{1}' AND oad.EndDate < '{2}' AND
                          oad.Supervisor.Code IN (SELECT session.UserAccount.Code 
                                                  FROM SessionHistoryData session
                                                  WHERE session.IsLoggedIn = true))";

        public const string GetLoggedInSupervisorCodesByOperatorCode =
            @"SELECT distinct oad.Supervisor.Code 
                            FROM OperatorAssignData oad 
                            WHERE oad.SupervisedOperator.Code = {0} AND 
                                  oad.EndDate > '{1}' AND oad.EndDate < '{2}' AND
                                  oad.Supervisor.Code IN (SELECT session.UserAccount.Code 
                                                          FROM SessionHistoryData session
                                                          WHERE session.IsLoggedIn = true)";

        public const string GetTodaysOperatorsBySupervisorByApplication =
            @"SELECT operator from OperatorData operator where operator.Code in
                (SELECT distinct oad.SupervisedOperator.Code From OperatorAssignData oad
                left join oad.Supervisor.Role.Profiles profileSup
                inner join profileSup.Accesses accessSup  
                left join oad.SupervisedOperator.Role.Profiles profOper
                inner join profOper.Accesses accessOper
                WHERE oad.Supervisor.Code = {0} AND
                '{1}' < oad.StartDate OR '{1}' < oad.EndDate AND
                accessSup.Name like '%{2}%' AND
                accessOper.Name like '%{2}%')";


        public const string GetLoggedInFirstLevelOperators = @"SELECT oper 
                                                             FROM OperatorData oper
                                                                  left join oper.Role.Profiles profile
                                                                  left join profile.Accesses access
                                                             WHERE access.Name = 'UserApplicationData+Basic+FirstLevel' AND
                                                                   oper.Code IN (SELECT session.UserAccount.Code 
                                                                                FROM SessionHistoryData session
                                                                                WHERE session.IsLoggedIn = true AND
                                                                                      session.UserApplication.Name = '{0}')";

        public const string GetLoggedInFirstLevelOperatorsCodes = @"SELECT oper.Code 
                                                             FROM OperatorData oper
                                                                  left join oper.Role.Profiles profile
                                                                  left join profile.Accesses access
                                                             WHERE access.Name = 'UserApplicationData+Basic+FirstLevel' AND
                                                                   oper.Code IN (SELECT session.UserAccount.Code 
                                                                                FROM SessionHistoryData session
                                                                                WHERE session.IsLoggedIn = true AND
                                                                                      session.UserApplication.Name = '{0}')";

        public const string GetLoggedInDispatchOperators = @"SELECT oper 
                                                             FROM OperatorData oper
                                                                  left join oper.DepartmentTypes dep
                                                                  left join oper.Role.Profiles profile
                                                                  left join profile.Accesses access
                                                             WHERE dep.Code = {0} AND
                                                                   access.Name = 'UserApplicationData+Basic+Dispatch' AND
                                                                   oper.Code IN (SELECT session.UserAccount.Code 
                                                                                FROM SessionHistoryData session
                                                                                WHERE session.IsLoggedIn = true AND
                                                                                      session.UserApplication.Name = '{1}')";

        public const string GetLoggedInDispatchOperatorsCodes = @"SELECT oper.Code 
                                                             FROM OperatorData oper
                                                                  left join oper.DepartmentTypes dep
                                                                  left join oper.Role.Profiles profile
                                                                  left join profile.Accesses access
                                                             WHERE dep.Code = {0} AND
                                                                   access.Name = 'UserApplicationData+Basic+Dispatch' AND
                                                                   oper.Code IN (SELECT session.UserAccount.Code 
                                                                                FROM SessionHistoryData session
                                                                                WHERE session.IsLoggedIn = true AND
                                                                                      session.UserApplication.Name = '{1}')";


        public const string GetLoggedInOperatorsSupervisedBySupervisor =
            @"SELECT operator 
              FROM OperatorData operator 
              WHERE operator.Code in (SELECT distinct oad.SupervisedOperator.Code 
                                      FROM OperatorAssignData oad 
                                      WHERE oad.Supervisor.Code = {0} AND 
                                            oad.EndDate > '{1}' AND oad.EndDate < '{2}' AND
                                            oad.SupervisedOperator.Code IN (SELECT session.UserAccount.Code 
                                                                            FROM SessionHistoryData session
                                                                            WHERE session.IsLoggedIn = true AND
                                                                                  session.UserApplication.Name = '{3}'))";

        public const string GetLoggedInOperatorsSupervisedCodesBySupervisor =
            @"SELECT distinct oad.SupervisedOperator.Code 
                                              FROM OperatorAssignData oad 
                                              WHERE oad.Supervisor.Code = {0} AND 
                                                    oad.EndDate > '{1}' AND oad.EndDate < '{2}' AND
                                                    oad.SupervisedOperator.Code IN (SELECT session.UserAccount.Code 
                                                                                    FROM SessionHistoryData session
                                                                                    WHERE session.IsLoggedIn = true AND
                                                                                          session.UserApplication.Name = '{3}')";
        

		public const string GetLoggedInGeneralSupervisors =
			@"SELECT oper From  OperatorData oper 
				left join oper.Role.Profiles profileSup
				inner join profileSup.Accesses accessSup  
                WHERE accessSup.Name = 'GeneralSupervisorName+Basic+Supervision' AND
                      oper.Code IN (SELECT session.UserAccount.Code 
                                    FROM SessionHistoryData session
                                    WHERE session.IsLoggedIn = true)";



        public const string GetLoggedInGeneralAndFirstLevelSupervisors =
            @"SELECT oper 
              FROM  OperatorData oper  
	                left join oper.Role.Profiles profileSup
	                inner join profileSup.Accesses accessSup  
              WHERE (accessSup.Name = 'GeneralSupervisorName+Basic+Supervision' OR
                    accessSup.Name = 'FirstLevelSupervisorName+Basic+Supervision') AND
                    oper.Code IN (SELECT session.UserAccount.Code 
                                  FROM SessionHistoryData session
                                  WHERE session.IsLoggedIn = true)";

        public const string GetLoggedInGeneralAndFirstLevelSupervisorsCodes =
            @"SELECT oper.Code 
                      FROM  OperatorData oper  
	                        left join oper.Role.Profiles profileSup
	                        inner join profileSup.Accesses accessSup  
                      WHERE (accessSup.Name = 'GeneralSupervisorName+Basic+Supervision' OR
                            accessSup.Name = 'FirstLevelSupervisorName+Basic+Supervision') AND
                            oper.Code IN (SELECT session.UserAccount.Code 
                                          FROM SessionHistoryData session
                                          WHERE session.IsLoggedIn = true)";

        public const string GetLoggedInGeneralAndDispatchSupervisors =
            @"SELECT oper 
              FROM  OperatorData oper
                    left join oper.DepartmentTypes dep     
                    left join oper.Role.Profiles profileSup
                    inner join profileSup.Accesses accessSup  
              WHERE dep.Code = {0} AND
                    (accessSup.Name = 'GeneralSupervisorName+Basic+Supervision' OR
                    accessSup.Name = 'DispatchSupervisorName+Basic+Supervision') AND
                    oper.Code IN (SELECT session.UserAccount.Code 
                                  FROM SessionHistoryData session
                                  WHERE session.IsLoggedIn = true)";

        public const string GetLoggedInGeneralAndDispatchSupervisorsCodes =
            @"SELECT oper.Code 
                      FROM  OperatorData oper
                            left join oper.DepartmentTypes dep     
                            left join oper.Role.Profiles profileSup
                            inner join profileSup.Accesses accessSup  
                      WHERE dep.Code = {0} AND
                            (accessSup.Name = 'GeneralSupervisorName+Basic+Supervision' OR
                            accessSup.Name = 'DispatchSupervisorName+Basic+Supervision') AND
                            oper.Code IN (SELECT session.UserAccount.Code 
                                          FROM SessionHistoryData session
                                          WHERE session.IsLoggedIn = true)";

        public const string GetLoggedInGeneralSupervisorsByDepartmentCode =
              @"SELECT operator 
                FROM  OperatorData operator 
                WHERE operator.Code in (SELECT oper.Code 
                                        FROM OperatorData oper 
                                        left join oper.DepartmentTypes dep 
				                        left join oper.Role.Profiles profileSup 
				                        inner join profileSup.Accesses accessSup  
                                        WHERE dep.Code IN {0} AND  
                                             accessSup.Name = 'GeneralSupervisorName+Basic+Supervision' AND 
                                             oper.Code IN (SELECT session.UserAccount.Code 
                                                          FROM SessionHistoryData session 
                                                          WHERE session.IsLoggedIn = true))";

        public const string GetOperatorsAssignForSupervisor =
           @"SELECT oad FROM OperatorAssignData oad WHERE oad.Supervisor.Code = {0}";

        public const string GetSupervisorWorkingNowWithAccess = @"Select oper from OperatorData oper
                                                                    left join oper.Role.Profiles profile
                                                                    inner join profile.Accesses access
                                                                    left join oper.WorkShifts ws 
                                                                    inner join ws.Schedules schedules                                                                    
                                                                    where access.Name like '%{0}%' and
                                                                    ('{1}' between schedules.Start and schedules.End)";

        /// <summary>
        /// 28-01-10 Este fue cambiado porque la lista de operadores cambio.
        /// </summary>
        public const string GetSupervisorWorkingNowByDepartment = @"SELECT operators.Operator FROM WorkShiftVariationData var
                                                                   inner join var.Schedules schedules
                                                                   inner join var.Operators operators
                                                                   left join operators.Operator.DepartmentTypes department
                                                                   WHERE var.Type != 2 AND var.ObjectType = 0 and 
																	    ('{0}' between schedules.Start and schedules.End) and
                                                                        department.Code = {1})";


//        <<<<<<< .mine
//                                                                   WHERE var.Type != 2 AND var.ObjectType = 0
//                                                                    AND ('{0}' between schedules.Start and schedules.End) and
//                                                                       operators IN (SELECT oper FROM OperatorData oper
//                                                                                     inner join oper.DepartmentTypes department
//                                                                                     WHERE department.Code = {1})";
//=======
       

        public const string GetSupervisorsDispacthByDepartmentName = @"Select oper from OperatorData oper
                                                            left join fetch oper.CategoryHistoryList
                                                            inner join oper.Role.Profiles profile
                                                            inner join profile.Accesses access
                                                            inner join oper.DepartmentTypes departments
                                                       where access.Name = 'DispatchSupervisorName' and departments.Name IN ('{0}')";

            
		public const string GetSupervisorsFirstLevel = @"Select oper from OperatorData oper
                                                            inner join oper.Role.Profiles profile
                                                            inner join profile.Accesses access
                                                        where access.Name like '%FirstLevelSupervisorName%'";            

        public const string GetOperatorsByApplication =
                                                        @"Select oper from OperatorData oper
                                                            left join fetch oper.CategoryHistoryList
                                                            left join oper.Role.Profiles profile
                                                            inner join profile.Accesses access
                                                        where access.UserApplication.Name = '{0}'";

        public const string GetOperatorsByApplications =
                                                        @"Select oper from OperatorData oper
                                                            left join fetch oper.CategoryHistoryList
                                                            left join oper.Role.Profiles profile
                                                            inner join profile.Accesses access
                                                        where access.UserApplication.Name IN {0}";

        public const string GetOperatorWithAccessByApp = @"Select access from OperatorData oper
                                                            left join oper.Role.Profiles profile
                                                            inner join profile.Accesses access
                                                        where oper.Code='{0}' and access.UserApplication.Name = '{1}'";

        public const string GetOperatorsByApplicationByWorkShift =
                                                        @"Select oper from OperatorData oper
                                                            left join fetch oper.CategoryHistoryList
                                                            inner join oper.WorkShifts ws
                                                            left join oper.Role.Profiles profile
                                                            inner join profile.Accesses access
                                                        where (access.UserApplication.Name = '{0}' OR access.UserApplication.Name = 'Supervision') and
                                                            ws.WorkShift.Code = {1}";      
      
        public const string CountWorkShiftVariationByTypeOperatorCode = @"SELECT count(var.Code) 
                                                                            FROM WorkShiftVariationData var inner join var.Operators operators
                                                                            WHERE var.Type != {0} AND var.ObjectType = 0 AND operators.Operator.Code = {1}";


        public const string CountWorkShiftVariationTimeOffByTypeOperatorCode = @"SELECT count(var.Code) 
                                                                            FROM WorkShiftVariationData var inner join var.Operators operators
                                                                            WHERE var.Type = {0} AND var.ObjectType = 0 AND operators.Operator.Code = {1}";

        public const string GetWorkShiftVariationByTypeAndAccess = @"SELECT distinct var
                                                              FROM WorkShiftVariationData var
                                                                  inner join var.Operators operators
                                                              WHERE var.Type != {0} AND var.ObjectType = 0  
                                                                AND operators.Operator IN (
                                                                    Select oper from OperatorData oper
                                                                    left join oper.Role.Profiles profile
                                                                    inner join profile.Accesses access
                                                                    inner join oper.DepartmentTypes departments
                                                                      where access.UserApplication.Name = '{1}' and
                                                                      ((departments is not null and departments.Name IN ('{2}')) OR departments is null))";

        public const string GetWorkShiftVariationTimeOffByTypeAndAccess = @"SELECT distinct var
                                                              FROM WorkShiftVariationData var
                                                                  inner join var.Operators operators
                                                              WHERE var.Type = {0} AND var.ObjectType = 0  
                                                                AND operators.Operator IN (
                                                                    Select oper from OperatorData oper
                                                                    left join oper.Role.Profiles profile
                                                                    inner join profile.Accesses access
                                                                    inner join oper.DepartmentTypes departments
                                                                      where access.UserApplication.Name = '{1}' and
                                                                       ((departments is not null and departments.Name IN ('{2}')) or departments is null))";

         public const string GetOperatorsByWorkShiftVariation = @"SELECT operators.Operator 
                                                                        FROM WorkShiftVariationData var 
                                                                        inner join var.Operators operators                                                                                                                                                            
                                                                        WHERE var.ObjectType = 0 AND var.Code = {0}";    

        public const string GetDispatchOperatorsByDeparment =
                                                @"Select oper from OperatorData oper
                                                            left join fetch oper.CategoryHistoryList
                                                            left join oper.Role.Profiles profile
                                                            inner join profile.Accesses access
                                                            inner join oper.DepartmentTypes departments
                                                        where access.UserApplication.Name = 'Dispatch' and
                                                            departments.Code IN ({0})";

        		public const string GetCurrentWorkShiftNameByOperatorCode = @"SELECT ws.Name FROM WorkShiftOperatorData wsOper 
                                                                   inner join wsOper.WorkShift ws 
                                                                   inner join ws.Schedules schedules
                                                                   WHERE wsOper.Operator.Code = {0} AND ws.ObjectType = 0 AND ws.Type != {1}  
                                                                       AND ('{2}' BETWEEN schedules.Start and schedules.End)";



        public const string GetWorkShiftSchedulesWorkTimeByOperatorCodeAndDate = @"SELECT schedules 
                                                                            FROM WorkShiftVariationData var inner join
                                                                                 var.Schedules schedules inner join
                                                                                 var.Operators operators
                                                                            WHERE operators.Operator.Code = {0} AND 
                                                                                  var.ObjectType = 0 AND 
																				  var.Type != {1} AND 
                                                                                  schedules.Start >= '{2}' AND 
                                                                                  schedules.End <= '{3}'";

        public const string GetWorkShiftSchedulesTimeOffByOperatorCodeAndDate = @"SELECT schedules 
                                                                            FROM WorkShiftVariationData var inner join
                                                                                 var.Schedules schedules inner join
                                                                                 var.Operators operators
                                                                            WHERE operators.Operator.Code = {0} AND 
                                                                                  var.Type = {1} AND 
                                                                                  var.ObjectType = 0 AND 
																				  schedules.Start >= '{2}' AND 
                                                                                  schedules.End <= '{3}'";

        public const string GetWorkShiftSchedulesWorkTimeByOperatorCodeAndEndDate = @"SELECT schedules 
                                                                            FROM WorkShiftVariationData var inner join
                                                                                 var.Schedules schedules inner join
                                                                                 var.Operators operators
                                                                            WHERE operators.Operator.Code = {0} AND 
                                                                                  var.ObjectType = 0 AND 
																				  var.Type != {1} AND 
                                                                                  schedules.End > '{2}'";

        public const string GetWorkShiftSchedulesTimeOffByOperatorCodeAndEndDate = @"SELECT schedules 
                                                                            FROM WorkShiftVariationData var inner join
                                                                                 var.Schedules schedules inner join
                                                                                 var.Operators operators
                                                                            WHERE operators.Operator.Code = {0} AND 
                                                                                  var.ObjectType = 0 AND 
																				  var.Type = {1} AND 
                                                                                  schedules.End > '{2}'";

        public const string GetWorkShiftScheduleWorkTimeByOperatorCodeSpecificDateTime = @"SELECT schedules 
                                                                            FROM WorkShiftVariationData var inner join
                                                                                 var.Schedules schedules inner join
                                                                                 var.Operators operators
                                                                            WHERE operators.Operator.Code = {0} AND 
                                                                                  var.ObjectType = 0 AND 
																				  var.Type != {1} AND 
                                                                                  '{2}' between schedules.Start and schedules.End";

        public const string GetWorkShiftScheduleTimeOffByOperatorCodeSpecificDateTime = @"SELECT schedules 
                                                                            FROM WorkShiftVariationData var inner join
                                                                                 var.Schedules schedules inner join
                                                                                 var.Operators operators
                                                                            WHERE operators.Operator.Code = {0} AND 
                                                                                  var.ObjectType = 0 AND 
																				  var.Type = {1} AND 
                                                                                  '{2}' between schedules.Start and schedules.End";

        public const string GetWorkShiftScheduleVariationsByWSVCode =
												   @"SELECT schedules 
                                                    FROM WorkShiftScheduleVariationData schedules 
                                                    WHERE schedules.WorkShiftVariation.ObjectType = 0 AND 
															schedules.WorkShiftVariation.Code = {0}";

        public const string GetNotExpiredWorkShiftScheduleVariationsByWSVCode =
                                           @"SELECT schedules 
                                                    FROM WorkShiftScheduleVariationData schedules 
                                                    WHERE schedules.WorkShiftVariation.ObjectType = 0 AND 
														  schedules.WorkShiftVariation.Code = {0} AND schedules.End > '{1}'";

        public const string GetSchedulesByOperatorCodeTypeStartEndWsvCode =
                                                       @"SELECT schedules 
                                                        FROM WorkShiftVariationData var inner join
                                                             var.Schedules schedules inner join 
                                                             var.Operators operators
                                                        WHERE operators.Operator.Code = {0} AND var.ObjectType = 0 AND var.Type != {1} and
                                                            ((schedules.Start < '{2}' and '{2}' < schedules.End) OR 
                                                             (schedules.Start < '{3}' and '{3}' < schedules.End) OR
                                                             ('{2}' < schedules.Start and schedules.Start < '{3}') OR
                                                             ('{2}' < schedules.End and schedules.End < '{3}')) AND
                                                              var.Code != {4}";

        public const string GetSchedulesByWsCode = @"SELECT schedules 
                                                        FROM WorkShiftVariationData ws left join
                                                             ws.Schedules schedules 
                                                        WHERE ws.Code = {0}";

        public const string GetSchedulesTimeOffByOperatorCodeTypeStartEndWsvCode =
                                                        @"SELECT schedules 
                                                        FROM WorkShiftVariationData var inner join
                                                             var.Schedules schedules inner join 
                                                             var.Operators operators
                                                        WHERE operators.Operator.Code = {0} AND var.ObjectType = 0 AND var.Type = {1} and
                                                            ((schedules.Start < '{2}' and '{2}' < schedules.End) OR 
                                                             (schedules.Start < '{3}' and '{3}' < schedules.End) OR
                                                             ('{2}' < schedules.Start and schedules.Start < '{3}') OR
                                                             ('{2}' < schedules.End and schedules.End < '{3}')) AND
                                                              var.Code != {4}";

        public const string GetSupervisorCodesByRole = @"select distinct oper.Code 
                                                         from OperatorData oper 
                                                                   left join oper.Role.Profiles profile 
                                                                   inner join profile.Accesses access
                                                         where access.Name like '%{0}%'";


        public const string GetOperatorsSchedulesNow = @"select distinct oper.Code 
                                                         from OperatorData oper left join 
                                                                   oper.Role.Profiles profile inner join
                                                                   profile.Accesses access
                                                         where (access.UserApplication.Name = '{0}' or '{0}' = '')";

        public const string GetOperatorWorkShiftVariationsNowByOperatorCode =
                                                   @"SELECT schedules 
                                                    FROM WorkShiftScheduleVariationData schedules inner join
                                                         schedules.WorkShiftVariation var inner join
                                                         var.Operators operators   
                                                    WHERE operators.Operator.Code = {0} AND var.Type != {1} AND 
                                                          ('{2}' BETWEEN schedules.Start AND schedules.End)";

        public const string GetOperatorTimeOffNowByOperatorCode =
                                                           @"SELECT schedules 
                                                    FROM WorkShiftScheduleVariationData schedules 
                                                    inner join schedules.WorkShiftVariation var 
                                                    inner join var.Operators operators   
                                                    WHERE operators.Operator.Code = {0} AND var.Type = {1} AND 
                                                          ('{2}' BETWEEN schedules.Start AND schedules.End)";
       
        public const string GetOperatorWithSupervisor = "select ope from OperatorData ope " +
                                                        "where ope.SupervisorOperator.Code = {0}";

        public const string GetIndicatorByName = "select data from IndicatorData data left join fetch data.Classes where data.Name = '{0}'";

        public const string GetIndicatorByCustomCode = "select data from IndicatorData data left join fetch data.Classes where data.CustomCode = '{0}'";

        public const string GetApplicationPreferenceByName = @" SELECT preference from ApplicationPreferenceData preference 
                                                                    WHERE preference.Name = '{0}'";

        public const string GetApplicationPreferenceGivenTheirNames = @" SELECT preference from ApplicationPreferenceData preference 
                                                                    WHERE preference.Name IN {0}";

        public const string GetAllApplicationPreferences = "SELECT data FROM ApplicationPreferenceData data";

        public const string GetAllRadioConfiguration = "SELECT data FROM RadioConfigurationData data";

        public const string GetRadioConfigurationByComputer = "SELECT data FROM RadioConfigurationData data where data.Computer = '{0}'";
        
        public const string GetAllTelephonyConfiguration = "SELECT data FROM TelephonyConfigurationData data";

        public const string GetTelephonyConfigurationByComputer = "SELECT data FROM TelephonyConfigurationData data where data.Computer = '{0}'";

        public const string GetOperatorCategories = "SELECT obj FROM OperatorCategoryData obj WHERE obj.DeletedId is null ORDER BY obj.Level";

        public const string GetOperatorCategoriesByCode = "SELECT obj FROM OperatorCategoryData obj WHERE obj.DeletedId is null AND obj.Code = {0}";

        public const string GetDepartmentsType = "SELECT obj FROM DepartmentTypeData obj WHERE obj.DeletedId is null ORDER BY obj.Name";

        public const string GetDispatchableDepartmentsType = "SELECT obj FROM DepartmentTypeData obj WHERE obj.DeletedId is null AND obj.Dispatch = true ORDER BY obj.Name";

		public const string GetDepartmentsTypeWithStationsAssociated =
                            @"SELECT obj FROM DepartmentTypeData obj 
                                WHERE obj.Dispatch = true AND obj.DeletedId is null AND 
                                (SELECT count(unit.Code) FROM UnitData unit
                                    WHERE unit.DepartmentStation.DepartmentZone.DepartmentType.Code = obj.Code) > 0
                                ORDER BY obj.Name";

        public const string GetDepartmentsTypeWithoutStationsAssociated =
                            @"SELECT obj FROM DepartmentTypeData obj 
                                WHERE obj.DeletedId is null AND 
                                (SELECT count(unit.Code) FROM UnitData unit
                                    WHERE unit.DepartmentStation.DepartmentZone.DepartmentType.Code = obj.Code) = 0
                                ORDER BY obj.Name";

        public const string CheckDepartmentTypeHasUnitsAssociated =
                            @"SELECT count(obj) FROM DepartmentTypeData obj 
                                WHERE obj.Dispatch = true AND obj.DeletedId is null AND
                                (SELECT count(unit.Code) FROM UnitData unit
                                    WHERE unit.DepartmentStation.DepartmentZone.DepartmentType.Code = obj.Code) > 0 AND
                                obj.Code = {0}";

        public const string GetDepartmentsTypeCodes = "SELECT obj.Code FROM DepartmentTypeData obj";

        public const string GetDepartmentsTypeByCode = @"SELECT department 
                                                                FROM DepartmentTypeData as department 
                                                                WHERE department.Code = {0}";

        public const string GetDepartmentsTypeByOperatorCode = "SELECT types FROM OperatorData oper left join oper.DepartmentTypes types WHERE oper.Code = {0} ORDER BY types.Name";

        public const string GetDevicesByStructCode = @"SELECT device 
                                                        FROM VideoDeviceData device
                                                        WHERE device.Struct.Code = {0}";

        public const string GetSupervisorsByOperatorCode = "SELECT ObjectData FROM OperatorAssignData ObjectData WHERE ObjectData.SupervisedOperator.Code = {0}";

        public const string GetOperatorsDistinctBySupervisorCode = "SELECT data from OperatorData data where data.Code in (SELECT distinct assign.SupervisedOperator.Code FROM OperatorAssignData assign WHERE assign.Supervisor.Code = {0})";

        public const string GetOperatorsBySupervisorCode = "SELECT ObjectData FROM OperatorAssignData ObjectData WHERE ObjectData.Supervisor.Code = {0}";

        public const string GetUserApplications = "SELECT obj FROM UserApplicationData obj WHERE obj.Name != 'Server' AND obj.DeletedId is null ORDER BY obj.FriendlyName";
        
        public const string GetUserApplicationsWithAccesses = "SELECT obj FROM UserApplicationData obj left join fetch obj.Accesses WHERE obj.Name != 'Server' AND obj.DeletedId is null ORDER BY obj.FriendlyName";

        public const string GetUserApplicationsByQuestion = @"SELECT app FROM QuestionData question left join question.App app WHERE question.Code = {0}";

        public const string GetStructsByCctvZoneCode = @" SELECT struct FROM StructData struct WHERE struct.Zone.Code = {0}";
        
        public const string GetAmountStructsByCctvZoneCode = @" SELECT count(*) FROM StructData struct WHERE struct.Zone.Code = {0}";

        public const string GetStructsByStructTypeAndCctvZone = @" SELECT struct FROM StructData struct WHERE struct.Type.Code = {0} AND struct.Zone.Code = {1}";

        public const string GetOperatorWithWorkShift = @"select ope from OperatorData ope where ope.WorkShifts is not null";		
        public const string GetGPSTypes = @"SELECT objectData FROM GPSTypeData objectData left join fetch objectData.Pines";

        public const string GetOperatorWorkShifts = @"Select workshifts 
                                                      from OperatorData oper left join oper.WorkShifts workshifts
                                                      where oper.Code = {0}";

        public const string GetOperatorSessionHistoryThatStarts = @"Select status
                                                                    From SessionHistoryData sh inner join sh.SessionHistoryStatusList status
                                                                    Where sh.UserAccount.Code = {0} and 
                                                                          sh.UserApplication.Name = '{1}' and 
                                                                          (status.EndDate > '{2}' or status.StartDate > '{2}' or 
                                                                          (status.EndDate is null and status.StartDate = (select max(status.StartDate) from sh.SessionHistoryStatusList status)))
                                                                    Order by status.StartDate";

        public const string GetOperatorSessionHistoryThatEnds = @"Select status
                                                                    From SessionHistoryData sh inner join sh.SessionHistoryStatusList status
                                                                    Where sh.UserAccount.Code = {0} and
                                                                          (status.EndDate > '{1}' or status.StartDate > '{1}')
                                                                    Order by status.StartDate";

        public const string GetOperatorBySessionHistoryCode = @"Select sh.UserAccount
                                                                    From SessionHistoryData sh 
                                                                    Where sh.Code = {0}";

        public const string GetOperatorByWorkShiftVariationCode = @"Select ope From WorkShiftVariationData var inner join var.Operators ope Where var.ObjectType = 0 AND var.Code = {0}";

        public const string GetOperatorCodesByWorkShiftVariationCode = @"Select ope.Operator.Code From WorkShiftVariationData var inner join var.Operators ope Where var.ObjectType = 0 AND var.Code = {0}";

        public const string GetAmountUnitGroupByDepartmentType = @"select count(*) 
                                                                                           from UnitData data
                                                                                           where data.DepartmentStation.DepartmentZone.DepartmentType.Code = {0}";

        public const string GetSessionStatusHistorySpecificDateOperatorNotReadySpecificStatus = "select data " +
                                                                "from SessionStatusHistoryData data " +
                                                                "where data.StartDate >= '{0}' " +
                                                                "and data.Session.UserAccount.Code = {2} " +
                                                                "and data.Status.NotReady = true " +
                                                                "and data.Status.CustomCode = '{3}'" +
                                                                "and data.Session.UserApplication.Code = {4}";

        public const string GetDepartmentStationAddressByDepartmentTypeCode = @"SELECT address 
                                                                                FROM DepartmentStationAddressData address
                                                                                WHERE address.Address.Lon != 0.0 AND address.Station.DepartmentZone.DepartmentType.Code = {0}";

        public const string IsLoggedOperatorInApplication = @"select count(*) 
                                                                      from SessionHistoryData data 
                                                                      where data.UserAccount.Code = {0} and 
                                                                            data.IsLoggedIn = {1} and                
                                                                            data.UserApplication.Code = {2}";
        public const string GetIndicatorWithShowDashboardTrue =
           @"select indicator 
                from IndicatorClassDashboardData icdd inner join icdd.Indicator indicator left join fetch indicator.Classes
                where icdd.ShowDashboard = true";

        public const string GetAllLastIndicatorsResultValue = @"select ir.*, irv.* from 
            (select indicator_code ic, max(time) tm from indicator_result group by indicator_code) test,
            indicator_result ir, indicator_result_values irv
            where ir.indicator_code = test.ic and ir.time = test.tm and ir.code = irv.indicator_result_code";

        public const string GetAllLastResultValues =
            @"SELECT result
            FROM IndicatorResultData result inner join result.Indicator left join fetch result.Result
            WHERE result.Time = (Select max(result1.Time) From IndicatorResultData result1 WHERE  result1.Indicator.Code = result.Indicator.Code)";

        public const string GetLastResultForIndicator =
            @"SELECT result
            FROM IndicatorResultData result inner join result.Indicator left join fetch result.Result
            WHERE  result.Indicator.Code = {0} and result.Time in (Select max(result.Time) From IndicatorResultData result WHERE  result.Indicator.Code = {0})";

        public const string GetLastDateTimeResultForIndicator =
            @"SELECT result.Time
            FROM IndicatorResultData result inner join result.Indicator left join result.Result
            WHERE  result.Indicator.Code = {0} and result.Time in (Select max(result.Time) From IndicatorResultData result WHERE  result.Indicator.Code = {0})";

        public const string GetIndicatorsWithClassesByDashboard = @"select data 
                                                                    from IndicatorData data left join fetch 
                                                                         data.Classes 
                                                                    where data.Dashboard = true";

		public const string GetAllIndicatorsClassDashboards = "SELECT data FROM IndicatorClassDashboardData data";

        public const string GetIndicatorClassDashboardsWithClassesByDashboard = @"select data 
                                                                                  from IndicatorClassDashboardData data
                                                                                  where data.Indicator.Dashboard = true";
        
        public const string GetIndicatorClassDashBoardsWithShowDashBoard = @"Select data 
                                                                            from IndicatorClassDashboardData data
                                                                            where data.ShowDashboard = true";

        public const string GetIndicatorCodeFromIndicatorInDashBoard = @"Select data.Code, data.Version 
                                                                            from IndicatorClassDashboardData data
                                                                            where data.ShowDashboard = true and
                                                                                  data.IndicatorClass.Name = 'SYSTEM'";

        public const string GetIndicatorForecastByIndicatorIdTypeCurrentDate = @"select data 
                                                                                 from IndicatorForecastData data 
                                                                                 where data.Indicator.Code = {0} and 
                                                                                       data.IndicatorGroupForecast.General = {1} and 
                                                                                       '{2}' between data.IndicatorGroupForecast.Start and data.IndicatorGroupForecast.End";

        public const string GetIndicatorForecastByIndicatorIdTypeAndDepartmentTypeCurrentDate = @"select data 
                                                                                 from IndicatorForecastData data 
                                                                                 where data.Indicator.Code = {0} and 
                                                                                       data.IndicatorGroupForecast.General = {1} and 
                                                                                       '{2}' between data.IndicatorGroupForecast.Start and data.IndicatorGroupForecast.End and
                                                                                       data.DepartmentType is not null";

        public const string GetCurrentCloseReportBySession = @"Select data
                                                               From SupervisorCloseReportData data left join fetch
                                                                    data.Messages
                                                               where data.Session.Code = {0}";

        public const string GetCurrentCloseReportBySessionWithoutMessages = @"Select data
                                                                             From SupervisorCloseReportData data
                                                                             Where data.Session.Code = {0}";

        public const string GetCloseReportsByDate = @"select data 
                                                      from SupervisorCloseReportData data left join fetch 
                                                           data.Messages 
                                                      where (data.Session.EndDateLogin >= '{0}' OR data.Session.EndDateLogin is null) AND data.Finished = true";

        public const string GetIndicatorGroupForecastWithForecastValues = @"select data 
                                                                            from IndicatorGroupForecastData data left join fetch 
                                                                                 data.ForecastValues";

        public const string GetOperatorIndicatorResultByIndicatorIndicatorClass = @"select ird
                                                                                    from IndicatorResultData ird left join fetch
                                                                                         ird.Result value
                                                                                    where value.IndicatorClass.Code = {0} and 
                                                                                          ird.Indicator.CustomCode = '{1}'
                                                                                    order by ird.Time desc";

        public const string CountIndicatorGroupForecastName = @"select igfd
              from IndicatorGroupForecastData igfd
                where igfd.Name = '{0}' and igfd.Code != {1}";

        public const string GetIndicatorGroupForecastById = @"select data 
                                                                            from IndicatorForecastData data
                                                                            where data.IndicatorGroupForecast.Code = {0}";

        public const string GetAmountIndicatorGroupForecastWithStartEndDatesAndDepartmentSimilarities =
            @"select distinct data
              from IndicatorGroupForecastData data inner join
                   data.ForecastValues values
              where (('{0}' >= data.Start and '{0}' <= data.End ) or
                     ('{1}' >= data.Start and '{1}' <= data.End ) or
                     ('{0}' <= data.Start and '{1}' >= data.End)) and
                    ( ({2} = 0 and values.DepartmentType is null) or
                      ({2} != 0 and values.DepartmentType.Code = {2}) ) and
                    data.General = false and data.Code <> {3}";

        public const string GetIndicatorGroupForecastDataByCode = @"SELECT obj FROM IndicatorGroupForecastData obj LEFT JOIN FETCH obj.ForecastValues values WHERE obj.Code = {0}";

        public const string GetIndicatorGroupForecastGeneral = 
            @"select data 
              from IndicatorGroupForecastData data 
              where data.General = {0} and
                    data.End > '{1}'";

        public const string GetIndicatorGroupForecastGeneralNotStarted =
              @"select data 
              from IndicatorGroupForecastData data 
              where data.General = true and
                    (data.End = '{0}') and
                    data.Start != data.End";

        public const string GetCountTrainingCourseEvaluatingorEvaluated =
            @"select count(*) 
              from TrainingCourseData tcd inner join
                   tcd.Schedules schedules
              where tcd.Code = {0} and 
                    ((schedules.End < '{1}' or schedules.Start < '{1}') and 
                     exists elements(schedules.Operators))";

        public const string GetCountTrainingCourseScheduleEvaluatingorEvaluated =
            @"select count(*) 
              from TrainingCourseScheduleData schedules
              where schedules.Code = {0} and
                    ((schedules.End < '{1}' or schedules.Start < '{1}') and 
                     exists elements(schedules.Operators))";

        public const string GetCountTrainingCourseScheduleStartedWithOperators = @"Select count(*) 
                                                from TrainingCourseScheduleData tcsd inner join
                                                tcsd.Parts parts
                                                where tcsd.TrainingCourse.Code = {0} and 
                                               ((parts.End < '{1}' or parts.Start < '{1}') and 
                                                exists elements(tcsd.Operators))";

        public const string GetOperatorAgentID = "Select data.AgentID from OperatorData data where data.Code = {0}";

        public const string GetTotalPerDayTelephonyField = "select sum(data.Value) from TelephonyStatsHistoryData data where data.Name = '{0}' and data.TimeStamp >= '{1}'";

        public const string GetMaxPerDayTelephonyField = "select max(data.Value) from TelephonyStatsHistoryData data where data.Name = '{0}' and data.TimeStamp >= '{1}'";
		
		public const string GetSupervisorsOfGivenDepartment = "select distinct opAss from OperatorAssignData opAss inner join opAss.Supervisor super " +
																 "inner join super.DepartmentTypes dep where dep.Name = '{1}' and opAss.SupervisedOperator.Code={0}";

        public const string GetGenesysStatFromDBByName = "select data from TelephonyStatsHistoryData data where data.Name = '{0}'";

        public const string GetAllAccessByOperatorAndApplication = "select access from OperatorData data left join data.Role.Profiles profile inner join profile.Accesses access where data.Code = {0} and access.UserApplication.Code = {1}";

        public const string GetDispatchReport = "Select data from DispatchReportData data";

        public const string GetDispatchReportByIncidentTypeAndDepartmentType = @"SELECT count(report.Code) FROM DispatchReportData report inner join report.DepartmentTypes departments inner join report.IncidentTypes incidents WHERE report.Code != {0} AND departments.Code = {1} AND incidents.Code = {2}";

        public const string GetRequiredQuestionDispatchReport = @"SELECT objectData FROM QuestionDispatchReportData objectData WHERE objectData.Required = true";

        public const string GetRequiredDispatchReportQuestionDispatchReport = @"SELECT objectData FROM DispatchReportQuestionDispatchReportData objectData WHERE objectData.Report.Code = {0} AND objectData.Question.Required = true";

		#region WorkShiftVariations

		public const string GetWorkShiftVariationWithEndedSchedulesOperators =
								@"select count(*) from WorkShiftVariationData var inner join 
								var.Schedules schedules where var.Name = '{0}' and 
								'{1}' > all (select schedules.End from var.Schedules schedules)
								AND var.ObjectType = 0";

		public const string GetWorkShiftVariationWithEndedSchedulesUnits =
								@"select count(*) from WorkShiftVariationData var inner join 
								var.Schedules schedules where var.Name = '{0}' and 
								'{1}' > all (select schedules.End from var.Schedules schedules)
								AND var.ObjectType = 1";

        public const string GetWorkShitftSchedulesWithOperators =
                                @"SELECT schedule 
                                    FROM WorkShiftScheduleVariationData schedule
                                    inner join schedule.WorkShiftVariation.Operators operators 
                                    WHERE operators.Operator.Code = {0} AND schedule.WorkShiftVariation.Type != {1}
                                        AND schedule.End >= '{2}'  
                                        AND schedule.WorkShiftVariation.ObjectType = 0 Order By schedule.Start";




		public const string GetWorkShiftVariationWithFirstLevelOperatorsWithStrictAccess =
						@"SELECT var FROM WorkShiftVariationData var 
                            inner join fetch var.Operators operators
                            WHERE var.Type != {0} AND var.ObjectType = 0 and
                                  operators.Operator.Code in (SELECT ope.Code FROM OperatorData ope 
                                                 left join ope.DepartmentTypes dep 
                                                 left join ope.Role.Profiles profiles 
                                                 inner join profiles.Accesses access 
                                                 WHERE access.Name like '%FirstLevel%') and
                                  operators.Operator.Code not in (select distinct ope.Code from OperatorData ope 
													left join ope.Role role  
													left join role.Profiles profile 
													inner join profile.Accesses access 
													where access.Name like '%Dispatch%')";


        public const string GetWorkShiftVariationWithDispatchOperatorsWithStrictAccess =
                @"SELECT var FROM WorkShiftVariationData var 
                            inner join fetch var.Operators operators
                            WHERE var.Type != {0} AND var.ObjectType = 0 and
                                  operators.Operator.Code in (SELECT ope.Code FROM OperatorData ope 
                                                 left join ope.DepartmentTypes dep 
                                                 left join ope.Role.Profiles profiles 
                                                 inner join profiles.Accesses access 
                                                 WHERE access.Name like '%Dispatch%' 
                                                 and dep.Code = {1}) and
                                  operators.Operator.Code not in (select distinct ope.Code from OperatorData ope 
													left join ope.Role role  
													left join role.Profiles profile 
													inner join profile.Accesses access 
													where access.Name like '%FirstLevel%')";
        
        public const string GetNotStartedWorkShiftSchedules = @"SELECT schedules FROM WorkShiftScheduleVariationData schedules 
										WHERE schedules.WorkShiftVariation.Code = {0} and schedules.End >= '{1}'";

		public const string GetNotStartedOperatorAssignsForGinvenWorkShiftVariationAndOperator = @"SELECT assign FROM OperatorAssignData assign 
										WHERE assign.SupervisedOperator.Code = {0} AND assign.SupervisedScheduleVariation.WorkShiftVariation.Code = {1} 
											AND assign.EndDate >= '{2}'";

		public const string GetNotStartedOperatorAssignsForGinvenWorkShiftVariationAndSupervisor = @"SELECT assign FROM OperatorAssignData assign 
										WHERE assign.Supervisor.Code = {0} AND assign.SupervisedScheduleVariation.WorkShiftVariation.Code = {1} 
											AND assign.EndDate >= '{2}'";

        public const string GetFutureWorkShiftVariationWithOperatorsBySupervisor =
                    @"SELECT var FROM WorkShiftVariationData var 
                        left join fetch var.Operators operators
                        WHERE var.Type != {0} AND var.ObjectType = 0 
							AND operators.Operator.Code IN (
                            SELECT distinct assign.SupervisedOperator.Code 
                            FROM OperatorAssignData assign 
                            WHERE assign.SupervisedScheduleVariation.WorkShiftVariation.Code = var.Code 
								AND  assign.Supervisor.Code = {1} AND assign.EndDate >= '{2}')";

//        public const string GetWorkShiftVariationWithOperatorsAssignForTheGivenSupervisor =
//                        @"SELECT var FROM WorkShiftVariationData var 
//                        left join fetch var.Operators operators
//                        WHERE var.Type != {0} AND var.ObjectType = 0
//							AND operators.Operator.Code IN (
//								SELECT distinct assign.SupervisedOperator.Code 
//								FROM OperatorAssignData assign 
//								WHERE assign.SupervisedScheduleVariation.WorkShiftVariation.Code = var.Code 
//									AND  assign.Supervisor.Code = {1} AND assign.StartDate >= '{2}')";
        public const string GetWorkShiftVariationWithOperatorsAssignForTheGivenSupervisor =
                @"SELECT var FROM WorkShiftVariationData var 
                        left join fetch var.Operators operators
                        WHERE var.Type != {0} AND var.ObjectType = 0
							AND operators.Operator.Code IN (
								SELECT distinct assign.SupervisedOperator.Code 
								FROM OperatorAssignData assign 
								WHERE assign.SupervisedScheduleVariation.WorkShiftVariation.Code = var.Code 
									AND  assign.Supervisor.Code = {1})";

//        public const string GetWorkShiftVariationWithOperatorsAssignForTheGivenOperator =
//                        @"SELECT var FROM WorkShiftVariationData var 
//                        left join fetch var.Operators operators
//                        WHERE var.Type != {0} AND var.ObjectType = 0
//							AND operators.Operator.Code IN (
//								SELECT distinct assign.Supervisor.Code 
//								FROM OperatorAssignData assign 
//								WHERE assign.SupervisorScheduleVariation.WorkShiftVariation.Code = var.Code 
//									AND assign.SupervisedOperator.Code = {1} AND assign.StartDate >= '{2}')";

        public const string GetWorkShiftVariationWithOperatorsAssignForTheGivenOperator =
                @"SELECT var FROM WorkShiftVariationData var 
                        left join fetch var.Operators operators
                        WHERE var.Type != {0} AND var.ObjectType = 0
							AND operators.Operator.Code IN (
								SELECT distinct assign.Supervisor.Code 
								FROM OperatorAssignData assign 
								WHERE assign.SupervisorScheduleVariation.WorkShiftVariation.Code = var.Code 
									AND assign.SupervisedOperator.Code = {1})";


        public const string GetWorkShiftVariationByOperatorCode = @"SELECT var FROM WorkShiftVariationData var 
left join var.Operators operators WHERE operators.Operator.Code = {0}";

        public const string WorkShiftVariationOperatorsByOperatorCode = @"SELECT var FROM WorkShiftOperatorData var WHERE var.Operator.Code = {0}";


		public const string WorkShiftVariationOperatorsByWorkShiftCode = @"SELECT objectData FROM WorkShiftOperatorData objectData WHERE objectData.WorkShift.Code = {0}";

		public const string GetAmmountOfAssignsByOperator = @"SELECT count(*) from OperatorAssignData assign 
															WHERE assign.SupervisedScheduleVariation.WorkShiftVariation.Code = {0} 
																AND (assign.Supervisor.Code in {1} OR assign.SupervisedOperator.Code in {1})";

		public const string GetOperatorAssignsByWorkShiftCodeAndOperatorCode = 
										@"SELECT assign FROM OperatorAssignData assign 
										WHERE assign.Supervisor.Code = {0} AND assign.SupervisedOperator.Code = {1} 
											AND assign.SupervisorScheduleVariation.WorkShiftVariation.Code = {2} AND assign.StartDate >= '{3}'";

		public const string GetOperatorAssignsByWorkShiftCodeAndSupervisorCode = 
										@"SELECT assign FROM OperatorAssignData assign 
										WHERE assign.Supervisor.Code = {0} AND assign.SupervisedOperator.Code = {1} 
											AND assign.SupervisedScheduleVariation.WorkShiftVariation.Code = {2} AND assign.StartDate >= '{3}'";

		public const string GetSessionHistoryCountInWorkShiftVariationByOperatorCode = 
											@"SELECT count(*) 
                                               FROM SessionHistoryData data inner join 
                                                    data.SessionHistoryStatusList statuses
                                               WHERE data.IsLoggedIn = true AND
                                                     statuses.EndDate is null AND
                                                     data.UserAccount.Code IN 
                                                     (SELECT ObjectData.SupervisedOperator.Code 
                                                      FROM OperatorAssignData ObjectData 
                                                      WHERE (ObjectData.SupervisedOperator.Code = {0} OR ObjectData.Supervisor.Code = {0}) AND
                                                             ObjectData.SupervisedScheduleVariation.WorkShiftVariation.Code = {2} AND
                                                            '{1}' BETWEEN ObjectData.StartDate AND ObjectData.EndDate)";

		#endregion

		public const string GetUnitByGpsNameWithUnitAlerts = @"Select unit From UnitData unit left join fetch unit.Alerts Where unit.GPS.Name = '{0}'";

        public const string UnitWithIncidentType = @"SELECT data 
                                        FROM UnitData data 
                                             inner join fetch data.SetIncidentTypes";

        public const string UnitWithOfficerAssign = @"SELECT data 
                                        FROM UnitData data 
                                             inner join fetch data.SetOfficerAssigns ";

        public const string UnitWithDepartmentTypeOfOperator = @"SELECT data 
                                                    FROM UnitData data 
                                                    WHERE data.DepartmentStation.DepartmentZone.DepartmentType.Dispatch = true
                                                         AND data.DepartmentStation.DepartmentZone.DepartmentType.Code IN (SELECT depTypes.Code 
                                                                                          FROM OperatorData oper
                                                                                          inner join oper.DepartmentTypes depTypes
                                                                                          WHERE oper.Code = {0})";


        public const string DepartmentZonesWithStationAndAddress = "select data from DepartmentZoneData data left join fetch data.DepartmentZoneAddres left join fetch data.SetDepartmentStations where data.DeletedId is null and data.DepartmentType.DeletedId is null order by data.Name Asc";

        public const string DepartmentZonesByDepartmentType = @"select data 
                                                                from DepartmentZoneData data 
                                                                inner join data.DepartmentType dt 
                                                                left join fetch data.DepartmentZoneAddres
                                                                where data.DeletedId is null and dt.DeletedId is null and dt.Code = {0}
                                                                order by data.Name ASC";

        public const string IncidentsOpen = "select data from IncidentData data where EndDate is null";

        public const string IncidentTypeQuestionByCodeWithDepartmentTypes = @"select data.Question, data from IncidentTypeQuestionData data left join fetch data.Question.SetPossibleAnswers left join fetch data.Question.App where data.IncidentType.Code = {0} and data.Question is not null";

        public const string OperatorWithSupervisors = @"select data from OperatorData data left join fetch data.Supervisors where data.Code = {0}";
        
        public const string OperatorWithDepartmentTypes = @"select data from OperatorData data left join fetch data.DepartmentTypes where data.Code = {0}";

        public const string GetRouteUnitAssociations = @"SELECT data  
												         FROM WorkShiftRouteData data                                                            
												         WHERE data.Route.Code = {0}";

        public const string GetCountRouteUnitAssociations = @"SELECT count(data.Code)  
												              FROM WorkShiftRouteData data                                                            
												              WHERE data.Route.Code = {0}";


		public const string GetWorkShiftRouteDataWithSchedulesByUnitCodeAndDate =
								@"SELECT wsrd.WorkShift 
									FROM WorkShiftRouteData wsrd LEFT JOIN FETCH wsrd.WorkShift.Schedules schedules 
									WHERE wsrd.Unit.Code = {0} AND '{1}' BETWEEN schedules.Start AND schedules.End";

		public const string GetWorkShiftRouteDataWithRouteAddressByUnitCode =
								@"SELECT wsrd.Route 
									FROM WorkShiftRouteData wsrd LEFT JOIN FETCH wsrd.Route.RouteAddress schedules 
									WHERE wsrd.Unit.Code = {0}";

		public const string GetWorkShiftRouteDataWithByUnitCodeAndDate =
								@"SELECT wsrd 
									FROM WorkShiftRouteData wsrd 
									WHERE wsrd.Unit.Code = {0}";

		public const string GetCountOfActiveAlertsByRouteCode = @"SELECT Count(*) 
					FROM UnitData unit left join 
						unit.WorkShiftRoutes wsr left join 
						unit.Alerts alertType, 
						AlertNotificationData alert  
					WHERE alert.UnitAlert.Code = alertType.Code AND wsr.Route.Code = {0} 
						AND alertType.Alert.CustomCode <> 'GPS_I/O' AND alertType.Alert.CustomCode <> 'SPEED_LIMIT'  
						AND alertType.Alert.CustomCode <> 'WITHOUT_COMMUNICATION' AND alert.Status != {1}";

        public const string TrainingCourseSchedulePartByDateAndOperator = @"Select parts
                               from TrainingCourseScheduleData tcs inner join 
                                    tcs.Parts parts inner join
                                    tcs.Operators operators
                               where '{0}' <= tcs.Start and
                                     tcs.End <= '{1}' and
                                     operators.Operator.Code = {2}";

        public const string TrainingCourseSchedulePartByOperatorAndCourse = @"Select parts
                               from TrainingCourseScheduleData tcs inner join 
                                    tcs.Parts parts inner join
                                    tcs.Operators operators
                               where 
                                     operators.Operator.Code = {0} and
                                     tcs.TrainingCourse.Code = {1}
                               ";

		public const string GetAlertNotificationByOperatorCode = "SELECT data FROM AlertNotificationData data Where data.AttendingOperator.Code = {0} and data.Status != 2";

        public const string GetAllAlarmQueryTwitter  = "SELECT data FROM AlarmQueryTwitterData data Where data.DeletedId is null and data.Type = 1";
        
        public const string GetAlarmQueryTwitterByQuery = "SELECT data FROM AlarmQueryTwitterData data Where data.TwitterQuery = '{0}' and data.DeletedId is null";

        public const string GetAlarmsTwitterByStatus = "SELECT data FROM AlarmTwitterData data Where data.Status = {0} and data.DeletedId is null";
        
        public const string GetAlarmTwitterByQueryCode = @"SELECT data 
                                                        FROM AlarmTwitterData data 
                                                        LEFT JOIN FETCH data.Tweets
                                                        WHERE data.Status = {0} AND data.AlarmQuery.Code = {1} AND data.DeletedId is null";

        public const string GetVAAlertByCameraOperatorId =
                  @"SELECT data FROM VAAlertCamData data 
                    LEFT JOIN data.Camera.Operators operators
                    WHERE operators.User.Code = '{0}'";

        public const string GetVAAlertByCameraOperatorId2 =
                  @"SELECT data FROM VAAlertCamData data 
                    LEFT JOIN data.Camera.Operators operators
                    WHERE operators.User.Code = '{0}'";
        

		public static string GetCustomHql(bool duplicateQuotes, string smartCadHql, params object[] args)
        {
            if (duplicateQuotes == true)
            {
                for (int i = 0; i < args.Length; i++)
                {
                    if (args[i] is string)
                        args[i] = args[i].ToString().Replace("'", "''");
                }
            }
            StringWriter writer = new StringWriter();
            writer.Write(smartCadHql, args);
            return writer.ToString().Replace("\n", "").Replace("\r", "").Replace("\t", "");
        }

        public static string GetCustomHql(string smartCadHql, params object[] args)
        {
            return GetCustomHql(true, smartCadHql, args);
        }
    }

    public static class SmartCadSQL
    {
        public const string UpdateReportBaseMultipleOrganismsByIncidentCode = @"UPDATE REPORT_BASE 
                                                                                SET MULTIPLE_ORGANISMS = {0}, VERSION = VERSION + 1
                                                                                WHERE INCIDENT_CODE = {1}";

        public const string UpdateIncidentNotificationStatusByIncidentCode = @"update incident_notification
                                                                                set incident_notification.incident_notification_status_code = (select code from incident_notification_status where custom_code = 'UPDATED'), incident_notification.VERSION = incident_notification.VERSION + 1
                                                                                from report_base rb,
                                                                                incident_notification_status insta
                                                                                where rb.incident_code = {0}
                                                                                and incident_notification.report_base_code = rb.code
                                                                                and incident_notification.incident_notification_status_code = insta.code
                                                                                and insta.custom_code not in ('CANCELLED', 'CLOSED', 'AUTOMATIC_SUPERVISOR', 'MANUAL_SUPERVISOR', 'NEW', 'UPDATED')";

        public const string UpdateIncidentNotificationLastStatusHistoryByIncidentCode =
            @"UPDATE INCIDENT_NOTIFICATION_STATUS_HISTORY 
                SET INCIDENT_NOTIFICATION_STATUS_HISTORY.END_DATE = GETDATE(), INCIDENT_NOTIFICATION_STATUS_HISTORY.VERSION = INCIDENT_NOTIFICATION_STATUS_HISTORY.VERSION + 1
                FROM REPORT_BASE rb, INCIDENT_NOTIFICATION inot
                WHERE INCIDENT_NOTIFICATION_STATUS_HISTORY.INCIDENT_NOTIFICATION_CODE = inot.CODE AND
                inot.REPORT_BASE_CODE = rb.CODE AND                
                INCIDENT_NOTIFICATION_STATUS_HISTORY.INCIDENT_NOTIFICATION_STATUS_CODE NOT IN 
                (
                    SELECT CODE FROM INCIDENT_NOTIFICATION_STATUS
                    WHERE CUSTOM_CODE IN ('CANCELLED', 'CLOSED', 'AUTOMATIC_SUPERVISOR', 'MANUAL_SUPERVISOR', 'NEW', 'UPDATED')
                ) AND INCIDENT_NOTIFICATION_STATUS_HISTORY.END_DATE IS NULL AND
                rb.INCIDENT_CODE = {0}";

        public const string UpdateHangedUpCallAndFinishedReportTime =
            @"UPDATE PHONE_REPORT
                SET HANGED_UP_CALL_TIME = '{1}',
                    FINISHED_REPORT_TIME = '{2}'
                WHERE PARENT_CODE = {0}";
        
        public const string InsertIncidentNotificationLastStatusHistoryByIncidentCode =
            @"INSERT INTO INCIDENT_NOTIFICATION_STATUS_HISTORY 
                (START_DATE, USER_ACCOUNT_CODE, INCIDENT_NOTIFICATION_CODE, INCIDENT_NOTIFICATION_STATUS_CODE, VERSION)
                SELECT getdate(), {0}, inot.code, {1}, 1
                    FROM INCIDENT_NOTIFICATION inot, REPORT_BASE rb
                    WHERE inot.REPORT_BASE_CODE = rb.CODE AND
                    inot.INCIDENT_NOTIFICATION_STATUS_CODE NOT IN 
                    (
                        SELECT CODE FROM INCIDENT_NOTIFICATION_STATUS
                        WHERE CUSTOM_CODE IN ('CANCELLED', 'CLOSED', 'AUTOMATIC_SUPERVISOR', 'MANUAL_SUPERVISOR', 'NEW', 'UPDATED')
                    ) AND           
                    rb.INCIDENT_CODE = {2}";


       

        public static string GetCustomSQL(string smartCadHql, params object[] args)
        {
            StringWriter writer = new StringWriter();
            writer.Write(smartCadHql, args);
            return writer.ToString().Replace("\n", "").Replace("\r", "").Replace("\t", "");
        }
    }
}

/**
// End of SmartCadHqls.cs
 */

