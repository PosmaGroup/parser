﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace SmartCadCore.Core
{

    public class CultureElement : ConfigurationElement
    {


        [ConfigurationProperty("language")]
        public CultureLanguageElement CultureLanguageElement
        {
            get
            {
                return this["language"] as CultureLanguageElement;
            }
            set
            {
                this["language"] = value;
            }
        }

        [ConfigurationProperty("country")]
        public CultureCountryElement CultureCountryElement
        {
            get
            {
                return this["country"] as CultureCountryElement;
            }
            set
            {
                this["country"] = value;
            }
        }

        
    }

    public class CultureLanguageElement : ConfigurationElement
    {
        public CultureLanguageElement()
        {
        }

        [ConfigurationProperty("name")]
        public string Name
        {
            get
            {
                return this["name"] as string;
            }
            set
            {
                this["name"] = value;
            }
        }


    }

    public class CultureCountryElement : ConfigurationElement
    {
        public CultureCountryElement()
        {
        }

        [ConfigurationProperty("name")]
        public string Name
        {
            get
            {
                return this["name"] as string;
            }
            set
            {
                this["name"] = value;
            }
        }


    }

    
}
