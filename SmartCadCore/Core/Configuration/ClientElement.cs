using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace SmartCadCore.Core
{
    #region Class CustomerElement Documentation
    /// <summary>
    /// TODO:
    /// </summary>
    /// <className>CustomerElement</className>
    /// <author email="atorres@smartmatic.com">Alden Torres</author>
    /// <date>2006/04/03</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    public class ClientElement : ConfigurationElement
    {
        public ClientElement()
        {
        }

        [ConfigurationProperty("customer")]
        public CustomerElement CustomerElement
        {
            get
            {
                return this["customer"] as CustomerElement;
            }
            set
            {
                this["customer"] = value;
            }
        }

        [ConfigurationProperty("software")]
        public SoftwareElement SoftwareElement
        {
            get
            {
                return this["software"] as SoftwareElement;
            }
            set
            {
                this["software"] = value;
            }
        }

        [ConfigurationProperty("serverAddress")]
        public ServerAddressElement ServerAddressElement
        {
            get
            {
                return this["serverAddress"] as ServerAddressElement;
            }
            set
            {
                this["serverAddress"] = value;
            }
        }

        [ConfigurationProperty("telephony")]
        public TelephonyElement TelephonyElement
        {
            get
            {
                return this["telephony"] as TelephonyElement;
            }
            set
            {
                this["telephony"] = value;
            }
        }
    }
}
