using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace SmartCadCore.Core
{
    #region Class NHibernateElement Documentation
    /// <summary>
    /// TODO:
    /// </summary>
    /// <className>NHibernateElement</className>
    /// <author email="atorres@smartmatic.com">Alden Torres</author>
    /// <date>2006/03/31</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    public class NHibernateElement : ConfigurationElement
    {
        public NHibernateElement()
        {
        }

        [ConfigurationProperty("properties", IsRequired = false)]
        [ConfigurationCollection(typeof(PropertiesCollection), AddItemName = "property")]
        public new PropertiesCollection Properties
        {
            get
            {
                return this["properties"] as PropertiesCollection;
            }
            set
            {
                this["properties"] = value;
            }
        }
    }
}

