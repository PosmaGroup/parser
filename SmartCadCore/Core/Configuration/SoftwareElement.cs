using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace SmartCadCore.Core
{
    #region Class SoftwareElement Documentation
    /// <summary>
    /// TODO:
    /// </summary>
    /// <className>SoftwareElement</className>
    /// <author email="atorres@smartmatic.com">Alden Torres</author>
    /// <date>2006/04/07</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    public class SoftwareElement : ConfigurationElement
    {
        public SoftwareElement()
        {
        }

        [ConfigurationProperty("version")]
        public string Version
        {
            get
            {
                return this["version"] as string;
            }
            set
            {
                this["version"] = value;
            }
        }
    }
}
