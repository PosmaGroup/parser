using System;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.IO;

using log4net.Config;
using SmartCadCore.Common;
using SmartCadCore.ClientData;

namespace SmartCadCore.Core
{
    #region Class SmartCadConfiguration Documentation
    /// <summary>
    /// Clase estatica para leer la configuracion y datos del sistema.
    /// </summary>
    /// <className>SmartCadConfiguration</className>
    /// <author email="atorres@smartmatic.com">Alden Torres</author>
    /// <date>2006/03/30</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    public class SmartCadConfiguration
    {
        public const string Version = "4.0.18";
#if (DEBUG)
        public static string ConfigFilename =  Path.Combine(Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.Parent.FullName, "parser\\Dist\\Configuration\\Configuration.xml");
#else
        public static string ConfigFilename = "../Configuration/Configuration.xml";//Path.Combine(Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.Parent.FullName, "Dist\\Configuration\\Configuration.xml");
#endif


#if (DEBUG)
        public static string LanguageDirectoryClient = Path.Combine(Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.Parent.FullName, "parser\\Bin\\Debug");
#else
        public static string LanguageDirectoryClient = "../Bin";
#endif


#if (DEBUG)
        public static string LanguageDirectory = Path.Combine(Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.Parent.FullName, "parser\\Bin\\Debug");
#else
        public static string LanguageDirectory = "../Bin";
#endif

#if (DEBUG)
        //public static string CameraTemplates = System.Text.RegularExpressions.Regex.Replace(Environment.CurrentDirectory, @"\\([^\\])+\\([^\\])+(\\)?$", "")
        //    + @"\Dist\CamerasTemplates\";

        public static string CameraTemplates = Path.Combine(Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.Parent.FullName, "parser\\Dist\\CamerasTemplates\\");
#else
        public const string CameraTemplates = "../CamerasTemplates/";
#endif

        public const string ConfigFolderFilename = "Configuration\\Configuration.xml";

#if (DEBUG)
        public static string DistFolder = Path.Combine(Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.FullName, "Dist");
#else
        public static string DistFolder = "../"; //Path.Combine(Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.Parent.FullName, "Dist");
#endif

#if (DEBUG)
        public const string CertificateFolder = "../../Dist/Cert/";
#else
        public const string CertificateFolder = "../Cert/";
#endif

#if (DEBUG)
        public const string MapsFolder = "../../Dist/Maps/";
#else
        public const string MapsFolder = "../Maps/";
#endif

#if (DEBUG)
        public const string MapsNewFolder = "./Images/";
#else
    public const string MapsNewFolder = "./Images/";
#endif

#if (DEBUG)
        public const string MapsIconsFolder = "\\..\\..\\Dist\\Maps\\Icons\\";
#else
        public const string MapsIconsFolder = "/../Maps/Icons/";
#endif

#if (DEBUG)
        public const string ReportsFolder = "../../Dist/Reports/";
#else
        public const string ReportsFolder = "../Reports/";
#endif

#if (DEBUG)
        public const string ImagesFolder = "../../Dist/Images/";
#else
        public const string ImagesFolder = "../Images/";
#endif

#if (DEBUG)
        public static string SkinsFolder = Path.Combine(Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.Parent.FullName, "parser\\Dist\\Skins\\");
#else
        public static string SkinsFolder = "../Skins/";//Path.Combine(Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.Parent.FullName, "Dist\\Skins\\");
#endif

#if (DEBUG)
        public const string SmartCadFrontClientFilename = "./SmartCadFrontClient.exe";
#else
        public const string SmartCadFrontClientFilename = "./SmartCadFrontClient.exe";
#endif

#if (DEBUG)
        public static string SmartCadMapFilename = Path.Combine(Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.Parent.FullName, "parser\\Bin\\Debug\\SmartCadMap.exe");
#else
        public const string SmartCadMapFilename = ".\\SmartCadMap.exe";
#endif

#if (DEBUG)
        public static string TelephonyConsoleFilename = Path.Combine(Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.Parent.FullName, "parser\\Bin\\Debug\\TelephonyConsole.exe");
#else
        public const string TelephonyConsoleFilename = "./TelephonyConsole.exe";
#endif

#if (DEBUG)
        public const string ClosedHandCursorFilename = "../../Dist/Bin/closedhand.ico";
#else
        public const string ClosedHandCursorFilename = "./closedhand.ico";
#endif

#if (DEBUG)
        public const string OpenHandCursorFilename = "../../Dist/Bin/openhand.ico";
#else
        public const string OpenHandCursorFilename = "./openhand.ico";
#endif


#if (DEBUG)
        public const string BusyCursorFilename = "../../Dist/Bin/AeroWorking.ani";
#else
        public const string BusyCursorFilename = "./AeroWorking.ani";
#endif


#if (DEBUG)
        public const string ReportXMLFilename = "../../Dist/Reports/Reports.xml";
#else
        public const string ReportXMLFilename = "../Reports/Reports.xml";
#endif

#if (DEBUG)
        public const string IncidentSql = "../../Dist/Bin/incident.sql";
#else
        public const string IncidentSql = "./incident.sql";
#endif

#if (DEBUG)
        public const string CctvIncident = "../../Dist/Bin/CctvIncident.sql";
#else
        public const string CctvIncident = "./CctvIncident.sql";
#endif
#if (DEBUG)
        public static string AlarmIncident = Path.Combine(Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.Parent.FullName, "parser\\Dist\\Bin\\AlarmIncident.sql");
#else
        public static string AlarmIncident = "./AlarmIncident.sql";//Path.Combine(Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.Parent.FullName, "Dist\\Bin\\AlarmIncident.sql");
#endif

#if (DEBUG)
        public const string IncidentHtmlExample = "../../Dist/Bin/incident.html";
#else
        public const string IncidentHtmlExample = "./incident.html";
#endif

#if (DEBUG)
        public static string UpgradeSchemaDataDB = Path.Combine(Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.Parent.FullName, "parser\\Dist\\Bin\\UpgradeSchemaDataDB.sql");
#else
        public static string UpgradeSchemaDataDB = "./UpgradeSchemaDataDB.sql";//Path.Combine(Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.Parent.FullName, "Dist\\Bin\\UpgradeSchemaDataDB.sql");
#endif

#if (DEBUG)
        public static string CheckSchemaDataDB = Path.Combine(Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.Parent.FullName, "parser\\Dist\\Bin\\CheckSchemaDataDB.sql");
#else
        public static string CheckSchemaDataDB = "./CheckSchemaDataDB.sql";//Path.Combine(Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.Parent.FullName, "Dist\\Bin\\CheckSchemaDataDB.sql");
#endif

#if (DEBUG)
        public const string ResourceFilename = "../../SmartCadCore.Resources/Resources.resx";
        public const string AudioRecordingFolder = "C:/AudioRecording";
#else
        public const string AudioRecordingFolder = "../AudioRecording/";
#endif

#if (DEBUG)
        public static string RingingWav = Path.Combine(Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.Parent.FullName, "parser\\Dist\\Bin\\Telephone.WAV");
#else
        public const string RingingWav = "./Telephone.WAV";
#endif

        public const int SERVER_SERVICE_PORT = 2007;

        public const string SERVER_SERVICE_ADDRESS = "Suass/ServerService";

        public const int TELEPHONY_SERVICE_PORT = 2009;

        public const string TELEPHONY_SERVICE_ADDRESS = "Suass/TelephonyService";

        public const int FILE_SERVER_SERVICE_PORT = 2010;

        public const string FILE_SERVER_SERVICE_ADDRESS = "Suass/FileServerService";

        public const string CLIENT_SERVICE_ADDRESS = "Suass/ClientService";

        public const int RECEIVE_TIMEOUT = 5000;

        public const int MAX_RESTART_TIMES = 3;

        public const int MINIMUM_RUNTIME = 1;

        private static System.Configuration.Configuration Configuration;

#if (DEBUG)
        private static string incidentXslt =  Path.Combine(Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.Parent.FullName, "parser\\Dist\\Bin\\incident_en.xsl");//"../../Dist/Bin/incident_en.xsl";
#else
        private static string incidentXslt = "./incident_en.xsl";
#endif

#if (DEBUG)
        // private static string cctvIncidentXslt = "Dist/Bin/CctvIncident_en.xsl";

        public static string cctvIncidentXslt = Path.Combine(Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.Parent.FullName, "parser\\Dist\\Bin\\CctvIncident_en.xsl");

#else
        private static string cctvIncidentXslt = "./CctvIncident_en.xsl";
#endif

#if (DEBUG)
        private static string lprIncidentXslt = "../../Dist/Bin/LprIncident_en.xsl";
#else
        private static string lprIncidentXslt = "../Bin/LprIncident_en.xsl";
#endif

#if (DEBUG)
        private static string vehicleRequestXslt = "../../Dist/Bin/vehicleRequest.xsl";
#else
        private static string vehicleRequestXslt = "../Bin/vehicleRequest.xsl";
#endif

#if (DEBUG)
        private static string alarmIncidentXslt = "../../Dist/Bin/AlarmIncident_en.xsl";
#else
        private static string alarmIncidentXslt = "../Bin/AlarmIncident_en.xsl";
#endif


#if (DEBUG)
        private static string phoneReportXslt =Path.Combine(Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.Parent.FullName, "parser\\Dist\\Bin\\phonereport_en.xsl");
#else
        private static string phoneReportXslt = "./phonereport_en.xsl";
#endif

#if (DEBUG)
        private static string cctvAlarmXslt = Path.Combine(Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.Parent.FullName, "parser\\Dist\\Bin\\CctvIncident_en.xsl");
#else
        private static string cctvAlarmXslt = "./CctvIncident_en.xsl";
#endif


        public SmartCadConfiguration()
        {

        }

        public static void Load()
        {
            Load(ConfigFilename);

            string language = "_" + SmartCadSection.CultureElement.CultureLanguageElement.Name;

            if (language != null && language != string.Empty)
            {
                incidentXslt = incidentXslt.Replace("_es", language);
                cctvIncidentXslt = cctvIncidentXslt.Replace("_es", language);
                phoneReportXslt = phoneReportXslt.Replace("_es", language);
                alarmIncidentXslt = alarmIncidentXslt.Replace("_es", language);
                lprIncidentXslt = lprIncidentXslt.Replace("_es", language);
            }
        }

        public static void Load(string path)
        {
            ExeConfigurationFileMap exeConfigurationFileMap = new ExeConfigurationFileMap();

            exeConfigurationFileMap.ExeConfigFilename = path;

            Configuration =
                ConfigurationManager.OpenMappedExeConfiguration(exeConfigurationFileMap,
                ConfigurationUserLevel.None);

            //log4net configuration
            XmlConfigurator.Configure(new FileInfo(Configuration.FilePath));
        }

        public static SmartCadConfigurationSection SmartCadSection
        {
            get
            {
                if (Configuration == null)
                    Load();
                SmartCadConfigurationSection configurationSection = Configuration.GetSection("smartCad")
                    as SmartCadConfigurationSection;

                return configurationSection;
            }
        }


        public static string IncidentXslt
        {
            get
            {
                return incidentXslt;
            }
        }

        public static string CctvIncidentXslt
        {
            get
            {
                return cctvIncidentXslt;
            }
        }

        public static string LprIncidentXslt
        {
            get
            {
                return lprIncidentXslt;
            }
        }

        public static string VehicleRequestXslt
        {
            get
            {
                return vehicleRequestXslt;
            }
        }

        public static string AlarmIncidentXslt
        {
            get
            {
                return alarmIncidentXslt;
            }
        }

        public static string PhoneReportXslt
        {
            get
            {
                return phoneReportXslt;
            }
        }


        public static string ServiceName
        {
            get
            {
                return ResourceLoader.GetString2("ServerConsoleService") + SmartCadConfiguration.Version;
            }
        }

        public static IDictionary<string, string> NHibernateProperties
        {
            get
            {
                NHibernateElement element = SmartCadSection.NHibernateElement;

                Dictionary<string, string> properties = new Dictionary<string, string>();

                foreach (PropertyElement property in element.Properties)
                {
                    properties.Add(property.Name, property.Value);
                }
                return properties;
            }
        }

        public static string DataBaseConnectionString
        {
            get
            {
                NHibernateElement element = SmartCadSection.NHibernateElement;
                PropertyElement property = element.Properties["connection.connection_string"];

                return property.Value;
            }
            set
            {
                NHibernateElement element = SmartCadSection.NHibernateElement;
                PropertyElement property = element.Properties["connection.connection_string"];

                property.Value = value;
            }
        }

        public static string SoftwareVersion
        {
            get
            {
                return Version + " ";
            }
        }

        public static string DataBaseServer
        {
            get
            {
                return GetConnectionStringValue("Server");
            }
            set
            {
                SetConnectionStringValue("Server", value);
            }
        }

        public static string DataBaseName
        {
            get
            {
                return GetConnectionStringValue("Initial Catalog");
            }
            set
            {
                SetConnectionStringValue("Initial Catalog", value);
            }
        }

        public static string ReportServer
        {
            get
            {
                return SmartCadSection.ServerElement.ReportsElement.DatabaseServer;
            }
            set
            {
                SmartCadSection.ServerElement.ReportsElement.DatabaseServer = value;
            }
        }

        public static string ReportName
        {
            get
            {
                return SmartCadSection.ServerElement.ReportsElement.DatabaseName;
            }
            set
            {
                SmartCadSection.ServerElement.ReportsElement.DatabaseName = value;
            }
        }


        public static string AVLServer
        {
            get
            {
                return SmartCadSection.AvlClient.IP;
            }
            set
            {
                SmartCadSection.AvlClient.IP = value;
            }
        }

        public static int AVLPort
        {
            get
            {
                return SmartCadSection.AvlClient.Port;
            }
            set
            {
                SmartCadSection.AvlClient.Port = value;
            }
        }

        public static string TrustedConnection
        {
            get
            {
                return GetConnectionStringValue("Trusted_Connection");
            }
            set
            {
                SetConnectionStringValue("Trusted_Connection", value);
            }
        }

        public static string ProductCode
        {
            get
            {
                return "ProductCode";
            }
        }

        public static string Serial
        {
            get
            {
                return "Serial";
            }
        }

        public static string ServerAddress
        {
            get
            {
                return SmartCadSection.ClientElement.ServerAddressElement.IP;
            }
            set
            {
                SmartCadSection.ClientElement.ServerAddressElement.IP = value;
            }
        }

        public static void Save()
        {
            Configuration.Save();
        }

        private static string GetConnectionStringValue(string name)
        {
            NHibernateElement element = SmartCadSection.NHibernateElement;
            PropertyElement property = element.Properties["connection.connection_string"];

            string[] arr = property.Value.Split(';');
            NameValueCollection collection = new NameValueCollection();

            foreach (string s in arr)
            {
                string[] temp = s.Split('=');
                if (temp.Length == 2)
                    collection.Add(temp[0], temp[1]);
            }

            return collection[name];
        }

        private static void SetConnectionStringValue(string name, string value)
        {
            NHibernateElement element = SmartCadSection.NHibernateElement;
            PropertyElement property = element.Properties["connection.connection_string"];

            string[] arr = property.Value.Split(';');
            NameValueCollection collection = new NameValueCollection();

            foreach (string s in arr)
            {
                string[] temp = s.Split('=');
                if (temp.Length == 2)
                    collection.Add(temp[0], temp[1]);
            }

            collection[name] = value;

            string connectionString = "";

            foreach (string key in collection.Keys)
            {
                connectionString = connectionString +
                    key + "=" + collection[key] + ";";
            }

            property.Value = connectionString;
        }

        public static void SaveConfigurationInClient(ConfigurationClientData configuration)
       {
            string nameMaps = configuration.MapName;
            SmartCadConfiguration.SmartCadSection.ServerElement.Map.Dll = configuration.MapDLL;
            SmartCadConfiguration.SmartCadSection.Maps[nameMaps].CenterLon = configuration.MapLon.ToString().Replace(",", ".");
            SmartCadConfiguration.SmartCadSection.Maps[nameMaps].CenterLat = configuration.MapLat.ToString().Replace(",", ".");
            SmartCadConfiguration.SmartCadSection.ServerElement.ReportsElement.DatabaseServer = configuration.ReportsDatabaseServer;
            SmartCadConfiguration.SmartCadSection.ServerElement.ReportsElement.DatabaseName = configuration.ReportsDatabaseName;
            Save();

        }

        //Para la vista de listar alarmas
        public static string CctvAlarmXslt
        {
            get
            {
                return cctvAlarmXslt;
            }
        }
        public static VideoAnaliticaCommunicationElement VideoAnaliticaProperties
        {
            get
            {
                return  SmartCadConfiguration.SmartCadSection.ServerElement.VideoAnaliticaCommunicationElement;               
            }
        }
}
}