using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace SmartCadCore.Core
{
    #region Class ServerAddressElement Documentation
    /// <summary>
    /// TODO:
    /// </summary>
    /// <className>CustomerElement</className>
    /// <author email="atorres@smartmatic.com">Alden Torres</author>
    /// <date>2006/03/30</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    public class ServerAddressElement : ConfigurationElement
    {
        public ServerAddressElement()
        {
        }

        [ConfigurationProperty("ip")]
        public string IP
        {
            get
            {
                return this["ip"] as string;
            }
            set
            {
                this["ip"] = value;
            }
        }
    }
}
