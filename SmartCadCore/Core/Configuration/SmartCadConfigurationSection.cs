using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Globalization;

namespace SmartCadCore.Core
{
    #region Class SmartCadConfigurationSection Documentation
    /// <summary>
    /// TODO:
    /// </summary>
    /// <className>SmartCadConfigurationSection</className>
    /// <author email="atorres@smartmatic.com">Alden Torres</author>
    /// <date>2006/03/30</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    public class SmartCadConfigurationSection : ConfigurationSection
    {
        public SmartCadConfigurationSection()
        {
        }
        
        [ConfigurationProperty("client")]
        public ClientElement ClientElement
        {
            get
            {
                return this["client"] as ClientElement;
            }
            set
            {
                this["client"] = value;
            }
        }

     

        [ConfigurationProperty("printToConsole")]
        public PrintToConsoleElement PrintToConsoleElement
        {
            get
            {
                return this["printToConsole"] as PrintToConsoleElement;
            }
            set
            {
                this["printToConsole"] = value;
            }
        }
        
        [ConfigurationProperty("nhibernate")]
        public NHibernateElement NHibernateElement
        {
            get
            {
                return this["nhibernate"] as NHibernateElement;
            }
            set
            {
                this["nhibernate"] = value;
            }
        }

        [ConfigurationProperty("communicationFoundation")]
        public CommunicationFoundationElement CommunicationFoundationElement
        {
            get
            {
                return this["communicationFoundation"] as CommunicationFoundationElement;
            }
            set
            {
                this["communicationFoundation"] = value;
            }
        }

        [ConfigurationProperty("server")]
        public ServerElement ServerElement
        {
            get
            {
                return this["server"] as ServerElement;
            }
            set
            {
                this["server"] = value;
            }
        }

        [ConfigurationProperty("culture")]
        public CultureElement CultureElement
        {
            get
            {
                return this["culture"] as CultureElement;
            }
            set
            {
                this["culture"] = value;
            }
        }

        [ConfigurationProperty("deviceServer")]
        public DeviceServerElement DeviceServer
        {
            get
            {
                return this["deviceServer"] as DeviceServerElement;
            }
            set
            {
                this["deviceServer"] = value;
            }
        }

        [ConfigurationProperty("avlSync")]
        public AvlSyncElement AvlSync
        {
            get
            {
                return this["avlSync"] as AvlSyncElement;
            }
            set
            {
                this["avlSync"] = value;
            }
        }

        [ConfigurationProperty("avlClient")]
        public AvlClientElement AvlClient
        {
            get
            {
                return this["avlClient"] as AvlClientElement;
            }
            set
            {
                this["avlClient"] = value;
            }
        }

        [ConfigurationProperty("mapName")]
        public MapNameElement MapName
        {
            get
            {
                return this["mapName"] as MapNameElement;
            }
            set
            {
                this["mapName"] = value;
            }
        }

        [ConfigurationProperty("maps")]
        [ConfigurationCollection(typeof(MapCollection), AddItemName = "map")]
        public MapCollection Maps
        {
            get
            {
                return this["maps"] as MapCollection;
            }
            set
            {
                this["maps"] = value;
            }
        }

        public double mapLon = double.MinValue;
        public double DefaultMapLon
        {
            get
            {
                //if (mapLon == double.MinValue)
                //{
                    foreach (MapElement element in SmartCadConfiguration.SmartCadSection.Maps)
                    {
                    //mapLon = double.Parse(element.CenterLon.Replace('.', ','));
                        double.TryParse(element.CenterLon.Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture, out mapLon);
                        break;
                    }
                    return mapLon;
                //}
                //else
                //{
                //    return mapLon;
                //}
            }
        }

        public double mapLat = double.MinValue;
        public double DefaultMapLat
        {
            get
            {
                //if (mapLat == double.MinValue)
                //{
                    foreach (MapElement element in SmartCadConfiguration.SmartCadSection.Maps)
                    {
                        //mapLat = double.Parse(element.CenterLat.Replace('.',','),CultureInfo.InvariantCulture);
                        double.TryParse(element.CenterLat.Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture, out mapLat);
                        break;
                    }
                    return mapLat;
                //}
                //else
                //{
                //    return mapLat;
                //}
            }
        }

    }
}
