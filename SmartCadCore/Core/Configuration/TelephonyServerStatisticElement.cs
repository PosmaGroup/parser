﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace SmartCadCore.Core
{
    public class TelephonyServerStatisticElement : ConfigurationElement
    {
        public TelephonyServerStatisticElement()
        {
        }

        public static TelephonyServerTypes CurrentServerType;

        /// <summary>
        /// This property specify if it is being used genesys, nortel or any other supported pbx.
        /// It would be requiered if this configuration only appeared in server, not in client.
        /// </summary>
        [ConfigurationProperty("serverType", DefaultValue = "Nortel" /*, IsRequired = true*/)]
        public string ServerType
        {
            get
            {
                CurrentServerType = ServerElement.GetServerType(this["serverType"] as string);
                return this["serverType"] as string;
            }
            set
            {
                this["serverType"] = value;
                CurrentServerType = ServerElement.GetServerType(this["serverType"] as string);
            }
        }

        [ConfigurationProperty("serverIp")]
        public string Server
        {
            get
            {
                CurrentServerType = ServerElement.GetServerType(this["serverType"] as string);
                return this["serverIp"] as string;
            }
            set
            {
                this["serverIp"] = value;
            }
        }

        [ConfigurationProperty("serverPort")]
        public string Port
        {
            get
            {
                return this["serverPort"] as string;
            }
            set
            {
                this["serverPort"] = value;
            }
        }

        [ConfigurationProperty("serviceName")]
        public string ServiceName
        {
            get
            {
                return (string)this["serviceName"];
            }
            set
            {
                this["serviceName"] = value;
            }
        }

        [ConfigurationProperty("serverLogin")]
        public string ServerLogin
        {
            get
            {
                return this["serverLogin"] as string;
            }
            set
            {
                this["serverLogin"] = value;
            }
        }

        [ConfigurationProperty("serverPassword")]
        public string ServerPassword
        {
            get
            {
                string p = this["serverPassword"] as string;
                if (string.IsNullOrEmpty(p) == true)
                    return string.Empty;
                else
                    return CryptoUtil.DecryptWithRijndael(this["serverPassword"] as string).TrimEnd('\0');
            }
            set
            {
                string p = string.Empty;
                if (string.IsNullOrEmpty(value) == false)
                    p = CryptoUtil.EncryptWithRijndael(value);
                this["serverPassword"] = p;
            }
        }

        [ConfigurationProperty("script")]
        public string Script
        {
            get
            {
                return this["script"] as string;
            }
            set
            {
                this["script"] = value;
            }
        }
    }
}
