using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace SmartCadCore.Core
{
    public enum TelephonyServerTypes
    {
        None,
        Genesys,
        Nortel,
        Asterisk,
        SIP,
        Avaya
    }

    public class ServerElement : ConfigurationElement
    {
        public static TelephonyServerTypes GetServerType(string serverTypeStr)
        {
            TelephonyServerTypes type = TelephonyServerTypes.None;
            if (string.IsNullOrEmpty(serverTypeStr) == false)
            {
                if (serverTypeStr.Equals(TelephonyServerTypes.Asterisk.ToString()))
                {
                    type = TelephonyServerTypes.Asterisk;
                }
                else if (serverTypeStr.Equals(TelephonyServerTypes.Genesys.ToString()))
                {
                    type = TelephonyServerTypes.Genesys;
                }
                else if (serverTypeStr.Equals(TelephonyServerTypes.Nortel.ToString()))
                {
                    type = TelephonyServerTypes.Nortel;
                }
                else if (serverTypeStr.Equals(TelephonyServerTypes.Avaya.ToString()))
                {
                    type = TelephonyServerTypes.Avaya;
                }
                else
                {
                    type = TelephonyServerTypes.SIP;
                }
            }
            return type;
        }

        [ConfigurationProperty("map")]
        public MapDllElement Map
        {
            get
            {
                return this["map"] as MapDllElement;
            }
            set
            {
                this["map"] = value;
            }
        }

        [ConfigurationProperty("spatialware")]
        public SpatialwareElement SpatialwareElement
        {
            get
            {
                return this["spatialware"] as SpatialwareElement;
            }
            set
            {
                this["spatialware"] = value;
            }
        }

        [ConfigurationProperty("initialData")]
        public InitialDataElement InitialDataElement
        {
            get
            {
                return this["initialData"] as InitialDataElement;
            }
            set
            {
                this["initialData"] = value;
            }
        }

        [ConfigurationProperty("reports")]
        public ReportsElement ReportsElement
        {
            get
            {
                return this["reports"] as ReportsElement;
            }
            set
            {
                this["reports"] = value;
            }
        }

        [ConfigurationProperty("internalServerAddress")]
        public InternalServerAddressElement InternalServerAddressElement
        {
            get
            {
                return this["internalServerAddress"] as InternalServerAddressElement;
            }
            set
            {
                this["internalServerAddress"] = value;
            }
        }

        [ConfigurationProperty("telephonyServerStatistics")]
        public TelephonyServerStatisticElement TelephonyServerStatisticElement
        {
            get
            {
                return this["telephonyServerStatistics"] as TelephonyServerStatisticElement;
            }
            set
            {
                this["telephonyServerStatistics"] = value;
            }
        }

        [ConfigurationProperty("telephonyServerCommunication")]
        public TelephonyServerCommunicationElement TelephonyServerCommunicationElement
        {
            get
            {
                return this["telephonyServerCommunication"] as TelephonyServerCommunicationElement;
            }
            set
            {
                this["telephonyServerCommunication"] = value;
            }
        }

        [ConfigurationProperty("vmsServerCommunication")]
        public VmsServerCommunicationElement VmsServerCommunicationElement
        {
            get
            {
                return this["vmsServerCommunication"] as VmsServerCommunicationElement;
            }
            set
            {
                this["vmsServerCommunication"] = value;
            }
        }

        [ConfigurationProperty("videoAnaliticaCommunication")]
        public VideoAnaliticaCommunicationElement VideoAnaliticaCommunicationElement
        {
            get
            {
                return this["videoAnaliticaCommunication"] as VideoAnaliticaCommunicationElement;
            }
            set
            {
                this["videoAnaliticaCommunication"] = value;
            }
        }

        [ConfigurationProperty("indicators")]
        public IndicatorsElement IndicatorsElement
        {
            get
            {
                return this["indicators"] as IndicatorsElement;
            }
            set
            {
                this["indicators"] = value;
            }
        }
    }

   
    public class VmsServerCommunicationElement : ConfigurationElement
    {
        public VmsServerCommunicationElement()
        { }

        [ConfigurationProperty("serverIp")]
        public string Server
        {
            get
            {
                return this["serverIp"] as string;
            }
            set
            {
                this["serverIp"] = value;
            }
        }

        [ConfigurationProperty("dll")]
        public string dllName
        {
            get
            {
                return this["dll"] as string;
            }
            set
            {
                this["dll"] = value;
            }
        }

        [ConfigurationProperty("login")]
        public string Login
        {
            get
            {
                return this["login"] as string;
            }
            set
            {
                this["login"] = value;
            }
        }

        [ConfigurationProperty("password")]
        public string Password
        {
            get
            {
                return this["password"] as string;
            }
            set
            {
                this["password"] = value;
            }
        }

        [ConfigurationProperty("portVms")]
        public string PortVms
        {
            get
            {
                return this["portVms"] as string;
            }
            set
            {
                this["portVms"] = value;
            }
        }


        [ConfigurationProperty("eventsServerIp")]
        public string EventsServer
        {
            get
            {
                return this["eventsServerIp"] as string;
            }
            set
            {
                this["eventsServerIp"] = value;
            }
        }
    }

    public class VideoAnaliticaCommunicationElement : ConfigurationElement{
        public VideoAnaliticaCommunicationElement(){

        }

        [ConfigurationProperty("activate")]
        public bool activate
        {
            get
            {
                return (bool)this["activate"];
            }
            set
            {
                this["activate"] = value;
            }
        }

        [ConfigurationProperty("serverIp")]
        public string ServerIP{
            get{
                return this["serverIp"] as string;
            }
            set
            {
                this["serverIp"] = value;
            }
        }

        [ConfigurationProperty ("serverPort")]
        public string ServerPort{
            get{
                return this["serverPort"] as string;
            }
            set
            {
                this["serverPort"] = value;
            }
        }

        [ConfigurationProperty ("serverUser")]
        public string ServerUser{
            get{
                return this["serverUser"] as string;
            }
            set
            {
                this["serverUser"] = value;
            }
        }

        [ConfigurationProperty ("serverPassword")]
        public string ServerPassword{
            get{
                return this["serverPassword"] as string;
            }
            set
            {
                this["serverPassword"] = value;
            }
        }
    }
    public class IndicatorsElement : ConfigurationElement
    {
        public IndicatorsElement()
        { }

        [ConfigurationProperty("sleepTime")]
        public int SleepTime
        {
            get
            {
                return (int)this["sleepTime"];
            }
            set
            {
                this["sleepTime"] = value;
            }
        }
    }
}