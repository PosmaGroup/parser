using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace SmartCadCore.Core
{
    #region Class ExtensionNumberElement Documentation
    /// <summary>
    /// TODO:
    /// </summary>
    /// <className>ExtensionNumberElement</className>
    /// <author email="atorres@smartmatic.com">Alden Torres</author>
    /// <author email="fsilva@smartmatic.com">Fernando Silva</author>
    /// <date>2006/03/30</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    public class ExtensionNumberElement : ConfigurationElement
    {
        public ExtensionNumberElement()
        {
        }

        [ConfigurationProperty("number")]
        public string Number
        {
            get
            {
                return this["number"] as string;
            }
            set
            {
                this["number"] = value;
            }
        }

        [ConfigurationProperty("password")]
        public string Password
        {
            get
            {
                return this["password"] as string;
            }
            set
            {
                this["password"] = value;
            }
        }

        [ConfigurationProperty("changeable")]
        public bool Changeable
        {
            get
            {
                return (bool)this["changeable"];
            }
            set
            {
                this["changeable"] = value;
            }
        }
    }
}
