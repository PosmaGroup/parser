using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace SmartCadCore.Core
{
    public class InitialDataElement : ConfigurationElement
    {
        public InitialDataElement()
        {
        }

        [ConfigurationProperty("createTestData")]
        public bool CreateTestData
        {
            get
            {
                return (bool)this["createTestData"];
            }
            set
            {
                this["createTestData"] = value;
            }
        }
    }
}
