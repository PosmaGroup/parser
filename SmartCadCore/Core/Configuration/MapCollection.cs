﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace SmartCadCore.Core
{

    public class MapCollection : ConfigurationElementCollection
    {
        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((MapElement)element).Id;
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new MapElement();
        }

        public new MapElement this[string id]
        {
            get
            {
                return BaseGet(id) as MapElement;
            }
        }
    }

    public class MapNameElement : ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true, IsKey = true)]
        public string Name
        {
            get
            {
                return this["name"] as string;
            }
            set
            {
                this["name"] = value;
            }
        }
    }
        public class MapElement : ConfigurationElement
    {
        [ConfigurationProperty("id", IsRequired = true, IsKey = true)]
        public string Id
        {
            get
            {
                return this["id"] as string;
            }
            set
            {
                this["id"] = value;
            }
        }
        [ConfigurationProperty("centerLat", IsRequired = true)]
        public string CenterLat
        {
            get
            {
                return this["centerLat"] as string;
            }
            set
            {
                this["centerLat"] = value;
            }
        }
        [ConfigurationProperty("centerLon", IsRequired = true)]
        public string CenterLon
        {
            get
            {
                return this["centerLon"] as string;
            }
            set
            {
                this["centerLon"] = value;
            }
        }

        [ConfigurationProperty("layers", IsRequired = true)]
        [ConfigurationCollection(typeof(LayerCollection), AddItemName = "layer")]
        public LayerCollection Layers
        {
            get
            {
                return this["layers"] as LayerCollection;
            }
            set
            {
                this["layers"] = value;
            }
        }
    }    
}
