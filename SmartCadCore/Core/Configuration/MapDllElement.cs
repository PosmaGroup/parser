﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace SmartCadCore.Core
{
    public class MapDllElement : ConfigurationElement
    {
        public MapDllElement()
        {
        }

        [ConfigurationProperty("dll", IsRequired = true, IsKey = true)]
        public string Dll
        {
            get
            {
                return this["dll"] as string;
            }
            set
            {
                this["dll"] = value;
            }
        }

        [ConfigurationProperty("informationFileName", IsRequired = true)]
        public string InformationFileName
        {
            get
            {
                return this["informationFileName"] as string;
            }
            set
            {
                this["informationFileName"] = value;
            }
        }

        [ConfigurationProperty("local", IsRequired = true)]
        public string Local
        {
            get
            {
                return this["local"] as string;
            }
            set
            {
                this["local"] = value;
            }
        }
    }
}

