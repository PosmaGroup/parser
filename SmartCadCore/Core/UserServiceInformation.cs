using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SmartCadCore.Core;
using System.Security.Principal;
using System.Threading;
using SmartCadCore;
using SmartCadCore.Common;

namespace SmartCadCore.Core
{
    public partial class UserServiceInformation :  Form
    {
        public UserServiceInformation()
        {
            InitializeComponent();
        }

        private void ActiveOkButton(object sender, EventArgs e)
        {
            if (textBoxUserName.Text.Trim() != string.Empty &&
                textBoxPassword.Text.Trim() != string.Empty &&
                textBoxDomain.Text.Trim() != string.Empty)
            {
                buttonOK.Enabled = true;
            }
            else
            {
                buttonOK.Enabled = false;
            }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            try
            {
                if (Common.Domain.ValidateUser(textBoxUserName.Text.Trim(), textBoxPassword.Text.Trim(), textBoxDomain.Text.Trim()) == false)
                {
                    MessageBox.Show(ResourceLoader.GetString2("InvalidLogin", textBoxUserName.Text.Trim()));
                    DialogResult = DialogResult.None;
                }
                else
                {
                    if (Common.Domain.IsDomainUserLocalAdmin(textBoxUserName.Text.Trim()) == false)
                    {
                        MessageBox.Show(ResourceLoader.GetString2("NotAdministratorUser", textBoxUserName.Text.Trim()));
                        DialogResult = System.Windows.Forms.DialogResult.None;
                    }
                }
            }
            catch
            { }
        }

        public string UserName
        {
            get
            {
                return textBoxUserName.Text.Trim();
            }
        }

        public string Password
        {
            get
            {
                return textBoxPassword.Text.Trim();
            }
        }

        public string Domain
        {
            get
            {
                return textBoxDomain.Text.Trim();
            }
        }
    }
}