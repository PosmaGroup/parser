using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.Model;
using System.Diagnostics;
using System.Reflection;
using SmartCadCore.ClientData;

namespace SmartCadCore.Core
{
    public class SmartCadAudit
    {
        private static AuditData Audit()
        {
            StackTrace callStack = new StackTrace(); // The call stack
            StackFrame frame = callStack.GetFrame(2); // The frame that called me
            MethodBase method = frame.GetMethod(); // The method that called me
            AuditData audit = new AuditData();
            audit.Action = method.Name;
            audit.DateTime = SmartCadDatabase.GetTimeFromBD();
            return audit;
        }

        public static void Audit(UserApplicationData application, string login,int code)
        {
            try
            {
                AuditData auditData = Audit();
                auditData.Login = login;
                auditData.OperatorCode = code+"";
                auditData.UserApplication = application.FriendlyName;
                SmartCadDatabase.SaveOrUpdate(auditData);
            }
            catch
            {
            }
        }

        public static void Audit(UserApplicationData application, string login, int code, string message)
        {
            try
            {
                AuditData auditData = Audit();
                auditData.Login = login;
                auditData.OperatorCode = code+"";
                auditData.UserApplication = application.FriendlyName;
                auditData.Message = message;
                SmartCadDatabase.SaveOrUpdate(auditData);
            }
            catch
            { 
            }
        }

        public static void Audit(UserApplicationData application, string login, int code, 
            UserResourceData userResource, UserActionData userAction)
        {
            try
            {
                AuditData auditData = Audit();
                auditData.Login = login;
                auditData.OperatorCode = code+"";
                auditData.UserApplication = application.FriendlyName;
                auditData.UserResource = userResource.FriendlyName;
                auditData.UserAction = userAction.FriendlyName;
                SmartCadDatabase.SaveOrUpdate(auditData);
            }
            catch
            {
            }
        }

        public static void Audit(UserApplicationData application, string login, int code, string message,ObjectData obj,OperatorActions action)
        {
            try
            {
                AuditData auditData = Audit();
                auditData.Login = login;
                auditData.OperatorCode = code+"";
                auditData.UserApplication = application.FriendlyName;
                auditData.Message = message;
                string table = "";
                int objCode = 0;
                GetTableFromObject(obj, ref table, ref objCode);
                auditData.ObjectCode = objCode;
                auditData.ObjectTable = table;
                auditData.UserAction = action.ToString() +" "+ message;
                SmartCadDatabase.SaveOrUpdate(auditData);
            }
            catch
            {
            }
        }

        private static void GetTableFromObject(ObjectData obj, ref string table, ref int objCode)
        {
            if (obj != null)
            {
                objCode = obj.Code;
                Type type = obj.GetType();
                foreach (object var in type.GetCustomAttributes(true))
                {
                    if (var is NHibernate.Mapping.Attributes.ClassAttribute)
                    {
                        table = (var as NHibernate.Mapping.Attributes.ClassAttribute).Table;
                        break;
                    }
                }
            }
        }

        public static void Audit(UserApplicationData application, string login, int code, string typeName, string message)
        {
            try
            {
                AuditData auditData = Audit();
                auditData.Login = login;
                auditData.OperatorCode = code+"";
                auditData.UserApplication = application.FriendlyName;
                auditData.TypeName = typeName;
                auditData.Message = message;
                SmartCadDatabase.SaveOrUpdate(auditData);
            }
            catch
            {
            }
        }

        public static void Audit(UserApplicationData application, string login, int code, OperatorActions action) 
        {
            try
            {
                AuditData auditData = Audit();
                auditData.Login = login;
                auditData.OperatorCode = code+"";
                auditData.UserApplication = application.FriendlyName;
                auditData.UserAction = action.ToString();
                SmartCadDatabase.SaveOrUpdate(auditData);
            }
            catch
            {
            }
            
        }
        
    }
}
