using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.Model;
using Iesi.Collections;
using SmartCadCore.Common;

namespace SmartCadCore.Core
{
    public partial class SmartCadInitialData
    {

        private static void CreateGenericQuestion()
        {
            //Define las preguntas genericas para los tipos de incidentes

            #region questionWhere
            QuestionData questionWhere = new QuestionData();
            questionWhere.Text = ResourceInitialDataLoader.GetString2("EmergencyWhere");
            questionWhere.ShortText = STRQUESTIONWHERE;
            questionWhere.CustomCode = "WHERE";
            questionWhere.RequirementType = RequirementTypeEnum.Required;//RequirementTypeEnum.None;
            

            SmartCadDatabase.SaveObject<QuestionData>(questionWhere);

            questionWhere.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            QuestionPossibleAnswerData answer1 = new QuestionPossibleAnswerData();
            answer1.Question = questionWhere;
            answer1.Description = ResourceLoader.GetString2("Zone");
            answer1.ControlName = "textEdit2";
            questionWhere.SetPossibleAnswers.Add(answer1);

            SmartCadDatabase.UpdateObject<QuestionData>(questionWhere);

            QuestionPossibleAnswerData answer2 = new QuestionPossibleAnswerData();
            answer2.Question = questionWhere;
            answer2.Description = ResourceLoader.GetString2("Street");
            answer2.ControlName = "textEdit1";
            questionWhere.SetPossibleAnswers.Add(answer2);

            SmartCadDatabase.UpdateObject<QuestionData>(questionWhere);

            QuestionPossibleAnswerData answer3 = new QuestionPossibleAnswerData();
            answer3.Question = questionWhere;
            answer3.Description = ResourceLoader.GetString2("Reference");
            answer3.ControlName = "textEdit3";
            questionWhere.SetPossibleAnswers.Add(answer3);

            SmartCadDatabase.UpdateObject<QuestionData>(questionWhere);

            QuestionPossibleAnswerData answer4 = new QuestionPossibleAnswerData();
            answer4.Question = questionWhere;
            answer4.Description = ResourceLoader.GetString2("More");
            answer4.ControlName = "textEdit4";
            questionWhere.SetPossibleAnswers.Add(answer4);

            SmartCadDatabase.UpdateObject<QuestionData>(questionWhere);

            //SmartCadDatabase.SaveObject<QuestionData>(questionWhere);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region RenderWhere
            string renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
<Control itemText='" + ResourceLoader.GetString2("Street") + @"' itemName='layoutControlItem2' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3449.27122, Culture=neutral, PublicKeyToken=null' name='textEdit1' />
  <Control itemText='" + ResourceLoader.GetString2("More") + @"' itemName='layoutControlItem5' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3449.27122, Culture=neutral, PublicKeyToken=null' name='textEdit4' />
  <Control itemText='layoutControlItem6' itemName='layoutControlItem6' type='DevExpress.XtraEditors.SimpleButton, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='copyAddress' image='$Image.CopyAddress' />
  <Control itemText='" + ResourceLoader.GetString2("Zone") + @"' itemName='layoutControlItem3' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3449.27122, Culture=neutral, PublicKeyToken=null' name='textEdit2' />
  <Control itemText='layoutControlItem7' itemName='layoutControlItem7' type='DevExpress.XtraEditors.SimpleButton, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='searchButton' image='$Image.SearchAddress'/>
  <Control itemText='" + ResourceLoader.GetString2("Reference") + @"' itemName='layoutControlItem4' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3449.27122, Culture=neutral, PublicKeyToken=null' name='textEdit3' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='8'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=332@2,Height=31</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>" + ResourceLoader.GetString2("Street") + @"</property>
      <property name='MinSize'>@3,Width=147@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>" + ResourceLoader.GetString2("Street") + @"</property>
      <property name='TextSize'>@2,Width=81@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>layoutControlItem2</property>
      <property name='Location'>@1,X=0@2,Y=68</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>textEdit1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=293@2,Height=31</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>" + ResourceLoader.GetString2("More") + @"</property>
      <property name='MinSize'>@3,Width=147@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>" + ResourceLoader.GetString2("More") + @"</property>
      <property name='TextSize'>@2,Width=81@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>layoutControlItem5</property>
      <property name='Location'>@3,X=332@2,Y=68</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>textEdit4</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item4' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@2,Width=37@2,Height=37</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>layoutControlItem6</property>
      <property name='MinSize'>@1,Width=1@1,Height=1</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>layoutControlItem6</property>
      <property name='TextSize'>@1,Width=0@1,Height=0</property>
      <property name='TextVisible'>false</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>0</property>
      <property name='Name'>layoutControlItem6</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@1,Height=0</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Custom</property>
      <property name='ControlName'>copyAddress</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item5' isnull='true' iskey='true'>
      <property name='Name'>emptySpaceItem1</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='ImageToTextDistance'>5</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='Text'>emptySpaceItem1</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Visibility'>Always</property>
      <property name='CustomizationFormText'>emptySpaceItem1</property>
      <property name='Size'>@3,Width=550@2,Height=37</property>
      <property name='TextSize'>@1,Width=0@1,Height=0</property>
      <property name='ParentName'>Root</property>
      <property name='Location'>@2,X=75@1,Y=0</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='TextVisible'>false</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='ControlName' isnull='true' />
      <property name='MinSize'>@3,Width=110@2,Height=30</property>
      <property name='TextLocation'>Default</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TypeName'>EmptySpaceItem</property>
      <property name='SizeConstraintsType'>Custom</property>
      <property name='MaxSize'>@1,Width=0@1,Height=0</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item6' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=332@2,Height=31</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>" + ResourceLoader.GetString2("Zone") + @"</property>
      <property name='MinSize'>@3,Width=147@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>" + ResourceLoader.GetString2("Zone") + @"</property>
      <property name='TextSize'>@2,Width=81@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>layoutControlItem3</property>
      <property name='Location'>@1,X=0@2,Y=37</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>textEdit2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item7' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@2,Width=38@2,Height=37</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>layoutControlItem7</property>
      <property name='MinSize'>@1,Width=1@1,Height=1</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>layoutControlItem7</property>
      <property name='TextSize'>@1,Width=0@1,Height=0</property>
      <property name='TextVisible'>false</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>0</property>
      <property name='Name'>layoutControlItem7</property>
      <property name='Location'>@2,X=37@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@1,Height=0</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Custom</property>
      <property name='ControlName'>searchButton</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item8' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=293@2,Height=31</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>" + ResourceLoader.GetString2("Reference") + @"</property>
      <property name='MinSize'>@3,Width=147@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>" + ResourceLoader.GetString2("Reference") + @"</property>
      <property name='TextSize'>@2,Width=81@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>layoutControlItem4</property>
      <property name='Location'>@3,X=332@2,Y=37</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>textEdit3</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>";
            questionWhere.RenderMethod = renderMethod; 
            #endregion

            SmartCadDatabase.UpdateObject<QuestionData>(questionWhere);
            #endregion

            if (SmartCadConfiguration.SmartCadSection.ServerElement.InitialDataElement.CreateTestData)
            {
                #region numberPerson
                QuestionData questionNumberPerson = new QuestionData();
                questionNumberPerson.Text = ResourceInitialDataLoader.GetString2("QuestionNumberPersonsText");
                questionNumberPerson.ShortText = STRQUESTIONNUMBERPERSON;
                questionNumberPerson.CustomCode = "PERSONNUMBER";
                questionNumberPerson.RequirementType = RequirementTypeEnum.Recommended;
                questionNumberPerson.SetPossibleAnswers = new HashedSet();
                

                //Esta estructura se maneja de manera generica para todos los incidentes
                answer1 = new QuestionPossibleAnswerData();
                answer1.Question = questionNumberPerson;
                answer1.Description = ResourceLoader.GetString2("0");
                answer1.ControlName= "RdBttn2";
                questionNumberPerson.SetPossibleAnswers.Add(answer1);

                answer2 = new QuestionPossibleAnswerData();
                answer2.Question = questionNumberPerson;
                answer2.Description = "1 "+ ResourceLoader.GetString2("To") + " 5";
                answer2.ControlName= "RdBttn3";
                questionNumberPerson.SetPossibleAnswers.Add(answer2);

                answer3 = new QuestionPossibleAnswerData();
                answer3.Question = questionNumberPerson;
                answer3.Description = "5 " + ResourceLoader.GetString2("To") + " 10";
                answer3.ControlName= "RdBttn4";
                questionNumberPerson.SetPossibleAnswers.Add(answer3);

                answer4 = new QuestionPossibleAnswerData();
                answer4.Question = questionNumberPerson;
                answer4.Description = ResourceLoader.GetString2("MoreThan") + " 10";
                answer4.ControlName= "RdBttn1";
                questionNumberPerson.SetPossibleAnswers.Add(answer4);

                SmartCadDatabase.SaveObject<QuestionData>(questionNumberPerson);

                //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
                #region Render Number Person
                renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Mas de 10' itemName='item1' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn1' />
  <Control itemText='0' itemName='item2' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn2' />
  <Control itemText='1 a 5' itemName='item3' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn3' />
  <Control itemText='5 a 10' itemName='item4' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn4' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='5'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=157@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Mas de 10</property>
      <property name='MinSize'>@2,Width=85@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Mas de 10</property>
      <property name='TextSize'>@2,Width=49@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@3,X=468@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=156@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>0</property>
      <property name='MinSize'>@2,Width=85@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>0</property>
      <property name='TextSize'>@2,Width=49@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item4' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=156@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>1 a 5</property>
      <property name='MinSize'>@2,Width=85@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>1 a 5</property>
      <property name='TextSize'>@2,Width=49@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item3</property>
      <property name='Location'>@3,X=156@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn3</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item5' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=156@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>5 a 10</property>
      <property name='MinSize'>@2,Width=85@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>5 a 10</property>
      <property name='TextSize'>@2,Width=49@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item4</property>
      <property name='Location'>@3,X=312@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn4</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>"; 
                #endregion
                questionNumberPerson.RenderMethod = renderMethod;

                SmartCadDatabase.UpdateObject<QuestionData>(questionNumberPerson);
                #endregion

                #region statePerson
                QuestionData questionStatePerson = new QuestionData();
                questionStatePerson.Text = ResourceInitialDataLoader.GetString2("QuestionPersonsStateText");
                questionStatePerson.ShortText = STRSTATEPERSON;
                questionStatePerson.CustomCode = "PERSONSTATE";
                questionStatePerson.RequirementType = RequirementTypeEnum.Recommended;
                questionStatePerson.SetPossibleAnswers = new HashedSet();
                

                //Esta estructura se maneja de manera generica para todos los incidentes
                answer1 = new QuestionPossibleAnswerData();
                answer1.Question = questionStatePerson;
                answer1.Description = ResourceLoader.GetString2("State");
                answer1.ControlName = "TxtBxx1";
                questionStatePerson.SetPossibleAnswers.Add(answer1);

                //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
                SmartCadDatabase.SaveObject<QuestionData>(questionStatePerson);

                //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
                #region Render State Person
                renderMethod = @"
<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Estado' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.19885, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Estado</property>
      <property name='MinSize'>@2,Width=93@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Estado</property>
      <property name='TextSize'>@2,Width=57@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>"; 
                #endregion
                questionStatePerson.RenderMethod = renderMethod;

                SmartCadDatabase.UpdateObject<QuestionData>(questionStatePerson);
                #endregion
            }
        }


        private static void CreateGenericQuestionFire()
        {

            //Crea los tipo de preguntas y procedimientos genericos para el tipo de incidente Fire
            #region howFire
            QuestionData question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("howdidstart");
            question.ShortText = ResourceInitialDataLoader.GetString2("HowQuestion");
            question.CustomCode = "HOW";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();
            

            //Esta estructura se maneja de manera generica para todos los incidentes
            QuestionPossibleAnswerData answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("HowQuestion");
            answer1.ControlName = "TxtBxx1";

            //IncidentTypeProcedure2Data * = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);

            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region Render HOW
            string renderMethod = @"
<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Como comenzo' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3449.33332, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Como comenzo</property>
      <property name='MinSize'>@3,Width=108@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Como comenzo</property>
      <property name='TextSize'>@2,Width=72@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>"; 
            #endregion
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region propagaFire
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("QuestionDangerOfPropText");
            question.ShortText = ResourceInitialDataLoader.GetString2("QuestionDangerOfProp");
            question.CustomCode = "PROPAGA";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();
            

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Yes");
            answer1.ControlName = "RdBttn3";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);

            QuestionPossibleAnswerData answer2 = new QuestionPossibleAnswerData();
            answer2.Question = question;
            answer2.Description = ResourceLoader.GetString2("No");
            answer2.ControlName = "RdBttn2";
            //IncidentTypeProcedure2Data * = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer2);

            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region Render PROPAGA
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='No' itemName='item2' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn2' />
  <Control itemText='Si' itemName='item3' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn3' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='3'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=313@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>No</property>
      <property name='MinSize'>@2,Width=49@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>No</property>
      <property name='TextSize'>@2,Width=13@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@3,X=312@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=312@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Si</property>
      <property name='MinSize'>@2,Width=49@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Si</property>
      <property name='TextSize'>@2,Width=13@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item3</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn3</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>"; 
            #endregion
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region ElementsFire
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("ElementsQuestionText");
            question.ShortText = ResourceInitialDataLoader.GetString2("ElementsQuestion");
            question.CustomCode = "ELEMENTS";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();
            

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Elements");
            answer1.ControlName = "TxtBxx1";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);

            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region RENDER ELEMENTS
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Elementos' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.19885, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Elementos</property>
      <property name='MinSize'>@2,Width=85@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Elementos</property>
      <property name='TextSize'>@2,Width=49@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>"; 
            #endregion
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);

            #endregion

            #region humoFire
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("ColorQuestionText");
            question.ShortText = ResourceInitialDataLoader.GetString2("ColorQuestion");
            question.CustomCode = "SMOKE";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();
            

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceLoader.GetString2("$Message.Color");
            answer1.ControlName = "TxtBxx1";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);

            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region RENDER SMOKE
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Humo' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.19885, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Humo</property>
      <property name='MinSize'>@2,Width=85@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Humo</property>
      <property name='TextSize'>@2,Width=49@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>"; 
            #endregion
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region MaterialFire
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("MaterialQuestionText");
            question.ShortText = ResourceInitialDataLoader.GetString2("MaterialQuestion");
            question.CustomCode = "MATERIAL";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();
           

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceLoader.GetString2("Material");
            answer1.ControlName = "TxtBxx1";

            question.SetPossibleAnswers.Add(answer1);
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            
            //este dise�o es igual al anterior por eso no se modifica ni se agrega nada.
            #region REnder Material
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Material' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.19885, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Material</property>
      <property name='MinSize'>@2,Width=85@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Material</property>
      <property name='TextSize'>@2,Width=49@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>"; 
            #endregion
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region VolatilFire
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("VolatileQuestionText");
            question.ShortText = ResourceInitialDataLoader.GetString2("VolatileQuestion");
            question.CustomCode = "VOLATIL";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();
            

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceLoader.GetString2("Yes");
            answer1.ControlName = "RdBttn3";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);

            answer2 = new QuestionPossibleAnswerData();
            answer2.Question = question;
            answer2.Description = ResourceLoader.GetString2("No");
            answer2.ControlName = "RdBttn2";
            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer2);

            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region Render VOLATIL
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='No' itemName='item2' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn2' />
  <Control itemText='Si' itemName='item3' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn3' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='3'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=313@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>No</property>
      <property name='MinSize'>@2,Width=49@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>No</property>
      <property name='TextSize'>@2,Width=13@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@3,X=312@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=312@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Si</property>
      <property name='MinSize'>@2,Width=49@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Si</property>
      <property name='TextSize'>@2,Width=13@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item3</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn3</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>";
            #endregion
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region inmueblesFire
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("FinesQuestionText");
            question.ShortText = ResourceInitialDataLoader.GetString2("FinesQuestion");
            question.CustomCode = "INMUEBLES";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();
            

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceLoader.GetString2("Yes");
            answer1.ControlName = "RdBttn1";

            question.SetPossibleAnswers.Add(answer1);

            answer2 = new QuestionPossibleAnswerData();
            answer2.Question = question;
            answer2.Description = ResourceLoader.GetString2("No");
            answer2.ControlName = "RdBttn2";
            question.SetPossibleAnswers.Add(answer2);

            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region RENDER IMNBN
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Si' itemName='item1' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn1' />
  <Control itemText='No' itemName='item2' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn2' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='3'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=312@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Si</property>
      <property name='MinSize'>@2,Width=49@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Si</property>
      <property name='TextSize'>@2,Width=13@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=313@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>No</property>
      <property name='MinSize'>@2,Width=49@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>No</property>
      <property name='TextSize'>@2,Width=13@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@3,X=312@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>"; 
            #endregion
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region sospechoso
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("SuspectsDescriptionQuestion");
            question.ShortText = ResourceInitialDataLoader.GetString2("SuspectQuestion");
            question.CustomCode = "SOSPECHOSOS";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();
           

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceLoader.GetString2("Suspects");
            answer1.ControlName = "TxtBxx1";
            question.SetPossibleAnswers.Add(answer1);

            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region RENDER SOSPECHOSOS
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Sospechoso' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.19885, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Sospechoso</property>
      <property name='MinSize'>@2,Width=93@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Sospechoso</property>
      <property name='TextSize'>@2,Width=57@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>"; 
            #endregion
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region personCatch
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("TrappedQuestionText");
            question.ShortText = ResourceInitialDataLoader.GetString2("TrappedQuestion");
            question.CustomCode = "CATCHPERSON";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();
            

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceLoader.GetString2("Yes");
            answer1.ControlName = "RdBttn1";
            question.SetPossibleAnswers.Add(answer1);

            answer2 = new QuestionPossibleAnswerData();
            answer2.Question = question;
            answer2.Description = ResourceLoader.GetString2("No");
            answer2.ControlName = "RdBttn2";
            question.SetPossibleAnswers.Add(answer2);

            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region RENDER CATHPERSON
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Si' itemName='item1' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn1' />
  <Control itemText='No' itemName='item2' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn2' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='3'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=312@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Si</property>
      <property name='MinSize'>@2,Width=49@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Si</property>
      <property name='TextSize'>@2,Width=13@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=313@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>No</property>
      <property name='MinSize'>@2,Width=49@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>No</property>
      <property name='TextSize'>@2,Width=13@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@3,X=312@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>"; 
            #endregion
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region extintor
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("ExtinguisherQuestionText");
            question.ShortText = ResourceInitialDataLoader.GetString2("ExtinguisherQuestion");
            question.CustomCode = "EXTINTOR";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();
           

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceLoader.GetString2("Yes");
            answer1.ControlName = "RdBttn1";

            question.SetPossibleAnswers.Add(answer1);

            answer2 = new QuestionPossibleAnswerData();
            answer2.Question = question;
            answer2.Description = ResourceLoader.GetString2("No");
            answer2.ControlName = "RdBttn2";
            question.SetPossibleAnswers.Add(answer2);

            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region RENDER EXTINTOR
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Si' itemName='item1' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn1' />
  <Control itemText='No' itemName='item2' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn2' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='3'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=312@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Si</property>
      <property name='MinSize'>@2,Width=49@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Si</property>
      <property name='TextSize'>@2,Width=13@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=313@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>No</property>
      <property name='MinSize'>@2,Width=49@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>No</property>
      <property name='TextSize'>@2,Width=13@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@3,X=312@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>"; 
            #endregion
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region owner
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("OwnerQuestionText");
            question.ShortText = ResourceInitialDataLoader.GetString2("OwnerQuestion");
            question.CustomCode = "PROPETARIO";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();
            

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceLoader.GetString2("Si");
            answer1.ControlName = "RdBttn1";
            question.SetPossibleAnswers.Add(answer1);

            answer2 = new QuestionPossibleAnswerData();
            answer2.Question = question;
            answer2.Description = ResourceLoader.GetString2("No");
            answer2.ControlName = "RdBttn2";
            question.SetPossibleAnswers.Add(answer2);

            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region RENDER OWNER
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Si' itemName='item1' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn1' />
  <Control itemText='No' itemName='item2' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn2' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='3'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=312@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Si</property>
      <property name='MinSize'>@2,Width=49@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Si</property>
      <property name='TextSize'>@2,Width=13@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=313@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>No</property>
      <property name='MinSize'>@2,Width=49@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>No</property>
      <property name='TextSize'>@2,Width=13@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@3,X=312@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>"; 
            #endregion
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

        }

        private static void CreateFireIncidentType()
        {
            //Este metodo crea la carga inicial para todos los tipos de incidentes de incencio
            //Para agregar un tipo de incidente debo realizar los siguientes pasos
            //1.- Crear el incidente.
            //2.- Asociarle los tipos de departamentos que dan soporte a ese incidente
            //3.- Indicar los tipos de procedimientos asociados al incidente
            //4.- Indicar los tipos de preguntas asociadas al incidente con la siguiente estructura
            //4.1.-     Indicar un procedimiento para cada pregunta
            //4.2.-     Indicar las posible respuesta asociadas a la pregunta
            //4.3.-     Indicar la forma en que se renderiza la pregunta en el frontclient indicando los id de las respuestas asociadas


            CreateGenericQuestionFire();

            #region IncendioResidencial
            //Paso 1
            IncidentTypeData incidentType = new IncidentTypeData();
            incidentType.CustomCode = "INR";
            incidentType.Name = "FireResidence";
            incidentType.FriendlyName = ResourceInitialDataLoader.GetString2("ResidenceFire");
            incidentType.NoEmergency = false;

            //Paso 2
            //Bomberos
            DepartmentTypeData dpFire = new DepartmentTypeData(DPFIRE);
            dpFire = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFire);

            //Bomberos (Atenci�n pre-hospitalaria)
            DepartmentTypeData dpFirePre = new DepartmentTypeData(DPFIREPRE);
            dpFirePre = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFirePre);

            incidentType.SetIncidentTypeDepartmentTypes = new HashedSet();

            IncidentTypeDepartmentTypeData itdFire = new IncidentTypeDepartmentTypeData();
            itdFire.DepartmentType = dpFire;
            itdFire.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFire);

            IncidentTypeDepartmentTypeData itdFirePre = new IncidentTypeDepartmentTypeData();
            itdFirePre.DepartmentType = dpFirePre;
            itdFirePre.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFirePre);
            incidentType.Procedure = @"<b>" + ResourceInitialDataLoader.GetString2("ProceduresTo") + incidentType.FriendlyName + @".</b> <ul> <li>" + ResourceInitialDataLoader.GetString2("Step1") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step2") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step3") + @"</li> </ul> ";

            //Paso 4

            //Indico las preguntas genericas para el incidente
            incidentType.SetIncidentTypeQuestions = new HashedSet();

            QuestionData questionWhere = new QuestionData();
            questionWhere.ShortText = STRQUESTIONWHERE;           
            questionWhere = SmartCadDatabase.SearchObject<QuestionData>(questionWhere);


            IncidentTypeQuestionData itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionWhere;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            QuestionData questionNumber = new QuestionData();
            questionNumber.ShortText = STRQUESTIONNUMBERPERSON;         
            questionNumber = SmartCadDatabase.SearchObject<QuestionData>(questionNumber);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionNumber;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            QuestionData questionState = new QuestionData();
            questionState.ShortText = STRSTATEPERSON;           
            questionState = SmartCadDatabase.SearchObject<QuestionData>(questionState);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionState;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            //Preguntas especificas
            QuestionData questionType = new QuestionData();
            questionType.Text = ResourceInitialDataLoader.GetString2("ConstructionOnFireType");
            questionType.ShortText = ResourceInitialDataLoader.GetString2("TypeQuestion");
            questionType.CustomCode = incidentType.CustomCode + "TYPE";
            questionType.RequirementType = RequirementTypeEnum.Recommended;
            questionType.SetPossibleAnswers = new HashedSet();           

            //Esta estructura se maneja de manera generica para todos los incidentes
            QuestionPossibleAnswerData answer1 = new QuestionPossibleAnswerData();
            answer1.Question = questionType;
            answer1.Description = ResourceInitialDataLoader.GetString2("House");
            answer1.ControlName = "Chckdt1";

            questionType.SetPossibleAnswers.Add(answer1);

            QuestionPossibleAnswerData answer2 = new QuestionPossibleAnswerData();
            answer2.Question = questionType;
            answer2.Description = ResourceInitialDataLoader.GetString2("BuildSlashApt");
            answer2.ControlName = "Chckdt2";

            questionType.SetPossibleAnswers.Add(answer2);

            SmartCadDatabase.SaveObject<QuestionData>(questionType);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region Render
            string renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Casa' itemName='item1' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt1' />
  <Control itemText='Edificio/apartamento' itemName='item2' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt2' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='3'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=312@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Casa</property>
      <property name='MinSize'>@2,Width=58@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Casa</property>
      <property name='TextSize'>@2,Width=22@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=313@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Edificio/apartamento</property>
      <property name='MinSize'>@2,Width=58@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Edificio/apartamento</property>
      <property name='TextSize'>@2,Width=22@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@3,X=312@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>"; 
            #endregion

            questionType.RenderMethod = renderMethod;
            SmartCadDatabase.UpdateObject<QuestionData>(questionType);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionType;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            //Asocio las preguntas genericas para cada incendio
            QuestionData question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("HowQuestion");            
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("QuestionDangerOfProp");           
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);


            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("ElementsQuestion");          
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("ColorQuestion");           
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("MaterialQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("VolatileQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            SmartCadDatabase.SaveObject<IncidentTypeData>(incidentType);
            #endregion

            #region incendioIndustrial
            //Paso 1
            incidentType = new IncidentTypeData();
            incidentType.CustomCode = "ICI";
            incidentType.Name = "FireIndustrial";
            incidentType.FriendlyName = ResourceInitialDataLoader.GetString2("IndustrialFire");
            incidentType.NoEmergency = false;

            //Paso 2
            //Bomberos
            dpFire = new DepartmentTypeData(DPFIRE);
            dpFire = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFire);

            //Bomberos (Atenci�n pre-hospitalaria)
            dpFirePre = new DepartmentTypeData(DPFIREPRE);
            dpFirePre = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFirePre);

            incidentType.SetIncidentTypeDepartmentTypes = new HashedSet();

            itdFire = new IncidentTypeDepartmentTypeData();
            itdFire.DepartmentType = dpFire;
            itdFire.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFire);

            itdFirePre = new IncidentTypeDepartmentTypeData();
            itdFirePre.DepartmentType = dpFirePre;
            itdFirePre.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFirePre);

            //Paso 3 Esto va a cambiar 
            incidentType.Procedure = @"<b>" + ResourceInitialDataLoader.GetString2("ProceduresTo") + incidentType.FriendlyName + @".</b> <ul> <li>" + ResourceInitialDataLoader.GetString2("Step1") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step2") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step3") + @"</li> </ul> ";



            //Paso 4

            //Indico las preguntas genericas para el incidente
            incidentType.SetIncidentTypeQuestions = new HashedSet();

            questionWhere = new QuestionData();
            questionWhere.ShortText = STRQUESTIONWHERE;            
            questionWhere = SmartCadDatabase.SearchObject<QuestionData>(questionWhere);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionWhere;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            questionNumber = new QuestionData();
            questionNumber.ShortText = STRQUESTIONNUMBERPERSON;           
            questionNumber = SmartCadDatabase.SearchObject<QuestionData>(questionNumber);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionNumber;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            questionState = new QuestionData();
            questionState.ShortText = STRSTATEPERSON;           
            questionState = SmartCadDatabase.SearchObject<QuestionData>(questionState);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionState;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            //Preguntas especificas
            questionType = new QuestionData();
            questionType.Text = ResourceInitialDataLoader.GetString2("IndustryOnFire");
            questionType.ShortText = ResourceInitialDataLoader.GetString2("FireTypeQuestion");
            questionType.CustomCode = incidentType.CustomCode + "TYPE";
            questionType.RequirementType = RequirementTypeEnum.Recommended;
            questionType.SetPossibleAnswers = new HashedSet();            

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = questionType;
            answer1.Description = ResourceInitialDataLoader.GetString2("Wineries");
            answer1.ControlName = "Chckdt1";

            questionType.SetPossibleAnswers.Add(answer1);

            answer2 = new QuestionPossibleAnswerData();
            answer2.Question = questionType;
            answer2.Description = ResourceInitialDataLoader.GetString2("Offices");
            answer2.ControlName = "Chckdt2";

            questionType.SetPossibleAnswers.Add(answer2);

            SmartCadDatabase.SaveObject<QuestionData>(questionType);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region Render
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Bodegas' itemName='item1' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt1' />
  <Control itemText='Oficinas' itemName='item2' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt2' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='3'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=312@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Bodegas</property>
      <property name='MinSize'>@2,Width=58@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Bodegas</property>
      <property name='TextSize'>@2,Width=22@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=313@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Oficinas</property>
      <property name='MinSize'>@2,Width=58@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Oficinas</property>
      <property name='TextSize'>@2,Width=22@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@3,X=312@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>"; 
            #endregion

            questionType.RenderMethod = renderMethod;
            SmartCadDatabase.UpdateObject<QuestionData>(questionType);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionType;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            //Asocio las preguntas genericas para cada incendio
            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("HowQuestion");           
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("QuestionDangerOfProp");           
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);


            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("ElementsQuestion");           
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("ColorQuestion");            
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("MaterialQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("VolatileQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            SmartCadDatabase.SaveObject<IncidentTypeData>(incidentType);
            #endregion

            #region incendioForestal
            //Paso 1
            incidentType = new IncidentTypeData();
            incidentType.CustomCode = "INF";
            incidentType.Name = "FireForest";
            incidentType.FriendlyName = ResourceInitialDataLoader.GetString2("ForestFire");
            incidentType.NoEmergency = false;

            //Paso 2
            //Bomberos
            dpFire = new DepartmentTypeData(DPFIRE);
            dpFire = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFire);

            //Bomberos (Atenci�n pre-hospitalaria)
            dpFirePre = new DepartmentTypeData(DPFIREPRE);
            dpFirePre = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFirePre);

            incidentType.SetIncidentTypeDepartmentTypes = new HashedSet();

            itdFire = new IncidentTypeDepartmentTypeData();
            itdFire.DepartmentType = dpFire;
            itdFire.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFire);

            itdFirePre = new IncidentTypeDepartmentTypeData();
            itdFirePre.DepartmentType = dpFirePre;
            itdFirePre.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFirePre);

            //Paso 3 Esto va a cambiar 

            incidentType.Procedure =
            @"<b>" + ResourceInitialDataLoader.GetString2("ProceduresTo") + incidentType.FriendlyName + @".</b> <ul> <li>" + ResourceInitialDataLoader.GetString2("Step1") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step2") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step3") + @"</li> </ul> ";



            //Paso 4

            //Indico las preguntas genericas para el incidente
            incidentType.SetIncidentTypeQuestions = new HashedSet();

            questionWhere = new QuestionData();
            questionWhere.ShortText = STRQUESTIONWHERE;           
            questionWhere = SmartCadDatabase.SearchObject<QuestionData>(questionWhere);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionWhere;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            questionNumber = new QuestionData();
            questionNumber.ShortText = STRQUESTIONNUMBERPERSON;            
            questionNumber = SmartCadDatabase.SearchObject<QuestionData>(questionNumber);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionNumber;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            questionState = new QuestionData();
            questionState.ShortText = STRSTATEPERSON;            
            questionState = SmartCadDatabase.SearchObject<QuestionData>(questionState);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionState;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            //Preguntas especificas
            questionType = new QuestionData();
            questionType.Text = ResourceInitialDataLoader.GetString2("FireTypeOf");
            questionType.ShortText = ResourceInitialDataLoader.GetString2("WhatFire");
            questionType.CustomCode = incidentType.CustomCode + "TYPE";
            questionType.RequirementType = RequirementTypeEnum.Recommended;
            questionType.SetPossibleAnswers = new HashedSet();           

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = questionType;
            answer1.Description = ResourceInitialDataLoader.GetString2("HighVegetation");
            answer1.ControlName = "Chckdt5";

            questionType.SetPossibleAnswers.Add(answer1);

            answer2 = new QuestionPossibleAnswerData();
            answer2.Question = questionType;
            answer2.Description = ResourceInitialDataLoader.GetString2("Grassland");
            answer2.ControlName = "Chckdt6";

            questionType.SetPossibleAnswers.Add(answer2);

            QuestionPossibleAnswerData answer3 = new QuestionPossibleAnswerData();
            answer3.Question = questionType;
            answer3.Description = ResourceInitialDataLoader.GetString2("Fires");
            answer3.ControlName = "Chckdt4";

            questionType.SetPossibleAnswers.Add(answer3);

            SmartCadDatabase.SaveObject<QuestionData>(questionType);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region Render
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Fogatas' itemName='item2' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt4' />
  <Control itemText='Vegetacion alta' itemName='item3' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt5' />
  <Control itemText='Pastizales' itemName='item4' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt6' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='4'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=195@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Seleccion multiple</property>
      <property name='MinSize'>@3,Width=110@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Fogatas</property>
      <property name='TextSize'>@2,Width=74@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@3,X=430@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt4</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=311@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Vegetacion alta</property>
      <property name='MinSize'>@3,Width=110@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Vegetacion alta</property>
      <property name='TextSize'>@2,Width=74@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item3</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt5</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item4' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=119@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Seleccion multiple</property>
      <property name='MinSize'>@3,Width=110@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Pastizales</property>
      <property name='TextSize'>@2,Width=74@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item4</property>
      <property name='Location'>@3,X=311@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt6</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>"; 
            #endregion

            questionType.RenderMethod = renderMethod;
            SmartCadDatabase.UpdateObject<QuestionData>(questionType);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionType;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            //Asocio las preguntas genericas para cada incendio
            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("HowQuestion");           
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("QuestionDangerOfProp");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("FinesQuestion");          
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("SuspectQuestion");            
            question = SmartCadDatabase.SearchObject<QuestionData>(question);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            SmartCadDatabase.SaveObject<IncidentTypeData>(incidentType);
            #endregion

            #region incendioVehiculos
            //Paso 1
            incidentType = new IncidentTypeData();
            incidentType.CustomCode = "INV";
            incidentType.Name = "FireVehicle";
            incidentType.FriendlyName = ResourceInitialDataLoader.GetString2("VehicleFire");
            incidentType.NoEmergency = false;

            //Paso 2
            //Bomberos
            dpFire = new DepartmentTypeData(DPFIRE);
            dpFire = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFire);

            //Bomberos (Atenci�n pre-hospitalaria)
            dpFirePre = new DepartmentTypeData(DPFIREPRE);
            dpFirePre = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFirePre);

            incidentType.SetIncidentTypeDepartmentTypes = new HashedSet();

            itdFire = new IncidentTypeDepartmentTypeData();
            itdFire.DepartmentType = dpFire;
            itdFire.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFire);

            itdFirePre = new IncidentTypeDepartmentTypeData();
            itdFirePre.DepartmentType = dpFirePre;
            itdFirePre.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFirePre);

            //Paso 3 Esto va a cambiar 

            incidentType.Procedure =
            @"<b>" + ResourceInitialDataLoader.GetString2("ProceduresTo") + incidentType.FriendlyName + @".</b> <ul> <li>" + ResourceInitialDataLoader.GetString2("Step1") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step2") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step3") + @"</li> </ul> ";



            //Paso 4

            //Indico las preguntas genericas para el incidente
            incidentType.SetIncidentTypeQuestions = new HashedSet();

            questionWhere = new QuestionData();
            questionWhere.ShortText = STRQUESTIONWHERE;           
            questionWhere = SmartCadDatabase.SearchObject<QuestionData>(questionWhere);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionWhere;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            questionNumber = new QuestionData();
            questionNumber.ShortText = STRQUESTIONNUMBERPERSON;            
            questionNumber = SmartCadDatabase.SearchObject<QuestionData>(questionNumber);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionNumber;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            questionState = new QuestionData();
            questionState.ShortText = STRSTATEPERSON;            
            questionState = SmartCadDatabase.SearchObject<QuestionData>(questionState);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionState;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            //Preguntas especificas
            questionType = new QuestionData();
            questionType.Text = ResourceInitialDataLoader.GetString2("WhatTypeOfFireQuestion");
            questionType.ShortText = ResourceInitialDataLoader.GetString2("WichFireQuestion");
            questionType.CustomCode = incidentType.CustomCode + "TYPE";
            questionType.RequirementType = RequirementTypeEnum.Recommended;
            questionType.SetPossibleAnswers = new HashedSet();            

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = questionType;
            answer1.Description = ResourceInitialDataLoader.GetString2("Particular");
            answer1.ControlName = "Chckdt6";
            //proc*.Items.Add(item*);

            questionType.SetPossibleAnswers.Add(answer1);

            answer2 = new QuestionPossibleAnswerData();
            answer2.Question = questionType;
            answer2.Description = ResourceInitialDataLoader.GetString2("Public");
            answer2.ControlName = "Chckdt5";

            questionType.SetPossibleAnswers.Add(answer2);

            answer3 = new QuestionPossibleAnswerData();
            answer3.Question = questionType;
            answer3.Description = ResourceInitialDataLoader.GetString2("Colective");
            answer3.ControlName = "Chckdt8";

            questionType.SetPossibleAnswers.Add(answer3);

            QuestionPossibleAnswerData answer4 = new QuestionPossibleAnswerData();
            answer4.Question = questionType;
            answer4.Description = ResourceInitialDataLoader.GetString2("Tankcar");
            answer4.ControlName = "Chckdt7";
            questionType.SetPossibleAnswers.Add(answer4);

            QuestionPossibleAnswerData answer5 = new QuestionPossibleAnswerData();
            answer5.Question = questionType;
            answer5.Description = ResourceInitialDataLoader.GetString2("LoadTruck");
            answer5.ControlName = "Chckdt4";

            questionType.SetPossibleAnswers.Add(answer5);

            SmartCadDatabase.SaveObject<QuestionData>(questionType);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region Render
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Cami�n de carga' itemName='item1' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt4' />
  <Control itemText='Particular' itemName='item3' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt6' />
  <Control itemText='Carrotanque' itemName='item4' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt7' />
  <Control itemText='Colectivo' itemName='item5' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt8' />
  <Control itemText='Publico' itemName='item2' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt5' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='6'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=119@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Cami�n de carga</property>
      <property name='MinSize'>@3,Width=116@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Cami�n de carga</property>
      <property name='TextSize'>@2,Width=80@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@3,X=506@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt4</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=149@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Particular</property>
      <property name='MinSize'>@3,Width=116@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Particular</property>
      <property name='TextSize'>@2,Width=80@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item3</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt6</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item4' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=119@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Carrotanque</property>
      <property name='MinSize'>@3,Width=116@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Carrotanque</property>
      <property name='TextSize'>@2,Width=80@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item4</property>
      <property name='Location'>@3,X=387@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt7</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item5' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=119@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Colectivo</property>
      <property name='MinSize'>@3,Width=116@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Colectivo</property>
      <property name='TextSize'>@2,Width=80@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item5</property>
      <property name='Location'>@3,X=268@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt8</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item6' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=119@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Publico</property>
      <property name='MinSize'>@3,Width=116@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Publico</property>
      <property name='TextSize'>@2,Width=80@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@3,X=149@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt5</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>"; 
            #endregion

            questionType.RenderMethod = renderMethod;
            SmartCadDatabase.UpdateObject<QuestionData>(questionType);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionType;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            //Asocio las preguntas genericas para cada incendio
            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("HowQuestion");           
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("TrappedQuestion");           
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("ExtinguisherQuestion");            
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("OwnerQuestion");            
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("ElementsQuestion");           
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);


            SmartCadDatabase.SaveObject<IncidentTypeData>(incidentType);
            #endregion

            #region incendiobasura
            //Paso 1
            incidentType = new IncidentTypeData();
            incidentType.CustomCode = "INT";
            incidentType.Name = "FireTrash";
            incidentType.FriendlyName = ResourceInitialDataLoader.GetString2("GarbageFire");
            incidentType.NoEmergency = false;

            //Paso 2
            //Bomberos
            dpFire = new DepartmentTypeData(DPFIRE);
            dpFire = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFire);

            //Bomberos (Atenci�n pre-hospitalaria)
            dpFirePre = new DepartmentTypeData(DPFIREPRE);
            dpFirePre = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFirePre);

            incidentType.SetIncidentTypeDepartmentTypes = new HashedSet();

            itdFire = new IncidentTypeDepartmentTypeData();
            itdFire.DepartmentType = dpFire;
            itdFire.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFire);

            itdFirePre = new IncidentTypeDepartmentTypeData();
            itdFirePre.DepartmentType = dpFirePre;
            itdFirePre.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFirePre);

            //Paso 3 Esto va a cambiar 

            incidentType.Procedure =
            @"<b>" + ResourceInitialDataLoader.GetString2("ProceduresTo") + incidentType.FriendlyName + @".</b> <ul> <li>" + ResourceInitialDataLoader.GetString2("Step1") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step2") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step3") + @"</li> </ul> ";



            //Paso 4

            //Indico las preguntas genericas para el incidente
            incidentType.SetIncidentTypeQuestions = new HashedSet();

            questionWhere = new QuestionData();
            questionWhere.ShortText = STRQUESTIONWHERE;           
            questionWhere = SmartCadDatabase.SearchObject<QuestionData>(questionWhere);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionWhere;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            questionNumber = new QuestionData();
            questionNumber.ShortText = STRQUESTIONNUMBERPERSON;           
            questionNumber = SmartCadDatabase.SearchObject<QuestionData>(questionNumber);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionNumber;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            questionState = new QuestionData();
            questionState.ShortText = STRSTATEPERSON;           
            questionState = SmartCadDatabase.SearchObject<QuestionData>(questionState);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionState;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);


            //Asocio las preguntas genericas para cada incendio
            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("HowQuestion");           
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("QuestionDangerOfProp");           
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("FinesQuestion");            
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("SuspectQuestion");            
            question = SmartCadDatabase.SearchObject<QuestionData>(question);
            
            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            SmartCadDatabase.SaveObject<IncidentTypeData>(incidentType);

            #endregion
        }

        private static void CreateGenericQuestionAccident()
        {
            //Crea los tipo de preguntas y procedimientos genericos para el tipo de incidente Fire
            #region fractures
            QuestionData question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("FractureQuestionText");
            question.ShortText = ResourceInitialDataLoader.GetString2("FractureQuestion");
            question.CustomCode = "FRACTURES";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();            

            //Esta estructura se maneja de manera generica para todos los incidentes
            QuestionPossibleAnswerData answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Fracture");
            answer1.ControlName = "TxtBxx1";


            question.SetPossibleAnswers.Add(answer1);

            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region Render Fractura
            string renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Fractura' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.19885, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Fractura</property>
      <property name='MinSize'>@2,Width=93@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Fractura</property>
      <property name='TextSize'>@2,Width=57@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>"; 
            #endregion
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region contusion
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("ContusionQuestionText");
            question.ShortText = ResourceInitialDataLoader.GetString2("ContusionQuestion");
            question.CustomCode = "CONTUSION";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();           

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Contusion");
            answer1.ControlName = "TxtBxx1";

            question.SetPossibleAnswers.Add(answer1);

            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region Render Contunsion
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Contunsion' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.19885, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Contunsion</property>
      <property name='MinSize'>@2,Width=93@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Contunsion</property>
      <property name='TextSize'>@2,Width=57@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>"; 
            #endregion
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region hemorragia
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("BleedingQuestionText");
            question.ShortText = ResourceInitialDataLoader.GetString2("Bleeding");
            question.CustomCode = "BLEEDING";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();            

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceLoader.GetString2("Yes");
            answer1.ControlName = "RdBttn1";

            question.SetPossibleAnswers.Add(answer1);

            QuestionPossibleAnswerData answer2 = new QuestionPossibleAnswerData();
            answer2.Question = question;
            answer2.Description = ResourceLoader.GetString2("No");
            answer2.ControlName = "RdBttn2";

            question.SetPossibleAnswers.Add(answer2);

            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region REnder Hemorragia
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Si' itemName='item1' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn1' />
  <Control itemText='No' itemName='item2' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn2' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='3'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=312@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Si</property>
      <property name='MinSize'>@2,Width=49@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Si</property>
      <property name='TextSize'>@2,Width=13@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=313@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>No</property>
      <property name='MinSize'>@2,Width=49@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>No</property>
      <property name='TextSize'>@2,Width=13@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@3,X=312@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>"; 
            #endregion
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region sintomas
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("SintomesQuestionText");
            question.ShortText = ResourceInitialDataLoader.GetString2("Sintomes");
            question.CustomCode = "SINTOMAS";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();            

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Vomite");
            answer1.ControlName  ="Chckdt2";

            question.SetPossibleAnswers.Add(answer1);

            answer2 = new QuestionPossibleAnswerData();
            answer2.Question = question;
            answer2.Description = ResourceInitialDataLoader.GetString2("Sweat");
            answer2.ControlName  = "Chckdt1";

            question.SetPossibleAnswers.Add(answer2);

            QuestionPossibleAnswerData answer3 = new QuestionPossibleAnswerData();
            answer3.Question = question;
            answer3.Description = ResourceInitialDataLoader.GetString2("LossOfConsciousness");
            answer3.ControlName = "Chckdt3";

            question.SetPossibleAnswers.Add(answer3);

            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region Render Simtomas
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Vomito' itemName='item2' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt2' />
  <Control itemText='Perdida de conciencia' itemName='item3' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt3' />
  <Control itemText='Sudoracion' itemName='item1' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='4'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=155@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Vomito</property>
      <property name='MinSize'>@3,Width=139@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Vomito</property>
      <property name='TextSize'>@3,Width=103@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=314@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Perdida de conciencia</property>
      <property name='MinSize'>@3,Width=139@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Perdida de conciencia</property>
      <property name='TextSize'>@3,Width=103@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item3</property>
      <property name='Location'>@3,X=311@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt3</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item4' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=156@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Sudoracion</property>
      <property name='MinSize'>@3,Width=139@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Sudoracion</property>
      <property name='TextSize'>@3,Width=103@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@3,X=155@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>"; 
            #endregion
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region toxico
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("ToxicQuestionText");
            question.ShortText = ResourceInitialDataLoader.GetString2("ToxicQuestion");
            question.CustomCode = "TOXIC";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();           

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Type");
            answer1.ControlName = "TxtBxx1";

            question.SetPossibleAnswers.Add(answer1);

            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region Render Toxico
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Tipo' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.19885, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Tipo</property>
      <property name='MinSize'>@2,Width=93@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Tipo</property>
      <property name='TextSize'>@2,Width=57@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>"; 
            #endregion
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region suicida
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("SuicideQuestionText");
            question.ShortText = ResourceInitialDataLoader.GetString2("SuicideQuestion");
            question.CustomCode = "SUICIDE";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();           

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Situation");
            answer1.ControlName = "TxtBxx1";

            question.SetPossibleAnswers.Add(answer1);

            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region Suicida
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Situacion' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.19885, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Situacion</property>
      <property name='MinSize'>@2,Width=93@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Situacion</property>
      <property name='TextSize'>@2,Width=57@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>"; 
            #endregion
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion
        }

        private static void CreateAccidentType()
        {
            //Este metodo crea la carga inicial para todos los tipos de incidentes de accidente
            //Para agregar un tipo de incidente debo realizar los siguientes pasos
            //1.- Crear el incidente.
            //2.- Asociarle los tipos de departamentos que dan soporte a ese incidente
            //3.- Indicar los tipos de procedimientos asociados al incidente
            //4.- Indicar los tipos de preguntas asociadas al incidente con la siguiente estructura
            //4.1.-     Indicar un procedimiento para cada pregunta
            //4.2.-     Indicar las posible respuesta asociadas a la pregunta
            //4.3.-     Indicar la forma en que se renderiza la pregunta en el frontclient indicando los id de las respuestas asociadas
            CreateGenericQuestionAccident();

            #region AccidenteResidencial
            //Paso 1
            IncidentTypeData incidentType = new IncidentTypeData();
            incidentType.CustomCode = "ACR";
            incidentType.Name = "AccidentResidence";
            incidentType.FriendlyName = ResourceInitialDataLoader.GetString2("ResidenceAccident");
            incidentType.NoEmergency = false;

            //Paso 2
            //Bomberos
            DepartmentTypeData dpFire = new DepartmentTypeData(DPFIRERESCUE);
            dpFire = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFire);

            //Bomberos (Atenci�n pre-hospitalaria)
            DepartmentTypeData dpFirePre = new DepartmentTypeData(DPFIREPRE);
            dpFirePre = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFirePre);

            //Transito
            DepartmentTypeData dpTransito = new DepartmentTypeData(DPTRANS);
            dpTransito = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpTransito);

            incidentType.SetIncidentTypeDepartmentTypes = new HashedSet();

            IncidentTypeDepartmentTypeData itdFire = new IncidentTypeDepartmentTypeData();
            itdFire.DepartmentType = dpFire;
            itdFire.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFire);

            IncidentTypeDepartmentTypeData itdFirePre = new IncidentTypeDepartmentTypeData();
            itdFirePre.DepartmentType = dpFirePre;
            itdFirePre.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFirePre);

            IncidentTypeDepartmentTypeData itdTransito = new IncidentTypeDepartmentTypeData();
            itdTransito.DepartmentType = dpTransito;
            itdTransito.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdTransito);

            incidentType.Procedure = @"<b>" + ResourceInitialDataLoader.GetString2("KeepItOnline") + incidentType.FriendlyName + ".</b>";



            //Paso 4

            //Indico las preguntas genericas para el incidente
            incidentType.SetIncidentTypeQuestions = new HashedSet();

            QuestionData questionWhere = new QuestionData();
            questionWhere.ShortText = STRQUESTIONWHERE;            
            questionWhere = SmartCadDatabase.SearchObject<QuestionData>(questionWhere);


            IncidentTypeQuestionData itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionWhere;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            QuestionData questionNumber = new QuestionData();
            questionNumber.ShortText = STRQUESTIONNUMBERPERSON;           
            questionNumber = SmartCadDatabase.SearchObject<QuestionData>(questionNumber);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionNumber;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            QuestionData questionState = new QuestionData();
            questionState.ShortText = STRSTATEPERSON;            
            questionState = SmartCadDatabase.SearchObject<QuestionData>(questionState);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionState;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            //Preguntas especificas
            QuestionData questionType = new QuestionData();
            questionType.Text = ResourceInitialDataLoader.GetString2("AccidentType");
            questionType.ShortText = ResourceInitialDataLoader.GetString2("TraumaType");
            questionType.CustomCode = incidentType.CustomCode + "TYPE";
            questionType.RequirementType = RequirementTypeEnum.Recommended;
            questionType.SetPossibleAnswers = new HashedSet();            

            //Esta estructura se maneja de manera generica para todos los incidentes
            QuestionPossibleAnswerData answer1 = new QuestionPossibleAnswerData();
            answer1.Question = questionType;
            answer1.Description = ResourceInitialDataLoader.GetString2("TraumaAndContusions");
            answer1.ControlName = "Chckdt6";

            questionType.SetPossibleAnswers.Add(answer1);

            QuestionPossibleAnswerData answer2 = new QuestionPossibleAnswerData();
            answer2.Question = questionType;
            answer2.Description = ResourceInitialDataLoader.GetString2("Poison");
            answer2.ControlName = "Chckdt5";

            questionType.SetPossibleAnswers.Add(answer2);

            QuestionPossibleAnswerData answer3 = new QuestionPossibleAnswerData();
            answer3.Question = questionType;
            answer3.Description = ResourceInitialDataLoader.GetString2("Suicide");
            answer3.ControlName = "Chckdt4";

            questionType.SetPossibleAnswers.Add(answer3);

            SmartCadDatabase.SaveObject<QuestionData>(questionType);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            string renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Suicidas' itemName='item1' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt4' />
  <Control itemText='Trauma y contusiones' itemName='item3' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt6' />
  <Control itemText='Intoxicaci�n' itemName='item2' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt5' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='4'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=335@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Cami?n de carga</property>
      <property name='MinSize'>@3,Width=141@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Suicidas</property>
      <property name='TextSize'>@3,Width=105@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@3,X=290@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt4</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=149@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Particular</property>
      <property name='MinSize'>@3,Width=141@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Trauma y contusiones</property>
      <property name='TextSize'>@3,Width=105@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item3</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt6</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item4' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=141@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Publico</property>
      <property name='MinSize'>@3,Width=141@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Intoxicaci�n</property>
      <property name='TextSize'>@3,Width=105@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@3,X=149@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt5</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>";

            questionType.RenderMethod = renderMethod;
            SmartCadDatabase.UpdateObject<QuestionData>(questionType);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionType;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            //Asocio las preguntas genericas para cada incendio
            QuestionData question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("FractureQuestion");          
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("ContusionQuestion");            
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);


            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("Bleeding");            
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("Sintomes");            
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("ToxicQuestion");            
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("SuicideQuestion");            
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

           
            SmartCadDatabase.SaveObject<IncidentTypeData>(incidentType);
            #endregion

            #region AccidenteIndustrial
            //Paso 1
            incidentType = new IncidentTypeData();
            incidentType.CustomCode = "ACI";
            incidentType.Name = "AccidentIndistrial";
            incidentType.FriendlyName = ResourceInitialDataLoader.GetString2("IndustrialAccident");
            incidentType.NoEmergency = false;

            //Paso 2
            //Bomberos
            dpFire = new DepartmentTypeData(DPFIRERESCUE);
            dpFire = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFire);

            //Bomberos (Atenci�n pre-hospitalaria)
            dpFirePre = new DepartmentTypeData(DPFIREPRE);
            dpFirePre = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFirePre);

            //Transito
            dpTransito = new DepartmentTypeData(DPTRANS);
            dpTransito = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpTransito);

            incidentType.SetIncidentTypeDepartmentTypes = new HashedSet();

            itdFire = new IncidentTypeDepartmentTypeData();
            itdFire.DepartmentType = dpFire;
            itdFire.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFire);

            itdFirePre = new IncidentTypeDepartmentTypeData();
            itdFirePre.DepartmentType = dpFirePre;
            itdFirePre.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFirePre);

            itdTransito = new IncidentTypeDepartmentTypeData();
            itdTransito.DepartmentType = dpTransito;
            itdTransito.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdTransito);

            incidentType.Procedure = @"<b>" + ResourceInitialDataLoader.GetString2("ProceduresTo") + incidentType.FriendlyName + @".</b> <ul> <li>" + ResourceInitialDataLoader.GetString2("Step1") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step2") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step3") + @"</li> </ul> ";



            //Paso 4

            //Indico las preguntas genericas para el incidente
            incidentType.SetIncidentTypeQuestions = new HashedSet();

            questionWhere = new QuestionData();
            questionWhere.ShortText = STRQUESTIONWHERE;           
            questionWhere = SmartCadDatabase.SearchObject<QuestionData>(questionWhere);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionWhere;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            questionNumber = new QuestionData();
            questionNumber.ShortText = STRQUESTIONNUMBERPERSON;            
            questionNumber = SmartCadDatabase.SearchObject<QuestionData>(questionNumber);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionNumber;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            questionState = new QuestionData();
            questionState.ShortText = STRSTATEPERSON;           
            questionState = SmartCadDatabase.SearchObject<QuestionData>(questionState);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionState;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            //Preguntas especificas
            questionType = new QuestionData();
            questionType.Text = ResourceInitialDataLoader.GetString2("KindOfToxic");
            questionType.ShortText = ResourceInitialDataLoader.GetString2("KindOfToxicQuestion");
            questionType.CustomCode = incidentType.CustomCode + "TYPE";
            questionType.RequirementType = RequirementTypeEnum.Recommended;
            questionType.SetPossibleAnswers = new HashedSet();           

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = questionType;
            answer1.Description = ResourceInitialDataLoader.GetString2("Toxics");
            answer1.ControlName = "Chckdt6";
            questionType.SetPossibleAnswers.Add(answer1);

            answer2 = new QuestionPossibleAnswerData();
            answer2.Question = questionType;
            answer2.Description = ResourceInitialDataLoader.GetString2("TraumaAndContusions");
            answer2.ControlName = "Chckdt5";
            questionType.SetPossibleAnswers.Add(answer2);

            answer3 = new QuestionPossibleAnswerData();
            answer3.Question = questionType;
            answer3.Description = ResourceInitialDataLoader.GetString2("BurnsQuestion");
            answer3.ControlName = "Chckdt4";
            questionType.SetPossibleAnswers.Add(answer3);


            SmartCadDatabase.SaveObject<QuestionData>(questionType);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region Render
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='�Quemaduras?' itemName='item1' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt4' />
  <Control itemText='T�xicos' itemName='item3' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt6' />
  <Control itemText='Trauma y contusiones' itemName='item2' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt5' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='4'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=335@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Cami?n de carga</property>
      <property name='MinSize'>@3,Width=141@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>�Quemaduras?</property>
      <property name='TextSize'>@3,Width=105@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@3,X=290@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt4</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=149@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Particular</property>
      <property name='MinSize'>@3,Width=141@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>T�xicos</property>
      <property name='TextSize'>@3,Width=105@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item3</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt6</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item4' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=141@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Publico</property>
      <property name='MinSize'>@3,Width=141@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Trauma y contusiones</property>
      <property name='TextSize'>@3,Width=105@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@3,X=149@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt5</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>"; 
            #endregion

            questionType.RenderMethod = renderMethod;
            SmartCadDatabase.UpdateObject<QuestionData>(questionType);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionType;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            //Asocio las preguntas genericas para cada incendio
            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("FractureQuestion");            
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("ContusionQuestion");            
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);


            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("Bleeding");            
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("Sintomes");            
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("ToxicQuestion");            
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

             SmartCadDatabase.SaveObject<IncidentTypeData>(incidentType);
            #endregion

            #region AccidenteTransito
            //Paso 1
            incidentType = new IncidentTypeData();
            incidentType.CustomCode = "ACT";
            incidentType.Name = "AccidentTransit";
            incidentType.FriendlyName = ResourceInitialDataLoader.GetString2("TransitAccident");
            incidentType.NoEmergency = false;

            //Paso 2
            //Bomberos
            dpFire = new DepartmentTypeData(DPFIRERESCUE);
            dpFire = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFire);

            //Bomberos (Atenci�n pre-hospitalaria)
            dpFirePre = new DepartmentTypeData(DPFIREPRE);
            dpFirePre = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFirePre);

            //Transito
            dpTransito = new DepartmentTypeData(DPTRANS);
            dpTransito = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpTransito);

            incidentType.SetIncidentTypeDepartmentTypes = new HashedSet();

            itdFire = new IncidentTypeDepartmentTypeData();
            itdFire.DepartmentType = dpFire;
            itdFire.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFire);

            itdFirePre = new IncidentTypeDepartmentTypeData();
            itdFirePre.DepartmentType = dpFirePre;
            itdFirePre.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFirePre);

            itdTransito = new IncidentTypeDepartmentTypeData();
            itdTransito.DepartmentType = dpTransito;
            itdTransito.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdTransito);

            incidentType.Procedure = @"<b>" + ResourceInitialDataLoader.GetString2("ProceduresTo") + incidentType.FriendlyName + @".</b> <ul> <li>" + ResourceInitialDataLoader.GetString2("Step1") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step2") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step3") + @"</li> </ul> ";



            //Paso 4

            //Indico las preguntas genericas para el incidente
            incidentType.SetIncidentTypeQuestions = new HashedSet();

            questionWhere = new QuestionData();
            questionWhere.ShortText = STRQUESTIONWHERE;           
            questionWhere = SmartCadDatabase.SearchObject<QuestionData>(questionWhere);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionWhere;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            questionNumber = new QuestionData();
            questionNumber.ShortText = STRQUESTIONNUMBERPERSON;           
            questionNumber = SmartCadDatabase.SearchObject<QuestionData>(questionNumber);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionNumber;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            questionState = new QuestionData();
            questionState.ShortText = STRSTATEPERSON;           
            questionState = SmartCadDatabase.SearchObject<QuestionData>(questionState);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionState;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            //Preguntas especificas
            questionType = new QuestionData();
            questionType.Text = ResourceInitialDataLoader.GetString2("CrashTypeQuestion");
            questionType.ShortText = ResourceInitialDataLoader.GetString2("CollisionTypeQuestion");
            questionType.CustomCode = incidentType.CustomCode + "TYPE";
            questionType.RequirementType = RequirementTypeEnum.Recommended;
            questionType.SetPossibleAnswers = new HashedSet();           

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = questionType;
            answer1.Description = ResourceInitialDataLoader.GetString2("Collision");
            answer1.ControlName = "Chckdt6";

            questionType.SetPossibleAnswers.Add(answer1);

            answer2 = new QuestionPossibleAnswerData();
            answer2.Question = questionType;
            answer2.Description = ResourceInitialDataLoader.GetString2("Dumping");
            answer2.ControlName = "Chckdt5";

            questionType.SetPossibleAnswers.Add(answer2);

            answer3 = new QuestionPossibleAnswerData();
            answer3.Question = questionType;
            answer3.Description = ResourceInitialDataLoader.GetString2("RunOver");
            answer3.ControlName = "Chckdt4";
            questionType.SetPossibleAnswers.Add(answer3);

            SmartCadDatabase.SaveObject<QuestionData>(questionType);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region Render
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Arrollamiento' itemName='item1' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt4' />
  <Control itemText='�Tipo Colisi�n?' itemName='item3' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt6' />
  <Control itemText='Volcamiento' itemName='item2' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt5' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='4'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=335@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Cami?n de carga</property>
      <property name='MinSize'>@3,Width=105@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Arrollamiento</property>
      <property name='TextSize'>@2,Width=69@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@3,X=290@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt4</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=149@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Particular</property>
      <property name='MinSize'>@3,Width=105@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>�Tipo Colisi�n?</property>
      <property name='TextSize'>@2,Width=69@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item3</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt6</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item4' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=141@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Publico</property>
      <property name='MinSize'>@3,Width=105@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Volcamiento</property>
      <property name='TextSize'>@2,Width=69@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@3,X=149@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt5</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>"; 
            #endregion

            questionType.RenderMethod = renderMethod;
            SmartCadDatabase.UpdateObject<QuestionData>(questionType);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionType;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            questionType = new QuestionData();
            questionType.Text = ResourceInitialDataLoader.GetString2("VehicleTypeOf");
            questionType.ShortText = ResourceInitialDataLoader.GetString2("Vehicle");
            questionType.CustomCode = incidentType.CustomCode + "VEHICLE";
            questionType.RequirementType = RequirementTypeEnum.Recommended;
            questionType.SetPossibleAnswers = new HashedSet();            

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = questionType;
            answer1.Description = ResourceInitialDataLoader.GetString2("Type");
            answer1.ControlName = "RdBttn1";
            questionType.SetPossibleAnswers.Add(answer1);

            SmartCadDatabase.SaveObject<QuestionData>(questionType);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region Render
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Tipo' itemName='item1' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Tipo</property>
      <property name='MinSize'>@2,Width=56@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Tipo</property>
      <property name='TextSize'>@2,Width=20@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>";

            #endregion
            questionType.RenderMethod = renderMethod;
            SmartCadDatabase.UpdateObject<QuestionData>(questionType);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionType;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            questionType = new QuestionData();
            questionType.Text = ResourceInitialDataLoader.GetString2("AmmountOfVehicle");
            questionType.ShortText = ResourceInitialDataLoader.GetString2("Ammount");
            questionType.CustomCode = incidentType.CustomCode + "COUNT";
            questionType.RequirementType = RequirementTypeEnum.Recommended;
            questionType.SetPossibleAnswers = new HashedSet();           

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = questionType;
            answer1.Description = ResourceInitialDataLoader.GetString2("One");
            answer1.ControlName = "RdBttn1";
            questionType.SetPossibleAnswers.Add(answer1);

            SmartCadDatabase.SaveObject<QuestionData>(questionType);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region Render
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='1' itemName='item1' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>1</property>
      <property name='MinSize'>@2,Width=56@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>1</property>
      <property name='TextSize'>@2,Width=20@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>"; 
            #endregion

            questionType.RenderMethod = renderMethod;
            SmartCadDatabase.UpdateObject<QuestionData>(questionType);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionType;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            SmartCadDatabase.SaveObject<IncidentTypeData>(incidentType);
            #endregion
        }

        private static void CreateGenericQuestionCatastofre()
        {
            //Crea los tipo de preguntas y procedimientos genericos para el tipo de incidente catastofre natural
            #region edificaci�n
            QuestionData question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("BuildingsQuestionText");
            question.ShortText = ResourceInitialDataLoader.GetString2("BuildingsQuestion");
            question.CustomCode = "BUILDINGS";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            QuestionPossibleAnswerData answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceLoader.GetString2("Building");
            answer1.ControlName = "TxtBxx1";

            question.SetPossibleAnswers.Add(answer1);

            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region Render edificacion
            string renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Edificacion' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.19885, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Edificacion</property>
      <property name='MinSize'>@2,Width=93@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Edificacion</property>
      <property name='TextSize'>@2,Width=57@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>";
            #endregion
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region fisura
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("�FissureQuestionText");
            question.ShortText = ResourceInitialDataLoader.GetString2("FissureQuestion");
            question.CustomCode = "FISSURE";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Fissure");
            answer1.ControlName = "TxtBxx1";

            question.SetPossibleAnswers.Add(answer1);

            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region REnder Fisura
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Fisura' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.19885, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Fisura</property>
      <property name='MinSize'>@2,Width=93@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Fisura</property>
      <property name='TextSize'>@2,Width=57@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>";
            #endregion
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region cuantas
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("HowManyTrappedQuestionText");
            question.ShortText = ResourceInitialDataLoader.GetString2("HowManyTrappedQuestion");
            question.CustomCode = "HOWMANY";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = "1";
            answer1.ControlName = "RdBttn1";
            question.SetPossibleAnswers.Add(answer1);

            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region Render Cuantas
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='1' itemName='item1' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>1</property>
      <property name='MinSize'>@2,Width=42@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>1</property>
      <property name='TextSize'>@1,Width=6@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>";
            #endregion
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region situacion
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("SituationQuestionText");
            question.ShortText = ResourceInitialDataLoader.GetString2("SituationQuestion");
            question.CustomCode = "SITUATION";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Situation");
            answer1.ControlName = "TxtBxx1";
            question.SetPossibleAnswers.Add(answer1);

            SmartCadDatabase.SaveObject<QuestionData>(question);
            #region renderMethod
            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Situaci�n' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.19885, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Situaci�n</property>
      <property name='MinSize'>@2,Width=93@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Situaci�n</property>
      <property name='TextSize'>@2,Width=57@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>";
            question.RenderMethod = renderMethod;
            #endregion
            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region sepultadas
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("BuriedQuestionText");
            question.ShortText = ResourceInitialDataLoader.GetString2("BuriedQuestion");
            question.CustomCode = "BURIED";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = "1";
            answer1.ControlName = "RdBttn1";
            question.SetPossibleAnswers.Add(answer1);

            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region Render Sepultadas
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='1' itemName='item1' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>1</property>
      <property name='MinSize'>@2,Width=42@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>1</property>
      <property name='TextSize'>@1,Width=6@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>";
            #endregion
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region via
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("RoadQuestionText");
            question.ShortText = ResourceInitialDataLoader.GetString2("RoadQuestion");
            question.CustomCode = "VIA";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Road");
            answer1.ControlName = "TxtBxx1";
            question.SetPossibleAnswers.Add(answer1);

            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region Render Via
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='V�a' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.19885, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>V�a</property>
      <property name='MinSize'>@2,Width=93@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>V�a</property>
      <property name='TextSize'>@2,Width=57@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>";
            #endregion
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region derrumbe
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("CollapseQuestionText");
            question.ShortText = ResourceInitialDataLoader.GetString2("CollapseQuestion");
            question.CustomCode = "COLLAPSE";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Rocks");
            answer1.ControlName = "Chckdt2";
            question.SetPossibleAnswers.Add(answer1);

            QuestionPossibleAnswerData answer2 = new QuestionPossibleAnswerData();
            answer2.Question = question;
            answer2.Description = ResourceInitialDataLoader.GetString2("Soil");
            answer2.ControlName = "Chckdt1";
            question.SetPossibleAnswers.Add(answer2);

            QuestionPossibleAnswerData answer3 = new QuestionPossibleAnswerData();
            answer3.Question = question;
            answer3.Description = ResourceInitialDataLoader.GetString2("Debris");
            answer3.ControlName = "Chckdt3";
            question.SetPossibleAnswers.Add(answer3);

            QuestionPossibleAnswerData answer4 = new QuestionPossibleAnswerData();
            answer4.Question = question;
            answer4.Description = ResourceInitialDataLoader.GetString2("Trees");
            answer4.ControlName = "Chckdt4";
            question.SetPossibleAnswers.Add(answer4);

            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region Render Derrumbe
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Rocas' itemName='item2' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt2' />
  <Control itemText='Escombros' itemName='item3' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt3' />
  <Control itemText='Tierra' itemName='item1' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt1' />
  <Control itemText='Arboles' itemName='item0' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt4' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='5'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=155@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Vomito</property>
      <property name='MinSize'>@2,Width=87@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Rocas</property>
      <property name='TextSize'>@2,Width=51@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=157@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Perdida de conciencia</property>
      <property name='MinSize'>@2,Width=87@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Escombros</property>
      <property name='TextSize'>@2,Width=51@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item3</property>
      <property name='Location'>@3,X=311@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt3</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item4' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=156@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Sudoracion</property>
      <property name='MinSize'>@2,Width=87@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Tierra</property>
      <property name='TextSize'>@2,Width=51@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@3,X=155@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item5' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=157@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Camion</property>
      <property name='MinSize'>@2,Width=87@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Arboles</property>
      <property name='TextSize'>@2,Width=51@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item0</property>
      <property name='Location'>@3,X=468@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt4</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>";
            #endregion
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region quebrada
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("RiversQuestion");
            question.ShortText = ResourceInitialDataLoader.GetString2("RiversQuestion");
            question.CustomCode = "RIVER";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Rivers");
            answer1.ControlName = "TxtBxx1";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

            //* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);

            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region Render Quebrada
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Quebrada' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.19885, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Quebrada</property>
      <property name='MinSize'>@2,Width=93@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Quebrada</property>
      <property name='TextSize'>@2,Width=57@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>";
            #endregion
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region lluvia
            question = new QuestionData();
            question.Text = ResourceLoader.GetString2("RainLevelQuestion");
            question.ShortText = ResourceInitialDataLoader.GetString2("RainsQuestion");
            question.CustomCode = "RAIN";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceLoader.GetString2("Level");
            answer1.ControlName = "TxtBxx1";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

            //* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);

            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region Render LLuvia
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Lluvia' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.19885, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Lluvia</property>
      <property name='MinSize'>@2,Width=93@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Lluvia</property>
      <property name='TextSize'>@2,Width=57@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>";
            #endregion
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion
        }

        private static void CreateCatastrofeType()
        {
            //Este metodo crea la carga inicial para todos los tipos de incidentes de catastofre natural
            //Para agregar un tipo de incidente debo realizar los siguientes pasos
            //1.- Crear el incidente.
            //2.- Asociarle los tipos de departamentos que dan soporte a ese incidente
            //3.- Indicar los tipos de procedimientos asociados al incidente
            //4.- Indicar los tipos de preguntas asociadas al incidente con la siguiente estructura
            //4.1.-     Indicar un procedimiento para cada pregunta
            //4.2.-     Indicar las posible respuesta asociadas a la pregunta
            //4.3.-     Indicar la forma en que se renderiza la pregunta en el frontclient indicando los id de las respuestas asociadas

            CreateGenericQuestionCatastofre();

            #region CatastofreTemblorTerremoto
            //Paso 1
            IncidentTypeData incidentType = new IncidentTypeData();
            incidentType.CustomCode = "TER";
            incidentType.Name = "CatastofreTerremoto";
            incidentType.FriendlyName = ResourceInitialDataLoader.GetString2("EarthquakeDisaster");
            incidentType.NoEmergency = false;

            //Paso 2
            //Bomberos Rescate
            DepartmentTypeData dpFire = new DepartmentTypeData(DPFIRERESCUE);
            dpFire = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFire);

            //Bomberos (Atenci�n pre-hospitalaria)
            DepartmentTypeData dpFirePre = new DepartmentTypeData(DPFIREPRE);
            dpFirePre = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFirePre);

            //Protecci�n Civil
            DepartmentTypeData dpTransito = new DepartmentTypeData(DPPRECIVIL);
            dpTransito = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpTransito);

            incidentType.SetIncidentTypeDepartmentTypes = new HashedSet();

            IncidentTypeDepartmentTypeData itdFire = new IncidentTypeDepartmentTypeData();
            itdFire.DepartmentType = dpFire;
            itdFire.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFire);

            IncidentTypeDepartmentTypeData itdFirePre = new IncidentTypeDepartmentTypeData();
            itdFirePre.DepartmentType = dpFirePre;
            itdFirePre.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFirePre);

            IncidentTypeDepartmentTypeData itdTransito = new IncidentTypeDepartmentTypeData();
            itdTransito.DepartmentType = dpTransito;
            itdTransito.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdTransito);

            incidentType.Procedure = @"<b>" + ResourceInitialDataLoader.GetString2("ProceduresTo") + incidentType.FriendlyName + @".</b> <ul> <li>" + ResourceInitialDataLoader.GetString2("Step1") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step2") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step3") + @"</li> </ul> ";



            //Paso 4

            //Indico las preguntas genericas para el incidente
            incidentType.SetIncidentTypeQuestions = new HashedSet();

            QuestionData questionWhere = new QuestionData();
            questionWhere.ShortText = STRQUESTIONWHERE;
            questionWhere = SmartCadDatabase.SearchObject<QuestionData>(questionWhere);


            IncidentTypeQuestionData itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionWhere;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            QuestionData questionNumber = new QuestionData();
            questionNumber.ShortText = STRQUESTIONNUMBERPERSON;
            questionNumber = SmartCadDatabase.SearchObject<QuestionData>(questionNumber);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionNumber;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            QuestionData questionState = new QuestionData();
            questionState.ShortText = STRSTATEPERSON;
            questionState = SmartCadDatabase.SearchObject<QuestionData>(questionState);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionState;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            //Preguntas especificas
            QuestionData questionType = new QuestionData();
            questionType.Text = ResourceInitialDataLoader.GetString2("EarthquakeType");
            questionType.ShortText = ResourceInitialDataLoader.GetString2("EarthquakeTypeQuestion");
            questionType.CustomCode = incidentType.CustomCode + "TYPE";
            questionType.RequirementType = RequirementTypeEnum.Recommended;
            questionType.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            QuestionPossibleAnswerData answer1 = new QuestionPossibleAnswerData();
            answer1.Question = questionType;
            answer1.Description = ResourceInitialDataLoader.GetString2("Trapped");
            answer1.ControlName = "Chckdt2"; 
            questionType.SetPossibleAnswers.Add(answer1);

            QuestionPossibleAnswerData answer2 = new QuestionPossibleAnswerData();
            answer2.Question = questionType;
            answer2.Description = ResourceInitialDataLoader.GetString2("BuildingsCollapse");
            answer2.ControlName = "Chckdt3";
            questionType.SetPossibleAnswers.Add(answer2);

            QuestionPossibleAnswerData answer3 = new QuestionPossibleAnswerData();
            answer3.Question = questionType;
            answer3.Description = ResourceInitialDataLoader.GetString2("BuildingsFissure");
            answer3.ControlName = "Chckdt1";
            questionType.SetPossibleAnswers.Add(answer3);

            SmartCadDatabase.SaveObject<QuestionData>(questionType);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region Render
            string renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Fisura de edificaciones' itemName='item1' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt1' />
  <Control itemText='Atrapados' itemName='item2' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt2' />
  <Control itemText='Derrumbe de edificaciones' itemName='item3' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt3' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='4'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=301@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Fisura de edificaciones</property>
      <property name='MinSize'>@3,Width=162@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Fisura de edificaciones</property>
      <property name='TextSize'>@3,Width=126@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@3,X=324@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=162@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Atrapados</property>
      <property name='MinSize'>@3,Width=162@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Atrapados</property>
      <property name='TextSize'>@3,Width=126@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item4' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=162@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Derrumbe de edificaciones</property>
      <property name='MinSize'>@3,Width=162@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Derrumbe de edificaciones</property>
      <property name='TextSize'>@3,Width=126@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item3</property>
      <property name='Location'>@3,X=162@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt3</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>"; 
            #endregion

            questionType.RenderMethod = renderMethod;
            SmartCadDatabase.UpdateObject<QuestionData>(questionType);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionType;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            //Asocio las preguntas genericas para cada incendio
            QuestionData question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("BuildingsQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("FissureQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);


            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("HowManyTrappedQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            SmartCadDatabase.SaveObject<IncidentTypeData>(incidentType);
            #endregion

            #region CatastofreDerrumbre
            //Paso 1
            incidentType = new IncidentTypeData();
            incidentType.CustomCode = "DER";
            incidentType.Name = "CatastofreDerrumbe";
            incidentType.FriendlyName = ResourceInitialDataLoader.GetString2("CollapseDisaster");
            incidentType.NoEmergency = false;

            //Paso 2
            //Bomberos Rescate
            dpFire = new DepartmentTypeData(DPFIRERESCUE);
            dpFire = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFire);

            //Bomberos (Atenci�n pre-hospitalaria)
            dpFirePre = new DepartmentTypeData(DPFIREPRE);
            dpFirePre = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFirePre);

            //Protecci�n Civil
            dpTransito = new DepartmentTypeData(DPPRECIVIL);
            dpTransito = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpTransito);

            incidentType.SetIncidentTypeDepartmentTypes = new HashedSet();

            itdFire = new IncidentTypeDepartmentTypeData();
            itdFire.DepartmentType = dpFire;
            itdFire.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFire);

            itdFirePre = new IncidentTypeDepartmentTypeData();
            itdFirePre.DepartmentType = dpFirePre;
            itdFirePre.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFirePre);

            itdTransito = new IncidentTypeDepartmentTypeData();
            itdTransito.DepartmentType = dpTransito;
            itdTransito.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdTransito);

            incidentType.Procedure = @"<b>" + ResourceInitialDataLoader.GetString2("ProceduresTo") + incidentType.FriendlyName + @".</b> <ul> <li>" + ResourceInitialDataLoader.GetString2("Step1") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step2") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step3") + @"</li> </ul> ";



            //Paso 4

            //Indico las preguntas genericas para el incidente
            incidentType.SetIncidentTypeQuestions = new HashedSet();

            questionWhere = new QuestionData();
            questionWhere.ShortText = STRQUESTIONWHERE;
            questionWhere = SmartCadDatabase.SearchObject<QuestionData>(questionWhere);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionWhere;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            questionNumber = new QuestionData();
            questionNumber.ShortText = STRQUESTIONNUMBERPERSON;
            questionNumber = SmartCadDatabase.SearchObject<QuestionData>(questionNumber);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionNumber;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            questionState = new QuestionData();
            questionState.ShortText = STRSTATEPERSON;
            questionState = SmartCadDatabase.SearchObject<QuestionData>(questionState);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionState;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            //Preguntas especificas
            questionType = new QuestionData();
            questionType.Text = ResourceInitialDataLoader.GetString2("CollapseType");
            questionType.ShortText = ResourceInitialDataLoader.GetString2("CollapseTypeQuestion");
            questionType.CustomCode = incidentType.CustomCode + "TYPE";
            questionType.RequirementType = RequirementTypeEnum.Recommended;
            questionType.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = questionType;
            answer1.Description = ResourceInitialDataLoader.GetString2("Trapped");
            answer1.ControlName = "Chckdt2";
            questionType.SetPossibleAnswers.Add(answer1);

            answer2 = new QuestionPossibleAnswerData();
            answer2.Question = questionType;
            answer2.Description = ResourceInitialDataLoader.GetString2("RoadBlock");
            answer2.ControlName = "Chckdt3";
            questionType.SetPossibleAnswers.Add(answer2);


            SmartCadDatabase.SaveObject<QuestionData>(questionType);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region Render
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Atrapados' itemName='item2' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt2' />
  <Control itemText='Bloqueo de vias' itemName='item3' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt3' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='3'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=162@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Atrapados</property>
      <property name='MinSize'>@3,Width=111@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Atrapados</property>
      <property name='TextSize'>@2,Width=75@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=463@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Derrumbe de edificaciones</property>
      <property name='MinSize'>@3,Width=111@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Bloqueo de vias</property>
      <property name='TextSize'>@2,Width=75@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item3</property>
      <property name='Location'>@3,X=162@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt3</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>"; 
            #endregion

            questionType.RenderMethod = renderMethod;
            SmartCadDatabase.UpdateObject<QuestionData>(questionType);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionType;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            //Asocio las preguntas genericas para cada incendio
            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("HowManyTrappedQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("BuriedQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);


            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("RoadQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("CollapseQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);


            SmartCadDatabase.SaveObject<IncidentTypeData>(incidentType);
            #endregion

            #region CatastofreAvalancha
            //Paso 1
            incidentType = new IncidentTypeData();
            incidentType.CustomCode = "AVA";
            incidentType.Name = "CatastofreAvalancha";
            incidentType.FriendlyName = ResourceInitialDataLoader.GetString2("AvalancheDisaster");
            incidentType.NoEmergency = false;

            //Paso 2
            //Bomberos Rescate
            dpFire = new DepartmentTypeData(DPFIRERESCUE);
            dpFire = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFire);

            //Bomberos (Atenci�n pre-hospitalaria)
            dpFirePre = new DepartmentTypeData(DPFIREPRE);
            dpFirePre = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFirePre);

            //Protecci�n Civil
            dpTransito = new DepartmentTypeData(DPPRECIVIL);
            dpTransito = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpTransito);

            incidentType.SetIncidentTypeDepartmentTypes = new HashedSet();

            itdFire = new IncidentTypeDepartmentTypeData();
            itdFire.DepartmentType = dpFire;
            itdFire.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFire);

            itdFirePre = new IncidentTypeDepartmentTypeData();
            itdFirePre.DepartmentType = dpFirePre;
            itdFirePre.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFirePre);

            itdTransito = new IncidentTypeDepartmentTypeData();
            itdTransito.DepartmentType = dpTransito;
            itdTransito.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdTransito);

            incidentType.Procedure = @"<b>" + ResourceInitialDataLoader.GetString2("ProceduresTo") + incidentType.FriendlyName + @".</b> <ul> <li>" + ResourceInitialDataLoader.GetString2("Step1") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step2") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step3") + @"</li> </ul> ";



            //Paso 4

            //Indico las preguntas genericas para el incidente
            incidentType.SetIncidentTypeQuestions = new HashedSet();

            questionWhere = new QuestionData();
            questionWhere.ShortText = STRQUESTIONWHERE;
            questionWhere = SmartCadDatabase.SearchObject<QuestionData>(questionWhere);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionWhere;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            questionNumber = new QuestionData();
            questionNumber.ShortText = STRQUESTIONNUMBERPERSON;
            questionNumber = SmartCadDatabase.SearchObject<QuestionData>(questionNumber);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionNumber;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            questionState = new QuestionData();
            questionState.ShortText = STRSTATEPERSON;
            questionState = SmartCadDatabase.SearchObject<QuestionData>(questionState);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionState;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            //Preguntas especificas
            questionType = new QuestionData();
            questionType.Text = ResourceInitialDataLoader.GetString2("AvalancheType");
            questionType.ShortText = ResourceInitialDataLoader.GetString2("AvalancheTypeQuestion");
            questionType.CustomCode = incidentType.CustomCode + "TYPE";
            questionType.RequirementType = RequirementTypeEnum.Recommended;
            questionType.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = questionType;
            answer1.Description = ResourceInitialDataLoader.GetString2("Trapped");
            answer1.ControlName = "Chckdt2";
            questionType.SetPossibleAnswers.Add(answer1);

            answer2 = new QuestionPossibleAnswerData();
            answer2.Question = questionType;
            answer2.Description = ResourceInitialDataLoader.GetString2("RoadBlock");
            answer2.ControlName = "Chckdt3";
            questionType.SetPossibleAnswers.Add(answer2);

            answer3 = new QuestionPossibleAnswerData();
            answer3.Question = questionType;
            answer3.Description = ResourceInitialDataLoader.GetString2("Buried");
            answer3.ControlName = "Chckdt4";
            questionType.SetPossibleAnswers.Add(answer3);

            SmartCadDatabase.SaveObject<QuestionData>(questionType);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region Render
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Atrapados' itemName='item2' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt2' />
  <Control itemText='Bloqueo de vias' itemName='item3' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt3' />
  <Control itemText='�Sepultadas?' itemName='item0' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt4' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='4'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=162@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Atrapados</property>
      <property name='MinSize'>@3,Width=111@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Atrapados</property>
      <property name='TextSize'>@2,Width=75@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=231@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Derrumbe de edificaciones</property>
      <property name='MinSize'>@3,Width=111@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Bloqueo de vias</property>
      <property name='TextSize'>@2,Width=75@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item3</property>
      <property name='Location'>@3,X=162@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt3</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item4' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=232@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>�Sepultadas?</property>
      <property name='MinSize'>@3,Width=111@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>�Sepultadas?</property>
      <property name='TextSize'>@2,Width=75@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item0</property>
      <property name='Location'>@3,X=393@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt4</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>"; 
            #endregion

            questionType.RenderMethod = renderMethod;
            SmartCadDatabase.UpdateObject<QuestionData>(questionType);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionType;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            //Asocio las preguntas genericas para cada incendio
            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("HowManyTrappedQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("BuriedQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);


            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("RoadQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("CollapseQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            SmartCadDatabase.SaveObject<IncidentTypeData>(incidentType);
            #endregion

            #region CatastofreInundacion
            //Paso 1
            incidentType = new IncidentTypeData();
            incidentType.CustomCode = "INU";
            incidentType.Name = "CatastofreInundaci�n";
            incidentType.FriendlyName = ResourceInitialDataLoader.GetString2("FloodDisaster");
            incidentType.NoEmergency = false;

            //Paso 2
            //Bomberos Rescate
            dpFire = new DepartmentTypeData(DPFIRERESCUE);
            dpFire = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFire);

            //Bomberos (Atenci�n pre-hospitalaria)
            dpFirePre = new DepartmentTypeData(DPFIREPRE);
            dpFirePre = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFirePre);

            //Protecci�n Civil
            dpTransito = new DepartmentTypeData(DPPRECIVIL);
            dpTransito = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpTransito);

            incidentType.SetIncidentTypeDepartmentTypes = new HashedSet();

            itdFire = new IncidentTypeDepartmentTypeData();
            itdFire.DepartmentType = dpFire;
            itdFire.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFire);

            itdFirePre = new IncidentTypeDepartmentTypeData();
            itdFirePre.DepartmentType = dpFirePre;
            itdFirePre.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFirePre);

            itdTransito = new IncidentTypeDepartmentTypeData();
            itdTransito.DepartmentType = dpTransito;
            itdTransito.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdTransito);

            incidentType.Procedure = @"<b>" + ResourceInitialDataLoader.GetString2("ProceduresTo") + incidentType.FriendlyName + @".</b> <ul> <li>" + ResourceInitialDataLoader.GetString2("Step1") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step2") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step3") + @"</li> </ul> ";



            //Paso 4

            //Indico las preguntas genericas para el incidente
            incidentType.SetIncidentTypeQuestions = new HashedSet();

            questionWhere = new QuestionData();
            questionWhere.ShortText = STRQUESTIONWHERE;
            questionWhere = SmartCadDatabase.SearchObject<QuestionData>(questionWhere);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionWhere;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            questionNumber = new QuestionData();
            questionNumber.ShortText = STRQUESTIONNUMBERPERSON;
            questionNumber = SmartCadDatabase.SearchObject<QuestionData>(questionNumber);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionNumber;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            questionState = new QuestionData();
            questionState.ShortText = STRSTATEPERSON;
            questionState = SmartCadDatabase.SearchObject<QuestionData>(questionState);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionState;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            //Preguntas especificas
            questionType = new QuestionData();
            questionType.Text = ResourceInitialDataLoader.GetString2("FloodType");
            questionType.ShortText = ResourceInitialDataLoader.GetString2("RainTypeQuestion");
            questionType.CustomCode = incidentType.CustomCode + "TYPE";
            questionType.RequirementType = RequirementTypeEnum.Recommended;
            questionType.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = questionType;
            answer1.Description = ResourceInitialDataLoader.GetString2("Rains");
            answer1.ControlName = "Chckdt2";
            questionType.SetPossibleAnswers.Add(answer1);

            answer2 = new QuestionPossibleAnswerData();
            answer2.Question = questionType;
            answer2.Description = ResourceInitialDataLoader.GetString2("Rivers");
            answer2.ControlName = "Chckdt3";
            questionType.SetPossibleAnswers.Add(answer2);

            answer3 = new QuestionPossibleAnswerData();
            answer3.Question = questionType;
            answer3.Description = ResourceInitialDataLoader.GetString2("BuildingsAndStructures");
            answer3.ControlName = "Chckdt4";
            questionType.SetPossibleAnswers.Add(answer3);

            SmartCadDatabase.SaveObject<QuestionData>(questionType);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region Render
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Lluvias' itemName='item2' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt2' />
  <Control itemText='Quebradas' itemName='item3' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt3' />
  <Control itemText='Edificios y estructuras' itemName='item0' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt4' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='4'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=162@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Atrapados</property>
      <property name='MinSize'>@3,Width=141@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Lluvias</property>
      <property name='TextSize'>@3,Width=105@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=231@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Derrumbe de edificaciones</property>
      <property name='MinSize'>@3,Width=141@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Quebradas</property>
      <property name='TextSize'>@3,Width=105@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item3</property>
      <property name='Location'>@3,X=162@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt3</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item4' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=232@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>?Sepultadas?</property>
      <property name='MinSize'>@3,Width=141@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Edificios y estructuras</property>
      <property name='TextSize'>@3,Width=105@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item0</property>
      <property name='Location'>@3,X=393@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt4</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>"; 
            #endregion

            questionType.RenderMethod = renderMethod;
            SmartCadDatabase.UpdateObject<QuestionData>(questionType);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionType;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            //Asocio las preguntas genericas para cada incendio

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("RiversQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("RainsQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);


            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("HowManyTrappedQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("FinesQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            SmartCadDatabase.SaveObject<IncidentTypeData>(incidentType);
            #endregion
        }

    }
}
