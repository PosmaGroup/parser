using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.Model;
using Iesi.Collections;
using SmartCadCore.Common;

namespace SmartCadCore.Core
{
    public partial class SmartCadInitialData
    {
        private static void CreateGenericQuestionEmergency()
        {
            #region ingerido
            QuestionData question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("KindOfDrugsPoisonGasQuestion");
            question.ShortText = ResourceInitialDataLoader.GetString2("Inger");
            question.CustomCode = "INGERIDO";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();
           
            //Esta estructura se maneja de manera generica para todos los incidentes
            QuestionPossibleAnswerData answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Type");
            answer1.ControlName = "TxtBxx1";

            //IncidentTypeProcedure2Data * = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);

            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            #region renderMethod
            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            string renderMethod = @" <XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Tipo' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.28881, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Tipo</property>
      <property name='MinSize'>@2,Width=56@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Tipo</property>
      <property name='TextSize'>@2,Width=20@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>";
#endregion renderMethod
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

           
            #region time
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("OnTouchWithChemicalTime");
            question.ShortText = ResourceInitialDataLoader.GetString2("TimeQuestion");
            question.CustomCode = "TIME";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("TotalTime");
            answer1.ControlName = "TxtBxx2";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);

            
            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();            
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region renderMethod
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Texto libre' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.32118, Culture=neutral, PublicKeyToken=null' name='TxtBxx2' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Texto libre</property>
      <property name='MinSize'>@2,Width=87@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Texto libre</property>
      <property name='TextSize'>@2,Width=51@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>

";
#endregion renderMethod
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion
            

            #region presenta
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("VomiteNauseaQuestion");
            question.ShortText = ResourceInitialDataLoader.GetString2("ShowsQuestion");
            question.CustomCode = "PRESENTA";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Vomite");
            answer1.ControlName = "Chckdt2";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);

            QuestionPossibleAnswerData answer2 = new QuestionPossibleAnswerData();
            answer2.Question = question;
            answer2.Description = ResourceInitialDataLoader.GetString2("Sweat");
            answer2.ControlName = "Chckdt3";
            //IncidentTypeProcedure2Data * = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer2);

            QuestionPossibleAnswerData answer3 = new QuestionPossibleAnswerData();
            answer3.Question = question;
            answer3.Description = ResourceInitialDataLoader.GetString2("Nausea");
            answer3.ControlName = "Chckdt1";
            //IncidentTypeProcedure2Data * = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer3);

            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region renderMethod
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Nauseas' itemName='item2' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt2' />
  <Control itemText='Vomito' itemName='item3' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt3' />
  <Control itemText='Sudoraci�n' itemName='item1' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='4'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=313@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Nauseas</property>
      <property name='MinSize'>@2,Width=89@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Nauseas</property>
      <property name='TextSize'>@2,Width=53@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@3,X=312@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=156@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Vomito</property>
      <property name='MinSize'>@2,Width=89@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Vomito</property>
      <property name='TextSize'>@2,Width=53@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item3</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt3</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item4' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=156@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Sudoraci�n</property>
      <property name='MinSize'>@2,Width=89@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Sudoraci�n</property>
      <property name='TextSize'>@2,Width=53@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@3,X=156@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
";
            #endregion renderMethod
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion
            
            #region reaccion
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("BodyReactionQuestion");
            question.ShortText = ResourceInitialDataLoader.GetString2("ReactionQuestion");
            question.CustomCode = "REACCION";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Reaction");
            answer1.ControlName = "TxtBxx1";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);


            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region  renderMethod
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Reacci�n' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.28881, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Reacci�n</property>
      <property name='MinSize'>@2,Width=79@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Reacci�n</property>
      <property name='TextSize'>@2,Width=43@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
";
#endregion  renderMethod
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region dolor
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("Pain1Question");
            question.ShortText = ResourceInitialDataLoader.GetString2("PainQuestion");
            question.CustomCode = "DOLOR";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Pain");
            answer1.ControlName = "TxtBxx1";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);


            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region  renderMethod
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Reacci�n' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.28881, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Reacci�n</property>
      <property name='MinSize'>@2,Width=79@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Reacci�n</property>
      <property name='TextSize'>@2,Width=43@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
";
            #endregion  renderMethod
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region infarto
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("TimeSinceHeartAttackQuestion");
            question.ShortText = ResourceInitialDataLoader.GetString2("HeartAttackQuestion");
            question.CustomCode = "INFARTO";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("TotalTime");
            answer1.ControlName = "TxtBxx1";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);


            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region  renderMethod
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Reacci�n' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.28881, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Reacci�n</property>
      <property name='MinSize'>@2,Width=79@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Reacci�n</property>
      <property name='TextSize'>@2,Width=43@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
";
            #endregion  renderMethod
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            
            #region conciente
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("PersonConscientQuestion");
            question.ShortText = ResourceInitialDataLoader.GetString2("ConscientQuestion");
            question.CustomCode = "CONCIENTE";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Yes");
            answer1.ControlName = "RdBttn1";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);

            answer2 = new QuestionPossibleAnswerData();
            answer2.Question = question;
            answer2.Description = ResourceInitialDataLoader.GetString2("No");
            answer2.ControlName = "RdBttn2";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer2);

            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region renderMethod
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Si' itemName='item1' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn1' />
  <Control itemText='No' itemName='item2' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn2' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='3'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=312@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Si</property>
      <property name='MinSize'>@2,Width=49@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Si</property>
      <property name='TextSize'>@2,Width=13@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=313@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>No</property>
      <property name='MinSize'>@2,Width=49@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>No</property>
      <property name='TextSize'>@2,Width=13@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@3,X=312@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
";
#endregion renderMethod
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion
           

            #region auxilios
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("KindOfAidQuestion");
            question.ShortText = ResourceInitialDataLoader.GetString2("AidQuestion");
            question.CustomCode = "AUXILIO";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Type");
            answer1.ControlName = "TxtBxx1";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);


            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region  renderMethod
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Reacci�n' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.28881, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Reacci�n</property>
      <property name='MinSize'>@2,Width=79@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Reacci�n</property>
      <property name='TextSize'>@2,Width=43@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
";
            #endregion  renderMethod
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

           
            #region localiza
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("WhereIsTheBleedingQuestion");
            question.ShortText = ResourceInitialDataLoader.GetString2("LocationQuestion");
            question.CustomCode = "LOCALIZA";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Where");
            answer1.ControlName = "TxtBxx2";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);


            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region renderMethod
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Texto libre' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.32118, Culture=neutral, PublicKeyToken=null' name='TxtBxx2' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Texto libre</property>
      <property name='MinSize'>@2,Width=87@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Texto libre</property>
      <property name='TextSize'>@2,Width=51@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>

";
            #endregion renderMethod
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion
            

            #region cortadura
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("CutTypeQuestion");
            question.ShortText = ResourceInitialDataLoader.GetString2("CutQuestion");
            question.CustomCode = "CORTADURA";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Type");
            answer1.ControlName = "TxtBxx1";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);


            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region  renderMethod
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Reacci�n' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.28881, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Reacci�n</property>
      <property name='MinSize'>@2,Width=79@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Reacci�n</property>
      <property name='TextSize'>@2,Width=43@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
";
            #endregion  renderMethod
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region fracturas
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("WhereIsTheFractureQuestionText");
            question.ShortText = ResourceInitialDataLoader.GetString2("WhereIsTheFractureQuestion");
            question.CustomCode = "FRACTURE";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Where");
            answer1.ControlName = "TxtBxx1";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);


            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region  renderMethod
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Reacci�n' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.28881, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Reacci�n</property>
      <property name='MinSize'>@2,Width=79@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Reacci�n</property>
      <property name='TextSize'>@2,Width=43@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
";
            #endregion  renderMethod
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region desplazamiento
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("IsItDisplacementQuesion");
            question.ShortText = ResourceInitialDataLoader.GetString2("DisplacementQuestion");
            question.CustomCode = "DESPLAZAMIENTO";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Yes");
            answer1.ControlName = "RdBttn1";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);

            answer2 = new QuestionPossibleAnswerData();
            answer2.Question = question;
            answer2.Description = ResourceInitialDataLoader.GetString2("No");
            answer2.ControlName = "RdBttn2";
            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer2);

            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region renderMethod
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='No' itemName='item2' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn2' />
  <Control itemText='SI' itemName='item1' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='3'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=313@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>No</property>
      <property name='MinSize'>@2,Width=49@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>No</property>
      <property name='TextSize'>@2,Width=13@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@3,X=312@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=312@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>SI</property>
      <property name='MinSize'>@2,Width=49@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>SI</property>
      <property name='TextSize'>@2,Width=13@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
";
            #endregion renderMethod
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region inmovilizado
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("ParalyzedQuestion");
            question.ShortText = ResourceInitialDataLoader.GetString2("Paralysed");
            question.CustomCode = "INMOVILIZADO";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Yes");
            answer1.ControlName = "RdBttn1";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);

            answer2 = new QuestionPossibleAnswerData();
            answer2.Question = question;
            answer2.Description = ResourceInitialDataLoader.GetString2("No");
            answer2.ControlName = "RdBttn2";
            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer2);

            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region renderMethod
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='No' itemName='item2' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn2' />
  <Control itemText='SI' itemName='item1' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='3'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=313@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>No</property>
      <property name='MinSize'>@2,Width=49@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>No</property>
      <property name='TextSize'>@2,Width=13@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@3,X=312@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=312@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>SI</property>
      <property name='MinSize'>@2,Width=49@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>SI</property>
      <property name='TextSize'>@2,Width=13@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
";
            #endregion renderMethod
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region encuentra
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("WhereIsTheHurtQuestion");
            question.ShortText = ResourceInitialDataLoader.GetString2("IsIt");
            question.CustomCode = "ENCUENTRA";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Yes");
            answer1.ControlName = "TxtBxx1";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);

            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();           
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region renderMethod
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Lugar' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.28881, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Lugar</property>
      <property name='MinSize'>@2,Width=63@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Lugar</property>
      <property name='TextSize'>@2,Width=27@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
";
            #endregion renderMethod
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region urgencia
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("UrgentTypeQuestion");
            question.ShortText = ResourceInitialDataLoader.GetString2("UrgentQuestion");
            question.CustomCode = "URGENCIA";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Type");
            answer1.ControlName = "TxtBxx1";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);


            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region renderMethod
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Tipo de urgencia' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.28881, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Lugar</property>
      <property name='MinSize'>@3,Width=115@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Tipo de urgencia</property>
      <property name='TextSize'>@2,Width=79@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>

";
            #endregion renderMethod
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region hospital
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("WichHospitalQuestion");
            question.ShortText = ResourceInitialDataLoader.GetString2("HospitalQuestion");
            question.CustomCode = "HOSPITAL";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Hospital");
            answer1.ControlName = "TxtBxx1";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);


            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region  renderMethod
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Hospital' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.28881, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Lugar</property>
      <property name='MinSize'>@2,Width=74@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Hospital</property>
      <property name='TextSize'>@2,Width=38@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>

";
            #endregion  renderMethod
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region paciente
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("PatientInfoQuestion");
            question.ShortText = ResourceInitialDataLoader.GetString2("PatientQuestion");
            question.CustomCode = "PACIENTE";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Information");
            answer1.ControlName = "TxtBxx1";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);


            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region renderMethod
            renderMethod = @"
<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Informaci�n' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.28881, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Lugar</property>
      <property name='MinSize'>@2,Width=93@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Informaci�n</property>
      <property name='TextSize'>@2,Width=57@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
";
#endregion renderMehod
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region ataque
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("HasAttackQuestion");
            question.ShortText = ResourceInitialDataLoader.GetString2("AttackQuestion");
            question.CustomCode = "ATAQUE";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Yes");
            answer1.ControlName = "RdBttn2";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);

            answer2 = new QuestionPossibleAnswerData();
            answer2.Question = question;
            answer2.Description = ResourceInitialDataLoader.GetString2("No");
            answer2.ControlName = "RdBttn1";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer2);

            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region renderMethod
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Si' itemName='item2' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn2' />
  <Control itemText='No' itemName='item1' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='3'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=312@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Si</property>
      <property name='MinSize'>@2,Width=49@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Si</property>
      <property name='TextSize'>@2,Width=13@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=313@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>No</property>
      <property name='MinSize'>@2,Width=49@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>No</property>
      <property name='TextSize'>@2,Width=13@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@3,X=312@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
";
#endregion renderMethod
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region armado
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("IsArmedQuestion");
            question.ShortText = ResourceInitialDataLoader.GetString2("ArmedQuestion");
            question.CustomCode = "ARMADO";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Yes");
            answer1.ControlName = "RdBttn2";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);

            answer2 = new QuestionPossibleAnswerData();
            answer2.Question = question;
            answer2.Description = ResourceInitialDataLoader.GetString2("No");
            answer2.ControlName = "RdBttn1";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer2);

            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region renderMethod
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Si' itemName='item2' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn2' />
  <Control itemText='No' itemName='item1' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='3'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=312@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Si</property>
      <property name='MinSize'>@2,Width=49@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Si</property>
      <property name='TextSize'>@2,Width=13@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=313@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>No</property>
      <property name='MinSize'>@2,Width=49@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>No</property>
      <property name='TextSize'>@2,Width=13@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@3,X=312@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
";
            #endregion renderMethod
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region agredirse
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("HurtIttentionsQuestion");
            question.ShortText = ResourceInitialDataLoader.GetString2("HurtHimselfQuestion");
            question.CustomCode = "AGREDIRSE";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Yes");
            answer1.ControlName = "RdBttn2";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);

            answer2 = new QuestionPossibleAnswerData();
            answer2.Question = question;
            answer2.Description = ResourceInitialDataLoader.GetString2("No");
            answer2.ControlName = "RdBttn1";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer2);

            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region renderMethod
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Si' itemName='item2' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn2' />
  <Control itemText='No' itemName='item1' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='3'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=312@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Si</property>
      <property name='MinSize'>@2,Width=49@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Si</property>
      <property name='TextSize'>@2,Width=13@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=313@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>No</property>
      <property name='MinSize'>@2,Width=49@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>No</property>
      <property name='TextSize'>@2,Width=13@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@3,X=312@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
";
            #endregion renderMethod
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region droga
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("ItsOnDrugsQuestion");
            question.ShortText = ResourceInitialDataLoader.GetString2("DrugsQuestion");
            question.CustomCode = "DROGA";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Yes");
            answer1.ControlName = "RdBttn2";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);

            answer2 = new QuestionPossibleAnswerData();
            answer2.Question = question;
            answer2.Description = ResourceInitialDataLoader.GetString2("No");
            answer2.ControlName = "RdBttn1";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer2);

            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region renderMethod
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Si' itemName='item2' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn2' />
  <Control itemText='No' itemName='item1' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='3'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=312@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Si</property>
      <property name='MinSize'>@2,Width=49@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Si</property>
      <property name='TextSize'>@2,Width=13@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=313@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>No</property>
      <property name='MinSize'>@2,Width=49@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>No</property>
      <property name='TextSize'>@2,Width=13@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@3,X=312@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
";
            #endregion renderMethod
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region familiar
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("RelativeNearQuestion");
            question.ShortText = ResourceInitialDataLoader.GetString2("RelativeQuestion");
            question.CustomCode = "FAMILIAR";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Yes");
            answer1.ControlName = "RdBttn2";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);

            answer2 = new QuestionPossibleAnswerData();
            answer2.Question = question;
            answer2.Description = ResourceInitialDataLoader.GetString2("No");
            answer2.ControlName = "RdBttn1";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer2);

            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region renderMethod
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Si' itemName='item2' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn2' />
  <Control itemText='No' itemName='item1' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='3'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=312@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Si</property>
      <property name='MinSize'>@2,Width=49@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Si</property>
      <property name='TextSize'>@2,Width=13@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=313@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>No</property>
      <property name='MinSize'>@2,Width=49@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>No</property>
      <property name='TextSize'>@2,Width=13@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@3,X=312@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
";
            #endregion renderMethod
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion
        }

        private static void CreateEmergencyType()
        {
            //Este metodo crea la carga inicial para todos los tipos de incidentes de emergencia medica
            //Para agregar un tipo de incidente debo realizar los siguientes pasos
            //1.- Crear el incidente.
            //2.- Asociarle los tipos de departamentos que dan soporte a ese incidente
            //3.- Indicar los tipos de procedimientos asociados al incidente
            //4.- Indicar los tipos de preguntas asociadas al incidente con la siguiente estructura
            //4.1.-     Indicar un procedimiento para cada pregunta
            //4.2.-     Indicar las posible respuesta asociadas a la pregunta
            //4.3.-     Indicar la forma en que se renderiza la pregunta en el frontclient indicando los id de las respuestas asociadas

            CreateGenericQuestionEmergency();
            
            #region EmergenciaIntoxicacion
            //Paso 1
            IncidentTypeData incidentType = new IncidentTypeData();
            incidentType.CustomCode = "TOX";
            incidentType.Name = "EmergencyToxic";
            incidentType.FriendlyName = ResourceInitialDataLoader.GetString2("PoisonEmergency");
            incidentType.NoEmergency = false;

            //Paso 2
            //Bomberos
            DepartmentTypeData dpFire = new DepartmentTypeData(DPAMBUMETRO);
            dpFire = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFire);

            //Bomberos (Atenci�n pre-hospitalaria)
            DepartmentTypeData dpFirePre = new DepartmentTypeData(DPFIREPRE);
            dpFirePre = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFirePre);

            incidentType.SetIncidentTypeDepartmentTypes = new HashedSet();

            IncidentTypeDepartmentTypeData itdFire = new IncidentTypeDepartmentTypeData();
            itdFire.DepartmentType = dpFire;
            itdFire.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFire);

            IncidentTypeDepartmentTypeData itdFirePre = new IncidentTypeDepartmentTypeData();
            itdFirePre.DepartmentType = dpFirePre;
            itdFirePre.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFirePre);
            incidentType.Procedure = @"<b>" + ResourceInitialDataLoader.GetString2("ProceduresTo") + incidentType.FriendlyName + @".</b> <ul> <li>" + ResourceInitialDataLoader.GetString2("Step1") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step2") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step3") + @"</li> </ul> ";



            //Paso 4

            //Indico las preguntas genericas para el incidente
            incidentType.SetIncidentTypeQuestions = new HashedSet();

            QuestionData questionWhere = new QuestionData();
            questionWhere.ShortText = STRQUESTIONWHERE;
            questionWhere = SmartCadDatabase.SearchObject<QuestionData>(questionWhere);


            IncidentTypeQuestionData itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionWhere;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            QuestionData questionNumber = new QuestionData();
            questionNumber.ShortText = STRQUESTIONNUMBERPERSON;
            questionNumber = SmartCadDatabase.SearchObject<QuestionData>(questionNumber);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionNumber;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            QuestionData questionState = new QuestionData();
            questionState.ShortText = STRSTATEPERSON;
            questionState = SmartCadDatabase.SearchObject<QuestionData>(questionState);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionState;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            //Preguntas especificas
            QuestionData questionType = new QuestionData();
            questionType.Text = ResourceInitialDataLoader.GetString2("PoisonType");
            questionType.ShortText = ResourceInitialDataLoader.GetString2("PoisonTypeQuestion");
            questionType.CustomCode = incidentType.CustomCode + "TYPE";
            questionType.RequirementType = RequirementTypeEnum.Recommended;
            questionType.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            QuestionPossibleAnswerData answer1 = new QuestionPossibleAnswerData();
            answer1.Question = questionType;
            answer1.Description = ResourceInitialDataLoader.GetString2("Drugs");
            answer1.ControlName = "Chckdt2";
            questionType.SetPossibleAnswers.Add(answer1);

            QuestionPossibleAnswerData answer2 = new QuestionPossibleAnswerData();
            answer2.Question = questionType;
            answer2.Description = ResourceInitialDataLoader.GetString2("Poisoning");
            answer2.ControlName = "Chckdt3";
            questionType.SetPossibleAnswers.Add(answer2);


            QuestionPossibleAnswerData answer3 = new QuestionPossibleAnswerData();
            answer3.Question = questionType;
            answer3.Description = ResourceInitialDataLoader.GetString2("PoisonGas");
            answer3.ControlName = "Chckdt4";
            questionType.SetPossibleAnswers.Add(answer3);

            SmartCadDatabase.SaveObject<QuestionData>(questionType);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region Render
            string renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='F�rmacos' itemName='item2' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt2' />
  <Control itemText='Evenenamiento' itemName='item3' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt3' />
  <Control itemText='Gases t�xicos' itemName='item0' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt4' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='4'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=162@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Atrapados</property>
      <property name='MinSize'>@3,Width=110@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>F�rmacos</property>
      <property name='TextSize'>@2,Width=74@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=231@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Derrumbe de edificaciones</property>
      <property name='MinSize'>@3,Width=110@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Evenenamiento</property>
      <property name='TextSize'>@2,Width=74@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item3</property>
      <property name='Location'>@3,X=162@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt3</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item4' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=232@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>?Sepultadas?</property>
      <property name='MinSize'>@3,Width=110@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Gases t�xicos</property>
      <property name='TextSize'>@2,Width=74@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item0</property>
      <property name='Location'>@3,X=393@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt4</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>"; 
            #endregion

            questionType.RenderMethod = renderMethod;
            SmartCadDatabase.UpdateObject<QuestionData>(questionType);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionType;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            //Asocio las preguntas genericas para cada incendio
            QuestionData question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("Inger");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("TimeQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);


            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("ShowsQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("ReactionQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);
           
            SmartCadDatabase.SaveObject<IncidentTypeData>(incidentType);
            #endregion

            #region EmergenciaInfartoHemorragia
            //Paso 1
            incidentType = new IncidentTypeData();
            incidentType.CustomCode = "HEM";
            incidentType.Name = "EmergencyInfarctHemorrhage";
            incidentType.FriendlyName = ResourceInitialDataLoader.GetString2("HeartAttackAndBleedingEmergency");
            incidentType.NoEmergency = false;

            //Paso 2
            //Bomberos
            dpFire = new DepartmentTypeData(DPAMBUMETRO);
            dpFire = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFire);

            //Bomberos (Atenci�n pre-hospitalaria)
            dpFirePre = new DepartmentTypeData(DPFIREPRE);
            dpFirePre = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFirePre);

            incidentType.SetIncidentTypeDepartmentTypes = new HashedSet();

            itdFire = new IncidentTypeDepartmentTypeData();
            itdFire.DepartmentType = dpFire;
            itdFire.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFire);

            itdFirePre = new IncidentTypeDepartmentTypeData();
            itdFirePre.DepartmentType = dpFirePre;
            itdFirePre.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFirePre);
            incidentType.Procedure = @"<b>" + ResourceInitialDataLoader.GetString2("ProceduresTo") + incidentType.FriendlyName + @".</b> <ul> <li>" + ResourceInitialDataLoader.GetString2("Step1") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step2") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step3") + @"</li> </ul> ";



            //Paso 4

            //Indico las preguntas genericas para el incidente
            incidentType.SetIncidentTypeQuestions = new HashedSet();

            questionWhere = new QuestionData();
            questionWhere.ShortText = STRQUESTIONWHERE;
            questionWhere = SmartCadDatabase.SearchObject<QuestionData>(questionWhere);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionWhere;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            questionNumber = new QuestionData();
            questionNumber.ShortText = STRQUESTIONNUMBERPERSON;
            questionNumber = SmartCadDatabase.SearchObject<QuestionData>(questionNumber);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionNumber;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            questionState = new QuestionData();
            questionState.ShortText = STRSTATEPERSON;
            questionState = SmartCadDatabase.SearchObject<QuestionData>(questionState);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionState;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            //Preguntas especificas
            questionType = new QuestionData();
            questionType.Text = ResourceInitialDataLoader.GetString2("EmergencyTypeOfQuestion");
            questionType.ShortText = ResourceInitialDataLoader.GetString2("EmergencyTypeQuestion");
            questionType.CustomCode = incidentType.CustomCode + "TYPE";
            questionType.RequirementType = RequirementTypeEnum.Recommended;
            questionType.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = questionType;
            answer1.Description = ResourceInitialDataLoader.GetString2("HeartAttack");
            answer1.ControlName = "Chckdt2";
            questionType.SetPossibleAnswers.Add(answer1);

            answer2 = new QuestionPossibleAnswerData();
            answer2.Question = questionType;
            answer2.Description = ResourceInitialDataLoader.GetString2("BleedingType");
            answer2.ControlName = "Chckdt3";
            questionType.SetPossibleAnswers.Add(answer2);

            SmartCadDatabase.SaveObject<QuestionData>(questionType);

            #region renderMethod
            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Infarto' itemName='item2' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt2' />
  <Control itemText='Tipo de hemorragia' itemName='item3' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt3' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='3'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=162@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Atrapados</property>
      <property name='MinSize'>@3,Width=128@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Infarto</property>
      <property name='TextSize'>@2,Width=92@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=463@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Derrumbe de edificaciones</property>
      <property name='MinSize'>@3,Width=128@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Tipo de hemorragia</property>
      <property name='TextSize'>@2,Width=92@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item3</property>
      <property name='Location'>@3,X=162@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt3</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>";

            #endregion

            questionType.RenderMethod = renderMethod;
            SmartCadDatabase.UpdateObject<QuestionData>(questionType);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionType;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            //Asocio las preguntas genericas para cada incendio
            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("PainQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("HeartAttackQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("AidQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("LocationQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("CutQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            SmartCadDatabase.SaveObject<IncidentTypeData>(incidentType);
            #endregion

            #region EmergenciaTraumaFracture
            //Paso 1
            incidentType = new IncidentTypeData();
            incidentType.CustomCode = "FRA";
            incidentType.Name = "EmergencyTraumaFracture";
            incidentType.FriendlyName = ResourceInitialDataLoader.GetString2("TraumaAndFractureEmergency");
            incidentType.NoEmergency = false;

            //Paso 2
            //Bomberos
            dpFire = new DepartmentTypeData(DPAMBUMETRO);
            dpFire = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFire);

            //Bomberos (Atenci�n pre-hospitalaria)
            dpFirePre = new DepartmentTypeData(DPFIREPRE);
            dpFirePre = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFirePre);

            incidentType.SetIncidentTypeDepartmentTypes = new HashedSet();

            itdFire = new IncidentTypeDepartmentTypeData();
            itdFire.DepartmentType = dpFire;
            itdFire.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFire);

            itdFirePre = new IncidentTypeDepartmentTypeData();
            itdFirePre.DepartmentType = dpFirePre;
            itdFirePre.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFirePre);
            incidentType.Procedure = @"<b>" + ResourceInitialDataLoader.GetString2("ProceduresTo") + incidentType.FriendlyName + @".</b> <ul> <li>" + ResourceInitialDataLoader.GetString2("Step1") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step2") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step3") + @"</li> </ul> ";



            //Paso 4

            //Indico las preguntas genericas para el incidente
            incidentType.SetIncidentTypeQuestions = new HashedSet();

            questionWhere = new QuestionData();
            questionWhere.ShortText = STRQUESTIONWHERE;
            questionWhere = SmartCadDatabase.SearchObject<QuestionData>(questionWhere);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionWhere;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            questionNumber = new QuestionData();
            questionNumber.ShortText = STRQUESTIONNUMBERPERSON;
            questionNumber = SmartCadDatabase.SearchObject<QuestionData>(questionNumber);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionNumber;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            questionState = new QuestionData();
            questionState.ShortText = STRSTATEPERSON;
            questionState = SmartCadDatabase.SearchObject<QuestionData>(questionState);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionState;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            //Preguntas especificas
            questionType = new QuestionData();
            questionType.Text = ResourceInitialDataLoader.GetString2("TraumaAndFractureType");
            questionType.ShortText = ResourceInitialDataLoader.GetString2("");
            questionType.CustomCode = incidentType.CustomCode + "TYPE";
            questionType.RequirementType = RequirementTypeEnum.Recommended;
            questionType.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = questionType;
            answer1.Description = ResourceInitialDataLoader.GetString2("Limbs");
            answer1.ControlName = "Chckdt2";
            questionType.SetPossibleAnswers.Add(answer1);

            answer2 = new QuestionPossibleAnswerData();
            answer2.Question = questionType;
            answer2.Description = ResourceInitialDataLoader.GetString2("SpineAndHead");
            answer2.ControlName = "Chckdt3";
            questionType.SetPossibleAnswers.Add(answer2);

            answer3 = new QuestionPossibleAnswerData();
            answer3.Question = questionType;
            answer3.Description = ResourceInitialDataLoader.GetString2("Contusion");
            answer3.ControlName = "Chckdt4";
            questionType.SetPossibleAnswers.Add(answer3);

            QuestionPossibleAnswerData answer4 = new QuestionPossibleAnswerData();
            answer4.Question = questionType;
            answer4.Description = ResourceInitialDataLoader.GetString2("BlackOut");
            answer4.ControlName = "Chckdt5";
            questionType.SetPossibleAnswers.Add(answer4);

            SmartCadDatabase.SaveObject<QuestionData>(questionType);

			#region RenderMethod
			//Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Extremidades' itemName='item2' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt2' />
  <Control itemText='Columna y cabeza' itemName='item3' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt3' />
  <Control itemText='�Contusi�n?' itemName='item0' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt4' />
  <Control itemText='P?rdida de conciencia' itemName='item1' type='DevExpress.XtraEditors.CheckEdit, DevExpress.XtraEditors.v8.3, Version=8.3.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a' name='Chckdt5' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='5'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=162@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Atrapados</property>
      <property name='MinSize'>@3,Width=138@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Extremidades</property>
      <property name='TextSize'>@3,Width=102@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=185@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Derrumbe de edificaciones</property>
      <property name='MinSize'>@3,Width=138@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Columna y cabeza</property>
      <property name='TextSize'>@3,Width=102@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item3</property>
      <property name='Location'>@3,X=162@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt3</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item4' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=139@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>?Contusi?n?</property>
      <property name='MinSize'>@3,Width=138@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>?Contusi?n?</property>
      <property name='TextSize'>@3,Width=102@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item0</property>
      <property name='Location'>@3,X=347@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt4</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item5' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=139@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>P?rdida de conciencia</property>
      <property name='MinSize'>@3,Width=138@2,Height=29</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>P?rdida de conciencia</property>
      <property name='TextSize'>@3,Width=102@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@3,X=486@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=29</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>Chckdt5</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>";
			#endregion

			questionType.RenderMethod = renderMethod;
            SmartCadDatabase.UpdateObject<QuestionData>(questionType);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionType;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            //Asocio las preguntas genericas para cada incendio
            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("FractureQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("DisplacementQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("Paralysed");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("IsIt");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);
           
            SmartCadDatabase.SaveObject<IncidentTypeData>(incidentType);
            #endregion

            #region EmergenciaTraslado
            //Paso 1
            incidentType = new IncidentTypeData();
            incidentType.CustomCode = "ETR";
            incidentType.Name = "EmergencyTransfer";
            incidentType.FriendlyName = ResourceInitialDataLoader.GetString2("InterhospitalTransferEmergency");
            incidentType.NoEmergency = false;

            //Paso 2
            //Bomberos
            dpFire = new DepartmentTypeData(DPAMBUMETRO);
            dpFire = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFire);

            //Bomberos (Atenci�n pre-hospitalaria)
            dpFirePre = new DepartmentTypeData(DPFIREPRE);
            dpFirePre = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFirePre);

            incidentType.SetIncidentTypeDepartmentTypes = new HashedSet();

            itdFire = new IncidentTypeDepartmentTypeData();
            itdFire.DepartmentType = dpFire;
            itdFire.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFire);

            itdFirePre = new IncidentTypeDepartmentTypeData();
            itdFirePre.DepartmentType = dpFirePre;
            itdFirePre.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFirePre);
            incidentType.Procedure = @"<b>" + ResourceInitialDataLoader.GetString2("ProceduresTo") + incidentType.FriendlyName + @".</b> <ul> <li>" + ResourceInitialDataLoader.GetString2("Step1") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step2") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step3") + @"</li> </ul> ";



            //Paso 4

            //Indico las preguntas genericas para el incidente
            incidentType.SetIncidentTypeQuestions = new HashedSet();

            questionWhere = new QuestionData();
            questionWhere.ShortText = STRQUESTIONWHERE;
            questionWhere = SmartCadDatabase.SearchObject<QuestionData>(questionWhere);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionWhere;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            questionNumber = new QuestionData();
            questionNumber.ShortText = STRQUESTIONNUMBERPERSON;
            questionNumber = SmartCadDatabase.SearchObject<QuestionData>(questionNumber);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionNumber;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            questionState = new QuestionData();
            questionState.ShortText = STRSTATEPERSON;
            questionState = SmartCadDatabase.SearchObject<QuestionData>(questionState);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionState;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            //Preguntas especificas
            questionType = new QuestionData();
            questionType.Text = ResourceInitialDataLoader.GetString2("TransferType");
            questionType.ShortText = ResourceInitialDataLoader.GetString2("TransferTypeQuestion");
            questionType.CustomCode = incidentType.CustomCode + "TYPE";
            questionType.RequirementType = RequirementTypeEnum.Recommended;
            questionType.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = questionType;
            answer1.Description = ResourceInitialDataLoader.GetString2("Urgent");
            answer1.ControlName = "RdBttn1";
            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            questionType.SetPossibleAnswers.Add(answer1);

            answer2 = new QuestionPossibleAnswerData();
            answer2.Question = questionType;
            answer2.Description = ResourceInitialDataLoader.GetString2("ProgramTransfer");
            answer2.ControlName = "RdBttn2";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            questionType.SetPossibleAnswers.Add(answer2);


            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(questionType);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region renderMethod
            renderMethod = @"
           <XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Si' itemName='item1' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn1' />
  <Control itemText='No' itemName='item2' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn2' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='3'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=312@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Si</property>
      <property name='MinSize'>@2,Width=49@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Si</property>
      <property name='TextSize'>@2,Width=13@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=313@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>No</property>
      <property name='MinSize'>@2,Width=49@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>No</property>
      <property name='TextSize'>@2,Width=13@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@3,X=312@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
            ";
#endregion renderMethod

            questionType.RenderMethod = renderMethod;
            SmartCadDatabase.UpdateObject<QuestionData>(questionType);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionType;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            //Asocio las preguntas genericas para cada incendio
            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("UrgentQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("HospitalQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);


            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("PatientQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);
            
            SmartCadDatabase.SaveObject<IncidentTypeData>(incidentType);
            #endregion

            #region EmergenciaDemencia
            //Paso 1
            incidentType = new IncidentTypeData();
            incidentType.CustomCode = "DEM";
            incidentType.Name = "EmergencyDemency";
            incidentType.FriendlyName = ResourceInitialDataLoader.GetString2("DisturbedEmergency");
            incidentType.NoEmergency = false;

            //Paso 2
            //Bomberos
            dpFire = new DepartmentTypeData(DPAMBUMETRO);
            dpFire = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFire);

            //Bomberos (Atenci�n pre-hospitalaria)
            dpFirePre = new DepartmentTypeData(DPFIREPRE);
            dpFirePre = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFirePre);

            incidentType.SetIncidentTypeDepartmentTypes = new HashedSet();

            itdFire = new IncidentTypeDepartmentTypeData();
            itdFire.DepartmentType = dpFire;
            itdFire.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFire);

            itdFirePre = new IncidentTypeDepartmentTypeData();
            itdFirePre.DepartmentType = dpFirePre;
            itdFirePre.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFirePre);
            incidentType.Procedure = @"<b>" + ResourceInitialDataLoader.GetString2("ProceduresTo") + incidentType.FriendlyName + @".</b> <ul> <li>" + ResourceInitialDataLoader.GetString2("Step1") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step2") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step3") + @"</li> </ul> ";



            //Paso 4

            //Indico las preguntas genericas para el incidente
            incidentType.SetIncidentTypeQuestions = new HashedSet();

            questionWhere = new QuestionData();
            questionWhere.ShortText = STRQUESTIONWHERE;
            questionWhere = SmartCadDatabase.SearchObject<QuestionData>(questionWhere);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionWhere;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            questionNumber = new QuestionData();
            questionNumber.ShortText = STRQUESTIONNUMBERPERSON;
            questionNumber = SmartCadDatabase.SearchObject<QuestionData>(questionNumber);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionNumber;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            questionState = new QuestionData();
            questionState.ShortText = STRSTATEPERSON;
            questionState = SmartCadDatabase.SearchObject<QuestionData>(questionState);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionState;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            //Preguntas especificas
            questionType = new QuestionData();
            questionType.Text = ResourceInitialDataLoader.GetString2("WhatKindOfEmergency");
            questionType.ShortText = ResourceInitialDataLoader.GetString2("EmergencyType1Question");
            questionType.CustomCode = incidentType.CustomCode + "TYPE";
            questionType.RequirementType = RequirementTypeEnum.Recommended;
            questionType.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = questionType;
            answer1.Description = ResourceInitialDataLoader.GetString2("DangerWithOthers");
            answer1.ControlName = "RdBttn1";
            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            questionType.SetPossibleAnswers.Add(answer1);

            answer2 = new QuestionPossibleAnswerData();
            answer2.Question = questionType;
            answer2.Description = ResourceInitialDataLoader.GetString2("DirectDanger");
            answer2.ControlName = "RdBttn2";
            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            questionType.SetPossibleAnswers.Add(answer2);


            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(questionType);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region renderMethod
            renderMethod = @"
           <XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Si' itemName='item1' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn1' />
  <Control itemText='No' itemName='item2' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn2' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='3'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=312@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Si</property>
      <property name='MinSize'>@2,Width=49@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Si</property>
      <property name='TextSize'>@2,Width=13@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=313@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>No</property>
      <property name='MinSize'>@2,Width=49@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>No</property>
      <property name='TextSize'>@2,Width=13@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@3,X=312@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
            ";
            #endregion renderMethod

            questionType.RenderMethod = renderMethod;
            SmartCadDatabase.UpdateObject<QuestionData>(questionType);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionType;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            //Asocio las preguntas genericas para cada incendio
            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("AttackQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("ArmedQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);


            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("HurtHimselfQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("DrugsQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("RelativeQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            SmartCadDatabase.SaveObject<IncidentTypeData>(incidentType);
            #endregion
        }

        private static void CreateGenericQuestionRescueAnimal()
        {
            #region animal
            QuestionData question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("AnimalInjuriesQuestion");
            question.ShortText = ResourceInitialDataLoader.GetString2("AnimalQuestion");
            question.CustomCode = "ANIMAL";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            QuestionPossibleAnswerData answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Type");
            answer1.ControlName = "TxtBxx2";

            //IncidentTypeProcedure2Data * = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);

            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento

            #region renderMethod
            string renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Tipo' itemName='item2' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.32118, Culture=neutral, PublicKeyToken=null' name='TxtBxx2' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Tipo</property>
      <property name='MinSize'>@2,Width=56@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Tipo</property>
      <property name='TextSize'>@2,Width=20@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
";
            #endregion renderMethod
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region conciente
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("OwnerThereQuestion");
            question.ShortText = ResourceInitialDataLoader.GetString2("Owner");
            question.CustomCode = "OWNER";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Yes");
            answer1.ControlName = "RdBttn2";

            question.SetPossibleAnswers.Add(answer1);

            QuestionPossibleAnswerData answer2 = new QuestionPossibleAnswerData();
            answer2.Question = question;
            answer2.Description = ResourceInitialDataLoader.GetString2("No");
            answer2.ControlName = "RdBttn1";
            question.SetPossibleAnswers.Add(answer2);

            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region  renderMethod
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='No' itemName='item1' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn1' />
  <Control itemText='Si' itemName='item2' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn2' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='3'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=313@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>No</property>
      <property name='MinSize'>@2,Width=49@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>No</property>
      <property name='TextSize'>@2,Width=13@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@3,X=312@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=312@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Si</property>
      <property name='MinSize'>@2,Width=49@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Si</property>
      <property name='TextSize'>@2,Width=13@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
";

            #endregion  renderMethod
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region reconoce
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("OwnerKnowsIt");
            question.ShortText = ResourceInitialDataLoader.GetString2("Knows");
            question.CustomCode = "KNOWS";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Yes");
            answer1.ControlName = "RdBttn2";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);

            answer2 = new QuestionPossibleAnswerData();
            answer2.Question = question;
            answer2.Description = ResourceInitialDataLoader.GetString2("No");
            answer2.ControlName = "RdBttn1";
            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer2);

            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region  renderMethod
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='No' itemName='item1' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn1' />
  <Control itemText='Si' itemName='item2' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn2' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='3'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=313@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>No</property>
      <property name='MinSize'>@2,Width=49@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>No</property>
      <property name='TextSize'>@2,Width=13@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@3,X=312@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=312@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Si</property>
      <property name='MinSize'>@2,Width=49@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Si</property>
      <property name='TextSize'>@2,Width=13@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
";

            #endregion  renderMethod
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region agresivo
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("AgressiveAnimalQuestion");
            question.ShortText = ResourceInitialDataLoader.GetString2("AgressiveQuestion");
            question.CustomCode = "AGRESSIVE";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Yes");
            answer1.ControlName = "RdBttn2";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);

            answer2 = new QuestionPossibleAnswerData();
            answer2.Question = question;
            answer2.Description = ResourceInitialDataLoader.GetString2("No");
            answer2.ControlName = "RdBttn1";
            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer2);

            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region  renderMethod
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='No' itemName='item1' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn1' />
  <Control itemText='Si' itemName='item2' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn2' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='3'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=313@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>No</property>
      <property name='MinSize'>@2,Width=49@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>No</property>
      <property name='TextSize'>@2,Width=13@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@3,X=312@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=312@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Si</property>
      <property name='MinSize'>@2,Width=49@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Si</property>
      <property name='TextSize'>@2,Width=13@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
";

            #endregion  renderMethod
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion

            #region enfermedad
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("AnimalWithIllness");
            question.ShortText = ResourceInitialDataLoader.GetString2("Illness");
            question.CustomCode = "ILLANESS";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Yes");
            answer1.ControlName = "RdBttn2";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);

            answer2 = new QuestionPossibleAnswerData();
            answer2.Question = question;
            answer2.Description = ResourceInitialDataLoader.GetString2("No");
            answer2.ControlName = "RdBttn1";
            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer2);

            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region  renderMethod
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='No' itemName='item1' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn1' />
  <Control itemText='Si' itemName='item2' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn2' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='3'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=313@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>No</property>
      <property name='MinSize'>@2,Width=49@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>No</property>
      <property name='TextSize'>@2,Width=13@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@3,X=312@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=312@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Si</property>
      <property name='MinSize'>@2,Width=49@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Si</property>
      <property name='TextSize'>@2,Width=13@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
";

            #endregion  renderMethod
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion
            
            #region herida
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("PersonHurtQuestion");
            question.ShortText = ResourceInitialDataLoader.GetString2("HurtQuestion");
            question.CustomCode = "HURT";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Yes");
            answer1.ControlName = "RdBttn1";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);

            answer2 = new QuestionPossibleAnswerData();
            answer2.Question = question;
            answer2.Description = ResourceInitialDataLoader.GetString2("No");
            answer2.ControlName = "RdBttn2";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer2);

            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region renderMethod
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Si' itemName='item1' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn1' />
  <Control itemText='No' itemName='item2' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn2' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='3'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=312@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Si</property>
      <property name='MinSize'>@2,Width=49@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Si</property>
      <property name='TextSize'>@2,Width=13@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=313@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>No</property>
      <property name='MinSize'>@2,Width=49@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>No</property>
      <property name='TextSize'>@2,Width=13@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@3,X=312@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
";
            #endregion renderMethod
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion           

            #region hospital
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("PeopleNamesQuestion");
            question.ShortText = ResourceInitialDataLoader.GetString2("NamesQuestion");
            question.CustomCode = "NAMES";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("SingleOrNotNames");
            answer1.ControlName = "TxtBxx1";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);


            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region renderMethod
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Nombre' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.32118, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Nombre</property>
      <property name='MinSize'>@2,Width=73@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Nombre</property>
      <property name='TextSize'>@2,Width=37@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx1</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>

";
#endregion renderMethod
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion
            
            #region ropa
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("ClothWereUsing");
            question.ShortText = ResourceInitialDataLoader.GetString2("ClothQuestion");
            question.CustomCode = "CLOTH";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Clothes");
            answer1.ControlName = "TxtBxx2";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);


            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region renderMethod
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Texto libre' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.32118, Culture=neutral, PublicKeyToken=null' name='TxtBxx2' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Texto libre</property>
      <property name='MinSize'>@2,Width=87@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Texto libre</property>
      <property name='TextSize'>@2,Width=51@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>

";
            #endregion renderMethod
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion
                        
            #region vehiculo
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("CarTypeAndPlaceTrappedQuestion");
            question.ShortText = ResourceInitialDataLoader.GetString2("CarTypeQuestion");
            question.CustomCode = "VEHICLE";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("KidnappingVehicle");
            answer1.ControlName = "TxtBxx2";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);

            answer2 = new QuestionPossibleAnswerData();
            answer2.Question = question;
            answer2.Description = ResourceInitialDataLoader.GetString2("Place");
            answer2.ControlName = "TxtBxx3";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer2);

            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);
            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region renderMethod
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Texto libre' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.32118, Culture=neutral, PublicKeyToken=null' name='TxtBxx2' />
  <Control itemText='Texto libre' itemName='item2' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.32118, Culture=neutral, PublicKeyToken=null' name='TxtBxx3' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='3'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=31</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Texto libre</property>
      <property name='MinSize'>@2,Width=87@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Texto libre</property>
      <property name='TextSize'>@2,Width=51@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=68</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Texto libre</property>
      <property name='MinSize'>@2,Width=87@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Texto libre</property>
      <property name='TextSize'>@2,Width=51@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item2</property>
      <property name='Location'>@1,X=0@2,Y=31</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx3</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>

";
#endregion renderMethod
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion
                        
            #region victima
            question = new QuestionData();
            question.Text = ResourceInitialDataLoader.GetString2("VictimLocationQuestion");
            question.ShortText = ResourceInitialDataLoader.GetString2("VictimPlaceQuestion");
            question.CustomCode = "VICTIM";
            question.RequirementType = RequirementTypeEnum.None;
            question.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Place");
            answer1.ControlName = "TxtBxx2";

            //proc* = new IncidentTypeProcedure2Data();
            //proc*.PossibleAnswer = answer*;
            //proc*.Items = new ArrayList();

//* item* = new IncidentTypeProcedureItemData();
            //item*.Description = *
            //item*.Procedure = proc*;
            //proc*.Items.Add(item*);

            question.SetPossibleAnswers.Add(answer1);


            //SmartCadDatabase.SaveObject<IncidentTypeProcedure2Data>();
            SmartCadDatabase.SaveObject<QuestionData>(question);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region renderMethod
            renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Texto libre' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.32118, Culture=neutral, PublicKeyToken=null' name='TxtBxx2' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Texto libre</property>
      <property name='MinSize'>@2,Width=87@2,Height=31</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Texto libre</property>
      <property name='TextSize'>@2,Width=51@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=31</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>TxtBxx2</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>

";
            #endregion renderMethod
            question.RenderMethod = renderMethod;

            SmartCadDatabase.UpdateObject<QuestionData>(question);
            #endregion
           
        }

        private static void CreateRescueAnimalType()
        {
            //Este metodo crea la carga inicial para todos los tipos de incidentes de rescate de animales
            //Para agregar un tipo de incidente debo realizar los siguientes pasos
            //1.- Crear el incidente.
            //2.- Asociarle los tipos de departamentos que dan soporte a ese incidente
            //3.- Indicar los tipos de procedimientos asociados al incidente
            //4.- Indicar los tipos de preguntas asociadas al incidente con la siguiente estructura
            //4.1.-     Indicar un procedimiento para cada pregunta
            //4.2.-     Indicar las posible respuesta asociadas a la pregunta
            //4.3.-     Indicar la forma en que se renderiza la pregunta en el frontclient indicando los id de las respuestas asociadas

            CreateGenericQuestionRescueAnimal();

            #region RescueAnimal
            //Paso 1
            IncidentTypeData incidentType = new IncidentTypeData();
            incidentType.CustomCode = "RCA";
            incidentType.Name = "ANIMALRESCUE";
            incidentType.FriendlyName = ResourceInitialDataLoader.GetString2("AnimalRescue");
            incidentType.NoEmergency = false;

            //Paso 2
            //Bomberos (Rescate)
            DepartmentTypeData dpFire = new DepartmentTypeData(DPFIRERESCUE);
            dpFire = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFire);

            //Bomberos (Atenci�n pre-hospitalaria)
            DepartmentTypeData dpFirePre = new DepartmentTypeData(DPFIREPRE);
            dpFirePre = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFirePre);

            //Proteccion civil
            DepartmentTypeData dpProCivil = new DepartmentTypeData(DPPRECIVIL);
            dpProCivil = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpProCivil);

            incidentType.SetIncidentTypeDepartmentTypes = new HashedSet();

            IncidentTypeDepartmentTypeData itdFire = new IncidentTypeDepartmentTypeData();
            itdFire.DepartmentType = dpFire;
            itdFire.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFire);

            IncidentTypeDepartmentTypeData itdFirePre = new IncidentTypeDepartmentTypeData();
            itdFirePre.DepartmentType = dpFirePre;
            itdFirePre.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFirePre);

            IncidentTypeDepartmentTypeData itdProCivil = new IncidentTypeDepartmentTypeData();
            itdProCivil.DepartmentType = dpProCivil;
            itdProCivil.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdProCivil);

            incidentType.Procedure = @"<b>" + ResourceInitialDataLoader.GetString2("ProceduresTo") + incidentType.FriendlyName + @".</b> <ul> <li>" + ResourceInitialDataLoader.GetString2("Step1") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step2") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step3") + @"</li> </ul> ";



            //Paso 4

            //Indico las preguntas genericas para el incidente
            incidentType.SetIncidentTypeQuestions = new HashedSet();

            QuestionData questionWhere = new QuestionData();
            questionWhere.ShortText = STRQUESTIONWHERE;
            questionWhere = SmartCadDatabase.SearchObject<QuestionData>(questionWhere);


            IncidentTypeQuestionData itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionWhere;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            QuestionData questionNumber = new QuestionData();
            questionNumber.ShortText = STRQUESTIONNUMBERPERSON;
            questionNumber = SmartCadDatabase.SearchObject<QuestionData>(questionNumber);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionNumber;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);
            

            //Preguntas especificas
            QuestionData questionType = new QuestionData();
            questionType.Text = ResourceInitialDataLoader.GetString2("AnimalTypeOf");
            questionType.ShortText = ResourceInitialDataLoader.GetString2("AnimalTypeQuestion");
            questionType.CustomCode = incidentType.CustomCode + "TYPE";
            questionType.RequirementType = RequirementTypeEnum.Recommended;
            questionType.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            QuestionPossibleAnswerData answer1 = new QuestionPossibleAnswerData();
            answer1.Question = questionType;
            answer1.Description = ResourceInitialDataLoader.GetString2("DangerousAnimal");
            answer1.ControlName = "RdBttn8";

            questionType.SetPossibleAnswers.Add(answer1);

            QuestionPossibleAnswerData answer2 = new QuestionPossibleAnswerData();
            answer2.Question = questionType;
            answer2.Description = ResourceInitialDataLoader.GetString2("DomesticAnimal");
            answer2.ControlName = "RdBttn9";
            questionType.SetPossibleAnswers.Add(answer2);


            QuestionPossibleAnswerData answer3 = new QuestionPossibleAnswerData();
            answer3.Question = questionType;
            answer3.Description = ResourceInitialDataLoader.GetString2("ExoticAnimals");
            answer3.ControlName = "RdBttn10";
            questionType.SetPossibleAnswers.Add(answer3);

            QuestionPossibleAnswerData answer4 = new QuestionPossibleAnswerData();
            answer4.Question = questionType;
            answer4.Description = ResourceInitialDataLoader.GetString2("Bees");
            answer4.ControlName = "RdBttn11";
            questionType.SetPossibleAnswers.Add(answer4);

            SmartCadDatabase.SaveObject<QuestionData>(questionType);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region renderMethod
            string renderMethod = @"
           <XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Seleccion simple' itemName='item6' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn8' />
  <Control itemText='Seleccion simple' itemName='item7' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn9' />
  <Control itemText='Seleccion simple' itemName='item8' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn10' />
  <Control itemText='Seleccion simple' itemName='item9' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn11' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='5'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=156@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Seleccion simple</property>
      <property name='MinSize'>@3,Width=112@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Seleccion simple</property>
      <property name='TextSize'>@2,Width=76@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item6</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn8</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=156@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Seleccion simple</property>
      <property name='MinSize'>@3,Width=112@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Seleccion simple</property>
      <property name='TextSize'>@2,Width=76@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item7</property>
      <property name='Location'>@3,X=312@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn9</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item4' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=156@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Seleccion simple</property>
      <property name='MinSize'>@3,Width=112@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Seleccion simple</property>
      <property name='TextSize'>@2,Width=76@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item8</property>
      <property name='Location'>@3,X=156@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn10</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item5' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=157@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Seleccion simple</property>
      <property name='MinSize'>@3,Width=112@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Seleccion simple</property>
      <property name='TextSize'>@2,Width=76@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item9</property>
      <property name='Location'>@3,X=468@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn11</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
            ";
            #endregion renderMethod
            questionType.RenderMethod = renderMethod;
            SmartCadDatabase.UpdateObject<QuestionData>(questionType);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionType;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            //Asocio las preguntas genericas para cada incendio
            QuestionData question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("AnimalQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("Owner");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("AgressiveQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);
            
            SmartCadDatabase.SaveObject<IncidentTypeData>(incidentType);
            #endregion

            #region RescuePerson
            //Paso 1
            incidentType = new IncidentTypeData();
            incidentType.CustomCode = "REP";
            incidentType.Name = "RESCUE";
            incidentType.FriendlyName = ResourceInitialDataLoader.GetString2("RescuePeople");
            incidentType.NoEmergency = false;

            //Paso 2
            //Bomberos (Rescate)
            dpFire = new DepartmentTypeData(DPFIRERESCUE);
            dpFire = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFire);

            //Bomberos (Atenci�n pre-hospitalaria)
            dpFirePre = new DepartmentTypeData(DPFIREPRE);
            dpFirePre = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFirePre);

            //Proteccion civil
            dpProCivil = new DepartmentTypeData(DPPRECIVIL);
            dpProCivil = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpProCivil);

            incidentType.SetIncidentTypeDepartmentTypes = new HashedSet();

            itdFire = new IncidentTypeDepartmentTypeData();
            itdFire.DepartmentType = dpFire;
            itdFire.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFire);

            itdFirePre = new IncidentTypeDepartmentTypeData();
            itdFirePre.DepartmentType = dpFirePre;
            itdFirePre.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdFirePre);

            itdProCivil = new IncidentTypeDepartmentTypeData();
            itdProCivil.DepartmentType = dpProCivil;
            itdProCivil.IncidentType = incidentType;
            incidentType.SetIncidentTypeDepartmentTypes.Add(itdProCivil);

            incidentType.Procedure = @"<b>" + ResourceInitialDataLoader.GetString2("ProceduresTo") + incidentType.FriendlyName + @".</b> <ul> <li>" + ResourceInitialDataLoader.GetString2("Step1") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step2") + @"</li>  <li>" + ResourceInitialDataLoader.GetString2("Step3") + @"</li> </ul> ";



            //Paso 4

            //Indico las preguntas genericas para el incidente
            incidentType.SetIncidentTypeQuestions = new HashedSet();

            questionWhere = new QuestionData();
            questionWhere.ShortText = STRQUESTIONWHERE;
            questionWhere = SmartCadDatabase.SearchObject<QuestionData>(questionWhere);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionWhere;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            questionNumber = new QuestionData();
            questionNumber.ShortText = STRQUESTIONNUMBERPERSON;
            questionNumber = SmartCadDatabase.SearchObject<QuestionData>(questionNumber);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionNumber;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);


            //Preguntas especificas
            questionType = new QuestionData();
            questionType.Text = ResourceInitialDataLoader.GetString2("RescueType");
            questionType.ShortText = ResourceInitialDataLoader.GetString2("RescueTypeQuestion");
            questionType.CustomCode = incidentType.CustomCode + "TYPE";
            questionType.RequirementType = RequirementTypeEnum.Recommended;
            questionType.SetPossibleAnswers = new HashedSet();

            //Esta estructura se maneja de manera generica para todos los incidentes
            answer1 = new QuestionPossibleAnswerData();
            answer1.Question = questionType;
            answer1.Description = ResourceInitialDataLoader.GetString2("SubacuaticQuestion");
            answer1.ControlName = "RdBttn8";
            questionType.SetPossibleAnswers.Add(answer1);

            answer2 = new QuestionPossibleAnswerData();
            answer2.Question = questionType;
            answer2.Description = ResourceInitialDataLoader.GetString2("HighMountain");
            answer2.ControlName = "RdBttn9";
            questionType.SetPossibleAnswers.Add(answer2);


            answer3 = new QuestionPossibleAnswerData();
            answer3.Question = questionType;
            answer3.Description = ResourceInitialDataLoader.GetString2("ConfinedPlaces");
            answer3.ControlName = "RdBttn10";
            questionType.SetPossibleAnswers.Add(answer3);

            answer4 = new QuestionPossibleAnswerData();
            answer4.Question = questionType;
            answer4.Description = ResourceInitialDataLoader.GetString2("Vehicles");
            answer4.ControlName = "RdBttn11";
            questionType.SetPossibleAnswers.Add(answer4);

            QuestionPossibleAnswerData answer5 = new QuestionPossibleAnswerData();
            answer5.Question = questionType;
            answer5.Description = ResourceInitialDataLoader.GetString2("NotEasyAccesPlaces");
            answer5.ControlName = "RdBttn5";
            questionType.SetPossibleAnswers.Add(answer5);

            SmartCadDatabase.SaveObject<QuestionData>(questionType);

            //Ya alamacenado actualizo el render de la opcion con los id ya asociados al procedimiento
            #region renderMethod
            renderMethod = @"
            <XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Seleccion simple' itemName='item6' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn8' />
  <Control itemText='Seleccion simple' itemName='item7' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn9' />
  <Control itemText='Seleccion simple' itemName='item8' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn10' />
  <Control itemText='Seleccion simple' itemName='item9' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn11' />
  <Control itemText='Seleccion simple' itemName='item1' type='System.Windows.Forms.RadioButton, System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089' name='RdBttn5' />
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='6'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=173@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Seleccion simple</property>
      <property name='MinSize'>@3,Width=112@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Seleccion simple</property>
      <property name='TextSize'>@2,Width=76@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item6</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn8</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item3' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=112@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Seleccion simple</property>
      <property name='MinSize'>@3,Width=112@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Seleccion simple</property>
      <property name='TextSize'>@2,Width=76@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item7</property>
      <property name='Location'>@3,X=289@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn9</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item4' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=116@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Seleccion simple</property>
      <property name='MinSize'>@3,Width=112@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Seleccion simple</property>
      <property name='TextSize'>@2,Width=76@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item8</property>
      <property name='Location'>@3,X=173@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn10</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item5' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=112@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Seleccion simple</property>
      <property name='MinSize'>@3,Width=112@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Seleccion simple</property>
      <property name='TextSize'>@2,Width=76@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item9</property>
      <property name='Location'>@3,X=401@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn11</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
    <property name='Item6' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=112@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Seleccion simple</property>
      <property name='MinSize'>@3,Width=112@2,Height=36</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Seleccion simple</property>
      <property name='TextSize'>@2,Width=76@2,Height=20</property>
      <property name='TextVisible'>true</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>5</property>
      <property name='Name'>item1</property>
      <property name='Location'>@3,X=513@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@2,Height=36</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Default</property>
      <property name='ControlName'>RdBttn5</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
            ";
#endregion renderMethod
            questionType.RenderMethod = renderMethod;
            SmartCadDatabase.UpdateObject<QuestionData>(questionType);

            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = questionType;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            //Asocio las preguntas genericas para cada incendio
            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("HurtQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("NamesQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("ClothQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("Vehicle");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("LocationQuestion");
            question = SmartCadDatabase.SearchObject<QuestionData>(question);


            itqWhere = new IncidentTypeQuestionData();
            itqWhere.IncidentType = incidentType;
            itqWhere.Question = question;
            incidentType.SetIncidentTypeQuestions.Add(itqWhere);

            SmartCadDatabase.SaveObject<IncidentTypeData>(incidentType);
            #endregion
        }
    }
}
