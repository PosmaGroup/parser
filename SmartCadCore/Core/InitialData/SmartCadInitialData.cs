using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Drawing;
using System.IO;

using SmartCadCore.Model;
using System.Reflection;
using NHibernate;
using Iesi.Collections;
using SmartCadCore.Common;
using SmartCadCore.ClientData;
using System.Threading;
using System.Globalization;
using SmartCadCore.Model.Util;

namespace SmartCadCore.Core
{
    public partial class SmartCadInitialData
    {
        //Names of deparmentType
        public static string DPFIRE = "";
        public static string DPFIREPRE = "";
        public static string DPFIRERESCUE = "";
        public static string DPTRANS = "";
        public static string DPAMBUMETRO = "";
        public static string DPPOLICE = "";
        public static string DPPRECIVIL = "";
        public static string DPCICPC = "";

        public static string STRQUESTIONWHERE = "";
        public static string STRQUESTIONNUMBERPERSON = "";
        public static string STRSTATEPERSON = "";

        private static List<UserActionData> allActions;
        private static List<UserActionData> mapActions;


        public static void CreateInitialData(ChangeTaskDelegate changeTaskFunction, ChangeValueDelegate changeValueFunction)
        {
            CultureInfo ci = new CultureInfo(
                SmartCadConfiguration.SmartCadSection.CultureElement.CultureLanguageElement.Name + "-" +
                SmartCadConfiguration.SmartCadSection.CultureElement.CultureCountryElement.Name);

            ResourceLoader.UpdatedLanguage(ci);
            ResourceInitialDataLoader.UpdatedLanguage(ci);
            

            #region Initialization

            STRQUESTIONWHERE = ResourceInitialDataLoader.GetString2("STRQUESTIONWHERE");
            STRQUESTIONNUMBERPERSON = ResourceInitialDataLoader.GetString2("STRQUESTIONNUMBERPERSON");
            STRSTATEPERSON = ResourceInitialDataLoader.GetString2("STRSTATEPERSON");

            //Names of deparmentType
        	DPFIRE =  ResourceInitialDataLoader.GetString2("DPFIRE");
        	DPFIREPRE = ResourceInitialDataLoader.GetString2("DPFIREPRE");
        	DPFIRERESCUE = ResourceInitialDataLoader.GetString2("DPFIRERESCUE");
        	DPTRANS = ResourceInitialDataLoader.GetString2("DPTRANS");
        	DPAMBUMETRO = ResourceInitialDataLoader.GetString2("DPAMBUMETRO");
        	DPPOLICE = ResourceInitialDataLoader.GetString2("DPPOLICE");
        	DPPRECIVIL = ResourceInitialDataLoader.GetString2("DPPRECIVIL");
        	DPCICPC = ResourceInitialDataLoader.GetString2("CICPC");

            #endregion


            #region Dummy

            DummyData dummy = new DummyData();
            dummy.Name = "Dummy 1";
            SmartCadDatabase.SaveObject<DummyData>(dummy);

            #endregion

            #region Actions

            changeValueFunction(5);
            changeTaskFunction(ResourceInitialDataLoader.GetString2("CreatingActions"));
            CreateActionInitialData();
            LoadInitialData(false);
            allActions = new List<UserActionData>();
            allActions.Add(UserActionData.Search);
            allActions.Add(UserActionData.Insert);
            allActions.Add(UserActionData.Delete);
            allActions.Add(UserActionData.Update);
            //allActions.Add(UserActionData.Duplicate);

            #endregion

            #region Applications

            changeTaskFunction(ResourceInitialDataLoader.GetString2("CreatingApplications"));
            CreateApplicationInitialData();
            LoadInitialData(false);

            #endregion

            #region Resources

            changeTaskFunction(ResourceInitialDataLoader.GetString2("CreatingResources"));
            CreateUserResourceInitialData();
            CreateAdministrationUserResourceInitialData();
            CreateSupervisionUserResourceInitialData();
            CreateMapLayersResourceInitialData();

            #endregion

            #region Profiles

            changeValueFunction(10);
            changeTaskFunction(ResourceInitialDataLoader.GetString2("CreatingProfiles"));
            CreateProfileInitialData();

            #endregion

            #region Roles

            changeTaskFunction(ResourceInitialDataLoader.GetString2("CreatingRoles"));
            CreateRolesInitialData();

            #endregion


            #region Status

            changeValueFunction(15);
            changeTaskFunction(ResourceInitialDataLoader.GetString2("CreatingStatus"));
            CreateOperatorStatus();
            CreateDispatchOrderStatus();
            CreateIncidentNotificationStatus();
            CreateIncidentStatus();
            CreateUnitStatus();

            #endregion

            #region Priorities

            changeTaskFunction(ResourceInitialDataLoader.GetString2("CreatingPriority"));
            CreatePriorities();

            #endregion

            #region DepartmentType

            if (SmartCadConfiguration.SmartCadSection.ServerElement.InitialDataElement.CreateTestData)
            {
                changeTaskFunction(ResourceInitialDataLoader.GetString2("CreatingDepartamentTypes"));
                CreateDepartmentTypeInitialData();
                CreatePositionDepartmentInitialData();
                //TODO: Falta por incluir.
                CreateRankDepartmentInitialData();
            }

            #endregion

            #region Account
            #region OperatorCategory

            changeTaskFunction(ResourceInitialDataLoader.GetString2("CreatingOperatorsCategory"));
            CreateOperatorCategory();

            #endregion
            changeTaskFunction(ResourceInitialDataLoader.GetString2("Creando cuentas de usuario..."));
            CreateInitialUserAccounts();
            if (SmartCadConfiguration.SmartCadSection.ServerElement.InitialDataElement.CreateTestData)
            {
                CreateDeploymentUserAccounts();
                CreateDevelopmentUserAccounts();
            }

            #endregion

            #region IncidentTypeCategory

            changeTaskFunction(ResourceInitialDataLoader.GetString2("CreatingCategoryIncidentType"));
            CreatePhoneReportIncidentTypeCategory();
            CreateCctvReportIncidentTypeCategory();

            #endregion

            #region IncidentType

            changeValueFunction(20);
            changeTaskFunction(ResourceInitialDataLoader.GetString2("CreatingIncidentTypes"));

            CreateGenericQuestion();

            if (SmartCadConfiguration.SmartCadSection.ServerElement.InitialDataElement.CreateTestData)
            {
                changeValueFunction(25);
                CreateFireIncidentType();

                changeValueFunction(30);
                CreateAccidentType();

                changeValueFunction(35);
                CreateCatastrofeType();

                changeValueFunction(40);
                CreateEmergencyType();

                changeValueFunction(45);
                CreateRescueAnimalType();

                changeValueFunction(50);
                CreateHomicideIncident();

                changeValueFunction(55);
                CreateRobberyIncident();

                changeValueFunction(60);
                CreateDisturbanceIncident();

                changeValueFunction(65);
                CreateBombIncident();

                changeValueFunction(70);
                CreateKidnappingIncident();
                CreateJokeIncidentType();
                CreateInformationIncidentType();

                CreateOthersIncident();

               
            }

            CreateReportIncidentType(IncidentTypeCategoryData.PhoneCall);
            CreateReportIncidentType(IncidentTypeCategoryData.Camera);

            #endregion

            #region PublicLine

            changeValueFunction(75);
            changeTaskFunction(ResourceInitialDataLoader.GetString2("CreatingUsersExtensions"));
            if (SmartCadConfiguration.SmartCadSection.ServerElement.InitialDataElement.CreateTestData)
            {
                CreatePublicLineInitialData();
            }

            #endregion

            #region Unit Types

            changeTaskFunction(ResourceInitialDataLoader.GetString2("CreatingUnitTypes"));
            if (SmartCadConfiguration.SmartCadSection.ServerElement.InitialDataElement.CreateTestData)
            {
                CreateUnitTypes();
            }

            #endregion

            
            #region GPSType
            changeValueFunction(78);
            changeTaskFunction(ResourceInitialDataLoader.GetString2("CreatingGPSType"));
            CreateGPSType();


            #endregion            

            #region Units
            if (SmartCadConfiguration.SmartCadSection.ServerElement.InitialDataElement.CreateTestData)
            {
                changeValueFunction(80);
                changeTaskFunction(ResourceInitialDataLoader.GetString2("CreatingUnits"));
                CreateUnitData();
                             
                
                changeValueFunction(85);
                changeTaskFunction(ResourceInitialDataLoader.GetString2("CreatingOfficers"));
                CreateOfficer();
            }           

            #endregion

            #region ApplicationPreferences

            changeValueFunction(87);
            changeTaskFunction(ResourceInitialDataLoader.GetString2("CreatingApplicationPreferences"));
            CreateApplicationPreference();

            #endregion

            #region DispatchOrderNoteType

            changeValueFunction(90);
            changeTaskFunction(ResourceInitialDataLoader.GetString2("CreatingDispatchNotesTypes"));
            CreateDispatchOrderNoteType();

            #endregion

            #region UnitEndingReportType

            changeValueFunction(92);
            changeTaskFunction(ResourceInitialDataLoader.GetString2("CreateFinishReportType"));
            CreateUnitEndingReportType();

            #endregion

            #region Indicators
            changeValueFunction(93);
            changeTaskFunction(ResourceInitialDataLoader.GetString2("CreateIndicators"));
            CreateIndicators();
            #endregion

            #region Cameras

            changeValueFunction(94);
            changeTaskFunction(ResourceInitialDataLoader.GetString2("CreatingCameras"));
            CreateCameraTypes();

            // COMMENTED FOR BATAAN PROJECT. AA.
            //CreatelprTypes();
            //CreatelprWays();
            CreateConnectionCamerasTypes();
            CreateStructTypes();
            SetQuestionFirstLevelType();
            CreateCctvQuestions();
            #endregion Cameras

            #region Observation Types
            changeValueFunction(95);
            changeTaskFunction(ResourceInitialDataLoader.GetString2("CreatingObservationTypes"));
            CreateObservationTypes();

            #endregion

            #region Motive
            changeValueFunction(96);
            changeTaskFunction(ResourceInitialDataLoader.GetString2("CreatingExtraTimeMotive"));
            CreateExtraTimeMotive();
            changeTaskFunction(ResourceInitialDataLoader.GetString2("CreatingFreeTimeMotive"));
            CreateFreeTimeMotive();

            #endregion

            #region Alerts
            changeValueFunction(98);
            changeTaskFunction(ResourceInitialDataLoader.GetString2("CreatingAlerts"));
            CreateAlerts();
            #endregion

            #region Sensors
            changeValueFunction(100);
            changeTaskFunction(ResourceInitialDataLoader.GetString2("CreatingSensors"));
            CreateSensors();
       
            #endregion

            #region EndReportQuestions

            CreateEndReportQuestions();
            #endregion

            #region Applications

            CreateApplicationNames();

            #endregion

            #region VARules
            changeTaskFunction(ResourceInitialDataLoader.GetString2("CreatingVARules"));
            CreateVideoAnaliticaRules();
            #endregion


            
        }

        private static void CreateApplicationNames() 
        {
            InstalledApplicationData ApplicationAdministration = new InstalledApplicationData() { Name = "Administration", Status = false };
            SmartCadDatabase.SaveNamedObject<InstalledApplicationData>(ApplicationAdministration);

            InstalledApplicationData ApplicationDispatch = new InstalledApplicationData() { Name = "Dispatch", Status = false };
            SmartCadDatabase.SaveNamedObject<InstalledApplicationData>(ApplicationDispatch);

            InstalledApplicationData ApplicationMap = new InstalledApplicationData() { Name = "Map", Status = false };
            SmartCadDatabase.SaveNamedObject<InstalledApplicationData>(ApplicationMap);

            InstalledApplicationData ApplicationCCTV = new InstalledApplicationData() { Name = "CCTV", Status = false };
            SmartCadDatabase.SaveNamedObject<InstalledApplicationData>(ApplicationCCTV);

            InstalledApplicationData ApplicationAlarm = new InstalledApplicationData() { Name = "Alarm", Status = false };
            SmartCadDatabase.SaveNamedObject<InstalledApplicationData>(ApplicationAlarm);

            InstalledApplicationData ApplicationSupervision = new InstalledApplicationData() { Name = "Supervision", Status = false };
            SmartCadDatabase.SaveNamedObject<InstalledApplicationData>(ApplicationSupervision);

            InstalledApplicationData ApplicationReports = new InstalledApplicationData() { Name = "Reports", Status = false };
            SmartCadDatabase.SaveNamedObject<InstalledApplicationData>(ApplicationReports);

            InstalledApplicationData ApplicationFirstLevel = new InstalledApplicationData() { Name = "First Level", Status = false };
            SmartCadDatabase.SaveNamedObject<InstalledApplicationData>(ApplicationFirstLevel);

            InstalledApplicationData Parser = new InstalledApplicationData() { Name = "Parser", Status = false };
            SmartCadDatabase.SaveNamedObject<InstalledApplicationData>(Parser);

            InstalledApplicationData Telephony = new InstalledApplicationData() { Name = "Telephony", Status = false };
            SmartCadDatabase.SaveNamedObject<InstalledApplicationData>(Telephony);

        }

        private static void CreateGPSType()
        {
            #region Skypatrol - Evolution Series
            GPSTypeData gpsType = new GPSTypeData();
            gpsType.Brand = "Skypatrol";
            gpsType.Model = "Evolution Series";
            gpsType.Pines = new ArrayList();
            GPSPinData pin;

            for (int i = 0; i < 2; i++)
            {
                pin = new GPSPinData();
                pin.Number = i;
                pin.Type = GPSPinData.PinType.Input;
                gpsType.Pines.Add(pin);
            }

            pin = new GPSPinData();
            pin.Number = 0;
            pin.Type = GPSPinData.PinType.Output;
            gpsType.Pines.Add(pin);
            SmartCadDatabase.SaveObject<GPSTypeData>(gpsType);
            #endregion

            #region Skypatrol - TT850
            gpsType = new GPSTypeData();
            gpsType.Brand = "Skypatrol";
            gpsType.Model = "TT850";
            gpsType.Pines = new ArrayList();

            for (int i = 0; i < 2; i++)
            {
                pin = new GPSPinData();
                pin.Number = i;
                pin.Type = GPSPinData.PinType.Input;
                gpsType.Pines.Add(pin);
            }
            pin = new GPSPinData();
            pin.Number = 0;
            pin.Type = GPSPinData.PinType.Output;
            gpsType.Pines.Add(pin);
            SmartCadDatabase.SaveObject<GPSTypeData>(gpsType);

            #endregion

            #region Intellitrac - X1
            gpsType = new GPSTypeData();
            gpsType.Brand = "Intellitrac";
            gpsType.Model = "X1";
            gpsType.Pines = new ArrayList();

            for (int i = 1; i < 5; i++)
            {
                pin = new GPSPinData();
                pin.Number = i;
                pin.Type = GPSPinData.PinType.Input;
                gpsType.Pines.Add(pin);
            }

            for (int i = 1; i < 5; i++)
            {
                pin = new GPSPinData();
                pin.Number = i;
                pin.Type = GPSPinData.PinType.Output;
                gpsType.Pines.Add(pin);
            }
            SmartCadDatabase.SaveObject<GPSTypeData>(gpsType);
            #endregion

            #region Intellitrac - A1
            gpsType = new GPSTypeData();
            gpsType.Brand = "Intellitrac";
            gpsType.Model = "A1";
            gpsType.Pines = new ArrayList();

            for (int i = 1; i < 5; i++)
            {
                pin = new GPSPinData();
                pin.Number = i;
                pin.Type = GPSPinData.PinType.Input;
                gpsType.Pines.Add(pin);
            }

            for (int i = 1; i < 2; i++)
            {
                pin = new GPSPinData();
                pin.Number = i;
                pin.Type = GPSPinData.PinType.Output;
                gpsType.Pines.Add(pin);
            }
            SmartCadDatabase.SaveObject<GPSTypeData>(gpsType);
            #endregion

            #region Tramigo - T23
            gpsType = new GPSTypeData();
            gpsType.Brand = "Tramigo";
            gpsType.Model = "T23";
            gpsType.Pines = new ArrayList();

            for (int i = 1; i < 5; i++)
            {
                pin = new GPSPinData();
                pin.Number = i;
                pin.Type = GPSPinData.PinType.Input;
                gpsType.Pines.Add(pin);
            }

            for (int i = 1; i < 4; i++)
            {
                pin = new GPSPinData();
                pin.Number = i;
                pin.Type = GPSPinData.PinType.Output;
                gpsType.Pines.Add(pin);
            }
            SmartCadDatabase.SaveObject<GPSTypeData>(gpsType);
            #endregion

            #region Arknav - R9
            gpsType = new GPSTypeData();
            gpsType.Brand = "Arknav";
            gpsType.Model = "R9";
            gpsType.Pines = new ArrayList();

            for (int i = 0; i < 4; i++)
            {
                pin = new GPSPinData();
                pin.Number = i;
                pin.Type = GPSPinData.PinType.Input;
                gpsType.Pines.Add(pin);
            }

            for (int i = 0; i < 4; i++)
            {
                pin = new GPSPinData();
                pin.Number = i;
                pin.Type = GPSPinData.PinType.Output;
                gpsType.Pines.Add(pin);
            }
            SmartCadDatabase.SaveObject<GPSTypeData>(gpsType);
            #endregion

            #region Dtc
            gpsType = new GPSTypeData();
            gpsType.Brand = "Dtc";
            gpsType.Model = "Syrus";
            gpsType.Pines = new ArrayList();
            

            for (int i = 0; i < 2; i++)
            {
                pin = new GPSPinData();
                pin.Number = i;
                pin.Type = GPSPinData.PinType.Input;
                gpsType.Pines.Add(pin);
            }

            pin = new GPSPinData();
            pin.Number = 0;
            pin.Type = GPSPinData.PinType.Output;
            gpsType.Pines.Add(pin);
            SmartCadDatabase.SaveObject<GPSTypeData>(gpsType);
            #endregion
        }

        private static void CreateSensors() 
        {
            SensorData sensor = new SensorData();
            sensor.CustomCode = "CRASH";
            sensor.Name = ResourceInitialDataLoader.GetString2("CRASH");
            sensor.Type = SensorData.SensorType.Input;
            SmartCadDatabase.SaveNamedObject<SensorData>(sensor);

            sensor = new SensorData();
            sensor.CustomCode = "CAVA_TEMPERATURE";
            sensor.Name = ResourceInitialDataLoader.GetString2("CAVA_TEMPERATURE");
            sensor.Type = SensorData.SensorType.Input;
            SmartCadDatabase.SaveNamedObject<SensorData>(sensor);

            sensor = new SensorData();
            sensor.CustomCode = "PANIC_BUTTON";
            sensor.Name = ResourceInitialDataLoader.GetString2("PANIC_BUTTON");
            sensor.Type = SensorData.SensorType.Input;
            SmartCadDatabase.SaveNamedObject<SensorData>(sensor);

            sensor = new SensorData();
            sensor.CustomCode = "DOOR_STATUS";
            sensor.Name = ResourceInitialDataLoader.GetString2("DOOR_STATUS");
            sensor.Type = SensorData.SensorType.Input;
            SmartCadDatabase.SaveNamedObject<SensorData>(sensor);

            sensor = new SensorData();
            sensor.CustomCode = "GASOLINE";
            sensor.Name = ResourceInitialDataLoader.GetString2("GASOLINE");
            sensor.Type = SensorData.SensorType.Input;
            SmartCadDatabase.SaveNamedObject<SensorData>(sensor);

            sensor = new SensorData();
            sensor.CustomCode = "BURGLAR_ALARM";
            sensor.Name = ResourceInitialDataLoader.GetString2("BURGLAR_ALARM");
            sensor.Type = SensorData.SensorType.Input;
            SmartCadDatabase.SaveNamedObject<SensorData>(sensor);

            sensor = new SensorData();
            sensor.CustomCode = "INCLINOMETER";
            sensor.Name = ResourceInitialDataLoader.GetString2("INCLINOMETER");
            sensor.Type = SensorData.SensorType.Input;
            SmartCadDatabase.SaveNamedObject<SensorData>(sensor);

            sensor = new SensorData();
            sensor.CustomCode = "UNIT_BATTERY";
            sensor.Name = ResourceInitialDataLoader.GetString2("UNIT_BATTERY");
            sensor.Type = SensorData.SensorType.Input;
            SmartCadDatabase.SaveNamedObject<SensorData>(sensor);

            sensor = new SensorData();
            sensor.CustomCode = "MOTOR_TEMPERATURE";
            sensor.Name = ResourceInitialDataLoader.GetString2("MOTOR_TEMPERATURE");
            sensor.Type = SensorData.SensorType.Input;
            SmartCadDatabase.SaveNamedObject<SensorData>(sensor);
            
            sensor = new SensorData();
            sensor.CustomCode = "MERMAID";
            sensor.Name = ResourceInitialDataLoader.GetString2("MERMAID");
            sensor.Type = SensorData.SensorType.Output;
            SmartCadDatabase.SaveNamedObject<SensorData>(sensor);
        }

        private static void CreateAlerts() 
        {
            AlertData alert = new AlertData();
            alert.CustomCode = "GPS_I/O";
            alert.Name = ResourceInitialDataLoader.GetString2("GPS_I/O");
            alert.Measure = null;
            SmartCadDatabase.SaveNamedObject<AlertData>(alert);

            alert.CustomCode = "SPEED_LIMIT";
            alert.Name = ResourceInitialDataLoader.GetString2("SPEED_LIMIT");
            SmartCadDatabase.SaveNamedObject<AlertData>(alert);

            alert.CustomCode = "WITHOUT_COMMUNICATION";
            alert.Name = ResourceInitialDataLoader.GetString2("WITHOUT_COMMUNICATION");
            alert.Measure = null;
            SmartCadDatabase.SaveNamedObject<AlertData>(alert);

            alert.CustomCode = "UNSPECIFIED_STOP";
            alert.Name = ResourceInitialDataLoader.GetString2("UNSPECIFIED_STOP");
            SmartCadDatabase.SaveNamedObject<AlertData>(alert);

            alert.CustomCode = "STOP_TIME_LIMIT";
            alert.Name = ResourceInitialDataLoader.GetString2("STOP_TIME_LIMIT");
            SmartCadDatabase.SaveNamedObject<AlertData>(alert);

            alert.CustomCode = "OFF_ROUTE";
            alert.Name = ResourceInitialDataLoader.GetString2("OFF_ROUTE");
            alert.Measure = null;
            SmartCadDatabase.SaveNamedObject<AlertData>(alert);
            
            alert.CustomCode = "IN_ROUTE";
            alert.Name = ResourceInitialDataLoader.GetString2("IN_ROUTE");
            alert.Measure = null;
            SmartCadDatabase.SaveNamedObject<AlertData>(alert);
        }

        private static void CreateMapLayersResourceInitialData()
        {
            UserResourceData resource;

            string nameLayer = "MapLayer";
            
            //Units Layer.
            resource = new UserResourceData();
            resource.Name = nameLayer + "Units";
            resource.FriendlyName = ResourceInitialDataLoader.GetString2("LayerThe") +" "+ ResourceInitialDataLoader.GetString2("UNITS");
            resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
            resource.Actions = mapActions;
            CreateAccess(resource, UserApplicationData.Map);

            //Incidents Layer.
            resource = new UserResourceData();
            resource.Name = nameLayer + "Incidents";
            resource.FriendlyName = ResourceInitialDataLoader.GetString2("Incidents");
            resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
            resource.Actions = mapActions;
            CreateAccess(resource, UserApplicationData.Map);

            //Cctv Layer.
            resource = new UserResourceData();
            resource.Name = nameLayer + "PostMap";
            resource.FriendlyName = ResourceInitialDataLoader.GetString2("PostMap");
            resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
            resource.Actions = mapActions;
            CreateAccess(resource, UserApplicationData.Map);

            //DepartmentZones Layer.
            resource = new UserResourceData();
            resource.Name = nameLayer + "DepartmentZones";
            resource.FriendlyName = ResourceInitialDataLoader.GetString2("LayerThe") +" "+ ResourceInitialDataLoader.GetString2("DepartmentZones");
            resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
            resource.Actions = mapActions;
            CreateAccess(resource, UserApplicationData.Map);

            //DepartmentStations Layer.
            resource = new UserResourceData();
            resource.Name = nameLayer + "DepartmentStations";
            resource.FriendlyName = ResourceInitialDataLoader.GetString2("LayerThe") +" "+ ResourceInitialDataLoader.GetString2("DepartmentStations");
            resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
            resource.Actions = mapActions;
            CreateAccess(resource, UserApplicationData.Map);
      
            //MovilSecurity Layer.
            resource = new UserResourceData();
            resource.Name = nameLayer + "MovilSecurity";
            resource.FriendlyName = ResourceInitialDataLoader.GetString2("MovilSecurity");
            resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
            resource.Actions = mapActions;
            CreateAccess(resource, UserApplicationData.Map);

			//HistoricTracks 
            resource = new UserResourceData();
			resource.Name = "HistoricTracks";
			resource.FriendlyName = ResourceInitialDataLoader.GetString2("HistoricTracks");
            resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
            resource.Actions = new ArrayList();

			//HistoricTracks Action
			UserActionData action = new UserActionData("ViewHistoricTracks", ResourceInitialDataLoader.GetString2("ViewHistoricTracks"));
            SmartCadDatabase.SaveNamedObject<UserActionData>(action);
            resource.Actions.Add(action);
            CreateAccess(resource, UserApplicationData.Map);

			//FleetControl 
			resource = new UserResourceData();
			resource.Name = "FleetControl";
			resource.FriendlyName = ResourceInitialDataLoader.GetString2("FleetControl");
			resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
			resource.Actions = new ArrayList();

			//FleetControl Action
			action = new UserActionData("ViewFleetControl", ResourceInitialDataLoader.GetString2("ViewFleetControl"));
			SmartCadDatabase.SaveNamedObject<UserActionData>(action);
			resource.Actions.Add(action);
			CreateAccess(resource, UserApplicationData.Map);

        }          
      
        private static void CreateExtraTimeMotive()
        {
            /***************** Extra Times ***************/
            MotiveVariationData motive = new MotiveVariationData();
            motive.Name = ResourceInitialDataLoader.GetString2("RotativeSchedules");
            motive.Type = true;  
            SmartCadDatabase.SaveNamedObject<MotiveVariationData>(motive);

            motive.Name = ResourceInitialDataLoader.GetString2("ExtraDays");
            motive.Type = true;
            SmartCadDatabase.SaveNamedObject<MotiveVariationData>(motive);

            motive.Name = ResourceInitialDataLoader.GetString2("ExtraHours");
            motive.Type = true;
            SmartCadDatabase.SaveNamedObject<MotiveVariationData>(motive);

            motive.Name = ResourceInitialDataLoader.GetString2("OtherExtraTimeMotive");
            motive.Type = true;
            SmartCadDatabase.SaveNamedObject<MotiveVariationData>(motive);

        }

        private static void CreateFreeTimeMotive()
        {
          /***************** Free Times **********************/
            MotiveVariationData motive = new MotiveVariationData();
            motive.Name = ResourceInitialDataLoader.GetString2("MedicalLicense");
            motive.Type = false;
            SmartCadDatabase.SaveNamedObject<MotiveVariationData>(motive);

            motive.Name = ResourceInitialDataLoader.GetString2("MedicalSleep");
            motive.Type = false;
            SmartCadDatabase.SaveNamedObject<MotiveVariationData>(motive);

            motive.Name = ResourceInitialDataLoader.GetString2("Vacations");
            motive.Type = false;
            SmartCadDatabase.SaveNamedObject<MotiveVariationData>(motive);

            motive.Name = ResourceInitialDataLoader.GetString2("PrenatalLicense");
            motive.Type = false;
            SmartCadDatabase.SaveNamedObject<MotiveVariationData>(motive);

            motive.Name = ResourceInitialDataLoader.GetString2("PostnatalLicense");
            motive.Type = false;
            SmartCadDatabase.SaveNamedObject<MotiveVariationData>(motive);

            motive.Name = ResourceInitialDataLoader.GetString2("FamilyEmergency");
            motive.Type = false;
            SmartCadDatabase.SaveNamedObject<MotiveVariationData>(motive);

            motive.Name = ResourceInitialDataLoader.GetString2("FamilyDied");
            motive.Type = false;
            SmartCadDatabase.SaveNamedObject<MotiveVariationData>(motive);

            motive.Name = ResourceInitialDataLoader.GetString2("Studies");
            motive.Type = false;
            SmartCadDatabase.SaveNamedObject<MotiveVariationData>(motive);

            motive.Name = ResourceInitialDataLoader.GetString2("HealthProblems");
            motive.Type = false;
            SmartCadDatabase.SaveNamedObject<MotiveVariationData>(motive);

            motive.Name = ResourceInitialDataLoader.GetString2("MedicalEmergency");
            motive.Type = false;
            SmartCadDatabase.SaveNamedObject<MotiveVariationData>(motive);

            motive.Name = ResourceInitialDataLoader.GetString2("Travel");
            motive.Type = false;
            SmartCadDatabase.SaveNamedObject<MotiveVariationData>(motive);

            motive.Name = ResourceInitialDataLoader.GetString2("PersonalProceedings");
            motive.Type = false;
            SmartCadDatabase.SaveNamedObject<MotiveVariationData>(motive);

            motive.Name = ResourceInitialDataLoader.GetString2("Accident");
            motive.Type = false;
            SmartCadDatabase.SaveNamedObject<MotiveVariationData>(motive);

            motive.Name = ResourceInitialDataLoader.GetString2("OtherFreeTimeMotive");
            motive.Type = false;
            SmartCadDatabase.SaveNamedObject<MotiveVariationData>(motive);
        
        }

        private static void CreateCctvQuestions()
        {
            QuestionData question = new QuestionData();
            question.ShortText = ResourceInitialDataLoader.GetString2("CctvQuestionShortText");
            question.Text = ResourceInitialDataLoader.GetString2("CctvQuestionText");
            question.CustomCode = "CCTV";
            question.RequirementType = RequirementTypeEnum.Required;
            question.App = cctvQuestionsAppList;
            question.SetPossibleAnswers = new HashedSet();
            QuestionPossibleAnswerData answer1 = new QuestionPossibleAnswerData();
            answer1.Question = question;
            answer1.Description = ResourceInitialDataLoader.GetString2("Status");
            answer1.ControlName = "TxtBxx1";
            question.SetPossibleAnswers.Add(answer1);
            SmartCadDatabase.SaveObject<QuestionData>(question);       //20 150     
            #region renderMethod
            string renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
              <Control itemText='Texto libre' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.39657, Culture=neutral, PublicKeyToken=null' name='TxtBxx1' multiline='True'/>
              <property name='#LayoutVersion' />
              <property name='Items' iskey='true' value='2'>
                <property name='Item1' isnull='true' iskey='true'>
                  <property name='ExpandButtonVisible'>false</property>
                  <property name='Size'>@3,Width=627@3,Height=101</property>
                  <property name='Padding'>0, 0, 0, 0</property>
                  <property name='ParentName' />
                  <property name='TextVisible'>false</property>
                  <property name='Expanded'>true</property>
                  <property name='ExpandOnDoubleClick'>false</property>
                  <property name='DefaultLayoutType'>Vertical</property>
                  <property name='Location'>@1,X=0@1,Y=0</property>
                  <property name='ShowTabPageCloseButton'>false</property>
                  <property name='TextLocation'>Top</property>
                  <property name='OptionsToolTip' isnull='true' iskey='true'>
                    <property name='IconToolTipTitle' />
                    <property name='IconToolTipIconType'>None</property>
                    <property name='EnableIconToolTip'>true</property>
                    <property name='IconToolTip' />
                    <property name='ToolTip' />
                    <property name='ToolTipTitle' />
                    <property name='ToolTipIconType'>None</property>
                  </property>
                  <property name='OptionsCustomization' isnull='true' iskey='true'>
                    <property name='AllowDrop'>Default</property>
                    <property name='AllowDrag'>Default</property>
                  </property>
                  <property name='Spacing'>0, 0, 0, 0</property>
                  <property name='AppearanceItemCaption' isnull='true' iskey='true'>
                    <property name='Font'>Tahoma, 8.25pt</property>
                    <property name='BackColor2' />
                    <property name='BackColor' />
                    <property name='ForeColor' />
                    <property name='BorderColor' />
                    <property name='GradientMode'>Horizontal</property>
                  </property>
                  <property name='AppearanceTabPage' isnull='true' iskey='true'>
                    <property name='HeaderHotTracked' isnull='true' iskey='true'>
                      <property name='Font'>Tahoma, 8.25pt</property>
                      <property name='BackColor2' />
                      <property name='BackColor' />
                      <property name='ForeColor' />
                      <property name='BorderColor' />
                      <property name='GradientMode'>Horizontal</property>
                    </property>
                    <property name='PageClient' isnull='true' iskey='true'>
                      <property name='Font'>Tahoma, 8.25pt</property>
                      <property name='BackColor2' />
                      <property name='BackColor' />
                      <property name='ForeColor' />
                      <property name='BorderColor' />
                      <property name='GradientMode'>Horizontal</property>
                    </property>
                    <property name='HeaderDisabled' isnull='true' iskey='true'>
                      <property name='Font'>Tahoma, 8.25pt</property>
                      <property name='BackColor2' />
                      <property name='BackColor' />
                      <property name='ForeColor' />
                      <property name='BorderColor' />
                      <property name='GradientMode'>Horizontal</property>
                    </property>
                    <property name='Header' isnull='true' iskey='true'>
                      <property name='Font'>Tahoma, 8.25pt</property>
                      <property name='BackColor2' />
                      <property name='BackColor' />
                      <property name='ForeColor' />
                      <property name='BorderColor' />
                      <property name='GradientMode'>Horizontal</property>
                    </property>
                    <property name='HeaderActive' isnull='true' iskey='true'>
                      <property name='Font'>Tahoma, 8.25pt</property>
                      <property name='BackColor2' />
                      <property name='BackColor' />
                      <property name='ForeColor' />
                      <property name='BorderColor' />
                      <property name='GradientMode'>Horizontal</property>
                    </property>
                  </property>
                  <property name='Name'>Root</property>
                  <property name='AppearanceGroup' isnull='true' iskey='true'>
                    <property name='Font'>Tahoma, 8.25pt</property>
                    <property name='BackColor2' />
                    <property name='BackColor' />
                    <property name='ForeColor' />
                    <property name='BorderColor' />
                    <property name='GradientMode'>Horizontal</property>
                  </property>
                  <property name='CaptionImageLocation'>Default</property>
                  <property name='CaptionImageVisible'>true</property>
                  <property name='ExpandButtonLocation'>Default</property>
                  <property name='CaptionImageIndex'>-1</property>
                  <property name='TextToControlDistance'>0</property>
                  <property name='TabbedGroupParentName' />
                  <property name='TypeName'>LayoutGroup</property>
                  <property name='GroupBordersVisible'>true</property>
                  <property name='OptionsItemText' isnull='true' iskey='true'>
                    <property name='TextAlignMode'>UseParentOptions</property>
                    <property name='TextToControlDistance'>5</property>
                  </property>
                  <property name='AllowDrawBackground'>true</property>
                  <property name='Visibility'>Always</property>
                  <property name='ShowInCustomizationForm'>true</property>
                  <property name='Text'>Root</property>
                  <property name='CustomizationFormText'>layoutControlGroup1</property>
                </property>
                <property name='Item2' isnull='true' iskey='true'>
                  <property name='OptionsCustomization' isnull='true' iskey='true'>
                    <property name='AllowDrop'>Default</property>
                    <property name='AllowDrag'>Default</property>
                  </property>
                  <property name='Size'>@3,Width=625@2,Height=99</property>
                  <property name='OptionsToolTip' isnull='true' iskey='true'>
                    <property name='IconToolTipTitle' />
                    <property name='IconToolTipIconType'>None</property>
                    <property name='EnableIconToolTip'>true</property>
                    <property name='IconToolTip' />
                    <property name='ToolTip' />
                    <property name='ToolTipTitle' />
                    <property name='ToolTipIconType'>None</property>
                  </property>
                  <property name='CustomizationFormText'>Texto libre</property>
                  <property name='MinSize'>@2,Width=25@2,Height=25</property>
                  <property name='ShowInCustomizationForm'>true</property>
                  <property name='Text'>Texto libre</property>
                  <property name='TextSize'>@1,Width=0@1,Height=0</property>
                  <property name='TextVisible'>false</property>
                  <property name='Padding'>5, 5, 5, 5</property>
                  <property name='TextToControlDistance'>0</property>
                  <property name='Name'>item1</property>
                  <property name='Location'>@1,X=0@1,Y=0</property>
                  <property name='ParentName'>Root</property>
                  <property name='AppearanceItemCaption' isnull='true' iskey='true'>
                    <property name='Font'>Tahoma, 8.25pt</property>
                    <property name='BackColor2' />
                    <property name='BackColor' />
                    <property name='ForeColor' />
                    <property name='BorderColor' />
                    <property name='GradientMode'>Horizontal</property>
                  </property>
                  <property name='MaxSize'>@1,Width=0@1,Height=0</property>
                  <property name='Spacing'>0, 0, 0, 0</property>
                  <property name='TextLocation'>Left</property>
                  <property name='Visibility'>Always</property>
                  <property name='SizeConstraintsType'>Custom</property>
                  <property name='ControlName'>TxtBxx1</property>
                  <property name='TypeName'>LayoutControlItem</property>
                  <property name='TextAlignMode'>UseParentOptions</property>
                  <property name='AllowHtmlStringInCaption'>false</property>
                  <property name='ImageAlignment'>MiddleLeft</property>
                  <property name='ImageIndex'>-1</property>
                  <property name='ImageToTextDistance'>5</property>
                  <property name='Image' isnull='true' />
                </property>
              </property>
              <property name='LookAndFeel' isnull='true' iskey='true'>
                <property name='UseDefaultLookAndFeel'>true</property>
                <property name='UseWindowsXPTheme'>false</property>
                <property name='SkinName'>Caramel</property>
                <property name='Style'>Skin</property>
              </property>
            </XtraSerializer>
            ";
            #endregion renderMethod
            question.RenderMethod = renderMethod;

            IList incidentTypeData = SmartCadDatabase.SearchObjects(SmartCadHqls.GetIncidentTypesInitialData);

            try
            {
                foreach (IncidentTypeData incidentType in incidentTypeData)
                {
                    IncidentTypeQuestionData itqWhere = new IncidentTypeQuestionData();
                    itqWhere.IncidentType = incidentType;
                    itqWhere.Question = question;

                    incidentType.SetIncidentTypeQuestions.Add(itqWhere);
                    SmartCadDatabase.UpdateObject<IncidentTypeData>(incidentType);
                }
            }
            catch (Exception e)
            {
            }

            SmartCadDatabase.SaveOrUpdate(question);
        }

        private static void CreateEndReportQuestions()
        {
            QuestionDispatchReportData question = new QuestionDispatchReportData();
            question.Text = ResourceInitialDataLoader.GetString2("Observations");
            question.Required = true;

            #region renderMethod
            string renderMethod = @"<XtraSerializer version='1.0' application='LayoutControl'>
  <Control itemText='Texto libre' itemName='item1' type='SmartCadControls.Controls.TextBoxEx, SmartCadControls, Version=1.0.3450.39657, Culture=neutral, PublicKeyToken=null' name='TxtBxxObservations' multiline='True'/>
  <property name='#LayoutVersion' />
  <property name='Items' iskey='true' value='2'>
    <property name='Item1' isnull='true' iskey='true'>
      <property name='ExpandButtonVisible'>false</property>
      <property name='Size'>@3,Width=627@3,Height=101</property>
      <property name='Padding'>0, 0, 0, 0</property>
      <property name='ParentName' />
      <property name='TextVisible'>false</property>
      <property name='Expanded'>true</property>
      <property name='ExpandOnDoubleClick'>false</property>
      <property name='DefaultLayoutType'>Vertical</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ShowTabPageCloseButton'>false</property>
      <property name='TextLocation'>Top</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='AppearanceTabPage' isnull='true' iskey='true'>
        <property name='HeaderHotTracked' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='PageClient' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderDisabled' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='Header' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
        <property name='HeaderActive' isnull='true' iskey='true'>
          <property name='Font'>Tahoma, 8.25pt</property>
          <property name='BackColor2' />
          <property name='BackColor' />
          <property name='ForeColor' />
          <property name='BorderColor' />
          <property name='GradientMode'>Horizontal</property>
        </property>
      </property>
      <property name='Name'>Root</property>
      <property name='AppearanceGroup' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='CaptionImageLocation'>Default</property>
      <property name='CaptionImageVisible'>true</property>
      <property name='ExpandButtonLocation'>Default</property>
      <property name='CaptionImageIndex'>-1</property>
      <property name='TextToControlDistance'>0</property>
      <property name='TabbedGroupParentName' />
      <property name='TypeName'>LayoutGroup</property>
      <property name='GroupBordersVisible'>true</property>
      <property name='OptionsItemText' isnull='true' iskey='true'>
        <property name='TextAlignMode'>UseParentOptions</property>
        <property name='TextToControlDistance'>5</property>
      </property>
      <property name='AllowDrawBackground'>true</property>
      <property name='Visibility'>Always</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Root</property>
      <property name='CustomizationFormText'>layoutControlGroup1</property>
    </property>
    <property name='Item2' isnull='true' iskey='true'>
      <property name='OptionsCustomization' isnull='true' iskey='true'>
        <property name='AllowDrop'>Default</property>
        <property name='AllowDrag'>Default</property>
      </property>
      <property name='Size'>@3,Width=625@2,Height=99</property>
      <property name='OptionsToolTip' isnull='true' iskey='true'>
        <property name='IconToolTipTitle' />
        <property name='IconToolTipIconType'>None</property>
        <property name='EnableIconToolTip'>true</property>
        <property name='IconToolTip' />
        <property name='ToolTip' />
        <property name='ToolTipTitle' />
        <property name='ToolTipIconType'>None</property>
      </property>
      <property name='CustomizationFormText'>Texto libre</property>
      <property name='MinSize'>@2,Width=25@2,Height=25</property>
      <property name='ShowInCustomizationForm'>true</property>
      <property name='Text'>Texto libre</property>
      <property name='TextSize'>@1,Width=0@1,Height=0</property>
      <property name='TextVisible'>false</property>
      <property name='Padding'>5, 5, 5, 5</property>
      <property name='TextToControlDistance'>0</property>
      <property name='Name'>item1</property>
      <property name='Location'>@1,X=0@1,Y=0</property>
      <property name='ParentName'>Root</property>
      <property name='AppearanceItemCaption' isnull='true' iskey='true'>
        <property name='Font'>Tahoma, 8.25pt</property>
        <property name='BackColor2' />
        <property name='BackColor' />
        <property name='ForeColor' />
        <property name='BorderColor' />
        <property name='GradientMode'>Horizontal</property>
      </property>
      <property name='MaxSize'>@1,Width=0@1,Height=0</property>
      <property name='Spacing'>0, 0, 0, 0</property>
      <property name='TextLocation'>Left</property>
      <property name='Visibility'>Always</property>
      <property name='SizeConstraintsType'>Custom</property>
      <property name='ControlName'>TxtBxxObservations</property>
      <property name='TypeName'>LayoutControlItem</property>
      <property name='TextAlignMode'>UseParentOptions</property>
      <property name='AllowHtmlStringInCaption'>false</property>
      <property name='ImageAlignment'>MiddleLeft</property>
      <property name='ImageIndex'>-1</property>
      <property name='ImageToTextDistance'>5</property>
      <property name='Image' isnull='true' />
    </property>
  </property>
  <property name='LookAndFeel' isnull='true' iskey='true'>
    <property name='UseDefaultLookAndFeel'>true</property>
    <property name='UseWindowsXPTheme'>false</property>
    <property name='SkinName'>Caramel</property>
    <property name='Style'>Skin</property>
  </property>
</XtraSerializer>
";
            #endregion renderMethod
            question.Render = renderMethod;

            SmartCadDatabase.SaveObject<QuestionDispatchReportData>(question);

        }

        private static void SetQuestionFirstLevelType()
        {
            IList questions = SmartCadDatabase.SearchObjects(SmartCadHqls.GetQuestionsInitialData);
            HashedSet incidensTypeQuestion = new HashedSet();
            foreach (QuestionData question in questions)
            {
                question.App = firstLevelQuestionsAppList;

                foreach (IncidentTypeQuestionData incidentType in question.IncidentTypes)
                {
                    IncidentTypeQuestionData incidentTypeQuestion1 = new IncidentTypeQuestionData();
                    incidentTypeQuestion1.IncidentType = incidentType.IncidentType;
                    incidentTypeQuestion1.Question = question;
                    incidensTypeQuestion.Add(incidentTypeQuestion1);
                }
                question.IncidentTypes = new ListSet();
                SmartCadDatabase.SaveOrUpdate(question);

                foreach (IncidentTypeQuestionData incidentType in incidensTypeQuestion)
                {
                    IncidentTypeQuestionData incidentTypeQuestion = new IncidentTypeQuestionData();
                    incidentTypeQuestion.IncidentType = incidentType.IncidentType;
                    incidentTypeQuestion.Question = question;
                    
                    //SmartCadDatabase.SaveOrUpdate(incidentTypeQuestion);
                }
                incidensTypeQuestion.Clear();
            }
        }

        private static void CreateObservationTypes()
        {
            //This observations are generic for all the operators..

			IList accesses = SmartCadDatabase.SearchObjects(new UserAccessData());
			UserAccessData firstLevel = null;
			UserAccessData dispatch = null;
			foreach (UserAccessData userAccess in accesses)
			{
				if (userAccess.Name.Equals("UserApplicationData+Basic+Dispatch"))
					dispatch = userAccess;

				if (userAccess.Name.Equals("UserApplicationData+Basic+FirstLevel"))
					firstLevel = userAccess;
			}	
            //Schedule
            ObservationTypeData obs = new ObservationTypeData();
            obs.Name = "Schedule";
            obs.FriendlyName = ResourceInitialDataLoader.GetString2("ScheduleObservationType");
			obs.UserAccess = new ArrayList();
			obs.UserAccess.Add(firstLevel);
			obs.UserAccess.Add(dispatch);
            SmartCadDatabase.SaveNamedObject<ObservationTypeData>(obs);
			
            //Level of Service
            obs.Name = "ServiceLevel";
			obs.FriendlyName = ResourceInitialDataLoader.GetString2("ServiceLevelObservationType");
			obs.UserAccess = new ArrayList();
			obs.UserAccess.Add(firstLevel);
			obs.UserAccess.Add(dispatch);
            SmartCadDatabase.SaveNamedObject<ObservationTypeData>(obs);

			//Calls Attention

			obs.Name = "CallsAttention";
			obs.FriendlyName = ResourceInitialDataLoader.GetString2("CallsAttentionObservationType");
			obs.UserAccess = new ArrayList();
			obs.UserAccess.Add(firstLevel);
			SmartCadDatabase.SaveObject<ObservationTypeData>(obs);

			//Dispatchs Attenction

			obs.Name = "DispatchAttencion";
			obs.FriendlyName = ResourceInitialDataLoader.GetString2("DispatchAttentionObservationType");
			obs.UserAccess = new ArrayList();
			obs.UserAccess.Add(dispatch);
			SmartCadDatabase.SaveObject<ObservationTypeData>(obs);

            //Training
            obs.Name = "Training";
			obs.FriendlyName = ResourceInitialDataLoader.GetString2("TrainingObservationType");
			obs.UserAccess = new ArrayList();
			obs.UserAccess.Add(firstLevel);
			obs.UserAccess.Add(dispatch);
            SmartCadDatabase.SaveNamedObject<ObservationTypeData>(obs);

			//FirstLevel Procedure Use
			obs.Name = "FirstLevelProcedureUse";
			obs.FriendlyName = ResourceInitialDataLoader.GetString2("FirstLevelProcedureUseObservationType");
			obs.UserAccess = new ArrayList();
			obs.UserAccess.Add(firstLevel);
			SmartCadDatabase.SaveNamedObject<ObservationTypeData>(obs);
			
			//Dispatch Procedure Use
			obs.Name = "DispatchProcedureUse";
			obs.FriendlyName = ResourceInitialDataLoader.GetString2("DispatchProcedureUseObservationType");
			obs.UserAccess = new ArrayList();
			obs.UserAccess.Add(dispatch);
			SmartCadDatabase.SaveNamedObject<ObservationTypeData>(obs);

			//Management
			obs.Name = "Management";
			obs.FriendlyName = ResourceInitialDataLoader.GetString2("ManagementObservationType");
			obs.UserAccess = new ArrayList();
			obs.UserAccess.Add(firstLevel);
			obs.UserAccess.Add(dispatch);
			SmartCadDatabase.SaveNamedObject<ObservationTypeData>(obs);

            //Orientation
            obs.Name = "Orientation";
			obs.FriendlyName = ResourceInitialDataLoader.GetString2("OrientationObservationType");
			obs.UserAccess = new ArrayList();
			obs.UserAccess.Add(firstLevel);
			obs.UserAccess.Add(dispatch);
            SmartCadDatabase.SaveNamedObject<ObservationTypeData>(obs);

			//Efficiency
			obs.Name = "Efficiency";
			obs.FriendlyName = ResourceInitialDataLoader.GetString2("EfficiencyObservationType");
			obs.UserAccess = new ArrayList();
			obs.UserAccess.Add(firstLevel);
			obs.UserAccess.Add(dispatch);
			SmartCadDatabase.SaveNamedObject<ObservationTypeData>(obs);
			//Behavior with the instructor
			obs.Name = "BehaviorWithTheInstructor";
			obs.FriendlyName = ResourceInitialDataLoader.GetString2("BehaviorWithTheInstructorObservationType");
			obs.UserAccess = new ArrayList();
			obs.UserAccess.Add(firstLevel);
			obs.UserAccess.Add(dispatch);
			SmartCadDatabase.SaveNamedObject<ObservationTypeData>(obs);

			//Inappropriate behavior within facilities
			obs.Name = "InappropriateBehaviorWithinFacilities";
			obs.FriendlyName = ResourceInitialDataLoader.GetString2("InappropriateBehaviorWithinFacilitiesObservationType");
			obs.UserAccess = new ArrayList();
			obs.UserAccess.Add(firstLevel);
			obs.UserAccess.Add(dispatch);
			SmartCadDatabase.SaveNamedObject<ObservationTypeData>(obs);
			
			//Procedures execute speed
			obs.Name = "ProceduresExecuteSpeed";
			obs.FriendlyName = ResourceInitialDataLoader.GetString2("ProceduresExecuteSpeedObservationType");
			obs.UserAccess = new ArrayList();
			obs.UserAccess.Add(firstLevel);
			obs.UserAccess.Add(dispatch);
			SmartCadDatabase.SaveNamedObject<ObservationTypeData>(obs);

            //Others
            obs.Name = "Others";
			obs.FriendlyName = ResourceInitialDataLoader.GetString2("OthersObservationType");
			obs.UserAccess = new ArrayList();
			obs.UserAccess.Add(firstLevel);
			obs.UserAccess.Add(dispatch);
            SmartCadDatabase.SaveNamedObject<ObservationTypeData>(obs);
        }

        private static void CreateIndicators()
        {
            IndicatorTypeData indicatorTypeCalls;
            IndicatorTypeData indicatorTypeFirstLevel;
            IndicatorTypeData indicatorTypeDispatch;

            indicatorTypeCalls = new IndicatorTypeData();
            indicatorTypeCalls.Name = "CALLS";
            indicatorTypeCalls.FriendlyName = ResourceLoader.GetString2("Calls");
            indicatorTypeCalls.Description = ResourceInitialDataLoader.GetString2("IndicatorTypeCallDescription");
            indicatorTypeCalls.ApplicationType = ApplicationType.FIRST_LEVEL;
            indicatorTypeCalls = SmartCadDatabase.SaveNamedObject<IndicatorTypeData>(indicatorTypeCalls);

            indicatorTypeFirstLevel = new IndicatorTypeData();
            indicatorTypeFirstLevel.Name = "FIRSTLEVEL";
            indicatorTypeFirstLevel.FriendlyName = ResourceLoader.GetString2("FirstLevel");
            indicatorTypeFirstLevel.Description = ResourceInitialDataLoader.GetString2("IndicatorTypeFirstLevelDescription");
            indicatorTypeFirstLevel.ApplicationType = ApplicationType.FIRST_LEVEL;
            indicatorTypeFirstLevel = SmartCadDatabase.SaveNamedObject<IndicatorTypeData>(indicatorTypeFirstLevel);

            indicatorTypeDispatch = new IndicatorTypeData();
            indicatorTypeDispatch.Name = "DISPATCH";
            indicatorTypeDispatch.FriendlyName = ResourceLoader.GetString2("Dispatch");
            indicatorTypeDispatch.Description = ResourceInitialDataLoader.GetString2("IndicatorTypeDispatchDescription");
            indicatorTypeDispatch.ApplicationType = ApplicationType.DISPATCH;
            indicatorTypeDispatch = SmartCadDatabase.SaveNamedObject<IndicatorTypeData>(indicatorTypeDispatch);

            IndicatorClassData indicatorClassSystem;
            IndicatorClassData indicatorClassGroup;
            IndicatorClassData indicatorClassDepartment;
            IndicatorClassData indicatorClassOperator;

            indicatorClassSystem = new IndicatorClassData();
            indicatorClassSystem.Name = "SYSTEM";
            indicatorClassSystem.FriendlyName = ResourceLoader.GetString2("System");
            indicatorClassSystem.Description = ResourceInitialDataLoader.GetString2("IndicatorClassSystemDescription");
            indicatorClassSystem = SmartCadDatabase.SaveNamedObject<IndicatorClassData>(indicatorClassSystem);

            indicatorClassGroup = new IndicatorClassData();
            indicatorClassGroup.Name = "GROUP";
            indicatorClassGroup.FriendlyName = ResourceLoader.GetString2("Group");
            indicatorClassGroup.Description = ResourceInitialDataLoader.GetString2("IndicatorClassGroupDescription");
            indicatorClassGroup = SmartCadDatabase.SaveNamedObject<IndicatorClassData>(indicatorClassGroup);

            indicatorClassDepartment = new IndicatorClassData();
            indicatorClassDepartment.Name = "DEPARTMENT";
            indicatorClassDepartment.FriendlyName = ResourceLoader.GetString2("Department");
            indicatorClassDepartment.Description = ResourceInitialDataLoader.GetString2("IndicatorClassDepartmentDescription");
            indicatorClassDepartment = SmartCadDatabase.SaveNamedObject<IndicatorClassData>(indicatorClassDepartment);

            indicatorClassOperator = new IndicatorClassData();
            indicatorClassOperator.Name = "OPERATOR";
            indicatorClassOperator.FriendlyName = ResourceLoader.GetString2("Operator");
            indicatorClassOperator.Description = ResourceInitialDataLoader.GetString2("IndicatorClassOperatorDescription");
            indicatorClassOperator = SmartCadDatabase.SaveNamedObject<IndicatorClassData>(indicatorClassOperator);

            IndicatorGroupForecastData indicatorGroupForecast = new IndicatorGroupForecastData();
            indicatorGroupForecast.General = true;
            indicatorGroupForecast.FactoryData = true;
            indicatorGroupForecast.Name = ResourceLoader.GetString2("PredeterminatedValues");
            indicatorGroupForecast.Description = ResourceLoader.GetString2("PredeterminatedValuesDescription");
            indicatorGroupForecast.Start = SmartCadDatabase.GetTimeFromBD();
            indicatorGroupForecast.End = DateTime.MaxValue;
            indicatorGroupForecast.ForecastValues = new ArrayList();
            indicatorGroupForecast = (IndicatorGroupForecastData)SmartCadDatabase.SaveOrUpdate(indicatorGroupForecast);


            IndicatorData indicator;
            IndicatorForecastData general;

            #region Indicator L01
            indicator = new IndicatorData();
            
            indicator.Classes = new Iesi.Collections.ListSet();
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassOperator, indicator));

            indicator.Dashboard = true;
            indicator.Description = ResourceLoader.GetString2("IndicatorL01Desc");
            indicator.MeasureUnit = ResourceLoader.GetString2("seg");
            indicator.Mnemonic = "ASA";
            indicator.Name = ResourceLoader.GetString2("IndicatorL01Name");
            indicator.CustomCode = "L01";
            indicator.Type = indicatorTypeCalls;
            indicator.Frecuency = 120;
            indicator.IndicatorLocation = IndicatorPosition.Up;
            indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);

            general = new IndicatorForecastData();
            general.High = 9;
            general.Indicator = indicator;
            general.Low = 3;
            general.Pattern = PositivePattern.Decrease;
            
            
            general.IndicatorGroupForecast = indicatorGroupForecast;
            SmartCadDatabase.SaveObject<IndicatorForecastData>(general);
            #endregion

            #region Indicator L02
            indicator = new IndicatorData();

            indicator.Classes = new Iesi.Collections.ListSet();
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));

            indicator.Dashboard = true;
            indicator.Description = ResourceLoader.GetString2("IndicatorL02Desc");
            indicator.MeasureUnit = ResourceLoader.GetString2("");
            indicator.Mnemonic = "ABAN";
            indicator.Name = ResourceLoader.GetString2("IndicatorL02Name");
            indicator.CustomCode = "L02";
            indicator.Type = indicatorTypeCalls;
            indicator.Frecuency = 60;
            indicator.IndicatorLocation = IndicatorPosition.Up;
            indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);

            general = new IndicatorForecastData();
            general.High = 5;
            general.Indicator = indicator;
            general.Low = 2;
            general.Pattern = PositivePattern.Decrease;


            general.IndicatorGroupForecast = indicatorGroupForecast;
            SmartCadDatabase.SaveObject<IndicatorForecastData>(general);
            #endregion

            #region Indicator L03
            indicator = new IndicatorData();

            indicator.Classes = new Iesi.Collections.ListSet();
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassOperator, indicator));

            indicator.Dashboard = true;
            indicator.Description = ResourceLoader.GetString2("IndicatorL03Desc");
            indicator.MeasureUnit = ResourceLoader.GetString2("seg");
            indicator.Mnemonic = "AHT";
            indicator.Name = ResourceLoader.GetString2("IndicatorL03Name");
            indicator.CustomCode = "L03";
            indicator.Type = indicatorTypeCalls;
            indicator.Frecuency = 300;
            indicator.IndicatorLocation = IndicatorPosition.Up;
            indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);

            general = new IndicatorForecastData();
            general.High = 180;
            general.Indicator = indicator;
            general.Low = 100;
            general.Pattern = PositivePattern.Decrease;
            
            
            general.IndicatorGroupForecast = indicatorGroupForecast;
            SmartCadDatabase.SaveObject<IndicatorForecastData>(general);
            #endregion

            #region Indicator L04
            indicator = new IndicatorData();

            indicator.Classes = new Iesi.Collections.ListSet();
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassOperator, indicator));

            indicator.Dashboard = true;
            indicator.Description = ResourceLoader.GetString2("IndicatorL04Desc");
            indicator.MeasureUnit = ResourceLoader.GetString2("seg");
            indicator.Name = ResourceLoader.GetString2("IndicatorL04Name");
            indicator.CustomCode = "L04";
            indicator.Type = indicatorTypeCalls;
            indicator.Frecuency = 600;
            indicator.IndicatorLocation = IndicatorPosition.Up;
            indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);

            general = new IndicatorForecastData();
            general.High = 20;
            general.Indicator = indicator;
            general.Low = 10;
            general.Pattern = PositivePattern.Decrease;
            
            
            general.IndicatorGroupForecast = indicatorGroupForecast;
            SmartCadDatabase.SaveObject<IndicatorForecastData>(general);
            #endregion

            #region Indicator L05
            //COMMENTED IN RELEASE 3.2.8 bataan A.A.
            //indicator = new IndicatorData();

            //indicator.Classes = new Iesi.Collections.ListSet();
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));

            //indicator.Dashboard = true;
            //indicator.Description = ResourceLoader.GetString2("IndicatorL05Desc");
            //indicator.MeasureUnit = ResourceLoader.GetString2("");
            //indicator.Mnemonic = "";
            //indicator.Name = ResourceLoader.GetString2("IndicatorL05Name");
            //indicator.CustomCode = "L05";
            //indicator.Type = indicatorTypeCalls;
            //indicator.Frecuency = 60;
            //indicator.IndicatorLocation = IndicatorPosition.Up;
            //indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);

            //general = new IndicatorForecastData();
            //general.High = 2;
            //general.Indicator = indicator;
            //general.Low = 1;
            //general.Pattern = PositivePattern.Decrease;

            //general.IndicatorGroupForecast = indicatorGroupForecast;
            //SmartCadDatabase.SaveObject<IndicatorForecastData>(general);
            #endregion

            #region Indicator L06
            indicator = new IndicatorData();

            indicator.Classes = new Iesi.Collections.ListSet();
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));

            indicator.Dashboard = true;
            indicator.Description = ResourceLoader.GetString2("IndicatorL06Desc");
            indicator.MeasureUnit = ResourceLoader.GetString2("seg");
            indicator.Mnemonic = "";
            indicator.Name = ResourceLoader.GetString2("IndicatorL06Name");
            indicator.CustomCode = "L06";
            indicator.Type = indicatorTypeCalls;
            indicator.Frecuency = 120;
            indicator.IndicatorLocation = IndicatorPosition.Up;
            indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);

            general = new IndicatorForecastData();
            general.High = 10;
            general.Indicator = indicator;
            general.Low = 8;
            general.Pattern = PositivePattern.Decrease;


            general.IndicatorGroupForecast = indicatorGroupForecast;
            SmartCadDatabase.SaveObject<IndicatorForecastData>(general);
            #endregion
            
            #region Indicator L07
            indicator = new IndicatorData();

            indicator.Classes = new Iesi.Collections.ListSet();
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassOperator, indicator));
            
            indicator.Dashboard = true;
            indicator.Description = ResourceLoader.GetString2("IndicatorL07Desc");
            indicator.MeasureUnit = ResourceLoader.GetString2("%");
            indicator.Name = ResourceLoader.GetString2("IndicatorL07Name");
            indicator.CustomCode = "L07";
            indicator.Type = indicatorTypeCalls;
            indicator.Frecuency = 600;
            indicator.IndicatorLocation = IndicatorPosition.Up;
            indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);

            general = new IndicatorForecastData();
            general.High = 80;
            general.Indicator = indicator;
            general.Low = 60;
            general.Pattern = PositivePattern.Decrease;
            
            general.IndicatorGroupForecast = indicatorGroupForecast;
            SmartCadDatabase.SaveObject<IndicatorForecastData>(general);
            #endregion

            #region doc L08/13

            //#region Indicator L08
            //indicator = new IndicatorData();

            //indicator.Classes = new Iesi.Collections.ListSet();
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassOperator, indicator));

            //indicator.Dashboard = false;
            //indicator.Description = ResourceLoader.GetString2("IndicatorL08Desc");
            ////indicator.MeasureUnit = ResourceLoader.GetString2("seg");
            //indicator.Name = ResourceLoader.GetString2("IndicatorL08Name");
            //indicator.CustomCode = "L08";
            //indicator.Type = indicatorTypeCalls;
            //indicator.Frecuency = 300;
            //indicator.IndicatorLocation = IndicatorPosition.Down;
            //indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);
            //#endregion

            //#region Indicator L13
            //indicator = new IndicatorData();

            //indicator.Classes = new Iesi.Collections.ListSet();
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassOperator, indicator));

            //indicator.Dashboard = false;
            //indicator.Description = ResourceLoader.GetString2("IndicatorL13Desc");
            ////indicator.MeasureUnit = ResourceLoader.GetString2("seg");
            //indicator.Name = ResourceLoader.GetString2("IndicatorL13Name");
            //indicator.CustomCode = "L13";
            //indicator.Type = indicatorTypeCalls;
            //indicator.Frecuency = 300;
            //indicator.IndicatorLocation = IndicatorPosition.Down;
            //indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);

            ////general = new IndicatorForecastData();
            ////general.High = 80;
            ////general.Indicator = indicator;
            ////general.Low = 60;
            ////general.Pattern = PositivePattern.Decrease;


            ////general.IndicatorGroupForecast = indicatorGroupForecast;
            ////SmartCadDatabase.SaveObject<IndicatorForecastData>(general);
            //#endregion

            #endregion doc L08/13

            #region Indicator L09
            indicator = new IndicatorData();

            indicator.Classes = new Iesi.Collections.ListSet();
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));

            indicator.Dashboard = true;
            indicator.Description = ResourceLoader.GetString2("IndicatorL09Desc");
            indicator.MeasureUnit = ResourceLoader.GetString2("");
            indicator.Name = ResourceLoader.GetString2("IndicatorL09Name");
            indicator.CustomCode = "L09";
            indicator.Mnemonic = "ANSW";
            indicator.Type = indicatorTypeCalls;
            indicator.Frecuency = 60;
            indicator.IndicatorLocation = IndicatorPosition.Up;
            indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);
            #endregion

            #region Indicator L10
            indicator = new IndicatorData();

            indicator.Classes = new Iesi.Collections.ListSet();
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassOperator, indicator));

            indicator.Dashboard = true;
            indicator.Description = ResourceLoader.GetString2("IndicatorL10Desc");
            indicator.Name = ResourceLoader.GetString2("IndicatorL10Name");
            indicator.CustomCode = "L10";
            indicator.Type = indicatorTypeCalls;
            indicator.Frecuency = 60;
            indicator.IndicatorLocation = IndicatorPosition.Up;
            SmartCadDatabase.SaveObject<IndicatorData>(indicator);
            #endregion

            #region Indicator L11
            //COMMENTED IN RELEASE 3.2.8 bataan A.A.
            //indicator = new IndicatorData();

            //indicator.Classes = new Iesi.Collections.ListSet();
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassOperator, indicator));

            //indicator.Dashboard = true;
            //indicator.Description = ResourceLoader.GetString2("IndicatorL11Desc");
            //indicator.MeasureUnit = ResourceLoader.GetString2("");
            //indicator.Name = ResourceLoader.GetString2("IndicatorL11Name");
            //indicator.CustomCode = "L11";
            //indicator.Type = indicatorTypeCalls;
            //indicator.Frecuency = 60;
            //indicator.IndicatorLocation = IndicatorPosition.Up;
            //indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);

            //general = new IndicatorForecastData();
            //general.High = 2;
            //general.Indicator = indicator;
            //general.Low = 1;
            //general.Pattern = PositivePattern.Decrease;


            //general.IndicatorGroupForecast = indicatorGroupForecast;
            //SmartCadDatabase.SaveObject<IndicatorForecastData>(general);
            #endregion

            #region Indicator L12
            indicator = new IndicatorData();

            indicator.Classes = new Iesi.Collections.ListSet();
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));

            indicator.Dashboard = true;
            indicator.Description = ResourceLoader.GetString2("IndicatorL12Desc");
            indicator.MeasureUnit = ResourceLoader.GetString2("%");
            indicator.Name = ResourceLoader.GetString2("IndicatorL12Name");
            indicator.CustomCode = "L12";
            indicator.Mnemonic = "TSF";
            indicator.Type = indicatorTypeCalls;
            indicator.Frecuency = 60;
            indicator.IndicatorLocation = IndicatorPosition.Up;
            indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);

            general = new IndicatorForecastData();
            general.High = 95;
            general.Indicator = indicator;
            general.Low = 90;
            general.Pattern = PositivePattern.Increase;

            general.IndicatorGroupForecast = indicatorGroupForecast;
            SmartCadDatabase.SaveObject<IndicatorForecastData>(general);
            #endregion

            

            #region doc PN01/03-05/15-18
            //#region Indicator PN01
            //indicator = new IndicatorData();

            //indicator.Classes = new Iesi.Collections.ListSet();
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));            

            
            //indicator.Dashboard = false;
            //indicator.Description = ResourceLoader.GetString2("IndicatorPN01Desc");
            //indicator.MeasureUnit = ResourceLoader.GetString2("%");
            //indicator.Name = ResourceLoader.GetString2("IndicatorPN01Name");
            //indicator.CustomCode = "PN01";
            //indicator.Type = indicatorTypeFirstLevel;
            //indicator.Frecuency = 60;
            //indicator.IndicatorLocation = IndicatorPosition.Down;
            //indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);

            ///*general = new IndicatorForecastData();
            //general.High = 10;
            //general.Indicator = indicator;
            //general.Low = 5;
            //general.Pattern = PositivePattern.Maintain;
            
            
            //general.IndicatorGroupForecast = indicatorGroupForecast;
            //SmartCadDatabase.SaveObject<IndicatorForecastData>(general);*/
            //#endregion

            //#region Indicator PN03
            //indicator = new IndicatorData();

            //indicator.Classes = new Iesi.Collections.ListSet();
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));


            //indicator.Dashboard = false;
            //indicator.Description = ResourceLoader.GetString2("IndicatorPN03Desc");
            //indicator.MeasureUnit = ResourceLoader.GetString2("%");
            //indicator.Name = ResourceLoader.GetString2("IndicatorPN03Name");
            //indicator.CustomCode = "PN03";
            //indicator.Type = indicatorTypeFirstLevel;
            //indicator.Frecuency = 60;
            //indicator.IndicatorLocation = IndicatorPosition.Down;
            //SmartCadDatabase.SaveObject<IndicatorData>(indicator);
            //#endregion

            //#region Indicator PN04
            //indicator = new IndicatorData();

            //indicator.Classes = new Iesi.Collections.ListSet();
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));

            //indicator.Dashboard = false;
            //indicator.Description = ResourceLoader.GetString2("IndicatorPN04Desc");
            //indicator.MeasureUnit = ResourceLoader.GetString2("%");
            //indicator.Name = ResourceLoader.GetString2("IndicatorPN04Name");
            //indicator.CustomCode = "PN04";
            //indicator.Type = indicatorTypeFirstLevel;
            //indicator.Frecuency = 60;
            //indicator.IndicatorLocation = IndicatorPosition.Down;
            //indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);

            ///*general = new IndicatorForecastData();
            //general.High = 98;
            //general.Indicator = indicator;
            //general.Low = 96;
            //general.Pattern = PositivePattern.Increase;
            
            
            //general.IndicatorGroupForecast = indicatorGroupForecast;
            //SmartCadDatabase.SaveObject<IndicatorForecastData>(general);*/
            //#endregion

            //#region Indicator PN05
            //indicator = new IndicatorData();

            //indicator.Classes = new Iesi.Collections.ListSet();
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));

            //indicator.Dashboard = false;
            //indicator.Description = ResourceLoader.GetString2("IndicatorPN05Desc");
            //indicator.MeasureUnit = ResourceLoader.GetString2("%");
            //indicator.Name = ResourceLoader.GetString2("IndicatorPN05Name");
            //indicator.CustomCode = "PN05";
            //indicator.Type = indicatorTypeFirstLevel;
            //indicator.Frecuency = 60;
            //indicator.IndicatorLocation = IndicatorPosition.Down;
            //indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);

            ///*general = new IndicatorForecastData();
            //general.High = 5;
            //general.Indicator = indicator;
            //general.Low = 2;
            //general.Pattern = PositivePattern.Decrease;
            
            
            //general.IndicatorGroupForecast = indicatorGroupForecast;
            //SmartCadDatabase.SaveObject<IndicatorForecastData>(general);*/
            //#endregion

            //#region Indicator PN15
            //indicator = new IndicatorData();

            //indicator.Classes = new Iesi.Collections.ListSet();
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));


            //indicator.Dashboard = false;
            //indicator.Description = ResourceLoader.GetString2("IndicatorPN15Desc");
            //indicator.MeasureUnit = ResourceLoader.GetString2("%");
            //indicator.Name = ResourceLoader.GetString2("IndicatorPN15Name");
            //indicator.CustomCode = "PN15";
            //indicator.Type = indicatorTypeFirstLevel;
            //indicator.Frecuency = 60;
            //indicator.IndicatorLocation = IndicatorPosition.Down;
            //SmartCadDatabase.SaveObject<IndicatorData>(indicator);
            //#endregion

            //#region Indicator PN16
            //indicator = new IndicatorData();

            //indicator.Classes = new Iesi.Collections.ListSet();
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));


            //indicator.Dashboard = false;
            //indicator.Description = ResourceLoader.GetString2("IndicatorPN16Desc");
            //indicator.MeasureUnit = ResourceLoader.GetString2("%");
            //indicator.Name = ResourceLoader.GetString2("IndicatorPN16Name");
            //indicator.CustomCode = "PN16";
            //indicator.Type = indicatorTypeFirstLevel;
            //indicator.Frecuency = 60;
            //indicator.IndicatorLocation = IndicatorPosition.Down;
            //SmartCadDatabase.SaveObject<IndicatorData>(indicator);
            //#endregion

            //#region Indicator PN17
            //indicator = new IndicatorData();

            //indicator.Classes = new Iesi.Collections.ListSet();
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));


            //indicator.Dashboard = false;
            //indicator.Description = ResourceLoader.GetString2("IndicatorPN17Desc");
            //indicator.MeasureUnit = ResourceLoader.GetString2("%");
            //indicator.Name = ResourceLoader.GetString2("IndicatorPN17Name");
            //indicator.CustomCode = "PN17";
            //indicator.Type = indicatorTypeFirstLevel;
            //indicator.Frecuency = 60;
            //indicator.IndicatorLocation = IndicatorPosition.Down;
            //SmartCadDatabase.SaveObject<IndicatorData>(indicator);
            //#endregion

            //#region Indicator PN18
            //indicator = new IndicatorData();

            //indicator.Classes = new Iesi.Collections.ListSet();
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));


            //indicator.Dashboard = false;
            //indicator.Description = ResourceLoader.GetString2("IndicatorPN18Desc");
            //indicator.MeasureUnit = ResourceLoader.GetString2("%");
            //indicator.Name = ResourceLoader.GetString2("IndicatorPN18Name");
            //indicator.CustomCode = "PN18";
            //indicator.Type = indicatorTypeFirstLevel;
            //indicator.Frecuency = 60;
            //indicator.IndicatorLocation = IndicatorPosition.Down;
            //SmartCadDatabase.SaveObject<IndicatorData>(indicator);
            //#endregion

            #endregion doc PN01/03-05/15-18

            #region Indicator PN02
            indicator = new IndicatorData();

            indicator.Classes = new Iesi.Collections.ListSet();
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));


            indicator.Dashboard = true;
            indicator.Description = ResourceLoader.GetString2("IndicatorPN02Desc");
            indicator.MeasureUnit = ResourceLoader.GetString2("%");
            indicator.Name = ResourceLoader.GetString2("IndicatorPN02Name");
            indicator.CustomCode = "PN02";
            indicator.Type = indicatorTypeFirstLevel;
            indicator.Frecuency = 60;
            indicator.IndicatorLocation = IndicatorPosition.Up;
            indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);

            general = new IndicatorForecastData();
            general.High = 5;
            general.Indicator = indicator;
            general.Low = 2;
            general.Pattern = PositivePattern.Decrease;
            
            general.IndicatorGroupForecast = indicatorGroupForecast;
            SmartCadDatabase.SaveObject<IndicatorForecastData>(general);
            #endregion
                    
            #region Indicator PN06
            indicator = new IndicatorData();

            indicator.Classes = new Iesi.Collections.ListSet();
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassOperator, indicator));

            indicator.Dashboard = true;
            indicator.Description = ResourceLoader.GetString2("IndicatorPN06Desc");
            indicator.MeasureUnit = ResourceLoader.GetString2("%");
            indicator.Name = ResourceLoader.GetString2("IndicatorPN06Name");
            indicator.CustomCode = "PN06";
            indicator.Type = indicatorTypeFirstLevel;
            indicator.Frecuency = 300;
            indicator.IndicatorLocation = IndicatorPosition.Up;
            SmartCadDatabase.SaveObject<IndicatorData>(indicator);
            #endregion

            #region Indicator PN07
            indicator = new IndicatorData();

            indicator.Classes = new Iesi.Collections.ListSet();
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassOperator, indicator));

            indicator.Dashboard = true;
            indicator.Description = ResourceLoader.GetString2("IndicatorPN07Desc");
            indicator.MeasureUnit = ResourceLoader.GetString2("%");
            indicator.Name = ResourceLoader.GetString2("IndicatorPN07Name");
            indicator.CustomCode = "PN07";
            indicator.Type = indicatorTypeFirstLevel;
            indicator.Frecuency = 300;
            indicator.IndicatorLocation = IndicatorPosition.Up;
            SmartCadDatabase.SaveObject<IndicatorData>(indicator);
            #endregion

            #region Indicator PN08
            indicator = new IndicatorData();

            indicator.Classes = new Iesi.Collections.ListSet();
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));

            indicator.Dashboard = true;
            indicator.Description = ResourceLoader.GetString2("IndicatorPN08Desc");
            indicator.MeasureUnit =  ResourceLoader.GetString2("%");
            indicator.Name = ResourceLoader.GetString2("IndicatorPN08Name");
            indicator.CustomCode = "PN08";
            indicator.Type = indicatorTypeFirstLevel;
            indicator.Frecuency = 900;//604800;
            indicator.IndicatorLocation = IndicatorPosition.Up;
            indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);

            general = new IndicatorForecastData();
            general.High = 15;
            general.Indicator = indicator;
            general.Low = 5;
            general.Pattern = PositivePattern.Decrease;
            
            
            general.IndicatorGroupForecast = indicatorGroupForecast;
            SmartCadDatabase.SaveObject<IndicatorForecastData>(general);
            #endregion

            #region Indicator PN10
            indicator = new IndicatorData();

            indicator.Classes = new Iesi.Collections.ListSet();
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassOperator, indicator));

            indicator.Dashboard = true;
            indicator.Description = ResourceLoader.GetString2("IndicatorPN10Desc");
            indicator.MeasureUnit = ResourceLoader.GetString2("%");
            indicator.Name = ResourceLoader.GetString2("IndicatorPN10Name");
            indicator.CustomCode = "PN10";
            indicator.Type = indicatorTypeFirstLevel;
            indicator.Frecuency = 120;
            indicator.IndicatorLocation = IndicatorPosition.Up;
            indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);

            general = new IndicatorForecastData();
            general.Low = 90;
            general.Indicator = indicator;
            general.High = 95;
            general.Pattern = PositivePattern.Increase;
            
            
            general.IndicatorGroupForecast = indicatorGroupForecast;
            SmartCadDatabase.SaveObject<IndicatorForecastData>(general);
            #endregion

            #region Indicator PN12
            indicator = new IndicatorData();

            indicator.Classes = new Iesi.Collections.ListSet();
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassOperator, indicator));
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));
            
            indicator.Dashboard = true;
            indicator.Description = ResourceLoader.GetString2("IndicatorPN12Desc");
            indicator.MeasureUnit = ResourceLoader.GetString2("seg");
            indicator.Name = ResourceLoader.GetString2("IndicatorPN12Name");
            indicator.CustomCode = "PN12";
            indicator.Type = indicatorTypeFirstLevel;
            indicator.Frecuency = 300;
            indicator.IndicatorLocation = IndicatorPosition.Up;
            indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);

            general = new IndicatorForecastData();
            general.Low = 80;
            general.Indicator = indicator;
            general.High = 90;
            general.Pattern = PositivePattern.Decrease;
            
            
            general.IndicatorGroupForecast = indicatorGroupForecast;
            SmartCadDatabase.SaveObject<IndicatorForecastData>(general);
            #endregion

            #region Indicator PN13
            indicator = new IndicatorData();

            indicator.Classes = new Iesi.Collections.ListSet();
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassOperator, indicator));
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));

            indicator.Dashboard = true;
            indicator.Description = ResourceLoader.GetString2("IndicatorPN13Desc");
            indicator.MeasureUnit =  ResourceLoader.GetString2("seg");
            indicator.Name = ResourceLoader.GetString2("IndicatorPN13Name");
            indicator.CustomCode = "PN13";
            indicator.Type = indicatorTypeFirstLevel;
            indicator.Frecuency = 300;
            indicator.IndicatorLocation = IndicatorPosition.Up;
            indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);

            general = new IndicatorForecastData();
            general.Low = 80;
            general.Indicator = indicator;
            general.High = 90;
            general.Pattern = PositivePattern.Decrease;
            
            
            general.IndicatorGroupForecast = indicatorGroupForecast;
            SmartCadDatabase.SaveObject<IndicatorForecastData>(general);
            #endregion

            #region Indicator PN14
            indicator = new IndicatorData();

            indicator.Classes = new Iesi.Collections.ListSet();         
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassOperator, indicator));

            indicator.Dashboard = true;
            indicator.Description = ResourceLoader.GetString2("IndicatorPN14Desc");
            indicator.MeasureUnit = ResourceLoader.GetString2("seg");
            indicator.Name = ResourceLoader.GetString2("IndicatorPN14Name");
            indicator.CustomCode = "PN14";
            indicator.Type = indicatorTypeFirstLevel;
            indicator.Frecuency = 300;
            indicator.IndicatorLocation = IndicatorPosition.Up;
            indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);

            general = new IndicatorForecastData();
            general.Low = 15;
            general.Indicator = indicator;
            general.High = 30;
            general.Pattern = PositivePattern.Decrease;


            general.IndicatorGroupForecast = indicatorGroupForecast;
            SmartCadDatabase.SaveObject<IndicatorForecastData>(general);
            #endregion          

            #region Indicator PN19
            indicator = new IndicatorData();
            indicator.Classes = new Iesi.Collections.ListSet();            
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));
            indicator.Dashboard = false;
            indicator.Description = ResourceLoader.GetString2("IndicatorPN19Desc");            
            indicator.Name = ResourceLoader.GetString2("IndicatorPN19Name");
            indicator.CustomCode = "PN19";
            indicator.Type = indicatorTypeFirstLevel;
            indicator.Frecuency = 60;
            indicator.IndicatorLocation = IndicatorPosition.Down;
            SmartCadDatabase.SaveObject<IndicatorData>(indicator);
            #endregion

            #region Indicator PN20
            indicator = new IndicatorData();

            indicator.Classes = new Iesi.Collections.ListSet();
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));

            indicator.Dashboard = false;
            indicator.Description = ResourceLoader.GetString2("IndicatorPN20Desc");
            indicator.MeasureUnit = ResourceLoader.GetString2("sec");
            indicator.Name = ResourceLoader.GetString2("IndicatorPN20Name");
            indicator.CustomCode = "PN20";
            indicator.Type = indicatorTypeFirstLevel;
            indicator.Frecuency = 60;
            indicator.IndicatorLocation = IndicatorPosition.Down;
            indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);          
            #endregion

            #region New indicators for first level 

            #region doc PND15/16/17/22/23
            //#region Indicator PND15
            //indicator = new IndicatorData();

            //indicator.Classes = new Iesi.Collections.ListSet();
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassOperator, indicator));

            //indicator.Dashboard = false;
            //indicator.Description = ResourceLoader.GetString2("IndicatorPND15Desc");
            //indicator.MeasureUnit = ResourceLoader.GetString2("seg");
            //indicator.Name = ResourceLoader.GetString2("IndicatorPND15Name");
            //indicator.CustomCode = "PND15";
            //indicator.Type = indicatorTypeFirstLevel;
            //indicator.Frecuency = 300;
            //indicator.IndicatorLocation = IndicatorPosition.Down;
            //indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);
            //#endregion

            //#region Indicator PND16
            //indicator = new IndicatorData();

            //indicator.Classes = new Iesi.Collections.ListSet();
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassOperator, indicator));

            //indicator.Dashboard = false;
            //indicator.Description = ResourceLoader.GetString2("IndicatorPND16Desc");
            //indicator.MeasureUnit = ResourceLoader.GetString2("seg");
            //indicator.Name = ResourceLoader.GetString2("IndicatorPND16Name");
            //indicator.CustomCode = "PND16";
            //indicator.Type = indicatorTypeFirstLevel;
            //indicator.Frecuency = 300;
            //indicator.IndicatorLocation = IndicatorPosition.Down;
            //indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);
            //#endregion
        
            //#region Indicator PND17
            ////indicator = new IndicatorData();

            ////indicator.Classes = new Iesi.Collections.ListSet();
            ////indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));
            ////indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));

            ////indicator.Dashboard = false;
            ////indicator.Description = ResourceLoader.GetString2("IndicatorPND17Desc");
            ////indicator.MeasureUnit = ResourceLoader.GetString2("seg");
            ////indicator.Name = ResourceLoader.GetString2("IndicatorPND17Name");
            ////indicator.CustomCode = "PND17";
            ////indicator.Type = indicatorTypeFirstLevel;
            ////indicator.Frecuency = 60;
            ////indicator.IndicatorLocation = IndicatorPosition.Down;
            ////indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);
            //#endregion           

            //#region Indicator PND22
            //indicator = new IndicatorData();

            //indicator.Classes = new Iesi.Collections.ListSet();
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));

            //indicator.Dashboard = false;
            //indicator.Description = ResourceLoader.GetString2("IndicatorPND17Desc");
            //indicator.MeasureUnit = ResourceLoader.GetString2("seg");
            //indicator.Name = ResourceLoader.GetString2("IndicatorPND22Name");
            //indicator.CustomCode = "PND22";
            //indicator.Type = indicatorTypeFirstLevel;
            //indicator.Frecuency = 300;
            //indicator.IndicatorLocation = IndicatorPosition.Down;
            //indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);
            //#endregion

            //#region Indicator PND23
            //indicator = new IndicatorData();

            //indicator.Classes = new Iesi.Collections.ListSet();
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));

            //indicator.Dashboard = false;
            //indicator.Description = ResourceLoader.GetString2("IndicatorPND17Desc");
            //indicator.MeasureUnit = ResourceLoader.GetString2("seg");
            //indicator.Name = ResourceLoader.GetString2("IndicatorPND23Name");
            //indicator.CustomCode = "PND23";
            //indicator.Type = indicatorTypeFirstLevel;
            //indicator.Frecuency = 300;
            //indicator.IndicatorLocation = IndicatorPosition.Down;
            //indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);
            //#endregion

            #endregion doc PND15/16/17/22/23

            #region Indicator PND24
            indicator = new IndicatorData();

            indicator.Classes = new Iesi.Collections.ListSet();
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassOperator, indicator));

            indicator.Dashboard = false;
            indicator.Description = ResourceLoader.GetString2("IndicatorPND15Desc");
            indicator.MeasureUnit = ResourceLoader.GetString2("seg");
            indicator.Name = ResourceLoader.GetString2("IndicatorPND24Name");
            indicator.CustomCode = "PND24";
            indicator.Type = indicatorTypeFirstLevel;
            indicator.Frecuency = 300;
            indicator.IndicatorLocation = IndicatorPosition.Down;
            indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);
            #endregion

            #region Indicator PND25
            indicator = new IndicatorData();

            indicator.Classes = new Iesi.Collections.ListSet();
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));

            indicator.Dashboard = false;
            indicator.Description = ResourceLoader.GetString2("IndicatorPND25Desc");
            indicator.MeasureUnit = ResourceLoader.GetString2("%");
            indicator.Name = ResourceLoader.GetString2("IndicatorPND25Name");
            indicator.CustomCode = "PND25";
            indicator.Type = indicatorTypeFirstLevel;
            indicator.Frecuency = 300;
            indicator.IndicatorLocation = IndicatorPosition.Down;
            indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);
            #endregion

            #endregion New indicators for first level

            #region New indicators for Dispatch

            #region doc DPN15/16/17/18/22/23
            //#region Indicator DPN15
            //indicator = new IndicatorData();

            //indicator.Classes = new Iesi.Collections.ListSet();
            ////indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassOperator, indicator));
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassDepartment, indicator));

            //indicator.Dashboard = false;
            //indicator.Description = ResourceLoader.GetString2("IndicatorDPN15Desc");
            //indicator.MeasureUnit = ResourceLoader.GetString2("seg");
            //indicator.Name = ResourceLoader.GetString2("IndicatorDPN15Name");
            //indicator.CustomCode = "DPN15";
            //indicator.Type = indicatorTypeDispatch;
            //indicator.Frecuency = 300;
            //indicator.IndicatorLocation = IndicatorPosition.Down;
            //indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);
            //#endregion

            //#region Indicator DPN16
            //indicator = new IndicatorData();

            //indicator.Classes = new Iesi.Collections.ListSet();
            ////indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassOperator, indicator));
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassDepartment, indicator));

            //indicator.Dashboard = false;
            //indicator.Description = ResourceLoader.GetString2("IndicatorDPN16Desc");
            //indicator.MeasureUnit = ResourceLoader.GetString2("seg");
            //indicator.Name = ResourceLoader.GetString2("IndicatorDPN16Name");
            //indicator.CustomCode = "DPN16";
            //indicator.Type = indicatorTypeDispatch;
            //indicator.Frecuency = 300;
            //indicator.IndicatorLocation = IndicatorPosition.Down;
            //indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);
            //#endregion          

            //#region Indicator DPN17
            //indicator = new IndicatorData();
            //indicator.Classes = new Iesi.Collections.ListSet();
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));           
            //indicator.Dashboard = false;
            //indicator.Description = ResourceLoader.GetString2("IndicatorDPN17Desc");
            //indicator.MeasureUnit = ResourceLoader.GetString2("seg");
            //indicator.Name = ResourceLoader.GetString2("IndicatorDPN17Name");
            //indicator.CustomCode = "DPN17";
            //indicator.Type = indicatorTypeDispatch;
            //indicator.Frecuency = 300;
            //indicator.IndicatorLocation = IndicatorPosition.Down;
            //indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);
            //#endregion

            //#region Indicator DPN18
            //indicator = new IndicatorData();

            //indicator.Classes = new Iesi.Collections.ListSet();
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));
            //indicator.Dashboard = false;
            //indicator.Description = ResourceLoader.GetString2("IndicatorDPN18Desc");
            //indicator.MeasureUnit = ResourceLoader.GetString2("seg");
            //indicator.Name = ResourceLoader.GetString2("IndicatorDPN18Name");
            //indicator.CustomCode = "DPN18";
            //indicator.Type = indicatorTypeDispatch;
            //indicator.Frecuency = 300;
            //indicator.IndicatorLocation = IndicatorPosition.Down;
            //indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);
            //#endregion          

            //#region Indicator DPN22
            //indicator = new IndicatorData();

            //indicator.Classes = new Iesi.Collections.ListSet();
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));

            //indicator.Dashboard = false;
            //indicator.Description = ResourceLoader.GetString2("IndicatorDPN17Desc");
            //indicator.MeasureUnit = ResourceLoader.GetString2("seg");
            //indicator.Name = ResourceLoader.GetString2("IndicatorDPN22Name");
            //indicator.CustomCode = "DPN22";
            //indicator.Type = indicatorTypeDispatch;
            //indicator.Frecuency = 300;
            //indicator.IndicatorLocation = IndicatorPosition.Down;
            //indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);
            //#endregion

            //#region Indicator DPN23
            //indicator = new IndicatorData();

            //indicator.Classes = new Iesi.Collections.ListSet();
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));

            //indicator.Dashboard = false;
            //indicator.Description = ResourceLoader.GetString2("IndicatorDPN17Desc");
            //indicator.MeasureUnit = ResourceLoader.GetString2("seg");
            //indicator.Name = ResourceLoader.GetString2("IndicatorDPN23Name");
            //indicator.CustomCode = "DPN23";
            //indicator.Type = indicatorTypeDispatch;
            //indicator.Frecuency = 300;
            //indicator.IndicatorLocation = IndicatorPosition.Down;
            //indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);
            //#endregion

            #endregion doc DPN15/16/17/18/22/23

            #region Indicator DPN24
            indicator = new IndicatorData();
            indicator.Classes = new Iesi.Collections.ListSet();       
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassOperator, indicator));
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassDepartment, indicator));
            indicator.Dashboard = false;
            indicator.Description = ResourceLoader.GetString2("IndicatorDPN24Desc");
            indicator.MeasureUnit = ResourceLoader.GetString2("seg");
            indicator.Name = ResourceLoader.GetString2("IndicatorDPN24Name");
            indicator.CustomCode = "DPN24";
            indicator.Type = indicatorTypeDispatch;
            indicator.Frecuency = 300;
            indicator.IndicatorLocation = IndicatorPosition.Down;
            indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);
            #endregion

            #region Indicator DPN25
            indicator = new IndicatorData();
            indicator.Classes = new Iesi.Collections.ListSet();
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));
            indicator.Dashboard = false;
            indicator.Description = ResourceLoader.GetString2("IndicatorDPN25Desc");
            indicator.MeasureUnit = ResourceLoader.GetString2("seg");
            indicator.Name = ResourceLoader.GetString2("IndicatorDPN25Name");
            indicator.CustomCode = "DPN25";
            indicator.Type = indicatorTypeDispatch;
            indicator.Frecuency = 300;
            indicator.IndicatorLocation = IndicatorPosition.Down;
            indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);
            #endregion

            #endregion New indicators for Dispatch

            #region Indicator D01
            indicator = new IndicatorData();

            indicator.Classes = new Iesi.Collections.ListSet();
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassDepartment, indicator));

            indicator.Dashboard = false;
            indicator.Description = ResourceLoader.GetString2("IndicatorD01Desc");
            indicator.MeasureUnit = ResourceLoader.GetString2("seg");
            indicator.Name = ResourceLoader.GetString2("IndicatorD01Name");
            indicator.CustomCode = "D01";
            indicator.Type = indicatorTypeDispatch;
            indicator.Frecuency = 600;
            indicator.IndicatorLocation = IndicatorPosition.Up;
            SmartCadDatabase.SaveObject<IndicatorData>(indicator);
            #endregion

            #region Indicator D04
            indicator = new IndicatorData();

            indicator.Classes = new Iesi.Collections.ListSet();
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));           
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassDepartment, indicator));
            indicator.Dashboard = false;            
            indicator.Description = ResourceLoader.GetString2("IndicatorD04Desc");
            indicator.MeasureUnit = ResourceLoader.GetString2("seg");
            indicator.Name = ResourceLoader.GetString2("IndicatorD04Name");
            indicator.CustomCode = "D04";
            indicator.Type = indicatorTypeDispatch;
            indicator.Frecuency = 180;
            indicator.IndicatorLocation = IndicatorPosition.Up;
            SmartCadDatabase.SaveObject<IndicatorData>(indicator);
            #endregion

            #region Indicator D07
            indicator = new IndicatorData();

            indicator.Classes = new Iesi.Collections.ListSet();
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassDepartment, indicator));

            indicator.Dashboard = false;
            indicator.Description = ResourceLoader.GetString2("IndicatorD07Desc");
            indicator.MeasureUnit = ResourceLoader.GetString2("");
            indicator.Name = ResourceLoader.GetString2("IndicatorD07Name");
            indicator.CustomCode = "D07";
            indicator.Type = indicatorTypeDispatch;
            indicator.Frecuency = 300;
            indicator.IndicatorLocation = IndicatorPosition.Down;
            indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);
            #endregion

            #region Indicator D08
            indicator = new IndicatorData();

            indicator.Classes = new Iesi.Collections.ListSet();
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassDepartment, indicator));
            
            indicator.Dashboard = false;
            indicator.Description = ResourceLoader.GetString2("IndicatorD08Desc");
            indicator.MeasureUnit = ResourceLoader.GetString2("%");
            indicator.Name = ResourceLoader.GetString2("IndicatorD08Name");
            indicator.CustomCode = "D08";
            indicator.Type = indicatorTypeDispatch;
            indicator.Frecuency = 300;
            indicator.IndicatorLocation = IndicatorPosition.Up;
            indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);
            #endregion

            #region Indicator D11
            indicator = new IndicatorData();

            indicator.Classes = new Iesi.Collections.ListSet();
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassDepartment, indicator));

            indicator.Dashboard = false;
            indicator.Description = ResourceLoader.GetString2("IndicatorD11Desc");
            indicator.MeasureUnit = ResourceLoader.GetString2("%");
            indicator.Name = ResourceLoader.GetString2("IndicatorD11Name");
            indicator.CustomCode = "D11";
            indicator.Type = indicatorTypeDispatch;
            indicator.Frecuency = 600;
            indicator.IndicatorLocation = IndicatorPosition.Down;
            SmartCadDatabase.SaveObject<IndicatorData>(indicator);
            #endregion

            #region Indicator D13
            indicator = new IndicatorData();

            indicator.Classes = new Iesi.Collections.ListSet();
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassDepartment, indicator));

            indicator.Dashboard = true;
            indicator.Description = ResourceLoader.GetString2("IndicatorD13Desc");
            indicator.MeasureUnit = ResourceLoader.GetString2("");
            indicator.Name = ResourceLoader.GetString2("IndicatorD13Name");
            indicator.CustomCode = "D13";
            indicator.Type = indicatorTypeDispatch;
            indicator.Frecuency = 600;
            indicator.IndicatorLocation = IndicatorPosition.Up;
            SmartCadDatabase.SaveObject<IndicatorData>(indicator);
            #endregion

            #region Indicator D14
            indicator = new IndicatorData();

            indicator.Classes = new Iesi.Collections.ListSet();
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassDepartment, indicator));

            indicator.Dashboard = false;
            indicator.Description = ResourceLoader.GetString2("IndicatorD14Desc");
            indicator.MeasureUnit = ResourceLoader.GetString2("%");
            indicator.Name = ResourceLoader.GetString2("IndicatorD14Name");
            indicator.CustomCode = "D14";
            indicator.Type = indicatorTypeDispatch;
            indicator.Frecuency = 300;
            indicator.IndicatorLocation = IndicatorPosition.Up;
            SmartCadDatabase.SaveObject<IndicatorData>(indicator);
            #endregion

            #region Indicator D15
            indicator = new IndicatorData();

            indicator.Classes = new Iesi.Collections.ListSet();
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassDepartment, indicator));

            indicator.Dashboard = false;
            indicator.Description = ResourceLoader.GetString2("IndicatorD15Desc");
            indicator.MeasureUnit = ResourceLoader.GetString2("%");
            indicator.Name = ResourceLoader.GetString2("IndicatorD15Name");
            indicator.CustomCode = "D15";
            indicator.Type = indicatorTypeDispatch;
            indicator.Frecuency = 300;
            indicator.IndicatorLocation = IndicatorPosition.Up;
            SmartCadDatabase.SaveObject<IndicatorData>(indicator);
            #endregion

            #region Indicator D16
            indicator = new IndicatorData();

            indicator.Classes = new Iesi.Collections.ListSet();
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassDepartment, indicator));

            indicator.Dashboard = true;
            indicator.Description = ResourceLoader.GetString2("IndicatorD16Desc");
            indicator.MeasureUnit = ResourceLoader.GetString2("");
            indicator.Name = ResourceLoader.GetString2("IndicatorD16Name");
            indicator.CustomCode = "D16";
            indicator.Type = indicatorTypeDispatch;
            indicator.Frecuency = 900;
            indicator.IndicatorLocation = IndicatorPosition.Up;
            SmartCadDatabase.SaveObject<IndicatorData>(indicator);
            #endregion

            #region Indicator D17
            indicator = new IndicatorData();

            indicator.Classes = new Iesi.Collections.ListSet();
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassDepartment, indicator));

            indicator.Dashboard = true;
            indicator.Description = ResourceLoader.GetString2("IndicatorD17Desc");
            indicator.MeasureUnit = ResourceLoader.GetString2("");
            indicator.Name = ResourceLoader.GetString2("IndicatorD17Name");
            indicator.CustomCode = "D17";
            indicator.Type = indicatorTypeDispatch;
            indicator.Frecuency = 900;
            indicator.IndicatorLocation = IndicatorPosition.Up;
            SmartCadDatabase.SaveObject<IndicatorData>(indicator);
            #endregion
            
            #region Indicator D21
            indicator = new IndicatorData();

            indicator.Classes = new Iesi.Collections.ListSet();
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));

            indicator.Dashboard = true;
            indicator.Description = ResourceLoader.GetString2("IndicatorD21Desc");
            indicator.MeasureUnit = ResourceLoader.GetString2("%");
            indicator.Name = ResourceLoader.GetString2("IndicatorD21Name");
            indicator.CustomCode = "D21";
            indicator.Type = indicatorTypeDispatch;
            indicator.Frecuency = 60;
            indicator.IndicatorLocation = IndicatorPosition.Up;
            indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);

            general = new IndicatorForecastData();
            general.Low = 2;
            general.Indicator = indicator;
            general.High = 5;
            general.Pattern = PositivePattern.Decrease;
            
            
            general.IndicatorGroupForecast = indicatorGroupForecast;
            SmartCadDatabase.SaveObject<IndicatorForecastData>(general);
            #endregion
           
            #region Indicator D24
            indicator = new IndicatorData();

            indicator.Classes = new Iesi.Collections.ListSet();
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));

            indicator.Dashboard = true;
            indicator.Description = ResourceLoader.GetString2("IndicatorD24Desc");
            indicator.MeasureUnit = ResourceLoader.GetString2("%");
            indicator.Name = ResourceLoader.GetString2("IndicatorD24Name");
            indicator.CustomCode = "D24";
            indicator.Type = indicatorTypeDispatch;
            indicator.Frecuency = 900;
            indicator.IndicatorLocation = IndicatorPosition.Up;
            SmartCadDatabase.SaveObject<IndicatorData>(indicator);


            general = new IndicatorForecastData();
            general.Low = 5;
            general.Indicator = indicator;
            general.High = 15;
            general.Pattern = PositivePattern.Decrease;


            general.IndicatorGroupForecast = indicatorGroupForecast;
            SmartCadDatabase.SaveObject<IndicatorForecastData>(general);

            #endregion

            #region Indicator D25
            indicator = new IndicatorData();

            indicator.Classes = new Iesi.Collections.ListSet();
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassDepartment, indicator));

            indicator.Dashboard = false;
            indicator.Description = ResourceLoader.GetString2("IndicatorD25Desc");
            indicator.MeasureUnit = ResourceLoader.GetString2("seg");
            indicator.Name = ResourceLoader.GetString2("IndicatorD25Name");
            indicator.CustomCode = "D25";
            indicator.Type = indicatorTypeDispatch;
            indicator.Frecuency = 300;
            indicator.IndicatorLocation = IndicatorPosition.Up;
            indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);

            #endregion

            #region Indicator D26
            indicator = new IndicatorData();

            indicator.Classes = new Iesi.Collections.ListSet();
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassOperator, indicator));

            indicator.Dashboard = true;
            indicator.Description = ResourceLoader.GetString2("IndicatorD26Desc");
            indicator.MeasureUnit = ResourceLoader.GetString2("%");
            indicator.Name = ResourceLoader.GetString2("IndicatorD26Name");
            indicator.CustomCode = "D26";
            indicator.Type = indicatorTypeDispatch;
            indicator.Frecuency = 120;
            indicator.IndicatorLocation = IndicatorPosition.Up;
            indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);

            general = new IndicatorForecastData();
            general.Low = 95;
            general.Indicator = indicator;
            general.High = 98;
            general.Pattern = PositivePattern.Increase;
            
            
            general.IndicatorGroupForecast = indicatorGroupForecast;
            SmartCadDatabase.SaveObject<IndicatorForecastData>(general);
            #endregion

            #region Indicator D27
            indicator = new IndicatorData();

            indicator.Classes = new Iesi.Collections.ListSet();
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassDepartment, indicator));

            indicator.Dashboard = false;
            indicator.Description = ResourceLoader.GetString2("IndicatorD27Desc");
            indicator.MeasureUnit = ResourceLoader.GetString2("");
            indicator.Name = ResourceLoader.GetString2("IndicatorD27Name");
            indicator.CustomCode = "D27";
            indicator.Type = indicatorTypeDispatch;
            indicator.Frecuency = 300;
            indicator.IndicatorLocation = IndicatorPosition.Up;
            SmartCadDatabase.SaveObject<IndicatorData>(indicator);
            #endregion

            #region Indicator D28
            indicator = new IndicatorData();

            indicator.Classes = new Iesi.Collections.ListSet();
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassDepartment, indicator));

            indicator.Dashboard = false;
            indicator.Description = ResourceLoader.GetString2("IndicatorD28Desc");
            indicator.MeasureUnit = ResourceLoader.GetString2("");
            indicator.Name = ResourceLoader.GetString2("IndicatorD28Name");
            indicator.CustomCode = "D28";
            indicator.Type = indicatorTypeDispatch;
            indicator.Frecuency = 300;
            indicator.IndicatorLocation = IndicatorPosition.Up;
            SmartCadDatabase.SaveObject<IndicatorData>(indicator);
            #endregion

            #region Indicator D30
            indicator = new IndicatorData();

            indicator.Classes = new Iesi.Collections.ListSet();
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassDepartment, indicator));

            indicator.Dashboard = false;
            indicator.Description = ResourceLoader.GetString2("IndicatorD30Desc");
            indicator.MeasureUnit = ResourceLoader.GetString2("%");
            indicator.Name = ResourceLoader.GetString2("IndicatorD30Name");
            indicator.CustomCode = "D30";
            indicator.Type = indicatorTypeDispatch;
            indicator.Frecuency = 900;
            indicator.IndicatorLocation = IndicatorPosition.Up;
            SmartCadDatabase.SaveObject<IndicatorData>(indicator);
            #endregion

            #region Indicator D31
            indicator = new IndicatorData();

            indicator.Classes = new Iesi.Collections.ListSet();
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassDepartment, indicator));

            indicator.Dashboard = false;
            indicator.Description = ResourceLoader.GetString2("IndicatorD31Desc");
            indicator.MeasureUnit = ResourceLoader.GetString2("%");
            indicator.Name = ResourceLoader.GetString2("IndicatorD31Name");
            indicator.CustomCode = "D31";
            indicator.Type = indicatorTypeDispatch;
            indicator.Frecuency = 900;
            indicator.IndicatorLocation = IndicatorPosition.Up;
            SmartCadDatabase.SaveObject<IndicatorData>(indicator);
            #endregion

            #region Indicator D32
            indicator = new IndicatorData();

            indicator.Classes = new Iesi.Collections.ListSet();
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassOperator, indicator));

            indicator.Dashboard = true;
            indicator.Description = ResourceLoader.GetString2("IndicatorD32Desc");
            indicator.MeasureUnit = ResourceLoader.GetString2("");
            indicator.Name = ResourceLoader.GetString2("IndicatorD32Name");
            indicator.CustomCode = "D32";
            indicator.Type = indicatorTypeDispatch;
            indicator.Frecuency = 300;
            indicator.IndicatorLocation = IndicatorPosition.Up;
            SmartCadDatabase.SaveObject<IndicatorData>(indicator);
            #endregion

            #region Indicator D33
            indicator = new IndicatorData();

            indicator.Classes = new Iesi.Collections.ListSet();
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassDepartment, indicator));

            indicator.Dashboard = false;
            indicator.Description = ResourceLoader.GetString2("IndicatorD33Desc");
            indicator.MeasureUnit = ResourceLoader.GetString2("");
            indicator.Name = ResourceLoader.GetString2("IndicatorD33Name");
            indicator.CustomCode = "D33";
            indicator.Type = indicatorTypeDispatch;
            indicator.Frecuency = 60;
            indicator.IndicatorLocation = IndicatorPosition.Down;
            //21-05-09
            //This indicator was removed from the last list is not being used, but is not going to be removed in case that we have to add it again in the future.
            //SmartCadDatabase.SaveObject<IndicatorData>(indicator);
            #endregion

            #region doc D20/22/23/34-39
            //#region Indicator D20
            //indicator = new IndicatorData();

            //indicator.Classes = new Iesi.Collections.ListSet();
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassDepartment, indicator));

            //indicator.Dashboard = false;
            //indicator.Description = ResourceLoader.GetString2("IndicatorD20Desc");
            //indicator.MeasureUnit = ResourceLoader.GetString2("%");
            //indicator.Name = ResourceLoader.GetString2("IndicatorD20Name");
            //indicator.CustomCode = "D20";
            //indicator.Type = indicatorTypeDispatch;
            //indicator.Frecuency = 60;
            //indicator.IndicatorLocation = IndicatorPosition.Down;
            //indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);
            //#endregion

            //#region Indicator D22
            //indicator = new IndicatorData();

            //indicator.Classes = new Iesi.Collections.ListSet();
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassDepartment, indicator));

            //indicator.Dashboard = false;
            //indicator.Description = ResourceLoader.GetString2("IndicatorD22Desc");
            //indicator.MeasureUnit = ResourceLoader.GetString2("%");
            //indicator.Name = ResourceLoader.GetString2("IndicatorD22Name");
            //indicator.CustomCode = "D22";
            //indicator.Type = indicatorTypeDispatch;
            //indicator.Frecuency = 60;
            //indicator.IndicatorLocation = IndicatorPosition.Down;
            //indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);
            //#endregion

            //#region Indicator D23
            //indicator = new IndicatorData();

            //indicator.Classes = new Iesi.Collections.ListSet();
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));

            //indicator.Dashboard = false;
            //indicator.Description = ResourceLoader.GetString2("IndicatorD23Desc");
            //indicator.MeasureUnit = ResourceLoader.GetString2("%");
            //indicator.Name = ResourceLoader.GetString2("IndicatorD23Name");
            //indicator.CustomCode = "D23";
            //indicator.Type = indicatorTypeDispatch;
            //indicator.Frecuency = 60;
            //indicator.IndicatorLocation = IndicatorPosition.Down;
            //indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);
            //#endregion

            //#region Indicator D34
            //indicator = new IndicatorData();

            //indicator.Classes = new Iesi.Collections.ListSet();
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassDepartment, indicator));

            //indicator.Dashboard = false;
            //indicator.Description = ResourceLoader.GetString2("IndicatorD34Desc");
            //indicator.MeasureUnit = ResourceLoader.GetString2("%");
            //indicator.Name = ResourceLoader.GetString2("IndicatorD34Name");
            //indicator.CustomCode = "D34";
            //indicator.Type = indicatorTypeDispatch;
            //indicator.Frecuency = 60;
            //indicator.IndicatorLocation = IndicatorPosition.Down;
            //indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);
           
            //#endregion

            //#region Indicator D35
            //indicator = new IndicatorData();

            //indicator.Classes = new Iesi.Collections.ListSet();
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassDepartment, indicator));

            //indicator.Dashboard = false;
            //indicator.Description = ResourceLoader.GetString2("IndicatorD35Desc");
            //indicator.MeasureUnit = ResourceLoader.GetString2("%");
            //indicator.Name = ResourceLoader.GetString2("IndicatorD35Name");
            //indicator.CustomCode = "D35";
            //indicator.Type = indicatorTypeDispatch;
            //indicator.Frecuency = 60;
            //indicator.IndicatorLocation = IndicatorPosition.Down;
            //indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);

            //#endregion

            //#region Indicator D36
            //indicator = new IndicatorData();

            //indicator.Classes = new Iesi.Collections.ListSet();
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassDepartment, indicator));

            //indicator.Dashboard = false;
            //indicator.Description = ResourceLoader.GetString2("IndicatorD36Desc");
            //indicator.MeasureUnit = ResourceLoader.GetString2("%");
            //indicator.Name = ResourceLoader.GetString2("IndicatorD36Name");
            //indicator.CustomCode = "D36";
            //indicator.Type = indicatorTypeDispatch;
            //indicator.Frecuency = 60;
            //indicator.IndicatorLocation = IndicatorPosition.Down;
            //indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);
            
            //#endregion

            //#region Indicator D37
            //indicator = new IndicatorData();

            //indicator.Classes = new Iesi.Collections.ListSet();
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassDepartment, indicator));

            //indicator.Dashboard = false;
            //indicator.Description = ResourceLoader.GetString2("IndicatorD37Desc");
            //indicator.MeasureUnit = ResourceLoader.GetString2("%");
            //indicator.Name = ResourceLoader.GetString2("IndicatorD37Name");
            //indicator.CustomCode = "D37";
            //indicator.Type = indicatorTypeDispatch;
            //indicator.Frecuency = 60;
            //indicator.IndicatorLocation = IndicatorPosition.Down;
            //indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);

            //#endregion

            //#region Indicator D38
            //indicator = new IndicatorData();

            //indicator.Classes = new Iesi.Collections.ListSet();
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassDepartment, indicator));

            //indicator.Dashboard = false;
            //indicator.Description = ResourceLoader.GetString2("IndicatorD38Desc");
            //indicator.MeasureUnit = ResourceLoader.GetString2("%");
            //indicator.Name = ResourceLoader.GetString2("IndicatorD38Name");
            //indicator.CustomCode = "D38";
            //indicator.Type = indicatorTypeDispatch;
            //indicator.Frecuency = 60;
            //indicator.IndicatorLocation = IndicatorPosition.Down;
            //indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);

            //#endregion
            #endregion doc D20/22/23/34-39

            #region doc D39-D49
            //#region Indicator D39
            //indicator = new IndicatorData();

            //indicator.Classes = new Iesi.Collections.ListSet();
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassDepartment, indicator));
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));

            //indicator.Dashboard = false;
            //indicator.Description = ResourceLoader.GetString2("IndicatorD39Desc");
            //indicator.MeasureUnit = ResourceLoader.GetString2("%");
            //indicator.Name = ResourceLoader.GetString2("IndicatorD39Name");
            //indicator.CustomCode = "D39";
            //indicator.Type = indicatorTypeDispatch;
            //indicator.Frecuency = 300;
            //indicator.IndicatorLocation = IndicatorPosition.Down;
            //indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);

            //#endregion

            //#region Indicator D40
            //indicator = new IndicatorData();

            //indicator.Classes = new Iesi.Collections.ListSet();
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassDepartment, indicator));
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));

            //indicator.Dashboard = false;
            //indicator.Description = ResourceLoader.GetString2("IndicatorD40Desc");
            //indicator.MeasureUnit = ResourceLoader.GetString2("%");
            //indicator.Name = ResourceLoader.GetString2("IndicatorD40Name");
            //indicator.CustomCode = "D40";
            //indicator.Type = indicatorTypeDispatch;
            //indicator.Frecuency = 300;
            //indicator.IndicatorLocation = IndicatorPosition.Down;
            //indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);

            //#endregion

            //#region Indicator D41
            //indicator = new IndicatorData();

            //indicator.Classes = new Iesi.Collections.ListSet();
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassDepartment, indicator));
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));

            //indicator.Dashboard = false;
            //indicator.Description = ResourceLoader.GetString2("IndicatorD41Desc");
            //indicator.MeasureUnit = ResourceLoader.GetString2("%");
            //indicator.Name = ResourceLoader.GetString2("IndicatorD41Name");
            //indicator.CustomCode = "D41";
            //indicator.Type = indicatorTypeDispatch;
            //indicator.Frecuency = 300;
            //indicator.IndicatorLocation = IndicatorPosition.Down;
            //indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);

            //#endregion
          
            //#region Indicator D45
            //indicator = new IndicatorData();

            //indicator.Classes = new Iesi.Collections.ListSet();
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassDepartment, indicator));
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));

            //indicator.Dashboard = false;
            //indicator.Description = ResourceLoader.GetString2("IndicatorD45Desc");
            //indicator.MeasureUnit = ResourceLoader.GetString2("%");
            //indicator.Name = ResourceLoader.GetString2("IndicatorD45Name");
            //indicator.CustomCode = "D45";
            //indicator.Type = indicatorTypeDispatch;
            //indicator.Frecuency = 300;
            //indicator.IndicatorLocation = IndicatorPosition.Down;
            //indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);

            //#endregion

            //#region Indicator D46
            //indicator = new IndicatorData();

            //indicator.Classes = new Iesi.Collections.ListSet();
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassDepartment, indicator));
            ////indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));

            //indicator.Dashboard = false;
            //indicator.Description = ResourceLoader.GetString2("IndicatorD46Desc");
            //indicator.MeasureUnit = ResourceLoader.GetString2("%");
            //indicator.Name = ResourceLoader.GetString2("IndicatorD46Name");
            //indicator.CustomCode = "D46";
            //indicator.Type = indicatorTypeDispatch;
            //indicator.Frecuency = 300;
            //indicator.IndicatorLocation = IndicatorPosition.Down;
            //indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);

            //#endregion

            //#region Indicator D47
            //indicator = new IndicatorData();

            //indicator.Classes = new Iesi.Collections.ListSet();
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassDepartment, indicator));
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));

            //indicator.Dashboard = false;
            //indicator.Description = ResourceLoader.GetString2("IndicatorD47Desc");
            //indicator.MeasureUnit = ResourceLoader.GetString2("%");
            //indicator.Name = ResourceLoader.GetString2("IndicatorD47Name");
            //indicator.CustomCode = "D47";
            //indicator.Type = indicatorTypeDispatch;
            //indicator.Frecuency = 300;
            //indicator.IndicatorLocation = IndicatorPosition.Down;
            //indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);

            //#endregion

            //#region Indicator D48
            //indicator = new IndicatorData();

            //indicator.Classes = new Iesi.Collections.ListSet();
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassDepartment, indicator));
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));

            //indicator.Dashboard = false;
            //indicator.Description = ResourceLoader.GetString2("IndicatorD48Desc");
            //indicator.MeasureUnit = ResourceLoader.GetString2("%");
            //indicator.Name = ResourceLoader.GetString2("IndicatorD48Name");
            //indicator.CustomCode = "D48";
            //indicator.Type = indicatorTypeDispatch;
            //indicator.Frecuency = 300;
            //indicator.IndicatorLocation = IndicatorPosition.Down;
            //indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);

            //#endregion

            //#region Indicator D49
            //indicator = new IndicatorData();

            //indicator.Classes = new Iesi.Collections.ListSet();
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassDepartment, indicator));
            //indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));

            //indicator.Dashboard = false;
            //indicator.Description = ResourceLoader.GetString2("IndicatorD49Desc");
            //indicator.MeasureUnit = ResourceLoader.GetString2("%");
            //indicator.Name = ResourceLoader.GetString2("IndicatorD49Name");
            //indicator.CustomCode = "D49";
            //indicator.Type = indicatorTypeDispatch;
            //indicator.Frecuency = 300;
            //indicator.IndicatorLocation = IndicatorPosition.Down;
            //indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);

            //#endregion

            #endregion doc D39-D49

            #region Indicator D42
            indicator = new IndicatorData();

            indicator.Classes = new Iesi.Collections.ListSet();
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassDepartment, indicator));
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));

            indicator.Dashboard = false;
            indicator.Description = ResourceLoader.GetString2("IndicatorD42Desc");
            indicator.Name = ResourceLoader.GetString2("IndicatorD42Name");
            indicator.CustomCode = "D42";
            indicator.Type = indicatorTypeDispatch;
            indicator.Frecuency = 900;
            indicator.IndicatorLocation = IndicatorPosition.Up;
            indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);

            #endregion

            #region Indicator D43
            indicator = new IndicatorData();

            indicator.Classes = new Iesi.Collections.ListSet();
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassDepartment, indicator));
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));

            indicator.Dashboard = false;
            indicator.Description = ResourceLoader.GetString2("IndicatorD43Desc");
            indicator.Name = ResourceLoader.GetString2("IndicatorD43Name");
            indicator.CustomCode = "D43";
            indicator.Type = indicatorTypeDispatch;
            indicator.Frecuency = 900;
            indicator.IndicatorLocation = IndicatorPosition.Up;
            indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);

            #endregion

            #region Indicator D44
            indicator = new IndicatorData();

            indicator.Classes = new Iesi.Collections.ListSet();
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassDepartment, indicator));
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));

            indicator.Dashboard = false;
            indicator.Description = ResourceLoader.GetString2("IndicatorD44Desc");
            indicator.Name = ResourceLoader.GetString2("IndicatorD44Name");
            indicator.CustomCode = "D44";
            indicator.Type = indicatorTypeDispatch;
            indicator.Frecuency = 900;
            indicator.IndicatorLocation = IndicatorPosition.Up;
            indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);

            #endregion

            #region Indicator D50
            indicator = new IndicatorData();

            indicator.Classes = new Iesi.Collections.ListSet();
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassDepartment, indicator));
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));

            indicator.Dashboard = false;
            indicator.Description = ResourceLoader.GetString2("IndicatorD50Desc");
            indicator.MeasureUnit = ResourceLoader.GetString2("%");
            indicator.Name = ResourceLoader.GetString2("IndicatorD50Name");
            indicator.CustomCode = "D50";
            indicator.Type = indicatorTypeDispatch;
            indicator.Frecuency = 300;
            indicator.IndicatorLocation = IndicatorPosition.Down;
            indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);

            #endregion

            #region Indicator D51
            indicator = new IndicatorData();

            indicator.Classes = new Iesi.Collections.ListSet();
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassSystem, indicator));
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassGroup, indicator));
            indicator.Classes.Add(new IndicatorClassDashboardData(indicatorClassDepartment, indicator));

            indicator.Dashboard = false;
            indicator.Description = ResourceLoader.GetString2("IndicatorD51Desc");
            indicator.MeasureUnit = ResourceLoader.GetString2("%");
            indicator.Name = ResourceLoader.GetString2("IndicatorD51Name");
            indicator.CustomCode = "D51";
            indicator.Type = indicatorTypeDispatch;
            indicator.Frecuency = 60;
            indicator.IndicatorLocation = IndicatorPosition.Down;
            indicator = SmartCadDatabase.SaveObject<IndicatorData>(indicator);
            #endregion
        }

        private static void CreateReportIncidentType(IncidentTypeCategoryData typeCategory)
        {
            LoadInitialData(typeof(IncidentTypeCategoryData), false);
            
            IList savedIncidentTypes = SmartCadDatabase.SearchObjects(new IncidentTypeData());
            foreach (IncidentTypeData savedIncidentType in savedIncidentTypes)
            {
                savedIncidentType.IncidentTypeCategories = new ArrayList();
                savedIncidentType.IncidentTypeCategories.Add(typeCategory);
                SmartCadDatabase.UpdateObject<IncidentTypeData>(savedIncidentType);
            }
            
        }

        private static void CreatePhoneReportIncidentTypeCategory()
        {
            IncidentTypeCategoryData phoneReportIncidentTypeCategory = new IncidentTypeCategoryData(
                "PhoneReport", "PHONECALL");
            SmartCadDatabase.SaveNamedObject<IncidentTypeCategoryData>(phoneReportIncidentTypeCategory);
        }

        private static void CreateCctvReportIncidentTypeCategory()
        {
            IncidentTypeCategoryData cctvReportIncidentTypeCategory = new IncidentTypeCategoryData(
                "CctvReport", "CAMERA");
            SmartCadDatabase.SaveNamedObject<IncidentTypeCategoryData>(cctvReportIncidentTypeCategory);
        }

        private static void CreateOperatorCategory()
        {
            OperatorCategoryData operatorCategory = new OperatorCategoryData(1,"OnTraining");
            operatorCategory.FriendlyName = ResourceInitialDataLoader.GetString2("OperatorCategory.OnTraining");
			operatorCategory.Description = ResourceInitialDataLoader.GetString2("OnTrainingDescription");
            SmartCadDatabase.SaveNamedObject<OperatorCategoryData>(operatorCategory);

            operatorCategory = new OperatorCategoryData(2, "Junior");
            operatorCategory.FriendlyName = ResourceInitialDataLoader.GetString2("OperatorCategory.Junior");
			operatorCategory.Description = ResourceInitialDataLoader.GetString2("JuniorDescription");
            SmartCadDatabase.SaveNamedObject<OperatorCategoryData>(operatorCategory);

            operatorCategory = new OperatorCategoryData(3, "Senior");
            operatorCategory.FriendlyName = ResourceInitialDataLoader.GetString2("OperatorCategory.Senior");
			operatorCategory.Description = ResourceInitialDataLoader.GetString2("SeniorDescription");
            SmartCadDatabase.SaveNamedObject<OperatorCategoryData>(operatorCategory);
        }

        private static void CreateActionInitialData()
        {
            UserActionData action;

            #region Security Basic Actions
            action = new UserActionData("Basic", ResourceInitialDataLoader.GetString2("Access"));
            SmartCadDatabase.SaveNamedObject<UserActionData>(action);
            action = new UserActionData("Search", ResourceInitialDataLoader.GetString2("Search"));
            SmartCadDatabase.SaveNamedObject<UserActionData>(action);
            action = new UserActionData("Insert", ResourceInitialDataLoader.GetString2("Insert"));
            SmartCadDatabase.SaveNamedObject<UserActionData>(action);
            action = new UserActionData("Delete", ResourceInitialDataLoader.GetString2("Delete"));
            SmartCadDatabase.SaveNamedObject<UserActionData>(action);
            action = new UserActionData("Update", ResourceInitialDataLoader.GetString2("Update"));
            SmartCadDatabase.SaveNamedObject<UserActionData>(action);
            action = new UserActionData("Duplicate", ResourceInitialDataLoader.GetString2("Duplicate"));
            SmartCadDatabase.SaveNamedObject<UserActionData>(action);
            
            #endregion

            #region Map Action
            action = new UserActionData("Incident location", ResourceInitialDataLoader.GetString2("SeekIncidents"));
            SmartCadDatabase.SaveNamedObject<UserActionData>(action);
            action = new UserActionData("Change unit status", ResourceInitialDataLoader.GetString2("ChangeStatusUnits"));
            SmartCadDatabase.SaveNamedObject<UserActionData>(action);
            action = new UserActionData("ViewMapLayer", ResourceInitialDataLoader.GetString2("ViewMapLayer"));
            SmartCadDatabase.SaveNamedObject<UserActionData>(action);
            mapActions = new List<UserActionData>();
            mapActions.Add(action);
            #endregion
        }
        private static List<UserApplicationData> firstLevelQuestionsAppList;
        private static List<UserApplicationData> cctvQuestionsAppList;
        private static void CreateApplicationInitialData()
        {
            firstLevelQuestionsAppList = new List<UserApplicationData>();
            cctvQuestionsAppList = new List<UserApplicationData>();
            UserApplicationData application;

            application = new UserApplicationData("Administration", ResourceInitialDataLoader.GetString2("Administration"));
            application = SmartCadDatabase.SaveNamedObject<UserApplicationData>(application);
            firstLevelQuestionsAppList.Add(application);

            application = new UserApplicationData("FirstLevel", ResourceInitialDataLoader.GetString2("FirstLevelAtention"));
            application = SmartCadDatabase.SaveNamedObject<UserApplicationData>(application);
            firstLevelQuestionsAppList.Add(application);

            application = new UserApplicationData("Dispatch", ResourceInitialDataLoader.GetString2("Dispatch"));
            SmartCadDatabase.SaveNamedObject<UserApplicationData>(application);

            application = new UserApplicationData("Map", ResourceInitialDataLoader.GetString2("Maps"));
            SmartCadDatabase.SaveNamedObject<UserApplicationData>(application);
            

            application = new UserApplicationData("Report", ResourceInitialDataLoader.GetString2("Reports"));
            SmartCadDatabase.SaveNamedObject<UserApplicationData>(application);
            

            application = new UserApplicationData("Server", ResourceInitialDataLoader.GetString2("Server"));
            SmartCadDatabase.SaveNamedObject<UserApplicationData>(application);
            

            application = new UserApplicationData("Supervision", ResourceInitialDataLoader.GetString2("Supervision"));
            SmartCadDatabase.SaveNamedObject<UserApplicationData>(application);

            application = new UserApplicationData("Alarm", ResourceInitialDataLoader.GetString2("Alarm"));
            SmartCadDatabase.SaveNamedObject<UserApplicationData>(application);
            cctvQuestionsAppList.Add(application);

            application = new UserApplicationData("Cctv", ResourceInitialDataLoader.GetString2("Cctv"));         
            application = SmartCadDatabase.SaveNamedObject<UserApplicationData>(application);
            cctvQuestionsAppList.Add(application);

            application = new UserApplicationData("Lpr", ResourceInitialDataLoader.GetString2("Lpr"));
            application = SmartCadDatabase.SaveNamedObject<UserApplicationData>(application);
            cctvQuestionsAppList.Add(application);

            application = new UserApplicationData("SocialNetworks", ResourceInitialDataLoader.GetString2("SocialNetworks"));
            application = SmartCadDatabase.SaveNamedObject<UserApplicationData>(application);
            cctvQuestionsAppList.Add(application);
           
        }

        private static void CreateUserResourceInitialData()
        {
            UserResourceData resource;
            UserActionData action = UserActionData.Basic;
            UserApplicationData firstLevelApplication = UserApplicationData.FirstLevel;
            UserApplicationData dispatchApplication = UserApplicationData.Dispatch;
            UserApplicationData mapApplication = UserApplicationData.Map;
            UserApplicationData administrationApplication = UserApplicationData.Administration;
            UserApplicationData reportApplication = UserApplicationData.Report;
            UserApplicationData supervisionApplication = UserApplicationData.Supervision;

            UserApplicationData alarmApplication = UserApplicationData.Alarm;
            UserApplicationData cctvApplication = UserApplicationData.Cctv;

            UserApplicationData lprApplication = UserApplicationData.Lpr;

            UserApplicationData socialNetworksApplication = UserApplicationData.SocialNetworks;

            resource = new UserResourceData();
            resource.Name = typeof(UserApplicationData).Name;
            resource.FriendlyName = ResourceInitialDataLoader.GetString2("Application");
            resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);

            UserPermissionData permission = new UserPermissionData(
                resource.Name + "+" + action.Name,
                resource,
                action
                );

            permission = SmartCadDatabase.SaveNamedObject<UserPermissionData>(permission);

            CreateAccess(
                permission,
                new UserApplicationData[]{
                    firstLevelApplication}
                );
            CreateAccess(
                permission,
                new UserApplicationData[]{
                                dispatchApplication}
                );
            /*CreateAccess(
                permission,
                new UserApplicationData[]{
                                mapApplication}
                );*/
            //Como la aplicacion de administracion tiene mas de un hijo, no se necesita la opcion de entrar.
            /*CreateAccess(
                permission,
                new UserApplicationData[]{
                                administrationApplication}
                );*/
            CreateAccess(
                permission,
                new UserApplicationData[]{
                                reportApplication}
                );

            // COMMENTED FOR BATAAN PROJECT. AA.
            CreateAccess(
               permission,
               new UserApplicationData[]{
                                alarmApplication}
               );            
            CreateAccess(
                permission,
                new UserApplicationData[]{
                                cctvApplication}
                );

            // COMMENTED FOR BATAAN PROJECT. AA.
            //CreateAccess(
            //    permission,
            //    new UserApplicationData[]{
            //                    lprApplication}
            //    );
            //CreateAccess(
            //    permission,
            //    new UserApplicationData[]{
            //                    socialNetworksApplication}
            //    );  
        }
        
        private static void CreateAdministrationUserResourceInitialData()
        {
            UserResourceData resource;

            UserApplicationData administrationApplication = UserApplicationData.Administration;

            resource = new UserResourceData();
            resource.Name = typeof(UserProfileData).Name;
            resource.FriendlyName = ResourceInitialDataLoader.GetString2("Profiles");
            resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
            resource.Actions = allActions;
            CreateAccess(resource, UserApplicationData.Administration);

            resource = new UserResourceData();
            resource.Name = typeof(UserRoleData).Name;
            resource.FriendlyName = ResourceInitialDataLoader.GetString2("Roles");
            resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
            resource.Actions = allActions;
            CreateAccess(resource, UserApplicationData.Administration);

            resource = new UserResourceData();
            resource.Name = typeof(UserAccountData).Name;
            resource.FriendlyName = ResourceInitialDataLoader.GetString2("Users");
            resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
            resource.Actions = allActions;
            CreateAccess(resource, UserApplicationData.Administration);

            resource = new UserResourceData();
            resource.Name = typeof(OperatorStatusData).Name;
            resource.FriendlyName = ResourceInitialDataLoader.GetString2("OperatorStatus");
            resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
            resource.Actions = allActions;
            CreateAccess(resource, UserApplicationData.Administration);

            resource = new UserResourceData();
            resource.Name = typeof(IncidentTypeData).Name;
            resource.FriendlyName = ResourceInitialDataLoader.GetString2("IncidentType");
            resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
            resource.Actions = allActions;
            CreateAccess(resource, UserApplicationData.Administration);

            resource = new UserResourceData();
            resource.Name = typeof(QuestionData).Name;
            resource.FriendlyName = ResourceInitialDataLoader.GetString2("QuestionTipication");
            resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
            resource.Actions = allActions;
            CreateAccess(resource, UserApplicationData.Administration);

            resource = new UserResourceData();
            resource.Name = typeof(UnitTypeData).Name;
            resource.FriendlyName = ResourceInitialDataLoader.GetString2("UnitType");
            resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
            resource.Actions = allActions;
            CreateAccess(resource, UserApplicationData.Administration);

            resource = new UserResourceData();
            resource.Name = typeof(UnitData).Name;
            resource.FriendlyName = ResourceInitialDataLoader.GetString2("Units");
            resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
            resource.Actions = allActions;
            CreateAccess(resource, UserApplicationData.Administration);

            resource = new UserResourceData();
            resource.Name = typeof(OfficerData).Name;
            resource.FriendlyName = ResourceInitialDataLoader.GetString2("Officers");
            resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
            resource.Actions = allActions;
            CreateAccess(resource, UserApplicationData.Administration);

            resource = new UserResourceData();
            resource.Name = typeof(DepartmentStationAssignHistoryData).Name;
            resource.FriendlyName = ResourceInitialDataLoader.GetString2("UnitOfficerAssign");
            resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
            resource.Actions = allActions;
            CreateAccess(resource, UserApplicationData.Administration);
			
			resource = new UserResourceData();
            resource.Name = typeof(EvaluationData).Name;
            resource.FriendlyName = ResourceInitialDataLoader.GetString2("PerformanceEvaluation");
            resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
            resource.Actions = allActions;
            resource.Actions.Add(UserActionData.Duplicate);
            CreateAccess(resource, UserApplicationData.Administration);

            resource = new UserResourceData();
            resource.Name = typeof(CctvZoneData).Name;
            resource.FriendlyName = ResourceInitialDataLoader.GetString2("CctvZone");
            resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
            resource.Actions = allActions;
            CreateAccess(resource, UserApplicationData.Administration);
            
            resource = new UserResourceData();
            resource.Name = typeof(StructData).Name;
            resource.FriendlyName = ResourceInitialDataLoader.GetString2("Structs");
            resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
            resource.Actions = allActions;
            CreateAccess(resource, UserApplicationData.Administration);

            resource = new UserResourceData();
            resource.Name = typeof(CameraData).Name;
            resource.FriendlyName = ResourceInitialDataLoader.GetString2("Cameras");
            resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
            resource.Actions = allActions;
            CreateAccess(resource, UserApplicationData.Administration);

            resource = new UserResourceData();
            resource.Name = typeof(DataloggerData).Name;
            resource.FriendlyName = ResourceInitialDataLoader.GetString2("Dataloggers");
            resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
            resource.Actions = allActions;
            CreateAccess(resource, UserApplicationData.Administration);

            resource = new UserResourceData();
            resource.Name = typeof(SensorTelemetryData).Name;
            resource.FriendlyName = ResourceInitialDataLoader.GetString2("SensorTelemetrys");
            resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
            resource.Actions = allActions;
            CreateAccess(resource, UserApplicationData.Administration);

            //COMMENTED FOR BATAAN PROJECT. AA
            //resource = new UserResourceData();
            //resource.Name = typeof(LprData).Name;
            //resource.FriendlyName = ResourceInitialDataLoader.GetString2("Lpr");
            //resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
            //resource.Actions = allActions;
            //CreateAccess(resource, UserApplicationData.Administration);

            resource = new UserResourceData();
            resource.Name = typeof(GPSData).Name;
            resource.FriendlyName = ResourceInitialDataLoader.GetString2("GPS");
            resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
            resource.Actions = allActions;
            CreateAccess(resource, UserApplicationData.Administration);

            //resource = new UserResourceData();
            //resource.Name = typeof(DeviceData).Name;
            //resource.FriendlyName = ResourceInitialDataLoader.GetString2("Device");
            //resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
            //resource.Actions = allActions;
            //CreateAccess(resource, UserApplicationData.Administration);

            //YT: No hace falta, ya que workShift se elimin de administracin  
            //resource = new UserResourceData();
            //resource.Name = typeof(WorkShiftData).Name;
            //resource.FriendlyName = ResourceInitialDataLoader.GetString2("Shift");
            //resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
            //resource.Actions = allActions;
            //CreateAccess(resource, UserApplicationData.Administration);

            resource = new UserResourceData();
            resource.Name = typeof(RoomData).Name;
            resource.FriendlyName = ResourceInitialDataLoader.GetString2("Room");
            resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
            resource.Actions = allActions;
            CreateAccess(resource, UserApplicationData.Administration);

            resource = new UserResourceData();
            resource.Name = typeof(ApplicationPreferenceData).Name;
            resource.FriendlyName = ResourceInitialDataLoader.GetString2("ApplicationPreferences");
            resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
            resource.Actions = allActions;
            CreateAccess(resource, UserApplicationData.Administration);

            resource = new UserResourceData();
            resource.Name = typeof(DepartmentTypeData).Name;
            resource.FriendlyName = ResourceInitialDataLoader.GetString2("DepartmentTypes");
            resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
            resource.Actions = allActions;
            CreateAccess(resource, UserApplicationData.Administration);

            resource = new UserResourceData();
            resource.Name = typeof(DepartmentZoneData).Name;
            resource.FriendlyName = ResourceInitialDataLoader.GetString2("DepartmentZones");
            resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
            resource.Actions = allActions;
            CreateAccess(resource, UserApplicationData.Administration);

            resource = new UserResourceData();
            resource.Name = typeof(DepartmentStationData).Name;
            resource.FriendlyName = ResourceInitialDataLoader.GetString2("DepartmentStations");
            resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
            resource.Actions = allActions;
            CreateAccess(resource, UserApplicationData.Administration);

            resource = new UserResourceData();
            resource.Name = typeof(RankData).Name;
            resource.FriendlyName = ResourceInitialDataLoader.GetString2("Ranks");
            resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
            resource.Actions = allActions;
            CreateAccess(resource, UserApplicationData.Administration);

            resource = new UserResourceData();
            resource.Name = typeof(PositionData).Name;
            resource.FriendlyName = ResourceInitialDataLoader.GetString2("Positions");
            resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
            resource.Actions = allActions;
            CreateAccess(resource, UserApplicationData.Administration);

            resource = new UserResourceData();
            resource.Name = typeof(UnitOfficerAssignHistoryData).Name;
            resource.FriendlyName = ResourceInitialDataLoader.GetString2("UnitOfficerAssignment");
            resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
            resource.Actions = allActions;
            CreateAccess(resource, UserApplicationData.Administration);

            //resource = new UserResourceData();
            //resource.Name = typeof(DepartmentZoneAddressData).Name;
            //resource.FriendlyName = ResourceInitialDataLoader.GetString2("DepartmentZoneAddressData");
            //resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
            //resource.Actions = allActions;
            //CreateAccess(resource, UserApplicationData.Administration);

            //resource = new UserResourceData();
            //resource.Name = typeof(StructTypeData).Name;
            //resource.FriendlyName = ResourceInitialDataLoader.GetString2("StructTypeData");
            //resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
            //resource.Actions = allActions;
            //CreateAccess(resource, UserApplicationData.Administration);
            
            //resource = new UserResourceData();
            //resource.Name = typeof(CameraTypeData).Name;
            //resource.FriendlyName = ResourceInitialDataLoader.GetString2("CameraTypeData");
            //resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
            //resource.Actions = allActions;
            //CreateAccess(resource, UserApplicationData.Administration);
            
            //resource = new UserResourceData();
            //resource.Name = typeof(ConnectionTypeData).Name;
            //resource.FriendlyName = ResourceInitialDataLoader.GetString2("ConnectionTypeData");
            //resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
            //resource.Actions = allActions;
            //CreateAccess(resource, UserApplicationData.Administration);

            //resource = new UserResourceData();
            //resource.Name = typeof(DepartmentStationAddressData).Name;
            //resource.FriendlyName = ResourceInitialDataLoader.GetString2("DepartmentStationAddressData");
            //resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
            //resource.Actions = allActions;
            //CreateAccess(resource, UserApplicationData.Administration); 
			
            //resource = new UserResourceData();
            //resource.Name = typeof(RouteAddressData).Name;
            //resource.FriendlyName = ResourceInitialDataLoader.GetString2("RouteAddressData");
            //resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
            //resource.Actions = allActions;
            //CreateAccess(resource, UserApplicationData.Administration); 
			
			resource = new UserResourceData();
			resource.Name = typeof(RouteData).Name;
			resource.FriendlyName = ResourceInitialDataLoader.GetString2("RouteData");
			resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
			resource.Actions = allActions;
			CreateAccess(resource, UserApplicationData.Administration);

			resource = new UserResourceData();
			resource.Name = typeof(WorkShiftVariationData).Name;
			resource.FriendlyName = ResourceInitialDataLoader.GetString2("WorkShiftVariationData");
			resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
			resource.Actions = allActions;
			CreateAccess(resource, UserApplicationData.Administration);

            resource = new UserResourceData();
            resource.Name = typeof(QuestionDispatchReportData).Name;
            resource.FriendlyName = ResourceInitialDataLoader.GetString2("Questions");
            resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
            resource.Actions = allActions;
            CreateAccess(resource, UserApplicationData.Administration);

            resource = new UserResourceData();
            resource.Name = typeof(DispatchReportData).Name;
            resource.FriendlyName = ResourceInitialDataLoader.GetString2("DispatchReportData");
            resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
            resource.Actions = allActions;
            CreateAccess(resource, UserApplicationData.Administration);
            
            //resource = new UserResourceData();
            //resource.Name = typeof(CamerasTemplateData).Name;
            //resource.FriendlyName = ResourceInitialDataLoader.GetString2("CamerasTemplateData");
            //resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
            //resource.Actions = allActions;
            //CreateAccess(resource, UserApplicationData.Administration);

            resource = new UserResourceData();
            resource.Name = typeof(TelephonyConfigurationData).Name;
            resource.FriendlyName = ResourceInitialDataLoader.GetString2("TelephonyConfigurationData");
            resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
            resource.Actions = allActions;
            CreateAccess(resource, UserApplicationData.Administration);

            resource = new UserResourceData();
            resource.Name = typeof(RadioConfigurationData).Name;
            resource.FriendlyName = ResourceInitialDataLoader.GetString2("RadioConfigurationData");
            resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
            resource.Actions = allActions;
            CreateAccess(resource, UserApplicationData.Administration);

            resource = new UserResourceData();
            resource.Name = typeof(AlarmQueryTwitterData).Name;
            resource.FriendlyName = ResourceInitialDataLoader.GetString2("AlarmQueryTwitterData");
            resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
            resource.Actions = allActions;
            CreateAccess(resource, UserApplicationData.Administration);
        }

        private static void CreateSupervisionUserResourceInitialData()
        {
            ArrayList list = new ArrayList();
            list.Add(UserActionData.Basic);
            UserResourceData resource;

			resource = new UserResourceData();
			resource.Name = "GeneralSupervisorName";
			resource.FriendlyName = ResourceInitialDataLoader.GetString2("GeneralSupervisor");
			resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
			resource.Actions = list;
			CreateAccess(resource, UserApplicationData.Supervision); 
			
			resource = new UserResourceData();
			resource.Name = "FirstLevelSupervisorName";
            resource.FriendlyName = ResourceInitialDataLoader.GetString2("FirstLevelSupervisor");
            resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
            resource.Actions = list;
            CreateAccess(resource, UserApplicationData.Supervision);

            resource = new UserResourceData();
            resource.Name = "DispatchSupervisorName";
            resource.FriendlyName = ResourceInitialDataLoader.GetString2("DispatchSupervisor");
            resource = SmartCadDatabase.SaveNamedObject<UserResourceData>(resource);
            resource.Actions = list;
            CreateAccess(resource, UserApplicationData.Supervision);			
        }

        private static void CreateApplicationPreference()
        {
            /* 
             * Dependiendo del tipo de preferencia se deben cambiar los respectivos valores, 
             * en el caso de querer agregar una preferencia de tipo string 
             * , con el nombre “Ejemplo”, el valor “EJ”, la descripción de la misma es “EjemploDescripcionDeEstaPreferencia”, 
             * el tipo de data que es “ApplicationPreferenceData.Unit.TextUnit” y el rango permitido en que se va a fijar dicha preferencia “EjemploRange”, 
             * luego de tener todos los campos establecidos se debe llamar a la función  SmartCadDatabase.SaveObject(appPreferenceXX); 
             * con el nombre de nuestro objeto, en este caso appPreferenceXX  para guardar la nueva preferencia en la base de datos.
             * 
             * En Caso de querer agregar una preferencia de tipo entero, booleano, etc, se debe cambiar el tipo de variable(int,bool,string), el tipo de data
             * y el rango del mismo, para una preferencia de tipo booleana, el rango seria "Active" y "Inactive".
             */
            ApplicationPreferenceData appPreference1 = new ApplicationPreferenceData(
                "InRatioSimilarIncidentValue", "int", "25", "SimilarIncidents",
                "InRatioSimilarIncidentValueDescription", ApplicationPreferenceData.Unit.WeightUnit,
                "InRatioSimilarIncidentValueRange", "0", "100");
            SmartCadDatabase.SaveObject(appPreference1);

            ApplicationPreferenceData appPreference2 = new ApplicationPreferenceData(
                "TelephoneNumberWeight", "int", "25", "SimilarIncidents",
                "TelephoneNumberWeightDescription", ApplicationPreferenceData.Unit.WeightUnit,
                "TelephoneNumberWeightRange", "0", "100");
            SmartCadDatabase.SaveObject(appPreference2);

            ApplicationPreferenceData appPreference3 = new ApplicationPreferenceData(
                "IncidentAddressWeight", "int", "25", "SimilarIncidents",
                "IncidentAddressWeightDescription", ApplicationPreferenceData.Unit.WeightUnit,
                "IncidentAddressWeightRange", "0", "100");
            SmartCadDatabase.SaveObject(appPreference3);

            ApplicationPreferenceData appPreference4 = new ApplicationPreferenceData(
                "IncidentTypeWeight", "int", "25", "SimilarIncidents",
                "IncidentTypeWeightDescription", ApplicationPreferenceData.Unit.WeightUnit,
                "IncidentTypeWeightRange", "0", "100");
            SmartCadDatabase.SaveObject(appPreference4);

            ApplicationPreferenceData appPreference5 = new ApplicationPreferenceData(
                "BottomSimilarIncidentValue", "int", "40", "SimilarIncidents",
                "BottomSimilarIncidentValueDescription", ApplicationPreferenceData.Unit.WeightUnit,
                "BottomSimilarIncidentValueRange", "0", "100");
            SmartCadDatabase.SaveObject(appPreference5);

            ApplicationPreferenceData appPreference7 = new ApplicationPreferenceData(
                "AlarmTimeLimit", "int", "90", "ChronometerControl",
                "AlarmTimeLimitDescription", ApplicationPreferenceData.Unit.SecondsUnit,
                "AlarmTimeLimitRange", "60", "3600");
            SmartCadDatabase.SaveObject(appPreference7);

            ApplicationPreferenceData preferenceLoginMode = new ApplicationPreferenceData(
                "CheckLogInMode", "bool", true.ToString(), "LoginFirstLevelAndDispatch",
                "CheckLogInModeDescription", ApplicationPreferenceData.Unit.LogicalValueUnit,
                "CheckLogInModeRange", "Active", "Inactive");
            SmartCadDatabase.SaveObject(preferenceLoginMode);

            ApplicationPreferenceData appPreference8 = new ApplicationPreferenceData(
                "FrontClientIncidentCodePrefix", "string", "INC", "FrontClient",
                "GivesThePrefixForTheCustomCodeOnFrontClientIncidents", ApplicationPreferenceData.Unit.TextUnit,
                "FrontClientIncidentCodePrefixRange", "AlphaNum", "AlphaNum");
            SmartCadDatabase.SaveObject(appPreference8);

            //COMMENTED FOR BATAAN PROJECT. AA
            //appPreference8 = new ApplicationPreferenceData(
            //    "SNIncidentCodePrefix", "string", "INC", "SocialNetworks",
            //    "GivesThePrefixForTheCustomCodeOnSNIncidents", ApplicationPreferenceData.Unit.TextUnit,
            //    "SNtIncidentCodePrefixRange", "AlphaNum", "AlphaNum");
            //SmartCadDatabase.SaveObject(appPreference8);

            appPreference8 = new ApplicationPreferenceData(
                "CCTVIncidentCodePrefix", "string", "TV", "CCTV",
                "GivesThePrefixForTheCustomCodeOnCCTVIncidents", ApplicationPreferenceData.Unit.TextUnit,
                "CCTVIncidentCodePrefixRange", "AlphaNum", "AlphaNum");
            SmartCadDatabase.SaveObject(appPreference8);

            //COMMENTED FOR BATAAN PROJECT. AA
            //appPreference8 = new ApplicationPreferenceData(
            //    "LPRIncidentCodePrefix", "string", "LPR", "LPR",
            //    "GivesThePrefixForTheCustomCodeOnLPRIncidents", ApplicationPreferenceData.Unit.TextUnit,
            //    "LPRIncidentCodePrefixRange", "AlphaNum", "AlphaNum");
            //SmartCadDatabase.SaveObject(appPreference8);

            appPreference8 = new ApplicationPreferenceData(
                "CctvVideoSecondsBefore", "int", "2", "CCTV",
                "CctvVideoSecondsBeforeDescription", ApplicationPreferenceData.Unit.SecondsUnit,
                "CctvVideoSecondsBeforeRange", "1", "600");
            SmartCadDatabase.SaveObject(appPreference8);

            appPreference8 = new ApplicationPreferenceData(
                "CctvVideoSecondsAfter", "int", "2", "CCTV",
                "CctvVideoSecondsAfterDescription", ApplicationPreferenceData.Unit.SecondsUnit,
                "CctvVideoSecondsAfterRange", "1", "600");
            SmartCadDatabase.SaveObject(appPreference8);

            // COMMENTED FOR BATAAN PROJECT. AA.
            appPreference8 = new ApplicationPreferenceData(
                "ALARMIncidentCodePrefix", "string", "AL", "ALARM",
                "GivesThePrefixForTheCustomCodeOnALARMIncidents", ApplicationPreferenceData.Unit.TextUnit,
                "ALARMIncidentCodePrefixRange", "AlphaNum", "AlphaNum");
            SmartCadDatabase.SaveObject(appPreference8);

            appPreference8 = new ApplicationPreferenceData(
                "IsIncidentCodeSuffix", "bool", false.ToString(), "FrontClientAndCCTVModule",
                "SaysIfThePrefixIsASuffix", ApplicationPreferenceData.Unit.LogicalValueUnit,
                "IsIncidentCodeSuffixRange", "Active", "Inactive");
            SmartCadDatabase.SaveObject(appPreference8);

            //COMMENTED FOR BATAAN PROJECT. AA
            //appPreference8 = new ApplicationPreferenceData(
            //    "IsSNIncidentCodeSuffix", "bool", false.ToString(), "SocialNetworks",
            //    "SaysIfThePrefixIsASuffix", ApplicationPreferenceData.Unit.LogicalValueUnit,
            //    "IsSNIncidentCodeSuffixRange", "Active", "Inactive");
            //SmartCadDatabase.SaveObject(appPreference8);

			ApplicationPreferenceData appPreference9 = new ApplicationPreferenceData(
				"SimilarIncidentsRatio", "int", "50", "FrontClient", 
				"SetTheDistanceForSimilarIncidents", ApplicationPreferenceData.Unit.MetersUnit, 
				"SimilarIncidentsRatioRange", "0", "300");
			SmartCadDatabase.SaveObject(appPreference9);

            ApplicationPreferenceData appPreference10 = new ApplicationPreferenceData(
                "ViewAllDepartmentsInMap", "bool", false.ToString(), "Map",
                "SaysUserDispatchCanSeeAllDepartments", ApplicationPreferenceData.Unit.LogicalValueUnit,
                "CheckAllDepartmentsDispatchUsers", "Active", "Inactive");
            SmartCadDatabase.SaveObject(appPreference10);

            ApplicationPreferenceData appPreference11 = new ApplicationPreferenceData(
                "ToleranceForRoutes", "int", "20", "Map",
                "SaysToleranceForRoutes", ApplicationPreferenceData.Unit.MetersUnit,
                "ToleranceForRoutesRange", "0", "100");
            SmartCadDatabase.SaveObject(appPreference11);

            ApplicationPreferenceData appPreference12 = new ApplicationPreferenceData(
               "MaxWaitTimeForCommunicationWithGPS", "int", "10", "Map",
               "SaysMaxWaitTimeForCommunicationWithGPS", ApplicationPreferenceData.Unit.MinutesUnit,
               "MaxWaitTimeForCommunicationWithGPSRange", "0", "100");
            SmartCadDatabase.SaveObject(appPreference12);

            // COMMENTED FOR BATAAN PROJECT. AA.
            //ApplicationPreferenceData appPreference13 = new ApplicationPreferenceData(
            //   "ToleranceForStopApproximation", "int", "10", "Map",
            //   "SaysToleranceForStopApproximation", ApplicationPreferenceData.Unit.MetersUnit,
            //   "ToleranceForStopApproximationRange", "0", "100");
            //SmartCadDatabase.SaveObject(appPreference13);

            // COMMENTED FOR BATAAN PROJECT. AA.
            ApplicationPreferenceData appPreference14 = new ApplicationPreferenceData(
               "AlarmUpdateValue", "string", "Incident update with the newest value of the sensor", "Alarm",
               "AlarmUpdateValueDescription", ApplicationPreferenceData.Unit.TextUnit,
               "AlarmUpdateValueRange", "AlphaNum", "AlphaNum");
            SmartCadDatabase.SaveObject(appPreference14);

            ApplicationPreferenceData appPreference15 = new ApplicationPreferenceData(
               "RecordCalls", "bool", true.ToString(), "FrontClient",
               "RecordCallsDescription", ApplicationPreferenceData.Unit.LogicalValueUnit,
               "RecordCallsRange", "Active", "Inactive");
            SmartCadDatabase.SaveObject(appPreference15);


            #region Dispatch preferences

            ApplicationPreferenceData appPreferenceDispatch = new ApplicationPreferenceData("TimeWithoutBeAttended",
                "int", "60", "AssignedIncidentNotifications",
                "TimeWithoutBeAttendedDescription", ApplicationPreferenceData.Unit.SecondsUnit,
                "TimeWithoutBeAttendedRange", "30", "300");
            SmartCadDatabase.SaveObject(appPreferenceDispatch);

            //appPreferenceDispatch = new ApplicationPreferenceData(
            //    "TimeCycleCheckOperators", "int", "90000", "ServiceHelper",
            //    "TimeCycleCheckOperatorsDescription", ApplicationPreferenceData.Unit.MillisecondsUnit,
            //    "TimeCycleCheckOperatorsRange", "60000", "360000");
            //SmartCadDatabase.SaveObject(appPreferenceDispatch);

			appPreferenceDispatch = new ApplicationPreferenceData(
				"CanAssignUnitsFromOtherStation","bool",true.ToString(),"Dispatch",
				"SaysIfUserCanChooseUnitsFromOtherStations",ApplicationPreferenceData.Unit.LogicalValueUnit,
				"CanAssignUnitsFromOtherStationRange","Active","Inactive");
			SmartCadDatabase.SaveObject(appPreferenceDispatch);

			appPreferenceDispatch = new ApplicationPreferenceData(
				"CanCancelDispatchUnit","bool",true.ToString(),"Dispatch",
				"SaysIfCanCancelAnUnitDispatch",ApplicationPreferenceData.Unit.LogicalValueUnit,
				"CanCancelDispatchUnitRange","Active","Inactive");
			SmartCadDatabase.SaveObject(appPreferenceDispatch);

			appPreferenceDispatch = new ApplicationPreferenceData(
				"CanChangeIncidentNotificationStation", "bool",true.ToString(),"Dispatch",
				"SaysIfUserCanChangeTheStationOfAnIncidentNotification",ApplicationPreferenceData.Unit.LogicalValueUnit,
				"CanChangeIncidentNotificationStationRange","Active","Inactive");
			SmartCadDatabase.SaveObject(appPreferenceDispatch);

			appPreferenceDispatch = new ApplicationPreferenceData(
				"CanChangeUnitStatus","bool",true.ToString(),"Dispatch",
				"SaysIfUserCanChangeAnUnitStatus",ApplicationPreferenceData.Unit.LogicalValueUnit,
				"CanChangeUnitStatusRange","Active","Inactive");
			SmartCadDatabase.SaveObject(appPreferenceDispatch);

            appPreferenceDispatch = new ApplicationPreferenceData(
                "DispatchAutomaticAbsent", "int", "1", "Dispatch",
                "DispatchAutomaticAbsentDescription", ApplicationPreferenceData.Unit.MinutesUnit,
                "DispatchAutomaticAbsentRange", "1", "60");
            SmartCadDatabase.SaveObject(appPreferenceDispatch);

            #endregion

            #region Supervision Preferences
            //Max number of reassigned notifications
            ApplicationPreferenceData supervisionPreference = new ApplicationPreferenceData(
                "MaxNotificationsToReassign",
                "int",
                "3",
                "IncidentNotificationDistribution",
                "MaxNotificationsToReassignDescription", ApplicationPreferenceData.Unit.QuantityUnit,
                "MaxNotificationsToReassignRange", "1", "10");
            SmartCadDatabase.SaveObject(supervisionPreference);

            //Interval of time to execute the notification reassignment 
            supervisionPreference = new ApplicationPreferenceData(
                "TimeIntervalToRunReassignments",
                "int",
                "1", //minutes
                "IncidentNotificationDistribution",
                "TimeIntervalToRunReassignmentsDescription", ApplicationPreferenceData.Unit.MinutesUnit,
                "TimeIntervalToRunReassignmentsRange", "1", "5");

            SmartCadDatabase.SaveObject(supervisionPreference);

            //Interval of time to avoid notification reassignment to a specific operator
            supervisionPreference = new ApplicationPreferenceData(
                "TimeIntervalToAvoidReassignmentsToOperator",
                "int",
                "5", //minutes
                "IncidentNotificationDistribution",
                "TimeIntervalToAvoidReassignmentsToOperatorDescription", ApplicationPreferenceData.Unit.MinutesUnit,
                "TimeIntervalToAvoidReassignmentsToOperatorRange", "0", "30");

            SmartCadDatabase.SaveObject(supervisionPreference);


            supervisionPreference = new ApplicationPreferenceData(
             "HoursCloseReport",
             "int",
             "3",  //hours
             "CloseReport",
             "CloseReportDescription", ApplicationPreferenceData.Unit.HoursUnit,
             "HoursCloseReportRange", "0", "24");

            SmartCadDatabase.SaveObject(supervisionPreference);

            supervisionPreference = new ApplicationPreferenceData(
                "TimeIntervalForecastSeparation",
                "int",
                "60", //minutes
                "ForecastModule",
                "TimeIntervalForecastSeparationDescription", ApplicationPreferenceData.Unit.MinutesUnit,
                "TimeIntervalForecastSeparationRange", "5", "60");

            SmartCadDatabase.SaveObject(supervisionPreference);

            supervisionPreference = new ApplicationPreferenceData(
                "TimeToDeleteWorkShift",
                "int",
                "1", //hours
                "WorkShifts",
                "TimeToDeleteWorkShiftDescription", ApplicationPreferenceData.Unit.MinutesUnit,
                "SeparationRange", "0", "720");

            SmartCadDatabase.SaveObject(supervisionPreference);

            supervisionPreference = new ApplicationPreferenceData(
                "TimeToDeleteVacation",
                "int",
                "1", //hours
                "VacationsAndPermissions",
                "TimeToDeleteVacationDescription", ApplicationPreferenceData.Unit.MinutesUnit,
                "SeparationRange", "0", "720");

            SmartCadDatabase.SaveObject(supervisionPreference);


            #endregion

            #region Indicators
            ApplicationPreferenceData indicatorPreference = new ApplicationPreferenceData(
                "SearchClientObjectsMaxRows",
                "int",
                "500",
                "Clients",
                "SearchClientObjectsMaxRowsDescription", ApplicationPreferenceData.Unit.QuantityUnit,
                "SearchClientObjectsMaxRowsRange", "100", "1000");
            SmartCadDatabase.SaveObject(indicatorPreference);
            #endregion

            // COMMENTED FOR BATAAN PROJECT. AA.
            //appPreference14 = new ApplicationPreferenceData(
            //    "MaxTimeToShowAlerts", "int", "24", "Map",
            //    "ItSaysWhatsTheMaximunTimeToShowAnAlertNotification", ApplicationPreferenceData.Unit.MinutesUnit,
            //    "FleetControlShowAlertsPreferenceRange", "0", "720");
            //SmartCadDatabase.SaveObject(appPreference14);

            ApplicationPreferenceData appPreference16 = new ApplicationPreferenceData(
                "ExtraTimeToEnterApplication", "int", "2", "LoginFirstLevelAndDispatch",
                "ExtraTimeToEnterApplicationDescription", ApplicationPreferenceData.Unit.MinutesUnit,
                "ExtraTimeToEnterApplicationRange", "0", "10");
            SmartCadDatabase.SaveObject(appPreference16);

            ApplicationPreferenceData appPreference17 = new ApplicationPreferenceData(
                "VoiceRecordingPath", "string", @"C:\SmartCadDist\VoiceRecording\", "Server",
                "VoiceRecordingPathDescription", ApplicationPreferenceData.Unit.TextUnit,
                "VoiceRecordingPathRange", "AlphaNum", "AlphaNum");
            SmartCadDatabase.SaveObject(appPreference17);

            ///URDANETA
            ApplicationPreferenceData appPreference18 = new ApplicationPreferenceData(
                "VACodePrefix","string", "VA", "VideoAnalitica",
                "GivesThePrefixForTheCustomCodeOnVideoAnalitica", ApplicationPreferenceData.Unit.TextUnit,
                "The value to assing is an alphanumeric string.", "AlphaNum", "AlphaNum");
            SmartCadDatabase.SaveObject(appPreference18);

            #region Error Configuration
            ApplicationPreferenceData errorconfigPreference = new ApplicationPreferenceData(
                "ShowErrorDetails", "bool", "true", "LoginModule",
                "ShowErrorDetailsDescription", ApplicationPreferenceData.Unit.LogicalValueUnit,
                "ShowErrorDetailsRange", "Active", "Inactive");
            SmartCadDatabase.SaveObject(errorconfigPreference);
            #endregion

            #region Telephony Configuration
            ApplicationPreferenceData telephonyPreference = new ApplicationPreferenceData(
                "VirtualCallActive", "bool", true.ToString(), "VirtualCallForFirstLevel",
                "VirtualCallForFirstLevelDescription", ApplicationPreferenceData.Unit.LogicalValueUnit,
                "VirtualCallActivationRange", "Active", "Inactive");
            SmartCadDatabase.SaveObject(telephonyPreference);
            #endregion
        }
        
        private static void CreateUnitData()
        {
            #region getDeparments
            //Get deparments
            DepartmentTypeData dpPoliceMetro = new DepartmentTypeData();
            dpPoliceMetro.Name = DPPOLICE;
            dpPoliceMetro = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpPoliceMetro);

            DepartmentTypeData dpFire = new DepartmentTypeData();
            dpFire.Name = DPFIRE;
            dpFire = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFire);

            DepartmentTypeData dpFireRescue = new DepartmentTypeData();
            dpFireRescue.Name = DPFIRERESCUE;
            dpFireRescue = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFireRescue);

            DepartmentTypeData dpPreFire = new DepartmentTypeData();
            dpPreFire.Name = DPFIREPRE;
            dpPreFire = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpPreFire);

            DepartmentTypeData dpTransit = new DepartmentTypeData();
            dpTransit.Name = DPTRANS;
            dpTransit = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpTransit);

            DepartmentTypeData dpAmbulanceMetro = new DepartmentTypeData();
            dpAmbulanceMetro.Name = DPAMBUMETRO;
            dpAmbulanceMetro = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpAmbulanceMetro);

            DepartmentTypeData dpProtecCivil = new DepartmentTypeData();
            dpProtecCivil.Name = DPPRECIVIL;
            dpProtecCivil = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpProtecCivil);

            DepartmentTypeData dpCICPC = new DepartmentTypeData();
            dpCICPC.Name = DPCICPC;
            dpCICPC = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpCICPC);

            #endregion

            #region IncidentTypes
            ArrayList incidentTypes = new ArrayList(SmartCadDatabase.SearchObjects(new IncidentTypeData()));

            #endregion

            #region getUnitTypes
            //Get unit types
            UnitTypeData uPoliceCar = new UnitTypeData();
            uPoliceCar.Name = "Police car";
            uPoliceCar = SmartCadDatabase.SearchObject<UnitTypeData>(uPoliceCar);

            UnitTypeData uAmbulance = new UnitTypeData();
            uAmbulance.Name = "Ambulance";
            uAmbulance = SmartCadDatabase.SearchObject<UnitTypeData>(uAmbulance);            

            UnitTypeData uWatertruck = new UnitTypeData();
            uWatertruck.Name = "Water truck";
            uWatertruck = SmartCadDatabase.SearchObject<UnitTypeData>(uWatertruck);

            UnitTypeData uHelicopter = new UnitTypeData();
            uHelicopter.Name = "helicopter";
            uHelicopter = SmartCadDatabase.SearchObject<UnitTypeData>(uHelicopter);

            UnitTypeData uMoto = new UnitTypeData();
            uMoto.Name = "moto";
            uMoto = SmartCadDatabase.SearchObject<UnitTypeData>(uMoto);

            UnitTypeData uMiniWaterTruck = new UnitTypeData();
            uMiniWaterTruck.Name = "mini water truck";
            uMiniWaterTruck = SmartCadDatabase.SearchObject<UnitTypeData>(uMiniWaterTruck);

            UnitTypeData uAllTerrain = new UnitTypeData();
            uAllTerrain.Name = "All terrain";
            uAllTerrain = SmartCadDatabase.SearchObject<UnitTypeData>(uAllTerrain);

            UnitTypeData uSpecialUnit = new UnitTypeData();
            uSpecialUnit.Name = "special unit";
            uSpecialUnit = SmartCadDatabase.SearchObject<UnitTypeData>(uSpecialUnit);

            #endregion

            //get Status

            UnitStatusData status = new UnitStatusData();
            status.CustomCode = "AVAILABLE";
            status = SmartCadDatabase.SearchObject<UnitStatusData>(status);
            
            //Set unitType
            UnitData unit = null;
            ArrayList unitsArray = new ArrayList();
            Random random = new Random();

            int unitNumber = 100;

            IList departmentStations = SmartCadDatabase.SearchObjects("select ds from DepartmentStationData ds");
            IList unitTypes = SmartCadDatabase.SearchObjects(new UnitTypeData());

            // agregado para que sea posible eliminar los tipos de incidentes desde administracion
            int initIndex = 0;
            int step = 12;
            int typesCount = incidentTypes.Count;
            ArrayList incidentTypesSubList = new ArrayList();
            //
            foreach (DepartmentStationData departmentStation in departmentStations)
            {
                // 
                if (initIndex < typesCount - step)
                {
                    incidentTypesSubList = incidentTypes.GetRange(initIndex, step);
                }
                else
                {
                    initIndex = 0;
                    incidentTypesSubList = incidentTypes.GetRange(initIndex, step);
                }
                //
                foreach (UnitTypeData unitType in unitTypes)
                {
                    if (unitType.DepartmentType == departmentStation.DepartmentZone.DepartmentType)
                    {
                        unit = new UnitData();
                        unit.Number = ++unitNumber;
                        unit.Type = unitType;
                        unit.Status = status;
                        unit.Description = unit.Type.Name;
                        unit.CustomCode = "AAA-" + unit.Number;                      
                        unit.IdRadio = unit.Number.ToString();
                        unit.SetIncidentTypes = new HashedSet(incidentTypesSubList);
                        unit.Capacity = random.Next(1, 5);
                        unit.Velocity = (double) (random.NextDouble() + .1) * 200;
                        unit.DepartmentStation = departmentStation;
                        unitsArray.Add(unit);
                    }
                }
                //
                initIndex += step;
                //
            }

            int max = (int)(Math.Sqrt(unitsArray.Count) + 0.5);

            IDictionary<string, string> properties = new Dictionary<string, string>();
            GPSData gps = null;
            #region GPSTypes
            ArrayList gpsTypes = new ArrayList(SmartCadDatabase.SearchObjects(new GPSTypeData()));
            #endregion
            for (int i = 0; i <= max; i++)
            {
                for (int j = 0; j < max; j++)
                {
                    int index = i * max + j;

                    if (index < unitsArray.Count)
                    {
                        unit = (UnitData)unitsArray[index];
                        double lat = SmartCadConfiguration.SmartCadSection.DefaultMapLat + i * 0.003;
                        double lon = SmartCadConfiguration.SmartCadSection.DefaultMapLon + j * 0.003;

                        gps = new GPSData();
                        gps.Name = (unit.Number-100).ToString();
                        gps.IsSynchronized = true;
                        gps.Latitude = lat;
                        gps.Longitude = lon;
                        gps.CoordinatesDate = DateTime.Now;
                        gps.Type = gpsTypes[i % gpsTypes.Count] as GPSTypeData;
                        gps = SmartCadDatabase.SaveObject<GPSData>(gps);
                        unit.GPS = gps;

                        unit = SmartCadDatabase.SaveObject<UnitData>(unit);
                        //unit.Code = SmartCadDatabase.SaveObject(databaseSession, unit);

                    }
                }
            }
        }
       
        private static void CreateStructTypes()
        {
            StructTypeData type = new StructTypeData();
            type.Name = ResourceInitialDataLoader.GetString2("Building");
            SmartCadDatabase.SaveObject<StructTypeData>(type);
            
            type = new StructTypeData();
            type.Name = ResourceInitialDataLoader.GetString2("Post");
            SmartCadDatabase.SaveObject<StructTypeData>(type);
            
            type = new StructTypeData();
            type.Name = ResourceInitialDataLoader.GetString2("Other");
            SmartCadDatabase.SaveObject<StructTypeData>(type);

            if (SmartCadConfiguration.SmartCadSection.ServerElement.InitialDataElement.CreateTestData == true)
            {
                CreateCctvZoneData(type);
            }
        }

        static ArrayList connectionCameraType = new ArrayList();
        private static void CreateConnectionCamerasTypes()
        {
            ConnectionTypeData type = new ConnectionTypeData();
            type.Name = ResourceInitialDataLoader.GetString2("Unicast");
            SmartCadDatabase.SaveObject<ConnectionTypeData>(type);
            connectionCameraType.Add(type);

            type = new ConnectionTypeData();
            type.Name = ResourceInitialDataLoader.GetString2("Multicast");
            SmartCadDatabase.SaveObject<ConnectionTypeData>(type);
            connectionCameraType.Add(type);

            type = new ConnectionTypeData();
            type.Name = ResourceInitialDataLoader.GetString2("Other");
            SmartCadDatabase.SaveObject<ConnectionTypeData>(type);
            connectionCameraType.Add(type);

            if (SmartCadConfiguration.SmartCadSection.ServerElement.VmsServerCommunicationElement.dllName != "")
            {
                type = new ConnectionTypeData();
                type.Name = ResourceInitialDataLoader.GetString2("VMSConnection");
                SmartCadDatabase.SaveObject<ConnectionTypeData>(type);
                connectionCameraType.Add(type);
            
            }
        }

        static ArrayList cameraType = new ArrayList();
        private static void CreateCameraTypes()
        {
            CameraTypeData type = new CameraTypeData();
            type.Name = ResourceInitialDataLoader.GetString2("Axis214PTZ");
            SmartCadDatabase.SaveObject<CameraTypeData>(type);
            cameraType.Add(type);
            if (SmartCadConfiguration.SmartCadSection.ServerElement.VmsServerCommunicationElement.dllName != "")
            {
                type.Name = ResourceInitialDataLoader.GetString2("CameraImported");
                SmartCadDatabase.SaveObject<CameraTypeData>(type);
                cameraType.Add(type);
            }
        }

        static ArrayList lprType = new ArrayList();
        private static void CreatelprTypes()
        {
            LprTypeData type = new LprTypeData();
            type.Name = ResourceInitialDataLoader.GetString2("Tattile");
            SmartCadDatabase.SaveObject<LprTypeData>(type);
            lprType.Add(type);
            type = new LprTypeData();
            type.Name = ResourceInitialDataLoader.GetString2("Quercus");
            SmartCadDatabase.SaveObject<LprTypeData>(type);
            lprType.Add(type);
        }

        static ArrayList lprWay = new ArrayList();
        private static void CreatelprWays()
        {
            LprWayData way = new LprWayData();
            way.Name = ResourceInitialDataLoader.GetString2("North");
            SmartCadDatabase.SaveObject<LprWayData>(way);
            lprWay.Add(way);
            way = new LprWayData();
            way.Name = ResourceInitialDataLoader.GetString2("South");
            SmartCadDatabase.SaveObject<LprWayData>(way);
            lprWay.Add(way);
            way = new LprWayData();
            way.Name = ResourceInitialDataLoader.GetString2("East");
            SmartCadDatabase.SaveObject<LprWayData>(way);
            lprWay.Add(way);
            way = new LprWayData();
            way.Name = ResourceInitialDataLoader.GetString2("West");
            SmartCadDatabase.SaveObject<LprWayData>(way);
            lprWay.Add(way);
        }

        private static void CreateCctvZoneData(StructTypeData type)
        {
            ipNumberCamera = 0;
            for (int i = 0; i < 3; i++)
            {
                CctvZoneData zone = new CctvZoneData();               
                zone.Name = "zona" + i.ToString();                
                SmartCadDatabase.SaveObject<CctvZoneData>(zone);
                CreateStructData(zone, type);
            }
        }
        private static int ipNumberCamera = 0;
        private static double structLong = SmartCadConfiguration.SmartCadSection.DefaultMapLon;
        private static void CreateStructData(CctvZoneData zone, StructTypeData type)
        {
            Random random = new Random();
            for (int i = 0; i < 5; i++)
            {
                StructData myStruct = new StructData();
                myStruct.Name = "Poste" + random.Next().ToString();
                myStruct.Zone = zone;
                myStruct.Type = type;
                myStruct.Addres = new StructAddressData(new AddressData(zone.Name, "calle" + i, null, null));
                myStruct.Addres.State = "Miranda";
                myStruct.Addres.Town = "Baruta";
                myStruct.Addres.Lon = structLong +   0.003;
                structLong = myStruct.Addres.Lon;
                myStruct.Addres.Lat = SmartCadConfiguration.SmartCadSection.DefaultMapLat;               
                myStruct.Addres.IsSynchronized = true;
                SmartCadDatabase.SaveObject<StructData>(myStruct);
                if (SmartCadConfiguration.SmartCadSection.ServerElement.VmsServerCommunicationElement.dllName == "")
                {
                    CreateCameraData(myStruct, ipNumberCamera++);
                }
               // if (SmartCadConfiguration.SmartCadSection.ServerElement.VmsServerCommunicationElement.dllName == "")
                CreateDataloggerData(myStruct, ipNumberCamera++);
            }

            for (int i = 0; i < 5; i++)
            {
                StructData myStruct = new StructData();
                myStruct.Name = "Poste" + random.Next().ToString();
                myStruct.Zone = zone;
                myStruct.Type = type;
                myStruct.Addres = new StructAddressData(new AddressData(zone.Name, "calle" + i, null, null));
                myStruct.Addres.State = "Miranda";
                myStruct.Addres.Town = "Baruta";
                myStruct.Addres.Lon = structLong + 0.003;
                structLong = myStruct.Addres.Lon;
                myStruct.Addres.Lat = SmartCadConfiguration.SmartCadSection.DefaultMapLat;
                myStruct.Addres.IsSynchronized = true;
                SmartCadDatabase.SaveObject<StructData>(myStruct);

                // COMMENTED FOR BATAAN PROJECT. AA.
                //CreateLprData(myStruct, ipNumberCamera++);
              
            }

        }
        private static void CreateLprData(StructData myStruct, int number)
        {
            Random random = new Random();


            LprData lpr = new LprData();
            lpr.Type = (lprType[0] as LprTypeData);            
            lpr.Name = "Lpr" + random.Next().ToString();
            lpr.Ip = "132.5.31." + number.ToString();//7
            lpr.IpServer = "127.0.0.1";
            lpr.Port = "80";
            lpr.Login = "login" + random.Next().ToString();
            lpr.Password = "pass";            
            lpr.Serial = "Referencia 1";
            lpr.Way = (lprWay[0] as LprWayData);
           
            lpr.Struct = myStruct;
            SmartCadDatabase.SaveObject<LprData>(lpr);

            lpr = new LprData();
            lpr.Type = (lprType[1] as LprTypeData);
            lpr.Name = "Lpr" + random.Next().ToString();
            lpr.Ip = "160.53.120." + number.ToString();//119           
            lpr.IpServer = "127.0.0.1";
            lpr.Port = "80";
            lpr.Login = "login" + random.Next().ToString();
            lpr.Password = "pass";            
            lpr.Serial = "Referencia 2";
            lpr.Way = (lprWay[1] as LprWayData);
          
            lpr.Struct = myStruct;            
            SmartCadDatabase.SaveObject<LprData>(lpr);

            lpr = new LprData();
            lpr.Type = (lprType[0] as LprTypeData);
            lpr.Name = "Lpr" + random.Next().ToString();
            lpr.Ip = "135.165.99." + number.ToString();//86
            lpr.IpServer = "127.0.0.1";
            lpr.Port = "80";
            lpr.Login = "login" + random.Next().ToString();
            lpr.Password = "pass";        
            lpr.Serial = "Referencia 3";
            lpr.Way = (lprWay[2] as LprWayData);
           
            lpr.Struct = myStruct;            
            SmartCadDatabase.SaveObject<LprData>(lpr);
            
            CreateVehicleRequestsData();

        }
        
        private static void CreateVehicleRequestsData()
        {
            Random random = new Random();

            VehicleRequestData vehicleRequest = new VehicleRequestData();
            vehicleRequest.Brand = "Ford";
            vehicleRequest.Model = "Focus";
            vehicleRequest.Plate = "ABCD1" + random.Next(0, 9) + random.Next(0, 9);
            vehicleRequest.Color = Color.Blue.Name;
            vehicleRequest.Details = "El carro tiene" + random.Next(2, 4) + "puertas";
            vehicleRequest.DateAndTime = DateTime.Now;
            vehicleRequest.RequestByDepartment = SmartCadDatabase.SearchObject<DepartmentTypeData>(SmartCadHqls.GetCustomHql(
                    SmartCadHqls.GetDepartmentsTypeByCode,random.Next(1, 5)));
            vehicleRequest.RequestByOfficcer = "Pedro Perez";
            vehicleRequest.RequestFor = "Robo";
            SmartCadDatabase.SaveObject<VehicleRequestData>(vehicleRequest);                                    
        }
        
        private static void CreateCameraData(StructData myStruct, int number) 
        {
            Random random = new Random();
           
            CameraData camera = new CameraData();
            camera.Type = (cameraType[0] as CameraTypeData);
            camera.ConnectionType = (connectionCameraType[0] as ConnectionTypeData);
            camera.Name = "Camera" + random.Next().ToString();      
            camera.Ip = "133.5.31."+number.ToString();//7
            camera.IpServer = "127.0.0.1";
            camera.Port = "80";            
            camera.Login = "login" + random.Next().ToString();
            camera.Password = "pass";            
            camera.FrameRate = "0";
            camera.StreamType = "Jpeg";
            camera.Resolution = "640x480";
            camera.Struct = myStruct;       
            SmartCadDatabase.SaveObject<CameraData>(camera);

            camera = new CameraData();
            camera.Type = (cameraType[0] as CameraTypeData);
            camera.ConnectionType = (connectionCameraType[1] as ConnectionTypeData);
            camera.Name = "Camera" + random.Next().ToString();
            camera.Ip = "161.53.120." + number.ToString();//119           
            camera.IpServer = "127.0.0.1";
            camera.Port = "80";
            camera.Login = "login" + random.Next().ToString();
            camera.Password = "pass";
            camera.FrameRate = "0";
            camera.StreamType = "Jpeg";
            camera.Resolution = "640x480";
            camera.Struct = myStruct;         
            SmartCadDatabase.SaveObject<CameraData>(camera);

            camera = new CameraData();
            camera.Type = (cameraType[0] as CameraTypeData);
            camera.ConnectionType = (connectionCameraType[2] as ConnectionTypeData);
            camera.Name = "Camera" + random.Next().ToString();
            camera.Ip = "136.165.99." + number.ToString();//86
            camera.IpServer = "127.0.0.1";
            camera.Port = "80";
            camera.Login = "login" + random.Next().ToString();
            camera.Password = "pass";
            camera.FrameRate = "0";
            camera.StreamType = "MJpeg";
            camera.Resolution = "320x240";
            camera.Struct = myStruct;         
            SmartCadDatabase.SaveObject<CameraData>(camera);           

        }

        static int DataLoggerCustomCode = 1;
        private static void CreateDataloggerData(StructData myStruct, int number)
        {
            Random random = new Random();

            DataloggerData datalogger = new DataloggerData();
            datalogger.Name = "Datalogger" + random.Next().ToString();
            datalogger.CustomCode = DataLoggerCustomCode++;
            datalogger.Ip = "134.5.31." + number.ToString();
            datalogger.IpServer = "127.0.0.1";
            datalogger.Login = "user1" + number.ToString();
            datalogger.Password = "password1" + number.ToString();
            datalogger.Port = number.ToString();
            datalogger.Struct = myStruct;

            SmartCadDatabase.SaveObject<DataloggerData>(datalogger);

            SensorTelemetryData sensor = new SensorTelemetryData()
            {
                Name = "Sensor" + random.Next().ToString(),
                Variable = "Variable" + random.Next().ToString(),
                Datalogger = datalogger
            };
            SmartCadDatabase.SaveObject<SensorTelemetryData>(sensor);

            SensorTelemetryThresholdData threshold = new SensorTelemetryThresholdData();
            threshold.High = random.Next(101,1000)*number+1;
            threshold.Low = random.Next(1,100)*number;
            threshold.Sensor = sensor;
            SmartCadDatabase.SaveObject<SensorTelemetryThresholdData>(threshold);

            sensor.Thresholds = new HashedSet();
            sensor.Thresholds.Add(threshold);

            SmartCadDatabase.SaveOrUpdate(sensor);
            SmartCadDatabase.SaveOrUpdate(threshold);

            datalogger.SensorTelemetrys = new HashedSet();            
            datalogger.SensorTelemetrys.Add(sensor);

            
            
            SmartCadDatabase.SaveOrUpdate(datalogger);
        }

        private static DepartmentStationData GetDepartmentStation(DepartmentTypeData departmentType)
        {
            Random random = new Random();
            int position = random.Next(50) % departmentType.DepartmentZones.Count;
            IList deptZoneList = new ArrayList(departmentType.DepartmentZones);
            DepartmentZoneData departmentZone = deptZoneList[position] as DepartmentZoneData;
            position = random.Next(50) % departmentZone.SetDepartmentStations.Count;
            IList stationList = new ArrayList(departmentZone.SetDepartmentStations);
            return stationList[position] as DepartmentStationData;
        }

        private static void CreatePublicLineInitialData()
        {
            PublicLineData publicLine;
            string[][] lines = {
                new string[]{"3048", "Rodrigo Meneses", "La Urbina" },
                new string[]{"3053", "Oscar Ortegano", "Sabana Grande" },
                new string[]{"3051", "Julio Ynojosa", "Bello Monte" },
                new string[]{"3052", "Alden Torres", "El Silencio" },
                new string[]{"3049", "Armando Torres", "La Trinidad" },
                new string[]{"3054", "Susana Chang", "Petare" },
                new string[]{"3050", "Fernando Silva", "El Paraso" }
            };

            foreach (string[] pubLineStr in lines)
            {
                publicLine = new PublicLineData();
                publicLine.Telephone = pubLineStr[0];
                publicLine.Name = pubLineStr[1];
                PublicLineAddressData address = new PublicLineAddressData(new AddressData(pubLineStr[2], string.Empty, string.Empty, string.Empty));
                //address.PublicLine = publicLine;
                publicLine.Address = address;
                SmartCadDatabase.SaveObject(publicLine);
            }
        }

        private static void CreateAccess(UserResourceData resource, UserApplicationData application)
        {
            CreateAccess(resource, new UserApplicationData[] { application });
        }

        private static void CreateAccess( 
            UserPermissionData permission,
            UserApplicationData[] applications)
        {
            foreach (UserApplicationData application in applications)
            {
                UserAccessData access = new UserAccessData(
                    permission.Name + "+" + application.Name,
                    permission,
                    application
                    );

                SmartCadDatabase.SaveNamedObject<UserAccessData>(access);
            }
        }

        private static void CreateAccess(UserResourceData resource, UserApplicationData[] applications)
        {
            //SmartCadDatabase.InitializeLazy(resource, resource.Actions);
            foreach (UserActionData action in resource.Actions)
            {
                UserPermissionData permission;

                permission = new UserPermissionData(
                    resource.Name + "+" + action.Name,
                    resource,
                    action
                    );

                permission = SmartCadDatabase.SaveNamedObject<UserPermissionData>(permission);

                foreach (UserApplicationData application in applications)
                {
                    UserAccessData access = new UserAccessData(
                        permission.Name + "+" + application.Name,
                        permission,
                        application
                        );

                    SmartCadDatabase.SaveNamedObject<UserAccessData>(access);
                }
            }
        }

        private static void CreateInitialUserAccounts()
        {            
            OperatorData userAccount;

            UserRoleData dispachtOperatorRole = SmartCadDatabase.SearchNamedObject<UserRoleData>("DispatchOperator");
            UserRoleData administratorRole = SmartCadDatabase.SearchNamedObject<UserRoleData>("Administrator");

            #region Supervisor
            userAccount = new OperatorData();
            userAccount.FirstName = "Internal System Supervisor";
            userAccount.LastName = "";
            userAccount.PersonId = "6666666666666";
            userAccount.AgentID = "6666666666666";
            userAccount.Login = "supervisor66666666666666666666666666666666666666666666666666";
            userAccount.Password = CryptoUtil.Hash("supervisor");
            userAccount.Role = dispachtOperatorRole;
            userAccount.Birthday = DateTime.Now;
            userAccount.CategoryHistoryList = null;
            SmartCadDatabase.SaveObject<OperatorData>(userAccount);
            #endregion

            //CREAR OPERADOR ADMINISTRADOR

            OperatorData adminAccount = new OperatorData();
            adminAccount.FirstName = "Administrador";
            adminAccount.LastName = "Administrador";
            adminAccount.PersonId = 1 + "";
            adminAccount.AgentID = 1 + "";
            adminAccount.Login = "Administrador";
            adminAccount.Password = CryptoUtil.Hash("Administrador");
            adminAccount.Role = administratorRole;
            adminAccount.Birthday = DateTime.Parse("12/12/1980");
            adminAccount.Windows = false;
            adminAccount.CategoryHistoryList = null;
            SmartCadDatabase.SaveObject<OperatorData>(adminAccount);
        }

        private static void CreateDeploymentUserAccounts()
        {
            OperatorData userAccount;

            UserRoleData firstLevelOperatorRole = SmartCadDatabase.SearchNamedObject<UserRoleData>("FirstLevelOperator");
            UserRoleData dispachtOperatorRole = SmartCadDatabase.SearchNamedObject<UserRoleData>("DispatchOperator");
            UserRoleData administratorRole = SmartCadDatabase.SearchNamedObject<UserRoleData>("Administrator");
            UserRoleData firstLevelSupervisorRole = SmartCadDatabase.SearchNamedObject<UserRoleData>("FirstLevelSupervisor");
            UserRoleData dispatchSupervisorRole = SmartCadDatabase.SearchNamedObject<UserRoleData>("DispatchSupervisor");
            UserRoleData generalSupervisorRole = SmartCadDatabase.SearchNamedObject<UserRoleData>("GeneralSupervisor");

            // COMMENTED FOR BATAAN PROJECT. AA.
            //UserRoleData lprRole = SmartCadDatabase.SearchNamedObject<UserRoleData>("LprOperator");
            //UserRoleData snRole = SmartCadDatabase.SearchNamedObject<UserRoleData>("SocialNetworksOperator");
            UserRoleData alarmRole = SmartCadDatabase.SearchNamedObject<UserRoleData>("AlarmOperator");

            UserRoleData reportRole = SmartCadDatabase.SearchNamedObject<UserRoleData>("ReportAnalist");
            DepartmentTypeData police = SmartCadDatabase.SearchNamedObject<DepartmentTypeData>(DPPOLICE);
            DepartmentTypeData fire = SmartCadDatabase.SearchNamedObject<DepartmentTypeData>(DPFIRE);
            DepartmentTypeData ambumetro = SmartCadDatabase.SearchNamedObject<DepartmentTypeData>(DPAMBUMETRO);
            DepartmentTypeData cicpc = SmartCadDatabase.SearchNamedObject<DepartmentTypeData>(DPCICPC);
            DepartmentTypeData firepre = SmartCadDatabase.SearchNamedObject<DepartmentTypeData>(DPFIREPRE);
            DepartmentTypeData firerescue = SmartCadDatabase.SearchNamedObject<DepartmentTypeData>(DPFIRERESCUE);
            DepartmentTypeData precivil = SmartCadDatabase.SearchNamedObject<DepartmentTypeData>(DPPRECIVIL);
            DepartmentTypeData trans = SmartCadDatabase.SearchNamedObject<DepartmentTypeData>(DPTRANS);

            OperatorCategoryData categoryTraining = SmartCadDatabase.SearchNamedObject<OperatorCategoryData>("OnTraining");
            
            OperatorCategoryHistoryData operatorCategory = null;
            ArrayList list = new ArrayList();
            list.Add(police);
            list.Add(fire);
            list.Add(ambumetro);
            list.Add(cicpc);
            list.Add(firepre);
            list.Add(firerescue);
            list.Add(precivil);
            list.Add(trans);

            // Crea operadores de primer nivel
            for (int i = 1; i <= 10; i++)
            {
                userAccount = new OperatorData();
                userAccount.FirstName = "user" + i;
                userAccount.LastName = "user" + i;
                userAccount.PersonId = (1110 + i).ToString();
                userAccount.AgentID = (1000 + i).ToString();
                userAccount.AgentPassword = (1000 + i).ToString();
                userAccount.Login = "user" + i;
                userAccount.Password = CryptoUtil.Hash("asd" + i);
                userAccount.Role = firstLevelOperatorRole;
                userAccount.Birthday = DateTime.Now;
                userAccount.CategoryHistoryList = new HashedSet();
                
                operatorCategory = new OperatorCategoryHistoryData();
                operatorCategory.Category = categoryTraining;
                operatorCategory.StartDate = DateTime.Now;
                operatorCategory.Operator = userAccount;
                userAccount.CategoryHistoryList.Add(operatorCategory);
                
                SmartCadDatabase.SaveObject<OperatorData>(userAccount);
            }

            // Crea operadores de despacho
            for (int i = 1; i <= 10; i++)
            {
                userAccount = new OperatorData();
                userAccount.FirstName = "disp" + i;
                userAccount.LastName = "disp" + i;
                userAccount.PersonId = (5550 + i).ToString();
                userAccount.AgentID = (5550 + i).ToString();
                userAccount.AgentPassword = (5550 + i).ToString();
                userAccount.Login = "disp" + i;
                userAccount.Password = CryptoUtil.Hash("asd" + i);
                userAccount.Role = dispachtOperatorRole;
                userAccount.DepartmentTypes = list;
                userAccount.Birthday = DateTime.Now;
                userAccount.CategoryHistoryList = new HashedSet();

                operatorCategory = new OperatorCategoryHistoryData();
                operatorCategory.Category = categoryTraining;
                operatorCategory.StartDate = DateTime.Now;
                operatorCategory.Operator = userAccount;
                userAccount.CategoryHistoryList.Add(operatorCategory);

                SmartCadDatabase.SaveObject<OperatorData>(userAccount);
            }
            
            // Crea operadores de administracin
            for (int i = 1; i <= 10; i++)
            {
                userAccount = new OperatorData();
                userAccount.FirstName = "adm" + i;
                userAccount.LastName = "adm" + i;
                userAccount.PersonId = (0111 + i).ToString();
                userAccount.AgentID = (0111 + i).ToString();
                userAccount.Login = "adm" + i;
                userAccount.Password = CryptoUtil.Hash("adm" + i);
                userAccount.Role = administratorRole;
                userAccount.Birthday = DateTime.Now;
                userAccount.CategoryHistoryList = new HashedSet();

                operatorCategory = new OperatorCategoryHistoryData();
                operatorCategory.Category = categoryTraining;
                operatorCategory.StartDate = DateTime.Now;
                operatorCategory.Operator = userAccount;
                userAccount.CategoryHistoryList.Add(operatorCategory);

                SmartCadDatabase.SaveObject<OperatorData>(userAccount);
            }

            //COMMENTED FOR PROJECT BATAAN. AA
            // Crea operadores de alarmas
            for (int i = 1; i <= 10; i++)
            {
                userAccount = new OperatorData();
                userAccount.FirstName = "alarm" + i;
                userAccount.LastName = "alarm" + i;
                userAccount.PersonId = (0311 + i).ToString();
                userAccount.AgentID = (0311 + i).ToString();
                userAccount.Login = "alarm" + i;
                userAccount.Password = CryptoUtil.Hash("alarm" + i);
                userAccount.Role = alarmRole;
                userAccount.Birthday = DateTime.Now;
                userAccount.CategoryHistoryList = new HashedSet();

                operatorCategory = new OperatorCategoryHistoryData();
                operatorCategory.Category = categoryTraining;
                operatorCategory.StartDate = DateTime.Now;
                operatorCategory.Operator = userAccount;
                userAccount.CategoryHistoryList.Add(operatorCategory);

                SmartCadDatabase.SaveObject<OperatorData>(userAccount);
            }

            //COMMENTED FOR PROJECT BATAAN. AA
            //// Crea operadores de Social Networks
            //for (int i = 1; i <= 2; i++)
            //{
            //    userAccount = new OperatorData();
            //    userAccount.FirstName = "sn" + i;
            //    userAccount.LastName = "sn" + i;
            //    userAccount.PersonId = (0411 + i).ToString();
            //    userAccount.AgentID = (0411 + i).ToString();
            //    userAccount.Login = "sn" + i;
            //    userAccount.Password = CryptoUtil.Hash("sn" + i);
            //    userAccount.Role = snRole;
            //    userAccount.Birthday = DateTime.Now;
            //    userAccount.CategoryHistoryList = new HashedSet();

            //    operatorCategory = new OperatorCategoryHistoryData();
            //    operatorCategory.Category = categoryTraining;
            //    operatorCategory.StartDate = DateTime.Now;
            //    operatorCategory.Operator = userAccount;
            //    userAccount.CategoryHistoryList.Add(operatorCategory);

            //    SmartCadDatabase.SaveObject<OperatorData>(userAccount);
            //}

            // Crea operadores de Reportes
            for (int i = 1; i <= 10; i++)
            {
                userAccount = new OperatorData();
                userAccount.FirstName = "rpt" + i;
                userAccount.LastName = "rpt" + i;
                userAccount.PersonId = (0211 + i).ToString();
                userAccount.AgentID = (0211 + i).ToString();
                userAccount.Login = "rpt" + i;
                userAccount.Password = CryptoUtil.Hash("rpt" + i);
                userAccount.Role = reportRole;
                userAccount.Birthday = DateTime.Now;
                userAccount.CategoryHistoryList = new HashedSet();

                operatorCategory = new OperatorCategoryHistoryData();
                operatorCategory.Category = categoryTraining;
                operatorCategory.StartDate = DateTime.Now;
                operatorCategory.Operator = userAccount;
                userAccount.CategoryHistoryList.Add(operatorCategory);

                SmartCadDatabase.SaveObject<OperatorData>(userAccount);
            }

            // COMMENTED FOR BATAAN PROJECT. AA.
            // Crea operadores de LPR
            //for (int i = 1; i <= 2; i++)
            //{
            //    userAccount = new OperatorData();
            //    userAccount.FirstName = "lpr" + i;
            //    userAccount.LastName = "lpr" + i;
            //    userAccount.PersonId = (0511 + i).ToString();
            //    userAccount.AgentID = (0511 + i).ToString();
            //    userAccount.Login = "lpr" + i;
            //    userAccount.Password = CryptoUtil.Hash("lpr" + i);
            //    userAccount.Role = lprRole;
            //    userAccount.Birthday = DateTime.Now;
            //    userAccount.CategoryHistoryList = new HashedSet();

            //    operatorCategory = new OperatorCategoryHistoryData();
            //    operatorCategory.Category = categoryTraining;
            //    operatorCategory.StartDate = DateTime.Now;
            //    operatorCategory.Operator = userAccount;
            //    userAccount.CategoryHistoryList.Add(operatorCategory);

            //    SmartCadDatabase.SaveObject<OperatorData>(userAccount);
            //}

            //Crear Supervisores de Primer Nivel
            IList supervisorspn = new ArrayList();
            for (int i = 1; i <= 3; i++)
            {
                userAccount = new OperatorData();
                userAccount.FirstName = "supervisorpn" + i;
                userAccount.LastName = "supervisorpn" + i;
                userAccount.PersonId = (9000 + i).ToString();
                userAccount.AgentID = (9000 + i).ToString();
                userAccount.Login = "supervisorpn" + i;
                userAccount.Password = CryptoUtil.Hash("super" + i);
                userAccount.Role = firstLevelSupervisorRole;
                userAccount.Birthday = DateTime.Now;
                userAccount.CategoryHistoryList = new HashedSet();

                operatorCategory = new OperatorCategoryHistoryData();
                operatorCategory.Category = categoryTraining;
                operatorCategory.StartDate = DateTime.Now;
                operatorCategory.Operator = userAccount;
                userAccount.CategoryHistoryList.Add(operatorCategory);

                SmartCadDatabase.SaveObject<OperatorData>(userAccount);
            }

            //Crear Supervisores de Despacho
            IList supervisorsdesp = new ArrayList();
            for (int i = 1; i <= 3; i++)
            {
                userAccount = new OperatorData();
                userAccount.FirstName = "supervisord" + i;
                userAccount.LastName = "supervisord" + i;
                userAccount.PersonId = (9110 + i).ToString();
                userAccount.AgentID = (9110 + i).ToString();
                userAccount.Login = "supervisord" + i;
                userAccount.Password = CryptoUtil.Hash("super" + i);
                userAccount.Role = dispatchSupervisorRole;
                userAccount.Birthday = DateTime.Now;
                userAccount.DepartmentTypes = new ArrayList();
                userAccount.DepartmentTypes.Add(list[i%list.Count]);
                userAccount.CategoryHistoryList = new HashedSet();

                operatorCategory = new OperatorCategoryHistoryData();
                operatorCategory.Category = categoryTraining;
                operatorCategory.StartDate = DateTime.Now;
                operatorCategory.Operator = userAccount;
                userAccount.CategoryHistoryList.Add(operatorCategory);

                SmartCadDatabase.SaveObject<OperatorData>(userAccount);
            }

            //Crear Supervisores Generales
            for (int i = 1; i <= 3; i++)
            {
                userAccount = new OperatorData();
                userAccount.FirstName = "supervisorg" + i;
                userAccount.LastName = "supervisorg" + i;
                userAccount.PersonId = (9120 + i).ToString();
                userAccount.AgentID = (9120 + i).ToString();
                userAccount.Login = "supervisorg" + i;
                userAccount.Password = CryptoUtil.Hash("super" + i);
                userAccount.Role = generalSupervisorRole;
                userAccount.Birthday = DateTime.Now;
                userAccount.DepartmentTypes = new ArrayList();
                userAccount.DepartmentTypes = list;
                userAccount.CategoryHistoryList = new HashedSet();

                operatorCategory = new OperatorCategoryHistoryData();
                operatorCategory.Category = categoryTraining;
                operatorCategory.StartDate = DateTime.Now;
                operatorCategory.Operator = userAccount;
                userAccount.CategoryHistoryList.Add(operatorCategory);

                SmartCadDatabase.SaveObject<OperatorData>(userAccount);
            }            
        }

        private static void CreateRolesInitialData()
        {
            UserProfileData administratorProfile = SmartCadDatabase.SearchNamedObject<UserProfileData>("Administrator");
            UserProfileData firstLevelProfile = SmartCadDatabase.SearchNamedObject<UserProfileData>("FirstLevel");
            UserProfileData disptachProfile = SmartCadDatabase.SearchNamedObject<UserProfileData>("Dispatch");
            UserProfileData reportProfile = SmartCadDatabase.SearchNamedObject<UserProfileData>("Report");
            UserProfileData firstLevelSupervisorProfile = SmartCadDatabase.SearchNamedObject<UserProfileData>(ResourceInitialDataLoader.GetString2("FirstLevelSupervisorName"));
            UserProfileData cctvProfile = SmartCadDatabase.SearchNamedObject<UserProfileData>("Cctv");

            // COMMENTED FOR BATAAN PROJECT. AA.
            //UserProfileData lprProfile = SmartCadDatabase.SearchNamedObject<UserProfileData>("LPR");
            UserProfileData alarmProfile = SmartCadDatabase.SearchNamedObject<UserProfileData>("Alarm");
            //UserProfileData snProfile = SmartCadDatabase.SearchNamedObject<UserProfileData>("SocialNetworks");
            
            UserProfileData dispatchSupervisorProfile = SmartCadDatabase.SearchNamedObject<UserProfileData>(ResourceInitialDataLoader.GetString2("DispatchSupervisorName"));
            UserProfileData generalSupervisorProfile = SmartCadDatabase.SearchNamedObject<UserProfileData>("GeneralSupervisor");
            
            UserRoleData administratorRole = new UserRoleData(
                "Administrator",
                ResourceInitialDataLoader.GetString2("Administrator"),
                ResourceInitialDataLoader.GetString2("FullAccess"), 
                new object[1] { administratorProfile }
                );
            administratorRole.Immutable = true;
            SmartCadDatabase.SaveNamedObject<UserRoleData>(administratorRole);
            
            UserRoleData firstLevel = new UserRoleData(
                "FirstLevelOperator",
                ResourceInitialDataLoader.GetString2("FirstLevelOperator"),
                ResourceInitialDataLoader.GetString2("FirstLevelOperator"),
                new object[1] { firstLevelProfile }
                );
            firstLevel.Immutable = true;
            SmartCadDatabase.SaveNamedObject<UserRoleData>(firstLevel);


            UserRoleData cctv = new UserRoleData(
                "CctvOperator",
                ResourceInitialDataLoader.GetString2("CctvOperator"),
                ResourceInitialDataLoader.GetString2("CctvOperator"),
                new object[1] { cctvProfile }
                );
            cctv.Immutable = true;

            
            SmartCadDatabase.SaveNamedObject<UserRoleData>(cctv);

            //COMMENTED FOR PROJECT BATAAN. AA
            UserRoleData alarm = new UserRoleData(
               "AlarmOperator",
               ResourceInitialDataLoader.GetString2("AlarmOperator"),
               ResourceInitialDataLoader.GetString2("AlarmOperator"),
               new object[1] { alarmProfile }
               );
            alarm.Immutable = true;
            SmartCadDatabase.SaveNamedObject<UserRoleData>(alarm);

            //COMMENTED FOR PROJECT BATAAN. AA
            //UserRoleData socialN = new UserRoleData(
            //   "SocialNetworksOperator",
            //   ResourceInitialDataLoader.GetString2("SocialNetworksOperator"),
            //   ResourceInitialDataLoader.GetString2("SocialNetworksOperator"),
            //   new object[1] { snProfile }
            //   );
            //socialN.Immutable = true;
            //SmartCadDatabase.SaveNamedObject<UserRoleData>(socialN);

            // COMMENTED FOR BATAAN PROJECT. AA.
            //UserRoleData Lpr = new UserRoleData(
            //   "LprOperator",
            //   ResourceInitialDataLoader.GetString2("LprOperator"),
            //   ResourceInitialDataLoader.GetString2("LprOperator"),
            //   new object[1] { lprProfile }
            //   );
            //Lpr.Immutable = true;
            //SmartCadDatabase.SaveNamedObject<UserRoleData>(Lpr);

            UserRoleData dispatchOperator = new UserRoleData(
                "DispatchOperator",
                ResourceInitialDataLoader.GetString2("DispatchOperator"),
                ResourceInitialDataLoader.GetString2("DispatchOperator"),
                new object[1] { disptachProfile }
                );
            dispatchOperator.Immutable = true;
            SmartCadDatabase.SaveNamedObject<UserRoleData>(dispatchOperator);

            UserRoleData reportOperator = new UserRoleData(
                "ReportAnalist",
                ResourceInitialDataLoader.GetString2("ReportAnalist"),
                ResourceInitialDataLoader.GetString2("ReportAnalist"),
                new object[1] { reportProfile }
                );
            reportOperator.Immutable = true;
            SmartCadDatabase.SaveNamedObject<UserRoleData>(reportOperator);

            UserRoleData firstLevelSupervisor = new UserRoleData(
                "FirstLevelSupervisor",
                ResourceInitialDataLoader.GetString2("FirstLevelSupervisor"),
                ResourceInitialDataLoader.GetString2("FirstLevelSupervisor"),
                new object[1] { firstLevelSupervisorProfile }
                );
            firstLevelSupervisor.Immutable = true;
            SmartCadDatabase.SaveNamedObject<UserRoleData>(firstLevelSupervisor);

            UserRoleData dispatchSupervisor = new UserRoleData(
                "DispatchSupervisor",
                ResourceInitialDataLoader.GetString2("DispatchSupervisor"),
                ResourceInitialDataLoader.GetString2("DispatchSupervisor"),
                new object[1] { dispatchSupervisorProfile }
                );
            dispatchSupervisor.Immutable = true;
            SmartCadDatabase.SaveNamedObject<UserRoleData>(dispatchSupervisor);

            UserRoleData generalSupervisor = new UserRoleData(
                "GeneralSupervisor",
                ResourceInitialDataLoader.GetString2("GeneralSupervisor"),
                ResourceInitialDataLoader.GetString2("GeneralSupervisor"),
                new object[1] { generalSupervisorProfile }
                );
            generalSupervisor.Immutable = true;
            SmartCadDatabase.SaveNamedObject<UserRoleData>(generalSupervisor);

            if (SmartCadConfiguration.SmartCadSection.ServerElement.InitialDataElement.CreateTestData)
            {
                UserRoleData developer = new UserRoleData(
                    "Developer",
                    ResourceInitialDataLoader.GetString2("Developer"),
                    ResourceInitialDataLoader.GetString2("Developer"),
                    
                    // COMMENTED FOR BATAAN PROJECT. AA.
                    //new object[10] {
                    new object[9] {
                    administratorProfile,
                    firstLevelProfile,
                    disptachProfile,
                    reportProfile, 
                    dispatchSupervisorProfile,
                    firstLevelSupervisorProfile,
                    generalSupervisorProfile, 
                    // COMMENTED FOR BATAAN PROJECT. AA.
                     alarmProfile,
                    // COMMENTED FOR BATAAN PROJECT. AA.
                    // lprProfile,
                    cctvProfile
                    }

                    );
                developer.Immutable = true;
                SmartCadDatabase.SaveNamedObject<UserRoleData>(developer);
            }
        }

        private static void CreateProfileInitialData()
        {
            //Administrador
            UserProfileData administratorProfile = new UserProfileData(
                "Administrator",
                ResourceInitialDataLoader.GetString2("Administrator"),
                ResourceInitialDataLoader.GetString2("FullAccess")
            );
            administratorProfile.Immutable = true;
            ISet accessesAdministration = new ListSet(SmartCadDatabase.SearchObjects(
               SmartCadHqls.GetCustomHql(SmartCadHqls.GetUserAccessByApplication, "Administration")));
            accessesAdministration.AddAll(new ListSet(SmartCadDatabase.SearchObjects(
                SmartCadHqls.GetCustomHql(SmartCadHqls.GetUserAccessByApplication, "Map"))));
            administratorProfile.Accesses = accessesAdministration;
            //administratorProfile.Accesses = new HashedSet(SmartCadDatabase.SearchObjects(
            //    SmartCadHqls.GetCustomHql(SmartCadHqls.GetUserAccessByApplication, "Administration")));
            administratorProfile = SmartCadDatabase.SaveNamedObject<UserProfileData>(administratorProfile);

            //FirstLevel
            UserProfileData firstLevelProfile = new UserProfileData(
                "FirstLevel",
                ResourceInitialDataLoader.GetString2("FirstLevel"),
                ResourceInitialDataLoader.GetString2("FullAccess")
            );
            firstLevelProfile.Immutable = true;
            ISet accesses = new HashedSet(SmartCadDatabase.SearchObjects(
                SmartCadHqls.GetCustomHql(SmartCadHqls.GetUserAccessByApplication, "FirstLevel")));
            accesses.AddAll(new HashedSet(SmartCadDatabase.SearchObjects(
               SmartCadHqls.GetCustomHql(SmartCadHqls.GetUserAccessByApplication, "Map"))));
            //accesses.Union(new HashedSet(SmartCadDatabase.SearchObjects(
            //    SmartCadHqls.GetCustomHql(SmartCadHqls.GetUserAccessByApplication, "Map"))));
            firstLevelProfile.Accesses = accesses;
            firstLevelProfile = SmartCadDatabase.SaveNamedObject<UserProfileData>(firstLevelProfile);


            //CCTV
             UserProfileData cctvProfile = new UserProfileData(
                "Cctv",
                ResourceInitialDataLoader.GetString2("Cctv"),
                ResourceInitialDataLoader.GetString2("FullAccess")
            );
           cctvProfile.Immutable = true;
             accesses = new HashedSet(SmartCadDatabase.SearchObjects(
                SmartCadHqls.GetCustomHql(SmartCadHqls.GetUserAccessByApplication, "Cctv")));
             accesses.AddAll(new HashedSet(SmartCadDatabase.SearchObjects(
                SmartCadHqls.GetCustomHql(SmartCadHqls.GetUserAccessByApplication, "Map"))));
            cctvProfile.Accesses = accesses;
            cctvProfile = SmartCadDatabase.SaveNamedObject<UserProfileData>(cctvProfile);


            // COMMENTED FOR BATAAN PROJECT. AA.
            //Alarm
            UserProfileData alarmProfile = new UserProfileData(
                "Alarm",
                ResourceInitialDataLoader.GetString2("Alarm"),
                ResourceInitialDataLoader.GetString2("FullAccess")
            );
            alarmProfile.Immutable = true;
            accesses = new HashedSet(SmartCadDatabase.SearchObjects(
               SmartCadHqls.GetCustomHql(SmartCadHqls.GetUserAccessByApplication, "Alarm")));
            accesses.AddAll(new HashedSet(SmartCadDatabase.SearchObjects(
               SmartCadHqls.GetCustomHql(SmartCadHqls.GetUserAccessByApplication, "Map"))));
            accesses.AddAll(new HashedSet(SmartCadDatabase.SearchObjects(
               SmartCadHqls.GetCustomHql(SmartCadHqls.GetUserAccessByApplication, "Cctv"))));
            alarmProfile.Accesses = accesses;
            alarmProfile = SmartCadDatabase.SaveNamedObject<UserProfileData>(alarmProfile);


            // COMMENTED FOR BATAAN PROJECT. AA.
            //SocialNetworks
            //UserProfileData snProfile = new UserProfileData(
            //    "SocialNetworks",
            //    ResourceInitialDataLoader.GetString2("SocialNetworks"),
            //    ResourceInitialDataLoader.GetString2("FullAccess")
            //);
            //snProfile.Immutable = true;
            //accesses = new HashedSet(SmartCadDatabase.SearchObjects(
            //   SmartCadHqls.GetCustomHql(SmartCadHqls.GetUserAccessByApplication, "SocialNetworks")));
            //accesses.AddAll(new HashedSet(SmartCadDatabase.SearchObjects(
            //   SmartCadHqls.GetCustomHql(SmartCadHqls.GetUserAccessByApplication, "Map"))));
            //snProfile.Accesses = accesses;
            //snProfile = SmartCadDatabase.SaveNamedObject<UserProfileData>(snProfile);

            
            // COMMENTED FOR BATAAN PROJECT. AA.
            //LPR
            //UserProfileData lprProfile = new UserProfileData(
            //    "Lpr",
            //    ResourceInitialDataLoader.GetString2("Lpr"),
            //    ResourceInitialDataLoader.GetString2("FullAccess")
            //);
            //lprProfile.Immutable = true;
            //accesses = new HashedSet(SmartCadDatabase.SearchObjects(
            //   SmartCadHqls.GetCustomHql(SmartCadHqls.GetUserAccessByApplication, "LPR")));
            //accesses.AddAll(new HashedSet(SmartCadDatabase.SearchObjects(
            //   SmartCadHqls.GetCustomHql(SmartCadHqls.GetUserAccessByApplication, "Map"))));
            //lprProfile.Accesses = accesses;
            //lprProfile = SmartCadDatabase.SaveNamedObject<UserProfileData>(lprProfile);


            //Dispatch
            UserProfileData dispatchProfile = new UserProfileData(
                "Dispatch",
                ResourceInitialDataLoader.GetString2("Dispatch"),
                ResourceInitialDataLoader.GetString2("FullAccess")
                );
            dispatchProfile.Immutable = true;
            accesses = new HashedSet(SmartCadDatabase.SearchObjects(
                SmartCadHqls.GetCustomHql(SmartCadHqls.GetUserAccessByApplication, "Dispatch")));
            accesses.AddAll(new HashedSet(SmartCadDatabase.SearchObjects(
                SmartCadHqls.GetCustomHql(SmartCadHqls.GetUserAccessByApplication, "Map"))));
            dispatchProfile.Accesses = accesses;
            dispatchProfile = SmartCadDatabase.SaveNamedObject<UserProfileData>(dispatchProfile);

            //Report
            UserProfileData reportProfile = new UserProfileData(
                "Report",
                ResourceInitialDataLoader.GetString2("Reports"),
                ResourceInitialDataLoader.GetString2("FullAccess")
                );
            reportProfile.Immutable = true;
            reportProfile.Accesses = new HashedSet(SmartCadDatabase.SearchObjects(
                SmartCadHqls.GetCustomHql(SmartCadHqls.GetUserAccessByApplication, "Report")));
            reportProfile = SmartCadDatabase.SaveNamedObject<UserProfileData>(reportProfile);

            //FirstLevelSupervisor
            UserProfileData firstLevelSupervisor = new UserProfileData(
                ResourceInitialDataLoader.GetString2("FirstLevelSupervisorName"),
                ResourceInitialDataLoader.GetString2("FirstLevelSupervisor"),
                ResourceInitialDataLoader.GetString2("FirstLevelSupervisorAccess")
                );
            firstLevelSupervisor.Immutable = true;
            firstLevelSupervisor.Accesses = new HashedSet(SmartCadDatabase.SearchObjects(
                SmartCadHqls.GetCustomHql(SmartCadHqls.GetUserAccessByApplicationResource, UserApplicationData.Supervision.Name, ResourceInitialDataLoader.GetString2("FirstLevelSupervisorName"))));
            firstLevelSupervisor = SmartCadDatabase.SaveNamedObject<UserProfileData>(firstLevelSupervisor);

            //DispatchSupervisor
            UserProfileData dispatchSupervisor = new UserProfileData(
                ResourceInitialDataLoader.GetString2("DispatchSupervisorName"),
                ResourceInitialDataLoader.GetString2("DispatchSupervisor"),
                ResourceInitialDataLoader.GetString2("DispatchSupervisorAccess")
                );
            dispatchSupervisor.Immutable = true;
            dispatchSupervisor.Accesses = new HashedSet(SmartCadDatabase.SearchObjects(
                SmartCadHqls.GetCustomHql(SmartCadHqls.GetUserAccessByApplicationResource, UserApplicationData.Supervision.Name, ResourceInitialDataLoader.GetString2("DispatchSupervisorName"))));
            dispatchSupervisor = SmartCadDatabase.SaveNamedObject<UserProfileData>(dispatchSupervisor);

            //GeneralSupervisor
            UserProfileData generalSupervisor = new UserProfileData(
                "GeneralSupervisor",
                ResourceInitialDataLoader.GetString2("GeneralSupervisor"),
                ResourceInitialDataLoader.GetString2("FullAccess")
                );
            generalSupervisor.Immutable = true;
            accesses = new HashedSet(SmartCadDatabase.SearchObjects(
                SmartCadHqls.GetCustomHql(SmartCadHqls.GetUserAccessByApplicationResource, UserApplicationData.Supervision.Name, "FirstLevelSupervisorName")));
            IList temp = SmartCadDatabase.SearchObjects(
                SmartCadHqls.GetCustomHql(SmartCadHqls.GetUserAccessByApplicationResource, UserApplicationData.Supervision.Name, "DispatchSupervisorName"));
            accesses.Add(temp[0]);
			temp = SmartCadDatabase.SearchObjects(
				SmartCadHqls.GetCustomHql(SmartCadHqls.GetUserAccessByApplicationResource, UserApplicationData.Supervision.Name, "GeneralSupervisorName"));
			accesses.Add(temp[0]);
            generalSupervisor.Accesses = accesses;
            generalSupervisor = SmartCadDatabase.SaveNamedObject<UserProfileData>(generalSupervisor);
        }

        private static void CreateRankDepartmentInitialData()
        {
            #region setRankPoliceMetro
            //Policia metropolitana
            DepartmentTypeData departmentType = new DepartmentTypeData(DPPOLICE);
            departmentType = SmartCadDatabase.SearchObject<DepartmentTypeData>(departmentType);
            
            
            RankData rank = new RankData("Supervisor general", "Supervisor general", departmentType);
            SmartCadDatabase.SaveObject<RankData>(rank);                        
            rank = new RankData("Supervisor por la zona 1", "Supervisor por la zona 1", departmentType);
            SmartCadDatabase.SaveObject<RankData>(rank);
            rank = new RankData("Supervisor por la zona 2", "Supervisor por la zona 2", departmentType);
            SmartCadDatabase.SaveObject<RankData>(rank);
            rank = new RankData("Supervisor por la zona 3", "Supervisor por la zona 3", departmentType);
            SmartCadDatabase.SaveObject<RankData>(rank);
            rank = new RankData("Supervisor por la zona 4", "Supervisor por la zona 4", departmentType);            
            SmartCadDatabase.SaveObject<RankData>(rank);
            rank = new RankData("Supervisor por la zona 5", "Supervisor por la zona 5", departmentType);
            SmartCadDatabase.SaveObject<RankData>(rank);
            rank = new RankData("Supervisor por la zona 6", "Supervisor por la zona 6", departmentType);
            SmartCadDatabase.SaveObject<RankData>(rank);
            rank = new RankData("Supervisor por la zona 7", "Supervisor por la zona 7", departmentType);
            SmartCadDatabase.SaveObject<RankData>(rank);
            rank = new RankData("Supervisor por la zona 8", "Supervisor por la zona 8", departmentType);
            SmartCadDatabase.SaveObject<RankData>(rank);
            rank = new RankData("Supervisor por la zona 9", "Supervisor por la zona 9", departmentType);
            SmartCadDatabase.SaveObject<RankData>(rank);
            rank = new RankData("Supervisor por la zona 10", "Supervisor por la zona 10", departmentType);
            SmartCadDatabase.SaveObject<RankData>(rank);
            rank = new RankData("Conductor del comandante", "Conductor del comandante", departmentType);
            SmartCadDatabase.SaveObject<RankData>(rank);
            rank = new RankData("Jefe de los servicios", "Jefe de los servicios", departmentType);
            SmartCadDatabase.SaveObject<RankData>(rank);
            rank = new RankData("Parquero y prevencin", "Parquero y prevencin", departmentType);
            SmartCadDatabase.SaveObject<RankData>(rank);
            rank = new RankData("Prevencin", "Prevencin", departmentType);
            SmartCadDatabase.SaveObject<RankData>(rank);
            rank = new RankData("Puesto policial Garca Carballo", "Puesto policial Garca Carballo", departmentType);
            SmartCadDatabase.SaveObject<RankData>(rank);
            rank = new RankData("A/O jefatura caricuao", "A/O jefatura caricuao", departmentType);
            SmartCadDatabase.SaveObject<RankData>(rank);
            rank = new RankData("Jefe de administracin", "Jefe de administracin", departmentType);
            SmartCadDatabase.SaveObject<RankData>(rank);
            rank = new RankData("Apoyo de area", "Apoyo de area", departmentType);
            SmartCadDatabase.SaveObject<RankData>(rank);
            rank = new RankData("Ajubenpol", "Ajubenpol", departmentType);
            SmartCadDatabase.SaveObject<RankData>(rank);
            rank = new RankData("Mecnico I", "Mecnico I", departmentType);
            SmartCadDatabase.SaveObject<RankData>(rank);
            rank = new RankData("Curso comando motorizado", "Curso comando motorizado", departmentType);
            SmartCadDatabase.SaveObject<RankData>(rank);
            rank = new RankData("Grupo 'A' patrullaje", "Grupo 'A' patrullaje", departmentType);
            SmartCadDatabase.SaveObject<RankData>(rank);
            rank = new RankData("Grupo 'B' patrullaje", "Grupo 'B' patrullaje", departmentType);
            SmartCadDatabase.SaveObject<RankData>(rank);
            rank = new RankData("Recorrido UD-3", "Recorrido UD-3", departmentType);
            SmartCadDatabase.SaveObject<RankData>(rank);
            rank = new RankData("Conductor del super", "Conductor del super", departmentType);
            SmartCadDatabase.SaveObject<RankData>(rank);
            rank = new RankData("PCA. Redoma R. Pineda", "PCA. Redoma R. Pineda", departmentType);
            SmartCadDatabase.SaveObject<RankData>(rank);
            rank = new RankData("Grupo 'C' patrullaje", "Grupo 'C' patrullaje", departmentType);
            SmartCadDatabase.SaveObject<RankData>(rank);
            #endregion

            foreach (DepartmentTypeData departmentTypeAux in SmartCadDatabase.SearchObjects(new DepartmentTypeData()))
            {
                RankData rankAux = new RankData("Dependencia de " + departmentTypeAux.Name, "Dependencia de " + departmentTypeAux.Name, departmentTypeAux);
                SmartCadDatabase.SaveObject<RankData>(rankAux);
            }
        }
        
        private static void CreatePositionDepartmentInitialData()
        {
            #region setPoliceMetro
            //Policia metropolitana
            DepartmentTypeData departmentType = new DepartmentTypeData(DPPOLICE);
            departmentType = SmartCadDatabase.SearchObject<DepartmentTypeData>(departmentType);
            
            PositionData position;

            position = new PositionData();
            position.Name = ResourceInitialDataLoader.GetString2("Agent");
            position.Description = ResourceInitialDataLoader.GetString2("Agent");
            position.DepartmentType = departmentType;            
            SmartCadDatabase.SaveObject(position);

            position = new PositionData();
            position.Name = ResourceInitialDataLoader.GetString2("Distinguido");
            position.Description = ResourceInitialDataLoader.GetString2("Distinguido");
            position.DepartmentType = departmentType;
            SmartCadDatabase.SaveObject(position);

            position = new PositionData();
            position.Name = ResourceInitialDataLoader.GetString2("SecondOfficer");
            position.Description = ResourceInitialDataLoader.GetString2("SecondOfficer");
            position.DepartmentType = departmentType;
            SmartCadDatabase.SaveObject(position);

            position = new PositionData();
            position.Name = ResourceInitialDataLoader.GetString2("FirstOfficer");
            position.Description = ResourceInitialDataLoader.GetString2("FirstOfficer");
            position.DepartmentType = departmentType;
            SmartCadDatabase.SaveObject(position);

            position = new PositionData();
            position.Name = ResourceInitialDataLoader.GetString2("SecondSargent");
            position.Description = ResourceInitialDataLoader.GetString2("SecondSargent");
            position.DepartmentType = departmentType;
            SmartCadDatabase.SaveObject(position);

            position = new PositionData();
            position.Name = ResourceInitialDataLoader.GetString2("FirstSargent");
            position.Description = ResourceInitialDataLoader.GetString2("FirstSargent");
            position.DepartmentType = departmentType;
            SmartCadDatabase.SaveObject(position);

            position = new PositionData();
            position.Name = ResourceInitialDataLoader.GetString2("UpperSargent");
            position.Description = ResourceInitialDataLoader.GetString2("UpperSargent");
            position.DepartmentType = departmentType;
            SmartCadDatabase.SaveObject(position);

            position = new PositionData();
            position.Name = ResourceInitialDataLoader.GetString2("SubInspector");
            position.Description = ResourceInitialDataLoader.GetString2("SubInspector");
            position.DepartmentType = departmentType;
            SmartCadDatabase.SaveObject(position);

            position = new PositionData();
            position.Name = ResourceInitialDataLoader.GetString2("SubCommissioner");
            position.Description = ResourceInitialDataLoader.GetString2("SubCommissioner");
            position.DepartmentType = departmentType;
            SmartCadDatabase.SaveObject(position);

            position = new PositionData();
            position.Name = ResourceInitialDataLoader.GetString2("Inspector");
            position.Description = ResourceInitialDataLoader.GetString2("Inspector");
            position.DepartmentType = departmentType;
            SmartCadDatabase.SaveObject(position);

            position = new PositionData();
            position.Name = ResourceInitialDataLoader.GetString2("Commissioner");
            position.Description = ResourceInitialDataLoader.GetString2("Commissioner");
            position.DepartmentType = departmentType;
            SmartCadDatabase.SaveObject(position);

            #endregion

            foreach (DepartmentTypeData departmentTypeAux in SmartCadDatabase.SearchObjects(new DepartmentTypeData()))
            {
                PositionData positionAux = new PositionData();
                positionAux.Name = "Jerarquia de " + departmentTypeAux.Name;
                positionAux.Description = "Jerarquia de " + departmentTypeAux.Name;
                positionAux.DepartmentType = departmentTypeAux;
                SmartCadDatabase.SaveObject<PositionData>(positionAux);
            }
        }

        private static void CreateDepartmentTypeInitialData()
        {
         
            //Policia Metropolitana
            DepartmentTypeData departmentType = new DepartmentTypeData(DPPOLICE);
            departmentType.Dispatch = true;  
            departmentType.RestartFrequency = DepartmentTypeData.Frequency.None;
            DepartmentTypeCounterData counter = new DepartmentTypeCounterData();
            counter.Value = 0;
            counter.RestartDate = DateTime.Now;
            counter = (DepartmentTypeCounterData)SmartCadDatabase.SaveOrUpdate(counter);
            departmentType.Counter = counter;
            departmentType = SmartCadDatabase.SaveObject<DepartmentTypeData>(departmentType);
            CreateDepartmentZone(departmentType);

            //Bomberos
            departmentType = new DepartmentTypeData(DPFIRE);
            departmentType.Dispatch = true;
            departmentType.RestartFrequency = DepartmentTypeData.Frequency.None;
            counter = new DepartmentTypeCounterData();
            counter.Value = 0;
            counter.RestartDate = DateTime.Now;
            counter = (DepartmentTypeCounterData)SmartCadDatabase.SaveOrUpdate(counter);
            departmentType.Counter = counter;
            departmentType = SmartCadDatabase.SaveObject<DepartmentTypeData>(departmentType);
            CreateDepartmentZone(departmentType);

            //Bomberos (Unidad de rescate)
            departmentType = new DepartmentTypeData(DPFIRERESCUE);
            departmentType.Dispatch = true;
            departmentType.RestartFrequency = DepartmentTypeData.Frequency.None;
            counter = new DepartmentTypeCounterData();
            counter.Value = 0;
            counter.RestartDate = DateTime.Now;
            counter = (DepartmentTypeCounterData)SmartCadDatabase.SaveOrUpdate(counter);
            departmentType.Counter = counter;
            departmentType = SmartCadDatabase.SaveObject<DepartmentTypeData>(departmentType);
            CreateDepartmentZone(departmentType);

            //Bomberos (Atencin pre-hospitalaria)
            departmentType = new DepartmentTypeData(DPFIREPRE);
            departmentType.Dispatch = true;
            counter = new DepartmentTypeCounterData();
            counter.Value = 0;
            counter.RestartDate = DateTime.Now;
            counter = (DepartmentTypeCounterData)SmartCadDatabase.SaveOrUpdate(counter);
            departmentType.Counter = counter;
            departmentType.RestartFrequency = DepartmentTypeData.Frequency.None;
            departmentType = SmartCadDatabase.SaveObject<DepartmentTypeData>(departmentType);
            CreateDepartmentZone(departmentType);

            //Trnsito terrestre
            departmentType = new DepartmentTypeData(DPTRANS);
            departmentType.Dispatch = true;
            counter = new DepartmentTypeCounterData();
            counter.Value = 0;
            counter.RestartDate = DateTime.Now;
            counter = (DepartmentTypeCounterData)SmartCadDatabase.SaveOrUpdate(counter);
            departmentType.Counter = counter;
            departmentType.RestartFrequency = DepartmentTypeData.Frequency.None;
            departmentType = SmartCadDatabase.SaveObject<DepartmentTypeData>(departmentType);
            CreateDepartmentZone(departmentType);

            //Ambulancias metropolitanas
            departmentType = new DepartmentTypeData(DPAMBUMETRO);
            departmentType.Dispatch = true;
            counter = new DepartmentTypeCounterData();
            counter.Value = 0;
            counter.RestartDate = DateTime.Now;
            counter = (DepartmentTypeCounterData)SmartCadDatabase.SaveOrUpdate(counter);
            departmentType.Counter = counter;
            departmentType.RestartFrequency = DepartmentTypeData.Frequency.None;
            departmentType = SmartCadDatabase.SaveObject<DepartmentTypeData>(departmentType);
            CreateDepartmentZone(departmentType);

            //Proteccin Civil
            departmentType = new DepartmentTypeData(DPPRECIVIL);
            departmentType.Dispatch = true;
            counter = new DepartmentTypeCounterData();
            counter.Value = 0;
            counter.RestartDate = DateTime.Now;
            counter = (DepartmentTypeCounterData)SmartCadDatabase.SaveOrUpdate(counter);
            departmentType.Counter = counter;
            departmentType.RestartFrequency = DepartmentTypeData.Frequency.None;
            departmentType = SmartCadDatabase.SaveObject<DepartmentTypeData>(departmentType);
            CreateDepartmentZone(departmentType);

            //CICPC
            departmentType = new DepartmentTypeData(DPCICPC);
            departmentType.Dispatch = true;
            counter = new DepartmentTypeCounterData();
            counter.Value = 0;
            counter.RestartDate = DateTime.Now;
            counter = (DepartmentTypeCounterData)SmartCadDatabase.SaveOrUpdate(counter);
            departmentType.Counter = counter;
            departmentType.RestartFrequency = DepartmentTypeData.Frequency.None;
            departmentType = SmartCadDatabase.SaveObject<DepartmentTypeData>(departmentType);
            CreateDepartmentZone(departmentType);
        }

        private static void CreateDepartmentZone(DepartmentTypeData departmentType)
        {
            DepartmentZoneData zone;
            DepartmentStationData station;
            for (int i = 0; i <7; i++)
            {
                zone = new DepartmentZoneData();
                zone.CustomCode = departmentType.Name + " Zona " + i.ToString();
                zone.Name = zone.CustomCode;
                zone.DepartmentType = departmentType;
                zone.SetDepartmentStations = new HashedSet();


                for (int j = 0; j < 18; j++)
                {
                    station = new DepartmentStationData();
                    station.CustomCode = zone.CustomCode + " Estacin " + j.ToString();
                    station.Name = station.CustomCode;
                    station.DepartmentZone = zone;
                    //station = SmartCadDatabase.SaveObject<DepartmentStationData>(station);
                    zone.SetDepartmentStations.Add(station);
                }
                zone = SmartCadDatabase.SaveObject<DepartmentZoneData>(zone);
                if ((i ==0)||(i==2))
                {
                    //CreateDepartmentZoneAddress(zone);
                }

                
            }
        }

        private static double departmentZoneLong = SmartCadConfiguration.SmartCadSection.DefaultMapLon;
        private static double lonFactor = 0.0045;
        private static void CreateDepartmentZoneAddress(DepartmentZoneData zone)
        {
            DepartmentZoneAddressData address = new DepartmentZoneAddressData();
            address.Zone = zone;
            departmentZoneLong += 0.00015;
            for (int i = 0; i < 3; i++)
            {
                address.Address = new MultipleAddressData();
                if (i == 2)
                {
                    address.Address.Lon = departmentZoneLong + lonFactor;
                    address.Address.Lat = SmartCadConfiguration.SmartCadSection.DefaultMapLat;
                }
                else
                {
                    address.Address.Lon = departmentZoneLong + ((lonFactor - 0.0015) * (i + 1));
                    address.Address.Lat = SmartCadConfiguration.SmartCadSection.DefaultMapLat;
                }
                address.PointNumber = i.ToString();
                address.CustomCode = address.Address.Lon.ToString() + address.Address.Lat.ToString() + i.ToString();
                address.Address.IsSynchronized = true;
                SmartCadDatabase.SaveObject<DepartmentZoneAddressData>(address);

            }
        }

        private static void CreatePriorities()
        {
            IncidentNotificationPriorityData priority = new IncidentNotificationPriorityData();
            priority.Name = "High";
            priority.CustomCode = ResourceInitialDataLoader.GetString2("High_priority");
           // priority.Color = System.Windows.Forms.ControlPaint.LightLight(Color.Green);
            priority.ComponentColor = new ComponentColorData(Color.Green);
            SmartCadDatabase.SaveObject<IncidentNotificationPriorityData>(priority);

            priority = new IncidentNotificationPriorityData();
            priority.Name = "Medium";
            priority.CustomCode = ResourceInitialDataLoader.GetString2("Medium_priority");
            //priority.Color = Color.Yellow;
            priority.ComponentColor = new ComponentColorData(Color.Yellow);
            SmartCadDatabase.SaveObject<IncidentNotificationPriorityData>(priority);

            priority = new IncidentNotificationPriorityData();
            priority.Name = "Low";
            priority.CustomCode = ResourceInitialDataLoader.GetString2("Low_priority");
            //priority.Color = Color.Red;
            priority.ComponentColor = new ComponentColorData(Color.Red);
            SmartCadDatabase.SaveObject<IncidentNotificationPriorityData>(priority);
        }

        private static void CreateUnitTypes()
        {
            #region IncidentTypes
            ArrayList incidentTypes = new ArrayList(SmartCadDatabase.SearchObjects(new IncidentTypeData()));
            #endregion


            #region getDeparments
            //Get deparments
            DepartmentTypeData dpPoliceMetro = new DepartmentTypeData();
            dpPoliceMetro.Name = DPPOLICE;
            dpPoliceMetro = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpPoliceMetro);

            DepartmentTypeData dpFire = new DepartmentTypeData();
            dpFire.Name = DPFIRE;
            dpFire = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFire);

            DepartmentTypeData dpFireRescue = new DepartmentTypeData();
            dpFireRescue.Name = DPFIRERESCUE;
            dpFireRescue = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpFireRescue);

            DepartmentTypeData dpPreFire = new DepartmentTypeData();
            dpPreFire.Name = DPFIREPRE;
            dpPreFire = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpPreFire);

            DepartmentTypeData dpTransit = new DepartmentTypeData();
            dpTransit.Name = DPTRANS;
            dpTransit = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpTransit);

            DepartmentTypeData dpAmbulanceMetro = new DepartmentTypeData();
            dpAmbulanceMetro.Name = DPAMBUMETRO;
            dpAmbulanceMetro = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpAmbulanceMetro);

            DepartmentTypeData dpProtecCivil = new DepartmentTypeData();
            dpProtecCivil.Name = DPPRECIVIL;
            dpProtecCivil = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpProtecCivil);

            DepartmentTypeData dpCICPC = new DepartmentTypeData();
            dpCICPC.Name = DPCICPC;
            dpCICPC = SmartCadDatabase.SearchObject<DepartmentTypeData>(dpCICPC);

            #endregion

            UnitTypeData type = new UnitTypeData();
            type.Name = "Police car";
            type.Color = Color.Black;
            type.DepartmentType = dpPoliceMetro;
            type.IncidentTypes = incidentTypes;
            type.Velocity = 100d;
            type.Image = "POLI1-32.BMP";
            SmartCadDatabase.SaveObject<UnitTypeData>(type);            

            type = new UnitTypeData();
            type.Name = "Ambulance";
            type.Color = Color.Red;
            type.DepartmentType = dpAmbulanceMetro;
            type.IncidentTypes = incidentTypes;
            type.Velocity = 100d;
            type.Image = "AMBU-64.BMP";
            SmartCadDatabase.SaveObject<UnitTypeData>(type);

            type = new UnitTypeData();
            type.Name = "Water truck";
            type.Color = Color.Bisque;
            type.DepartmentType = dpFire;
            type.IncidentTypes = incidentTypes;
            type.Velocity = 100d;
            type.Image = "FIRE1-32.BMP";
            SmartCadDatabase.SaveObject<UnitTypeData>(type);

            type = new UnitTypeData();
            type.Name = "Helicopter";
            type.Color = Color.CornflowerBlue;
            type.DepartmentType = dpTransit;
            type.IncidentTypes = incidentTypes;
            type.Velocity = 200d;
            type.Image = "POLI1-32.BMP";
            SmartCadDatabase.SaveObject<UnitTypeData>(type);
            

            type = new UnitTypeData();
            type.Name = "Moto";
            type.Color = Color.DarkKhaki;
            type.DepartmentType = dpCICPC;
            type.IncidentTypes = incidentTypes;
            type.Velocity = 100d;
            type.Image = "TRUC-64.BMP";
            SmartCadDatabase.SaveObject<UnitTypeData>(type);

            type = new UnitTypeData();
            type.Name = "Mini water truck";
            type.DepartmentType = dpPreFire;
            type.Color = Color.BlanchedAlmond;
            type.IncidentTypes = incidentTypes;
            type.Velocity = 100d;
            type.Image = "FIRE1-32.BMP";
            SmartCadDatabase.SaveObject<UnitTypeData>(type);

            type = new UnitTypeData();
            type.Name = "All terrain";
            type.Color = Color.AliceBlue;
            type.DepartmentType = dpProtecCivil;
            type.IncidentTypes = incidentTypes;
            type.Velocity = 100d;
            type.Image = "TRUC2-32.BMP";
            SmartCadDatabase.SaveObject<UnitTypeData>(type);

            type = new UnitTypeData();
            type.Name = "Special unit";
            type.Color = Color.BurlyWood;
            type.DepartmentType = dpFireRescue;
            type.IncidentTypes = incidentTypes;
            type.Velocity = 100d;
            type.Image = "FIRE-64.BMP";
            SmartCadDatabase.SaveObject<UnitTypeData>(type);

        }

        #region Load Initial Data
        internal static readonly string DataAssembly = "SmartCadCore";
        static void LoadInitialData(bool throwNotFound)
        {
            Assembly assembly = Assembly.Load(DataAssembly);

            Type[] types = assembly.GetTypes();

            foreach (Type type in types)
            {
                if (type.IsClass == true
                    && type.IsAbstract == false
                    && type.IsSubclassOf(typeof(ObjectData)))
                {
                    LoadInitialData(type, throwNotFound);
                }
            }
        }

        static void LoadInitialData(Type type, bool throwNotFoundException)
        {
            object obj;

            try
            {
                ObjectData objectData = (ObjectData) type.GetConstructor(new Type[0] { }).Invoke(new object[0] { });

                foreach (FieldInfo fi in objectData.GetType().GetFields(BindingFlags.Public | BindingFlags.Static))
                {
                    if (fi.IsLiteral == false && fi.GetValue(null) == null)
                    {
                        obj = fi.FieldType.GetConstructor(new Type[0] { }).Invoke(new object[0] { });

                        object[] atttributes = fi.GetCustomAttributes(typeof(InitialDataAttribute), true);
                        InitialDataAttribute initialDataAttribute = null;
                        if (atttributes.Length > 0)
                            initialDataAttribute = (InitialDataAttribute)atttributes[0];

                        if (initialDataAttribute != null)
                        {
                            PropertyInfo property = obj.GetType().GetProperty(initialDataAttribute.PropertyName);
                            if (property != null)
                            {
                                property.SetValue(obj, initialDataAttribute.PropertyValue, null);
                            }
                        }
                        IList result = SmartCadDatabase.SearchObjects((ObjectData)obj);
                        if (result.Count > 0)
                        {
                            obj = result[0];
                            fi.SetValue(objectData, obj);
                        }
                        else
                        {
                            if (throwNotFoundException)
                            {
                                throw new DatabaseObjectException(obj as ObjectData, ResourceInitialDataLoader.GetString2("NotFoundInitialDataObject"));
                            }
                        }
                    }
                }
            }
            catch
            {
                return;
            }
        }
        #endregion
    }
}
