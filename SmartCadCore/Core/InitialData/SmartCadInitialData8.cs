﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using SmartCadCore.Model;

namespace SmartCadCore.Core
{
    public partial class SmartCadInitialData
    {
        public static void CreateVideoAnaliticaRules()
        {
            #region personMovingArea
            VARuleData rule = new VARuleData();
            rule.SaviCode = "{8145163D-C405-4D48-9162-E97D11255422}";
            rule.Name = ResourceInitialDataLoader.GetString2("PersonMovingArea");
            SmartCadDatabase.SaveObject(rule);
            #endregion

            #region vehicleMovingArea
            rule = new VARuleData();
            rule.SaviCode = "{DAAA0657-9291-41C3-A72D-2A7ABC6C80C8}";
            rule.Name = ResourceInitialDataLoader.GetString2("VehicleMovingArea");
            SmartCadDatabase.SaveObject(rule);
            #endregion

            #region personCrossingLine
            rule = new VARuleData();
            rule.SaviCode = "{14C37FDC-7C5A-4B3C-9577-7EB8E81AF20C}";
            rule.Name = ResourceInitialDataLoader.GetString2("PersonCrossingLine");
            SmartCadDatabase.SaveObject(rule);
            #endregion

            #region vehicleCrossingLine
            rule = new VARuleData();
            rule.SaviCode = "{A2C850C6-9984-49D6-BD99-B45505558441}";
            rule.Name = ResourceInitialDataLoader.GetString2("VehicleCrossingLine");
            SmartCadDatabase.SaveObject(rule);
            #endregion

            #region personTailgating
            rule = new VARuleData();
            rule.SaviCode = "{0E4E5113-873A-4C9D-87E5-EED67A85D803}";
            rule.Name = ResourceInitialDataLoader.GetString2("PersonTailgating");
            SmartCadDatabase.SaveObject(rule);
            #endregion

            #region vehicleTailgating
            rule = new VARuleData();
            rule.SaviCode = "{20951C44-D981-47DC-84AE-AD54962036F8}";
            rule.Name = ResourceInitialDataLoader.GetString2("VehicleTailgating");
            SmartCadDatabase.SaveObject(rule);
            #endregion

            #region loitering
            rule = new VARuleData();
            rule.SaviCode = "{BBA7C602-83AB-45f2-A2F1-4C631A86440C}";
            rule.Name = ResourceInitialDataLoader.GetString2("Loitering");
            SmartCadDatabase.SaveObject(rule);
            #endregion

            #region suspiciousObject
            rule = new VARuleData();
            rule.SaviCode = "{B233819A-BD5A-4AD8-97FF-B25509F6AD04}";
            rule.Name = ResourceInitialDataLoader.GetString2("SuspiciousObject");
            SmartCadDatabase.SaveObject(rule);
            #endregion

            #region assetsProtection
            rule = new VARuleData();
            rule.SaviCode = "{7693AB6D-B8CE-43F2-9E4A-A05C400A38DF}";
            rule.Name = ResourceInitialDataLoader.GetString2("AssetsProtection");
            SmartCadDatabase.SaveObject(rule);
            #endregion

        }
    }
}
