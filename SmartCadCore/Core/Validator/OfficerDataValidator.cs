using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.Model;
using System.Collections;
using SmartCadCore.Common;



namespace SmartCadCore.Core.Validator
{
    public class OfficerDataValidator : ObjectDataValidator<OfficerData>
    {

        public override void BeforeDelete(NHibernate.ISession session, OfficerData dataObject)
        {
            try
            {
                dataObject = SmartCadDatabase.RefreshObject(session, dataObject) as OfficerData;
            }
            catch (Exception)
            {
                throw new DatabaseObjectException(dataObject, string.Format(ResourceLoader.GetString2("DeleteOfficerData"), (dataObject.FirstName + " " + dataObject.LastName)));
            }

            //if (HaveOfficerCurrentAssings(dataObject.Code))                
            //    throw new DatabaseObjectException(dataObject, ResourceLoader.GetString2("NoDeleteOfficerUnitsAssigned", (dataObject.FirstName + " " + dataObject.LastName)));
      
            IList officerAssigns = OfficerAssings(dataObject.Code);

            if (officerAssigns.Count > 0)
            {
                DeleteOfficerAssings(officerAssigns);
                
                DeleteDepartmentStationHistoryAssing(dataObject.DepartmentStation.Code);
             }
        
        }


        public override void BeforeUpdate(NHibernate.ISession session, OfficerData dataObject)
        {
            OfficerData dataObjectAux = new OfficerData();
            dataObjectAux.Code = dataObject.Code;
            dataObjectAux = SmartCadDatabase.RefreshObject(dataObjectAux, true) as OfficerData;

            if (HaveOfficerCurrentAssings(dataObjectAux.Code))
                throw new DatabaseObjectException(dataObjectAux, ResourceLoader.GetString2("NoUpdateOfficerUnitsAssigned"));
               
            IList officerAssigns = OfficerAssings(dataObjectAux.Code);
            
            if (officerAssigns.Count > 0)
            {
                DeleteOfficerAssings(officerAssigns);
                                
                DeleteDepartmentStationHistoryAssing(dataObjectAux.DepartmentStation.Code);
            }   
               
        }

        private void DeleteOfficerAssings(IList officerAssings) 
        {
            
            DateTime date = SmartCadDatabase.GetTimeFromBD();

            foreach (UnitOfficerAssignHistoryData var in officerAssings)
            {
                SmartCadDatabase.DeleteObject(var, null, true);
            }                    
        }

        private void DeleteDepartmentStationHistoryAssing(int stationCode) {
            
            string DateNHibernateFormat = "yyyyMMdd HH:mm";
            string date = SmartCadDatabase.GetTimeFromBD().ToString(DateNHibernateFormat);
            string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetDepartmentStationHistoryAssing, stationCode, date);
            ArrayList list = (ArrayList)SmartCadDatabase.SearchBasicObjects(hql);

            foreach (DepartmentStationAssignHistoryData departmentAssing in list)
            {
                SmartCadDatabase.InitializeLazy(departmentAssing, departmentAssing.UnitOfficerAssign);
                if (departmentAssing.UnitOfficerAssign.Count == 0)
                {
                    SmartCadDatabase.DeleteObject(departmentAssing, null);
                }
            }        
        }


        private ArrayList OfficerAssings(int officerCode)
        {
            string DateNHibernateFormat = "yyyyMMdd HH:mm";
            string date = SmartCadDatabase.GetTimeFromBD().ToString(DateNHibernateFormat);
            string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetUnitOfficerAssignsByOfficerCode, officerCode, date);
            ArrayList list = (ArrayList)SmartCadDatabase.SearchBasicObjects(hql);

            return list;
        }

        private bool HaveOfficerCurrentAssings(int officerCode)
        {
            string DateNHibernateFormat = "yyyyMMdd HH:mm";
            string date = SmartCadDatabase.GetTimeFromBD().ToString(DateNHibernateFormat);
            string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.CountUnitOfficerCurrentAssignsByOfficerCode, officerCode, date);
            IList list = (IList)SmartCadDatabase.SearchBasicObjects(hql);

            if (list.Count > 0)
            {
                long count = (long)list[0];
                if (count > 0)
                {
                    return true;
                }
            }
            return false;
        }

    }
}
