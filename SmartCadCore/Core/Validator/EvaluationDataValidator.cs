using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.Model;
using System.Collections;

namespace SmartCadCore.Core.Validator
{
    class EvaluationDataValidator : ObjectDataValidator<EvaluationData>
    {
        public override void BeforeDelete(NHibernate.ISession session, EvaluationData dataObject)
        {
            //DeleteDepartamentTypes
            if (SmartCadDatabase.IsInitialize(dataObject.SetEvaluationDepartamentsType) == false)
                SmartCadDatabase.InitializeLazy(dataObject, dataObject.SetEvaluationDepartamentsType);
            
            foreach (EvaluationDepartmentTypeData dep in dataObject.SetEvaluationDepartamentsType)
            {
                SmartCadDatabase.DeleteObject(dep, session);
            }

            //DeleteApplications
            if (SmartCadDatabase.IsInitialize(dataObject.SetEvaluationUserApplications) == false)
                SmartCadDatabase.InitializeLazy(dataObject, dataObject.SetEvaluationUserApplications);
            foreach (EvaluationUserApplicationData app in dataObject.SetEvaluationUserApplications)
            {
                SmartCadDatabase.DeleteObject(app, session);
            }

            //DeleteQuestions
            if (SmartCadDatabase.IsInitialize(dataObject.SetQuestions) == false)
                SmartCadDatabase.InitializeLazy(dataObject, dataObject.SetQuestions);
            foreach (EvaluationQuestionData ques in dataObject.SetQuestions)
            {
                SmartCadDatabase.DeleteObject(ques, session);
            }
        }
    }
}