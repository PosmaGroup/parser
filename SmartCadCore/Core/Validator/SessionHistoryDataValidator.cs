using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

using SmartCadCore.Model;

namespace SmartCadCore.Core.Validator
{
    public class SessionHistoryDataValidator : ObjectDataValidator<SessionHistoryData>
    {
        public override void AfterUpdate(NHibernate.ISession session, SessionHistoryData dataObject)
        {
            if (dataObject.IsLoggedIn == false && dataObject.EndDateLogin != null)
            {
                SessionStatusHistoryData currentSessionStatusHistory = GetCurrentSessionStatusHistory(dataObject.UserApplication, dataObject.UserAccount);
                if (currentSessionStatusHistory != null)
                {
                    currentSessionStatusHistory.EndDate = dataObject.EndDateLogin;
                    SmartCadDatabase.UpdateObject(session, currentSessionStatusHistory);
                }
            }
        }

        private SessionStatusHistoryData GetCurrentSessionStatusHistory(UserApplicationData application, OperatorData ope)
        {
            return SmartCadDatabase.SearchObject<SessionStatusHistoryData>(SmartCadHqls.GetCustomHql(SmartCadHqls.GetCurrentSessionStatusHistory, application.Code, ope.Login));
        }
    }
}
