using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.Model;
using NHibernate;
using SmartCadCore.Common;
using System.Collections;

namespace SmartCadCore.Core.Validator
{
    public class RoomDataValidator : ObjectDataValidator<RoomData>
    {
        public override void BeforeSave(ISession session, RoomData dataObject)
        {
            SmartCadDatabase.InitializeLazy(dataObject, dataObject.ListSeats);
            if (dataObject.ListSeats != null)
            {
                foreach (RoomSeatData seat in dataObject.ListSeats)
                {
                    try
                    {
                        SmartCadDatabase.SaveObject(session, seat);
                    }
                    catch
                    {
                        throw new DatabaseObjectException(dataObject, ResourceLoader.GetString2("UK_ROOM_SEAT_COMPUTER_NAME", seat.ComputerName));
                    }
                }
            }
        }

        public override void BeforeUpdate(ISession session, RoomData dataObject)
        {

            // Below codes are written just to correct the "error message" and nothing more 
            // when user entry on computer assignment is invalid.

            IList result = SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetAllSeats));

            SmartCadDatabase.InitializeLazy(dataObject, dataObject.ListSeats);

            if (dataObject.ListSeats != null)
            {
                

                foreach (RoomSeatData seat in dataObject.ListSeats)
                {
                    
                        
                        
                        
                        foreach (RoomSeatData SeatInResult in result)
                        {
                            if (SeatInResult.ComputerName == seat.ComputerName)                            
                            {
                                if (SeatInResult.Room.Code != dataObject.Code)
                                {
                                    throw new DatabaseObjectException(dataObject, ResourceLoader.GetString2("UK_ROOM_SEAT_COMPUTER_NAME", seat.ComputerName));
                                }
                            }
                        }
                        
                        
                        
                        
                        
                    
                    
                }
            }
        }

		public override void BeforeDelete(ISession session, RoomData dataObject)
        {
            try
            {
                dataObject = SmartCadDatabase.RefreshObject(session, dataObject) as RoomData;
            }
            catch (Exception)
            {
                throw new DatabaseObjectException(dataObject, string.Format(ResourceLoader.GetString2("DeleteRoomData"), dataObject.Name));
            }
            if (dataObject.ListSeats.Count > 0) {

                foreach (RoomSeatData var in dataObject.ListSeats)
                {
                    SmartCadDatabase.DeleteObject(session, var,false);
				}                        
            }
        }
    }
}
 