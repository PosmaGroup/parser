using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.Model;
using System.Collections;
using SmartCadCore.Common;

namespace SmartCadCore.Core.Validator
{
    public class IndicatorGroupForecastDataValidator : ObjectDataValidator<IndicatorGroupForecastData>
    {
        public override void BeforeDelete(NHibernate.ISession session, IndicatorGroupForecastData dataObject)
        {
            FindForecast(dataObject, false);
            dataObject = (IndicatorGroupForecastData)SmartCadDatabase.RefreshObject(dataObject);
            SmartCadDatabase.InitializeLazy(dataObject, dataObject.ForecastValues);
            foreach (IndicatorForecastData item in dataObject.ForecastValues)
            {
                SmartCadDatabase.DeleteObject(session, item,false); 
            }
        }

        public override void BeforeSave(NHibernate.ISession session, IndicatorGroupForecastData dataObject)
        {
            if (dataObject.General == true)
            {
                CheckAndCloseGeneralForecast(session);
                CheckNameSpecificForecast(session, dataObject);
            }

            if (dataObject.ForecastValues.Count > 0 && dataObject.General == false)
            {
                CheckSpecificForecast(session, dataObject);
                CheckNameSpecificForecast(session, dataObject);
            }
        }

        private void CheckNameSpecificForecast(NHibernate.ISession session, IndicatorGroupForecastData dataObject)
        {
            IList list = SmartCadDatabase.SearchObjects(session, SmartCadHqls.GetCustomHql(SmartCadHqls.CountIndicatorGroupForecastName, dataObject.Name, dataObject.Code));
            if (list != null && list.Count >= 1)
            {
                throw new DatabaseObjectException(dataObject, ResourceLoader.GetString2("InvalidForecastName"));
            }
        }

        public override void BeforeUpdate(NHibernate.ISession session, IndicatorGroupForecastData dataObject)
        {
            FindForecast(dataObject, true);
            CheckNameSpecificForecast(session, dataObject);
            if (dataObject.End > DateTime.Now && dataObject.General == false)
                CheckSpecificForecast(session, dataObject);
        }

        private void FindForecast(IndicatorGroupForecastData dataObject, bool update)
        {
            IndicatorGroupForecastData igfd =
                (IndicatorGroupForecastData)SmartCadDatabase.SearchObjectData(
                SmartCadHqls.GetCustomHql(SmartCadHqls.GetIndicatorGroupForecastDataByCode,
                dataObject.Code));

            if (update == true)
            {
                if (igfd.End < SmartCadDatabase.GetTimeFromBD())
                {
                    string message = string.Empty;
                    message = ResourceLoader.GetString2("InvalidForecastUpdated", dataObject.Name);
                    throw new DatabaseObjectException(dataObject, message);
                }
            }
            else
            {
                if (igfd.Start.AddSeconds(-10) < SmartCadDatabase.GetTimeFromBD())
                {
                    string message = string.Empty;
                    message = ResourceLoader.GetString2("InvalidForecastDeleted", dataObject.Name);
                    throw new DatabaseObjectException(dataObject, message);
                }
            }
        }

        private void CheckAndCloseGeneralForecast(NHibernate.ISession session)
        {
            DateTime maxDT = new DateTime(DateTime.MaxValue.Year,
                                        DateTime.MaxValue.Month,
                                        DateTime.MaxValue.Day,
                                        DateTime.MaxValue.Hour,
                                        DateTime.MaxValue.Minute,
                                        0);
            IList igfds = 
                SmartCadDatabase.SearchObjects(session,
                SmartCadHqls.GetCustomHql(SmartCadHqls.GetIndicatorGroupForecastGeneralNotStarted, 
                ApplicationUtil.GetDataBaseFormattedDate(
                maxDT)));

            foreach (IndicatorGroupForecastData igfd in igfds)
            {
                try
                {
                    igfd.End = igfd.Start;
                    //SmartCadDatabase.InitializeLazy(igfd, igfd.ForecastValues);
                    SmartCadDatabase.UpdateObject(session, igfd);
                }
                catch { }
            }
        }

        private void CheckSpecificForecast(NHibernate.ISession session, IndicatorGroupForecastData data)
        {
            ArrayList listFv = new ArrayList(data.ForecastValues);
            IndicatorForecastData ifd = listFv[0] as IndicatorForecastData;
            string hql = SmartCadHqls.GetCustomHql(
                                SmartCadHqls.GetAmountIndicatorGroupForecastWithStartEndDatesAndDepartmentSimilarities,
                                data.Start.ToString(ApplicationUtil.DateNHibernateFormatWithSeconds),
                                data.End.ToString(ApplicationUtil.DateNHibernateFormatWithSeconds),
                                ifd.DepartmentType != null ? ifd.DepartmentType.Code : 0,
                                data.Code);
            IList list = SmartCadDatabase.SearchObjects(session, hql);

            if (list != null && list.Count == 1)
            {
                IndicatorGroupForecastData item = (IndicatorGroupForecastData)list[0];
                if (item.Code != data.Code)
                    throw new DatabaseObjectException(data, ResourceLoader.GetString2("InvalidForecastData"));
                else
                    session.Evict(item);
            }
            else if (list != null && list.Count > 1)
                throw new DatabaseObjectException(data, ResourceLoader.GetString2("InvalidForecastData"));
        }
    }
}
