﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.Model;
using SmartCadCore.Common;

namespace SmartCadCore.Core.Validator
{
    class OperatorAssignDataValidator: ObjectDataValidator<OperatorAssignData>
    {
        public override void BeforeDelete(NHibernate.ISession session, OperatorAssignData dataObject)
        {
            try
            {
                dataObject = SmartCadDatabase.RefreshObject(session, dataObject) as OperatorAssignData;
            }
            catch (Exception)
            {
               

            }

            //Validate that the action cannot be perform if an operator is connected.
            if (dataObject.StartDate <= DateTime.Now && dataObject.EndDate >= DateTime.Now)
            {
                string hql = @"select count(*) 
                               from SessionHistoryData data left join 
                               data.SessionHistoryStatusList statuses
                               where data.IsLoggedIn = true and
                               statuses.EndDate is null and
                               data.UserAccount.Code = {0}";


                hql = SmartCadHqls.GetCustomHql(hql, dataObject.SupervisedOperator.Code);

                bool connected = (long)SmartCadDatabase.SearchBasicObject(hql) > 0;
                if (connected)
                    throw new DatabaseObjectException(dataObject, ResourceLoader.GetString2("DeleteOperatorAssignOperatorConnectedData"));
            }
            
 	    }

      

    }
}
