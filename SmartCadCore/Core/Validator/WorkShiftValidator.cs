//using System;
//using System.Collections.Generic;
//using System.Text;

//using SmartCadCore.Model;
//using System.Collections;
//using SmartCadCore.Gis;

//namespace SmartCadCore.Core.Validator
//{
//    public class WorkShiftValidator : ObjectDataValidator<WorkShiftData>
//    {
//        public override void BeforeDelete(NHibernate.ISession session, WorkShiftData dataObject)
//        {
//            try
//            {
//                dataObject = SmartCadDatabase.RefreshObject(session, dataObject) as WorkShiftData;
//            }
//            catch (Exception)
//            {
//                throw new DatabaseObjectException(dataObject, string.Format(ResourceLoader.GetString2("DeleteWorkShift"), dataObject.Name));
//            }

//            if (dataObject.Schedules.Count > 0)
//            {
//                foreach (WorkShiftScheduleData schedule in dataObject.Schedules)
//                {
//                    SmartCadDatabase.DeleteObject(session, schedule, false);
//                }
//            }

//            if (dataObject.Operators.Count > 0)
//            {
//                IList deletedAssign = new ArrayList();
//                foreach (OperatorData oper in dataObject.Operators)
//                {
//                    oper.WorkShift = null;
//                    oper.SupervisorOperator = null;

//                    if (SmartCadDatabase.IsInitialize(oper.Supervisors) == false)
//                        SmartCadDatabase.InitializeLazy(oper, oper.Supervisors);

//                    foreach (OperatorAssignData operAssign in oper.Supervisors)
//                    {
//                        if (deletedAssign.Contains(operAssign.Code) == false)
//                        {
//                            SmartCadDatabase.DeleteObject(session, operAssign, true);
//                            deletedAssign.Add(operAssign.Code);
//                        }
//                    }

//                    if (SmartCadDatabase.IsInitialize(oper.Operators) == false)
//                        SmartCadDatabase.InitializeLazy(oper, oper.Operators);

//                    foreach (OperatorAssignData operAssign in oper.Operators)
//                    {
//                        if (deletedAssign.Contains(operAssign.Code) == false)
//                        {
//                            SmartCadDatabase.DeleteObject(session, operAssign, true);
//                            deletedAssign.Add(operAssign.Code);
//                        }
//                    }

//                    //SmartCadDatabase.UpdateObject(session, oper);
//                }

//                IList wsvs = SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetWorkShiftVariationsByWSCode, dataObject.Code));
//                if (wsvs != null)
//                {
//                    foreach (WorkShiftVariationData wsv in wsvs)
//                    {
//                        wsv.WorkShift = null;
//                        SmartCadDatabase.UpdateObject<WorkShiftVariationData>(wsv);
//                    }
//                }
//            }
//        }

//        public override void BeforeUpdate(NHibernate.ISession session, WorkShiftData dataObject)
//        {

//            WorkShiftData dataObjectAux = new WorkShiftData();

//            dataObjectAux.Code = dataObject.Code;

//            dataObjectAux = (WorkShiftData)SmartCadDatabase.RefreshObject(dataObjectAux);

//            if (SmartCadDatabase.IsInitialize(dataObject.Operators) == false)
//                SmartCadDatabase.InitializeLazy(dataObject, dataObject.Operators);

//            if (SmartCadDatabase.IsInitialize(dataObjectAux.Operators) == false)
//                SmartCadDatabase.InitializeLazy(dataObjectAux, dataObjectAux.Operators);

//            if (SmartCadDatabase.IsInitialize(dataObject.Schedules) == false)
//                SmartCadDatabase.InitializeLazy(dataObject, dataObject.Schedules);

//            if (SmartCadDatabase.IsInitialize(dataObjectAux.Schedules) == false)
//                SmartCadDatabase.InitializeLazy(dataObjectAux, dataObjectAux.Schedules);


//            try
//            {

//                int i = dataObject.Schedules.Count;
//            }
//            catch
//            {

//                dataObject.Schedules = new ArrayList(dataObjectAux.Schedules);
//            }

//            //Check changes in operators in the workshift.
//            IList deletedAssigns = new ArrayList();

//            //Remove old schedules
//            foreach (WorkShiftScheduleData schedule in dataObjectAux.Schedules)
//            {
//                if (dataObject.Schedules.Contains(schedule) == false)
//                {
//                    SmartCadDatabase.DeleteObject(session, schedule, false);
//                }
//            }


//            //Check changes in the schedules.
//            deletedAssigns = new ArrayList();
//            //We keep the assign that should NOT be deleted.
//            ArrayList safeAssigns = new ArrayList();
//            //We keep all assign of the operator into the work shift.
//            ArrayList allAssigns = new ArrayList();
//            //We save in the list the assign that are validate
//            foreach (OperatorData oper in dataObjectAux.Operators)
//            {
//                if (!dataObject.Operators.Contains(oper))
//                {
//                    if (SmartCadDatabase.IsInitialize(oper.Supervisors) == false)
//                        SmartCadDatabase.InitializeLazy(oper, oper.Supervisors);

//                    foreach (OperatorAssignData opAss in oper.Supervisors)
//                    {

//                        if (CheckExtraAssign(opAss, dataObject.Schedules) == true)
//                        {
//                            if (safeAssigns.Contains(opAss.Code) == false && allAssigns.Contains(opAss) == false)
//                                safeAssigns.Add(opAss.Code);
//                        }
//                        if (allAssigns.Contains(opAss.Code) == false)
//                            allAssigns.Add(opAss);
//                    }

//                    if (SmartCadDatabase.IsInitialize(oper.Operators) == false)
//                        SmartCadDatabase.InitializeLazy(oper, oper.Operators);

//                    foreach (OperatorAssignData opAss in oper.Operators)
//                    {

//                        if (CheckExtraAssign(opAss, dataObject.Schedules) == true)
//                        {
//                            if (safeAssigns.Contains(opAss.Code) == false && allAssigns.Contains(opAss) == false)
//                                safeAssigns.Add(opAss.Code);
//                        }
//                        if (allAssigns.Contains(opAss.Code) == false)
//                            allAssigns.Add(opAss);
//                    }
//                }
//            }
//            //We save in the list the assign that are validate
//            foreach (WorkShiftScheduleData newSchedule in dataObject.Schedules)
//            {
//                foreach (OperatorData oper in dataObject.Operators)
//                {
//                    if (SmartCadDatabase.IsInitialize(oper.Supervisors) == false)
//                        SmartCadDatabase.InitializeLazy(oper, oper.Supervisors);

//                    foreach (OperatorAssignData opAss in oper.Supervisors)
//                    {

//                        if (((newSchedule.Start <= opAss.StartDate && opAss.EndDate <= newSchedule.End) &&
//                            ServiceUtil.SelectedDateInSchedule(opAss.Day, newSchedule) == true) ||
//                            CheckExtraAssign(opAss, dataObject.Schedules) == true)
//                        {
//                            if (safeAssigns.Contains(opAss.Code) == false && allAssigns.Contains(opAss) == false)
//                                safeAssigns.Add(opAss.Code);
//                        }
//                        if (allAssigns.Contains(opAss.Code) == false)
//                            allAssigns.Add(opAss);
//                    }

//                    if (SmartCadDatabase.IsInitialize(oper.Operators) == false)
//                        SmartCadDatabase.InitializeLazy(oper, oper.Operators);

//                    foreach (OperatorAssignData opAss in oper.Operators)
//                    {

//                        if (((newSchedule.Start <= opAss.StartDate && opAss.EndDate <= newSchedule.End) &&
//                            ServiceUtil.SelectedDateInSchedule(opAss.Day, newSchedule) == true) ||
//                            CheckExtraAssign(opAss, dataObject.Schedules) == true)
//                        {
//                            if (safeAssigns.Contains(opAss.Code) == false && allAssigns.Contains(opAss) == false)
//                                safeAssigns.Add(opAss.Code);
//                        }
//                        if (allAssigns.Contains(opAss.Code) == false)
//                            allAssigns.Add(opAss);
//                    }
//                }
//            }

//            //We deleted those assigns that are not validated.
//            foreach (OperatorAssignData assign in allAssigns)
//            {
//                if (safeAssigns.Contains(assign.Code) == false && deletedAssigns.Contains(assign.Code) == false)
//                {
//                    SmartCadDatabase.DeleteObject(session, assign, true);
//                    deletedAssigns.Add(assign.Code);
//                }
//            }
//        }


     

//        /// <summary>
//        /// Check if the extra assign is validated. Is validated if is outside the scope of the schedule.
//        /// </summary>
//        /// <param name="assign">Assign</param>
//        /// <param name="schedules">Schedules</param>
//        /// <returns>true if assign is validate, otherwise false</returns>
//        private bool CheckExtraAssign(OperatorAssignData assign, IList schedules)
//        {
//            bool result = true;
//            foreach (WorkShiftScheduleData schedule in schedules)
//            {
//                if (CheckExtraAssign(assign, schedule) == false)
//                {
//                    result = false;
//                    break;
//                }
//            }
//            return result;
//        }

//        /// <summary>
//        /// Check if the extra assign is validated. Is validated if is outside the scope of the schedule.
//        /// </summary>
//        /// <param name="assign">Assign</param>
//        /// <param name="schedule">Schedule</param>
//        /// <returns>true if assign is validate, otherwise false</returns>
//        private bool CheckExtraAssign(OperatorAssignData assign, WorkShiftScheduleData schedule)
//        {
//            bool result = false;
//            if (assign != null)
//            {
//                //First if the assign is outside the scope of the schedule on the same day
//                //Second check if the day of the extra is not in the schedule
//                //Third that is an extra assign
//                if ((((assign.EndDate.TimeOfDay <= schedule.Start.TimeOfDay || 
//                      schedule.End.TimeOfDay <= assign.StartDate.TimeOfDay) &&
//                      (ServiceUtil.SelectedDateInSchedule(assign.Day, schedule) == true)) ||
//                    (ServiceUtil.SelectedDateInSchedule(assign.Day, schedule) == false)) &&
//                    assign.SupervisedScheduleVariation != null)
//                    result = true;
//            }
//            return result;
//        }
//    }
//}


