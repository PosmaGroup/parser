using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.Model;
using System.Collections;
using SmartCadCore.Common;



namespace SmartCadCore.Core.Validator
{
    public class UnitTypeDataValidator : ObjectDataValidator<UnitTypeData>
    {
        public override void BeforeDelete(NHibernate.ISession session, UnitTypeData dataObject)
        {
            try
            {
                dataObject = SmartCadDatabase.RefreshObject(session, dataObject) as UnitTypeData;
            }
            catch (Exception)
            {
                throw new DatabaseObjectException(dataObject, string.Format(ResourceLoader.GetString2("DeleteUnitTypeData"),dataObject.Name));
            }
            string queryHQL = SmartCadHqls.GetCustomHql(SmartCadHqls.GetUnitsByType, dataObject.Code);
            IList objectList = SmartCadDatabase.SearchObjects(session, queryHQL);
            if (objectList.Count > 0)
            {
                throw new DatabaseObjectException(dataObject, string.Format(ResourceLoader.GetString2("NoDeleteUnitTypeExistUnits"),dataObject.Name));
            }
            else
            {
                SmartCadDatabase.InitializeLazy(dataObject, dataObject.IncidentTypes);
                dataObject.IncidentTypes.Clear();    
                dataObject.DeletedId = Guid.NewGuid();
            }
        }

        public override void AfterDelete(NHibernate.ISession session, UnitTypeData dataObject)
        {
        }

        private string GetCodesUnits(UnitTypeData dataObject) 
        { 
            StringBuilder unitsCodes = new StringBuilder();
            string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetUnitCodesByType, dataObject.Code);
            
            ArrayList list = (ArrayList)SmartCadDatabase.SearchBasicObjects(hql);
            if (list.Count > 0) 
            {
                bool first = true;
                foreach (int code in list) 
                {
                    if (first) 
                    {
                        unitsCodes.Append("('" + code + "'");
                        first = false;
                    }
                    else
                        unitsCodes.Append(",'" + code + "'");
                }
                unitsCodes.Append(")");
            }
            return unitsCodes.ToString();
        }

        public override void AfterUpdate(NHibernate.ISession session, UnitTypeData dataObject)
        {
            string codes = GetCodesUnits(dataObject);
            if (!string.IsNullOrEmpty(codes)) 
            {
                string image = "";
                if (string.IsNullOrEmpty(dataObject.Image))
                    image = ResourceLoader.GetString("DefaultIconUnitType") + ".BMP";
                else
                {
                    image = dataObject.Image;
                    //TODO: GUSTAVO. Actualizar iconos.
                }
            }
        }

        public override void BeforeUpdate(NHibernate.ISession session, UnitTypeData dataObject)
        {
            
            
            string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.CountUnitsByUnitTypeCode, dataObject.Code);
            long count = (long)SmartCadDatabase.SearchBasicObject(hql);

            if (count > 0)
            {
                throw new DatabaseObjectException(dataObject,ResourceLoader.GetString2("NoUpdateUnitTypeOrgExistUnits"));
            }
            
        }
    }
}
