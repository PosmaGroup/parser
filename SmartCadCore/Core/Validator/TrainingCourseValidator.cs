using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.Model;
using System.Collections;
using SmartCadCore.Common;

namespace SmartCadCore.Core.Validator
{
    public class TrainingCourseValidator : ObjectDataValidator<TrainingCourseData>
    {

        public override void BeforeUpdate(NHibernate.ISession session, TrainingCourseData dataObject)
        {
            TrainingCourseData dataObjectAux = (TrainingCourseData)SmartCadDatabase.SearchObjectData(SmartCadHqls.GetCustomHql(SmartCadHqls.GetTrainingCourseByCode, dataObject.Code));

            long count = (long)SmartCadDatabase.SearchObject(session, SmartCadHqls.GetCustomHql(SmartCadHqls.GetCountTrainingCourseEvaluatingorEvaluated, dataObject.Code, ApplicationUtil.GetDataBaseFormattedDate(DateTime.Now)));
            if (count > 0)
                throw new DatabaseObjectException(dataObject, ResourceLoader.GetString2("TrainingCourseCanNotBeUpdated", dataObject.Name));
        }

        public override void BeforeDelete(NHibernate.ISession session, TrainingCourseData dataObject)
        {
            string DateNHibernateFormat = "yyyyMMdd HH:mm";
            string date = SmartCadDatabase.GetTimeFromBD().ToString(DateNHibernateFormat);

            long count = (long)SmartCadDatabase.SearchObject(session, SmartCadHqls.GetCustomHql(SmartCadHqls.GetCountTrainingCourseEvaluatingorEvaluated, dataObject.Code, ApplicationUtil.GetDataBaseFormattedDate(DateTime.Now)));

            if (count > 0)
                throw new DatabaseObjectException(dataObject, ResourceLoader.GetString2("TrainingCourseCanNotBeDeleted", dataObject.Name));

            IList schedules = SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetTrainingCourseScheduleByCourseCodeWithScheduleParts, dataObject.Code));

			foreach (TrainingCourseScheduleData schedule in schedules)
            {
				foreach (TrainingCourseSchedulePartsData part in schedule.Parts)
                {
                    SmartCadDatabase.DeleteObject(session, part,false);
                }

                if (SmartCadDatabase.IsInitialize(schedule.Operators) == false)
                    SmartCadDatabase.InitializeLazy(schedule, schedule.Operators);

                foreach (OperatorTrainingCourseData oper in schedule.Operators)
                {
                    SmartCadDatabase.DeleteObject(session, oper,false);
                }

                SmartCadDatabase.DeleteObject(session, schedule,false);

            }

            
        }

    }
}