using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.Model;

namespace SmartCadCore.Core.Validator
{
    public class IndicatorDataValidator : ObjectDataValidator<IndicatorData>
    {
        public override void AfterLoad(NHibernate.ISession session, System.Collections.IList dataObject)
        {
            foreach (IndicatorData indicator in dataObject)
            {
                indicator.General = SmartCadDatabase.SearchObject<IndicatorForecastData>(SmartCadHqls.GetCustomHql(
                    SmartCadHqls.GetIndicatorForecastByIndicatorIdTypeCurrentDate, 
                    indicator.Code, 
                    true.ToString().ToLower(), 
                    ApplicationUtil.GetDataBaseFormattedDate(DateTime.Now)));

                indicator.Forecast = SmartCadDatabase.SearchObject<IndicatorForecastData>(SmartCadHqls.GetCustomHql(
                    SmartCadHqls.GetIndicatorForecastByIndicatorIdTypeAndDepartmentTypeCurrentDate, 
                    indicator.Code, 
                    false.ToString().ToLower(), 
                    ApplicationUtil.GetDataBaseFormattedDate(DateTime.Now)));

                if (indicator.Forecast == null)
                {
                    indicator.Forecast = SmartCadDatabase.SearchObject<IndicatorForecastData>(
                        SmartCadHqls.GetCustomHql(SmartCadHqls.GetIndicatorForecastByIndicatorIdTypeCurrentDate, 
                        indicator.Code, 
                        false.ToString().ToLower(), 
                        ApplicationUtil.GetDataBaseFormattedDate(DateTime.Now)));
                }
            }
        }
    }
}
