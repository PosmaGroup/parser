﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.Model;
using System.Collections;
using SmartCadCore.Common;

namespace SmartCadCore.Core.Validator
{
    public class DepartmentTypeValidator : ObjectDataValidator<DepartmentTypeData>
    {
        public override void BeforeDelete(NHibernate.ISession session, DepartmentTypeData dataObject)
        {
            try
            {
                dataObject = SmartCadDatabase.RefreshObject(session, dataObject) as DepartmentTypeData;
            }
            catch (Exception)
            {
                throw new DatabaseObjectException(dataObject, string.Format(ResourceLoader.GetString2("DeleteDepartmentTypeData"), dataObject.Name));
            }
            CheckNotAssociatedObjects(dataObject, true);

            IList depAlerts = SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(@"SELECT objectData FROM DepartmentTypeAlertData objectData WHERE objectData.DepartmentType.Code = {0}", dataObject.Code));
            IList unitAlerts = SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(@"SELECT objectData FROM UnitAlertData objectData WHERE objectData.Unit.DepartmentStation.DepartmentZone.DepartmentType.Code = {0}", dataObject.Code));

            if (depAlerts != null && depAlerts.Count > 0)
                SmartCadDatabase.DeleteObjectCollection(depAlerts, null);

			if (unitAlerts != null)
			{
				foreach (UnitAlertData item in unitAlerts)
					item.Active = false;

				SmartCadDatabase.SaveOrUpdateCollection(unitAlerts, null);
			}


            DepartmentTypeCounterData counter = new DepartmentTypeCounterData();
            counter = ((DepartmentTypeData)SmartCadDatabase.SearchObjectData(SmartCadHqls.GetCustomHql(SmartCadHqls.GetDepartmentTypeCounter, dataObject.Code))).Counter;

            if (counter != null)
                SmartCadDatabase.DeleteObject(counter, null);

        }

        private static void CheckNotAssociatedObjects(DepartmentTypeData dataObject, bool deleting)
        {
            object checker = SmartCadDatabase.SearchBasicObject(
                    SmartCadHqls.GetCustomHql(SmartCadHqls.CheckAssociatedObjectsToDepartmentType, dataObject.Code));

            if (checker != null)
            {
                string message = string.Empty;
                if (deleting)
                {
                    message = ResourceLoader.GetString2("DepartmentTypeCanNotBeDeleted");
                }
                else
                {
                    message = ResourceLoader.GetString2("MessageCanNotChangeDepartmentType");
                }
                throw new DatabaseObjectException(dataObject, string.Format(message, dataObject.Name));
            }
        }

        public override void BeforeSave(NHibernate.ISession session, DepartmentTypeData dataObject)
        {
            DepartmentTypeCounterData counter = new DepartmentTypeCounterData();
            counter.Value = 0;
            counter.RestartDate = DateTime.Now;
            counter = (DepartmentTypeCounterData)SmartCadDatabase.SaveOrUpdate(session, counter);           
            dataObject.Counter = counter;            
        }

        public override void BeforeUpdate(NHibernate.ISession session, DepartmentTypeData dataObject)
        {
            IList unitAlertsToSave = new ArrayList();
            IList unitAlertsToDeactivate = new ArrayList();
            bool found = false;
            UnitAlertData newUnitAlert;

            DepartmentTypeData dataObjectAux = new DepartmentTypeData();
            dataObjectAux.Code = dataObject.Code;
            dataObjectAux = (DepartmentTypeData)SmartCadDatabase.RefreshObject(dataObjectAux);
            
            //Check if an operator can update dispatch property
            if (dataObjectAux.Dispatch == true && dataObject.Dispatch == false) 
            {
                CheckIncidentNotificationAssociated(dataObject);
                
                CheckDispatchAssociated(dataObject);

                IList operators = SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetOperatorsByDepartmentType, dataObject.Code));

                if (operators != null && operators.Count > 0) 
                {
                    for (int i = 0; i < operators.Count; i++)
                    {
                        OperatorData oper = operators[i] as OperatorData;

                        if (oper.DepartmentTypes.Count == 1)
                            throw new DatabaseObjectException(dataObject, ResourceLoader.GetString2("NotUpdatePropertyDispatchObjectAssociated", dataObject.Name));
                        else 
                        {
                            oper.DepartmentTypes.Remove(dataObject);
                        }
                        SmartCadDatabase.UpdateObject(session, oper);
                    }
                }

                //Elimino las asociaciones del organismo con cualquier tipo de incidente.
                IList incTypeDepartments = SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetIncidentTypesDepartmentByDept, dataObject.Code));

                for (int i = 0; i < incTypeDepartments.Count; i++)
                {
                    IncidentTypeDepartmentTypeData incTypeDep = incTypeDepartments[i] as IncidentTypeDepartmentTypeData;

                    SmartCadDatabase.DeleteObject(session, incTypeDep, false);
                }
            }

            //Update Alerts and UnitAlerts
            #region UpdatingAlerts

            if (SmartCadDatabase.IsInitialize(dataObjectAux.Alerts) == false)
                SmartCadDatabase.InitializeLazy(dataObjectAux, dataObjectAux.Alerts);

            if (SmartCadDatabase.IsInitialize(dataObjectAux.Alerts) == true) 
            {
                foreach (DepartmentTypeAlertData depAlert in dataObjectAux.Alerts)
                {
                    if (dataObject.Alerts.Contains(depAlert) == false) 
                            SmartCadDatabase.DeleteObject(depAlert, null);
                    
                }            
            }

			IList units = SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetUnitsByDepartmentTypeCode, dataObject.Code));
			IList unitAlerts = SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetUnitAlertsByDepartmentTypeCode, dataObject.Code));
                       
            foreach (DepartmentTypeAlertData depAlert in dataObject.Alerts)
            {
                found = false;
                foreach (UnitAlertData unitAlert in unitAlerts)
                {
                    if (depAlert.Alert.Code == unitAlert.Alert.Code)
                    {
                        unitAlert.MaxValue = depAlert.MaxValue;
                        unitAlert.Priority = (UnitAlertData.AlertPriority)(int)depAlert.Priority;
						unitAlert.Active = true;
                        unitAlertsToSave.Add(unitAlert);
                        found = true;
                    }
                }
                if (found == false)
                {
                    foreach (UnitData unit in units)
                    {
                        newUnitAlert = new UnitAlertData();
						newUnitAlert.Active = true;
                        newUnitAlert.Alert = depAlert.Alert;
                        newUnitAlert.MaxValue = depAlert.MaxValue;
                        newUnitAlert.Priority = (UnitAlertData.AlertPriority)(int)depAlert.Priority;
                        newUnitAlert.Unit = unit;
                        unitAlertsToSave.Add(newUnitAlert);
                    }
                }
            }

            foreach (UnitAlertData unitAlert in unitAlerts)
            {
                found = false;
                foreach (DepartmentTypeAlertData depAlert in dataObject.Alerts)
                {
                    if (unitAlert.Alert.Code == depAlert.Alert.Code)
                    {
                        found = true;
                        break;
                    }

                }
				if (found == false)
				{
					unitAlert.Active = false;
					unitAlertsToDeactivate.Add(unitAlert);
				}
            }


            if (unitAlertsToSave.Count > 0)
                SmartCadDatabase.SaveOrUpdateCollection(unitAlertsToSave, null);

			if (unitAlertsToDeactivate.Count > 0)
				SmartCadDatabase.SaveOrUpdateCollection(unitAlertsToDeactivate, null);

            #endregion

        }

        private void CheckIncidentNotificationAssociated(DepartmentTypeData dataObject)
        {
            string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetNotificationOpenCountByDepartment, dataObject.Code, (int)IncidentNotificationStatusData.Cancelled.Code, (int)IncidentNotificationStatusData.Closed.Code);

            long count = (long)SmartCadDatabase.SearchBasicObject(hql);
         
            if (count > 0)
            {
                throw new DatabaseObjectException(dataObject, ResourceLoader.GetString2("NotUpdatePropertyDispatchObjectAssociated", dataObject.Name));
            }

        }

        private void CheckDispatchAssociated(DepartmentTypeData dataObject)
        {
            string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetDispatchReportCountByDepartmentTypeCode, dataObject.Code);

            long count = (long)SmartCadDatabase.SearchBasicObject(hql);

            if (count > 0)
            {
                throw new DatabaseObjectException(dataObject, ResourceLoader.GetString2("NotUpdatePropertyDispatchObjectAssociated", dataObject.Name));
            }
            
        }
        
    }
}
