using System;
using System.Collections;
using System.Text;
using NHibernate;
using SmartCadCore.Core;
using SmartCadCore.Model;
using System.ServiceModel;
using NHibernate.Collection;

using System.Collections.Generic;
using SmartCadCore.Common;

namespace SmartCadCore.Core.Validator
{
    public class IncidentNotificationDataValidator : ObjectDataValidator<IncidentNotificationData>
    {
        private void AddStatusHistory(NHibernate.ISession session,
            IncidentNotificationData dataObject, IncidentNotificationData dataObjectAux, bool firstSaved)
        {
            if (dataObjectAux == null ||
                dataObject.Status.Equals(dataObjectAux.Status) == false)
            {
                IncidentNotificationStatusHistoryData temp = null;
                if (dataObject.Code != 0)
                {
                    temp = SmartCadDatabase.SearchBasicObject(
                        SmartCadHqls.GetCustomHql(SmartCadHqls.GetCurrentIncidentNotificationStatusHistory, dataObject.Code))
                        as IncidentNotificationStatusHistoryData;
                }
                if (temp != null)
                {
                    temp.End = SmartCadDatabase.GetTimeFromBD();
                    try
                    {
                        SmartCadDatabase.UpdateObject(session, temp);
                    }
                    catch(Exception ex)
                    {
                        StaleObjectStateException staleObjectException =
                            new StaleObjectStateException(typeof(IncidentNotificationData).Name, dataObjectAux.Code);
                        throw new DatabaseStaleObjectException(dataObjectAux,
                            SmartCadDatabase.GetConcurrencyErrorByObject(dataObjectAux), staleObjectException);
                    }
                }
                temp = new IncidentNotificationStatusHistoryData();
                temp.Start = SmartCadDatabase.GetTimeFromBD();
                temp.Status = dataObject.Status;
                temp.UserAccount = dataObject.DispatchOperator;                    
                //if (dataObject.Status.Equals(IncidentNotificationStatusData.AutomaticSupervisor) == true)
                //{
                //    temp.UserAccount = OperatorData.InternalSupervisor;
                //}
                //else if (dataObject.Status.Equals(IncidentNotificationStatusData.ManualSupervisor) == true)
                //{
                //    temp.UserAccount = OperatorData.InternalSupervisor;
                //}
                //else
                {
                    if (dataObject.Status.Equals(IncidentNotificationStatusData.Cancelled) == true)
                    {
                        temp.Detail = dataObject.CancelDetail;
                        temp.End = SmartCadDatabase.GetTimeFromBD();
                    }
                    else if (dataObject.Status.Equals(IncidentNotificationStatusData.Closed) == true)
                    {
                        temp.End = SmartCadDatabase.GetTimeFromBD();
                    }
                }
                temp.IncidentNotification = dataObject;
                if (firstSaved == true)
                {
                    dataObject.StatusHistoryList = new ArrayList();
                    dataObject.StatusHistoryList.Add(temp);
                }
                else
                {
                    try
                    {
                        SmartCadDatabase.SaveObject(session, temp);
                    }
                    catch (Exception ex)
                    {
                        StaleObjectStateException staleObjectException =
                            new StaleObjectStateException(typeof(IncidentNotificationData).Name, dataObjectAux.Code);
                        throw new DatabaseStaleObjectException(dataObjectAux,
                            SmartCadDatabase.GetConcurrencyErrorByObject(dataObjectAux), staleObjectException);
                    }
                }
            }
            
        }

        private void AddChangeZoneHistory(NHibernate.ISession session, IncidentNotificationData dataObject,
            IncidentNotificationData dataObjectAux)
        {
            bool changed = false;

            IncidentNotificationDepartmentStationHistoryData zone = null;

            if (dataObjectAux.DepartmentStation != null &&
                dataObjectAux.DepartmentStation.CustomCode != dataObject.DepartmentStation.CustomCode)
            {
                zone = SmartCadDatabase.SearchBasicObject(
                    SmartCadHqls.GetCustomHql(SmartCadHqls.GetCurrentIncidentNotificationDepartmentStationHistory, dataObject.Code))
                    as IncidentNotificationDepartmentStationHistoryData;
                zone.End = SmartCadDatabase.GetTimeFromBD();
                SmartCadDatabase.UpdateObject(session, zone);
                changed = true;
            }

            if ((dataObject.DepartmentStation != null && dataObjectAux.DepartmentStation == null) ||
                (changed == true))
            {
                zone = new IncidentNotificationDepartmentStationHistoryData();
                zone.DepartmentStation = dataObject.DepartmentStation;
                zone.IncidentNotification = dataObject;
                zone.UserAccount = dataObject.DispatchOperator;
                zone.Start = SmartCadDatabase.GetTimeFromBD();
                SmartCadDatabase.SaveObject(session, zone);
            }
        }

        public IncidentNotificationStatusData GetLastOperationalStatus(IncidentNotificationData incidentNotification)
        {
            IncidentNotificationStatusData status = incidentNotification.Status;
            if (incidentNotification.RecalculateStatus == true)
            {
                IList historyList = SmartCadDatabase.SearchObjects(
                    SmartCadHqls.GetCustomHql(
                        SmartCadHqls.GetLastIncidentNotificationRealStatus,
                        incidentNotification.Code));
                if (historyList != null && historyList.Count > 0)
                {
                    IncidentNotificationStatusHistoryData lastHistory = historyList[0] as IncidentNotificationStatusHistoryData;
                    status = lastHistory.Status;
                }
            }

            return status;
        }

        public override void BeforeUpdate(NHibernate.ISession session, IncidentNotificationData dataObject)
        {
            IncidentNotificationData dataObjectAux = new IncidentNotificationData();
            dataObject.Status = GetLastOperationalStatus(dataObject);
            dataObjectAux.Code = dataObject.Code;
            dataObjectAux = (IncidentNotificationData)SmartCadDatabase.SearchObjectData( 
                SmartCadDatabase.GetHQL(dataObjectAux));
            if (dataObject.Status == IncidentNotificationStatusData.Pending)
            {
                //SmartCadDatabase.InitializeLazy(session, dataObject, dataObject.SetDispatchOrders);
                dataObject.Status = IncidentNotificationStatusData.Pending;
                if (dataObject.Status != dataObjectAux.Status)
                {//si son iguales solo registrara el cambio de zona
                    AddStatusHistory(session, dataObject, dataObjectAux, false);
                }
                AddChangeZoneHistory(session, dataObject, dataObjectAux);
            }
            else if (dataObject.Status == IncidentNotificationStatusData.Cancelled ||
                dataObject.Status == IncidentNotificationStatusData.Closed)
            {
                dataObject.EndDate = SmartCadDatabase.GetTimeFromBD();
                AddStatusHistory(session, dataObject, dataObjectAux, false);
                AddChangeZoneHistory(session, dataObject, dataObjectAux);
            }
            else if ((dataObjectAux.Status == IncidentNotificationStatusData.Assigned) &&
                     (dataObject.Status == IncidentNotificationStatusData.Assigned) &&
                     (dataObjectAux.DispatchOperator != null) &&
                     (dataObjectAux.DispatchOperator != dataObject.DispatchOperator))
            {
                throw new DatabaseObjectException(dataObject, ResourceLoader.GetString2("NotificationAlreadyAssignedToOperator"));
            }
            else if (dataObject.Status == IncidentNotificationStatusData.Reassigned)
            {
                dataObject.Status = SmartCadDatabase.SearchObject<IncidentNotificationStatusData>(SmartCadHqls.GetCustomHql(SmartCadHqls.IncidentNotificationLastStatus, dataObject.Code));
                AddStatusHistory(session, dataObject, dataObjectAux, false);
                AddChangeZoneHistory(session, dataObject, dataObjectAux);
            }
            else if (dataObject.Status == IncidentNotificationStatusData.Updated ||
                     dataObject.Status == IncidentNotificationStatusData.AutomaticSupervisor ||
                     dataObject.Status == IncidentNotificationStatusData.ManualSupervisor ||
                     dataObject.Status == IncidentNotificationStatusData.Assigned ||
                     dataObject.Status == IncidentNotificationStatusData.Delayed ||
                     dataObject.Status == IncidentNotificationStatusData.InProgress)
            {
                AddStatusHistory(session, dataObject, dataObjectAux, false);
                AddChangeZoneHistory(session, dataObject, dataObjectAux);
            }
        }

        public override void AfterUpdate(ISession session, IncidentNotificationData dataObject)
        {
            if (dataObject.Status.Name == IncidentNotificationStatusData.Cancelled.Name ||
                dataObject.Status.Name == IncidentNotificationStatusData.Closed.Name)
            {
                IncidentData inc = dataObject.ReportBase.Incident;
                UpdateIncidentStatus(session, ref inc, dataObject.DispatchOperator);
                dataObject.ReportBase.Incident = inc;
            }
        }

        private void UpdateIncidentStatus(NHibernate.ISession session, ref IncidentData incidentData, OperatorData closeIncidentOperator)
        {
            incidentData = (IncidentData)SmartCadDatabase.SearchObjectData(SmartCadDatabase.GetHQL<IncidentData>(incidentData));

            long incidentNotificationByIncident = (long)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql("select count(*) from IncidentNotificationData inot where inot.ReportBase.Incident.Code = {0}", incidentData.Code));
            long incidentNotificationClosedOrCancelledByIncident = (long)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql("select count(*) from IncidentNotificationData inot where inot.ReportBase.Incident.Code = {0} and (inot.Status.Code = {1} or inot.Status.Code = {2})", incidentData.Code, IncidentNotificationStatusData.Cancelled.Code, IncidentNotificationStatusData.Closed.Code));

            bool closeIncident = true;

            if (incidentNotificationByIncident > incidentNotificationClosedOrCancelledByIncident + 1)
                closeIncident = false;

            if (closeIncident == true )
            {
                //Close dispatch status of current incidentNotification
                incidentData.CloseOperator = closeIncidentOperator;
                incidentData.Status = IncidentStatusData.Closed;
                incidentData.EndDate = SmartCadDatabase.GetTimeFromBD();
                incidentData.Address.IsSynchronized = false;

                SmartCadDatabase.UpdateObject(session, incidentData);
                //TODO: GUSTAVO. Actualizar el incidente.
            }
        }
    }
}
