﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.Model;
using System.Collections;
using SmartCadCore.Common;

namespace SmartCadCore.Core.Validator
{
	class RouteValidator:ObjectDataValidator<RouteData>
	{

		public override void BeforeUpdate(NHibernate.ISession session, RouteData dataObject)
		{
			if (HasActiveAlerts(dataObject))
				throw new DatabaseObjectException(dataObject, ResourceLoader.GetString2("RouteWithActiveAlerts"));

			RouteData dataObjectAux = new RouteData();
			dataObjectAux.Code = dataObject.Code;
			try
			{
				dataObjectAux = SmartCadDatabase.RefreshObject(dataObjectAux) as RouteData;
				SmartCadDatabase.InitializeLazy(dataObjectAux, dataObjectAux.RouteAddress);
			}
			catch
			{
				throw new DatabaseObjectException(dataObject, ResourceLoader.GetString2("DeleteRouteData"));
			}
			if (dataObject.RouteAddress.Count > 0)
				if (dataObject.RouteAddress.Cast<RouteAddressData>().ToList()[0].Code == 0)
				{
					SmartCadDatabase.DeleteObjectCollection(dataObjectAux.RouteAddress.Cast<RouteAddressData>().ToList(), false);
					SmartCadDatabase.DeleteObjectCollection(
							SmartCadDatabase.SearchObjects(
								SmartCadHqls.GetCustomHql(
									SmartCadHqls.GetRouteUnitAssociations,
									dataObject.Code)), null);
					return;
				}

			if (dataObjectAux.DepartmentType.Code != dataObject.DepartmentType.Code ||
				dataObjectAux.Type != dataObject.Type )
			{
				SmartCadDatabase.DeleteObjectCollection(
						SmartCadDatabase.SearchObjects(
							SmartCadHqls.GetCustomHql(
								SmartCadHqls.GetRouteUnitAssociations,
								dataObject.Code)), null);
			}
		}

		private bool HasActiveAlerts(RouteData dataObject)
		{
			return (long)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(SmartCadHqls.GetCountOfActiveAlertsByRouteCode, dataObject.Code,(int)AlertNotificationData.AlertStatus.Ended)) > 0;					
		}

		public override void BeforeDelete(NHibernate.ISession session, RouteData dataObject)
		{
			if (HasActiveAlerts(dataObject))
                throw new DatabaseObjectException(dataObject, ResourceLoader.GetString2("DeleteRouteWithActiveAlerts", dataObject.Name)); 
			try
			{
				dataObject = SmartCadDatabase.RefreshObject(session, dataObject) as RouteData;
			}
			catch
			{
				throw new DatabaseObjectException(dataObject, ResourceLoader.GetString2("DeleteRouteData"));
			}
			if (dataObject.RouteAddress.Count > 0)
			{
				foreach (RouteAddressData address in dataObject.RouteAddress)
				{
					SmartCadDatabase.DeleteObject(session, address, false);
				}
			}
            
            IList routeAssigns = SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql("SELECT ObjectData FROM WorkShiftRouteData ObjectData WHERE ObjectData.Route.Code = {0}", dataObject.Code));

            if (routeAssigns != null)
            {
                foreach (WorkShiftRouteData route in routeAssigns)
                {  
                    SmartCadDatabase.InitializeLazy(route.Unit, route.Unit.WorkShiftRoutes);
                    route.Unit.WorkShiftRoutes.Remove(route);
                    SmartCadDatabase.UpdateObject(session, route.Unit);

                    SmartCadDatabase.DeleteObject(session, route, true);
                }
            }
		}
	}
}
