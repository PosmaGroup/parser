
using System;
using System.Collections.Generic;
using System.Text;

using SmartCadCore.Model;
using System.Collections;
using NHibernate;
using Iesi.Collections;

namespace SmartCadCore.Core.Validator
{
    public class AlarmReportDataValidator : ObjectDataValidator<AlarmReportData>
    {
        private void AddStatusHistory(IncidentNotificationData dataObject, OperatorData oper)
        {
            IncidentNotificationStatusHistoryData temp = new IncidentNotificationStatusHistoryData();
            temp.Start = SmartCadDatabase.GetTimeFromBD();
            temp.Status = dataObject.Status;
            temp.UserAccount = oper;
            temp.IncidentNotification = dataObject;
            dataObject.StatusHistoryList = new ArrayList();
            dataObject.StatusHistoryList.Add(temp);
        }

        public override void BeforeSave(NHibernate.ISession session, AlarmReportData dataObject)
        {
            dataObject.IncidentNotifications = new ArrayList();
            //dataObject.StatrtIncidentSensorTelemetryTime = SmartCadDatabase.GetTimeFromBD();
            foreach(ReportBaseDepartmentTypeData departmentType in dataObject.ReportBaseDepartmentTypes)
            {
                if (ServiceUtil.DuplicatedNotification(departmentType, dataObject.Incident) == false)
                {
                    IncidentNotificationData notification = new IncidentNotificationData();
                    notification.Comment = string.Empty;
                    notification.CustomCode = Guid.NewGuid().ToString();
                    notification.FriendlyCustomCode = ServiceUtil.GenerateNotificationCustomCode(departmentType.DepartmentType, dataObject.Operator.Code);
                    notification.DepartmentType = departmentType.DepartmentType;
                    notification.ReportBase = dataObject;
                    notification.Priority = departmentType.Priority;
                    notification.CreationDate = SmartCadDatabase.GetTimeFromBD();
                    notification.Status = IncidentNotificationStatusData.New;
                    notification.SetDispatchOrders = new HashedSet();
                    AddStatusHistory(notification, dataObject.Operator);
                    dataObject.IncidentNotifications.Add(notification);
                }
            }
            bool multipleOrganisms = (long)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(SmartCadHqls.IncidentMultipleOrganisms, dataObject.Incident.Code)) + dataObject.IncidentNotifications.Count > 1;
            dataObject.MultipleOrganisms = multipleOrganisms;            
            SmartCadDatabase.UpdateSqlWithOutValidator(session, SmartCadSQL.GetCustomSQL(SmartCadSQL.UpdateIncidentNotificationLastStatusHistoryByIncidentCode, dataObject.Incident.Code));
            SmartCadDatabase.UpdateSqlWithOutValidator(session, 
                SmartCadSQL.GetCustomSQL(
                    SmartCadSQL.InsertIncidentNotificationLastStatusHistoryByIncidentCode, 
                    dataObject.Operator.Code,
                    IncidentNotificationStatusData.Updated.Code,
                    dataObject.Incident.Code));
            SmartCadDatabase.UpdateSqlWithOutValidator(session, SmartCadSQL.GetCustomSQL(SmartCadSQL.UpdateIncidentNotificationStatusByIncidentCode, dataObject.Incident.Code));
            SmartCadDatabase.UpdateSqlWithOutValidator(session, SmartCadSQL.GetCustomSQL(SmartCadSQL.UpdateReportBaseMultipleOrganismsByIncidentCode, Convert.ToInt16(multipleOrganisms), dataObject.Incident.Code));
        }

        public override void BeforeUpdate(NHibernate.ISession session, AlarmReportData dataObject)
        {
            SmartCadDatabase.UpdateSqlWithOutValidator(session, SmartCadSQL.GetCustomSQL(SmartCadSQL.UpdateIncidentNotificationLastStatusHistoryByIncidentCode, dataObject.Incident.Code));
            SmartCadDatabase.UpdateSqlWithOutValidator(session,
                SmartCadSQL.GetCustomSQL(
                    SmartCadSQL.InsertIncidentNotificationLastStatusHistoryByIncidentCode,
                    dataObject.Operator.Code,
                    IncidentNotificationStatusData.Updated.Code,
                    dataObject.Incident.Code));
            SmartCadDatabase.UpdateSqlWithOutValidator(session, SmartCadSQL.GetCustomSQL(SmartCadSQL.UpdateIncidentNotificationStatusByIncidentCode, dataObject.Incident.Code));            
        }
       
    }
}

