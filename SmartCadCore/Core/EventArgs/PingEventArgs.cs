using System;
using System.Collections.Generic;
using System.Text;

namespace SmartCadCore.Core
{
    public delegate void PingEventHandler();

    [Serializable]
    public class PingActionEventArgs : EventArgs
    {
        string application;
        string login;
        int instance;

        public PingActionEventArgs(string application, string login, int instance)
        {
            this.application = application;
            this.login = login;
            this.instance = instance;
        }

        public string Application
        {
            get
            {
                return this.application;
            }
        }

        public string Login
        {
            get
            {
                return this.login;
            }
        }

        public int Instance
        {
            get
            {
                return this.instance;
            }
        }
    }
}
