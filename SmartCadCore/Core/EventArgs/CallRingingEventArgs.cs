﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartCadCore.Core
{
    /// <summary>
    /// This event occurs when a call is being received.
    /// </summary>
    [Serializable]
    public class CallRingingEventArgs : TelephonyActionEventArgs
    {
        private string ani;
        private string agentID;
        private string queue;
        private TimeSpan time;

        /// <summary>
        /// Phone number where call was originated.
        /// </summary>
        public string ANI
        {
            get
            {
                return ani;
            }
            set
            {
                ani = value;
            }
        }
        
        /// <summary>
        /// Agent ID
        /// </summary>
        public string AgentID
        {
            get
            {
                return agentID;
            }
            set
            {
                agentID = value;
            }
        }

        /// <summary>
        /// Queue identifier 
        /// </summary>
        public string Queue
        {
            get
            {
                return queue;
            }
            set
            {
                queue = value;
            }
        }

        /// <summary>
        /// Time when occurs the event
        /// </summary>
        public TimeSpan Time
        {
            get
            {
                return time;
            }
            set
            {
                time = value;
            }
        }

        public CallRingingEventArgs(string ani, string agentID, string queue, TimeSpan eventTime)
        {
            this.ani = ani;
            this.agentID = agentID;
            this.queue = queue;
            this.time = eventTime;
        }
    }
}
