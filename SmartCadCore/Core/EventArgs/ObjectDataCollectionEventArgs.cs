using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using SmartCadCore.Model;

namespace SmartCadCore.Core
{
    [Serializable]
    public class ObjectDataCollectionEventArgs : EventArgs
    {
        private SmartCadContext context;

        /// <summary>
        /// Operator context about who made the change on received object.
        /// </summary>
        public SmartCadContext Context
        {
            get
            {
                return context;
            }
            set
            {
                context = value;
            }
        }

        protected IList objects;

        public IList Objects
        {
            get
            {
                return objects;
            }
			set
			{
				objects = value;
			}
        }

        public ObjectDataCollectionEventArgs(IList objects, SmartCadContext ctx) : this(ctx)
        {
            this.objects = objects;
        }

        protected ObjectDataCollectionEventArgs(SmartCadContext ctx)
        {
            this.Context = ctx;
        }
    }
}
