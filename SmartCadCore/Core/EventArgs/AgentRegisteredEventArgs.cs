﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartCadCore.Core
{
    /// <summary>
    /// Once this event has occurred, it's possible to request agent login.
    /// </summary>
    [Serializable]
    public class AgentRegisteredEventArgs : TelephonyActionEventArgs
    {
        private string thisNumber;
        private string agentID;
        private string queue;
        private TimeSpan time;

        /// <summary>
        /// It refers to the extension number that the agent is taking control.
        /// </summary>
        public string ThisNumber
        {
            get
            {
                return thisNumber;
            }
            set
            {
                thisNumber = value;
            }
        }
        
        /// <summary>
        /// Agent ID.
        /// </summary>
        public string AgentID
        {
            get
            {
                return agentID;
            }
            set
            {
                agentID = value;
            }
        }

        /// <summary>
        /// Queue identifier 
        /// </summary>
        public string Queue
        {
            get
            {
                return queue;
            }
            set
            {
                queue = value;
            }
        }

        /// <summary>
        /// Time when occurs the event
        /// </summary>
        public TimeSpan Time
        {
            get
            {
                return time;
            }
            set
            {
                time = value;
            }
        }

        public AgentRegisteredEventArgs(string number, string agentID, string queue, TimeSpan eventTime)
        {
            this.thisNumber = number;
            this.agentID = agentID;
            this.queue = queue;
            this.time = eventTime;
        }

    }

    [Serializable]
    public class TerminalUnRegisteredEventArgs : TelephonyActionEventArgs
    {
        public TerminalUnRegisteredEventArgs()
        {
        }

    }

    [Serializable]
    public class CallRecordedEventArgs : TelephonyActionEventArgs
    {
        public string PhoneReportCustomCode { get; set; }
        public CallRecordedEventArgs(string phoneReportCustomCode)
        {
            PhoneReportCustomCode = phoneReportCustomCode;
        }

    }
}
