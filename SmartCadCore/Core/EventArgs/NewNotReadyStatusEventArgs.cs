using System;
using System.Collections.Generic;
using System.Text;

namespace SmartCadCore.Core
{
    [Serializable]
    public class NewNotReadyStatusEventArgs : EventArgs
    {
        public bool notReadyStatus;
        public NewNotReadyStatusEventArgs(bool notReadyStatus)
        {
            this.notReadyStatus = notReadyStatus;
        }
    }
}
