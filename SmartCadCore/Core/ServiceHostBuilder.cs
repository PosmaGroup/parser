using System;
using System.Collections.Generic;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Channels;

namespace SmartCadCore.Core
{
    public class ServiceHostBuilder //: ServiceHost
    {
        //private ServiceHostBuilder()
        //{ }

        public static ServiceHost ConfigureHost(Type serviceType, Type implemmentedType, Binding binding, string address)
        {
            ServiceHost host = new ServiceHost(serviceType);
            host.AddServiceEndpoint(implemmentedType, binding, address);

            foreach (ServiceEndpoint ep in host.Description.Endpoints)
            {
                foreach (OperationDescription op in ep.Contract.Operations)
                {
                    DataContractSerializerOperationBehavior dataContractBehavior =
                       op.Behaviors.Remove<DataContractSerializerOperationBehavior>()
                            as DataContractSerializerOperationBehavior;

                    op.Behaviors.Add(new NetDataContractSerializerOperationBehavior(op));
                }
            }

            ServiceThrottlingBehavior serviceThrottlingBehavior = new ServiceThrottlingBehavior();

            BindingBuilder.ConfigureParam(serviceThrottlingBehavior, "MaxConcurrentCalls", "service_Throttling_Behavior_MaxConcurrentCalls");
            BindingBuilder.ConfigureParam(serviceThrottlingBehavior, "MaxConcurrentInstances", "service_Throttling_Behavior_MaxConcurrentInstances");
            BindingBuilder.ConfigureParam(serviceThrottlingBehavior, "MaxConcurrentSessions", "service_Throttling_Behavior_MaxConcurrentSessions");
            
            host.Description.Behaviors.Add(serviceThrottlingBehavior);

            return host;
        }

        public static ServiceHost ConfigureHost(Type serviceType, Type implemmentedType, Binding[] binding, string[] address)
        {
            ServiceHost host = new ServiceHost(serviceType);
            for (int i = 0; i < binding.Length; i++)
            {
                host.AddServiceEndpoint(implemmentedType, binding[i], address[i]);
            }

            foreach (ServiceEndpoint ep in host.Description.Endpoints)
            {
                foreach (OperationDescription op in ep.Contract.Operations)
                {
                    DataContractSerializerOperationBehavior dataContractBehavior =
                       op.Behaviors.Remove<DataContractSerializerOperationBehavior>()
                            as DataContractSerializerOperationBehavior;

                    op.Behaviors.Add(new NetDataContractSerializerOperationBehavior(op));
                }
            }

            ServiceThrottlingBehavior serviceThrottlingBehavior = new ServiceThrottlingBehavior();

            BindingBuilder.ConfigureParam(serviceThrottlingBehavior, "MaxConcurrentCalls", "service_Throttling_Behavior_MaxConcurrentCalls");
            BindingBuilder.ConfigureParam(serviceThrottlingBehavior, "MaxConcurrentInstances", "service_Throttling_Behavior_MaxConcurrentInstances");
            BindingBuilder.ConfigureParam(serviceThrottlingBehavior, "MaxConcurrentSessions", "service_Throttling_Behavior_MaxConcurrentSessions");

            host.Description.Behaviors.Add(serviceThrottlingBehavior);

            return host;
        }

        public static ServiceHost ConfigureHost(object serviceType, Type implemmentedType, Binding binding, string address)
        {
            ServiceHost host = new ServiceHost(serviceType);
            host.AddServiceEndpoint(implemmentedType, binding, address);

            foreach (ServiceEndpoint ep in host.Description.Endpoints)
            {
                foreach (OperationDescription op in ep.Contract.Operations)
                {
                    DataContractSerializerOperationBehavior dataContractBehavior =
                       op.Behaviors.Remove<DataContractSerializerOperationBehavior>()
                            as DataContractSerializerOperationBehavior;

                    op.Behaviors.Add(new NetDataContractSerializerOperationBehavior(op));
                }
            }

            ServiceThrottlingBehavior serviceThrottlingBehavior = new ServiceThrottlingBehavior();

            BindingBuilder.ConfigureParam(serviceThrottlingBehavior, "MaxConcurrentCalls", "service_Throttling_Behavior_MaxConcurrentCalls");
            BindingBuilder.ConfigureParam(serviceThrottlingBehavior, "MaxConcurrentInstances", "service_Throttling_Behavior_MaxConcurrentInstances");
            BindingBuilder.ConfigureParam(serviceThrottlingBehavior, "MaxConcurrentSessions", "service_Throttling_Behavior_MaxConcurrentSessions");

            host.Description.Behaviors.Add(serviceThrottlingBehavior);

            return host;
        }

        public static ServiceHost ConfigureSimpleHost(Type serviceType, Type implemmentedType, Binding binding, string address)
        {
            ServiceHost host = new ServiceHost(serviceType);
            host.AddServiceEndpoint(implemmentedType, binding, address);

            foreach (ServiceEndpoint ep in host.Description.Endpoints)
            {
                foreach (OperationDescription op in ep.Contract.Operations)
                {
                    DataContractSerializerOperationBehavior dataContractBehavior =
                       op.Behaviors.Remove<DataContractSerializerOperationBehavior>()
                            as DataContractSerializerOperationBehavior;

                    op.Behaviors.Add(new NetDataContractSerializerOperationBehavior(op));
                }
            }

            return host;
        }
    }
}
