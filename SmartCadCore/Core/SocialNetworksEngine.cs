﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Reflection;
using System.Collections;
using SmartCadCore.Core.Indicators;
using SmartCadCore.Statistic;
using SmartCadCore.Common;
using SmartCadCore.Model;
using System.Xml.Linq;
using System.Linq;
using System.Globalization;
using System.IO;
using System.Net;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using Iesi.Collections.Generic;
using Iesi.Collections;

namespace SmartCadCore.Core
{
    public static class SocialNetworksEngine
    {
        private static Thread runThread = null;
        private static Thread runThread2 = null;
        private static bool isRunning = false;
        private static bool isMentionsRunning = false;
        
        private static string oAuthConsumerKey = "AcatEc63slEf82Nwha8JIQ";
        private static string oAuthConsumerSecret = "zq2Ka9jYEDpBkudcs9WLSlxoxINJSHt8sTfv14KErU";
        private static string oAuthUrl = "https://api.twitter.com/oauth2/token";
        private static string resource_url = "http://api.twitter.com/1.1/statuses/update.json";
        private static string mentions_url = "https://api.twitter.com/1.1/statuses/mentions_timeline.json";

        private static string oAuthAccessToken = "1603767871-0Pbo7Itc3i1MlIky6mrhu1da6W7FJnvCpdgHNj6";
        private static string oAuthAccessTokenSecret = "KO9Z0Gpj2mmZ5MNUzjX8y6DLoJmaUHW4aTq6tr3J6hE";
        private const string SIGNATURE_METHOD = "HMAC-SHA1";
        private static long since_id_timeline = 0;

        public static void Start()
        {
            //build application preference
            if (SmartCadConfiguration.SmartCadSection.ServerElement.IndicatorsElement.SleepTime != -1)
            {
                runThread = new Thread(new ThreadStart(TwitterAlertsChecker));
                runThread.Start();

                runThread2 = new Thread(new ThreadStart(TwitterMentionsChecker));
                runThread2.Start();
            }
        }

        public static void Stop()
        {
            isRunning = false;
            isMentionsRunning = false;

        }

        private static void TwitterAlertsChecker()
        {
            if (isRunning)
                return;

            isRunning = true;
            ///Used to keep the last incomming tweets group by the query.
            Dictionary<AlarmQueryTwitterData, List<TweetData>> tweetsByAlarmQuery = new Dictionary<AlarmQueryTwitterData, List<TweetData>>();
            /// Used to keep the recently "Alarmed" tweets and avoid alarms duplicated.
            Dictionary<AlarmQueryTwitterData, List<TweetData>> usedTweetsByAlarmQuery = new Dictionary<AlarmQueryTwitterData, List<TweetData>>();
            
            int delay = 10;
            while (isRunning == true)
            {
                if (delay == 10) // the delay was reached
                {
                    Dictionary<AlarmQueryTwitterData, List<TweetData>> tempTweets = tweetsByAlarmQuery;
                    tweetsByAlarmQuery.Clear();

                    IList alarmQueries = SmartCadDatabase.SearchObjects(SmartCadHqls.GetAllAlarmQueryTwitter) as IList;
                    foreach (AlarmQueryTwitterData alarm in alarmQueries)
                    {
                        List<TweetData> oldTweets = tempTweets.ContainsKey(alarm) ? tempTweets[alarm] : new List<TweetData>();
                        tweetsByAlarmQuery.Add(alarm, oldTweets);
                    }
                    tempTweets = null;
                    delay = 0;
                }

                foreach (AlarmQueryTwitterData query in tweetsByAlarmQuery.Keys)
	            {
                    try
                    {
                        //AVOID DUPLICATED TWEETS
                        string sinceID = "";
                        try
                        {
                            if (tweetsByAlarmQuery[query].Count > 0)
                            {
                                sinceID += "&since_id=" + (long.Parse(tweetsByAlarmQuery[query][0].CustomCode) + 1);
                            }
                        }
                        catch { }

                        List<Tweet> results = GetSearchResults(query.TwitterQuery, sinceID);
                        Iesi.Collections.Generic.ISet<TweetData> tweets = new HashedSet<TweetData>();

                        foreach (Tweet tweet in results)
                        {
                            tweets.Add(new TweetData()
                            {
                                CustomCode = tweet.id_str,
                                Published = DateTime.ParseExact(tweet.created_at, "ddd MMM dd HH:mm:ss zzzz yyyy", CultureInfo.InvariantCulture).ToLocalTime(),
                                AuthorName = tweet.user.name,
                                AuthorUserName = tweet.user.screen_name,
                                AuthorUri = tweet.user.url,
                                ImageUrl = tweet.user.profile_image_url, //Regex.Match(tweet.text, @"http://t.co/[^\s]+").Value,
                                Link = tweet.textReferenceUrl,//TODO: ver q carajo es esto
                                Content = tweet.text
                            });
                        }

                        if (tweets.Count > 0)
                        {
                            //Remove old tweets
                            if (tweetsByAlarmQuery[query].Count > 0)
                            {
                                for (int i = tweetsByAlarmQuery[query].Count - 1; i > -1; i--)
                                {
                                    if (DateTime.Now.Subtract(tweetsByAlarmQuery[query][i].Published).TotalSeconds > query.TimeInterval)
                                        tweetsByAlarmQuery[query].RemoveAt(i);
                                    else
                                        break; // if the last one isnt old, the previous neither.
                                }
                            }

                            //Add new tweets
                            bool added = false;
                            foreach (TweetData tweet in tweets.Where(t => !tweetsByAlarmQuery[query].Contains(t)
                                && DateTime.Now.Subtract(t.Published).TotalSeconds <= query.TimeInterval))
                            {
                                tweetsByAlarmQuery[query].Add(tweet);
                                added = true;
                            }

                            if (added)
                            {
                                //Sort by Date
                                tweetsByAlarmQuery[query].Sort();

                                //Check Alarm conditions
                                if (tweetsByAlarmQuery[query].Count >= query.TweetsAmount)
                                {
                                    //Look for created Alarm
                                    AlarmTwitterData alarm = (AlarmTwitterData)SmartCadDatabase.SearchObjectData(SmartCadHqls.GetCustomHql(
                                        SmartCadHqls.GetAlarmTwitterByQueryCode, 0, query.Code));
                                    if (alarm == null)
                                    {
                                        //Generate Alarm
                                        alarm = new AlarmTwitterData()
                                        {
                                            AlarmQuery = query,
                                            StartDate = DateTime.Now
                                        };
                                        alarm.Tweets = new HashedSet();
                                    }
                                    else
                                    {
                                    }

                                    foreach (TweetData t in tweetsByAlarmQuery[query])
                                    {
                                        if(!alarm.Tweets.Contains(t))
                                        {
                                            alarm.Tweets.Add(t);
                                        }
                                    }     

                                    SmartCadDatabase.SaveOrUpdate(alarm);
                                    //tweetsByAlarmQuery[query].Clear();
                                }
                            }
                        }
                    }
                    catch
                    {
                    }
	            }
                Thread.Sleep(3000);
                delay++;
            }
        }
        
        private static void TwitterMentionsChecker()
        {
            if (isMentionsRunning)
                return;

            isMentionsRunning = true;
            while (isMentionsRunning == true)
            {
                try
                {
                    List<Tweet> results = GetMentions("&since_id=" + since_id_timeline);
                    List<TweetData> tweets = new List<TweetData>();

                    foreach (Tweet tweet in results)
                    {
                        TweetData data = new TweetData()
                        {
                            CustomCode = tweet.id_str,
                            Published = DateTime.ParseExact(tweet.created_at, "ddd MMM dd HH:mm:ss zzzz yyyy", CultureInfo.InvariantCulture).ToLocalTime(),
                            AuthorName = tweet.user.name,
                            AuthorUserName = tweet.user.screen_name,
                            AuthorUri = tweet.user.url,
                            ImageUrl = tweet.user.profile_image_url, //Regex.Match(tweet.text, @"http://t.co/[^\s]+").Value,
                            Link = tweet.textReferenceUrl,//TODO: ver q carajo es esto
                            Content = tweet.text
                        };

                        if (long.Parse(tweet.id_str) > since_id_timeline)
                        {
                            since_id_timeline = long.Parse(tweet.id_str);
                        }

                        try
                        {
                            SmartCadDatabase.SaveOrUpdate(data);
                        }
                        catch { }
                    }
                }
                catch
                {
                }
                Thread.Sleep(61000); //More than a minute to avoid rete limited
            }
        }


        public static List<Tweet> GetSearchResults(string searchString, params string[] extras)
        {
            try
            {
                //CODE FOR TWITTER API V 1.1
                // Do the Authenticate
                var authHeaderFormat = "Basic {0}";
                var authHeader = string.Format(authHeaderFormat,
                                               Convert.ToBase64String(
                                                   Encoding.UTF8.GetBytes(Uri.EscapeDataString(oAuthConsumerKey) + ":" +
                                                                          Uri.EscapeDataString((oAuthConsumerSecret)))

                                                   ));
                var postBody = "grant_type=client_credentials";
                HttpWebRequest authRequest = (HttpWebRequest)WebRequest.Create(oAuthUrl);

                authRequest.Headers.Add("Authorization", authHeader);
                authRequest.Method = "POST";
                authRequest.ContentType = "application/x-www-form-urlencoded;charset=UTF-8";
                authRequest.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                using (Stream stream = authRequest.GetRequestStream())
                {
                    byte[] content = ASCIIEncoding.ASCII.GetBytes(postBody);
                    stream.Write(content, 0, content.Length);
                }
                authRequest.Headers.Add("Accept-Encoding", "gzip");
                WebResponse authResponse = authRequest.GetResponse();
                // deserialize into an object
                TwitAuthenticateResponse twitAuthResponse;
                using (authResponse)
                {
                    using (var reader = new StreamReader(authResponse.GetResponseStream()))
                    {
                        JavaScriptSerializer js = new JavaScriptSerializer();
                        var objectText = reader.ReadToEnd();
                        twitAuthResponse = JsonConvert.DeserializeObject<TwitAuthenticateResponse>(objectText);
                    }
                }

                // Do the search
                string queryUTF8 = Uri.EscapeDataString(searchString);
                var searchUrl = "https://api.twitter.com/1.1/search/tweets.json?q=" + queryUTF8 
                    + ((extras.Length > 0)?extras[0]:"") + /*"&geocode=10.496673,-680953,25mi*/ "&result_type=recent&count=20";

                HttpWebRequest timeLineRequest = (HttpWebRequest)WebRequest.Create(searchUrl);
                var timelineHeaderFormat = "{0} {1}";
                timeLineRequest.Headers.Add("Authorization",
                                            string.Format(timelineHeaderFormat, twitAuthResponse.token_type,
                                                          twitAuthResponse.access_token));
                timeLineRequest.Method = "Get";
                WebResponse timeLineResponse = timeLineRequest.GetResponse();

                var timeLineJson = string.Empty;
                using (authResponse)
                {
                    using (var reader = new StreamReader(timeLineResponse.GetResponseStream()))
                    {
                        string results = reader.ReadToEnd();

                        try
                        {
                            string jsonList = JsonConvert.DeserializeObject<JObject>(results).First.First.ToString();
                            return JsonConvert.DeserializeObject<List<Tweet>>(jsonList);
                        }
                        catch (Exception ex) 
                        { SmartLogger.Print(ex); }
                    }
                }
            }
            catch { }

            return null;
        }

        public static void SetStatus(string tweet)
        {
            try
            {
                var oauth_version = "1.0";
                var oauth_signature_method = "HMAC-SHA1";
                var oauth_nonce = Convert.ToBase64String(new ASCIIEncoding().GetBytes(
                                    DateTime.Now.Ticks.ToString()));
                var timeSpan = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0,
                                    DateTimeKind.Utc);
                var oauth_timestamp = Convert.ToInt64(timeSpan.TotalSeconds).ToString();

                var baseFormat = "oauth_consumer_key={0}&oauth_nonce={1}&oauth_signature_method={2}" +
                    "&oauth_timestamp={3}&oauth_token={4}&oauth_version={5}&status={6}";

                var baseString = string.Format(baseFormat,
                                            oAuthConsumerKey,
                                            oauth_nonce,
                                            oauth_signature_method,
                                            oauth_timestamp,
                                            oAuthAccessToken,
                                            oauth_version,
                                            Uri.EscapeDataString(tweet)
                                            );

                baseString = string.Concat("POST&", Uri.EscapeDataString(resource_url),
                             "&", Uri.EscapeDataString(baseString));

                var compositeKey = string.Concat(Uri.EscapeDataString(oAuthConsumerSecret),
                            "&", Uri.EscapeDataString(oAuthAccessTokenSecret));

                string oauth_signature;
                using (HMACSHA1 hasher = new HMACSHA1(ASCIIEncoding.ASCII.GetBytes(compositeKey)))
                {
                    oauth_signature = Convert.ToBase64String(
                        hasher.ComputeHash(ASCIIEncoding.ASCII.GetBytes(baseString)));
                }

                var headerFormat = "OAuth oauth_nonce=\"{0}\", oauth_signature_method=\"{1}\", " +
                       "oauth_timestamp=\"{2}\", oauth_consumer_key=\"{3}\", " +
                       "oauth_token=\"{4}\", oauth_signature=\"{5}\", " +
                       "oauth_version=\"{6}\"";

                var authHeader = string.Format(headerFormat,
                                        Uri.EscapeDataString(oauth_nonce),
                                        Uri.EscapeDataString(oauth_signature_method),
                                        Uri.EscapeDataString(oauth_timestamp),
                                        Uri.EscapeDataString(oAuthConsumerKey),
                                        Uri.EscapeDataString(oAuthAccessToken),
                                        Uri.EscapeDataString(oauth_signature),
                                        Uri.EscapeDataString(oauth_version)
                                );

                var postBody = "status=" + Uri.EscapeDataString(tweet);

                ServicePointManager.Expect100Continue = false;

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(resource_url);
                request.ProtocolVersion = HttpVersion.Version10;
                request.Headers.Add("Authorization", authHeader);
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                using (Stream stream = request.GetRequestStream())
                {
                    byte[] content = ASCIIEncoding.ASCII.GetBytes(postBody);
                    stream.Write(content, 0, content.Length);
                }
                WebResponse response = request.GetResponse();
            }
            catch (WebException ex)
            {
                using (var stream = ex.Response.GetResponseStream())
                using (var reader = new StreamReader(stream))
                {
                    Console.WriteLine(reader.ReadToEnd());
                }
            }
        }

        public static List<Tweet> GetMentions(string tweet)
        {
            try
            {
                var oauth_version = "1.0";
                var oauth_signature_method = "HMAC-SHA1";
                var oauth_nonce = Convert.ToBase64String(new ASCIIEncoding().GetBytes(
                                    DateTime.Now.Ticks.ToString()));
                var timeSpan = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0,
                                    DateTimeKind.Utc);
                var oauth_timestamp = Convert.ToInt64(timeSpan.TotalSeconds).ToString();

                var baseFormat = "oauth_consumer_key={0}&oauth_nonce={1}&oauth_signature_method={2}" +
                    "&oauth_timestamp={3}&oauth_token={4}&oauth_version={5}";

                var baseString = string.Format(baseFormat,
                                            oAuthConsumerKey,
                                            oauth_nonce,
                                            oauth_signature_method,
                                            oauth_timestamp,
                                            oAuthAccessToken,
                                            oauth_version
                                            );

                baseString = string.Concat("GET&", Uri.EscapeDataString(mentions_url),
                             "&", Uri.EscapeDataString(baseString));

                var compositeKey = string.Concat(Uri.EscapeDataString(oAuthConsumerSecret),
                            "&", Uri.EscapeDataString(oAuthAccessTokenSecret));

                string oauth_signature;
                using (HMACSHA1 hasher = new HMACSHA1(ASCIIEncoding.ASCII.GetBytes(compositeKey)))
                {
                    oauth_signature = Convert.ToBase64String(
                        hasher.ComputeHash(ASCIIEncoding.ASCII.GetBytes(baseString)));
                }

                var headerFormat = "OAuth oauth_nonce=\"{0}\", oauth_signature_method=\"{1}\", " +
                       "oauth_timestamp=\"{2}\", oauth_consumer_key=\"{3}\", " +
                       "oauth_token=\"{4}\", oauth_signature=\"{5}\", " +
                       "oauth_version=\"{6}\"";

                var authHeader = string.Format(headerFormat,
                                        Uri.EscapeDataString(oauth_nonce),
                                        Uri.EscapeDataString(oauth_signature_method),
                                        Uri.EscapeDataString(oauth_timestamp),
                                        Uri.EscapeDataString(oAuthConsumerKey),
                                        Uri.EscapeDataString(oAuthAccessToken),
                                        Uri.EscapeDataString(oauth_signature),
                                        Uri.EscapeDataString(oauth_version)
                                );

                //var postBody = "status=" + Uri.EscapeDataString(tweet);

                ServicePointManager.Expect100Continue = false;

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(mentions_url);
                request.ProtocolVersion = HttpVersion.Version10;
                request.Headers.Add("Authorization", authHeader);
                request.Method = "GET";
                //request.ContentType = "application/x-www-form-urlencoded"; 
                
                WebResponse response = request.GetResponse();

                var timeLineJson = string.Empty;
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string results = reader.ReadToEnd();

                    try
                    {
                        //string jsonList = JsonConvert.DeserializeObject<JArray>(results).First.First.ToString();
                        return JsonConvert.DeserializeObject<List<Tweet>>(results);
                    }
                    catch { }
                }
            }
            catch (WebException ex)
            {
                using (var stream = ex.Response.GetResponseStream())
                using (var reader = new StreamReader(stream))
                {
                    Console.WriteLine(reader.ReadToEnd());
                }
            }

            return null;
        }
    }

    public class TwitAuthenticateResponse
    {
        public string token_type { get; set; }
        public string access_token { get; set; }
    }

    public class Tweet
    {
        public string created_at { get; set; }
        public string id_str { get; set; }
        public string text { get; set; }
        public string source { get; set; }
        public TwitterGeoInfo geo { get; set; }
        public TwitterGeoInfo coordinates { get; set; }
        public TwitterUser user { get; set; }
        private TwitterEntities entities { get; set; }
        public string textReferenceUrl
        {
            get
            {
                try
                {
                    return "";// return entities.urls.expanded_url;
                }
                catch
                {
                    return "";
                }
            }
        }
    }

    public class TwitterUser
    {
        public string name { get; set; }
        public string screen_name { get; set; }
        public string location { get; set; }
        public string url { get; set; }
        public string profile_image_url { get; set; }
    }

    public class TwitterEntities
    {
        public List<TwitterMedia> media { get; set; }
        //public TwitterUrls urls { get; set; }
        //public string user_mentions { get; set; }
        //public string hashtags { get; set; }
    }

    public class TwitterUrls
    {
        public string expanded_url { get; set; }
    }

    public class TwitterMedia
    {
        public string media_url { get; set; }
        public string media_url_https { get; set; }
        public string url { get; set; }
        public string display_url { get; set; }
        public string expanded_url { get; set; }
    }

    public class TwitterGeoInfo
    {
        public long[] coordinates { get; set; }
    }
}

