using System;
using System.Runtime.InteropServices;
using System.Security;
using System.Collections;
using System.Net;
using System.Collections.Generic;
using System.Management;

namespace SmartCadCore.Core
{
    public sealed class NetworkBrowser
    {
        #region Dll Imports

        [DllImport("Netapi32", CharSet = CharSet.Auto,
        SetLastError = true),
        SuppressUnmanagedCodeSecurityAttribute]
        private static extern int NetServerEnum(
            string ServerNane, // must be null
            int dwLevel,
            ref IntPtr pBuf,
            int dwPrefMaxLen,
            out int dwEntriesRead,
            out int dwTotalEntries,
            int dwServerType,
            string domain, // null for login domain

            out int dwResumeHandle
            );

        [DllImport("Netapi32", SetLastError = true),
        SuppressUnmanagedCodeSecurityAttribute]
        private static extern int NetApiBufferFree(
            IntPtr pBuf);

        [StructLayout(LayoutKind.Sequential)]
        private struct _SERVER_INFO_100
        {
            internal int sv100_platform_id;
            [MarshalAs(UnmanagedType.LPWStr)]
            internal string sv100_name;
        }
        #endregion

        #region Public Methods

        public static List<string> GetNetworkComputers()
        {
            List<string> networkComputers = new List<string>();
            const int MAX_PREFERRED_LENGTH = -1;
            int SV_TYPE_WORKSTATION = 1;
            int SV_TYPE_SERVER = 2;
            IntPtr buffer = IntPtr.Zero;
            IntPtr tmpBuffer = IntPtr.Zero;
            int entriesRead = 0;
            int totalEntries = 0;
            int resHandle = 0;
            int sizeofINFO = Marshal.SizeOf(typeof(_SERVER_INFO_100));


            try
            {
                int ret = NetServerEnum(null, 100, ref buffer,
                    MAX_PREFERRED_LENGTH,
                    out entriesRead,
                    out totalEntries, SV_TYPE_WORKSTATION |
                    SV_TYPE_SERVER, null, out 
                    resHandle);

                if (ret == 0)
                {
                    for (int i = 0; i < totalEntries; i++)
                    {
                        tmpBuffer = new IntPtr((int)buffer +
                                   (i * sizeofINFO));

                        _SERVER_INFO_100 svrInfo = (_SERVER_INFO_100)
                            Marshal.PtrToStructure(tmpBuffer,
                                    typeof(_SERVER_INFO_100));

                        networkComputers.Add(svrInfo.sv100_name);
                    }
                }
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                NetApiBufferFree(buffer);
            }
            return networkComputers;

        }
        #endregion
    }

    public class ComputerInfo
    {
        public string Name;
        public List<string> IPs;

        public ComputerInfo(string name, List<string> ips)
        {
            Name = name;
            IPs = ips;
        }

        public string IP()
        {
            if (IPs.Count > 0)
                return IPs[0];
            else
                return string.Empty;
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is ComputerInfo)
            {
                result = this.Name == ((ComputerInfo)obj).Name;
            }
            return result;
        }

        public override string ToString()
        {
            return Name;
        }
    }

    public class ComputerInfoList
    {
        static List<ComputerInfo> computerInfos = new List<ComputerInfo>();

        private static List<ComputerInfo> Load()
        {
            List<string> networkMachineNames = NetworkBrowser.GetNetworkComputers();
            foreach (string computerName in networkMachineNames)
            {
                List<string> ips = new List<string>();
                ComputerInfo ci = new ComputerInfo(computerName, ips);
                computerInfos.Add(ci);
            }
            return computerInfos;
        }

        public static List<ComputerInfo> Filter(string NameOrIp)
        {
            List<ComputerInfo> cis = new List<ComputerInfo>();
            foreach (ComputerInfo ci in computerInfos)
            {
                if (ci.Name.Contains(NameOrIp) == true || ci.IPs.Contains(NameOrIp) == true)
                    cis.Add(ci);
            }
            return cis;
        }

        public static List<ComputerInfo> List
        {
            get
            {
                if (computerInfos.Count == 0)
                    Load();
                return computerInfos;
            }
        }
    }
}
