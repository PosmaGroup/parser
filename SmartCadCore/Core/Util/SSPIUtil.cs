using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Security.Principal;
using System.Net.Security;

namespace SmartCadCore.Core
{
    public class SSPIUtil
    {
        public static bool LogonUser(string userName, string password, string domain)
        {
            TcpListener tcpListener = null;

            try
            {
                tcpListener = new TcpListener(IPAddress.Loopback, 0);

                tcpListener.Start();

                tcpListener.BeginAcceptTcpClient(delegate(IAsyncResult asyncResult)
                {
                    using (NegotiateStream serverSide = new NegotiateStream(tcpListener.EndAcceptTcpClient(asyncResult).GetStream()))
                    {
                        try
                        {
                            serverSide.AuthenticateAsServer(
                                CredentialCache.DefaultNetworkCredentials,
                                ProtectionLevel.None,
                                TokenImpersonationLevel.Impersonation);
                        }
                        catch
                        {
                        }
                    }
                }, null);

                using (NegotiateStream clientSide = new NegotiateStream(new TcpClient(IPAddress.Loopback.ToString(),
                    ((IPEndPoint)tcpListener.LocalEndpoint).Port).GetStream()))
                {
                    clientSide.AuthenticateAsClient(
                        new NetworkCredential(userName, password, domain),
                        "",
                        ProtectionLevel.None, TokenImpersonationLevel.Impersonation);
                }

                return true;
            }
            catch
            {
                return false;
            }
            finally
            {
                tcpListener.Stop();
            }
        }
    }
}
