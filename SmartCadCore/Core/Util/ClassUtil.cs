using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace SmartCadCore.Core
{
    public static class ClassUtil
    {
        public static T NewInstance<T>() where T : class, new()
        {
            try
            {
                Type type = typeof(T); 
                ConstructorInfo constructorInfo = type.GetConstructor(new Type[] { });

                return constructorInfo.Invoke(new object[] { }) as T;
            }
            catch
            {
                return null;
            }
        }

        public static object NewInstance(Type type)
        {
            try
            {
                ConstructorInfo constructorInfo = type.GetConstructor(new Type[] { });

                return constructorInfo.Invoke(new object[] { });
            }
            catch
            {
                return null;
            }
        }

        public static string GetPropertyName(object obj, object property)
        {
            if (obj == null)
                throw new ArgumentNullException("obj");

            if (property == null)
                throw new ArgumentNullException("property");

            bool propertyFounded = false;
            string propertyName = string.Empty;

            PropertyInfo[] properties = obj.GetType().GetProperties();

            int index = 0;

            while (index < properties.Length && !propertyFounded)
            {
                if (properties[index].PropertyType.IsInstanceOfType(property))
                {
                    MethodInfo method = properties[index].GetGetMethod();
                    
                    if (method.Invoke(obj, null) == property)
                    {
                        propertyName = properties[index].Name;
                        propertyFounded = true;
                    }
                }

                index++;
            }

            return propertyName;
        }

        public static void SetValue(object obj, string propertyName, object val)
        {
            PropertyInfo propertyInfo = obj.GetType().GetProperty(propertyName);
            propertyInfo.SetValue(obj, val, null);
        }
    }
}