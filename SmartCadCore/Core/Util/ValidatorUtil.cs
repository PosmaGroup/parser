using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace SmartCadCore.Core
{
    public class ValidatorUtil
    {
        public static bool ValidatePhone(string phone)
        {

            int hyphencount = 0;
            try
            {
                if (phone.Length >= 25)
                {
                    return false;
                }
                for (int i = 0; i < phone.Length; i++)
                {
                    if (phone[i].ToString() == "+")
                        continue;
                    if (phone[i].ToString() == " ")
                        continue;
                    if (phone[i].ToString() == "-")
                    {
                        hyphencount++;
                        continue;
                    }
                    if (phone[i].ToString() == "/")
                        continue;
                    if (phone[i].ToString() == ".")
                        continue;
                    if (phone[i].ToString() == "(")
                        continue;
                    if (phone[i].ToString() == ")")
                        continue;
                    if (char.IsLetter(phone[i]) || char.IsPunctuation(phone[i]))
                    {
                        return false;
                    }
                }
                if (hyphencount > 4)
                {
                    return false;
                }
            }
            catch
            {
                throw new Exception("...");
            }
            return true;
        }

        public static bool ValidateString(string name)
        {
            bool res = true;
            if (name != null)
            {
                for (int i = 0; i <= name.Length - 1; i++)
                {
                    if (name[i].ToString() == "'")
                    {
                        res = false;
                    }
                }
            }
            return res;
        }

        public static bool ValidateEmail(string email)
        {
            return ValidateEmail(email, true);
        }

        public static bool ValidateEmail(string email, bool strict)
        {
            string patternLenient = @"^([\w-]+\.)*?[\w-]+@[\w-]+\.([\w-]+\.)*?[\w]+$";
            string patternStrict = @"^[a-z][a-z0-9]*([_.-]?[a-z0-9]+)*" +
               @"@[a-z0-9]{3}[a-z0-9]*\.([a-z0-9]+([.-][a-z0-9]+)*)$";

           // Regex regex = null;

            Match match = Regex.Match(email, patternStrict, RegexOptions.IgnoreCase);

            //if (strict)
            //    regex = new Regex(patternStrict,RegexOptions.IgnoreCase);
            //else
            //    regex = new Regex(patternLenient);

            return match.Success;
        }
    }
}
