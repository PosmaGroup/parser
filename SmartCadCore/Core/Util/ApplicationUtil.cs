using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Windows.Forms;
using System.ServiceModel;
using System.ServiceProcess;
using System.Security.Cryptography.X509Certificates;
using System.IO;

using Timer = System.Threading.Timer;
using System.Threading;
using System.Globalization;
using System.Xml.Xsl;
using System.Xml;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Drawing;
using System.Drawing.Drawing2D;
using SmartCadCore.ClientData;
using System.Collections;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using SmartCadCore.Common;
using Microsoft.Win32;
using System.Net.NetworkInformation;
using System.Net;

namespace SmartCadCore.Core
{
    public delegate void ChangeValueDelegate(int Value);
    public delegate void ChangeTaskDelegate(string Task);

    #region Class ApplicationUtil Documentation
    /// <summary>
    /// TODO:
    /// </summary>
    /// <className>ApplicationUtil</className>
    /// <author email="atorres@smartmatic.com">Alden Torres</author>
    /// <date>2006/12/04</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    public static class ApplicationUtil
    {
        public static string DataBaseFormattedDate = "yyyyMMdd HH:mm:ss.f";
        public static string DateNHibernateFormat = "yyyyMMdd HH:mm";
        public static string DateNHibernateFormatWithSeconds = "yyyyMMdd HH:mm:ss";
        public static string SupervisionApplicationName = "";
        public static string RegistryKey = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall";

        private static CultureInfo currentCulture;
        //public static readonly DateTime SCHEDULE_REFERENCE_DATE = new DateTime(2000, 01, 01);

        //public static string GetDataBaseFormattedDateWithScheduleReferenceDate(DateTime datetime)
        //{
        //    return GetDataBaseFormattedDate(new DateTime(SCHEDULE_REFERENCE_DATE.Year, SCHEDULE_REFERENCE_DATE.Month, SCHEDULE_REFERENCE_DATE.Day, datetime.Hour, datetime.Minute, datetime.Second));
        //}


        public static bool VerifyLanguage(ConfigurationClientData cfg)
        {
            string filePath = SmartCadConfiguration.LanguageDirectoryClient;
            string[] fileEntries = Directory.GetDirectories(filePath);
            List<string> listDirectories = new List<string>();
            foreach (var item in fileEntries)
            {
                var finalFolder = item.Split('\\');
               if ( ((string)finalFolder.GetValue(finalFolder.Length - 1)).Equals(cfg.Language))
                {
                    return true;
                }
            }
            return false;
        }
        public static List<string> GetLanguages()
        {
            string filePath = SmartCadConfiguration.LanguageDirectory;//@"C:\Users\Posma-developer\Documents\Posma\server\SmartCadServer\Smartmatic.SmartCad.Resources\bin\Debug";
            string[] fileEntries = Directory.GetDirectories(filePath);
            List<string> listDirectories = new List<string>();
            List<string> listLanguagesNames = new List<string>();
            foreach (var item in fileEntries)
            {
                var finalFolder = item.Split('\\');
                listDirectories.Add((string)finalFolder.GetValue(finalFolder.Length - 1));

                CultureInfo ci = CultureInfo.InstalledUICulture;
                CultureInfo[] cinfo = CultureInfo.GetCultures(CultureTypes.AllCultures & ~CultureTypes.NeutralCultures);

                foreach (CultureInfo cul in cinfo)
                {
                    if (cul.Name.Equals((string)finalFolder.GetValue(finalFolder.Length - 1)))
                    {
                        listLanguagesNames.Add(cul.DisplayName);
                        Console.WriteLine(cul.DisplayName);
                    }
                }
            }

            return listLanguagesNames;
        }

        public static string GetKeyLanguagesByName(string nameLanguage)
        {
                string keyLanguage="";
                CultureInfo[] cinfo = CultureInfo.GetCultures(CultureTypes.AllCultures & ~CultureTypes.NeutralCultures);

                foreach (CultureInfo cul in cinfo)
                {
                    if (cul.DisplayName.Equals(nameLanguage))
                    {
                        keyLanguage= cul.Name;
                    }
                }           
            return keyLanguage;
        }

        public static string GetNameLanguageByKey(string keyLanguage)
        {
            string nameLanguage = "";
            CultureInfo[] cinfo = CultureInfo.GetCultures(CultureTypes.AllCultures & ~CultureTypes.NeutralCultures);

            foreach (CultureInfo cul in cinfo)
            {
                if (cul.Name.Equals(keyLanguage))
                {
                    nameLanguage = cul.DisplayName;
                }
            }
            return nameLanguage;
        }


        public static bool CompareDateTime(DateTime dt1, DateTime dt2)
        {
            DateTime dt1Aux = new DateTime(dt1.Year, dt1.Month, dt1.Day, dt1.Hour, dt1.Minute, dt1.Second);
            DateTime dt2Aux = new DateTime(dt2.Year, dt2.Month, dt2.Day, dt2.Hour, dt2.Minute, dt2.Second);
            return dt1Aux == dt2Aux;
        }

        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("Local IP Address Not Found!");
        }

        public static string GetMACAddress()
        {
            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
            String sMacAddress = string.Empty;
            foreach (NetworkInterface adapter in nics)
            {
                if (sMacAddress == String.Empty)// only return MAC Address from first card  
                {
                    IPInterfaceProperties properties = adapter.GetIPProperties();
                    sMacAddress = adapter.GetPhysicalAddress().ToString();
                }
            } return sMacAddress;
        }


        //public static DayOfWeek ConvertIndexToDayOfWeek(int index)
        //{
        //    DayOfWeek day = DayOfWeek.Monday;
        //    if (index == 7)
        //    {
        //        index = 0;
        //    }
        //    day = (DayOfWeek)index;
        //    return day;
        //}

        //public static int GetIndexFromDayOfWeek(DayOfWeek day)
        //{
        //    int index = 0;
        //    if (day == DayOfWeek.Sunday)
        //    {
        //        index = 7;
        //    }
        //    else
        //    {
        //        index = (int)day;
        //    }
        //    return index;
        //}


        public static bool IsInstalled(string program)
        {

            string displayName;
            RegistryKey key;

            // search in: CurrentUser
            //key = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall");
            //SmartLogger.Print(key.ToString());
            //foreach (String keyName in key.GetSubKeyNames())
            //{
            //    RegistryKey subkey = key.OpenSubKey(keyName);
            //    if (subkey.GetValue("DisplayName") != null)
            //    {
            //        displayName = subkey.GetValue("DisplayName") as string;
            //        if (program.Equals(displayName, StringComparison.OrdinalIgnoreCase) == true)
            //        {
            //            return true;
            //        }
            //    }
            //}

            //// search in: LocalMachine_32
            //key = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall");
            //SmartLogger.Print(key.ToString());
            //foreach (String keyName in key.GetSubKeyNames())
            //{
            //    RegistryKey subkey = key.OpenSubKey(keyName);
            //    if (subkey.GetValue("DisplayName") != null)
            //    {
            //        displayName = subkey.GetValue("DisplayName") as string;
            //        if (program.Equals(displayName, StringComparison.OrdinalIgnoreCase) == true)
            //        {
            //            return true;
            //        }
            //    }
            //}

            // search in: LocalMachine_64
            SmartLogger.Print("Begining Check");
            key = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall");

            foreach (String keyName in key.GetSubKeyNames())
            {
                RegistryKey subkey = key.OpenSubKey(keyName);
                if (subkey.GetValue("DisplayName") != null)
                {
                    displayName = subkey.GetValue("DisplayName") as string;
                    if (program.Equals(displayName, StringComparison.OrdinalIgnoreCase) == true)
                    {
                        SmartLogger.Print("Installed");
                        return true;
                    }
                }

            }
            SmartLogger.Print("Not Installed");
            // NOT FOUND
            return false;

            //using (Microsoft.Win32.RegistryKey key = Registry.LocalMachine.OpenSubKey(RegistryKey))
            //{
            //    foreach (string subkeyName in key.GetSubKeyNames())
            //    {
            //        using (RegistryKey subkey = key.OpenSubKey(subkeyName))
            //        {

            //            if (subkey.GetValue("DisplayName") != null)
            //            {
            //                SmartLogger.Print(subkey.GetValue("DisplayName").ToString());
            //                var type = subkey.GetValue("DisplayName").GetType();
            //                if (subkey.GetValue("DisplayName").ToString() == program)
            //                {
            //                    return true;
            //                }
            //            }

            //        }
            //    }
            //    return false;
            //}
        }

        public static string GetDayName(DayOfWeek day)
        {
            return ResourceLoader.GetString(day.ToString());
        }

        public static void ActivateProcess(Process process)
        {
            Process[] processes = Process.GetProcessesByName(process.ProcessName);

            if (processes.Length > 1)
            {
                foreach (Process p in processes)
                {
                    if (p.Handle != process.Handle)
                    {
                        Win32.ShowWindow(p.MainWindowHandle, Win32.SW_RESTORE);
                        Win32.SetForegroundWindow(p.MainWindowHandle);
                    }
                }

                process.Kill();
            }
        }

        public static void LaunchHelp(string helpFile)
        {
               
            ProcessStartInfo processStartInfo = new ProcessStartInfo(Application.StartupPath + "\\" + helpFile);
            processStartInfo.WindowStyle = ProcessWindowStyle.Maximized;
            Process p = Process.Start(processStartInfo);
            Process[] processes = Process.GetProcessesByName(p.ProcessName);

            if (processes.Length > 1) {
                p.Kill();
            } 
                    
        }

        public static void LaunchApplicationWithArguments(string path, object[] args)
        {
            try
            {
                MemoryStream stream = new MemoryStream();
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, args);
                string str = Convert.ToBase64String(stream.ToArray());

                ProcessStartInfo startInfo = new ProcessStartInfo(path);
                startInfo.WorkingDirectory = Environment.CurrentDirectory;
                startInfo.UseShellExecute = true;
                startInfo.Arguments = str;

                string fileName = Path.GetFileName(path);
                if (Process.GetProcessesByName(fileName).Length == 0)
                    Process.Start(startInfo);
                Thread.Sleep(1000);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static object[] RetrieveArguments(string str)
        {
            try
            {
                BinaryFormatter formatter = new BinaryFormatter();

                byte[] buff = Convert.FromBase64String(str);
                MemoryStream stream = new MemoryStream(buff);

                return formatter.Deserialize(stream) as object[];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string BuildServiceUrl(string protocol, string ipAddress, int port, string serviceAddress)
        {
            return protocol + "://" + ipAddress + ":" + port + "/" + serviceAddress;
        }

        public static void InstallCertificate(X509Store store, string fileName)
        {
            try
            {
                X509Certificate2 certificate = new X509Certificate2(fileName);

                store.Open(OpenFlags.ReadWrite);

                X509Certificate2Collection collection = store.Certificates;

                X509Certificate2Collection result = collection.Find(X509FindType.FindBySubjectName, certificate.SubjectName, false);

                collection.RemoveRange(result);

                store.Add(certificate);
            }
            finally
            {
                store.Close();
            }
        }

        public static void InstallCertificateOnLocalMachine(string fileName)
        {
            X509Store store = new X509Store(StoreName.Root, StoreLocation.LocalMachine);

            InstallCertificate(store, fileName);
        }

        public static string DistFolder
        {
            get
            {
                return Directory.GetParent(Application.StartupPath).FullName;
            }
        }

        public static Timer RecurrentExecuteMethod(Delegate method, Delegate errorMethod, object state, int dueTime, int period)
        {
            return new Timer(new TimerCallback(delegate
            {
                try
                {
                    if (method != null)
                        method.DynamicInvoke();
                }
                catch(Exception ex)
                {
                    if (ex.InnerException != null)
                        SmartLogger.Print(ex.InnerException);
                    else
                        SmartLogger.Print(ex);

                    try
                    {
                        if (errorMethod != null)
                            errorMethod.DynamicInvoke();
                    }
                    catch { }
                }
            }), state, dueTime, period);
        }

        public static string GetDomainName(string hostName)
        {
            throw new NotImplementedException(ResourceLoader.GetString2("NotImplementedMethod"));
        }

        public static string GetFormattedDate(DateTime time)
        {
            string dateFull = time.ToString("D", CultureInfo.CurrentCulture);
            string timeRegular = time.ToString("HH:mm", CultureInfo.CurrentCulture);
            string timeRet = dateFull + " " + timeRegular;
            //timeRet = timeRet.Remove(timeRet.Length - 3);
            string firstChar = timeRet[0].ToString().ToUpper();

            return firstChar + "" + timeRet.Remove(0, 1);
        }

        public static string GetDataBaseFormattedDate(DateTime time)
        {
            return time.ToString(DataBaseFormattedDate);
        }

        public static string GetDataBaseFormattedDate_1(DateTime time)
        {
            string str = time.ToString(DataBaseFormattedDate);
            str = str.Substring(0, 8);
            return str;
        }


        public static string GetNHibernateFormattedDate(DateTime time)
        {
            return time.ToString(DateNHibernateFormat);
        }

        /// <summary>
        /// Returns a formatted datetime to be displayed to the user.
        /// This format is: "title" HH:MM:SS. Ej Connected: 05:35:59
        /// </summary>
        /// <param name="title"></param>
        /// <param name="currentTime"></param>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static string GetFormattedElapsedTime(string title, DateTime currentTime, DateTime dateTime)
        {
            TimeSpan elapsedTime = currentTime - dateTime;
            string[] args = new string[3];
            args[0] = Math.Truncate(elapsedTime.TotalHours).ToString().PadLeft(2, '0');
            args[1] = elapsedTime.Minutes.ToString().PadLeft(2, '0');
            args[2] = elapsedTime.Seconds.ToString().PadLeft(2, '0');
            
            StringWriter writer = new StringWriter();
            writer.Write(title, args);
            return writer.ToString();
        }

        public static bool IsRTF(string text)
        {
            return text.StartsWith("{\\rtf1");
        }

        /// <summary>
        /// M�todo que hace un resize de la imagen pasada como parametro.
        /// </summary>
        /// <param name="mg">Imagen a la cual queremos aplicar el resize</param>
        /// <param name="newSize">Nuevo tama�o de la imagen</param>
        /// <param name="color">Color del panel que contiene a la imagen.</param>
        /// <returns></returns>
        public static Image ResizeImage(Image mg, Size newSize, Color color)
        {
            double ratio = 0d;
            double myThumbWidth = 0d;
            double myThumbHeight = 0d;
            int x = 0;
            int y = 0;

            Bitmap bp;

            if ((mg.Width / Convert.ToDouble(newSize.Width)) > (mg.Height /
            Convert.ToDouble(newSize.Height)))
                ratio = Convert.ToDouble(mg.Width) / Convert.ToDouble(newSize.Width);
            else
                ratio = Convert.ToDouble(mg.Height) / Convert.ToDouble(newSize.Height);
            myThumbHeight = Math.Ceiling(mg.Height / ratio);
            myThumbWidth = Math.Ceiling(mg.Width / ratio);


            Size thumbSize = new Size((int)myThumbWidth, (int)myThumbHeight);
            bp = new Bitmap(newSize.Width, newSize.Height);
            x = (newSize.Width - thumbSize.Width) / 2;
            y = (newSize.Height - thumbSize.Height);
     
            Graphics g = Graphics.FromImage(bp);
            g.SmoothingMode = SmoothingMode.HighQuality;
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
            g.PixelOffsetMode = PixelOffsetMode.HighQuality;
            Rectangle rect = new Rectangle(x, y, thumbSize.Width, thumbSize.Height);
            g.Clear(color);
            g.DrawImage(mg, rect, 0, 0, mg.Width, mg.Height, GraphicsUnit.Pixel);
            return bp;
        }

        /// <summary>
        /// Metodo que parsea un string con la restriccion 
        /// de que el numero parseado este entre begin y end 
        /// inclusive ambos parametros.
        /// </summary>
        /// <param name="s">String a ser parseado.</param>
        /// <param name="begin">comienzo</param>
        /// <param name="end">fin</param>
        /// <returns>True si pudo parsearlo con los contraints, False en caso contrario.</returns>
        public static bool ParseConstraints(string s,int begin,int end) 
        { 
            int res = 0;
            if (int.TryParse(s, out res))
            {
                if (begin <= res && res <= end)
                {
                    return true;
                }
                else
                    return false;
            }
            return false;
        }

        public static bool ParseConstraints(string s, double begin, double end)
        {
            double res = 0;
            if (double.TryParse(s, out res))
            {
                if (begin <= res && res <= end)
                {
                    return true;
                }
                else
                    return false;
            }
            return false;
        }


        public static TimeSpan TimeSpanFromDouble(double val)
        {
            int integral = (int)Math.Truncate(val);
            double fractional = val - integral;

            TimeSpan result = TimeSpan.FromMinutes(1);
            try
            {
                result = new TimeSpan(integral, (int)Math.Round(59 * fractional, 0, MidpointRounding.AwayFromZero), 0);
            }
            catch
            {
                result = TimeSpan.FromMinutes(1);
            }

            return result;
        }
        
        public static FaultException NewFaultException(Exception ex)
        {
            return new FaultException(ResourceLoader.GetString2(ex.Message), new FaultCode(ex.GetType().Name));            
        }

        public static string GetStringWithoutAccent(string originalWord)
        {
            string result = null;
            if (originalWord != null)
            {
                StringBuilder sb = new StringBuilder(originalWord);
                string[] accentedVowels = { "�", "�", "�", "�", "�", "�", "�", "�", "�", "�" };
                string[] unAccentedVowels = { "a", "e", "i", "o", "u", "A", "E", "I", "O", "U" };
                int count = 0;
                while (count < accentedVowels.Length)
                {
                    sb.Replace(accentedVowels[count], unAccentedVowels[count]);
                    count++;
                }
                result = sb.ToString();
            }
            return result;
        }

        
        public static bool TestForService(string address, int port, int seconds)
        {
            bool serviceUp = false;

            TcpClient tcpClient = new TcpClient();

            int n = 0;

            while (n < seconds && !serviceUp)
            {
                try
                {
                    tcpClient.Connect(address, port);
                    tcpClient.Close();
                    serviceUp = true;
                }
                catch
                {
                    Thread.Sleep(TimeSpan.FromSeconds(1));
                }

                n++;
            }

            return serviceUp;
        }

        public static bool IsAgeValid(DateTime birth, int age) {
 
            int days = 365 * 15;
            for (int i = DateTime.Now.Year; i > DateTime.Now.Year - 15; i--)
            {
                if (DateTime.IsLeapYear(i))
                    days += 1;
            }

            return birth > DateTime.Now.Subtract(new TimeSpan(days, 0, 0, 0, 0));
        
        }

        public static string GetFirstLine(string str)
        {
            if (str != null)
            {
                int i = str.IndexOf("\r");

                if (i > 0)
                    str = str.Substring(0, i).Trim() + "...";

            }
            return str;
        }


        public static bool CheckPrimaryScreenBounds(int left)
        {
            return Screen.PrimaryScreen.Bounds.Left < left + 10 && left + 10 > Screen.PrimaryScreen.Bounds.Width;
        }

        public static bool IsServiceUp(string name)
        {
            ServiceController[] allServices;
            allServices = ServiceController.GetServices();
            foreach (ServiceController service in allServices)
            {
                if (service.ServiceName.Contains(name))
                {
                    if (service.Status.Equals(ServiceControllerStatus.Running))
                    {
                        return true;
                    }
                    
                }
            }
            return false;
        }

        public static CultureInfo GetCurrentCulture()
        {
            if (currentCulture == null)
            {
                currentCulture = CultureInfo.CurrentCulture;  //.CreateSpecificCulture("es-VE");
            }
            return currentCulture;
        }

        public static bool CheckFileOpenByAnother(string file) 
        {
            bool opened = false;
            try
            {
                FileStream fs = new FileStream(file, FileMode.Open, FileAccess.Write, FileShare.None);
                fs.Close();
                opened = false;
            }
            catch (FileNotFoundException ex)
            {
                opened = false;
            }
            catch (UnauthorizedAccessException e)
            {
                opened = true;
            }
            catch (IOException ew) 
            {
                opened = true;
            }

            return opened;
        }

        public static bool IsRecommendedUnitsByIncidentTypes(IList units, IList incidentTypeNames)
        {
            bool result = false;
            for (int i = 0; i < units.Count && result == false; i++)
            {
                UnitClientData unit = (UnitClientData)units[i];
                result = IsRecommendedUnitByIncidentTypes(unit, incidentTypeNames);
            }
            return result;
        }

        public static bool IsRecommendedUnitByIncidentTypes(UnitClientData unit, IList incidentTypes)
        {
            bool result = false;

            for (int i = 0; i < unit.IncidentTypes.Count && result == false; i++)
            {
                result = incidentTypes.Contains(((IncidentTypeClientData)unit.IncidentTypes[i]).Name);
            }
            return result;
        }

        private const char INDICATOR_RESULT_SEPARATOR = (char)124; // "|" will be used to separate data in the string

        public static bool IndicatorMultipleResult(IndicatorResultValuesClientData result)
        {
            return result.ResultValue.IndexOf(INDICATOR_RESULT_SEPARATOR) > -1;
        }

        public static string ConvertIndicatorResultValuesToString(List<double> values)
        {
            StringBuilder result = new StringBuilder();
            if (values != null && values.Count > 0)
            {
                int counter = values.Count;
                for (int i = 0; i < counter; i++)
                {
                    result.Append(values[i]);
                    if (i + 1 < counter)
                    {
                        result.Append(INDICATOR_RESULT_SEPARATOR);
                    }
                }
            }
            return result.ToString();
        }

        public static List<double> ConvertIndicatorResultValuesFromString(string result)
        {
            string[] splittedResult = GetSplittedResult(result);
            List<double> resultList = new List<double>();
            foreach (string res in splittedResult)
            {
                double num = 0;
                bool resultDouble = double.TryParse(res, out num);
                if (resultDouble == false)
                    num = 0;
                resultList.Add(num);
            }
            return resultList;
        }

        public static string[] GetSplittedResult(string resultValue)
        {
            return resultValue.Split(new char[] { INDICATOR_RESULT_SEPARATOR });
        }

        public static string GetTimeSpanCustomString(TimeSpan ts)
        {
            return string.Format("{0:00}:", Math.Floor(ts.TotalHours)) +
                string.Format("{0:00}:", ts.Minutes) +
                string.Format("{0:00}", ts.Seconds);
        }

        public static byte[] GetScreenShot(IntPtr handle)
        {
            //return GetByteArrayFromImage(CaptureScreenEngine.CaptureScreenWithCursor(handle));
            //return GetByteArrayFromImage(CaptureScreenEngine.CaptureDesktopWithCursor());
            return GetByteArrayFromImage(SCapture.FullScreen(true));
        }

        public static byte[] GetByteArrayFromImage(Image image)
        {
            byte[] result = null;

            MemoryStream ms = new MemoryStream();
            image.Save(ms, ImageFormat.Png);
            result = ms.GetBuffer();
            ms.Dispose();

            return result;
        }

        public static Image GetImageFromByteArray(byte[] bytes)
        {
            MemoryStream ms = new MemoryStream(bytes);
            Image result = Image.FromStream(ms);
            ms.Dispose();
            return result;
        }

		public static SessionStatusHistoryClientData GetOperatorStatus(OperatorClientData OperatorClient, string SupervisedApplicationName)
		{
			SessionStatusHistoryClientData status = new SessionStatusHistoryClientData();
			status.StatusFriendlyName = ResourceLoader.GetString("MSG_NotConnected");
            status.StatusName = "NotConnected";
			if (OperatorClient.LastSessions != null)
			{
				SessionHistoryClientData lastSession = GetLastSession(OperatorClient.LastSessions, SupervisedApplicationName);
				if (lastSession != null)
				{
					if (lastSession.IsLoggedIn.Value == true)
					{
						bool found = false;
						DateTime minDate = DateTime.MinValue;
						for (int i = 0; i < lastSession.StatusHistoryList.Count && found == false; i++)
						{
							SessionStatusHistoryClientData sshcd = (SessionStatusHistoryClientData)lastSession.StatusHistoryList[i];
							if (sshcd.EndDateStatus == DateTime.MinValue)
							{
								status = sshcd;
								found = true;
							}
							else if (sshcd.EndDateStatus > minDate)
							{
								status = sshcd;
								minDate = status.EndDateStatus;
							}
						}
						if (SupervisedApplicationName!=null && SupervisedApplicationName.Equals(UserApplicationClientData.Supervision.Name))
						{
							status.StatusFriendlyName = OperatorStatusClientData.Ready.FriendlyName;
							status.StatusName = OperatorStatusClientData.Ready.Name;
						}
					}
				}
			}
			return status;
		}

		public static SessionHistoryClientData GetLastSession(IList sessions, string applicationName)
		{
			SessionHistoryClientData session = null;
			bool found = false;
			DateTime minDate = DateTime.MinValue;
			for (int i = 0; i < sessions.Count && found == false; i++)
			{
				SessionHistoryClientData temp = (SessionHistoryClientData)sessions[i];
				if (temp.UserApplication == applicationName)
				{
					if (temp.EndDateLogin == DateTime.MinValue)
					{
						session = temp;
						found = true;
					}
					else if (temp.EndDateLogin > minDate)
					{
						session = temp;
						minDate = temp.EndDateLogin.Value;
					}
				}
			}
			return session;
		}

        /// <summary>
        /// http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html
        /// </summary>
        /// <param name="p">Point to find</param>
        /// <param name="poly">Polygon points</param>
        /// <returns>Inside or outside.</returns>
		public static bool PointInPolygon(GeoPoint p, List<GeoPoint> poly)
		{
            int i = 0, j = 0;
            
            bool c = false;

            for (i = 0, j = poly.Count - 1; i < poly.Count; j = i++)
            {
                if (((poly[i].Lat > p.Lat) != (poly[j].Lat > p.Lat)) &&
                 (p.Lon < (poly[j].Lon - poly[i].Lon) * (p.Lat - poly[i].Lat) / (poly[j].Lat - poly[i].Lat) + poly[i].Lon))
                    c = !c;
            }
            return c;
		}

        public static string BuildHtml(string strXml, XslCompiledTransform xslCompiledTransform, string debugFileName = "untitled")
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                XmlReader input = XmlReader.Create(new StringReader(strXml));
#if DEBUG
                File.WriteAllText(debugFileName + ".html", strXml);
#endif
                XmlWriter output = XmlWriter.Create(sb);
                xslCompiledTransform.Transform(input, output);
                return sb.ToString().Replace("language=\"javascript\" />", "language=\"javascript\"></script>");
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
                return "";
            }
        }

        public static string RecoverBlanks(string str)
        {
            string tmp;
            tmp = str.Replace("_lf", "<br>");
            tmp = tmp.Replace("_sp", " &nbsp;");
            return tmp;
        }

        public static string ReplaceBlanks(string str)
        {
            string tmp;
            tmp = "_lf" + str.Replace("\r\n", "_lf");
            tmp = tmp.Replace("  ", "_sp");
            return tmp;
        }
    }

	public class GeoCodeCalc
	{
		public const double EarthRadiusInKilometers = 6367.0;
		
		public static double ToRadian(double val) 
		{ 
			return val * (Math.PI / 180);
		}
		
		public static double DiffRadian(double val1, double val2) 
		{ 
			return ToRadian(val2) - ToRadian(val1);
		}
	
		/// <summary> 
		/// Calculate the distance between two geocodes. 
		/// </summary> 
		public static double CalcDistance(double lat1, double lng1, double lat2, double lng2)
		{
			double radius = GeoCodeCalc.EarthRadiusInKilometers;
			return radius * 2 * Math.Asin(Math.Min(1, Math.Sqrt((Math.Pow(Math.Sin((DiffRadian(lat1, lat2)) / 2.0), 2.0) + Math.Cos(ToRadian(lat1)) * Math.Cos(ToRadian(lat2)) * Math.Pow(Math.Sin((DiffRadian(lng1, lng2)) / 2.0), 2.0)))));
		}
	}

    public class FlashingWindow
    {
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool FlashWindowEx(ref FLASHWINFO pwfi);

        [StructLayout(LayoutKind.Sequential)]
        public struct FLASHWINFO
        {
            public UInt32 cbSize;
            public IntPtr hwnd;
            public UInt32 dwFlags;
            public UInt32 uCount;
            public UInt32 dwTimeout;
        }

        //Stop flashing. The system restores the window to its original state.
        public const UInt32 FLASHW_STOP = 0;
        //Flash the window caption.
        public const UInt32 FLASHW_CAPTION = 1;
        //Flash the taskbar button.
        public const UInt32 FLASHW_TRAY = 2;
        //Flash both the window caption and taskbar button.
        //This is equivalent to setting the FLASHW_CAPTION | FLASHW_TRAY flags.
        public const UInt32 FLASHW_ALL = 3;
        //Flash continuously, until the FLASHW_STOP flag is set.
        public const UInt32 FLASHW_TIMER = 4;
        //Flash continuously until the window comes to the foreground.
        public const UInt32 FLASHW_TIMERNOFG = 12;

        /// <summary>
        /// Flashes a window
        /// </summary>
        /// <param name="hWnd">The handle to the window to flash</param>
        /// <returns>whether or not the window needed flashing</returns>
        public static bool FlashWindowEx(IntPtr hWnd)
        {
            FLASHWINFO fInfo = new FLASHWINFO();

            fInfo.cbSize = Convert.ToUInt32(Marshal.SizeOf(fInfo));
            fInfo.hwnd = hWnd;
            fInfo.dwFlags = FLASHW_ALL | FLASHW_TIMERNOFG;
            fInfo.uCount = UInt32.MaxValue;
            fInfo.dwTimeout = 0;

            return FlashWindowEx(ref fInfo);
        }

        /// Minor adjust to the code above
        /// <summary>
        /// Flashes a window until the window comes to the foreground
        /// Receives the form that will flash
        /// </summary>
        /// <param name="hWnd">The handle to the window to flash</param>
        /// <returns>whether or not the window needed flashing</returns>
        public static bool FlashWindowEx(Form frm)
        {
            return FlashWindowEx(frm.Handle);
        }

		public static void Flashing(ref FLASHWINFO fInfo, bool stop)
		{
			fInfo.dwFlags = stop ? FLASHW_STOP : FLASHW_ALL | FLASHW_TIMER;
			FlashWindowEx(ref fInfo);
		}
	}
}