﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StatisticNames = SmartCadCore.Statistic.StatisticRetriever.StatisticNames;
using SmartCadCore.Statistic;
using SmartCadCore.Model;
using System.Collections;
using SmartCadCore.Common;
namespace SmartCadCore.Core
{
    public class GenesysStatWrapper
    {
        private static DateTime firstDateTime = DateTime.MinValue;
        private static string hostAddress;
        private static string portAddress;
        private static string userServer;
        private static string passwordServer;
        private static string statServiceName;

        private static bool failedConnection = true;
        public static void Init(string host, string port, string userhost, string paswHost,
            string statService)
        {
            try
            {
                hostAddress = host;
                portAddress = port;
                userServer = userhost;
                passwordServer = paswHost;
                statServiceName = statService;
                StatisticRetriever.Init(hostAddress, portAddress, userServer, passwordServer, statServiceName);
                StatisticRetriever.GetStatisticRetriever().ConnectionLost += new EventHandler(GenesysStatWrapper_ConnectionLost);
                StatisticRetriever.GetStatisticRetriever().L11TotalCallsAbandonedWR();
                failedConnection = false;
            }
            catch
            {
                throw;
            }
        }

        static void GenesysStatWrapper_ConnectionLost(object sender, EventArgs e)
        {
            bool run = false;
            if (StatisticRetriever.GetStatisticRetriever() != null)
            {
                StatisticRetriever.GetStatisticRetriever().ConnectionLost -= new EventHandler(GenesysStatWrapper_ConnectionLost);
                string message = string.Empty;
                StatisticRetriever.GetStatisticRetriever().Disconnect(ref message);
                if (string.IsNullOrEmpty(message) == false)
                {
                    SmartLogger.Print(message);
                }
            }
            SmartLogger.Print("Connection to GIS Lost.");
            failedConnection = true;
            while (run == false)
            {
                try
                {
                    System.Threading.Thread.Sleep(10000);
                    Init(hostAddress, portAddress, userServer, passwordServer, statServiceName);
                    run = true;
                    SmartLogger.Print("Connection to GIS recovered.");
                }
                catch (Exception ex)
                {
                    SmartLogger.Print("Connection to GIS was not recovered. Trying again.");
                    System.Threading.Thread.Sleep(10000);
                    failedConnection = true;
                }
            }
        }
        
        public static double GetData(StatisticNames statisticType)
        {
            double result = 0d;
            if (failedConnection == false)
            {
                DateTime currentTime = SmartCadDatabase.GetTimeFromBD();
                if (firstDateTime == DateTime.MinValue || firstDateTime.Date != currentTime.Date)
                {
                    RestartIndicators();
                    if (firstDateTime != DateTime.MinValue)
                    {
                        //Wait 5 minutes for Stat server reinit their statistic                    
                        //System.Threading.Thread.Sleep(TimeSpan.FromMinutes(5));
                    }
                    firstDateTime = currentTime;
                }
                if (statisticType == StatisticNames.L05CurrentCallsInQueue)
                {
                    result = GetDataFromStatServer(statisticType);
                }
                else if (statisticType == StatisticNames.L12ServiceLevel)
                {
                    result = GetServiceLevel();
                }
                else
                {
                    result = GetAcummulativeStatistic(statisticType);
                }
            }
            else
            {
                try
                {
                    TelephonyStatsHistoryData statDB = GetLastStatObjectInDB(statisticType);
                    if (statDB != null)
                    {
                        result = statDB.Value;
                    }
                }
                catch(Exception ex)
                {
                    SmartLogger.Print(ex);
                }
            }
            return result;
        }

        private static double GetServiceLevel()
        {
            double res = 0d;
            double LCallsAbandonedInTh = 0d;
            double LCallsAnsweredInTh = 0d;
            try
            {
                try
                {
                    GetAcummulativeStatistic(StatisticNames.CallsAbandonedInThresholdSubscription);
                    System.Threading.Thread.Sleep(5000);
                    GetAcummulativeStatistic(StatisticNames.CallsAnsweredInThresholdSubscription);
                }
                catch
                {
                }
                string[] args = new string[] { 
                    StatisticNames.L02TotalCallsAbandoned.ToString(),
                    StatisticNames.L10TotalAnsweredCalls.ToString(),
                    StatisticNames.CallsAbandonedInThresholdSubscription.ToString(),
                    StatisticNames.CallsAnsweredInThresholdSubscription.ToString(),
                    StatisticNames.L12ServiceLevel.ToString()};
                IList<TelephonyStatsHistoryData> indicators = SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(
                    "SELECT data FROM TelephonyStatsHistoryData data WHERE data.Name IN ('{0}','{1}','{2}','{3}', '{4}')", args)).Cast<TelephonyStatsHistoryData>().ToList();

                double L10 = indicators.Single(data => data.Name == StatisticNames.L10TotalAnsweredCalls.ToString()).Value;
                double L02 = indicators.Single(data => data.Name == StatisticNames.L02TotalCallsAbandoned.ToString()).Value;
                LCallsAbandonedInTh = indicators.Single(data => data.Name == StatisticNames.CallsAbandonedInThresholdSubscription.ToString()).Value;
                LCallsAnsweredInTh = indicators.Single(data => data.Name == StatisticNames.CallsAnsweredInThresholdSubscription.ToString()).Value;
                TelephonyStatsHistoryData serviceLevelStat = indicators.Single(data => data.Name == StatisticNames.L12ServiceLevel.ToString());
                if ((L10 + L02) > 0)
                {
                    res = (LCallsAbandonedInTh + LCallsAnsweredInTh) / (L10 + L02);
                }
                if (res > 1d)
                    res = 1d;
                serviceLevelStat.Value = res;
                serviceLevelStat.TimeStamp = SmartCadDatabase.GetTimeFromBD();
                SmartCadDatabase.SaveOrUpdate(serviceLevelStat);
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
            return res;
        }
        public static double GetDataFromStatServer(StatisticNames statisticName)
        {
            double result = 0d;
            switch (statisticName)
            {
                case StatisticNames.L02TotalCallsAbandoned:
                    StatisticRetriever.GetStatisticRetriever().L02TotalCallsAbandoned();
                    break;
                case StatisticNames.L05CurrentCallsInQueue:
                    StatisticRetriever.GetStatisticRetriever().L05CurrentCallsInQueue();
                    break;
                case StatisticNames.L06CurrMaxCallWaitingTime:
                    StatisticRetriever.GetStatisticRetriever().L06CurrMaxCallWaitingTime(DateTime.Now);
                    break;
                case StatisticNames.L09TotalCallsEntered:
                    StatisticRetriever.GetStatisticRetriever().L09TotalCallsEntered();
                    break;
                case StatisticNames.L10TotalAnsweredCalls:
                    StatisticRetriever.GetStatisticRetriever().L10TotalAnsweredCalls();
                    break;
                case StatisticNames.L11TotalCallsAbandonedWR:
                    StatisticRetriever.GetStatisticRetriever().L11TotalCallsAbandonedWR();
                    break;
                case StatisticNames.L12ServiceLevel:
                    StatisticRetriever.GetStatisticRetriever().L12ServiceFactor();
                    break;
                case StatisticNames.CallsAbandonedInThresholdSubscription:
                    StatisticRetriever.GetStatisticRetriever().LCallsAbandonedInThresholdCalculate();
                    break;
                case StatisticNames.CallsAnsweredInThresholdSubscription:
                    StatisticRetriever.GetStatisticRetriever().LCallsAnsweredInThresholdCalculate();
                    break;
                default:
                    result = 0d;
                    break;
            }
            result = StatisticRetriever.GetStatisticRetriever().GetStatisticValue(statisticName);
            return result;
        }
        /// <summary>
        /// Method to be used when midnight is reached
        /// </summary>
        private static void RestartIndicators()
        {
            DateTime lastRunTime = DateTime.MinValue;
            try
            {
                string[] indicatorNames = new string[] { 
                StatisticNames.L02TotalCallsAbandoned.ToString(),
                StatisticNames.L05CurrentCallsInQueue.ToString(),
                StatisticNames.L06CurrMaxCallWaitingTime.ToString(),
                StatisticNames.L09TotalCallsEntered.ToString(),
                StatisticNames.L10TotalAnsweredCalls.ToString(),
                StatisticNames.L11TotalCallsAbandonedWR.ToString(),
                StatisticNames.L12ServiceLevel.ToString(),
                StatisticNames.CallsAbandonedInThresholdSubscription.ToString(),
                StatisticNames.CallsAnsweredInThresholdSubscription.ToString()
                };

                string hql = @"SELECT MIN(date.TimeStamp) FROM TelephonyStatsHistoryData date 
                            WHERE date.Name IN ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}')";
                object result = SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(false, hql, indicatorNames));
                DateTime currentTime = SmartCadDatabase.GetTimeFromBD();
                if (result != null)
                    lastRunTime = (DateTime)result;
                if (result == null || lastRunTime.Date != currentTime.Date)
                {
                    foreach (string indName in indicatorNames)
                    {
                        TelephonyStatsHistoryData statDB = SmartCadDatabase.SearchObject<TelephonyStatsHistoryData>(
                               SmartCadHqls.GetCustomHql(
                               SmartCadHqls.GetGenesysStatFromDBByName,
                               indName));
                        if (statDB == null)
                        {
                            statDB = new TelephonyStatsHistoryData();
                        }
                        statDB.Name = indName;
                        statDB.Value = 0d;
                        statDB.ValueRecovery = 0d;
                        statDB.TimeStamp = SmartCadDatabase.GetTimeFromBD();
                        SmartCadDatabase.SaveOrUpdate(statDB);
                        System.Threading.Thread.Sleep(500);
                    }
                }
            }
            catch(Exception ex)
            {
                SmartLogger.Print(ex);                
            }
        }

        private static TelephonyStatsHistoryData GetLastStatObjectInDB(StatisticNames statType)
        {
            TelephonyStatsHistoryData statDB = SmartCadDatabase.SearchObject<TelephonyStatsHistoryData>(
                       SmartCadHqls.GetCustomHql(
                       SmartCadHqls.GetGenesysStatFromDBByName,
                       statType.ToString()));
            if (statDB == null || statDB.TimeStamp.Date != SmartCadDatabase.GetTimeFromBD().Date)
            {
                statDB = null;
            }
            return statDB;
        }

        private static double GetAcummulativeStatistic(StatisticNames statType)
        {
            double result = 0d;
            double valueDB = 0d;
            double valueStat = 0d;
            try
            {
                TelephonyStatsHistoryData statDB = GetLastStatObjectInDB(statType);
                if (statDB != null)
                {
                    valueDB = statDB.Value;
                }
                else
                {
                    statDB = new TelephonyStatsHistoryData();
                    statDB.Name = statType.ToString();
                    statDB.ValueRecovery = 0d;
                }
                valueStat = GetDataFromStatServer(statType);
                
                if (valueDB > valueStat)
                {
                    /*detecto que se cayó pq el valor q llega es menor al anterior y
                     porque el valor de fallo almacenado mas la estadistica*/
                    if (valueDB > statDB.ValueRecovery + valueStat)
                    {                        
                        statDB.ValueRecovery = valueDB;                        
                    }
                    valueDB = statDB.ValueRecovery + valueStat;
                    statDB.Value = valueDB;                    
                }
                else
                {
                    statDB.Value = valueStat;
                }
                
                statDB.TimeStamp = SmartCadDatabase.GetTimeFromBD();
                result = statDB.Value;
                SmartCadDatabase.SaveOrUpdate(statDB);
                System.Threading.Thread.Sleep(5000);
            }
            catch(Exception ex)
            {
                SmartLogger.Print(ex);
            }
            return result;
        }

        public static bool IsNotInitiated()
        {
            return failedConnection == true;
        }
    }
}
