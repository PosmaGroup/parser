using System;
using System.Collections.Generic;
using System.Text;
using System.Data.OleDb;
using System.Data;
using SmartCadCore.Common;

namespace SmartCadCore.Core.Util
{
    public class ExcelReader
    {
        #region Fields

        private OleDbConnection connection;
        private string[] worksheetNames;
        private string connectionstring;

        #endregion

        #region Constants

        private const string PROVIDER = "Microsoft.ACE.OLEDB.12.0";//"Microsoft.Jet.OLEDB.4.0";
        private const string EXCEL_VERSION = "Excel 8.0";
        private const string HDR = "No";
        private const string IMEX = "1";

        #endregion

        #region Properties

        public string ConnectionString
        {
            get { return connectionstring; }
            set { connectionstring = value; }
        }

        public string[] WorksheetNames
        {
            get 
            { 
                return worksheetNames; 
            }
            
        }
	
        #endregion

        #region Constructors
        
        public ExcelReader()
        {
            
        } 
        
        #endregion

        #region PublicMethods

        public DataSet ReadExcel(string path)
        {
            ConnectionString = @"Provider=" + PROVIDER + ";Data Source=" + path + @";Extended Properties=""Excel 8.0;HDR=No;IMEX=1""";
            InitializeWorkSheets(path);
            return GetWorkSheets(path);
        }

        public void InitializeWorkSheets(string path)
        {
            this.worksheetNames = GetWorkSheetNames(path);
        }
        
        #endregion

        #region PrivateMethods

        private string[] GetWorkSheetNames(string path)
        {
            
            connection = new OleDbConnection(ConnectionString);
            string[] retval;
            try
            {
                connection.Open();

                // Get all of the Table names from the Excel workbook
                DataTable dt = connection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                retval = new string[dt.Rows.Count];

                //Add the Table name to the string array.
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    retval[i] = (string)dt.Rows[i]["TABLE_NAME"];
                }
            }
            catch (OleDbException oleE)
            {
                throw new Exception(ResourceLoader.GetString2("CanNotOpenFile"));   
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                connection.Close();
            }

            return retval;
        }

        private DataSet GetWorkSheets(string filepath)
        {
            DataSet retval = new DataSet();
            try
            {
                // If the worksheet name exists...
                for (int i = 0; i < WorksheetNames.Length; i++)
                {
                    // Open the connection to the Excel document, and select the data from the first worksheet
                    connection = new OleDbConnection(ConnectionString);
                    connection.Open();
                    OleDbDataAdapter da = new OleDbDataAdapter(@"SELECT * FROM [" + WorksheetNames[i] + "]", connection);
                    da.Fill(retval, "Table" + i);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                connection.Close();
            }

            return retval;
        }

        
        #endregion
    }
}
