//using System;
//using System.Collections.Generic;
//using System.Text;
//using SmartCadCore.Model;
//using System.Collections;

//namespace SmartCadCore.Core.Indicators
//{
//    public class DPN22IndicatorValuesCalculator : IndicatorValuesCalculator
//    {
//        protected override string GetIndicator()
//        {
//            return "DPN22";
//        }

//        protected override void Calculate()
//        {
//            CalculateSystemIndicators();         
//        }
//        private void CalculateSystemIndicators()
//        {
//            #region new
//            string sql = @"SELECT		COUNT(REB.CODE) AS CANTIDAD			
//			,/*CASE*/	INC.IS_EMERGENCY AS EMERGENCIA
//					--WHEN 0 THEN 'NO EMERGENCIA'
//					--WHEN 1 THEN 'EMERGENCIA'
//			--END AS EMERGENCIA
//FROM		PHONE_REPORT AS PHR
//			,REPORT_BASE AS REB
//			,INCIDENT AS INC
//WHERE		CONVERT(DATETIME,CONVERT(CHAR(10),PHR.PICKED_UP_CALL_TIME,101),101)= 
//			CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(),101),101)
//AND			PHR.PARENT_CODE = REB.CODE
//AND			REB.INCIDENT_CODE = INC.CODE
//GROUP BY	INC.IS_EMERGENCY";

//            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);

//            double totalCalls = 0;
//            double emergency = 0;
//            List<double> resultsValues = new List<double>();
//            Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();

//            foreach (object[] item in data)
//            {
//                totalCalls += double.Parse(item[0].ToString());
//            }
//            foreach (object[] item in data)
//            {
//                if((bool)item[1] == true)
//                {
//                    emergency = double.Parse(item[0].ToString());
//                    break;
//                }
//            }
//            resultsValues.Add(emergency);
//            if (totalCalls == 0)
//            {
//                resultsValues.Add(0);
//            }
//            else
//            {
//                resultsValues.Add((emergency * 100) / totalCalls);
//            }
//            result.Add("emergency", resultsValues);
//            SaveIndicatorMultiplesValues(null, IndicatorClassData.System, result);

//            #endregion new

//            #region doc
//            //double mediaTime = 0;
//            //double totalEmergencyCalls = 0;
//            //double percentValueEmergencyCalls = 0;

//            //IList phoneCallEmergency = SmartCadDatabase.SearchObjects(
//            //    SmartCadHqls.GetCustomHql(SmartCadHqls.GetPhoneReportsEmergencyCurrentday,
//            //    ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today),
//            //    ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today.AddDays(1))));

//            //IList phoneCallNoEmergency = SmartCadDatabase.SearchObjects(
//            //       SmartCadHqls.GetCustomHql(SmartCadHqls.GetPhoneReportsNoEmergencyCurrentday,
//            //       ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today),
//            //       ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today.AddDays(1))));


//            //if (phoneCallEmergency.Count != 0)
//            //{
//            //    foreach (PhoneReportData prd in phoneCallEmergency)
//            //    {
//            //        if (prd.HangedUpCallTime != null)
//            //        {
//            //            TimeSpan ts = prd.HangedUpCallTime.Value.Subtract(prd.PickedUpCallTime.Value);
//            //            mediaTime += ts.TotalMilliseconds;
//            //        }
//            //    }
//            //    mediaTime /= 1000;
//            //    mediaTime /= phoneCallEmergency.Count;                
//            //    totalEmergencyCalls = phoneCallEmergency.Count;
//            //    percentValueEmergencyCalls = (phoneCallEmergency.Count * 100) / (phoneCallEmergency.Count + phoneCallNoEmergency.Count);
//            //}
//            //    List<double> resultsValues = new List<double>();
//            //    resultsValues.Add(totalEmergencyCalls);
//            //    resultsValues.Add(percentValueEmergencyCalls);
//            //    resultsValues.Add(mediaTime);                
//            //    Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
//            //    result.Add("Emergency", resultsValues);
//            //    SaveIndicatorMultiplesValues(null, IndicatorClassData.System, result);
//            #endregion doc

//        }
    
      
//    }
//}

