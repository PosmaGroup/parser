using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.Model;
using System.Collections;

namespace SmartCadCore.Core.Indicators
{
    public class PN08IndicatorValuesCalculator : IndicatorValuesCalculator
    {
        protected override string GetIndicator()
        {
            return "PN08";
        }

        protected override void Calculate()
        {
            CalculateSystem();
            CalculateGroup();
        }

        private void CalculateSystem()
        {
            #region new
            string sql = @" SELECT	(SUM(TEMP.CANTIDAD_TRA)-SUM(TEMP.CANTIDAD_TRA_ONLINE))*100/SUM(TEMP.CANTIDAD_SUPERVISED) AS TRAINING
		,TEMP.APLICACION AS APLICACION
FROM
(
SELECT	COUNT(OPA.OPERATOR_CODE) AS CANTIDAD_TRA
		,0 AS CANTIDAD_SUPERVISED
		,0 AS CANTIDAD_TRA_ONLINE
		,APP.CODE AS APLICACION
FROM        OPERATOR_ASSIGN AS OPA
			,OPERATOR AS OPE
			,OPERATOR AS OPE1
			,OPERATOR_TRAINING_COURSE AS OTC
			,TRAINING_COURSE_SCHEDULE AS TCS
			,TRAINING_COURSE_SCHEDULE_PARTS AS TCSP
			,USER_ROLE AS USR
			,USER_ROLE_PROFILE AS URP
			,USER_PROFILE AS USP
			,USER_PROFILE_ACCESS AS UPA
			,USER_ACCESS AS USA
			,USER_APPLICATION AS APP
WHERE		OPA.DELETED_ID IS NULL
AND			(
				GETDATE()
				BETWEEN OPA.START_DATE AND OPA.END_DATE
			)
AND			OPA.SUPERVISOR_CODE = OPE.CODE
AND			OPE.CODE <> 1
AND			OTC.OPERATOR_CODE = OPA.OPERATOR_CODE
AND			TCS.CODE = OTC.TRAINING_COURSE_SCHEDULE_CODE
AND			TCS.DELETED_ID IS NULL 
AND			TCS.CODE = TCSP.TRAINING_COURSE_SCHEDULE_CODE
AND			TCSP.DELETED_ID IS NULL
AND         TCSP.START_DATE <= GETDATE()
AND			TCSP.END_DATE >= GETDATE()
AND			OPE1.CODE = OPA.OPERATOR_CODE
AND			USR.CODE = OPE1.USER_ROLE_CODE
AND			URP.USER_ROLE_CODE = USR.CODE
AND			USP.CODE = URP.USER_PROFILE_CODE
AND			UPA.USER_PROFILE_CODE = USP.CODE
AND			USA.CODE = UPA.USER_ACCESS_CODE
AND			APP.CODE = USA.USER_APPLICATION_CODE
AND			(
			APP.CODE = 2 
			
			)
GROUP BY APP.CODE
UNION ALL
SELECT	0 AS CANTIDAD_TRA
		,0 AS CANTIDAD_SUPERVISED
		,COUNT(DISTINCT OPA.OPERATOR_CODE) AS CANTIDAD_TRA_ONLINE
		,APP.CODE AS APLICACION
FROM        OPERATOR_ASSIGN AS OPA
			,OPERATOR AS OPE
			,OPERATOR AS OPE1
			,OPERATOR_TRAINING_COURSE AS OTC
			,TRAINING_COURSE_SCHEDULE AS TCS
			,TRAINING_COURSE_SCHEDULE_PARTS AS TCSP
			,USER_ROLE AS USR
			,USER_ROLE_PROFILE AS URP
			,USER_PROFILE AS USP
			,USER_PROFILE_ACCESS AS UPA
			,USER_ACCESS AS USA
			,USER_APPLICATION AS APP
			,SESSION_HISTORY AS SEH
			,SESSION_STATUS_HISTORY AS STH
			,OPERATOR_STATUS AS OPS
WHERE		OPA.DELETED_ID IS NULL
AND			(
				GETDATE()
				BETWEEN OPA.START_DATE AND OPA.END_DATE
			)
AND			OPA.SUPERVISOR_CODE = OPE.CODE
AND			OPE.CODE <> 1
AND			OPA.OPERATOR_CODE = SEH.USER_ACCOUNT_CODE
AND			SEH.IS_LOGGED_IN = 'TRUE'
AND			SEH.END_DATE_LOGIN IS NULL
AND			SEH.USER_APPLICATION_CODE IN (2)
AND			OTC.OPERATOR_CODE = OPA.OPERATOR_CODE
AND			TCS.CODE = OTC.TRAINING_COURSE_SCHEDULE_CODE
AND			TCS.DELETED_ID IS NULL 
AND			TCS.CODE = TCSP.TRAINING_COURSE_SCHEDULE_CODE
AND			TCSP.DELETED_ID IS NULL
AND         TCSP.START_DATE <= GETDATE()
AND			TCSP.END_DATE >= GETDATE()
AND			OPE1.CODE = OPA.OPERATOR_CODE
AND			USR.CODE = OPE1.USER_ROLE_CODE
AND			URP.USER_ROLE_CODE = USR.CODE
AND			USP.CODE = URP.USER_PROFILE_CODE
AND			UPA.USER_PROFILE_CODE = USP.CODE
AND			USA.CODE = UPA.USER_ACCESS_CODE
AND			APP.CODE = USA.USER_APPLICATION_CODE
AND			(
			APP.CODE = 2 
			)
GROUP BY APP.CODE
UNION ALL
SELECT	0 AS CANTIDAD_TRA
		,COUNT(OPA.OPERATOR_CODE) AS CANTIDAD_SUPERVISED
		,0 AS CANTIDAD_TRA_ONLINE
		,APP.CODE AS APLICACION
FROM	OPERATOR_ASSIGN AS OPA
		,OPERATOR AS OPE
		,USER_ROLE AS USR
		,USER_ROLE_PROFILE AS URP
		,USER_PROFILE AS USP
		,USER_PROFILE_ACCESS AS UPA
		,USER_ACCESS AS USA
		,USER_APPLICATION AS APP
WHERE	OPA.DELETED_ID IS NULL
AND		GETDATE()
		BETWEEN OPA.START_DATE AND OPA.END_DATE
AND		OPA.DELETED_ID IS NULL
AND		OPE.CODE = OPA.OPERATOR_CODE
AND		USR.CODE = OPE.USER_ROLE_CODE
AND		URP.USER_ROLE_CODE = USR.CODE
AND		USP.CODE = URP.USER_PROFILE_CODE
AND		UPA.USER_PROFILE_CODE = USP.CODE
AND		USA.CODE = UPA.USER_ACCESS_CODE
AND		APP.CODE = USA.USER_APPLICATION_CODE
AND		(
		APP.CODE = 2 
		)
GROUP BY APP.CODE
) AS TEMP
GROUP BY TEMP.APLICACION
";


            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);
            double result = 0;
            if (data.Count > 0)
            {
                object[] item = (object[])data[0];
                result = double.Parse(item[0].ToString());
                result = result / 100;
            }
            SaveIndicatorsValues(IndicatorClassData.System, result);

            #endregion new

            #region doc
//            double operatorsInCourseNow = 0;
//            IList operators = OperatorScheduleManager.GetOperatorsWorkingNowByApplication(UserApplicationData.FirstLevel.Name, false); 
//            foreach (int operatorCode in operators)
//            {
//                string hql = @"select count(*)
//                           from OperatorTrainingCourseData otcd inner join
//                                otcd.TrainingCourseSchedule.Parts part,
//                                SessionHistoryData
//                           where '{0}' between part.Start and part.End and
//                                 otcd.Operator.Code = {1}";

//                double operatorInCourseNow = (long)SmartCadDatabase.SearchBasicObject(
//                                                SmartCadHqls.GetCustomHql(
//                                                    hql,
//                                                    ApplicationUtil.GetDataBaseFormattedDate(DateTime.Now),
//                                                    operatorCode));

//                if (operatorInCourseNow > 0)
//                {
//                    double operatorConnected = (long)SmartCadDatabase.SearchBasicObject(
//                                                SmartCadHqls.GetCustomHql(
//                                                SmartCadHqls.GetCountLoggedOperatorByApplication,
//                                                true.ToString().ToLower(),
//                                                UserApplicationData.FirstLevel.Code,
//                                                operatorCode));

//                    if (operatorConnected == 0)
//                        operatorsInCourseNow += 1;
//                }
//            }

//            //double operatorsWorkingNow = OperatorScheduleManager.GetAllOperatorsWorkingNow(false).Count;

//            double result = 0;

//            if (operators.Count > 0)
//                result = operatorsInCourseNow / operators.Count;

//            SaveIndicatorsValues(IndicatorClassData.System, result);
            #endregion doc
        }

        private void CalculateGroup()
        {
            #region new
            string sql = @" SELECT	(SUM(TEMP.QTY_TRAINNING)-SUM(TEMP.QTY_TRAINNING_ONLINE))*100/SUM(TEMP.QTY_SUPERVISED) AS TRAINING
		,TEMP.SUPERVISOR
FROM
(
SELECT		COUNT(DISTINCT OPA.OPERATOR_CODE) AS QTY_SUPERVISED
			,0 AS QTY_TRAINNING_ONLINE
			,0 AS QTY_TRAINNING
			,OPE.CODE AS SUPERVISOR
FROM		OPERATOR AS OPE
			,OPERATOR_ASSIGN AS OPA
WHERE		OPA.DELETED_ID IS NULL
AND			OPE.CODE = OPA.SUPERVISOR_CODE
AND			(
				GETDATE()
				BETWEEN OPA.START_DATE AND OPA.END_DATE
			)
GROUP BY	OPE.CODE
UNION ALL
SELECT		0 AS QTY_SUPERVISED
			,COUNT(DISTINCT OPA.OPERATOR_CODE) AS QTY_TRAINNING_ONLINE
			,0 AS QTY_TRAINNING
			,OPE.CODE AS SUPERVISOR
FROM        OPERATOR_ASSIGN AS OPA
			,OPERATOR AS OPE
			,SESSION_HISTORY AS SEH
			,OPERATOR_TRAINING_COURSE AS OTC
			,TRAINING_COURSE_SCHEDULE AS TCS
			,TRAINING_COURSE_SCHEDULE_PARTS AS TCSP
WHERE		OPA.DELETED_ID IS NULL
AND			(
				GETDATE()
				BETWEEN OPA.START_DATE AND OPA.END_DATE
			)
AND			OPA.SUPERVISOR_CODE = OPE.CODE
AND			OPE.CODE <> 1
AND			OPA.OPERATOR_CODE = SEH.USER_ACCOUNT_CODE
AND			SEH.IS_LOGGED_IN = 'TRUE'
AND			SEH.END_DATE_LOGIN IS NULL
AND			SEH.USER_APPLICATION_CODE IN (2)
AND			OTC.OPERATOR_CODE = OPA.OPERATOR_CODE
AND			TCS.CODE = OTC.TRAINING_COURSE_SCHEDULE_CODE
AND			TCS.DELETED_ID IS NULL 
AND			TCS.CODE = TCSP.TRAINING_COURSE_SCHEDULE_CODE
AND			TCSP.DELETED_ID IS NULL
AND         TCSP.START_DATE <= GETDATE()
AND			TCSP.END_DATE >= GETDATE()
GROUP BY	OPE.CODE  
UNION ALL
SELECT		0 AS QTY_SUPERVISED
			,0 AS QTY_TRAINNING_ONLINE
			,COUNT(DISTINCT OPA.OPERATOR_CODE) AS QTY_TRAINNING
			,OPE.CODE AS SUPERVISOR
FROM        OPERATOR_ASSIGN AS OPA
			,OPERATOR AS OPE
			,OPERATOR_TRAINING_COURSE AS OTC
			,TRAINING_COURSE_SCHEDULE AS TCS
			,TRAINING_COURSE_SCHEDULE_PARTS AS TCSP
WHERE		OPA.DELETED_ID IS NULL
AND			(
				GETDATE()
				BETWEEN OPA.START_DATE AND OPA.END_DATE
			)
AND			OPA.SUPERVISOR_CODE = OPE.CODE
AND			OPE.CODE <> 1
AND			OTC.OPERATOR_CODE = OPA.OPERATOR_CODE
AND			TCS.CODE = OTC.TRAINING_COURSE_SCHEDULE_CODE
AND			TCS.DELETED_ID IS NULL 
AND			TCS.CODE = TCSP.TRAINING_COURSE_SCHEDULE_CODE
AND			TCSP.DELETED_ID IS NULL
AND         TCSP.START_DATE <= GETDATE()
AND			TCSP.END_DATE >= GETDATE()
GROUP BY	OPE.CODE
) AS TEMP
GROUP BY TEMP.SUPERVISOR
";

            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);


            foreach (object[] item in data)
            {
                double result = double.Parse(item[0].ToString());
                result = result / 100;
                ObjectData objectData = new ObjectData();
                objectData.Code = int.Parse(item[1].ToString());
                SaveIndicatorsValues(objectData, IndicatorClassData.Group, result);
            }
            #endregion new


            #region doc
//            IList supervisors = OperatorScheduleManager.GetSupervisorsWorkingNowByApplication("FirstLevelSupervisorName");
//            foreach (int supervisorCode in supervisors)
//            {
//                IList operators = OperatorScheduleManager.GetOperatorsBySupervisor(supervisorCode);

//                double operatorsInCourseNow = 0;

//                foreach (OperatorData operatorData in operators)
//                {
//                    string hql = @"select count(*)
//                           from OperatorTrainingCourseData otcd inner join
//                                otcd.TrainingCourseSchedule.Parts part
//                           where '{0}' between part.Start and part.End and
//                                 otcd.Operator.Code = {1}";
                    
//                    long operatorInCourse = (long)SmartCadDatabase.SearchBasicObject(
//                                SmartCadHqls.GetCustomHql(
//                                    hql,
//                                    ApplicationUtil.GetDataBaseFormattedDate(DateTime.Now),
//                                    operatorData.Code));

//                    if (operatorInCourse > 0)
//                    {
//                        double operatorConnected = (long)SmartCadDatabase.SearchBasicObject(
//                                                SmartCadHqls.GetCustomHql(
//                                                SmartCadHqls.GetCountLoggedOperatorByApplication,
//                                                true.ToString().ToLower(),
//                                                UserApplicationData.FirstLevel.Code,
//                                                operatorData.Code));

//                        if (operatorConnected == 0)
//                            operatorsInCourseNow += 1;
//                    }
//                }
                
//                double operatorsWorkingNow = operators.Count;

//                double result = 0;

//                if (operatorsInCourseNow > 0 && operatorsWorkingNow > 0)
//                    result = operatorsInCourseNow / operatorsWorkingNow;

//                ObjectData objectData = new ObjectData();
//                objectData.Code = supervisorCode;
//                SaveIndicatorsValues(objectData, IndicatorClassData.Group, result);
//            }
            #endregion doc
        } 
    }
}
