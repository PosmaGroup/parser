using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.Model;
using System.Collections;
using SmartCadCore.Common;

namespace SmartCadCore.Core.Indicators
{
    public class PN20IndicatorValuesCalculator : IndicatorValuesCalculator
    {       

        protected override string GetIndicator()
        {
            return "PN20";
        }

        protected override void Calculate()
        {
            CalculateSystem();
            CalculateGroup();
        }

        private void CalculateSystem()
        {
            #region new all
            string sqlConnected = @" SELECT		COUNT(SEH.USER_ACCOUNT_CODE) AS CANTIDAD
			,OPS.CODE AS ESTADO		
FROM        USER_APPLICATION AS USA
			,SESSION_HISTORY AS SEH
			,SESSION_STATUS_HISTORY AS STH
			,OPERATOR_STATUS AS OPS
WHERE		(
			
			USA.CODE = 2
			)
AND			USA.CODE = SEH.USER_APPLICATION_CODE
AND			SEH.CODE = STH.SESSION_HISTORY_CODE
AND			STH.OPERATOR_STATUS_CODE = OPS.CODE
AND			SEH.END_DATE_LOGIN IS NULL
AND			STH.END_DATE IS NULL 
AND         SEH.IS_LOGGED_IN = 1
GROUP BY	OPS.CODE,USA.FRIENDLY_NAME

";

            string sqlNoConnected = @" SELECT	COUNT(OPA.OPERATOR_CODE) AS CANTIDAD
		,'No Conectado' AS ESTADOS	
FROM	OPERATOR_ASSIGN AS OPA
		,OPERATOR AS OPE
		,USER_ROLE AS USR
		,USER_ROLE_PROFILE AS URP
		,USER_PROFILE AS USP
		,USER_PROFILE_ACCESS AS UPA
		,USER_ACCESS AS USA
		,USER_APPLICATION AS APP
WHERE	OPA.DELETED_ID IS NULL
AND		GETDATE()
		BETWEEN OPA.START_DATE AND OPA.END_DATE
AND		OPA.OPERATOR_CODE NOT IN (
									SELECT	SEH.USER_ACCOUNT_CODE AS OPERATOR
									FROM	SESSION_HISTORY AS SEH
									WHERE	SEH.IS_LOGGED_IN = 1
									)
AND		OPE.CODE = OPA.OPERATOR_CODE
AND		USR.CODE = OPE.USER_ROLE_CODE
AND		URP.USER_ROLE_CODE = USR.CODE
AND		USP.CODE = URP.USER_PROFILE_CODE
AND		UPA.USER_PROFILE_CODE = USP.CODE
AND		USA.CODE = UPA.USER_ACCESS_CODE
AND		APP.CODE = USA.USER_APPLICATION_CODE
AND		(
		
		APP.CODE = 2
		)
";
            IList operatorsConnected = (IList)SmartCadDatabase.SearchBasicObjects(sqlConnected, false);
            IList operatorsNoConnected = (IList)SmartCadDatabase.SearchBasicObjects(sqlNoConnected, false);

           // object[] dataTest = new object { 3};
            double totalOperatorsConnected = 0;
            double totalOperatorsNoConnected = 0;
            double totalOperators = 0;
            List<double> resultsValues = new List<double>();
            Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();

            foreach (object[] item in operatorsConnected)
            {
                totalOperatorsConnected += int.Parse(item[0].ToString());
            }
            foreach (object[] item in operatorsNoConnected)
            {
                totalOperatorsNoConnected += int.Parse(item[0].ToString());
            }
            totalOperators = totalOperatorsConnected + totalOperatorsNoConnected;



            resultsValues.Add(totalOperatorsConnected);
            resultsValues.Add((totalOperatorsConnected / totalOperators) * 100);
            result.Add(ResourceLoader.GetString2("IndicatorD22Name"), resultsValues);
            SaveIndicatorMultiplesValues(null, IndicatorClassData.System, result);


            result.Clear();
            resultsValues.Clear();
            resultsValues.Add(totalOperatorsNoConnected);
            resultsValues.Add((totalOperatorsNoConnected / totalOperators) * 100);
            result.Add(ResourceLoader.GetString2("IndicatorD23Name"), resultsValues);
            SaveIndicatorMultiplesValues(null, IndicatorClassData.System, result);

            result.Clear();
            resultsValues.Clear();
            foreach (object[] item in operatorsConnected)
            {
                
                resultsValues.Add(double.Parse(item[0].ToString()));
                resultsValues.Add((double.Parse(item[0].ToString()) / totalOperatorsConnected) * 100);

                if (int.Parse(item[1].ToString()) == OperatorStatusData.Absent.Code)
                {
                    result.Add(ResourceLoader.GetString2("IndicatorD35Name"), resultsValues);
                }
                else if (int.Parse(item[1].ToString()) == OperatorStatusData.Bathroom.Code)
                {
                    result.Add(ResourceLoader.GetString2("IndicatorD38Name"), resultsValues);
                }
                else if (int.Parse(item[1].ToString()) == OperatorStatusData.Busy.Code)
                {
                    result.Add(ResourceLoader.GetString2("IndicatorD34Name"), resultsValues);
                }
                else if (int.Parse(item[1].ToString()) == OperatorStatusData.Ready.Code)
                {
                    result.Add(ResourceLoader.GetString2("IndicatorD20Name"), resultsValues);
                }
                else if (int.Parse(item[1].ToString()) == OperatorStatusData.Rest.Code)
                {
                    result.Add(ResourceLoader.GetString2("IndicatorD37Name"), resultsValues);
                }
                else if (int.Parse(item[1].ToString()) == OperatorStatusData.Reunion.Code)
                {
                    result.Add(ResourceLoader.GetString2("IndicatorD36Name"), resultsValues);
                }
                SaveIndicatorMultiplesValues(null, IndicatorClassData.System, result);
                result.Clear();
                resultsValues.Clear();
            }




            #endregion new all

        }

        private void CalculateGroup()
        {
            #region new all
            string operatorsConnectedSql = @" SELECT		COUNT(SEH.USER_ACCOUNT_CODE) AS CANTIDAD						
			,OPS.CODE As STATUS
			,OPE.CODE AS SUPERVISOR			
FROM        USER_APPLICATION AS USA
			,SESSION_HISTORY AS SEH
			,SESSION_STATUS_HISTORY AS STH
			,OPERATOR_STATUS AS OPS
			,OPERATOR_ASSIGN AS OPA
			,OPERATOR AS OPE
WHERE		USA.CODE = SEH.USER_APPLICATION_CODE
AND			USA.CODE = 2			
AND			SEH.CODE = STH.SESSION_HISTORY_CODE
AND			STH.OPERATOR_STATUS_CODE = OPS.CODE
AND			SEH.END_DATE_LOGIN IS NULL
AND			STH.END_DATE IS NULL 
AND         SEH.IS_LOGGED_IN = 1
AND			SEH.USER_ACCOUNT_CODE = OPA.OPERATOR_CODE
AND			OPA.DELETED_ID IS NULL
AND			(
				(				
				GETDATE()
				BETWEEN OPA.START_DATE AND OPA.END_DATE
				)			
			)
AND			OPA.SUPERVISOR_CODE = OPE.CODE
GROUP BY	OPE.CODE, OPS.CODE
order by OPE.CODE
";

            string operatorsNoConnectedSql = @"SELECT	count(distinct(OPA.OPERATOR_CODE)) AS CANTIDAD		
		,OPE2.CODE AS SUPERVISOR		
FROM	OPERATOR_ASSIGN AS OPA
		,OPERATOR AS OPE
		,OPERATOR AS OPE2
		,USER_ROLE AS USR
		,USER_ROLE_PROFILE AS URP
		,USER_PROFILE AS USP
		,USER_PROFILE_ACCESS AS UPA
		,USER_ACCESS AS USA
		,USER_APPLICATION AS APP		
WHERE	OPA.DELETED_ID IS NULL
AND		GETDATE()
		BETWEEN OPA.START_DATE AND OPA.END_DATE
AND		OPA.OPERATOR_CODE NOT IN (
									SELECT	SEH.USER_ACCOUNT_CODE AS OPERATOR
									FROM	SESSION_HISTORY AS SEH
									WHERE	SEH.IS_LOGGED_IN = 1
									)
AND		OPE.CODE = OPA.OPERATOR_CODE
AND		OPE2.CODE = OPA.SUPERVISOR_CODE
AND		USR.CODE = OPE.USER_ROLE_CODE
AND		URP.USER_ROLE_CODE = USR.CODE
AND		USP.CODE = URP.USER_PROFILE_CODE
AND		UPA.USER_PROFILE_CODE = USP.CODE
AND		USA.CODE = UPA.USER_ACCESS_CODE
AND		APP.CODE = USA.USER_APPLICATION_CODE
AND		APP.CODE = 2		
GROUP By OPE2.CODE
order By OPE2.CODE 
";

            IList operatorsConnected = (IList)SmartCadDatabase.SearchBasicObjects(operatorsConnectedSql, false);
            IList operatorsNoConnected = (IList)SmartCadDatabase.SearchBasicObjects(operatorsNoConnectedSql, false);

            Dictionary<int, double> supervisorCodesTotalOperators = new Dictionary<int, double>();
            Dictionary<int, double> supervisorCodesConnectedOperators = new Dictionary<int, double>();
            Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
            List<double> resultsValues = new List<double>();

            foreach (object[] item in operatorsConnected)
            {

                if (supervisorCodesTotalOperators.ContainsKey(int.Parse(item[2].ToString())) == false)
                {
                    supervisorCodesTotalOperators.Add(int.Parse(item[2].ToString()), int.Parse(item[0].ToString()));
                    supervisorCodesConnectedOperators.Add(int.Parse(item[2].ToString()), int.Parse(item[0].ToString()));
                }
                else
                {
                    supervisorCodesConnectedOperators[int.Parse(item[2].ToString())] += int.Parse(item[0].ToString());
                    supervisorCodesTotalOperators[int.Parse(item[2].ToString())] += int.Parse(item[0].ToString());
                }

            }
            foreach (object[] item in operatorsNoConnected)
            {


                if (supervisorCodesTotalOperators.ContainsKey(int.Parse(item[1].ToString())) == false)
                {
                    supervisorCodesTotalOperators.Add(int.Parse(item[1].ToString()), int.Parse(item[0].ToString()));
                }
                else
                {
                    supervisorCodesTotalOperators[int.Parse(item[1].ToString())] += int.Parse(item[0].ToString());
                }
            }

            foreach (KeyValuePair<int, double> pair in supervisorCodesTotalOperators)
            {
                ObjectData od1 = new ObjectData();
                od1.Code = pair.Key;

                if (supervisorCodesConnectedOperators.ContainsKey(pair.Key) == true)
                {
                    resultsValues.Add(supervisorCodesConnectedOperators[pair.Key]);
                    resultsValues.Add((supervisorCodesConnectedOperators[pair.Key] / pair.Value) * 100);
                    result.Add(ResourceLoader.GetString2("IndicatorD22Name"), resultsValues);
                }
                else
                {
                    resultsValues.Add(0);
                    resultsValues.Add(0);
                    result.Add(ResourceLoader.GetString2("IndicatorD22Name"), resultsValues);
                }

                SaveIndicatorMultiplesValues(od1, IndicatorClassData.Group, result);
                result.Clear();
                resultsValues.Clear();

                if (supervisorCodesConnectedOperators.ContainsKey(pair.Key) == true)
                {
                    double operatorsNoConnectedByGroup = pair.Value - supervisorCodesConnectedOperators[pair.Key];
                    resultsValues.Add(operatorsNoConnectedByGroup);
                    resultsValues.Add((operatorsNoConnectedByGroup / pair.Value) * 100);
                    result.Add(ResourceLoader.GetString2("IndicatorD23Name"), resultsValues);
                }
                else
                {
                    resultsValues.Add(pair.Value);
                    resultsValues.Add(100);
                    result.Add(ResourceLoader.GetString2("IndicatorD23Name"), resultsValues);
                }
                SaveIndicatorMultiplesValues(od1, IndicatorClassData.Group, result);
                result.Clear();
                resultsValues.Clear();

                foreach (object[] item in operatorsConnected)
                {
                    if (int.Parse(item[2].ToString()) == pair.Key)
                    {
                        resultsValues.Add(int.Parse(item[0].ToString()));
                        resultsValues.Add((int.Parse(item[0].ToString()) * 100) /
                            supervisorCodesConnectedOperators[int.Parse(item[2].ToString())]);
                        if (int.Parse(item[1].ToString()) == OperatorStatusData.Absent.Code)
                        {
                            result.Add(ResourceLoader.GetString2("IndicatorD35Name"), resultsValues);
                        }
                        else if (int.Parse(item[1].ToString()) == OperatorStatusData.Bathroom.Code)
                        {
                            result.Add(ResourceLoader.GetString2("IndicatorD38Name"), resultsValues);
                        }
                        else if (int.Parse(item[1].ToString()) == OperatorStatusData.Busy.Code)
                        {
                            result.Add(ResourceLoader.GetString2("IndicatorD34Name"), resultsValues);
                        }
                        else if (int.Parse(item[1].ToString()) == OperatorStatusData.Ready.Code)
                        {
                            result.Add(ResourceLoader.GetString2("IndicatorD20Name"), resultsValues);
                        }
                        else if (int.Parse(item[1].ToString()) == OperatorStatusData.Rest.Code)
                        {
                            result.Add(ResourceLoader.GetString2("IndicatorD37Name"), resultsValues);
                        }
                        else if (int.Parse(item[1].ToString()) == OperatorStatusData.Reunion.Code)
                        {
                            result.Add(ResourceLoader.GetString2("IndicatorD36Name"), resultsValues);
                        }
                        SaveIndicatorMultiplesValues(od1, IndicatorClassData.Group, result);
                        result.Clear();
                        resultsValues = new List<double>();
                    }
                }
            }
            #endregion new            
        }       
    }
}
