﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.Model;
using SmartCadCore.Statistic;
using SmartCadCore.Core.Util;

namespace SmartCadCore.Core.Indicators
{
    public class L06IndicatorValuesCalculator : IndicatorValuesCalculator
    {
        protected override string GetIndicator()
        {
            return "L06";
        }

        protected override void Calculate()
        {
            try
            {
                if (TelephonyServerStatisticElement.CurrentServerType == TelephonyServerTypes.Genesys)
                {
                    double value = GenesysStatWrapper.GetData(StatisticRetriever.StatisticNames.L06CurrMaxCallWaitingTime);
                    SaveIndicatorsValues(IndicatorClassData.System, value);
                }
                else if (TelephonyServerStatisticElement.CurrentServerType == TelephonyServerTypes.Nortel)
                {
                    SaveIndicatorsValues(IndicatorClassData.System, NortelRTDWrapper.GetData((int)NRTD_Values.NIrtd_APPL_MAX_WAITING_TIME));
                }
                else if (TelephonyServerStatisticElement.CurrentServerType == TelephonyServerTypes.Avaya)
                {
                    
                    SaveIndicatorsValues(IndicatorClassData.System,
                        AvayaStatWrapper.GetData(StatisticRetriever.StatisticNames.L06CurrMaxCallWaitingTime));
                }
                else if (TelephonyServerStatisticElement.CurrentServerType == TelephonyServerTypes.Asterisk)
                {
                    AsteriskStatWrapper AsteriskWrapper = new AsteriskStatWrapper();
                    SaveIndicatorsValues(IndicatorClassData.System,
                        AsteriskStatWrapper.GetData(StatisticRetriever.StatisticNames.L06CurrMaxCallWaitingTime));
                }
            }
            catch (Exception ex)
            {
            }            
        }
    }
}
