using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.Model;
using System.Collections;
using SmartCadCore.Common;

namespace SmartCadCore.Core.Indicators
{
    public class DPN24IndicatorValuesCalculator : IndicatorValuesCalculator
    {
        protected override string GetIndicator()
        {
            return "DPN24";
        }


        protected override void Calculate()
        {      
            CalculateDepartment();
        }             
        private void CalculateDepartment()
        {
            #region new
            string openIncidentNotificationSql = @" SELECT		COUNT(INOT.CODE) AS ABIERTAS,DETY.CODE AS ORGANISMO
FROM		INCIDENT_NOTIFICATION AS INOT
			,DEPARTMENT_TYPE AS DETY
WHERE		INOT.END_DATE IS NULL
AND			INOT.DEPARTMENT_TYPE_CODE = DETY.CODE
GROUP BY DETY.CODE
order by DETY.CODE
";

            string closeIncidentNotificationSql = @" SELECT		COUNT(INOT.CODE) AS CERRADAS,DETY.CODE AS ORGANISMO
FROM		INCIDENT_NOTIFICATION AS INOT
			,DEPARTMENT_TYPE AS DETY
WHERE		INOT.START_DATE IS NOT NULL
AND			CONVERT(DATETIME,CONVERT(CHAR(10),INOT.END_DATE,101),101)= 
			CONVERT(DATETIME,CONVERT(CHAR(10),GETDATE(),101),101)
AND			INOT.DEPARTMENT_TYPE_CODE = DETY.CODE
GROUP BY DETY.CODE
order by DETY.CODE
";

            IList openList = (IList)SmartCadDatabase.SearchBasicObjects(openIncidentNotificationSql, false);
            IList closeList = (IList)SmartCadDatabase.SearchBasicObjects(closeIncidentNotificationSql, false);

            Dictionary<int, double> departmentCodes = new Dictionary<int, double>();
            Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
            List<double> list = new List<double>();
            foreach (object[] item in openList)
            {
                if (departmentCodes.ContainsKey(int.Parse(item[1].ToString())) == false)
                {
                    departmentCodes.Add(int.Parse(item[1].ToString()), int.Parse(item[0].ToString()));
                }

            }
            foreach (object[] item in closeList)
            {

                if (departmentCodes.ContainsKey(int.Parse(item[1].ToString())) == false)
                {
                    departmentCodes.Add(int.Parse(item[1].ToString()), int.Parse(item[0].ToString()));
                }
                else
                {
                    departmentCodes[int.Parse(item[1].ToString())] += int.Parse(item[0].ToString());
                }
            }

            foreach (KeyValuePair<int, double> pair in departmentCodes)
            {

                foreach (object[] item in openList)
                {
                    if (int.Parse(item[1].ToString()) == pair.Key)
                    {
                        ObjectData od1 = new ObjectData();
                        od1.Code = int.Parse(item[1].ToString());
                        list.Add(int.Parse(item[0].ToString()));
                        list.Add((int.Parse(item[0].ToString()) * 100) / pair.Value);
                        result.Add(ResourceLoader.GetString2("IndicatorDPN15Name"), list);
                        SaveIndicatorMultiplesValues(od1, IndicatorClassData.Department, result);
                        result.Clear();
                        list = new List<double>();
                        break;
                    }
                }

                foreach (object[] item in closeList)
                {
                    if (int.Parse(item[1].ToString()) == pair.Key)
                    {
                        ObjectData od1 = new ObjectData();
                        od1.Code = int.Parse(item[1].ToString());
                        list.Add(int.Parse(item[0].ToString()));
                        list.Add((int.Parse(item[0].ToString()) * 100) / pair.Value);
                        result.Add(ResourceLoader.GetString2("IndicatorDPN16Name"), list);
                        SaveIndicatorMultiplesValues(od1, IndicatorClassData.Department, result);
                        result.Clear();
                        list = new List<double>();
                        break;
                    }
                }
            }
            #endregion new
            
        }
    }
}

