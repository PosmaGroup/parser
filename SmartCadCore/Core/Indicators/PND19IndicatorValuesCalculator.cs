//using System;
//using System.Collections.Generic;
//using System.Text;
//using SmartCadCore.Model;
//using System.Collections;

//namespace SmartCadCore.Core.Indicators
//{
//    public class PND19IndicatorValuesCalculator : IndicatorValuesCalculator
//    {
//        protected override IndicatorData GetIndicator()
//        {
//            return SmartCadDatabase.SearchObject<IndicatorData>(SmartCadHqls.GetCustomHql(SmartCadHqls.GetIndicatorByName, ResourceLoader.GetString2("IndicatorPND19Name")));
//        }

//        protected override void Calculate()
//        {
//            CalculateIndicatorResultSystemGroupOperator(false);
//        }

//        protected override double CalculateOperatorValue(int supervisorCode, int operatorCode)
//        {
//            double res = 0;           
//           res =  Convert.ToDouble(SmartCadDatabase.SearchObject(SmartCadHqls.GetCustomHql(
//            SmartCadHqls.LoggedOperatorInApplication,
//                operatorCode, true.ToString().ToLower(), UserApplicationData.FirstLevel.Code)));
//            if (res == 0)
//            {
//                res = 1;
//            }
//            else if (res == 1)
//            {
//                res = 0;
//            }           
//            return res;
//        }
//    }
//}
