using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
//using SmartCadCore.Model;

//namespace SmartCadCore.Core.Indicators
//{
//    public class D47IndicatorValuesCalculator : IndicatorValuesCalculator
//    {
//        protected override string GetIndicator()
//        {
//            return "D47";
//        }

//        protected override void Calculate()
//        {
//            CalculateGroup();
//            CalculateDepartment();
//        }

//        private void CalculateGroup()
//        {
//            IList supervisors = OperatorScheduleManager.GetSupervisorsWorkingNowByApplication("DispatchSupervisorName");

//            foreach (int supervisor in supervisors)
//            {
//                IList operators = OperatorScheduleManager.GetAllDayOperatorsBySupervisor(supervisor);
//                WorkShiftScheduleVariationData bshSupervisor =
//                    OperatorScheduleManager.GetCurrentWorkShiftSchedulesByOperator(supervisor, SmartCadDatabase.GetTimeFromBD().Date);

//                double totalIncidentNotification = 0;
//                double totalAssignedIncidentNotification = 0;
//                double percentAssignedIncidentNotification = 0;
//                foreach (OperatorData operatorData in operators)
//                {

//                    IList  incidentsNotification = SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(
//                        SmartCadHqls.GetIncidentNotificationByOperator,
//                         operatorData.Code));

//                    IList bshList =
//                    OperatorScheduleManager.GetAllDayOperatorAssignBySupervisorAndByDayRealHours(supervisor, operatorData.Code);
//                    bool incidentAdded = false;

//                    foreach (IncidentNotificationData incidentnotification in incidentsNotification)
//                    {
//                        if (incidentnotification.Status == IncidentNotificationStatusData.Assigned)
//                        {
//                            totalAssignedIncidentNotification += 1;
//                        }
//                        else if (incidentnotification.Status == IncidentNotificationStatusData.Closed ||
//                            incidentnotification.Status == IncidentNotificationStatusData.Cancelled)
//                        {
//                            foreach (OperatorAssignData bsh in bshList)
//                            {
//                                if ((bshSupervisor != null) &&
//                             (((bsh.StartDate.TimeOfDay >= bshSupervisor.Start.Value.TimeOfDay) &&
//                            (bsh.StartDate.TimeOfDay <= bshSupervisor.End.Value.TimeOfDay)) ||
//                            ((bsh.EndDate.TimeOfDay <= bshSupervisor.End.Value.TimeOfDay) &&
//                            (bsh.EndDate.TimeOfDay >= bshSupervisor.Start.Value.TimeOfDay))))
//                                {
//                                    if ((bsh.StartDate <= incidentnotification.EndDate) &&
//                                        (bsh.EndDate >= incidentnotification.EndDate))
//                                    {
//                                        //totalIncidentNotification -= 1;
//                                        incidentAdded = true;
//                                    }
//                                }

//                            }
//                            if (incidentAdded == false)
//                            {
//                                totalIncidentNotification -= 1;
//                            }
//                            incidentAdded = false;
//                        }
//                    }                    
//                    totalIncidentNotification += incidentsNotification.Count;                    
//                }

//                if (totalIncidentNotification > 0)
//                {
//                    percentAssignedIncidentNotification = (totalAssignedIncidentNotification * 100) / (totalIncidentNotification);
//                }
//                else
//                {
//                    percentAssignedIncidentNotification = 0;
//                }

//                List<double> resultsValues = new List<double>();
//                resultsValues.Add(totalAssignedIncidentNotification);
//                resultsValues.Add(percentAssignedIncidentNotification);
//                Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
//                result.Add("AssignedIncidents", resultsValues);
//                ObjectData objectdata = new ObjectData();
//                objectdata.Code = supervisor;
//                SaveIndicatorMultiplesValues(objectdata, IndicatorClassData.Group, result);
//            }
//        }

//        ArrayList departmentsAllZeroValue = new ArrayList() { 1, 2, 3, 4, 5, 6, 7, 8 };
//        private void CalculateDepartment()
//        {
//            foreach (int item in departmentsAllZeroValue)
//            {
//                Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
//                List<double> resultsValues = new List<double>();
//                resultsValues.Add(0);
//                resultsValues.Add(0);
//                result.Add("Empty", resultsValues);
//                ObjectData od = new ObjectData();
//                od.Code = item;
//                SaveIndicatorMultiplesValues(od, IndicatorClassData.Department, result);
//            }

//            IList IncidentNotificationByDepartment = (IList)SmartCadDatabase.SearchObjects(SmartCadHqls.GetCustomHql(
//                   SmartCadHqls.GetIncidentNotificationCountByDepartmentByStatus,
//                   IncidentNotificationStatusData.Assigned.Code));


//            double percentOpenIncidents = 0;

//            for (int i = 0; i < IncidentNotificationByDepartment.Count; i++)
//            {
//                Dictionary<string, List<double>> result = new Dictionary<string, List<double>>();
//                List<double> resultsValues = new List<double>();

//                int currentDTCode = (int)((object[])IncidentNotificationByDepartment[i])[0];
//                object[] item = (object[])IncidentNotificationByDepartment[i];

//                double totalIncidents = (long)SmartCadDatabase.SearchBasicObject(SmartCadHqls.GetCustomHql(
//                       SmartCadHqls.GetAllIncidentNotificationByDepartment, currentDTCode,
//                       ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today)));

//                if (totalIncidents > 0)
//                {
//                    percentOpenIncidents = (Convert.ToDouble(item[1]) * 100) / (totalIncidents);
//                }
//                else
//                {
//                    percentOpenIncidents = 0;
//                }

//                resultsValues.Add(Convert.ToDouble(item[1]));
//                resultsValues.Add(percentOpenIncidents);
//                result.Add("AssignedIncidentsByDepartment", resultsValues);
//                ObjectData od = new ObjectData();
//                od.Code = currentDTCode;
//                SaveIndicatorMultiplesValues(od, IndicatorClassData.Department, result);
//            }
//        }


     
//    }
//}
