﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.Model;
using SmartCadCore.Statistic;
using SmartCadCore.Core.Util;

namespace SmartCadCore.Core.Indicators
{
    public class L12IndicatorValuesCalculator : IndicatorValuesCalculator
    {
        protected override string GetIndicator()
        {
            return "L12";
        }

        protected override void Calculate()
        {
            try
            {
                if (TelephonyServerStatisticElement.CurrentServerType == TelephonyServerTypes.Genesys)
                {
                    double value = GenesysStatWrapper.GetData(StatisticRetriever.StatisticNames.L12ServiceLevel);                        
                    SaveIndicatorsValues(IndicatorClassData.System, value);
                }
                else if (TelephonyServerStatisticElement.CurrentServerType == TelephonyServerTypes.Nortel)
                {
                    try
                    {
                        SaveIndicatorsValues(IndicatorClassData.System, NortelRTDWrapper.GetData(0));
                    }
                    catch (Exception ex)
                    {
                    }

                    /*SybaseConnection con = new SybaseConnection(SmartCadConfiguration.SmartCadSection.ServerElement.TelephonyServerElement.ConnectionString);
                    con.Open();
                    DateTime dt = DateTime.Today;
                    string sql = @"select SUM(CallsAnswered + CallsAbandoned - CallsAnsweredAftThreshold - CallsAbandonedAftThreshold) 
                                            / SUM(CallsAnswered + CallsAbandoned)  
                                   from iApplicationStat 
                                   where year(Timestamp) = " + dt.ToString("yyyy") + 
                                   " and month(Timestamp) = " + dt.ToString("MM") + 
                                   " and day(Timestamp) = " + dt.ToString("dd") + 
                                   " and ApplicationID = 1";
                    SybaseCommand com = new SybaseCommand(sql, con);

                    try
                    {
                        SybaseDataReader reader = com.ExecuteReader();
                        while (reader.Read())
                        {
                            double serviceLevel = reader.GetInt32(0);
                            SaveIndicatorsValues(IndicatorClassData.System, serviceLevel);
                        }
                        reader.Close();
                    }
                    catch (Exception ex)
                    {
                    }

                    com.Dispose();
                    con.Close();
                    con.Dispose();*/
                }
                else if (TelephonyServerStatisticElement.CurrentServerType == TelephonyServerTypes.Avaya)
                {
                    
                    SaveIndicatorsValues(IndicatorClassData.System,
                        AvayaStatWrapper.GetData(StatisticRetriever.StatisticNames.L12ServiceLevel));
                }
                else if (TelephonyServerStatisticElement.CurrentServerType == TelephonyServerTypes.Asterisk)
                {
                    AsteriskStatWrapper AsteriskWrapper = new AsteriskStatWrapper();
                    SaveIndicatorsValues(IndicatorClassData.System,
                        AsteriskStatWrapper.GetData(StatisticRetriever.StatisticNames.L12ServiceLevel));
                }
            }
            catch (Exception ex)
            {
            }            
        }
    }
}
