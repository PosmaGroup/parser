using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using SmartCadCore.Model;

namespace SmartCadCore.Core.Indicators
{
    public class D32IndicatorValuesCalculator : IndicatorValuesCalculator
    {
        protected override string GetIndicator()
        {
            return "D32";
        }

        protected override void Calculate()
        {
            CalculateOperators();
        }

        private void CalculateOperators()
        {

            #region new
            string sql = @"SELECT		CONVERT(FLOAT,SUM(PARCIAL.TIEMPO))/COUNT(PARCIAL.TIEMPO) AS TIEMPO, PARCIAL.OPERADOR AS OPERADOR
FROM		(SELECT	DATEDIFF(SS,MIN(INSH.START_DATE),MIN(INSH1.START_DATE)) AS TIEMPO
					,OPE.CODE AS OPERADOR
			FROM	INCIDENT_NOTIFICATION_STATUS_HISTORY AS INSH
					,INCIDENT_NOTIFICATION_STATUS_HISTORY AS INSH1
					,OPERATOR AS OPE
			WHERE	INSH.INCIDENT_NOTIFICATION_STATUS_CODE = 3
			AND		INSH1.INCIDENT_NOTIFICATION_CODE = INSH.INCIDENT_NOTIFICATION_CODE
			AND		INSH1.INCIDENT_NOTIFICATION_STATUS_CODE = 8
			AND		INSH1.USER_ACCOUNT_CODE = INSH.USER_ACCOUNT_CODE
			AND		OPE.CODE = INSH.USER_ACCOUNT_CODE
			GROUP BY INSH.START_DATE,OPE.CODE) AS PARCIAL
GROUP BY	PARCIAL.OPERADOR
";

            IList data = (IList)SmartCadDatabase.SearchBasicObjects(sql, false);
            foreach (object[] item in data)
            {
                ObjectData objectData = new ObjectData();
                objectData.Code = int.Parse(item[1].ToString());
                SaveIndicatorsValues(objectData, IndicatorClassData.Operator, double.Parse(item[0].ToString()));
            }           
            #endregion new

            #region doc

            //IList operators = OperatorScheduleManager.GetOperatorsWorkingNowByApplication(UserApplicationData.Dispatch.Name, false);
            //IList IncidentNotificationList = SmartCadDatabase.SearchObjects(
            //       SmartCadHqls.GetCustomHql(
            //       SmartCadHqls.GetAllIncidentNotificationCountByDay,
            //       ApplicationUtil.GetDataBaseFormattedDate(DateTime.Now.Date)
            //       ));
            //foreach (int operatorCode in operators)
            //{
            //    double result = 0;
            //    int count = 0;
            //    int incident = 0;
            //    for (int i = IncidentNotificationList.Count - 1; i > 0; i--)
            //    {
            //        incident = (int)IncidentNotificationList[i];
            //        object NotificationAndDateFirstDispatch = SmartCadDatabase.SearchBasicObject(
            //           SmartCadHqls.GetCustomHql(
            //           SmartCadHqls.GetDateNotificationAndDateFirstDispatch,
            //           incident, 
            //           operatorCode
            //           ));
                   
            //        if ((NotificationAndDateFirstDispatch as object[])[0] != null)
            //        {
            //            IncidentNotificationList.RemoveAt(i);
            //            DateTime start = (DateTime)(NotificationAndDateFirstDispatch as object[])[0];
            //            DateTime end = (DateTime)(NotificationAndDateFirstDispatch as object[])[1];
            //            TimeSpan subtract = end.Subtract(start);

            //            result += subtract.TotalSeconds;
            //            count++;
            //        }
            //    }

            //    if (count > 0)
            //    {
            //        result /= count;
            //    }

            //    ObjectData objectData = new ObjectData();
            //    objectData.Code = operatorCode;
            //    SaveIndicatorsValues(objectData, IndicatorClassData.Operator, result);
            //}
            #endregion doc
        }
    }
}
