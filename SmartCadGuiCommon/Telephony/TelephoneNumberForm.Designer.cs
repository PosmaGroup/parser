using SmartCadControls.Controls;

namespace SmartCadGuiCommon
{
    partial class TelephoneNumberForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelExTelephoneNumber = new LabelEx();
            this.textBoxExTelephoneNumber = new TextBoxEx();
            this.buttonExAccept = new SimpleButtonEx();
            this.buttonExCancel = new SimpleButtonEx();
            this.SuspendLayout();
            // 
            // labelExTelephoneNumber
            // 
            this.labelExTelephoneNumber.AutoSize = true;
            this.labelExTelephoneNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExTelephoneNumber.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExTelephoneNumber.Location = new System.Drawing.Point(8, 12);
            this.labelExTelephoneNumber.Name = "labelExTelephoneNumber";
            this.labelExTelephoneNumber.Size = new System.Drawing.Size(76, 13);
            this.labelExTelephoneNumber.TabIndex = 0;
            this.labelExTelephoneNumber.Text = "Phonenumber:";
            // 
            // textBoxExTelephoneNumber
            // 
            this.textBoxExTelephoneNumber.AllowsLetters = false;
            this.textBoxExTelephoneNumber.AllowsNumbers = true;
            this.textBoxExTelephoneNumber.AllowsPunctuation = false;
            this.textBoxExTelephoneNumber.AllowsSeparators = false;
            this.textBoxExTelephoneNumber.AllowsSymbols = false;
            this.textBoxExTelephoneNumber.AllowsWhiteSpaces = false;
            this.textBoxExTelephoneNumber.ExtraAllowedChars = "/-()xX";
            this.textBoxExTelephoneNumber.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExTelephoneNumber.Location = new System.Drawing.Point(108, 8);
            this.textBoxExTelephoneNumber.MaxLength = 15;
            this.textBoxExTelephoneNumber.Name = "textBoxExTelephoneNumber";
            this.textBoxExTelephoneNumber.NonAllowedCharacters = "";
            this.textBoxExTelephoneNumber.RegularExpresion = "";
            this.textBoxExTelephoneNumber.Size = new System.Drawing.Size(160, 20);
            this.textBoxExTelephoneNumber.TabIndex = 1;
            this.textBoxExTelephoneNumber.TextChanged += new System.EventHandler(this.textBoxExTelephoneNumber_TextChanged);
            // 
            // buttonExAccept
            // 
            this.buttonExAccept.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExAccept.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonExAccept.Appearance.Options.UseFont = true;
            this.buttonExAccept.Appearance.Options.UseForeColor = true;
            this.buttonExAccept.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonExAccept.Enabled = false;
            this.buttonExAccept.FontFormat = FontFormat.GetFormat("ButtonFormat");
            this.buttonExAccept.Location = new System.Drawing.Point(114, 40);
            this.buttonExAccept.Name = "buttonExAccept";
            this.buttonExAccept.Size = new System.Drawing.Size(75, 23);
            this.buttonExAccept.TabIndex = 2;
            this.buttonExAccept.Text = "Accept";
            // 
            // buttonExCancel
            // 
            this.buttonExCancel.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExCancel.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonExCancel.Appearance.Options.UseFont = true;
            this.buttonExCancel.Appearance.Options.UseForeColor = true;
            this.buttonExCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonExCancel.FontFormat = FontFormat.GetFormat("ButtonFormat");
            this.buttonExCancel.Location = new System.Drawing.Point(194, 40);
            this.buttonExCancel.Name = "buttonExCancel";
            this.buttonExCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonExCancel.TabIndex = 3;
            this.buttonExCancel.Text = "Cancel";
            // 
            // TelephoneNumberForm
            // 
            this.AcceptButton = this.buttonExAccept;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonExCancel;
            this.ClientSize = new System.Drawing.Size(282, 78);
            this.Controls.Add(this.buttonExCancel);
            this.Controls.Add(this.buttonExAccept);
            this.Controls.Add(this.textBoxExTelephoneNumber);
            this.Controls.Add(this.labelExTelephoneNumber);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TelephoneNumberForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TelephoneNumberForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private LabelEx labelExTelephoneNumber;
        private TextBoxEx textBoxExTelephoneNumber;
        private SimpleButtonEx buttonExAccept;
        private SimpleButtonEx buttonExCancel;
    }
}