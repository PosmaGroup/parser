using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using SmartCadCore.Core;
using System.Net.Sockets;
using System.Net;
using System.Threading;

namespace SmartCadGuiCommon
{
    public class TelephonyConsoleManager
    {
        private Process proc;
        private bool stopping;

        public void Run()
        {
            stopping = false;
            proc = new Process();
            proc.StartInfo = new ProcessStartInfo(SmartCadConfiguration.TelephonyConsoleFilename, Process.GetCurrentProcess().Id.ToString());
            //if (SmartCadConfiguration.SmartCadSection.ClientElement.TelephonyElement.ShowConsole == false)
            //{
                proc.StartInfo.CreateNoWindow = true;
                proc.StartInfo.UseShellExecute = false;
            //}
            proc.EnableRaisingEvents = true;
            proc.Exited += new EventHandler(proc_Exited);
            proc.Start();
        }

        public bool WaitForService(int seconds)
        {
            bool serviceUp = false;
            int n = 0;

            while (n < seconds && !serviceUp)
            {
                try
                {
                    TcpClient tcpClient = new TcpClient();
                    tcpClient.Connect("localhost", SmartCadConfiguration.TELEPHONY_SERVICE_PORT);
                    tcpClient.Close();
                    serviceUp = true;
                }
                catch
                {
                    Thread.Sleep(1000);
                }
                n++;
            }

            return serviceUp;
        }

        public void Stop()
        {
            stopping = true;
            if (proc != null && proc.HasExited == false)
                proc.Kill();

            proc.WaitForExit();
        }

        private void proc_Exited(object sender, EventArgs e)
        {
            // El proceso se cayo y hay que reiniciarlo.
            if (!stopping)
            {
                //(sender as Process).Start();
            }
        }
    }
}
