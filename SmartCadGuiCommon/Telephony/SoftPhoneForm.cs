﻿using DevExpress.XtraEditors;
using SmartCadControls.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SmartCadGuiCommon
{
    public partial class SoftPhoneForm : XtraForm
    {
        public SoftPhoneControl SoftPhone { get; set; }

        public bool ActiveCall { get { return SoftPhone.ActiveCall; } set { SoftPhone.ActiveCall = value; } }

        public SoftPhoneForm()
        {
            InitializeComponent();
            SoftPhone = new SoftPhoneControl();
            this.Controls.Add(SoftPhone);
        }

        public void Initialize(string caption, string parentCustomCode, string phoneToCall = "")
        {
            SoftPhone.Initialize(parentCustomCode, phoneToCall);
            this.Text = caption;
        }

        private void SoftPhoneForm_FormClosing(object sender, FormClosingEventArgs e)
        {
                if (e.CloseReason == CloseReason.UserClosing)
                {
                    this.Hide();
                    e.Cancel = true;
                }
                else SoftPhone.Close();         
        }
    }
}
