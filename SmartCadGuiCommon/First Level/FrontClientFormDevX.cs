using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Smartmatic.SmartCad.Service;
using System.Net;


using System.Collections;
using System.Threading;
using DevExpress.XtraBars;
using System.Reflection;
using DevExpress.XtraEditors.Repository;
using SmartCadCore.Core;
using SmartCadGuiCommon;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadCore.Enums;

namespace SmartCadGuiCommon
{
	public partial class FrontClientFormDevX : XtraForm
	{
		#region Fields

		DefaultFrontClientFormDevX defaultFrontClientFormDevX;
        private IncidentsHistoryForm incidentsHistoryForm;
		System.Threading.Timer chatTimer;
		private OperatorChatForm chatForm;
		private NetworkCredential networkCredential;
		public string ExtNumber;
        public event EventHandler<CommittedObjectDataCollectionEventArgs> FrontClientFormDevXCommittedChanges;
        private bool loaded = false;

        private DateTime syncServerDateTime;
        private TimeSpan sinceLoggedIn;
        private const int GLOBAL_TIMER_PERIOD = 10000;
        private readonly TimeSpan ADD_TIMESPAN;
        public System.Threading.Timer globalTimer;
        private string labelUserCaption = "";
        private string labelConnectedCaption = "";

        ServerServiceClient serverServiceClient = null;
		#endregion

        public event EventHandler PostFrontClientFormDevXLoad;

		#region Properties

        /// <summary>
        /// When telephony throws any error, this variable is set to close the application.
        /// </summary>
        public bool DoNotEnsureLogout { get; set; }

		public ServerServiceClient ServerServiceClient
		{
			set
			{
				defaultFrontClientFormDevX.ServerServiceClient = value;
			}
		}
		public System.Threading.Timer TimerCheckServer
		{
			set
			{
				defaultFrontClientFormDevX.TimerCheckServer = value;
			}
		}

		public NetworkCredential NetworkCredential
		{
			get
			{
				return networkCredential;
			}

			set
			{
				networkCredential = value;
			}
		}

		public ConfigurationClientData ConfigurationClient
		{
			set
			{
				defaultFrontClientFormDevX.ConfigurationClient = value;
			}
		}

		public IList GlobalIncidents
		{
			set
			{
				defaultFrontClientFormDevX.GlobalIncidents = value;
			}
		}

		public Dictionary<int, IncidentTypeClientData> GlobalIncidentTypes
		{
			set
			{
				defaultFrontClientFormDevX.GlobalIncidentTypes = value;
			}
		}

		public Dictionary<int, DepartmentTypeClientData> GlobalDepartmentTypes
		{
			set
			{
				defaultFrontClientFormDevX.GlobalDepartmentTypes = value;
			}
		}

		public Dictionary<string, IncidentNotificationPriorityClientData> GlobalNotificationPriorities
		{
			set
			{
				defaultFrontClientFormDevX.GlobalNotificationPriorities = value;
			}
		}

		public Dictionary<string, ApplicationPreferenceClientData> GlobalApplicationPreference
		{
			set
			{
				defaultFrontClientFormDevX.GlobalApplicationPreference = value;
			}
		}


        public bool RecordCalls { get; set; }

		#endregion

		public FrontClientFormDevX()
		{
            var applications = ServerServiceClient.GetInstance().GetActiveApplications();
			InitializeComponent();
            if(!applications.Contains(SmartCadApplications.SmartCadMap))
            {
                ribbonPageGroupApplications.Visible = false;
            }
            if (!applications.Contains(SmartCadApplications.SmartCadSupervision))
            {
                barButtonItemChat.Visibility = BarItemVisibility.Never;
            }
            

            defaultFrontClientFormDevX = new DefaultFrontClientFormDevX(this);
			defaultFrontClientFormDevX.MdiParent = this;
			LoadLanguage();
			ServerServiceClient.GetInstance().CommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(FrontClientFormDevX_CommittedChanges);
            this.ribbonControl1.MinimizedChanged += new EventHandler(RibbonControl_MinimizedChanged);
            ADD_TIMESPAN = new TimeSpan(0, 0, 0, 0, GLOBAL_TIMER_PERIOD);
            this.xtraTabbedMdiManager1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.xtraTabbedMdiManager1.BorderStylePage = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.xtraTabbedMdiManager1.ClosePageButtonShowMode = DevExpress.XtraTab.ClosePageButtonShowMode.InActiveTabPageHeader;
            this.xtraTabbedMdiManager1.HeaderButtons = ((DevExpress.XtraTab.TabButtons)(((DevExpress.XtraTab.TabButtons.Prev | DevExpress.XtraTab.TabButtons.Next)
                        | DevExpress.XtraTab.TabButtons.Default)));
            this.xtraTabbedMdiManager1.HeaderButtonsShowMode = DevExpress.XtraTab.TabButtonShowMode.WhenNeeded;
            this.xtraTabbedMdiManager1.MdiParent = this;
            this.xtraTabbedMdiManager1.SetNextMdiChildMode = DevExpress.XtraTabbedMdi.SetNextMdiChildMode.TabControl;

            
            

		}

        private void FillStatusStrip()
        {
            serverServiceClient = ServerServiceClient.GetInstance();
            syncServerDateTime = serverServiceClient.GetTime();
            sinceLoggedIn = TimeSpan.Zero;
            this.labelConnected.Caption = "00:00";

            labelDate.Caption = ApplicationUtil.GetFormattedDate(serverServiceClient.GetTime());
            globalTimer = ApplicationUtil.RecurrentExecuteMethod(
                new SimpleDelegate(
                delegate
                {
                    globalTimer_Tick(null, null);
                }), null, null, GLOBAL_TIMER_PERIOD, GLOBAL_TIMER_PERIOD);

            labelUser.Caption = labelUserCaption + serverServiceClient.OperatorClient.FirstName
                + " " + serverServiceClient.OperatorClient.LastName;
            labelConnected.Caption = labelConnectedCaption + "00:00";
        }

        private void globalTimer_Tick(object sender, EventArgs e)
        {
            syncServerDateTime = syncServerDateTime.AddMilliseconds((double)GLOBAL_TIMER_PERIOD);
            sinceLoggedIn = sinceLoggedIn.Add(ADD_TIMESPAN);
            FormUtil.InvokeRequired(ribbonStatusBar1, delegate
            {

                ribbonStatusBar1.Invalidate(true);
                labelDate.Caption = ApplicationUtil.GetFormattedDate(serverServiceClient.GetTime());
                labelConnected.Caption = labelConnectedCaption + Math.Truncate(sinceLoggedIn.TotalHours).ToString().PadLeft(2, '0') + ":" + sinceLoggedIn.Minutes.ToString().PadLeft(2, '0');
                ribbonStatusBar1.ResumeLayout(true);
                ribbonStatusBar1.LayoutChanged();
            });
        }

        void RibbonControl_MinimizedChanged(object sender, EventArgs e)
        {
            if (this.ribbonControl1.Minimized == true)
            {
                this.ribbonControl1.Minimized = false;
            }
        }


		private void LoadLanguage()
		{
			barButtonItemAddToIncident.Caption = ResourceLoader.GetString2("AddToIncident");
			barButtonItemChat.Caption = ResourceLoader.GetString2("Chat");
			barButtonItemFinalize.Caption = ResourceLoader.GetString2("Finalize");
			barButtonItemOpenMap.Caption = ResourceLoader.GetString2("Open");
			barButtonItemPhoneReboot.Caption = ResourceLoader.GetString2("PhoneReinit");
			barButtonItemPrint.Caption = ResourceLoader.GetString2("Print");
			barButtonItemRefresh.Caption = ResourceLoader.GetString2("Refresh");
			barButtonItemSave.Caption = ResourceLoader.GetString2("Save");
			barButtonItemVirtualCall.Caption = ResourceLoader.GetString2("VirtualCall");
			ribbonPageGroupUser.Text = ResourceLoader.GetString2("Status");
			ribbonPageGroupGeneralOptions.Text = ResourceLoader.GetString2("GeneralOptions");
			ribbonPageGroupTool.Text = ResourceLoader.GetString2("Tools");
			ribbonPageGroupApplications.Text = ResourceLoader.GetString2("Maps");
			ribbonPageGroupCallInfo.Text = ResourceLoader.GetString2("CallInfo");
            ribbonPage1.Text = ResourceLoader.GetString2("FirstLevel");
            this.Text = ResourceLoader.GetString2("FrontClientFormDevX");
            this.Icon = ResourceLoader.GetIcon("FirstLevelIcon");
			this.barStaticItemChonometer.Caption = ResourceLoader.GetString2("Chronometer");
			this.barStaticItemTimeBar.Caption = ResourceLoader.GetString2("TimeBar");
			this.ribbonPageGroupTelephone.Text = ResourceLoader.GetString2("Phone");
            this.barButtonItemHistory.Caption = ResourceLoader.GetString2("IncidentsHistoryForm");
            this.barButtonItemCallBack.Caption = ResourceLoader.GetString2("CallBack");

            #region Footer
            labelUserCaption = ResourceLoader.GetString2("User") + ": ";
            labelConnectedCaption = ResourceLoader.GetString2("Connected") + ": ";
            #endregion
		}
		
		void FrontClientFormDevX_CommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
		{
			try
			{
				if (e != null && e.Objects != null && e.Objects.Count > 0)
				{
                    if (FrontClientFormDevXCommittedChanges != null)
                    {
                        FrontClientFormDevXCommittedChanges(null, e);
                    }
					if (e.Objects[0] is ApplicationMessageClientData)
					{
                        if (loaded)
                        {
                            ApplicationMessageClientData amcd = ((ApplicationMessageClientData)e.Objects[0]);
                            if (amcd.ToOperators.Contains(ServerServiceClient.GetInstance().OperatorClient))
                            {
                                if (chatForm != null && chatForm.IsDisposed == false)
                                {
                                    FormUtil.InvokeRequired(this, delegate
                                    {
                                        chatForm.OpenNewTab(amcd.Operator, amcd);
                                        chatForm.TopMost = false;
                                        FlashingWindow.FlashWindowEx(chatForm);
                                    });
                                }
                                else
                                {
                                    FormUtil.InvokeRequired(this, delegate
                                    {
                                        alertControl1.Show(this, ResourceLoader.GetString2("MessageFrom") + ": " + amcd.Operator.Login, amcd.Message);
                                        chatForm = new OperatorChatForm(UserApplicationClientData.FirstLevel.Name, ChatOperatorType.Operator);
                                        chatForm.OpenNewTab(amcd.Operator, amcd);
                                        chatForm.WindowState = FormWindowState.Minimized;
                                        chatForm.Show();
                                        FlashingWindow.FlashWindowEx(chatForm);
                                    });
                                }
                            }
                        }
					}
					else if (e.Objects[0] is ApplicationPreferenceClientData)
					{
						ApplicationPreferenceClientData preference = e.Objects[0] as ApplicationPreferenceClientData;
						if (e.Action == CommittedDataAction.Update)
						{
							if (preference.Name.Equals("AlarmTimeLimit"))
							{
								FormUtil.InvokeRequired(this,
									delegate
									{
										repositoryItemProgressBar1.Maximum = int.Parse(preference.Value);
									});
							}
						}
					}
				}
			}
			catch
			{
			}
		}

		public void SetPassword(string p)
		{
			defaultFrontClientFormDevX.SetPassword(p);
            ServerServiceClient.GetInstance().OperatorClient.Password = p;
		}

		private void FrontClientFormDevX_Load(object sender, EventArgs e)
		{            
            this.Location = new Point(0, 0);
            this.Width = Screen.PrimaryScreen.WorkingArea.Width;
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
			CheckAccessToMap();
			xtraTabbedMdiManager1.Pages[defaultFrontClientFormDevX].ShowCloseButton = DevExpress.Utils.DefaultBoolean.False;
			defaultFrontClientFormDevX.Show();
			barButtonItemOpenMap.Glyph = ResourceLoader.GetImage("$Image.OpenMap");
			this.barButtonItemChat.Caption = ResourceLoader.GetString2("Chat");
			defaultFrontClientFormDevX.Reset();
            if (PostFrontClientFormDevXLoad != null)
                PostFrontClientFormDevXLoad(null, null);

            ////TelephonyService = TelephonyServiceClient.GetServerService();
            if (TelephonyServiceClient.GetInstance() != null)
            {
                if (ServerServiceClient.GetInstance().CheckVirtualCall() && true/*TelephonyServiceClient.GetInstance().IsVirtual()*/)
                {
                    ribbonPageGroupTelephone.Visible = true;
                    barButtonItemVirtualCall.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
                }
                else
                {
                    ribbonPageGroupTelephone.Visible = false;
                    barButtonItemVirtualCall.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                }
            }
            else
                barButtonItemVirtualCall.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
			barCheckItemIncompleteCall.Caption = ResourceLoader.GetString2("UnfinishedCall");
            loaded = true;


            FillStatusStrip();
		}

		private void FrontClientFormDevX_FormClosing(object sender, FormClosingEventArgs e)
		{
			//check if a call has entered or if an incident is being typed
			if (this.defaultFrontClientFormDevX.FrontClientState != FrontClientStateEnum.WaitingForIncident)
				e.Cancel = true;
			else if (DoNotEnsureLogout || EnsureLogout())
			{
                try
                {
                    TelephonyServiceClient.GetInstance().TelephonyAction -= new EventHandler<TelephonyActionEventArgs>(defaultFrontClientFormDevX.FrontClientForm_TelephonyAction);
                    //TelephonyServiceClient.GetInstance().Dispose();
                }
                catch { }

				if (defaultFrontClientFormDevX.TimerCheckServer != null)
					defaultFrontClientFormDevX.TimerCheckServer.Dispose();
				if (defaultFrontClientFormDevX.globalTimer != null)
					defaultFrontClientFormDevX.globalTimer.Dispose();
                if (globalTimer != null)
                    globalTimer.Dispose();
				
				defaultFrontClientFormDevX.callInformationControl.Enter -= new EventHandler(defaultFrontClientFormDevX.callReceiverControl1_Enter);
				defaultFrontClientFormDevX.ServerServiceClient.CommittedChanges -= new EventHandler<CommittedObjectDataCollectionEventArgs>(defaultFrontClientFormDevX.serverService_CommittedChanges);
				}
			else
				e.Cancel = true;
		}

		private void FrontClientFormDevX_FormClosed(object sender, FormClosedEventArgs e)
		{
			ServerServiceClient.GetInstance().CloseSession();
		}

		private bool EnsureLogout()
		{
			DialogResult dialogResult = MessageForm.Show(ResourceLoader.GetString2("$Message.ExitApplication"), MessageFormType.Question);

			return dialogResult == DialogResult.Yes;
		}

		private void CheckAccessToMap()
		{
			if (ServerServiceClient.GetInstance().CheckAccessClient(UserResourceClientData.Application,
				UserActionClientData.Basic, UserApplicationClientData.Map, false) == true)
				barButtonItemOpenMap.Enabled = true;
			else
				barButtonItemOpenMap.Enabled = false;
		}

		private void barButtonItemOpenMap_Click(object sender, EventArgs e)
		{
			try
			{
				ApplicationUtil.LaunchApplicationWithArguments(
						SmartCadConfiguration.SmartCadMapFilename,
						new object[] { ServerServiceClient.GetInstance().OperatorClient.Login, 
                            defaultFrontClientFormDevX.password, 
                            ApplicationUtil.CheckPrimaryScreenBounds(this.Left),
                            new SendActivateLayerEventArgs(ShapeType.Unit),
                            defaultFrontClientFormDevX.FrontClientState == FrontClientStateEnum.RegisteringIncident ? 
                                new SendEnableButtonEventArgs(ButtonType.AddPoint, true):
                                new SendEnableButtonEventArgs(ButtonType.AddPoint, false),
                            new SendActivateLayerEventArgs(ShapeType.Incident)});
			}
			catch (Exception ex)
			{
                SmartLogger.Print(ex);
                MessageForm.Show(ex.Message, ex);
			}
		}

		private void alertControl1_AlertClick(object sender, DevExpress.XtraBars.Alerter.AlertClickEventArgs e)
		{
			if (chatForm == null || chatForm.IsDisposed == true)
			{
				chatForm = new OperatorChatForm(UserApplicationClientData.FirstLevel.Name, ChatOperatorType.Operator);
			}
			chatForm.WindowState = FormWindowState.Normal;
			chatForm.Activate();
			chatForm.Show();
			chatForm.BringToFront();
		}        

		private void barButtonItemChat_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
            if (chatForm == null || chatForm.IsDisposed == true)
                chatForm = new OperatorChatForm(UserApplicationClientData.FirstLevel.Name, ChatOperatorType.Operator);

            chatForm.Activate();
			chatForm.Show();
		}

		private void barButtonItemExit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
			this.Close();
		}

		private void barButtonItemHelp_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
			try
            {
                ApplicationUtil.LaunchHelp(SmartCadConfiguration.DistFolder + "/Help/FIRST LEVEL Module.chm");
            }
            catch (Exception ex)
            {
                MessageForm.Show(ResourceLoader.GetString2("$Message.HelpNotInstalled"), ex);
            }
        }

		private void barButtonItemOpenMap_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
			try
            {
                ApplicationUtil.LaunchApplicationWithArguments(
                        SmartCadConfiguration.SmartCadMapFilename,
                        new object[] { ServerServiceClient.GetInstance().OperatorClient.Login, 
                            defaultFrontClientFormDevX.password, 
                            ApplicationUtil.CheckPrimaryScreenBounds(this.Left),
                            defaultFrontClientFormDevX.FrontClientState == FrontClientStateEnum.RegisteringIncident ? 
                                new SendEnableButtonEventArgs(ButtonType.AddPoint, true):
                                new SendEnableButtonEventArgs(ButtonType.AddPoint, false),
                            new SendActivateLayerEventArgs(ShapeType.Incident)});
            }
            catch (Exception ex)
            {
                MessageForm.Show(ex.Message, ex);
            }
		}
		
		private void barButtonItemVirtualCall_ItemClick(object sender, ItemClickEventArgs e)
		{
			TelephoneNumberForm telephoneNumberForm = new TelephoneNumberForm();
            TelephonyServiceClient.GetInstance().SetVirtualCall(true);
            DialogResult result = telephoneNumberForm.ShowDialog();
            if (result == DialogResult.OK)
            {
                TelephonyServiceClient.GetInstance().GenerateCall(telephoneNumberForm.TelephoneNumber);
            }
            else if (result == DialogResult.Cancel)
            {
                TelephonyServiceClient.GetInstance().SetVirtualCall(false);
            }
		}

		private void barButtonItemPhoneReboot_ItemClick(object sender, ItemClickEventArgs e)
		{
			try
            {
                BackgroundProcessForm processForm = new BackgroundProcessForm(
                    ResourceLoader.GetString2("RestartPhone"), this, new MethodInfo[] { this.GetType().GetMethod("ReInitTelephone",
                        BindingFlags.NonPublic | BindingFlags.Instance) },
                        new object[][] { new object[] { } });
                processForm.CanThrowError = false;
                processForm.ShowDialog();
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
		}

        public void RecoverConnection()
        {
            TelephonyServiceClient.GetInstance().RecoverConnection();
        }

		private void ReInitTelephone()
		{
            //NO SIRVEEEEEEEEE. COMMENTED ON VERSION 3.2.8 bataan
            //FormUtil.InvokeRequired(this, delegate
            //{
            //    defaultFrontClientFormDevX.doNotDisturbMessageShow = true;


            //    if (defaultFrontClientFormDevX.TelephoneStatus == TelephoneStatusType.Incoming)
            //        defaultFrontClientFormDevX.TelephoneStatus = TelephoneStatusType.None;

            //    TelephonyServiceClient.GetInstance().TelephonyAction -= new EventHandler<TelephonyActionEventArgs>(defaultFrontClientFormDevX.FrontClientForm_TelephonyAction);
            //    TelephonyServiceClient.GetInstance().Logout();
            //    Thread.Sleep(100);
            //    TelephonyServiceClient.GetInstance().Close();
            //    TelephonyServiceClient.GetInstance().Abort();
            //    telephonyConsoleManager.Stop();

            //    telephonyConsoleManager = new TelephonyConsoleManager();
            //    telephonyConsoleManager.Run();
            //    telephonyConsoleManager.WaitForService(3);

            //    //FormUtil.InvokeRequired(this, delegate
            //    //{
            //    //    this.Focus();
            //    //    Win32.SetForegroundWindow(this.Handle);
            //    //});


            //    ApplicationPreferenceClientData recordCalls = (ApplicationPreferenceClientData)ServerServiceClient.GetInstance().SearchClientObject(SmartCadHqls.GetCustomHql(SmartCadHqls.GetApplicationPreferenceByName, "RecordCalls"));

            //    TelephonyServiceClient.GetServerService();
            //    TelephonyServiceClient.GetInstance().SetConfiguration(ServerServiceClient.GetInstance().GetConfiguration());
            //    Thread.Sleep(100);
            //    TelephonyServiceClient.GetInstance().Login(
            //        networkCredential.UserName,
            //        networkCredential.Password,
            //        ServerServiceClient.GetInstance().OperatorClient.AgentId,
            //        ServerServiceClient.GetInstance().OperatorClient.AgentPassword,
            //        recordCalls == null ? false : bool.Parse(recordCalls.Value),
            //        ExtNumber, true);
            //    TelephonyServiceClient.GetInstance().TelephonyAction += new EventHandler<TelephonyActionEventArgs>(defaultFrontClientFormDevX.FrontClientForm_TelephonyAction);

            //    defaultFrontClientFormDevX.doNotDisturbMessageShow = false;

            //    defaultFrontClientFormDevX.SetReady();
            //});
            
		}

        private void barButtonItemParkCall_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                PhoneReportClientData phoneReport = (this.MdiChildren[0] as DefaultFrontClientFormDevX).GlobalPhoneReport;
                if (phoneReport != null && !string.IsNullOrEmpty(phoneReport.CustomCode))
                {
                    ParkedCallClientData client = new ParkedCallClientData();
                    client.PhoneReportCustomCode = phoneReport.CustomCode;
                    client.ANIReceived = phoneReport.PhoneReportLineClient.Telephone;
                    client.DateWhenParked = ServerServiceClient.GetInstance().GetTime();
                    client.DateWhenUnParked = DateTime.MaxValue;
                    client.OperatorCodeWhoParked = ServerServiceClient.GetInstance().OperatorClient.Code;
                    client = ServerServiceClient.GetInstance().SaveOrUpdateClientDataWithReturn(client) as ParkedCallClientData;
                    TelephonyServiceClient.GetInstance().ParkCall("7000", client.PhoneReportCustomCode);
                }
            }
            catch (Exception ex)
            {
                MessageForm.Show(ex.Message, ex);
            }
        }

        private void barButtonItemHistory_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (incidentsHistoryForm == null || incidentsHistoryForm.IsDisposed == true)
            {
                incidentsHistoryForm = new IncidentsHistoryForm();
            }
            incidentsHistoryForm.ControlBox = true;
            incidentsHistoryForm.MdiParent = this;
            incidentsHistoryForm.Activate();
            incidentsHistoryForm.Show();
        }

        private void barButtonItemFinalize_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void barButtonItemStatus_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void ribbonControl1_Click(object sender, EventArgs e)
        {

        }
    }
}
