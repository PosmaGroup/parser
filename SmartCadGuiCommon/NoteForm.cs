using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using SmartCadCore.Core;
using Smartmatic.SmartCad.Service;
using SmartCadCore.ClientData;
using System.Reflection;
using System.ServiceModel;
using SmartCadCore.Common;

namespace SmartCadGuiCommon
{
    public partial class NoteForm<T> : DevExpress.XtraEditors.XtraForm where T : ClientData
    {
        private NoteClientData note;
        private ClientData clientData;

        public NoteForm(T clientData)
        {
            InitializeComponent();
            this.clientData = clientData;
        }

        private void NoteForm_Load(object sender, EventArgs e)
        {
            if (clientData is UnitClientData)
            {
                this.note = new UnitNoteClientData();
                this.Text = ResourceLoader.GetString2("AddUnitNote");
                this.groupBoxNote.Text = ResourceLoader.GetString2("UnitNote");
                this.simpleButtonAccept.Text = ResourceLoader.GetString2("$MessageUnitNote.Add");
                this.simpleButtonCancel.Text = ResourceLoader.GetString2("$Message.Cancel");
            }
        }

        private void buttonExAccept_Click(object sender, EventArgs e)
        {
            BackgroundProcessForm processForm = new BackgroundProcessForm(this,
                new MethodInfo[1]
                { 
                    GetType().GetMethod("SaveNote", BindingFlags.NonPublic | BindingFlags.Instance) 
                },
                new object[1][] { new object[0] });
            processForm.CanThrowError = true;
            processForm.ShowDialog();
        }

        private void SaveNote()
        {
            OperatorClientData oper = ServerServiceClient.GetInstance().OperatorClient;
            note.CompleteUserName = oper.FirstName + " " + oper.LastName;
            note.CreationDate = ServerServiceClient.GetInstance().GetTime();
            note.Detail = this.textBoxNote.Text;
            note.UserLogin = oper.Login;
            if (clientData is UnitClientData)
            {
                (note as UnitNoteClientData).UnitCode = clientData.Code;
                (note as UnitNoteClientData).DispatchOrderCode = (clientData as UnitClientData).DispatchOrder.Code;
            }
            try
            {
                ServerServiceClient.GetInstance().SaveOrUpdateClientData(note);
            }
            catch (CommunicationObjectFaultedException)
            {
                throw;
            }
            catch (FaultException ex)
            {
                if (ex.Code != null && ex.Code.Name != null
                    && ex.Code.Name.Equals(typeof(DatabaseServerDownException).Name))
                {
                    throw new CommunicationObjectFaultedException(ex.Message, ex);
                }
                else
                {
                    throw ex;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void EnableAcceptButton(object sender, EventArgs e)
        {
            if (this.textBoxNote.Text.Trim().Length > 0)
            {
                this.simpleButtonAccept.Enabled = true;
            }
            else
            {
                this.simpleButtonAccept.Enabled = false;
            }
        }

    }
}