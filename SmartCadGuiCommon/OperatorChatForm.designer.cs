using SmartCadControls;

namespace SmartCadGuiCommon
{
	partial class OperatorChatForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OperatorChatForm));
            this.gridControlExOpers = new GridControlEx();
            this.gridViewExOpers = new GridViewEx();
            this.layoutConverter1 = new DevExpress.XtraLayout.Converter.LayoutConverter(this.components);
            this.OperatorChatFormConvertedLayout = new DevExpress.XtraLayout.LayoutControl();
            this.ChatXtraTabControl = new DevExpress.XtraTab.XtraTabControl();
            this.simpleButtonSentToAll = new DevExpress.XtraEditors.SimpleButton();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemTabControl = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroupSendAll = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemGridControl = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExOpers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExOpers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OperatorChatFormConvertedLayout)).BeginInit();
            this.OperatorChatFormConvertedLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChatXtraTabControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTabControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupSendAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemGridControl)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControlExOpers
            // 
            this.gridControlExOpers.EnableAutoFilter = false;
            this.gridControlExOpers.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExOpers.Location = new System.Drawing.Point(12, 12);
            this.gridControlExOpers.MainView = this.gridViewExOpers;
            this.gridControlExOpers.Name = "gridControlExOpers";
            this.gridControlExOpers.Size = new System.Drawing.Size(523, 233);
            this.gridControlExOpers.TabIndex = 0;
            this.gridControlExOpers.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExOpers});
            this.gridControlExOpers.ViewTotalRows = false;
            // 
            // gridViewExOpers
            // 
            this.gridViewExOpers.AllowFocusedRowChanged = true;
            this.gridViewExOpers.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewExOpers.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExOpers.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewExOpers.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewExOpers.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewExOpers.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExOpers.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewExOpers.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewExOpers.EnablePreviewLineForFocusedRow = false;
            this.gridViewExOpers.GridControl = this.gridControlExOpers;
            this.gridViewExOpers.Name = "gridViewExOpers";
            this.gridViewExOpers.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExOpers.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewExOpers.OptionsView.ShowGroupPanel = false;
            this.gridViewExOpers.OptionsView.ShowIndicator = false;
            this.gridViewExOpers.ViewTotalRows = false;
            this.gridViewExOpers.DoubleClick += new System.EventHandler(this.gridViewEx1_DoubleClick);
            // 
            // OperatorChatFormConvertedLayout
            // 
            this.OperatorChatFormConvertedLayout.AllowCustomizationMenu = false;
            this.OperatorChatFormConvertedLayout.Controls.Add(this.ChatXtraTabControl);
            this.OperatorChatFormConvertedLayout.Controls.Add(this.simpleButtonSentToAll);
            this.OperatorChatFormConvertedLayout.Controls.Add(this.richTextBox1);
            this.OperatorChatFormConvertedLayout.Controls.Add(this.gridControlExOpers);
            this.OperatorChatFormConvertedLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OperatorChatFormConvertedLayout.Location = new System.Drawing.Point(0, 0);
            this.OperatorChatFormConvertedLayout.Name = "OperatorChatFormConvertedLayout";
            this.OperatorChatFormConvertedLayout.Root = this.layoutControlGroup1;
            this.OperatorChatFormConvertedLayout.Size = new System.Drawing.Size(547, 778);
            this.OperatorChatFormConvertedLayout.TabIndex = 2;
            // 
            // ChatXtraTabControl
            // 
            this.ChatXtraTabControl.ClosePageButtonShowMode = DevExpress.XtraTab.ClosePageButtonShowMode.InActiveTabPageHeader;
            this.ChatXtraTabControl.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.False;
            this.ChatXtraTabControl.HeaderButtons = ((DevExpress.XtraTab.TabButtons)((DevExpress.XtraTab.TabButtons.Prev | DevExpress.XtraTab.TabButtons.Next)));
            this.ChatXtraTabControl.HeaderButtonsShowMode = DevExpress.XtraTab.TabButtonShowMode.WhenNeeded;
            this.ChatXtraTabControl.Location = new System.Drawing.Point(12, 279);
            this.ChatXtraTabControl.LookAndFeel.SkinName = "Liquid Sky";
            this.ChatXtraTabControl.Name = "ChatXtraTabControl";
            this.ChatXtraTabControl.PaintStyleName = "Skin";
            this.ChatXtraTabControl.Size = new System.Drawing.Size(523, 487);
            this.ChatXtraTabControl.TabIndex = 4;
            this.ChatXtraTabControl.CloseButtonClick += new System.EventHandler(this.ChatsXtraTabControl_CloseButtonClick);
            // 
            // simpleButtonSentToAll
            // 
            this.simpleButtonSentToAll.Location = new System.Drawing.Point(435, 293);
            this.simpleButtonSentToAll.Name = "simpleButtonSentToAll";
            this.simpleButtonSentToAll.Size = new System.Drawing.Size(76, 26);
            this.simpleButtonSentToAll.StyleController = this.OperatorChatFormConvertedLayout;
            this.simpleButtonSentToAll.TabIndex = 6;
            this.simpleButtonSentToAll.Click += new System.EventHandler(this.simpleButtonSentToAll_Click);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(12, 249);
            this.richTextBox1.MinimumSize = new System.Drawing.Size(412, 62);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(419, 70);
            this.richTextBox1.TabIndex = 5;
            this.richTextBox1.Text = "";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemTabControl,
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(547, 778);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItemTabControl
            // 
            this.layoutControlItemTabControl.Control = this.ChatXtraTabControl;
            this.layoutControlItemTabControl.CustomizationFormText = "layoutControlItem2layoutControlItemTabControl";
            this.layoutControlItemTabControl.Location = new System.Drawing.Point(0, 267);
            this.layoutControlItemTabControl.MinSize = new System.Drawing.Size(111, 31);
            this.layoutControlItemTabControl.Name = "layoutControlItemTabControl";
            this.layoutControlItemTabControl.Size = new System.Drawing.Size(527, 491);
            this.layoutControlItemTabControl.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemTabControl.Text = "layoutControlItemTabControl";
            this.layoutControlItemTabControl.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemTabControl.TextToControlDistance = 0;
            this.layoutControlItemTabControl.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupSendAll,
            this.layoutControlItemGridControl});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(527, 267);
            this.layoutControlGroup2.Text = "layoutControlGroup2";
            // 
            // layoutControlGroupSendAll
            // 
            this.layoutControlGroupSendAll.CustomizationFormText = "layoutControlGroupSendAll";
            this.layoutControlGroupSendAll.ExpandButtonMode = DevExpress.Utils.Controls.ExpandButtonMode.Inverted;
            this.layoutControlGroupSendAll.ExpandButtonVisible = true;
            this.layoutControlGroupSendAll.Expanded = false;
            this.layoutControlGroupSendAll.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.emptySpaceItem1,
            this.layoutControlItem3});
            this.layoutControlGroupSendAll.Location = new System.Drawing.Point(0, 237);
            this.layoutControlGroupSendAll.Name = "layoutControlGroupSendAll";
            this.layoutControlGroupSendAll.Size = new System.Drawing.Size(527, 30);
            this.layoutControlGroupSendAll.Text = "layoutControlGroupSendAll";
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.simpleButtonSentToAll;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(423, 44);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(80, 30);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(80, 30);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(80, 30);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(423, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(80, 44);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.richTextBox1;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(423, 74);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(423, 74);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItemGridControl
            // 
            this.layoutControlItemGridControl.Control = this.gridControlExOpers;
            this.layoutControlItemGridControl.CustomizationFormText = "gridControlExSupervisorsitem";
            this.layoutControlItemGridControl.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemGridControl.MinSize = new System.Drawing.Size(111, 31);
            this.layoutControlItemGridControl.Name = "layoutControlItemGridControl";
            this.layoutControlItemGridControl.Size = new System.Drawing.Size(527, 237);
            this.layoutControlItemGridControl.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemGridControl.Text = "layoutControlItemGridControl";
            this.layoutControlItemGridControl.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemGridControl.TextToControlDistance = 0;
            this.layoutControlItemGridControl.TextVisible = false;
            // 
            // OperatorChatForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(547, 778);
            this.Controls.Add(this.OperatorChatFormConvertedLayout);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.LookAndFeel.SkinName = "Blue";
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(563, 816);
            this.Name = "OperatorChatForm";
            this.Opacity = 0.9D;
            this.Load += new System.EventHandler(this.OperatorChatForm_Load);
            this.Click += new System.EventHandler(this.OperatorChatForm_Click);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExOpers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExOpers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OperatorChatFormConvertedLayout)).EndInit();
            this.OperatorChatFormConvertedLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChatXtraTabControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTabControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupSendAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemGridControl)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

		private GridControlEx gridControlExOpers;
		private GridViewEx gridViewExOpers;
		private DevExpress.XtraLayout.Converter.LayoutConverter layoutConverter1;
		private DevExpress.XtraLayout.LayoutControl OperatorChatFormConvertedLayout;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemGridControl;
		private DevExpress.XtraTab.XtraTabControl ChatXtraTabControl;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTabControl;
		private DevExpress.XtraEditors.SimpleButton simpleButtonSentToAll;
		private System.Windows.Forms.RichTextBox richTextBox1;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupSendAll;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;


	}
}