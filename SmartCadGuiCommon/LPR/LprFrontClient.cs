using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Smartmatic.SmartCad.Service;
using System.Net;

using System.Collections;
using DevExpress.XtraBars;
using System.IO;
using System.Xml;
using SmartCadCore.Core;
using SmartCadCore.Common;
using SmartCadCore.ClientData;
using SmartCadCore.Enums;
using SmartCadGuiCommon;
using SmartCadGuiCommon.Classes;
using SmartCadGuiCommon.Controls.Administration;
using VideoOS.Platform;
using Smartmatic.SmartCad.Vms;
using SmartCadGuiCommon.Util;
using SmartCadGuiCommon.Controls;

namespace SmartCadGuiCommon
{
	public partial class LprFrontClient : XtraForm
	{
		#region Fields

        OperatorChatForm chatForm;
        private ConfigurationClientData config = null;
		private ApplicationMessageClientData alertControlAmcd = null;
        private DateTime syncServerDateTime;
        private TimeSpan sinceLoggedIn;
        private const int GLOBAL_TIMER_PERIOD = 10000;
        private readonly TimeSpan ADD_TIMESPAN;
        public System.Threading.Timer globalTimer;
        private string labelUserCaption = "";
        private string labelConnectedCaption = "";
        ServerServiceClient serverServiceClient = null;
        private NetworkCredential networkCredential;
        private ConfigurationClientData configurationClient;
        public string password;
        #endregion

        #region Properties

        public ServerServiceClient ServerServiceClient
        {
            get
            {
                return serverServiceClient;
            }
            set
            {
                serverServiceClient = value;
            }
        }

        public NetworkCredential NetworkCredential
        {
            get
            {
                return networkCredential;
            }

            set
            {
                networkCredential = value;
            }
        }

        public ConfigurationClientData ConfigurationClient
        {
            get
            {
                return configurationClient;
            }
            set
            {
                configurationClient = value;
            }
        }

        #endregion

        public LprFrontClient()
		{
            var applications = ServerServiceClient.GetInstance().GetActiveApplications();

            InitializeComponent();

            if (!applications.Contains(SmartCadApplications.SmartCadMap)) 
            {
                ribbonPageGroupTools.Visible = false;
            }

            if (!applications.Contains(SmartCadApplications.SmartCadSupervision)) 
            {
                barButtonItemChat.Visibility = BarItemVisibility.Never;
            }
            WindowState = FormWindowState.Maximized;

			LoadLanguage();
			ServerServiceClient.GetInstance().CommittedChanges += LprFrontClient_CommittedChanges;
            ADD_TIMESPAN = new TimeSpan(0, 0, 0, 0, GLOBAL_TIMER_PERIOD);

            config = ServerServiceClient.GetInstance().GetConfiguration();
        }

        private void FillStatusStrip()
        {
            try
            {
                syncServerDateTime = ServerServiceClient.GetInstance().GetTime();
                sinceLoggedIn = TimeSpan.Zero;
                this.labelConnected.Caption = "00:00";

                labelDate.Caption = ApplicationUtil.GetFormattedDate(serverServiceClient.GetTime());
                globalTimer = ApplicationUtil.RecurrentExecuteMethod(
                    new SimpleDelegate(
                    delegate
                    {
                        globalTimer_Tick(null, null);
                    }), null, null, GLOBAL_TIMER_PERIOD, GLOBAL_TIMER_PERIOD);

                labelUser.Caption = labelUserCaption + serverServiceClient.OperatorClient.FirstName
                    + " " + serverServiceClient.OperatorClient.LastName;
                labelConnected.Caption = labelConnectedCaption + "00:00";
            }
            catch { }
        }

        private void globalTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                syncServerDateTime = syncServerDateTime.AddMilliseconds((double)GLOBAL_TIMER_PERIOD);
                sinceLoggedIn = sinceLoggedIn.Add(ADD_TIMESPAN);
                FormUtil.InvokeRequired(this, delegate
                {
                    labelDate.Caption = ApplicationUtil.GetFormattedDate(ServerServiceClient.GetInstance().GetTime());
                    labelConnected.Caption = labelConnectedCaption + Math.Truncate(sinceLoggedIn.TotalHours).ToString().PadLeft(2, '0') + ":" + sinceLoggedIn.Minutes.ToString().PadLeft(2, '0');
                });

            }
            catch {
                Console.WriteLine("Error X ROJA");
            }
        }

		private void LoadLanguage()
		{
            Text = ResourceLoader.GetString2("LPRModule");
            barButtonItemOpenMap.Glyph = ResourceLoader.GetImage("$Image.OpenMap");
            barButtonItemChat.Caption = ResourceLoader.GetString2("Chat");
            barButtonItemOpenMap.LargeGlyph = ResourceLoader.GetImage("$Image.OpenMap");

			barButtonItemChat.Caption = ResourceLoader.GetString2("Chat");
			barButtonItemOpenMap.Caption = ResourceLoader.GetString2("Open");
			barButtonItemPrint.Caption = ResourceLoader.GetString2("Print");
			barButtonItemRefresh.Caption = ResourceLoader.GetString2("Refresh");
			barButtonItemSave.Caption = ResourceLoader.GetString2("Save");
			ribbonPageGroupGeneralOptions.Text = ResourceLoader.GetString2("GeneralOptions");
			ribbonPageGroupTools.Text = ResourceLoader.GetString2("Maps");
			ribbonPage1.Text = ResourceLoader.GetString2("Lpr");

            #region Footer
            labelUserCaption = ResourceLoader.GetString2("User") + ": ";
            labelConnectedCaption = ResourceLoader.GetString2("Connected") + ": ";
            #endregion
		}
		
		public void LprFrontClient_CommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
		{
			//try
			//{
			//	if (e != null && e.Objects != null && e.Objects.Count > 0)
			//	{
			//		if (e.Objects[0] is ApplicationMessageClientData)
			//		{
			//			ApplicationMessageClientData amcd = ((ApplicationMessageClientData)e.Objects[0]);
			//			if (amcd.ToOperators.Contains(ServerServiceClient.GetInstance().OperatorClient))
			//			{
			//				if (chatForm != null && chatForm.IsDisposed == false)
			//				{
            //                    FormUtil.InvokeRequired(this, () =>
			//					{
			//						chatForm.OpenNewTab(amcd.Operator, amcd);
			//						chatForm.Activate();
			//					});
			//				}
			//				else
			//				{
			//					alertControlAmcd = amcd;
			//					FormUtil.InvokeRequired(this, () =>
			//					{
			//						alertControl1.Show(this, "Message from: " + amcd.Operator.Login, amcd.Message);
			//					});
			//				}
			//			}
			//		}
			//		else if (e.Objects[0] is ApplicationPreferenceClientData)
            //        {
            //            #region ApplicationPreferenceClientData
            //            ApplicationPreferenceClientData preference = e.Objects[0] as ApplicationPreferenceClientData;
			//			if (e.Action == CommittedDataAction.Update)
			//			{
			//				if (preference.Name.Equals("AlarmTimeLimit"))
			//				{
			//					FormUtil.InvokeRequired(this, () =>
            //                    {
			//						repositoryItemProgressBar1.Maximum = int.Parse(preference.Value);
			//					});
			//				}
            //                else if (preference.Name.Equals("CCTVIncidentCodePrefix"))
            //                {
            //                    historyForm.CCTVIncidentCodePrefix.Value = preference.Value;
            //                }
            //                else if (preference.Name.Equals("CctvVideoSecondsBefore"))
            //                {
            //                    FormUtil.InvokeRequired(this, () =>
            //                    {
            //                        historyForm.CctvVideoSecondsBefore.Value = preference.Value;
            //                        //cameraAlarmsForm.CctvVideoSecondsBefore.Value = preference.Value;
            //                    });
            //                }
            //                else if (preference.Name.Equals("CctvVideoSecondsAfter"))
            //                {
            //                    FormUtil.InvokeRequired(this, () =>
            //                    {
            //                        historyForm.CctvVideoSecondsAfter.Value = preference.Value;
            //                        //cameraAlarmsForm.CctvVideoSecondsAfter.Value = preference.Value;
            //                    });
            //                }
            //            }
            //            #endregion
            //        }
            //        else if (e.Objects[0] is Item)
            //        {
            //            CameraClientData camera = ((CameraClientData)e.Objects[0]);
            //            FormUtil.InvokeRequired(this, () =>
            //            {
            //                foreach (CctvCameraNode data in Cameras)
            //                {
            //                    if (data.Camera.Code == camera.Code)
            //                    {
            //                        data.Camera = camera;
            //                        cameraVisualizeForm.LoadCameras();
            //                        if (cameraVisualizeForm.SelectedCamera != null && (cameraVisualizeForm.SelectedCamera.Camera.Code) == camera.Code)
            //                            cameraVisualizeForm.SelectedCamera.Camera = camera;
            //                        playBackForm.LoadCameras();
            //                        //cameraAlarmsForm.LoadInitialData();
            //                        //if (cameraAlarmsForm.SelectedAlarm != null && cameraAlarmsForm.SelectedAlarm.Camera.Code == camera.Code)
            //                        //    cameraAlarmsForm.SelectedAlarm.Camera = camera;
            //                        break;
            //                    }
            //                }
            //            });
            //        }
			//	}
			//}
			//catch
			//{
			//}
		}

		public void SetPassword(string p)
        {
            this.password = p;
            ServerServiceClient.GetInstance().OperatorClient.Password = p;
		}

        public void LoadInitialData()
        {
            serverServiceClient = ServerServiceClient.GetInstance();            

            CheckAccessToMap();

            FillStatusStrip();
        }

		private void LprFrontClient_Load(object sender, EventArgs e)
		{  

			repositoryItemProgressBar1.Step = 1;

            xtraTabbedMdiManager1.SelectedPageChanged += new EventHandler(xtraTabbedMdiManager1_SelectedPageChanged);
            
            
		}

        void xtraTabbedMdiManager1_SelectedPageChanged(object sender, EventArgs e)
        {
            
        }

        private void LprFrontClient_FormClosing(object sender, FormClosingEventArgs e)
        {
            //check if a call has entered or if an incident is being typed
            //if (e.CloseReason == CloseReason.None)
            //{

            //    ServerServiceClient.GetInstance().CommittedChanges -= FrontClientFormDevX_CommittedChanges;
            //    Application.Exit();
            //}
            //else
            //{
            //    e.Cancel = true;
            //}
            ServerServiceClient.GetInstance().CommittedChanges -= LprFrontClient_CommittedChanges;
            

            if (!EnsureLogout())
            {
                e.Cancel = true;
            }
            else 
            {
                e.Cancel = false;
            }

        }

        private static bool EnsureLogout()
		{
			DialogResult dialogResult = MessageForm.Show(ResourceLoader.GetString2("$Message.ExitApplication"), MessageFormType.Question);

			return dialogResult == DialogResult.Yes;
		}

		private void CheckAccessToMap()
		{
			if (ServerServiceClient.GetInstance().CheckAccessClient(UserResourceClientData.Application,
				UserActionClientData.Basic, UserApplicationClientData.Map, false) == true)
				barButtonItemOpenMap.Enabled = true;
			else
				barButtonItemOpenMap.Enabled = false;
		}

		private void alertControl1_AlertClick(object sender, DevExpress.XtraBars.Alerter.AlertClickEventArgs e)
		{
			if (chatForm == null || chatForm.IsDisposed == true)
			{
				chatForm = new OperatorChatForm(UserApplicationClientData.Cctv.Name, ChatOperatorType.Operator);
			}
			chatForm.WindowState = FormWindowState.Normal;
			chatForm.Activate();
			chatForm.Show();
			chatForm.BringToFront();
		}        

		private void barButtonItemChat_ItemClick(object sender, ItemClickEventArgs e)
		{
			if (chatForm == null || chatForm.IsDisposed == true)
				chatForm = new OperatorChatForm(UserApplicationClientData.Cctv.Name,ChatOperatorType.Operator);
			
			chatForm.Activate();
			chatForm.Show();
		}

		private void barButtonItemExit_ItemClick(object sender, ItemClickEventArgs e)
		{
			this.Close();
		}

		public void barButtonItemHelp_ItemClick(object sender, ItemClickEventArgs e)
		{
			try
            {
                ApplicationUtil.LaunchHelp(SmartCadConfiguration.DistFolder + "/Help/LPR Module.chm");
            }
            catch (Exception ex)
            {
                MessageForm.Show(ResourceLoader.GetString2("$Message.HelpNotInstalled"), ex);
            }
        }

		private void barButtonItemOpenMap_ItemClick(object sender, ItemClickEventArgs e)
		{
			//try
            //{
            //    ApplicationUtil.LaunchApplicationWithArguments(
            //            SmartCadConfiguration.SmartCadMapFilename,
			//			new object[] { ServerServiceClient.GetInstance().OperatorClient.Login,
            //                defaultCctvFrontClientFormDevX.password, 
            //                ApplicationUtil.CheckPrimaryScreenBounds(Left),
            //                new SendActivateLayerEventArgs(ShapeType.Incident),
            //                new SendActivateLayerEventArgs(ShapeType.Struct)
            //            });
            //}
            //catch (Exception ex)
            //{
            //    MessageForm.Show(ex.Message, ex);
            //}
		}
        

        private void labelUser_ItemClick(object sender, ItemClickEventArgs e)
        {

        }
	}
}