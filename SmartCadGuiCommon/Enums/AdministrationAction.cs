﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadGuiCommon.Enums
{
    public enum AdministrationAction
    {
        Create,
        Modify,
        Delete
    }
}
