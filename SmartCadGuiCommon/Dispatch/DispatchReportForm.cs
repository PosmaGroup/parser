using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

using System.Collections;
using System.ServiceModel;
using Smartmatic.SmartCad.Service;
using DevExpress.XtraEditors.Repository;
using SmartCadCore.ClientData;
using SmartCadCore.Enums;
using SmartCadCore.Common;
using SmartCadCore.Core;
using SmartCadControls.Controls;
using SmartCadControls;
using SmartCadGuiCommon.Controls;

namespace SmartCadGuiCommon
{
    public partial class DispatchReportForm : BaseDispatchReportForm
    {
        private int departmentsChecked;
        private int incidentsChecked;

        #region Constructors
        public DispatchReportForm()
        {
            InitializeComponent();
         
        }

        public DispatchReportForm(AdministrationRibbonForm form)
            : base(form)
        {
            InitializeComponent();
                    
        }

        public DispatchReportForm(AdministrationRibbonForm form, DispatchReportClientData data, FormBehavior behaviour)
            :this(form)
        {
            gridControlExIncident.EnableAutoFilter = true;
            gridControlExIncident.Type = typeof(GridControlDataCheckIncidentType);
            gridViewExIncident.OptionsBehavior.Editable = true;
            gridViewExIncident.Columns["Checked"].OptionsColumn.AllowEdit = true;
            gridViewExIncident.Columns["Checked"].OptionsColumn.AllowMove = false;
            gridViewExIncident.Columns["Checked"].OptionsColumn.ShowCaption = false;           
            gridViewExIncident.Columns["Name"].OptionsColumn.AllowEdit = false;
            

            gridControlExDepartment.EnableAutoFilter = true;
            gridControlExDepartment.Type = typeof(GridControlDataCheckDepartment);
            gridViewExDepartment.OptionsBehavior.Editable = true;
            gridViewExDepartment.Columns["Checked"].OptionsColumn.AllowEdit = true;
            gridViewExDepartment.Columns["Checked"].OptionsColumn.AllowMove = false;
            gridViewExDepartment.Columns["Checked"].OptionsColumn.ShowCaption = false;
            gridViewExDepartment.Columns["Name"].OptionsColumn.AllowEdit = false;
            selectUnSelectControlDevxMain.InitializeGrids(typeof(GridControlDataDispatchReportQuestion));
            selectUnSelectControlDevxMain.Parameters_Changed += new SelectUnSelectControlDevx.ControlParametersChanged(selectUnSelectControlDevxMain_Parameters_Changed);
            selectUnSelectControlDevxMain.LayoutControlItem = 0;
            LoadData();
            SelectedData = data;
            Behavior = behaviour;

         
        }
        
        #endregion

        #region Properties
        public override DispatchReportClientData SelectedData
        {
            get
            {
                return data;
            }
            set
            {
                data = value;

                if (data != null)
                {
                    textEditName.Text = data.Name;
                 
                    if (data.DepartmentTypes != null)
                        CheckDepartmentTypes(data.DepartmentTypes);

                    if (data.IncidentTypes != null)
                        CheckIncidentTypes(data.IncidentTypes);

                    if (data.Questions != null)
                        FillQuestionAssigned(data.Questions);                        

                }

                CheckButtons();
            }
        }

        protected override string UpdateMessage
        {
            get { return "UpdateFormDispatchReportData"; }
        }

        protected override string DeleteMessage
        {
            get { return "DeleteFormDispatchReportClientData"; }
        }

        protected override string CreateFormText
        {
            get { return "CreateDispatchReportFormText"; }
        }

        protected override string EditFormText
        {
            get { return "EditDispatchReportFormText"; }
        }
        #endregion

        #region Events

        /// <summary>
        /// Create or updates the current report.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButtonAccept_Click(object sender, EventArgs e)
        {
            ButtonAccept_Click(sender, e);
        }

        
        private void selectUnSelectControlDevxMain_Parameters_Changed(object sender, EventArgs e)
        {
            CheckButtonsEventHandler(null, null);
        }
        #endregion

        #region Methods

        private void FillQuestionAssigned(IList reportQuestionsList) 
        {
            QuestionDispatchReportClientData question;
            GridControlDataDispatchReportQuestion gridData;
            foreach (DispatchReportQuestionDispatchReportClientData reportQuestion in reportQuestionsList)
            {
                if (reportQuestion.QuestionRequired == false)
                {
                    question = new QuestionDispatchReportClientData();
                    question.Code = reportQuestion.QuestionCode;
                    question.Text = reportQuestion.QuestionText;
                    question.Render = reportQuestion.QuestionRender;

                    gridData = new GridControlDataDispatchReportQuestion(question);
                    selectUnSelectControlDevxMain.gridControlExNotSelected.DeleteItem(gridData);

                    gridData.ReportQuestionCode = reportQuestion.Code;
                    gridData.ReportQuestionVersion = reportQuestion.Version;
                    selectUnSelectControlDevxMain.gridControlExSelected.AddOrUpdateItem(gridData);
               }
            }
           
        }

        private void CheckDepartmentTypes(IList departmentTypes)
        {
            gridViewExDepartment.BeginUpdate();
            foreach (DepartmentTypeClientData dep in departmentTypes)
            {
                foreach (GridControlDataCheckDepartment gridData in gridControlExDepartment.Items)
                {
                    if (dep.Code == gridData.Code)
                    {
                        gridData.Checked = true;
                        departmentsChecked++;
                        break;
                    }
                }
            }

            gridViewExDepartment.EndUpdate();

            UpdateDepartmentCheckState();
        }

        private void CheckIncidentTypes(IList incidentTypes)
        {
            gridViewExIncident.BeginUpdate();
            foreach (IncidentTypeClientData incident in incidentTypes)
            {
                foreach (GridControlDataCheckIncidentType gridData in gridControlExIncident.Items)
                {
                    if (incident.Code == gridData.Code)
                    {
                        gridData.Checked = true;
                        incidentsChecked++;
                        break;
                    }
                }
            }
            gridViewExIncident.EndUpdate();

            UpdateIncidentCheckState();
        }
        /// <summary>
        /// Loads all the necessary string to display on the forms.
        /// </summary>
        protected override void LoadLanguage()
        {
            Icon = ResourceLoader.GetIcon("$Icon.DispatchReport");

            layoutControlGroupName.Text = ResourceLoader.GetString2("EndReportInfo");
            layoutControlItemName.Text = ResourceLoader.GetString2("Name") + ":*";

            layoutControlGroupGridIncident.Text = ResourceLoader.GetString2("Incidents") + " *";
            layoutControlGroupGridDepartment.Text = ResourceLoader.GetString2("Departments") + " *";
            layoutControlGroupSelectUnSelect.Text = ResourceLoader.GetString2("Questions");
            simpleButtonPreview.Text = ResourceLoader.GetString2("Preview");
            checkEditAllDep.Text = ResourceLoader.GetString2("SelectAll");
            checkEditAllIncidents.Text = ResourceLoader.GetString2("SelectAll");
            simpleButtonCancel.Text = ResourceLoader.GetString2("Cancel");

            if (behavior == FormBehavior.Create)
                simpleButtonAccept.Text = ResourceLoader.GetString2("Create");
            else
                simpleButtonAccept.Text = ResourceLoader.GetString2("Accept");
        }

        /// <summary>
        /// Check if the conditions are meet to enable the accept button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void CheckButtons()
        {
            if (string.IsNullOrEmpty(textEditName.Text) == true ||
                departmentsChecked == 0 ||
                incidentsChecked == 0)
                simpleButtonAccept.Enabled = false;
            else
                simpleButtonAccept.Enabled = true;
        }

        
        /// <summary>
        /// Execute the creation or update of the fleet.
        /// </summary>
        protected override void Save_Help()
        {
            buttonOkPressed = true;

            data = GetReport();

            ServerServiceClient.GetInstance().SaveOrUpdateClientData(data);

            buttonOkPressed = false;
        }

        private DispatchReportClientData GetReport() 
        {
            if (Behavior == FormBehavior.Create)
                data = new DispatchReportClientData();

            data.Name = textEditName.Text.Trim();
            data.DepartmentTypes = new List<DepartmentTypeClientData>();
            foreach (GridControlDataCheckDepartment gridData in gridControlExDepartment.Items)
            {
                if (gridData.Checked == true)
                    data.DepartmentTypes.Add(gridData.Tag as DepartmentTypeClientData);
            }

            data.IncidentTypes = new List<IncidentTypeClientData>();
            foreach (GridControlDataCheckIncidentType gridData in gridControlExIncident.Items)
            {
                if (gridData.Checked == true)
                    data.IncidentTypes.Add(gridData.Tag as IncidentTypeClientData);
            }

            DispatchReportQuestionDispatchReportClientData reportQuestion;
            data.Questions = new List<DispatchReportQuestionDispatchReportClientData>();
            for (int i = 0; i < selectUnSelectControlDevxMain.gridControlExSelected.Items.Count; i++)
            {
                GridControlDataDispatchReportQuestion gridData = (GridControlDataDispatchReportQuestion)selectUnSelectControlDevxMain.gridControlExSelected.Items[i];
                reportQuestion = new DispatchReportQuestionDispatchReportClientData();
                reportQuestion.Code = gridData.ReportQuestionCode;
                reportQuestion.QuestionCode = (gridData.Tag as QuestionDispatchReportClientData).Code;
                reportQuestion.QuestionText = (gridData.Tag as QuestionDispatchReportClientData).Text;
                reportQuestion.QuestionRender = (gridData.Tag as QuestionDispatchReportClientData).Render;
                reportQuestion.Version = gridData.ReportQuestionVersion;
                reportQuestion.ReportCode = data.Code;
                reportQuestion.Order = i;
                data.Questions.Add(reportQuestion);
            }

            return data;
        }

        /// <summary>
        /// Position the cursor on the first field where an error occur.
        /// </summary>
        /// <param name="ex">Error.</param>
        protected override void GetErrorFocus(FaultException ex)
        {
            if (ex.Message.Contains(ResourceLoader.GetString2("Name").ToLower()))
            {
                if (behavior == FormBehavior.Create)
                    textEditName.Text = textEditName.Text;
                textEditName.Focus();
            }
        }

        /// <summary>
        /// Invoke the CheckButtons method.
        /// Method to subscribe to control events.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CheckButtonsEventHandler(object sender, EventArgs e)
        {
            CheckButtons();
       
        }

        /// <summary>
        /// Loads the data that the forms need to work.
        /// </summary>
        protected override void LoadData()
        {
            int code = 0;
            if (data != null)
                code = data.Code;

            IList departments = ServerServiceClient.GetInstance().SearchClientObjectsMaxRows(SmartCadHqls.GetDispatchableDepartmentsType, true);
            IList gridDataDepartment = new ArrayList();
            foreach (DepartmentTypeClientData department in departments)
            {
                gridDataDepartment.Add(new GridControlDataCheckDepartment(department));
            }
            gridControlExDepartment.AddOrUpdateList(gridDataDepartment);

            IList incidentTypes = ServerServiceClient.GetInstance().SearchClientObjectsMaxRows(SmartCadHqls.GetEmergencyIncidentType, true);
            IList gridDataIncidentType = new ArrayList();
            foreach (IncidentTypeClientData incidentType in incidentTypes)
            {
                gridDataIncidentType.Add(new GridControlDataCheckIncidentType(incidentType));
            }
            gridControlExIncident.AddOrUpdateList(gridDataIncidentType);

            IList list = ServerServiceClient.GetInstance().SearchClientObjectsMaxRows(SmartCadHqls.GetQuestionsDispatchReportWithoutRequired, true);
            ArrayList unselected = new ArrayList();
            foreach (QuestionDispatchReportClientData question in list)
            {
                unselected.Add(new GridControlDataDispatchReportQuestion(question));
            }
            selectUnSelectControlDevxMain.InitializeData(unselected);
        }
        #endregion

        private void simpleButtonPreview_Click(object sender, EventArgs e)
        {
            DispatchReportClientData report = GetReport();

            EndDispatchReportForm form = new EndDispatchReportForm(FormBehavior.View, report);

            form.ShowDialog();
        }

        private void gridViewExDepartment_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            checkEditAllDep.CheckStateChanged -= new EventHandler(checkEditAllDep_CheckStateChanged);
            if (e.RowHandle > -1)
            {
                if (((bool)e.Value) == true)
                    departmentsChecked++;
                else
                    departmentsChecked--;

                UpdateDepartmentCheckState();
                CheckButtonsEventHandler(null, null);
            }
            checkEditAllDep.CheckStateChanged += new EventHandler(checkEditAllDep_CheckStateChanged);
        }

        private void gridViewExIncident_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            checkEditAllIncidents.CheckStateChanged -=new EventHandler(checkEditAllIncidents_CheckStateChanged);
            if (e.RowHandle > -1)
            {
                if (((bool)e.Value) == true)
                    incidentsChecked++;
                else
                    incidentsChecked--;

                UpdateIncidentCheckState();
                CheckButtonsEventHandler(null, null);
            }
            checkEditAllIncidents.CheckStateChanged += new EventHandler(checkEditAllIncidents_CheckStateChanged);
        }

        private void UpdateDepartmentCheckState()
        {        
            checkEditAllDep.CheckedChanged -=new EventHandler(checkEditAllDep_CheckedChanged);
            if (departmentsChecked == gridControlExDepartment.Items.Count)
                checkEditAllDep.CheckState = CheckState.Checked;
            else if (departmentsChecked == 0)
                checkEditAllDep.CheckState = CheckState.Unchecked;
            else
                checkEditAllDep.CheckState = CheckState.Indeterminate;
        
            checkEditAllDep.CheckedChanged += new EventHandler(checkEditAllDep_CheckedChanged);
        
        }

        private void UpdateIncidentCheckState()
        {
            checkEditAllIncidents.CheckedChanged -= new EventHandler(checkEditAllIncidents_CheckedChanged);
            if (incidentsChecked == gridControlExIncident.Items.Count)
              checkEditAllIncidents.CheckState = CheckState.Checked;
            else if (incidentsChecked == 0)
                checkEditAllIncidents.CheckState = CheckState.Unchecked;
            else
                checkEditAllIncidents.CheckState = CheckState.Indeterminate;
            checkEditAllIncidents.CheckedChanged += new EventHandler(checkEditAllIncidents_CheckedChanged);
         
        }

        private void checkEditAllDep_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEditAllDep.CheckState != CheckState.Indeterminate)
            {
                gridControlExDepartment.BeginUpdate();
                foreach (GridControlDataCheckDepartment gridData in gridControlExDepartment.Items)
                {
                    if (checkEditAllDep.CheckState == CheckState.Checked)
                    {
                        if (gridData.Checked == false)
                        {
                            gridData.Checked = true;
                            departmentsChecked++;
                        }
                    }
                    else
                    {
                        if (gridData.Checked == true)
                        {
                            gridData.Checked = false;
                            departmentsChecked--;
                        }

                    }
                }
                gridControlExDepartment.EndUpdate();
            }
        }

        private void checkEditAllIncidents_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEditAllIncidents.CheckState != CheckState.Indeterminate)
            {
                gridViewExIncident.BeginUpdate();
                foreach (GridControlDataCheckIncidentType gridData in gridControlExIncident.Items)
                {
                    if (checkEditAllIncidents.CheckState == CheckState.Checked)
                    {
                        if (gridData.Checked == false)
                        {
                            gridData.Checked = true;
                            incidentsChecked++;
                        }
                    }
                    else
                    {
                        if (gridData.Checked == true)
                        {
                            gridData.Checked = false;
                            incidentsChecked--;
                        }

                    }
                }
                gridViewExIncident.EndUpdate();
            }

        }

        private void checkEditAllDep_CheckStateChanged(object sender, EventArgs e)
        {
            if (checkEditAllDep.CheckState == CheckState.Indeterminate)
                checkEditAllDep.CheckState = CheckState.Unchecked;
        }

        private void checkEditAllIncidents_CheckStateChanged(object sender, EventArgs e)
        {
            if (checkEditAllIncidents.CheckState == CheckState.Indeterminate)
                checkEditAllIncidents.CheckState = CheckState.Unchecked;

        }
               
    }

    public class BaseDispatchReportForm : XtraFormAdmistration<DispatchReportClientData>
    { 
        public BaseDispatchReportForm()
        {
        }

        public BaseDispatchReportForm(AdministrationRibbonForm form)
            : base(form)
        {
        }

        public BaseDispatchReportForm(AdministrationRibbonForm form, DispatchReportClientData data, FormBehavior behaviour)
            :base(form, data, behaviour)
        {
        }
    }

    public class GridControlDataCheckDepartment : GridControlData
    {
        public DepartmentTypeClientData Department { get; set; }
        private bool check;

        public GridControlDataCheckDepartment(DepartmentTypeClientData department)
            : base(department)
        {
            Department = department;
        }

        public int Code
        {
            get
            {
                return Department.Code;
            }
            set { }
        }

        [GridControlRowInfoData(Visible = true, RepositoryType = typeof(RepositoryItemCheckEdit), Searchable = false, Width = 30)]
        public bool Checked
        {
            get { return check;
            }
            set { check = value;
            }
        }

        [GridControlRowInfoData(Visible = true, Width=350)]
        public string Name
        {
            get
            {
                return Department.Name;
            }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is GridControlDataCheckDepartment)
            {
                result = Department.Code.Equals(((GridControlDataCheckDepartment)obj).Department.Code);
            }
            return result;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    public class GridControlDataCheckIncidentType : GridControlData
    {
        public IncidentTypeClientData IncidentType { get; set; }
        public bool check;

        public GridControlDataCheckIncidentType(IncidentTypeClientData incidentType)
            : base(incidentType)
        {
            IncidentType = incidentType;
        }

        public int Code
        {
            get
            {
                return IncidentType.Code;
            }
            set { }
        }

        [GridControlRowInfoData(Visible = true, RepositoryType = typeof(RepositoryItemCheckEdit), Searchable = false, Width = 30)]
        public bool Checked
        {
            get
            {
                return check;
            }
            set
            {
                check = value;
            }
        }

        [GridControlRowInfoData(Visible = true, Width = 350)]
        public string Name
        {
            get
            {
                return IncidentType.FriendlyName + " (" + IncidentType.CustomCode + ")";                                     
            }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is GridControlDataCheckIncidentType)
            {
                result = IncidentType.Code.Equals(((GridControlDataCheckIncidentType)obj).IncidentType.Code);
            }
            return result;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    public class GridControlDataDispatchReportQuestion : GridControlData
    {
        public QuestionDispatchReportClientData Question { get; set; }

        public GridControlDataDispatchReportQuestion(QuestionDispatchReportClientData question)
            : base(question)
        {
            Question = question;
        }

        public int Code
        {
            get
            {
                return Question.Code;
            }
            set { }
        }

        [GridControlRowInfoData(Visible = true)]
        public string Name
        {
            get
            {
                return Question.Text;
            }
        }

        public int ReportQuestionCode
        {
            get;
            set;
        }

        public int ReportQuestionVersion
        {
            get;
            set;
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is GridControlDataDispatchReportQuestion)
            {
                result = Question.Code.Equals(((GridControlDataDispatchReportQuestion)obj).Question.Code);
            }
            return result;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}