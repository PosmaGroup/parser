using SmartCadControls;
using SmartCadGuiCommon.Controls;
namespace SmartCadGuiCommon
{
    partial class DispatchReportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        ///// <summary>
        ///// Clean up any resources being used.
        ///// </summary>
        ///// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControlMain = new DevExpress.XtraLayout.LayoutControl();
            this.checkEditAllIncidents = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditAllDep = new DevExpress.XtraEditors.CheckEdit();
            this.simpleButtonPreview = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonAccept = new DevExpress.XtraEditors.SimpleButton();
            this.selectUnSelectControlDevxMain = new SelectUnSelectControlDevx();
            this.gridControlExDepartment = new GridControlEx();
            this.gridViewExDepartment = new GridViewEx();
            this.textEditName = new DevExpress.XtraEditors.TextEdit();
            this.gridControlExIncident = new GridControlEx();
            this.gridViewExIncident = new GridViewEx();
            this.layoutControlGroupMain = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroupName = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupGridIncident = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemGridIncident = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroupGridDepartment = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemDepartment = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroupSelectUnSelect = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemSelectUnSelect = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemAccept = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemCancel = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItemButtons = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItemPreview = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlMain)).BeginInit();
            this.layoutControlMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditAllIncidents.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditAllDep.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExDepartment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExDepartment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExIncident)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExIncident)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupGridIncident)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemGridIncident)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupGridDepartment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDepartment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupSelectUnSelect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemSelectUnSelect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAccept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItemButtons)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPreview)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControlMain
            // 
            this.layoutControlMain.AllowCustomizationMenu = false;
            this.layoutControlMain.Controls.Add(this.checkEditAllIncidents);
            this.layoutControlMain.Controls.Add(this.checkEditAllDep);
            this.layoutControlMain.Controls.Add(this.simpleButtonPreview);
            this.layoutControlMain.Controls.Add(this.simpleButtonCancel);
            this.layoutControlMain.Controls.Add(this.simpleButtonAccept);
            this.layoutControlMain.Controls.Add(this.selectUnSelectControlDevxMain);
            this.layoutControlMain.Controls.Add(this.gridControlExDepartment);
            this.layoutControlMain.Controls.Add(this.textEditName);
            this.layoutControlMain.Controls.Add(this.gridControlExIncident);
            this.layoutControlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlMain.Location = new System.Drawing.Point(0, 0);
            this.layoutControlMain.Name = "layoutControlMain";
            this.layoutControlMain.Root = this.layoutControlGroupMain;
            this.layoutControlMain.Size = new System.Drawing.Size(892, 636);
            this.layoutControlMain.TabIndex = 0;
            this.layoutControlMain.Text = "layoutControl1";
            // 
            // checkEditAllIncidents
            // 
            this.checkEditAllIncidents.Location = new System.Drawing.Point(13, 84);
            this.checkEditAllIncidents.Name = "checkEditAllIncidents";
            this.checkEditAllIncidents.Properties.AllowGrayed = true;
            this.checkEditAllIncidents.Properties.Caption = "Seleccionar todos";
            this.checkEditAllIncidents.Size = new System.Drawing.Size(156, 19);
            this.checkEditAllIncidents.StyleController = this.layoutControlMain;
            this.checkEditAllIncidents.TabIndex = 12;
            this.checkEditAllIncidents.CheckedChanged += new System.EventHandler(this.checkEditAllIncidents_CheckedChanged);
            this.checkEditAllIncidents.CheckStateChanged += new System.EventHandler(this.checkEditAllIncidents_CheckStateChanged);
            // 
            // checkEditAllDep
            // 
            this.checkEditAllDep.Location = new System.Drawing.Point(456, 84);
            this.checkEditAllDep.Name = "checkEditAllDep";
            this.checkEditAllDep.Properties.AllowGrayed = true;
            this.checkEditAllDep.Properties.Caption = "Seleccionar todos";
            this.checkEditAllDep.Size = new System.Drawing.Size(164, 19);
            this.checkEditAllDep.StyleController = this.layoutControlMain;
            this.checkEditAllDep.TabIndex = 11;
            this.checkEditAllDep.CheckedChanged += new System.EventHandler(this.checkEditAllDep_CheckedChanged);
            this.checkEditAllDep.CheckStateChanged += new System.EventHandler(this.checkEditAllDep_CheckStateChanged);
            // 
            // simpleButtonPreview
            // 
            this.simpleButtonPreview.Location = new System.Drawing.Point(10, 594);
            this.simpleButtonPreview.Name = "simpleButtonPreview";
            this.simpleButtonPreview.Size = new System.Drawing.Size(90, 32);
            this.simpleButtonPreview.StyleController = this.layoutControlMain;
            this.simpleButtonPreview.TabIndex = 10;
            this.simpleButtonPreview.Text = "simpleButtonPreview";
            this.simpleButtonPreview.Click += new System.EventHandler(this.simpleButtonPreview_Click);
            // 
            // simpleButtonCancel
            // 
            this.simpleButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonCancel.Location = new System.Drawing.Point(800, 594);
            this.simpleButtonCancel.Name = "simpleButtonCancel";
            this.simpleButtonCancel.Size = new System.Drawing.Size(82, 32);
            this.simpleButtonCancel.StyleController = this.layoutControlMain;
            this.simpleButtonCancel.TabIndex = 9;
            this.simpleButtonCancel.Text = "simpleButtonCancel";
            // 
            // simpleButtonAccept
            // 
            this.simpleButtonAccept.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.simpleButtonAccept.Location = new System.Drawing.Point(714, 594);
            this.simpleButtonAccept.Name = "simpleButtonAccept";
            this.simpleButtonAccept.Size = new System.Drawing.Size(82, 32);
            this.simpleButtonAccept.StyleController = this.layoutControlMain;
            this.simpleButtonAccept.TabIndex = 8;
            this.simpleButtonAccept.Text = "simpleButtonAccept";
            this.simpleButtonAccept.Click += new System.EventHandler(this.simpleButtonAccept_Click);
            // 
            // selectUnSelectControlDevxMain
            // 
            this.selectUnSelectControlDevxMain.Location = new System.Drawing.Point(9, 326);
            this.selectUnSelectControlDevxMain.Name = "selectUnSelectControlDevxMain";
            this.selectUnSelectControlDevxMain.Size = new System.Drawing.Size(874, 265);
            this.selectUnSelectControlDevxMain.TabIndex = 7;
            // 
            // gridControlExDepartment
            // 
            this.gridControlExDepartment.EnableAutoFilter = false;
            this.gridControlExDepartment.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExDepartment.Location = new System.Drawing.Point(453, 107);
            this.gridControlExDepartment.MainView = this.gridViewExDepartment;
            this.gridControlExDepartment.Name = "gridControlExDepartment";
            this.gridControlExDepartment.Size = new System.Drawing.Size(429, 186);
            this.gridControlExDepartment.TabIndex = 6;
            this.gridControlExDepartment.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExDepartment});
            this.gridControlExDepartment.ViewTotalRows = false;
            // 
            // gridViewExDepartment
            // 
            this.gridViewExDepartment.AllowFocusedRowChanged = true;
            this.gridViewExDepartment.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewExDepartment.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExDepartment.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewExDepartment.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewExDepartment.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewExDepartment.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExDepartment.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewExDepartment.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewExDepartment.EnablePreviewLineForFocusedRow = false;
            this.gridViewExDepartment.GridControl = this.gridControlExDepartment;
            this.gridViewExDepartment.Name = "gridViewExDepartment";
            this.gridViewExDepartment.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExDepartment.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewExDepartment.OptionsView.ShowGroupPanel = false;
            this.gridViewExDepartment.ViewTotalRows = false;
            this.gridViewExDepartment.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewExDepartment_CellValueChanging);
            // 
            // textEditName
            // 
            this.textEditName.Location = new System.Drawing.Point(128, 30);
            this.textEditName.Name = "textEditName";
            this.textEditName.Size = new System.Drawing.Size(754, 20);
            this.textEditName.StyleController = this.layoutControlMain;
            this.textEditName.TabIndex = 4;
            this.textEditName.TextChanged += new System.EventHandler(this.CheckButtonsEventHandler);
            // 
            // gridControlExIncident
            // 
            this.gridControlExIncident.EnableAutoFilter = false;
            this.gridControlExIncident.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExIncident.Location = new System.Drawing.Point(10, 107);
            this.gridControlExIncident.MainView = this.gridViewExIncident;
            this.gridControlExIncident.Name = "gridControlExIncident";
            this.gridControlExIncident.Size = new System.Drawing.Size(429, 186);
            this.gridControlExIncident.TabIndex = 5;
            this.gridControlExIncident.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExIncident});
            this.gridControlExIncident.ViewTotalRows = false;
            // 
            // gridViewExIncident
            // 
            this.gridViewExIncident.AllowFocusedRowChanged = true;
            this.gridViewExIncident.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewExIncident.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExIncident.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewExIncident.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewExIncident.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewExIncident.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExIncident.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewExIncident.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewExIncident.EnablePreviewLineForFocusedRow = false;
            this.gridViewExIncident.GridControl = this.gridControlExIncident;
            this.gridViewExIncident.Name = "gridViewExIncident";
            this.gridViewExIncident.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExIncident.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewExIncident.OptionsView.ShowGroupPanel = false;
            this.gridViewExIncident.ViewTotalRows = false;
            this.gridViewExIncident.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewExIncident_CellValueChanging);
            // 
            // layoutControlGroupMain
            // 
            this.layoutControlGroupMain.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroupMain.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupName,
            this.layoutControlGroupGridIncident,
            this.layoutControlGroupGridDepartment,
            this.layoutControlGroupSelectUnSelect});
            this.layoutControlGroupMain.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupMain.Name = "Root";
            this.layoutControlGroupMain.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupMain.Size = new System.Drawing.Size(892, 636);
            this.layoutControlGroupMain.Text = "Root";
            this.layoutControlGroupMain.TextVisible = false;
            // 
            // layoutControlGroupName
            // 
            this.layoutControlGroupName.CustomizationFormText = "layoutControlGroupName";
            this.layoutControlGroupName.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemName});
            this.layoutControlGroupName.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupName.Name = "layoutControlGroupName";
            this.layoutControlGroupName.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupName.Size = new System.Drawing.Size(886, 54);
            this.layoutControlGroupName.Text = "layoutControlGroupName";
            // 
            // layoutControlItemName
            // 
            this.layoutControlItemName.Control = this.textEditName;
            this.layoutControlItemName.CustomizationFormText = "layoutControlItemName";
            this.layoutControlItemName.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemName.Name = "layoutControlItemName";
            this.layoutControlItemName.Size = new System.Drawing.Size(876, 24);
            this.layoutControlItemName.Text = "layoutControlItemName";
            this.layoutControlItemName.TextSize = new System.Drawing.Size(114, 13);
            // 
            // layoutControlGroupGridIncident
            // 
            this.layoutControlGroupGridIncident.CustomizationFormText = "layoutControlGroupGridIncident";
            this.layoutControlGroupGridIncident.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemGridIncident,
            this.layoutControlItem2,
            this.emptySpaceItem1});
            this.layoutControlGroupGridIncident.Location = new System.Drawing.Point(0, 54);
            this.layoutControlGroupGridIncident.Name = "layoutControlGroupGridIncident";
            this.layoutControlGroupGridIncident.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupGridIncident.Size = new System.Drawing.Size(443, 243);
            this.layoutControlGroupGridIncident.Text = "layoutControlGroupGridIncident";
            // 
            // layoutControlItemGridIncident
            // 
            this.layoutControlItemGridIncident.Control = this.gridControlExIncident;
            this.layoutControlItemGridIncident.CustomizationFormText = "layoutControlItemGridIncident";
            this.layoutControlItemGridIncident.Location = new System.Drawing.Point(0, 23);
            this.layoutControlItemGridIncident.Name = "layoutControlItemGridIncident";
            this.layoutControlItemGridIncident.Size = new System.Drawing.Size(433, 190);
            this.layoutControlItemGridIncident.Text = "layoutControlItemGridIncident";
            this.layoutControlItemGridIncident.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemGridIncident.TextToControlDistance = 0;
            this.layoutControlItemGridIncident.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.checkEditAllIncidents;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 2, 2);
            this.layoutControlItem2.Size = new System.Drawing.Size(166, 23);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(166, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(267, 23);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroupGridDepartment
            // 
            this.layoutControlGroupGridDepartment.CustomizationFormText = "layoutControlGroupGridDepartment";
            this.layoutControlGroupGridDepartment.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemDepartment,
            this.layoutControlItem1,
            this.emptySpaceItem2});
            this.layoutControlGroupGridDepartment.Location = new System.Drawing.Point(443, 54);
            this.layoutControlGroupGridDepartment.Name = "layoutControlGroupGridDepartment";
            this.layoutControlGroupGridDepartment.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupGridDepartment.Size = new System.Drawing.Size(443, 243);
            this.layoutControlGroupGridDepartment.Text = "layoutControlGroupGridDepartment";
            // 
            // layoutControlItemDepartment
            // 
            this.layoutControlItemDepartment.Control = this.gridControlExDepartment;
            this.layoutControlItemDepartment.CustomizationFormText = "layoutControlItemDepartment";
            this.layoutControlItemDepartment.Location = new System.Drawing.Point(0, 23);
            this.layoutControlItemDepartment.Name = "layoutControlItemDepartment";
            this.layoutControlItemDepartment.Size = new System.Drawing.Size(433, 190);
            this.layoutControlItemDepartment.Text = "layoutControlItemDepartment";
            this.layoutControlItemDepartment.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemDepartment.TextToControlDistance = 0;
            this.layoutControlItemDepartment.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.checkEditAllDep;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 2, 2);
            this.layoutControlItem1.Size = new System.Drawing.Size(174, 23);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(174, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(259, 23);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroupSelectUnSelect
            // 
            this.layoutControlGroupSelectUnSelect.CustomizationFormText = "layoutControlGroupSelectUnSelect";
            this.layoutControlGroupSelectUnSelect.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemSelectUnSelect,
            this.layoutControlItemAccept,
            this.layoutControlItemCancel,
            this.emptySpaceItemButtons,
            this.layoutControlItemPreview});
            this.layoutControlGroupSelectUnSelect.Location = new System.Drawing.Point(0, 297);
            this.layoutControlGroupSelectUnSelect.Name = "layoutControlGroupSelectUnSelect";
            this.layoutControlGroupSelectUnSelect.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupSelectUnSelect.Size = new System.Drawing.Size(886, 333);
            this.layoutControlGroupSelectUnSelect.Text = "layoutControlGroupSelectUnSelect";
            // 
            // layoutControlItemSelectUnSelect
            // 
            this.layoutControlItemSelectUnSelect.Control = this.selectUnSelectControlDevxMain;
            this.layoutControlItemSelectUnSelect.CustomizationFormText = "layoutControlItemSelectUnSelect";
            this.layoutControlItemSelectUnSelect.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemSelectUnSelect.Name = "layoutControlItemSelectUnSelect";
            this.layoutControlItemSelectUnSelect.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItemSelectUnSelect.Size = new System.Drawing.Size(876, 267);
            this.layoutControlItemSelectUnSelect.Text = "layoutControlItemSelectUnSelect";
            this.layoutControlItemSelectUnSelect.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemSelectUnSelect.TextToControlDistance = 0;
            this.layoutControlItemSelectUnSelect.TextVisible = false;
            // 
            // layoutControlItemAccept
            // 
            this.layoutControlItemAccept.Control = this.simpleButtonAccept;
            this.layoutControlItemAccept.CustomizationFormText = "layoutControlItemAccept";
            this.layoutControlItemAccept.Location = new System.Drawing.Point(704, 267);
            this.layoutControlItemAccept.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItemAccept.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItemAccept.Name = "layoutControlItemAccept";
            this.layoutControlItemAccept.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItemAccept.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemAccept.Text = "layoutControlItemAccept";
            this.layoutControlItemAccept.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemAccept.TextToControlDistance = 0;
            this.layoutControlItemAccept.TextVisible = false;
            // 
            // layoutControlItemCancel
            // 
            this.layoutControlItemCancel.Control = this.simpleButtonCancel;
            this.layoutControlItemCancel.CustomizationFormText = "layoutControlItemCancel";
            this.layoutControlItemCancel.Location = new System.Drawing.Point(790, 267);
            this.layoutControlItemCancel.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItemCancel.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItemCancel.Name = "layoutControlItemCancel";
            this.layoutControlItemCancel.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItemCancel.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemCancel.Text = "layoutControlItemCancel";
            this.layoutControlItemCancel.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemCancel.TextToControlDistance = 0;
            this.layoutControlItemCancel.TextVisible = false;
            // 
            // emptySpaceItemButtons
            // 
            this.emptySpaceItemButtons.AllowHotTrack = false;
            this.emptySpaceItemButtons.CustomizationFormText = "emptySpaceItemButtons";
            this.emptySpaceItemButtons.Location = new System.Drawing.Point(94, 267);
            this.emptySpaceItemButtons.Name = "emptySpaceItemButtons";
            this.emptySpaceItemButtons.Size = new System.Drawing.Size(610, 36);
            this.emptySpaceItemButtons.Text = "emptySpaceItemButtons";
            this.emptySpaceItemButtons.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItemPreview
            // 
            this.layoutControlItemPreview.Control = this.simpleButtonPreview;
            this.layoutControlItemPreview.CustomizationFormText = "layoutControlItemPreview";
            this.layoutControlItemPreview.Location = new System.Drawing.Point(0, 267);
            this.layoutControlItemPreview.MinSize = new System.Drawing.Size(91, 33);
            this.layoutControlItemPreview.Name = "layoutControlItemPreview";
            this.layoutControlItemPreview.Size = new System.Drawing.Size(94, 36);
            this.layoutControlItemPreview.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemPreview.Text = "layoutControlItemPreview";
            this.layoutControlItemPreview.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemPreview.TextToControlDistance = 0;
            this.layoutControlItemPreview.TextVisible = false;
            // 
            // DispatchReportForm
            // 
            this.AcceptButton = this.simpleButtonAccept;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.simpleButtonCancel;
            this.ClientSize = new System.Drawing.Size(892, 636);
            this.Controls.Add(this.layoutControlMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(900, 670);
            this.Name = "DispatchReportForm";
            this.Text = "DispatchReportForm";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlMain)).EndInit();
            this.layoutControlMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEditAllIncidents.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditAllDep.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExDepartment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExDepartment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExIncident)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExIncident)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupGridIncident)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemGridIncident)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupGridDepartment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDepartment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupSelectUnSelect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemSelectUnSelect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAccept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItemButtons)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPreview)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControlMain;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupMain;
        private DevExpress.XtraEditors.TextEdit textEditName;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemName;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupName;
        private GridControlEx gridControlExIncident;
        private GridViewEx gridViewExIncident;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemGridIncident;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupGridIncident;
        private SelectUnSelectControlDevx selectUnSelectControlDevxMain;
        private GridControlEx gridControlExDepartment;
        private GridViewEx gridViewExDepartment;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupGridDepartment;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDepartment;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupSelectUnSelect;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemSelectUnSelect;
        private DevExpress.XtraEditors.SimpleButton simpleButtonPreview;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCancel;
        private DevExpress.XtraEditors.SimpleButton simpleButtonAccept;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemAccept;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemCancel;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItemButtons;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemPreview;
        private DevExpress.XtraEditors.CheckEdit checkEditAllDep;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.CheckEdit checkEditAllIncidents;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
    }
}