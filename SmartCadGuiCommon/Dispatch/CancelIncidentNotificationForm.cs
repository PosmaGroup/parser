using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

using Smartmatic.SmartCad.Service;
using SmartCadCore.Core;
using SmartCadCore.ClientData;
using DevExpress.XtraEditors;
using SmartCadCore.Common;

namespace SmartCadGuiCommon
{
    public partial class CancelIncidentNotificationForm : XtraForm
    {
        IncidentNotificationClientData incidentNotification;

        public CancelIncidentNotificationForm(IncidentNotificationClientData incidentNotification)
            :this()
        {
            this.incidentNotification = incidentNotification;
        }

        public CancelIncidentNotificationForm()
        {
            InitializeComponent();
            LoadLanguage();
            this.buttonExAccept.Enabled = false;
        }

        public string Comments
        {
            get
            {
                return textBoxExComment.Text;
            }
        }

        private void textBoxExComment_TextChanged(object sender, EventArgs e)
        {
            if (this.textBoxExComment.Text.Trim().Length > 0)
            {
                this.buttonExAccept.Enabled = true;
            }
            else
            {
                this.buttonExAccept.Enabled = false;
            }
        }

        private void buttonExAccept_Click(object sender, EventArgs e)
        {
            if (incidentNotification != null)
            {
                BackgroundProcessForm processForm = new BackgroundProcessForm(this,
                    new MethodInfo[1]
                { 
                    GetType().GetMethod("buttonExAccept_Click_Help", BindingFlags.NonPublic | BindingFlags.Instance) 
                }, new object[1][] { new object[0] });
                processForm.ShowDialog();
            }
        }

        private void buttonExAccept_Click_Help()
        {
            this.incidentNotification.Detail = textBoxExComment.Text;
            this.incidentNotification.Status = IncidentNotificationStatusClientData.Cancelled;
            this.incidentNotification.LastStatusUpdate = ServerServiceClient.GetInstance().GetTime();
            this.incidentNotification.LastOperator = ServerServiceClient.GetInstance().OperatorClient.Code;
            ServerServiceClient.GetInstance().SaveOrUpdateClientData(this.incidentNotification);
        }

        private void CancelIncidentNotificationForm_Load(object sender, EventArgs e)
        {
            
            this.textBoxExComment.SelectAll();
            this.textBoxExComment.Focus();            
        }

        private void LoadLanguage()
        {
            this.Text = ResourceLoader.GetString2(this.Name);
            this.groupBoxComment.Text = ResourceLoader.GetString2("ReasonOrderCancellation");
            this.buttonExAccept.Text = ResourceLoader.GetString2("$Message.Accept");
            this.buttonExCancel.Text = ResourceLoader.GetString2("$Message.Cancel");
        }
    }
}
