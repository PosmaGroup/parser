using SmartCadControls.Controls;
using Smartmatic.SmartCad.Map;
namespace SmartCadGuiCommon
{
    partial class CrimeMapForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager();
            this.hideContainerLeft = new DevExpress.XtraBars.Docking.AutoHideContainer();
            this.dockPanelResults = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel2_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.searchFilterResultsBar1 = new SearchFilterResultsBar();
            this.dockPanel1 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.searchFilterBar1 = new SearchFilterBar();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.hideContainerLeft.SuspendLayout();
            this.dockPanelResults.SuspendLayout();
            this.dockPanel2_Container.SuspendLayout();
            this.dockPanel1.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            this.SuspendLayout();
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(795, 690);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(50, 20);
            this.layoutControlItem1.TextToControlDistance = 5;
            // 
            // dockManager1
            // 
            this.dockManager1.AutoHideContainers.AddRange(new DevExpress.XtraBars.Docking.AutoHideContainer[] {
            this.hideContainerLeft});
            this.dockManager1.Form = this;
            this.dockManager1.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanel1});
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "System.Windows.Forms.StatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            // 
            // hideContainerLeft
            // 
            this.hideContainerLeft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.hideContainerLeft.Controls.Add(this.dockPanelResults);
            this.hideContainerLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.hideContainerLeft.Location = new System.Drawing.Point(0, 0);
            this.hideContainerLeft.Name = "hideContainerLeft";
            this.hideContainerLeft.Size = new System.Drawing.Size(19, 692);
            // 
            // dockPanelResults
            // 
            this.dockPanelResults.Controls.Add(this.dockPanel2_Container);
            this.dockPanelResults.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanelResults.ID = new System.Guid("fd96e0ca-f576-47b8-8598-de62ee6865c5");
            this.dockPanelResults.Location = new System.Drawing.Point(0, 0);
            this.dockPanelResults.Name = "dockPanelResults";
            this.dockPanelResults.Options.AllowDockFill = false;
            this.dockPanelResults.Options.AllowDockTop = false;
            this.dockPanelResults.Options.ShowCloseButton = false;
            this.dockPanelResults.Options.ShowMaximizeButton = false;
            this.dockPanelResults.OriginalSize = new System.Drawing.Size(0, 0);
            this.dockPanelResults.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanelResults.SavedIndex = 1;
            this.dockPanelResults.Size = new System.Drawing.Size(248, 692);
            this.dockPanelResults.Text = "dockPanelGrid";
            this.dockPanelResults.Visibility = DevExpress.XtraBars.Docking.DockVisibility.AutoHide;
            // 
            // dockPanel2_Container
            // 
            this.dockPanel2_Container.Controls.Add(this.searchFilterResultsBar1);
            this.dockPanel2_Container.Location = new System.Drawing.Point(3, 25);
            this.dockPanel2_Container.Name = "dockPanel2_Container";
            this.dockPanel2_Container.Size = new System.Drawing.Size(242, 664);
            this.dockPanel2_Container.TabIndex = 0;
            // 
            // searchFilterResultsBar1
            // 
            this.searchFilterResultsBar1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.searchFilterResultsBar1.Location = new System.Drawing.Point(0, 0);
            this.searchFilterResultsBar1.Name = "searchFilterResultsBar1";
            this.searchFilterResultsBar1.Size = new System.Drawing.Size(242, 664);
            this.searchFilterResultsBar1.TabIndex = 0;
            // 
            // dockPanel1
            // 
            this.dockPanel1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.dockPanel1.Appearance.Options.UseBackColor = true;
            this.dockPanel1.Controls.Add(this.dockPanel1_Container);
            this.dockPanel1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanel1.ID = new System.Guid("471da6ac-a016-472d-9140-2353b31e4409");
            this.dockPanel1.Location = new System.Drawing.Point(19, 0);
            this.dockPanel1.Name = "dockPanel1";
            this.dockPanel1.Options.AllowDockFill = false;
            this.dockPanel1.Options.AllowDockTop = false;
            this.dockPanel1.Options.ShowCloseButton = false;
            this.dockPanel1.Options.ShowMaximizeButton = false;
            this.dockPanel1.OriginalSize = new System.Drawing.Size(200, 200);
            this.dockPanel1.Size = new System.Drawing.Size(200, 692);
            this.dockPanel1.Text = "Options";
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.searchFilterBar1);
            this.dockPanel1_Container.Location = new System.Drawing.Point(3, 29);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(194, 660);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // searchFilterBar1
            // 
            this.searchFilterBar1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.searchFilterBar1.Location = new System.Drawing.Point(0, 0);
            this.searchFilterBar1.Name = "searchFilterBar1";
            this.searchFilterBar1.Size = new System.Drawing.Size(194, 660);
            this.searchFilterBar1.TabIndex = 0;
            // 
            // CrimeMapForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(869, 692);
            this.Controls.Add(this.dockPanel1);
            this.Controls.Add(this.hideContainerLeft);
            this.Name = "CrimeMapForm";
            this.Text = "CrimeMapForm";
            this.Load += new System.EventHandler(this.CrimeMapForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.hideContainerLeft.ResumeLayout(false);
            this.dockPanelResults.ResumeLayout(false);
            this.dockPanel2_Container.ResumeLayout(false);
            this.dockPanel1.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public MapControlEx mapControlEx1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel1;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private SearchFilterBar searchFilterBar1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelResults;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel2_Container;
        private SearchFilterResultsBar searchFilterResultsBar1;
        private DevExpress.XtraBars.Docking.AutoHideContainer hideContainerLeft;
    }
}