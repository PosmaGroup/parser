﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Smartmatic.SmartCad.Service;
using System.Net;
using SmartCadCore.Core;
using DevExpress.Utils;
using SmartCadCore.ClientData;

using System.IO;
using DevExpress.XtraEditors;
using SmartCadCore.Common;

namespace SmartCadGuiCommon
{
    public partial class MapFormDevX : XtraForm
	{
		#region Const

		public static string Country = "VE";
		public static string City = "Ccs";
        private const string ZOOM_IN_CODE = "ZoomIn";
        private const string ZOOM_OUT_CODE = "ZoomOut";
        private const string PAN_CODE = "Pan";
        private const string CENTER_CODE = "Center";
        private const string SELECT_CODE = "Select";
        private const string SELECT_RECT_CODE = "SelectRect";
        private const string DISTANCE_CODE = "AddPolyline";
        public const string SELECT_RADIUS_CODE = "SelectRadius";

		#endregion

        #region Private Props
        UnitTrackingForm unitTrackingForm;
        UnitTrackingForm fleetControlForm;
        CrimeMapForm crimeMapForm;

        //COMMENTED IN VERSION 3.2.8 bataan
        CrimeMapForm heatMapForm;
        private DateTime syncServerDateTime;
        private TimeSpan sinceLoggedIn;
        private const int GLOBAL_TIMER_PERIOD = 10000;
        private System.Threading.Timer globalTimer;
        private TimeSpan ADD_TIMESPAN;
        private string labelUserCaption = "";
        private string labelConnectedCaption = "";
        private ServerServiceClient serverServiceClient;
        private bool trustedConnection;
        private NetworkCredential networkCredential;
        private IDictionary<string, ApplicationPreferenceClientData> globalApplicationPreferences;
        //private WorkSpaceLoader loaderAux;
        private string userPassword;
        #endregion

        #region Public Props
        public DefaultMapFormDevX defaultMapFormDevX;

        public string UserPassword
        {
            get
            {
                return userPassword;
            }
            set
            {
                userPassword = value;
            }
        }
        public ServerServiceClient ServerServiceClient
        {
            get
            {
                return this.serverServiceClient;
            }
            set
            {
                this.serverServiceClient = value;
            }
        }

        public NetworkCredential NetworkCredential
        {
            get
            {
                return this.networkCredential;
            }
            set
            {
                this.networkCredential = value;
            }
        }

        public IDictionary<string, ApplicationPreferenceClientData> GlobalApplicationPreference
        {
            get
            {
                return this.globalApplicationPreferences;
            }
            set
            {
                this.globalApplicationPreferences = value;
            }
        }

        public bool TrustedConnection
        {
            get
            {
                return this.trustedConnection;
            }
            set
            {
                this.trustedConnection = value;
            }
        }

        private Rectangle bounds;

        public Rectangle TempBounds
        {
            set
            {
                bounds = value;
            }
        }

        public int CurrentDeparmentSelected { get; set; }
    
        public List<GisActionEventArgs> InitializeActions = new List<GisActionEventArgs>();

        #endregion
        
        public MapFormDevX()
        {
            InitializeComponent();
            LoadLanguage();
            this.ribbonControl1.MinimizedChanged += new EventHandler(RibbonControl_MinimizedChanged);
        }

        void RibbonControl_MinimizedChanged(object sender, EventArgs e)
        {
            if (this.ribbonControl1.Minimized == true)
            {
                this.ribbonControl1.Minimized = false;
            }
        }
       
        private void LoadLanguage()
        {
            labelUserCaption = ResourceLoader.GetString2("User") + ": ";
            labelConnectedCaption = ResourceLoader.GetString2("Connected") + ": ";

            this.barButtonPan.Caption = ResourceLoader.GetString2("Pan");
            this.barButtonSelect.Caption = ResourceLoader.GetString2("Select");
            this.barButtonZoomIn.Caption = ResourceLoader.GetString2("ZoomIn");
            this.barButtonZoomOut.Caption = ResourceLoader.GetString2("ZoomOut");
            this.barButtonDistance.Caption = ResourceLoader.GetString2("Distance");
            this.barButtonClearMap.Caption = ResourceLoader.GetString2("ClearMap");
            this.barButtonCenter.Caption = ResourceLoader.GetString2("Center");
			this.barButtonItemEnd.Caption = ResourceLoader.GetString2("ToEnd");
			this.barButtonItemProcessing.Caption = ResourceLoader.GetString2("ToProcess");
			this.barButtonItemObservations.Caption = ResourceLoader.GetString2("Observations");
			this.barButtonItemCommands.Caption = ResourceLoader.GetString2("Commands");
			this.barButtonItemFollow.Caption = ResourceLoader.GetString2("Follow");
			this.barButtonItemSetPosition.Caption = ResourceLoader.GetString2("SetPosition");
            this.barButtonItemCrimeMap.Caption = ResourceLoader.GetString2("CrimeMapForm");

            //COMMENTED IN VERSION 3.2.8 bataan
            this.barButtonItemHeatMap.Caption = ResourceLoader.GetString2("HeatMapForm");
            this.ribbonPageGroupCrimeMaps.Text = ResourceLoader.GetString2("CrimeMaps");
            
            this.barButtonAddPosition.Caption = ResourceLoader.GetString2("AddPosition");
            this.barButtonAddRoute.Caption = ResourceLoader.GetString2("AddRoute");
			this.barButtonItemFleetControl.Caption = ResourceLoader.GetString2("FleetControl");

			barButtonItemUnitTracking.Caption = ResourceLoader.GetString2("HistoricTracks");
            ribbonPageGroupMapControls.Text = ResourceLoader.GetString2("Tools");
            ribbonPageGroupUnitTracking.Text = ResourceLoader.GetString2("Units");
            ribbonPageMapControls.Text = ResourceLoader.GetString2("Options");
			ribbonPageGroupLocation.Text = ResourceLoader.GetString2("Location");
			ribbonPageGroupAlerts.Text = ResourceLoader.GetString2("Alerts");

            this.barButtonItemHelp.SuperTip = BuildToolTip("ToolTip_HelpOnline");
            
        }

        public static SuperToolTip BuildToolTip(string keyToolTip)
        {
            SuperToolTip toolTip = null;
            string data = ResourceLoader.GetString2(keyToolTip);
            string[] dataElements = ApplicationUtil.GetSplittedResult(data);
            if (dataElements.Length > 0)
            {
                toolTip = new DevExpress.Utils.SuperToolTip();
                ToolTipTitleItem titleItem = new ToolTipTitleItem();
                titleItem.Text = dataElements[0];
                toolTip.Items.Add(titleItem);
            }
            if (dataElements.Length > 1)
            {
                ToolTipItem toolTipItem = new ToolTipItem();
                toolTipItem.Text = dataElements[1];
                toolTip.Items.Add(toolTipItem);
            }
            return toolTip;
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            syncServerDateTime = syncServerDateTime.AddMilliseconds((double)GLOBAL_TIMER_PERIOD);
            sinceLoggedIn = sinceLoggedIn.Add(ADD_TIMESPAN);
            FormUtil.InvokeRequired(ribbonStatusBar1, delegate
            {
                this.labelDate.Caption = ApplicationUtil.GetFormattedDate(ServerServiceClient.GetInstance().GetTime());
                this.labelConnected.Caption = labelConnectedCaption + Math.Truncate(sinceLoggedIn.TotalHours).ToString().PadLeft(2, '0') + ":" + sinceLoggedIn.Minutes.ToString().PadLeft(2, '0');
                ribbonStatusBar1.Refresh();
            });
        }

        private void FillStatusStrip()
        {
            labelUser.Caption = labelUserCaption + ServerServiceClient.OperatorClient.FirstName + " " + ServerServiceClient.OperatorClient.LastName;
        }

        private void MapFormDevX_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (EnsureLogout())
            {
                if (this.globalTimer != null)
                {
                    this.globalTimer.Dispose();
                    this.globalTimer = null;
                }
                this.serverServiceClient.CloseSession();
            }
            else
            {
                e.Cancel = true;
            }
        }

        private bool EnsureLogout()
        {
            DialogResult dialogResult = MessageForm.Show(ResourceLoader.GetString2("$Message.ExitApplication"), MessageFormType.Question);

            return dialogResult == DialogResult.Yes;
        }

        private void MapFormDevX_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                barButtonItemHelp_ItemClick(null, null);
            }
        }

        private void MapFormDevX_Load(object sender, EventArgs e)
        {
            this.Location = new Point(0, 0);
            this.Width = Screen.PrimaryScreen.WorkingArea.Width;
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
            this.Text = ResourceLoader.GetString2("MapFormDevXText");
            this.Icon = ResourceLoader.GetIcon("$Icon.Maps");
            ADD_TIMESPAN = new TimeSpan(0, 0, 0, 0, GLOBAL_TIMER_PERIOD);
            syncServerDateTime = ServerServiceClient.GetInstance().GetTime();
            sinceLoggedIn = TimeSpan.Zero;
            FillStatusStrip();
            this.labelConnected.Caption = labelConnectedCaption + "00:00";
            this.labelDate.Caption = ApplicationUtil.GetFormattedDate(syncServerDateTime);
    
            globalTimer = ApplicationUtil.RecurrentExecuteMethod(
             new SimpleDelegate(
             delegate
             {
                 timer_Tick(null, null);
             }), null, null, GLOBAL_TIMER_PERIOD, GLOBAL_TIMER_PERIOD);

            defaultMapFormDevX = new DefaultMapFormDevX();
            defaultMapFormDevX.MdiParent = this;
            defaultMapFormDevX.TrustedConnection = TrustedConnection;
            defaultMapFormDevX.NetworkCredential = NetworkCredential;
            defaultMapFormDevX.ServerServiceClient = serverServiceClient;

            xtraTabbedMdiManager.Pages[defaultMapFormDevX].ShowCloseButton = DevExpress.Utils.DefaultBoolean.False;
            

            int role = serverServiceClient.OperatorClient.Code;
            string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetOperatorWithAccessByApp, role, UserApplicationClientData.Map.Name);
            System.Collections.IList listAccess = ServerServiceClient.SearchClientObjects(hql);
            List<UserAccessClientData> uacc = listAccess.Cast<UserAccessClientData>().ToList();
            bool historicTracksEnable = uacc.Exists(ua => ua.UserPermission.UserResource.Name.Equals("HistoricTracks"));
			bool fleetControlEnable = uacc.Exists(ua => ua.UserPermission.UserResource.Name.Equals("FleetControl"));
            this.barButtonItemUnitTracking.Enabled = historicTracksEnable;
			this.barButtonItemFleetControl.Enabled = fleetControlEnable;

            defaultMapFormDevX.mapLayoutDevX1.mapLayersControl.MapControlEx.Initialized += new EventHandler(MapControlEx_Initialized);

            defaultMapFormDevX.Show();
            
            ServerServiceClient.GetInstance().OperatorClient.Password = UserPassword;
            MakeVisbleAddPostionButton();

            barButtonZoomIn.Tag = ZOOM_IN_CODE;
            barButtonZoomOut.Tag = ZOOM_OUT_CODE;
            barButtonPan.Tag = PAN_CODE;
            barButtonCenter.Tag = CENTER_CODE;
            barButtonSelect.Tag = SELECT_CODE;
            barButtonDistance.Tag = barButtonAddRoute.Tag = DISTANCE_CODE;
        }

        void MapControlEx_Initialized(object sender, EventArgs e)
        {
            FormUtil.InvokeRequired(this, delegate
            {
                foreach (GisActionEventArgs action in InitializeActions)
                {
                    if (action != null)
                    {
                        defaultMapFormDevX.ServerServiceClient_GisAction(null, action);
                        defaultMapFormDevX.mapLayoutDevX1.mapLayersControl.serverServiceClient_GisAction(null, action);
                        
                    }
                }
                defaultMapFormDevX.mapLayoutDevX1.mapLayersControl.MapIsInitialized = true;
            });
        }

        private void MakeVisbleAddPostionButton()
        {
            string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetOperatorAccessByAdmCctvFirstLevel, serverServiceClient.OperatorClient.Code, UserApplicationClientData.Administration.Name);

            bool haveAccess = (long)serverServiceClient.SearchBasicObject(hql) > 0;

            if (!haveAccess)
            {
                this.barButtonAddPosition.Enabled = false;
                this.barButtonAddPosition.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            }
        }

        /*private void LoadMap()
        {
            loaderAux = new WorkSpaceLoader(new MemoryStream(this.serverServiceClient.GetWorkSpaceStream()));
            loaderAux = MXSession.FormatWorkSpace(loaderAux, SmartCadConfiguration.MapsFolder);
        }*/  

        private void barButtonItemHelp_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                ApplicationUtil.LaunchHelp(SmartCadConfiguration.DistFolder + "/Help/MAPS Module.chm");
            }
            catch (Exception ex)
            {
                MessageForm.Show(ResourceLoader.GetString2("$Message.HelpNotInstalled"), ex);
            }
        }

        private void barButtonItemExit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }

        private void barButtonItemUnitTracking_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
			if (unitTrackingForm == null || unitTrackingForm.IsDisposed == true)
			{
                unitTrackingForm = new UnitTrackingForm(this, defaultMapFormDevX.mapLayoutDevX1.mapLayersControl.treeListSmartCad, true);
			}
			unitTrackingForm.MdiParent = this;
			unitTrackingForm.Activate();
			unitTrackingForm.Show();
		}

		private void barButtonItemFleetControl_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
			if (fleetControlForm == null || fleetControlForm.IsDisposed == true)
			{
                fleetControlForm = new UnitTrackingForm(this, defaultMapFormDevX.mapLayoutDevX1.mapLayersControl.treeListSmartCad, false);
			}
			fleetControlForm.MdiParent = this;
			fleetControlForm.Activate();
			fleetControlForm.Show();
		}

        private void MapFormDevX_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        //COMMENTED IN VERSION 3.2.8 bataan
        private void barButtonItemHeatMap_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (heatMapForm == null || heatMapForm.IsDisposed == true)
            {
                heatMapForm = new CrimeMapForm(defaultMapFormDevX.mapLayoutDevX1.mapLayersControl.TableIncidents.MapObjects.Values.ToList(), true);
            }
            heatMapForm.MdiParent = this;
            heatMapForm.Activate();
            heatMapForm.Show();
        }

        private void barButtonItemCrimeMap_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (crimeMapForm == null || crimeMapForm.IsDisposed == true)
            {
                crimeMapForm = new CrimeMapForm(defaultMapFormDevX.mapLayoutDevX1.mapLayersControl.TableIncidents.MapObjects.Values.ToList(),false);
            }
            crimeMapForm.MdiParent = this;
            crimeMapForm.Activate();
            crimeMapForm.Show();
        }

        private void barButtonMapTools_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (e.Item.Tag != null)
            {
                if (crimeMapForm!= null && this.ActiveMdiChild.GetHashCode() == crimeMapForm.GetHashCode())
                {
                    crimeMapForm.mapControlEx1.SetLeftButtonTool(e.Item.Tag.ToString());
                }
                // COMMENTED IN VERSION 3.2.8 FOR BATAAN
                else if (heatMapForm != null && this.ActiveMdiChild.GetHashCode() == heatMapForm.GetHashCode())
                {
                    heatMapForm.mapControlEx1.SetLeftButtonTool(e.Item.Tag.ToString());
                }
                else if (unitTrackingForm != null && this.ActiveMdiChild.GetHashCode() == unitTrackingForm.GetHashCode())
                {
                    unitTrackingForm.mapControlEx1.SetLeftButtonTool(e.Item.Tag.ToString());
                }
                else if (fleetControlForm != null && this.ActiveMdiChild.GetHashCode() == fleetControlForm.GetHashCode())
                {
                    fleetControlForm.mapControlEx1.SetLeftButtonTool(e.Item.Tag.ToString());
                }
            }
        }
    }
}
