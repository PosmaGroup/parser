using SmartCadControls;
using SmartCadControls.Controls;
using SmartCadGuiCommon.Controls;
using SmartCadCore.Core;
using SmartCadCore.Enums;
using Smartmatic.SmartCad.Map;

namespace SmartCadGuiCommon
{
	partial class UnitTrackingForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UnitTrackingForm));
            this.gridControlExUnits = new GridControlEx();
            this.gridViewExUnits = new GridViewEx();
            this.gridControlExUnitPositionHistory = new GridControlEx();
            this.gridViewExUnitPositionHistory = new GridViewEx();
            this.layoutControlLeftPanel = new DevExpress.XtraLayout.LayoutControl();
            this.mapLayersControl1 = new MapLayersControl();
            this.pauseButton = new DevExpress.XtraEditors.SimpleButton();
            this.fastForwardButton = new DevExpress.XtraEditors.SimpleButton();
            this.stopButton = new DevExpress.XtraEditors.SimpleButton();
            this.playButton = new DevExpress.XtraEditors.SimpleButton();
            this.searchUnitPositionHistory1 = new SearchUnitPositionHistory();
            this.layoutControlGroupLeftPanel = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroupUnits = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemGridControl = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupPositionHistorySearch = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemUnitPositionSearch = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupPositionHistoryResults = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemGridControlUnitPosition = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemPlay = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemFastForward = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemStop = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemPause = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupTreeView = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanelLeft = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.dockPanelRight = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel2_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.layoutControlRigthPanel = new DevExpress.XtraLayout.LayoutControl();
            this.labelControlActualSpeed = new DevExpress.XtraEditors.LabelControl();
            this.labelControlDepartmentType = new DevExpress.XtraEditors.LabelControl();
            this.labelControlWorkShift = new DevExpress.XtraEditors.LabelControl();
            this.labelControlDriver = new DevExpress.XtraEditors.LabelControl();
            this.labelControlAverageStopTime = new DevExpress.XtraEditors.LabelControl();
            this.labelControlZone = new DevExpress.XtraEditors.LabelControl();
            this.labelControlLastStopTime = new DevExpress.XtraEditors.LabelControl();
            this.labelControlSatellitesCount = new DevExpress.XtraEditors.LabelControl();
            this.labelControlLastStop = new DevExpress.XtraEditors.LabelControl();
            this.labelControlSchedule = new DevExpress.XtraEditors.LabelControl();
            this.labelControlDirection = new DevExpress.XtraEditors.LabelControl();
            this.labelControlAverageSpeed = new DevExpress.XtraEditors.LabelControl();
            this.labelControlCustomCode = new DevExpress.XtraEditors.LabelControl();
            this.labelControlLon = new DevExpress.XtraEditors.LabelControl();
            this.labelControlUnitId = new DevExpress.XtraEditors.LabelControl();
            this.labelControlSentTime = new DevExpress.XtraEditors.LabelControl();
            this.labelControlRunTime = new DevExpress.XtraEditors.LabelControl();
            this.labelControlLat = new DevExpress.XtraEditors.LabelControl();
            this.labelControlUnitType = new DevExpress.XtraEditors.LabelControl();
            this.labelControlMileage = new DevExpress.XtraEditors.LabelControl();
            this.labelControlStation = new DevExpress.XtraEditors.LabelControl();
            this.labelControlHasAlarm = new DevExpress.XtraEditors.LabelControl();
            this.labelControlRoute = new DevExpress.XtraEditors.LabelControl();
            this.labelControlNextStop = new DevExpress.XtraEditors.LabelControl();
            this.labelControlUnitStatus = new DevExpress.XtraEditors.LabelControl();
            this.layoutControlGroupRightPanel = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroupUnitDetails = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemDepartment = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemDriver = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemWorkShift = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemCustomCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemSchedule = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemUnitType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemUnitId = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemZone = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemStation = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemUnitStatus = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupRoute = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemRoute = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemMileage = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemRunTime = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemAverageSpeed = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemLastStop = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemLastStopTime = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemNextStop = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemAverageStopTime = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupGPSInfo = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemHasAlarm = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemLat = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemLon = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemDirection = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemActualSpeed = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemSatellitesCount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemSentDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.dockPanelDown = new DevExpress.XtraBars.Docking.DockPanel();
            this.controlContainer1 = new DevExpress.XtraBars.Docking.ControlContainer();
            this.layoutControlDownPanel = new DevExpress.XtraLayout.LayoutControl();
            this.labelControlAlertCount = new DevExpress.XtraEditors.LabelControl();
            this.memoEditObservations = new DevExpress.XtraEditors.MemoEdit();
            this.simpleButtonSelectedUnit = new SimpleButtonEx();
            this.simpleButtonAllAlerts = new SimpleButtonEx();
            this.gridControlExAlerts = new GridControlEx();
            this.gridViewExAlerts = new GridViewEx();
            this.layoutControlGroupDownPanel = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroupObservations = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemTextEditObs = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemGridControlAlerts = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemSimpleButtonAllAlerts = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemSimpleButtonSelectedUnit = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemAlertCount = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroupMainWindow = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemMapControl = new DevExpress.XtraLayout.LayoutControlItem();
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barButtonZoomIn = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonZoomOut = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonPan = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonCenter = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonAddPolygon = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonDistance = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonSelect = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonAddPosition = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonClearMap = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonSelectRect = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonSelectRadius = new DevExpress.XtraBars.BarButtonItem();
            this.comboBoxSelectTool = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.barButtonItemProcessing = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemEnd = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemObservations = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemFollow = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemCommands = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemSetPosition = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPageMapControls = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroupMapControls = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupLocation = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupUnits = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupAlerts = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.toolTipController1 = new DevExpress.Utils.ToolTipController(this.components);
            this.layoutControlMainWindow = new DevExpress.XtraLayout.LayoutControl();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExUnits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExUnits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExUnitPositionHistory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExUnitPositionHistory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlLeftPanel)).BeginInit();
            this.layoutControlLeftPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupLeftPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupUnits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupPositionHistorySearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemUnitPositionSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupPositionHistoryResults)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemGridControlUnitPosition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPlay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemFastForward)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemStop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPause)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupTreeView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.dockPanelLeft.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            this.dockPanelRight.SuspendLayout();
            this.dockPanel2_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlRigthPanel)).BeginInit();
            this.layoutControlRigthPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupRightPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupUnitDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDepartment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDriver)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWorkShift)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCustomCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemSchedule)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemUnitType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemUnitId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemZone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemStation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemUnitStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupRoute)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemRoute)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemMileage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemRunTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAverageSpeed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLastStop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLastStopTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemNextStop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAverageStopTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupGPSInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemHasAlarm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDirection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemActualSpeed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemSatellitesCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemSentDate)).BeginInit();
            this.dockPanelDown.SuspendLayout();
            this.controlContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlDownPanel)).BeginInit();
            this.layoutControlDownPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditObservations.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExAlerts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExAlerts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupDownPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupObservations)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTextEditObs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemGridControlAlerts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemSimpleButtonAllAlerts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemSimpleButtonSelectedUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAlertCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupMainWindow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemMapControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlMainWindow)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControlExUnits
            // 
            this.gridControlExUnits.EnableAutoFilter = false;
            this.gridControlExUnits.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExUnits.Location = new System.Drawing.Point(10, 30);
            this.gridControlExUnits.MainView = this.gridViewExUnits;
            this.gridControlExUnits.Name = "gridControlExUnits";
            this.gridControlExUnits.Size = new System.Drawing.Size(276, 158);
            this.gridControlExUnits.TabIndex = 0;
            this.gridControlExUnits.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExUnits});
            this.gridControlExUnits.ViewTotalRows = false;
            // 
            // gridViewExUnits
            // 
            this.gridViewExUnits.AllowFocusedRowChanged = true;
            this.gridViewExUnits.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewExUnits.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExUnits.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewExUnits.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewExUnits.Appearance.FooterPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewExUnits.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewExUnits.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewExUnits.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExUnits.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewExUnits.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewExUnits.EnablePreviewLineForFocusedRow = false;
            this.gridViewExUnits.GridControl = this.gridControlExUnits;
            this.gridViewExUnits.Name = "gridViewExUnits";
            this.gridViewExUnits.OptionsMenu.EnableFooterMenu = false;
            this.gridViewExUnits.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExUnits.OptionsSelection.MultiSelect = true;
            this.gridViewExUnits.OptionsView.ShowAutoFilterRow = true;
            this.gridViewExUnits.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewExUnits.OptionsView.ShowFooter = true;
            this.gridViewExUnits.ViewTotalRows = false;
            this.gridViewExUnits.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridViewExUnits_SelectionChanged);
            this.gridViewExUnits.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewExUnits_FocusedRowChanged);
            // 
            // gridControlExUnitPositionHistory
            // 
            this.gridControlExUnitPositionHistory.EnableAutoFilter = false;
            this.gridControlExUnitPositionHistory.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExUnitPositionHistory.Location = new System.Drawing.Point(10, 368);
            this.gridControlExUnitPositionHistory.MainView = this.gridViewExUnitPositionHistory;
            this.gridControlExUnitPositionHistory.Name = "gridControlExUnitPositionHistory";
            this.gridControlExUnitPositionHistory.Size = new System.Drawing.Size(276, 153);
            this.gridControlExUnitPositionHistory.TabIndex = 1;
            this.gridControlExUnitPositionHistory.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExUnitPositionHistory});
            this.gridControlExUnitPositionHistory.ViewTotalRows = false;
            // 
            // gridViewExUnitPositionHistory
            // 
            this.gridViewExUnitPositionHistory.AllowFocusedRowChanged = true;
            this.gridViewExUnitPositionHistory.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewExUnitPositionHistory.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExUnitPositionHistory.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewExUnitPositionHistory.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewExUnitPositionHistory.Appearance.FooterPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewExUnitPositionHistory.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewExUnitPositionHistory.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewExUnitPositionHistory.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExUnitPositionHistory.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewExUnitPositionHistory.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewExUnitPositionHistory.EnablePreviewLineForFocusedRow = false;
            this.gridViewExUnitPositionHistory.GridControl = this.gridControlExUnitPositionHistory;
            this.gridViewExUnitPositionHistory.GroupFormat = "{0}: [#image]{1}";
            this.gridViewExUnitPositionHistory.Name = "gridViewExUnitPositionHistory";
            this.gridViewExUnitPositionHistory.OptionsMenu.EnableFooterMenu = false;
            this.gridViewExUnitPositionHistory.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExUnitPositionHistory.OptionsView.ShowAutoFilterRow = true;
            this.gridViewExUnitPositionHistory.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewExUnitPositionHistory.OptionsView.ShowFooter = true;
            this.gridViewExUnitPositionHistory.ViewTotalRows = false;
            this.gridViewExUnitPositionHistory.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.gridViewExUnitPositionHistory_RowStyle);
            this.gridViewExUnitPositionHistory.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewExUnitPositionHistory_FocusedRowChanged);
            // 
            // layoutControlLeftPanel
            // 
            this.layoutControlLeftPanel.AllowCustomizationMenu = false;
            this.layoutControlLeftPanel.Controls.Add(this.mapLayersControl1);
            this.layoutControlLeftPanel.Controls.Add(this.gridControlExUnits);
            this.layoutControlLeftPanel.Controls.Add(this.pauseButton);
            this.layoutControlLeftPanel.Controls.Add(this.fastForwardButton);
            this.layoutControlLeftPanel.Controls.Add(this.stopButton);
            this.layoutControlLeftPanel.Controls.Add(this.playButton);
            this.layoutControlLeftPanel.Controls.Add(this.gridControlExUnitPositionHistory);
            this.layoutControlLeftPanel.Controls.Add(this.searchUnitPositionHistory1);
            this.layoutControlLeftPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlLeftPanel.Location = new System.Drawing.Point(0, 0);
            this.layoutControlLeftPanel.Name = "layoutControlLeftPanel";
            this.layoutControlLeftPanel.Root = this.layoutControlGroupLeftPanel;
            this.layoutControlLeftPanel.Size = new System.Drawing.Size(296, 751);
            this.layoutControlLeftPanel.TabIndex = 15;
            this.layoutControlLeftPanel.Text = "layoutControl1";
            // 
            // mapLayersControl1
            // 
            this.mapLayersControl1.CurrentShapeType = ShapeType.None;
            this.mapLayersControl1.GlobalApplicationPreference = null;
            this.mapLayersControl1.LaspointInRoute = null;
            this.mapLayersControl1.ListDepartmentByUser = null;
            this.mapLayersControl1.Location = new System.Drawing.Point(10, 584);
            this.mapLayersControl1.MapControlEx = null;
            this.mapLayersControl1.Name = "mapLayersControl1";
            this.mapLayersControl1.PermissionsUser = null;
            this.mapLayersControl1.Size = new System.Drawing.Size(276, 157);
            this.mapLayersControl1.TabIndex = 11;
            this.mapLayersControl1.TableIncidents = null;
            this.mapLayersControl1.TableRoutes = null;
            this.mapLayersControl1.TableStations = null;
            this.mapLayersControl1.TableStructs = null;
            this.mapLayersControl1.TableUnits = null;
            this.mapLayersControl1.TableZones = null;
            this.mapLayersControl1.TreeViewIncidents = null;
            this.mapLayersControl1.TreeViewRoutes = null;
            this.mapLayersControl1.TreeViewStations = null;
            this.mapLayersControl1.TreeViewStructs = null;
            this.mapLayersControl1.TreeViewUnits = null;
            this.mapLayersControl1.TreeViewZones = null;
            this.mapLayersControl1.UnitsList = null;
            // 
            // pauseButton
            // 
            this.pauseButton.Enabled = false;
            this.pauseButton.Image = ((System.Drawing.Image)(resources.GetObject("pauseButton.Image")));
            this.pauseButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.pauseButton.Location = new System.Drawing.Point(185, 525);
            this.pauseButton.Name = "pauseButton";
            this.pauseButton.Size = new System.Drawing.Size(30, 25);
            this.pauseButton.StyleController = this.layoutControlLeftPanel;
            this.pauseButton.TabIndex = 10;
            this.pauseButton.ToolTip = "Pause";
            this.pauseButton.Click += new System.EventHandler(this.pauseButton_Click);
            // 
            // fastForwardButton
            // 
            this.fastForwardButton.Enabled = false;
            this.fastForwardButton.Image = ((System.Drawing.Image)(resources.GetObject("fastForwardButton.Image")));
            this.fastForwardButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.fastForwardButton.Location = new System.Drawing.Point(253, 525);
            this.fastForwardButton.Name = "fastForwardButton";
            this.fastForwardButton.Size = new System.Drawing.Size(30, 25);
            this.fastForwardButton.StyleController = this.layoutControlLeftPanel;
            this.fastForwardButton.TabIndex = 8;
            this.fastForwardButton.ToolTip = "FastForward";
            this.fastForwardButton.Click += new System.EventHandler(this.fastForwardButton_Click);
            // 
            // stopButton
            // 
            this.stopButton.Enabled = false;
            this.stopButton.Image = ((System.Drawing.Image)(resources.GetObject("stopButton.Image")));
            this.stopButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.stopButton.Location = new System.Drawing.Point(219, 525);
            this.stopButton.Name = "stopButton";
            this.stopButton.Size = new System.Drawing.Size(30, 25);
            this.stopButton.StyleController = this.layoutControlLeftPanel;
            this.stopButton.TabIndex = 7;
            this.stopButton.ToolTip = "Stop";
            this.stopButton.Click += new System.EventHandler(this.stopButton_Click);
            // 
            // playButton
            // 
            this.playButton.Enabled = false;
            this.playButton.Image = ((System.Drawing.Image)(resources.GetObject("playButton.Image")));
            this.playButton.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.playButton.Location = new System.Drawing.Point(10, 525);
            this.playButton.Name = "playButton";
            this.playButton.Size = new System.Drawing.Size(171, 25);
            this.playButton.StyleController = this.layoutControlLeftPanel;
            this.playButton.TabIndex = 5;
            this.playButton.ToolTip = "Play";
            this.playButton.Click += new System.EventHandler(this.playButton_Click);
            // 
            // searchUnitPositionHistory1
            // 
            this.searchUnitPositionHistory1.Location = new System.Drawing.Point(10, 222);
            this.searchUnitPositionHistory1.Name = "searchUnitPositionHistory1";
            this.searchUnitPositionHistory1.Size = new System.Drawing.Size(276, 112);
            this.searchUnitPositionHistory1.TabIndex = 4;
            // 
            // layoutControlGroupLeftPanel
            // 
            this.layoutControlGroupLeftPanel.CustomizationFormText = "layoutControlGroupLeftPanel";
            this.layoutControlGroupLeftPanel.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupUnits,
            this.layoutControlGroupPositionHistorySearch,
            this.layoutControlGroupPositionHistoryResults,
            this.layoutControlGroupTreeView});
            this.layoutControlGroupLeftPanel.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupLeftPanel.Name = "layoutControlGroupLeftPanel";
            this.layoutControlGroupLeftPanel.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupLeftPanel.Size = new System.Drawing.Size(296, 751);
            this.layoutControlGroupLeftPanel.Text = "layoutControlGroupLeftPanel";
            this.layoutControlGroupLeftPanel.TextVisible = false;
            // 
            // layoutControlGroupUnits
            // 
            this.layoutControlGroupUnits.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroupUnits.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroupUnits.CustomizationFormText = "layoutControlGroupUnits";
            this.layoutControlGroupUnits.ExpandButtonLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroupUnits.ExpandButtonVisible = true;
            this.layoutControlGroupUnits.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemGridControl});
            this.layoutControlGroupUnits.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupUnits.Name = "layoutControlGroupUnits";
            this.layoutControlGroupUnits.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupUnits.Size = new System.Drawing.Size(290, 192);
            this.layoutControlGroupUnits.Text = "layoutControlGroupUnits";
            // 
            // layoutControlItemGridControl
            // 
            this.layoutControlItemGridControl.Control = this.gridControlExUnits;
            this.layoutControlItemGridControl.CustomizationFormText = "layoutControlItemGridControl";
            this.layoutControlItemGridControl.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemGridControl.MinSize = new System.Drawing.Size(111, 31);
            this.layoutControlItemGridControl.Name = "layoutControlItemGridControl";
            this.layoutControlItemGridControl.Size = new System.Drawing.Size(280, 162);
            this.layoutControlItemGridControl.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemGridControl.Text = "layoutControlItemGridControl";
            this.layoutControlItemGridControl.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemGridControl.TextToControlDistance = 0;
            this.layoutControlItemGridControl.TextVisible = false;
            // 
            // layoutControlGroupPositionHistorySearch
            // 
            this.layoutControlGroupPositionHistorySearch.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroupPositionHistorySearch.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroupPositionHistorySearch.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlGroupPositionHistorySearch.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroupPositionHistorySearch.CustomizationFormText = "layoutControlGroupPositionHistorySearch";
            this.layoutControlGroupPositionHistorySearch.ExpandButtonLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroupPositionHistorySearch.ExpandButtonVisible = true;
            this.layoutControlGroupPositionHistorySearch.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemUnitPositionSearch});
            this.layoutControlGroupPositionHistorySearch.Location = new System.Drawing.Point(0, 192);
            this.layoutControlGroupPositionHistorySearch.Name = "layoutControlGroupPositionHistorySearch";
            this.layoutControlGroupPositionHistorySearch.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupPositionHistorySearch.Size = new System.Drawing.Size(290, 146);
            this.layoutControlGroupPositionHistorySearch.Text = "layoutControlGroupPositionHistorySearch";
            // 
            // layoutControlItemUnitPositionSearch
            // 
            this.layoutControlItemUnitPositionSearch.Control = this.searchUnitPositionHistory1;
            this.layoutControlItemUnitPositionSearch.CustomizationFormText = "layoutControlItemUnitPositionSearch";
            this.layoutControlItemUnitPositionSearch.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemUnitPositionSearch.MinSize = new System.Drawing.Size(1, 1);
            this.layoutControlItemUnitPositionSearch.Name = "layoutControlItemUnitPositionSearch";
            this.layoutControlItemUnitPositionSearch.Size = new System.Drawing.Size(280, 116);
            this.layoutControlItemUnitPositionSearch.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemUnitPositionSearch.Text = "layoutControlItemUnitPositionSearch";
            this.layoutControlItemUnitPositionSearch.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemUnitPositionSearch.TextToControlDistance = 0;
            this.layoutControlItemUnitPositionSearch.TextVisible = false;
            // 
            // layoutControlGroupPositionHistoryResults
            // 
            this.layoutControlGroupPositionHistoryResults.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroupPositionHistoryResults.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroupPositionHistoryResults.CustomizationFormText = "layoutControlGroupPositionHistoryResults";
            this.layoutControlGroupPositionHistoryResults.ExpandButtonLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroupPositionHistoryResults.ExpandButtonVisible = true;
            this.layoutControlGroupPositionHistoryResults.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemGridControlUnitPosition,
            this.layoutControlItemPlay,
            this.layoutControlItemFastForward,
            this.layoutControlItemStop,
            this.layoutControlItemPause});
            this.layoutControlGroupPositionHistoryResults.Location = new System.Drawing.Point(0, 338);
            this.layoutControlGroupPositionHistoryResults.Name = "layoutControlGroupPositionHistoryResults";
            this.layoutControlGroupPositionHistoryResults.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupPositionHistoryResults.Size = new System.Drawing.Size(290, 216);
            this.layoutControlGroupPositionHistoryResults.Text = "layoutControlGroupPositionHistoryResults";
            // 
            // layoutControlItemGridControlUnitPosition
            // 
            this.layoutControlItemGridControlUnitPosition.Control = this.gridControlExUnitPositionHistory;
            this.layoutControlItemGridControlUnitPosition.CustomizationFormText = "layoutControlItemGridControlUnitPosition";
            this.layoutControlItemGridControlUnitPosition.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemGridControlUnitPosition.MinSize = new System.Drawing.Size(111, 31);
            this.layoutControlItemGridControlUnitPosition.Name = "layoutControlItemGridControlUnitPosition";
            this.layoutControlItemGridControlUnitPosition.Size = new System.Drawing.Size(280, 157);
            this.layoutControlItemGridControlUnitPosition.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemGridControlUnitPosition.Text = "layoutControlItemGridControlUnitPosition";
            this.layoutControlItemGridControlUnitPosition.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemGridControlUnitPosition.TextToControlDistance = 0;
            this.layoutControlItemGridControlUnitPosition.TextVisible = false;
            // 
            // layoutControlItemPlay
            // 
            this.layoutControlItemPlay.Control = this.playButton;
            this.layoutControlItemPlay.CustomizationFormText = "layoutControlItemPlay";
            this.layoutControlItemPlay.Location = new System.Drawing.Point(0, 157);
            this.layoutControlItemPlay.MaxSize = new System.Drawing.Size(175, 29);
            this.layoutControlItemPlay.MinSize = new System.Drawing.Size(175, 29);
            this.layoutControlItemPlay.Name = "layoutControlItemPlay";
            this.layoutControlItemPlay.Size = new System.Drawing.Size(175, 29);
            this.layoutControlItemPlay.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemPlay.Text = "layoutControlItemPlay";
            this.layoutControlItemPlay.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemPlay.TextToControlDistance = 0;
            this.layoutControlItemPlay.TextVisible = false;
            // 
            // layoutControlItemFastForward
            // 
            this.layoutControlItemFastForward.Control = this.fastForwardButton;
            this.layoutControlItemFastForward.CustomizationFormText = "layoutControlItemFastForward";
            this.layoutControlItemFastForward.Location = new System.Drawing.Point(243, 157);
            this.layoutControlItemFastForward.MaxSize = new System.Drawing.Size(34, 29);
            this.layoutControlItemFastForward.MinSize = new System.Drawing.Size(34, 29);
            this.layoutControlItemFastForward.Name = "layoutControlItemFastForward";
            this.layoutControlItemFastForward.Size = new System.Drawing.Size(37, 29);
            this.layoutControlItemFastForward.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemFastForward.Text = "layoutControlItemFastForward";
            this.layoutControlItemFastForward.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemFastForward.TextToControlDistance = 0;
            this.layoutControlItemFastForward.TextVisible = false;
            // 
            // layoutControlItemStop
            // 
            this.layoutControlItemStop.Control = this.stopButton;
            this.layoutControlItemStop.CustomizationFormText = "layoutControlItemStop";
            this.layoutControlItemStop.Location = new System.Drawing.Point(209, 157);
            this.layoutControlItemStop.MaxSize = new System.Drawing.Size(34, 29);
            this.layoutControlItemStop.MinSize = new System.Drawing.Size(34, 29);
            this.layoutControlItemStop.Name = "layoutControlItemStop";
            this.layoutControlItemStop.Size = new System.Drawing.Size(34, 29);
            this.layoutControlItemStop.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemStop.Text = "layoutControlItemStop";
            this.layoutControlItemStop.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemStop.TextToControlDistance = 0;
            this.layoutControlItemStop.TextVisible = false;
            // 
            // layoutControlItemPause
            // 
            this.layoutControlItemPause.Control = this.pauseButton;
            this.layoutControlItemPause.CustomizationFormText = "layoutControlItem11";
            this.layoutControlItemPause.Location = new System.Drawing.Point(175, 157);
            this.layoutControlItemPause.MaxSize = new System.Drawing.Size(34, 29);
            this.layoutControlItemPause.MinSize = new System.Drawing.Size(34, 29);
            this.layoutControlItemPause.Name = "layoutControlItemPause";
            this.layoutControlItemPause.Size = new System.Drawing.Size(34, 29);
            this.layoutControlItemPause.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemPause.Text = "layoutControlItemPause";
            this.layoutControlItemPause.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemPause.TextToControlDistance = 0;
            this.layoutControlItemPause.TextVisible = false;
            // 
            // layoutControlGroupTreeView
            // 
            this.layoutControlGroupTreeView.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroupTreeView.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroupTreeView.CustomizationFormText = "layoutControlGroup9";
            this.layoutControlGroupTreeView.ExpandButtonLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroupTreeView.ExpandButtonVisible = true;
            this.layoutControlGroupTreeView.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.layoutControlGroupTreeView.Location = new System.Drawing.Point(0, 554);
            this.layoutControlGroupTreeView.Name = "layoutControlGroupTreeView";
            this.layoutControlGroupTreeView.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupTreeView.Size = new System.Drawing.Size(290, 191);
            this.layoutControlGroupTreeView.Text = "layoutControlGroupTreeView";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.mapLayersControl1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItemMapLayersControl";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(280, 161);
            this.layoutControlItem1.Text = "layoutControlItemMapLayersControl";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // dockManager1
            // 
            this.dockManager1.Form = this;
            this.dockManager1.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanelLeft,
            this.dockPanelRight,
            this.dockPanelDown});
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "System.Windows.Forms.StatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            // 
            // dockPanelLeft
            // 
            this.dockPanelLeft.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(239)))));
            this.dockPanelLeft.Appearance.Options.UseBackColor = true;
            this.dockPanelLeft.Controls.Add(this.dockPanel1_Container);
            this.dockPanelLeft.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanelLeft.ID = new System.Guid("93956f79-4021-4b13-9431-072fa8d5e6a8");
            this.dockPanelLeft.Location = new System.Drawing.Point(0, 0);
            this.dockPanelLeft.Name = "dockPanelLeft";
            this.dockPanelLeft.Options.AllowDockBottom = false;
            this.dockPanelLeft.Options.AllowDockFill = false;
            this.dockPanelLeft.Options.AllowDockTop = false;
            this.dockPanelLeft.Options.AllowFloating = false;
            this.dockPanelLeft.Options.FloatOnDblClick = false;
            this.dockPanelLeft.Options.ShowCloseButton = false;
            this.dockPanelLeft.Options.ShowMaximizeButton = false;
            this.dockPanelLeft.OriginalSize = new System.Drawing.Size(304, 200);
            this.dockPanelLeft.Size = new System.Drawing.Size(304, 778);
            this.dockPanelLeft.Text = "dockPaneLeft";
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.layoutControlLeftPanel);
            this.dockPanel1_Container.Location = new System.Drawing.Point(4, 23);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(296, 751);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // dockPanelRight
            // 
            this.dockPanelRight.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(239)))));
            this.dockPanelRight.Appearance.Options.UseBackColor = true;
            this.dockPanelRight.Controls.Add(this.dockPanel2_Container);
            this.dockPanelRight.Dock = DevExpress.XtraBars.Docking.DockingStyle.Right;
            this.dockPanelRight.ID = new System.Guid("9a6fcf59-facd-477c-95b4-c92db9c0bac3");
            this.dockPanelRight.Location = new System.Drawing.Point(1070, 0);
            this.dockPanelRight.Name = "dockPanelRight";
            this.dockPanelRight.Options.AllowDockBottom = false;
            this.dockPanelRight.Options.AllowDockFill = false;
            this.dockPanelRight.Options.AllowDockLeft = false;
            this.dockPanelRight.Options.AllowDockTop = false;
            this.dockPanelRight.Options.AllowFloating = false;
            this.dockPanelRight.Options.FloatOnDblClick = false;
            this.dockPanelRight.Options.ShowCloseButton = false;
            this.dockPanelRight.Options.ShowMaximizeButton = false;
            this.dockPanelRight.OriginalSize = new System.Drawing.Size(200, 200);
            this.dockPanelRight.Size = new System.Drawing.Size(200, 778);
            this.dockPanelRight.Text = "dockPanelRight";
            // 
            // dockPanel2_Container
            // 
            this.dockPanel2_Container.Controls.Add(this.layoutControlRigthPanel);
            this.dockPanel2_Container.Location = new System.Drawing.Point(4, 23);
            this.dockPanel2_Container.Name = "dockPanel2_Container";
            this.dockPanel2_Container.Size = new System.Drawing.Size(192, 751);
            this.dockPanel2_Container.TabIndex = 0;
            // 
            // layoutControlRigthPanel
            // 
            this.layoutControlRigthPanel.Controls.Add(this.labelControlActualSpeed);
            this.layoutControlRigthPanel.Controls.Add(this.labelControlDepartmentType);
            this.layoutControlRigthPanel.Controls.Add(this.labelControlWorkShift);
            this.layoutControlRigthPanel.Controls.Add(this.labelControlDriver);
            this.layoutControlRigthPanel.Controls.Add(this.labelControlAverageStopTime);
            this.layoutControlRigthPanel.Controls.Add(this.labelControlZone);
            this.layoutControlRigthPanel.Controls.Add(this.labelControlLastStopTime);
            this.layoutControlRigthPanel.Controls.Add(this.labelControlSatellitesCount);
            this.layoutControlRigthPanel.Controls.Add(this.labelControlLastStop);
            this.layoutControlRigthPanel.Controls.Add(this.labelControlSchedule);
            this.layoutControlRigthPanel.Controls.Add(this.labelControlDirection);
            this.layoutControlRigthPanel.Controls.Add(this.labelControlAverageSpeed);
            this.layoutControlRigthPanel.Controls.Add(this.labelControlCustomCode);
            this.layoutControlRigthPanel.Controls.Add(this.labelControlLon);
            this.layoutControlRigthPanel.Controls.Add(this.labelControlUnitId);
            this.layoutControlRigthPanel.Controls.Add(this.labelControlSentTime);
            this.layoutControlRigthPanel.Controls.Add(this.labelControlRunTime);
            this.layoutControlRigthPanel.Controls.Add(this.labelControlLat);
            this.layoutControlRigthPanel.Controls.Add(this.labelControlUnitType);
            this.layoutControlRigthPanel.Controls.Add(this.labelControlMileage);
            this.layoutControlRigthPanel.Controls.Add(this.labelControlStation);
            this.layoutControlRigthPanel.Controls.Add(this.labelControlHasAlarm);
            this.layoutControlRigthPanel.Controls.Add(this.labelControlRoute);
            this.layoutControlRigthPanel.Controls.Add(this.labelControlNextStop);
            this.layoutControlRigthPanel.Controls.Add(this.labelControlUnitStatus);
            this.layoutControlRigthPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlRigthPanel.Location = new System.Drawing.Point(0, 0);
            this.layoutControlRigthPanel.Name = "layoutControlRigthPanel";
            this.layoutControlRigthPanel.Root = this.layoutControlGroupRightPanel;
            this.layoutControlRigthPanel.Size = new System.Drawing.Size(192, 751);
            this.layoutControlRigthPanel.TabIndex = 0;
            this.layoutControlRigthPanel.Text = "layoutControl4";
            // 
            // labelControlActualSpeed
            // 
            this.labelControlActualSpeed.Location = new System.Drawing.Point(10, 464);
            this.labelControlActualSpeed.Name = "labelControlActualSpeed";
            this.labelControlActualSpeed.Size = new System.Drawing.Size(117, 13);
            this.labelControlActualSpeed.StyleController = this.layoutControlRigthPanel;
            this.labelControlActualSpeed.TabIndex = 4;
            this.labelControlActualSpeed.Text = "labelControlActualSpeed";
            // 
            // labelControlDepartmentType
            // 
            this.labelControlDepartmentType.Location = new System.Drawing.Point(10, 30);
            this.labelControlDepartmentType.Name = "labelControlDepartmentType";
            this.labelControlDepartmentType.Size = new System.Drawing.Size(138, 13);
            this.labelControlDepartmentType.StyleController = this.layoutControlRigthPanel;
            this.labelControlDepartmentType.TabIndex = 5;
            this.labelControlDepartmentType.Text = "labelControlDepartmentType";
            // 
            // labelControlWorkShift
            // 
            this.labelControlWorkShift.Location = new System.Drawing.Point(10, 64);
            this.labelControlWorkShift.Name = "labelControlWorkShift";
            this.labelControlWorkShift.Size = new System.Drawing.Size(104, 13);
            this.labelControlWorkShift.StyleController = this.layoutControlRigthPanel;
            this.labelControlWorkShift.TabIndex = 4;
            this.labelControlWorkShift.Text = "labelControlWorkShift";
            // 
            // labelControlDriver
            // 
            this.labelControlDriver.Location = new System.Drawing.Point(10, 47);
            this.labelControlDriver.Name = "labelControlDriver";
            this.labelControlDriver.Size = new System.Drawing.Size(86, 13);
            this.labelControlDriver.StyleController = this.layoutControlRigthPanel;
            this.labelControlDriver.TabIndex = 4;
            this.labelControlDriver.Text = "labelControlDriver";
            // 
            // labelControlAverageStopTime
            // 
            this.labelControlAverageStopTime.Location = new System.Drawing.Point(10, 349);
            this.labelControlAverageStopTime.Name = "labelControlAverageStopTime";
            this.labelControlAverageStopTime.Size = new System.Drawing.Size(142, 13);
            this.labelControlAverageStopTime.StyleController = this.layoutControlRigthPanel;
            this.labelControlAverageStopTime.TabIndex = 4;
            this.labelControlAverageStopTime.Text = "labelControlAverageStopTime";
            // 
            // labelControlZone
            // 
            this.labelControlZone.Location = new System.Drawing.Point(10, 166);
            this.labelControlZone.Name = "labelControlZone";
            this.labelControlZone.Size = new System.Drawing.Size(81, 13);
            this.labelControlZone.StyleController = this.layoutControlRigthPanel;
            this.labelControlZone.TabIndex = 4;
            this.labelControlZone.Text = "labelControlZone";
            // 
            // labelControlLastStopTime
            // 
            this.labelControlLastStopTime.Location = new System.Drawing.Point(10, 315);
            this.labelControlLastStopTime.Name = "labelControlLastStopTime";
            this.labelControlLastStopTime.Size = new System.Drawing.Size(121, 13);
            this.labelControlLastStopTime.StyleController = this.layoutControlRigthPanel;
            this.labelControlLastStopTime.TabIndex = 5;
            this.labelControlLastStopTime.Text = "labelControlLastStopTime";
            // 
            // labelControlSatellitesCount
            // 
            this.labelControlSatellitesCount.Location = new System.Drawing.Point(10, 498);
            this.labelControlSatellitesCount.Name = "labelControlSatellitesCount";
            this.labelControlSatellitesCount.Size = new System.Drawing.Size(129, 13);
            this.labelControlSatellitesCount.StyleController = this.layoutControlRigthPanel;
            this.labelControlSatellitesCount.TabIndex = 4;
            this.labelControlSatellitesCount.Text = "labelControlSatellitesCount";
            // 
            // labelControlLastStop
            // 
            this.labelControlLastStop.Location = new System.Drawing.Point(10, 298);
            this.labelControlLastStop.Name = "labelControlLastStop";
            this.labelControlLastStop.Size = new System.Drawing.Size(99, 13);
            this.labelControlLastStop.StyleController = this.layoutControlRigthPanel;
            this.labelControlLastStop.TabIndex = 4;
            this.labelControlLastStop.Text = "labelControlLastStop";
            // 
            // labelControlSchedule
            // 
            this.labelControlSchedule.Location = new System.Drawing.Point(10, 98);
            this.labelControlSchedule.Name = "labelControlSchedule";
            this.labelControlSchedule.Size = new System.Drawing.Size(100, 13);
            this.labelControlSchedule.StyleController = this.layoutControlRigthPanel;
            this.labelControlSchedule.TabIndex = 5;
            this.labelControlSchedule.Text = "labelControlSchedule";
            // 
            // labelControlDirection
            // 
            this.labelControlDirection.Location = new System.Drawing.Point(10, 447);
            this.labelControlDirection.Name = "labelControlDirection";
            this.labelControlDirection.Size = new System.Drawing.Size(99, 13);
            this.labelControlDirection.StyleController = this.layoutControlRigthPanel;
            this.labelControlDirection.TabIndex = 5;
            this.labelControlDirection.Text = "labelControlDirection";
            // 
            // labelControlAverageSpeed
            // 
            this.labelControlAverageSpeed.Location = new System.Drawing.Point(10, 281);
            this.labelControlAverageSpeed.Name = "labelControlAverageSpeed";
            this.labelControlAverageSpeed.Size = new System.Drawing.Size(128, 13);
            this.labelControlAverageSpeed.StyleController = this.layoutControlRigthPanel;
            this.labelControlAverageSpeed.TabIndex = 4;
            this.labelControlAverageSpeed.Text = "labelControlAverageSpeed";
            // 
            // labelControlCustomCode
            // 
            this.labelControlCustomCode.Location = new System.Drawing.Point(10, 81);
            this.labelControlCustomCode.Name = "labelControlCustomCode";
            this.labelControlCustomCode.Size = new System.Drawing.Size(118, 13);
            this.labelControlCustomCode.StyleController = this.layoutControlRigthPanel;
            this.labelControlCustomCode.TabIndex = 4;
            this.labelControlCustomCode.Text = "labelControlCustomCode";
            // 
            // labelControlLon
            // 
            this.labelControlLon.Location = new System.Drawing.Point(10, 430);
            this.labelControlLon.Name = "labelControlLon";
            this.labelControlLon.Size = new System.Drawing.Size(74, 13);
            this.labelControlLon.StyleController = this.layoutControlRigthPanel;
            this.labelControlLon.TabIndex = 4;
            this.labelControlLon.Text = "labelControlLon";
            // 
            // labelControlUnitId
            // 
            this.labelControlUnitId.Location = new System.Drawing.Point(10, 132);
            this.labelControlUnitId.Name = "labelControlUnitId";
            this.labelControlUnitId.Size = new System.Drawing.Size(86, 13);
            this.labelControlUnitId.StyleController = this.layoutControlRigthPanel;
            this.labelControlUnitId.TabIndex = 4;
            this.labelControlUnitId.Text = "labelControlUnitId";
            // 
            // labelControlSentTime
            // 
            this.labelControlSentTime.Location = new System.Drawing.Point(10, 481);
            this.labelControlSentTime.Name = "labelControlSentTime";
            this.labelControlSentTime.Size = new System.Drawing.Size(101, 13);
            this.labelControlSentTime.StyleController = this.layoutControlRigthPanel;
            this.labelControlSentTime.TabIndex = 4;
            this.labelControlSentTime.Text = "labelControlSentTime";
            // 
            // labelControlRunTime
            // 
            this.labelControlRunTime.Location = new System.Drawing.Point(10, 264);
            this.labelControlRunTime.Name = "labelControlRunTime";
            this.labelControlRunTime.Size = new System.Drawing.Size(98, 13);
            this.labelControlRunTime.StyleController = this.layoutControlRigthPanel;
            this.labelControlRunTime.TabIndex = 4;
            this.labelControlRunTime.Text = "labelControlRunTime";
            // 
            // labelControlLat
            // 
            this.labelControlLat.Location = new System.Drawing.Point(10, 413);
            this.labelControlLat.Name = "labelControlLat";
            this.labelControlLat.Size = new System.Drawing.Size(72, 13);
            this.labelControlLat.StyleController = this.layoutControlRigthPanel;
            this.labelControlLat.TabIndex = 4;
            this.labelControlLat.Text = "labelControlLat";
            // 
            // labelControlUnitType
            // 
            this.labelControlUnitType.Location = new System.Drawing.Point(10, 115);
            this.labelControlUnitType.Name = "labelControlUnitType";
            this.labelControlUnitType.Size = new System.Drawing.Size(100, 13);
            this.labelControlUnitType.StyleController = this.layoutControlRigthPanel;
            this.labelControlUnitType.TabIndex = 4;
            this.labelControlUnitType.Text = "labelControlUnitType";
            // 
            // labelControlMileage
            // 
            this.labelControlMileage.Location = new System.Drawing.Point(10, 247);
            this.labelControlMileage.Name = "labelControlMileage";
            this.labelControlMileage.Size = new System.Drawing.Size(93, 13);
            this.labelControlMileage.StyleController = this.layoutControlRigthPanel;
            this.labelControlMileage.TabIndex = 5;
            this.labelControlMileage.Text = "labelControlMileage";
            // 
            // labelControlStation
            // 
            this.labelControlStation.Location = new System.Drawing.Point(10, 183);
            this.labelControlStation.Name = "labelControlStation";
            this.labelControlStation.Size = new System.Drawing.Size(91, 13);
            this.labelControlStation.StyleController = this.layoutControlRigthPanel;
            this.labelControlStation.TabIndex = 4;
            this.labelControlStation.Text = "labelControlStation";
            // 
            // labelControlHasAlarm
            // 
            this.labelControlHasAlarm.Location = new System.Drawing.Point(10, 396);
            this.labelControlHasAlarm.Name = "labelControlHasAlarm";
            this.labelControlHasAlarm.Size = new System.Drawing.Size(102, 13);
            this.labelControlHasAlarm.StyleController = this.layoutControlRigthPanel;
            this.labelControlHasAlarm.TabIndex = 4;
            this.labelControlHasAlarm.Text = "labelControlHasAlarm";
            // 
            // labelControlRoute
            // 
            this.labelControlRoute.Location = new System.Drawing.Point(10, 230);
            this.labelControlRoute.Name = "labelControlRoute";
            this.labelControlRoute.Size = new System.Drawing.Size(86, 13);
            this.labelControlRoute.StyleController = this.layoutControlRigthPanel;
            this.labelControlRoute.TabIndex = 4;
            this.labelControlRoute.Text = "labelControlRoute";
            // 
            // labelControlNextStop
            // 
            this.labelControlNextStop.Location = new System.Drawing.Point(10, 332);
            this.labelControlNextStop.Name = "labelControlNextStop";
            this.labelControlNextStop.Size = new System.Drawing.Size(102, 13);
            this.labelControlNextStop.StyleController = this.layoutControlRigthPanel;
            this.labelControlNextStop.TabIndex = 4;
            this.labelControlNextStop.Text = "labelControlNextStop";
            // 
            // labelControlUnitStatus
            // 
            this.labelControlUnitStatus.Location = new System.Drawing.Point(10, 149);
            this.labelControlUnitStatus.Name = "labelControlUnitStatus";
            this.labelControlUnitStatus.Size = new System.Drawing.Size(103, 13);
            this.labelControlUnitStatus.StyleController = this.layoutControlRigthPanel;
            this.labelControlUnitStatus.TabIndex = 5;
            this.labelControlUnitStatus.Text = "labelControlUnitStaus";
            // 
            // layoutControlGroupRightPanel
            // 
            this.layoutControlGroupRightPanel.CustomizationFormText = "layoutControlGroupRightPanel";
            this.layoutControlGroupRightPanel.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupUnitDetails,
            this.layoutControlGroupRoute,
            this.layoutControlGroupGPSInfo});
            this.layoutControlGroupRightPanel.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupRightPanel.Name = "layoutControlGroupRightPanel";
            this.layoutControlGroupRightPanel.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupRightPanel.Size = new System.Drawing.Size(192, 751);
            this.layoutControlGroupRightPanel.Text = "layoutControlGroupRightPanel";
            this.layoutControlGroupRightPanel.TextVisible = false;
            // 
            // layoutControlGroupUnitDetails
            // 
            this.layoutControlGroupUnitDetails.CustomizationFormText = "layoutControlGroupUnitDetails";
            this.layoutControlGroupUnitDetails.ExpandButtonVisible = true;
            this.layoutControlGroupUnitDetails.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemDepartment,
            this.layoutControlItemDriver,
            this.layoutControlItemWorkShift,
            this.layoutControlItemCustomCode,
            this.layoutControlItemSchedule,
            this.layoutControlItemUnitType,
            this.layoutControlItemUnitId,
            this.layoutControlItemZone,
            this.layoutControlItemStation,
            this.layoutControlItemUnitStatus});
            this.layoutControlGroupUnitDetails.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupUnitDetails.Name = "layoutControlGroupUnitDetails";
            this.layoutControlGroupUnitDetails.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupUnitDetails.Size = new System.Drawing.Size(186, 200);
            this.layoutControlGroupUnitDetails.Text = "layoutControlGroupUnitDetails";
            // 
            // layoutControlItemDepartment
            // 
            this.layoutControlItemDepartment.Control = this.labelControlDepartmentType;
            this.layoutControlItemDepartment.CustomizationFormText = "layoutControlItemDepartment";
            this.layoutControlItemDepartment.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemDepartment.Name = "layoutControlItemDepartment";
            this.layoutControlItemDepartment.Size = new System.Drawing.Size(176, 17);
            this.layoutControlItemDepartment.Text = "layoutControlItemDepartment";
            this.layoutControlItemDepartment.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemDepartment.TextToControlDistance = 0;
            this.layoutControlItemDepartment.TextVisible = false;
            // 
            // layoutControlItemDriver
            // 
            this.layoutControlItemDriver.Control = this.labelControlDriver;
            this.layoutControlItemDriver.CustomizationFormText = "layoutControlItemDriver";
            this.layoutControlItemDriver.Location = new System.Drawing.Point(0, 17);
            this.layoutControlItemDriver.Name = "layoutControlItemDriver";
            this.layoutControlItemDriver.Size = new System.Drawing.Size(176, 17);
            this.layoutControlItemDriver.Text = "layoutControlItemDriver";
            this.layoutControlItemDriver.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemDriver.TextToControlDistance = 0;
            this.layoutControlItemDriver.TextVisible = false;
            // 
            // layoutControlItemWorkShift
            // 
            this.layoutControlItemWorkShift.Control = this.labelControlWorkShift;
            this.layoutControlItemWorkShift.CustomizationFormText = "layoutControlItemWorkShift";
            this.layoutControlItemWorkShift.Location = new System.Drawing.Point(0, 34);
            this.layoutControlItemWorkShift.Name = "layoutControlItemWorkShift";
            this.layoutControlItemWorkShift.Size = new System.Drawing.Size(176, 17);
            this.layoutControlItemWorkShift.Text = "layoutControlItemWorkShift";
            this.layoutControlItemWorkShift.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemWorkShift.TextToControlDistance = 0;
            this.layoutControlItemWorkShift.TextVisible = false;
            // 
            // layoutControlItemCustomCode
            // 
            this.layoutControlItemCustomCode.Control = this.labelControlCustomCode;
            this.layoutControlItemCustomCode.CustomizationFormText = "layoutControlItemCustomCode";
            this.layoutControlItemCustomCode.Location = new System.Drawing.Point(0, 51);
            this.layoutControlItemCustomCode.Name = "layoutControlItemCustomCode";
            this.layoutControlItemCustomCode.Size = new System.Drawing.Size(176, 17);
            this.layoutControlItemCustomCode.Text = "layoutControlItemCustomCode";
            this.layoutControlItemCustomCode.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemCustomCode.TextToControlDistance = 0;
            this.layoutControlItemCustomCode.TextVisible = false;
            // 
            // layoutControlItemSchedule
            // 
            this.layoutControlItemSchedule.Control = this.labelControlSchedule;
            this.layoutControlItemSchedule.CustomizationFormText = "layoutControlItemSchedule";
            this.layoutControlItemSchedule.Location = new System.Drawing.Point(0, 68);
            this.layoutControlItemSchedule.Name = "layoutControlItemSchedule";
            this.layoutControlItemSchedule.Size = new System.Drawing.Size(176, 17);
            this.layoutControlItemSchedule.Text = "layoutControlItemSchedule";
            this.layoutControlItemSchedule.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemSchedule.TextToControlDistance = 0;
            this.layoutControlItemSchedule.TextVisible = false;
            // 
            // layoutControlItemUnitType
            // 
            this.layoutControlItemUnitType.Control = this.labelControlUnitType;
            this.layoutControlItemUnitType.CustomizationFormText = "layoutControlItemUnitType";
            this.layoutControlItemUnitType.Location = new System.Drawing.Point(0, 85);
            this.layoutControlItemUnitType.Name = "layoutControlItemUnitType";
            this.layoutControlItemUnitType.Size = new System.Drawing.Size(176, 17);
            this.layoutControlItemUnitType.Text = "layoutControlItemUnitType";
            this.layoutControlItemUnitType.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemUnitType.TextToControlDistance = 0;
            this.layoutControlItemUnitType.TextVisible = false;
            // 
            // layoutControlItemUnitId
            // 
            this.layoutControlItemUnitId.Control = this.labelControlUnitId;
            this.layoutControlItemUnitId.CustomizationFormText = "layoutControlItemUnitId";
            this.layoutControlItemUnitId.Location = new System.Drawing.Point(0, 102);
            this.layoutControlItemUnitId.Name = "layoutControlItemUnitId";
            this.layoutControlItemUnitId.Size = new System.Drawing.Size(176, 17);
            this.layoutControlItemUnitId.Text = "layoutControlItemUnitId";
            this.layoutControlItemUnitId.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemUnitId.TextToControlDistance = 0;
            this.layoutControlItemUnitId.TextVisible = false;
            // 
            // layoutControlItemZone
            // 
            this.layoutControlItemZone.Control = this.labelControlZone;
            this.layoutControlItemZone.CustomizationFormText = "layoutControlItemZone";
            this.layoutControlItemZone.Location = new System.Drawing.Point(0, 136);
            this.layoutControlItemZone.Name = "layoutControlItemZone";
            this.layoutControlItemZone.Size = new System.Drawing.Size(176, 17);
            this.layoutControlItemZone.Text = "layoutControlItemZone";
            this.layoutControlItemZone.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemZone.TextToControlDistance = 0;
            this.layoutControlItemZone.TextVisible = false;
            // 
            // layoutControlItemStation
            // 
            this.layoutControlItemStation.Control = this.labelControlStation;
            this.layoutControlItemStation.CustomizationFormText = "layoutControlItemStation";
            this.layoutControlItemStation.Location = new System.Drawing.Point(0, 153);
            this.layoutControlItemStation.Name = "layoutControlItemStation";
            this.layoutControlItemStation.Size = new System.Drawing.Size(176, 17);
            this.layoutControlItemStation.Text = "layoutControlItemStation";
            this.layoutControlItemStation.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemStation.TextToControlDistance = 0;
            this.layoutControlItemStation.TextVisible = false;
            // 
            // layoutControlItemUnitStatus
            // 
            this.layoutControlItemUnitStatus.Control = this.labelControlUnitStatus;
            this.layoutControlItemUnitStatus.CustomizationFormText = "layoutControlItemUnitStatus";
            this.layoutControlItemUnitStatus.Location = new System.Drawing.Point(0, 119);
            this.layoutControlItemUnitStatus.Name = "layoutControlItemUnitStatus";
            this.layoutControlItemUnitStatus.Size = new System.Drawing.Size(176, 17);
            this.layoutControlItemUnitStatus.Text = "layoutControlItemUnitStatus";
            this.layoutControlItemUnitStatus.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemUnitStatus.TextToControlDistance = 0;
            this.layoutControlItemUnitStatus.TextVisible = false;
            // 
            // layoutControlGroupRoute
            // 
            this.layoutControlGroupRoute.CustomizationFormText = "layoutControlGroupRoute";
            this.layoutControlGroupRoute.ExpandButtonVisible = true;
            this.layoutControlGroupRoute.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemRoute,
            this.layoutControlItemMileage,
            this.layoutControlItemRunTime,
            this.layoutControlItemAverageSpeed,
            this.layoutControlItemLastStop,
            this.layoutControlItemLastStopTime,
            this.layoutControlItemNextStop,
            this.layoutControlItemAverageStopTime});
            this.layoutControlGroupRoute.Location = new System.Drawing.Point(0, 200);
            this.layoutControlGroupRoute.Name = "layoutControlGroupRoute";
            this.layoutControlGroupRoute.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupRoute.Size = new System.Drawing.Size(186, 166);
            this.layoutControlGroupRoute.Text = "layoutControlGroupRoute";
            // 
            // layoutControlItemRoute
            // 
            this.layoutControlItemRoute.Control = this.labelControlRoute;
            this.layoutControlItemRoute.CustomizationFormText = "layoutControlItemRoute";
            this.layoutControlItemRoute.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemRoute.Name = "layoutControlItemRoute";
            this.layoutControlItemRoute.Size = new System.Drawing.Size(176, 17);
            this.layoutControlItemRoute.Text = "layoutControlItemRoute";
            this.layoutControlItemRoute.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemRoute.TextToControlDistance = 0;
            this.layoutControlItemRoute.TextVisible = false;
            // 
            // layoutControlItemMileage
            // 
            this.layoutControlItemMileage.Control = this.labelControlMileage;
            this.layoutControlItemMileage.CustomizationFormText = "layoutControlItemMileage";
            this.layoutControlItemMileage.Location = new System.Drawing.Point(0, 17);
            this.layoutControlItemMileage.Name = "layoutControlItemMileage";
            this.layoutControlItemMileage.Size = new System.Drawing.Size(176, 17);
            this.layoutControlItemMileage.Text = "layoutControlItemMileage";
            this.layoutControlItemMileage.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemMileage.TextToControlDistance = 0;
            this.layoutControlItemMileage.TextVisible = false;
            // 
            // layoutControlItemRunTime
            // 
            this.layoutControlItemRunTime.Control = this.labelControlRunTime;
            this.layoutControlItemRunTime.CustomizationFormText = "layoutControlItemRunTime";
            this.layoutControlItemRunTime.Location = new System.Drawing.Point(0, 34);
            this.layoutControlItemRunTime.Name = "layoutControlItemRunTime";
            this.layoutControlItemRunTime.Size = new System.Drawing.Size(176, 17);
            this.layoutControlItemRunTime.Text = "layoutControlItemRunTime";
            this.layoutControlItemRunTime.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemRunTime.TextToControlDistance = 0;
            this.layoutControlItemRunTime.TextVisible = false;
            // 
            // layoutControlItemAverageSpeed
            // 
            this.layoutControlItemAverageSpeed.Control = this.labelControlAverageSpeed;
            this.layoutControlItemAverageSpeed.CustomizationFormText = "layoutControlItemAverageSpeed";
            this.layoutControlItemAverageSpeed.Location = new System.Drawing.Point(0, 51);
            this.layoutControlItemAverageSpeed.Name = "layoutControlItemAverageSpeed";
            this.layoutControlItemAverageSpeed.Size = new System.Drawing.Size(176, 17);
            this.layoutControlItemAverageSpeed.Text = "layoutControlItemAverageSpeed";
            this.layoutControlItemAverageSpeed.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemAverageSpeed.TextToControlDistance = 0;
            this.layoutControlItemAverageSpeed.TextVisible = false;
            // 
            // layoutControlItemLastStop
            // 
            this.layoutControlItemLastStop.Control = this.labelControlLastStop;
            this.layoutControlItemLastStop.CustomizationFormText = "layoutControlItemLastStop";
            this.layoutControlItemLastStop.Location = new System.Drawing.Point(0, 68);
            this.layoutControlItemLastStop.Name = "layoutControlItemLastStop";
            this.layoutControlItemLastStop.Size = new System.Drawing.Size(176, 17);
            this.layoutControlItemLastStop.Text = "layoutControlItemLastStop";
            this.layoutControlItemLastStop.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemLastStop.TextToControlDistance = 0;
            this.layoutControlItemLastStop.TextVisible = false;
            // 
            // layoutControlItemLastStopTime
            // 
            this.layoutControlItemLastStopTime.Control = this.labelControlLastStopTime;
            this.layoutControlItemLastStopTime.CustomizationFormText = "layoutControlItemLastStopTime";
            this.layoutControlItemLastStopTime.Location = new System.Drawing.Point(0, 85);
            this.layoutControlItemLastStopTime.Name = "layoutControlItemLastStopTime";
            this.layoutControlItemLastStopTime.Size = new System.Drawing.Size(176, 17);
            this.layoutControlItemLastStopTime.Text = "layoutControlItemLastStopTime";
            this.layoutControlItemLastStopTime.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemLastStopTime.TextToControlDistance = 0;
            this.layoutControlItemLastStopTime.TextVisible = false;
            // 
            // layoutControlItemNextStop
            // 
            this.layoutControlItemNextStop.Control = this.labelControlNextStop;
            this.layoutControlItemNextStop.CustomizationFormText = "layoutControlItemNextStop";
            this.layoutControlItemNextStop.Location = new System.Drawing.Point(0, 102);
            this.layoutControlItemNextStop.Name = "layoutControlItemNextStop";
            this.layoutControlItemNextStop.Size = new System.Drawing.Size(176, 17);
            this.layoutControlItemNextStop.Text = "layoutControlItemNextStop";
            this.layoutControlItemNextStop.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemNextStop.TextToControlDistance = 0;
            this.layoutControlItemNextStop.TextVisible = false;
            // 
            // layoutControlItemAverageStopTime
            // 
            this.layoutControlItemAverageStopTime.Control = this.labelControlAverageStopTime;
            this.layoutControlItemAverageStopTime.CustomizationFormText = "layoutControlItemAverageStopTime";
            this.layoutControlItemAverageStopTime.Location = new System.Drawing.Point(0, 119);
            this.layoutControlItemAverageStopTime.Name = "layoutControlItemAverageStopTime";
            this.layoutControlItemAverageStopTime.Size = new System.Drawing.Size(176, 17);
            this.layoutControlItemAverageStopTime.Text = "layoutControlItemAverageStopTime";
            this.layoutControlItemAverageStopTime.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemAverageStopTime.TextToControlDistance = 0;
            this.layoutControlItemAverageStopTime.TextVisible = false;
            // 
            // layoutControlGroupGPSInfo
            // 
            this.layoutControlGroupGPSInfo.CustomizationFormText = "layoutControlGroupGPSInfo";
            this.layoutControlGroupGPSInfo.ExpandButtonVisible = true;
            this.layoutControlGroupGPSInfo.ExpandOnDoubleClick = true;
            this.layoutControlGroupGPSInfo.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemHasAlarm,
            this.layoutControlItemLat,
            this.layoutControlItemLon,
            this.layoutControlItemDirection,
            this.layoutControlItemActualSpeed,
            this.layoutControlItemSatellitesCount,
            this.layoutControlItemSentDate});
            this.layoutControlGroupGPSInfo.Location = new System.Drawing.Point(0, 366);
            this.layoutControlGroupGPSInfo.Name = "layoutControlGroupGPSInfo";
            this.layoutControlGroupGPSInfo.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupGPSInfo.Size = new System.Drawing.Size(186, 379);
            this.layoutControlGroupGPSInfo.Text = "layoutControlGroupGPSInfo";
            // 
            // layoutControlItemHasAlarm
            // 
            this.layoutControlItemHasAlarm.Control = this.labelControlHasAlarm;
            this.layoutControlItemHasAlarm.CustomizationFormText = "layoutControlItemHasAlarm";
            this.layoutControlItemHasAlarm.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemHasAlarm.Name = "layoutControlItemHasAlarm";
            this.layoutControlItemHasAlarm.Size = new System.Drawing.Size(176, 17);
            this.layoutControlItemHasAlarm.Text = "layoutControlItemHasAlarm";
            this.layoutControlItemHasAlarm.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemHasAlarm.TextToControlDistance = 0;
            this.layoutControlItemHasAlarm.TextVisible = false;
            // 
            // layoutControlItemLat
            // 
            this.layoutControlItemLat.Control = this.labelControlLat;
            this.layoutControlItemLat.CustomizationFormText = "layoutControlItemLat";
            this.layoutControlItemLat.Location = new System.Drawing.Point(0, 17);
            this.layoutControlItemLat.Name = "layoutControlItemLat";
            this.layoutControlItemLat.Size = new System.Drawing.Size(176, 17);
            this.layoutControlItemLat.Text = "layoutControlItemLat";
            this.layoutControlItemLat.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemLat.TextToControlDistance = 0;
            this.layoutControlItemLat.TextVisible = false;
            // 
            // layoutControlItemLon
            // 
            this.layoutControlItemLon.Control = this.labelControlLon;
            this.layoutControlItemLon.CustomizationFormText = "layoutControlItemLon";
            this.layoutControlItemLon.Location = new System.Drawing.Point(0, 34);
            this.layoutControlItemLon.Name = "layoutControlItemLon";
            this.layoutControlItemLon.Size = new System.Drawing.Size(176, 17);
            this.layoutControlItemLon.Text = "layoutControlItemLon";
            this.layoutControlItemLon.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemLon.TextToControlDistance = 0;
            this.layoutControlItemLon.TextVisible = false;
            // 
            // layoutControlItemDirection
            // 
            this.layoutControlItemDirection.Control = this.labelControlDirection;
            this.layoutControlItemDirection.CustomizationFormText = "layoutControlItemDirection";
            this.layoutControlItemDirection.Location = new System.Drawing.Point(0, 51);
            this.layoutControlItemDirection.Name = "layoutControlItemDirection";
            this.layoutControlItemDirection.Size = new System.Drawing.Size(176, 17);
            this.layoutControlItemDirection.Text = "layoutControlItemDirection";
            this.layoutControlItemDirection.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemDirection.TextToControlDistance = 0;
            this.layoutControlItemDirection.TextVisible = false;
            // 
            // layoutControlItemActualSpeed
            // 
            this.layoutControlItemActualSpeed.Control = this.labelControlActualSpeed;
            this.layoutControlItemActualSpeed.CustomizationFormText = "layoutControlItemActualSpeed";
            this.layoutControlItemActualSpeed.Location = new System.Drawing.Point(0, 68);
            this.layoutControlItemActualSpeed.Name = "layoutControlItemActualSpeed";
            this.layoutControlItemActualSpeed.Size = new System.Drawing.Size(176, 17);
            this.layoutControlItemActualSpeed.Text = "layoutControlItemActualSpeed";
            this.layoutControlItemActualSpeed.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemActualSpeed.TextToControlDistance = 0;
            this.layoutControlItemActualSpeed.TextVisible = false;
            // 
            // layoutControlItemSatellitesCount
            // 
            this.layoutControlItemSatellitesCount.Control = this.labelControlSatellitesCount;
            this.layoutControlItemSatellitesCount.CustomizationFormText = "layoutControlItemSatellitesCount";
            this.layoutControlItemSatellitesCount.Location = new System.Drawing.Point(0, 102);
            this.layoutControlItemSatellitesCount.Name = "layoutControlItemSatellitesCount";
            this.layoutControlItemSatellitesCount.Size = new System.Drawing.Size(176, 247);
            this.layoutControlItemSatellitesCount.Text = "layoutControlItemSatellitesCount";
            this.layoutControlItemSatellitesCount.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemSatellitesCount.TextToControlDistance = 0;
            this.layoutControlItemSatellitesCount.TextVisible = false;
            // 
            // layoutControlItemSentDate
            // 
            this.layoutControlItemSentDate.Control = this.labelControlSentTime;
            this.layoutControlItemSentDate.CustomizationFormText = "layoutControlItemSentDate";
            this.layoutControlItemSentDate.Location = new System.Drawing.Point(0, 85);
            this.layoutControlItemSentDate.Name = "layoutControlItemSentDate";
            this.layoutControlItemSentDate.Size = new System.Drawing.Size(176, 17);
            this.layoutControlItemSentDate.Text = "layoutControlItemSentDate";
            this.layoutControlItemSentDate.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemSentDate.TextToControlDistance = 0;
            this.layoutControlItemSentDate.TextVisible = false;
            // 
            // dockPanelDown
            // 
            this.dockPanelDown.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(239)))));
            this.dockPanelDown.Appearance.Options.UseBackColor = true;
            this.dockPanelDown.Controls.Add(this.controlContainer1);
            this.dockPanelDown.Dock = DevExpress.XtraBars.Docking.DockingStyle.Bottom;
            this.dockPanelDown.FloatVertical = true;
            this.dockPanelDown.ID = new System.Guid("42953d33-df51-473e-950d-e04b41adfa41");
            this.dockPanelDown.Location = new System.Drawing.Point(304, 578);
            this.dockPanelDown.Name = "dockPanelDown";
            this.dockPanelDown.Options.AllowDockFill = false;
            this.dockPanelDown.Options.AllowDockLeft = false;
            this.dockPanelDown.Options.AllowDockRight = false;
            this.dockPanelDown.Options.AllowDockTop = false;
            this.dockPanelDown.Options.AllowFloating = false;
            this.dockPanelDown.Options.FloatOnDblClick = false;
            this.dockPanelDown.Options.ShowCloseButton = false;
            this.dockPanelDown.Options.ShowMaximizeButton = false;
            this.dockPanelDown.OriginalSize = new System.Drawing.Size(200, 200);
            this.dockPanelDown.Size = new System.Drawing.Size(766, 200);
            this.dockPanelDown.Text = "dockPanelDown";
            // 
            // controlContainer1
            // 
            this.controlContainer1.Controls.Add(this.layoutControlDownPanel);
            this.controlContainer1.Location = new System.Drawing.Point(4, 23);
            this.controlContainer1.Name = "controlContainer1";
            this.controlContainer1.Size = new System.Drawing.Size(758, 173);
            this.controlContainer1.TabIndex = 0;
            // 
            // layoutControlDownPanel
            // 
            this.layoutControlDownPanel.Controls.Add(this.labelControlAlertCount);
            this.layoutControlDownPanel.Controls.Add(this.memoEditObservations);
            this.layoutControlDownPanel.Controls.Add(this.simpleButtonSelectedUnit);
            this.layoutControlDownPanel.Controls.Add(this.simpleButtonAllAlerts);
            this.layoutControlDownPanel.Controls.Add(this.gridControlExAlerts);
            this.layoutControlDownPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlDownPanel.Location = new System.Drawing.Point(0, 0);
            this.layoutControlDownPanel.Name = "layoutControlDownPanel";
            this.layoutControlDownPanel.Root = this.layoutControlGroupDownPanel;
            this.layoutControlDownPanel.Size = new System.Drawing.Size(758, 173);
            this.layoutControlDownPanel.TabIndex = 0;
            this.layoutControlDownPanel.Text = "layoutControl1";
            // 
            // labelControlAlertCount
            // 
            this.labelControlAlertCount.Location = new System.Drawing.Point(350, 5);
            this.labelControlAlertCount.Name = "labelControlAlertCount";
            this.labelControlAlertCount.Size = new System.Drawing.Size(109, 13);
            this.labelControlAlertCount.StyleController = this.layoutControlDownPanel;
            this.labelControlAlertCount.TabIndex = 7;
            this.labelControlAlertCount.Text = "labelControlAlertCount";
            // 
            // memoEditObservations
            // 
            this.memoEditObservations.Location = new System.Drawing.Point(468, 30);
            this.memoEditObservations.Name = "memoEditObservations";
            this.memoEditObservations.Properties.ReadOnly = true;
            this.memoEditObservations.Size = new System.Drawing.Size(280, 133);
            this.memoEditObservations.StyleController = this.layoutControlDownPanel;
            this.memoEditObservations.TabIndex = 6;
            // 
            // simpleButtonSelectedUnit
            // 
            this.simpleButtonSelectedUnit.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButtonSelectedUnit.Appearance.ForeColor = System.Drawing.Color.Black;
            this.simpleButtonSelectedUnit.Appearance.Options.UseFont = true;
            this.simpleButtonSelectedUnit.Appearance.Options.UseForeColor = true;
            this.simpleButtonSelectedUnit.FontFormat = FontFormat.GetFormat("ButtonFormat");
            this.simpleButtonSelectedUnit.Location = new System.Drawing.Point(92, 5);
            this.simpleButtonSelectedUnit.Name = "simpleButtonSelectedUnit";
            this.simpleButtonSelectedUnit.Size = new System.Drawing.Size(83, 25);
            this.simpleButtonSelectedUnit.StyleController = this.layoutControlDownPanel;
            this.simpleButtonSelectedUnit.TabIndex = 5;
            this.simpleButtonSelectedUnit.Text = "simpleButtonSelectedUnit";
            this.simpleButtonSelectedUnit.Click += new System.EventHandler(this.simpleButtonSelectedUnit_Click);
            // 
            // simpleButtonAllAlerts
            // 
            this.simpleButtonAllAlerts.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButtonAllAlerts.Appearance.ForeColor = System.Drawing.Color.Black;
            this.simpleButtonAllAlerts.Appearance.Options.UseFont = true;
            this.simpleButtonAllAlerts.Appearance.Options.UseForeColor = true;
            this.simpleButtonAllAlerts.FontFormat = FontFormat.GetFormat("ButtonFormat");
            this.simpleButtonAllAlerts.Location = new System.Drawing.Point(5, 5);
            this.simpleButtonAllAlerts.Name = "simpleButtonAllAlerts";
            this.simpleButtonAllAlerts.Size = new System.Drawing.Size(83, 25);
            this.simpleButtonAllAlerts.StyleController = this.layoutControlDownPanel;
            this.simpleButtonAllAlerts.TabIndex = 4;
            this.simpleButtonAllAlerts.Text = "simpleButtonAllAlerts";
            this.simpleButtonAllAlerts.Click += new System.EventHandler(this.simpleButtonAllAlerts_Click);
            // 
            // gridControlExAlerts
            // 
            this.gridControlExAlerts.EnableAutoFilter = false;
            this.gridControlExAlerts.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExAlerts.Location = new System.Drawing.Point(5, 34);
            this.gridControlExAlerts.MainView = this.gridViewExAlerts;
            this.gridControlExAlerts.Name = "gridControlExAlerts";
            this.gridControlExAlerts.Size = new System.Drawing.Size(454, 134);
            this.gridControlExAlerts.TabIndex = 0;
            this.gridControlExAlerts.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExAlerts});
            this.gridControlExAlerts.ViewTotalRows = false;
            // 
            // gridViewExAlerts
            // 
            this.gridViewExAlerts.AllowFocusedRowChanged = true;
            this.gridViewExAlerts.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewExAlerts.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExAlerts.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewExAlerts.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewExAlerts.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewExAlerts.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExAlerts.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewExAlerts.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewExAlerts.EnablePreviewLineForFocusedRow = false;
            this.gridViewExAlerts.GridControl = this.gridControlExAlerts;
            this.gridViewExAlerts.Name = "gridViewExAlerts";
            this.gridViewExAlerts.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExAlerts.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewExAlerts.ViewTotalRows = false;
            this.gridViewExAlerts.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewExAlerts_FocusedRowChanged);
            // 
            // layoutControlGroupDownPanel
            // 
            this.layoutControlGroupDownPanel.CustomizationFormText = "layoutControlGroupDownPanel";
            this.layoutControlGroupDownPanel.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupObservations,
            this.layoutControlItemGridControlAlerts,
            this.layoutControlItemSimpleButtonAllAlerts,
            this.layoutControlItemSimpleButtonSelectedUnit,
            this.layoutControlItemAlertCount,
            this.emptySpaceItem1});
            this.layoutControlGroupDownPanel.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupDownPanel.Name = "layoutControlGroupDownPanel";
            this.layoutControlGroupDownPanel.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupDownPanel.Size = new System.Drawing.Size(758, 173);
            this.layoutControlGroupDownPanel.Text = "layoutControlGroupDownPanel";
            this.layoutControlGroupDownPanel.TextVisible = false;
            // 
            // layoutControlGroupObservations
            // 
            this.layoutControlGroupObservations.CustomizationFormText = "layoutControlGroupObservations";
            this.layoutControlGroupObservations.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemTextEditObs});
            this.layoutControlGroupObservations.Location = new System.Drawing.Point(458, 0);
            this.layoutControlGroupObservations.Name = "layoutControlGroupObservations";
            this.layoutControlGroupObservations.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupObservations.Size = new System.Drawing.Size(294, 167);
            this.layoutControlGroupObservations.Text = "layoutControlGroupObservations";
            // 
            // layoutControlItemTextEditObs
            // 
            this.layoutControlItemTextEditObs.Control = this.memoEditObservations;
            this.layoutControlItemTextEditObs.CustomizationFormText = "layoutControlItemTextEditObs";
            this.layoutControlItemTextEditObs.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemTextEditObs.Name = "layoutControlItemTextEditObs";
            this.layoutControlItemTextEditObs.Size = new System.Drawing.Size(284, 137);
            this.layoutControlItemTextEditObs.Text = "layoutControlItemTextEditObs";
            this.layoutControlItemTextEditObs.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemTextEditObs.TextToControlDistance = 0;
            this.layoutControlItemTextEditObs.TextVisible = false;
            // 
            // layoutControlItemGridControlAlerts
            // 
            this.layoutControlItemGridControlAlerts.Control = this.gridControlExAlerts;
            this.layoutControlItemGridControlAlerts.CustomizationFormText = "layoutControlItemGridControlAlerts";
            this.layoutControlItemGridControlAlerts.Location = new System.Drawing.Point(0, 29);
            this.layoutControlItemGridControlAlerts.Name = "layoutControlItemGridControlAlerts";
            this.layoutControlItemGridControlAlerts.Size = new System.Drawing.Size(458, 138);
            this.layoutControlItemGridControlAlerts.Text = "layoutControlItemGridControlAlerts";
            this.layoutControlItemGridControlAlerts.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemGridControlAlerts.TextToControlDistance = 0;
            this.layoutControlItemGridControlAlerts.TextVisible = false;
            // 
            // layoutControlItemSimpleButtonAllAlerts
            // 
            this.layoutControlItemSimpleButtonAllAlerts.Control = this.simpleButtonAllAlerts;
            this.layoutControlItemSimpleButtonAllAlerts.CustomizationFormText = "layoutControlItemSimpleButtonAllAlerts";
            this.layoutControlItemSimpleButtonAllAlerts.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemSimpleButtonAllAlerts.MaxSize = new System.Drawing.Size(87, 29);
            this.layoutControlItemSimpleButtonAllAlerts.MinSize = new System.Drawing.Size(87, 29);
            this.layoutControlItemSimpleButtonAllAlerts.Name = "layoutControlItemSimpleButtonAllAlerts";
            this.layoutControlItemSimpleButtonAllAlerts.Size = new System.Drawing.Size(87, 29);
            this.layoutControlItemSimpleButtonAllAlerts.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemSimpleButtonAllAlerts.Text = "layoutControlItemSimpleButtonAllAlerts";
            this.layoutControlItemSimpleButtonAllAlerts.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemSimpleButtonAllAlerts.TextToControlDistance = 0;
            this.layoutControlItemSimpleButtonAllAlerts.TextVisible = false;
            // 
            // layoutControlItemSimpleButtonSelectedUnit
            // 
            this.layoutControlItemSimpleButtonSelectedUnit.Control = this.simpleButtonSelectedUnit;
            this.layoutControlItemSimpleButtonSelectedUnit.CustomizationFormText = "layoutControlItemSimpleButtonSelectedUnit";
            this.layoutControlItemSimpleButtonSelectedUnit.Location = new System.Drawing.Point(87, 0);
            this.layoutControlItemSimpleButtonSelectedUnit.MaxSize = new System.Drawing.Size(87, 29);
            this.layoutControlItemSimpleButtonSelectedUnit.MinSize = new System.Drawing.Size(87, 29);
            this.layoutControlItemSimpleButtonSelectedUnit.Name = "layoutControlItemSimpleButtonSelectedUnit";
            this.layoutControlItemSimpleButtonSelectedUnit.Size = new System.Drawing.Size(87, 29);
            this.layoutControlItemSimpleButtonSelectedUnit.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemSimpleButtonSelectedUnit.Text = "layoutControlItemSimpleButtonSelectedUnit";
            this.layoutControlItemSimpleButtonSelectedUnit.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemSimpleButtonSelectedUnit.TextToControlDistance = 0;
            this.layoutControlItemSimpleButtonSelectedUnit.TextVisible = false;
            // 
            // layoutControlItemAlertCount
            // 
            this.layoutControlItemAlertCount.Control = this.labelControlAlertCount;
            this.layoutControlItemAlertCount.CustomizationFormText = "layoutControlItemAlertCount";
            this.layoutControlItemAlertCount.Location = new System.Drawing.Point(345, 0);
            this.layoutControlItemAlertCount.Name = "layoutControlItemAlertCount";
            this.layoutControlItemAlertCount.Size = new System.Drawing.Size(113, 29);
            this.layoutControlItemAlertCount.Text = "layoutControlItemAlertCount";
            this.layoutControlItemAlertCount.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemAlertCount.TextToControlDistance = 0;
            this.layoutControlItemAlertCount.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(174, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(171, 29);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroupMainWindow
            // 
            this.layoutControlGroupMainWindow.CustomizationFormText = "layoutControlGroupMainWindow";
            this.layoutControlGroupMainWindow.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemMapControl});
            this.layoutControlGroupMainWindow.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupMainWindow.Name = "layoutControlGroupMainWindow";
            this.layoutControlGroupMainWindow.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupMainWindow.Size = new System.Drawing.Size(766, 578);
            this.layoutControlGroupMainWindow.Text = "layoutControlGroupMainWindow";
            this.layoutControlGroupMainWindow.TextVisible = false;
            // 
            // layoutControlItemMapControl
            // 
            this.layoutControlItemMapControl.CustomizationFormText = "layoutControlItemMapControl";
            this.layoutControlItemMapControl.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemMapControl.Name = "layoutControlItemMapControl";
            this.layoutControlItemMapControl.Size = new System.Drawing.Size(760, 572);
            this.layoutControlItemMapControl.Text = "layoutControlItemMapControl";
            this.layoutControlItemMapControl.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemMapControl.TextToControlDistance = 0;
            this.layoutControlItemMapControl.TextVisible = false;
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.BackColor = System.Drawing.Color.White;
            this.ribbonControl1.Dock = System.Windows.Forms.DockStyle.None;
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.ExpandCollapseItem.Name = "";
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.barButtonZoomIn,
            this.barButtonZoomOut,
            this.barButtonPan,
            this.barButtonCenter,
            this.barButtonAddPolygon,
            this.barButtonDistance,
            this.barButtonSelect,
            this.barButtonAddPosition,
            this.barButtonClearMap,
            this.barButtonSelectRect,
            this.barButtonSelectRadius,
            this.comboBoxSelectTool,
            this.barButtonItemProcessing,
            this.barButtonItemEnd,
            this.barButtonItemObservations,
            this.barButtonItemFollow,
            this.barButtonItemCommands,
            this.barButtonItemSetPosition});
            this.ribbonControl1.Location = new System.Drawing.Point(313, 25);
            this.ribbonControl1.MaxItemId = 34;
            this.ribbonControl1.MdiMergeStyle = DevExpress.XtraBars.Ribbon.RibbonMdiMergeStyle.Always;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPageMapControls});
            this.ribbonControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox1,
            this.repositoryItemImageComboBox1});
            this.ribbonControl1.ShowToolbarCustomizeItem = false;
            this.ribbonControl1.Size = new System.Drawing.Size(809, 120);
            this.ribbonControl1.Toolbar.ShowCustomizeItem = false;
            this.ribbonControl1.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            // 
            // barButtonZoomIn
            // 
            this.barButtonZoomIn.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.barButtonZoomIn.Caption = "barButtonZoomIn";
            this.barButtonZoomIn.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonZoomIn.Glyph")));
            this.barButtonZoomIn.GroupIndex = 1;
            this.barButtonZoomIn.Id = 4;
            this.barButtonZoomIn.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonZoomIn.Name = "barButtonZoomIn";
            this.barButtonZoomIn.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonZoomIn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonZoomIn_ItemClick);
            // 
            // barButtonZoomOut
            // 
            this.barButtonZoomOut.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.barButtonZoomOut.Caption = "barButtonZoomOut";
            this.barButtonZoomOut.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonZoomOut.Glyph")));
            this.barButtonZoomOut.GroupIndex = 1;
            this.barButtonZoomOut.Id = 6;
            this.barButtonZoomOut.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonZoomOut.Name = "barButtonZoomOut";
            this.barButtonZoomOut.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonZoomOut.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonZoomIn_ItemClick);
            // 
            // barButtonPan
            // 
            this.barButtonPan.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.barButtonPan.Caption = "barButtonPan";
            this.barButtonPan.Down = true;
            this.barButtonPan.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonPan.Glyph")));
            this.barButtonPan.GroupIndex = 1;
            this.barButtonPan.Id = 7;
            this.barButtonPan.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonPan.Name = "barButtonPan";
            this.barButtonPan.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonPan.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonZoomIn_ItemClick);
            // 
            // barButtonCenter
            // 
            this.barButtonCenter.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.barButtonCenter.Caption = "barButtonCenter";
            this.barButtonCenter.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonCenter.Glyph")));
            this.barButtonCenter.GroupIndex = 1;
            this.barButtonCenter.Id = 8;
            this.barButtonCenter.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonCenter.Name = "barButtonCenter";
            this.barButtonCenter.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonCenter.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonZoomIn_ItemClick);
            // 
            // barButtonAddPolygon
            // 
            this.barButtonAddPolygon.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.barButtonAddPolygon.Caption = "barButtonAddPolygon";
            this.barButtonAddPolygon.Enabled = false;
            this.barButtonAddPolygon.Glyph = global::SmartCadGuiCommon.XRDesignRibbonControllerResources.RibbonUserDesigner_HorizSpaceIncrease;
            this.barButtonAddPolygon.GroupIndex = 1;
            this.barButtonAddPolygon.Id = 9;
            this.barButtonAddPolygon.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonAddPolygon.Name = "barButtonAddPolygon";
            this.barButtonAddPolygon.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // barButtonDistance
            // 
            this.barButtonDistance.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.barButtonDistance.Caption = "barButtonDistance";
            this.barButtonDistance.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonDistance.Glyph")));
            this.barButtonDistance.GroupIndex = 1;
            this.barButtonDistance.Id = 21;
            this.barButtonDistance.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonDistance.Name = "barButtonDistance";
            this.barButtonDistance.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonDistance.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonZoomIn_ItemClick);
            // 
            // barButtonSelect
            // 
            this.barButtonSelect.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.barButtonSelect.Caption = "barButtonSelect";
            this.barButtonSelect.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonSelect.Glyph")));
            this.barButtonSelect.GroupIndex = 1;
            this.barButtonSelect.Id = 10;
            this.barButtonSelect.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonSelect.Name = "barButtonSelect";
            this.barButtonSelect.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonSelect.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonZoomIn_ItemClick);
            // 
            // barButtonAddPosition
            // 
            this.barButtonAddPosition.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.barButtonAddPosition.Caption = "barButtonAddPosition";
            this.barButtonAddPosition.Enabled = false;
            this.barButtonAddPosition.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonAddPosition.Glyph")));
            this.barButtonAddPosition.GroupIndex = 1;
            this.barButtonAddPosition.Id = 12;
            this.barButtonAddPosition.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonAddPosition.Name = "barButtonAddPosition";
            this.barButtonAddPosition.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // barButtonClearMap
            // 
            this.barButtonClearMap.Caption = "barButtonClearMap";
            this.barButtonClearMap.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonClearMap.Glyph")));
            this.barButtonClearMap.GroupIndex = 1;
            this.barButtonClearMap.Id = 15;
            this.barButtonClearMap.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonClearMap.Name = "barButtonClearMap";
            this.barButtonClearMap.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonClearMap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonClearMap_ItemClick);
            // 
            // barButtonSelectRect
            // 
            this.barButtonSelectRect.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.barButtonSelectRect.Caption = "barButtonSelectRect";
            this.barButtonSelectRect.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonSelectRect.Glyph")));
            this.barButtonSelectRect.GroupIndex = 1;
            this.barButtonSelectRect.Id = 19;
            this.barButtonSelectRect.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonSelectRect.Name = "barButtonSelectRect";
            this.barButtonSelectRect.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonSelectRect.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.barButtonSelectRect.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonZoomIn_ItemClick);
            // 
            // barButtonSelectRadius
            // 
            this.barButtonSelectRadius.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.barButtonSelectRadius.Caption = "barButtonSelectRadius";
            this.barButtonSelectRadius.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonSelectRadius.Glyph")));
            this.barButtonSelectRadius.GroupIndex = 1;
            this.barButtonSelectRadius.Id = 20;
            this.barButtonSelectRadius.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonSelectRadius.Name = "barButtonSelectRadius";
            this.barButtonSelectRadius.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonSelectRadius.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.barButtonSelectRadius.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonZoomIn_ItemClick);
            // 
            // comboBoxSelectTool
            // 
            this.comboBoxSelectTool.Caption = "barEditItem1";
            this.comboBoxSelectTool.Edit = this.repositoryItemImageComboBox1;
            this.comboBoxSelectTool.Id = 30;
            this.comboBoxSelectTool.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.comboBoxSelectTool.Name = "comboBoxSelectTool";
            this.comboBoxSelectTool.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.comboBoxSelectTool.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.comboBoxSelectTool.EditValueChanged += new System.EventHandler(this.comboBoxSelectTool_EditValueChanged);
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AutoHeight = false;
            this.repositoryItemImageComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            // 
            // barButtonItemProcessing
            // 
            this.barButtonItemProcessing.Caption = "barButtonItemProcessing";
            this.barButtonItemProcessing.Enabled = false;
            this.barButtonItemProcessing.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemProcessing.Glyph")));
            this.barButtonItemProcessing.Id = 28;
            this.barButtonItemProcessing.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemProcessing.Name = "barButtonItemProcessing";
            this.barButtonItemProcessing.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemProcessing_ItemClick);
            // 
            // barButtonItemEnd
            // 
            this.barButtonItemEnd.Caption = "barButtonItemEnd";
            this.barButtonItemEnd.Enabled = false;
            this.barButtonItemEnd.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemEnd.Glyph")));
            this.barButtonItemEnd.Id = 29;
            this.barButtonItemEnd.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemEnd.Name = "barButtonItemEnd";
            this.barButtonItemEnd.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemEnd_ItemClick);
            // 
            // barButtonItemObservations
            // 
            this.barButtonItemObservations.Caption = "barButtonItemObservations";
            this.barButtonItemObservations.Enabled = false;
            this.barButtonItemObservations.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemObservations.Glyph")));
            this.barButtonItemObservations.Id = 30;
            this.barButtonItemObservations.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemObservations.Name = "barButtonItemObservations";
            this.barButtonItemObservations.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemObservations_ItemClick);
            // 
            // barButtonItemFollow
            // 
            this.barButtonItemFollow.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.barButtonItemFollow.Caption = "barButtonItemFollow";
            this.barButtonItemFollow.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemFollow.Glyph")));
            this.barButtonItemFollow.Id = 31;
            this.barButtonItemFollow.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemFollow.Name = "barButtonItemFollow";
            this.barButtonItemFollow.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemFollow_ItemClick);
            // 
            // barButtonItemCommands
            // 
            this.barButtonItemCommands.Caption = "barButtonItemComands";
            this.barButtonItemCommands.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemCommands.Glyph")));
            this.barButtonItemCommands.Id = 32;
            this.barButtonItemCommands.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemCommands.Name = "barButtonItemCommands";
            this.barButtonItemCommands.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemCommands_ItemClick);
            // 
            // barButtonItemSetPosition
            // 
            this.barButtonItemSetPosition.Caption = "barButtonItemSetPosition";
            this.barButtonItemSetPosition.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemSetPosition.Glyph")));
            this.barButtonItemSetPosition.Id = 33;
            this.barButtonItemSetPosition.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemSetPosition.Name = "barButtonItemSetPosition";
            this.barButtonItemSetPosition.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemSetPosition_ItemClick);
            // 
            // ribbonPageMapControls
            // 
            this.ribbonPageMapControls.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupMapControls,
            this.ribbonPageGroupLocation,
            this.ribbonPageGroupUnits,
            this.ribbonPageGroupAlerts});
            this.ribbonPageMapControls.Name = "ribbonPageMapControls";
            this.ribbonPageMapControls.Text = "ribbonPageMapControls";
            // 
            // ribbonPageGroupMapControls
            // 
            this.ribbonPageGroupMapControls.AllowMinimize = false;
            this.ribbonPageGroupMapControls.AllowTextClipping = false;
            this.ribbonPageGroupMapControls.ItemLinks.Add(this.barButtonZoomIn);
            this.ribbonPageGroupMapControls.ItemLinks.Add(this.barButtonZoomOut);
            this.ribbonPageGroupMapControls.ItemLinks.Add(this.barButtonCenter);
            this.ribbonPageGroupMapControls.ItemLinks.Add(this.comboBoxSelectTool);
            this.ribbonPageGroupMapControls.ItemLinks.Add(this.barButtonSelect);
            this.ribbonPageGroupMapControls.ItemLinks.Add(this.barButtonPan);
            this.ribbonPageGroupMapControls.ItemLinks.Add(this.barButtonSelectRect);
            this.ribbonPageGroupMapControls.ItemLinks.Add(this.barButtonSelectRadius);
            this.ribbonPageGroupMapControls.ItemLinks.Add(this.barButtonDistance);
            this.ribbonPageGroupMapControls.Name = "ribbonPageGroupMapControls";
            this.ribbonPageGroupMapControls.ShowCaptionButton = false;
            this.ribbonPageGroupMapControls.Text = "ribbonPageGroupMapControls";
            // 
            // ribbonPageGroupLocation
            // 
            this.ribbonPageGroupLocation.AllowMinimize = false;
            this.ribbonPageGroupLocation.AllowTextClipping = false;
            this.ribbonPageGroupLocation.ItemLinks.Add(this.barButtonAddPosition);
            this.ribbonPageGroupLocation.ItemLinks.Add(this.barButtonAddPolygon);
            this.ribbonPageGroupLocation.ItemLinks.Add(this.barButtonClearMap);
            this.ribbonPageGroupLocation.Name = "ribbonPageGroupLocation";
            this.ribbonPageGroupLocation.ShowCaptionButton = false;
            this.ribbonPageGroupLocation.Text = "ribbonPageGroupLocation";
            // 
            // ribbonPageGroupUnits
            // 
            this.ribbonPageGroupUnits.AllowMinimize = false;
            this.ribbonPageGroupUnits.AllowTextClipping = false;
            this.ribbonPageGroupUnits.ItemLinks.Add(this.barButtonItemFollow);
            this.ribbonPageGroupUnits.ItemLinks.Add(this.barButtonItemSetPosition);
            this.ribbonPageGroupUnits.ItemLinks.Add(this.barButtonItemCommands);
            this.ribbonPageGroupUnits.Name = "ribbonPageGroupUnits";
            this.ribbonPageGroupUnits.ShowCaptionButton = false;
            this.ribbonPageGroupUnits.Text = "ribbonPageGroupUnits";
            // 
            // ribbonPageGroupAlerts
            // 
            this.ribbonPageGroupAlerts.AllowMinimize = false;
            this.ribbonPageGroupAlerts.AllowTextClipping = false;
            this.ribbonPageGroupAlerts.ItemLinks.Add(this.barButtonItemProcessing);
            this.ribbonPageGroupAlerts.ItemLinks.Add(this.barButtonItemEnd);
            this.ribbonPageGroupAlerts.ItemLinks.Add(this.barButtonItemObservations);
            this.ribbonPageGroupAlerts.Name = "ribbonPageGroupAlerts";
            this.ribbonPageGroupAlerts.ShowCaptionButton = false;
            this.ribbonPageGroupAlerts.Text = "ribbonPageGroupAlerts";
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.SpinDown)});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // layoutControlMainWindow
            // 
            this.layoutControlMainWindow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlMainWindow.Location = new System.Drawing.Point(304, 0);
            this.layoutControlMainWindow.Name = "layoutControlMainWindow";
            this.layoutControlMainWindow.Root = this.layoutControlGroupMainWindow;
            this.layoutControlMainWindow.Size = new System.Drawing.Size(766, 578);
            this.layoutControlMainWindow.TabIndex = 3;
            this.layoutControlMainWindow.Text = "layoutControl1";
            // 
            // UnitTrackingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1270, 778);
            this.Controls.Add(this.layoutControlMainWindow);
            this.Controls.Add(this.dockPanelDown);
            this.Controls.Add(this.dockPanelRight);
            this.Controls.Add(this.dockPanelLeft);
            this.Controls.Add(this.ribbonControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "UnitTrackingForm";
            this.ShowIcon = false;
            this.Text = "XtraForm1";
            this.Load += new System.EventHandler(this.UnitTrackingForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExUnits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExUnits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExUnitPositionHistory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExUnitPositionHistory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlLeftPanel)).EndInit();
            this.layoutControlLeftPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupLeftPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupUnits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupPositionHistorySearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemUnitPositionSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupPositionHistoryResults)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemGridControlUnitPosition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPlay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemFastForward)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemStop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPause)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupTreeView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.dockPanelLeft.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            this.dockPanelRight.ResumeLayout(false);
            this.dockPanel2_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlRigthPanel)).EndInit();
            this.layoutControlRigthPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupRightPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupUnitDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDepartment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDriver)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemWorkShift)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCustomCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemSchedule)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemUnitType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemUnitId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemZone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemStation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemUnitStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupRoute)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemRoute)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemMileage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemRunTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAverageSpeed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLastStop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLastStopTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemNextStop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAverageStopTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupGPSInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemHasAlarm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDirection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemActualSpeed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemSatellitesCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemSentDate)).EndInit();
            this.dockPanelDown.ResumeLayout(false);
            this.controlContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlDownPanel)).EndInit();
            this.layoutControlDownPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.memoEditObservations.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExAlerts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExAlerts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupDownPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupObservations)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTextEditObs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemGridControlAlerts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemSimpleButtonAllAlerts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemSimpleButtonSelectedUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAlertCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupMainWindow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemMapControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlMainWindow)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

		private GridControlEx gridControlExUnits;
		private GridViewEx gridViewExUnits;
		private GridControlEx gridControlExUnitPositionHistory;
		private GridViewEx gridViewExUnitPositionHistory;
		private DevExpress.XtraBars.Docking.DockManager dockManager1;
		private DevExpress.XtraBars.Docking.DockPanel dockPanelLeft;
		private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupMainWindow;
		private DevExpress.XtraLayout.LayoutControl layoutControlLeftPanel;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupLeftPanel;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupUnits;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemGridControl;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupPositionHistorySearch;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupPositionHistoryResults;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemGridControlUnitPosition;
		private SearchUnitPositionHistory searchUnitPositionHistory1;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemUnitPositionSearch;
		public MapControlEx mapControlEx1;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemMapControl;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.XtraBars.BarButtonItem barButtonZoomIn;
        private DevExpress.XtraBars.BarButtonItem barButtonZoomOut;
        private DevExpress.XtraBars.BarButtonItem barButtonPan;
		private DevExpress.XtraBars.BarButtonItem barButtonCenter;
		internal DevExpress.XtraBars.BarButtonItem barButtonAddPolygon;
		internal DevExpress.XtraBars.BarButtonItem barButtonSelect;
		internal DevExpress.XtraBars.BarButtonItem barButtonAddPosition;
		internal DevExpress.XtraBars.BarButtonItem barButtonClearMap;
        private DevExpress.XtraBars.BarButtonItem barButtonSelectRect;
        private DevExpress.XtraBars.BarButtonItem barButtonSelectRadius;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPageMapControls;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupMapControls;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupAlerts;
        private DevExpress.XtraEditors.SimpleButton fastForwardButton;
        private DevExpress.XtraEditors.SimpleButton stopButton;
        private DevExpress.XtraEditors.SimpleButton playButton;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemPlay;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemFastForward;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemStop;
		private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
		private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox1;
		private DevExpress.XtraEditors.SimpleButton pauseButton;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemPause;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupTreeView;
        private DevExpress.Utils.ToolTipController toolTipController1;		
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupLocation;
		internal DevExpress.XtraBars.BarEditItem comboBoxSelectTool;
		private DevExpress.XtraBars.BarButtonItem barButtonDistance;
		private DevExpress.XtraBars.Docking.DockPanel dockPanelRight;
		private DevExpress.XtraBars.Docking.ControlContainer dockPanel2_Container;
		private DevExpress.XtraLayout.LayoutControl layoutControlRigthPanel;
		private DevExpress.XtraEditors.LabelControl labelControlActualSpeed;
		private DevExpress.XtraEditors.LabelControl labelControlDepartmentType;
		private DevExpress.XtraEditors.LabelControl labelControlWorkShift;
		private DevExpress.XtraEditors.LabelControl labelControlDriver;
		private DevExpress.XtraEditors.LabelControl labelControlAverageStopTime;
		private DevExpress.XtraEditors.LabelControl labelControlZone;
		private DevExpress.XtraEditors.LabelControl labelControlLastStopTime;
		private DevExpress.XtraEditors.LabelControl labelControlSatellitesCount;
		private DevExpress.XtraEditors.LabelControl labelControlLastStop;
		private DevExpress.XtraEditors.LabelControl labelControlSchedule;
		private DevExpress.XtraEditors.LabelControl labelControlDirection;
		private DevExpress.XtraEditors.LabelControl labelControlAverageSpeed;
		private DevExpress.XtraEditors.LabelControl labelControlCustomCode;
		private DevExpress.XtraEditors.LabelControl labelControlLon;
		private DevExpress.XtraEditors.LabelControl labelControlUnitId;
		private DevExpress.XtraEditors.LabelControl labelControlSentTime;
		private DevExpress.XtraEditors.LabelControl labelControlRunTime;
		private DevExpress.XtraEditors.LabelControl labelControlLat;
		private DevExpress.XtraEditors.LabelControl labelControlUnitType;
		private DevExpress.XtraEditors.LabelControl labelControlMileage;
		private DevExpress.XtraEditors.LabelControl labelControlStation;
		private DevExpress.XtraEditors.LabelControl labelControlHasAlarm;
		private DevExpress.XtraEditors.LabelControl labelControlRoute;
		private DevExpress.XtraEditors.LabelControl labelControlNextStop;
		private DevExpress.XtraEditors.LabelControl labelControlUnitStatus;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupRightPanel;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupUnitDetails;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDepartment;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDriver;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemWorkShift;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemCustomCode;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemSchedule;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemUnitType;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemUnitId;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemZone;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemStation;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupRoute;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemRoute;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemMileage;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemRunTime;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemAverageSpeed;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemLastStop;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemLastStopTime;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemNextStop;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemAverageStopTime;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupGPSInfo;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemUnitStatus;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemHasAlarm;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemLat;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemLon;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDirection;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemSentDate;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemActualSpeed;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemSatellitesCount;
		private DevExpress.XtraLayout.LayoutControl layoutControlMainWindow;
		private DevExpress.XtraBars.Docking.DockPanel dockPanelDown;
		private DevExpress.XtraBars.Docking.ControlContainer controlContainer1;
		private DevExpress.XtraLayout.LayoutControl layoutControlDownPanel;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupDownPanel;
		private GridControlEx gridControlExAlerts;
		private GridViewEx gridViewExAlerts;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupObservations;
		private SimpleButtonEx simpleButtonSelectedUnit;
		private SimpleButtonEx simpleButtonAllAlerts;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemGridControlAlerts;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemSimpleButtonAllAlerts;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemSimpleButtonSelectedUnit;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
		private DevExpress.XtraEditors.MemoEdit memoEditObservations;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTextEditObs;
		private DevExpress.XtraBars.BarButtonItem barButtonItemProcessing;
		private DevExpress.XtraBars.BarButtonItem barButtonItemEnd;
		private DevExpress.XtraBars.BarButtonItem barButtonItemObservations;
		private DevExpress.XtraBars.BarButtonItem barButtonItemFollow;
		private DevExpress.XtraBars.BarButtonItem barButtonItemCommands;
		private DevExpress.XtraBars.BarButtonItem barButtonItemSetPosition;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupUnits;
        private MapLayersControl mapLayersControl1;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
		private DevExpress.XtraEditors.LabelControl labelControlAlertCount;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemAlertCount;
	}
}
