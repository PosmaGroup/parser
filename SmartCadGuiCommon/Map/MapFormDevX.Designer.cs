﻿using DevExpress.XtraEditors;
namespace SmartCadGuiCommon
{
    partial class MapFormDevX : XtraForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MapFormDevX));
            this.labelLogo = new System.Windows.Forms.Label();
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barButtonGroupMapControls = new DevExpress.XtraBars.BarButtonGroup();
            this.barButtonZoomIn = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonZoomOut = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonPan = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonCenter = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonSelect = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonAddPosition = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonClearMap = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemHelp = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonDistance = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemUnitTracking = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonAddRoute = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemFleetControl = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemProcessing = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemEnd = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemObservations = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemFollow = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemCommands = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemSetPosition = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemCrimeMap = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemHeatMap = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemLogo = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPageMapControls = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroupMapControls = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupLocation = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupUnitTracking = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupAlerts = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupCrimeMaps = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.ribbonStatusBar1 = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.labelUser = new DevExpress.XtraBars.BarStaticItem();
            this.labelConnected = new DevExpress.XtraBars.BarStaticItem();
            this.labelDate = new DevExpress.XtraBars.BarStaticItem();
            this.xtraTabbedMdiManager = new DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(this.components);
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.ribbonPageGroupFleetcontrol = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroupCallInfo = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.barButtonItemStartRegIncident = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemCreateNewIncident = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemCancelIncident = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPageGroupApplications = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.barButtonItemChat = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemHistory = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPageGroupGeneralOptions = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.barButtonItemPrint = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemSave = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPageGroupTools = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.barButtonItemOpenMap = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.SuspendLayout();
            // 
            // labelLogo
            // 
            this.labelLogo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLogo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.labelLogo.Image = ((System.Drawing.Image)(resources.GetObject("labelLogo.Image")));
            this.labelLogo.Location = new System.Drawing.Point(1223, 2);
            this.labelLogo.Name = "labelLogo";
            this.labelLogo.Size = new System.Drawing.Size(57, 22);
            this.labelLogo.TabIndex = 22;
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ApplicationButtonAnimationLength = 1;
            this.ribbonControl1.BackColor = System.Drawing.Color.White;
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.ExpandCollapseItem.Name = "";
            this.ribbonControl1.GroupAnimationLength = 1;
            this.ribbonControl1.ItemAnimationLength = 1;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.barButtonGroupMapControls,
            this.barButtonZoomIn,
            this.barButtonZoomOut,
            this.barButtonPan,
            this.barButtonCenter,
            this.barButtonSelect,
            this.barButtonAddPosition,
            this.barButtonClearMap,
            this.barButtonItemHelp,
            this.barButtonDistance,
            this.barButtonItemUnitTracking,
            this.barButtonAddRoute,
            this.barButtonItemFleetControl,
            this.barButtonItemProcessing,
            this.barButtonItemEnd,
            this.barButtonItemObservations,
            this.barButtonItemFollow,
            this.barButtonItemCommands,
            this.barButtonItemSetPosition,
            this.barButtonItemCrimeMap,
            this.barButtonItemHeatMap,
            this.barButtonItemLogo});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 46;
            this.ribbonControl1.MdiMergeStyle = DevExpress.XtraBars.Ribbon.RibbonMdiMergeStyle.Always;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.PageAnimationLength = 1;
            this.ribbonControl1.PageHeaderItemLinks.Add(this.barButtonItemHelp);
            this.ribbonControl1.PageHeaderItemLinks.Add(this.barButtonItemLogo);
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPageMapControls});
            this.ribbonControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox1,
            this.repositoryItemImageComboBox1});
            this.ribbonControl1.ShowExpandCollapseButton = DevExpress.Utils.DefaultBoolean.True;
            this.ribbonControl1.ShowToolbarCustomizeItem = false;
            this.ribbonControl1.Size = new System.Drawing.Size(1280, 119);
            this.ribbonControl1.StatusBar = this.ribbonStatusBar1;
            this.ribbonControl1.Toolbar.ShowCustomizeItem = false;
            this.ribbonControl1.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            // 
            // barButtonGroupMapControls
            // 
            this.barButtonGroupMapControls.Caption = "barButtonGroupMapControls";
            this.barButtonGroupMapControls.Id = 2;
            this.barButtonGroupMapControls.Name = "barButtonGroupMapControls";
            // 
            // barButtonZoomIn
            // 
            this.barButtonZoomIn.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.barButtonZoomIn.Caption = "barButtonZoomIn";
            this.barButtonZoomIn.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonZoomIn.Glyph")));
            this.barButtonZoomIn.GroupIndex = 1;
            this.barButtonZoomIn.Id = 4;
            this.barButtonZoomIn.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonZoomIn.Name = "barButtonZoomIn";
            this.barButtonZoomIn.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonZoomIn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonMapTools_ItemClick);
            // 
            // barButtonZoomOut
            // 
            this.barButtonZoomOut.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.barButtonZoomOut.Caption = "barButtonZoomOut";
            this.barButtonZoomOut.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonZoomOut.Glyph")));
            this.barButtonZoomOut.GroupIndex = 1;
            this.barButtonZoomOut.Id = 6;
            this.barButtonZoomOut.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonZoomOut.Name = "barButtonZoomOut";
            this.barButtonZoomOut.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonZoomOut.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonMapTools_ItemClick);
            // 
            // barButtonPan
            // 
            this.barButtonPan.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.barButtonPan.Caption = "barButtonPan";
            this.barButtonPan.Down = true;
            this.barButtonPan.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonPan.Glyph")));
            this.barButtonPan.GroupIndex = 1;
            this.barButtonPan.Id = 7;
            this.barButtonPan.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonPan.Name = "barButtonPan";
            this.barButtonPan.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonPan.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonMapTools_ItemClick);
            // 
            // barButtonCenter
            // 
            this.barButtonCenter.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.barButtonCenter.Caption = "barButtonCenter";
            this.barButtonCenter.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonCenter.Glyph")));
            this.barButtonCenter.GroupIndex = 1;
            this.barButtonCenter.Id = 8;
            this.barButtonCenter.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonCenter.Name = "barButtonCenter";
            this.barButtonCenter.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonCenter.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonMapTools_ItemClick);
            // 
            // barButtonSelect
            // 
            this.barButtonSelect.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.barButtonSelect.Caption = "barButtonSelect";
            this.barButtonSelect.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonSelect.Glyph")));
            this.barButtonSelect.GroupIndex = 1;
            this.barButtonSelect.Id = 10;
            this.barButtonSelect.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonSelect.Name = "barButtonSelect";
            this.barButtonSelect.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonSelect.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonMapTools_ItemClick);
            // 
            // barButtonAddPosition
            // 
            this.barButtonAddPosition.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.barButtonAddPosition.Caption = "barButtonAddPosition";
            this.barButtonAddPosition.Enabled = false;
            this.barButtonAddPosition.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonAddPosition.Glyph")));
            this.barButtonAddPosition.GroupIndex = 1;
            this.barButtonAddPosition.Id = 12;
            this.barButtonAddPosition.MergeType = DevExpress.XtraBars.BarMenuMerge.Remove;
            this.barButtonAddPosition.Name = "barButtonAddPosition";
            this.barButtonAddPosition.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // barButtonClearMap
            // 
            this.barButtonClearMap.Caption = "barButtonClearMap";
            this.barButtonClearMap.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonClearMap.Glyph")));
            this.barButtonClearMap.GroupIndex = 1;
            this.barButtonClearMap.Id = 15;
            this.barButtonClearMap.MergeType = DevExpress.XtraBars.BarMenuMerge.Remove;
            this.barButtonClearMap.Name = "barButtonClearMap";
            this.barButtonClearMap.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // barButtonItemHelp
            // 
            this.barButtonItemHelp.Caption = "Help";
            this.barButtonItemHelp.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemHelp.Glyph")));
            this.barButtonItemHelp.Id = 16;
            this.barButtonItemHelp.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F1);
            this.barButtonItemHelp.Name = "barButtonItemHelp";
            this.barButtonItemHelp.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemHelp_ItemClick);
            // 
            // barButtonDistance
            // 
            this.barButtonDistance.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.barButtonDistance.Caption = "barButtonDistance";
            this.barButtonDistance.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonDistance.Glyph")));
            this.barButtonDistance.GroupIndex = 1;
            this.barButtonDistance.Id = 21;
            this.barButtonDistance.MergeType = DevExpress.XtraBars.BarMenuMerge.Remove;
            this.barButtonDistance.Name = "barButtonDistance";
            this.barButtonDistance.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // barButtonItemUnitTracking
            // 
            this.barButtonItemUnitTracking.Caption = "barButtonItemUnitTracking";
            this.barButtonItemUnitTracking.GroupIndex = 1;
            this.barButtonItemUnitTracking.Id = 24;
            this.barButtonItemUnitTracking.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemUnitTracking.LargeGlyph")));
            this.barButtonItemUnitTracking.Name = "barButtonItemUnitTracking";
            this.barButtonItemUnitTracking.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItemUnitTracking.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemUnitTracking_ItemClick);
            // 
            // barButtonAddRoute
            // 
            this.barButtonAddRoute.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.barButtonAddRoute.Caption = "barButtonAddRoute";
            this.barButtonAddRoute.Enabled = false;
            this.barButtonAddRoute.Id = 32;
            this.barButtonAddRoute.MergeType = DevExpress.XtraBars.BarMenuMerge.Remove;
            this.barButtonAddRoute.Name = "barButtonAddRoute";
            this.barButtonAddRoute.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // barButtonItemFleetControl
            // 
            this.barButtonItemFleetControl.Caption = "barButtonItemFleetControl";
            this.barButtonItemFleetControl.Id = 33;
            this.barButtonItemFleetControl.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemFleetControl.LargeGlyph")));
            this.barButtonItemFleetControl.Name = "barButtonItemFleetControl";
            this.barButtonItemFleetControl.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItemFleetControl.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemFleetControl_ItemClick);
            // 
            // barButtonItemProcessing
            // 
            this.barButtonItemProcessing.Caption = "barButtonItemProcessing";
            this.barButtonItemProcessing.Enabled = false;
            this.barButtonItemProcessing.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemProcessing.Glyph")));
            this.barButtonItemProcessing.Id = 28;
            this.barButtonItemProcessing.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemProcessing.Name = "barButtonItemProcessing";
            // 
            // barButtonItemEnd
            // 
            this.barButtonItemEnd.Caption = "barButtonItemEnd";
            this.barButtonItemEnd.Enabled = false;
            this.barButtonItemEnd.Id = 29;
            this.barButtonItemEnd.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemEnd.Name = "barButtonItemEnd";
            // 
            // barButtonItemObservations
            // 
            this.barButtonItemObservations.Caption = "barButtonItemObservations";
            this.barButtonItemObservations.Enabled = false;
            this.barButtonItemObservations.Id = 30;
            this.barButtonItemObservations.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemObservations.Name = "barButtonItemObservations";
            // 
            // barButtonItemFollow
            // 
            this.barButtonItemFollow.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.barButtonItemFollow.Caption = "barButtonItemFollow";
            this.barButtonItemFollow.Enabled = false;
            this.barButtonItemFollow.Id = 31;
            this.barButtonItemFollow.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemFollow.Name = "barButtonItemFollow";
            // 
            // barButtonItemCommands
            // 
            this.barButtonItemCommands.Caption = "barButtonItemComands";
            this.barButtonItemCommands.Enabled = false;
            this.barButtonItemCommands.Id = 32;
            this.barButtonItemCommands.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemCommands.Name = "barButtonItemCommands";
            // 
            // barButtonItemSetPosition
            // 
            this.barButtonItemSetPosition.Caption = "barButtonItemSetPosition";
            this.barButtonItemSetPosition.Enabled = false;
            this.barButtonItemSetPosition.Id = 33;
            this.barButtonItemSetPosition.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemSetPosition.Name = "barButtonItemSetPosition";
            // 
            // barButtonItemCrimeMap
            // 
            this.barButtonItemCrimeMap.Caption = "barButtonItemCrimeMap";
            this.barButtonItemCrimeMap.GroupIndex = 1;
            this.barButtonItemCrimeMap.Id = 43;
            this.barButtonItemCrimeMap.LargeGlyph = global::SmartCadGuiCommon.Properties.Resources.crimeMapIcon;
            this.barButtonItemCrimeMap.Name = "barButtonItemCrimeMap";
            this.barButtonItemCrimeMap.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItemCrimeMap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemCrimeMap_ItemClick);
            // 
            // barButtonItemHeatMap
            // 
            this.barButtonItemHeatMap.Caption = "barButtonItemHeatMap";
            this.barButtonItemHeatMap.Id = 44;
            this.barButtonItemHeatMap.LargeGlyph = global::SmartCadGuiCommon.Properties.Resources.heatMapIcon;
            this.barButtonItemHeatMap.Name = "barButtonItemHeatMap";
            this.barButtonItemHeatMap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemHeatMap_ItemClick);
            // 
            // barButtonItemLogo
            // 
            this.barButtonItemLogo.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemLogo.Glyph")));
            this.barButtonItemLogo.Id = 45;
            this.barButtonItemLogo.Name = "barButtonItemLogo";
            this.barButtonItemLogo.SmallWithoutTextWidth = 57;
            // 
            // ribbonPageMapControls
            // 
            this.ribbonPageMapControls.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupMapControls,
            this.ribbonPageGroupLocation,
            this.ribbonPageGroupUnitTracking,
            this.ribbonPageGroupAlerts,
            this.ribbonPageGroupCrimeMaps});
            this.ribbonPageMapControls.Name = "ribbonPageMapControls";
            this.ribbonPageMapControls.Text = "ribbonPageMapControls";
            // 
            // ribbonPageGroupMapControls
            // 
            this.ribbonPageGroupMapControls.AllowMinimize = false;
            this.ribbonPageGroupMapControls.AllowTextClipping = false;
            this.ribbonPageGroupMapControls.ItemLinks.Add(this.barButtonZoomIn);
            this.ribbonPageGroupMapControls.ItemLinks.Add(this.barButtonZoomOut);
            this.ribbonPageGroupMapControls.ItemLinks.Add(this.barButtonCenter);
            this.ribbonPageGroupMapControls.ItemLinks.Add(this.barButtonSelect);
            this.ribbonPageGroupMapControls.ItemLinks.Add(this.barButtonPan);
            this.ribbonPageGroupMapControls.ItemLinks.Add(this.barButtonDistance);
            this.ribbonPageGroupMapControls.Name = "ribbonPageGroupMapControls";
            this.ribbonPageGroupMapControls.ShowCaptionButton = false;
            this.ribbonPageGroupMapControls.Text = "ribbonPageGroupMapControls";
            // 
            // ribbonPageGroupLocation
            // 
            this.ribbonPageGroupLocation.AllowMinimize = false;
            this.ribbonPageGroupLocation.AllowTextClipping = false;
            this.ribbonPageGroupLocation.ItemLinks.Add(this.barButtonAddPosition);
            this.ribbonPageGroupLocation.ItemLinks.Add(this.barButtonClearMap);
            this.ribbonPageGroupLocation.ItemLinks.Add(this.barButtonAddRoute);
            this.ribbonPageGroupLocation.Name = "ribbonPageGroupLocation";
            this.ribbonPageGroupLocation.ShowCaptionButton = false;
            this.ribbonPageGroupLocation.Text = "ribbonPageGroupLocation";
            // 
            // ribbonPageGroupUnitTracking
            // 
            this.ribbonPageGroupUnitTracking.AllowMinimize = false;
            this.ribbonPageGroupUnitTracking.AllowTextClipping = false;
            this.ribbonPageGroupUnitTracking.ItemLinks.Add(this.barButtonItemUnitTracking);
            this.ribbonPageGroupUnitTracking.ItemLinks.Add(this.barButtonItemFleetControl, true);
            this.ribbonPageGroupUnitTracking.ItemLinks.Add(this.barButtonItemFollow);
            this.ribbonPageGroupUnitTracking.ItemLinks.Add(this.barButtonItemSetPosition);
            this.ribbonPageGroupUnitTracking.ItemLinks.Add(this.barButtonItemCommands);
            this.ribbonPageGroupUnitTracking.Name = "ribbonPageGroupUnitTracking";
            this.ribbonPageGroupUnitTracking.ShowCaptionButton = false;
            this.ribbonPageGroupUnitTracking.Text = "ribbonPageGroupUnitTracking";
            this.ribbonPageGroupUnitTracking.Visible = false;
            // 
            // ribbonPageGroupAlerts
            // 
            this.ribbonPageGroupAlerts.AllowMinimize = false;
            this.ribbonPageGroupAlerts.AllowTextClipping = false;
            this.ribbonPageGroupAlerts.ItemLinks.Add(this.barButtonItemProcessing);
            this.ribbonPageGroupAlerts.ItemLinks.Add(this.barButtonItemEnd);
            this.ribbonPageGroupAlerts.ItemLinks.Add(this.barButtonItemObservations);
            this.ribbonPageGroupAlerts.Name = "ribbonPageGroupAlerts";
            this.ribbonPageGroupAlerts.ShowCaptionButton = false;
            this.ribbonPageGroupAlerts.Text = "ribbonPageGroupAlerts";
            this.ribbonPageGroupAlerts.Visible = false;
            // 
            // ribbonPageGroupCrimeMaps
            // 
            this.ribbonPageGroupCrimeMaps.ItemLinks.Add(this.barButtonItemCrimeMap);
            this.ribbonPageGroupCrimeMaps.ItemLinks.Add(this.barButtonItemHeatMap);
            this.ribbonPageGroupCrimeMaps.Name = "ribbonPageGroupCrimeMaps";
            this.ribbonPageGroupCrimeMaps.Text = "ribbonPageGroupCrimeMaps";
            this.ribbonPageGroupCrimeMaps.Visible = false;
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.SpinDown)});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AutoHeight = false;
            this.repositoryItemImageComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            // 
            // ribbonStatusBar1
            // 
            this.ribbonStatusBar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ribbonStatusBar1.ItemLinks.Add(this.labelUser);
            this.ribbonStatusBar1.ItemLinks.Add(this.labelConnected);
            this.ribbonStatusBar1.ItemLinks.Add(this.labelDate);
            this.ribbonStatusBar1.Location = new System.Drawing.Point(0, 727);
            this.ribbonStatusBar1.Name = "ribbonStatusBar1";
            this.ribbonStatusBar1.Ribbon = this.ribbonControl1;
            this.ribbonStatusBar1.Size = new System.Drawing.Size(1280, 23);
            // 
            // labelUser
            // 
            this.labelUser.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUser.Appearance.Options.UseFont = true;
            this.labelUser.Caption = "Usuario";
            this.labelUser.Id = 26;
            this.labelUser.Name = "labelUser";
            this.labelUser.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // labelConnected
            // 
            this.labelConnected.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelConnected.Appearance.Options.UseFont = true;
            this.labelConnected.Caption = "Connected";
            this.labelConnected.Id = 25;
            this.labelConnected.Name = "labelConnected";
            this.labelConnected.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // labelDate
            // 
            this.labelDate.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.labelDate.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDate.Appearance.Options.UseFont = true;
            this.labelDate.Caption = "Date";
            this.labelDate.Id = 27;
            this.labelDate.Name = "labelDate";
            this.labelDate.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // xtraTabbedMdiManager
            // 
            this.xtraTabbedMdiManager.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.xtraTabbedMdiManager.BorderStylePage = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.xtraTabbedMdiManager.ClosePageButtonShowMode = DevExpress.XtraTab.ClosePageButtonShowMode.InActiveTabPageHeader;
            this.xtraTabbedMdiManager.HeaderButtons = ((DevExpress.XtraTab.TabButtons)(((DevExpress.XtraTab.TabButtons.Prev | DevExpress.XtraTab.TabButtons.Next) 
            | DevExpress.XtraTab.TabButtons.Default)));
            this.xtraTabbedMdiManager.HeaderButtonsShowMode = DevExpress.XtraTab.TabButtonShowMode.WhenNeeded;
            this.xtraTabbedMdiManager.MdiParent = this;
            this.xtraTabbedMdiManager.SetNextMdiChildMode = DevExpress.XtraTabbedMdi.SetNextMdiChildMode.TabControl;
            // 
            // panelControl1
            // 
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1280, 750);
            this.panelControl1.TabIndex = 1;
            this.panelControl1.Visible = false;
            // 
            // defaultLookAndFeel1
            // 
            this.defaultLookAndFeel1.LookAndFeel.SkinName = "Blue";
            // 
            // ribbonPageGroupFleetcontrol
            // 
            this.ribbonPageGroupFleetcontrol.AllowMinimize = false;
            this.ribbonPageGroupFleetcontrol.AllowTextClipping = false;
            this.ribbonPageGroupFleetcontrol.Name = "ribbonPageGroupFleetcontrol";
            this.ribbonPageGroupFleetcontrol.ShowCaptionButton = false;
            this.ribbonPageGroupFleetcontrol.Text = "ribbonPageGroupFleetcontrol";
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupCallInfo,
            this.ribbonPageGroupApplications,
            this.ribbonPageGroupGeneralOptions,
            this.ribbonPageGroupTools});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "ribbonPage1";
            // 
            // ribbonPageGroupCallInfo
            // 
            this.ribbonPageGroupCallInfo.AllowMinimize = false;
            this.ribbonPageGroupCallInfo.AllowTextClipping = false;
            this.ribbonPageGroupCallInfo.ItemLinks.Add(this.barButtonItemStartRegIncident);
            this.ribbonPageGroupCallInfo.ItemLinks.Add(this.barButtonItemCreateNewIncident, true);
            this.ribbonPageGroupCallInfo.ItemLinks.Add(this.barButtonItemCancelIncident);
            this.ribbonPageGroupCallInfo.Name = "ribbonPageGroupCallInfo";
            this.ribbonPageGroupCallInfo.ShowCaptionButton = false;
            this.ribbonPageGroupCallInfo.Text = "ribbonPageGroupCallInfo";
            // 
            // barButtonItemStartRegIncident
            // 
            this.barButtonItemStartRegIncident.Caption = "ItemStartRegIncident";
            this.barButtonItemStartRegIncident.Enabled = false;
            this.barButtonItemStartRegIncident.Id = 12;
            this.barButtonItemStartRegIncident.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemStartRegIncident.LargeGlyph")));
            this.barButtonItemStartRegIncident.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemStartRegIncident.Name = "barButtonItemStartRegIncident";
            this.barButtonItemStartRegIncident.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonItemCreateNewIncident
            // 
            this.barButtonItemCreateNewIncident.Caption = "ItemCreateNewIncident";
            this.barButtonItemCreateNewIncident.Enabled = false;
            this.barButtonItemCreateNewIncident.Id = 13;
            this.barButtonItemCreateNewIncident.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemCreateNewIncident.LargeGlyph")));
            this.barButtonItemCreateNewIncident.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemCreateNewIncident.Name = "barButtonItemCreateNewIncident";
            this.barButtonItemCreateNewIncident.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonItemCancelIncident
            // 
            this.barButtonItemCancelIncident.Caption = "ItemCancelIncident";
            this.barButtonItemCancelIncident.Enabled = false;
            this.barButtonItemCancelIncident.Id = 15;
            this.barButtonItemCancelIncident.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemCancelIncident.LargeGlyph")));
            this.barButtonItemCancelIncident.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemCancelIncident.Name = "barButtonItemCancelIncident";
            this.barButtonItemCancelIncident.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // ribbonPageGroupApplications
            // 
            this.ribbonPageGroupApplications.AllowMinimize = false;
            this.ribbonPageGroupApplications.AllowTextClipping = false;
            this.ribbonPageGroupApplications.ItemLinks.Add(this.barButtonItemChat);
            this.ribbonPageGroupApplications.ItemLinks.Add(this.barButtonItemHistory);
            this.ribbonPageGroupApplications.Name = "ribbonPageGroupApplications";
            this.ribbonPageGroupApplications.ShowCaptionButton = false;
            this.ribbonPageGroupApplications.Text = "Applications";
            // 
            // barButtonItemChat
            // 
            this.barButtonItemChat.Caption = "ItemChat";
            this.barButtonItemChat.Id = 5;
            this.barButtonItemChat.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemChat.LargeGlyph")));
            this.barButtonItemChat.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemChat.Name = "barButtonItemChat";
            this.barButtonItemChat.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItemChat.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barButtonItemHistory
            // 
            this.barButtonItemHistory.Caption = "History";
            this.barButtonItemHistory.Id = 21;
            this.barButtonItemHistory.LargeGlyph = global::SmartCadGuiCommon.Properties.Resources.Historial;
            this.barButtonItemHistory.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemHistory.Name = "barButtonItemHistory";
            this.barButtonItemHistory.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // ribbonPageGroupGeneralOptions
            // 
            this.ribbonPageGroupGeneralOptions.AllowMinimize = false;
            this.ribbonPageGroupGeneralOptions.AllowTextClipping = false;
            this.ribbonPageGroupGeneralOptions.ItemLinks.Add(this.barButtonItemPrint);
            this.ribbonPageGroupGeneralOptions.ItemLinks.Add(this.barButtonItemSave);
            this.ribbonPageGroupGeneralOptions.ItemLinks.Add(this.barButtonItemRefresh);
            this.ribbonPageGroupGeneralOptions.Name = "ribbonPageGroupGeneralOptions";
            this.ribbonPageGroupGeneralOptions.ShowCaptionButton = false;
            this.ribbonPageGroupGeneralOptions.Text = "ribbonPageGroupGeneralOptions";
            // 
            // barButtonItemPrint
            // 
            this.barButtonItemPrint.Caption = "ItemPrint";
            this.barButtonItemPrint.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPrint.Glyph")));
            this.barButtonItemPrint.Id = 6;
            this.barButtonItemPrint.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemPrint.Name = "barButtonItemPrint";
            // 
            // barButtonItemSave
            // 
            this.barButtonItemSave.Caption = "ItemSave";
            this.barButtonItemSave.Glyph = global::SmartCadGuiCommon.PrintRibbonControllerResources.RibbonPrintPreview_Save;
            this.barButtonItemSave.Id = 7;
            this.barButtonItemSave.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemSave.Name = "barButtonItemSave";
            // 
            // barButtonItemRefresh
            // 
            this.barButtonItemRefresh.Caption = "ItemRefresh";
            this.barButtonItemRefresh.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemRefresh.Glyph")));
            this.barButtonItemRefresh.Id = 8;
            this.barButtonItemRefresh.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemRefresh.Name = "barButtonItemRefresh";
            // 
            // ribbonPageGroupTools
            // 
            this.ribbonPageGroupTools.AllowMinimize = false;
            this.ribbonPageGroupTools.AllowTextClipping = false;
            this.ribbonPageGroupTools.ItemLinks.Add(this.barButtonItemOpenMap);
            this.ribbonPageGroupTools.Name = "ribbonPageGroupTools";
            this.ribbonPageGroupTools.ShowCaptionButton = false;
            this.ribbonPageGroupTools.Text = "ribbonPageGroupTools";
            // 
            // barButtonItemOpenMap
            // 
            this.barButtonItemOpenMap.Caption = "ItemOpenMap";
            this.barButtonItemOpenMap.Id = 4;
            this.barButtonItemOpenMap.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemOpenMap.LargeGlyph")));
            this.barButtonItemOpenMap.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemOpenMap.Name = "barButtonItemOpenMap";
            this.barButtonItemOpenMap.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "barButtonItemHelp";
            this.barButtonItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.Glyph")));
            this.barButtonItem1.Id = 0;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "barButtonItem4";
            this.barButtonItem4.Id = 24;
            this.barButtonItem4.Name = "barButtonItem4";
            this.barButtonItem4.SmallWithoutTextWidth = 70;
            // 
            // MapFormDevX
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1280, 750);
            this.Controls.Add(this.ribbonStatusBar1);
            this.Controls.Add(this.labelLogo);
            this.Controls.Add(this.ribbonControl1);
            this.Controls.Add(this.panelControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(1278, 726);
            this.Name = "MapFormDevX";
            this.Text = "MapFormDevX";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MapFormDevX_FormClosing);
            this.Load += new System.EventHandler(this.MapFormDevX_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MapFormDevX_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MapFormDevX_KeyPress);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.XtraBars.BarButtonGroup barButtonGroupMapControls;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
		private DevExpress.XtraBars.BarButtonItem barButtonItemHelp;
        private System.Windows.Forms.Label labelLogo;
        private DevExpress.XtraTabbedMdi.XtraTabbedMdiManager xtraTabbedMdiManager;
        internal DevExpress.XtraBars.Ribbon.RibbonPage ribbonPageMapControls;
        internal DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupMapControls;
        internal DevExpress.XtraBars.BarButtonItem barButtonZoomIn;
        internal DevExpress.XtraBars.BarButtonItem barButtonZoomOut;
        internal DevExpress.XtraBars.BarButtonItem barButtonPan;
        internal DevExpress.XtraBars.BarButtonItem barButtonCenter;
        internal DevExpress.XtraBars.BarButtonItem barButtonSelect;
        internal DevExpress.XtraBars.BarButtonItem barButtonAddPosition;
        internal DevExpress.XtraBars.BarButtonItem barButtonClearMap;
		internal DevExpress.XtraBars.BarButtonItem barButtonDistance;
		internal DevExpress.XtraBars.BarButtonItem barButtonItemUnitTracking;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupUnitTracking;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupLocation;
        internal DevExpress.XtraBars.BarButtonItem barButtonAddRoute;
		private DevExpress.XtraBars.BarButtonItem barButtonItemFleetControl;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupFleetcontrol;
		private DevExpress.XtraBars.BarButtonItem barButtonItemProcessing;
		private DevExpress.XtraBars.BarButtonItem barButtonItemEnd;
		private DevExpress.XtraBars.BarButtonItem barButtonItemObservations;
		private DevExpress.XtraBars.BarButtonItem barButtonItemFollow;
		private DevExpress.XtraBars.BarButtonItem barButtonItemCommands;
		private DevExpress.XtraBars.BarButtonItem barButtonItemSetPosition;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupAlerts;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemCrimeMap;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupCrimeMaps;
        private DevExpress.XtraBars.BarButtonItem barButtonItemHeatMap;
        private DevExpress.XtraBars.BarButtonItem barButtonItemLogo;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar1;
        private DevExpress.XtraBars.BarStaticItem labelUser;
        private DevExpress.XtraBars.BarStaticItem labelConnected;
        private DevExpress.XtraBars.BarStaticItem labelDate;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupCallInfo;
        public DevExpress.XtraBars.BarButtonItem barButtonItemStartRegIncident;
        public DevExpress.XtraBars.BarButtonItem barButtonItemCreateNewIncident;
        private DevExpress.XtraBars.BarButtonItem barButtonItemCancelIncident;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupApplications;
        private DevExpress.XtraBars.BarButtonItem barButtonItemChat;
        private DevExpress.XtraBars.BarButtonItem barButtonItemHistory;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupGeneralOptions;
        private DevExpress.XtraBars.BarButtonItem barButtonItemPrint;
        private DevExpress.XtraBars.BarButtonItem barButtonItemSave;
        private DevExpress.XtraBars.BarButtonItem barButtonItemRefresh;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupTools;
        private DevExpress.XtraBars.BarButtonItem barButtonItemOpenMap;
        public DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
    }
}
