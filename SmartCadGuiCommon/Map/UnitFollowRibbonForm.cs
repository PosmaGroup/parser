using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Collections;
using Smartmatic.SmartCad.Service;
using SmartCadCore.ClientData;
using SmartCadCore.Core;
using SmartCadControls.Controls;
using SmartCadCore.Common;
using Smartmatic.SmartCad.Map;

namespace SmartCadGuiCommon
{
	public partial class UnitFollowRibbonForm : DevExpress.XtraEditors.XtraForm
	{
		public IDictionary<string, ApplicationPreferenceClientData> GlobalApplicationPreference { get; set; }
        public List<int> sendingUnits = new List<int>();

		public UnitFollowRibbonForm()
		{
			InitializeComponent();
            UnitFollowForm.CreatedLayers = false;
			this.mapLayersControl1.GlobalApplicationPreference = GlobalApplicationPreference;
            this.mapLayersControl1.TreeViewMapLayerChange += new EventHandler<EventArgs>(mapLayersControl1_TreeViewMapLayerChange);
            this.mapLayersControl1.MapControlEx = MapControlEx.GetInstance(SmartCadConfiguration.SmartCadSection.ServerElement.Map.Dll);

            if (mapLayersControl1.MapControlEx != null)
            {
                mapLayersControl1.MapControlEx.InitializeMaps();
                mapLayersControl1.MapControlEx.Initialized += new EventHandler(mapControlEx_Initialized);
            }

            xtraTabbedMdiManager1.SelectedPageChanged += new EventHandler(xtraTabbedMdiManager1_SelectedPageChanged);
		}

        void mapLayersControl1_TreeViewMapLayerChange(object sender, EventArgs e)
        {
            foreach (UnitFollowForm form in MdiChildren)
            {
                //if (page != xtraTabbedMdiManager1.SelectedPage)
                //{
                    mapLayersControl1.MapControlEx = form.mapControlEx1;
                    mapLayersControl1.UpdateMapsLayersFromTreeView();
                //}
            }

            mapLayersControl1.MapControlEx = ((UnitFollowForm)xtraTabbedMdiManager1.SelectedPage.MdiChild).mapControlEx1;
        }

        void xtraTabbedMdiManager1_SelectedPageChanged(object sender, EventArgs e)
        {

        }

        void mapControlEx_Initialized(object sender, EventArgs e)
        {
            mapLayersControl1.CreateMapsLayersTreeView();
            mapLayersControl1.CreateMapLayersSmartCad("UnitFollow", false);
            mapLayersControl1.HideSmartCadContent();
        }

		private void barButtonItemCascade_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
			xtraTabbedMdiManager1.MdiParent = null;
			this.LayoutMdi(MdiLayout.Cascade);			
		}

		private void barButtonItemArrange_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
			xtraTabbedMdiManager1.MdiParent = this;
		}

		private void barButtonItemTileHorizontal_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
			xtraTabbedMdiManager1.MdiParent = null;
			LayoutMdi(MdiLayout.TileHorizontal);
		}

		private void barButtonItemTileVertical_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
			xtraTabbedMdiManager1.MdiParent = null;
			LayoutMdi(MdiLayout.TileVertical);
		}

		private void UnitFollowRibbonForm_Load(object sender, EventArgs e)
		{
			LoadLanguage();
		}

		private void LoadLanguage()
		{
			barButtonItemArrange.Caption = ResourceLoader.GetString2("Arrange");
			barButtonItemCascade.Caption = ResourceLoader.GetString2("Cascade");
			barButtonItemTileHorizontal.Caption = ResourceLoader.GetString2("TileHorizontal");
			barButtonItemTileVertical.Caption = ResourceLoader.GetString2("TileVertical");
			Text = ResourceLoader.GetString2("UnitFollowRibbonForm");
            this.ribbonPage1.Text = ResourceLoader.GetString2("ViewWindows");
            this.ribbonPageGroup1.Text = ResourceLoader.GetString2("WindowsOrder");
            dockPanel1.Text = ResourceLoader.GetString2("Display");
		}

		private void UnitFollowRibbonForm_FormClosing(object sender, FormClosingEventArgs e)
		{
            sendingUnits = new List<int>();
		}		
	}
}