using SmartCadControls.Controls;
using Smartmatic.SmartCad.Map;
namespace SmartCadGuiCommon
{
	partial class UnitFollowForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UnitFollowForm));
            this.layoutConverter1 = new DevExpress.XtraLayout.Converter.LayoutConverter(this.components);
            this.UnitFollowFormConvertedLayout = new DevExpress.XtraLayout.LayoutControl();
            this.mapControlEx1 = new MapControlEx();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.toolTipController1 = new DevExpress.Utils.ToolTipController(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.UnitFollowFormConvertedLayout)).BeginInit();
            this.UnitFollowFormConvertedLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // UnitFollowFormConvertedLayout
            // 
            this.UnitFollowFormConvertedLayout.Appearance.DisabledLayoutGroupCaption.ForeColor = System.Drawing.SystemColors.GrayText;
            this.UnitFollowFormConvertedLayout.Appearance.DisabledLayoutGroupCaption.Options.UseForeColor = true;
            this.UnitFollowFormConvertedLayout.Appearance.DisabledLayoutItem.ForeColor = System.Drawing.SystemColors.GrayText;
            this.UnitFollowFormConvertedLayout.Appearance.DisabledLayoutItem.Options.UseForeColor = true;
            this.UnitFollowFormConvertedLayout.Controls.Add(this.mapControlEx1);
            this.UnitFollowFormConvertedLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.UnitFollowFormConvertedLayout.Location = new System.Drawing.Point(0, 0);
            this.UnitFollowFormConvertedLayout.Name = "UnitFollowFormConvertedLayout";
            this.UnitFollowFormConvertedLayout.Root = this.layoutControlGroup1;
            this.UnitFollowFormConvertedLayout.Size = new System.Drawing.Size(361, 275);
            this.UnitFollowFormConvertedLayout.TabIndex = 1;
            // 
            // mapControlEx1
            // 
            this.mapControlEx1.Distance = 0;
            this.mapControlEx1.DistanceUnit = "mts";
            this.mapControlEx1.Location = new System.Drawing.Point(7, 7);
            this.mapControlEx1.Name = "mapControlEx1";
            this.mapControlEx1.PostFixTables = null;
            this.mapControlEx1.ShowDistance = false;
            this.mapControlEx1.Size = new System.Drawing.Size(348, 262);
            this.mapControlEx1.TabIndex = 0;
            this.mapControlEx1.ToolInUse = null;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(361, 275);
            this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.mapControlEx1;
            this.layoutControlItem1.CustomizationFormText = "MapControlExitem";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "MapControlExitem";
            this.layoutControlItem1.Size = new System.Drawing.Size(359, 273);
            this.layoutControlItem1.Text = "MapControlExitem";
            this.layoutControlItem1.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // UnitFollowForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(361, 275);
            this.Controls.Add(this.UnitFollowFormConvertedLayout);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "UnitFollowForm";
            this.Text = "UnitFollowForm";
            this.Load += new System.EventHandler(this.UnitFollowForm_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.UnitFollowForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.UnitFollowFormConvertedLayout)).EndInit();
            this.UnitFollowFormConvertedLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

		public MapControlEx mapControlEx1;
		private DevExpress.XtraLayout.Converter.LayoutConverter layoutConverter1;
		private DevExpress.XtraLayout.LayoutControl UnitFollowFormConvertedLayout;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.Utils.ToolTipController toolTipController1;
	}
}