using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Net;
using Smartmatic.SmartCad.Service;
using System.Collections;
using System.IO;
using System.Xml;
using System.Threading;
using System.Diagnostics;
using System.ServiceModel;
using DevExpress.XtraLayout.Utils;
using DevExpress.XtraLayout;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.Utils;
using SmartCadCore.Common;
using SmartCadControls.Util;
using SmartCadCore.ClientData;
using SmartCadControls.Controls;
using SmartCadCore.Core;
using SmartCadControls.SyncBoxes;
using SmartCadControls;
using SmartCadCore.Enums;
using Smartmatic.SmartCad.Map;

namespace SmartCadGuiCommon
{
	public partial class UnitTrackingForm : DevExpress.XtraEditors.XtraForm
	{
		public enum PlayerStatus
		{
			Stoped,
			Playing,
			FastForward,
			NotStarted,
			Finished,
			Backwards
		}

		#region Consts
		private static Color[] trackColors = { Color.FromArgb(18,5,250),Color.FromArgb(83,210,51),Color.FromArgb(248,242,16),Color.FromArgb(255,0,0),
												  Color.FromArgb(7,223,248),Color.FromArgb(163,159,168),Color.FromArgb(252,109,11),Color.FromArgb(20,1,20),
												  Color.FromArgb(164,92,40),Color.FromArgb(235,18,219)};
		#endregion

		#region Fields

		private int unitsPositionRecoder = 0;
		private int ratePlayer = 1000;
		private object syncCommited = new object();
		private bool following = false;
		private System.Drawing.Rectangle bounds;
        private List<GeoPoint> points = new List<GeoPoint>();
		private System.Windows.Forms.Timer playerTimer;
		private System.Windows.Forms.Timer alertsTimer;
        private List<MapPoint> unitsRecoder;
		private GridControlDataUnitDatePosition selectedGridDataBeforePlay;
		private PlayerStatus playerStatus = PlayerStatus.NotStarted;		
		private Dictionary<string, Color> unitColors;
		private UnitClientData selectedPosition;
		private int maxTimeToShowAlerts;
		//MessageBalloon messageBallon;
        private UnitClientData auxSelectedUnit;
        private IncidentClientData auxSelectedInc;
        Dictionary<string, List<GPSClientData>> positions = new Dictionary<string, List<GPSClientData>>();
		private int xxaux;
        private int yyaux;
		private int alertCount = 0;
		private IEnumerable<string> myDepartments;
		#endregion

		#region Propertiers

		public IDictionary<string, ApplicationPreferenceClientData> GlobalApplicationPreference
		{
			get
			{
				return (this.MdiParent as MapFormDevX).GlobalApplicationPreference;
			}
		}

		public bool Tracking { get; set; }

		public UnitClientData SelectedPosition
		{
			get
			{
				return selectedPosition;
			}
			set
			{
				selectedPosition = value;
				ThreadPool.QueueUserWorkItem(new WaitCallback(FillRightPanelInformation));
			}
		}  
		
		#endregion

		#region Constructor

        public UnitTrackingForm(Form mdiParent, TreeListEx originalTreeList, bool tracking)
        {
            InitializeComponent();

            MdiParent = mdiParent;
            
            Tracking = tracking;
            if (Tracking == true)
            {
                playerTimer = new System.Windows.Forms.Timer(this.components);
                playerTimer.Tick += Reproduce;
            }

            LoadBarButtonsComboBox();
            this.FormClosing += new FormClosingEventHandler(UnitTrackingForm_FormClosing);
            
            LoadMap();
        }

        void UnitTrackingForm_Load(object sender, System.EventArgs e)
        {
            LoadLanguage();
            SetDesktopBounds(bounds.X, bounds.Y, this.Width, this.Height);

            SetMapProperties();

            //Esconder hasta decidir que se va a hacer con el...
            layoutControlItemDriver.Visibility = LayoutVisibility.Never;
            this.mapLayersControl1.GlobalApplicationPreference = GlobalApplicationPreference;
            mapLayersControl1.MapControlEx.InitializeMaps();
        }

        private void SetMapProperties()
        {
            barButtonCenter.Tag = MapControlEx.CENTER_CODE;
            barButtonPan.Tag = MapControlEx.PAN_CODE;
            barButtonSelect.Tag = MapControlEx.SELECT_CODE;
            barButtonSelectRadius.Tag = MapControlEx.SELECT_RADIUS_CODE;
            barButtonSelectRect.Tag = MapControlEx.SELECT_RECT_CODE;
            barButtonZoomIn.Tag = MapControlEx.ZOOM_IN_CODE;
            barButtonZoomOut.Tag = MapControlEx.ZOOM_OUT_CODE;
            barButtonAddPolygon.Tag = MapControlEx.ADD_POLYGON_CODE;
            barButtonAddPosition.Tag = MapControlEx.ADD_POSITION_CODE;
            barButtonDistance.Tag = MapControlEx.ADD_POLYLINE_CODE;
            unitsRecoder = new List<MapPoint>();
            this.mapControlEx1.SetLeftButtonTool(MapControlEx.PAN_CODE);
            // this.mapControlEx1.CenterMap(5120);
        }

        private void LoadMap()
        {
            //The map is not updated becouse the main map is already open.

            layoutControlMainWindow.BeginUpdate();
            layoutControlMainWindow.Controls.Remove(mapControlEx1);
            mapControlEx1 = MapControlEx.GetNewInstance(SmartCadConfiguration.SmartCadSection.ServerElement.Map.Dll);
            layoutControlMainWindow.Controls.Add(mapControlEx1);
            
            layoutControlGroupMainWindow.Items.RemoveAt(0);
            layoutControlItemMapControl.Control = mapControlEx1;
            layoutControlGroupMainWindow.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            layoutControlItemMapControl});
            layoutControlMainWindow.Dock = DockStyle.Fill;

            layoutControlMainWindow.EndUpdate();

            //Designer
            mapControlEx1.Location = new System.Drawing.Point(7, 7);
            mapControlEx1.Size = new System.Drawing.Size(699, 540);
            mapControlEx1.TabIndex = 4;
            mapControlEx1.ShowDistance = false;
            mapControlEx1.Distance = 0;
            mapLayersControl1.MapControlEx = mapControlEx1;

            //InitializeSyncboxes();

            mapLayersControl1.MapControlEx.Initialized += new EventHandler(mapControlEx1_Initialized);
        }

        void mapControlEx1_Initialized(object sender, EventArgs e)
        {
            if (Tracking == true)
            {
                Thread t = new Thread(() =>
                {
                    //se ejecuta dos veces para verificar.
                    if (ServerServiceClient.GetInstance().DevicePing(ServerServiceClient.Device.GPS) == false &&
                       (ServerServiceClient.GetInstance().DevicePing(ServerServiceClient.Device.GPS) == false))
                    {
                        FormUtil.InvokeRequired(this, () =>
                        {
                            searchUnitPositionHistory1.Enabled = false;
                            gridControlExUnitPositionHistory.Enabled = false;
                            MessageForm.Show(ResourceLoader.GetString2("NoAvlConnectionStringCofigurated"), MessageFormType.Information);
                        });
                    }
                });
                t.Start();

                mapLayersControl1.CreateMapsLayersTreeView();
                mapLayersControl1.CreateMapLayersSmartCad("UnitTracking", false);
                mapLayersControl1.MapControlEx.CreateTable(new MapObjectTrack(),
                    mapLayersControl1.MapControlEx.DynTableTracksName, true, false, 100, 100);
                mapLayersControl1.MapControlEx.CreateTable(new MapObjectUnitTrack(),
                    mapLayersControl1.MapControlEx.DynTableUnitTracksName, true, false, 100, 100);
                
                dockPanelDown.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
                barButtonItemEnd.Enabled = false;
                barButtonItemObservations.Enabled = false;
                barButtonItemProcessing.Enabled = false;
                barButtonItemFollow.Enabled = false;
                barButtonItemSetPosition.Enabled = false;
                barButtonItemCommands.Enabled = false;
            }
            else
            {
                mapLayersControl1.CreateMapsLayersTreeView();
                mapLayersControl1.CreateMapLayersSmartCad("FleetControl");
                
                layoutControlGroupPositionHistorySearch.Visibility = LayoutVisibility.Never;
                layoutControlGroupPositionHistoryResults.Visibility = LayoutVisibility.Never;
                simpleButtonAllAlerts.LookAndFeel.UseDefaultLookAndFeel = false;
                simpleButtonSelectedUnit.LookAndFeel.UseDefaultLookAndFeel = false;
                simpleButtonAllAlerts.LookAndFeel.SkinName = "Black";

                simpleButtonAllAlerts.LookAndFeel.SkinName = "Black";
                simpleButtonSelectedUnit.LookAndFeel.SkinName = "Blue";

                //Disable until its will be tested.
                barButtonItemCommands.Enabled = false;
            }
            ServerServiceClient.GetInstance().CommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(UnitTrackingForm_CommittedChanges);

            mapLayersControl1.MapControlEx.ReloadMap();
            mapLayersControl1.MapControlEx.RecalculateAll();

            InitializeSyncboxes();
        }

        void UnitTrackingForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (alertsTimer != null && alertsTimer.Enabled)
                alertsTimer.Stop();
            ServerServiceClient.GetInstance().CommittedChanges -= new EventHandler<CommittedObjectDataCollectionEventArgs>(UnitTrackingForm_CommittedChanges);
        }

		#endregion

		private void LoadBarButtonsComboBox()
		{
			ImageComboBoxItem item = new ImageComboBoxItem("", this.barButtonSelect, 0);
			this.repositoryItemImageComboBox1.Items.Add(item);
			item = new ImageComboBoxItem("", this.barButtonSelectRadius, 1);
			this.repositoryItemImageComboBox1.Items.Add(item);
			item = new ImageComboBoxItem("", this.barButtonSelectRect, 2);
			this.repositoryItemImageComboBox1.Items.Add(item);
		}

		void UnitTrackingForm_CommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
		{
			try
			{
                if (e.Objects != null && e.Objects.Count > 0)
                {
                    lock (syncCommited)
                    {
                        try
                        {
                            if (ServerServiceClient.GetInstance().OperatorClient == null)
                            { }
                        }
                        catch
                        { }
                        if (e.Objects[0] is AlertNotificationClientData && Tracking == false)
                        {
                            #region AlertNotificationClientData
                            AlertNotificationClientData ancd = e.Objects[0] as AlertNotificationClientData;
                            if (e.Action.Equals(CommittedDataAction.Delete) == false && myDepartments.Contains(ancd.UnitAlert.UnitDepartmentTypeName))
                            {
                                gridControlExAlerts.AddOrUpdateItem(new GridControlDataAlertNotification(ancd));
                                FormUtil.InvokeRequired(this, delegate
                                {
                                    if (ancd.Status.Equals(AlertNotificationClientData.AlertStatus.NotTaken))
                                    {
                                        alertCount++;
                                        labelControlAlertCount.Text = ResourceLoader.GetString2("NotAttendedAlarms") + ": " + (alertCount >= 0 ? alertCount : alertCount = 0);
                                    }
                                    else if (ancd.Status.Equals(AlertNotificationClientData.AlertStatus.InProcess))
                                    {
                                        alertCount--;
                                        labelControlAlertCount.Text = ResourceLoader.GetString2("NotAttendedAlarms") + ": " + (alertCount >= 0 ? alertCount : alertCount = 0);
                                    }
                                });
                                if (gridControlExAlerts.SelectedItems.Count > 0)
                                {
                                    AlertNotificationClientData selectedAncd = (gridControlExAlerts.SelectedItems[0] as GridControlDataAlertNotification).Tag as AlertNotificationClientData;
                                    if (selectedAncd.Equals(ancd))
                                    {
                                        FormUtil.InvokeRequired(this, new MethodInvoker(UpdateObservations)); ;
                                    }
                                }
                            }
                            else
                                gridControlExAlerts.DeleteItem(new GridControlDataAlertNotification(ancd));

                            #endregion
                        }
                        else if (e.Objects[0] is UnitClientData)
                        {
                            #region UnitClientData
                            if (e.Action.Equals(CommittedDataAction.Delete) == false)
                            {
                                gridControlExUnits.AddOrUpdateItem(new GridControlDataUnitInfo(e.Objects[0] as UnitClientData));
                                if (Tracking == true)
                                {
                                    GridControlData gcd = gridControlExUnitPositionHistory.GetGridControlData(e.Objects[0] as UnitClientData);
                                    if (gcd != null)
                                    {
                                        gcd.Tag = e.Objects[0] as UnitClientData;
                                        gridControlExUnitPositionHistory.AddOrUpdateItem(gcd);
                                        FormUtil.InvokeRequired(this, delegate
                                        {
                                            FillRightPanelInformation(new object());
                                        });
                                    }
                                }
                            }
                            else
                                gridControlExUnits.DeleteItem(new GridControlDataUnitInfo(e.Objects[0] as UnitClientData));
                            #endregion
                        }
                        else if (e.Objects[0] is GPSClientData && Tracking == false)
                        {
                            #region GPSClientData

                            GPSClientData gps = (e.Objects[0] as GPSClientData);
                            FormUtil.InvokeRequired(this, delegate
                            {
                                UnitClientData unit = new UnitClientData();
                                unit.Code = gps.UnitCode;
                                int index = gridControlExUnits.Items.IndexOf(new GridControlDataUnitInfo(unit));
                                if (index >= 0)
                                {
                                    GridControlDataUnitInfo grid = (GridControlDataUnitInfo)gridControlExUnits.Items[index];
                                    UnitClientData item = (UnitClientData)(grid.Unit);
                                    item.Lat = gps.Lat;
                                    item.Lon = gps.Lon;
                                    item.Heading = gps.Heading;
                                    item.CoordinatesDate = gps.CoordinatesDate;
                                    item.Satellites = (int)gps.Satellites;
                                    item.Speed = gps.Speed;

                                    gridControlExUnits.BeginUpdate();
                                    gridControlExUnits.AddOrUpdateItem(grid);
                                    gridControlExUnits.EndUpdate();
                                    if (gridControlExUnits.SelectedItems.Count > 0 && gridControlExUnits.SelectedItems[0].Equals(grid))
                                        FillRightPanelInformation(new object());

                                }
                            });
                            #endregion
                        }
                    }
                }
			}
			catch
			{

			}
		}

		private void UpdateObservations()
		{
			memoEditObservations.Text = string.Empty;
			AlertNotificationClientData selectedAncd = (gridControlExAlerts.SelectedItems[0] as GridControlDataAlertNotification).Tag as AlertNotificationClientData;
			
			foreach (AlertNotificationObservationClientData anocd in selectedAncd.Observations)
				memoEditObservations.Text += string.Format("{0}:\r\n\r\n{1}\r\n\r\n",anocd.Operator.ToString() + " " + anocd.Date.ToString(), anocd.Text);
		}
		
		private void InitializeSyncboxes()
		{
			gridControlExUnits.Type = typeof(GridControlDataUnitInfo);
            gridViewExUnits.Columns["CustomCode"].Caption = ResourceLoader.GetString2("PlateNo");
			foreach (UnitClientData unit in mapLayersControl1.GetUnits())
				gridControlExUnits.AddOrUpdateItem(new GridControlDataUnitInfo(unit));

			this.gridViewExUnits.OptionsView.ShowAutoFilterRow = true;
			this.gridControlExUnits.ViewTotalRows = true;
            this.gridViewExUnits.OptionsSelection.MultiSelect = false;

			if (Tracking == true)
			{
				this.gridControlExUnitPositionHistory.Type = typeof(GridControlDataUnitDatePosition);
                this.gridViewExUnitPositionHistory.Columns["CustomCode"].Caption = ResourceLoader.GetString2("PlateNo");
				this.gridViewExUnitPositionHistory.OptionsView.ShowAutoFilterRow = true;
				this.gridControlExUnitPositionHistory.ViewTotalRows = true;
			}
			else
			{
				this.gridControlExAlerts.Type = typeof(GridControlDataAlertNotification);
				this.gridViewExAlerts.OptionsView.ShowAutoFilterRow = true;
				this.gridControlExAlerts.ViewTotalRows = true;

                int maxTimeToShowAlerts = int.Parse(GlobalApplicationPreference["MaxTimeToShowAlerts"].Value);

				Dictionary<string, IList> querys = new Dictionary<string, IList>();
				querys["AlertNotificationData"] = new ArrayList();
				querys["AlertNotificationData"]
					.Add(SmartCadHqls
							.GetCustomHql(@"Select alert From AlertNotificationData alert where alert.Status !=2 OR (alert.Status = 2 AND alert.Date > '{0}')",
							ApplicationUtil
							.GetDataBaseFormattedDate(ServerServiceClient
															.GetInstance()
															.GetTime()
                                                            .Subtract(new TimeSpan(0, maxTimeToShowAlerts, 0)))));
				querys["DepartmentsName"] = new ArrayList();
				querys["DepartmentsName"].Add(SmartCadHqls.GetCustomHql(SmartCadHqls.GetDepartmentsTypeByOperatorCode,
					ServerServiceClient.GetInstance().OperatorClient.Code));
				querys = ServerServiceClient.GetInstance().SearchClientObjectsMaxRowsMultiQuery(true,querys);

				myDepartments = querys["DepartmentsName"].Cast<DepartmentTypeClientData>().Select<DepartmentTypeClientData, string>(dep => dep != null ? dep.Name : string.Empty);
				
				foreach (AlertNotificationClientData alert in querys["AlertNotificationData"])
				{
					if (myDepartments.Contains(alert.UnitAlert.UnitDepartmentTypeName))
					{
						gridControlExAlerts.AddOrUpdateItem(new GridControlDataAlertNotification(alert));
						if (alert.Status.Equals(AlertNotificationClientData.AlertStatus.NotTaken))
							alertCount++;
					}
				}
				labelControlAlertCount.Text = ResourceLoader.GetString2("NotAttendedAlarms") + ": " + alertCount.ToString();

				alertsTimer = new System.Windows.Forms.Timer();
				alertsTimer.Interval = 60000;
				alertsTimer.Tick += new EventHandler(CheckAlertNotificationStatus);
				alertsTimer.Start();
			}
		}

		private void CheckAlertNotificationStatus(object sender, EventArgs e)
		{
			int i= 0;
			while(i<gridControlExAlerts.Items.Count)
			{
				GridControlDataAlertNotification gcdan = gridControlExAlerts.Items[i] as GridControlDataAlertNotification;
				AlertNotificationClientData alert = gcdan.Tag as AlertNotificationClientData;
                int maxTimeToShowAlerts = int.Parse(GlobalApplicationPreference["MaxTimeToShowAlerts"].Value);
                if (alert.GetFinishDate() < ServerServiceClient.GetInstance().GetTime().Subtract(new TimeSpan(0, maxTimeToShowAlerts, 0)) &&
					alert.Status.Equals(AlertNotificationClientData.AlertStatus.Ended))
				{
					gridControlExAlerts.DeleteItem(gcdan);
					i--;
				}
				i++;
			}
			searchUnitPositionHistory1.dateEditEnd.Properties.MaxValue = 
				searchUnitPositionHistory1.dateEditStart.Properties.MaxValue = DateTime.Now;
		}

		private void LoadLanguage()
		{
			this.Text = Tracking ? ResourceLoader.GetString2("HistoricTracks") : ResourceLoader.GetString2("FleetControl"); ;
			
			this.barButtonPan.Caption = ResourceLoader.GetString2("Pan");
			this.barButtonSelect.Caption = ResourceLoader.GetString2("Select");
			this.barButtonSelectRadius.Caption = ResourceLoader.GetString2("SelectRadius");
			this.barButtonSelectRect.Caption = ResourceLoader.GetString2("SelectRect");
			this.barButtonZoomIn.Caption = ResourceLoader.GetString2("ZoomIn");
			this.barButtonZoomOut.Caption = ResourceLoader.GetString2("ZoomOut");
			this.barButtonDistance.Caption = ResourceLoader.GetString2("Distance");
			this.barButtonClearMap.Caption = ResourceLoader.GetString2("ClearMap");
			this.barButtonCenter.Caption = ResourceLoader.GetString2("Center");
			this.barButtonAddPolygon.Caption = ResourceLoader.GetString2("AddPolygon");
			this.barButtonAddPosition.Caption = ResourceLoader.GetString2("AddPosition");
			this.barButtonItemEnd.Caption = ResourceLoader.GetString2("ToEnd");
			this.barButtonItemProcessing.Caption = ResourceLoader.GetString2("ToProcess");
			this.barButtonItemObservations.Caption = ResourceLoader.GetString2("Observations");
			this.barButtonItemCommands.Caption = ResourceLoader.GetString2("Commands");
            this.barButtonItemCommands.Glyph = ResourceLoader.GetImage("$Image.Commands");
			this.barButtonItemFollow.Caption = ResourceLoader.GetString2("Follow");
			this.barButtonItemSetPosition.Caption = ResourceLoader.GetString2("SetPosition");
			this.comboBoxSelectTool.Caption = ResourceLoader.GetString2("ComboBoxSelectTool");
			
			ribbonPageGroupMapControls.Text = ResourceLoader.GetString2("Tools");
			ribbonPageGroupAlerts.Text = ResourceLoader.GetString2("Alerts");
			ribbonPageMapControls.Text = ResourceLoader.GetString2("Options");
			ribbonPageGroupLocation.Text = ResourceLoader.GetString2("Location");
			ribbonPageGroupUnits.Text = ResourceLoader.GetString2("Units");

			LoadLeftPanelLanguage();
			LoadDownPanelLanguage();
			LoadRightPanelLanguage();			
		}

		private void LoadLeftPanelLanguage()
		{
			this.layoutControlGroupUnits.Text = ResourceLoader.GetString2("Units");
			this.layoutControlGroupPositionHistorySearch.Text = ResourceLoader.GetString2("Show");
			this.layoutControlGroupPositionHistoryResults.Text = ResourceLoader.GetString2("Points");
			this.layoutControlGroupTreeView.Text = ResourceLoader.GetString2("Visualization");
			this.dockPanelLeft.Text = ResourceLoader.GetString2("FollowToolsMaps");			
		}

		private void LoadDownPanelLanguage()
		{
			layoutControlGroupObservations.Text = ResourceLoader.GetString2("Observations");
			dockPanelDown.Text = ResourceLoader.GetString2("Alerts");
			simpleButtonAllAlerts.Text = ResourceLoader.GetString2("Fleets");
			simpleButtonSelectedUnit.Text = ResourceLoader.GetString2("SelectedUnit");
		}

		private void LoadRightPanelLanguage()
		{
			labelControlActualSpeed.Text = ResourceLoader.GetString2("ActualSpeed") + ": ";
			labelControlCustomCode.Text = ResourceLoader.GetString2("Plate") + ": ";
			labelControlDepartmentType.Text = ResourceLoader.GetString2("DepartmentType") + ": ";
			labelControlDirection.Text = ResourceLoader.GetString2("Direction") + ": ";
			labelControlDriver.Text = ResourceLoader.GetString2("Driver") + ": ";
			labelControlLat.Text = ResourceLoader.GetString2("Lat") + ": ";
			labelControlLon.Text = ResourceLoader.GetString2("Lon") + ": ";
			labelControlRoute.Text = ResourceLoader.GetString2("Route") + ": ";
			labelControlSatellitesCount.Text = ResourceLoader.GetString2("SatellitesCount") + ": ";
			labelControlSchedule.Text = ResourceLoader.GetString2("DayTrip") + ": ";
			labelControlSentTime.Text = ResourceLoader.GetString2("SentTime") + ": ";
			labelControlStation.Text = ResourceLoader.GetString2("Station") + ": ";
			labelControlUnitId.Text = ResourceLoader.GetString2("UnitId") + ": ";
			labelControlUnitStatus.Text = ResourceLoader.GetString2("Status") + ": ";
			labelControlUnitType.Text = ResourceLoader.GetString2("UnitType") + ": ";
			labelControlWorkShift.Text = ResourceLoader.GetString2("WorkShift") + ": ";
			labelControlZone.Text = ResourceLoader.GetString2("Zone") + ": ";
			labelControlAverageSpeed.Text = ResourceLoader.GetString2("AverageSpeed") + ": ";
			labelControlAverageStopTime.Text = ResourceLoader.GetString2("AverageStopTime") + ": ";
			labelControlHasAlarm.Text = ResourceLoader.GetString2("Alarm") + ": ";
			labelControlLastStop.Text = ResourceLoader.GetString2("LastStop") + ": ";
			labelControlLastStopTime.Text = ResourceLoader.GetString2("StopTime") + ": ";
			labelControlMileage.Text = ResourceLoader.GetString2("Mileage") + ": ";
			labelControlNextStop.Text = ResourceLoader.GetString2("NextStop") + ": ";
			labelControlRunTime.Text = ResourceLoader.GetString2("RunTime") + ": ";
            dockPanelRight.Text = ResourceLoader.GetString2("Information");
			layoutControlGroupRoute.Text = ResourceLoader.GetString2("Route");
			layoutControlGroupUnitDetails.Text = ResourceLoader.GetString2("Unit");
			layoutControlGroupGPSInfo.Text = ResourceLoader.GetString2("GpsInfo");
		}

		public void DrawUnitsTrack(DateTime start, DateTime end)
		{
			Random random = new Random((int)ServerServiceClient.GetInstance().GetTime().Ticks);
            mapControlEx1.ClearTable(mapControlEx1.DynTableTracksName);
            mapControlEx1.ClearTable(mapControlEx1.DynTableUnitTracksName);
			gridControlExUnitPositionHistory.ClearData();
			unitColors = new Dictionary<string, Color>();

			int max = gridControlExUnits.SelectedItems.Count;

			ProgressForm form = new ProgressForm(new TaskFunctionsDelegate(GetUnitsPositionHistory), new object[2]{start,end }, ResourceLoader.GetString2("SearchTrackHistory"));
			form.ButtonCancelVisibility = LayoutVisibility.Always;
			form.Max = max;
			form.CanThrowError = true;
			form.ShowDialog();
			IList data = new ArrayList();
			int i = 0;

			foreach (List<GPSClientData> unitList in positions.Values)
			{
				Color color = trackColors[i++];

                List<GeoPoint> points = new List<GeoPoint>();

				if (unitList.Count > 0)
				{
					UnitClientData unit = GetSelectedUnit(((GPSClientData)unitList[0]).Name);
					
					unitColors.Add(unit.CustomCode, color);

                    Dictionary<string, int> unitsCodes = new Dictionary<string, int>();

                    foreach (GPSClientData pos in unitList)
					{
                        pos.UnitCustomCode = unit.CustomCode;
                        pos.UnitCode = unit.Code;

                        if (unitsCodes.ContainsKey(pos.Name) == false)
                            unitsCodes.Add(pos.Name, 0);
                        else
                            unitsCodes[pos.Name] += 1;

                        GridControlDataUnitDatePosition gridData = 
                            new GridControlDataUnitDatePosition(pos) 
                            { Number = unitsCodes[pos.Name], Color = color };

                        points.Add(new GeoPoint(pos.Lon, pos.Lat));
						data.Add(gridData);
					}
					if (points.Count > 0)
					{
						mapControlEx1.InsertObject(new MapObjectTrack(unit, color, points));
					}						
				}
			}
			gridControlExUnitPositionHistory.DataSource = data;			
		}

		private void GetUnitsPositionHistory(ChangeTaskDelegate changeTaskFunction, ChangeValueDelegate changeValueFunction, object[] arguments)
		{
			positions = new Dictionary<string, List<GPSClientData>>();
            List<ClientData> results = new List<ClientData>();
			int min = 0;
			int max = 1000;
			int i = 0;
			try
			{
				DateTime startDate = (DateTime)arguments[0];
				DateTime endDate = (DateTime)arguments[1];

				foreach (GridControlDataUnitInfo item in gridControlExUnits.SelectedItems)
				{
                    changeTaskFunction(ResourceLoader.GetString2("SearchUnitInfo") + item.CustomCode);
					min = 0;
					max = 1000;
					changeValueFunction(i++);
					while ((results = ServerServiceClient.GetInstance().GetHistory(startDate, endDate, "GPS", item.Unit.IdGPS, min, max)).Count > 0)
					{
						if (positions.ContainsKey(item.CustomCode))
							positions[item.CustomCode].AddRange(results.Cast<GPSClientData>());
						else
                            positions.Add(item.CustomCode, results.Cast<GPSClientData>().ToList());

						min = max;
						max += 1000;
					}
				}
			}
			catch
			{

			}
		}

		private void gridViewExUnitPositionHistory_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
		{
            if (gridViewExUnitPositionHistory.FocusedRowHandle >= 0 && gridControlExUnitPositionHistory.SelectedItems.Count > 0)
			{
                GridControlDataUnitDatePosition gridControlDataUnitDatePosition = (GridControlDataUnitDatePosition)gridControlExUnitPositionHistory.SelectedItems[0];
                
                bool changeUnit = SelectedPosition == null ||
                    SelectedPosition.Code != gridControlDataUnitDatePosition.UnitPosition.Code;

                SelectedPosition = GetSelectedUnit(gridControlDataUnitDatePosition.UnitPosition.Name);
                SelectedPosition.IdGPS = gridControlDataUnitDatePosition.UnitPosition.Name;
                SelectedPosition.Lon = gridControlDataUnitDatePosition.UnitPosition.Lon;
                SelectedPosition.Lat = gridControlDataUnitDatePosition.UnitPosition.Lat;

                MapObjectUnitTrack mOUT = new MapObjectUnitTrack(SelectedPosition);

                if (playerTimer.Enabled == false)
                {
                    this.playButton.Enabled = true;

                    //mapControlEx1.DrawPing(new GeoPoint(selectedPosition.Lon, selectedPosition.Lat));

                    if (changeUnit)
                    {
                        mapControlEx1.DeleteObject(mOUT);
                        mapControlEx1.InsertObject(mOUT);
                    }
                    else
                    {
                        mapControlEx1.UpdateObject(mOUT);
                    }
                    mapControlEx1.CenterMapInPoint(mOUT.Position);
                }
                else
                {
                    mapControlEx1.UpdateObject(mOUT);
                    mapControlEx1.CenterMapInPoint(mOUT.Position);
                }
			}
			else
				LoadRightPanelLanguage();
		}

		private void FillRightPanelInformation(object obj)
		{
			if (selectedPosition != null)
			{
				Dictionary<string, string> values = new Dictionary<string, string>();
				values.Add(labelControlActualSpeed.Name, ResourceLoader.GetString2("ActualSpeed") + ": " + selectedPosition.Speed.ToString());
				values.Add(labelControlCustomCode.Name, ResourceLoader.GetString2("Plate") + ": " + selectedPosition.CustomCode);
				values.Add(labelControlDepartmentType.Name, ResourceLoader.GetString2("DepartmentType") + ": " + selectedPosition.DepartmentType.Name);
				//values.Add(labelControlDriver.Name, ResourceLoader.GetString2("Conductor") + ": " + assigns[0].Officer.FirstName + " " + assigns[0].Officer.LastName);
				values.Add(labelControlLat.Name, ResourceLoader.GetString2("Lat") + ": " + selectedPosition.Lat.ToString());
				values.Add(labelControlLon.Name, ResourceLoader.GetString2("Lon") + ": " + selectedPosition.Lon.ToString());
				values.Add(labelControlStation.Name, ResourceLoader.GetString2("Station") + ": " + selectedPosition.DepartmentStation.Name);
				values.Add(labelControlUnitId.Name, ResourceLoader.GetString2("UnitId") + ": " + selectedPosition.IdUnit);
				values.Add(labelControlUnitStatus.Name, ResourceLoader.GetString2("Status") + ": " + selectedPosition.Status.FriendlyName);
				values.Add(labelControlSatellitesCount.Name, ResourceLoader.GetString2("SatellitesCount") + ": " + selectedPosition.Satellites.ToString());
				values.Add(labelControlZone.Name, ResourceLoader.GetString2("Zone") + ": " + selectedPosition.DepartmentStation.DepartmentZone.Name);
				values.Add(labelControlSentTime.Name, ResourceLoader.GetString2("SentTime") + ": " + selectedPosition.CoordinatesDate.ToString());
				values.Add(labelControlUnitType.Name, ResourceLoader.GetString2("UnitType") + ": " + selectedPosition.Type.Name);
				values.Add(labelControlDirection.Name, ResourceLoader.GetString2("Direction") + ": " + GetHeadingName(selectedPosition.Heading));
				values.Add(labelControlHasAlarm.Name, ResourceLoader.GetString2("Alarm") + ": " + GetAlertName());
				Dictionary<string, IList> querys = GetWorkShiftRoutes();

				if (querys["WorkShiftRoutes"].Count > 0)
				{
					WorkShiftRouteClientData wsr = (WorkShiftRouteClientData)querys["WorkShiftRoutes"][0];
					values.Add(labelControlRoute.Name, ResourceLoader.GetString2("Route") + ": " + wsr.Route.Name);
					values.Add(labelControlWorkShift.Name, ResourceLoader.GetString2("WorkShift") + ": " + wsr.WorkShiftName);
					WorkShiftScheduleVariationClientData schedule = GetDayTripSchedule(wsr);
					if (schedule != null)
					{
						values.Add(labelControlSchedule.Name, ResourceLoader.GetString2("DayTrip") + ": " + schedule.ToString());

						TimeSpan runTime = selectedPosition.CoordinatesDate.Subtract(schedule.Start);
						values.Add(labelControlRunTime.Name, ResourceLoader.GetString2("RunTime") + ": " + runTime.ToString());

						double mileage = GetMileageForThisPoints(ServerServiceClient.GetInstance().GetRunTimePoints(selectedPosition, schedule), selectedPosition);
						values.Add(labelControlMileage.Name, ResourceLoader.GetString2("Mileage") + ": " + Math.Round(mileage, 3).ToString());

						double avgSpeed = ServerServiceClient.GetInstance().GetAverageSpeed(selectedPosition, schedule);
						values.Add(labelControlAverageSpeed.Name, ResourceLoader.GetString2("AverageSpeed") + ": " + Math.Round(avgSpeed, 3).ToString());
					}
					RouteAddressClientData stop = ServerServiceClient.GetInstance().GetLastStop(selectedPosition, wsr.Route);
					if (stop != null)
					{
						values.Add(labelControlLastStop.Name, ResourceLoader.GetString2("LastStop") + ": " + stop.Name);
						values.Add(labelControlLastStopTime.Name, ResourceLoader.GetString("StopTime") + ": " + stop.StopTime.ToString());
						values.Add(labelControlNextStop.Name, ResourceLoader.GetString2("NextStop") + ": " + GetNextStopName(wsr, stop));
					}
					TimeSpan avgStopTime = ServerServiceClient.GetInstance().GetAverageStopTime(selectedPosition, wsr);

					values.Add(labelControlAverageStopTime.Name, ResourceLoader.GetString("AverageStopTime") + ": " + avgStopTime.ToString());
				}
				FormUtil.InvokeRequired(this, delegate
				{
					foreach (Control label in layoutControlRigthPanel.Controls)
						if (label is DevExpress.XtraEditors.LabelControl &&
							values.ContainsKey(label.Name) == true)
							label.Text = values[label.Name];
				});
			}
		}

		private string GetAlertName()
		{
			if (gridControlExAlerts.Items.Count > 0)
				try
				{
					return gridControlExAlerts
						   .Items
						   .Cast<GridControlDataAlertNotification>()
						   .Where(data => data.Plate.Equals(selectedPosition.CustomCode) &&
							   data.Status.Equals(ResourceLoader.GetString2(AlertNotificationClientData.AlertStatus.Ended.ToString())) == false)
						   .OrderBy(data => data.Date)
						   .Last()
						   .AlertName;
				}
				catch
				{

				}
			return string.Empty;
		}

		private Dictionary<string, IList> GetWorkShiftRoutes()
		{
			Dictionary<string, IList> querys = new Dictionary<string, IList>();
			querys["WorkShiftRoutes"] = new ArrayList();
			DateTime date = selectedPosition.CoordinatesDate == DateTime.MinValue ? ServerServiceClient.GetInstance().GetTime() : selectedPosition.CoordinatesDate; 
			querys["WorkShiftRoutes"].Add(SmartCadHqls.GetCustomHql(
				SmartCadHqls.GetWorkShiftRouteDataWithSchedulesByUnitCodeAndDate,
				selectedPosition.Code,
				ApplicationUtil.GetDataBaseFormattedDate(date)));
			querys["WorkShiftRoutes"].Add(SmartCadHqls.GetCustomHql(
				SmartCadHqls.GetWorkShiftRouteDataWithRouteAddressByUnitCode,
					selectedPosition.Code));
			querys["WorkShiftRoutes"].Add(SmartCadHqls.GetCustomHql(
				SmartCadHqls.GetWorkShiftRouteDataWithByUnitCodeAndDate,
				selectedPosition.Code,
				ApplicationUtil.GetDataBaseFormattedDate(date)));
			querys = ServerServiceClient.GetInstance().SearchClientObjectsMaxRowsMultiQuery(true, querys);
			return querys;
		}

		private WorkShiftScheduleVariationClientData GetDayTripSchedule(WorkShiftRouteClientData wsr)
		{
			try
			{
				return wsr.WorkShiftShedules
					 .Cast<WorkShiftScheduleVariationClientData>()
					 .Single<WorkShiftScheduleVariationClientData>(
						 schedule => selectedPosition.CoordinatesDate >= schedule.Start &&
						 selectedPosition.CoordinatesDate < schedule.End);
			}
			catch
			{
				return null;
			}
		}

		private string GetNextStopName(WorkShiftRouteClientData wsr, RouteAddressClientData stop)
		{
			foreach (RouteAddressClientData routeAddress in wsr.Route.RouteAddress.Cast<RouteAddressClientData>().OrderBy(address => address.PointNumber))
				if (routeAddress.PointNumber > stop.PointNumber &&
					string.IsNullOrEmpty(routeAddress.Name) == false)
					return routeAddress.Name;
			
			return string.Empty;
		}

		private double GetMileageForThisPoints(IList list,UnitClientData unit)
		{
			double result = 0.0;

			if (list.Count > 0)
			{
				GPSUnitData unit1 = list[0] as GPSUnitData;
				for (int i = 1; i < list.Count; i++)
				{
					GPSUnitData unit2 = list[i] as GPSUnitData;
					result += GeoCodeCalc.CalcDistance(unit1.lat, unit1.lon, unit2.lat, unit2.lon);
					unit1 = list[i] as GPSUnitData;
				}
			}
			return result;
		}

		private string GetHeadingName(double p)
		{
			if (p == 0 || p == 360)
				return "N";
			else if (p > 0 && p < 90)
				return "NE";
			else if (p == 90)
				return "E";
			else if (p > 90 && p < 180)
				return "SE";
			else if (p == 180)
				return "S";
			else if (p > 180 && p < 270)
				return "SO";
			else if (p == 270)
				return "O";
			else if (p > 270 && p < 360)
				return "NO";
			return "";
		}

		private void barButtonZoomIn_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
            if (e.Item.Tag.ToString() == MapControlEx.ADD_POLYLINE_CODE)
            {
                mapLayersControl1.CurrentShapeType = ShapeType.Distance;
            }
            else
            {
                mapLayersControl1.CurrentShapeType = ShapeType.None;
            }
            mapControlEx1.SetLeftButtonTool(e.Item.Tag.ToString());
		}

		private void backButton_Click(object sender, EventArgs e)
		{

		}

		private void playButton_Click(object sender, EventArgs e)
		{
            if (gridControlExUnitPositionHistory.SelectedItems.Count > 0)
            {
                selectedGridDataBeforePlay = (GridControlDataUnitDatePosition)gridControlExUnitPositionHistory.SelectedItems[0];
                if (playerStatus == PlayerStatus.NotStarted)
                {
                    //mapControlEx1.DeleteAllUnitTracks();
                    EnableButtonsPlayers(true);
                    //Por ahora
                    //DeleteAllTracksUnless();
                    unitsRecoder = GetAllPointsForPlayer();
                    //InsertUnitInTable();
                    mapControlEx1.InsertObject(new MapObjectUnitTrack(SelectedPosition));

                    unitsPositionRecoder = selectedGridDataBeforePlay.Number;

                    playerStatus = PlayerStatus.Playing;
                    playerTimer.Interval = ratePlayer;
                    playerTimer.Start();
                    
                }
                else if (playerStatus == PlayerStatus.Stoped)
                {
                    //playerTimer.Interval = ratePlayer;
                    playerTimer.Start();
                }
            }
		}

		private void DeleteAllTracksUnless()
		{
			GridControlDataUnitDatePosition var = selectedGridDataBeforePlay;

            mapControlEx1.DeleteAllTracksUnless(var.UnitPosition.UnitCode);
		}

		private void InsertUnitInTable()
		{
            GridControlDataUnitDatePosition var = selectedGridDataBeforePlay;
            UnitClientData unit = new UnitClientData();
            unit.Code = var.UnitPosition.UnitCode;
            unit.IdGPS = var.UnitPosition.Name;
            unit.Lat = var.UnitPosition.Lat;
            unit.Lon = var.UnitPosition.Lon;
            unit.CustomCode = var.UnitPosition.UnitCustomCode;
            
            string iconName = (string)ServerServiceClient.GetInstance().SearchBasicObject(
                SmartCadHqls.GetCustomHql("select data.Type.Image from UnitData data where data.GPS is not null AND data.GPS.Name = '{0}'", var.UnitPosition.Name));
            
            if (string.IsNullOrEmpty(iconName))
                unit.IconName = ResourceLoader.GetString("DefaultIconUnitType") + ".BMP";
            else
                unit.IconName = iconName;

            mapControlEx1.InsertObject(new MapObjectUnitTrack(unit));
		}

        private List<MapPoint> GetAllPointsForPlayer()
		{
			List<GridControlDataUnitDatePosition> listFinal = gridControlExUnitPositionHistory.Items.Cast<GridControlDataUnitDatePosition>().Where(
																gdata => gdata.CustomCode == selectedGridDataBeforePlay.CustomCode).ToList();

            List<MapPoint> retval = listFinal.ConvertAll<MapPoint>(delegate(GridControlDataUnitDatePosition data)
			{
                MapPoint obj = new MapObjectUnit(GetSelectedUnit(data.UnitPosition.Name));
                return obj;
			});
			return retval;
		}

        private UnitClientData GetSelectedUnit(string idGPS)
        {
            GridControlDataUnitInfo selected = gridControlExUnits.SelectedItems.Cast<GridControlDataUnitInfo>().Single(
                                gridData => gridData.Unit.IdGPS.Equals(idGPS));
            if(selected != null)
                return selected.Unit;
            return SelectedPosition;
        }

		private void EnableButtonsPlayers(bool enable)
		{
			this.pauseButton.Enabled = enable;
			this.stopButton.Enabled = enable;
			this.fastForwardButton.Enabled = enable;
		}

		private void Reproduce(object sender, EventArgs e)
		{
            playerTimer.Interval = int.MaxValue;
            if (unitsPositionRecoder >= unitsRecoder.Count
                //|| mapControlEx1.ReproduceTrack(unitsRecoder[unitsPositionRecoder]) == true
                )
            {
                EnableButtonsPlayers(false);
                stopButton_Click(null, null);
                playerTimer.Stop();
                //RestoreAlltracks();
            }
            else
            {

                gridViewExUnitPositionHistory.FocusedRowHandle += 1;

                //mapControlEx1.DrawPing(unitsRecoder[unitsPositionRecoder].Position);

                //mapControlEx1.ReproduceTrack(unitsRecoder[unitsPositionRecoder]);

                //mapControlEx1.DrawPing(unitsRecoder[unitsPositionRecoder].Position);

                //mapControlEx1.UpdateObject(new MapObjectUnitTrack(SelectedPosition));

                unitsPositionRecoder++;

                playerTimer.Interval = ratePlayer;
            }
		}

		private void RestoreAlltracks()
		{
			GridControlDataUnitDatePosition var = selectedGridDataBeforePlay;
            mapControlEx1.RestoreAllTracksUnless(var.UnitPosition.UnitCode);
        }

		private void stopButton_Click(object sender, EventArgs e)
		{
			//playerTimer.Interval = int.MaxValue;
            playerTimer.Stop();
			playerStatus = PlayerStatus.NotStarted;
            EnableButtonsPlayers(false);
		}

		private void fastForwardButton_Click(object sender, EventArgs e)
		{
            ratePlayer /= 2;
			playerStatus = PlayerStatus.FastForward;
		}

		private void pauseButton_Click(object sender, EventArgs e)
		{
            playerTimer.Stop();
			//playerTimer.Interval = int.MaxValue;
			playerStatus = PlayerStatus.Stoped;
		}

		void gridViewExUnitPositionHistory_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
		{
			if (e.RowHandle < 0 && e.RowHandle != GridControlEx.AutoFilterRowHandle &&
				e.RowHandle != GridControlEx.InvalidRowHandle &&
				unitColors != null )
			{
				string text = gridViewExUnitPositionHistory.GetGroupRowValue(e.RowHandle) as string;
				if(text != null && unitColors.ContainsKey(text))
					e.Appearance.BackColor= unitColors[text];
			}
		}

		private void barButtonSelect_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
			mapControlEx1.SetLeftButtonTool(e.Item.Tag.ToString());
		}

		private void barButtonDistance_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
			mapControlEx1.SetLeftButtonTool(e.Item.Tag.ToString());
		}

        private void barButtonClearMap_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            mapLayersControl1.ClearMap();
            string s = Keys.Escape.ToString();
        }

		private void comboBoxSelectTool_EditValueChanged(object sender, EventArgs e)
		{
			object obj = comboBoxSelectTool.EditValue;
			if (obj != null)
			{
				DevExpress.XtraBars.BarButtonItem item = obj as DevExpress.XtraBars.BarButtonItem;
				item.PerformClick();
			}
		}

		private void simpleButtonSelectedUnit_Click(object sender, EventArgs e)
		{
			if ((sender as SimpleButtonEx).LookAndFeel.SkinName.Equals("Black") == false)
			{
				this.simpleButtonAllAlerts.LookAndFeel.SkinName = "Blue";
				(sender as SimpleButtonEx).LookAndFeel.SkinName = "Black";
				if (selectedPosition != null)
				{
					gridViewExAlerts.ActiveFilterCriteria = new DevExpress.Data.Filtering.BinaryOperator("Plate", selectedPosition.CustomCode);
				}
			}
		}

		private void simpleButtonAllAlerts_Click(object sender, EventArgs e)
		{
			if ((sender as SimpleButtonEx).LookAndFeel.SkinName.Equals("Black") == false)
			{
				this.simpleButtonSelectedUnit.LookAndFeel.SkinName = "Blue";
				(sender as SimpleButtonEx).LookAndFeel.SkinName = "Black";
				gridViewExAlerts.ActiveFilterString = "";
			}
		}

		private void barButtonItemProcessing_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
			if (gridControlExAlerts.SelectedItems.Count > 0)
			{
				AlertNotificationClientData alert = (AlertNotificationClientData)((GridControlDataAlertNotification)gridControlExAlerts.SelectedItems[0]).Tag;
				ProcessAlertForm processAlertForm = new ProcessAlertForm(ProcessAlertForm.ProcessType.ToProcess, alert);
				processAlertForm.ShowDialog();
				gridViewExAlerts_FocusedRowChanged(null, null);
			}
		}

		private void barButtonItemEnd_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
			if (gridControlExAlerts.SelectedItems.Count > 0)
			{
				AlertNotificationClientData alert = (AlertNotificationClientData)((GridControlDataAlertNotification)gridControlExAlerts.SelectedItems[0]).Tag;
				ProcessAlertForm processAlertForm = new ProcessAlertForm(ProcessAlertForm.ProcessType.ToEnd, alert);
				processAlertForm.ShowDialog();
				gridViewExAlerts_FocusedRowChanged(null, null);
			}
		}

		private void barButtonItemObservations_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
			if (gridControlExAlerts.SelectedItems.Count > 0)
			{
				AlertNotificationClientData alert = (AlertNotificationClientData)((GridControlDataAlertNotification)gridControlExAlerts.SelectedItems[0]).Tag;
				ProcessAlertForm processAlertForm = new ProcessAlertForm(ProcessAlertForm.ProcessType.Observation, alert);
				processAlertForm.ShowDialog();
			}
		}

		void gridViewExAlerts_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
		{
			if (gridControlExAlerts.SelectedItems.Count > 0)
			{
				AlertNotificationClientData alert = (gridControlExAlerts.SelectedItems[0] as GridControlDataAlertNotification).Tag as AlertNotificationClientData;
				if (alert.AttendingOperator == null || alert.AttendingOperator.Equals(ServerServiceClient.GetInstance().OperatorClient))
				{
					switch (alert.Status)
					{
						case AlertNotificationClientData.AlertStatus.NotTaken:
							barButtonItemProcessing.Enabled = true;
							barButtonItemObservations.Enabled = false;
							barButtonItemEnd.Enabled = false;
							break;
						case AlertNotificationClientData.AlertStatus.InProcess:
							barButtonItemProcessing.Enabled = false;
							barButtonItemObservations.Enabled = true;
							barButtonItemEnd.Enabled = true;
							break;
						case AlertNotificationClientData.AlertStatus.Ended:
							barButtonItemProcessing.Enabled = false;
							barButtonItemObservations.Enabled = false;
							barButtonItemEnd.Enabled = false;
							break;
						default:
							break;
					}
				}
				else
				{
					barButtonItemProcessing.Enabled = false;
					barButtonItemObservations.Enabled = false;
					barButtonItemEnd.Enabled = false;
				}
				UnitClientData unit = new UnitClientData();
				unit.Code = alert.UnitAlert.UnitCode;
				int i = gridControlExUnits.Items.IndexOf(new GridControlDataUnitInfo(unit));
				gridViewExUnits.FocusedRowHandle = gridViewExUnits.GetRowHandle(i);
				UpdateObservations();
			}
		}

		void gridViewExUnits_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
		{
			if (gridControlExUnits.SelectedItems.Count > 10)
			{
				MessageForm.Show(ResourceLoader.GetString2("MoreThan10UnitSelected"), MessageFormType.Information);
				int i = 0;
				while ((i = gridControlExUnits.SelectedItems.Count-1 ) >= 10)
				{
					GridControlDataUnitInfo item = gridControlExUnits.SelectedItems[i] as GridControlDataUnitInfo;
					int rowHandle = gridViewExUnits.GetRowHandle(gridControlExUnits.Items.IndexOf(item));
					gridViewExUnits.SelectionChanged -= new DevExpress.Data.SelectionChangedEventHandler(gridViewExUnits_SelectionChanged);
					gridViewExUnits.UnselectRow(rowHandle);
					gridViewExUnits.SelectionChanged +=new DevExpress.Data.SelectionChangedEventHandler(gridViewExUnits_SelectionChanged);
				}
			}			
		}

		void gridViewExUnits_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
		{
			if (gridControlExUnits.SelectedItems.Count > 0 && gridControlExUnits.SelectedItems.Count <= 10 && Tracking == false)
				SelectedPosition = ((GridControlDataUnitInfo)gridControlExUnits.SelectedItems[0]).Unit;
			else if (gridControlExUnits.SelectedItems.Count == 0 && Tracking == false)
				LoadRightPanelLanguage();
			else 
				barButtonItemFollow.Down = false;					
		}

		private void barButtonItemFollow_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
			following = barButtonItemFollow.Down;
		}

		private void barButtonItemSetPosition_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
			if (selectedPosition != null)
				mapControlEx1.CenterMapInPoint(new GeoPoint(selectedPosition.Lon, selectedPosition.Lat)); 
		}

		private void barButtonItemCommands_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
			if (gridControlExUnits.SelectedItems.Count > 0)
			{
				UnitClientData unit = (UnitClientData)((GridControlDataUnitInfo)gridControlExUnits.SelectedItems[0]).Tag;
				CommandsForm commandsForm = new CommandsForm(unit);

				commandsForm.Show();
			}
		}
	}
}
