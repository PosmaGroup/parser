﻿using Smartmatic.SmartCad.Vms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using SmartCadControls.Controls;

namespace SmartCadGuiCommon.Classes
{
    public interface IPlayBackPanel
    {
        event VmsControlEx.DateTimeValueChangedEventHandler DateTimeValueChanged;

        double PlaySpeed { set; }
        int Volume { set; }
        bool Mute { set; }
        int PlayDirection { set; }
        DateTime StartDate { set; }
        DateTime EndDate { set; }
        string Name { get; set; }

        void DisposePanelControl();

        void SetPlayMode(PlayModes playMode);
        void StopPlayBack();
        void StartPlayBack();
        void ExportVideo(DateTime startDate, DateTime endDate, string path, int format, int timestamp);
        void CameraPanelControlTemplates();
        void ClearControlPanel();
        void ClearVMSControl();
        void ClearVMSControlPlayback();
        void InitStopPlayback();
    }
}
