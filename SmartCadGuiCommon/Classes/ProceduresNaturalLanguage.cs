using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

using Smartmatic.SmartCad.Service;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using Net.Sgoliver.NRtfTree.SmartCadGuiCommon.Util;

namespace SmartCadGuiCommon.Classes
{
    public class ProceduresNaturalLanguage
    {
        private static void AddText(RichTextBox rtbTemp, RtfTree rtf, string text, Font font)
        {
            rtbTemp.Font = font;
            rtbTemp.Clear();
            RtfTree rtfTemp = new RtfTree();

            rtbTemp.Text = text;

            rtfTemp.LoadRtfText(rtbTemp.Rtf);

            AppendRtf(rtf, rtfTemp);
        }

        private static void AddRtf(RichTextBox rtbTemp, RtfTree rtf, string text)
        {
            rtbTemp.Clear();
            RtfTree rtfTemp = new RtfTree();

            rtbTemp.Rtf = text;

            rtfTemp.LoadRtfText(rtbTemp.Rtf);

            AppendRtf(rtf, rtfTemp);
        }

        private static void AppendRtf(RtfTree rtf1, RtfTree rtf2)
        {
            foreach (RtfTreeNode n in rtf2.RootNode.FirstChild.ChildNodes)
            {
                rtf1.RootNode.AppendChild(n);
            }
        }

        public static void Write(IList incidentTypes, RichTextBox richTextBox)
        {
            Font font = richTextBox.Font;
            Font boldFont = new Font(richTextBox.Font, FontStyle.Bold);

            RichTextBox rtbTemp = new RichTextBox();
            RtfTree rtf1 = new RtfTree();

            richTextBox.Clear();

            rtbTemp.Font = font;
            
            foreach (IncidentTypeClientData incidentType in incidentTypes)
            {
                AddText(rtbTemp, rtf1, ResourceLoader.GetString2("IncidentType")+":", boldFont);

                AddText(rtbTemp, rtf1, incidentType.Name, font);

                if (incidentType.Procedure != null)
                {
                    AddText(rtbTemp, rtf1, ResourceLoader.GetString2("Procedure")+":", boldFont);

                    try
                    {
                        AddRtf(rtbTemp, rtf1, incidentType.Procedure);
                    }
                    catch
                    {
                    }
                }
            }
            if (!string.IsNullOrEmpty(rtf1.Rtf))
                richTextBox.Rtf = "{" + rtf1.Rtf + "}";
            else
                richTextBox.Rtf = "";

            richTextBox.Font = font;
            boldFont.Dispose();
        }
    }
}
