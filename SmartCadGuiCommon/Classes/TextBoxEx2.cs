﻿using SmartCadControls.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SmartCadGuiCommon.Classes
{
    public class TextBoxEx2 : TextBoxEx
    {
        const int WM_MOUSEACTIVATE = 0x021;

        public TextBoxEx2()
            : base()
        { }

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == WM_MOUSEACTIVATE)
            {
                m.Result = (IntPtr)0x4;
            }
            else
            {
                base.WndProc(ref m);
            }
        }
    }
}
