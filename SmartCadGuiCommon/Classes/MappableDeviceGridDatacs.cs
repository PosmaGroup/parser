﻿using SmartCadControls;
using SmartCadControls.Controls;
using SmartCadCore.ClientData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadGuiCommon.Classes
{
    public class MappableDeviceGridData : GridControlData
    {
        public MappableDeviceGridData(VideoDeviceClientData device)
            : base(device)
        {
        }

        public VideoDeviceClientData Data
        {
            get
            {
                return (VideoDeviceClientData)Tag;
            }
        }

        [GridControlRowInfoData(Visible = true)]
        public string Name
        {
            get
            {
                return Data.Name;
            }
        }

        [GridControlRowInfoData(Visible = true)]
        public string DeviceType
        {
            get
            {
                if (Data is DataloggerClientData)
                {
                    return "Datalogger";
                }
                else if (Data is LprClientData)
                {
                    return "LPR";
                }
                else if (Data is CameraClientData)
                {
                    return "Camera";
                }
                else
                {
                    return "Unrecognized";
                }
            }
        }

        public override bool Equals(object obj)
        {
            if (obj is MappableDeviceGridData)
            {
                if ((obj as MappableDeviceGridData).Data.Code == this.Data.Code)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
