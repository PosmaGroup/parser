using System;
using System.Collections.Generic;
using System.Text;

using System.Collections;
using Smartmatic.SmartCad.Service;
using SmartCadControls.Filters;
using SmartCadCore.ClientData;
using SmartCadControls;
using SmartCadCore.Core;

namespace SmartCadGuiCommon.Classes
{
    public class IncidentsFilter : FilterBase
    {
        private Dictionary<string, ApplicationPreferenceClientData> globalApplicationPreferences;

        public IncidentsFilter(DataGridEx dataGrid, IList parameters)
            : base(dataGrid, parameters)
        { }

		public override bool Filter(object objectData)
        {
            bool result = false;
            IncidentClientData incident = (IncidentClientData)objectData;
            string filterToApply = parameters[0].ToString();
            #region Filtros de Supervision
            if (filterToApply.Equals("ALL") == true)
            {
                result = true;
            }
            else if (filterToApply.Equals("OPEN") == true)
            {
                if (incident.Status != null && incident.Status.CustomCode != IncidentStatusClientData.Closed.CustomCode)
                {
                    result = true;
                }
            }
            else if (filterToApply.Equals("CLOSED") == true)
            {
                if (incident.Status != null && incident.Status.CustomCode == IncidentStatusClientData.Closed.CustomCode)
                {
                    result = true;
                }
            }
            #endregion
            else if (filterToApply.Equals("OPEN_INCIDENTS") == true)
            {
                if (incident.Status!= null && incident.Status.CustomCode != IncidentStatusClientData.Closed.CustomCode)
                {
                    result = true;
                }
            }
            else if (filterToApply.Equals("SAME_NUMBER_INCIDENTS") == true)
            {
                if (parameters[1].ToString().Trim() != "" && incident.Status.CustomCode != IncidentStatusClientData.Closed.CustomCode)
                {
                    int index = 0;
                    bool found = false;
                    while (found != true && index < incident.PhoneReports.Count)
                    {
                        if (incident.PhoneReports[index] is PhoneReportClientData)
                        {
                            PhoneReportClientData phoneReport = (PhoneReportClientData)incident.PhoneReports[index];
                            if (phoneReport.PhoneReportLineClient.Telephone.Equals(parameters[1].ToString()) == true)
                            {
                                found = true;
                            }
                        }
                        index = index + 1;
                    }
                    if (found == true)
                        result = true;
                }
            }
            else if(filterToApply.Equals("SIMILAR_INCIDENTS") == true)
            {
                this.globalApplicationPreferences = parameters[5] as Dictionary<string, ApplicationPreferenceClientData>;
                if (incident.Status.CustomCode != IncidentStatusClientData.Closed.CustomCode)
                {
                    if (AreSimilar(incident, parameters) == true)
                        result = true;
                }
            }
            return result;
        }

        public override bool FilterByText(object objectData, string text)
        {
            bool result = false;
            
            IncidentClientData incident = objectData as IncidentClientData;

            if (incident.SourceIncidentApp == SourceIncidentAppEnum.Cctv)
            {
                if (incident.Address.State!=null && incident.Address.State.ToLower().Contains(text.ToLower()) == true)
                    result = true;
                else if (incident.Address.Town != null && incident.Address.Town.ToLower().Contains(text.ToLower()) == true)
                    result = true;
                else if (incident.Address.Zone != null && incident.Address.Zone.ToLower().Contains(text.ToLower()) == true)
                    result = true;
                else if (incident.Address.Street != null && incident.Address.Street.ToLower().Contains(text.ToLower()) == true)
                    result = true;
                else
                {
                    //IList resultList = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(
                    //    SmartCadHqls.GetCamerasByCustomId,
                    //    (incident.CctvReports[0] as CctvReportClientData).CctvReportCameraClient.Name));
                    //if ((resultList[0] as CameraClientData).Name.Contains(text) == true)
                    //    result = true;
                    //else if ((resultList[0] as CameraClientData).StructClientData.Name.ToLower().Contains(text.ToLower()) == true)
                    //    result = true;

                    CameraClientData camera = (incident.CctvReports[0] as CctvReportClientData).Camera;
                    if (camera.Name.Contains(text) == true)
                        result = true;
                    else if (camera.StructClientData.Name.ToLower().Contains(text.ToLower()) == true)
                        result = true;
                }
               
            }
            else 
            {
                text = text.ToLower();
                if (incident.Address.ToString().ToLower().Contains(text) == true)
                    result = true;
                else if (incident.Status.FriendlyName.ToLower().Contains(text) == true)
                    result = true;
                else if (incident.StartDate.ToString().ToLower().Contains(text) == true)
                    result = true;
                else
                {
                    int index = 0;
                    while (index < incident.IncidentTypes.Count && result != true)
                    {
                        IncidentTypeClientData incidentType = (IncidentTypeClientData)incident.IncidentTypes[index];
                        if ((incidentType.FriendlyName.ToLower().Contains(text) == true) ||
                            (incidentType.CustomCode.ToLower().Contains(text) == true))
                            result = true;
                        index = index + 1;
                    }
                }
                if (result == false)//now comparing answers...
                {
                    if (parameters.Count > 0 && parameters[0] is Hashtable)
					{
						Hashtable hash = (Hashtable)parameters[0];
                        if (hash.ContainsKey(incident.Code) == true)
                        {
                            long count = (long)hash[incident.Code];
                            result = count > 0;
                        }
                    }
                }
            }
            return result;
        }

        private ApplicationPreferenceClientData SearchPreference(string preferenceName)
        {
            return this.globalApplicationPreferences[preferenceName];
        }

        private bool AreSimilar(IncidentClientData bdIncident, IList parameters)
        {
            bool result = false;
            bool sameTelephoneNumber = false;
            bool sameIncidentType = false;
            //bool isInRatio = false;
         

            int telephoneNumberWeightPreference = int.Parse(SearchPreference("TelephoneNumberWeight").Value);
            int incidentTypesWeightPreference = int.Parse(SearchPreference("IncidentTypeWeight").Value);
            int similarIncidentTypesCount = 0;
            int incidentAddressWeightPreference = int.Parse(SearchPreference("IncidentAddressWeight").Value);
            int bottomSimilarIncidentValuePreference = int.Parse(SearchPreference("BottomSimilarIncidentValue").Value);
            int inRatioSimilarIncidentValue = int.Parse(SearchPreference("InRatioSimilarIncidentValue").Value);
            float acum = 0.0f;

            string newPhoneReportLineNumber = (string)parameters[1];
            IList newPhoneReportIncidentTypeCodes = (IList)parameters[2];
            AddressClientData newPhoneReportAddress = (AddressClientData)parameters[3];
            IList inRatioIncidentCodes = (IList)parameters[4];
            IList selectedIncidentTypes = (IList)parameters[6];
            bool isEmergency = false;
            if (selectedIncidentTypes != null && selectedIncidentTypes.Count > 0) { 
                isEmergency = !((IncidentTypeClientData)selectedIncidentTypes[0]).NoEmergency;              
            }

            if (newPhoneReportIncidentTypeCodes != null)
            {
                foreach (int incidentTypeCode in newPhoneReportIncidentTypeCodes)
                {
                    
                    foreach (IncidentTypeClientData incidentType in bdIncident.IncidentTypes)
                    {
                        if (incidentType.Code == incidentTypeCode)
                        {
                            similarIncidentTypesCount = similarIncidentTypesCount + 1;
                            sameIncidentType = true;
                            break;
                        }
                    }
                }
            }

            foreach (PhoneReportClientData prcd in bdIncident.PhoneReports)
            {
                if (prcd.PhoneReportLineClient.Telephone.Equals(newPhoneReportLineNumber) == true)
                {
                    sameTelephoneNumber = true;
                    break;
                }
            }

            /*if (isSpatialwareActive == true)
            {*/
                if (inRatioIncidentCodes.Contains(bdIncident.Code.ToString()) == true && newPhoneReportAddress.IsSynchronized == true)
                    acum = acum + (1 * (float)inRatioSimilarIncidentValue);
            /*}
            else
            {
                acum = acum + (1 * (float)inRatioSimilarIncidentValue);
            }*/

             if (sameTelephoneNumber == true)
                acum = acum + (1 * telephoneNumberWeightPreference);
            
            if (sameIncidentType == true)
            {
                acum = acum + (((float)similarIncidentTypesCount /
                    ((float)bdIncident.IncidentTypes.Count + (float)newPhoneReportIncidentTypeCodes.Count - (float)similarIncidentTypesCount)) *
                    (float)incidentTypesWeightPreference);
            }

            if (newPhoneReportAddress != null && isEmergency)
                acum = acum + (CompareAddress(newPhoneReportAddress, bdIncident.Address) * (float)incidentAddressWeightPreference);

            if (acum >= (float)bottomSimilarIncidentValuePreference)
                result = true;

            return result;
        }

        private float CompareAddress(AddressClientData add1, AddressClientData add2)
        {
            float coincidences = 0.0f;
            if (add1.Zone != null && add2.Zone != null && add1.Zone.Trim() != string.Empty && add2.Zone.Trim() != string.Empty &&
                ApplicationUtil.GetStringWithoutAccent(add1.Zone).ToLower().Trim().Equals(ApplicationUtil.GetStringWithoutAccent(add2.Zone).ToLower().Trim()) == true)
            {
                coincidences = coincidences + 1.0f ;
            }
            if (add1.Street != null && add2.Street != null && add1.Street.Trim() != string.Empty && add2.Street.Trim() != string.Empty &&
                ApplicationUtil.GetStringWithoutAccent(add1.Street).ToLower().Trim().Equals(ApplicationUtil.GetStringWithoutAccent(add2.Street).ToLower().Trim()) == true)
            {
                coincidences = coincidences + 1.0f;
            }
            if (add1.Reference != null && add2.Reference != null && add1.Reference.Trim() != string.Empty && add2.Reference.Trim() != string.Empty &&
               ApplicationUtil.GetStringWithoutAccent(add1.Reference).ToLower().Trim().Equals(ApplicationUtil.GetStringWithoutAccent(add2.Reference).ToLower().Trim()) == true)
            {
                coincidences = coincidences + 1.0f;
            }
            if (add1.More != null && add2.More != null && add1.More.Trim() != string.Empty && add2.More.Trim() != string.Empty &&
                ApplicationUtil.GetStringWithoutAccent(add1.More).ToLower().Trim().Equals(ApplicationUtil.GetStringWithoutAccent(add2.More).ToLower().Trim()) == true)
            {
                coincidences = coincidences + 1.0f;
            }
            return coincidences / 4.0f;
        }
    }
}
