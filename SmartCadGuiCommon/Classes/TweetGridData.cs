﻿using SmartCadControls;
using SmartCadCore.ClientData;
using SmartCadFirstLevel.Gui;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SmartCadGuiCommon.Classes
{
    public class TweetGridData
    {
        private PictureBox pictureBox = new PictureBox();
        public TweetClientData tweet;

        public TweetGridData(TweetClientData tweet)
        {
            this.tweet = tweet;
            this.ImageUrl = tweet.ImageUrl;
        }

        public Image ProfileImage { get; set; }

        private string imageUrl = "";
        public string ImageUrl
        {
            set
            {
                try
                {
                    imageUrl = value;
                    var request = WebRequest.Create(value);

                    using (var response = request.GetResponse())
                    using (var stream = response.GetResponseStream())
                    {
                        ProfileImage = Image.FromStream(stream);
                    }
                }
                catch { }
            }
            get
            {
                return imageUrl;
            }
        }

        [GridControlRowInfoData(Visible = true, Searchable = true)]
        public string Since
        {
            get
            {
                TimeSpan since = DateTime.Now.Subtract(Published);
                if (since.Days > 0) return since.Days + "d ago";
                else if (since.Hours > 0) return since.Hours + "h ago";
                else if (since.Minutes > 0) return since.Minutes + "m ago";
                else if (since.Seconds > 0) return since.Seconds + "s ago";
                else return "now";
            }
        }

        [GridControlRowInfoData(Visible = true, Searchable = true)]
        public string Content { get { return tweet.Content; } }

        [GridControlRowInfoData(Visible = true, Searchable = true)]
        public Author Author { get { return new Author() { Name = tweet.AuthorName, Uri = tweet.AuthorUri, UserName = tweet.AuthorUserName }; } }

        [GridControlRowInfoData(Visible = false)]
        public string Id { get { return tweet.CustomCode; } }

        [GridControlRowInfoData(Visible = false)]
        public DateTime Published { get { return tweet.Published; } }

        [GridControlRowInfoData(Visible = false)]
        public string Link { get { return tweet.Link; } }

    }
}
