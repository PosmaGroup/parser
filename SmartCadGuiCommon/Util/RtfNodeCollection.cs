/********************************************************************************
 *   This file is part of NRtfTree.
 *
 *   NRtfTree is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NRtfTree is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NRtfTree; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 ********************************************************************************/	

/********************************************************************************
 * Library:		NRtfTree
 * Version:     v0.2.1
 * Class:		ImageNode
 * Copyright:   2005 Salvador Gomez
 * Home Page:	http://www.sgoliver.net
 * SF Project:	http://nrtftree.sourceforge.net
 *				http://sourceforge.net/projects/nrtftree
 * Date:		10/12/2006
 * Description:	Coleccion de nodos de un arbol RTF.
 * ******************************************************************************/

using System;
using System.Collections;

namespace Net.Sgoliver.NRtfTree
{
    namespace SmartCadGuiCommon.Util
    {
        /// <summary>
        /// Coleccion de nodos de un documento RTF.
        /// </summary>
        public class RtfNodeCollection : CollectionBase
        {
            #region Metodos Publicos

            /// <summary>
            /// Anade un nuevo nodo a la coleccion actual.
            /// </summary>
            /// <param name="node">Nuevo nodo a anadir.</param>
            /// <returns>Posicion en la que se ha insertado el nuevo nodo.</returns>
            public int Add(RtfTreeNode node)
            {
                InnerList.Add(node);

                return (InnerList.Count - 1);
            }

            /// <summary>
            /// Indizador de la clase RtfNodeCollection. 
            /// Devuelve el nodo que ocupa la posicion 'index' dentro de la coleccion.
            /// </summary>
            public RtfTreeNode this[int index]
            {
                get
                {
                    return (RtfTreeNode)InnerList[index];
                }
                set
                {
                    InnerList[index] = value;
                }
            }

            /// <summary>
            /// Devuelve el indice del nodo pasado como parametro dentro de la lista de nodos de la coleccion.
            /// </summary>
            /// <param name="node">Nodo a buscar en la coleccion.</param>
            /// <returns>Indice del nodo buscado. Devolvera el valor -1 en caso de no encontrarse el nodo dentro de la coleccion.</returns>
            public int IndexOf(RtfTreeNode node)
            {
                return InnerList.IndexOf(node);
            }

            /// <summary>
            /// Anade al final de la coleccion una nueva lista de nodos.
            /// </summary>
            /// <param name="collection">Nueva lista de nodos a anadir a la coleccion actual.</param>
            public void AddRange(RtfNodeCollection collection)
            {
                InnerList.AddRange(collection);
            }

            #endregion
        }
    }
}
