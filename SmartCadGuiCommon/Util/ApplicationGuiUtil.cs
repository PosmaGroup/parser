﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.ServiceModel;
using System.Net.Sockets;
using System.Threading;
using SmartCadCore.Core;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using Smartmatic.SmartCad.Service;
using System.Windows;
using SmartCadGuiCommon;
using SmartCadGuiCommon.CCTV;

namespace SmartCadGuiCommon.Util
{
    public static class ApplicationGuiUtil
    {
        public const int GLOBAL_TIME_PERIOD = 10000;
        private static Timer CheckServerTimer = null;


        public static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {

            SmartCadGuiCommon.CctvFrontClientFormDevX Obj = new SmartCadGuiCommon.CctvFrontClientFormDevX();
            bool isCCTVPlus = Obj.ValueViewAlarm();
            if (e.Exception.GetType() == typeof(CommunicationObjectFaultedException) ||
                e.Exception.GetType() == typeof(EndpointNotFoundException) ||
                e.Exception.GetType() == typeof(SocketException))
            {
                CheckServerTimer.Dispose();
                ServerServiceClient.GetInstance().CloseSession(false);
                MessageForm.Show(ResourceLoader.GetString2("CommunicationObjectFaultedExceptionMessage"), e.Exception);
                Process.GetCurrentProcess().Kill();
            }
            else
            {
                //if (isCCTVPlus != false)// Si y solo si es la aplicacion CCTV+ 
                //{    
                    if (e.Exception.Message == "Object reference not set to an instance of an object.")
                    {
                        MessageBox.Show("You have not correctly configured the cameras. Contact the Administrator.");
                    }
                    else
                    {
                        if (e.Exception.Message == "Referencia a objeto no establecida como instancia de un objeto.")
                        {
                            MessageBox.Show("No ha configurado correctamente las cámaras. Contacte al administrador.");
                        }
                        else
                        {
                            MessageForm.Show(e.Exception.Message, e.Exception);
                        }
                    }
                //}
            }
        }
        public static Timer StartCheckServer()
        {
            CheckServerTimer =  ApplicationUtil.RecurrentExecuteMethod(
                new SimpleDelegate(delegate
                {
                    bool successfull = false;
                    int serverTries = 0;
                    while (successfull == false)
                    {
                        try
                        {
                            ServerServiceClient.GetInstance().Ping();
                            successfull = true;
                        }
                        catch
                        {
                            serverTries++;
                            if (serverTries >= 3)
                                throw;
                            Thread.Sleep(300);
                        }
                    }

                    try
                    {
                        if (TelephonyServiceClient.Instance != null && !TelephonyServiceClient.GetInstance().InVirtualCall())
                        {
                            TelephonyServiceClient.GetInstance().Ping();
                        }
                    }
                    catch (Exception ex)
                    {
                        SmartLogger.Print(ex);
                    }
                }),
                new SimpleDelegate(delegate
                {
                    CheckServerTimer.Change(Timeout.Infinite, Timeout.Infinite);
                    CheckServerTimer.Dispose();
                    ServerServiceClient.GetInstance().CloseSession(false);
                    MessageForm.Show(ResourceLoader.GetString2("CommunicationObjectFaultedExceptionMessage"), MessageFormType.Error);
                    Process.GetCurrentProcess().Kill();
                }),
                null, 15000, 15000);
            return CheckServerTimer;
        }

        public static void CheckErrorDetailButton()
        {
            bool checkErrorDetail = false;
            try
            {
                ApplicationPreferenceClientData preference = ServerServiceClient.GetInstance().SearchClientObject(
                    SmartCadHqls.GetCustomHql(SmartCadHqls.GetApplicationPreferenceByName, "ShowErrorDetails"))
                    as ApplicationPreferenceClientData;
                checkErrorDetail = Convert.ToBoolean(preference.Value);
                MessageForm.CheckPreference = checkErrorDetail;
            }
            catch (Exception ex)
            {
                MessageForm.CheckPreference = false;
            }
        }
    }
}
