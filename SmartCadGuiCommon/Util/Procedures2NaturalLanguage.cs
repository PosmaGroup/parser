using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

using Smartmatic.SmartCad.Service;
using SmartCadCore.ClientData;
using Net.Sgoliver.NRtfTree.SmartCadGuiCommon.Util;
using SmartCadCore.Common;

namespace SmartCadGuiCommon.Util
{
    public class Procedures2NaturalLanguage
    {
        private static void AddText(RichTextBox rtbTemp, RtfTree rtf, string text, Font font)
        {
            rtbTemp.Font = font;
            rtbTemp.Clear();
            RtfTree rtfTemp = new RtfTree();

            rtbTemp.Text = text;

            rtfTemp.LoadRtfText(rtbTemp.Rtf);

            AppendRtf(rtf, rtfTemp);
        }

        private static void AddRtf(RichTextBox rtbTemp, RtfTree rtf, string text)
        {
            rtbTemp.Clear();
            RtfTree rtfTemp = new RtfTree();

            rtbTemp.Rtf = text;

            rtfTemp.LoadRtfText(rtbTemp.Rtf);

            AppendRtf(rtf, rtfTemp);
        }

        private static void AppendRtf(RtfTree rtf1, RtfTree rtf2)
        {
            foreach (RtfTreeNode n in rtf2.RootNode.FirstChild.ChildNodes)
            {
                rtf1.RootNode.AppendChild(n);
            }
        }

        public static void Write(IList questionsWithAnswers, RichTextBox richTextBox)
        {
            ServerServiceClient serverServiceClient = ServerServiceClient.GetInstance();

            Font font = richTextBox.Font;
            Font boldFont = new Font(richTextBox.Font, FontStyle.Bold);
            Font underlineFont = new Font(richTextBox.Font, FontStyle.Underline);
            RichTextBox rtbTemp = new RichTextBox();
            RtfTree rtf1 = new RtfTree();

            richTextBox.Clear();

            rtbTemp.Font = font;
            
            foreach (ReportAnswerClientData phoneReportAnswer in questionsWithAnswers)
            {
                foreach (PossibleAnswerAnswerClientData possibleAnswerAnswer in phoneReportAnswer.Answers)
                {
                    string proc = possibleAnswerAnswer.QuestionPossibleAnswer.Procedure;
                    proc = proc == null ? null : proc.Trim();
                    if (!string.IsNullOrEmpty(proc))
                    {
                        //QUESTION...
                        try
                        {
                            RichTextBox aux = new RichTextBox();
                            aux.SelectionBullet = true;
                            aux.Font = boldFont;
                            aux.BulletIndent = 25;
                            aux.Text = phoneReportAnswer.QuestionText;

                            AddRtf(rtbTemp, rtf1, aux.Rtf);
                        }
                        catch
                        { }
                        //AddText(rtbTemp, rtf1, ResourceLoader.GetString2("Pregunta: "), boldFont);
                        
                        //AddText(rtbTemp, rtf1, phoneReportAnswer.QuestionText + Environment.NewLine, boldFont);

                        //PROCEDURE BY ANSWER...
                        
                        //AddText(rtbTemp, rtf1, ResourceLoader.GetString2("Respuesta: "), boldFont);

                        //aux.BulletIndent = 10;
                        try
                        {
                            RichTextBox aux = new RichTextBox();
                            aux.Font = font;
                            aux.Text = possibleAnswerAnswer.TextAnswer;
                            aux.SelectionIndent = 25;

                            AddRtf(rtbTemp, rtf1, aux.Rtf);
                        }
                        catch
                        { }


                        //AddText(rtbTemp, rtf1, possibleAnswerAnswer.TextAnswer + Environment.NewLine, font);

                        try
                        {
                            RichTextBox aux = new RichTextBox();
                            aux.Font = underlineFont;

                            aux.Text = ResourceLoader.GetString2("Procedure")+":";

                            AddRtf(rtbTemp, rtf1, aux.Rtf);
                            AddRtf(rtbTemp, rtf1, possibleAnswerAnswer.QuestionPossibleAnswer.Procedure);
                        }
                        catch
                        { }
                        //AddText(rtbTemp, rtf1, Environment.NewLine, font);
                        AddText(rtbTemp, rtf1, string.Empty, font);
                    }
                }
            }
            if (!string.IsNullOrEmpty(rtf1.Rtf))
                richTextBox.Rtf = "{" + rtf1.Rtf + "}";
            else
                richTextBox.Rtf = "";
            richTextBox.Font = font;
            boldFont.Dispose();
        }
    }
}
