using System;
using System.Threading;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Runtime.InteropServices;

using SmartCadCore.Core;
using DevExpress.XtraEditors;
using SmartCadCore.Common;

namespace SmartCadGuiCommon
{
    #region Class SplashForm Documentation
    /// <summary>
    /// TODO:
    /// </summary>
    /// <className>SplashForm</className>
    /// <author email="atorres@smartmatic.com">Alden Torres</author>
    /// <date>2006/12/01</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
	public class SplashForm : XtraForm
	{
		private System.Windows.Forms.PictureBox pictureBox;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label labelVersion;
        private PictureBox pictureBoxAppLoading;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private new System.ComponentModel.Container components = null;

		public SplashForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
            LoadLanguage();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}


		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SplashForm));
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBoxAppLoading = new System.Windows.Forms.PictureBox();
            this.labelVersion = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAppLoading)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox
            // 
            this.pictureBox.Location = new System.Drawing.Point(0, 0);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(553, 226);
            this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox.TabIndex = 14;
            this.pictureBox.TabStop = false;
            this.pictureBox.SizeChanged += new System.EventHandler(this.pictureBox_SizeChanged);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.pictureBoxAppLoading);
            this.panel1.Controls.Add(this.labelVersion);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 219);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(550, 87);
            this.panel1.TabIndex = 15;
            // 
            // pictureBoxAppLoading
            // 
            this.pictureBoxAppLoading.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxAppLoading.Image")));
            this.pictureBoxAppLoading.Location = new System.Drawing.Point(3, 0);
            this.pictureBoxAppLoading.Name = "pictureBoxAppLoading";
            this.pictureBoxAppLoading.Size = new System.Drawing.Size(547, 19);
            this.pictureBoxAppLoading.TabIndex = 14;
            this.pictureBoxAppLoading.TabStop = false;
            // 
            // labelVersion
            // 
            this.labelVersion.BackColor = System.Drawing.Color.White;
            this.labelVersion.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelVersion.Location = new System.Drawing.Point(0, 27);
            this.labelVersion.Name = "labelVersion";
            this.labelVersion.Size = new System.Drawing.Size(550, 17);
            this.labelVersion.TabIndex = 13;
            this.labelVersion.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label2.Location = new System.Drawing.Point(0, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(550, 17);
            this.label2.TabIndex = 12;
            this.label2.Text = "Copyright 2002 - 2012 Smartmatic Corporation.";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label1.Location = new System.Drawing.Point(0, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(550, 26);
            this.label1.TabIndex = 11;
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // SplashForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.ClientSize = new System.Drawing.Size(550, 306);
            this.ControlBox = false;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pictureBox);
            this.Cursor = System.Windows.Forms.Cursors.AppStarting;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SplashForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.TopMost = true;
            this.Load += new System.EventHandler(this.SplashForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAppLoading)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

        private void LoadLanguage()
        {
            this.label1.Text = ResourceLoader.GetString2("AllRightsReserved");
        }
		private void pictureBox_SizeChanged(object sender, System.EventArgs e)
		{
			Size = new Size(pictureBox.Width, pictureBox.Height + panel1.Height);
		}

		private void SplashForm_Load(object sender, System.EventArgs e)
		{
            LoadInfo();
            this.pictureBox.Image = ResourceLoader.GetImage("$Image.Splash");
		}

        private void LoadInfo()
        {
            labelVersion.Text = SmartCadConfiguration.SoftwareVersion;
            label2.Text = ResourceLoader.GetString("$Message.Copyright");
        }

        private static SplashForm splashForm;
        private static Thread thread;

        private static void ShowForm()
        {
            splashForm = new SplashForm();

            try
            {
                IntPtr handle = Win32.LoadCursorFromFile(SmartCadConfiguration.BusyCursorFilename);
                splashForm.Cursor = new Cursor(handle);
            }
            catch
            {
            }

            Application.Run(splashForm);
        }

        public static void ShowSplash()
        {
            thread = new Thread(new ThreadStart(SplashForm.ShowForm));
            thread.IsBackground = true;
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
        }

        public static void CloseSplash()
        {
            try
            {
                if (splashForm != null && !splashForm.IsDisposed)
                {
                    splashForm.BeginInvoke(new SimpleDelegate(delegate { splashForm.Close(); }));
                }
            }
            catch
            {
            }
        }
	}
}
