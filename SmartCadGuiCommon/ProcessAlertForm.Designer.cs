namespace SmartCadGuiCommon
{
	partial class ProcessAlertForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
			this.simpleButtonCancel = new DevExpress.XtraEditors.SimpleButton();
			this.simpleButtonAccept = new DevExpress.XtraEditors.SimpleButton();
			this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
			this.checkEditFalseAlarm = new DevExpress.XtraEditors.CheckEdit();
			this.labelControlStatus = new DevExpress.XtraEditors.LabelControl();
			this.labelControlAlertStatus = new DevExpress.XtraEditors.LabelControl();
			this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.layoutControlItemLabelAlertStatus = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItemLabelStatus = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItemCheckEditFalseAlarm = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItemObservations = new DevExpress.XtraLayout.LayoutControlItem();
			this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
			this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
			this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
			((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
			this.layoutControl1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.checkEditFalseAlarm.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLabelAlertStatus)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLabelStatus)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCheckEditFalseAlarm)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItemObservations)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
			this.SuspendLayout();
			// 
			// layoutControl1
			// 
			this.layoutControl1.Appearance.DisabledLayoutGroupCaption.ForeColor = System.Drawing.SystemColors.GrayText;
			this.layoutControl1.Appearance.DisabledLayoutGroupCaption.Options.UseForeColor = true;
			this.layoutControl1.Appearance.DisabledLayoutItem.ForeColor = System.Drawing.SystemColors.GrayText;
			this.layoutControl1.Appearance.DisabledLayoutItem.Options.UseForeColor = true;
			this.layoutControl1.Controls.Add(this.simpleButtonCancel);
			this.layoutControl1.Controls.Add(this.simpleButtonAccept);
			this.layoutControl1.Controls.Add(this.memoEdit1);
			this.layoutControl1.Controls.Add(this.checkEditFalseAlarm);
			this.layoutControl1.Controls.Add(this.labelControlStatus);
			this.layoutControl1.Controls.Add(this.labelControlAlertStatus);
			this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.layoutControl1.Location = new System.Drawing.Point(0, 0);
			this.layoutControl1.Name = "layoutControl1";
			this.layoutControl1.Root = this.layoutControlGroup1;
			this.layoutControl1.Size = new System.Drawing.Size(497, 409);
			this.layoutControl1.TabIndex = 0;
			this.layoutControl1.Text = "layoutControl1";
			// 
			// simpleButtonCancel
			// 
			this.simpleButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.simpleButtonCancel.Location = new System.Drawing.Point(385, 381);
			this.simpleButtonCancel.Name = "simpleButtonCancel";
			this.simpleButtonCancel.Size = new System.Drawing.Size(106, 22);
			this.simpleButtonCancel.StyleController = this.layoutControl1;
			this.simpleButtonCancel.TabIndex = 10;
			this.simpleButtonCancel.Text = "simpleButtonCancel";
			// 
			// simpleButtonAccept
			// 
			this.simpleButtonAccept.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.simpleButtonAccept.Enabled = false;
			this.simpleButtonAccept.Location = new System.Drawing.Point(267, 381);
			this.simpleButtonAccept.Name = "simpleButtonAccept";
			this.simpleButtonAccept.Size = new System.Drawing.Size(107, 22);
			this.simpleButtonAccept.StyleController = this.layoutControl1;
			this.simpleButtonAccept.TabIndex = 9;
			this.simpleButtonAccept.Text = "simpleButtonAccept";
			this.simpleButtonAccept.Click += new System.EventHandler(this.simpleButtonAccept_Click);
			// 
			// memoEdit1
			// 
			this.memoEdit1.Location = new System.Drawing.Point(7, 61);
			this.memoEdit1.Name = "memoEdit1";
			this.memoEdit1.Size = new System.Drawing.Size(484, 309);
			this.memoEdit1.StyleController = this.layoutControl1;
			this.memoEdit1.TabIndex = 8;
			this.memoEdit1.TextChanged += new System.EventHandler(this.memoEdit1_TextChanged);
			// 
			// checkEditFalseAlarm
			// 
			this.checkEditFalseAlarm.Location = new System.Drawing.Point(374, 7);
			this.checkEditFalseAlarm.Name = "checkEditFalseAlarm";
			this.checkEditFalseAlarm.Properties.Caption = "checkEditFalseAlarm";
			this.checkEditFalseAlarm.Size = new System.Drawing.Size(117, 19);
			this.checkEditFalseAlarm.StyleController = this.layoutControl1;
			this.checkEditFalseAlarm.TabIndex = 7;
			// 
			// labelControlStatus
			// 
			this.labelControlStatus.Location = new System.Drawing.Point(129, 7);
			this.labelControlStatus.Name = "labelControlStatus";
			this.labelControlStatus.Size = new System.Drawing.Size(88, 13);
			this.labelControlStatus.StyleController = this.layoutControl1;
			this.labelControlStatus.TabIndex = 5;
			this.labelControlStatus.Text = "labelControlStatus";
			// 
			// labelControlAlertStatus
			// 
			this.labelControlAlertStatus.Location = new System.Drawing.Point(7, 7);
			this.labelControlAlertStatus.Name = "labelControlAlertStatus";
			this.labelControlAlertStatus.Size = new System.Drawing.Size(111, 13);
			this.labelControlAlertStatus.StyleController = this.layoutControl1;
			this.labelControlAlertStatus.TabIndex = 4;
			this.labelControlAlertStatus.Text = "labelControlAlertStatus";
			// 
			// layoutControlGroup1
			// 
			this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
			this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemLabelAlertStatus,
            this.layoutControlItemLabelStatus,
            this.layoutControlItemCheckEditFalseAlarm,
            this.layoutControlItemObservations,
            this.emptySpaceItem1,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.emptySpaceItem2});
			this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
			this.layoutControlGroup1.Name = "layoutControlGroup1";
			this.layoutControlGroup1.Size = new System.Drawing.Size(497, 409);
			this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.layoutControlGroup1.Text = "layoutControlGroup1";
			this.layoutControlGroup1.TextVisible = false;
			// 
			// layoutControlItemLabelAlertStatus
			// 
			this.layoutControlItemLabelAlertStatus.Control = this.labelControlAlertStatus;
			this.layoutControlItemLabelAlertStatus.CustomizationFormText = "layoutControlItemLabelAlertStatus";
			this.layoutControlItemLabelAlertStatus.Location = new System.Drawing.Point(0, 0);
			this.layoutControlItemLabelAlertStatus.Name = "layoutControlItemLabelAlertStatus";
			this.layoutControlItemLabelAlertStatus.Size = new System.Drawing.Size(122, 29);
			this.layoutControlItemLabelAlertStatus.Text = "layoutControlItemLabelAlertStatus";
			this.layoutControlItemLabelAlertStatus.TextLocation = DevExpress.Utils.Locations.Left;
			this.layoutControlItemLabelAlertStatus.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItemLabelAlertStatus.TextToControlDistance = 0;
			this.layoutControlItemLabelAlertStatus.TextVisible = false;
			// 
			// layoutControlItemLabelStatus
			// 
			this.layoutControlItemLabelStatus.Control = this.labelControlStatus;
			this.layoutControlItemLabelStatus.CustomizationFormText = "layoutControlItemLabelStatus";
			this.layoutControlItemLabelStatus.Location = new System.Drawing.Point(122, 0);
			this.layoutControlItemLabelStatus.Name = "layoutControlItemLabelStatus";
			this.layoutControlItemLabelStatus.Size = new System.Drawing.Size(99, 29);
			this.layoutControlItemLabelStatus.Text = "layoutControlItemLabelStatus";
			this.layoutControlItemLabelStatus.TextLocation = DevExpress.Utils.Locations.Left;
			this.layoutControlItemLabelStatus.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItemLabelStatus.TextToControlDistance = 0;
			this.layoutControlItemLabelStatus.TextVisible = false;
			// 
			// layoutControlItemCheckEditFalseAlarm
			// 
			this.layoutControlItemCheckEditFalseAlarm.Control = this.checkEditFalseAlarm;
			this.layoutControlItemCheckEditFalseAlarm.CustomizationFormText = "layoutControlItemCheckEditFalseAlarm";
			this.layoutControlItemCheckEditFalseAlarm.Location = new System.Drawing.Point(367, 0);
			this.layoutControlItemCheckEditFalseAlarm.MaxSize = new System.Drawing.Size(128, 29);
			this.layoutControlItemCheckEditFalseAlarm.MinSize = new System.Drawing.Size(128, 29);
			this.layoutControlItemCheckEditFalseAlarm.Name = "layoutControlItemCheckEditFalseAlarm";
			this.layoutControlItemCheckEditFalseAlarm.Size = new System.Drawing.Size(128, 29);
			this.layoutControlItemCheckEditFalseAlarm.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItemCheckEditFalseAlarm.Text = "layoutControlItemCheckEditFalseAlarm";
			this.layoutControlItemCheckEditFalseAlarm.TextLocation = DevExpress.Utils.Locations.Left;
			this.layoutControlItemCheckEditFalseAlarm.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItemCheckEditFalseAlarm.TextToControlDistance = 0;
			this.layoutControlItemCheckEditFalseAlarm.TextVisible = false;
			// 
			// layoutControlItemObservations
			// 
			this.layoutControlItemObservations.Control = this.memoEdit1;
			this.layoutControlItemObservations.CustomizationFormText = "layoutControlItemObservations";
			this.layoutControlItemObservations.Location = new System.Drawing.Point(0, 29);
			this.layoutControlItemObservations.Name = "layoutControlItemObservations";
			this.layoutControlItemObservations.Size = new System.Drawing.Size(495, 345);
			this.layoutControlItemObservations.Text = "layoutControlItemObservations";
			this.layoutControlItemObservations.TextLocation = DevExpress.Utils.Locations.Top;
			this.layoutControlItemObservations.TextSize = new System.Drawing.Size(151, 20);
			// 
			// emptySpaceItem1
			// 
			this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
			this.emptySpaceItem1.Location = new System.Drawing.Point(221, 0);
			this.emptySpaceItem1.Name = "emptySpaceItem1";
			this.emptySpaceItem1.Size = new System.Drawing.Size(146, 29);
			this.emptySpaceItem1.Text = "emptySpaceItem1";
			this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
			// 
			// layoutControlItem1
			// 
			this.layoutControlItem1.Control = this.simpleButtonAccept;
			this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
			this.layoutControlItem1.Location = new System.Drawing.Point(260, 374);
			this.layoutControlItem1.MaxSize = new System.Drawing.Size(118, 33);
			this.layoutControlItem1.MinSize = new System.Drawing.Size(118, 33);
			this.layoutControlItem1.Name = "layoutControlItem1";
			this.layoutControlItem1.Size = new System.Drawing.Size(118, 33);
			this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem1.Text = "layoutControlItem1";
			this.layoutControlItem1.TextLocation = DevExpress.Utils.Locations.Left;
			this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem1.TextToControlDistance = 0;
			this.layoutControlItem1.TextVisible = false;
			// 
			// layoutControlItem2
			// 
			this.layoutControlItem2.Control = this.simpleButtonCancel;
			this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
			this.layoutControlItem2.Location = new System.Drawing.Point(378, 374);
			this.layoutControlItem2.MaxSize = new System.Drawing.Size(117, 33);
			this.layoutControlItem2.MinSize = new System.Drawing.Size(117, 33);
			this.layoutControlItem2.Name = "layoutControlItem2";
			this.layoutControlItem2.Size = new System.Drawing.Size(117, 33);
			this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.layoutControlItem2.Text = "layoutControlItem2";
			this.layoutControlItem2.TextLocation = DevExpress.Utils.Locations.Left;
			this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
			this.layoutControlItem2.TextToControlDistance = 0;
			this.layoutControlItem2.TextVisible = false;
			// 
			// emptySpaceItem2
			// 
			this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
			this.emptySpaceItem2.Location = new System.Drawing.Point(0, 374);
			this.emptySpaceItem2.Name = "emptySpaceItem2";
			this.emptySpaceItem2.Size = new System.Drawing.Size(260, 33);
			this.emptySpaceItem2.Text = "emptySpaceItem2";
			this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
			// 
			// ProcessAlertForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(497, 409);
			this.Controls.Add(this.layoutControl1);
			this.Name = "ProcessAlertForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "ProcessAlertForm";
			this.Load += new System.EventHandler(this.ProcessAlertForm_Load);
			((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
			this.layoutControl1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.checkEditFalseAlarm.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLabelAlertStatus)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLabelStatus)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCheckEditFalseAlarm)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItemObservations)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private DevExpress.XtraLayout.LayoutControl layoutControl1;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
		private DevExpress.XtraEditors.MemoEdit memoEdit1;
		private DevExpress.XtraEditors.CheckEdit checkEditFalseAlarm;
		private DevExpress.XtraEditors.LabelControl labelControlStatus;
		private DevExpress.XtraEditors.LabelControl labelControlAlertStatus;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemLabelAlertStatus;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemLabelStatus;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemCheckEditFalseAlarm;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemObservations;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
		private DevExpress.XtraEditors.SimpleButton simpleButtonCancel;
		private DevExpress.XtraEditors.SimpleButton simpleButtonAccept;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
	}
}