using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Text;
using System.Collections.Generic;
using Smartmatic.SmartCad.Service;
using System.IO;
using System.Drawing.Imaging;
using System.Linq;
using SmartCadCore.ClientData;
using SmartCadCore.Common;

namespace SmartCadGuiCommon
{
    public partial class ReportSupervisionClose : DevExpress.XtraReports.UI.XtraReport
    {
        public ReportSupervisionClose()
        {
            InitializeComponent();
        }

        public ReportSupervisionClose(SupervisorCloseReportClientData report, Image img)
        {
            InitializeComponent();
            LoadLenguage();
            SetReport(report);
            SetImage(img);
        }

        private void LoadLenguage()
        {
            this.Title.Text = ResourceLoader.GetString2("CloseReport");
            this.supervisorNameLabel.Text = ResourceLoader.GetString2("Supervisor") + ":";
            this.supervisorTypeLabel.Text = ResourceLoader.GetString2("Profile") + ":";
            this.supervisorIdLabel.Text = ResourceLoader.GetString2("PersonID") + ":";
            this.messagesLabel.Text = ResourceLoader.GetString2("Observations") + ":";
            this.beginSessionLabel.Text = ResourceLoader.GetString2("StartSession") + ":";
            this.endReportLabel.Text = ResourceLoader.GetString2("ReportEndDateTime") + ":";
            this.indicatorsLabel.Text = ResourceLoader.GetString2("Indicators") + ":";
        }

        public void SetReport(SupervisorCloseReportClientData report)
        {
            this.supervisorName.Text = report.SessionFullName;
            this.supervisorId.Text = report.SupervisorPersonId;
            this.supervisorType.Text = report.Supervisor;

            if (report.DepartamentTypes == null || report.DepartamentTypes.Count == 0)
            {
                supervisorDepartmentTypes.Visible = false;
                supervisorDepartmentTypesLabel.Visible = false;
            }
            else
            {
                this.supervisorDepartmentTypes.Text = string.Join(",", report.DepartamentTypes.Cast<string>().ToArray());
            }
            
            this.beginSession.Text = report.Start.ToString();
            this.endReport.Text = report.End.ToString();
            
            this.messages.Text = "";
            foreach (SupervisorCloseReportMessageClientData msg in report.Messages)
            {
                this.messages.Text += MakeMessage(msg);
            }
        }

        public void SetImage(Image img)
        {
            this.IndicatorsPhoto.Image = img;
        }

        public string MakeMessage(SupervisorCloseReportMessageClientData message)
        {
            string text = "";
            text = message.Time.ToString("MM/dd/yyyy HH:mm") + ": ";
            text += Environment.NewLine;
            text += message.Message.Trim();
            text += Environment.NewLine;
            text += Environment.NewLine;
            return text;
        }

        
    }
}
