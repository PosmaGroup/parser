namespace SmartCadGuiCommon
{
    partial class ReportPersonaFile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.winControlContainer1 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.operatorPhoto = new System.Windows.Forms.PictureBox();
            this.PanelEvaluations = new DevExpress.XtraReports.UI.XRPanel();
            this.operatorEvaluationsTable = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.nameEvaluationLabel = new DevExpress.XtraReports.UI.XRTableCell();
            this.qualificationEvaluationLabel = new DevExpress.XtraReports.UI.XRTableCell();
            this.dateEvaluationLabel = new DevExpress.XtraReports.UI.XRTableCell();
            this.evaluatorLabel = new DevExpress.XtraReports.UI.XRTableCell();
            this.PanelCourses = new DevExpress.XtraReports.UI.XRPanel();
            this.operatorCourseTable = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.courseLabel = new DevExpress.XtraReports.UI.XRTableCell();
            this.DateCourseLabel = new DevExpress.XtraReports.UI.XRTableCell();
            this.PanelObservations = new DevExpress.XtraReports.UI.XRPanel();
            this.operatorObservationsTable = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.typeObservationLabel = new DevExpress.XtraReports.UI.XRTableCell();
            this.observationLabel = new DevExpress.XtraReports.UI.XRTableCell();
            this.DatetimeLabel = new DevExpress.XtraReports.UI.XRTableCell();
            this.supervisorLabel = new DevExpress.XtraReports.UI.XRTableCell();
            this.PanelCategory = new DevExpress.XtraReports.UI.XRPanel();
            this.operatorCategoryTable = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.categoryTableLabel = new DevExpress.XtraReports.UI.XRTableCell();
            this.dateTableLabel = new DevExpress.XtraReports.UI.XRTableCell();
            this.evaluationsLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.coursesLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.observationsLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.operatorPhone = new DevExpress.XtraReports.UI.XRLabel();
            this.phoneLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.categoryLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.operatorDepartamets = new DevExpress.XtraReports.UI.XRTableCell();
            this.departamentsLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.operatorRole = new DevExpress.XtraReports.UI.XRLabel();
            this.roleLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.operatorEmail = new DevExpress.XtraReports.UI.XRLabel();
            this.emailLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.operatorAddress = new DevExpress.XtraReports.UI.XRLabel();
            this.addressLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.operatorDateBirth = new DevExpress.XtraReports.UI.XRLabel();
            this.dateBirthLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.operatorID = new DevExpress.XtraReports.UI.XRLabel();
            this.IdLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.operatorLastName = new DevExpress.XtraReports.UI.XRLabel();
            this.lastNameLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.OperatorName = new DevExpress.XtraReports.UI.XRLabel();
            this.nameLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrControlStyle1 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.operatorPhoto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.operatorEvaluationsTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.operatorCourseTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.operatorObservationsTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.operatorCategoryTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.winControlContainer1,
            this.PanelEvaluations,
            this.PanelCourses,
            this.PanelObservations,
            this.PanelCategory,
            this.evaluationsLabel,
            this.coursesLabel,
            this.observationsLabel,
            this.operatorPhone,
            this.phoneLabel,
            this.categoryLabel,
            this.xrTable1,
            this.departamentsLabel,
            this.operatorRole,
            this.roleLabel,
            this.operatorEmail,
            this.emailLabel,
            this.operatorAddress,
            this.addressLabel,
            this.operatorDateBirth,
            this.dateBirthLabel,
            this.operatorID,
            this.IdLabel,
            this.operatorLastName,
            this.lastNameLabel,
            this.OperatorName,
            this.nameLabel});
            this.Detail.HeightF = 978F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // winControlContainer1
            // 
            this.winControlContainer1.LocationFloat = new DevExpress.Utils.PointFloat(492F, 33F);
            this.winControlContainer1.Name = "winControlContainer1";
            this.winControlContainer1.SizeF = new System.Drawing.SizeF(100F, 131F);
            this.winControlContainer1.WinControl = this.operatorPhoto;
            // 
            // operatorPhoto
            // 
            this.operatorPhoto.Location = new System.Drawing.Point(0, 0);
            this.operatorPhoto.Name = "operatorPhoto";
            this.operatorPhoto.Size = new System.Drawing.Size(96, 126);
            this.operatorPhoto.TabIndex = 0;
            this.operatorPhoto.TabStop = false;
            // 
            // PanelEvaluations
            // 
            this.PanelEvaluations.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.operatorEvaluationsTable});
            this.PanelEvaluations.LocationFloat = new DevExpress.Utils.PointFloat(200F, 758F);
            this.PanelEvaluations.Name = "PanelEvaluations";
            this.PanelEvaluations.SizeF = new System.Drawing.SizeF(420F, 25F);
            // 
            // operatorEvaluationsTable
            // 
            this.operatorEvaluationsTable.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.operatorEvaluationsTable.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.operatorEvaluationsTable.Name = "operatorEvaluationsTable";
            this.operatorEvaluationsTable.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5});
            this.operatorEvaluationsTable.SizeF = new System.Drawing.SizeF(420F, 25F);
            this.operatorEvaluationsTable.StylePriority.UseBorders = false;
            this.operatorEvaluationsTable.StylePriority.UseTextAlignment = false;
            this.operatorEvaluationsTable.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.nameEvaluationLabel,
            this.qualificationEvaluationLabel,
            this.dateEvaluationLabel,
            this.evaluatorLabel});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 1D;
            // 
            // nameEvaluationLabel
            // 
            this.nameEvaluationLabel.Name = "nameEvaluationLabel";
            this.nameEvaluationLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.nameEvaluationLabel.Text = "Nombre";
            this.nameEvaluationLabel.Weight = 0.25D;
            // 
            // qualificationEvaluationLabel
            // 
            this.qualificationEvaluationLabel.Name = "qualificationEvaluationLabel";
            this.qualificationEvaluationLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.qualificationEvaluationLabel.Text = "Calificacion";
            this.qualificationEvaluationLabel.Weight = 0.25D;
            // 
            // dateEvaluationLabel
            // 
            this.dateEvaluationLabel.Name = "dateEvaluationLabel";
            this.dateEvaluationLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.dateEvaluationLabel.Text = "Fecha";
            this.dateEvaluationLabel.Weight = 0.25D;
            // 
            // evaluatorLabel
            // 
            this.evaluatorLabel.Name = "evaluatorLabel";
            this.evaluatorLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.evaluatorLabel.Text = "Evaluador";
            this.evaluatorLabel.Weight = 0.25D;
            // 
            // PanelCourses
            // 
            this.PanelCourses.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.operatorCourseTable});
            this.PanelCourses.LocationFloat = new DevExpress.Utils.PointFloat(200F, 717F);
            this.PanelCourses.Name = "PanelCourses";
            this.PanelCourses.SizeF = new System.Drawing.SizeF(420F, 25F);
            // 
            // operatorCourseTable
            // 
            this.operatorCourseTable.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.operatorCourseTable.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.operatorCourseTable.Name = "operatorCourseTable";
            this.operatorCourseTable.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.operatorCourseTable.SizeF = new System.Drawing.SizeF(420F, 25F);
            this.operatorCourseTable.StylePriority.UseBorders = false;
            this.operatorCourseTable.StylePriority.UseTextAlignment = false;
            this.operatorCourseTable.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.courseLabel,
            this.DateCourseLabel});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1D;
            // 
            // courseLabel
            // 
            this.courseLabel.Name = "courseLabel";
            this.courseLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.courseLabel.Text = "Curso";
            this.courseLabel.Weight = 0.65476190476190477D;
            // 
            // DateCourseLabel
            // 
            this.DateCourseLabel.Name = "DateCourseLabel";
            this.DateCourseLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DateCourseLabel.Text = "Fecha";
            this.DateCourseLabel.Weight = 0.34523809523809523D;
            // 
            // PanelObservations
            // 
            this.PanelObservations.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.operatorObservationsTable});
            this.PanelObservations.LocationFloat = new DevExpress.Utils.PointFloat(200F, 675F);
            this.PanelObservations.Name = "PanelObservations";
            this.PanelObservations.SizeF = new System.Drawing.SizeF(420F, 25F);
            // 
            // operatorObservationsTable
            // 
            this.operatorObservationsTable.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.operatorObservationsTable.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.operatorObservationsTable.Name = "operatorObservationsTable";
            this.operatorObservationsTable.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.operatorObservationsTable.SizeF = new System.Drawing.SizeF(420F, 25F);
            this.operatorObservationsTable.StylePriority.UseBorders = false;
            this.operatorObservationsTable.StylePriority.UseTextAlignment = false;
            this.operatorObservationsTable.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.typeObservationLabel,
            this.observationLabel,
            this.DatetimeLabel,
            this.supervisorLabel});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1D;
            // 
            // typeObservationLabel
            // 
            this.typeObservationLabel.KeepTogether = true;
            this.typeObservationLabel.Multiline = true;
            this.typeObservationLabel.Name = "typeObservationLabel";
            this.typeObservationLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.typeObservationLabel.Text = "Tipo";
            this.typeObservationLabel.Weight = 0.2D;
            // 
            // observationLabel
            // 
            this.observationLabel.Name = "observationLabel";
            this.observationLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.observationLabel.Text = "Observacion";
            this.observationLabel.Weight = 0.31666666666666665D;
            // 
            // DatetimeLabel
            // 
            this.DatetimeLabel.Name = "DatetimeLabel";
            this.DatetimeLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DatetimeLabel.Text = "Fecha y hora";
            this.DatetimeLabel.Weight = 0.28333333333333333D;
            // 
            // supervisorLabel
            // 
            this.supervisorLabel.Name = "supervisorLabel";
            this.supervisorLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.supervisorLabel.Text = "Supervisor";
            this.supervisorLabel.Weight = 0.2D;
            // 
            // PanelCategory
            // 
            this.PanelCategory.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.operatorCategoryTable});
            this.PanelCategory.LocationFloat = new DevExpress.Utils.PointFloat(200F, 633F);
            this.PanelCategory.Name = "PanelCategory";
            this.PanelCategory.SizeF = new System.Drawing.SizeF(242F, 25F);
            // 
            // operatorCategoryTable
            // 
            this.operatorCategoryTable.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.operatorCategoryTable.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.operatorCategoryTable.Name = "operatorCategoryTable";
            this.operatorCategoryTable.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.operatorCategoryTable.SizeF = new System.Drawing.SizeF(242F, 25F);
            this.operatorCategoryTable.StylePriority.UseBorders = false;
            this.operatorCategoryTable.StylePriority.UseTextAlignment = false;
            this.operatorCategoryTable.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.categoryTableLabel,
            this.dateTableLabel});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // categoryTableLabel
            // 
            this.categoryTableLabel.Name = "categoryTableLabel";
            this.categoryTableLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.categoryTableLabel.StylePriority.UseTextAlignment = false;
            this.categoryTableLabel.Text = "Categoria";
            this.categoryTableLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.categoryTableLabel.Weight = 0.5D;
            // 
            // dateTableLabel
            // 
            this.dateTableLabel.Name = "dateTableLabel";
            this.dateTableLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.dateTableLabel.StylePriority.UseTextAlignment = false;
            this.dateTableLabel.Text = "Desde";
            this.dateTableLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.dateTableLabel.Weight = 0.5D;
            // 
            // evaluationsLabel
            // 
            this.evaluationsLabel.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.evaluationsLabel.LocationFloat = new DevExpress.Utils.PointFloat(42F, 758F);
            this.evaluationsLabel.Name = "evaluationsLabel";
            this.evaluationsLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.evaluationsLabel.SizeF = new System.Drawing.SizeF(100F, 25F);
            this.evaluationsLabel.StylePriority.UseFont = false;
            this.evaluationsLabel.Text = "Evaluaciones";
            // 
            // coursesLabel
            // 
            this.coursesLabel.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.coursesLabel.LocationFloat = new DevExpress.Utils.PointFloat(42F, 717F);
            this.coursesLabel.Name = "coursesLabel";
            this.coursesLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.coursesLabel.SizeF = new System.Drawing.SizeF(117F, 25F);
            this.coursesLabel.StylePriority.UseFont = false;
            this.coursesLabel.Text = "Cursos realizados :";
            // 
            // observationsLabel
            // 
            this.observationsLabel.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.observationsLabel.LocationFloat = new DevExpress.Utils.PointFloat(42F, 675F);
            this.observationsLabel.Name = "observationsLabel";
            this.observationsLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.observationsLabel.SizeF = new System.Drawing.SizeF(100F, 25F);
            this.observationsLabel.StylePriority.UseFont = false;
            this.observationsLabel.Text = "Observaciones :";
            // 
            // operatorPhone
            // 
            this.operatorPhone.LocationFloat = new DevExpress.Utils.PointFloat(200F, 225F);
            this.operatorPhone.Name = "operatorPhone";
            this.operatorPhone.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.operatorPhone.SizeF = new System.Drawing.SizeF(100F, 25F);
            this.operatorPhone.Text = "operatorPhone";
            // 
            // phoneLabel
            // 
            this.phoneLabel.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.phoneLabel.LocationFloat = new DevExpress.Utils.PointFloat(42F, 225F);
            this.phoneLabel.Name = "phoneLabel";
            this.phoneLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.phoneLabel.SizeF = new System.Drawing.SizeF(100F, 25F);
            this.phoneLabel.StylePriority.UseFont = false;
            this.phoneLabel.Text = "Telefono :";
            // 
            // categoryLabel
            // 
            this.categoryLabel.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.categoryLabel.LocationFloat = new DevExpress.Utils.PointFloat(42F, 633F);
            this.categoryLabel.Name = "categoryLabel";
            this.categoryLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.categoryLabel.SizeF = new System.Drawing.SizeF(100F, 25F);
            this.categoryLabel.StylePriority.UseFont = false;
            this.categoryLabel.Text = "Categoria :";
            this.categoryLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable1
            // 
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(200F, 500F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(183F, 108F);
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.operatorDepartamets});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // operatorDepartamets
            // 
            this.operatorDepartamets.Name = "operatorDepartamets";
            this.operatorDepartamets.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.operatorDepartamets.StylePriority.UseTextAlignment = false;
            this.operatorDepartamets.Text = "operatorDepartamets";
            this.operatorDepartamets.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            this.operatorDepartamets.Weight = 1D;
            // 
            // departamentsLabel
            // 
            this.departamentsLabel.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.departamentsLabel.LocationFloat = new DevExpress.Utils.PointFloat(42F, 500F);
            this.departamentsLabel.Name = "departamentsLabel";
            this.departamentsLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.departamentsLabel.SizeF = new System.Drawing.SizeF(100F, 25F);
            this.departamentsLabel.StylePriority.UseFont = false;
            this.departamentsLabel.Text = "Organismos :";
            this.departamentsLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // operatorRole
            // 
            this.operatorRole.LocationFloat = new DevExpress.Utils.PointFloat(200F, 442F);
            this.operatorRole.Name = "operatorRole";
            this.operatorRole.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.operatorRole.SizeF = new System.Drawing.SizeF(183F, 25F);
            this.operatorRole.Text = "operatorRole";
            // 
            // roleLabel
            // 
            this.roleLabel.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.roleLabel.LocationFloat = new DevExpress.Utils.PointFloat(42F, 442F);
            this.roleLabel.Name = "roleLabel";
            this.roleLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.roleLabel.SizeF = new System.Drawing.SizeF(100F, 25F);
            this.roleLabel.StylePriority.UseFont = false;
            this.roleLabel.Text = "Rol :";
            this.roleLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // operatorEmail
            // 
            this.operatorEmail.LocationFloat = new DevExpress.Utils.PointFloat(200F, 392F);
            this.operatorEmail.Name = "operatorEmail";
            this.operatorEmail.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.operatorEmail.SizeF = new System.Drawing.SizeF(192F, 25F);
            this.operatorEmail.Text = "operatorEmail";
            // 
            // emailLabel
            // 
            this.emailLabel.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.emailLabel.LocationFloat = new DevExpress.Utils.PointFloat(42F, 392F);
            this.emailLabel.Name = "emailLabel";
            this.emailLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.emailLabel.SizeF = new System.Drawing.SizeF(125F, 25F);
            this.emailLabel.StylePriority.UseFont = false;
            this.emailLabel.Text = "Correo electronico :";
            this.emailLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // operatorAddress
            // 
            this.operatorAddress.LocationFloat = new DevExpress.Utils.PointFloat(200F, 267F);
            this.operatorAddress.Name = "operatorAddress";
            this.operatorAddress.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.operatorAddress.SizeF = new System.Drawing.SizeF(417F, 100F);
            this.operatorAddress.Text = "operatorAddress";
            // 
            // addressLabel
            // 
            this.addressLabel.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.addressLabel.LocationFloat = new DevExpress.Utils.PointFloat(42F, 267F);
            this.addressLabel.Name = "addressLabel";
            this.addressLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.addressLabel.SizeF = new System.Drawing.SizeF(100F, 25F);
            this.addressLabel.StylePriority.UseFont = false;
            this.addressLabel.Text = "Direccion :";
            this.addressLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // operatorDateBirth
            // 
            this.operatorDateBirth.LocationFloat = new DevExpress.Utils.PointFloat(200F, 183F);
            this.operatorDateBirth.Name = "operatorDateBirth";
            this.operatorDateBirth.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.operatorDateBirth.SizeF = new System.Drawing.SizeF(192F, 25F);
            this.operatorDateBirth.Text = "operatorDateBirth";
            // 
            // dateBirthLabel
            // 
            this.dateBirthLabel.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.dateBirthLabel.LocationFloat = new DevExpress.Utils.PointFloat(42F, 183F);
            this.dateBirthLabel.Name = "dateBirthLabel";
            this.dateBirthLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.dateBirthLabel.SizeF = new System.Drawing.SizeF(142F, 25F);
            this.dateBirthLabel.StylePriority.UseFont = false;
            this.dateBirthLabel.Text = "Fecha de Nacimiento :";
            this.dateBirthLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // operatorID
            // 
            this.operatorID.LocationFloat = new DevExpress.Utils.PointFloat(200F, 133F);
            this.operatorID.Name = "operatorID";
            this.operatorID.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.operatorID.SizeF = new System.Drawing.SizeF(183F, 25F);
            this.operatorID.Text = "operatorID";
            // 
            // IdLabel
            // 
            this.IdLabel.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.IdLabel.LocationFloat = new DevExpress.Utils.PointFloat(42F, 133F);
            this.IdLabel.Name = "IdLabel";
            this.IdLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.IdLabel.SizeF = new System.Drawing.SizeF(100F, 25F);
            this.IdLabel.StylePriority.UseFont = false;
            this.IdLabel.Text = "Cedula :";
            this.IdLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // operatorLastName
            // 
            this.operatorLastName.LocationFloat = new DevExpress.Utils.PointFloat(200F, 83F);
            this.operatorLastName.Name = "operatorLastName";
            this.operatorLastName.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.operatorLastName.SizeF = new System.Drawing.SizeF(183F, 25F);
            this.operatorLastName.Text = "operatorLastName";
            // 
            // lastNameLabel
            // 
            this.lastNameLabel.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.lastNameLabel.LocationFloat = new DevExpress.Utils.PointFloat(42F, 83F);
            this.lastNameLabel.Name = "lastNameLabel";
            this.lastNameLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lastNameLabel.SizeF = new System.Drawing.SizeF(100F, 25F);
            this.lastNameLabel.StylePriority.UseFont = false;
            this.lastNameLabel.Text = "Apellidos :";
            this.lastNameLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // OperatorName
            // 
            this.OperatorName.LocationFloat = new DevExpress.Utils.PointFloat(200F, 33F);
            this.OperatorName.Name = "OperatorName";
            this.OperatorName.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.OperatorName.SizeF = new System.Drawing.SizeF(183F, 25F);
            this.OperatorName.Text = "OperatorName";
            // 
            // nameLabel
            // 
            this.nameLabel.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.nameLabel.LocationFloat = new DevExpress.Utils.PointFloat(42F, 33F);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.nameLabel.SizeF = new System.Drawing.SizeF(100F, 25F);
            this.nameLabel.StylePriority.UseFont = false;
            this.nameLabel.Text = "Nombre :";
            this.nameLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageHeader
            // 
            this.PageHeader.HeightF = 30F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageFooter
            // 
            this.PageFooter.HeightF = 30F;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrControlStyle1
            // 
            this.xrControlStyle1.Name = "xrControlStyle1";
            this.xrControlStyle1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // ReportPersonaFile
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.PageFooter,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.ExportOptions.PrintPreview.SaveMode = DevExpress.XtraPrinting.SaveMode.UsingDefaultPath;
            this.ExportOptions.PrintPreview.ShowOptionsBeforeExport = false;
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.xrControlStyle1});
            this.Version = "11.1";
            ((System.ComponentModel.ISupportInitialize)(this.operatorPhoto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.operatorEvaluationsTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.operatorCourseTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.operatorObservationsTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.operatorCategoryTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRLabel nameLabel;
        private DevExpress.XtraReports.UI.XRLabel operatorLastName;
        private DevExpress.XtraReports.UI.XRLabel lastNameLabel;
        private DevExpress.XtraReports.UI.XRLabel OperatorName;
        private DevExpress.XtraReports.UI.XRLabel operatorID;
        private DevExpress.XtraReports.UI.XRLabel IdLabel;
        private DevExpress.XtraReports.UI.XRLabel operatorAddress;
        private DevExpress.XtraReports.UI.XRLabel addressLabel;
        private DevExpress.XtraReports.UI.XRLabel operatorDateBirth;
        private DevExpress.XtraReports.UI.XRLabel dateBirthLabel;
        private DevExpress.XtraReports.UI.XRLabel operatorRole;
        private DevExpress.XtraReports.UI.XRLabel roleLabel;
        private DevExpress.XtraReports.UI.XRLabel operatorEmail;
        private DevExpress.XtraReports.UI.XRLabel emailLabel;
        private DevExpress.XtraReports.UI.XRLabel categoryLabel;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell operatorDepartamets;
        private DevExpress.XtraReports.UI.XRLabel departamentsLabel;
        private DevExpress.XtraReports.UI.XRControlStyle xrControlStyle1;
        private DevExpress.XtraReports.UI.XRTable operatorCategoryTable;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell categoryTableLabel;
        private DevExpress.XtraReports.UI.XRTableCell dateTableLabel;
        private DevExpress.XtraReports.UI.XRLabel operatorPhone;
        private DevExpress.XtraReports.UI.XRLabel phoneLabel;
        private DevExpress.XtraReports.UI.XRTable operatorObservationsTable;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell typeObservationLabel;
        private DevExpress.XtraReports.UI.XRTableCell observationLabel;
        private DevExpress.XtraReports.UI.XRTableCell DatetimeLabel;
        private DevExpress.XtraReports.UI.XRTableCell supervisorLabel;
        private DevExpress.XtraReports.UI.XRLabel observationsLabel;
        private DevExpress.XtraReports.UI.XRTable operatorCourseTable;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell courseLabel;
        private DevExpress.XtraReports.UI.XRTableCell DateCourseLabel;
        private DevExpress.XtraReports.UI.XRLabel coursesLabel;
        private DevExpress.XtraReports.UI.XRTable operatorEvaluationsTable;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell nameEvaluationLabel;
        private DevExpress.XtraReports.UI.XRTableCell qualificationEvaluationLabel;
        private DevExpress.XtraReports.UI.XRTableCell dateEvaluationLabel;
        private DevExpress.XtraReports.UI.XRTableCell evaluatorLabel;
        private DevExpress.XtraReports.UI.XRLabel evaluationsLabel;
        private DevExpress.XtraReports.UI.XRPanel PanelCategory;
        private DevExpress.XtraReports.UI.XRPanel PanelObservations;
        private DevExpress.XtraReports.UI.XRPanel PanelEvaluations;
        private DevExpress.XtraReports.UI.XRPanel PanelCourses;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer1;
        private System.Windows.Forms.PictureBox operatorPhoto;
        private DevExpress.XtraReports.UI.TopMarginBand topMarginBand1;
        private DevExpress.XtraReports.UI.BottomMarginBand bottomMarginBand1;
    }
}
