using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Smartmatic.SmartCad.Service;
using System.Net;
using System.Collections;
using DevExpress.XtraBars;
using System.IO;
using System.Xml;
using SmartCadCore.Common;
using SmartCadCore.Core;
using SmartCadCore.ClientData;
using SmartCadCore.Enums;
using SmartCadGuiCommon.CCTV.Nodes;
using SmartCadGuiCommon;
using SmartCadGuiCommon.Classes;

namespace SmartCadFirstLevel.Gui
{
	public partial class TwitterFrontClientFormDevX : XtraForm
	{
		#region Fields

        TwitterAlarmsForm twitterAlarmsForm;
        DefaultTwitterFrontClientForm defaultTwitterFrontClientForm;
        CctvIncidentsHistoryForm historyForm;
        OperatorChatForm chatForm;

		private ApplicationMessageClientData alertControlAmcd = null;

        public BindingList<CctvCameraNode> Cameras = new BindingList<CctvCameraNode>();
        public BindingList<CctvTemplateNode> Templates = new BindingList<CctvTemplateNode>();

        private DateTime syncServerDateTime;
        private TimeSpan sinceLoggedIn;
        private const int GLOBAL_TIMER_PERIOD = 10000;
        private readonly TimeSpan ADD_TIMESPAN;
        public System.Threading.Timer globalTimer;
        private string labelUserCaption = "";
        private string labelConnectedCaption = "";

        ServerServiceClient serverServiceClient = null;

		#endregion

		#region Properties

		public ServerServiceClient ServerServiceClient
		{
			set
			{
                defaultTwitterFrontClientForm.ServerServiceClient = value;
			}
        }

        public NetworkCredential NetworkCredential
        {
            set
            {
                defaultTwitterFrontClientForm.NetworkCredential = value;
            }
        }

        public ConfigurationClientData ConfigurationClient
        {
            set
            {
                defaultTwitterFrontClientForm.ConfigurationClient = value;
            }
        }

        #endregion

        public TwitterFrontClientFormDevX()
		{
            InitializeComponent(); 
            WindowState = FormWindowState.Maximized;

            defaultTwitterFrontClientForm = new DefaultTwitterFrontClientForm() { MdiParent = this };
			LoadLanguage();
			ServerServiceClient.GetInstance().CommittedChanges += FrontClientFormDevX_CommittedChanges;
            ADD_TIMESPAN = new TimeSpan(0, 0, 0, 0, GLOBAL_TIMER_PERIOD);
		}

        public void LoadInitialData()
        {
            serverServiceClient = ServerServiceClient.GetInstance();
            twitterAlarmsForm = new TwitterAlarmsForm() { MdiParent = this };
            defaultTwitterFrontClientForm.ServerServiceClient = serverServiceClient;
            
            defaultTwitterFrontClientForm.LoadInitialData();
            twitterAlarmsForm.LoadInitialData();
            CheckAccessToMap();

            xtraTabbedMdiManager1.Pages[defaultTwitterFrontClientForm].ShowCloseButton = DevExpress.Utils.DefaultBoolean.False;
            xtraTabbedMdiManager1.Pages[twitterAlarmsForm].ShowCloseButton = DevExpress.Utils.DefaultBoolean.False;

            FillStatusStrip();
        }

        private void FrontClientFormDevX_Load(object sender, EventArgs e)
        {
            defaultTwitterFrontClientForm.Show();
            twitterAlarmsForm.Show();

            repositoryItemProgressBar1.Step = 1;
            repositoryItemProgressBar1.Maximum = int.Parse(defaultTwitterFrontClientForm.GlobalApplicationPreference["AlarmTimeLimit"].Value);

            xtraTabbedMdiManager1.SelectedPageChanged += xtraTabbedMdiManager1_SelectedPageChanged;
        }

        private void LoadLanguage()
        {
            Text = ResourceLoader.GetString2("SocialNetworksModule");
            barButtonItemOpenMap.Glyph = ResourceLoader.GetImage("$Image.OpenMap");
            barButtonItemChat.Caption = ResourceLoader.GetString2("Chat");
            barButtonItemOpenMap.LargeGlyph = ResourceLoader.GetImage("$Image.OpenMap");

            barButtonItemChat.Caption = ResourceLoader.GetString2("Chat");
            barButtonItemOpenMap.Caption = ResourceLoader.GetString2("Open");
            barButtonItemPrint.Caption = ResourceLoader.GetString2("Print");
            barButtonItemRefresh.Caption = ResourceLoader.GetString2("Refresh");
            barButtonItemSave.Caption = ResourceLoader.GetString2("Save");
            barButtonItemCancelIncident.Caption = ResourceLoader.GetString2("Cancel");
            barButtonItemCreateNewIncident.Caption = ResourceLoader.GetString2("Register");
            barButtonItemStartRegIncident.Caption = ResourceLoader.GetString2("New");
            barButtonItemCancelAlarm.Caption = ResourceLoader.GetString2("CancelAlert");
            barButtonItemHistory.Caption = ResourceLoader.GetString2("IncidentsHistoryForm");
            ribbonPageGroupCallInfo.Text = ResourceLoader.GetString2("Incidents");
            ribbonPageGroupApplications.Text = ResourceLoader.GetString2("Applications");
            //Text = defaultCctvFrontClientFormDevX.Text;
            ribbonPage1.Text = ResourceLoader.GetString2("SocialNetworks");

            #region Footer
            labelUserCaption = ResourceLoader.GetString2("User") + ": ";
            labelConnectedCaption = ResourceLoader.GetString2("Connected") + ": ";
            #endregion
        }

        private void FillStatusStrip()
        {
            syncServerDateTime = ServerServiceClient.GetInstance().GetTime();
            sinceLoggedIn = TimeSpan.Zero;
            this.labelConnected.Caption = "00:00";

            labelDate.Caption = ApplicationUtil.GetFormattedDate(serverServiceClient.GetTime());
            globalTimer = ApplicationUtil.RecurrentExecuteMethod(
                new SimpleDelegate(
                delegate
                {
                    globalTimer_Tick(null, null);
                }), null, null, GLOBAL_TIMER_PERIOD, GLOBAL_TIMER_PERIOD);

            labelUser.Caption = labelUserCaption + serverServiceClient.OperatorClient.FirstName
                + " " + serverServiceClient.OperatorClient.LastName;
            labelConnected.Caption = labelConnectedCaption + "00:00";
        }

        private void globalTimer_Tick(object sender, EventArgs e)
        {
            syncServerDateTime = syncServerDateTime.AddMilliseconds((double)GLOBAL_TIMER_PERIOD);
            sinceLoggedIn = sinceLoggedIn.Add(ADD_TIMESPAN);
            FormUtil.InvokeRequired(this, delegate
            {
                labelDate.Caption = ApplicationUtil.GetFormattedDate(ServerServiceClient.GetInstance().GetTime());
                labelConnected.Caption = labelConnectedCaption + Math.Truncate(sinceLoggedIn.TotalHours).ToString().PadLeft(2, '0') + ":" + sinceLoggedIn.Minutes.ToString().PadLeft(2, '0');
            });
        }

		public void FrontClientFormDevX_CommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
		{
			try
			{
				if (e != null && e.Objects != null && e.Objects.Count > 0)
				{
					if (e.Objects[0] is ApplicationMessageClientData)
					{
                        #region ApplicationMessageClientData
						ApplicationMessageClientData amcd = ((ApplicationMessageClientData)e.Objects[0]);
						if (amcd.ToOperators.Contains(ServerServiceClient.GetInstance().OperatorClient))
						{
							if (chatForm != null && chatForm.IsDisposed == false)
							{
                                FormUtil.InvokeRequired(this, () =>
								{
									chatForm.OpenNewTab(amcd.Operator, amcd);
									chatForm.Activate();
								});
							}
							else
							{
								alertControlAmcd = amcd;
								FormUtil.InvokeRequired(this, () =>
								{
									alertControl1.Show(this, "Mensaje de: " + amcd.Operator.Login, amcd.Message);
								});
							}
                        }
                        #endregion
                    }
					else if (e.Objects[0] is ApplicationPreferenceClientData)
                    {
                        #region ApplicationPreferenceClientData
                        ApplicationPreferenceClientData preference = e.Objects[0] as ApplicationPreferenceClientData;
						if (e.Action == CommittedDataAction.Update)
						{
							if (preference.Name.Equals("AlarmTimeLimit"))
							{
								FormUtil.InvokeRequired(this, () =>
                                {
									repositoryItemProgressBar1.Maximum = int.Parse(preference.Value);
								});
							}
                            else if (preference.Name.Equals("LPRIncidentCodePrefix"))
                            {
                                historyForm.CCTVIncidentCodePrefix.Value = preference.Value;
                            }
                        }
                        #endregion
                    }
                    //esto va en el default
                    //else if (e.Objects[0] is AlarmLprClientData)
                    //{
                    //    #region AlarmLprClientData
                    //    AlarmLprClientData camera = ((AlarmLprClientData)e.Objects[0]);
                    //    FormUtil.InvokeRequired(this, () =>
                    //    {
                    //        //TODO: agregar al grid.
                    //    });
                    //    #endregion
                    //}
				}
			}
			catch
			{
			}
		}

		public void SetPassword(string p)
		{
            defaultTwitterFrontClientForm.Password = p;
            ServerServiceClient.GetInstance().OperatorClient.Password = p;
		}

        void xtraTabbedMdiManager1_SelectedPageChanged(object sender, EventArgs e)
        {
            
            if (xtraTabbedMdiManager1.SelectedPage.Text == defaultTwitterFrontClientForm.Text)
            {
                if (defaultTwitterFrontClientForm.FrontClientState == FrontClientStateEnum.RegisteringIncident)
                    barButtonItemStartRegIncident.Enabled = false;
                else
                    barButtonItemStartRegIncident.Enabled = true;
                //{
                //    if (defaultLprFrontClientXtraForm.SelectedAlarm != null)
                //        barButtonItemStartRegIncident.Enabled = true;
                //    else
                //        barButtonItemStartRegIncident.Enabled = false;
                //}
            }
        }

		private void FrontClientFormDevX_FormClosing(object sender, FormClosingEventArgs e)
		{
            //check if a call has entered or if an incident is being typed
        //    if (e.CloseReason == CloseReason.None)
        //    {
        //        ServerServiceClient.GetInstance().CommittedChanges -= FrontClientFormDevX_CommittedChanges;
        //        Application.Exit();
        //    }
        //    else
        //    {
        //        e.Cancel = true;
        //    }
        }

		private static bool EnsureLogout()
		{
			DialogResult dialogResult = MessageForm.Show(ResourceLoader.GetString2("$Message.ExitApplication"), MessageFormType.Question);

			return dialogResult == DialogResult.Yes;
		}

		private void CheckAccessToMap()
		{
			if (ServerServiceClient.GetInstance().CheckAccessClient(UserResourceClientData.Application,
				UserActionClientData.Basic, UserApplicationClientData.Map, false) == true)
				barButtonItemOpenMap.Enabled = true;
			else
				barButtonItemOpenMap.Enabled = false;
		}

		private void alertControl1_AlertClick(object sender, DevExpress.XtraBars.Alerter.AlertClickEventArgs e)
		{
			if (chatForm == null || chatForm.IsDisposed == true)
			{
				chatForm = new OperatorChatForm(UserApplicationClientData.Lpr.Name, ChatOperatorType.Operator);
			}
			chatForm.WindowState = FormWindowState.Normal;
			chatForm.Activate();
			chatForm.Show();
			chatForm.BringToFront();
		}        

		private void barButtonItemChat_ItemClick(object sender, ItemClickEventArgs e)
		{
			if (chatForm == null || chatForm.IsDisposed == true)
				chatForm = new OperatorChatForm(UserApplicationClientData.Lpr.Name,ChatOperatorType.Operator);
			
			chatForm.Activate();
			chatForm.Show();
		}

		private void barButtonItemExit_ItemClick(object sender, ItemClickEventArgs e)
		{
			this.Close();
		}

		public void barButtonItemHelp_ItemClick(object sender, ItemClickEventArgs e)
		{
			try
            {
                ApplicationUtil.LaunchHelp(SmartCadConfiguration.DistFolder + "/Help/SmartCad.chm");
            }
            catch (Exception ex)
            {
                MessageForm.Show(ResourceLoader.GetString2("$Message.HelpNotInstalled"), ex);
            }
        }

		private void barButtonItemOpenMap_ItemClick(object sender, ItemClickEventArgs e)
		{
			try
            {
                ApplicationUtil.LaunchApplicationWithArguments(
                        SmartCadConfiguration.SmartCadMapFilename,
						new object[] { ServerServiceClient.GetInstance().OperatorClient.Login,
                            defaultTwitterFrontClientForm.Password, 
                            ApplicationUtil.CheckPrimaryScreenBounds(Left),
                            new SendActivateLayerEventArgs(ShapeType.Incident),
                            new SendActivateLayerEventArgs(ShapeType.Struct)
                        });
            }
            catch (Exception ex)
            {
                MessageForm.Show(ex.Message, ex);
            }
		}

        private object selectedTrigger;

        public void FisnishAlarm()
        {
            xtraTabbedMdiManager1.SelectedPage = xtraTabbedMdiManager1.Pages[twitterAlarmsForm];
            //TODO: DELETE
            //if (selectedTrigger is TwitterAlarmGridData)
            //    twitterAlarmsForm.FisnishAlarm(twitterAlarmsForm.SelectedAlarm);
        }

        public void ReleaseAlarm()
        {
            if (selectedTrigger is TwitterAlarmGridData)
                twitterAlarmsForm.ReleaseAlarm(twitterAlarmsForm.SelectedAlarm);
        }

        private void barButtonItemStartRegIncident_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (xtraTabbedMdiManager1.SelectedPage.Text == twitterAlarmsForm.Text)
            {
                if (twitterAlarmsForm.SelectedAlarm != null)
                {
                    selectedTrigger = twitterAlarmsForm.SelectedAlarm;
                    twitterAlarmsForm.TakeAlarm(twitterAlarmsForm.SelectedAlarm);
                    defaultTwitterFrontClientForm.barButtonItemStartRegIncident_ItemClick(twitterAlarmsForm.SelectedAlarm, null);
                }
            }
            xtraTabbedMdiManager1.SelectedPage = xtraTabbedMdiManager1.Pages[defaultTwitterFrontClientForm];
        }

        private void barButtonItemCancelAlarm_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (xtraTabbedMdiManager1.SelectedPage.Text == twitterAlarmsForm.Text)
            {
                twitterAlarmsForm.CancelAlarm();
            }
            //xtraTabbedMdiManager1.SelectedPage = xtraTabbedMdiManager1.Pages[defaultLprFrontClientForm];
        }

        private void barButtonItemHistory_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (historyForm == null || historyForm.IsDisposed == true)
            {
                historyForm = new CctvIncidentsHistoryForm();
            }
            historyForm.MdiParent = this;
            historyForm.Activate();
            historyForm.Show();
        }

	}
}