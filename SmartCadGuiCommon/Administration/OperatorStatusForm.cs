using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using System.ServiceModel;
using DevExpress.XtraEditors;
using SmartCadGuiCommon;
using SmartCadCore.ClientData;
using SmartCadCore.Enums;
using SmartCadCore.Core;
using SmartCadCore.Common;
using Smartmatic.SmartCad.Service;

namespace Smartmatic.SmartCad.Gui
{
    public partial class OperatorStatusForm : XtraForm
    {
        private object syncCommitted = new object();
        private AdministrationRibbonForm parentForm;
        OperatorStatusClientData selectedOperatorStatus = null;
        FormBehavior behaviorType = FormBehavior.Edit;
        byte[] available = FormUtil.ImageToArray(ResourceLoader.GetImage("$Image.Available"));
        byte[] Busy = FormUtil.ImageToArray(ResourceLoader.GetImage("$Image.Busy"));

        public OperatorStatusForm(AdministrationRibbonForm parentForm)
        {
            InitializeComponent();
            available = FormUtil.ImageToArray(ResourceLoader.GetImage("$Image.Available"));
            LoadLanguague();
            this.parentForm = parentForm;
            this.parentForm.AdministrationCommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(OperatorStatusForm_AdministrationCommittedChanges);
            FormClosing += new FormClosingEventHandler(OperatorStatusForm_FormClosing);
        }

        void OperatorStatusForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.parentForm != null)
            {
                this.parentForm.AdministrationCommittedChanges -= new EventHandler<CommittedObjectDataCollectionEventArgs>(OperatorStatusForm_AdministrationCommittedChanges);
            }
        }

        void OperatorStatusForm_AdministrationCommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
        {
            try
            {
                if (e.Objects != null && e.Objects.Count > 0)
                {
                    lock (syncCommitted)
                    {
                        try
                        {
                            if (ServerServiceClient.GetInstance().OperatorClient == null)
                            { }
                        }
                        catch
                        { }

                        if (e.Objects[0] is OperatorStatusClientData)
                        {
                            #region OperatorStatusClientData
                            OperatorStatusClientData operatorStatus =
                                    e.Objects[0] as OperatorStatusClientData;
                            if (SelectedOperatorStatus != null && SelectedOperatorStatus.Equals(operatorStatus) && ButtonOkPressed == false)
                            {
                                if (e.Action == CommittedDataAction.Update)
                                {
                                    MessageForm.Show(ResourceLoader.GetString2("UpdateFormOperatorStatusData"), MessageFormType.Warning);
                                    SelectedOperatorStatus = operatorStatus;
                                }
                                else if (e.Action == CommittedDataAction.Delete)
                                {
                                    MessageForm.Show(ResourceLoader.GetString2("DeleteFormOperatorStatusData"), MessageFormType.Warning);
                                    FormUtil.InvokeRequired(this,
                                    delegate
                                    {
                                        this.Close();
                                    });
                                }

                            }
                            #endregion
                        }                        
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        private void LoadLanguague()
        {
            this.layoutControlGroupData.Text = ResourceLoader.GetString2("OperatorStatusData");
            this.layoutControlItemName.Text = ResourceLoader.GetString2("$Message.Name") + ":";
            this.layoutControlItemColor.Text = ResourceLoader.GetString2("OperatorStatusColor") + ":";
            this.layoutControlItemPorcentage.Text = ResourceLoader.GetString2("Percentage") + ":";
            this.layoutControlItemTolerance.Text = ResourceLoader.GetString2("Tolerance") + ":";
            this.buttonCancel.Text = ResourceLoader.GetString2("$Message.Cancel");
        }

        public OperatorStatusForm(AdministrationRibbonForm parentForm, OperatorStatusClientData operatorStatus, FormBehavior type)
            :this(parentForm)
		{
			BehaviorType = type;
			SelectedOperatorStatus = operatorStatus;
		}

        public FormBehavior BehaviorType
        {
            get
            {
                return behaviorType;
            }
            set
            {
               
                behaviorType = value;
                Clear();
            }
        }

        public OperatorStatusClientData SelectedOperatorStatus
        {
            get
            {
                return selectedOperatorStatus;
            }
            set
            {

                Clear();
                selectedOperatorStatus = value;
                if (selectedOperatorStatus != null)
                {
                    textBoxName.Text = selectedOperatorStatus.FriendlyName;
                    if (selectedOperatorStatus.NotReady.HasValue == true)
                        checkBoxAvailable.Checked = selectedOperatorStatus.NotReady.Value == false;
                    else
                        checkBoxAvailable.Checked = false;

                    if (selectedOperatorStatus.Percentage != null)
                        textBoxExPorcentage.Text = selectedOperatorStatus.Percentage.ToString();
                    else
                        textBoxExPorcentage.Text = "0";

                    if (selectedOperatorStatus.Tolerance != null)
                        textBoxExTolerance.Text = selectedOperatorStatus.Tolerance.ToString();
                    else
                        textBoxExTolerance.Text = "0";

                    labelExStatusColor.BackColor = selectedOperatorStatus.Color;

                }
             
                OperatorStatusParameters_Change(null, null);
            }
        }

       

        private void Clear()
        {
            textBoxName.Clear();
            textBoxExPorcentage.Clear();
            textBoxExTolerance.Clear();
            labelExStatusColor.BackColor = Color.Black;
            checkBoxAvailable.Checked = false;
        }

        private void OperatorStatusParameters_Change(object sender, System.EventArgs e)
        {
            if (string.IsNullOrEmpty(textBoxName.Text) ||
                string.IsNullOrEmpty(textBoxExTolerance.Text) ||
                string.IsNullOrEmpty(textBoxExPorcentage.Text))
                buttonOk.Enabled = false;
            else
                buttonOk.Enabled = true;
           
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                BackgroundProcessForm processForm = new BackgroundProcessForm(this, new MethodInfo[1] { GetType().GetMethod("buttonOk_Click1", BindingFlags.NonPublic | BindingFlags.Instance) }, new object[1][] { new object[0] });
                processForm.CanThrowError = true;
                processForm.ShowDialog();
            }
            catch (FaultException ex)
            {
                SelectedOperatorStatus = (OperatorStatusClientData)ServerServiceClient.GetInstance().RefreshClient(SelectedOperatorStatus);
                GetErrorFocus(ex);
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, MessageFormType.Error);
                buttonOkPressed = false;
            }
            catch (Exception ex)
            {
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, ex);
                buttonOkPressed = false;
            }
        }

        private void GetErrorFocus(FaultException ex)
        {
            if (ex.Message.Contains("nombre"))
            {
                textBoxName.Focus();
            }
        }

        private bool buttonOkPressed = false;

        public bool ButtonOkPressed
        {
            get { return buttonOkPressed; }
        }

        private void buttonOk_Click1()
        {
            buttonOkPressed = true;
            if (selectedOperatorStatus != null)
            {
                if ((int.Parse(textBoxExPorcentage.Text.Trim()) + (int.Parse(textBoxExTolerance.Text.Trim()) * int.Parse(textBoxExPorcentage.Text.Trim()) / 100.0) > 100))
                    throw new FaultException(ResourceLoader.GetString2("MessageErrorToleranceNot100percent"));
                selectedOperatorStatus.FriendlyName = textBoxName.Text.Trim();
                selectedOperatorStatus.Color = labelExStatusColor.BackColor;
                if (!string.IsNullOrEmpty(textBoxExPorcentage.Text))
                    selectedOperatorStatus.Percentage = int.Parse(textBoxExPorcentage.Text);
                else
                    selectedOperatorStatus.Percentage = 0;

                if (!string.IsNullOrEmpty(textBoxExTolerance.Text))
                    selectedOperatorStatus.Tolerance = int.Parse(textBoxExTolerance.Text);
                else
                    selectedOperatorStatus.Tolerance = 0;

                selectedOperatorStatus.NotReady = true;
                if (selectedOperatorStatus.Name == "Ready")
                {
                    selectedOperatorStatus.NotReady = false;
                }

                ServerServiceClient.GetInstance().SaveOrUpdateClientData(selectedOperatorStatus);

            }

        }

        private void OperatorStatusForm_Load(object sender, EventArgs e)
        {
            this.Width = 508;
            this.Height = 191;
            this.Icon = ResourceLoader.GetIcon("$Icon.OperatorStatus");
            if (behaviorType == FormBehavior.Create)
            {
                Text = ResourceLoader.GetString2("OperatorStatusFormCreateText");
                buttonOk.Text = ResourceLoader.GetString("OperatorStatusFormCreateButtonOkText");
            }
            else if (behaviorType == FormBehavior.Edit)
            {
                Text = ResourceLoader.GetString2("OperatorStatusFormEditText");
                buttonOk.Text = ResourceLoader.GetString("OperatorStatusFormEditButtonOkText");

                if (selectedOperatorStatus.CustomCode == "REUNION" || selectedOperatorStatus.CustomCode == "BATHROOM" || selectedOperatorStatus.CustomCode == "REST")
                {
                    this.layoutControlItemPorcentage.Control.Visible = true;
                    this.layoutControlItemPorcentage.TextVisible = true;
                    this.layoutControlItemTolerance.Control.Visible = true;
                    this.layoutControlItemTolerance.TextVisible = true;
                }
                else
                {
                    this.layoutControl1.BeginUpdate();
                    this.layoutControl1.Controls.Remove(this.textBoxExTolerance);
                    this.layoutControl1.Controls.Remove(this.textBoxExPorcentage);
                    this.layoutControl1.EndUpdate();
                    this.layoutControlItemPorcentage.Control.Visible = false;
                    this.layoutControlItemPorcentage.ContentVisible = false;
                    this.layoutControlItemPorcentage.TextVisible = false;
                    this.layoutControlItemTolerance.Control.Visible = false;
                    this.layoutControlItemTolerance.ContentVisible = false;
                    this.layoutControlItemTolerance.TextVisible = false;
                    //this.Size = new Size(458, 170);
                }
            }     
        }

        private void OperatorStatusForm_SizeChanged(object sender, EventArgs e)
        {
            this.Size = new Size(this.Size.Width, 220);
        }

        private void labelExStatusColor_Click(object sender, EventArgs e)
        {

        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            IList<int> outputs = new List<int>();
            outputs.Add(4);
            //ServerServiceClient.GetInstance().SetGPSOutputs("101", outputs, true, 30, 1);
        }
        
    }
}