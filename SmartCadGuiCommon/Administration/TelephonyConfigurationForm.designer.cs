﻿namespace SmartCadGuiCommon
{
    partial class TelephonyConfigurationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TelephonyConfigurationForm));
            this.layoutControlMain = new DevExpress.XtraLayout.LayoutControl();
            this.textEditPassword = new DevExpress.XtraEditors.TextEdit();
            this.simpleButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.radioGroupVirtual = new DevExpress.XtraEditors.RadioGroup();
            this.textEditExtension = new DevExpress.XtraEditors.TextEdit();
            this.comoboBoxEditComputer = new DevExpress.XtraEditors.ComboBoxEdit();
            this.simpleButtonAccept = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroupData = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemComputer = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemExtension = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemVirtual = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemAccept = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemCancel = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItemPassword = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlMain)).BeginInit();
            this.layoutControlMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPassword.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupVirtual.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditExtension.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comoboBoxEditComputer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemComputer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemExtension)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemVirtual)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAccept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPassword)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControlMain
            // 
            this.layoutControlMain.Controls.Add(this.textEditPassword);
            this.layoutControlMain.Controls.Add(this.simpleButtonCancel);
            this.layoutControlMain.Controls.Add(this.radioGroupVirtual);
            this.layoutControlMain.Controls.Add(this.textEditExtension);
            this.layoutControlMain.Controls.Add(this.comoboBoxEditComputer);
            this.layoutControlMain.Controls.Add(this.simpleButtonAccept);
            this.layoutControlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlMain.Location = new System.Drawing.Point(0, 0);
            this.layoutControlMain.Name = "layoutControlMain";
            this.layoutControlMain.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(419, 210, 250, 350);
            this.layoutControlMain.Root = this.layoutControlGroupData;
            this.layoutControlMain.Size = new System.Drawing.Size(464, 127);
            this.layoutControlMain.TabIndex = 0;
            this.layoutControlMain.Text = "layoutControl1";
            // 
            // textEditPassword
            // 
            this.textEditPassword.Location = new System.Drawing.Point(142, 73);
            this.textEditPassword.Name = "textEditPassword";
            this.textEditPassword.Properties.PasswordChar = '*';
            this.textEditPassword.Size = new System.Drawing.Size(300, 20);
            this.textEditPassword.StyleController = this.layoutControlMain;
            this.textEditPassword.TabIndex = 10;
            // 
            // simpleButtonCancel
            // 
            this.simpleButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonCancel.Location = new System.Drawing.Point(345, 122);
            this.simpleButtonCancel.Name = "simpleButtonCancel";
            this.simpleButtonCancel.Size = new System.Drawing.Size(97, 22);
            this.simpleButtonCancel.StyleController = this.layoutControlMain;
            this.simpleButtonCancel.TabIndex = 9;
            this.simpleButtonCancel.Text = "simpleButton2";
            // 
            // radioGroupVirtual
            // 
            this.radioGroupVirtual.AutoSizeInLayoutControl = true;
            this.radioGroupVirtual.Location = new System.Drawing.Point(142, 97);
            this.radioGroupVirtual.MaximumSize = new System.Drawing.Size(380, 0);
            this.radioGroupVirtual.MinimumSize = new System.Drawing.Size(380, 0);
            this.radioGroupVirtual.Name = "radioGroupVirtual";
            this.radioGroupVirtual.Size = new System.Drawing.Size(380, 21);
            this.radioGroupVirtual.StyleController = this.layoutControlMain;
            this.radioGroupVirtual.TabIndex = 7;
            // 
            // textEditExtension
            // 
            this.textEditExtension.Location = new System.Drawing.Point(142, 49);
            this.textEditExtension.Name = "textEditExtension";
            this.textEditExtension.Size = new System.Drawing.Size(300, 20);
            this.textEditExtension.StyleController = this.layoutControlMain;
            this.textEditExtension.TabIndex = 5;
            // 
            // comoboBoxEditComputer
            // 
            this.comoboBoxEditComputer.Location = new System.Drawing.Point(142, 25);
            this.comoboBoxEditComputer.Name = "comoboBoxEditComputer";
            this.comoboBoxEditComputer.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comoboBoxEditComputer.Size = new System.Drawing.Size(300, 20);
            this.comoboBoxEditComputer.StyleController = this.layoutControlMain;
            this.comoboBoxEditComputer.TabIndex = 4;
            // 
            // simpleButtonAccept
            // 
            this.simpleButtonAccept.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.simpleButtonAccept.Enabled = false;
            this.simpleButtonAccept.Location = new System.Drawing.Point(247, 122);
            this.simpleButtonAccept.Name = "simpleButtonAccept";
            this.simpleButtonAccept.Size = new System.Drawing.Size(94, 22);
            this.simpleButtonAccept.StyleController = this.layoutControlMain;
            this.simpleButtonAccept.TabIndex = 8;
            this.simpleButtonAccept.Text = "simpleButton1";
            this.simpleButtonAccept.Click += new System.EventHandler(this.simpleButtonAccept_Click);
            // 
            // layoutControlGroupData
            // 
            this.layoutControlGroupData.CustomizationFormText = "Root";
            this.layoutControlGroupData.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.False;
            this.layoutControlGroupData.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemComputer,
            this.layoutControlItemExtension,
            this.layoutControlItemVirtual,
            this.layoutControlItemAccept,
            this.layoutControlItemCancel,
            this.emptySpaceItem1,
            this.layoutControlItemPassword});
            this.layoutControlGroupData.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupData.Name = "Root";
            this.layoutControlGroupData.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupData.Size = new System.Drawing.Size(447, 149);
            this.layoutControlGroupData.Text = "Root";
            // 
            // layoutControlItemComputer
            // 
            this.layoutControlItemComputer.Control = this.comoboBoxEditComputer;
            this.layoutControlItemComputer.CustomizationFormText = "layoutControlItemComputer";
            this.layoutControlItemComputer.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemComputer.Name = "layoutControlItemComputer";
            this.layoutControlItemComputer.Size = new System.Drawing.Size(441, 24);
            this.layoutControlItemComputer.Text = "layoutControlItemComputer";
            this.layoutControlItemComputer.TextSize = new System.Drawing.Size(134, 13);
            // 
            // layoutControlItemExtension
            // 
            this.layoutControlItemExtension.Control = this.textEditExtension;
            this.layoutControlItemExtension.CustomizationFormText = "layoutControlItemExtension";
            this.layoutControlItemExtension.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItemExtension.Name = "layoutControlItemExtension";
            this.layoutControlItemExtension.Size = new System.Drawing.Size(441, 24);
            this.layoutControlItemExtension.Text = "layoutControlItemExtension";
            this.layoutControlItemExtension.TextSize = new System.Drawing.Size(134, 13);
            // 
            // layoutControlItemVirtual
            // 
            this.layoutControlItemVirtual.Control = this.radioGroupVirtual;
            this.layoutControlItemVirtual.CustomizationFormText = "layoutControlItemVirtual";
            this.layoutControlItemVirtual.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItemVirtual.MaxSize = new System.Drawing.Size(363, 25);
            this.layoutControlItemVirtual.MinSize = new System.Drawing.Size(363, 25);
            this.layoutControlItemVirtual.Name = "layoutControlItemVirtual";
            this.layoutControlItemVirtual.Size = new System.Drawing.Size(441, 25);
            this.layoutControlItemVirtual.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemVirtual.Text = "layoutControlItemVirtual";
            this.layoutControlItemVirtual.TextSize = new System.Drawing.Size(134, 13);
            this.layoutControlItemVirtual.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItemAccept
            // 
            this.layoutControlItemAccept.Control = this.simpleButtonAccept;
            this.layoutControlItemAccept.CustomizationFormText = "layoutControlItemAccept";
            this.layoutControlItemAccept.Location = new System.Drawing.Point(242, 97);
            this.layoutControlItemAccept.MinSize = new System.Drawing.Size(82, 26);
            this.layoutControlItemAccept.Name = "layoutControlItemAccept";
            this.layoutControlItemAccept.Size = new System.Drawing.Size(98, 26);
            this.layoutControlItemAccept.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemAccept.Text = "layoutControlItemAccept";
            this.layoutControlItemAccept.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemAccept.TextToControlDistance = 0;
            this.layoutControlItemAccept.TextVisible = false;
            // 
            // layoutControlItemCancel
            // 
            this.layoutControlItemCancel.Control = this.simpleButtonCancel;
            this.layoutControlItemCancel.CustomizationFormText = "layoutControlItemCancel";
            this.layoutControlItemCancel.Location = new System.Drawing.Point(340, 97);
            this.layoutControlItemCancel.MinSize = new System.Drawing.Size(82, 26);
            this.layoutControlItemCancel.Name = "layoutControlItemCancel";
            this.layoutControlItemCancel.Size = new System.Drawing.Size(101, 26);
            this.layoutControlItemCancel.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemCancel.Text = "layoutControlItemCancel";
            this.layoutControlItemCancel.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemCancel.TextToControlDistance = 0;
            this.layoutControlItemCancel.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 97);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(104, 24);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(242, 26);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItemPassword
            // 
            this.layoutControlItemPassword.Control = this.textEditPassword;
            this.layoutControlItemPassword.CustomizationFormText = "layoutControlItemPassword";
            this.layoutControlItemPassword.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItemPassword.Name = "layoutControlItemPassword";
            this.layoutControlItemPassword.Size = new System.Drawing.Size(441, 24);
            this.layoutControlItemPassword.Text = "layoutControlItemPassword";
            this.layoutControlItemPassword.TextSize = new System.Drawing.Size(134, 13);
            // 
            // TelephonyConfigurationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(464, 127);
            this.Controls.Add(this.layoutControlMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(480, 165);
            this.MinimumSize = new System.Drawing.Size(480, 165);
            this.Name = "TelephonyConfigurationForm";
            this.Text = "TelephonyConfigurationForm";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlMain)).EndInit();
            this.layoutControlMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEditPassword.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupVirtual.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditExtension.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comoboBoxEditComputer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemComputer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemExtension)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemVirtual)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAccept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPassword)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControlMain;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupData;
        private DevExpress.XtraEditors.ComboBoxEdit comoboBoxEditComputer;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemComputer;
        private DevExpress.XtraEditors.RadioGroup radioGroupVirtual;
        private DevExpress.XtraEditors.TextEdit textEditExtension;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemExtension;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemVirtual;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCancel;
        private DevExpress.XtraEditors.SimpleButton simpleButtonAccept;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemAccept;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemCancel;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.TextEdit textEditPassword;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemPassword;
    }
}