using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using SmartCadCore.ClientData;
using System.Collections;
using Smartmatic.SmartCad.Service;
using SmartCadCore.Core;
using SmartCadCore.Common;
using SmartCadCore.Enums;
using SmartCadGuiCommon.Controls.Administration;
using SmartCadGuiCommon.Administration;

namespace SmartCadGuiCommon
{
	public partial class WorkShiftRouteForm : WorkShiftRouteBaseForm
	{
		public override UnitClientData SelectedData
		{
			get
			{
				return data;
			}
			set
			{
				data = value;
				if (SelectedData.WorkShiftsRoutes != null)
					foreach (WorkShiftRouteClientData wsRoute in SelectedData.WorkShiftsRoutes)
						gridControlExWSRoutes.AddOrUpdateItem(new GridControlDataWSRoute(wsRoute));				
			}
		}
		
		public WorkShiftRouteForm()
		{
			InitializeComponent();
		}

		public WorkShiftRouteForm(AdministrationRibbonForm form)
			: base(form)
		{
			InitializeComponent();
		}
		
		public WorkShiftRouteForm(AdministrationRibbonForm form,UnitClientData unit, FormBehavior behavior) : this(form)
		{
			InitializeGrid();
			FillWorkShifts();
			this.SelectedData = unit;
			FillRoutes(SelectedData.DepartmentType.Code);
		}

		void WorkShiftRouteForm_Load(object sender, System.EventArgs e)
		{
			LoadLanguage();
		}

		protected override void LoadLanguage()
		{
            this.Icon = ResourceLoader.GetIcon("$Icon.WorkShiftRoute");
			simpleButtonAccept.Text = ResourceLoader.GetString2("Accept");
			simpleButtonCancel.Text = ResourceLoader.GetString2("Cancel");
			simpleButtonAdd.Text = ResourceLoader.GetString2("Add");
			layoutControlGroupWorkShift.Text = ResourceLoader.GetString2("WorkShifts");
			layoutControlGroupWSAndRoutes.Text = ResourceLoader.GetString2("WorkShiftsAndRoutes");
			layoutControlGroupWSRoutes.Text = ResourceLoader.GetString2("WorkShiftsRoutes");
			layoutControlGroupRoutes.Text = ResourceLoader.GetString2("Routes");
			this.Text = ResourceLoader.GetString2("WorkShiftRouteFormText");
            this.simpleButtonClean.Text = ResourceLoader.GetString2("Clear");
            this.simpleButtonDelete.Text = ResourceLoader.GetString2("$Message.Delete");
		}

		private void InitializeGrid()
		{
			gridControlExRoutes.Type = typeof(GridControlDataRoute);
			gridControlExRoutes.ViewTotalRows = true;
			gridControlExRoutes.EnableAutoFilter = true;
			gridViewExRoutes.OptionsView.ColumnAutoWidth = true;

			gridControlExWorkShift.Type = typeof(GridControlDataWorkShift);
			gridControlExWorkShift.ViewTotalRows = true;
			gridControlExWorkShift.EnableAutoFilter = true;
			gridViewExWorkShift.OptionsView.ColumnAutoWidth = true;

			gridControlExWSRoutes.Type = typeof(GridControlDataWSRoute);
			gridControlExWSRoutes.ViewTotalRows = true;
			gridControlExWSRoutes.EnableAutoFilter = true;
			gridViewExWSRoutes.OptionsView.ColumnAutoWidth = true;

		}

		private void FillRoutes(int depCode)
		{
			BindingList<GridControlDataRoute> dataSource = new BindingList<GridControlDataRoute>();
			IList routes = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetRoutesByDepartmentType, depCode));

			foreach (RouteClientData route in routes)
				dataSource.Add(new GridControlDataRoute(route));

			gridControlExRoutes.BeginInit();
			gridControlExRoutes.DataSource = dataSource;
			gridControlExRoutes.EndInit();

		}

		private void FillWorkShifts()
		{
			BindingList<GridControlDataWorkShift> dataSource = new BindingList<GridControlDataWorkShift>();
			DateTime now = ServerServiceClient.GetInstance().GetTime();

			IList wsList = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetUnitNotExpiredWorkShift, ApplicationUtil.GetDataBaseFormattedDate(now)));

			foreach (WorkShiftVariationClientData ws in wsList)
				dataSource.Add(new GridControlDataWorkShift(ws));


			gridControlExWorkShift.BeginInit();
			gridControlExWorkShift.DataSource = dataSource;
			gridControlExWorkShift.EndInit();

		}

		protected override void Save_Help()
		{
			foreach (GridControlDataWSRoute wsRoute in gridControlExWSRoutes.Items)
			{
				WorkShiftRouteClientData wsrcd = wsRoute.Tag as WorkShiftRouteClientData;
				wsrcd.UnitCode = SelectedData.Code;
				ServerServiceClient.GetInstance().SaveOrUpdateClientData(wsrcd);
			}
		}

		private void UnitParameters_Change(object sender, EventArgs e)
		{

		}
		
		private void simpleButtonAdd_Click(object sender, EventArgs e)
		{
			if (gridControlExRoutes.SelectedItems.Count > 0 && gridControlExWorkShift.SelectedItems.Count > 0)
			{
				GridControlDataRoute routeData = gridControlExRoutes.SelectedItems[0] as GridControlDataRoute;
				GridControlDataWorkShift wsData = gridControlExWorkShift.SelectedItems[0] as GridControlDataWorkShift;

				WorkShiftRouteClientData wsRoute = new WorkShiftRouteClientData();
				wsRoute.Route = new RouteClientData();
				wsRoute.Route.Code = routeData.Code;
				wsRoute.Route.Name = routeData.Name;

				wsRoute.WorkShiftCode = wsData.Code;
				wsRoute.WorkShiftName = wsData.Name;

				GridControlDataWSRoute wsRouteData = new GridControlDataWSRoute(wsRoute);

				if (ExistWorkShiftRouteCollision(wsRouteData) == false)
				{
					gridControlExWSRoutes.BeginUpdate();
					if (gridControlExWSRoutes.DataSource == null)
						gridControlExWSRoutes.DataSource = new BindingList<GridControlDataWSRoute>();
					((BindingList<GridControlDataWSRoute>)gridControlExWSRoutes.DataSource).Add(wsRouteData);
					gridControlExWSRoutes.EndUpdate();
				}
				else
				{
					MessageForm.Show(ResourceLoader.GetString2("WSCollide"), MessageFormType.Information);
				}
			}
			UnitParameters_Change(null, null);
		}


		private bool ExistWorkShiftRouteCollision(GridControlDataWSRoute wsData)
		{
			if (gridControlExWSRoutes.Items.Count > 0)
			{
				IList<int> l = new List<int>();
				foreach (GridControlDataWSRoute gridData in gridControlExWSRoutes.Items)
				{
					l.Add(gridData.WorkShiftCode);
				}

				IList args = new ArrayList();
				args.Add(wsData.WorkShiftCode);
				args.Add(l);

				return (bool)ServerServiceClient.GetInstance().OperatorScheduleManagerMethod("CollideWSRoute", args);
			}
			return false;
		}

		private void simpleButtonDelete_Click(object sender, EventArgs e)
		{
			if (this.gridControlExWSRoutes.SelectedItems.Count != 0)
			{
				gridControlExWSRoutes.BeginUpdate();
				((BindingList<GridControlDataWSRoute>)gridControlExWSRoutes.DataSource).Remove(gridControlExWSRoutes.SelectedItems[0] as GridControlDataWSRoute);
				gridControlExWSRoutes.EndUpdate();
                
			}
            UnitParameters_Change(null, null);
		}

		private void simpleButtonClean_Click(object sender, EventArgs e)
		{
			gridControlExWSRoutes.BeginUpdate();
			gridControlExWSRoutes.DataSource = null;
			gridControlExWSRoutes.EndUpdate();

			UnitParameters_Change(null, null);
		}

		private void simpleButtonAccept_Click(object sender, EventArgs e)
		{
			Save_Help();
		}		
	}

    public class WorkShiftRouteBaseForm : XtraFormAdmistration<UnitClientData>
    {
        public WorkShiftRouteBaseForm()
        {

        }

        public WorkShiftRouteBaseForm(AdministrationRibbonForm form)
            : base(form)
        {

        }

        public WorkShiftRouteBaseForm(AdministrationRibbonForm form, UnitClientData wsrcd, FormBehavior behavior)
            : base(form, wsrcd, behavior)
        {

        }
    }
}