using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

using SmartCadCore.Core;
using System.Collections;
using Smartmatic.SmartCad.Service;
using SmartCadCore.ClientData;
using System.ServiceModel;
using System.Threading;
using System.Reflection;
using DevExpress.XtraLayout;
using SmartCadCore.Common;
using SmartCadCore.Enums;
using SmartCadControls.Controls;
using SmartCadControls.SyncBoxes;
using SmartCadGuiCommon;

namespace SmartCadGuiCommon
{
    public partial class ApplicationPreferenceXtraForm : DevExpress.XtraEditors.XtraForm
    {
       

        private Control selectedEditValueControl;
        private ApplicationPreferenceClientData currentPreference;
        private object syncCommitted = new object();
        private Font defaultFont;
        private AdministrationRibbonForm parentForm;
        private IList specialPreferenceList;
        //private IList specialPreferenceListClonned;
        private GridControlSynBox syncBox;

        public ApplicationPreferenceClientData CurrentPreference
        {
            get
            {
                return currentPreference;
            }
            set
            {
                currentPreference = value;                
                if (value != null)
                {
                    syncBox.Sync(new GridControlDataPreference(currentPreference), CommittedDataAction.Update);
                    FormUtil.InvokeRequired(this,
                        delegate
                        {
                           this.memoEditDescription.Text = ResourceLoader.GetString2(currentPreference.Description);
                            this.textEditType.Text = ResourceLoader.GetString2(currentPreference.Type);
                            this.textEditType.ToolTip = this.textEditType.Text;
                            this.textEditModule.Text = ResourceLoader.GetString2(currentPreference.Module);
                            this.textEditModule.ToolTip = this.textEditModule.Text;
                            this.textEditUnit.Text = ResourceLoader.GetString2(currentPreference.MeasureUnit);
                            SupportedTypes selectedType = GetDataType(CurrentPreference.Type);
                            this.textEditValueRange.Text = string.Format(
                                ResourceLoader.GetString2(currentPreference.ValueRange),
                                CurrentPreference.MinValue, CurrentPreference.MaxValue);
                            SetValueToControl();
                        });

                    if (selectedEditValueControl != null && selectedEditValueControl is TextEdit)
                    {
                        if (currentPreference.Name == "FrontClientIncidentCodePrefix" || currentPreference.Name == "CCTVIncidentCodePrefix" || currentPreference.Name == "ALARMIncidentCodePrefix")
                            ((TextEdit)selectedEditValueControl).Properties.MaxLength = 20;
                    }
                }
            }
        }

        
        public ApplicationPreferenceXtraForm(AdministrationRibbonForm parentForm, ApplicationPreferenceClientData selectedPreference)
        {
            InitializeComponent();
            gridControlExPreference.Type = typeof(GridControlDataPreference);
            syncBox = new GridControlSynBox(this.gridControlExPreference);
            string preferencesSimilarIncident = "('InRatioSimilarIncidentValue', 'TelephoneNumberWeight', 'IncidentAddressWeight', 'IncidentTypeWeight')";
            specialPreferenceList = ServerServiceClient.GetInstance().SearchClientObjects(
                SmartCadHqls.GetCustomHql(false, SmartCadHqls.GetApplicationPreferenceGivenTheirNames,
                preferencesSimilarIncident));
            currentPreference = selectedPreference.Clone() as ApplicationPreferenceClientData;
            SetRequiredControl();
            if (specialPreferenceList.Contains(selectedPreference))
            {
                syncBox.Sync(specialPreferenceList);
            }
            else
            {
                syncBox.Sync(new GridControlDataPreference((ApplicationPreferenceClientData)currentPreference), 
                    CommittedDataAction.Update);
            }
            CurrentPreference = currentPreference;
            this.parentForm = parentForm;
            defaultFont = new Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular);
            (this.parentForm as AdministrationRibbonForm).AdministrationCommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(ApplicationPreferenceXtraForm_AdministrationCommittedChanges);            
        }

        void ApplicationPreferenceXtraForm_AdministrationCommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
        {
            try
            {
                if (e.Objects != null && e.Objects.Count > 0)
                {
                    lock (syncCommitted)
                    {
                        try
                        {
                            if (ServerServiceClient.GetInstance().OperatorClient == null)
                            { }
                        }
                        catch
                        { }

                        if (e.Objects[0] is ApplicationPreferenceClientData)
                        {
                            #region ApplicationPreferenceClientData
                            ApplicationPreferenceClientData preference =
                                e.Objects[0] as ApplicationPreferenceClientData;
                            int index = specialPreferenceList.IndexOf(preference);
                            if (index != -1)
                            {
                                specialPreferenceList[index] = preference;
                                FormUtil.InvokeRequired(gridControlExPreference,
                                    delegate
                                    {
                                        GridControlDataPreference gridData = new GridControlDataPreference(preference);
                                        index = gridControlExPreference.Items.IndexOf(gridData);
                                        if (index != -1)
                                        {
                                            syncBox.Sync(gridData, e.Action);
                                        }
                                    });
                            }
                            if (CurrentPreference != null && CurrentPreference.Equals(preference) && ButtonOkPressed == false)
                            {
                                MessageForm.Show(ResourceLoader.GetString2("UpdateFormApplicationPreferenceData"), MessageFormType.Warning);
                                CurrentPreference = preference;
                            }
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        private void ApplicationPreferenceXtraForm_Load(object sender, EventArgs e)
        {            
            LoadLanguage();
        }

        private void LoadLanguage()
        {
            this.Text = ResourceLoader.GetString2("ApplicationPreferenceForm_Title");
            this.layoutControlItemDescription.Text = ResourceLoader.GetString2("Description") + ":";
            this.layoutControlItemModule.Text = ResourceLoader.GetString2("Preference_Module") + ":";
            this.layoutControlItemRange.Text = ResourceLoader.GetString2("Range") + ":";
            this.layoutControlItemValue.Text = ResourceLoader.GetString2("Preference_Value") + ":";
            this.layoutControlItemType.Text = ResourceLoader.GetString2("Preference_Type") + ":";
            this.layoutControlItemUnit.Text = ResourceLoader.GetString2("MeasureUnit") + ":";
            this.layoutControlGroupInformation.Text = ResourceLoader.GetString2("PreferenceInformation");
            this.layoutControlGroupPreference.Text = ResourceLoader.GetString2("Preferences");
            this.simpleButtonAccept.Text = ResourceLoader.GetString2("Accept");
            this.simpleButtonCancel.Text = ResourceLoader.GetString2("$Message.Cancel");
        }

        private bool buttonOkPressed = false;

        private bool ButtonOkPressed
        {
            get
            {
                return buttonOkPressed;
            }
        }

        private void simpleButtonAccept_Click(object sender, EventArgs e)
        {
            try
            {
                BackgroundProcessForm processForm = new BackgroundProcessForm(this, new MethodInfo[1] { GetType().GetMethod("simpleButtonAccept_ClickHelper", BindingFlags.NonPublic | BindingFlags.Instance) }, new object[1][] { new object[0] });
                processForm.CanThrowError = true;
                processForm.ShowDialog();

                ServerServiceClient.GetInstance().RefreshClient(CurrentPreference);
            
            }
            catch (FaultException ex)
            {
                if (ex.Code != null && ex.Code.Name != null
                    && ex.Code.Name.Equals(typeof(DatabaseServerDownException).Name))
                {
                    throw new CommunicationObjectFaultedException(ex.Message, ex);
                }
                else
                {
                    ApplicationPreferenceClientData preferenceToUpdate = new ApplicationPreferenceClientData();
                    preferenceToUpdate.Code = CurrentPreference.Code;
                    ThreadPool.QueueUserWorkItem(delegate(object state)
                    {
                        try
                        {
                            AskUpdatedObjectToServer(preferenceToUpdate);
                        }
                        catch
                        { }
                    });
                    MessageForm.Show(ResourceLoader.GetString2("PreferenceUpdatedByAnotherOperator"), MessageFormType.Warning);
                }
            }
            catch (Exception ex)
            {
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, ex);
                buttonOkPressed = false;
            }
        }

        private void simpleButtonAccept_ClickHelper()
        {
            buttonOkPressed = true;
            try
            {
                if (specialPreferenceList.Contains(CurrentPreference))
                {
                    int total = 0;
                    bool all = true;
                    foreach (ApplicationPreferenceClientData pref in specialPreferenceList)
                    {
                        if (ValidatePreference(pref) == true)
                            total += Convert.ToInt32(pref.Value);
                        else
                        {
                            all = false;
                            DialogResult = DialogResult.None;
                            break;
                        }
                    }
                    if (all == true)
                    {
                        if (total == 100)
                        {
                            ServerServiceClient.GetInstance().SaveOrUpdateClientData(specialPreferenceList);
                        }
                        else
                        {
                            MessageForm.Show(ResourceLoader.GetString2("ValueDoesNotSum100"), MessageFormType.Error);
                            this.DialogResult = DialogResult.None;
                        }
                    }
                }
                else
                {
                    if (ValidatePreference(CurrentPreference) == true)
                        ServerServiceClient.GetInstance().SaveOrUpdateClientData(CurrentPreference);
                    else
                        DialogResult = DialogResult.None;
                }
            }
            catch (CommunicationObjectFaultedException)
            {
                throw;
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
                MessageForm.Show(ex.Message, ex);
            }
            buttonOkPressed = false;
        }

        private bool ValidatePreference(ApplicationPreferenceClientData preference)
        {
            bool result = true;
            try
            {
                string message = string.Empty;
                bool showErrorMessage = false;
                if (preference != null)
                {
                    SupportedTypes currentType = GetDataType(preference.Type);
                    switch (currentType)
                    {
                        case SupportedTypes.Int32:
                            {
                                int value = int.Parse(preference.Value);
                                int min = int.Parse(preference.MinValue);
                                int max = int.Parse(preference.MaxValue);
                                if (value < min || value > max)
                                {
                                    showErrorMessage = true;                                    
                                }
                            }
                            break;
                        case SupportedTypes.Double:
                            {
                                double value = double.Parse(preference.Value);
                                double min = double.Parse(preference.MinValue);
                                double max = double.Parse(preference.MaxValue);
                                if (value < min || value > max)
                                {
                                    showErrorMessage = true;
                                }
                            }
                            break;
                        case SupportedTypes.Long:
                            {
                                long value = long.Parse(preference.Value);
                                long min = long.Parse(preference.MinValue);
                                long max = long.Parse(preference.MaxValue);
                                if (value < min || value > max)
                                {
                                    showErrorMessage = true;
                                }
                            }
                            break;
                        case SupportedTypes.String:
                            break;
                        case SupportedTypes.Boolean:
                            break;
                        case SupportedTypes.DateTime:
                            break;
                        case SupportedTypes.TimeSpan:
                            break;
                        default:
                            MessageForm.Show(ResourceLoader.GetString2("NotSupportedDataType") + ": " + currentType, MessageFormType.Error);
                            break;
                    }
                    if (showErrorMessage == true)
                    {
                        message = ResourceLoader.GetString(preference.ValueRange);
                        message = string.Format(message, preference.MinValue, preference.MaxValue);
                        DialogResult dialog = MessageForm.Show(message, MessageFormType.Information);
                        result = false;
                    }
                }
            }
            catch
            { }
            return result;
        }

        private void AskUpdatedObjectToServer(ClientData clientObjectData)
        {
            if (clientObjectData != null && clientObjectData.Code > 0)
            {
                try
                {
                    ClientData objectData;
                    ConstructorInfo constructor = clientObjectData.GetType().GetConstructor(Type.EmptyTypes);
                    objectData = constructor.Invoke(null) as ClientData;
                    objectData.Code = clientObjectData.Code;
                    objectData = ServerServiceClient.GetInstance().RefreshClient(objectData);
                    if (objectData != null)
                    {
                        CommittedObjectDataCollectionEventArgs args =
                            new CommittedObjectDataCollectionEventArgs(objectData, CommittedDataAction.Update,null);
                        ApplicationPreferenceXtraForm_AdministrationCommittedChanges(null, args);
                    }
                }
                catch (Exception ex)
                {
                    SmartLogger.Print(ex);
                }
            }
        }

        private string GetValue(Control selectedControl)
        {
            string value = string.Empty;
            if (selectedControl != null && CurrentPreference != null)
            {
                SupportedTypes currentType = GetDataType(CurrentPreference.Type);
                switch (currentType)
                {
                    case SupportedTypes.Int32:
                    case SupportedTypes.Double:
                    case SupportedTypes.Long:
                    case SupportedTypes.String:
                        value = ((TextEdit)selectedControl).EditValue.ToString();
                        break;
                    case SupportedTypes.Boolean:
                        value = ((RadioGroup)selectedEditValueControl).EditValue.ToString();
                        break;
                    case SupportedTypes.DateTime:
                        value = ((DateEdit)selectedEditValueControl).EditValue.ToString();
                        break;
                    case SupportedTypes.TimeSpan:
                        value = ((TimeSpanXtraUserControl)selectedEditValueControl).TimespanValue.ToString();
                        break;
                    default:
                        MessageForm.Show(ResourceLoader.GetString2("NotSupportedDataType") + ": " + currentType, MessageFormType.Error);
                        break;
                }
            }
            return value;
        }

        private void ApplicationPreferenceXtraForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.parentForm != null)
            {
                (this.parentForm as AdministrationRibbonForm).AdministrationCommittedChanges -= new EventHandler<CommittedObjectDataCollectionEventArgs>(ApplicationPreferenceXtraForm_AdministrationCommittedChanges);                
            }
        }

        private void SetValueToControl()
        {
            if (CurrentPreference != null)
            {
                SupportedTypes currentType = GetDataType(CurrentPreference.Type);
                switch (currentType)
                {
                    case SupportedTypes.Int32:
                        ((TextEdit)selectedEditValueControl).EditValue = Convert.ToInt32(CurrentPreference.Value);
                        break;
                    case SupportedTypes.Boolean:
                        bool value = Convert.ToBoolean(CurrentPreference.Value);
                        ((RadioGroup)selectedEditValueControl).EditValue = value;
                        break;
                    case SupportedTypes.DateTime:
                        ((DateEdit)selectedEditValueControl).EditValue = Convert.ToDateTime(CurrentPreference.Value);
                        ((DateEdit)selectedEditValueControl).Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
                        ((DateEdit)selectedEditValueControl).Properties.Mask.EditMask = @"HH:mm:ss";
                        ((DateEdit)selectedEditValueControl).Properties.Mask.UseMaskAsDisplayFormat = true;
                        break;
                    case SupportedTypes.TimeSpan:
                        ((TimeSpanXtraUserControl)selectedEditValueControl).TimespanValue = TimeSpan.Parse(CurrentPreference.Value);
                        break;
                    case SupportedTypes.Double:
                        ((TextEdit)selectedEditValueControl).EditValue = Convert.ToDouble(CurrentPreference.Value);
                        break;
                    case SupportedTypes.Long:
                        ((TextEdit)selectedEditValueControl).EditValue = Convert.ToInt64(CurrentPreference.Value);
                        break;
                    case SupportedTypes.String:
                        ((TextEdit)selectedEditValueControl).EditValue = CurrentPreference.Value;
                        break;
                    default:
                        MessageForm.Show(ResourceLoader.GetString2("NotSupportedDataType") + ": " + currentType, MessageFormType.Error);
                        break;
                }
            }
        }

        private SupportedTypes GetDataType(string dataType)
        {
            SupportedTypes result = SupportedTypes.None;
            switch (dataType)
            {
                case "int":
                    result = SupportedTypes.Int32;                                        
                    break;
                case "bool":
                    result = SupportedTypes.Boolean;
                    break;
                case "DateTime":
                    result = SupportedTypes.DateTime;
                    break;
                case "TimeSpan":
                    result = SupportedTypes.TimeSpan;
                    break;
                case "double":
                    result = SupportedTypes.Double;
                    break;
                case "long":
                    result = SupportedTypes.Long;
                    break;
                case "string":
                    result = SupportedTypes.String;
                    break;
                default:
                    result = SupportedTypes.String;
                    break;
            }
            return result;
        }        

        private void SetRequiredControl()
        {
            if (CurrentPreference != null)
            {
                SupportedTypes currentType = GetDataType(CurrentPreference.Type);
                ApplicationPreferenceXtraFormConvertedLayout.BeginUpdate();
                if (this.ApplicationPreferenceXtraFormConvertedLayout.Controls.Contains(labelControlToReplace))
                {
                    layoutControlItemValue.Control = null;
                    ApplicationPreferenceXtraFormConvertedLayout.Controls.Remove(labelControlToReplace);
                }
                if (selectedEditValueControl != null && this.ApplicationPreferenceXtraFormConvertedLayout.Controls.Contains(selectedEditValueControl))
                {
                    this.ApplicationPreferenceXtraFormConvertedLayout.Controls.Remove(selectedEditValueControl);
                    selectedEditValueControl.Dispose();
                    selectedEditValueControl = null;
                }
                switch (currentType)
                {
                    case SupportedTypes.Int32:
                        selectedEditValueControl = new TextEdit();
                        //((TextEdit)selectedEditValueControl).Size = new Size(426, 24);
                        //((TextEdit)selectedEditValueControl).Location = new Point(102, 283);
                        ((TextEdit)selectedEditValueControl).Font = defaultFont;
                        ((TextEdit)selectedEditValueControl).Properties.Appearance.Font = defaultFont;
                        ((TextEdit)selectedEditValueControl).Properties.AppearanceDisabled.Font = defaultFont;
                        ((TextEdit)selectedEditValueControl).Properties.AppearanceReadOnly.Font = defaultFont;
                        ((TextEdit)selectedEditValueControl).Properties.AppearanceFocused.Font = defaultFont;
                        ((TextEdit)selectedEditValueControl).Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
                        ((TextEdit)selectedEditValueControl).Properties.Mask.IgnoreMaskBlank = true;
                        ((TextEdit)selectedEditValueControl).Properties.Mask.EditMask = "n0";
                        ((TextEdit)selectedEditValueControl).Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
                        ((TextEdit)selectedEditValueControl).EditValueChanged += new EventHandler(selectedEditValueControl_EditValueChanged);
                //        this.Controls.Add(selectedEditValueControl);
                                          break;
                    case SupportedTypes.Boolean:
                        selectedEditValueControl = new RadioGroup();
                        //((RadioGroup)selectedEditValueControl).Size = new Size(426, 22);
                        //((RadioGroup)selectedEditValueControl).Location = new Point(102, 283);
                        ((RadioGroup)selectedEditValueControl).Font = defaultFont;
                        ((RadioGroup)selectedEditValueControl).Properties.AppearanceDisabled.Font = defaultFont;
                        ((RadioGroup)selectedEditValueControl).Properties.AppearanceReadOnly.Font = defaultFont;
                        ((RadioGroup)selectedEditValueControl).Properties.AppearanceFocused.Font = defaultFont;
                        DevExpress.XtraEditors.Controls.RadioGroupItem itemActive = 
                            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, ResourceLoader.GetString2(CurrentPreference.MinValue));
                        itemActive.Value = true;
                        DevExpress.XtraEditors.Controls.RadioGroupItem itemInactive = 
                            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, ResourceLoader.GetString2(CurrentPreference.MaxValue));
                        itemInactive.Value = false;
                        ((RadioGroup)selectedEditValueControl).Properties.Items.Add(itemActive);
                        ((RadioGroup)selectedEditValueControl).Properties.Items.Add(itemInactive);
                        this.simpleButtonCalculator.Visible = false;
                        this.layoutControlItem2.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                        ((RadioGroup)selectedEditValueControl).EditValueChanged += new EventHandler(selectedEditValueControl_EditValueChanged);
                   //     this.Controls.Add(selectedEditValueControl);
                        break;
                    case SupportedTypes.DateTime:
                        selectedEditValueControl = new DateEdit();
                        //((DateEdit)selectedEditValueControl).Size = new Size(426, 22);
                        //((DateEdit)selectedEditValueControl).Location = new Point(102, 283);
                        ((DateEdit)selectedEditValueControl).Font = defaultFont;
                        ((DateEdit)selectedEditValueControl).Properties.AppearanceDisabled.Font = defaultFont;
                        ((DateEdit)selectedEditValueControl).Properties.AppearanceReadOnly.Font = defaultFont;
                        ((DateEdit)selectedEditValueControl).Properties.AppearanceFocused.Font = defaultFont;
                        ((DateEdit)selectedEditValueControl).Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
                        ((DateEdit)selectedEditValueControl).Properties.Mask.EditMask = "d";
                        ((DateEdit)selectedEditValueControl).EditValueChanged += new EventHandler(selectedEditValueControl_EditValueChanged);
                        this.simpleButtonCalculator.Visible = false;
                        this.layoutControlItem2.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                 //       this.Controls.Add(selectedEditValueControl);
                        break;
                    case SupportedTypes.TimeSpan:
                        selectedEditValueControl = new TimeSpanXtraUserControl();
                        //((TimeSpanXtraUserControl)selectedEditValueControl).Size = new Size(349, 124);
                        //((TimeSpanXtraUserControl)selectedEditValueControl).Location = new Point(99, 279);
                        ((TimeSpanXtraUserControl)selectedEditValueControl).Font = defaultFont;
                        ((TimeSpanXtraUserControl)selectedEditValueControl).Appearance.Font = defaultFont;
                        ((TimeSpanXtraUserControl)selectedEditValueControl).EditValueChanged += new EventHandler(selectedEditValueControl_EditValueChanged);
                        this.simpleButtonCalculator.Visible = false;
                        this.layoutControlItem2.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                 //       this.Controls.Add(selectedEditValueControl);
                        break;
                    case SupportedTypes.Double:
                        selectedEditValueControl = new TextEdit();
                        //((TextEdit)selectedEditValueControl).Size = new Size(426, 22);
                        //((TextEdit)selectedEditValueControl).Location = new Point(102, 283);
                        ((TextEdit)selectedEditValueControl).Font = defaultFont;
                        ((TextEdit)selectedEditValueControl).Properties.AppearanceDisabled.Font = defaultFont;
                        ((TextEdit)selectedEditValueControl).Properties.AppearanceReadOnly.Font = defaultFont;
                        ((TextEdit)selectedEditValueControl).Properties.AppearanceFocused.Font = defaultFont;
                        ((TextEdit)selectedEditValueControl).Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
                        ((TextEdit)selectedEditValueControl).Properties.Mask.EditMask = "f";
                        ((TextEdit)selectedEditValueControl).Properties.Mask.IgnoreMaskBlank = true;
                        ((TextEdit)selectedEditValueControl).Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
                        ((TextEdit)selectedEditValueControl).EditValueChanged += new EventHandler(selectedEditValueControl_EditValueChanged);
                //        this.Controls.Add(selectedEditValueControl);
                        break;
                    case SupportedTypes.Long:
                        selectedEditValueControl = new TextEdit();
                        //((TextEdit)selectedEditValueControl).Size = new Size(426, 22);
                        //((TextEdit)selectedEditValueControl).Location = new Point(102, 283);
                        ((TextEdit)selectedEditValueControl).Font = defaultFont;
                        ((TextEdit)selectedEditValueControl).Properties.AppearanceDisabled.Font = defaultFont;
                        ((TextEdit)selectedEditValueControl).Properties.AppearanceReadOnly.Font = defaultFont;
                        ((TextEdit)selectedEditValueControl).Properties.AppearanceFocused.Font = defaultFont;
                        ((TextEdit)selectedEditValueControl).Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
                        ((TextEdit)selectedEditValueControl).Properties.Mask.EditMask = "n0";
                        ((TextEdit)selectedEditValueControl).Properties.Mask.IgnoreMaskBlank = true;
                        ((TextEdit)selectedEditValueControl).Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
                        ((TextEdit)selectedEditValueControl).EditValueChanged += new EventHandler(selectedEditValueControl_EditValueChanged);
                 //       this.Controls.Add(selectedEditValueControl);
                        break;
                    case SupportedTypes.String:
                        selectedEditValueControl = new TextEdit();
                       // ((TextEdit)selectedEditValueControl).Size = new Size(426, 22);
                     //   ((TextEdit)selectedEditValueControl).Location = new Point(102, 283);
                        ((TextEdit)selectedEditValueControl).Font = defaultFont;
                        ((TextEdit)selectedEditValueControl).Properties.AppearanceDisabled.Font = defaultFont;
                        ((TextEdit)selectedEditValueControl).Properties.AppearanceReadOnly.Font = defaultFont;
                        ((TextEdit)selectedEditValueControl).Properties.AppearanceFocused.Font = defaultFont;
                        ((TextEdit)selectedEditValueControl).Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
                        ((TextEdit)selectedEditValueControl).EditValueChanged += new EventHandler(selectedEditValueControl_EditValueChanged);
                        this.simpleButtonCalculator.Visible = false;
                        this.layoutControlItem2.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                 //       this.Controls.Add(selectedEditValueControl);
                        break;
                    default:
                        MessageForm.Show(ResourceLoader.GetString2("NotSupportedDataType") + ": " + currentType , MessageFormType.Error);
                        break;
                }
                          
                ApplicationPreferenceXtraFormConvertedLayout.Controls.Add(selectedEditValueControl);
     //           ((TextEdit)selectedEditValueControl).StyleController = ApplicationPreferenceXtraFormConvertedLayout;
                this.layoutControlItemValue.Control = this.selectedEditValueControl;
                
                selectedEditValueControl.TabIndex = 12;
                if (CurrentPreference.MeasureUnit.Equals("DaysUnit") ||
                            CurrentPreference.MeasureUnit.Equals("HoursUnit") ||
                            CurrentPreference.MeasureUnit.Equals("MinutesUnit") ||
                            CurrentPreference.MeasureUnit.Equals("SecondsUnit") ||
                            CurrentPreference.MeasureUnit.Equals("MillisecondsUnit"))
                {
                    //   ((TextEdit)selectedEditValueControl).Size = new Size(401, 22);
                    this.simpleButtonCalculator.Visible = true;
                    this.layoutControlItem2.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    this.simpleButtonCalculator.ToolTip = ResourceLoader.GetString2("CalculateTimeToolTip");
                }
                else {
                    this.simpleButtonCalculator.Visible = false;
                    this.layoutControlItem2.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                }
                ApplicationPreferenceXtraFormConvertedLayout.EndUpdate();
                
            }
        }

        void selectedEditValueControl_EditValueChanged(object sender, EventArgs e)
        {
            ApplicationPreferenceClientData preferenceToUpdate = CurrentPreference;
            bool validValue = false;
            string oldValue = string.Empty;
            if (selectedEditValueControl is TextEdit)
            {
                FormUtil.InvokeRequired(this.selectedEditValueControl,
                    delegate
                    {
                        TextEdit textEdit = selectedEditValueControl as TextEdit;
                        oldValue = preferenceToUpdate.Value;
                        preferenceToUpdate.Value = textEdit.EditValue.ToString();
                        validValue = true;
                        //validValue = ValidatePreference(preferenceToUpdate);                        
                    });
            }
            else if (selectedEditValueControl is TimeSpanXtraUserControl)
            {
                FormUtil.InvokeRequired(this.selectedEditValueControl,
                    delegate
                    {
                        TimeSpanXtraUserControl timeSpanControl = selectedEditValueControl as TimeSpanXtraUserControl;
                        oldValue = preferenceToUpdate.Value;
                        preferenceToUpdate.Value = timeSpanControl.TimespanValue.ToString();
                        validValue = ValidatePreference(preferenceToUpdate);
                        if (validValue == false)
                        {
                            timeSpanControl.TimespanValue = TimeSpan.Parse(oldValue);
                        }
                    });
            }
            else if (selectedEditValueControl is DateEdit)
            {
                FormUtil.InvokeRequired(this.selectedEditValueControl,
                    delegate
                    {
                        DateEdit dateEdit = selectedEditValueControl as DateEdit;
                        oldValue = preferenceToUpdate.Value;
                        preferenceToUpdate.Value = ((DateTime)dateEdit.EditValue).ToString(ApplicationUtil.DataBaseFormattedDate);
                        validValue = ValidatePreference(preferenceToUpdate);
                        if (validValue == false)
                        {
                            dateEdit.EditValue = DateTime.Parse(oldValue);
                        }
                    });
            }
            else if (selectedEditValueControl is RadioGroup)
            {
                FormUtil.InvokeRequired(this.selectedEditValueControl,
                    delegate
                    {
                        RadioGroup radioGroup = selectedEditValueControl as RadioGroup;
                        oldValue = preferenceToUpdate.MaxValue;
                        preferenceToUpdate.Value = radioGroup.EditValue.ToString();
                        validValue = ValidatePreference(preferenceToUpdate);
                        if (validValue == false)
                        {
                            radioGroup.EditValue = bool.Parse(oldValue);
                        }
                    });
            }

            if (validValue == true)
            {
                CurrentPreference = preferenceToUpdate;
            }
            else
            {
                FormUtil.InvokeRequired(this.selectedEditValueControl,
                    delegate
                    {
                        preferenceToUpdate.Value = oldValue;
                        CurrentPreference = preferenceToUpdate;                        
                        this.selectedEditValueControl.Focus();
                        this.selectedEditValueControl.Select();
                    });
            }
        }

        private void simpleButtonCalculator_Click(object sender, EventArgs e)
        {
			TimeEditorXtraForm timeEditor = new TimeEditorXtraForm(CurrentPreference);
			DialogResult result = timeEditor.ShowDialog();
			if (result == DialogResult.OK)
			{
				TimeSpan res = timeEditor.Timespan;
				int total = 0;
				switch (CurrentPreference.MeasureUnit)
				{
					case "DaysUnit":
						total = (int)res.TotalDays;
						break;
					case "HoursUnit":
						total = (int)res.TotalHours;
						break;
					case "MinutesUnit":
						total = (int)res.TotalMinutes;
						break;
					case "SecondsUnit":
						total = (int)res.TotalSeconds;
						break;
					case "MillisecondsUnit":
						total = (int)res.TotalMilliseconds;
						break;
				}
				((TextEdit)selectedEditValueControl).EditValue = total;
			}
        }

        private void gridViewExPreferences_SelectionWillChange(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            FormUtil.InvokeRequired(gridControlExPreference,
                delegate
                {
                    if (gridControlExPreference.SelectedItems.Count > 0)
                    {
                        CurrentPreference = ((GridControlDataPreference)gridControlExPreference.SelectedItems[0]).Preference;
                    }
                });
        }
    }

}