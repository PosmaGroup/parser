using DevExpress.XtraEditors;
using SmartCadControls;
using SmartCadControls.Controls;
namespace Smartmatic.SmartCad.Gui
{
    partial class SecurityForm : XtraForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonExOK = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.gridControlExOperators = new GridControlEx();
            this.gridViewExOperators = new GridViewEx();
            this.comboBoxExStruct = new ComboBoxEx();
            this.comboBoxExStructType = new ComboBoxEx();
            this.buttonExCancel = new DevExpress.XtraEditors.SimpleButton();
            this.comboBoxCctvZone = new ComboBoxEx();
            this.buttonExApply = new DevExpress.XtraEditors.SimpleButton();
            this.buttonExRemoveAllCameras = new DevExpress.XtraEditors.SimpleButton();
            this.listBoxExAvailablesCameras = new ListBoxEx();
            this.listBoxExUnAvailablesCameras = new ListBoxEx();
            this.buttonExRemoveCamera = new DevExpress.XtraEditors.SimpleButton();
            this.buttonExAddAllCameras = new DevExpress.XtraEditors.SimpleButton();
            this.buttonExAddCamera = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroupCameras = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlCamerasAvailable = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlCameraAsociated = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlCCTVZone = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlStructType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlStruct = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroupOperators = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemOperators = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExOperators)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExOperators)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupCameras)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCamerasAvailable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCameraAsociated)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCCTVZone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlStructType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlStruct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupOperators)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemOperators)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonExOK
            // 
            this.buttonExOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonExOK.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExOK.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonExOK.Appearance.Options.UseFont = true;
            this.buttonExOK.Appearance.Options.UseForeColor = true;
            this.buttonExOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonExOK.Location = new System.Drawing.Point(251, 547);
            this.buttonExOK.Name = "buttonExOK";
            this.buttonExOK.Size = new System.Drawing.Size(82, 32);
            this.buttonExOK.StyleController = this.layoutControl1;
            this.buttonExOK.TabIndex = 3;
            this.buttonExOK.Text = "Accept";
            this.buttonExOK.Click += new System.EventHandler(this.buttonExOK_Click);
            // 
            // layoutControl1
            // 
            this.layoutControl1.AllowCustomizationMenu = false;
            this.layoutControl1.Controls.Add(this.gridControlExOperators);
            this.layoutControl1.Controls.Add(this.comboBoxExStruct);
            this.layoutControl1.Controls.Add(this.comboBoxExStructType);
            this.layoutControl1.Controls.Add(this.buttonExCancel);
            this.layoutControl1.Controls.Add(this.comboBoxCctvZone);
            this.layoutControl1.Controls.Add(this.buttonExOK);
            this.layoutControl1.Controls.Add(this.buttonExApply);
            this.layoutControl1.Controls.Add(this.buttonExRemoveAllCameras);
            this.layoutControl1.Controls.Add(this.listBoxExAvailablesCameras);
            this.layoutControl1.Controls.Add(this.listBoxExUnAvailablesCameras);
            this.layoutControl1.Controls.Add(this.buttonExRemoveCamera);
            this.layoutControl1.Controls.Add(this.buttonExAddAllCameras);
            this.layoutControl1.Controls.Add(this.buttonExAddCamera);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.ShowLayoutTreeView = false;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(507, 581);
            this.layoutControl1.TabIndex = 6;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // gridControlExOperators
            // 
            this.gridControlExOperators.EnableAutoFilter = true;
            this.gridControlExOperators.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExOperators.Location = new System.Drawing.Point(7, 27);
            this.gridControlExOperators.MainView = this.gridViewExOperators;
            this.gridControlExOperators.Name = "gridControlExOperators";
            this.gridControlExOperators.Size = new System.Drawing.Size(493, 179);
            this.gridControlExOperators.TabIndex = 9;
            this.gridControlExOperators.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExOperators});
            this.gridControlExOperators.ViewTotalRows = true;
            // 
            // gridViewExOperators
            // 
            this.gridViewExOperators.AllowFocusedRowChanged = true;
            this.gridViewExOperators.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewExOperators.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExOperators.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewExOperators.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewExOperators.Appearance.FooterPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewExOperators.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewExOperators.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewExOperators.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExOperators.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewExOperators.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewExOperators.EnablePreviewLineForFocusedRow = false;
            this.gridViewExOperators.GridControl = this.gridControlExOperators;
            this.gridViewExOperators.Name = "gridViewExOperators";
            this.gridViewExOperators.OptionsMenu.EnableFooterMenu = false;
            this.gridViewExOperators.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExOperators.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewExOperators.OptionsView.ShowFooter = true;
            this.gridViewExOperators.OptionsView.ShowGroupPanel = false;
            this.gridViewExOperators.ViewTotalRows = true;
            this.gridViewExOperators.SelectionWillChange += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewExOperators_SelectionWillChange);
            // 
            // comboBoxExStruct
            // 
            this.comboBoxExStruct.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxExStruct.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.comboBoxExStruct.FormattingEnabled = true;
            this.comboBoxExStruct.Location = new System.Drawing.Point(161, 290);
            this.comboBoxExStruct.Name = "comboBoxExStruct";
            this.comboBoxExStruct.Size = new System.Drawing.Size(339, 21);
            this.comboBoxExStruct.TabIndex = 2;
            this.comboBoxExStruct.SelectedIndexChanged += new System.EventHandler(this.comboBoxExStruct_SelectedIndexChanged);
            // 
            // comboBoxExStructType
            // 
            this.comboBoxExStructType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxExStructType.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.comboBoxExStructType.FormattingEnabled = true;
            this.comboBoxExStructType.Location = new System.Drawing.Point(161, 265);
            this.comboBoxExStructType.Name = "comboBoxExStructType";
            this.comboBoxExStructType.Size = new System.Drawing.Size(339, 21);
            this.comboBoxExStructType.TabIndex = 1;
            this.comboBoxExStructType.SelectedIndexChanged += new System.EventHandler(this.comboBoxExStructType_SelectedIndexChanged);
            // 
            // buttonExCancel
            // 
            this.buttonExCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonExCancel.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExCancel.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonExCancel.Appearance.Options.UseFont = true;
            this.buttonExCancel.Appearance.Options.UseForeColor = true;
            this.buttonExCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonExCancel.Location = new System.Drawing.Point(337, 547);
            this.buttonExCancel.Name = "buttonExCancel";
            this.buttonExCancel.Size = new System.Drawing.Size(82, 32);
            this.buttonExCancel.StyleController = this.layoutControl1;
            this.buttonExCancel.TabIndex = 4;
            this.buttonExCancel.Text = "Cancelar";
            // 
            // comboBoxCctvZone
            // 
            this.comboBoxCctvZone.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCctvZone.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.comboBoxCctvZone.FormattingEnabled = true;
            this.comboBoxCctvZone.Location = new System.Drawing.Point(161, 240);
            this.comboBoxCctvZone.Name = "comboBoxCctvZone";
            this.comboBoxCctvZone.Size = new System.Drawing.Size(339, 21);
            this.comboBoxCctvZone.TabIndex = 0;
            this.comboBoxCctvZone.SelectedIndexChanged += new System.EventHandler(this.comboBoxCctvZone_SelectedIndexChanged);
            // 
            // buttonExApply
            // 
            this.buttonExApply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonExApply.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExApply.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonExApply.Appearance.Options.UseFont = true;
            this.buttonExApply.Appearance.Options.UseForeColor = true;
            this.buttonExApply.Enabled = false;
            this.buttonExApply.Location = new System.Drawing.Point(423, 547);
            this.buttonExApply.Name = "buttonExApply";
            this.buttonExApply.Size = new System.Drawing.Size(82, 32);
            this.buttonExApply.StyleController = this.layoutControl1;
            this.buttonExApply.TabIndex = 0;
            this.buttonExApply.Click += new System.EventHandler(this.buttonExApply_Click);
            // 
            // buttonExRemoveAllCameras
            // 
            this.buttonExRemoveAllCameras.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExRemoveAllCameras.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonExRemoveAllCameras.Appearance.Options.UseFont = true;
            this.buttonExRemoveAllCameras.Appearance.Options.UseForeColor = true;
            this.buttonExRemoveAllCameras.Location = new System.Drawing.Point(232, 475);
            this.buttonExRemoveAllCameras.Name = "buttonExRemoveAllCameras";
            this.buttonExRemoveAllCameras.Size = new System.Drawing.Size(44, 29);
            this.buttonExRemoveAllCameras.StyleController = this.layoutControl1;
            this.buttonExRemoveAllCameras.TabIndex = 7;
            this.buttonExRemoveAllCameras.Text = "<<";
            this.buttonExRemoveAllCameras.Click += new System.EventHandler(this.buttonExRemoveAllCameras_Click);
            // 
            // listBoxExAvailablesCameras
            // 
            this.listBoxExAvailablesCameras.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.listBoxExAvailablesCameras.FormattingEnabled = true;
            this.listBoxExAvailablesCameras.IntegralHeight = false;
            this.listBoxExAvailablesCameras.Location = new System.Drawing.Point(7, 331);
            this.listBoxExAvailablesCameras.Name = "listBoxExAvailablesCameras";
            this.listBoxExAvailablesCameras.Size = new System.Drawing.Size(221, 207);
            this.listBoxExAvailablesCameras.TabIndex = 3;
            this.listBoxExAvailablesCameras.DoubleClick += new System.EventHandler(this.listBoxExAvailablesCameras_DoubleClick);
            this.listBoxExAvailablesCameras.Enter += new System.EventHandler(this.listBoxExAvailablesCameras_Enter);
            // 
            // listBoxExUnAvailablesCameras
            // 
            this.listBoxExUnAvailablesCameras.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.listBoxExUnAvailablesCameras.FormattingEnabled = true;
            this.listBoxExUnAvailablesCameras.IntegralHeight = false;
            this.listBoxExUnAvailablesCameras.Location = new System.Drawing.Point(280, 331);
            this.listBoxExUnAvailablesCameras.Name = "listBoxExUnAvailablesCameras";
            this.listBoxExUnAvailablesCameras.Size = new System.Drawing.Size(220, 207);
            this.listBoxExUnAvailablesCameras.TabIndex = 8;
            this.listBoxExUnAvailablesCameras.DoubleClick += new System.EventHandler(this.listBoxExUnAvailablesCameras_DoubleClick);
            this.listBoxExUnAvailablesCameras.Enter += new System.EventHandler(this.listBoxExUnAvailablesCameras_Enter);
            // 
            // buttonExRemoveCamera
            // 
            this.buttonExRemoveCamera.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExRemoveCamera.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonExRemoveCamera.Appearance.Options.UseFont = true;
            this.buttonExRemoveCamera.Appearance.Options.UseForeColor = true;
            this.buttonExRemoveCamera.Location = new System.Drawing.Point(232, 442);
            this.buttonExRemoveCamera.Name = "buttonExRemoveCamera";
            this.buttonExRemoveCamera.Size = new System.Drawing.Size(44, 29);
            this.buttonExRemoveCamera.StyleController = this.layoutControl1;
            this.buttonExRemoveCamera.TabIndex = 6;
            this.buttonExRemoveCamera.Text = "<";
            this.buttonExRemoveCamera.Click += new System.EventHandler(this.buttonExRemoveCamera_Click);
            // 
            // buttonExAddAllCameras
            // 
            this.buttonExAddAllCameras.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExAddAllCameras.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonExAddAllCameras.Appearance.Options.UseFont = true;
            this.buttonExAddAllCameras.Appearance.Options.UseForeColor = true;
            this.buttonExAddAllCameras.Location = new System.Drawing.Point(232, 409);
            this.buttonExAddAllCameras.Name = "buttonExAddAllCameras";
            this.buttonExAddAllCameras.Size = new System.Drawing.Size(44, 29);
            this.buttonExAddAllCameras.StyleController = this.layoutControl1;
            this.buttonExAddAllCameras.TabIndex = 5;
            this.buttonExAddAllCameras.Text = ">>";
            this.buttonExAddAllCameras.Click += new System.EventHandler(this.buttonExAddAllCameras_Click);
            // 
            // buttonExAddCamera
            // 
            this.buttonExAddCamera.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExAddCamera.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonExAddCamera.Appearance.Options.UseFont = true;
            this.buttonExAddCamera.Appearance.Options.UseForeColor = true;
            this.buttonExAddCamera.Location = new System.Drawing.Point(232, 376);
            this.buttonExAddCamera.Name = "buttonExAddCamera";
            this.buttonExAddCamera.Size = new System.Drawing.Size(44, 29);
            this.buttonExAddCamera.StyleController = this.layoutControl1;
            this.buttonExAddCamera.TabIndex = 4;
            this.buttonExAddCamera.Text = ">";
            this.buttonExAddCamera.Click += new System.EventHandler(this.buttonExAddCamera_Click);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupCameras,
            this.layoutControlItem11,
            this.layoutControlItem10,
            this.layoutControlItem9,
            this.emptySpaceItem1,
            this.layoutControlGroupOperators});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(507, 581);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroupCameras
            // 
            this.layoutControlGroupCameras.CustomizationFormText = "layoutControlGroupCameraAccess";
            this.layoutControlGroupCameras.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlCamerasAvailable,
            this.layoutControlCameraAsociated,
            this.layoutControlItem2,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.emptySpaceItem2,
            this.emptySpaceItem3,
            this.layoutControlCCTVZone,
            this.layoutControlStructType,
            this.layoutControlStruct});
            this.layoutControlGroupCameras.Location = new System.Drawing.Point(0, 213);
            this.layoutControlGroupCameras.Name = "layoutControlGroupCameras";
            this.layoutControlGroupCameras.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupCameras.Size = new System.Drawing.Size(507, 332);
            this.layoutControlGroupCameras.Text = "layoutControlGroupCameras";
            // 
            // layoutControlCamerasAvailable
            // 
            this.layoutControlCamerasAvailable.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlCamerasAvailable.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlCamerasAvailable.Control = this.listBoxExAvailablesCameras;
            this.layoutControlCamerasAvailable.CustomizationFormText = "layoutControlItem3";
            this.layoutControlCamerasAvailable.Location = new System.Drawing.Point(0, 75);
            this.layoutControlCamerasAvailable.MinSize = new System.Drawing.Size(129, 56);
            this.layoutControlCamerasAvailable.Name = "layoutControlCamerasAvailable";
            this.layoutControlCamerasAvailable.Size = new System.Drawing.Size(225, 227);
            this.layoutControlCamerasAvailable.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlCamerasAvailable.Text = "layoutControlCamerasAvailable";
            this.layoutControlCamerasAvailable.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlCamerasAvailable.TextSize = new System.Drawing.Size(150, 13);
            // 
            // layoutControlCameraAsociated
            // 
            this.layoutControlCameraAsociated.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlCameraAsociated.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlCameraAsociated.Control = this.listBoxExUnAvailablesCameras;
            this.layoutControlCameraAsociated.CustomizationFormText = "layoutControlItem4";
            this.layoutControlCameraAsociated.Location = new System.Drawing.Point(273, 75);
            this.layoutControlCameraAsociated.MinSize = new System.Drawing.Size(129, 56);
            this.layoutControlCameraAsociated.Name = "layoutControlCameraAsociated";
            this.layoutControlCameraAsociated.Size = new System.Drawing.Size(224, 227);
            this.layoutControlCameraAsociated.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlCameraAsociated.Text = "layoutControlCameraAsociated";
            this.layoutControlCameraAsociated.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlCameraAsociated.TextSize = new System.Drawing.Size(150, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.buttonExAddCamera;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(225, 136);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(48, 33);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(48, 33);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(48, 33);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.buttonExAddAllCameras;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(225, 169);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(48, 33);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(48, 33);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(48, 33);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.buttonExRemoveCamera;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(225, 202);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(48, 33);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(48, 33);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(48, 33);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.buttonExRemoveAllCameras;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(225, 235);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(48, 33);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(48, 33);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(48, 33);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(225, 75);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(48, 0);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(48, 10);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(48, 61);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(225, 268);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(48, 34);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlCCTVZone
            // 
            this.layoutControlCCTVZone.Control = this.comboBoxCctvZone;
            this.layoutControlCCTVZone.CustomizationFormText = "layoutControlItem8";
            this.layoutControlCCTVZone.Location = new System.Drawing.Point(0, 0);
            this.layoutControlCCTVZone.Name = "layoutControlCCTVZone";
            this.layoutControlCCTVZone.Size = new System.Drawing.Size(497, 25);
            this.layoutControlCCTVZone.Text = "layoutControlCCTVZone";
            this.layoutControlCCTVZone.TextSize = new System.Drawing.Size(150, 13);
            // 
            // layoutControlStructType
            // 
            this.layoutControlStructType.Control = this.comboBoxExStructType;
            this.layoutControlStructType.CustomizationFormText = "layoutControlItem12";
            this.layoutControlStructType.Location = new System.Drawing.Point(0, 25);
            this.layoutControlStructType.Name = "layoutControlStructType";
            this.layoutControlStructType.Size = new System.Drawing.Size(497, 25);
            this.layoutControlStructType.Text = "layoutControlStructType";
            this.layoutControlStructType.TextSize = new System.Drawing.Size(150, 13);
            // 
            // layoutControlStruct
            // 
            this.layoutControlStruct.Control = this.comboBoxExStruct;
            this.layoutControlStruct.CustomizationFormText = "layoutControlItem13";
            this.layoutControlStruct.Location = new System.Drawing.Point(0, 50);
            this.layoutControlStruct.Name = "layoutControlStruct";
            this.layoutControlStruct.Size = new System.Drawing.Size(497, 25);
            this.layoutControlStruct.Text = "layoutControlStruct";
            this.layoutControlStruct.TextSize = new System.Drawing.Size(150, 13);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.buttonExApply;
            this.layoutControlItem11.CustomizationFormText = "layoutControlItem11";
            this.layoutControlItem11.Location = new System.Drawing.Point(421, 545);
            this.layoutControlItem11.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Text = "layoutControlItem11";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextToControlDistance = 0;
            this.layoutControlItem11.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.buttonExCancel;
            this.layoutControlItem10.CustomizationFormText = "layoutControlItem10";
            this.layoutControlItem10.Location = new System.Drawing.Point(335, 545);
            this.layoutControlItem10.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem10.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem10.Text = "layoutControlItem10";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextToControlDistance = 0;
            this.layoutControlItem10.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.buttonExOK;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(249, 545);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Text = "layoutControlItem9";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextToControlDistance = 0;
            this.layoutControlItem9.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 545);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(110, 30);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(249, 36);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroupOperators
            // 
            this.layoutControlGroupOperators.CustomizationFormText = "layoutControlGroupOperators";
            this.layoutControlGroupOperators.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemOperators});
            this.layoutControlGroupOperators.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupOperators.Name = "layoutControlGroupOperators";
            this.layoutControlGroupOperators.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupOperators.Size = new System.Drawing.Size(507, 213);
            this.layoutControlGroupOperators.Text = "layoutControlGroupOperators";
            // 
            // layoutControlItemOperators
            // 
            this.layoutControlItemOperators.Control = this.gridControlExOperators;
            this.layoutControlItemOperators.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItemOperators.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemOperators.Name = "layoutControlItemOperators";
            this.layoutControlItemOperators.Size = new System.Drawing.Size(497, 183);
            this.layoutControlItemOperators.Text = "layoutControlItemOperators";
            this.layoutControlItemOperators.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemOperators.TextToControlDistance = 0;
            this.layoutControlItemOperators.TextVisible = false;
            // 
            // SecurityForm
            // 
            this.AcceptButton = this.buttonExOK;
            this.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonExCancel;
            this.ClientSize = new System.Drawing.Size(507, 581);
            this.Controls.Add(this.layoutControl1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(515, 615);
            this.Name = "SecurityForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SecurityForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SecurityForm_FormClosing);
            this.Load += new System.EventHandler(this.SecurityForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExOperators)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExOperators)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupCameras)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCamerasAvailable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCameraAsociated)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCCTVZone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlStructType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlStruct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupOperators)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemOperators)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton buttonExOK;
        private DevExpress.XtraEditors.SimpleButton buttonExCancel;
        private DevExpress.XtraEditors.SimpleButton buttonExApply;
        private ComboBoxEx comboBoxExStructType;
        private ComboBoxEx comboBoxExStruct;
        private ComboBoxEx comboBoxCctvZone;
        private DevExpress.XtraEditors.SimpleButton buttonExRemoveAllCameras;
        private DevExpress.XtraEditors.SimpleButton buttonExRemoveCamera;
        private DevExpress.XtraEditors.SimpleButton buttonExAddAllCameras;
        private DevExpress.XtraEditors.SimpleButton buttonExAddCamera;
        private ListBoxEx listBoxExAvailablesCameras;
        private ListBoxEx listBoxExUnAvailablesCameras;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupCameras;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlCamerasAvailable;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlCameraAsociated;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlCCTVZone;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlStructType;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlStruct;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private GridControlEx gridControlExOperators;
        private GridViewEx gridViewExOperators;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemOperators;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupOperators;
    }
}