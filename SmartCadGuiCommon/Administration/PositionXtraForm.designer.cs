namespace SmartCadGuiCommon
{
    partial class PositionXtraForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PositionXtraForm));
            this.comboBoxEditDepartmentTypes = new DevExpress.XtraEditors.ComboBoxEdit();
            this.PositionXtraFormConvertedLayout = new DevExpress.XtraLayout.LayoutControl();
            this.memoEditDescription = new DevExpress.XtraEditors.MemoEdit();
            this.simpleButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonAccept = new DevExpress.XtraEditors.SimpleButton();
            this.textEditPositionName = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupPosition = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemDepartment = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemName = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditDepartmentTypes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PositionXtraFormConvertedLayout)).BeginInit();
            this.PositionXtraFormConvertedLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPositionName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupPosition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDepartment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBoxEditDepartmentTypes
            // 
            this.comboBoxEditDepartmentTypes.Location = new System.Drawing.Point(155, 50);
            this.comboBoxEditDepartmentTypes.Name = "comboBoxEditDepartmentTypes";
            this.comboBoxEditDepartmentTypes.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxEditDepartmentTypes.Properties.Appearance.Options.UseFont = true;
            this.comboBoxEditDepartmentTypes.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditDepartmentTypes.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEditDepartmentTypes.Size = new System.Drawing.Size(315, 19);
            this.comboBoxEditDepartmentTypes.StyleController = this.PositionXtraFormConvertedLayout;
            this.comboBoxEditDepartmentTypes.TabIndex = 3;
            this.comboBoxEditDepartmentTypes.SelectedIndexChanged += new System.EventHandler(this.EnableButtons);
            // 
            // PositionXtraFormConvertedLayout
            // 
            this.PositionXtraFormConvertedLayout.AllowCustomizationMenu = false;
            this.PositionXtraFormConvertedLayout.Controls.Add(this.memoEditDescription);
            this.PositionXtraFormConvertedLayout.Controls.Add(this.comboBoxEditDepartmentTypes);
            this.PositionXtraFormConvertedLayout.Controls.Add(this.simpleButtonCancel);
            this.PositionXtraFormConvertedLayout.Controls.Add(this.simpleButtonAccept);
            this.PositionXtraFormConvertedLayout.Controls.Add(this.textEditPositionName);
            this.PositionXtraFormConvertedLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PositionXtraFormConvertedLayout.Location = new System.Drawing.Point(0, 0);
            this.PositionXtraFormConvertedLayout.Margin = new System.Windows.Forms.Padding(2);
            this.PositionXtraFormConvertedLayout.Name = "PositionXtraFormConvertedLayout";
            this.PositionXtraFormConvertedLayout.OptionsCustomizationForm.ShowLayoutTreeView = false;
            this.PositionXtraFormConvertedLayout.Root = this.layoutControlGroup1;
            this.PositionXtraFormConvertedLayout.Size = new System.Drawing.Size(477, 203);
            this.PositionXtraFormConvertedLayout.TabIndex = 8;
            // 
            // memoEditDescription
            // 
            this.memoEditDescription.Location = new System.Drawing.Point(155, 73);
            this.memoEditDescription.Margin = new System.Windows.Forms.Padding(2);
            this.memoEditDescription.Name = "memoEditDescription";
            this.memoEditDescription.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoEditDescription.Properties.Appearance.Options.UseFont = true;
            this.memoEditDescription.Properties.MaxLength = 200;
            this.memoEditDescription.Size = new System.Drawing.Size(315, 87);
            this.memoEditDescription.StyleController = this.PositionXtraFormConvertedLayout;
            this.memoEditDescription.TabIndex = 5;
            this.memoEditDescription.TextChanged += new System.EventHandler(this.EnableButtons);
            // 
            // simpleButtonCancel
            // 
            this.simpleButtonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonCancel.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButtonCancel.Appearance.Options.UseFont = true;
            this.simpleButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonCancel.Location = new System.Drawing.Point(393, 169);
            this.simpleButtonCancel.Name = "simpleButtonCancel";
            this.simpleButtonCancel.Size = new System.Drawing.Size(82, 32);
            this.simpleButtonCancel.StyleController = this.PositionXtraFormConvertedLayout;
            this.simpleButtonCancel.TabIndex = 7;
            this.simpleButtonCancel.Text = "Cancelar";
            // 
            // simpleButtonAccept
            // 
            this.simpleButtonAccept.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonAccept.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButtonAccept.Appearance.Options.UseFont = true;
            this.simpleButtonAccept.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.simpleButtonAccept.Location = new System.Drawing.Point(307, 169);
            this.simpleButtonAccept.Name = "simpleButtonAccept";
            this.simpleButtonAccept.Size = new System.Drawing.Size(82, 32);
            this.simpleButtonAccept.StyleController = this.PositionXtraFormConvertedLayout;
            this.simpleButtonAccept.TabIndex = 6;
            this.simpleButtonAccept.Text = "Accept";
            this.simpleButtonAccept.Click += new System.EventHandler(this.simpleButtonAccept_Click);
            // 
            // textEditPositionName
            // 
            this.textEditPositionName.Location = new System.Drawing.Point(155, 27);
            this.textEditPositionName.Name = "textEditPositionName";
            this.textEditPositionName.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEditPositionName.Properties.Appearance.Options.UseFont = true;
            this.textEditPositionName.Size = new System.Drawing.Size(315, 19);
            this.textEditPositionName.StyleController = this.PositionXtraFormConvertedLayout;
            this.textEditPositionName.TabIndex = 1;
            this.textEditPositionName.TextChanged += new System.EventHandler(this.EnableButtons);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlGroupPosition,
            this.emptySpaceItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(477, 203);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.simpleButtonCancel;
            this.layoutControlItem3.CustomizationFormText = "simpleButtonCancelitem";
            this.layoutControlItem3.Location = new System.Drawing.Point(391, 167);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem3.Name = "simpleButtonCancelitem";
            this.layoutControlItem3.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "simpleButtonCancelitem";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.simpleButtonAccept;
            this.layoutControlItem4.CustomizationFormText = "simpleButtonAcceptitem";
            this.layoutControlItem4.Location = new System.Drawing.Point(305, 167);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem4.Name = "simpleButtonAcceptitem";
            this.layoutControlItem4.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "simpleButtonAcceptitem";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlGroupPosition
            // 
            this.layoutControlGroupPosition.CustomizationFormText = "layoutControlGroupPosition";
            this.layoutControlGroupPosition.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemDescription,
            this.layoutControlItemDepartment,
            this.layoutControlItemName});
            this.layoutControlGroupPosition.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupPosition.Name = "layoutControlGroupPosition";
            this.layoutControlGroupPosition.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupPosition.Size = new System.Drawing.Size(477, 167);
            this.layoutControlGroupPosition.Text = "layoutControlGroupPosition";
            // 
            // layoutControlItemDescription
            // 
            this.layoutControlItemDescription.Control = this.memoEditDescription;
            this.layoutControlItemDescription.CustomizationFormText = "layoutControlItemDescription";
            this.layoutControlItemDescription.Location = new System.Drawing.Point(0, 46);
            this.layoutControlItemDescription.Name = "layoutControlItemDescription";
            this.layoutControlItemDescription.Size = new System.Drawing.Size(467, 91);
            this.layoutControlItemDescription.Text = "layoutControlItemDescription";
            this.layoutControlItemDescription.TextSize = new System.Drawing.Size(144, 13);
            // 
            // layoutControlItemDepartment
            // 
            this.layoutControlItemDepartment.Control = this.comboBoxEditDepartmentTypes;
            this.layoutControlItemDepartment.CustomizationFormText = "layoutControlItemDepartment";
            this.layoutControlItemDepartment.Location = new System.Drawing.Point(0, 23);
            this.layoutControlItemDepartment.Name = "layoutControlItemDepartment";
            this.layoutControlItemDepartment.Size = new System.Drawing.Size(467, 23);
            this.layoutControlItemDepartment.Text = "layoutControlItemDepartment";
            this.layoutControlItemDepartment.TextSize = new System.Drawing.Size(144, 13);
            // 
            // layoutControlItemName
            // 
            this.layoutControlItemName.Control = this.textEditPositionName;
            this.layoutControlItemName.CustomizationFormText = "layoutControlItemName";
            this.layoutControlItemName.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemName.Name = "layoutControlItemName";
            this.layoutControlItemName.Size = new System.Drawing.Size(467, 23);
            this.layoutControlItemName.Text = "layoutControlItemName";
            this.layoutControlItemName.TextSize = new System.Drawing.Size(144, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 167);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(110, 30);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(305, 36);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // PositionXtraForm
            // 
            this.AcceptButton = this.simpleButtonAccept;
            this.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.simpleButtonCancel;
            this.ClientSize = new System.Drawing.Size(477, 203);
            this.Controls.Add(this.PositionXtraFormConvertedLayout);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(485, 230);
            this.Name = "PositionXtraForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PositionXtraForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PositionXtraForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditDepartmentTypes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PositionXtraFormConvertedLayout)).EndInit();
            this.PositionXtraFormConvertedLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.memoEditDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPositionName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupPosition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDepartment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditDepartmentTypes;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCancel;
        private DevExpress.XtraEditors.SimpleButton simpleButtonAccept;
        private DevExpress.XtraEditors.TextEdit textEditPositionName;
        private DevExpress.XtraEditors.MemoEdit memoEditDescription;
        private DevExpress.XtraLayout.LayoutControl PositionXtraFormConvertedLayout;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupPosition;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDescription;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDepartment;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemName;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
    }
}