using System;
using System.Collections.Generic;
using System.Text;
using DevExpress.XtraEditors;
using SmartCadControls.Controls;
using SmartCadControls;

namespace Smartmatic.SmartCad.Gui
{
    public partial class RoleForm : XtraForm
    {
		private DevExpress.XtraEditors.SimpleButton buttonExOK;
		private DevExpress.XtraEditors.SimpleButton buttonExCancel;
        private TextBoxEx textBoxDescription;
        private TextBoxEx textBoxName;
        private DataGridEx dataGridExProfiles;
        //private System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle;
        //private System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            DatagGridDefaultGroup datagGridDefaultGroup1 = new DatagGridDefaultGroup();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.buttonExOK = new DevExpress.XtraEditors.SimpleButton();
            this.FormBaseExConvertedLayout = new DevExpress.XtraLayout.LayoutControl();
            this.buttonExCancel = new DevExpress.XtraEditors.SimpleButton();
            this.dataGridExProfiles = new DataGridEx();
            this.textBoxDescription = new TextBoxEx();
            this.textBoxName = new TextBoxEx();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupProfiles = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupData = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlDesc = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlName = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutConverter1 = new DevExpress.XtraLayout.Converter.LayoutConverter(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.FormBaseExConvertedLayout)).BeginInit();
            this.FormBaseExConvertedLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridExProfiles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupProfiles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonExOK
            // 
            this.buttonExOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonExOK.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExOK.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonExOK.Appearance.Options.UseFont = true;
            this.buttonExOK.Appearance.Options.UseForeColor = true;
            this.buttonExOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonExOK.Enabled = false;
            this.buttonExOK.Location = new System.Drawing.Point(372, 420);
            this.buttonExOK.Name = "buttonExOK";
            this.buttonExOK.Size = new System.Drawing.Size(82, 32);
            this.buttonExOK.StyleController = this.FormBaseExConvertedLayout;
            this.buttonExOK.TabIndex = 2;
            this.buttonExOK.Text = "Accept";
            this.buttonExOK.Click += new System.EventHandler(this.buttonExOK_Click);
            // 
            // FormBaseExConvertedLayout
            // 
            this.FormBaseExConvertedLayout.AllowCustomizationMenu = false;
            this.FormBaseExConvertedLayout.Controls.Add(this.buttonExOK);
            this.FormBaseExConvertedLayout.Controls.Add(this.buttonExCancel);
            this.FormBaseExConvertedLayout.Controls.Add(this.dataGridExProfiles);
            this.FormBaseExConvertedLayout.Controls.Add(this.textBoxDescription);
            this.FormBaseExConvertedLayout.Controls.Add(this.textBoxName);
            this.FormBaseExConvertedLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FormBaseExConvertedLayout.Location = new System.Drawing.Point(0, 0);
            this.FormBaseExConvertedLayout.Name = "FormBaseExConvertedLayout";
            this.FormBaseExConvertedLayout.OptionsCustomizationForm.ShowLayoutTreeView = false;
            this.FormBaseExConvertedLayout.Root = this.layoutControlGroup1;
            this.FormBaseExConvertedLayout.Size = new System.Drawing.Size(542, 454);
            this.FormBaseExConvertedLayout.TabIndex = 5;
            // 
            // buttonExCancel
            // 
            this.buttonExCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonExCancel.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExCancel.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonExCancel.Appearance.Options.UseFont = true;
            this.buttonExCancel.Appearance.Options.UseForeColor = true;
            this.buttonExCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonExCancel.Location = new System.Drawing.Point(458, 420);
            this.buttonExCancel.Name = "buttonExCancel";
            this.buttonExCancel.Size = new System.Drawing.Size(82, 32);
            this.buttonExCancel.StyleController = this.FormBaseExConvertedLayout;
            this.buttonExCancel.TabIndex = 3;
            this.buttonExCancel.Text = "Cancelar";
            // 
            // dataGridExProfiles
            // 
            this.dataGridExProfiles.AllowDrop = true;
            this.dataGridExProfiles.AllowEditing = false;
            this.dataGridExProfiles.AllowUserToAddRows = false;
            this.dataGridExProfiles.AllowUserToDeleteRows = false;
            this.dataGridExProfiles.AllowUserToOrderColumns = true;
            this.dataGridExProfiles.AllowUserToResizeRows = false;
            this.dataGridExProfiles.AllowUserToSortColumns = true;
            this.dataGridExProfiles.BackgroundColor = System.Drawing.Color.White;
            this.dataGridExProfiles.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dataGridExProfiles.ColumnHeadersBackColor = System.Drawing.Color.Empty;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.ButtonFace;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Tahoma", 8.25F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridExProfiles.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridExProfiles.ColumnHeadersHeight = 22;
            this.dataGridExProfiles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 8.25F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(31)))), ((int)(((byte)(53)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridExProfiles.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridExProfiles.Editing = false;
            this.dataGridExProfiles.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dataGridExProfiles.EnabledSelectionHandler = true;
            this.dataGridExProfiles.EnableHeadersVisualStyles = false;
            this.dataGridExProfiles.EnableSystemShortCuts = false;
            this.dataGridExProfiles.Grouping = false;
            datagGridDefaultGroup1.Collapsed = false;
            datagGridDefaultGroup1.Column = null;
            datagGridDefaultGroup1.Height = 34;
            datagGridDefaultGroup1.ItemCount = 0;
            datagGridDefaultGroup1.Text = "";
            datagGridDefaultGroup1.Value = null;
            this.dataGridExProfiles.GroupTemplate = datagGridDefaultGroup1;
            this.dataGridExProfiles.Location = new System.Drawing.Point(7, 176);
            this.dataGridExProfiles.MultiSelect = false;
            this.dataGridExProfiles.Name = "dataGridExProfiles";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Tahoma", 8.25F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridExProfiles.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridExProfiles.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridExProfiles.SelectedItemBackColor = System.Drawing.Color.Empty;
            this.dataGridExProfiles.SelectedItemForeColor = System.Drawing.Color.Empty;
            this.dataGridExProfiles.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridExProfiles.Size = new System.Drawing.Size(528, 235);
            this.dataGridExProfiles.SortedColumnColor = System.Drawing.Color.WhiteSmoke;
            this.dataGridExProfiles.StandardTab = true;
            this.dataGridExProfiles.TabIndex = 0;
            this.dataGridExProfiles.Type = null;
            this.dataGridExProfiles.VirtualMode = true;
            this.dataGridExProfiles.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridExProfiles_CellMouseUp);
            this.dataGridExProfiles.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dataGridExProfiles_KeyDown);
            this.dataGridExProfiles.KeyUp += new System.Windows.Forms.KeyEventHandler(this.dataGridExProfiles_KeyUp);
            this.dataGridExProfiles.LostFocus += new System.EventHandler(this.dataGridExProfiles_LostFocus);
            // 
            // textBoxDescription
            // 
            this.textBoxDescription.AcceptsReturn = true;
            this.textBoxDescription.AllowsLetters = true;
            this.textBoxDescription.AllowsNumbers = true;
            this.textBoxDescription.AllowsPunctuation = true;
            this.textBoxDescription.AllowsSeparators = true;
            this.textBoxDescription.AllowsSymbols = true;
            this.textBoxDescription.AllowsWhiteSpaces = true;
            this.textBoxDescription.ExtraAllowedChars = "";
            this.textBoxDescription.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxDescription.Location = new System.Drawing.Point(103, 58);
            this.textBoxDescription.MaxLength = 200;
            this.textBoxDescription.Multiline = true;
            this.textBoxDescription.Name = "textBoxDescription";
            this.textBoxDescription.NonAllowedCharacters = "";
            this.textBoxDescription.RegularExpresion = "";
            this.textBoxDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxDescription.Size = new System.Drawing.Size(432, 84);
            this.textBoxDescription.TabIndex = 1;
            // 
            // textBoxName
            // 
            this.textBoxName.AllowsLetters = true;
            this.textBoxName.AllowsNumbers = true;
            this.textBoxName.AllowsPunctuation = true;
            this.textBoxName.AllowsSeparators = true;
            this.textBoxName.AllowsSymbols = true;
            this.textBoxName.AllowsWhiteSpaces = true;
            this.textBoxName.ExtraAllowedChars = "";
            this.textBoxName.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxName.Location = new System.Drawing.Point(103, 27);
            this.textBoxName.MaxLength = 40;
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.NonAllowedCharacters = "";
            this.textBoxName.RegularExpresion = "";
            this.textBoxName.Size = new System.Drawing.Size(432, 27);
            this.textBoxName.TabIndex = 0;
            this.textBoxName.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textBoxName_KeyUp);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlGroupProfiles,
            this.layoutControlGroupData,
            this.emptySpaceItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(542, 454);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.buttonExOK;
            this.layoutControlItem1.CustomizationFormText = "buttonExOKitem";
            this.layoutControlItem1.Location = new System.Drawing.Point(370, 418);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem1.Name = "buttonExOKitem";
            this.layoutControlItem1.ShowInCustomizationForm = false;
            this.layoutControlItem1.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "buttonExOKitem";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.buttonExCancel;
            this.layoutControlItem2.CustomizationFormText = "buttonExCancelitem";
            this.layoutControlItem2.Location = new System.Drawing.Point(456, 418);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem2.Name = "buttonExCancelitem";
            this.layoutControlItem2.ShowInCustomizationForm = false;
            this.layoutControlItem2.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "buttonExCancelitem";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlGroupProfiles
            // 
            this.layoutControlGroupProfiles.CustomizationFormText = "Perfiles";
            this.layoutControlGroupProfiles.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4});
            this.layoutControlGroupProfiles.Location = new System.Drawing.Point(0, 149);
            this.layoutControlGroupProfiles.Name = "layoutControlGroupProfiles";
            this.layoutControlGroupProfiles.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupProfiles.ShowInCustomizationForm = false;
            this.layoutControlGroupProfiles.Size = new System.Drawing.Size(542, 269);
            this.layoutControlGroupProfiles.Text = "Perfiles";
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.dataGridExProfiles;
            this.layoutControlItem4.CustomizationFormText = "dataGridExProfilesitem";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "dataGridExProfilesitem";
            this.layoutControlItem4.Size = new System.Drawing.Size(532, 239);
            this.layoutControlItem4.Text = "dataGridExProfilesitem";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlGroupData
            // 
            this.layoutControlGroupData.CustomizationFormText = "Datos del rol";
            this.layoutControlGroupData.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlDesc,
            this.layoutControlName});
            this.layoutControlGroupData.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupData.Name = "layoutControlGroupData";
            this.layoutControlGroupData.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupData.ShowInCustomizationForm = false;
            this.layoutControlGroupData.Size = new System.Drawing.Size(542, 149);
            this.layoutControlGroupData.Text = "Datos del rol";
            // 
            // layoutControlDesc
            // 
            this.layoutControlDesc.Control = this.textBoxDescription;
            this.layoutControlDesc.CustomizationFormText = "Descripcion:";
            this.layoutControlDesc.Location = new System.Drawing.Point(0, 31);
            this.layoutControlDesc.MinSize = new System.Drawing.Size(94, 31);
            this.layoutControlDesc.Name = "layoutControlDesc";
            this.layoutControlDesc.Size = new System.Drawing.Size(532, 88);
            this.layoutControlDesc.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlDesc.Text = "layoutControlDesc";
            this.layoutControlDesc.TextSize = new System.Drawing.Size(92, 13);
            // 
            // layoutControlName
            // 
            this.layoutControlName.Control = this.textBoxName;
            this.layoutControlName.CustomizationFormText = "Nombre: *";
            this.layoutControlName.Location = new System.Drawing.Point(0, 0);
            this.layoutControlName.MaxSize = new System.Drawing.Size(0, 31);
            this.layoutControlName.MinSize = new System.Drawing.Size(94, 31);
            this.layoutControlName.Name = "layoutControlName";
            this.layoutControlName.Size = new System.Drawing.Size(532, 31);
            this.layoutControlName.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlName.Text = "layoutControlName";
            this.layoutControlName.TextSize = new System.Drawing.Size(92, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 418);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(110, 30);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.ShowInCustomizationForm = false;
            this.emptySpaceItem1.Size = new System.Drawing.Size(370, 36);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // RoleForm
            // 
            this.AcceptButton = this.buttonExOK;
            this.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonExCancel;
            this.ClientSize = new System.Drawing.Size(542, 454);
            this.Controls.Add(this.FormBaseExConvertedLayout);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(550, 481);
            this.Name = "RoleForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.RoleForm_FormClosing);
            this.Load += new System.EventHandler(this.RoleForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.FormBaseExConvertedLayout)).EndInit();
            this.FormBaseExConvertedLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridExProfiles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupProfiles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion

        private DevExpress.XtraLayout.LayoutControl FormBaseExConvertedLayout;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupProfiles;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupData;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlDesc;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlName;
        private DevExpress.XtraLayout.Converter.LayoutConverter layoutConverter1;
        private System.ComponentModel.IContainer components;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;

    }
}
