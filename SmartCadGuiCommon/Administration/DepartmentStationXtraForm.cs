using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Smartmatic.SmartCad.Service;
using SmartCadCore.ClientData;
using SmartCadCore.Core;
using System.Collections;
using System.Reflection;
using System.Linq;

using SmartCadCore.Common;
using SmartCadControls.SyncBoxes;
using SmartCadCore.Enums;
using SmartCadGuiCommon.Services;
using SmartCadGuiCommon.Controls;


namespace SmartCadGuiCommon
{
    public partial class DepartmentStationXtraForm : DevExpress.XtraEditors.XtraForm
    {
        private DepartmentStationClientData selectedDepartmentStation;
        private AdministrationRibbonForm parentForm;
        private object syncCommitted = new object();
        private ArrayList departmentTypes;
        private ArrayList zones;
        private ArrayList units;
        private ArrayList officers;
        private GridControlSynBox syncBoxUnits;
        private GridControlSynBox syncBoxOfficers;
        private bool internalUpdate = false;
        private DepartmentTypeClientData selectedType;
        private DepartmentZoneClientData selectedZone;
        private bool zoneWasModified;
        private IList originalAddress;
        private bool reloadSelectedData = true;
        IList originalPoints = new ArrayList();

        public DepartmentStationClientData SelectedDepartmentStation
        {
            get
            {
                return selectedDepartmentStation;
            }
            set
            {
                selectedDepartmentStation = value;
                if (Behavior == FormBehavior.Edit)
                {
                    FormUtil.InvokeRequired(this,
                        delegate
                        {
                            this.Size = new Size(524, 622);
                            this.MinimumSize = new Size(524, 622);
                        });
                    BackgroundProcessForm processForm = new BackgroundProcessForm(this,
                    new MethodInfo[1]
                    { 
                        GetType().GetMethod("LoadLists", BindingFlags.NonPublic | BindingFlags.Instance) 
                    },
                        new object[1][] 
                    { 
                        new object[0] { } 
                    });
                    processForm.CanThrowError = true;
                    processForm.ShowDialog();

                    FormUtil.InvokeRequired(this,
                        delegate
                        {
                            this.textEditDepartmentStationName.Text = selectedDepartmentStation.Name;

                            DepartmentTypeClientData department = new DepartmentTypeClientData();
                            department.Code = selectedDepartmentStation.DepartamentTypeCode;
                            int index = this.comboBoxEditDepartmentTypes.Properties.Items.IndexOf(department);
                            if (index != -1)
                                comboBoxEditDepartmentTypes.SelectedIndex = index;
                            
                            index = this.comboBoxEditDepartmentZones.Properties.Items.IndexOf(selectedDepartmentStation.DepartmentZone);
                            if (index != -1)
                                comboBoxEditDepartmentZones.SelectedIndex = index;



                            this.textEditCustomCode.Text = selectedDepartmentStation.CustomCode;
                            this.textEditTelephone.Text = selectedDepartmentStation.Telephone;
                            this.textEditCustomCode.Properties.AppearanceReadOnly.BackColor = Color.White;
                            this.textEditCustomCode.Properties.AppearanceReadOnly.BackColor2 = Color.White;
                            if (selectedDepartmentStation.DepartmentStationAddress != null)
                            {
                                this.gridControlExCoordinates.SetDataSource(selectedDepartmentStation.DepartmentStationAddress);
                                originalPoints = selectedDepartmentStation.DepartmentStationAddress;
                            }
                        });
                    if (units.Count > 0)
                    {
                        syncBoxUnits.Sync(units);
                    }
                    if (officers.Count > 0)
                    {
                        syncBoxOfficers.Sync(officers);
                    }
                    syncBoxOfficers.Sync(officers);
                }
                else
                {
                    FormUtil.InvokeRequired(this,
                        delegate
                        {
                            this.layoutControlGroupOfficers.HideToCustomization();
                            this.layoutControlGroupUnits.HideToCustomization();
                            this.Size = new Size(483, 350);
                            this.MinimumSize = new Size(483, 350);
                        });
                }
                internalUpdate = false;
            }
        }

        public FormBehavior Behavior { get; set; }

        public bool MapIsOpen { get; set; }

        public DepartmentStationXtraForm(AdministrationRibbonForm parentForm, DepartmentStationClientData departmentStation, FormBehavior behavior)
        {
            var applications = ServerServiceClient.GetInstance().GetActiveApplications();
            InitializeComponent();            
            ConfigureGridControls();
            this.Behavior = behavior;
            LoadDepartmentTypes();
            LoadDepartmentZones();
            if (departmentStation != null)
            {
                originalAddress = departmentStation.DepartmentStationAddress;
            }
            this.SelectedDepartmentStation = departmentStation;
            EnableButtons(null, EventArgs.Empty);

            LoadLanguage();
            this.parentForm = parentForm;
            this.parentForm.AdministrationCommittedChanges += DepartmentStationXtraForm_AdministrationCommittedChanges;
            ApplicationServerService.GisAction += serverServiceClient_GisAction;
            if (!applications.Contains(SmartCadApplications.SmartCadMap))
            {
                layoutControlItem1.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                layoutControlItem5.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                emptySpaceItem4.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                layoutControlItemMaps.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                if (departmentStation == null)
                {
                    this.MinimumSize = new Size(480, 270);
                    this.Size = new Size(480, 270);
                }             
            }

        }

        private void DepartmentStationXtraForm_Load(object sender, EventArgs e)
        {
            ApplicationServiceClient.Current(UserApplicationClientData.Map).SendACK(true);

            this.gridViewExCoordinates.SingleSelectionChanged += gridViewExCoordinates_SingleSelectionChanged;
            this.gridViewExCoordinates.Columns[0].SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;
            this.Width = 512;
            //this.Height = 565;
        }

        private void LoadDepartmentTypes()
        {
            try
            {
                departmentTypes.AddRange(ServerServiceClient.GetInstance().SearchClientObjectsMaxRows(
                    SmartCadHqls.GetDepartmentsType, true));
                FormUtil.InvokeRequired(this.comboBoxEditDepartmentTypes, () => this.comboBoxEditDepartmentTypes.Properties.Items.AddRange(departmentTypes));
            }
            catch (Exception ex)
            {
                MessageForm.Show(ex.Message, ex);
            }
        }

        private void LoadDepartmentZones()
        {
            try
            {
                if (selectedType == null)
                {
                    zones.AddRange(ServerServiceClient.GetInstance().SearchClientObjectsMaxRows(
                    SmartCadHqls.DepartmentZonesWithAddress, true));
                }
                else
                {
                    zones = new ArrayList();
                    zones.AddRange(ServerServiceClient.GetInstance().SearchClientObjectsMaxRows(
                    SmartCadHqls.GetCustomHql(SmartCadHqls.DepartmentZonesByDepartmentType, selectedType.Code), true));
                }
                FormUtil.InvokeRequired(this.comboBoxEditDepartmentZones, () =>
                {
                    this.comboBoxEditDepartmentZones.Properties.Items.Clear();
                    this.comboBoxEditDepartmentZones.SelectedIndex = -1;
                    this.comboBoxEditDepartmentZones.Properties.Items.AddRange(zones);
                });
            }
            catch (Exception ex)
            {
                MessageForm.Show(ex.Message, ex);
            }
        }

        private void LoadLists()
        {
            if (SelectedDepartmentStation != null && SelectedDepartmentStation.Code != 0)
            {
                units.AddRange(ServerServiceClient.GetInstance().SearchClientObjectsMaxRows(
                    SmartCadHqls.GetCustomHql(SmartCadHqls.GetUnitsByDepartmentStationCode,
                    SelectedDepartmentStation.Code), true));
                officers.AddRange(ServerServiceClient.GetInstance().SearchClientObjectsMaxRows(
                    SmartCadHqls.GetCustomHql(SmartCadHqls.GetOfficersByDepartmentStationCode,
                    SelectedDepartmentStation.Code), true));
            }
        }

        private void ConfigureGridControls()
        {
            departmentTypes = new ArrayList();
            zones = new ArrayList();
            units = new ArrayList();
            officers = new ArrayList();

            this.gridControlExOfficers.Type = typeof(GridControlDataAdmOfficer);
            this.gridControlExOfficers.EnableAutoFilter = false;
            this.gridControlExOfficers.ViewTotalRows = false;

            this.gridControlExUnits.Type = typeof(GridControlDataAdmUnit);
            this.gridControlExUnits.EnableAutoFilter = false;
            this.gridControlExUnits.ViewTotalRows = false;

            this.gridControlExCoordinates.Type = typeof(GridControlDataDepartmentStationCoord);
            this.gridControlExCoordinates.EnableAutoFilter = false;
            this.gridControlExCoordinates.ViewTotalRows = false;

            syncBoxUnits = new GridControlSynBox(gridControlExUnits);
            syncBoxOfficers = new GridControlSynBox(gridControlExOfficers);
        }

        void DepartmentStationXtraForm_AdministrationCommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
        {
            try
            {
                if (e.Objects != null && e.Objects.Count > 0)
                {
                    lock (syncCommitted)
                    {
                        try
                        {
                            if (ServerServiceClient.GetInstance().OperatorClient == null)
                            { }
                        }
                        catch
                        { }

                        if (e.Objects[0] is DepartmentStationClientData)
                        {
                            #region DepartmentStationClientData
                            DepartmentStationClientData departmentStation =
                                    e.Objects[0] as DepartmentStationClientData;
                            if (SelectedDepartmentStation != null && SelectedDepartmentStation.Equals(departmentStation))
                            {
                                if (e.Action == CommittedDataAction.Update)
                                {
                                    if (!internalUpdate)
                                    {
                                        MessageForm.Show(ResourceLoader.GetString2("DepartmentStationWasUpdated") + ": " + SelectedDepartmentStation.ToString(), MessageFormType.Warning);
                                    }
                                    if (reloadSelectedData)
                                    {
                                        SelectedDepartmentStation = departmentStation;
                                    }
                                    reloadSelectedData = false;
                                }
                                else if (e.Action == CommittedDataAction.Delete)
                                {
                                    MessageForm.Show(ResourceLoader.GetString2("DeleteFormDepartmentStationData") + ": " + SelectedDepartmentStation.ToString(), MessageFormType.Warning);
                                    FormUtil.InvokeRequired(this,
                                    delegate
                                    {
                                        this.Close();
                                    });
                                }
                            }
                            #endregion
                        }
                        else if (e.Objects[0] is DepartmentZoneClientData)
                        {
                            #region DepartmentZoneClientData
                            DepartmentZoneClientData departmentZone =
                                    e.Objects[0] as DepartmentZoneClientData;

                            if (e.Action == CommittedDataAction.Save)
                            {
                                this.comboBoxEditDepartmentZones.Properties.Items.Add(departmentZone);
                            }
                            else if (e.Action == CommittedDataAction.Update)
                            {
                                FormUtil.InvokeRequired(this.comboBoxEditDepartmentZones,
                                delegate
                                {
                                    int index = this.comboBoxEditDepartmentZones.Properties.Items.IndexOf(departmentZone);
                                    if (index != -1)
                                    {
                                        this.comboBoxEditDepartmentZones.Properties.Items[index] = departmentZone;
                                    }
                                    if (SelectedDepartmentStation != null && SelectedDepartmentStation.DepartmentZone.Code == departmentZone.Code)
                                    {
                                        MessageForm.Show(ResourceLoader.GetString2("DepartmentZoneWasUpdated") + ": " + departmentZone.ToString(), MessageFormType.Warning);
                                        try
                                        {
                                            this.comboBoxEditDepartmentZones.SelectedIndex = -1;
                                            this.comboBoxEditDepartmentZones.SelectedIndex = index;
                                        }
                                        catch { }
                                    }
                                    else if (index != -1 && comboBoxEditDepartmentZones.SelectedItem != null &&
                                        comboBoxEditDepartmentZones.SelectedItem.Equals(departmentZone))
                                    {
                                        this.comboBoxEditDepartmentZones.SelectedIndex = -1;
                                        this.comboBoxEditDepartmentZones.SelectedIndex = index;
                                    }
								});
							}
							#endregion
                        }
                        else if (e.Objects[0] is DepartmentTypeClientData)
                        {
                            #region DepartmentTypeClientData
                            DepartmentTypeClientData departmentType =
                                    e.Objects[0] as DepartmentTypeClientData;
                            if (e.Action == CommittedDataAction.Update)
                            {
                                UpdateUnitsByDepartmentType(departmentType);
                            }
                            #endregion
                        }
					}
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        private void UpdateUnitsByDepartmentType(DepartmentTypeClientData departmentType)
        {
            ArrayList gridUnitList = new ArrayList();
            gridUnitList.AddRange(gridControlExUnits.DataSource as IList);
            for (int i = 0; i < gridUnitList.Count; i++)
            {
                GridControlDataAdmUnit gridData = gridUnitList[i] as GridControlDataAdmUnit;
                if (gridData.Unit.DepartmentStation.DepartmentZone.DepartmentType.Code == departmentType.Code)
                {
                    gridData.Unit.DepartmentStation.DepartmentZone.DepartmentType = departmentType;
                    gridControlExUnits.AddOrUpdateItem(gridData);
                }
            }
        }

        private void LoadLanguage()
        {
            FormUtil.InvokeRequired(this, () =>
            {
                if (Behavior == FormBehavior.Create)
                {
                    this.Text = ResourceLoader.GetString2("FormCreateDepartmentStation");
                    this.simpleButtonAccept.Text = ResourceLoader.GetString2("Create");
                }
                else if (Behavior == FormBehavior.Edit)
                {
                    this.Text = ResourceLoader.GetString2("FormEditDepartmentStation");
                    this.simpleButtonAccept.Text = ResourceLoader.GetString2("Accept");
                }
                layoutControlGroupOfficers.Text = ResourceLoader.GetString2("Officers");
                layoutControlGroupStations.Text = ResourceLoader.GetString2("DepartmentStationsInformation");
                layoutControlGroupUnits.Text = ResourceLoader.GetString2("Units");
                this.layoutControlItemName.Text = ResourceLoader.GetString2("$Message.Name") + ":* ";
                this.layoutControlItemZone.Text = ResourceLoader.GetString2("DepartmentZone") + ":* ";
                this.layoutControlItemDepartmentType.Text = ResourceLoader.GetString2("DepartmentType") + ":* ";
                this.layoutControlItemCustomCode.Text = ResourceLoader.GetString2("CustomCode") + ": ";
                //this.simpleButtonAccept.Text = ResourceLoader.GetString2("Create");
                this.simpleButtonCancel.Text = ResourceLoader.GetString2("$Message.Cancel");
                this.simpleButtonMap.ToolTip = ResourceLoader.GetString2("OpenMap");
                this.labelControlCoordinates.Text = ResourceLoader.GetString2("Coordinates") + ":";
                this.layoutControlItemTelephone.Text = ResourceLoader.GetString2("Telephone") + ":";
            });
        }

        private void serverServiceClient_GisAction(object sender, GisActionEventArgs e)
        {
            try
            {
                if (e is SendGeoPointListEventArgs)
                {
                    #region GeoPointList
                    List<GeoPoint> points = (e as SendGeoPointListEventArgs).Points;
                    if (points != null && points.Count() > 0)
                    {
                        if (((DepartmentZoneClientData)comboBoxEditDepartmentZones.SelectedItem).DepartmentZoneAddress.Count > 0)
                        {
                            FormUtil.InvokeRequired(this, () =>
                            {
                                gridControlExCoordinates.ClearData();
                                int count = gridControlExCoordinates.Items.Count;
                                foreach (GeoPoint p in points)
                                {
                                    if (p.Lat != 0 && p.Lon != 0)
                                    {
                                        DepartmentStationAddressClientData dsacd = new DepartmentStationAddressClientData();
                                        dsacd.PointNumber = (count++) + "";
                                        dsacd.Lat = p.Lat;
                                        dsacd.Lon = p.Lon;
                                        gridControlExCoordinates.AddOrUpdateItem(new GridControlDataDepartmentStationCoord(dsacd));
                                    }
                                }
                            });
                            //List<GeoPoint> tosendFinal = SortConvertGridCoordinates();
                            //DepartmentZoneClientData zone = (this.comboBoxEditDepartmentZones.EditValue as DepartmentZoneClientData);
                            //ApplicationServiceClient.Current(UserApplicationClientData.Map).SendGeoPointList(tosendFinal, zone.DepartmentType.Code, zone.Code,
                            //    (selectedDepartmentStation != null) ? selectedDepartmentStation.Name : "", ShapeType.Station);

                        }
                        else
                        {
                            FormUtil.InvokeRequired(this, () => 
                                MessageForm.Show(ResourceLoader.GetString2("ZoneWithoutPoints"), MessageFormType.Error));
                        }
                    }
                    else if ((e as SendGeoPointListEventArgs).Type == ShapeType.None)
                    {
                        FormUtil.InvokeRequired(this, () =>
                        {
                            gridControlExCoordinates.ClearData();
                        });
                    }
                    #endregion
                }
                else if(e is SendACKEventArgs)
                {
                    if (((SendACKEventArgs)e).Connected)
                    {
                        MapIsOpen = true;

                        FormUtil.InvokeRequired(this, () => {
                            if (simpleButtonMap.Enabled)
                            {
                                SendActionsToMap();
                                simpleButtonMap.Enabled = false;
                            }
                        });
                    }
                    else
                    {
                        MapIsOpen = false;
                        FormUtil.InvokeRequired(this, () => 
                            simpleButtonMap.Enabled = (comboBoxEditDepartmentZones.SelectedIndex > -1));
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        private void simpleButtonAccept_Click(object sender, EventArgs e)
        {
            List<DepartmentStationAddressClientData> points = new List<DepartmentStationAddressClientData>();
            if (Behavior == FormBehavior.Create)
            {
                selectedDepartmentStation = new DepartmentStationClientData();
                selectedDepartmentStation.Name = this.textEditDepartmentStationName.Text;
                selectedDepartmentStation.CustomCode = this.textEditCustomCode.Text;
                selectedDepartmentStation.Telephone = this.textEditTelephone.Text;
                selectedDepartmentStation.DepartmentZone = selectedZone;
                SelectedDepartmentStation.DepartamentTypeCode = selectedType.Code;
                SelectedDepartmentStation.DepartamentTypeName = selectedType.Name;
                if (gridControlExCoordinates.Items.Count > 0 )
                {
                    foreach (GridControlDataDepartmentStationCoord var in gridControlExCoordinates.Items)
                    {
                        if (var.Lon != 0 && var.Lat != 0)
                        {
                            DepartmentStationAddressClientData ds = new DepartmentStationAddressClientData();
                            ds.Lon = var.Lon;
                            ds.Lat = var.Lat;
                            ds.PointNumber = var.PointNumber;
                            points.Add(ds);
                        }
                    }
                    selectedDepartmentStation.DepartmentStationAddress = points;
                }

                try
                {
                    ServerServiceClient.GetInstance().SaveOrUpdateClientData(selectedDepartmentStation);
                }
                catch (Exception ex)
                {
                    MessageForm.Show(ex.Message, ex);
                    HandleUKExceptions(ex);
                }
            }
            else if (Behavior == FormBehavior.Edit)
            {
                DepartmentStationClientData departmentStation = selectedDepartmentStation.Clone() as DepartmentStationClientData;
                departmentStation.Name = this.textEditDepartmentStationName.Text;
                departmentStation.CustomCode = this.textEditCustomCode.Text;
                departmentStation.Telephone = this.textEditTelephone.Text;
                departmentStation.DepartmentZone = selectedZone;
                SelectedDepartmentStation.DepartamentTypeCode = selectedType.Code;
                SelectedDepartmentStation.DepartamentTypeName = selectedType.Name;

                points.Clear();
                departmentStation.DepartmentStationAddress = new ArrayList();
                if (gridControlExCoordinates.Items.Count > 0)
                {
                    foreach (GridControlDataDepartmentStationCoord var in gridControlExCoordinates.Items)
                    {
                        DepartmentStationAddressClientData ds = new DepartmentStationAddressClientData();
                        ds.Lon = var.Lon;
                        ds.Lat = var.Lat;
                        ds.PointNumber = var.PointNumber;
                        points.Add(ds);
                    }
                    departmentStation.DepartmentStationAddress = points;                   
                }
                else
                {
                    if (zoneWasModified)
                    {
                        selectedDepartmentStation.DepartmentStationAddress.Clear();
                    }
                }
                try
                {
                    ServerServiceClient.GetInstance().SaveOrUpdateClientData(departmentStation);
                }
                catch (Exception ex)
                {
                    MessageForm.Show(ex.Message, MessageFormType.Error);
                    internalUpdate = true;
                    selectedDepartmentStation.DepartmentStationAddress = originalAddress;
                    ServerServiceClient.GetInstance().AskUpdatedObjectToServer(selectedDepartmentStation,
                        null);
                    HandleUKExceptions(ex);
                }
            }
        }

        private void HandleUKExceptions(Exception ex)
        {
            if (ex != null)
            {
                FormUtil.InvokeRequired(this, () =>
                {
                    if (ex.Message.Equals(ResourceLoader.GetString2("UK_DEPARTMENT_STATION_NAME")))
                    {
                        this.textEditDepartmentStationName.Focus();
                        this.textEditDepartmentStationName.SelectAll();
                        this.DialogResult = DialogResult.None;
                    }
                    else
                        if (ex.Message.Equals(ResourceLoader.GetString2("UK_DEPARTMENT_STATION_CUSTOM_CODE")))
                        {
                            this.textEditCustomCode.Focus();
                            this.textEditCustomCode.SelectAll();
                            this.DialogResult = DialogResult.None;
                        }
                });
            }
        }

        private void DepartmentStationXtraForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.parentForm != null)
            {
                this.parentForm.AdministrationCommittedChanges -= DepartmentStationXtraForm_AdministrationCommittedChanges;
            }
            ApplicationServerService.GisAction -= serverServiceClient_GisAction;
            ApplicationServiceClient.Current(UserApplicationClientData.Map).SendACK(false);
        }

        private void EnableButtons(object sender, EventArgs e)
        {
            FormUtil.InvokeRequired(this, () =>
            {
                if (SelectedDepartmentStation != null && SelectedDepartmentStation.Code != 0)
                {
                    selectedType = (DepartmentTypeClientData)this.comboBoxEditDepartmentTypes.SelectedItem;
                    selectedZone = (DepartmentZoneClientData)this.comboBoxEditDepartmentZones.SelectedItem;

                    //Checks if the zone or the deptype were modified
                    if ((selectedType != null && SelectedDepartmentStation.DepartamentTypeCode != selectedType.Code)
                        || (selectedZone != null && !selectedZone.Equals(SelectedDepartmentStation.DepartmentZone)))
                    {
                        if (SelectedDepartmentStation.DepartmentStationAddress != null && 
                            SelectedDepartmentStation.DepartmentStationAddress.Count > 0)
                        {
                            DialogResult result = MessageForm.Show(ResourceLoader.GetString2("LocationStationWillBeDeleted"), MessageFormType.Question);
                            if (result == DialogResult.Yes)
                            {
                                zoneWasModified = true;
                                internalUpdate = true;
                                reloadSelectedData = false; //used in commitedChanges
                                SelectedDepartmentStation.DepartmentStationAddress = new ArrayList();
                                ServerServiceClient.GetInstance().SaveOrUpdateClientData(SelectedDepartmentStation);

                                gridControlExCoordinates.ClearData();
                            }
                            else
                            {
                                comboBoxEditDepartmentTypes.SelectedItem = SelectedDepartmentStation.DepartmentZone.DepartmentType;
                                comboBoxEditDepartmentZones.SelectedItem = SelectedDepartmentStation.DepartmentZone;
                            }
                        }
                        //Notify the changes to map.
                        SendActionsToMap();
                    }
                }

                if (textEditDepartmentStationName.Text.Trim().Length > 0 && comboBoxEditDepartmentZones.SelectedIndex > -1)
                {
                    simpleButtonAccept.Enabled = true;
                }
                else
                {
                    simpleButtonAccept.Enabled = false;
                }
                simpleButtonMap.Enabled = !MapIsOpen && (comboBoxEditDepartmentZones.SelectedIndex > -1);
            });
        }

        private void simpleButtonMap_Click(object sender, EventArgs e)
        {
            if (comboBoxEditDepartmentZones.SelectedItem != null)
            {
                SendActionsToMap();
            }
            else
            {
                MessageForm.Show(ResourceLoader.GetString2("ZoneWithoutPoints"), MessageFormType.Error);
            }
        }

        private void SendActionsToMap()
        {
            DepartmentTypeClientData dep = ((DepartmentTypeClientData)comboBoxEditDepartmentTypes.SelectedItem);
            DepartmentZoneClientData zone = ((DepartmentZoneClientData)comboBoxEditDepartmentZones.SelectedItem);

            if (MapIsOpen)
            {
                ApplicationServiceClient.Current(UserApplicationClientData.Map).SendEnableButton(true, ButtonType.AddPolygon);
                ApplicationServiceClient.Current(UserApplicationClientData.Map).SendToActivateLayer(ShapeType.Zone);
                ApplicationServiceClient.Current(UserApplicationClientData.Map).SendToActivateLayer(
                    ShapeType.Station,
                    dep!=null? dep.Code : -1,
                    zone != null ? zone.Code : -1,
                    textEditDepartmentStationName.Text);
                
                if (selectedDepartmentStation != null && selectedDepartmentStation.DepartmentStationAddress != null &&
                    selectedDepartmentStation.DepartmentStationAddress.Count > 0)
                {
                    DepartmentStationAddressClientData address = (DepartmentStationAddressClientData)selectedDepartmentStation.DepartmentStationAddress[0];

                    ApplicationServiceClient.Current(UserApplicationClientData.Map).DisplayGeoPointInGis(new GeoPoint(address.Lon, address.Lat));
                }
            }
            else if (simpleButtonMap.Enabled)
            {
                DepartmentStationAddressClientData address = new DepartmentStationAddressClientData();

                if (selectedDepartmentStation != null && selectedDepartmentStation.DepartmentStationAddress != null &&
                    selectedDepartmentStation.DepartmentStationAddress.Count > 0)
                {
                    address = (DepartmentStationAddressClientData)selectedDepartmentStation.DepartmentStationAddress[0];
                }
                
                ApplicationUtil.LaunchApplicationWithArguments(
                    SmartCadConfiguration.SmartCadMapFilename,
                    new object[] { ServerServiceClient.GetInstance().OperatorClient.Login, 
                    AdministrationRibbonForm.Password, 
                    ApplicationUtil.CheckPrimaryScreenBounds(this.Left),
                    new SendEnableButtonEventArgs(ButtonType.AddPolygon,true),
                    new SendActivateLayerEventArgs(ShapeType.Zone),
                    new SendActivateLayerEventArgs(ShapeType.Station),
                    new SendGeoPointListEventArgs(new List<GeoPoint>(),
                    dep!=null? dep.Code : -1,
                    zone != null ? zone.Code : -1,
                    (selectedDepartmentStation != null) ? selectedDepartmentStation.Name : "",
                    ShapeType.Station),
                    new DisplayGeoPointEventArgs(new GeoPoint(address.Lon, address.Lat))
                    }
                );
            }
        }


        void gridViewExCoordinates_SingleSelectionChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (e.FocusedRowHandle >= 0)
            {
                try
                {
                    GridControlDataDepartmentStationCoord coord = null;
                    FormUtil.InvokeRequired(this.gridControlExCoordinates, () =>
                    {
                        IList list = this.gridControlExCoordinates.SelectedItems;
                        if (list.Count > 0)
                        {
                            coord = list[0] as GridControlDataDepartmentStationCoord;
                        }
                    });
                    if (coord != null)
                    {
                        ApplicationServiceClient.Current(UserApplicationClientData.Map).DisplayGeoPointInGis(new GeoPoint(coord.Lon, coord.Lat));
                    }
                }
                catch { }
            }
        }

        private void comboBoxEditDepartmentZones_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxEditDepartmentZones.EditValue as DepartmentZoneClientData != null)
            {
                selectedZone = (DepartmentZoneClientData)this.comboBoxEditDepartmentZones.SelectedItem;
                if(MapIsOpen) SendActionsToMap();
            }

            EnableButtons(null, null);
        }

        private List<GeoPoint> SortConvertGridCoordinates()
        {
            List<GridControlDataDepartmentStationCoord> tosend = gridControlExCoordinates.Items.Cast<GridControlDataDepartmentStationCoord>().ToList();
            tosend.Sort(delegate(GridControlDataDepartmentStationCoord a, GridControlDataDepartmentStationCoord b)
            {
                return Int32.Parse(a.PointNumber) - Int32.Parse(b.PointNumber);
            });

            List<GeoPoint> tosendFinal = tosend.ConvertAll<GeoPoint>(obj => new GeoPoint(obj.Lon, obj.Lat));
            return tosendFinal;
        }

        private void comboBoxEditDepartmentTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(selectedType != null)
                ApplicationServiceClient.Current(UserApplicationClientData.Map).SendACK(false);

            selectedType = (DepartmentTypeClientData)this.comboBoxEditDepartmentTypes.SelectedItem;
            LoadDepartmentZones();

            EnableButtons(null, null);
        }
    }
}
