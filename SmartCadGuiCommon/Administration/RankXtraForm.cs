using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using SmartCadCore.ClientData;
using SmartCadCore.Core;
using Smartmatic.SmartCad.Service;
using System.Collections;
using System.ServiceModel;
using SmartCadCore.Common;
using SmartCadCore.Enums;

namespace SmartCadGuiCommon
{
    public partial class RankXtraForm : DevExpress.XtraEditors.XtraForm
    {
        
        private FormBehavior behavior;
        private RankClientData selectedRank;
        private AdministrationRibbonForm parentForm;
        private object syncCommitted = new object();

        RankClientData rank;
        DepartmentTypeClientData dept;

        public RankClientData SelectedRank
        {
            get
            {
                return selectedRank;
            }
            set
            {                
                selectedRank = value;
                if (Behavior == FormBehavior.Edit)
                {
                    FormUtil.InvokeRequired(this,
                        delegate
                        {
                            this.textEditRankName.Text = selectedRank.Name;
                            this.memoEditDescription.Text = selectedRank.Description;

                            //this.comboBoxEditDepartmentTypes.Properties.ReadOnly = true;

                            if (selectedRank.DepartmentTypeName != null)
                            {
                                foreach (DepartmentTypeClientData dep in comboBoxEditDepartmentTypes.Properties.Items)
                                {
                                    if (dep.Name == selectedRank.DepartmentTypeName)
                                    {
                                        comboBoxEditDepartmentTypes.SelectedItem = dep;
                                        break;
                                    }
                                }
                            }
                        });
                }
            }
        }

        public FormBehavior Behavior
        {
            get
            {
                return behavior;
            }
            set
            {
                behavior = value;
            }
        }

        public RankXtraForm(AdministrationRibbonForm parentForm, RankClientData position, FormBehavior behavior)
        {
            InitializeComponent();
            LoadDepartments();

            this.Behavior = behavior;
            this.SelectedRank = position;

            LoadLanguage();
            this.parentForm = parentForm;
            this.parentForm.AdministrationCommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(RankXtraForm_AdministrationCommittedChanges);

                    if (Behavior == FormBehavior.Create)
                    {
                        selectedRank = new RankClientData();
                    }
                    
            

            rank = selectedRank.Clone() as RankClientData;
            //dept = comboBoxEditDepartmentTypes.SelectedItem as DepartmentTypeClientData;

            EnableButtons(null, EventArgs.Empty);

        }

        public void LoadDepartments()
        {
            IList departments = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetDepartmentsType);

            foreach (DepartmentTypeClientData dep in departments)
            {
                comboBoxEditDepartmentTypes.Properties.Items.Add(dep);
            }

        }

        void RankXtraForm_AdministrationCommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
        {
            try
            {
                if (e.Objects != null && e.Objects.Count > 0)
                {
                    lock (syncCommitted)
                    {
                        try
                        {
                            if (ServerServiceClient.GetInstance().OperatorClient == null)
                            { }
                        }
                        catch
                        { }

                        if (e.Objects[0] is RankClientData)
                        {
                            #region RankClientData
                            RankClientData departmentStation =
                                    e.Objects[0] as RankClientData;
                            if (SelectedRank != null && SelectedRank.Equals(departmentStation))
                            {
                                if (e.Action == CommittedDataAction.Update)
                                {
                                    if (ButtonOkPressed == false)
                                        MessageForm.Show(ResourceLoader.GetString2("UpdateFormRankData"), MessageFormType.Warning);
                                    SelectedRank = departmentStation;
                                }
                                else if (e.Action == CommittedDataAction.Delete)
                                {
                                    MessageForm.Show(ResourceLoader.GetString2("DeleteFormRankData"), MessageFormType.Warning);
                                    FormUtil.InvokeRequired(this,
                                    delegate
                                    {
                                        this.Close();
                                    });
                                }                                
                            }
                            #endregion
                        }
                        else if (e.Objects[0] is DepartmentTypeClientData)
                        {
                            #region DepartmentTypeClientData
                            DepartmentTypeClientData departmentType = e.Objects[0] as DepartmentTypeClientData;
                            if (e.Action == CommittedDataAction.Save)
                            {
                                FormUtil.InvokeRequired(this.comboBoxEditDepartmentTypes,
                                delegate
                                {
                                    comboBoxEditDepartmentTypes.Properties.Items.Add(departmentType);
                                });
                            }
                            else if (e.Action == CommittedDataAction.Update)
                            {
                                FormUtil.InvokeRequired(this.comboBoxEditDepartmentTypes,
                                   delegate
                                   {
                                       bool select = comboBoxEditDepartmentTypes.SelectedItem.Equals(departmentType);
                                       int index = this.comboBoxEditDepartmentTypes.Properties.Items.IndexOf(departmentType);
                                       if (index != -1)
                                       {
                                           comboBoxEditDepartmentTypes.Properties.Items[index] = departmentType;
                                       }
                                       if (select == true)
                                       {
                                           comboBoxEditDepartmentTypes.SelectedIndex = -1;
                                           comboBoxEditDepartmentTypes.SelectedItem = departmentType;
                                       }
                                   });
                            }
                            #endregion
                        }                      
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        private void LoadLanguage()
        {
            FormUtil.InvokeRequired(this,
                delegate
                {
                    if (Behavior == FormBehavior.Create)
                    {
                        this.Text = ResourceLoader.GetString2("FormCreateRank");
                        this.simpleButtonAccept.Text = ResourceLoader.GetString2("Create");
                    }
                    else if (Behavior == FormBehavior.Edit)
                    {
                        this.Text = ResourceLoader.GetString2("FormEditRank");
                        this.simpleButtonAccept.Text = ResourceLoader.GetString2("Accept");
                    }
                    this.layoutControlItemName.Text = ResourceLoader.GetString2("$Message.Name") + ": *";
                    this.layoutControlItemDepartment.Text = ResourceLoader.GetString2("DepartmentType") + ": *";
                    this.layoutControlItemDescription.Text = ResourceLoader.GetString2("Description") + ":";
                    layoutControlGroupRank.Text = ResourceLoader.GetString2("RankInformation");                    
                    this.simpleButtonCancel.Text = ResourceLoader.GetString2("$Message.Cancel");
                });
        }

        private bool buttonOkPressed = false;

        private bool ButtonOkPressed
        {
            get
            {
                return buttonOkPressed;
            }
        }

        private void simpleButtonAccept_Click(object sender, EventArgs e)
        {
            
            buttonOkPressed = true;
            dept = comboBoxEditDepartmentTypes.SelectedItem as DepartmentTypeClientData;

            if (Behavior == FormBehavior.Create)
            {                
                selectedRank.Name = this.textEditRankName.Text;
                selectedRank.Description = this.memoEditDescription.Text;
                

                if (dept != null)
                {
                    selectedRank.DepartmentTypeName = dept.Name;
                    selectedRank.DepartmentTypeCode = dept.Code;
                }

                try
                {
                    ServerServiceClient.GetInstance().SaveOrUpdateClientData(selectedRank);
                }
                catch (FaultException ex)
                {
                    GetErrorFocus(ex);
                    DialogResult = DialogResult.None;
                    MessageForm.Show(ex.Message, ex);
                }

            }

            else if (Behavior == FormBehavior.Edit)
            {                

                            if (selectedRank.DepartmentTypeCode != dept.Code)
                            {

                                long count = (long)ServerServiceClient.GetInstance().SearchBasicObject(SmartCadHqls.GetCustomHql(SmartCadHqls.CountOfficersWithRank, selectedRank.Code));

                                if (count > 0)
                                {
                                    if (MessageForm.Show(ResourceLoader.GetString2("RankAssignValidateDelete"), MessageFormType.Question) != DialogResult.Yes)
                                    {
                                        DialogResult = System.Windows.Forms.DialogResult.None;
                                        
                                        buttonOkPressed = false;
                                        return;
                                    }
                                }

                            }
                            
                rank.Name = this.textEditRankName.Text;
                rank.Description = this.memoEditDescription.Text;

                rank.Code = rank.Code;
                rank.DepartmentTypeCode = dept.Code;                
                rank.DepartmentTypeName = dept.Name;                
                


                try
                {
                    ServerServiceClient.GetInstance().SaveOrUpdateClientData(rank);
                }
                catch (FaultException ex)
                {
                
                    DialogResult = DialogResult.None;
                    MessageForm.Show(ex.Message, ex);
                    ServerServiceClient.GetInstance().AskUpdatedObjectToServer(selectedRank,
                        null);
                    GetErrorFocus(ex);
                }
            }

            buttonOkPressed = false;
        }

        private void GetErrorFocus(FaultException ex)
        {
            if (ex.Message.Contains(ResourceLoader.GetString2("UK_RANK_NAME")))
            {
                if (behavior == FormBehavior.Create)
                {
                    this.textEditRankName.Text = this.textEditRankName.Text;
                }
                textEditRankName.Focus();
            }
            
        }

        private void RankXtraForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.parentForm != null)
            {
                this.parentForm.AdministrationCommittedChanges -= new EventHandler<CommittedObjectDataCollectionEventArgs>(RankXtraForm_AdministrationCommittedChanges);
            }
        }
        
        private void EnableButtons(object sender, EventArgs e)
        {
            FormUtil.InvokeRequired(this,
                delegate
                {
                    if (this.textEditRankName.Text.Trim().Length > 0 &&
                        this.comboBoxEditDepartmentTypes.SelectedIndex > -1) /*&&
                        this.memoEditDescription.Text.Trim().Length > 0)*/
                    {
                        this.simpleButtonAccept.Enabled = true;
                    }
                    else
                    {
                        this.simpleButtonAccept.Enabled = false;
                    }
                });
        } 
    }
}