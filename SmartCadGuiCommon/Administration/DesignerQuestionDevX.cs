﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraLayout;
using DevExpress.XtraEditors;

using System.IO;
using System.Xml;
using Smartmatic.SmartCad.Controls;
using System.Reflection;
using SmartCadControls.Interfaces;
using SmartCadCore.Common;
using SmartCadControls.Controls;
using SmartCadGuiCommon.Controls;


namespace SmartCadGuiCommon
{
    public partial class DesignerQuestionDevX : UserControl, IDragManager
    {
        #region Fields
        public event EventHandler<EventArgs> ControlChanged;
        #endregion

        public DesignerQuestionDevX()
        {
            InitializeComponent();
            dragDropLayoutControlAnswers.ControlCreated += dragDropLayoutControlAnswers_ControlCreated;
        }

        void dragDropLayoutControlAnswers_ControlCreated(object sender, EventArgs e)
        {
            OnControlChanged();
        }

        public bool Fill
        {
            get
            {
                return dragDropLayoutControlAnswers.layoutControl.Items.Count > 1;
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            LoadLanguage();
        }

        private void LoadLanguage()
        {
            layoutControlGroupControls.Text = ResourceLoader.GetString2("AnswerType");
            layoutControlItemTools.Text = ResourceLoader.GetString2("SelectAnswerType") + ":";
            layoutControlGroupAnswers.Text = ResourceLoader.GetString2("AnswerDefinition");
            layoutControlGroupPropertyGrid.Text = ResourceLoader.GetString2("ControlParameters");

            listViewControls.Items[0].Text = ResourceLoader.GetString2("TextBoxesName");
            listViewControls.Items[1].Text = ResourceLoader.GetString2("RadioButtonsName");
            listViewControls.Items[3].Text = ResourceLoader.GetString2("CheckBoxesName");
            listViewControls.Items[2].Text = ResourceLoader.GetString2("NumericTextBoxesName");

        }

        LayoutControlItem dragItem = null;
        
        LayoutControlItem IDragManager.DragItem 
        { 
            get 
            { 
                return dragItem; 
            } 
            set 
            { 
                dragItem = value; 
            } 
        }
        
        void IDragManager.SetDragCursor(DragDropEffects e) 
        { 
            SetDragCursor(e); 
        }
        
        private void SetDefaultCursor()
        {
            Cursor = Cursors.Default;
        }

        private void SetDragCursor(DragDropEffects e)
        {
            if (e == DragDropEffects.Move)
                Cursor = Cursors.AppStarting;
            if (e == DragDropEffects.Copy)
                Cursor = Cursors.AppStarting;
            if (e == DragDropEffects.None)
                Cursor = Cursors.No;
        }
        private LayoutControlItem GetDragNode(IDataObject data)
        {
            return data.GetData(typeof(LayoutControlItem)) as LayoutControlItem;
        }

        //listView
        private ListViewItem newItem = null;
        private void listView1_MouseDown(object sender, MouseEventArgs e)
        {
            newItem = listViewControls.GetItemAt(e.X, e.Y);
        }

        private void listView1_MouseMove(object sender, MouseEventArgs e)
        {
            if (newItem == null || e.Button != MouseButtons.Left) 
                return;
            dragItem = new LayoutControlItem();
            dragItem.Name = Guid.NewGuid().ToString();
            if ((string)newItem.Tag == "RadioButton")
                dragItem.Control = new RadioButton();
            else if ((string)newItem.Tag == "RadioGroup")
                dragItem.Control = new RadioButton();
            else if ((string)newItem.Tag == "CheckEdit")
                dragItem.Control = new CheckEdit();
            else if ((string)newItem.Tag == "Numeric")
                dragItem.Control = new SpinEdit();
            else
            {
                dragItem.Control = new TextBoxEx();
                (dragItem.Control as TextBoxEx).MaxLength = 255;
            }
            dragItem.Control.Text = "";
            dragItem.Control.Tag = dragItem;
            dragItem.Control.Name = Guid.NewGuid().ToString();
            dragItem.Text = newItem.Text;
            dragItem.Control.Click += new EventHandler(Control_Click);
            dragItem.Click += new EventHandler(Control_Click);
            listViewControls.DoDragDrop(dragItem, DragDropEffects.Copy);
        }

        void Control_Click(object sender, EventArgs e)
        {
            if (sender is LayoutControlItem)
            {
                SelectLayoutItem(sender as LayoutControlItem);
            }
            else if (sender is Control)
            {
                Control cntrl = sender as Control;
                if (cntrl != null)
                {
                    SelectLayoutItem(dragDropLayoutControlAnswers.layoutControl.GetItemByControl(cntrl) as LayoutControlItem);
                }
            }
        }

        private void SelectLayoutItem(LayoutControlItem ite)
        {
            dragDropLayoutControlAnswers.layoutControl.BeginUpdate();
            for (int i = 0; i < dragDropLayoutControlAnswers.layoutControl.Items.Count; i++)
            {
                dragDropLayoutControlAnswers.layoutControl.Items[i].Selected = false;
            }
            ite.Selected = true;
            ite.Control.Focus();
            dragDropLayoutControlAnswers.layoutControl.EndUpdate();
            UpdatePropertyGrid(ite);
        }

        private void UpdatePropertyGrid(LayoutControlItem ite)
        {
            propertyGridControl.Visible = true;
            propertyGridControl.BeginUpdate();
            propertyGridControl.Rows.Clear();
            propertyGridControl.SelectedObject = ControlExProperties.GetProperties(ite);
            propertyGridControl.EndUpdate();
        }

        private void simpleButtonTrash_DragDrop(object sender, DragEventArgs e)
        {
            if (CanRecycleDragItem())
            {
                Control control = dragItem.Control;
                dragItem.Parent.Remove(dragItem);
                if (control != null)
                {
                    control.Parent = null;
                    control.Dispose();
                }

                dragItem = null;
                DeleteControlFromDesigner(control);

                OnControlChanged();
            }

            SetDefaultLabel();
        }

        private void DeleteControlFromDesigner(Control control)
        {
            propertyGridControl.Visible = false;
            propertyGridControl.BeginUpdate();
            propertyGridControl.Rows.Clear();
            propertyGridControl.EndUpdate();
        }

        private void simpleButtonTrash_DragEnter(object sender, DragEventArgs e)
        {
            if (CanRecycleDragItem())
            {
                simpleButtonTrash.ImageIndex = 1;
                e.Effect = DragDropEffects.Copy;
                Cursor = Cursors.Hand;
            }
        }

        private void simpleButtonTrash_DragLeave(object sender, EventArgs e)
        {
            SetDefaultLabel();
        }

        private void SetDefaultLabel()
        {
            simpleButtonTrash.ImageIndex = 0;
            SetDefaultCursor();
        }

        protected bool CanRecycleDragItem()
        {
            if (dragItem == null) return false;
            if (dragItem.Owner == null) return false;
            return true;
        }

        private void OnControlChanged()
        {
            if (ControlChanged != null)
                ControlChanged(this, new EventArgs());
        }

        public void SetDesignerXML(string render)
        {
            MemoryStream ms = GetControlsInXML(render);

            dragDropLayoutControlAnswers.layoutControl.RestoreLayoutFromStream(ms);

            foreach (BaseLayoutItem baseLayoutItem in dragDropLayoutControlAnswers.layoutControl.Items)
            {
                if (baseLayoutItem is LayoutControlItem)
                {
                    LayoutControlItem item = (LayoutControlItem)baseLayoutItem;
                    item.Click += new EventHandler(Control_Click);
                    if (item.Control != null)
                        item.Control.Click += new EventHandler(Control_Click);

                }
            }
        }

        private MemoryStream GetControlsInXML(string render)
        {
            byte[] byteArray = Encoding.UTF8.GetBytes(render);
            MemoryStream ms = new MemoryStream(byteArray);
            ms.Position = 0;
            XmlDocument doc = new XmlDocument();
            doc.Load(ms);
            XmlNode firstChild = doc.FirstChild;
            foreach (XmlNode node in firstChild.ChildNodes)
            {
                if (node.Name == "Control")
                {
                    Control control = (Control)Activator.CreateInstance(Type.GetType(node.Attributes["type"].Value));
                    control.Name = node.Attributes["name"].Value;
                    control.Parent = this;
                    control.Tag = node.Attributes["itemName"].Value;
                    control.Text = "";
                    if (control is TextBoxEx)
                    {
                        if (node.Attributes["multiline"] != null)
                        {
                            (control as TextBoxEx).Multiline = bool.Parse(node.Attributes["multiline"].Value);
                            (control as TextBoxEx).ScrollBars = ScrollBars.Vertical;
                        }
                    }
                    else if (control is SpinEdit)
                    {
                        if (node.Attributes["minimum"] != null)
                            (control as SpinEdit).Properties.MinValue = decimal.Parse(node.Attributes["minimum"].Value);

                        if (node.Attributes["maximum"] != null)
                            (control as SpinEdit).Properties.MaxValue = decimal.Parse(node.Attributes["maximum"].Value);
                    }
                    dragDropLayoutControlAnswers.layoutControl.Controls.Add(control);
                }
            }
            ms.Position = 0;
            return ms;
        }

        public string GetDesignerXML()
        {
            dragDropLayoutControlAnswers.layoutControl.Controls.Clear();
            MemoryStream ms = new MemoryStream();
            dragDropLayoutControlAnswers.layoutControl.SaveLayoutToStream(ms);
            ms.Position = 0;
            MemoryStream msTotal = SetControlsInXML(ms);
            msTotal.Position = 0;
            StreamReader sr = new StreamReader(msTotal);
            string son = sr.ReadToEnd();
            return son;
        }

        private MemoryStream SetControlsInXML(Stream s)
        {

            XmlDocument doc = new XmlDocument();
            doc.Load(s);
            XmlNode firstChild = doc.FirstChild.ChildNodes[0];
            foreach (BaseLayoutItem baseLayoutItem in dragDropLayoutControlAnswers.layoutControl.Items)
            {
                if (baseLayoutItem is LayoutControlItem)
                {
                    LayoutControlItem item = (LayoutControlItem)baseLayoutItem;
                    if (item.Control != null)
                    {                 
                        XmlNode node = doc.CreateNode(XmlNodeType.Element, "Control", "");
                        XmlAttribute control = doc.CreateAttribute("type");
                        control.Value = Assembly.CreateQualifiedName(item.Control.GetType().Assembly.FullName, item.Control.GetType().FullName);
                        XmlAttribute name = doc.CreateAttribute("name");
                        name.Value = item.Control.Name;
                        XmlAttribute itemName = doc.CreateAttribute("itemName");
                        itemName.Value = item.Name;
                        XmlAttribute itemTex = doc.CreateAttribute("itemText");
                        itemTex.Value = item.Text;
                        node.Attributes.Append(itemTex);
                        node.Attributes.Append(itemName);
                        node.Attributes.Append(control);
                        node.Attributes.Append(name);

                        if (item.Control is TextBoxEx)
                        {
                            XmlAttribute attribute = doc.CreateAttribute("multiline");
                            attribute.Value = ((TextBoxEx)item.Control).Multiline.ToString();
                            node.Attributes.Append(attribute);
                        }
                        else if (item.Control is SpinEdit)
                        {
                            XmlAttribute attribute = doc.CreateAttribute("minimum");
                            attribute.Value = ((SpinEdit)item.Control).Properties.MinValue.ToString();
                            node.Attributes.Append(attribute);

                            attribute = doc.CreateAttribute("maximum");
                            attribute.Value = ((SpinEdit)item.Control).Properties.MaxValue.ToString();
                            node.Attributes.Append(attribute);
                        }
                        doc.FirstChild.InsertBefore(node, firstChild);
                    }
                }
            }
            MemoryStream ms = new MemoryStream();
            doc.Save(ms);
            return ms;
        }

        private void simpleButtonTrash_Click(object sender, EventArgs e)
        {
            foreach (LayoutControlItem item in dragDropLayoutControlAnswers.layoutControl.Root.Items)
            {
                if (item.Selected == true)
                {
                    dragItem = item;
                    break;
                }
            }

            simpleButtonTrash_DragDrop(null, null);
        }
    }
}
