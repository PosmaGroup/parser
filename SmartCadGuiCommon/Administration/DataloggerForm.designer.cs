using SmartCadControls;
using SmartCadControls.Controls;
namespace SmartCadGuiCommon
{
    partial class DataloggerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DataloggerForm));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            this.comboBoxExStructName = new ComboBoxEx();
            this.comboBoxExStructType = new ComboBoxEx();
            this.comboBoxExCctvZone = new ComboBoxEx();
            this.buttonExAccept = new DevExpress.XtraEditors.SimpleButton();
            this.SensorTelemetryFormConvertedLayout = new DevExpress.XtraLayout.LayoutControl();
            this.textEditCustomCode = new DevExpress.XtraEditors.TextEdit();
            this.gridControlSensor = new GridControlEx();
            this.gridViewSensor = new GridViewEx();
            this.gridColumnSensor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemComboBoxSensor = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.gridColumnVariable = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnLow = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnHigh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnDelete = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEditDelete = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.buttonExCancel = new DevExpress.XtraEditors.SimpleButton();
            this.textBoxExxDataloggerName = new TextBoxEx();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroupSensorTelemetryInf = new DevExpress.XtraLayout.LayoutControlGroup();
            this.textBoxExDataloggerNameitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemGridSensor = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemCustomCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupUbication = new DevExpress.XtraLayout.LayoutControlGroup();
            this.comboBoxExCctvZoneitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.comboBoxExStructTypeitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.comboBoxExStructNameitem = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutConverter1 = new DevExpress.XtraLayout.Converter.LayoutConverter(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.SensorTelemetryFormConvertedLayout)).BeginInit();
            this.SensorTelemetryFormConvertedLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCustomCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSensor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSensor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBoxSensor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupSensorTelemetryInf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxExDataloggerNameitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemGridSensor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCustomCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupUbication)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxExCctvZoneitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxExStructTypeitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxExStructNameitem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBoxExStructName
            // 
            this.comboBoxExStructName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxExStructName.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.comboBoxExStructName.FormattingEnabled = true;
            this.comboBoxExStructName.Location = new System.Drawing.Point(161, 444);
            this.comboBoxExStructName.Name = "comboBoxExStructName";
            this.comboBoxExStructName.Size = new System.Drawing.Size(353, 21);
            this.comboBoxExStructName.Sorted = true;
            this.comboBoxExStructName.TabIndex = 13;
            this.comboBoxExStructName.SelectedValueChanged += new System.EventHandler(this.comboBoxExStructName_SelectedValueChanged);
            // 
            // comboBoxExStructType
            // 
            this.comboBoxExStructType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxExStructType.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.comboBoxExStructType.FormattingEnabled = true;
            this.comboBoxExStructType.Location = new System.Drawing.Point(161, 419);
            this.comboBoxExStructType.Name = "comboBoxExStructType";
            this.comboBoxExStructType.Size = new System.Drawing.Size(353, 21);
            this.comboBoxExStructType.Sorted = true;
            this.comboBoxExStructType.TabIndex = 12;
            this.comboBoxExStructType.SelectedValueChanged += new System.EventHandler(this.comboBoxExStructType_SelectedValueChanged);
            // 
            // comboBoxExCctvZone
            // 
            this.comboBoxExCctvZone.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxExCctvZone.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.comboBoxExCctvZone.FormattingEnabled = true;
            this.comboBoxExCctvZone.Location = new System.Drawing.Point(161, 394);
            this.comboBoxExCctvZone.Name = "comboBoxExCctvZone";
            this.comboBoxExCctvZone.Size = new System.Drawing.Size(353, 21);
            this.comboBoxExCctvZone.Sorted = true;
            this.comboBoxExCctvZone.TabIndex = 11;
            this.comboBoxExCctvZone.SelectedValueChanged += new System.EventHandler(this.comboBoxExCctvZone_SelectedValueChanged);
            // 
            // buttonExAccept
            // 
            this.buttonExAccept.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.buttonExAccept.Appearance.Options.UseFont = true;
            this.buttonExAccept.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonExAccept.Enabled = false;
            this.buttonExAccept.Location = new System.Drawing.Point(352, 475);
            this.buttonExAccept.Name = "buttonExAccept";
            this.buttonExAccept.Size = new System.Drawing.Size(82, 29);
            this.buttonExAccept.StyleController = this.SensorTelemetryFormConvertedLayout;
            this.buttonExAccept.TabIndex = 14;
            this.buttonExAccept.Text = "Accept";
            this.buttonExAccept.Click += new System.EventHandler(this.buttonExAccept_Click);
            // 
            // SensorTelemetryFormConvertedLayout
            // 
            this.SensorTelemetryFormConvertedLayout.AllowCustomizationMenu = false;
            this.SensorTelemetryFormConvertedLayout.Controls.Add(this.textEditCustomCode);
            this.SensorTelemetryFormConvertedLayout.Controls.Add(this.gridControlSensor);
            this.SensorTelemetryFormConvertedLayout.Controls.Add(this.comboBoxExStructName);
            this.SensorTelemetryFormConvertedLayout.Controls.Add(this.buttonExAccept);
            this.SensorTelemetryFormConvertedLayout.Controls.Add(this.comboBoxExStructType);
            this.SensorTelemetryFormConvertedLayout.Controls.Add(this.buttonExCancel);
            this.SensorTelemetryFormConvertedLayout.Controls.Add(this.comboBoxExCctvZone);
            this.SensorTelemetryFormConvertedLayout.Controls.Add(this.textBoxExxDataloggerName);
            this.SensorTelemetryFormConvertedLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SensorTelemetryFormConvertedLayout.Location = new System.Drawing.Point(0, 0);
            this.SensorTelemetryFormConvertedLayout.Name = "SensorTelemetryFormConvertedLayout";
            this.SensorTelemetryFormConvertedLayout.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(555, 253, 250, 350);
            this.SensorTelemetryFormConvertedLayout.OptionsCustomizationForm.ShowLayoutTreeView = false;
            this.SensorTelemetryFormConvertedLayout.Root = this.layoutControlGroup1;
            this.SensorTelemetryFormConvertedLayout.Size = new System.Drawing.Size(522, 506);
            this.SensorTelemetryFormConvertedLayout.TabIndex = 20;
            // 
            // textEditCustomCode
            // 
            this.textEditCustomCode.EditValue = "0";
            this.textEditCustomCode.Location = new System.Drawing.Point(161, 52);
            this.textEditCustomCode.Name = "textEditCustomCode";
            this.textEditCustomCode.Properties.Mask.EditMask = "d";
            this.textEditCustomCode.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEditCustomCode.Size = new System.Drawing.Size(353, 20);
            this.textEditCustomCode.StyleController = this.SensorTelemetryFormConvertedLayout;
            this.textEditCustomCode.TabIndex = 19;
            this.textEditCustomCode.TextChanged += new System.EventHandler(this.textBoxExxDataloggerName_TextChanged);
            // 
            // gridControlSensor
            // 
            this.gridControlSensor.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridControlSensor.EnableAutoFilter = false;
            this.gridControlSensor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlSensor.Location = new System.Drawing.Point(8, 92);
            this.gridControlSensor.MainView = this.gridViewSensor;
            this.gridControlSensor.Name = "gridControlSensor";
            this.gridControlSensor.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEditDelete,
            this.repositoryItemComboBoxSensor});
            this.gridControlSensor.Size = new System.Drawing.Size(506, 266);
            this.gridControlSensor.TabIndex = 18;
            this.gridControlSensor.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewSensor});
            this.gridControlSensor.ViewTotalRows = false;
            // 
            // gridViewSensor
            // 
            this.gridViewSensor.ActiveFilterEnabled = false;
            this.gridViewSensor.AllowFocusedRowChanged = true;
            this.gridViewSensor.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewSensor.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewSensor.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewSensor.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewSensor.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewSensor.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewSensor.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewSensor.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewSensor.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnSensor,
            this.gridColumnVariable,
            this.gridColumnLow,
            this.gridColumnHigh,
            this.gridColumnDelete});
            this.gridViewSensor.EnablePreviewLineForFocusedRow = false;
            this.gridViewSensor.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridViewSensor.GridControl = this.gridControlSensor;
            this.gridViewSensor.Name = "gridViewSensor";
            this.gridViewSensor.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.True;
            this.gridViewSensor.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridViewSensor.OptionsFilter.AllowFilterEditor = false;
            this.gridViewSensor.OptionsMenu.EnableColumnMenu = false;
            this.gridViewSensor.OptionsMenu.EnableFooterMenu = false;
            this.gridViewSensor.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridViewSensor.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewSensor.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top;
            this.gridViewSensor.OptionsView.ShowChildrenInGroupPanel = true;
            this.gridViewSensor.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewSensor.OptionsView.ShowGroupedColumns = true;
            this.gridViewSensor.OptionsView.ShowGroupPanel = false;
            this.gridViewSensor.ViewTotalRows = false;
            this.gridViewSensor.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewSensor_FocusedRowChanged);
            // 
            // gridColumnSensor
            // 
            this.gridColumnSensor.Caption = "gridColumnSensor";
            this.gridColumnSensor.ColumnEdit = this.repositoryItemComboBoxSensor;
            this.gridColumnSensor.FieldName = "Sensor";
            this.gridColumnSensor.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DisplayText;
            this.gridColumnSensor.Name = "gridColumnSensor";
            this.gridColumnSensor.OptionsFilter.AllowFilter = false;
            this.gridColumnSensor.SortMode = DevExpress.XtraGrid.ColumnSortMode.DisplayText;
            this.gridColumnSensor.Visible = true;
            this.gridColumnSensor.VisibleIndex = 0;
            // 
            // repositoryItemComboBoxSensor
            // 
            this.repositoryItemComboBoxSensor.AutoHeight = false;
            this.repositoryItemComboBoxSensor.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBoxSensor.Name = "repositoryItemComboBoxSensor";
            this.repositoryItemComboBoxSensor.ShowDropDown = DevExpress.XtraEditors.Controls.ShowDropDown.Never;
            // 
            // gridColumnVariable
            // 
            this.gridColumnVariable.Caption = "gridColumnVariable";
            this.gridColumnVariable.FieldName = "Variable";
            this.gridColumnVariable.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DisplayText;
            this.gridColumnVariable.Name = "gridColumnVariable";
            this.gridColumnVariable.OptionsFilter.AllowFilter = false;
            this.gridColumnVariable.SortMode = DevExpress.XtraGrid.ColumnSortMode.DisplayText;
            this.gridColumnVariable.Visible = true;
            this.gridColumnVariable.VisibleIndex = 1;
            // 
            // gridColumnLow
            // 
            this.gridColumnLow.Caption = "gridColumnLow";
            this.gridColumnLow.FieldName = "Low";
            this.gridColumnLow.Name = "gridColumnLow";
            this.gridColumnLow.OptionsFilter.AllowFilter = false;
            this.gridColumnLow.Visible = true;
            this.gridColumnLow.VisibleIndex = 2;
            // 
            // gridColumnHigh
            // 
            this.gridColumnHigh.Caption = "gridColumnHigh";
            this.gridColumnHigh.FieldName = "High";
            this.gridColumnHigh.Name = "gridColumnHigh";
            this.gridColumnHigh.OptionsFilter.AllowFilter = false;
            this.gridColumnHigh.Visible = true;
            this.gridColumnHigh.VisibleIndex = 3;
            // 
            // gridColumnDelete
            // 
            this.gridColumnDelete.ColumnEdit = this.repositoryItemButtonEditDelete;
            this.gridColumnDelete.Name = "gridColumnDelete";
            this.gridColumnDelete.OptionsFilter.AllowFilter = false;
            this.gridColumnDelete.Visible = true;
            this.gridColumnDelete.VisibleIndex = 4;
            // 
            // repositoryItemButtonEditDelete
            // 
            this.repositoryItemButtonEditDelete.AutoHeight = false;
            this.repositoryItemButtonEditDelete.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("repositoryItemButtonEditDelete.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "", null, null, true)});
            this.repositoryItemButtonEditDelete.Name = "repositoryItemButtonEditDelete";
            this.repositoryItemButtonEditDelete.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEditDelete.Click += new System.EventHandler(this.repositoryItemButtonEditDelete_Click);
            // 
            // buttonExCancel
            // 
            this.buttonExCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.buttonExCancel.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.buttonExCancel.Appearance.Options.UseFont = true;
            this.buttonExCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonExCancel.Location = new System.Drawing.Point(438, 475);
            this.buttonExCancel.Name = "buttonExCancel";
            this.buttonExCancel.Size = new System.Drawing.Size(82, 29);
            this.buttonExCancel.StyleController = this.SensorTelemetryFormConvertedLayout;
            this.buttonExCancel.TabIndex = 15;
            this.buttonExCancel.Text = "Cancelar";
            this.buttonExCancel.Click += new System.EventHandler(this.buttonExCancel_Click);
            // 
            // textBoxExxDataloggerName
            // 
            this.textBoxExxDataloggerName.AllowsLetters = true;
            this.textBoxExxDataloggerName.AllowsNumbers = true;
            this.textBoxExxDataloggerName.AllowsPunctuation = true;
            this.textBoxExxDataloggerName.AllowsSeparators = true;
            this.textBoxExxDataloggerName.AllowsSymbols = true;
            this.textBoxExxDataloggerName.AllowsWhiteSpaces = true;
            this.textBoxExxDataloggerName.ExtraAllowedChars = "";
            this.textBoxExxDataloggerName.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExxDataloggerName.Location = new System.Drawing.Point(161, 28);
            this.textBoxExxDataloggerName.MaxLength = 40;
            this.textBoxExxDataloggerName.Name = "textBoxExxDataloggerName";
            this.textBoxExxDataloggerName.NonAllowedCharacters = "";
            this.textBoxExxDataloggerName.RegularExpresion = "";
            this.textBoxExxDataloggerName.Size = new System.Drawing.Size(353, 20);
            this.textBoxExxDataloggerName.TabIndex = 0;
            this.textBoxExxDataloggerName.TextChanged += new System.EventHandler(this.textBoxExxDataloggerName_TextChanged);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.layoutControlGroupSensorTelemetryInf,
            this.layoutControlGroupUbication,
            this.layoutControlItem1,
            this.layoutControlItem3});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlGroup1.Size = new System.Drawing.Size(522, 506);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 473);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(104, 24);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(350, 33);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroupSensorTelemetryInf
            // 
            this.layoutControlGroupSensorTelemetryInf.CustomizationFormText = "layoutControlGroupSensorTelemetryInf";
            this.layoutControlGroupSensorTelemetryInf.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.textBoxExDataloggerNameitem,
            this.layoutControlItemGridSensor,
            this.layoutControlItemCustomCode});
            this.layoutControlGroupSensorTelemetryInf.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupSensorTelemetryInf.Name = "layoutControlGroupSensorTelemetryInf";
            this.layoutControlGroupSensorTelemetryInf.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlGroupSensorTelemetryInf.Size = new System.Drawing.Size(522, 366);
            this.layoutControlGroupSensorTelemetryInf.Text = "layoutControlGroupSensorTelemetryInf";
            // 
            // textBoxExDataloggerNameitem
            // 
            this.textBoxExDataloggerNameitem.Control = this.textBoxExxDataloggerName;
            this.textBoxExDataloggerNameitem.CustomizationFormText = "textBoxExSensorTelemetryNameitem";
            this.textBoxExDataloggerNameitem.Location = new System.Drawing.Point(0, 0);
            this.textBoxExDataloggerNameitem.Name = "textBoxExDataloggerNameitem";
            this.textBoxExDataloggerNameitem.Size = new System.Drawing.Size(510, 24);
            this.textBoxExDataloggerNameitem.Text = "textBoxExDataloggerNameitem";
            this.textBoxExDataloggerNameitem.TextSize = new System.Drawing.Size(150, 13);
            // 
            // layoutControlItemGridSensor
            // 
            this.layoutControlItemGridSensor.Control = this.gridControlSensor;
            this.layoutControlItemGridSensor.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItemGridSensor.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItemGridSensor.Name = "layoutControlItemGridSensor";
            this.layoutControlItemGridSensor.Size = new System.Drawing.Size(510, 286);
            this.layoutControlItemGridSensor.Text = "layoutControlItemGridSensor";
            this.layoutControlItemGridSensor.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItemGridSensor.TextSize = new System.Drawing.Size(150, 13);
            // 
            // layoutControlItemCustomCode
            // 
            this.layoutControlItemCustomCode.Control = this.textEditCustomCode;
            this.layoutControlItemCustomCode.CustomizationFormText = "layoutControlItemCustomCode";
            this.layoutControlItemCustomCode.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItemCustomCode.Name = "layoutControlItemCustomCode";
            this.layoutControlItemCustomCode.Size = new System.Drawing.Size(510, 24);
            this.layoutControlItemCustomCode.Text = "layoutControlItemCustomCode";
            this.layoutControlItemCustomCode.TextSize = new System.Drawing.Size(150, 13);
            // 
            // layoutControlGroupUbication
            // 
            this.layoutControlGroupUbication.CustomizationFormText = "layoutControlGroupUbication";
            this.layoutControlGroupUbication.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.comboBoxExCctvZoneitem,
            this.comboBoxExStructTypeitem,
            this.comboBoxExStructNameitem});
            this.layoutControlGroupUbication.Location = new System.Drawing.Point(0, 366);
            this.layoutControlGroupUbication.Name = "layoutControlGroupUbication";
            this.layoutControlGroupUbication.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlGroupUbication.Size = new System.Drawing.Size(522, 107);
            this.layoutControlGroupUbication.Text = "layoutControlGroupUbication";
            // 
            // comboBoxExCctvZoneitem
            // 
            this.comboBoxExCctvZoneitem.Control = this.comboBoxExCctvZone;
            this.comboBoxExCctvZoneitem.CustomizationFormText = "comboBoxExCctvZoneitem";
            this.comboBoxExCctvZoneitem.Location = new System.Drawing.Point(0, 0);
            this.comboBoxExCctvZoneitem.Name = "comboBoxExCctvZoneitem";
            this.comboBoxExCctvZoneitem.Size = new System.Drawing.Size(510, 25);
            this.comboBoxExCctvZoneitem.Text = "comboBoxExCctvZoneitem";
            this.comboBoxExCctvZoneitem.TextSize = new System.Drawing.Size(150, 13);
            // 
            // comboBoxExStructTypeitem
            // 
            this.comboBoxExStructTypeitem.Control = this.comboBoxExStructType;
            this.comboBoxExStructTypeitem.CustomizationFormText = "comboBoxExStructTypeitem";
            this.comboBoxExStructTypeitem.Location = new System.Drawing.Point(0, 25);
            this.comboBoxExStructTypeitem.Name = "comboBoxExStructTypeitem";
            this.comboBoxExStructTypeitem.Size = new System.Drawing.Size(510, 25);
            this.comboBoxExStructTypeitem.Text = "comboBoxExStructTypeitem";
            this.comboBoxExStructTypeitem.TextSize = new System.Drawing.Size(150, 13);
            // 
            // comboBoxExStructNameitem
            // 
            this.comboBoxExStructNameitem.Control = this.comboBoxExStructName;
            this.comboBoxExStructNameitem.CustomizationFormText = "comboBoxExStructNameitem";
            this.comboBoxExStructNameitem.Location = new System.Drawing.Point(0, 50);
            this.comboBoxExStructNameitem.Name = "comboBoxExStructNameitem";
            this.comboBoxExStructNameitem.Size = new System.Drawing.Size(510, 25);
            this.comboBoxExStructNameitem.Text = "comboBoxExStructNameitem";
            this.comboBoxExStructNameitem.TextSize = new System.Drawing.Size(150, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.buttonExCancel;
            this.layoutControlItem1.CustomizationFormText = "buttonExCancelitem";
            this.layoutControlItem1.Location = new System.Drawing.Point(436, 473);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(86, 33);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(86, 33);
            this.layoutControlItem1.Name = "buttonExCancelitem";
            this.layoutControlItem1.Size = new System.Drawing.Size(86, 33);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "buttonExCancelitem";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.buttonExAccept;
            this.layoutControlItem3.CustomizationFormText = "buttonExAcceptitem";
            this.layoutControlItem3.Location = new System.Drawing.Point(350, 473);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(86, 33);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(86, 33);
            this.layoutControlItem3.Name = "buttonExAcceptitem";
            this.layoutControlItem3.Size = new System.Drawing.Size(86, 33);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "buttonExAcceptitem";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // DataloggerForm
            // 
            this.AcceptButton = this.buttonExAccept;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.CancelButton = this.buttonExCancel;
            this.ClientSize = new System.Drawing.Size(522, 506);
            this.Controls.Add(this.SensorTelemetryFormConvertedLayout);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1000, 581);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(475, 281);
            this.Name = "DataloggerForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.SensorTelemetryForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.SensorTelemetryFormConvertedLayout)).EndInit();
            this.SensorTelemetryFormConvertedLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEditCustomCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSensor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSensor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBoxSensor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupSensorTelemetryInf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxExDataloggerNameitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemGridSensor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCustomCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupUbication)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxExCctvZoneitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxExStructTypeitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxExStructNameitem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton buttonExAccept;
        private DevExpress.XtraEditors.SimpleButton buttonExCancel;
        private ComboBoxEx comboBoxExStructName;
        private ComboBoxEx comboBoxExStructType;
        private ComboBoxEx comboBoxExCctvZone;
        private DevExpress.XtraLayout.LayoutControl SensorTelemetryFormConvertedLayout;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem comboBoxExStructNameitem;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem comboBoxExStructTypeitem;
        private DevExpress.XtraLayout.LayoutControlItem comboBoxExCctvZoneitem;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.Converter.LayoutConverter layoutConverter1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupSensorTelemetryInf;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupUbication;
        private TextBoxEx textBoxExxDataloggerName;
        private DevExpress.XtraLayout.LayoutControlItem textBoxExDataloggerNameitem;
        private GridControlEx gridControlSensor;
        private GridViewEx gridViewSensor;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnSensor;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnHigh;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnLow;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemGridSensor;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnDelete;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditDelete;
        private DevExpress.XtraEditors.TextEdit textEditCustomCode;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemCustomCode;
        public DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBoxSensor;
        protected DevExpress.XtraGrid.Columns.GridColumn gridColumnVariable;
    }
}
