using SmartCadControls;
using SmartCadControls.Controls;
using System;
using System.Collections.Generic;
using System.Text;

namespace SmartCadGuiCommon
{
    public partial class StructForm
    {
        private DevExpress.XtraEditors.SimpleButton buttonExOK;
        private DevExpress.XtraEditors.SimpleButton buttonExCancel;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StructForm));
            this.buttonExOK = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.textBoxExLat = new SmartCadControls.Controls.TextBoxEx();
            this.textBoxExLon = new SmartCadControls.Controls.TextBoxEx();
            this.labelAssociated = new System.Windows.Forms.Label();
            this.labelAvailable = new System.Windows.Forms.Label();
            this.gridControlSel = new SmartCadControls.GridControlEx();
            this.gridViewEx2 = new SmartCadControls.GridViewEx();
            this.gridControlAvail = new SmartCadControls.GridControlEx();
            this.gridViewEx1 = new SmartCadControls.GridViewEx();
            this.buttonExRemoveCamera = new DevExpress.XtraEditors.SimpleButton();
            this.buttonExRemoveAllCameras = new DevExpress.XtraEditors.SimpleButton();
            this.buttonExCancel = new DevExpress.XtraEditors.SimpleButton();
            this.buttonExAddAllCameras = new DevExpress.XtraEditors.SimpleButton();
            this.buttonExAddCamera = new DevExpress.XtraEditors.SimpleButton();
            this.textBoxExName = new SmartCadControls.Controls.TextBoxEx();
            this.textBoxExTown = new SmartCadControls.Controls.TextBoxEx();
            this.comboBoxExType = new SmartCadControls.Controls.ComboBoxEx();
            this.textBoxExAv = new SmartCadControls.Controls.TextBoxEx();
            this.buttonExMap = new DevExpress.XtraEditors.SimpleButton();
            this.comboBoxCctvZone = new SmartCadControls.Controls.ComboBoxEx();
            this.textBoxExState = new SmartCadControls.Controls.TextBoxEx();
            this.textBoxExSec = new SmartCadControls.Controls.TextBoxEx();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroupStructInf = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemStructType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemZone = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupAddress = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemStreet = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemState = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemMun = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemUrb = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItemLat = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemLon = new DevExpress.XtraLayout.LayoutControlItem();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.layoutControlGroupCameras = new DevExpress.XtraLayout.LayoutControlGroup();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEx2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlAvail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEx1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupStructInf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemStructType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemZone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemStreet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemMun)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemUrb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupCameras)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonExOK
            // 
            this.buttonExOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonExOK.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.buttonExOK.Appearance.Options.UseFont = true;
            this.buttonExOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonExOK.Enabled = false;
            this.buttonExOK.Location = new System.Drawing.Point(325, 506);
            this.buttonExOK.Name = "buttonExOK";
            this.buttonExOK.Size = new System.Drawing.Size(82, 32);
            this.buttonExOK.StyleController = this.layoutControl1;
            this.buttonExOK.TabIndex = 14;
            this.buttonExOK.Text = "Accept";
            this.buttonExOK.Click += new System.EventHandler(this.buttonExOK_Click);
            // 
            // layoutControl1
            // 
            this.layoutControl1.AllowCustomizationMenu = false;
            this.layoutControl1.Controls.Add(this.textBoxExLat);
            this.layoutControl1.Controls.Add(this.textBoxExLon);
            this.layoutControl1.Controls.Add(this.labelAssociated);
            this.layoutControl1.Controls.Add(this.labelAvailable);
            this.layoutControl1.Controls.Add(this.gridControlSel);
            this.layoutControl1.Controls.Add(this.gridControlAvail);
            this.layoutControl1.Controls.Add(this.buttonExRemoveCamera);
            this.layoutControl1.Controls.Add(this.buttonExOK);
            this.layoutControl1.Controls.Add(this.buttonExRemoveAllCameras);
            this.layoutControl1.Controls.Add(this.buttonExCancel);
            this.layoutControl1.Controls.Add(this.buttonExAddAllCameras);
            this.layoutControl1.Controls.Add(this.buttonExAddCamera);
            this.layoutControl1.Controls.Add(this.textBoxExName);
            this.layoutControl1.Controls.Add(this.textBoxExTown);
            this.layoutControl1.Controls.Add(this.comboBoxExType);
            this.layoutControl1.Controls.Add(this.textBoxExAv);
            this.layoutControl1.Controls.Add(this.buttonExMap);
            this.layoutControl1.Controls.Add(this.comboBoxCctvZone);
            this.layoutControl1.Controls.Add(this.textBoxExState);
            this.layoutControl1.Controls.Add(this.textBoxExSec);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(340, 169, 450, 350);
            this.layoutControl1.OptionsCustomizationForm.ShowLayoutTreeView = false;
            this.layoutControl1.OptionsFocus.EnableAutoTabOrder = false;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(500, 545);
            this.layoutControl1.TabIndex = 6;
            this.layoutControl1.Text = "layoutControlStruct";
            // 
            // textBoxExLat
            // 
            this.textBoxExLat.AllowsLetters = true;
            this.textBoxExLat.AllowsNumbers = true;
            this.textBoxExLat.AllowsPunctuation = true;
            this.textBoxExLat.AllowsSeparators = true;
            this.textBoxExLat.AllowsSymbols = true;
            this.textBoxExLat.AllowsWhiteSpaces = true;
            this.textBoxExLat.ExtraAllowedChars = "";
            this.textBoxExLat.Location = new System.Drawing.Point(150, 131);
            this.textBoxExLat.Name = "textBoxExLat";
            this.textBoxExLat.NonAllowedCharacters = "";
            this.textBoxExLat.ReadOnly = true;
            this.textBoxExLat.RegularExpresion = "";
            this.textBoxExLat.Size = new System.Drawing.Size(286, 20);
            this.textBoxExLat.TabIndex = 21;
            // 
            // textBoxExLon
            // 
            this.textBoxExLon.AllowsLetters = true;
            this.textBoxExLon.AllowsNumbers = true;
            this.textBoxExLon.AllowsPunctuation = true;
            this.textBoxExLon.AllowsSeparators = true;
            this.textBoxExLon.AllowsSymbols = true;
            this.textBoxExLon.AllowsWhiteSpaces = true;
            this.textBoxExLon.ExtraAllowedChars = "";
            this.textBoxExLon.Location = new System.Drawing.Point(150, 155);
            this.textBoxExLon.Name = "textBoxExLon";
            this.textBoxExLon.NonAllowedCharacters = "";
            this.textBoxExLon.ReadOnly = true;
            this.textBoxExLon.RegularExpresion = "";
            this.textBoxExLon.Size = new System.Drawing.Size(286, 20);
            this.textBoxExLon.TabIndex = 20;
            // 
            // labelAssociated
            // 
            this.labelAssociated.Location = new System.Drawing.Point(275, 300);
            this.labelAssociated.Name = "labelAssociated";
            this.labelAssociated.Size = new System.Drawing.Size(213, 20);
            this.labelAssociated.TabIndex = 19;
            this.labelAssociated.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelAvailable
            // 
            this.labelAvailable.Location = new System.Drawing.Point(12, 300);
            this.labelAvailable.Name = "labelAvailable";
            this.labelAvailable.Size = new System.Drawing.Size(209, 20);
            this.labelAvailable.TabIndex = 18;
            this.labelAvailable.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gridControlSel
            // 
            this.gridControlSel.EnableAutoFilter = false;
            this.gridControlSel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlSel.Location = new System.Drawing.Point(275, 324);
            this.gridControlSel.MainView = this.gridViewEx2;
            this.gridControlSel.Name = "gridControlSel";
            this.gridControlSel.Size = new System.Drawing.Size(213, 173);
            this.gridControlSel.TabIndex = 17;
            this.gridControlSel.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewEx2});
            this.gridControlSel.ViewTotalRows = false;
            // 
            // gridViewEx2
            // 
            this.gridViewEx2.AllowFocusedRowChanged = true;
            this.gridViewEx2.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewEx2.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewEx2.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewEx2.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewEx2.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewEx2.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewEx2.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewEx2.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewEx2.EnablePreviewLineForFocusedRow = false;
            this.gridViewEx2.GridControl = this.gridControlSel;
            this.gridViewEx2.Name = "gridViewEx2";
            this.gridViewEx2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewEx2.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewEx2.ViewTotalRows = false;
            // 
            // gridControlAvail
            // 
            this.gridControlAvail.EnableAutoFilter = false;
            this.gridControlAvail.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlAvail.Location = new System.Drawing.Point(12, 324);
            this.gridControlAvail.MainView = this.gridViewEx1;
            this.gridControlAvail.Name = "gridControlAvail";
            this.gridControlAvail.Size = new System.Drawing.Size(209, 173);
            this.gridControlAvail.TabIndex = 16;
            this.gridControlAvail.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewEx1});
            this.gridControlAvail.ViewTotalRows = false;
            // 
            // gridViewEx1
            // 
            this.gridViewEx1.AllowFocusedRowChanged = true;
            this.gridViewEx1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewEx1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewEx1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewEx1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewEx1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewEx1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewEx1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewEx1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewEx1.EnablePreviewLineForFocusedRow = false;
            this.gridViewEx1.GridControl = this.gridControlAvail;
            this.gridViewEx1.Name = "gridViewEx1";
            this.gridViewEx1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewEx1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewEx1.ViewTotalRows = false;
            // 
            // buttonExRemoveCamera
            // 
            this.buttonExRemoveCamera.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExRemoveCamera.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonExRemoveCamera.Appearance.Options.UseFont = true;
            this.buttonExRemoveCamera.Appearance.Options.UseForeColor = true;
            this.buttonExRemoveCamera.Location = new System.Drawing.Point(225, 414);
            this.buttonExRemoveCamera.Name = "buttonExRemoveCamera";
            this.buttonExRemoveCamera.Size = new System.Drawing.Size(46, 34);
            this.buttonExRemoveCamera.StyleController = this.layoutControl1;
            this.buttonExRemoveCamera.TabIndex = 11;
            this.buttonExRemoveCamera.Text = "<";
            this.buttonExRemoveCamera.Click += new System.EventHandler(this.buttonExRemoveCamera_Click);
            this.buttonExRemoveCamera.MouseHover += new System.EventHandler(this.buttonExRemoveCamera_MouseHover);
            // 
            // buttonExRemoveAllCameras
            // 
            this.buttonExRemoveAllCameras.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExRemoveAllCameras.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonExRemoveAllCameras.Appearance.Options.UseFont = true;
            this.buttonExRemoveAllCameras.Appearance.Options.UseForeColor = true;
            this.buttonExRemoveAllCameras.Location = new System.Drawing.Point(225, 452);
            this.buttonExRemoveAllCameras.Name = "buttonExRemoveAllCameras";
            this.buttonExRemoveAllCameras.Size = new System.Drawing.Size(46, 34);
            this.buttonExRemoveAllCameras.StyleController = this.layoutControl1;
            this.buttonExRemoveAllCameras.TabIndex = 12;
            this.buttonExRemoveAllCameras.Text = "<<";
            this.buttonExRemoveAllCameras.Click += new System.EventHandler(this.buttonExRemoveAllCameras_Click);
            this.buttonExRemoveAllCameras.MouseHover += new System.EventHandler(this.buttonExRemoveAllCameras_MouseHover);
            // 
            // buttonExCancel
            // 
            this.buttonExCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonExCancel.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.buttonExCancel.Appearance.Options.UseFont = true;
            this.buttonExCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonExCancel.Location = new System.Drawing.Point(411, 506);
            this.buttonExCancel.Name = "buttonExCancel";
            this.buttonExCancel.Size = new System.Drawing.Size(82, 32);
            this.buttonExCancel.StyleController = this.layoutControl1;
            this.buttonExCancel.TabIndex = 15;
            this.buttonExCancel.Text = "Cancelar";
            // 
            // buttonExAddAllCameras
            // 
            this.buttonExAddAllCameras.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExAddAllCameras.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonExAddAllCameras.Appearance.Options.UseFont = true;
            this.buttonExAddAllCameras.Appearance.Options.UseForeColor = true;
            this.buttonExAddAllCameras.Location = new System.Drawing.Point(225, 376);
            this.buttonExAddAllCameras.Name = "buttonExAddAllCameras";
            this.buttonExAddAllCameras.Size = new System.Drawing.Size(46, 34);
            this.buttonExAddAllCameras.StyleController = this.layoutControl1;
            this.buttonExAddAllCameras.TabIndex = 10;
            this.buttonExAddAllCameras.Text = ">>";
            this.buttonExAddAllCameras.Click += new System.EventHandler(this.buttonExAddAllCameras_Click);
            this.buttonExAddAllCameras.MouseHover += new System.EventHandler(this.buttonExAddAllCameras_MouseHover);
            // 
            // buttonExAddCamera
            // 
            this.buttonExAddCamera.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExAddCamera.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonExAddCamera.Appearance.Options.UseFont = true;
            this.buttonExAddCamera.Appearance.Options.UseForeColor = true;
            this.buttonExAddCamera.Location = new System.Drawing.Point(225, 338);
            this.buttonExAddCamera.Name = "buttonExAddCamera";
            this.buttonExAddCamera.Size = new System.Drawing.Size(46, 34);
            this.buttonExAddCamera.StyleController = this.layoutControl1;
            this.buttonExAddCamera.TabIndex = 9;
            this.buttonExAddCamera.Text = ">";
            this.buttonExAddCamera.Click += new System.EventHandler(this.buttonExAddCamera_Click);
            this.buttonExAddCamera.MouseHover += new System.EventHandler(this.buttonExAddCamera_MouseHover);
            // 
            // textBoxExName
            // 
            this.textBoxExName.AllowsLetters = true;
            this.textBoxExName.AllowsNumbers = true;
            this.textBoxExName.AllowsPunctuation = true;
            this.textBoxExName.AllowsSeparators = true;
            this.textBoxExName.AllowsSymbols = true;
            this.textBoxExName.AllowsWhiteSpaces = true;
            this.textBoxExName.ExtraAllowedChars = "";
            this.textBoxExName.Location = new System.Drawing.Point(150, 27);
            this.textBoxExName.MaxLength = 40;
            this.textBoxExName.Name = "textBoxExName";
            this.textBoxExName.NonAllowedCharacters = "";
            this.textBoxExName.RegularExpresion = "";
            this.textBoxExName.Size = new System.Drawing.Size(343, 20);
            this.textBoxExName.TabIndex = 0;
            this.textBoxExName.TextChanged += new System.EventHandler(this.StructStatusParameters_Change);
            // 
            // textBoxExTown
            // 
            this.textBoxExTown.AllowsLetters = true;
            this.textBoxExTown.AllowsNumbers = true;
            this.textBoxExTown.AllowsPunctuation = true;
            this.textBoxExTown.AllowsSeparators = true;
            this.textBoxExTown.AllowsSymbols = true;
            this.textBoxExTown.AllowsWhiteSpaces = true;
            this.textBoxExTown.ExtraAllowedChars = "";
            this.textBoxExTown.Location = new System.Drawing.Point(150, 203);
            this.textBoxExTown.MaxLength = 40;
            this.textBoxExTown.Name = "textBoxExTown";
            this.textBoxExTown.NonAllowedCharacters = "";
            this.textBoxExTown.RegularExpresion = "";
            this.textBoxExTown.Size = new System.Drawing.Size(286, 20);
            this.textBoxExTown.TabIndex = 4;
            // 
            // comboBoxExType
            // 
            this.comboBoxExType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxExType.FormattingEnabled = true;
            this.comboBoxExType.Location = new System.Drawing.Point(150, 51);
            this.comboBoxExType.Name = "comboBoxExType";
            this.comboBoxExType.Size = new System.Drawing.Size(343, 21);
            this.comboBoxExType.TabIndex = 1;
            this.comboBoxExType.SelectedIndexChanged += new System.EventHandler(this.comboBoxExType_SelectedIndexChanged);
            // 
            // textBoxExAv
            // 
            this.textBoxExAv.AllowsLetters = true;
            this.textBoxExAv.AllowsNumbers = true;
            this.textBoxExAv.AllowsPunctuation = true;
            this.textBoxExAv.AllowsSeparators = true;
            this.textBoxExAv.AllowsSymbols = true;
            this.textBoxExAv.AllowsWhiteSpaces = true;
            this.textBoxExAv.ExtraAllowedChars = "";
            this.textBoxExAv.Location = new System.Drawing.Point(150, 251);
            this.textBoxExAv.MaxLength = 40;
            this.textBoxExAv.Name = "textBoxExAv";
            this.textBoxExAv.NonAllowedCharacters = "";
            this.textBoxExAv.RegularExpresion = "";
            this.textBoxExAv.Size = new System.Drawing.Size(343, 20);
            this.textBoxExAv.TabIndex = 6;
            // 
            // buttonExMap
            // 
            this.buttonExMap.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExMap.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonExMap.Appearance.Options.UseFont = true;
            this.buttonExMap.Appearance.Options.UseForeColor = true;
            this.buttonExMap.Image = ((System.Drawing.Image)(resources.GetObject("buttonExMap.Image")));
            this.buttonExMap.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.buttonExMap.Location = new System.Drawing.Point(440, 131);
            this.buttonExMap.Name = "buttonExMap";
            this.buttonExMap.Size = new System.Drawing.Size(53, 58);
            this.buttonExMap.StyleController = this.layoutControl1;
            this.buttonExMap.TabIndex = 7;
            this.buttonExMap.Click += new System.EventHandler(this.buttonExMap_Click);
            this.buttonExMap.MouseHover += new System.EventHandler(this.buttonExMap_MouseHover);
            // 
            // comboBoxCctvZone
            // 
            this.comboBoxCctvZone.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCctvZone.FormattingEnabled = true;
            this.comboBoxCctvZone.Location = new System.Drawing.Point(150, 76);
            this.comboBoxCctvZone.Name = "comboBoxCctvZone";
            this.comboBoxCctvZone.Size = new System.Drawing.Size(343, 21);
            this.comboBoxCctvZone.Sorted = true;
            this.comboBoxCctvZone.TabIndex = 2;
            this.comboBoxCctvZone.SelectedIndexChanged += new System.EventHandler(this.comboBoxCctvZone_SelectedIndexChanged);
            this.comboBoxCctvZone.SelectedValueChanged += new System.EventHandler(this.comboBoxCctvZone_SelectedValueChanged);
            // 
            // textBoxExState
            // 
            this.textBoxExState.AllowsLetters = true;
            this.textBoxExState.AllowsNumbers = true;
            this.textBoxExState.AllowsPunctuation = true;
            this.textBoxExState.AllowsSeparators = true;
            this.textBoxExState.AllowsSymbols = true;
            this.textBoxExState.AllowsWhiteSpaces = true;
            this.textBoxExState.ExtraAllowedChars = "";
            this.textBoxExState.Location = new System.Drawing.Point(150, 179);
            this.textBoxExState.MaxLength = 40;
            this.textBoxExState.Name = "textBoxExState";
            this.textBoxExState.NonAllowedCharacters = "";
            this.textBoxExState.RegularExpresion = "";
            this.textBoxExState.Size = new System.Drawing.Size(286, 20);
            this.textBoxExState.TabIndex = 3;
            // 
            // textBoxExSec
            // 
            this.textBoxExSec.AllowsLetters = true;
            this.textBoxExSec.AllowsNumbers = true;
            this.textBoxExSec.AllowsPunctuation = true;
            this.textBoxExSec.AllowsSeparators = true;
            this.textBoxExSec.AllowsSymbols = true;
            this.textBoxExSec.AllowsWhiteSpaces = true;
            this.textBoxExSec.ExtraAllowedChars = "";
            this.textBoxExSec.Location = new System.Drawing.Point(150, 227);
            this.textBoxExSec.MaxLength = 40;
            this.textBoxExSec.Name = "textBoxExSec";
            this.textBoxExSec.NonAllowedCharacters = "";
            this.textBoxExSec.RegularExpresion = "";
            this.textBoxExSec.Size = new System.Drawing.Size(343, 20);
            this.textBoxExSec.TabIndex = 5;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupStructInf,
            this.layoutControlGroupAddress});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(500, 545);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroupStructInf
            // 
            this.layoutControlGroupStructInf.CustomizationFormText = "layoutControlGroupStructInf";
            this.layoutControlGroupStructInf.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemName,
            this.layoutControlItemStructType,
            this.layoutControlItemZone});
            this.layoutControlGroupStructInf.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupStructInf.Name = "layoutControlGroupStructInf";
            this.layoutControlGroupStructInf.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupStructInf.Size = new System.Drawing.Size(500, 104);
            this.layoutControlGroupStructInf.Text = "layoutControlGroupStructInf";
            // 
            // layoutControlItemName
            // 
            this.layoutControlItemName.Control = this.textBoxExName;
            this.layoutControlItemName.CustomizationFormText = "layoutControlItemName";
            this.layoutControlItemName.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemName.Name = "layoutControlItemName";
            this.layoutControlItemName.Size = new System.Drawing.Size(490, 24);
            this.layoutControlItemName.Text = "layoutControlItemName";
            this.layoutControlItemName.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlItemStructType
            // 
            this.layoutControlItemStructType.Control = this.comboBoxExType;
            this.layoutControlItemStructType.CustomizationFormText = "layoutControlItemStructType";
            this.layoutControlItemStructType.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItemStructType.Name = "layoutControlItemStructType";
            this.layoutControlItemStructType.Size = new System.Drawing.Size(490, 25);
            this.layoutControlItemStructType.Text = "layoutControlItemStructType";
            this.layoutControlItemStructType.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlItemZone
            // 
            this.layoutControlItemZone.Control = this.comboBoxCctvZone;
            this.layoutControlItemZone.CustomizationFormText = "layoutControlItemZone";
            this.layoutControlItemZone.Location = new System.Drawing.Point(0, 49);
            this.layoutControlItemZone.Name = "layoutControlItemZone";
            this.layoutControlItemZone.Size = new System.Drawing.Size(490, 25);
            this.layoutControlItemZone.Text = "layoutControlItemZone";
            this.layoutControlItemZone.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlGroupAddress
            // 
            this.layoutControlGroupAddress.CustomizationFormText = "layoutControlGroupAddress";
            this.layoutControlGroupAddress.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemStreet,
            this.layoutControlItemState,
            this.layoutControlItemMun,
            this.layoutControlItemUrb,
            this.layoutControlItem5,
            this.layoutControlGroup2,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.emptySpaceItem1,
            this.layoutControlItemLat,
            this.layoutControlItemLon});
            this.layoutControlGroupAddress.Location = new System.Drawing.Point(0, 104);
            this.layoutControlGroupAddress.Name = "layoutControlGroupAddress";
            this.layoutControlGroupAddress.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupAddress.Size = new System.Drawing.Size(500, 441);
            this.layoutControlGroupAddress.Text = "layoutControlGroupAddress";
            // 
            // layoutControlItemStreet
            // 
            this.layoutControlItemStreet.Control = this.textBoxExAv;
            this.layoutControlItemStreet.CustomizationFormText = "layoutControlItemStreet";
            this.layoutControlItemStreet.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItemStreet.Name = "layoutControlItemStreet";
            this.layoutControlItemStreet.Size = new System.Drawing.Size(490, 24);
            this.layoutControlItemStreet.Text = "layoutControlItemStreet";
            this.layoutControlItemStreet.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlItemState
            // 
            this.layoutControlItemState.Control = this.textBoxExState;
            this.layoutControlItemState.CustomizationFormText = "layoutControlItemState";
            this.layoutControlItemState.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItemState.Name = "layoutControlItemState";
            this.layoutControlItemState.Size = new System.Drawing.Size(433, 24);
            this.layoutControlItemState.Text = "layoutControlItemState";
            this.layoutControlItemState.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlItemMun
            // 
            this.layoutControlItemMun.Control = this.textBoxExTown;
            this.layoutControlItemMun.CustomizationFormText = "layoutControlItemMun";
            this.layoutControlItemMun.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItemMun.Name = "layoutControlItemMun";
            this.layoutControlItemMun.Size = new System.Drawing.Size(433, 24);
            this.layoutControlItemMun.Text = "layoutControlItemMun";
            this.layoutControlItemMun.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlItemUrb
            // 
            this.layoutControlItemUrb.Control = this.textBoxExSec;
            this.layoutControlItemUrb.CustomizationFormText = "layoutControlItemUrb";
            this.layoutControlItemUrb.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItemUrb.Name = "layoutControlItemUrb";
            this.layoutControlItemUrb.Size = new System.Drawing.Size(490, 24);
            this.layoutControlItemUrb.Text = "layoutControlItemUrb";
            this.layoutControlItemUrb.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.buttonExMap;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.layoutControlItem5.Location = new System.Drawing.Point(433, 0);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(57, 62);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(57, 62);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(57, 96);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.emptySpaceItem2,
            this.emptySpaceItem3,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem11});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 144);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup2.Size = new System.Drawing.Size(490, 231);
            this.layoutControlGroup2.Text = "layoutControlGroup2";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.buttonExAddCamera;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(213, 38);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(50, 38);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(50, 38);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(50, 38);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.buttonExAddAllCameras;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(213, 76);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(50, 38);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(50, 38);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(50, 38);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.buttonExRemoveCamera;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(213, 114);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(50, 38);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(50, 38);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(50, 38);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.buttonExRemoveAllCameras;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(213, 152);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(50, 38);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(50, 38);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(50, 38);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(213, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(50, 38);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(213, 190);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(50, 11);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.gridControlAvail;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(213, 177);
            this.layoutControlItem8.Text = "layoutControlItem8";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.gridControlSel;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(263, 24);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(217, 177);
            this.layoutControlItem9.Text = "layoutControlItem9";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextToControlDistance = 0;
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.labelAvailable;
            this.layoutControlItem10.CustomizationFormText = "layoutControlItem10";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(213, 24);
            this.layoutControlItem10.Text = "layoutControlItem10";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextToControlDistance = 0;
            this.layoutControlItem10.TextVisible = false;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.labelAssociated;
            this.layoutControlItem11.CustomizationFormText = "layoutControlItem11";
            this.layoutControlItem11.Location = new System.Drawing.Point(263, 0);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(217, 24);
            this.layoutControlItem11.Text = "layoutControlItem11";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextToControlDistance = 0;
            this.layoutControlItem11.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.buttonExOK;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(318, 375);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.buttonExCancel;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(404, 375);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 375);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(318, 36);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItemLat
            // 
            this.layoutControlItemLat.Control = this.textBoxExLat;
            this.layoutControlItemLat.CustomizationFormText = "layoutControlItemLat";
            this.layoutControlItemLat.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemLat.Name = "layoutControlItemLat";
            this.layoutControlItemLat.Size = new System.Drawing.Size(433, 24);
            this.layoutControlItemLat.Text = "layoutControlItemLat";
            this.layoutControlItemLat.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlItemLon
            // 
            this.layoutControlItemLon.Control = this.textBoxExLon;
            this.layoutControlItemLon.CustomizationFormText = "layoutControlItemLon";
            this.layoutControlItemLon.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItemLon.Name = "layoutControlItemLon";
            this.layoutControlItemLon.Size = new System.Drawing.Size(433, 24);
            this.layoutControlItemLon.Text = "layoutControlItemLon";
            this.layoutControlItemLon.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlGroupCameras
            // 
            this.layoutControlGroupCameras.CustomizationFormText = "layoutControlGroupCameras";
            this.layoutControlGroupCameras.Location = new System.Drawing.Point(0, 124);
            this.layoutControlGroupCameras.Name = "layoutControlGroupCameras";
            this.layoutControlGroupCameras.OptionsItemText.TextToControlDistance = 5;
            this.layoutControlGroupCameras.Size = new System.Drawing.Size(542, 239);
            this.layoutControlGroupCameras.Text = "layoutControlGroupCameras";
            // 
            // StructForm
            // 
            this.AcceptButton = this.buttonExOK;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.CancelButton = this.buttonExCancel;
            this.ClientSize = new System.Drawing.Size(500, 545);
            this.Controls.Add(this.layoutControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(510, 560);
            this.Name = "StructForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.StructForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEx2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlAvail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEx1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupStructInf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemStructType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemZone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemStreet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemMun)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemUrb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupCameras)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion

        private TextBoxEx textBoxExName;
        private ComboBoxEx comboBoxCctvZone;
        private DevExpress.XtraEditors.SimpleButton buttonExRemoveAllCameras;
        private DevExpress.XtraEditors.SimpleButton buttonExRemoveCamera;
        private DevExpress.XtraEditors.SimpleButton buttonExAddAllCameras;
        private DevExpress.XtraEditors.SimpleButton buttonExAddCamera;
        private TextBoxEx textBoxExAv;
        private TextBoxEx textBoxExSec;
        private ComboBoxEx comboBoxExType;
        private DevExpress.XtraEditors.SimpleButton buttonExMap;
        private TextBoxEx textBoxExTown;
        private TextBoxEx textBoxExState;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.ComponentModel.IContainer components;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupStructInf;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemName;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemStructType;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemZone;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupAddress;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemStreet;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemState;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemMun;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemUrb;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupCameras;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private GridControlEx gridControlSel;
        private GridViewEx gridViewEx2;
        private GridControlEx gridControlAvail;
        private GridViewEx gridViewEx1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private System.Windows.Forms.Label labelAssociated;
        private System.Windows.Forms.Label labelAvailable;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private TextBoxEx textBoxExLat;
        private TextBoxEx textBoxExLon;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemLat;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemLon;

    }
}
