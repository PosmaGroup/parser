using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Reflection;
using System.Collections.Generic;

using Smartmatic.SmartCad.Service;
using System.ServiceModel;
using Iesi.Collections;
using DevExpress.XtraEditors;
using SmartCadGuiCommon;
using SmartCadCore.Core;
using SmartCadCore.ClientData;
using SmartCadCore.Enums;
using SmartCadCore.Common;

namespace Smartmatic.SmartCad.Gui
{
    #region Class ProfileForm Documentation
    /// <summary>
    /// TODO:
    /// </summary>
    /// <className>ProfileForm</className>
    /// <author email="atorres@smartmatic.com">Alden Torres</author>
    /// <author email="fsilva@smartmatic.com">Fernando Silva</author>
    /// <date>2006/04/11</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    public partial class ProfileForm : XtraForm
	{
		#region Fields

		private AdministrationRibbonForm parentForm;
        private object syncCommitted = new object();
		private bool buttonOkPressed = false;
		private FormBehavior behavior;
		private UserProfileClientData selectedProfile;
		private IList<UserAccessClientData> selectedAccesses = new List<UserAccessClientData>();
        private bool loading = false;

		#endregion

		#region Properties

		public bool ButtonOkPressed
		{
			get { return buttonOkPressed; }
		}

		public FormBehavior Behavior
		{
			set
			{
				behavior = value;


			}
			get
			{
				return behavior;
			}
		}

		public UserProfileClientData SelectedProfile
		{
			get
			{
				return selectedProfile;
			}
			set
			{
				UserProfileClientData profileClonned = null;
				if (value != null)
				{
					profileClonned = value.Clone() as UserProfileClientData;
				}
				selectedProfile = profileClonned;

				if (Behavior == FormBehavior.Edit)
				{
					FormUtil.InvokeRequired(this,
						delegate
						{
							textBoxName.Text = selectedProfile.FriendlyName;
							textBoxDescription.Text = selectedProfile.Description;
							ClearCheckedNodes();
                            loading = true;
							foreach (UserAccessClientData access in selectedProfile.Accesses)
							{
								CheckAccessInTreeView(access);
							}
                            loading = false;
						});
				}
			}
		}

		public IList<UserAccessClientData> SelectedAccesses
		{
			set
			{
				selectedAccesses = value;
			}
			get
			{
				return selectedAccesses;
			}
		}
		#endregion

		#region Constructors

		public ProfileForm(AdministrationRibbonForm parentForm)
        {
            InitializeComponent();
            FillAccesses();
            LoadLanguage();
            this.parentForm = parentForm;
            this.parentForm.AdministrationCommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(ProfileForm_AdministrationCommittedChanges);
		}

		public ProfileForm(AdministrationRibbonForm parentForm, UserProfileClientData profile, FormBehavior behavior)
			: this(parentForm)
		{
			Behavior = behavior;
			SelectedProfile = profile;
		}

		#endregion

		void ProfileForm_AdministrationCommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
        {
            try
            {
                if (e.Objects != null && e.Objects.Count > 0)
                {
                    lock (syncCommitted)
                    {
                        try
                        {
                            if (ServerServiceClient.GetInstance().OperatorClient == null)
                            { }
                        }
                        catch
                        { }

                        if (e.Objects[0] is UserProfileClientData)
                        {
                            #region UserProfileClientData
                            UserProfileClientData profile =
                                    e.Objects[0] as UserProfileClientData;
                            if (SelectedProfile != null && SelectedProfile.Equals(profile) && ButtonOkPressed == false)
                            {
                                if (e.Action == CommittedDataAction.Update)
                                {
                                    MessageForm.Show(ResourceLoader.GetString2("UpdateFormUserProfileData"), MessageFormType.Warning);
                                    SelectedProfile = profile;
                                }
                                else if (e.Action == CommittedDataAction.Delete)
                                {
                                    MessageForm.Show(ResourceLoader.GetString2("DeleteFormUserProfileData"), MessageFormType.Warning);
                                    FormUtil.InvokeRequired(this,
                                    delegate
                                    {
                                        this.Close();
                                    });
                                }

                            }
                            #endregion
                        }                        
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        private void ButtonsActivation(object sender, System.EventArgs e)
        {
         //   if (!(selectedProfile != null && selectedProfile.Name == ResourceLoader.GetString("SuperUserName")))
            if (!(selectedProfile != null && ((bool)selectedProfile.Immutable)))
            {
                if (textBoxName.Text.Trim() == "")
                    buttonExOK.Enabled = false;
                else
                    buttonExOK.Enabled = true;
            }
        }

        private void LoadLanguage()
        {
            this.layoutControlGroupData.Text = ResourceLoader.GetString2("DataProfile");
            this.layoutControlName.Text = ResourceLoader.GetString2("$Message.Name") +":*";
            this.layoutControlDesc.Text = ResourceLoader.GetString2("$Message.Description") + ":";
            this.layoutControlGroupAccess.Text = ResourceLoader.GetString2("AccessPlural");
            this.layoutControlAccess.Text = ResourceLoader.GetString2("AccessPlural") + ":";
            this.buttonExCancel.Text = ResourceLoader.GetString2("$Message.Cancel");
        }
        
        private void buttonExOK_Click1()
        {
            buttonOkPressed = true;
            if (behavior == FormBehavior.Create)
            {
                selectedProfile = new UserProfileClientData();
                selectedProfile.Name = textBoxName.Text.Trim();
                selectedProfile.Immutable = false;
            }
            else { 
                if (!(bool)selectedProfile.Immutable)
                    selectedProfile.Name = textBoxName.Text.Trim();
                       
            }
            selectedProfile.FriendlyName = textBoxName.Text.Trim();
            selectedProfile.Description = textBoxDescription.Text.Trim();

            if (selectedProfile.Accesses != null)
                selectedProfile.Accesses.Clear();
            else
                selectedProfile.Accesses = new List<UserAccessClientData>();

            foreach (UserAccessClientData access in SelectedAccesses)
            {
                selectedProfile.Accesses.Add(access);
            }

            ServerServiceClient.GetInstance().SaveOrUpdateClientData(selectedProfile);
        }

        private void buttonExOK_Click(object sender, System.EventArgs e)
        {
            try
            {
                BackgroundProcessForm processForm
                    = new BackgroundProcessForm(this,
                    new MethodInfo[1] { GetType().GetMethod("buttonExOK_Click1", 
                        BindingFlags.NonPublic | BindingFlags.Instance) },
                        new object[1][] { new object[0] });
                processForm.CanThrowError = true;
                processForm.ShowDialog();
            }
            catch (FaultException ex)
            {
                if (SelectedProfile != null)
                {
                    if (SelectedProfile.Code != 0)
                        SelectedProfile = (UserProfileClientData)ServerServiceClient.GetInstance().RefreshClient(SelectedProfile, true);                    
                }
                GetErrorFocus(ex);
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, MessageFormType.Error);
                buttonOkPressed = false;
            }
            catch (Exception ex)
            {
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, ex);
                buttonOkPressed = false;
            }
        }

        private void GetErrorFocus(FaultException ex)
        {
            if (ex.Message.Contains(ResourceLoader.GetString2("Same"))) 
            {
                if (behavior == FormBehavior.Create)
                    textBoxName.Text = textBoxName.Text;
                textBoxName.Focus();
            }
        }

        private void FillAccesses()
        {
            treeViewAccess.Nodes.Clear();

            IList applications = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetUserApplicationsWithAccesses, true);

            foreach (UserApplicationClientData application in applications)
            {
                //ADDED FOR BATAAN PROJECT VERSION 3.3.1
                if (application.Name != "Server" &&
					application.Name != UserApplicationClientData.Alarm.Name &&
                    application.Name != UserApplicationClientData.Lpr.Name &&
                    application.Name != UserApplicationClientData.SocialNetworks.Name)
                {
                    TreeNode nodeApplication = new TreeNode();
                    nodeApplication.Text = ResourceLoader.GetString2(application.FriendlyName);

					IList<UserAccessClientData> accesses = application.Accesses;

                    Dictionary<int, TreeNode> dic = new Dictionary<int, TreeNode>();

                    foreach (UserAccessClientData access in accesses)
                    {
                        UserResourceClientData resource = access.UserPermission.UserResource;
                        UserActionClientData action = access.UserPermission.UserAction;

                        TreeNode nodeResource = null;

                        if (!dic.ContainsKey(resource.Code))
                        {
                            nodeResource = new TreeNode();
                            nodeResource.Text = ResourceLoader.GetString2(resource.FriendlyName);

                            nodeApplication.Nodes.Add(nodeResource);
                            dic.Add(resource.Code, nodeResource);
                        }

                        nodeResource = dic[resource.Code];

                        TreeNode nodeAction = new TreeNode();
                        nodeAction.Text = ResourceLoader.GetString2(action.FriendlyName);
                        nodeAction.Tag = access;


                        nodeResource.Nodes.Add(nodeAction);
                    }

                    treeViewAccess.Nodes.Add(nodeApplication);
                }
            }
        }

        private void CheckAccessInTreeView(UserAccessClientData access)
        {
            foreach (TreeNode nodeApplication in treeViewAccess.Nodes)
            {
                foreach (TreeNode nodeResource in nodeApplication.Nodes)
                {
                    foreach (TreeNode nodeAction in nodeResource.Nodes)
                    {
                        UserAccessClientData accessItem = nodeAction.Tag as UserAccessClientData;
                        if (accessItem.Code == access.Code)
                        {
                            treeViewAccess.SetChecked(nodeAction, TriStateTreeView.CheckState.Checked);
                        }
                    }
                }
            }
        }

        private void ClearCheckedNodes() 
        {
            foreach (TreeNode nodeApplication in treeViewAccess.Nodes)
            {
                foreach (TreeNode nodeResource in nodeApplication.Nodes)
                {
                    foreach (TreeNode nodeAction in nodeResource.Nodes)
                    {
                        treeViewAccess.SetChecked(nodeAction, TriStateTreeView.CheckState.Unchecked); 
                    }
                }
            }
        }

        private void treeViewAccess_AfterCheck(object sender, TreeViewEventArgs e)
        {
            UserAccessClientData access = e.Node.Tag as UserAccessClientData;

            if (access != null)
            {
                TriStateTreeView.CheckState state = treeViewAccess.GetChecked(e.Node);

                if (state == TriStateTreeView.CheckState.Checked && SelectedAccesses.Contains(access) == false)
                {
                    SelectedAccesses.Add(access);
                    if (/*(access.UserPermission.UserAction.Name == UserActionClientData.Update.Name ||
                         access.UserPermission.UserAction.Name == UserActionClientData.Delete.Name) &&*/
                        e.Node.Parent.FirstNode.Checked == false)
                    {
                        treeViewAccess.SetChecked(e.Node.Parent.FirstNode, TriStateTreeView.CheckState.Checked);
                    }
                        //Cuando se escoge supervisor general se debe escoger los otros dos, si se esta cargando el arbol
                        //esto no debe ocurrir.
                    else if (access.UserPermission.UserResource.Name.Equals(UserResourceClientData.GeneralSupervisor.Name) &&
                        loading == false)
                    {
                        if (HasOneChecked(e.Node.Parent) == false)
                            foreach (TreeNode node in e.Node.Parent.Parent.Nodes)
                                if (node != e.Node.Parent)
                                    treeViewAccess.SetChecked(node, TriStateTreeView.CheckState.Checked);
                    }
                }
                else if (state == TriStateTreeView.CheckState.Unchecked && SelectedAccesses.Contains(access) == true)
                {
                    SelectedAccesses.Remove(access);
                    if (/*(access.UserPermission.UserAction.Name == UserActionClientData.Update.Name ||
                         access.UserPermission.UserAction.Name == UserActionClientData.Delete.Name) &&*/
                        CheckEditOrDeleteActive(e.Node.Parent) == false)
                    {
                        treeViewAccess.SetChecked(e.Node.Parent.FirstNode, TriStateTreeView.CheckState.Unchecked);
                    }
                    else if (access.UserPermission.UserAction.Name == UserActionClientData.Search.Name)
                    {
                        foreach (TreeNode node in e.Node.Parent.Nodes)
                        {
                            treeViewAccess.SetChecked(node, TriStateTreeView.CheckState.Unchecked);
                        }
                        
                       // treeViewAccess.SetChecked(e.Node.Parent.Nodes[3], TriStateTreeView.CheckState.Unchecked);
					}
					else if (access.UserPermission.UserResource.Name.Equals(UserResourceClientData.FirstLevelSupervisor.Name))
					{
						if (SelectedAccessesHasReourceClientData(UserResourceClientData.DispatchSupervisor) == false)
							foreach (TreeNode node in e.Node.Parent.Parent.Nodes)
								treeViewAccess.SetChecked(node, TriStateTreeView.CheckState.Unchecked);
					}
					else if (access.UserPermission.UserResource.Name.Equals(UserResourceClientData.DispatchSupervisor.Name))
					{
						if (SelectedAccessesHasReourceClientData(UserResourceClientData.FirstLevelSupervisor) == false)
							foreach (TreeNode node in e.Node.Parent.Parent.Nodes)
								treeViewAccess.SetChecked(node, TriStateTreeView.CheckState.Unchecked);
					}
				}
                ButtonsActivation(null, null);
            }
        }

		private bool SelectedAccessesHasReourceClientData(UserResourceClientData resource)
		{
			foreach (UserAccessClientData access in selectedAccesses)
				if (access.UserPermission.UserResource.Name.Equals(resource.Name))
					return true;
			return false;
		}
		
		private bool HasOneChecked(TreeNode node)
		{
			foreach (TreeNode child in node.Parent.Nodes)
				if (child!= node &&
					(child.ImageIndex == (int)TriStateTreeView.CheckState.Checked ||
					child.ImageIndex == (int)TriStateTreeView.CheckState.GreyChecked))
					return true;
			return false;
		}

        private bool CheckEditOrDeleteActive(TreeNode node)
        {
            bool result = false;
            for (int i = 0; i < node.Nodes.Count && result == false; i++)
            { 
                TreeNode child = node.Nodes[i];
                TriStateTreeView.CheckState state = treeViewAccess.GetChecked(child);
                //UserAccessClientData access = child.Tag as UserAccessClientData;
                //if (access.UserPermission.UserAction.Name == UserActionClientData.Delete.Name ||
                //    access.UserPermission.UserAction.Name == UserActionClientData.Update.Name)
                //{
                    result |= state == TriStateTreeView.CheckState.Checked ? true : false;
                //}
            }
            return result;
        }

		private void treeViewAccess_Enter(object sender, EventArgs e)
        {
            treeViewAccess.SelectedNode = treeViewAccess.TopNode;
        }

        private void ProfileForm_Load(object sender, EventArgs e)
        {
            this.Icon = ResourceLoader.GetIcon("$Icon.Perfil");
            if (behavior == FormBehavior.Create)
            {
                Text = ResourceLoader.GetString2("ProfileFormCreateText");
                buttonExOK.Text = ResourceLoader.GetString("ProfileFormCreateButtonOkText");
            }
            else if (behavior == FormBehavior.Edit)
            {
                Text = ResourceLoader.GetString2("ProfileFormEditText");
                buttonExOK.Text = ResourceLoader.GetString("ProfileFormEditButtonOkText");


                //Acomodar si el administrador tiene otro nombre que Administrator.
                //string admin = ResourceLoader.GetString("SuperUserName");
                //if (selectedProfile.Name.CompareTo(admin) == 0) 
                if (selectedProfile != null && (bool)selectedProfile.Immutable)
                {
                    this.textBoxDescription.Enabled = false;
                    this.textBoxName.Enabled = false;
                    this.treeViewAccess.Enabled = false;
                    int temp = buttonExOK.Left;
                    buttonExOK.Left = buttonExCancel.Left;
                    buttonExCancel.Left = temp;
                    buttonExOK.Text = ResourceLoader.GetString("ProfileFormEditButtonCancelText");
                    buttonExOK.Enabled = false;
                    buttonExCancel.Text = ResourceLoader.GetString("ProfileFormEditButtonOkText");
                }
            }
        }

        private void ProfileForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.parentForm != null)
            {
                this.parentForm.AdministrationCommittedChanges -= new EventHandler<CommittedObjectDataCollectionEventArgs>(ProfileForm_AdministrationCommittedChanges);
            }
        }       
    }
}
