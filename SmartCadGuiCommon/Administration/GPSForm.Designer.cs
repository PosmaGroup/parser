using SmartCadControls;
namespace SmartCadGuiCommon
{
    partial class GPSForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GPSForm));
            this.layoutControlGPS = new DevExpress.XtraLayout.LayoutControl();
            this.textEditId = new DevExpress.XtraEditors.TextEdit();
            this.simpleButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonAccept = new DevExpress.XtraEditors.SimpleButton();
            this.gridControlExIOs = new GridControlEx();
            this.gridViewExIOs = new GridViewEx();
            this.comboBoxEditModel = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEditBrand = new DevExpress.XtraEditors.ComboBoxEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroupIOs = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupData = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemBrand = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemModel = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemId = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGPS)).BeginInit();
            this.layoutControlGPS.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditId.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExIOs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExIOs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditModel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditBrand.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupIOs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemBrand)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemModel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemId)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControlGPS
            // 
            this.layoutControlGPS.AllowCustomizationMenu = false;
            this.layoutControlGPS.Controls.Add(this.textEditId);
            this.layoutControlGPS.Controls.Add(this.simpleButtonCancel);
            this.layoutControlGPS.Controls.Add(this.simpleButtonAccept);
            this.layoutControlGPS.Controls.Add(this.gridControlExIOs);
            this.layoutControlGPS.Controls.Add(this.comboBoxEditModel);
            this.layoutControlGPS.Controls.Add(this.comboBoxEditBrand);
            this.layoutControlGPS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlGPS.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGPS.Name = "layoutControlGPS";
            this.layoutControlGPS.Root = this.layoutControlGroup1;
            this.layoutControlGPS.Size = new System.Drawing.Size(442, 389);
            this.layoutControlGPS.TabIndex = 0;
            this.layoutControlGPS.Text = "layoutControl1";
            // 
            // textEditId
            // 
            this.textEditId.Location = new System.Drawing.Point(82, 27);
            this.textEditId.Name = "textEditId";
            this.textEditId.Properties.MaxLength = 20;
            this.textEditId.Size = new System.Drawing.Size(353, 20);
            this.textEditId.StyleController = this.layoutControlGPS;
            this.textEditId.TabIndex = 10;
            this.textEditId.TextChanged += new System.EventHandler(this.CheckButtonsEventHandler);
            // 
            // simpleButtonCancel
            // 
            this.simpleButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonCancel.Location = new System.Drawing.Point(358, 355);
            this.simpleButtonCancel.Name = "simpleButtonCancel";
            this.simpleButtonCancel.Size = new System.Drawing.Size(82, 32);
            this.simpleButtonCancel.StyleController = this.layoutControlGPS;
            this.simpleButtonCancel.TabIndex = 9;
            this.simpleButtonCancel.Text = "Cancelar";
            // 
            // simpleButtonAccept
            // 
            this.simpleButtonAccept.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.simpleButtonAccept.Location = new System.Drawing.Point(272, 355);
            this.simpleButtonAccept.Name = "simpleButtonAccept";
            this.simpleButtonAccept.Size = new System.Drawing.Size(82, 32);
            this.simpleButtonAccept.StyleController = this.layoutControlGPS;
            this.simpleButtonAccept.TabIndex = 8;
            this.simpleButtonAccept.Text = "Accept";
            this.simpleButtonAccept.Click += new System.EventHandler(this.simpleButtonAccept_Click);
            // 
            // gridControlExIOs
            // 
            this.gridControlExIOs.EnableAutoFilter = false;
            this.gridControlExIOs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExIOs.Location = new System.Drawing.Point(7, 129);
            this.gridControlExIOs.MainView = this.gridViewExIOs;
            this.gridControlExIOs.Name = "gridControlExIOs";
            this.gridControlExIOs.Size = new System.Drawing.Size(428, 217);
            this.gridControlExIOs.TabIndex = 7;
            this.gridControlExIOs.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExIOs});
            this.gridControlExIOs.ViewTotalRows = false;
            // 
            // gridViewExIOs
            // 
            this.gridViewExIOs.AllowFocusedRowChanged = true;
            this.gridViewExIOs.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewExIOs.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExIOs.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewExIOs.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewExIOs.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewExIOs.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExIOs.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewExIOs.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewExIOs.EnablePreviewLineForFocusedRow = false;
            this.gridViewExIOs.GridControl = this.gridControlExIOs;
            this.gridViewExIOs.Name = "gridViewExIOs";
            this.gridViewExIOs.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExIOs.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewExIOs.OptionsView.ShowGroupPanel = false;
            this.gridViewExIOs.ViewTotalRows = false;
            this.gridViewExIOs.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridViewExIOs_ShowingEditor);
            // 
            // comboBoxEditModel
            // 
            this.comboBoxEditModel.Location = new System.Drawing.Point(82, 75);
            this.comboBoxEditModel.Name = "comboBoxEditModel";
            this.comboBoxEditModel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditModel.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEditModel.Size = new System.Drawing.Size(353, 20);
            this.comboBoxEditModel.StyleController = this.layoutControlGPS;
            this.comboBoxEditModel.TabIndex = 6;
            this.comboBoxEditModel.SelectedIndexChanged += new System.EventHandler(this.comboBoxEditModel_SelectedIndexChanged);
            // 
            // comboBoxEditBrand
            // 
            this.comboBoxEditBrand.Location = new System.Drawing.Point(82, 51);
            this.comboBoxEditBrand.Name = "comboBoxEditBrand";
            this.comboBoxEditBrand.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditBrand.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEditBrand.Size = new System.Drawing.Size(353, 20);
            this.comboBoxEditBrand.StyleController = this.layoutControlGPS;
            this.comboBoxEditBrand.TabIndex = 5;
            this.comboBoxEditBrand.SelectedIndexChanged += new System.EventHandler(this.comboBoxEditBrand_SelectedIndexChanged);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.emptySpaceItem1,
            this.layoutControlGroupIOs,
            this.layoutControlGroupData});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(442, 389);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.simpleButtonAccept;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(270, 353);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.simpleButtonCancel;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(356, 353);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 353);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(270, 36);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroupIOs
            // 
            this.layoutControlGroupIOs.CustomizationFormText = "layoutControlGroupIOs";
            this.layoutControlGroupIOs.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.layoutControlGroupIOs.Location = new System.Drawing.Point(0, 102);
            this.layoutControlGroupIOs.Name = "layoutControlGroupIOs";
            this.layoutControlGroupIOs.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupIOs.Size = new System.Drawing.Size(442, 251);
            this.layoutControlGroupIOs.Text = "Entradas/Salidas";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.gridControlExIOs;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(432, 221);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlGroupData
            // 
            this.layoutControlGroupData.CustomizationFormText = "layoutControlGroupData";
            this.layoutControlGroupData.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemBrand,
            this.layoutControlItemModel,
            this.layoutControlItemId});
            this.layoutControlGroupData.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupData.Name = "layoutControlGroupData";
            this.layoutControlGroupData.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupData.Size = new System.Drawing.Size(442, 102);
            this.layoutControlGroupData.Text = "Datos";
            // 
            // layoutControlItemBrand
            // 
            this.layoutControlItemBrand.Control = this.comboBoxEditBrand;
            this.layoutControlItemBrand.CustomizationFormText = "layoutControlItemBrand";
            this.layoutControlItemBrand.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItemBrand.Name = "layoutControlItemBrand";
            this.layoutControlItemBrand.Size = new System.Drawing.Size(432, 24);
            this.layoutControlItemBrand.Text = "Marca:*";
            this.layoutControlItemBrand.TextSize = new System.Drawing.Size(71, 13);
            // 
            // layoutControlItemModel
            // 
            this.layoutControlItemModel.Control = this.comboBoxEditModel;
            this.layoutControlItemModel.CustomizationFormText = "layoutControlItemModel";
            this.layoutControlItemModel.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItemModel.Name = "layoutControlItemModel";
            this.layoutControlItemModel.Size = new System.Drawing.Size(432, 24);
            this.layoutControlItemModel.Text = "Modelo:*";
            this.layoutControlItemModel.TextSize = new System.Drawing.Size(71, 13);
            // 
            // layoutControlItemId
            // 
            this.layoutControlItemId.Control = this.textEditId;
            this.layoutControlItemId.CustomizationFormText = "layoutControlItemId";
            this.layoutControlItemId.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemId.Name = "layoutControlItemId";
            this.layoutControlItemId.Size = new System.Drawing.Size(432, 24);
            this.layoutControlItemId.Text = "Identificador:*";
            this.layoutControlItemId.TextSize = new System.Drawing.Size(71, 13);
            // 
            // GPSForm
            // 
            this.AcceptButton = this.simpleButtonAccept;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.simpleButtonCancel;
            this.ClientSize = new System.Drawing.Size(442, 389);
            this.Controls.Add(this.layoutControlGPS);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "GPSForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Modulo de Administracion - Crear GPS";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGPS)).EndInit();
            this.layoutControlGPS.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEditId.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExIOs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExIOs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditModel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditBrand.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupIOs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemBrand)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemModel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemId)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControlGPS;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCancel;
        private DevExpress.XtraEditors.SimpleButton simpleButtonAccept;
        private GridControlEx gridControlExIOs;
        private GridViewEx gridViewExIOs;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditModel;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditBrand;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemBrand;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemModel;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupIOs;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupData;
        private DevExpress.XtraEditors.TextEdit textEditId;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemId;

    }
}