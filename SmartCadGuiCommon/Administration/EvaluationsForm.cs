using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SmartCadCore.Core;
using System.Collections;
using Smartmatic.SmartCad.Service;
using System.Reflection;
using System.ServiceModel;
using SmartCadCore.ClientData;
using DevExpress.XtraEditors.Repository;
using SmartCadCore.Common;
using SmartCadCore.Enums;
using SmartCadControls.Controls;
using SmartCadControls;
using SmartCadGuiCommon.Controls.Administration;

namespace SmartCadGuiCommon
{
    public partial class EvaluationsForm : DevExpress.XtraEditors.XtraForm
    {
        EvaluationClientData selectedEvaluation = null;
        FormBehavior behaviorType = FormBehavior.Edit;
        private bool questionsAreOk = false;
        private bool isDuplicate = false;
        private AdministrationRibbonForm parentForm;
        private object syncCommitted = new object();
        private RepositoryItemSpinEdit maxValueSpinEdit = new RepositoryItemSpinEdit();

        public EvaluationsForm(AdministrationRibbonForm form)
        {
            InitializeComponent();
            this.FormClosing += new FormClosingEventHandler(EvaluationsForm_FormClosing);
            AssignDataGridType();
            FillControls();
            this.parentForm = form;
            this.tabbedControlGroupEvaluation.SelectedTabPage = layoutControlGroupEvaluation;
            if (this.parentForm != null)
            {
                this.parentForm.AdministrationCommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(EvaluationsForm_AdministrationCommittedChanges);
            }
        }

        private void LoadLanguage() 
        {
            this.Icon = ResourceLoader.GetIcon("$Icon.EvaluationOperator");
            if (behaviorType == FormBehavior.Create && isDuplicate == false)
            {
                Text = ResourceLoader.GetString2("EvaluationFormCreateText");
                buttonExAccept.Text = ResourceLoader.GetString2("EvaluationFormCreateButtonOkText");
                buttonExCancel.Text = ResourceLoader.GetString2("EvaluationFormCreateButtonCancelText");
            }
            else if (behaviorType == FormBehavior.Edit && isDuplicate == false)
            {
                Text = ResourceLoader.GetString2("EvaluationFormEditText");
                buttonExAccept.Text = ResourceLoader.GetString2("EvaluationFormEditButtonOkText");
                //buttonExAccept.Text = ResourceLoader.GetString2("Create");
                buttonExCancel.Text = ResourceLoader.GetString2("EvaluationFormEditButtonCancelText");
            }
            else if (behaviorType == FormBehavior.Create && isDuplicate == true)
            {
                Text = ResourceLoader.GetString2("EvaluationFormDuplicateText");
                buttonExAccept.Text = ResourceLoader.GetString2("EvaluationFormCreateButtonOkText");
                buttonExCancel.Text = ResourceLoader.GetString2("EvaluationFormCreateButtonCancelText");
            }

            this.toolStripButton1.ToolTipText = ResourceLoader.GetString2("AddQuestion");
            this.toolStripButton2.ToolTipText = ResourceLoader.GetString2("DeleteQuestion");
            this.layoutControlGroupApplyTo.Text = ResourceLoader.GetString2("ApplyTo");
            this.layoutControlGroupEvalInformation.Text = ResourceLoader.GetString2("EvaluationInfo");
            this.layoutControlGroupQuestion.Text = ResourceLoader.GetString2("Questions");
            this.layoutControlGroupEvaluation.Text = ResourceLoader.GetString2("Evaluation");

            this.layoutControlItemApplications.Text = ResourceLoader.GetString2("OperatorsAccess") + ":*";
            this.layoutControlItemDepartments.Text = ResourceLoader.GetString2("EvaluationDepartamentsType") + ":";
            this.layoutControlItemDesc.Text = ResourceLoader.GetString2("ScaleDescription") + ":";
            this.layoutControlItemName.Text = ResourceLoader.GetString2("Name") + ":*";
            this.layoutControlItemOpeCat.Text = ResourceLoader.GetString2("EvaluationOperatorCategory") + ":*";
            this.layoutControlItemScale.Text = ResourceLoader.GetString2("ScaleToUse") + ":*";
            this.checkBoxExPon.Text = ResourceLoader.GetString2("EqualWeighting");
            this.label1.Text = ResourceLoader.GetString2("Total") + ":";
            
        }

        void EvaluationsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.parentForm != null)
            {
                this.parentForm.AdministrationCommittedChanges -= new EventHandler<CommittedObjectDataCollectionEventArgs>(EvaluationsForm_AdministrationCommittedChanges);
            }
        }

        void EvaluationsForm_AdministrationCommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
        {
            try
            {
                if (e.Objects != null && e.Objects.Count > 0 && closing == false)
                {
                    lock (syncCommitted)
                    {
                        try
                        {
                            if (ServerServiceClient.GetInstance().OperatorClient == null)
                            { }
                        }
                        catch
                        { }

                        if (e.Objects[0] is EvaluationClientData)
                        {
                            #region EvaluationClientData
                            EvaluationClientData evaluation =
                                e.Objects[0] as EvaluationClientData;

                            if (SelectedEvaluation != null && SelectedEvaluation.Equals(evaluation) && ButtonAcceptPressed == false)
                            {
                                if (e.Action == CommittedDataAction.Update)
                                {
                                    MessageForm.Show(ResourceLoader.GetString2("UpdateFormEvaluationData"), MessageFormType.Warning);
                                    FormUtil.InvokeRequired(this,
                                    delegate
                                    {
                                        SelectedEvaluation = evaluation;
                                    });
                                }
                                else if (e.Action == CommittedDataAction.Delete)
                                {
                                    MessageForm.Show(ResourceLoader.GetString2("DeleteFormEvaluationData"), MessageFormType.Warning);
                                    FormUtil.InvokeRequired(this,
                                    delegate
                                    {
                                        this.Close();
                                    });
                                }

                            }
                            #endregion
                        }
                        else if (e.Objects[0] is DepartmentTypeClientData)
                        {
                            #region DepartmentTypeClientData
                            DepartmentTypeClientData departmentType = e.Objects[0] as DepartmentTypeClientData;
                            if (e.Action == CommittedDataAction.Save)
                            {
                                FormUtil.InvokeRequired(this.checkedListBoxDepartaments,
                               delegate
                               {
                                   EvaluationDepartmentTypeClientData evalDep = new EvaluationDepartmentTypeClientData();
                                   evalDep.Department = departmentType;
                                   checkedListBoxDepartaments.BeginUpdate();
                                   checkedListBoxDepartaments.Items.Add(evalDep);
                                   checkedListBoxDepartaments.EndUpdate();
                               });
                            }
                            else if (e.Action == CommittedDataAction.Update)
                            {
                                FormUtil.InvokeRequired(this.checkedListBoxDepartaments,
                               delegate
                               {
                                   EvaluationDepartmentTypeClientData evalDep = new EvaluationDepartmentTypeClientData();
                                   evalDep.Department = departmentType;
                                   int index = checkedListBoxDepartaments.Items.IndexOf(evalDep);
                                   if (index != -1)
                                   {
                                       checkedListBoxDepartaments.Items[index] = evalDep;
                                   }
                               });
                            }
                            #endregion
                        } 
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        public EvaluationsForm(AdministrationRibbonForm form,
            EvaluationClientData evaluation, FormBehavior type, bool duplicate)
            : this(form)
        {    
            BehaviorType = type;
            SelectedEvaluation = evaluation;
            isDuplicate = duplicate;
            LoadLanguage();
        }

        public FormBehavior BehaviorType
        {
            get
            {
                return behaviorType;
            }
            set
            {
                behaviorType = value;              
            }
        }

        public EvaluationClientData SelectedEvaluation
        {
            get
            {
                return selectedEvaluation;
            }
            set
            {
                Clear();
                selectedEvaluation = value;
                if (selectedEvaluation != null)
                {
                    SetCurrentEvaluation();
                }
                EvaluationParameters_Change(null, null);
                checkedListBoxApplications_SelectedIndexChanged(null, null);
            }
        }

        private void SetCurrentEvaluation()
        {
            FormUtil.InvokeRequired(this,
                delegate
                {
                    int i = 0;
                    textBoxExEvaluationName.Text = SelectedEvaluation.Name;
                    textBoxExDesc.Text = selectedEvaluation.Description;
                    comboBoxExScale.SelectedItem = GetScaleName(selectedEvaluation.Scale);
                    comboBoxExOperatorCat.SelectedItem = selectedEvaluation.OperatorCategory;
                    checkedListBoxApplications.ClearCheckedItems();
                    foreach (EvaluationUserApplicationClientData selectedEvalApp in selectedEvaluation.Applications)
                    {
                        i = 0;
                        foreach (EvaluationUserApplicationClientData evalApp in checkedListBoxApplications.Items)
                        {
                            if (evalApp.Application.FriendlyName == selectedEvalApp.Application.FriendlyName)
                            {
                                evalApp.Code = selectedEvalApp.Code;
                                evalApp.Evaluation = selectedEvalApp.Evaluation;
                                checkedListBoxApplications.SetItemChecked(i, true);
                                break;
                            }
                            i++;
                        }
                    }

                    checkedListBoxDepartaments.ClearCheckedItems();
                    foreach (EvaluationDepartmentTypeClientData selectedEvalDep in selectedEvaluation.Departments)
                    {
                        i = 0;
                        foreach (EvaluationDepartmentTypeClientData evalDep in checkedListBoxDepartaments.Items)
                        {
                            if (selectedEvalDep.Department != null && evalDep.Department != null &&
                                evalDep.Department.Name == selectedEvalDep.Department.Name)
                            {
                                evalDep.Code = selectedEvalDep.Code;
                                evalDep.Evaluation = selectedEvalDep.Evaluation;
                                int index = checkedListBoxDepartaments.Items.IndexOf(evalDep);
                                checkedListBoxDepartaments.SetItemChecked(index, true);
                                break;
                            }
                            i++;
                        }
                    }

                    bool equal = true;
                    float weighting = 0;

                    for (int j = 0; j < selectedEvaluation.Questions.Count; j++)
                    {
                        EvaluationQuestionClientData question = selectedEvaluation.Questions[j] as EvaluationQuestionClientData;
                        gridControlExQuestions.AddOrUpdateItem(new GridControlDataEvaluationQuestion(question));

                        if (j == 0)
                            weighting = question.Weighting;

                        if (weighting != question.Weighting && equal == true)
                            equal = false;

                    }

                    if (equal == true)
                    {
                        checkBoxExPon.CheckedChanged -= new EventHandler(checkBoxExPon_CheckedChanged);
                        checkBoxExPon.Checked = true;
                        checkBoxExPon.CheckedChanged += new EventHandler(checkBoxExPon_CheckedChanged);
                    }
                });
        }

      
      
        private void Clear()
        {
            textBoxExEvaluationName.Clear();
            textBoxExDesc.Clear();
            comboBoxExScale.SelectedIndex = -1;
            comboBoxExOperatorCat.SelectedIndex = -1;
            checkedListBoxDepartaments.ClearSelected();
            checkedListBoxApplications.ClearSelected();
            gridControlExQuestions.ClearData();
          
        }


        private void AssignDataGridType()
        {
            gridControlExQuestions.Type = typeof(GridControlDataEvaluationQuestion);
            gridViewExQuestions.OptionsBehavior.Editable = true;
            gridViewExQuestions.Columns["Name"].OptionsColumn.AllowEdit = true;
            gridViewExQuestions.Columns["Weighting"].OptionsColumn.AllowEdit = true;
            gridControlExQuestions.EnableAutoFilter = true;

            maxValueSpinEdit.MaxValue = 100;
            maxValueSpinEdit.MinValue = 0;
            maxValueSpinEdit.EditMask = "f";
            gridViewExQuestions.Columns["Weighting"].ColumnEdit = maxValueSpinEdit;
          
        }

     

        private void EvaluationParameters_Change(object sender, System.EventArgs e)
        {
            EvaluationUserApplicationClientData disp = new EvaluationUserApplicationClientData();
            disp.Application = UserApplicationClientData.Dispatch;
            EvaluationUserApplicationClientData sup = new EvaluationUserApplicationClientData();
            sup.Application = UserApplicationClientData.Supervision;          
            
            ValidateQuestions();
            if (string.IsNullOrEmpty(textBoxExEvaluationName.Text.Trim()) || 
                comboBoxExOperatorCat.SelectedItem == null || 
                comboBoxExScale.SelectedItem == null || 
                checkedListBoxApplications.CheckedIndices.Count == 0 || 
                questionsAreOk == false || 
                (
                (checkedListBoxApplications.CheckedItems.Contains(disp) == true ||
                checkedListBoxApplications.CheckedItems.Contains(sup) == true)&&
                checkedListBoxDepartaments.CheckedIndices.Count == 0
                ))

                buttonExAccept.Enabled = false;
            else
                buttonExAccept.Enabled = true;
        }

        private void FillControls()
        {
            FillCheckedListBoxApplications();
            FillCheckedListBoxDepartaments();
            FillComboBoxExScale();
            FillcomboBoxExOperatorCat();
        }

        private void FillCheckedListBoxApplications()
        {
            EvaluationUserApplicationClientData evalApp = new EvaluationUserApplicationClientData();
            evalApp.Application = UserApplicationClientData.Dispatch;
            checkedListBoxApplications.Items.Add(evalApp);

            evalApp = new EvaluationUserApplicationClientData();
            evalApp.Application = UserApplicationClientData.FirstLevel;
            checkedListBoxApplications.Items.Add(evalApp);

            //evalApp = new EvaluationUserApplicationClientData();
            //evalApp.Application = UserApplicationClientData.Cctv;
            //checkedListBoxApplications.Items.Add(evalApp);
        }

        private void FillCheckedListBoxDepartaments()
        {
            IList departaments = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetDepartmentsType);
            foreach (DepartmentTypeClientData departament in departaments)
            {
                EvaluationDepartmentTypeClientData evalDep = new EvaluationDepartmentTypeClientData();
                evalDep.Department = departament;
                checkedListBoxDepartaments.Items.Add(evalDep);
            }
        }

        private void FillcomboBoxExOperatorCat()
        {
            IList categories = ServerServiceClient.GetInstance().SearchClientObjectsMaxRows(SmartCadHqls.GetOperatorCategories, true);
            foreach (OperatorCategoryClientData category in categories)
            {
                comboBoxExOperatorCat.Items.Add(category);
            }
        }

        private void FillComboBoxExScale()
        {
            comboBoxExScale.Items.Add(GetScaleName(EvaluationClientData.EvaluationScales.OneToThree));
            comboBoxExScale.Items.Add(GetScaleName(EvaluationClientData.EvaluationScales.OneToFive));
            comboBoxExScale.Items.Add(GetScaleName(EvaluationClientData.EvaluationScales.YesNo));
        }

        private string GetScaleName(EvaluationClientData.EvaluationScales s)
        {
            if (s == EvaluationClientData.EvaluationScales.OneToThree)
            {
                return ResourceLoader.GetString2("Scale.OneToThree");
            }
            else if (s == EvaluationClientData.EvaluationScales.OneToFive)
            {
                return ResourceLoader.GetString2("Scale.OneToFive");
            }
            else if (s == EvaluationClientData.EvaluationScales.YesNo)
            {
                return ResourceLoader.GetString2("Scale.YesNo");
            }
            else
            {
                return string.Empty;
            }
        }
             
        private bool buttonAcceptPressed = false;

        public bool ButtonAcceptPressed
        {
            get { return buttonAcceptPressed; }
        }

        private void buttonExAccept_Click(object sender, EventArgs e)
        {
            try
            {
                BackgroundProcessForm processForm = new BackgroundProcessForm(this, new MethodInfo[1] { GetType().GetMethod("buttonOk_Click1", BindingFlags.NonPublic | BindingFlags.Instance) }, new object[1][] { new object[0] });
                processForm.CanThrowError = true;
                processForm.ShowDialog();
            }
            catch (FaultException ex)
            {
                if (behaviorType == FormBehavior.Edit)
                {
                    SelectedEvaluation = (EvaluationClientData)ServerServiceClient.GetInstance().RefreshClient(SelectedEvaluation,true);
                }
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, MessageFormType.Error);
                buttonAcceptPressed = false;
            }
            catch (Exception ex)
            {
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, ex);
                buttonAcceptPressed = false;
            }
        }

        bool closing = false;
        private void buttonOk_Click1()
        {
            buttonAcceptPressed = true;
            if (behaviorType == FormBehavior.Create)
            {
                selectedEvaluation = new EvaluationClientData(textBoxExEvaluationName.Text);
                selectedEvaluation.Departments = new List<EvaluationDepartmentTypeClientData>();
                selectedEvaluation.Applications = new List<EvaluationUserApplicationClientData>();
                selectedEvaluation.Questions = new List<EvaluationQuestionClientData>();
            }
            if (selectedEvaluation != null)
            {
                FormUtil.InvokeRequired(this,
                    delegate
                    {
                        selectedEvaluation.Name = textBoxExEvaluationName.Text;
                        selectedEvaluation.Scale = (EvaluationClientData.EvaluationScales)comboBoxExScale.Tag;
                        selectedEvaluation.Description = textBoxExDesc.Text;
                        selectedEvaluation.OperatorCategory = (OperatorCategoryClientData)comboBoxExOperatorCat.SelectedItem;

                        GetDepartments();
                        GetApplications();
                        GetQuestions();
                    });
                closing = true;
                ServerServiceClient.GetInstance().SaveOrUpdateClientData(selectedEvaluation);
            }
        }

        private void GetDepartments()
        {
            IList tempList = new ArrayList(selectedEvaluation.Departments);
            foreach (EvaluationDepartmentTypeClientData dep in tempList)
            {
                if (checkedListBoxDepartaments.CheckedItems.Contains(dep) ==  false)
                {
                    //ServerServiceClient.GetInstance().DeleteClientObject(dep);
                    selectedEvaluation.Departments.Remove(dep);
                }
            }

            foreach (EvaluationDepartmentTypeClientData dep in checkedListBoxDepartaments.CheckedItems)
            {
                if (dep.Code == 0)
                {
                    EvaluationDepartmentTypeClientData depart = new EvaluationDepartmentTypeClientData();
                    depart.Department = dep.Department;
                    depart.Evaluation = selectedEvaluation;
                    selectedEvaluation.Departments.Add(depart);
                }
            }
        }

        private void GetApplications()
        {
            IList tempList = new ArrayList(selectedEvaluation.Applications);
            foreach (EvaluationUserApplicationClientData app in tempList)
            {
                if (checkedListBoxApplications.CheckedItems.Contains(app) == false)
                {
                    ServerServiceClient.GetInstance().DeleteClientObject(app);
                    selectedEvaluation.Applications.Remove(app);
                }
            }

            foreach (EvaluationUserApplicationClientData app in checkedListBoxApplications.CheckedItems)
            {
                if (app.Code == 0)
                {
                    EvaluationUserApplicationClientData application = new EvaluationUserApplicationClientData();
                    application.Application = app.Application;
                    application.Evaluation = selectedEvaluation;
                    selectedEvaluation.Applications.Add(application);
                }
            }
        }

        private void GetQuestions()
        {
            selectedEvaluation.Questions = new List<EvaluationQuestionClientData>();
            foreach (GridControlDataEvaluationQuestion gridEvalQues in gridControlExQuestions.Items)
            {
                if (gridEvalQues.Question != null)
                {
                    if (gridEvalQues.Question.Code < 0)
                        gridEvalQues.Question.Code = 0;                 
                    selectedEvaluation.Questions.Add(gridEvalQues.Question);
                }
            }
        }

        private int len = 0;
        private int codeTemp = -1;
        private void toolStripButton1_Click_1(object sender, EventArgs e)
        {
            EvaluationQuestionClientData question = new EvaluationQuestionClientData();
            question.Code = codeTemp;
            codeTemp--;
            question.Name = ResourceLoader.GetString2("NewEvaluationQuestion") + " " + len;
            GridControlDataEvaluationQuestion gridData = new GridControlDataEvaluationQuestion(question);

            gridControlExQuestions.AddOrUpdateItem(new GridControlDataEvaluationQuestion(question), true);
            
            len++;

            ReCalculateWeight();

            if (gridControlExQuestions.Items.Count >= 100)
                toolStripButton1.Enabled = false;
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridControlExQuestions.SelectedItems.Count > 0)
                {
                    foreach (GridControlDataEvaluationQuestion gridData in gridControlExQuestions.SelectedItems)
                        gridControlExQuestions.DeleteItem(gridData);
                    
                }
            }
            catch { }

            if (gridControlExQuestions.Items.Count > 0)
                gridViewExQuestions.FocusedRowHandle = 0;

            ReCalculateWeight();
  

            if (gridControlExQuestions.Items.Count < 100)
                toolStripButton1.Enabled = true;
        }

        private void checkBoxExPon_CheckedChanged(object sender, EventArgs e)
        {
            ReCalculateWeight();
			if (((CheckBox)sender).Checked == true)
			{
                gridViewExQuestions.Columns["Weighting"].OptionsColumn.AllowEdit = false;
			}
			else
			{
                gridViewExQuestions.Columns["Weighting"].OptionsColumn.AllowEdit = true;
			}
        }

        private void ReCalculateWeight()
        {
            if (checkBoxExPon.Checked == true)
            {
                questionsAreOk = true;
                float questions = this.gridControlExQuestions.Items.Count;
                
                if (questions > 0)
                {
                    float weight = 100 / questions;

                  //  gridViewExQuestions.CellValueChanging -= new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(gridViewExQuestions_CellValueChanging);
                    gridViewExQuestions.CellValueChanged -= new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(gridViewExQuestions_CellValueChanged);
                    gridViewExQuestions.BeginUpdate();
                    foreach (GridControlDataEvaluationQuestion gridData in gridControlExQuestions.Items) {
                        gridData.Weighting = weight;                   
                    }
                    gridViewExQuestions.EndUpdate();
                   gridViewExQuestions.CellValueChanged +=new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(gridViewExQuestions_CellValueChanged);
                 //   gridViewExQuestions.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(gridViewExQuestions_CellValueChanging);
                
                }
            }
            EvaluationParameters_Change(null, null);
        }

        

        private void ValidateQuestions()
        {
            float sum = 0;
            foreach (GridControlDataEvaluationQuestion gridData in gridControlExQuestions.Items)
            {
                sum += gridData.Weighting;
            }

            label2.Text = string.Format("{0:0}", sum) + "%";
            questionsAreOk = (Convert.ToInt32(sum) == 100);
        }


        private void comboBoxExScale_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxExScale.SelectedIndex == 0)
            {
                comboBoxExScale.Tag = EvaluationClientData.EvaluationScales.OneToThree;
                textBoxExDesc.Text = ResourceLoader.GetString2("Scale.OneToThreeDescription");
            }
            else if (comboBoxExScale.SelectedIndex == 1)
            {
                comboBoxExScale.Tag = EvaluationClientData.EvaluationScales.OneToFive;
                textBoxExDesc.Text = ResourceLoader.GetString2("Scale.OneToFiveDescription");
            }
            else
            {
                comboBoxExScale.Tag = EvaluationClientData.EvaluationScales.YesNo;
                textBoxExDesc.Text = ResourceLoader.GetString2("Scale.YesNoDescription");
            }

            EvaluationParameters_Change(null, null);
        }


        private void gridViewExQuestions_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            EvaluationParameters_Change(null, null);
            if (questionsAreOk == false)
            {
                try
                {
                    if (e.Column != null && e.Column.FieldName == "Weighting")
                        checkBoxExPon.Checked = false;

                }
                catch { }
            }
        }

        void gridViewExQuestions_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            //EvaluationParameters_Change(null, null);
            //if (questionsAreOk == false)
            //{
            //    try
            //    {
            //        if (e.Column != null && e.Column.FieldName == "Weighting")
            //            checkBoxExPon.Checked = false;

            //    }
            //    catch { }
            //}
        }

        private void EvaluationsForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (tabbedControlGroupEvaluation.SelectedTabPageIndex == 1)
            {
                if (e.KeyData == (Keys.Control | Keys.A))
                {
                    toolStripButton1_Click_1(null, null);
                }
                else if (e.KeyData == (Keys.Control | Keys.E))
                {
                    toolStripButton2_Click(null, null);
                }
            }
        }

        private void checkedListBoxApplications_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (checkedListBoxApplications.CheckedItems.Count > 0)
            {
                EvaluationUserApplicationClientData disp = new EvaluationUserApplicationClientData();
                disp.Application = UserApplicationClientData.Dispatch;

                if (checkedListBoxApplications.CheckedItems.Contains(disp) == true)
                {
                    checkedListBoxDepartaments.Enabled = true;
                }
                else
                {
                    checkedListBoxDepartaments.ClearCheckedItems();
                    checkedListBoxDepartaments.Enabled = false;
                }

                EvaluationParameters_Change(null, null);
            }
            else
            {
                checkedListBoxDepartaments.ClearCheckedItems();
                checkedListBoxDepartaments.Enabled = false;
            }
        }

        private void EvaluationsForm_Load(object sender, EventArgs e)
        {
            textBoxExEvaluationName.Focus();
            this.Width = 484;
            this.Height = 578;
           
        }

       
    }

}