using SmartCadControls;
namespace SmartCadGuiCommon
{
	partial class WorkShiftRouteForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.gridControlExWSRoutes = new GridControlEx();
            this.gridViewExWSRoutes = new GridViewEx();
            this.gridControlExRoutes = new GridControlEx();
            this.gridViewExRoutes = new GridViewEx();
            this.gridControlExWorkShift = new GridControlEx();
            this.gridViewExWorkShift = new GridViewEx();
            this.simpleButtonClean = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonAccept = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonDelete = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonAdd = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroupWSAndRoutes = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroupWSRoutes = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroupRoutes = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupWorkShift = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExWSRoutes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExWSRoutes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExRoutes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExRoutes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExWorkShift)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExWorkShift)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupWSAndRoutes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupWSRoutes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupRoutes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupWorkShift)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.AllowCustomizationMenu = false;
            this.layoutControl1.Controls.Add(this.gridControlExWSRoutes);
            this.layoutControl1.Controls.Add(this.gridControlExRoutes);
            this.layoutControl1.Controls.Add(this.gridControlExWorkShift);
            this.layoutControl1.Controls.Add(this.simpleButtonClean);
            this.layoutControl1.Controls.Add(this.simpleButtonAccept);
            this.layoutControl1.Controls.Add(this.simpleButtonDelete);
            this.layoutControl1.Controls.Add(this.simpleButtonAdd);
            this.layoutControl1.Controls.Add(this.simpleButtonCancel);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroupWSAndRoutes;
            this.layoutControl1.Size = new System.Drawing.Size(706, 454);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // gridControlExWSRoutes
            // 
            this.gridControlExWSRoutes.EnableAutoFilter = true;
            this.gridControlExWSRoutes.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExWSRoutes.Location = new System.Drawing.Point(7, 232);
            this.gridControlExWSRoutes.MainView = this.gridViewExWSRoutes;
            this.gridControlExWSRoutes.Name = "gridControlExWSRoutes";
            this.gridControlExWSRoutes.Size = new System.Drawing.Size(692, 155);
            this.gridControlExWSRoutes.TabIndex = 14;
            this.gridControlExWSRoutes.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExWSRoutes});
            this.gridControlExWSRoutes.ViewTotalRows = true;
            // 
            // gridViewExWSRoutes
            // 
            this.gridViewExWSRoutes.AllowFocusedRowChanged = true;
            this.gridViewExWSRoutes.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewExWSRoutes.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExWSRoutes.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewExWSRoutes.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewExWSRoutes.Appearance.FooterPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewExWSRoutes.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewExWSRoutes.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewExWSRoutes.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExWSRoutes.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewExWSRoutes.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewExWSRoutes.EnablePreviewLineForFocusedRow = false;
            this.gridViewExWSRoutes.GridControl = this.gridControlExWSRoutes;
            this.gridViewExWSRoutes.Name = "gridViewExWSRoutes";
            this.gridViewExWSRoutes.OptionsMenu.EnableFooterMenu = false;
            this.gridViewExWSRoutes.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExWSRoutes.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewExWSRoutes.OptionsView.ShowFooter = true;
            this.gridViewExWSRoutes.OptionsView.ShowGroupPanel = false;
            this.gridViewExWSRoutes.ViewTotalRows = true;
            // 
            // gridControlExRoutes
            // 
            this.gridControlExRoutes.EnableAutoFilter = true;
            this.gridControlExRoutes.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExRoutes.Location = new System.Drawing.Point(353, 27);
            this.gridControlExRoutes.MainView = this.gridViewExRoutes;
            this.gridControlExRoutes.Name = "gridControlExRoutes";
            this.gridControlExRoutes.Size = new System.Drawing.Size(346, 141);
            this.gridControlExRoutes.TabIndex = 13;
            this.gridControlExRoutes.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExRoutes});
            this.gridControlExRoutes.ViewTotalRows = true;
            // 
            // gridViewExRoutes
            // 
            this.gridViewExRoutes.AllowFocusedRowChanged = true;
            this.gridViewExRoutes.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewExRoutes.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExRoutes.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewExRoutes.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewExRoutes.Appearance.FooterPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewExRoutes.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewExRoutes.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewExRoutes.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExRoutes.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewExRoutes.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewExRoutes.EnablePreviewLineForFocusedRow = false;
            this.gridViewExRoutes.GridControl = this.gridControlExRoutes;
            this.gridViewExRoutes.Name = "gridViewExRoutes";
            this.gridViewExRoutes.OptionsMenu.EnableFooterMenu = false;
            this.gridViewExRoutes.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExRoutes.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewExRoutes.OptionsView.ShowFooter = true;
            this.gridViewExRoutes.OptionsView.ShowGroupPanel = false;
            this.gridViewExRoutes.ViewTotalRows = true;
            // 
            // gridControlExWorkShift
            // 
            this.gridControlExWorkShift.EnableAutoFilter = true;
            this.gridControlExWorkShift.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExWorkShift.Location = new System.Drawing.Point(7, 27);
            this.gridControlExWorkShift.MainView = this.gridViewExWorkShift;
            this.gridControlExWorkShift.Name = "gridControlExWorkShift";
            this.gridControlExWorkShift.Size = new System.Drawing.Size(332, 141);
            this.gridControlExWorkShift.TabIndex = 12;
            this.gridControlExWorkShift.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExWorkShift});
            this.gridControlExWorkShift.ViewTotalRows = true;
            // 
            // gridViewExWorkShift
            // 
            this.gridViewExWorkShift.AllowFocusedRowChanged = true;
            this.gridViewExWorkShift.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewExWorkShift.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExWorkShift.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewExWorkShift.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewExWorkShift.Appearance.FooterPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewExWorkShift.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewExWorkShift.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewExWorkShift.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExWorkShift.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewExWorkShift.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewExWorkShift.EnablePreviewLineForFocusedRow = false;
            this.gridViewExWorkShift.GridControl = this.gridControlExWorkShift;
            this.gridViewExWorkShift.Name = "gridViewExWorkShift";
            this.gridViewExWorkShift.OptionsMenu.EnableFooterMenu = false;
            this.gridViewExWorkShift.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExWorkShift.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewExWorkShift.OptionsView.ShowFooter = true;
            this.gridViewExWorkShift.OptionsView.ShowGroupPanel = false;
            this.gridViewExWorkShift.ViewTotalRows = true;
            // 
            // simpleButtonClean
            // 
            this.simpleButtonClean.Location = new System.Drawing.Point(543, 391);
            this.simpleButtonClean.Name = "simpleButtonClean";
            this.simpleButtonClean.Size = new System.Drawing.Size(76, 26);
            this.simpleButtonClean.StyleController = this.layoutControl1;
            this.simpleButtonClean.TabIndex = 21;
            this.simpleButtonClean.Click += new System.EventHandler(this.simpleButtonClean_Click);
            // 
            // simpleButtonAccept
            // 
            this.simpleButtonAccept.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.simpleButtonAccept.Location = new System.Drawing.Point(548, 426);
            this.simpleButtonAccept.Name = "simpleButtonAccept";
            this.simpleButtonAccept.Size = new System.Drawing.Size(76, 26);
            this.simpleButtonAccept.StyleController = this.layoutControl1;
            this.simpleButtonAccept.TabIndex = 22;
            this.simpleButtonAccept.Text = "simpleButtonAccept";
            this.simpleButtonAccept.Click += new System.EventHandler(this.simpleButtonAccept_Click);
            // 
            // simpleButtonDelete
            // 
            this.simpleButtonDelete.Location = new System.Drawing.Point(623, 391);
            this.simpleButtonDelete.Name = "simpleButtonDelete";
            this.simpleButtonDelete.Size = new System.Drawing.Size(76, 26);
            this.simpleButtonDelete.StyleController = this.layoutControl1;
            this.simpleButtonDelete.TabIndex = 20;
            this.simpleButtonDelete.Click += new System.EventHandler(this.simpleButtonDelete_Click);
            // 
            // simpleButtonAdd
            // 
            this.simpleButtonAdd.Location = new System.Drawing.Point(628, 177);
            this.simpleButtonAdd.Name = "simpleButtonAdd";
            this.simpleButtonAdd.Size = new System.Drawing.Size(76, 26);
            this.simpleButtonAdd.StyleController = this.layoutControl1;
            this.simpleButtonAdd.TabIndex = 15;
            this.simpleButtonAdd.Text = "Agregar";
            this.simpleButtonAdd.Click += new System.EventHandler(this.simpleButtonAdd_Click);
            // 
            // simpleButtonCancel
            // 
            this.simpleButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonCancel.Location = new System.Drawing.Point(628, 426);
            this.simpleButtonCancel.Name = "simpleButtonCancel";
            this.simpleButtonCancel.Size = new System.Drawing.Size(76, 26);
            this.simpleButtonCancel.StyleController = this.layoutControl1;
            this.simpleButtonCancel.TabIndex = 23;
            this.simpleButtonCancel.Text = "simpleButtonCancel";
            // 
            // layoutControlGroupWSAndRoutes
            // 
            this.layoutControlGroupWSAndRoutes.CustomizationFormText = "layoutControlGroupWSAndRoutes";
            this.layoutControlGroupWSAndRoutes.GroupBordersVisible = false;
            this.layoutControlGroupWSAndRoutes.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem10,
            this.emptySpaceItem6,
            this.layoutControlGroupWSRoutes,
            this.layoutControlGroupRoutes,
            this.layoutControlGroupWorkShift,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.emptySpaceItem1});
            this.layoutControlGroupWSAndRoutes.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupWSAndRoutes.Name = "layoutControlGroupWSAndRoutes";
            this.layoutControlGroupWSAndRoutes.Size = new System.Drawing.Size(706, 454);
            this.layoutControlGroupWSAndRoutes.Text = "layoutControlGroupWSAndRoutes";
            this.layoutControlGroupWSAndRoutes.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.simpleButtonAdd;
            this.layoutControlItem10.CustomizationFormText = "layoutControlItem10";
            this.layoutControlItem10.Location = new System.Drawing.Point(626, 175);
            this.layoutControlItem10.MaxSize = new System.Drawing.Size(80, 30);
            this.layoutControlItem10.MinSize = new System.Drawing.Size(80, 30);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(80, 30);
            this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem10.Text = "layoutControlItem10";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextToControlDistance = 0;
            this.layoutControlItem10.TextVisible = false;
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 175);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(626, 30);
            this.emptySpaceItem6.Text = "emptySpaceItem6";
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroupWSRoutes
            // 
            this.layoutControlGroupWSRoutes.CustomizationFormText = "layoutControlGroupWSRoutes";
            this.layoutControlGroupWSRoutes.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5,
            this.layoutControlItem11,
            this.layoutControlItem12,
            this.emptySpaceItem7});
            this.layoutControlGroupWSRoutes.Location = new System.Drawing.Point(0, 205);
            this.layoutControlGroupWSRoutes.Name = "layoutControlGroupWSRoutes";
            this.layoutControlGroupWSRoutes.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupWSRoutes.Size = new System.Drawing.Size(706, 219);
            this.layoutControlGroupWSRoutes.Text = "layoutControlGroupWSRoutes";
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.gridControlExWSRoutes;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(696, 159);
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.simpleButtonDelete;
            this.layoutControlItem11.CustomizationFormText = "layoutControlItem11";
            this.layoutControlItem11.Location = new System.Drawing.Point(616, 159);
            this.layoutControlItem11.MaxSize = new System.Drawing.Size(80, 30);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(80, 30);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(80, 30);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Text = "layoutControlItem11";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextToControlDistance = 0;
            this.layoutControlItem11.TextVisible = false;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.simpleButtonClean;
            this.layoutControlItem12.CustomizationFormText = "layoutControlItem12";
            this.layoutControlItem12.Location = new System.Drawing.Point(536, 159);
            this.layoutControlItem12.MaxSize = new System.Drawing.Size(80, 30);
            this.layoutControlItem12.MinSize = new System.Drawing.Size(80, 30);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(80, 30);
            this.layoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem12.Text = "layoutControlItem12";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextToControlDistance = 0;
            this.layoutControlItem12.TextVisible = false;
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(0, 159);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(536, 30);
            this.emptySpaceItem7.Text = "emptySpaceItem7";
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroupRoutes
            // 
            this.layoutControlGroupRoutes.CustomizationFormText = "layoutControlGroupRoutes";
            this.layoutControlGroupRoutes.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4});
            this.layoutControlGroupRoutes.Location = new System.Drawing.Point(346, 0);
            this.layoutControlGroupRoutes.Name = "layoutControlGroupRoutes";
            this.layoutControlGroupRoutes.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupRoutes.Size = new System.Drawing.Size(360, 175);
            this.layoutControlGroupRoutes.Text = "layoutControlGroupRoutes";
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.gridControlExRoutes;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(350, 145);
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlGroupWorkShift
            // 
            this.layoutControlGroupWorkShift.CustomizationFormText = "layoutControlGroupWorkShift";
            this.layoutControlGroupWorkShift.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3});
            this.layoutControlGroupWorkShift.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupWorkShift.Name = "layoutControlGroupWorkShift";
            this.layoutControlGroupWorkShift.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupWorkShift.Size = new System.Drawing.Size(346, 175);
            this.layoutControlGroupWorkShift.Text = "layoutControlGroupWorkShift";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.gridControlExWorkShift;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(336, 145);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.simpleButtonAccept;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(546, 424);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(80, 30);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(80, 30);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(80, 30);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.simpleButtonCancel;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(626, 424);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(80, 30);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(80, 30);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(80, 30);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 424);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(546, 30);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // WorkShiftRouteForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(706, 454);
            this.Controls.Add(this.layoutControl1);
            this.Name = "WorkShiftRouteForm";
            this.Text = "WorkShiftRouteForm";
            this.Load += new System.EventHandler(this.WorkShiftRouteForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExWSRoutes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExWSRoutes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExRoutes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExRoutes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExWorkShift)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExWorkShift)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupWSAndRoutes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupWSRoutes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupRoutes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupWorkShift)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

		private DevExpress.XtraLayout.LayoutControl layoutControl1;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupWSAndRoutes;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupWSRoutes;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupRoutes;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupWorkShift;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
		private DevExpress.XtraEditors.SimpleButton simpleButtonAdd;
		private DevExpress.XtraEditors.SimpleButton simpleButtonClean;
		private DevExpress.XtraEditors.SimpleButton simpleButtonDelete;
		private GridControlEx gridControlExWSRoutes;
		private GridViewEx gridViewExWSRoutes;
		private GridControlEx gridControlExRoutes;
		private GridViewEx gridViewExRoutes;
		private GridControlEx gridControlExWorkShift;
		private GridViewEx gridViewExWorkShift;
		private DevExpress.XtraEditors.SimpleButton simpleButtonAccept;
		private DevExpress.XtraEditors.SimpleButton simpleButtonCancel;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
	}
}