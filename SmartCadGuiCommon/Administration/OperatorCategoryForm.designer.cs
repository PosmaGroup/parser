using DevExpress.XtraEditors;
using SmartCadControls.Controls;
namespace Smartmatic.SmartCad.Gui
{
    partial class OperatorCategoryForm : XtraForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.textBoxExName1 = new TextBoxEx();
            this.textBoxExLevel1 = new TextBoxEx();
            this.textBoxExDescription1 = new TextBoxEx();
            this.textBoxExName2 = new TextBoxEx();
            this.textBoxExLevel2 = new TextBoxEx();
            this.textBoxExDescription2 = new TextBoxEx();
            this.textBoxExName3 = new TextBoxEx();
            this.textBoxExLevel3 = new TextBoxEx();
            this.textBoxExDescription3 = new TextBoxEx();
            this.buttonExCancel = new DevExpress.XtraEditors.SimpleButton();
            this.FormBaseExConvertedLayout = new DevExpress.XtraLayout.LayoutControl();
            this.buttonExOk = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupCategory1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemName1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemLevel1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemDescription1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupCategory2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemName2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemLevel2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemDescription2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupCategory3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemName3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemLevel3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemDescription3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutConverter1 = new DevExpress.XtraLayout.Converter.LayoutConverter(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.FormBaseExConvertedLayout)).BeginInit();
            this.FormBaseExConvertedLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupCategory1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemName1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLevel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDescription1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupCategory2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemName2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLevel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDescription2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupCategory3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemName3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLevel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDescription3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxExName1
            // 
            this.textBoxExName1.AllowsLetters = true;
            this.textBoxExName1.AllowsNumbers = true;
            this.textBoxExName1.AllowsPunctuation = true;
            this.textBoxExName1.AllowsSeparators = true;
            this.textBoxExName1.AllowsSymbols = true;
            this.textBoxExName1.AllowsWhiteSpaces = true;
            this.textBoxExName1.ExtraAllowedChars = "";
            this.textBoxExName1.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExName1.Location = new System.Drawing.Point(75, 54);
            this.textBoxExName1.MaxLength = 60;
            this.textBoxExName1.Name = "textBoxExName1";
            this.textBoxExName1.NonAllowedCharacters = "";
            this.textBoxExName1.RegularExpresion = "";
            this.textBoxExName1.Size = new System.Drawing.Size(351, 20);
            this.textBoxExName1.TabIndex = 3;
            this.textBoxExName1.TextChanged += new System.EventHandler(this.ButtonsActivation);
            // 
            // textBoxExLevel1
            // 
            this.textBoxExLevel1.AllowsLetters = true;
            this.textBoxExLevel1.AllowsNumbers = true;
            this.textBoxExLevel1.AllowsPunctuation = true;
            this.textBoxExLevel1.AllowsSeparators = true;
            this.textBoxExLevel1.AllowsSymbols = true;
            this.textBoxExLevel1.AllowsWhiteSpaces = true;
            this.textBoxExLevel1.BackColor = System.Drawing.SystemColors.Window;
            this.textBoxExLevel1.Enabled = false;
            this.textBoxExLevel1.ExtraAllowedChars = "";
            this.textBoxExLevel1.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExLevel1.Location = new System.Drawing.Point(75, 30);
            this.textBoxExLevel1.MaxLength = 1;
            this.textBoxExLevel1.Name = "textBoxExLevel1";
            this.textBoxExLevel1.NonAllowedCharacters = "";
            this.textBoxExLevel1.ReadOnly = true;
            this.textBoxExLevel1.RegularExpresion = "";
            this.textBoxExLevel1.Size = new System.Drawing.Size(351, 20);
            this.textBoxExLevel1.TabIndex = 1;
            this.textBoxExLevel1.TabStop = false;
            // 
            // textBoxExDescription1
            // 
            this.textBoxExDescription1.AcceptsReturn = true;
            this.textBoxExDescription1.AllowsLetters = true;
            this.textBoxExDescription1.AllowsNumbers = true;
            this.textBoxExDescription1.AllowsPunctuation = true;
            this.textBoxExDescription1.AllowsSeparators = true;
            this.textBoxExDescription1.AllowsSymbols = true;
            this.textBoxExDescription1.AllowsWhiteSpaces = true;
            this.textBoxExDescription1.BackColor = System.Drawing.SystemColors.Window;
            this.textBoxExDescription1.Enabled = false;
            this.textBoxExDescription1.ExtraAllowedChars = "";
            this.textBoxExDescription1.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExDescription1.Location = new System.Drawing.Point(75, 78);
            this.textBoxExDescription1.MaxLength = 200;
            this.textBoxExDescription1.Multiline = true;
            this.textBoxExDescription1.Name = "textBoxExDescription1";
            this.textBoxExDescription1.NonAllowedCharacters = "";
            this.textBoxExDescription1.ReadOnly = true;
            this.textBoxExDescription1.RegularExpresion = "";
            this.textBoxExDescription1.Size = new System.Drawing.Size(351, 93);
            this.textBoxExDescription1.TabIndex = 5;
            this.textBoxExDescription1.TextChanged += new System.EventHandler(this.ButtonsActivation);
            // 
            // textBoxExName2
            // 
            this.textBoxExName2.AllowsLetters = true;
            this.textBoxExName2.AllowsNumbers = true;
            this.textBoxExName2.AllowsPunctuation = true;
            this.textBoxExName2.AllowsSeparators = true;
            this.textBoxExName2.AllowsSymbols = true;
            this.textBoxExName2.AllowsWhiteSpaces = true;
            this.textBoxExName2.ExtraAllowedChars = "";
            this.textBoxExName2.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExName2.Location = new System.Drawing.Point(75, 236);
            this.textBoxExName2.MaxLength = 60;
            this.textBoxExName2.Name = "textBoxExName2";
            this.textBoxExName2.NonAllowedCharacters = "";
            this.textBoxExName2.RegularExpresion = "";
            this.textBoxExName2.Size = new System.Drawing.Size(341, 27);
            this.textBoxExName2.TabIndex = 3;
            this.textBoxExName2.TextChanged += new System.EventHandler(this.ButtonsActivation);
            // 
            // textBoxExLevel2
            // 
            this.textBoxExLevel2.AllowsLetters = true;
            this.textBoxExLevel2.AllowsNumbers = true;
            this.textBoxExLevel2.AllowsPunctuation = true;
            this.textBoxExLevel2.AllowsSeparators = true;
            this.textBoxExLevel2.AllowsSymbols = true;
            this.textBoxExLevel2.AllowsWhiteSpaces = true;
            this.textBoxExLevel2.BackColor = System.Drawing.SystemColors.Window;
            this.textBoxExLevel2.Enabled = false;
            this.textBoxExLevel2.ExtraAllowedChars = "";
            this.textBoxExLevel2.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExLevel2.Location = new System.Drawing.Point(75, 205);
            this.textBoxExLevel2.MaxLength = 1;
            this.textBoxExLevel2.Name = "textBoxExLevel2";
            this.textBoxExLevel2.NonAllowedCharacters = "";
            this.textBoxExLevel2.ReadOnly = true;
            this.textBoxExLevel2.RegularExpresion = "";
            this.textBoxExLevel2.Size = new System.Drawing.Size(341, 27);
            this.textBoxExLevel2.TabIndex = 1;
            this.textBoxExLevel2.TabStop = false;
            // 
            // textBoxExDescription2
            // 
            this.textBoxExDescription2.AllowsLetters = true;
            this.textBoxExDescription2.AllowsNumbers = true;
            this.textBoxExDescription2.AllowsPunctuation = true;
            this.textBoxExDescription2.AllowsSeparators = true;
            this.textBoxExDescription2.AllowsSymbols = true;
            this.textBoxExDescription2.AllowsWhiteSpaces = true;
            this.textBoxExDescription2.BackColor = System.Drawing.SystemColors.Window;
            this.textBoxExDescription2.Enabled = false;
            this.textBoxExDescription2.ExtraAllowedChars = "";
            this.textBoxExDescription2.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExDescription2.Location = new System.Drawing.Point(75, 267);
            this.textBoxExDescription2.MaxLength = 200;
            this.textBoxExDescription2.Multiline = true;
            this.textBoxExDescription2.Name = "textBoxExDescription2";
            this.textBoxExDescription2.NonAllowedCharacters = "";
            this.textBoxExDescription2.ReadOnly = true;
            this.textBoxExDescription2.RegularExpresion = "";
            this.textBoxExDescription2.Size = new System.Drawing.Size(351, 79);
            this.textBoxExDescription2.TabIndex = 5;
            this.textBoxExDescription2.TextChanged += new System.EventHandler(this.ButtonsActivation);
            // 
            // textBoxExName3
            // 
            this.textBoxExName3.AllowsLetters = true;
            this.textBoxExName3.AllowsNumbers = true;
            this.textBoxExName3.AllowsPunctuation = true;
            this.textBoxExName3.AllowsSeparators = true;
            this.textBoxExName3.AllowsSymbols = true;
            this.textBoxExName3.AllowsWhiteSpaces = true;
            this.textBoxExName3.ExtraAllowedChars = "";
            this.textBoxExName3.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExName3.Location = new System.Drawing.Point(75, 404);
            this.textBoxExName3.MaxLength = 60;
            this.textBoxExName3.Name = "textBoxExName3";
            this.textBoxExName3.NonAllowedCharacters = "";
            this.textBoxExName3.RegularExpresion = "";
            this.textBoxExName3.Size = new System.Drawing.Size(351, 20);
            this.textBoxExName3.TabIndex = 3;
            this.textBoxExName3.TextChanged += new System.EventHandler(this.ButtonsActivation);
            // 
            // textBoxExLevel3
            // 
            this.textBoxExLevel3.AllowsLetters = true;
            this.textBoxExLevel3.AllowsNumbers = true;
            this.textBoxExLevel3.AllowsPunctuation = true;
            this.textBoxExLevel3.AllowsSeparators = true;
            this.textBoxExLevel3.AllowsSymbols = true;
            this.textBoxExLevel3.AllowsWhiteSpaces = true;
            this.textBoxExLevel3.BackColor = System.Drawing.SystemColors.Window;
            this.textBoxExLevel3.Enabled = false;
            this.textBoxExLevel3.ExtraAllowedChars = "";
            this.textBoxExLevel3.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExLevel3.Location = new System.Drawing.Point(75, 380);
            this.textBoxExLevel3.MaxLength = 1;
            this.textBoxExLevel3.Name = "textBoxExLevel3";
            this.textBoxExLevel3.NonAllowedCharacters = "";
            this.textBoxExLevel3.ReadOnly = true;
            this.textBoxExLevel3.RegularExpresion = "";
            this.textBoxExLevel3.Size = new System.Drawing.Size(351, 20);
            this.textBoxExLevel3.TabIndex = 1;
            this.textBoxExLevel3.TabStop = false;
            // 
            // textBoxExDescription3
            // 
            this.textBoxExDescription3.AllowsLetters = true;
            this.textBoxExDescription3.AllowsNumbers = true;
            this.textBoxExDescription3.AllowsPunctuation = true;
            this.textBoxExDescription3.AllowsSeparators = true;
            this.textBoxExDescription3.AllowsSymbols = true;
            this.textBoxExDescription3.AllowsWhiteSpaces = true;
            this.textBoxExDescription3.BackColor = System.Drawing.SystemColors.Window;
            this.textBoxExDescription3.Enabled = false;
            this.textBoxExDescription3.ExtraAllowedChars = "";
            this.textBoxExDescription3.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExDescription3.Location = new System.Drawing.Point(75, 428);
            this.textBoxExDescription3.MaxLength = 200;
            this.textBoxExDescription3.Multiline = true;
            this.textBoxExDescription3.Name = "textBoxExDescription3";
            this.textBoxExDescription3.NonAllowedCharacters = "";
            this.textBoxExDescription3.ReadOnly = true;
            this.textBoxExDescription3.RegularExpresion = "";
            this.textBoxExDescription3.Size = new System.Drawing.Size(351, 92);
            this.textBoxExDescription3.TabIndex = 5;
            this.textBoxExDescription3.TextChanged += new System.EventHandler(this.ButtonsActivation);
            // 
            // buttonExCancel
            // 
            this.buttonExCancel.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExCancel.Appearance.Options.UseFont = true;
            this.buttonExCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonExCancel.Location = new System.Drawing.Point(349, 529);
            this.buttonExCancel.Name = "buttonExCancel";
            this.buttonExCancel.Size = new System.Drawing.Size(82, 32);
            this.buttonExCancel.StyleController = this.FormBaseExConvertedLayout;
            this.buttonExCancel.TabIndex = 4;
            this.buttonExCancel.Text = "Cancelar";
            // 
            // FormBaseExConvertedLayout
            // 
            this.FormBaseExConvertedLayout.AllowCustomizationMenu = false;
            this.FormBaseExConvertedLayout.Controls.Add(this.buttonExOk);
            this.FormBaseExConvertedLayout.Controls.Add(this.buttonExCancel);
            this.FormBaseExConvertedLayout.Controls.Add(this.textBoxExName3);
            this.FormBaseExConvertedLayout.Controls.Add(this.textBoxExLevel3);
            this.FormBaseExConvertedLayout.Controls.Add(this.textBoxExDescription3);
            this.FormBaseExConvertedLayout.Controls.Add(this.textBoxExName2);
            this.FormBaseExConvertedLayout.Controls.Add(this.textBoxExLevel2);
            this.FormBaseExConvertedLayout.Controls.Add(this.textBoxExDescription2);
            this.FormBaseExConvertedLayout.Controls.Add(this.textBoxExName1);
            this.FormBaseExConvertedLayout.Controls.Add(this.textBoxExLevel1);
            this.FormBaseExConvertedLayout.Controls.Add(this.textBoxExDescription1);
            this.FormBaseExConvertedLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FormBaseExConvertedLayout.Location = new System.Drawing.Point(0, 0);
            this.FormBaseExConvertedLayout.Name = "FormBaseExConvertedLayout";
            this.FormBaseExConvertedLayout.OptionsCustomizationForm.ShowLayoutTreeView = false;
            this.FormBaseExConvertedLayout.Root = this.layoutControlGroup1;
            this.FormBaseExConvertedLayout.Size = new System.Drawing.Size(436, 566);
            this.FormBaseExConvertedLayout.TabIndex = 7;
            // 
            // buttonExOk
            // 
            this.buttonExOk.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExOk.Appearance.Options.UseFont = true;
            this.buttonExOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonExOk.Enabled = false;
            this.buttonExOk.Location = new System.Drawing.Point(263, 529);
            this.buttonExOk.Name = "buttonExOk";
            this.buttonExOk.Size = new System.Drawing.Size(82, 32);
            this.buttonExOk.StyleController = this.FormBaseExConvertedLayout;
            this.buttonExOk.TabIndex = 3;
            this.buttonExOk.Text = "Accept";
            this.buttonExOk.Click += new System.EventHandler(this.buttonExOk_Click);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlGroupCategory1,
            this.layoutControlGroupCategory2,
            this.layoutControlGroupCategory3,
            this.emptySpaceItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(436, 566);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.buttonExOk;
            this.layoutControlItem1.CustomizationFormText = "buttonExOkitem";
            this.layoutControlItem1.Location = new System.Drawing.Point(258, 524);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem1.Name = "buttonExOkitem";
            this.layoutControlItem1.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "buttonExOkitem";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.buttonExCancel;
            this.layoutControlItem2.CustomizationFormText = "buttonExCancelitem";
            this.layoutControlItem2.Location = new System.Drawing.Point(344, 524);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem2.Name = "buttonExCancelitem";
            this.layoutControlItem2.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "buttonExCancelitem";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlGroupCategory1
            // 
            this.layoutControlGroupCategory1.CustomizationFormText = "Categoria 1";
            this.layoutControlGroupCategory1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemName1,
            this.layoutControlItemLevel1,
            this.layoutControlItemDescription1});
            this.layoutControlGroupCategory1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupCategory1.Name = "layoutControlGroupCategory1";
            this.layoutControlGroupCategory1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupCategory1.Size = new System.Drawing.Size(430, 175);
            this.layoutControlGroupCategory1.Text = "Categoria 1";
            // 
            // layoutControlItemName1
            // 
            this.layoutControlItemName1.Control = this.textBoxExName1;
            this.layoutControlItemName1.CustomizationFormText = "Nombre: *";
            this.layoutControlItemName1.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItemName1.Name = "layoutControlItemName1";
            this.layoutControlItemName1.Size = new System.Drawing.Size(420, 24);
            this.layoutControlItemName1.Text = "Nombre: *";
            this.layoutControlItemName1.TextSize = new System.Drawing.Size(61, 13);
            // 
            // layoutControlItemLevel1
            // 
            this.layoutControlItemLevel1.Control = this.textBoxExLevel1;
            this.layoutControlItemLevel1.CustomizationFormText = "Nivel";
            this.layoutControlItemLevel1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemLevel1.Name = "layoutControlItemLevel1";
            this.layoutControlItemLevel1.Size = new System.Drawing.Size(420, 24);
            this.layoutControlItemLevel1.Text = "Nivel";
            this.layoutControlItemLevel1.TextSize = new System.Drawing.Size(61, 13);
            // 
            // layoutControlItemDescription1
            // 
            this.layoutControlItemDescription1.Control = this.textBoxExDescription1;
            this.layoutControlItemDescription1.CustomizationFormText = "Descripcion:";
            this.layoutControlItemDescription1.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItemDescription1.MinSize = new System.Drawing.Size(103, 31);
            this.layoutControlItemDescription1.Name = "layoutControlItemDescription1";
            this.layoutControlItemDescription1.Size = new System.Drawing.Size(420, 97);
            this.layoutControlItemDescription1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemDescription1.Text = "Descripcion: ";
            this.layoutControlItemDescription1.TextSize = new System.Drawing.Size(61, 13);
            // 
            // layoutControlGroupCategory2
            // 
            this.layoutControlGroupCategory2.CustomizationFormText = "Categoria 2";
            this.layoutControlGroupCategory2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemName2,
            this.layoutControlItemLevel2,
            this.layoutControlItemDescription2});
            this.layoutControlGroupCategory2.Location = new System.Drawing.Point(0, 175);
            this.layoutControlGroupCategory2.Name = "layoutControlGroupCategory2";
            this.layoutControlGroupCategory2.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupCategory2.Size = new System.Drawing.Size(430, 175);
            this.layoutControlGroupCategory2.Text = "Categoria 2";
            // 
            // layoutControlItemName2
            // 
            this.layoutControlItemName2.Control = this.textBoxExName2;
            this.layoutControlItemName2.CustomizationFormText = "Nombre: *";
            this.layoutControlItemName2.Location = new System.Drawing.Point(0, 31);
            this.layoutControlItemName2.MaxSize = new System.Drawing.Size(410, 31);
            this.layoutControlItemName2.MinSize = new System.Drawing.Size(410, 31);
            this.layoutControlItemName2.Name = "layoutControlItemName2";
            this.layoutControlItemName2.Size = new System.Drawing.Size(420, 31);
            this.layoutControlItemName2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemName2.Text = "Nombre: *";
            this.layoutControlItemName2.TextSize = new System.Drawing.Size(61, 13);
            // 
            // layoutControlItemLevel2
            // 
            this.layoutControlItemLevel2.Control = this.textBoxExLevel2;
            this.layoutControlItemLevel2.CustomizationFormText = "Nivel";
            this.layoutControlItemLevel2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemLevel2.MaxSize = new System.Drawing.Size(410, 31);
            this.layoutControlItemLevel2.MinSize = new System.Drawing.Size(410, 31);
            this.layoutControlItemLevel2.Name = "layoutControlItemLevel2";
            this.layoutControlItemLevel2.Size = new System.Drawing.Size(420, 31);
            this.layoutControlItemLevel2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemLevel2.Text = "Nivel";
            this.layoutControlItemLevel2.TextSize = new System.Drawing.Size(61, 13);
            // 
            // layoutControlItemDescription2
            // 
            this.layoutControlItemDescription2.Control = this.textBoxExDescription2;
            this.layoutControlItemDescription2.CustomizationFormText = "Descripcion:";
            this.layoutControlItemDescription2.Location = new System.Drawing.Point(0, 62);
            this.layoutControlItemDescription2.MinSize = new System.Drawing.Size(103, 31);
            this.layoutControlItemDescription2.Name = "layoutControlItemDescription2";
            this.layoutControlItemDescription2.Size = new System.Drawing.Size(420, 83);
            this.layoutControlItemDescription2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemDescription2.Text = "Descripcion:";
            this.layoutControlItemDescription2.TextSize = new System.Drawing.Size(61, 13);
            // 
            // layoutControlGroupCategory3
            // 
            this.layoutControlGroupCategory3.CustomizationFormText = "Categoria 3";
            this.layoutControlGroupCategory3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemName3,
            this.layoutControlItemLevel3,
            this.layoutControlItemDescription3});
            this.layoutControlGroupCategory3.Location = new System.Drawing.Point(0, 350);
            this.layoutControlGroupCategory3.Name = "layoutControlGroupCategory3";
            this.layoutControlGroupCategory3.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupCategory3.Size = new System.Drawing.Size(430, 174);
            this.layoutControlGroupCategory3.Text = "Categoria 3";
            // 
            // layoutControlItemName3
            // 
            this.layoutControlItemName3.Control = this.textBoxExName3;
            this.layoutControlItemName3.CustomizationFormText = "Nombre: *";
            this.layoutControlItemName3.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItemName3.Name = "layoutControlItemName3";
            this.layoutControlItemName3.Size = new System.Drawing.Size(420, 24);
            this.layoutControlItemName3.Text = "Nombre: *";
            this.layoutControlItemName3.TextSize = new System.Drawing.Size(61, 13);
            // 
            // layoutControlItemLevel3
            // 
            this.layoutControlItemLevel3.Control = this.textBoxExLevel3;
            this.layoutControlItemLevel3.CustomizationFormText = "Nivel";
            this.layoutControlItemLevel3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemLevel3.Name = "layoutControlItemLevel3";
            this.layoutControlItemLevel3.Size = new System.Drawing.Size(420, 24);
            this.layoutControlItemLevel3.Text = "Nivel";
            this.layoutControlItemLevel3.TextSize = new System.Drawing.Size(61, 13);
            // 
            // layoutControlItemDescription3
            // 
            this.layoutControlItemDescription3.Control = this.textBoxExDescription3;
            this.layoutControlItemDescription3.CustomizationFormText = "Descripcion:";
            this.layoutControlItemDescription3.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItemDescription3.MinSize = new System.Drawing.Size(97, 31);
            this.layoutControlItemDescription3.Name = "layoutControlItemDescription3";
            this.layoutControlItemDescription3.Size = new System.Drawing.Size(420, 96);
            this.layoutControlItemDescription3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemDescription3.Text = "Descripcion:";
            this.layoutControlItemDescription3.TextSize = new System.Drawing.Size(61, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 524);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(110, 30);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(258, 36);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // OperatorCategoryForm
            // 
            this.AcceptButton = this.buttonExOk;
            this.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonExCancel;
            this.ClientSize = new System.Drawing.Size(436, 566);
            this.Controls.Add(this.FormBaseExConvertedLayout);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(426, 593);
            this.Name = "OperatorCategoryForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            ((System.ComponentModel.ISupportInitialize)(this.FormBaseExConvertedLayout)).EndInit();
            this.FormBaseExConvertedLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupCategory1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemName1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLevel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDescription1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupCategory2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemName2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLevel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDescription2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupCategory3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemName3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLevel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDescription3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TextBoxEx textBoxExName1;
        private TextBoxEx textBoxExLevel1;
        private TextBoxEx textBoxExDescription1;
        private TextBoxEx textBoxExName2;
        private TextBoxEx textBoxExLevel2;
        private TextBoxEx textBoxExDescription2;
        private TextBoxEx textBoxExName3;
        private TextBoxEx textBoxExLevel3;
        private TextBoxEx textBoxExDescription3;
        private DevExpress.XtraEditors.SimpleButton buttonExCancel;
        private DevExpress.XtraEditors.SimpleButton buttonExOk;
        private DevExpress.XtraLayout.LayoutControl FormBaseExConvertedLayout;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupCategory1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemName1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemLevel1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDescription1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupCategory2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemName2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemLevel2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDescription2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupCategory3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemName3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemLevel3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDescription3;
        private DevExpress.XtraLayout.Converter.LayoutConverter layoutConverter1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
    }
}