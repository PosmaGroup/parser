using SmartCadControls;
namespace SmartCadGuiCommon
{
    partial class DepartmentTypeXtraForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DepartmentTypeXtraForm));
            this.DepartmentTypeXtraFormConvertedLayout = new DevExpress.XtraLayout.LayoutControl();
            this.labelControlExample = new DevExpress.XtraEditors.LabelControl();
            this.pictureEditHelp = new DevExpress.XtraEditors.PictureEdit();
            this.toolTipControllerHelp = new DevExpress.Utils.ToolTipController(this.components);
            this.textEditPattern = new DevExpress.XtraEditors.TextEdit();
            this.comboBoxEditFrequency = new DevExpress.XtraEditors.ComboBoxEdit();
            this.gridControlExAlerts = new GridControlEx();
            this.gridViewExAlerts = new GridViewEx();
            this.gridViewEx1 = new GridViewEx();
            this.checkEditEnabledDispatch = new DevExpress.XtraEditors.CheckEdit();
            this.colorEdit = new DevExpress.XtraEditors.ColorEdit();
            this.textEditDepartmentName = new DevExpress.XtraEditors.TextEdit();
            this.simpleButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonAccept = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupDepartment = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemName = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroupFeatures = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroupAlerts = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupIncidentNotificationIdentify = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemFrequency = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemPattern = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItemExample = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemHelp = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemColor = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.DepartmentTypeXtraFormConvertedLayout)).BeginInit();
            this.DepartmentTypeXtraFormConvertedLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEditHelp.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPattern.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditFrequency.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExAlerts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExAlerts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEx1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditEnabledDispatch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDepartmentName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupDepartment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupFeatures)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupAlerts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupIncidentNotificationIdentify)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemFrequency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPattern)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemExample)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemHelp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemColor)).BeginInit();
            this.SuspendLayout();
            // 
            // DepartmentTypeXtraFormConvertedLayout
            // 
            this.DepartmentTypeXtraFormConvertedLayout.AllowCustomizationMenu = false;
            this.DepartmentTypeXtraFormConvertedLayout.Controls.Add(this.labelControlExample);
            this.DepartmentTypeXtraFormConvertedLayout.Controls.Add(this.pictureEditHelp);
            this.DepartmentTypeXtraFormConvertedLayout.Controls.Add(this.textEditPattern);
            this.DepartmentTypeXtraFormConvertedLayout.Controls.Add(this.comboBoxEditFrequency);
            this.DepartmentTypeXtraFormConvertedLayout.Controls.Add(this.gridControlExAlerts);
            this.DepartmentTypeXtraFormConvertedLayout.Controls.Add(this.checkEditEnabledDispatch);
            this.DepartmentTypeXtraFormConvertedLayout.Controls.Add(this.colorEdit);
            this.DepartmentTypeXtraFormConvertedLayout.Controls.Add(this.textEditDepartmentName);
            this.DepartmentTypeXtraFormConvertedLayout.Controls.Add(this.simpleButtonCancel);
            this.DepartmentTypeXtraFormConvertedLayout.Controls.Add(this.simpleButtonAccept);
            this.DepartmentTypeXtraFormConvertedLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DepartmentTypeXtraFormConvertedLayout.Location = new System.Drawing.Point(0, 0);
            this.DepartmentTypeXtraFormConvertedLayout.Margin = new System.Windows.Forms.Padding(2);
            this.DepartmentTypeXtraFormConvertedLayout.Name = "DepartmentTypeXtraFormConvertedLayout";
            this.DepartmentTypeXtraFormConvertedLayout.OptionsCustomizationForm.ShowLayoutTreeView = false;
            this.DepartmentTypeXtraFormConvertedLayout.Root = this.layoutControlGroup1;
            this.DepartmentTypeXtraFormConvertedLayout.Size = new System.Drawing.Size(566, 476);
            this.DepartmentTypeXtraFormConvertedLayout.TabIndex = 10;
            // 
            // labelControlExample
            // 
            this.labelControlExample.Location = new System.Drawing.Point(356, 170);
            this.labelControlExample.Name = "labelControlExample";
            this.labelControlExample.Size = new System.Drawing.Size(97, 13);
            this.labelControlExample.StyleController = this.DepartmentTypeXtraFormConvertedLayout;
            this.labelControlExample.TabIndex = 16;
            this.labelControlExample.Text = "labelControlExample";
            // 
            // pictureEditHelp
            // 
            this.pictureEditHelp.EditValue = ((object)(resources.GetObject("pictureEditHelp.EditValue")));
            this.pictureEditHelp.Location = new System.Drawing.Point(294, 169);
            this.pictureEditHelp.Margin = new System.Windows.Forms.Padding(4);
            this.pictureEditHelp.Name = "pictureEditHelp";
            this.pictureEditHelp.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEditHelp.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEditHelp.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEditHelp.Properties.ReadOnly = true;
            this.pictureEditHelp.Properties.ShowMenu = false;
            this.pictureEditHelp.Size = new System.Drawing.Size(24, 23);
            this.pictureEditHelp.StyleController = this.DepartmentTypeXtraFormConvertedLayout;
            this.pictureEditHelp.TabIndex = 15;
            this.pictureEditHelp.ToolTipController = this.toolTipControllerHelp;
            // 
            // toolTipControllerHelp
            // 
            this.toolTipControllerHelp.AutoPopDelay = 10000;
            this.toolTipControllerHelp.CloseOnClick = DevExpress.Utils.DefaultBoolean.True;
            this.toolTipControllerHelp.InitialDelay = 300;
            this.toolTipControllerHelp.ShowBeak = true;
            // 
            // textEditPattern
            // 
            this.textEditPattern.Enabled = false;
            this.textEditPattern.Location = new System.Drawing.Point(72, 167);
            this.textEditPattern.Name = "textEditPattern";
            this.textEditPattern.Size = new System.Drawing.Size(218, 20);
            this.textEditPattern.StyleController = this.DepartmentTypeXtraFormConvertedLayout;
            this.textEditPattern.TabIndex = 13;
            this.textEditPattern.TextChanged += new System.EventHandler(this.textEditPattern_TextChanged);
            // 
            // comboBoxEditFrequency
            // 
            this.comboBoxEditFrequency.Enabled = false;
            this.comboBoxEditFrequency.Location = new System.Drawing.Point(72, 136);
            this.comboBoxEditFrequency.Name = "comboBoxEditFrequency";
            this.comboBoxEditFrequency.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditFrequency.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEditFrequency.Size = new System.Drawing.Size(218, 20);
            this.comboBoxEditFrequency.StyleController = this.DepartmentTypeXtraFormConvertedLayout;
            this.comboBoxEditFrequency.TabIndex = 12;
            this.comboBoxEditFrequency.SelectedIndexChanged += new System.EventHandler(this.comboBoxEditFrequency_SelectedIndexChanged);
            // 
            // gridControlExAlerts
            // 
            this.gridControlExAlerts.EnableAutoFilter = true;
            this.gridControlExAlerts.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExAlerts.Location = new System.Drawing.Point(12, 259);
            this.gridControlExAlerts.MainView = this.gridViewExAlerts;
            this.gridControlExAlerts.Name = "gridControlExAlerts";
            this.gridControlExAlerts.Size = new System.Drawing.Size(542, 169);
            this.gridControlExAlerts.TabIndex = 11;
            this.gridControlExAlerts.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExAlerts,
            this.gridViewEx1});
            this.gridControlExAlerts.ViewTotalRows = true;
            // 
            // gridViewExAlerts
            // 
            this.gridViewExAlerts.AllowFocusedRowChanged = true;
            this.gridViewExAlerts.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewExAlerts.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExAlerts.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewExAlerts.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewExAlerts.Appearance.FooterPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewExAlerts.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewExAlerts.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewExAlerts.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExAlerts.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewExAlerts.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewExAlerts.EnablePreviewLineForFocusedRow = false;
            this.gridViewExAlerts.GridControl = this.gridControlExAlerts;
            this.gridViewExAlerts.Name = "gridViewExAlerts";
            this.gridViewExAlerts.OptionsMenu.EnableFooterMenu = false;
            this.gridViewExAlerts.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExAlerts.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewExAlerts.OptionsView.ShowFooter = true;
            this.gridViewExAlerts.OptionsView.ShowGroupPanel = false;
            this.gridViewExAlerts.ViewTotalRows = true;
            this.gridViewExAlerts.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridViewExAlerts_ShowingEditor);
            this.gridViewExAlerts.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewExAlerts_CellValueChanging);
            // 
            // gridViewEx1
            // 
            this.gridViewEx1.AllowFocusedRowChanged = true;
            this.gridViewEx1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewEx1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewEx1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewEx1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewEx1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewEx1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewEx1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewEx1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewEx1.EnablePreviewLineForFocusedRow = false;
            this.gridViewEx1.GridControl = this.gridControlExAlerts;
            this.gridViewEx1.Name = "gridViewEx1";
            this.gridViewEx1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewEx1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewEx1.ViewTotalRows = false;
            // 
            // checkEditEnabledDispatch
            // 
            this.checkEditEnabledDispatch.Location = new System.Drawing.Point(7, 80);
            this.checkEditEnabledDispatch.Name = "checkEditEnabledDispatch";
            this.checkEditEnabledDispatch.Properties.Caption = "";
            this.checkEditEnabledDispatch.Size = new System.Drawing.Size(151, 19);
            this.checkEditEnabledDispatch.StyleController = this.DepartmentTypeXtraFormConvertedLayout;
            this.checkEditEnabledDispatch.TabIndex = 10;
            this.checkEditEnabledDispatch.CheckedChanged += new System.EventHandler(this.checkEditEnabledDispatch_CheckedChanged);
            // 
            // colorEdit
            // 
            this.colorEdit.EditValue = System.Drawing.Color.Black;
            this.colorEdit.Location = new System.Drawing.Point(67, 203);
            this.colorEdit.Name = "colorEdit";
            this.colorEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.colorEdit.Properties.ColorAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colorEdit.Properties.ShowColorDialog = false;
            this.colorEdit.Properties.ShowPopupShadow = false;
            this.colorEdit.Properties.ShowSystemColors = false;
            this.colorEdit.Properties.ShowWebColors = false;
            this.colorEdit.Size = new System.Drawing.Size(64, 20);
            this.colorEdit.StyleController = this.DepartmentTypeXtraFormConvertedLayout;
            this.colorEdit.TabIndex = 2;
            // 
            // textEditDepartmentName
            // 
            this.textEditDepartmentName.Location = new System.Drawing.Point(67, 27);
            this.textEditDepartmentName.Margin = new System.Windows.Forms.Padding(2);
            this.textEditDepartmentName.Name = "textEditDepartmentName";
            this.textEditDepartmentName.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEditDepartmentName.Properties.Appearance.Options.UseFont = true;
            this.textEditDepartmentName.Properties.MaxLength = 255;
            this.textEditDepartmentName.Size = new System.Drawing.Size(492, 19);
            this.textEditDepartmentName.StyleController = this.DepartmentTypeXtraFormConvertedLayout;
            this.textEditDepartmentName.TabIndex = 1;
            this.textEditDepartmentName.TextChanged += new System.EventHandler(this.EnableButtons);
            // 
            // simpleButtonCancel
            // 
            this.simpleButtonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonCancel.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.simpleButtonCancel.Appearance.Options.UseFont = true;
            this.simpleButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonCancel.Location = new System.Drawing.Point(482, 442);
            this.simpleButtonCancel.Margin = new System.Windows.Forms.Padding(2);
            this.simpleButtonCancel.Name = "simpleButtonCancel";
            this.simpleButtonCancel.Size = new System.Drawing.Size(82, 32);
            this.simpleButtonCancel.StyleController = this.DepartmentTypeXtraFormConvertedLayout;
            this.simpleButtonCancel.TabIndex = 9;
            this.simpleButtonCancel.Text = "Cancelar";
            // 
            // simpleButtonAccept
            // 
            this.simpleButtonAccept.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonAccept.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.simpleButtonAccept.Appearance.Options.UseFont = true;
            this.simpleButtonAccept.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.simpleButtonAccept.Location = new System.Drawing.Point(396, 442);
            this.simpleButtonAccept.Margin = new System.Windows.Forms.Padding(2);
            this.simpleButtonAccept.Name = "simpleButtonAccept";
            this.simpleButtonAccept.Size = new System.Drawing.Size(82, 32);
            this.simpleButtonAccept.StyleController = this.DepartmentTypeXtraFormConvertedLayout;
            this.simpleButtonAccept.TabIndex = 8;
            this.simpleButtonAccept.Text = "Accept";
            this.simpleButtonAccept.Click += new System.EventHandler(this.simpleButtonAccept_Click);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlGroupDepartment,
            this.emptySpaceItem1,
            this.layoutControlGroupFeatures});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(566, 476);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.simpleButtonCancel;
            this.layoutControlItem4.CustomizationFormText = "simpleButtonCancelitem";
            this.layoutControlItem4.Location = new System.Drawing.Point(480, 440);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem4.Name = "simpleButtonCancelitem";
            this.layoutControlItem4.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "simpleButtonCancelitem";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.simpleButtonAccept;
            this.layoutControlItem5.CustomizationFormText = "simpleButtonAcceptitem";
            this.layoutControlItem5.Location = new System.Drawing.Point(394, 440);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem5.Name = "simpleButtonAcceptitem";
            this.layoutControlItem5.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "simpleButtonAcceptitem";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlGroupDepartment
            // 
            this.layoutControlGroupDepartment.CustomizationFormText = "layoutControlGroupDepartment";
            this.layoutControlGroupDepartment.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemName});
            this.layoutControlGroupDepartment.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupDepartment.Name = "layoutControlGroupDepartment";
            this.layoutControlGroupDepartment.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupDepartment.Size = new System.Drawing.Size(566, 53);
            this.layoutControlGroupDepartment.Text = "layoutControlGroupDepartment";
            // 
            // layoutControlItemName
            // 
            this.layoutControlItemName.Control = this.textEditDepartmentName;
            this.layoutControlItemName.CustomizationFormText = "layoutControlItemName";
            this.layoutControlItemName.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemName.Name = "layoutControlItemName";
            this.layoutControlItemName.Size = new System.Drawing.Size(556, 23);
            this.layoutControlItemName.Text = "Nombre:";
            this.layoutControlItemName.TextSize = new System.Drawing.Size(56, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 440);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(394, 36);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroupFeatures
            // 
            this.layoutControlGroupFeatures.CustomizationFormText = "layoutControlGroupFeatures";
            this.layoutControlGroupFeatures.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem5,
            this.layoutControlGroupAlerts,
            this.layoutControlItem6,
            this.layoutControlGroupIncidentNotificationIdentify,
            this.layoutControlItemColor});
            this.layoutControlGroupFeatures.Location = new System.Drawing.Point(0, 53);
            this.layoutControlGroupFeatures.Name = "layoutControlGroupFeatures";
            this.layoutControlGroupFeatures.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupFeatures.Size = new System.Drawing.Size(566, 387);
            this.layoutControlGroupFeatures.Text = "layoutControlGroupFeatures";
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(155, 0);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(401, 31);
            this.emptySpaceItem5.Text = "emptySpaceItem5";
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroupAlerts
            // 
            this.layoutControlGroupAlerts.CustomizationFormText = "layoutControlGroupAlerts";
            this.layoutControlGroupAlerts.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.layoutControlGroupAlerts.Location = new System.Drawing.Point(0, 154);
            this.layoutControlGroupAlerts.Name = "layoutControlGroupAlerts";
            this.layoutControlGroupAlerts.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupAlerts.Size = new System.Drawing.Size(556, 203);
            this.layoutControlGroupAlerts.Text = "layoutControlGroupAlerts";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridControlExAlerts;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(546, 173);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.checkEditEnabledDispatch;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(155, 31);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(155, 31);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(155, 31);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlGroupIncidentNotificationIdentify
            // 
            this.layoutControlGroupIncidentNotificationIdentify.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroupIncidentNotificationIdentify.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemFrequency,
            this.layoutControlItemPattern,
            this.emptySpaceItem3,
            this.layoutControlItemExample,
            this.layoutControlItemHelp});
            this.layoutControlGroupIncidentNotificationIdentify.Location = new System.Drawing.Point(0, 31);
            this.layoutControlGroupIncidentNotificationIdentify.Name = "layoutControlGroupIncidentNotificationIdentify";
            this.layoutControlGroupIncidentNotificationIdentify.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupIncidentNotificationIdentify.Size = new System.Drawing.Size(556, 92);
            this.layoutControlGroupIncidentNotificationIdentify.Text = "layoutControlGroupIncidentNotificationIdentify";
            // 
            // layoutControlItemFrequency
            // 
            this.layoutControlItemFrequency.Control = this.comboBoxEditFrequency;
            this.layoutControlItemFrequency.CustomizationFormText = "layoutControlItemFrequency";
            this.layoutControlItemFrequency.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemFrequency.MaxSize = new System.Drawing.Size(282, 31);
            this.layoutControlItemFrequency.MinSize = new System.Drawing.Size(282, 31);
            this.layoutControlItemFrequency.Name = "layoutControlItemFrequency";
            this.layoutControlItemFrequency.Size = new System.Drawing.Size(282, 31);
            this.layoutControlItemFrequency.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemFrequency.Text = "Frecuencia:";
            this.layoutControlItemFrequency.TextSize = new System.Drawing.Size(56, 13);
            // 
            // layoutControlItemPattern
            // 
            this.layoutControlItemPattern.Control = this.textEditPattern;
            this.layoutControlItemPattern.CustomizationFormText = "layoutControlItemPattern";
            this.layoutControlItemPattern.Location = new System.Drawing.Point(0, 31);
            this.layoutControlItemPattern.MaxSize = new System.Drawing.Size(282, 31);
            this.layoutControlItemPattern.MinSize = new System.Drawing.Size(282, 31);
            this.layoutControlItemPattern.Name = "layoutControlItemPattern";
            this.layoutControlItemPattern.Size = new System.Drawing.Size(282, 31);
            this.layoutControlItemPattern.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemPattern.Text = "Formato:";
            this.layoutControlItemPattern.TextSize = new System.Drawing.Size(56, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(282, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(264, 31);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItemExample
            // 
            this.layoutControlItemExample.Control = this.labelControlExample;
            this.layoutControlItemExample.CustomizationFormText = "layoutControlItemExample";
            this.layoutControlItemExample.Location = new System.Drawing.Point(313, 31);
            this.layoutControlItemExample.Name = "layoutControlItemExample";
            this.layoutControlItemExample.Padding = new DevExpress.XtraLayout.Utils.Padding(10, 5, 5, 5);
            this.layoutControlItemExample.Size = new System.Drawing.Size(233, 31);
            this.layoutControlItemExample.Text = "ej.";
            this.layoutControlItemExample.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItemExample.TextSize = new System.Drawing.Size(13, 13);
            this.layoutControlItemExample.TextToControlDistance = 10;
            // 
            // layoutControlItemHelp
            // 
            this.layoutControlItemHelp.Control = this.pictureEditHelp;
            this.layoutControlItemHelp.ControlAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.layoutControlItemHelp.CustomizationFormText = "layoutControlItemHelp";
            this.layoutControlItemHelp.Location = new System.Drawing.Point(282, 31);
            this.layoutControlItemHelp.MaxSize = new System.Drawing.Size(31, 31);
            this.layoutControlItemHelp.MinSize = new System.Drawing.Size(31, 31);
            this.layoutControlItemHelp.Name = "layoutControlItemHelp";
            this.layoutControlItemHelp.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 5, 4, 4);
            this.layoutControlItemHelp.Size = new System.Drawing.Size(31, 31);
            this.layoutControlItemHelp.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemHelp.Text = "layoutControlItemHelp";
            this.layoutControlItemHelp.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemHelp.TextToControlDistance = 0;
            this.layoutControlItemHelp.TextVisible = false;
            // 
            // layoutControlItemColor
            // 
            this.layoutControlItemColor.Control = this.colorEdit;
            this.layoutControlItemColor.CustomizationFormText = "layoutControlItemColor";
            this.layoutControlItemColor.Location = new System.Drawing.Point(0, 123);
            this.layoutControlItemColor.MaxSize = new System.Drawing.Size(128, 31);
            this.layoutControlItemColor.MinSize = new System.Drawing.Size(128, 31);
            this.layoutControlItemColor.Name = "layoutControlItemColor";
            this.layoutControlItemColor.Size = new System.Drawing.Size(556, 31);
            this.layoutControlItemColor.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemColor.Text = "Color:";
            this.layoutControlItemColor.TextSize = new System.Drawing.Size(56, 13);
            // 
            // DepartmentTypeXtraForm
            // 
            this.AcceptButton = this.simpleButtonAccept;
            this.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.simpleButtonCancel;
            this.ClientSize = new System.Drawing.Size(566, 476);
            this.Controls.Add(this.DepartmentTypeXtraFormConvertedLayout);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(574, 510);
            this.Name = "DepartmentTypeXtraForm";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Modulo de Administracion - Crear organismo";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DepartmentTypeXtraForm_FormClosing);
            this.Load += new System.EventHandler(this.DepartmentTypeXtraForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DepartmentTypeXtraFormConvertedLayout)).EndInit();
            this.DepartmentTypeXtraFormConvertedLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureEditHelp.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPattern.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditFrequency.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExAlerts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExAlerts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEx1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditEnabledDispatch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDepartmentName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupDepartment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupFeatures)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupAlerts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupIncidentNotificationIdentify)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemFrequency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPattern)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemExample)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemHelp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemColor)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.ColorEdit colorEdit;
        private DevExpress.XtraEditors.SimpleButton simpleButtonAccept;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCancel;
        private DevExpress.XtraLayout.LayoutControl DepartmentTypeXtraFormConvertedLayout;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupDepartment;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.TextEdit textEditDepartmentName;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemName;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemColor;
        private DevExpress.XtraEditors.CheckEdit checkEditEnabledDispatch;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private GridControlEx gridControlExAlerts;
        private GridViewEx gridViewExAlerts;
        private GridViewEx gridViewEx1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupAlerts;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.TextEdit textEditPattern;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditFrequency;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemFrequency;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemPattern;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupFeatures;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraEditors.PictureEdit pictureEditHelp;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemHelp;
        private DevExpress.Utils.ToolTipController toolTipControllerHelp;
        private DevExpress.XtraEditors.LabelControl labelControlExample;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemExample;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupIncidentNotificationIdentify;
    }
}