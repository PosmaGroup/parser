using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Reflection;
using System.Collections.Generic;
using SmartCadCore.Core;
using Smartmatic.SmartCad.Service;
using System.ServiceModel;

using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadCore.Enums;
using SmartCadControls.Controls;
using SmartCadControls;
using SmartCadGuiCommon.Services;
using SmartCadGuiCommon.Classes;

namespace SmartCadGuiCommon
{
	/// <summary>
    /// Summary description for StructForm.
	/// </summary>
	public partial class StructForm : DevExpress.XtraEditors.XtraForm
	{      
        private CctvZoneClientData cctvZoneSelected;
        private StructClientData selectedStruct;
        private GeoPoint StructGeoeoPoint;
        private bool buttonOkPressed = false;
        private AdministrationRibbonForm parentForm;
        private object syncCommitted = new object();

        enum DeviceType
        {
            Unknown,
            Camera,
            Datalogger
        }

        public StructForm()
		{
            InitializeComponent();     
            FillCctvZones();
            FillCamerasList();
            FillStructTypes();
            InitializeGridControls();
            ApplicationServerService.GisAction += new EventHandler<GisActionEventArgs>(serverServiceClient_GisAction);
            this.FormClosing += new FormClosingEventHandler(StructForm_FormClosing);
            gridControlAvail.Type = typeof(MappableDeviceGridData);
            gridControlSel.Type = typeof(MappableDeviceGridData);

            var applications = ServerServiceClient.GetInstance().GetActiveApplications();
            if (!applications.Contains(SmartCadApplications.SmartCadMap)) 
            {
                layoutControlItem5.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                textBoxExLat.ReadOnly = false;
                textBoxExLon.ReadOnly = false;
            }
            


		}

        private void InitializeGridControls()
        {
        }

        public StructForm(AdministrationRibbonForm form, StructClientData myStruct, FormBehavior behavior)
            :this()
		{
            FormClosing += new FormClosingEventHandler(StructForm_FormClosing);
            this.parentForm = form;
            this.parentForm.AdministrationCommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(StructForm_AdministrationCommittedChanges);

			Behavior = behavior;
            LoadLanguage();
            SelectedStruct = myStruct;   
        }

        void StructForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.parentForm != null)
            {
                this.parentForm.AdministrationCommittedChanges -= new EventHandler<CommittedObjectDataCollectionEventArgs>(StructForm_AdministrationCommittedChanges);
            }
            ApplicationServerService.GisAction -= serverServiceClient_GisAction;
            ApplicationServiceClient.Current(UserApplicationClientData.Map).SendACK(false);
        }

        void StructForm_AdministrationCommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
        {
            try
            {
                if (e.Objects != null && e.Objects.Count > 0)
                {
                    lock (syncCommitted)
                    {
                        try
                        {
                            if (ServerServiceClient.GetInstance().OperatorClient == null)
                            { }
                        }
                        catch
                        { }

                        if (e.Objects[0] is StructClientData)
                        {
                            #region EvaluationClientData
                            StructClientData structClient =
                                e.Objects[0] as StructClientData;

                            if (SelectedStruct != null && SelectedStruct.Equals(structClient) && ButtonOkPressed == false)
                            {
                                if (e.Action == CommittedDataAction.Update)
                                {
                                    MessageForm.Show(ResourceLoader.GetString2("UpdateFormStructData"), MessageFormType.Warning);
                                    SelectedStruct = structClient;
                                }
                                else if (e.Action == CommittedDataAction.Delete)
                                {
                                    MessageForm.Show(ResourceLoader.GetString2("DeleteFormStructData"), MessageFormType.Warning);
                                    FormUtil.InvokeRequired(this,
                                    delegate
                                    {
                                        this.Close();
                                    });
                                }

                            }
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        private void LoadLanguage() 
        {
            this.Icon = ResourceLoader.GetIcon("$Icon.Struct");
            if (Behavior == FormBehavior.Create)
            {
                Text = ResourceLoader.GetString2("StructFormCreateText");
                buttonExOK.Text = ResourceLoader.GetString("StructFormCreateButtonOkText");
                buttonExCancel.Text = ResourceLoader.GetString("StructFormCreateButtonCancelText");
            }
            else if (Behavior == FormBehavior.Edit)
            {
                Text = ResourceLoader.GetString2("StructFormEditText");
                buttonExOK.Text = ResourceLoader.GetString("StructFormEditButtonOkText");
                buttonExCancel.Text = ResourceLoader.GetString("StructFormEditButtonCancelText");
            }

            this.layoutControlItemLat.Text = ResourceLoader.GetString2("Lat") + ":* ";
            this.layoutControlItemLon.Text = ResourceLoader.GetString2("Lon") + ":* ";
            this.layoutControlGroupAddress.Text = ResourceLoader.GetString2("Address");
            this.layoutControlGroup2.Text = ResourceLoader.GetString2("Devices");
            this.layoutControlGroupStructInf.Text = ResourceLoader.GetString2("StructInformation");
            this.layoutControlItemMun.Text = ResourceLoader.GetString2("StructTown") + ":";
            this.layoutControlItemName.Text = ResourceLoader.GetString2("Name") + ":*";
            this.layoutControlItemState.Text = ResourceLoader.GetString2("StructState") + ":";
            this.layoutControlItemStreet.Text = ResourceLoader.GetString2("StructStreetAv") + ":";
            this.layoutControlItemStructType.Text = ResourceLoader.GetString2("StructType") + ":*";
            this.layoutControlItemUrb.Text = ResourceLoader.GetString2("StructUrbSector") + ":";
            this.layoutControlItemZone.Text = ResourceLoader.GetString2("CCTVZone") + ":*";
            this.labelAssociated.Text = ResourceLoader.GetString2("AssociatedDevices");
            this.labelAvailable.Text = ResourceLoader.GetString2("AvailableDevices");
            }

        private void FillStructTypes()
        {
            IList structTypes = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetAllStructTypes);
            foreach (StructTypeClientData structType in structTypes)
            {
                comboBoxExType.Items.Add(structType);
            }
        }

        public FormBehavior Behavior { get; set; }
        
        public bool MapIsOpen { get; set; }
        
        public StructClientData SelectedStruct
		{
            get
			{
                return selectedStruct;
			}
			set
			{
                selectedStruct = value;
                cctvZoneSelected = null;
                if (selectedStruct != null)
                {
                    gridControlAvail.ClearData();
                    gridControlSel.ClearData();

                    FillCamerasList();

                    textBoxExLat.Text = selectedStruct.Lat.ToString();
                    textBoxExLon.Text = selectedStruct.Lon.ToString();
                    textBoxExState.Text = selectedStruct.State;
                    textBoxExTown.Text = selectedStruct.Town;
                    textBoxExAv.Text = selectedStruct.Street;
                    textBoxExSec.Text = selectedStruct.ZoneSecRef;

                    textBoxExName.Text = selectedStruct.Name;
                    comboBoxCctvZone.SelectedItem = selectedStruct.CctvZone;
                    comboBoxExType.SelectedItem = selectedStruct.Type;
                    comboBoxExType.Tag = selectedStruct.Type;
                }
                StructStatusParameters_Change(null, null);
			}
		}
        
        public bool ButtonOkPressed
        {
            get { return buttonOkPressed; }
        }

        private void StructStatusParameters_Change(object sender, System.EventArgs e)
        {
            if ((textBoxExName.Text.Trim() == string.Empty) || comboBoxExType.SelectedItem == null
                || textBoxExLat.Text.Trim() == string.Empty  || textBoxExLon.Text.Trim() == string.Empty 
                || comboBoxCctvZone.SelectedIndex < 0)
               buttonExOK.Enabled = false;
            else
                buttonExOK.Enabled = true;
        }

        private void buttonExOK_Click(object sender, System.EventArgs e)
		{
			try
			{
                BackgroundProcessForm processForm = new BackgroundProcessForm(this, new MethodInfo[1] { GetType().GetMethod("buttonExOK_Click1", BindingFlags.NonPublic | BindingFlags.Instance) }, new object[1][] { new object[0] });
				processForm.CanThrowError = true;
				processForm.ShowDialog();	
			}
            catch (FaultException ex)
            {
                if (selectedStruct.Code > 0)
                    SelectedStruct = (StructClientData)ServerServiceClient.GetInstance().RefreshClient(selectedStruct);
                GetErrorFocus(ex);
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, MessageFormType.Information);
                buttonOkPressed = false;
            }
            catch (Exception ex)
            {
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, ex);
                buttonOkPressed = false;
            }
		}

      
        private void buttonExOK_Click1()
        {
            buttonOkPressed = true;
            if (Behavior == FormBehavior.Create)
            {
                selectedStruct = new StructClientData();
            }
            selectedStruct.Name = textBoxExName.Text;
            selectedStruct.Type = (StructTypeClientData)comboBoxExType.Tag;
            selectedStruct.Devices = new ArrayList();
            selectedStruct.CctvZone = cctvZoneSelected;
            /*if (selectedStruct.Addres == null)
                selectedStruct.Addres = new CctvStructAddressData(new AddressData("", "", null, null));*/
            selectedStruct.State = textBoxExState.Text;
            selectedStruct.Town = textBoxExTown.Text;
            selectedStruct.ZoneSecRef = textBoxExSec.Text;
            selectedStruct.Street = textBoxExAv.Text;
            if (StructGeoeoPoint != null)
            {
                selectedStruct.Lon = StructGeoeoPoint.Lon;
                selectedStruct.Lat = StructGeoeoPoint.Lat;
                selectedStruct.IsSynchronized = true;
            }

            foreach (MappableDeviceGridData device in gridControlSel.Items)
            {
                selectedStruct.Devices.Add(device.Data as DeviceClientData);
            }

            ServerServiceClient.GetInstance().SaveOrUpdateClientData(selectedStruct);
            //ApplicationServiceClient.Current(UserApplicationClientData.Map).SendEnableButton(false, ButtonType.AddPoint);
        }

        private void GetErrorFocus(FaultException ex)
        {
            if (ex.Message.Contains(ResourceLoader.GetString2("UK_STRUCT_CUSTOM_CODE")))
            {
                if (Behavior == FormBehavior.Create)
                    this.textBoxExName.Text = textBoxExName.Text;

                textBoxExName.Focus();
            }
           
        }

        private void FillCctvZones()
        {         
            foreach (CctvZoneClientData cctvZone in ServerServiceClient.GetInstance().SearchClientObjects(typeof(CctvZoneClientData)))
            {
                comboBoxCctvZone.Items.Add(cctvZone);
            }
            comboBoxCctvZone.Items.Add("");
        }
        
        private void FillCamerasList()
        {
            
            if (comboBoxCctvZone.SelectedItem != null)
            {
                //*************to do Fernando********************
                //create a query that will retrieve all devices
                //including their types (e.g. camera, datalogger)
                //***********************************************

                gridControlAvail.ClearData();
                gridControlSel.ClearData();

                string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetAllVideoDevices);//, ((CctvZoneClientData)comboBoxCctvZone.SelectedItem).Name);
                ArrayList list = (ArrayList)ServerServiceClient.GetInstance().SearchClientObjects(hql);
                foreach (VideoDeviceClientData device in list)
                {
                    if (device.StructClientData != null && selectedStruct != null)
                    {
                        if (device.StructClientData.Name == selectedStruct.Name)
                        {
                            gridControlSel.AddOrUpdateItem(new MappableDeviceGridData(device));
                        }
                    }
                    else if (device.StructClientData == null)
                    {
                        gridControlAvail.AddOrUpdateItem(new MappableDeviceGridData(device));
                    }
                }
            }
        }

        private DeviceType GetDeviceType(VideoDeviceClientData device)
        {
            IList list = ServerServiceClient.GetInstance().SearchClientObjects(
            SmartCadHqls.GetCustomHql(SmartCadHqls.GetCameraByCode,
                device.Code));
            if (list.Count > 0)
            {
                return DeviceType.Camera;
            }
            
            list = null;
            list = ServerServiceClient.GetInstance().SearchClientObjects(
            SmartCadHqls.GetCustomHql(SmartCadHqls.GetDataloggerByCode,
                device.Code));
            if (list.Count > 0)
            {
                return DeviceType.Datalogger;
            }

            return DeviceType.Unknown;
        }

        private void comboBoxCctvZone_SelectedValueChanged(object sender, EventArgs e)
        {
            if ((sender as ComboBox).SelectedItem.ToString() != string.Empty)
            {
                cctvZoneSelected = (CctvZoneClientData)(sender as ComboBox).SelectedItem;
                StructStatusParameters_Change(null, null);
            }
        }

        private void listBoxExAvailablesCameras_DoubleClick(object sender, EventArgs e)
        {
            buttonExAddCamera_Click(sender, e);
        }
        
        private void listBoxExAvailablesCameras_Enter(object sender, EventArgs e)
        {
            
        }       
        
        private void listBoxExUnAvailablesCameras_DoubleClick(object sender, EventArgs e)
        {
            buttonExRemoveCamera_Click(sender, e);
        }
        
        private void listBoxExUnAvailablesCameras_Enter(object sender, EventArgs e)
        {
            
        }

        private void buttonExAddCamera_Click(object sender, EventArgs e)
        {
            foreach (MappableDeviceGridData device in gridControlAvail.SelectedItems)
            {
                gridControlSel.AddOrUpdateItem(device);
                gridControlAvail.DeleteItem(device);
            }
        }
        
        private void buttonExAddAllCameras_Click(object sender, EventArgs e)
        {
            gridControlSel.AddOrUpdateList(gridControlAvail.Items);
            gridControlAvail.ClearData();
        }
        
        private void buttonExRemoveCamera_Click(object sender, EventArgs e)
        {
            foreach (MappableDeviceGridData device in gridControlSel.SelectedItems)
            {
                gridControlAvail.AddOrUpdateItem(device);
                gridControlSel.DeleteItem(device);
            }
        }
        
        private void buttonExRemoveAllCameras_Click(object sender, EventArgs e)
        {
            gridControlAvail.AddOrUpdateList(gridControlSel.Items);
            gridControlSel.ClearData();
        }

        private void serverServiceClient_GisAction(object sender, GisActionEventArgs e)
        {
            try
            {
                if (e is SendGeoPointEventArgs)
                {
                    GeoPoint point = (e as SendGeoPointEventArgs).Point;
                    if ( point != null)
                    {
                        StructGeoeoPoint = point;
                        FormUtil.InvokeRequired(this, () =>
                        {
                            textBoxExLat.Text = point.Lat.ToString();
                            textBoxExLon.Text = point.Lon.ToString();
                            StructStatusParameters_Change(null, null);
                        });

                        if (SelectedStruct != null)
                        {
                            SelectedStruct.Lon = point.Lon;
                            SelectedStruct.Lat = point.Lat;
                        }
                    }
                }
                else if (e is SendACKEventArgs)
                {
                    if (((SendACKEventArgs)e).Connected)
                    {
                        MapIsOpen = true;
                        FormUtil.InvokeRequired(this, () =>
                        {
                            if (buttonExMap.Enabled)
                            {
                                SendActionsToMap();
                                buttonExMap.Enabled = true;
                            }
                        });
                    }
                    else
                    {
                        MapIsOpen = false;
                        FormUtil.InvokeRequired(this, () => buttonExMap.Enabled = true);
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        private void buttonExMap_Click(object sender, EventArgs e)
        {
           
          //  System.Threading.Thread.Sleep(30000);
            SendActionsToMap();
        }

       private void SendActionsToMap()
        {
            if (MapIsOpen)
            {
                if (SelectedStruct != null && SelectedStruct.Lon != 0.0 && SelectedStruct.Lat != 0.0)
                {
                    ApplicationServiceClient.Current(UserApplicationClientData.Map).DisplayGeoPointInGis
                        (new GeoPoint(SelectedStruct.Lon, SelectedStruct.Lat));
                }

                ApplicationServiceClient.Current(UserApplicationClientData.Map).SendEnableButton(true, ButtonType.AddPoint);
                ApplicationServiceClient.Current(UserApplicationClientData.Map).SendToActivateLayer(ShapeType.Struct);
                ApplicationServiceClient.Current(UserApplicationClientData.Map).InsertAddressDataInGis(
                    new AddressClientData(textBoxExSec.Text, textBoxExAv.Text, string.Empty, string.Empty, true));
            }
            else if (buttonExMap.Enabled)
            {
                GeoPoint point = new GeoPoint(0, 0);
                if (SelectedStruct != null && SelectedStruct.Lon != 0.0 && SelectedStruct.Lat != 0.0)
                {
                    point = new GeoPoint(SelectedStruct.Lon, SelectedStruct.Lat);
                }

                ApplicationUtil.LaunchApplicationWithArguments(
                    SmartCadConfiguration.SmartCadMapFilename,
                    new object[] 
                    { 
                        ServerServiceClient.GetInstance().OperatorClient.Login,
                        AdministrationRibbonForm.Password,
                        ApplicationUtil.CheckPrimaryScreenBounds(this.Left),
                        new SendActivateLayerEventArgs(ShapeType.Struct),
                        new SendEnableButtonEventArgs(ButtonType.AddPoint, true),
                        new DisplayGeoPointEventArgs(point)
                    }
                );
            }
        }



        private void comboBoxExType_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBoxExType.Tag = (StructTypeClientData)(sender as ComboBox).SelectedItem;
            StructStatusParameters_Change(null, null);
        }

        private void comboBoxCctvZone_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillCamerasList();
        }

        private void buttonExMap_MouseHover(object sender, EventArgs e)
        {
            this.toolTip1.SetToolTip(this.buttonExMap, ResourceLoader.GetString2("LocateOnMap"));
        }

        private void buttonExAddCamera_MouseHover(object sender, EventArgs e)
        {
            this.toolTip1.SetToolTip(this.buttonExAddCamera, ResourceLoader.GetString2("StructForm.AddCamera"));
        }

        private void buttonExAddAllCameras_MouseHover(object sender, EventArgs e)
        {
            this.toolTip1.SetToolTip(this.buttonExAddAllCameras, ResourceLoader.GetString2("StructForm.AddAllCameras"));
        }

        private void buttonExRemoveCamera_MouseHover(object sender, EventArgs e)
        {
            this.toolTip1.SetToolTip(this.buttonExRemoveCamera, ResourceLoader.GetString2("StructForm.RemoveCamera"));
        }

        private void buttonExRemoveAllCameras_MouseHover(object sender, EventArgs e)
        {
            this.toolTip1.SetToolTip(this.buttonExRemoveAllCameras, ResourceLoader.GetString2("StructForm.RemoveAllCameras"));
        }

        private void StructForm_Load(object sender, EventArgs e)
        {
            ApplicationServiceClient.Current(UserApplicationClientData.Map).SendACK(true);
            //this.gridControlDatalogger.Type = typeof(DataloggerGridData);
        }      
    }
}
