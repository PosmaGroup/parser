using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using SmartCadCore.ClientData;
using SmartCadCore.Core;
using Smartmatic.SmartCad.Service;

using System.Collections;
using System.Linq;
using System.Threading;
using SmartCadCore.Common;
using System.IO;
using System.Diagnostics;
using SmartCadCore.Enums;
using SmartCadControls.SyncBoxes;
using Smartmatic.SmartCad.Map;
using SmartCadGuiCommon.Services;
using SmartCadGuiCommon.Controls;

namespace SmartCadGuiCommon
{
    public partial class DepartmentZoneXtraForm : DevExpress.XtraEditors.XtraForm
    {
        private DepartmentZoneClientData selectedDepartmentZone;
        private AdministrationRibbonForm parentForm;
        private object syncCommitted = new object();
        private bool internalUpdate = false;
        IList originalPoints = new ArrayList();
        private bool reloadSelectedData = true;
        //private bool depTypeWasModified;

        public DepartmentZoneClientData SelectedDepartmentZone
        {
            get
            {
                return selectedDepartmentZone;
            }
            set
            {
                selectedDepartmentZone = value;
                if (Behavior == FormBehavior.Edit)
                {
                    FormUtil.InvokeRequired(this, () =>
                    {
                        this.textEditDepartmentZoneName.Text = selectedDepartmentZone.Name;
                        int index = this.comboBoxEditDepartmentTypes.Properties.Items.IndexOf(selectedDepartmentZone.DepartmentType);
                        if (index != -1)
                        {
                            this.comboBoxEditDepartmentTypes.SelectedIndex = index;
                        }
                        this.textEditCustomCode.Text = selectedDepartmentZone.CustomCode;
                        this.Size = new Size(480, 462);
                        this.MinimumSize = new Size(480, 462);
                        if (selectedDepartmentZone.DepartmentZoneAddress != null)
                        {
                            this.gridControlExCoordinates.SetDataSource(selectedDepartmentZone.DepartmentZoneAddress);
                            this.gridControlExCoordinates.AutoAdjustColumnWidth();
                            originalPoints = selectedDepartmentZone.DepartmentZoneAddress;
                        }
                        if (selectedDepartmentZone.DepartmentStations != null)
                        {
                            this.gridControlExStations.SetDataSource(selectedDepartmentZone.DepartmentStations);
                            this.gridControlExStations.AutoAdjustColumnWidth();
                        }
                    });
                }
                else
                {
                    FormUtil.InvokeRequired(this, () =>
                    {
                        this.layoutControlGroupStations.HideToCustomization();
                        this.MinimumSize = new Size(480, 313);
                        this.Size = new Size(480, 313);
                    });
                }
                internalUpdate = false;
            }
        }

        public FormBehavior Behavior { get; set; }

        public bool MapIsOpen { get; set; }

        public DepartmentZoneXtraForm(AdministrationRibbonForm parentForm,
        DepartmentZoneClientData departmentZone, FormBehavior behavior)
        {
            InitializeComponent();
            ConfigureGridControls();
            this.Behavior = behavior;
            ApplicationServerService.GisAction += serverServiceClient_GisAction;
            
            IList departments = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetDepartmentsType);
            foreach (DepartmentTypeClientData dep in departments)
            {
                comboBoxEditDepartmentTypes.Properties.Items.Add(dep);
            }
            this.SelectedDepartmentZone = departmentZone;
            LoadLanguage();
            this.parentForm = parentForm;
            this.parentForm.AdministrationCommittedChanges += DepartmentZoneXtraForm_AdministrationCommittedChanges;



            EnableButtons(null, EventArgs.Empty);

            var applications = ServerServiceClient.GetInstance().GetActiveApplications();
            if (!applications.Contains(SmartCadApplications.SmartCadMap))
            {
              
                emptySpaceItem2.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                layoutControlItem4.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                layoutControlItem1.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                layoutControlItemCoordinates.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                this.AutoSize = true;
                if (departmentZone == null) 
                {
                    this.MinimumSize = new Size(480, 200);
                    this.Size = new Size(480, 200);
                }               
            }

        }

        private void DepartmentZoneXtraForm_Load(object sender, EventArgs e)
        {
            ApplicationServiceClient.Current(UserApplicationClientData.Map).SendACK(true);
            var applications = ServerServiceClient.GetInstance().GetActiveApplications();
            this.gridViewExCoordinates.SingleSelectionChanged += gridViewExCoordinates_SingleSelectionChanged;
            this.gridViewExCoordinates.Columns[0].SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;
            this.Width = 488;
            
            //this.Height = 463;
            
            

            //if (Behavior == FormBehavior.Edit && MapIsOpen)
            //{
            //    SendActionsToMap();
            //}
        }

        private void ConfigureGridControls()
        {
            this.gridControlExStations.Type = typeof(GridControlDataAdmDepartmentStation);
            this.gridControlExStations.EnableAutoFilter = false;
            this.gridControlExStations.ViewTotalRows = false;

            this.gridControlExCoordinates.Type = typeof(GridControlDataDepartmentZoneCoord);
            this.gridControlExCoordinates.EnableAutoFilter = false;
            this.gridControlExCoordinates.ViewTotalRows = false;
            this.comboBoxEditDepartmentTypes.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
        }

        void DepartmentZoneXtraForm_AdministrationCommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
        {
            try
            {
                if (e.Objects != null && e.Objects.Count > 0)
                {
                    lock (syncCommitted)
                    {
                        try
                        {
                            if (ServerServiceClient.GetInstance().OperatorClient == null)
                            {
                            }
                        }
                        catch
                        {
                        }

                        if (e.Objects[0] is DepartmentZoneClientData)
                        {
                            #region DepartmentZoneClientData
                            DepartmentZoneClientData departmentZone = e.Objects[0] as DepartmentZoneClientData;
                            if (SelectedDepartmentZone != null && SelectedDepartmentZone.Equals(departmentZone))
                            {
                                if (e.Action == CommittedDataAction.Update)
                                {
                                    if (!internalUpdate)
                                    {
                                        MessageForm.Show(String.Format("{0}: {1}", ResourceLoader.GetString2("DepartmentZoneWasUpdated"), SelectedDepartmentZone), MessageFormType.Warning);
                                    }
                                    
                                    if (reloadSelectedData)
                                    {
                                        SelectedDepartmentZone = departmentZone;
                                    }
                                    reloadSelectedData = false;
                                }
                                else
                                    if (e.Action == CommittedDataAction.Delete)
                                    {
                                        MessageForm.Show(String.Format("{0}: {1}", ResourceLoader.GetString2("DeleteFormDepartmentZoneData"), SelectedDepartmentZone), MessageFormType.Warning);
                                        FormUtil.InvokeRequired(this, () => this.Close());
                                    }
                            }
                            #endregion
                        }
                        else
                            if (e.Objects[0] is DepartmentTypeClientData)
                            {
                                #region DepartmentTypeClientData
                                DepartmentTypeClientData departmentType =
                                e.Objects[0] as DepartmentTypeClientData;

                                if (e.Action == CommittedDataAction.Save)
                                {
                                    FormUtil.InvokeRequired(this.comboBoxEditDepartmentTypes, () => comboBoxEditDepartmentTypes.Properties.Items.Add(departmentType));
                                }
                                else
                                    if (e.Action == CommittedDataAction.Update)
                                    {
                                        FormUtil.InvokeRequired(this.comboBoxEditDepartmentTypes, () =>
                                        {
                                            int index = this.comboBoxEditDepartmentTypes.Properties.Items.IndexOf(departmentType);
                                            if (index != -1)
                                            {
                                                this.comboBoxEditDepartmentTypes.Properties.Items[index] = departmentType;
                                            }
                                            if (SelectedDepartmentZone != null && SelectedDepartmentZone.DepartmentType.Code == departmentType.Code)
                                            {
                                                MessageForm.Show(String.Format("{0}: {1}", ResourceLoader.GetString2("DepartmentTypeWasUpdated"), departmentType), MessageFormType.Warning);
                                                try
                                                {
                                                    this.comboBoxEditDepartmentTypes.SelectedIndex = -1;
                                                    this.comboBoxEditDepartmentTypes.SelectedIndex = index;
                                                }
                                                catch
                                                {
                                                }
                                            }
                                            else
                                                if (index != -1 && comboBoxEditDepartmentTypes.SelectedItem != null && comboBoxEditDepartmentTypes.SelectedItem.Equals(departmentType))
                                                {
                                                    this.comboBoxEditDepartmentTypes.SelectedIndex = -1;
                                                    this.comboBoxEditDepartmentTypes.SelectedIndex = index;
                                                }
                                        });
                                    }
                                #endregion
                            }
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        private void LoadLanguage()
        {
            FormUtil.InvokeRequired(this, () =>
            {
                if (Behavior == FormBehavior.Create)
                {
                    this.Text = ResourceLoader.GetString2("FormCreateDepartmentZone");
                    this.simpleButtonAccept.Text = ResourceLoader.GetString2("Create");
                }
                else
                    if (Behavior == FormBehavior.Edit)
                    {
                        this.Text = ResourceLoader.GetString2("FormEditDepartmentZone");
                        this.simpleButtonAccept.Text = ResourceLoader.GetString2("Accept");
                    }
                this.layoutControlItemZoneName.Text = ResourceLoader.GetString2("$Message.Name") + ":* ";
                this.layoutControlItemDepartment.Text = ResourceLoader.GetString2("DepartmentType") + ":* ";
                this.layoutControlItemCustomCode.Text = ResourceLoader.GetString2("CustomCode") + ":";
                //this.simpleButtonAccept.Text = ResourceLoader.GetString2("Create");
                this.simpleButtonCancel.Text = ResourceLoader.GetString2("$Message.Cancel");
                this.layoutControlGroupData.Text = ResourceLoader.GetString2("ZoneInformation");
                this.layoutControlGroupStations.Text = ResourceLoader.GetString2("DepartmentStations");
                this.simpleButtonMap.ToolTip = ResourceLoader.GetString2("OpenMap");
                this.labelControlCoordinates.Text = ResourceLoader.GetString2("Coordinates") + ":";
            });
        }

        private double CalculateArea(IList<DepartmentZoneAddressClientData> points)
        {
            float area = 0;
            double x1, x2, y1, y2;
            for (int i = 0; i < points.Count; i++)
            {
                if ((i + 1) == points.Count)
                {
                    x1 = Math.Abs(points[i].Lon);
                    y1 = Math.Abs(points[i].Lat);
                    x2 = Math.Abs(points[0].Lon);
                    y2 = Math.Abs(points[0].Lat);
                }
                else
                {
                    x1 = Math.Abs(points[i].Lon);
                    y1 = Math.Abs(points[i].Lat);
                    x2 = Math.Abs(points[i + 1].Lon);
                    y2 = Math.Abs(points[i + 1].Lat);
                }

                area += (float)Math.Abs(x1 * y2 - x2 * y1);
            }
            return GisServiceUtil.DISTANCE_PER_DEGREE * Math.Abs(area / 2);
        }

        private void simpleButtonAccept_Click(object sender, EventArgs e)
        {
            List<DepartmentZoneAddressClientData> points = new List<DepartmentZoneAddressClientData>();
            if (Behavior == FormBehavior.Create)
            {
                selectedDepartmentZone = new DepartmentZoneClientData();
                selectedDepartmentZone.Name = this.textEditDepartmentZoneName.Text;
                selectedDepartmentZone.CustomCode = this.textEditCustomCode.Text;

                if (gridControlExCoordinates.Items.Count > 0)
                {
                    foreach (GridControlDataDepartmentZoneCoord var in gridControlExCoordinates.Items)
                    {
                        DepartmentZoneAddressClientData dz = new DepartmentZoneAddressClientData();
                        dz.Lon = var.Lon;
                        dz.Lat = var.Lat;
                        dz.PointNumber = var.PointNumber;
                        points.Add(dz);
                    }
                    if (CalculateArea(points) <= 0.0001)
                    {
                        MessageForm.Show(ResourceLoader.GetString2("PolygonWithoutArea"), MessageFormType.Error);
                        DialogResult = DialogResult.None;
                        return;
                    }
                }

                selectedDepartmentZone.DepartmentZoneAddress = points;
                DepartmentTypeClientData dept = comboBoxEditDepartmentTypes.SelectedItem as DepartmentTypeClientData;
                if (dept != null)
                {
                    selectedDepartmentZone.DepartmentType = dept;
                }
                try
                {
                    ServerServiceClient.GetInstance().SaveOrUpdateClientData(selectedDepartmentZone);
                }
                catch (Exception ex)
                {
                    DialogResult = DialogResult.None;
                    MessageForm.Show(ex.Message, ex);
                    HandleUKExceptions(ex);
                }
            }
            else
                if (Behavior == FormBehavior.Edit)
                {
                    DepartmentZoneClientData departmentZone = selectedDepartmentZone.Clone() as DepartmentZoneClientData;
                    departmentZone.Name = this.textEditDepartmentZoneName.Text;
                    departmentZone.CustomCode = this.textEditCustomCode.Text;
                    points.Clear();
                    DepartmentTypeClientData dept = comboBoxEditDepartmentTypes.SelectedItem as DepartmentTypeClientData;
                    if (dept != null)
                    {
                        departmentZone.DepartmentType = dept;
                    }

                    departmentZone.DepartmentZoneAddress = new ArrayList();
                    if (gridControlExCoordinates.DataSource != null)
                    {
                        foreach (GridControlDataDepartmentZoneCoord var in gridControlExCoordinates.Items)
                        {
                            departmentZone.DepartmentZoneAddress.Add((DepartmentZoneAddressClientData)var.Tag);
                            points.Add((DepartmentZoneAddressClientData)var.Tag);
                        }

                        if (points.Count > 0 && CalculateArea(points) <= 0.0001)
                        {
                            MessageForm.Show(ResourceLoader.GetString2("PolygonWithoutArea"), MessageFormType.Error);
                            DialogResult = DialogResult.None;
                            return;
                        }
                        else
                        {
                            ApplicationServiceClient.Current(UserApplicationClientData.Map).SendGeoPointList(
                                points.ConvertAll<GeoPoint>(obj => new GeoPoint(obj.Lon,obj.Lat))
                                , departmentZone.DepartmentType.Code, departmentZone.Code, ShapeType.Zone);
                        }
                    }
                    else
                    {
                        departmentZone.DepartmentZoneAddress = selectedDepartmentZone.DepartmentZoneAddress;

                        //if (depTypeWasModified)
                        //{
                        //    selectedDepartmentZone.DepartmentZoneAddress.Clear();
                        //}
                    }
                   
                    try
                    {
                        ServerServiceClient.GetInstance().SaveOrUpdateClientData(departmentZone);
                    }
                    catch (Exception ex)
                    {
                        MessageForm.Show(ex.Message, MessageFormType.Error);
                        internalUpdate = true;
                        DialogResult = DialogResult.None;
                        ServerServiceClient.GetInstance().AskUpdatedObjectToServer(selectedDepartmentZone, null);
                        HandleUKExceptions(ex);
                    }
                }
        }

        private void HandleUKExceptions(Exception ex)
        {
            if (ex != null)
            {
                FormUtil.InvokeRequired(this, () =>
                {
                    if (ex.Message.Equals(ResourceLoader.GetString2("UK_DEPARTMENT_ZONE_NAME")))
                    {
                        this.textEditDepartmentZoneName.Focus();
                        this.textEditDepartmentZoneName.SelectAll();
                    }
                    else
                        if (ex.Message.Equals(ResourceLoader.GetString2("UK_DEPARTMENT_ZONE_CUSTOM_CODE")))
                        {
                            this.textEditCustomCode.Focus();
                            this.textEditCustomCode.SelectAll();
                        }
                });
            }
        }

        private void DepartmentZoneXtraForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.parentForm != null)
            {
                this.parentForm.AdministrationCommittedChanges -= DepartmentZoneXtraForm_AdministrationCommittedChanges;
            }
            ApplicationServerService.GisAction -= serverServiceClient_GisAction;
            ApplicationServiceClient.Current(UserApplicationClientData.Map).SendACK(false);
        }

        private void EnableButtons(object sender, EventArgs e)
        {
            FormUtil.InvokeRequired(this, () =>
            {
                if (SelectedDepartmentZone != null && SelectedDepartmentZone.Code != 0)
                {
                    DepartmentTypeClientData selectedDepartmentType = (DepartmentTypeClientData)this.comboBoxEditDepartmentTypes.SelectedItem;
                    
                    if (selectedDepartmentType != null && SelectedDepartmentZone.DepartmentType.Code != selectedDepartmentType.Code)
                    {
                        if (SelectedDepartmentZone.DepartmentZoneAddress != null &&
                            SelectedDepartmentZone.DepartmentZoneAddress.Count > 0)
                        {
                            DialogResult result = MessageForm.Show(ResourceLoader.GetString2("LocationStationWillBeDeleted"), MessageFormType.Question);
                            if (result == DialogResult.Yes)
                            {
                                //depTypeWasModified = true;
                                internalUpdate = true;
                                reloadSelectedData = false; //used in commitedChanges
                                SelectedDepartmentZone.DepartmentZoneAddress = new ArrayList();
                                ServerServiceClient.GetInstance().SaveOrUpdateClientData(SelectedDepartmentZone);
                                
                                //SelectedDepartmentZone.DepartmentType = selectedDepartmentType;
                                gridControlExCoordinates.BeginUpdate();
                                gridControlExCoordinates.ClearData();
                                gridControlExCoordinates.EndUpdate();
                            }
                            else
                            {
                                comboBoxEditDepartmentTypes.SelectedItem = selectedDepartmentZone.DepartmentType;
                            }
                        }
                        //Notify the changes to map.
                        SendActionsToMap();
                    }
                }
                this.simpleButtonMap.Enabled = !MapIsOpen && comboBoxEditDepartmentTypes.SelectedIndex > -1;
                if (this.textEditDepartmentZoneName.Text.Trim().Length > 0 && this.comboBoxEditDepartmentTypes.SelectedIndex > -1)
                {
                    this.simpleButtonAccept.Enabled = true;
                }
                else
                {
                    this.simpleButtonAccept.Enabled = false;
                }
            });
        }

        private void simpleButtonMap_Click(object sender, EventArgs e)
        {
            SendActionsToMap();
        }

        private void SendActionsToMap()
        {
            if (MapIsOpen)
            {
                ApplicationServiceClient.Current(UserApplicationClientData.Map).SendEnableButton(true, ButtonType.AddPolygon);

                DepartmentTypeClientData dep = ((DepartmentTypeClientData)comboBoxEditDepartmentTypes.SelectedItem);
                ApplicationServiceClient.Current(UserApplicationClientData.Map).SendToActivateLayer(
                ShapeType.Zone,
                dep != null ? dep.Code : -1,
                (selectedDepartmentZone != null) ? selectedDepartmentZone.Code : -1);

                if (selectedDepartmentZone != null && selectedDepartmentZone.DepartmentZoneAddress != null &&
                selectedDepartmentZone.DepartmentZoneAddress.Count > 0)
                {
                    DepartmentZoneAddressClientData address = (DepartmentZoneAddressClientData)selectedDepartmentZone.DepartmentZoneAddress[0];
                    ApplicationServiceClient.Current(UserApplicationClientData.Map).DisplayGeoPointInGis(new GeoPoint(address.Lon, address.Lat));
                }
            }
            else if (simpleButtonMap.Enabled)
            {
                DepartmentZoneAddressClientData address = new DepartmentZoneAddressClientData();

                if (selectedDepartmentZone != null && selectedDepartmentZone.DepartmentZoneAddress != null &&
                selectedDepartmentZone.DepartmentZoneAddress.Count > 0)
                {
                    address = (DepartmentZoneAddressClientData)selectedDepartmentZone.DepartmentZoneAddress[0];
                }

                ApplicationUtil.LaunchApplicationWithArguments(
                SmartCadConfiguration.SmartCadMapFilename,
                new object[] { ServerServiceClient.GetInstance().OperatorClient.Login,
                AdministrationRibbonForm.Password,
                ApplicationUtil.CheckPrimaryScreenBounds(this.Left),
                new SendEnableButtonEventArgs(ButtonType.AddPolygon, true),
                new SendActivateLayerEventArgs(ShapeType.Zone),
                new SendActivateLayerEventArgs(ShapeType.Station),
                new SendGeoPointListEventArgs(new List<GeoPoint>(),
                ((DepartmentTypeClientData)comboBoxEditDepartmentTypes.SelectedItem).Code,
                (selectedDepartmentZone != null) ? selectedDepartmentZone.Code : -1,
                ShapeType.Zone),
                new DisplayGeoPointEventArgs(new GeoPoint(address.Lon, address.Lat)) });
            }
        }

        private void serverServiceClient_GisAction(object sender, GisActionEventArgs e)
        {
            try
            {
                if (e is SendGeoPointListEventArgs)
                {
                    #region GeoPointList
                    List<GeoPoint> points = (e as SendGeoPointListEventArgs).Points;
                    FormUtil.InvokeRequired(this, () =>
                    {
                        if (points != null && points.Count() > 0)
                        {
                            gridControlExCoordinates.ClearData();
                            int count = gridControlExCoordinates.Items.Count;
                            gridControlExCoordinates.BeginUpdate();
                            foreach (GeoPoint p in points)
                            {
                                DepartmentZoneAddressClientData dzacd = new DepartmentZoneAddressClientData();
                                dzacd.PointNumber = (count++) + "";
                                dzacd.Lat = p.Lat;
                                dzacd.Lon = p.Lon;
                                gridControlExCoordinates.AddOrUpdateItem(new GridControlDataDepartmentZoneCoord(dzacd));
                            }
                            //DepartmentTypeClientData deptType = comboBoxEditDepartmentTypes.SelectedItem as DepartmentTypeClientData;
                            List<GeoPoint> tosendFinal = SortConvertGridCoordinates();
                            gridControlExCoordinates.EndUpdate();

                            //ApplicationServiceClient.Current(UserApplicationClientData.Map).SendGeoPointList(tosendFinal, deptType.Code,
                            //(selectedDepartmentZone != null) ? selectedDepartmentZone.Code : -1, ShapeType.Zone);
                        }
                        else
                        {
                            if ((e as SendGeoPointListEventArgs).Type == ShapeType.None)
                            {
                                gridControlExCoordinates.ClearData();
                            }
                        }
                    });
                    #endregion
                }
                else if (e is SendACKEventArgs)
                {
                    if (((SendACKEventArgs)e).Connected)
                    {
                        MapIsOpen = true;
                        FormUtil.InvokeRequired(this, () =>
                        {
                            if (simpleButtonMap.Enabled)
                            {
                                SendActionsToMap();
                                simpleButtonMap.Enabled = false;
                            }
                        });
                    }
                    else
                    {
                        MapIsOpen = false;
                        FormUtil.InvokeRequired(this, () => 
                            simpleButtonMap.Enabled = (comboBoxEditDepartmentTypes.SelectedIndex > -1));
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        void gridViewExCoordinates_SingleSelectionChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (e.FocusedRowHandle >= 0)
            {
                try
                {
                    GridControlDataDepartmentZoneCoord coord = null;
                    FormUtil.InvokeRequired(this.gridControlExCoordinates, () =>
                    {
                        IList list = this.gridControlExCoordinates.SelectedItems;
                        if (list.Count > 0)
                        {
                            coord = list[0] as GridControlDataDepartmentZoneCoord;
                        }
                    });

                    if (coord != null)
                    {
                        ApplicationServiceClient.Current(UserApplicationClientData.Map).
                            DisplayGeoPointInGis(new GeoPoint(coord.Lon, coord.Lat));
                    }
                }
                catch
                {
                }
            }
        }

        private void comboBoxEditDepartmentTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxEditDepartmentTypes.SelectedItem != null && MapIsOpen)
                SendActionsToMap();

            EnableButtons(null, null);
        }

        private List<GeoPoint> SortConvertGridCoordinates()
        {
            List<GridControlDataDepartmentZoneCoord> tosend = gridControlExCoordinates.Items.Cast<GridControlDataDepartmentZoneCoord>().ToList();
            tosend.Sort((a, b) => Int32.Parse(a.PointNumber) - Int32.Parse(b.PointNumber));

            List<GeoPoint> tosendFinal = tosend.ConvertAll<GeoPoint>(obj => new GeoPoint(obj.Lon, obj.Lat));
            return tosendFinal;
        }
    }
}
