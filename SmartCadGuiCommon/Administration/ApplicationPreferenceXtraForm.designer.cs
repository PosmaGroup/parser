using SmartCadControls;
namespace SmartCadGuiCommon
{
    partial class ApplicationPreferenceXtraForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ApplicationPreferenceXtraForm));
            this.textEditType = new DevExpress.XtraEditors.TextEdit();
            this.ApplicationPreferenceXtraFormConvertedLayout = new DevExpress.XtraLayout.LayoutControl();
            this.labelControlToReplace = new DevExpress.XtraEditors.LabelControl();
            this.gridControlExPreference = new SmartCadControls.GridControlEx();
            this.gridViewExPreferences = new SmartCadControls.GridViewEx();
            this.textEditValueRange = new DevExpress.XtraEditors.TextEdit();
            this.textEditUnit = new DevExpress.XtraEditors.TextEdit();
            this.memoEditDescription = new DevExpress.XtraEditors.MemoEdit();
            this.textEditModule = new DevExpress.XtraEditors.TextEdit();
            this.simpleButtonCalculator = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonAccept = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupPreference = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupInformation = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemUnit = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemModule = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemRange = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.textEditType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ApplicationPreferenceXtraFormConvertedLayout)).BeginInit();
            this.ApplicationPreferenceXtraFormConvertedLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExPreference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExPreferences)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditValueRange.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditUnit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditModule.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupPreference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupInformation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemModule)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // textEditType
            // 
            this.textEditType.Enabled = false;
            this.textEditType.Location = new System.Drawing.Point(151, 259);
            this.textEditType.Name = "textEditType";
            this.textEditType.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEditType.Properties.Appearance.BackColor2 = System.Drawing.Color.White;
            this.textEditType.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEditType.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.textEditType.Properties.Appearance.Options.UseBackColor = true;
            this.textEditType.Properties.Appearance.Options.UseFont = true;
            this.textEditType.Properties.Appearance.Options.UseForeColor = true;
            this.textEditType.Properties.Appearance.Options.UseTextOptions = true;
            this.textEditType.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditType.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White;
            this.textEditType.Properties.AppearanceDisabled.BackColor2 = System.Drawing.Color.White;
            this.textEditType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.textEditType.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEditType.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.textEditType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEditType.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEditType.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEditType.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditType.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White;
            this.textEditType.Properties.AppearanceReadOnly.BackColor2 = System.Drawing.Color.White;
            this.textEditType.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black;
            this.textEditType.Properties.AppearanceReadOnly.Options.UseBackColor = true;
            this.textEditType.Properties.AppearanceReadOnly.Options.UseForeColor = true;
            this.textEditType.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.textEditType.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditType.Size = new System.Drawing.Size(313, 19);
            this.textEditType.StyleController = this.ApplicationPreferenceXtraFormConvertedLayout;
            this.textEditType.TabIndex = 4;
            // 
            // ApplicationPreferenceXtraFormConvertedLayout
            // 
            this.ApplicationPreferenceXtraFormConvertedLayout.AllowCustomizationMenu = false;
            this.ApplicationPreferenceXtraFormConvertedLayout.Controls.Add(this.labelControlToReplace);
            this.ApplicationPreferenceXtraFormConvertedLayout.Controls.Add(this.gridControlExPreference);
            this.ApplicationPreferenceXtraFormConvertedLayout.Controls.Add(this.textEditValueRange);
            this.ApplicationPreferenceXtraFormConvertedLayout.Controls.Add(this.textEditUnit);
            this.ApplicationPreferenceXtraFormConvertedLayout.Controls.Add(this.memoEditDescription);
            this.ApplicationPreferenceXtraFormConvertedLayout.Controls.Add(this.textEditModule);
            this.ApplicationPreferenceXtraFormConvertedLayout.Controls.Add(this.simpleButtonCalculator);
            this.ApplicationPreferenceXtraFormConvertedLayout.Controls.Add(this.simpleButtonCancel);
            this.ApplicationPreferenceXtraFormConvertedLayout.Controls.Add(this.textEditType);
            this.ApplicationPreferenceXtraFormConvertedLayout.Controls.Add(this.simpleButtonAccept);
            this.ApplicationPreferenceXtraFormConvertedLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ApplicationPreferenceXtraFormConvertedLayout.Location = new System.Drawing.Point(0, 0);
            this.ApplicationPreferenceXtraFormConvertedLayout.Margin = new System.Windows.Forms.Padding(2);
            this.ApplicationPreferenceXtraFormConvertedLayout.Name = "ApplicationPreferenceXtraFormConvertedLayout";
            this.ApplicationPreferenceXtraFormConvertedLayout.Root = this.layoutControlGroup1;
            this.ApplicationPreferenceXtraFormConvertedLayout.Size = new System.Drawing.Size(472, 426);
            this.ApplicationPreferenceXtraFormConvertedLayout.TabIndex = 16;
            // 
            // labelControlToReplace
            // 
            this.labelControlToReplace.Location = new System.Drawing.Point(151, 359);
            this.labelControlToReplace.Margin = new System.Windows.Forms.Padding(2);
            this.labelControlToReplace.Name = "labelControlToReplace";
            this.labelControlToReplace.Size = new System.Drawing.Size(280, 23);
            this.labelControlToReplace.StyleController = this.ApplicationPreferenceXtraFormConvertedLayout;
            this.labelControlToReplace.TabIndex = 16;
            this.labelControlToReplace.Text = "labelControlToReplace";
            // 
            // gridControlExPreference
            // 
            this.gridControlExPreference.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(2);
            this.gridControlExPreference.EnableAutoFilter = false;
            this.gridControlExPreference.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExPreference.Location = new System.Drawing.Point(7, 27);
            this.gridControlExPreference.MainView = this.gridViewExPreferences;
            this.gridControlExPreference.Margin = new System.Windows.Forms.Padding(2);
            this.gridControlExPreference.Name = "gridControlExPreference";
            this.gridControlExPreference.Size = new System.Drawing.Size(458, 132);
            this.gridControlExPreference.TabIndex = 0;
            this.gridControlExPreference.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExPreferences});
            this.gridControlExPreference.ViewTotalRows = false;
            // 
            // gridViewExPreferences
            // 
            this.gridViewExPreferences.AllowFocusedRowChanged = true;
            this.gridViewExPreferences.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewExPreferences.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExPreferences.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewExPreferences.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewExPreferences.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewExPreferences.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExPreferences.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewExPreferences.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewExPreferences.EnablePreviewLineForFocusedRow = false;
            this.gridViewExPreferences.GridControl = this.gridControlExPreference;
            this.gridViewExPreferences.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.gridViewExPreferences.Name = "gridViewExPreferences";
            this.gridViewExPreferences.OptionsBehavior.FocusLeaveOnTab = true;
            this.gridViewExPreferences.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExPreferences.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewExPreferences.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.gridViewExPreferences.ViewTotalRows = false;
            this.gridViewExPreferences.SelectionWillChange += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewExPreferences_SelectionWillChange);
            // 
            // textEditValueRange
            // 
            this.textEditValueRange.Enabled = false;
            this.textEditValueRange.Location = new System.Drawing.Point(151, 334);
            this.textEditValueRange.Name = "textEditValueRange";
            this.textEditValueRange.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEditValueRange.Properties.Appearance.BackColor2 = System.Drawing.Color.White;
            this.textEditValueRange.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEditValueRange.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.textEditValueRange.Properties.Appearance.Options.UseBackColor = true;
            this.textEditValueRange.Properties.Appearance.Options.UseFont = true;
            this.textEditValueRange.Properties.Appearance.Options.UseForeColor = true;
            this.textEditValueRange.Properties.Appearance.Options.UseTextOptions = true;
            this.textEditValueRange.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditValueRange.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White;
            this.textEditValueRange.Properties.AppearanceDisabled.BackColor2 = System.Drawing.Color.White;
            this.textEditValueRange.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEditValueRange.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEditValueRange.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.textEditValueRange.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEditValueRange.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEditValueRange.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEditValueRange.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditValueRange.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White;
            this.textEditValueRange.Properties.AppearanceReadOnly.BackColor2 = System.Drawing.Color.White;
            this.textEditValueRange.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black;
            this.textEditValueRange.Properties.AppearanceReadOnly.Options.UseBackColor = true;
            this.textEditValueRange.Properties.AppearanceReadOnly.Options.UseForeColor = true;
            this.textEditValueRange.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.textEditValueRange.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditValueRange.Properties.Mask.BeepOnError = true;
            this.textEditValueRange.Properties.Mask.EditMask = "n0";
            this.textEditValueRange.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEditValueRange.Size = new System.Drawing.Size(313, 19);
            this.textEditValueRange.StyleController = this.ApplicationPreferenceXtraFormConvertedLayout;
            this.textEditValueRange.TabIndex = 10;
            // 
            // textEditUnit
            // 
            this.textEditUnit.Enabled = false;
            this.textEditUnit.Location = new System.Drawing.Point(151, 284);
            this.textEditUnit.Name = "textEditUnit";
            this.textEditUnit.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEditUnit.Properties.Appearance.BackColor2 = System.Drawing.Color.White;
            this.textEditUnit.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEditUnit.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.textEditUnit.Properties.Appearance.Options.UseBackColor = true;
            this.textEditUnit.Properties.Appearance.Options.UseFont = true;
            this.textEditUnit.Properties.Appearance.Options.UseForeColor = true;
            this.textEditUnit.Properties.Appearance.Options.UseTextOptions = true;
            this.textEditUnit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditUnit.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White;
            this.textEditUnit.Properties.AppearanceDisabled.BackColor2 = System.Drawing.Color.White;
            this.textEditUnit.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEditUnit.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEditUnit.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.textEditUnit.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEditUnit.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEditUnit.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEditUnit.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditUnit.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White;
            this.textEditUnit.Properties.AppearanceReadOnly.BackColor2 = System.Drawing.Color.White;
            this.textEditUnit.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black;
            this.textEditUnit.Properties.AppearanceReadOnly.Options.UseBackColor = true;
            this.textEditUnit.Properties.AppearanceReadOnly.Options.UseForeColor = true;
            this.textEditUnit.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.textEditUnit.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditUnit.Size = new System.Drawing.Size(313, 19);
            this.textEditUnit.StyleController = this.ApplicationPreferenceXtraFormConvertedLayout;
            this.textEditUnit.TabIndex = 6;
            // 
            // memoEditDescription
            // 
            this.memoEditDescription.Location = new System.Drawing.Point(151, 194);
            this.memoEditDescription.Margin = new System.Windows.Forms.Padding(2);
            this.memoEditDescription.Name = "memoEditDescription";
            this.memoEditDescription.Properties.AllowFocused = false;
            this.memoEditDescription.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.memoEditDescription.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoEditDescription.Properties.Appearance.Options.UseBackColor = true;
            this.memoEditDescription.Properties.Appearance.Options.UseFont = true;
            this.memoEditDescription.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White;
            this.memoEditDescription.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoEditDescription.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.memoEditDescription.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.memoEditDescription.Properties.AppearanceDisabled.Options.UseFont = true;
            this.memoEditDescription.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.memoEditDescription.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoEditDescription.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black;
            this.memoEditDescription.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.memoEditDescription.Properties.AppearanceReadOnly.Options.UseForeColor = true;
            this.memoEditDescription.Properties.LinesCount = 3;
            this.memoEditDescription.Properties.MaxLength = 255;
            this.memoEditDescription.Properties.ReadOnly = true;
            this.memoEditDescription.Size = new System.Drawing.Size(313, 59);
            this.memoEditDescription.StyleController = this.ApplicationPreferenceXtraFormConvertedLayout;
            this.memoEditDescription.TabIndex = 2;
            // 
            // textEditModule
            // 
            this.textEditModule.Enabled = false;
            this.textEditModule.Location = new System.Drawing.Point(151, 309);
            this.textEditModule.Name = "textEditModule";
            this.textEditModule.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEditModule.Properties.Appearance.BackColor2 = System.Drawing.Color.White;
            this.textEditModule.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEditModule.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.textEditModule.Properties.Appearance.Options.UseBackColor = true;
            this.textEditModule.Properties.Appearance.Options.UseFont = true;
            this.textEditModule.Properties.Appearance.Options.UseForeColor = true;
            this.textEditModule.Properties.Appearance.Options.UseTextOptions = true;
            this.textEditModule.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditModule.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White;
            this.textEditModule.Properties.AppearanceDisabled.BackColor2 = System.Drawing.Color.White;
            this.textEditModule.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEditModule.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textEditModule.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.textEditModule.Properties.AppearanceDisabled.Options.UseFont = true;
            this.textEditModule.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textEditModule.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.textEditModule.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditModule.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White;
            this.textEditModule.Properties.AppearanceReadOnly.BackColor2 = System.Drawing.Color.White;
            this.textEditModule.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black;
            this.textEditModule.Properties.AppearanceReadOnly.Options.UseBackColor = true;
            this.textEditModule.Properties.AppearanceReadOnly.Options.UseForeColor = true;
            this.textEditModule.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.textEditModule.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEditModule.Properties.Mask.BeepOnError = true;
            this.textEditModule.Properties.Mask.EditMask = "n0";
            this.textEditModule.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEditModule.Size = new System.Drawing.Size(313, 19);
            this.textEditModule.StyleController = this.ApplicationPreferenceXtraFormConvertedLayout;
            this.textEditModule.TabIndex = 8;
            // 
            // simpleButtonCalculator
            // 
            this.simpleButtonCalculator.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.simpleButtonCalculator.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButtonCalculator.Appearance.Options.UseFont = true;
            this.simpleButtonCalculator.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonCalculator.Image")));
            this.simpleButtonCalculator.Location = new System.Drawing.Point(436, 358);
            this.simpleButtonCalculator.Name = "simpleButtonCalculator";
            this.simpleButtonCalculator.Size = new System.Drawing.Size(29, 25);
            this.simpleButtonCalculator.StyleController = this.ApplicationPreferenceXtraFormConvertedLayout;
            this.simpleButtonCalculator.TabIndex = 13;
            this.simpleButtonCalculator.ToolTip = "Calcular tiempo";
            this.simpleButtonCalculator.Visible = false;
            this.simpleButtonCalculator.Click += new System.EventHandler(this.simpleButtonCalculator_Click);
            // 
            // simpleButtonCancel
            // 
            this.simpleButtonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonCancel.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButtonCancel.Appearance.Options.UseFont = true;
            this.simpleButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonCancel.Location = new System.Drawing.Point(388, 392);
            this.simpleButtonCancel.Name = "simpleButtonCancel";
            this.simpleButtonCancel.Size = new System.Drawing.Size(82, 32);
            this.simpleButtonCancel.StyleController = this.ApplicationPreferenceXtraFormConvertedLayout;
            this.simpleButtonCancel.TabIndex = 15;
            this.simpleButtonCancel.Text = "Cancelar";
            // 
            // simpleButtonAccept
            // 
            this.simpleButtonAccept.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonAccept.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButtonAccept.Appearance.Options.UseFont = true;
            this.simpleButtonAccept.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.simpleButtonAccept.Location = new System.Drawing.Point(302, 392);
            this.simpleButtonAccept.Name = "simpleButtonAccept";
            this.simpleButtonAccept.Size = new System.Drawing.Size(82, 32);
            this.simpleButtonAccept.StyleController = this.ApplicationPreferenceXtraFormConvertedLayout;
            this.simpleButtonAccept.TabIndex = 14;
            this.simpleButtonAccept.Text = "Accept";
            this.simpleButtonAccept.Click += new System.EventHandler(this.simpleButtonAccept_Click);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlGroupPreference,
            this.layoutControlGroupInformation,
            this.emptySpaceItem2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(472, 426);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.simpleButtonCancel;
            this.layoutControlItem7.CustomizationFormText = "simpleButtonCancelitem";
            this.layoutControlItem7.Location = new System.Drawing.Point(386, 390);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem7.Name = "simpleButtonCancelitem";
            this.layoutControlItem7.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "simpleButtonCancelitem";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.simpleButtonAccept;
            this.layoutControlItem8.CustomizationFormText = "simpleButtonAcceptitem";
            this.layoutControlItem8.Location = new System.Drawing.Point(300, 390);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem8.Name = "simpleButtonAcceptitem";
            this.layoutControlItem8.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "simpleButtonAcceptitem";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlGroupPreference
            // 
            this.layoutControlGroupPreference.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroupPreference.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.layoutControlGroupPreference.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupPreference.Name = "layoutControlGroupPreference";
            this.layoutControlGroupPreference.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupPreference.Size = new System.Drawing.Size(472, 166);
            this.layoutControlGroupPreference.Text = "layoutControlGroupPreference";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridControlExPreference;
            this.layoutControlItem1.CustomizationFormText = "gridControlExPreferenceitem";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "gridControlExPreferenceitem";
            this.layoutControlItem1.Size = new System.Drawing.Size(462, 136);
            this.layoutControlItem1.Text = "gridControlExPreferenceitem";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlGroupInformation
            // 
            this.layoutControlGroupInformation.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroupInformation.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemDescription,
            this.layoutControlItemType,
            this.layoutControlItemUnit,
            this.layoutControlItemModule,
            this.layoutControlItemRange,
            this.layoutControlItem2,
            this.layoutControlItemValue});
            this.layoutControlGroupInformation.Location = new System.Drawing.Point(0, 166);
            this.layoutControlGroupInformation.Name = "layoutControlGroupInformation";
            this.layoutControlGroupInformation.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupInformation.Size = new System.Drawing.Size(472, 224);
            this.layoutControlGroupInformation.Text = "layoutControlGroupInformation";
            // 
            // layoutControlItemDescription
            // 
            this.layoutControlItemDescription.Control = this.memoEditDescription;
            this.layoutControlItemDescription.CustomizationFormText = "layoutControlItemDescription";
            this.layoutControlItemDescription.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemDescription.MinSize = new System.Drawing.Size(162, 22);
            this.layoutControlItemDescription.Name = "layoutControlItemDescription";
            this.layoutControlItemDescription.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItemDescription.Size = new System.Drawing.Size(462, 65);
            this.layoutControlItemDescription.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemDescription.Text = "layoutControlItemDescription";
            this.layoutControlItemDescription.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlItemType
            // 
            this.layoutControlItemType.Control = this.textEditType;
            this.layoutControlItemType.CustomizationFormText = "layoutControlItemType";
            this.layoutControlItemType.Location = new System.Drawing.Point(0, 65);
            this.layoutControlItemType.Name = "layoutControlItemType";
            this.layoutControlItemType.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItemType.Size = new System.Drawing.Size(462, 25);
            this.layoutControlItemType.Text = "layoutControlItemType";
            this.layoutControlItemType.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlItemUnit
            // 
            this.layoutControlItemUnit.Control = this.textEditUnit;
            this.layoutControlItemUnit.CustomizationFormText = "layoutControlItemUnit";
            this.layoutControlItemUnit.Location = new System.Drawing.Point(0, 90);
            this.layoutControlItemUnit.Name = "layoutControlItemUnit";
            this.layoutControlItemUnit.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItemUnit.Size = new System.Drawing.Size(462, 25);
            this.layoutControlItemUnit.Text = "layoutControlItemUnit";
            this.layoutControlItemUnit.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlItemModule
            // 
            this.layoutControlItemModule.Control = this.textEditModule;
            this.layoutControlItemModule.CustomizationFormText = "layoutControlItemModule";
            this.layoutControlItemModule.Location = new System.Drawing.Point(0, 115);
            this.layoutControlItemModule.Name = "layoutControlItemModule";
            this.layoutControlItemModule.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItemModule.Size = new System.Drawing.Size(462, 25);
            this.layoutControlItemModule.Text = "layoutControlItemModule";
            this.layoutControlItemModule.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlItemRange
            // 
            this.layoutControlItemRange.Control = this.textEditValueRange;
            this.layoutControlItemRange.CustomizationFormText = "layoutControlItemRange";
            this.layoutControlItemRange.Location = new System.Drawing.Point(0, 140);
            this.layoutControlItemRange.Name = "layoutControlItemRange";
            this.layoutControlItemRange.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItemRange.Size = new System.Drawing.Size(462, 25);
            this.layoutControlItemRange.Text = "layoutControlItemRange";
            this.layoutControlItemRange.TextSize = new System.Drawing.Size(140, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.simpleButtonCalculator;
            this.layoutControlItem2.CustomizationFormText = "simpleButtonCalculatoritem";
            this.layoutControlItem2.ImageAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlItem2.Location = new System.Drawing.Point(429, 165);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(33, 29);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(33, 29);
            this.layoutControlItem2.Name = "simpleButtonCalculatoritem";
            this.layoutControlItem2.Size = new System.Drawing.Size(33, 29);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "simpleButtonCalculatoritem";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItemValue
            // 
            this.layoutControlItemValue.Control = this.labelControlToReplace;
            this.layoutControlItemValue.CustomizationFormText = "layoutControlItemValue";
            this.layoutControlItemValue.Location = new System.Drawing.Point(0, 165);
            this.layoutControlItemValue.MinSize = new System.Drawing.Size(259, 20);
            this.layoutControlItemValue.Name = "layoutControlItemValue";
            this.layoutControlItemValue.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlItemValue.Size = new System.Drawing.Size(429, 29);
            this.layoutControlItemValue.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemValue.Text = "layoutControlItemValue";
            this.layoutControlItemValue.TextSize = new System.Drawing.Size(140, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 390);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(110, 30);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(300, 36);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ApplicationPreferenceXtraForm
            // 
            this.AcceptButton = this.simpleButtonAccept;
            this.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.simpleButtonCancel;
            this.ClientSize = new System.Drawing.Size(472, 426);
            this.Controls.Add(this.ApplicationPreferenceXtraFormConvertedLayout);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.LookAndFeel.SkinName = "Blue";
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(480, 460);
            this.Name = "ApplicationPreferenceXtraForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Preferencias de la aplicacion";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ApplicationPreferenceXtraForm_FormClosing);
            this.Load += new System.EventHandler(this.ApplicationPreferenceXtraForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.textEditType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ApplicationPreferenceXtraFormConvertedLayout)).EndInit();
            this.ApplicationPreferenceXtraFormConvertedLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExPreference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExPreferences)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditValueRange.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditUnit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditModule.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupPreference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupInformation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemModule)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit textEditType;
        private DevExpress.XtraEditors.SimpleButton simpleButtonAccept;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCancel;
        private DevExpress.XtraEditors.TextEdit textEditModule;
        private DevExpress.XtraEditors.MemoEdit memoEditDescription;
        private DevExpress.XtraEditors.TextEdit textEditUnit;
        private DevExpress.XtraEditors.TextEdit textEditValueRange;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCalculator;
        private GridControlEx gridControlExPreference;
        private GridViewEx gridViewExPreferences;
        private DevExpress.XtraLayout.LayoutControl ApplicationPreferenceXtraFormConvertedLayout;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupPreference;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupInformation;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDescription;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemType;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemUnit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemModule;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemRange;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.LabelControl labelControlToReplace;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemValue;
    }
}