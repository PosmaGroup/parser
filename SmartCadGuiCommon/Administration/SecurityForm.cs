using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using Smartmatic.SmartCad.Service;
using System.ServiceModel;
using System.Reflection;
using Iesi.Collections;
using DevExpress.XtraEditors;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadCore.Enums;
using SmartCadCore.Core;

namespace Smartmatic.SmartCad.Gui
{
    public partial class SecurityForm : XtraForm
    {
        #region Synchronization objects

        private object syncCommited = new object();

        #endregion

        #region Fields

        private FormBehavior behavior;
        private OperatorClientData selectedUser;
        private bool buttonOkPressed = false;
        private bool applyPressed = false;
        private IList rolesWithCCTVAccess;

        #endregion  
        
        public SecurityForm()
        {
            InitializeComponent();
            AssignDataGridType();
            FillUsersList();
            FillCctvZones();
            ServerServiceClient.GetInstance().CommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(SecurityForm_CommittedChanges);
            LoadLanguage();
        }
		
        private void LoadLanguage()
        {
            this.layoutControlGroupOperators.Text = ResourceLoader.GetString2("Users");
            this.layoutControlGroupCameras.Text = ResourceLoader.GetString2("AssignCameraActionToolTip");
            this.layoutControlCCTVZone.Text = ResourceLoader.GetString2("CCTVZone") + ":";
            this.layoutControlStructType.Text = ResourceLoader.GetString2("StructType") + ":";
            this.layoutControlStruct.Text = ResourceLoader.GetString2("Struct") + ":";
            this.layoutControlCamerasAvailable.Text = ResourceLoader.GetString2("AvailablesCameras");
            this.layoutControlCameraAsociated.Text = ResourceLoader.GetString2("AssociatedCameras");
            this.buttonExApply.Text = ResourceLoader.GetString2("$Message.Apply");
            
        }

        public SecurityForm(OperatorClientData user, FormBehavior behavior)
            :this()
		{
			Behavior = behavior;
            SelectedUser = user;
            if (selectedUser != null)
            {
                GridControlDataCctvSecurityOperator oper = new GridControlDataCctvSecurityOperator(selectedUser);
                this.gridControlExOperators.SelectData(oper);
                this.gridViewExOperators.MakeRowVisible(this.gridViewExOperators.FocusedRowHandle, false);                
                this.gridViewExOperators.TopRowIndex = this.gridViewExOperators.FocusedRowHandle;                              
            }
        }

		public FormBehavior Behavior
		{
			set
			{
				behavior = value;
               
			}
			get
			{
				return behavior;
			}
		}
        
        public OperatorClientData SelectedUser
		{
            get
			{
                return selectedUser;
			}
            set
            {
                FormUtil.InvokeRequired(this, delegate
                {
                    selectedUser = value;
                    if (selectedUser != null)
                    {
                        buttonExRemoveAllCameras_Click(null, null);
                 
                        foreach (OperatorDeviceClientData cam in selectedUser.Cameras)
                        {
                            listBoxExUnAvailablesCameras.Items.Add(cam.Device);
                            if (listBoxExAvailablesCameras.Items.Contains(cam.Device) == true)
                            {
                                listBoxExAvailablesCameras.Items.Remove(cam.Device);
                            }
                        }
                    }
                });

                if (applyPressed == true)
                {
                    buttonOkPressed = false;
                    applyPressed = false;                  
                }

                CheckButtons();
            }
		}
        
        public bool ButtonOkPressed
        {
            get { return buttonOkPressed; }
        }

       
        
        private void Clear()
        {
            listBoxExAvailablesCameras.Items.Clear();
            listBoxExUnAvailablesCameras.Items.Clear();
        }

        private void AssignDataGridType()
        {
          gridControlExOperators.Type = typeof(GridControlDataCctvSecurityOperator);
          gridControlExOperators.ViewTotalRows = true;
          gridControlExOperators.EnableAutoFilter = true;
          gridViewExOperators.ViewTotalRows = true;

        }

        private void buttonExOK_Click(object sender, EventArgs e)
        {
            try
            {
                BackgroundProcessForm processForm = new BackgroundProcessForm(this, new MethodInfo[1] { GetType().GetMethod("buttonExOK_Click1", BindingFlags.NonPublic | BindingFlags.Instance) }, new object[1][] { new object[0] });
                processForm.CanThrowError = true;
                processForm.ShowDialog();
       
            }
            catch (FaultException ex)
            {
                selectedUser = (OperatorClientData)ServerServiceClient.GetInstance().RefreshClient(selectedUser);
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, MessageFormType.Error);
                buttonOkPressed = false;
            }
            catch (Exception ex)
            {
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, ex);
                buttonOkPressed = false;
            }
        }

        private void buttonExOK_Click1()
        {
            try
            {
                buttonOkPressed = true;

                if (selectedUser == null)
                {
                    GridControlDataCctvSecurityOperator gridData = gridControlExOperators.SelectedItems[0] as GridControlDataCctvSecurityOperator;
                    SelectedUser = gridData.Operator;
                }

                GetCameras();
                SelectedUser = (OperatorClientData)ServerServiceClient.GetInstance().SaveOrUpdateClientDataWithReturn(SelectedUser);
            }
            catch (Exception ex)
            { if (ex.Message == ResourceLoader.GetString2("NoDeleteUserLoggedIn")) MessageForm.Show(ex.Message, ex); }
        }

        private void GetCameras()
        {
            //ServerServiceClient.GetInstance().InitializeLazy(selectedUser, selectedUser.Cameras);
            //selectedUser.Cameras = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetCamerasByOperatorCode, selectedUser.Code));
            //if (selectedUser.Cameras == null)
            //{
            //    selectedUser.Cameras = new ArrayList();
            //}

            //IList tempList = new ArrayList(selectedUser.Cameras);
            //foreach (OperatorDeviceClientData operCam in tempList)
            //{
            //    if (listBoxExUnAvailablesCameras.Items.Contains(operCam) == false)
            //    {
            //        operCam.End = DateTime.Now;
            //        ServerServiceClient.GetInstance().DeleteClientObject(operCam);
            //        selectedUser.Cameras.Remove(operCam);
            //    }
            //}

            //foreach (OperatorDeviceClientData operCam in listBoxExUnAvailablesCameras.Items)
            //{
            //    if (operCam.Code == 0)
            //    {
            //        OperatorDeviceClientData cam = new OperatorDeviceClientData();
            //        cam.User = selectedUser;
            //        cam.Device = operCam.Device;
            //        cam.Start = DateTime.Now;
            //        selectedUser.Cameras.Add(cam);
            //    }
            //}

            if (selectedUser.Cameras == null)
            {
                selectedUser.Cameras = new ArrayList();
            }
            IList tempList = new ArrayList(selectedUser.Cameras);
            foreach (OperatorDeviceClientData operCam in tempList)
            {
                if (listBoxExAvailablesCameras.Items.Contains(operCam.Device) == true)
                {
                    operCam.End = DateTime.Now;
                    ServerServiceClient.GetInstance().DeleteClientObject(operCam);
                    selectedUser.Cameras.Remove(operCam);
                }
            }
            selectedUser.Cameras.Clear();
            foreach (CameraClientData operCam in listBoxExUnAvailablesCameras.Items)
            {
                 //if (operCam.Code == 0)
                {
                    OperatorDeviceClientData cam = new OperatorDeviceClientData();
                    cam.User = selectedUser;
                    cam.Device = operCam;
                    cam.Start = DateTime.Now;
                    selectedUser.Cameras.Add(cam);
                }
            }
        }
        
        private void listBoxExAvailablesCameras_DoubleClick(object sender, EventArgs e)
        {
            buttonExAddCamera_Click(sender, e);
        }

        private void listBoxExAvailablesCameras_Enter(object sender, EventArgs e)
        {
            listBoxExUnAvailablesCameras.ClearSelected();
        }

        private void listBoxExUnAvailablesCameras_DoubleClick(object sender, EventArgs e)
        {
            buttonExRemoveCamera_Click(sender, e);
        }

        private void listBoxExUnAvailablesCameras_Enter(object sender, EventArgs e)
        {
            listBoxExAvailablesCameras.ClearSelected();
        }

        private void buttonExAddCamera_Click(object sender, EventArgs e)
        {
            ArrayList toDelete = new ArrayList();
            foreach (CameraClientData camera in listBoxExAvailablesCameras.SelectedItems)
            {
                if (buttonExApply.Enabled == false)
                    buttonExApply.Enabled = true;
                toDelete.Add(camera);
                listBoxExUnAvailablesCameras.Items.Add(camera);
            }
            foreach (CameraClientData camera in toDelete)
            {
               listBoxExAvailablesCameras.Items.Remove(camera);
            }

            CheckButtons();
        }

        private void buttonExAddAllCameras_Click(object sender, EventArgs e)
        {

            foreach (CameraClientData camera in listBoxExAvailablesCameras.Items)
            {
                if (buttonExApply.Enabled == false)
                    buttonExApply.Enabled = true;
                listBoxExUnAvailablesCameras.Items.Add(camera);
            }
            listBoxExAvailablesCameras.Items.Clear();


            CheckButtons();
        }

        private void buttonExRemoveCamera_Click(object sender, EventArgs e)
        {
            ArrayList toDelete = new ArrayList();
            foreach (CameraClientData camera in listBoxExUnAvailablesCameras.SelectedItems)
            {
                if (buttonExApply.Enabled == false)
                    buttonExApply.Enabled = true;
                toDelete.Add(camera);
                if ((comboBoxCctvZone.Text == ResourceLoader.GetString2("SecurityForm.Zones.All")) || (camera.StructClientData == null) ||
                    (camera.StructClientData.CctvZone.Code == ((CctvZoneClientData)comboBoxCctvZone.SelectedItem).Code))
                {
                    if ((comboBoxExStructType.Text == ResourceLoader.GetString2("SecurityForm.StructTypes.All")) || (camera.StructClientData == null) ||
                        (camera.StructClientData.Type.Code == ((StructTypeClientData)comboBoxExStructType.SelectedItem).Code))
                    {
                        if ((comboBoxExStruct.SelectedItem.ToString() == ResourceLoader.GetString2("SecurityForm.Structs.All")) || (camera.StructClientData == null) ||
                            (camera.StructClientData.Code == ((StructClientData)comboBoxExStruct.SelectedItem).Code))
                        {
                            if (listBoxExAvailablesCameras.Items.Contains(camera) == false)
                            {
                                listBoxExAvailablesCameras.Items.Add(camera);
                            }
                        }
                    }
                }
            }
            foreach (CameraClientData camera in toDelete)
            {
                int index = -1;
                int i = 0;
                foreach (CameraClientData cam in listBoxExUnAvailablesCameras.Items)
                {
                    if (cam.Code == camera.Code)
                    {
                        index = i;
                        break;
                    }
                    i++;
                }
                if (index != -1)
                    listBoxExUnAvailablesCameras.Items.RemoveAt(index);
            }

            CheckButtons();
        }

        private void buttonExRemoveAllCameras_Click(object sender, EventArgs e)
        {
            foreach (CameraClientData camera in listBoxExUnAvailablesCameras.Items)
            {
                if (buttonExApply.Enabled == false && sender != null)
                    buttonExApply.Enabled = true;

                if ((comboBoxCctvZone.Text == ResourceLoader.GetString2("SecurityForm.Zones.All")) || (camera.StructClientData == null) ||
                    (camera.StructClientData.CctvZone.Code == ((CctvZoneClientData)comboBoxCctvZone.SelectedItem).Code))
                {
                    if ((comboBoxExStructType.Text == ResourceLoader.GetString2("SecurityForm.StructTypes.All")) || (camera.StructClientData == null) ||
                        (camera.StructClientData.Type.Code == ((StructTypeClientData)comboBoxExStructType.SelectedItem).Code))
                    {
                        if ((comboBoxExStruct.SelectedItem.ToString() == ResourceLoader.GetString2("SecurityForm.Structs.All")) || (camera.StructClientData == null) ||
                            (camera.StructClientData.Code == ((StructClientData)comboBoxExStruct.SelectedItem).Code))
                        {
                            if (listBoxExAvailablesCameras.Items.Contains(camera) == false)
                            {
                                listBoxExAvailablesCameras.Items.Add(camera);
                            }
                        }
                    }
                }
            }
            listBoxExUnAvailablesCameras.Items.Clear();

            CheckButtons();
        }

        private void comboBoxCctvZone_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillStructTypes();
        }

        private void comboBoxExStructType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxExStructType.Items.Count > 0) FillStructs();
        }

        private void comboBoxExStruct_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillCamerasList();
        }

        private void FillUsersList()
        {
            IList users = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetDispatchOrCctvOperators,true);
            foreach (OperatorClientData user in users)
            {
                gridControlExOperators.AddOrUpdateItem(new GridControlDataCctvSecurityOperator(user));
            }

            if (selectedUser != null)
                gridControlExOperators.SelectData(new GridControlDataCctvSecurityOperator(selectedUser));
        }

        private void FillCamerasList()
        {
            if (comboBoxCctvZone.SelectedItem != null && comboBoxExStruct.SelectedItem != null && comboBoxExStructType.SelectedItem != null)
            {
                string hql;
                listBoxExAvailablesCameras.Items.Clear();
                if (comboBoxExStruct.SelectedItem.ToString() == ResourceLoader.GetString2("SecurityForm.Structs.All"))
                {
                    if (comboBoxExStructType.Text == ResourceLoader.GetString2("SecurityForm.StructTypes.All"))
                    {
                        if (comboBoxCctvZone.SelectedItem.ToString() == ResourceLoader.GetString2("SecurityForm.Zones.All"))
                        {
                            hql = SmartCadHqls.GetAllCameras;
                        }
                        else
                        {
                            hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetCamerasByZoneName, 
                                ((CctvZoneClientData)comboBoxCctvZone.SelectedItem).Name);
                        }
                    }
                    else
                    {
                        if (comboBoxCctvZone.SelectedItem.ToString() == ResourceLoader.GetString2("SecurityForm.Zones.All"))
                        {
                            hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetCamerasByStructTypeName,
                                ((StructTypeClientData)comboBoxExStructType.SelectedItem).Name);
                        }
                        else
                        {
                            hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetCamerasByStructTypeNameZoneName,
                                ((StructTypeClientData)comboBoxExStructType.SelectedItem).Name,
                                ((CctvZoneClientData)comboBoxCctvZone.SelectedItem).Name) ;
                        }
                    }
                }
                else
                {
                    hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetCamerasByStructName,
                        ((StructClientData)comboBoxExStruct.SelectedItem).Name);
                }

                ArrayList list = (ArrayList)ServerServiceClient.GetInstance().SearchClientObjects(hql);
                foreach (CameraClientData camera in list)
                {
                    OperatorDeviceClientData operCam = new OperatorDeviceClientData();
                    operCam.Device = camera;

                    if (listBoxExUnAvailablesCameras.Items.Contains(operCam.Device) == false)
                    {
                        if (listBoxExAvailablesCameras.Items.Contains(operCam.Device) == false)
                        {
                            listBoxExAvailablesCameras.Items.Add(operCam.Device);
                        }
                    }
                }
            }
        }

        private void FillCctvZones()
        {
            comboBoxCctvZone.Items.Clear();
            IList list = ServerServiceClient.GetInstance().SearchClientObjects(typeof(CctvZoneClientData));
            foreach (CctvZoneClientData cctvZone in list)
            {
                comboBoxCctvZone.Items.Add(cctvZone);
            }

            CctvZoneClientData all = new CctvZoneClientData();
            all.Name = ResourceLoader.GetString2("SecurityForm.Zones.All");
            comboBoxCctvZone.Items.Add(all);
            comboBoxCctvZone.SelectedItem = all;
        }

        private void FillStructTypes()
        {
            StructTypeClientData select = null;
            IList list = new ArrayList();
            if (comboBoxCctvZone.Text != ResourceLoader.GetString2("SecurityForm.Zones.All"))
            {
                list = ServerServiceClient.GetInstance().SearchClientObjects(
                    SmartCadHqls.GetCustomHql(SmartCadHqls.GetStructsByCctvZoneCode,
                    ((CctvZoneClientData)comboBoxCctvZone.SelectedItem).Code));
            }
            else
            {
                list = ServerServiceClient.GetInstance().SearchClientObjects(typeof(StructClientData));
            }

            if (comboBoxExStructType.SelectedItem != null)
                select = (StructTypeClientData)comboBoxExStructType.SelectedItem;

            comboBoxExStructType.Items.Clear();
            foreach (StructClientData str in list)
            {
                if (comboBoxExStructType.Items.Contains(str.Type) == false )
                    comboBoxExStructType.Items.Add(str.Type);
            }

            StructTypeClientData all = new StructTypeClientData();
            all.Name = ResourceLoader.GetString2("SecurityForm.StructTypes.All");
            comboBoxExStructType.Items.Add(all);

            comboBoxExStructType.SelectedItem = select;
            if (comboBoxExStructType.SelectedItem == null)
                comboBoxExStructType.SelectedItem = all; 
                
        }

        private void FillStructs()
        {
            comboBoxExStruct.Items.Clear();
            IList list = new ArrayList();
            if (comboBoxCctvZone.Text != ResourceLoader.GetString2("SecurityForm.Zones.All"))
            {
                list = ServerServiceClient.GetInstance().SearchClientObjects(
                    SmartCadHqls.GetCustomHql(SmartCadHqls.GetStructsByCctvZoneCode,
                    ((CctvZoneClientData)comboBoxCctvZone.SelectedItem).Code));
            }
            else
            {
                list = ServerServiceClient.GetInstance().SearchClientObjects(typeof(StructClientData));
            }

            foreach (StructClientData str in list)
            {
                if ((comboBoxExStructType.SelectedItem.ToString() == ResourceLoader.GetString2("SecurityForm.StructTypes.All")) ||
                    (str.Type.Name == ((StructTypeClientData)comboBoxExStructType.SelectedItem).Name))
                {
                    comboBoxExStruct.Items.Add(str);
                }
            }

            StructClientData all = new StructClientData();
            all.Name = ResourceLoader.GetString2("SecurityForm.Structs.All");
            comboBoxExStruct.Items.Add(all);
            comboBoxExStruct.SelectedItem = all;
        }


        void SecurityForm_CommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
        {
            try
            {
                if (e.Objects != null && e.Objects.Count > 0)
                {
                    lock (syncCommited)
                    {
                        try
                        {
                            if (ServerServiceClient.GetInstance().OperatorClient == null)
                            { }
                        }
                        catch
                        { }
                        if (e.Objects[0] is OperatorClientData)
                        {
                            #region OperatorClientData
                            OperatorClientData oper = e.Objects[0] as OperatorClientData;

                            if (e.Action != CommittedDataAction.Delete)
                            {
                                IList res = ServerServiceClient.GetInstance().SearchClientObjects(
                                    SmartCadHqls.GetCustomHql(SmartCadHqls.GetOperatorByCode, oper.Code), true);
                                if (res.Count > 0)
                                {
                                    OperatorClientData oper2 = res[0] as OperatorClientData;
                                    gridControlExOperators.AddOrUpdateItem(new GridControlDataCctvSecurityOperator(oper2));
                                    gridViewExOperators_SelectionWillChange(null, null);
                                }
                            }
                            else
                            {
                                gridControlExOperators.DeleteItem(new GridControlDataCctvSecurityOperator(oper));
                            }
                            
                            if (selectedUser != null)
                                SelectedUser = (OperatorClientData)ServerServiceClient.GetInstance().RefreshClient(selectedUser,true);

                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        private void buttonExApply_Click(object sender, EventArgs e)
        {
            buttonExApply.Enabled = false;
            buttonExOK_Click(null, null);
            applyPressed = true;
        }

        private void CheckButtons()
        {
            if (selectedUser != null)
            {
                if (listBoxExUnAvailablesCameras.Items.Count > 0)
                {
                    buttonExApply.Enabled = true;
                    buttonExOK.Enabled = true;
                }
                else
                {
                    if (rolesWithCCTVAccess != null && rolesWithCCTVAccess.Contains(selectedUser.RoleCode) == true)
                    {
                        buttonExApply.Enabled = false;
                        buttonExOK.Enabled = false;
                    }
                    else
                    {
                        buttonExApply.Enabled = true;
                        buttonExOK.Enabled = true;
                    }
                    buttonExApply.Enabled = true;
                    buttonExOK.Enabled = true;
                }
            }
            else
            {
                buttonExApply.Enabled = false;
                buttonExOK.Enabled = false;
                    
            }
        }

        private void SecurityForm_Load(object sender, EventArgs e)
        {
            //Get roles with access to cctv application
            rolesWithCCTVAccess = (IList)ServerServiceClient.GetInstance().SearchBasicObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetRolesCodeByAccess, UserApplicationClientData.Cctv.Name));


            this.Icon = ResourceLoader.GetIcon("IconAccessToCameras");
            if (behavior == FormBehavior.Create)
            {
                this.Text = ResourceLoader.GetString2("SecurityFormCreateText");
                buttonExOK.Text = ResourceLoader.GetString2("SecurityFormCreateButtonOkText");
                buttonExCancel.Text = ResourceLoader.GetString2("SecurityFormCreateButtonCancelText");
            }
            else if (behavior == FormBehavior.Edit)
            {
                this.Text = ResourceLoader.GetString2("SecurityFormEditText");
                buttonExOK.Text = ResourceLoader.GetString2("SecurityFormEditButtonOkText");
                buttonExCancel.Text = ResourceLoader.GetString2("SecurityFormEditButtonCancelText");
            }   
        }

        private void gridViewExOperators_SelectionWillChange(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (gridControlExOperators.SelectedItems.Count > 0)
            {
                GridControlDataCctvSecurityOperator gridData = gridControlExOperators.SelectedItems[0] as GridControlDataCctvSecurityOperator;
                SelectedUser = gridData.Operator;
            }
           
        }

        private void SecurityForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            ServerServiceClient.GetInstance().CommittedChanges -= new EventHandler<CommittedObjectDataCollectionEventArgs>(SecurityForm_CommittedChanges);
        }
    }
}