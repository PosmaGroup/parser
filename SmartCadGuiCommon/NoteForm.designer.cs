using SmartCadControls.Controls;
namespace SmartCadGuiCommon
{
    partial class NoteForm<T> 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxNote = new DevExpress.XtraEditors.GroupControl();
            this.textBoxNote = new TextBoxEx();
            this.simpleButtonAccept = new SimpleButtonEx();
            this.simpleButtonCancel = new SimpleButtonEx();
            ((System.ComponentModel.ISupportInitialize)(this.groupBoxNote)).BeginInit();
            this.groupBoxNote.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxNote
            // 
            this.groupBoxNote.Controls.Add(this.textBoxNote);
            this.groupBoxNote.Location = new System.Drawing.Point(4, 5);
            this.groupBoxNote.Name = "groupBoxNote";
            this.groupBoxNote.Size = new System.Drawing.Size(296, 170);
            this.groupBoxNote.TabIndex = 0;
            this.groupBoxNote.Text = "Nota de la unidad";
            // 
            // textBoxNote
            // 
            this.textBoxNote.AcceptsReturn = true;
            this.textBoxNote.AllowsLetters = true;
            this.textBoxNote.AllowsNumbers = true;
            this.textBoxNote.AllowsPunctuation = true;
            this.textBoxNote.AllowsSeparators = true;
            this.textBoxNote.AllowsSymbols = true;
            this.textBoxNote.AllowsWhiteSpaces = true;
            this.textBoxNote.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxNote.ExtraAllowedChars = "";
            this.textBoxNote.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxNote.Location = new System.Drawing.Point(4, 22);
            this.textBoxNote.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxNote.MaxLength = 1000;
            this.textBoxNote.Multiline = true;
            this.textBoxNote.Name = "textBoxNote";
            this.textBoxNote.NonAllowedCharacters = "";
            this.textBoxNote.RegularExpresion = "";
            this.textBoxNote.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxNote.Size = new System.Drawing.Size(288, 144);
            this.textBoxNote.TabIndex = 0;
            this.textBoxNote.TextChanged += new System.EventHandler(this.EnableAcceptButton);
            // 
            // simpleButtonAccept
            // 
            this.simpleButtonAccept.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonAccept.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButtonAccept.Appearance.ForeColor = System.Drawing.Color.Black;
            this.simpleButtonAccept.Appearance.Options.UseFont = true;
            this.simpleButtonAccept.Appearance.Options.UseForeColor = true;
            this.simpleButtonAccept.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.simpleButtonAccept.FontFormat = FontFormat.GetFormat("ButtonFormat");
            this.simpleButtonAccept.Location = new System.Drawing.Point(113, 178);
            this.simpleButtonAccept.Margin = new System.Windows.Forms.Padding(2);
            this.simpleButtonAccept.Name = "simpleButtonAccept";
            this.simpleButtonAccept.Size = new System.Drawing.Size(90, 27);
            this.simpleButtonAccept.TabIndex = 1;
            this.simpleButtonAccept.Text = "Accept";
            this.simpleButtonAccept.Click += new System.EventHandler(this.buttonExAccept_Click);
            // 
            // simpleButtonCancel
            // 
            this.simpleButtonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonCancel.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButtonCancel.Appearance.ForeColor = System.Drawing.Color.Black;
            this.simpleButtonCancel.Appearance.Options.UseFont = true;
            this.simpleButtonCancel.Appearance.Options.UseForeColor = true;
            this.simpleButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonCancel.FontFormat = FontFormat.GetFormat("ButtonFormat");
            this.simpleButtonCancel.Location = new System.Drawing.Point(207, 178);
            this.simpleButtonCancel.Margin = new System.Windows.Forms.Padding(2);
            this.simpleButtonCancel.Name = "simpleButtonCancel";
            this.simpleButtonCancel.Size = new System.Drawing.Size(90, 27);
            this.simpleButtonCancel.TabIndex = 2;
            this.simpleButtonCancel.Text = "Cancelar";
            // 
            // NoteForm
            // 
            this.AcceptButton = this.simpleButtonAccept;
            this.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(305, 208);
            this.Controls.Add(this.simpleButtonCancel);
            this.Controls.Add(this.simpleButtonAccept);
            this.Controls.Add(this.groupBoxNote);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "NoteForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Agregar nota a la unidad";
            this.Load += new System.EventHandler(this.NoteForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupBoxNote)).EndInit();
            this.groupBoxNote.ResumeLayout(false);
            this.groupBoxNote.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupBoxNote;
        private TextBoxEx textBoxNote;
        private SimpleButtonEx simpleButtonAccept;
        private SimpleButtonEx simpleButtonCancel;
    }
}