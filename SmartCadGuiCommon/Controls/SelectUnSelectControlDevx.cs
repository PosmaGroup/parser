using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Collections;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadControls;
using SmartCadCore.Core;
using SmartCadControls.Controls;


namespace SmartCadGuiCommon.Controls
{
    public partial class SelectUnSelectControlDevx : DevExpress.XtraEditors.XtraUserControl
    {
        
        public event ControlParametersChanged Parameters_Changed;        
        public delegate void ControlParametersChanged(object isender, EventArgs e);
        public event ControlCheckActive CheckActive;
        public delegate bool ControlCheckActive(object sender, EventArgs e, int OpCode);
        public event GridControlSelectionChanged SelectionChanged;
        private OperatorClientData OperatorData;
        public static IList OperatorNotActiveList = null;
        IList OperatorActiveAdd = new ArrayList();       
        WorkShiftVariationClientData selectedVariation = null;
        private int LayoutControlType = 0;
        public delegate void GridControlSelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e);
        

        public SelectUnSelectControlDevx()
        {
            InitializeComponent();                       
        }

        public int LayoutControlItem {
            set
            {
                if (value == 0)
                {
                    LayoutControlType = 0;
                    layoutControlItemNotSelected.Text = ResourceLoader.GetString2("NotSelectedF");
                    layoutControlItemSelected.Text = ResourceLoader.GetString2("SelectedF");
                }
                else
                {
                    LayoutControlType = 1;
                    layoutControlItemNotSelected.Text = ResourceLoader.GetString2("NotSelectedStaff");
                    layoutControlItemSelected.Text = ResourceLoader.GetString2("SelectedStaff");
                }
            }
        }

        /// <summary>
        /// Initialize the grids
        /// </summary>
        /// <param name="gridData">GridControlData that contains the information about the columns</param>
        public void InitializeGrids(Type gridData)
        {
            gridControlExNotSelected.EnableAutoFilter = true;
            gridControlExNotSelected.Type = gridData;
            gridControlExNotSelected.ViewTotalRows = true;
            gridViewExNotSelected.OptionsSelection.MultiSelect = true;

            gridControlExSelected.EnableAutoFilter = true;
            gridControlExSelected.Type = gridData;
            gridControlExSelected.ViewTotalRows = true;
            gridViewExSelected.OptionsSelection.MultiSelect = true;
        }

        /// <summary>
        /// Initialize the not selected list of the grid.
        /// </summary>
        /// <param name="notSelected">List of the not selected items.</param>
        public void InitializeData(IList notSelected)
        {
            InitializeData(notSelected, new ArrayList());
        }

        /// <summary>
        /// Initialize each list of the grid.
        /// </summary>
        /// <param name="notSelected">List of the not selected items.</param>
        /// <param name="selected">List of the selected items.</param>
        public void InitializeData(IList notSelected, IList selected)
        {
            gridControlExNotSelected.AddOrUpdateList(notSelected);
            gridControlExSelected.AddOrUpdateList(selected);
            simpleButtonExAddAll.Enabled = gridViewExNotSelected.RowCount > 0;
            simpleButtonExAdd.Enabled = gridViewExNotSelected.RowCount > 0;
            simpleButtonExRemove.Enabled = gridViewExSelected.RowCount > 0;
            simpleButtonExRemoveAll.Enabled = gridViewExSelected.RowCount > 0;
        }

        /// <summary>
        /// Occurs when the grid changes it selection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void gridViewExNotSelected_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            simpleButtonExAdd.Enabled = ((GridViewEx)sender).SelectedRowsCount > 0;
            simpleButtonExAddAll.Enabled = ((GridViewEx)sender).RowCount > 0;
            if (SelectionChanged != null)
            SelectionChanged(sender, e);
                      
          
        }

        /// <summary>
        /// Occurs when the grid changes it selection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void gridViewExSelected_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            simpleButtonExRemove.Enabled = ((GridViewEx)sender).SelectedRowsCount > 0;
            simpleButtonExRemoveAll.Enabled = ((GridViewEx)sender).RowCount > 0;
            if (SelectionChanged != null)
            SelectionChanged(sender, e);
        }

        /// <summary>
        /// Add the selected data into the selected list.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButtonExAdd_Click(object sender, EventArgs e)
        {
            if (gridControlExNotSelected.SelectedItems.Count > 0)
            {
                AddList(gridControlExNotSelected.SelectedItems);
                gridViewExNotSelected.ClearSelection();
                simpleButtonExRemoveAll.Enabled = true;
            }
            Parameters_Changed(null, null);
        }

        /// <summary>
        /// Add all the data not selected into the selected list.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButtonExAddAll_Click(object sender, EventArgs e)
        {
            AddList(gridControlExNotSelected.Items);
            gridViewExNotSelected.ClearSelection();
            simpleButtonExAddAll.Enabled = false;
            simpleButtonExRemoveAll.Enabled = true;

            Parameters_Changed(null, null);
        }

        /// <summary>
        /// Perform the adding of the list of data into the selected list.
        /// </summary>
        /// <param name="list">List of data to add.</param>
        private void AddList(IList list)
        {
            gridControlExSelected.AddOrUpdateList(list);
            if (list.Count == gridViewExNotSelected.RowCount)
                gridControlExNotSelected.ClearData();
            else
                gridControlExNotSelected.DeleteList(list);

          
        }

        /// <summary>
        /// Perform the removal of the list of data into the selected list.
        /// </summary>
        /// <param name="list">List of data to remove.</param>
        private void RemoveList(IList list)
        {
            gridControlExNotSelected.AddOrUpdateList(list);
            if (list.Count == gridViewExSelected.RowCount)
                gridControlExSelected.ClearData();
            else
                gridControlExSelected.DeleteList(list);
        }

        /// <summary>
        /// Remove the data selected from the selected list.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButtonExRemove_Click(object sender, EventArgs e)
        {
            if (this.LayoutControlType == 1)
            {
                IList operatorsAssigned = SelectedClientDataWS(gridControlExSelected.SelectedItems);
                OperatorNotActiveList = operatorsAssigned;
                string OperatorActive = CheckActiveOperator();
                if (OperatorActive != "")
                {
                    MessageForm.Show(string.Format(ResourceLoader.GetString2("RemoveActiveOperatorToWorkShiftError"), OperatorActive), MessageFormType.Error);
                }

                if (OperatorNotActiveList.Count > 0)
                {
                    RemoveList(OperatorNotActiveList);
                    gridViewExSelected.ClearSelection();
                    simpleButtonExAddAll.Enabled = true;
                }
            }
            else
            {
                RemoveList(gridControlExSelected.SelectedItems);
                gridViewExSelected.ClearSelection();
                simpleButtonExAddAll.Enabled = true;
            }

            Parameters_Changed(null, null);
        }

        private string CheckActiveOperator()
        {
            string isActiveOperator = "";            
            int OpIdx = 0;
            IList OperatorIdx = new ArrayList();
            OperatorActiveAdd = null;
            foreach (GridControlDataOperatorWS data in OperatorNotActiveList)
            {
                OperatorData =  data.Tag as OperatorClientData;                
                SessionStatusHistoryClientData operatorLog = ApplicationUtil.GetOperatorStatus(OperatorData, ApplicationUtil.SupervisionApplicationName);
                
                if (operatorLog.StatusFriendlyName.ToLower() != "not connected")
                {
                    bool resActive = CheckActive(null, null, data.Tag.Code);
                    if (resActive == true)
                    {                        
                        isActiveOperator += OperatorData.FirstName + " " + OperatorData.LastName + ", ";
                        OperatorIdx.Add(OpIdx);                       
                    }
                }
                OpIdx++;
            }
            RemoveOperatorActiveList(OperatorIdx);
            if (isActiveOperator != string.Empty)
                isActiveOperator = isActiveOperator.Substring(0, isActiveOperator.Length - 2);
            OperatorIdx = null;
            return isActiveOperator;
        }

        private void RemoveOperatorActiveList(IList OperatorIdx)
        {
            if (OperatorIdx.Count > 0)
            {
                OperatorNotActiveList.Clear();
                //foreach (int idx in OperatorIdx)
                //{
                //    OperatorNotActiveList.RemoveAt(idx);
                //}
            }
        }

        /// <summary>
        /// Remove all the data selected from the selected list.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButtonExRemoveAll_Click(object sender, EventArgs e)
        {
            if (this.LayoutControlType == 1)
            {
                IList operatorsAssigned = SelectedClientDataWS(gridControlExSelected.Items);
                OperatorNotActiveList = operatorsAssigned;
                string OperatorActive = CheckActiveOperator();
                if (OperatorActive != "")
                {
                    MessageForm.Show(string.Format(ResourceLoader.GetString2("RemoveActiveOperatorToWorkShiftError"), OperatorActive), MessageFormType.Error);
                }
                else
                {
                    RemoveList(OperatorNotActiveList);
                }
            }
            else
            {
                RemoveList(gridControlExSelected.Items);
            }
                       
            gridViewExSelected.ClearSelection();
            simpleButtonExRemoveAll.Enabled = false;
            simpleButtonExAddAll.Enabled = true;

            Parameters_Changed(null, null);
        }


        /// <summary>
        /// Return the list of client data selected.
        /// </summary>
        /// <returns></returns>
        public IList SelectedClientData()
        {
            ArrayList clientData = new ArrayList();
            foreach (GridControlData data in gridControlExSelected.Items)
            {
                clientData.Add(data.Tag);
            }
            return clientData;
        }

        public IList SelectedClientDataWS(IList OperatorMode)
        {
            ArrayList clientData = new ArrayList();
            foreach (GridControlDataOperatorWS data in OperatorMode)
            {
                clientData.Add(data);
            }
            return clientData;
        }
    }
}
