using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraCharts;
using SmartCadCore.Core;


namespace SmartCadGuiCommon.Controls
{
    public partial class DispatchOrderImprtantStatusIndicatorControl : DevExpress.XtraEditors.XtraUserControl
    {
        public DispatchOrderImprtantStatusIndicatorControl()
        {
            InitializeComponent();
            this.dispatchOrderImprtantStatusGridIndicatorControl1.DispatchOrderImprtantStatusIndicatorEvent += 
                new DispatchOrderImprtantStatusIndicatorCharEventHandler(
                    DispatchOrderImprtantStatusGridIndicatorControl1_DispatchOrderImprtantStatusIndicatorEvent);
        }

        
        private void DispatchOrderImprtantStatusGridIndicatorControl1_DispatchOrderImprtantStatusIndicatorEvent(object sender,
            DispatchOrderImprtantStatusIndicatorChartEventArgs e)
        {
            if (e.List.Count > 0)
            {
                FormUtil.InvokeRequired(this.chartControl1, delegate
                {
                    this.chartControl1.Series.BeginUpdate();
                    this.chartControl1.Series[0].Points.Clear();                    
                    for (int i = 0; i < e.List.Count; i++)
                    {
                        DispatchOrderImprtantStatusGridControlData indicator = (e.List[i] as DispatchOrderImprtantStatusGridControlData);
                        PieSeriesLabel label = chartControl1.Series[0].Label as PieSeriesLabel;
                        if (label != null)
                        {
                            label.Position = PieSeriesLabelPosition.Radial;
                            label.Antialiasing = true;
                        }

                        PiePointOptions options = chartControl1.Series[0].PointOptions as PiePointOptions;
                        if (options != null)
                        {
                            options.PercentOptions.ValueAsPercent = true;
                            SetNumericOptions(chartControl1.Series[0], NumericFormat.Percent, 0);
                        }

                        SeriesPoint sp = new SeriesPoint();
                        sp.Argument = indicator.Name;
                        sp.Values = new double[] { Convert.ToDouble(indicator.RealValue) };
                        this.chartControl1.Series[0].Points.Add(sp);
                    }

                    bool isEmpty = true;
                    foreach (SeriesPoint point in this.chartControl1.Series[0].Points)
                    {
                        if (point.Values[0] > 0)
                        {
                            isEmpty = false;
                        }
                    }
                    if (isEmpty)
                    {
                        this.chartControl1.Series[0].Visible = false;
                    }
                    else
                    {
                        this.chartControl1.Series[0].Visible = true;
                    }
                    this.chartControl1.Series.EndUpdate();

                });
            }
        }
        private void SetNumericOptions(Series series, NumericFormat format, int precision)
        {
            series.PointOptions.ValueNumericOptions.Format = format;
            series.PointOptions.ValueNumericOptions.Precision = precision;
        }
    }
}
