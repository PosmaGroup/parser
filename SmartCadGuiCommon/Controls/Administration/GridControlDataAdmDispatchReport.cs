﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SmartCadGuiCommon.Controls;
using SmartCadCore.Core;
using SmartCadCore.ClientData;
using SmartCadControls;
using SmartCadCore.Common;
using SmartCadCore.Enums;

namespace SmartCadGuiCommon
{
    class GridControlDataAdmDispatchReport : GridControlDataAdm
    {
        public GridControlDataAdmDispatchReport(DispatchReportClientData question)
            : base(question)
        {
            ApplicationOwner.Add(SmartCadApplications.SmartCadDispatch);
        }

        public DispatchReportClientData DispatchReport
        {
            get
            {
                return this.Tag as DispatchReportClientData;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string Name
        {
            get
            {
                return DispatchReport.Name;
            }

        }


        [GridControlRowInfoData(Searchable = true)]
        public string IncidentTypes
        {
            get
            {
                string result = string.Empty;

                if (DispatchReport.IncidentTypes != null)
                {
                    foreach (IncidentTypeClientData incType in DispatchReport.IncidentTypes)
                    {
                        result += ", " + incType.CustomCode;
                    }
                }

                if (result != string.Empty)
                    result = result.Substring(1).Trim();

                return result;
            }

        }

        [GridControlRowInfoData(Searchable = true)]
        public string Departments
        {
            get
            {
                string result = string.Empty;

                if (DispatchReport.DepartmentTypes != null) 
                {
                    foreach (DepartmentTypeClientData dep in DispatchReport.DepartmentTypes)
                    {
                        result += ", " + dep.Name;            
                    }                
                }

                if (result != string.Empty)
                    result = result.Substring(1).Trim();

                return result;
            }

        }

        [GridControlRowInfoData(Searchable = true)]
        public string HasQuestions
        {
            get
            {
                string result = ResourceLoader.GetString2("$Message.No");
                if (this.DispatchReport.Questions != null &&
                    this.DispatchReport.Questions.Count > 1)
                {
                    result = ResourceLoader.GetString2("$Message.Yes");
                }
                
                return result;
                               
            }

        }


        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is GridControlDataAdmDispatchReport)
            {
                result = DispatchReport.Code == (obj as GridControlDataAdmDispatchReport).DispatchReport.Code;
            }
            return result;
        }

        #region Override
        public override System.Windows.Forms.Form GetForm(AdministrationRibbonForm form, ClientData data, FormBehavior behaviour)
        {
            return new DispatchReportForm(form, data as DispatchReportClientData, behaviour);
        }

        public override string GetDeletedMessage
        {
            get { return ResourceLoader.GetString2("DeleteDispatchReportMessage"); }
        }

        public override string GetNotFoundMessage
        {
            get { return ResourceLoader.GetString2("DeleteDispatchReportData"); }
        }

        public override bool ShowPreview
        {
            get { return false; }
        }

        public override bool PageSearch
        {
            get { return false; }
        }

        public override bool InitializeCollections
        {
            get { return true; }
        }

        public override bool Refresh
        {
            get { return true; }
        }

        public override string GroupText
        {
            get { return ResourceLoader.GetString2("DispatchReportData"); }
        }

        public override string Query
        {
            get { return SmartCadHqls.GetDispatchReport; }
        }

        protected override System.Drawing.Image ViewImage
        {
            get
            {
                return ResourceLoader.GetImage("$Image.DispatchReport");
            }
        }

        public override string PageText
        {
            get
            {
                return ResourceLoader.GetString("Dispatch");
            }
        }

        protected override int PageOrder
        {
            get { return 5; }
        }

        protected override UserResourceClientData Resource
        {
            get
            {
                return DispatchReportClientData.Resource;
            }
        }

        protected override string ViewToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ViewDispatchReport");
            }
        }

        protected override string AddToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_CreateDispatchReport");
            }
        }

        protected override string DeleteToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_DeleteDispatchReport");
            }
        }

        protected override string EditToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ModifyDispatchReport");
            }
        }

        protected override int GroupOrder
        {
            get
            {
                return 4;
            }
        }

        #endregion
    }
}
