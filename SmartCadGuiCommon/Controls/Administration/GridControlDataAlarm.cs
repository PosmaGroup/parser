﻿using SmartCadControls;
using SmartCadControls.Controls;
using SmartCadCore.ClientData;
using Smartmatic.SmartCad.Vms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadGuiCommon.Controls.Administration
{
    public class GridControlDataAlarm : GridControlData
    {
        public VmsAlarm Alarm { get; set; }
        public CameraClientData Camera { get; set; }

        public GridControlDataAlarm(VmsAlarm alarm, CameraClientData camera)
            : base(alarm)
        {
            this.Alarm = alarm;
            this.Camera = camera;
        }

        [GridControlRowInfoData(Visible = true, Width = 70, Searchable = true)]
        public string CtcvAlertCameraName
        {
            get
            {
                return Camera != null ? Camera.Name : "";
            }
        }

        [GridControlRowInfoData(Visible = true, Width = 40, Searchable = true)]
        public string CtcvAlertPriority
        {
            get
            {
                return Alarm.Priority.ToString();
            }
        }

        [GridControlRowInfoData(Visible = false, Width = 40, Searchable = true)]
        public string AlarmStatus
        {
            get
            {
                return Alarm.Status.ToString();
            }
        }


        [GridControlRowInfoData(Visible = true, Width = 60, Searchable = true)]
        public string CtcvAlertDescription
        {
            get
            {
                return Alarm.Description;
            }
        }

        [GridControlRowInfoData(Visible = true, Width = 60, Searchable = true, DisplayFormat = "MM/dd/yyyy HH:mm:ss.fff")]
        public DateTime DateTime
        {
            get
            {
                return Alarm.StartDate;
            }
        }


        public override bool Equals(object obj)
        {
            if (obj is GridControlDataAlarm == false)
                return false;
            GridControlDataAlarm data = obj as GridControlDataAlarm;
            if (data.Alarm.AlarmId == Alarm.AlarmId)
                return true;
            return false;
        }
    }
}
