﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SmartCadCore.ClientData;
using SmartCadCore.Core;
using SmartCadCore.Common;
using SmartCadControls;
using SmartCadCore.Enums;

namespace SmartCadGuiCommon.Controls
{
    public class GridControlDataAdmUnitType : GridControlDataAdm
    {
        public GridControlDataAdmUnitType(UnitTypeClientData unitType)
            : base(unitType)
        {
            ApplicationOwner.Add(SmartCadApplications.SmartCadAdministration);
        }
        public UnitTypeClientData UnitType
        {
            get
            {
                return this.Tag as UnitTypeClientData;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string UnitName
        {
            get
            {
                return UnitType.Name;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string DepartmentType
        {
            get
            {
                return UnitType.DepartmentType.Name;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string UnitDescription
        {
            get
            {
                return UnitType.Description;
            }
        }
        
        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is GridControlDataAdmUnitType)
            {
                result = UnitType.Code == (obj as GridControlDataAdmUnitType).UnitType.Code;
            }
            return result;
        }

        #region GridControlDataAdm
        public override System.Windows.Forms.Form GetForm(AdministrationRibbonForm form, ClientData data, FormBehavior behaviour)
        {
            return new UnitTypeForm(form, data as UnitTypeClientData, behaviour);
        }

        public override string GetDeletedMessage
        {
            get { return ResourceLoader.GetString2("DeleteUnitType"); }
        }

        public override string GetNotFoundMessage
        {
            get { return ResourceLoader.GetString2("DeleteUnitTypeData"); }
        }

        public override bool ShowPreview
        {
            get { return false; }
        }

        public override bool PageSearch
        {
            get { return false; }
        }

        public override bool InitializeCollections
        {
            get { return true; }
        }

        public override bool Refresh
        {
            get { return true; }
        }

        public override string GroupText
        {
            get { return ResourceLoader.GetString2("UnitsTypes"); }
        }

        public override string Query
        {
            get { return SmartCadHqls.GetAllUnitsTypes; }
        }

        protected override System.Drawing.Image ViewImage
        {
            get
            {
                return ResourceLoader.GetImage("$Image.UnitType");
            }
        }

        public override string PageText
        {
            get { return ResourceLoader.GetString("MobileUnits"); }
        }

        protected override int PageOrder
        {
            get { return 4; }
        }

        protected override UserResourceClientData Resource
        {
            get { return UnitTypeClientData.Resource; }
        }

        protected override string ViewToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ViewUnitType");
            }
        }

        protected override string AddToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_CreateUnitType");
            }
        }

        protected override string DeleteToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_DeleteUnitType");
            }
        }

        protected override string EditToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ModifyUnitType");
            }
        }

        protected override int GroupOrder
        {
            get
            {
                return 0;
            }
        }
        #endregion
    }
}
