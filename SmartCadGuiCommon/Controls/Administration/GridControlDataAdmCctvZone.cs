﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Core;
using SmartCadCore.Common;
using SmartCadControls;
using SmartCadCore.Enums;

namespace SmartCadGuiCommon.Controls
{
    public class GridControlDataAdmCctvZone : GridControlDataAdm
    {
        public GridControlDataAdmCctvZone(CctvZoneClientData zone)
            : base(zone)
        {
            ApplicationOwner.Add(SmartCadApplications.SmartCadCCTV);
            ApplicationOwner.Add(SmartCadApplications.SmartCadAlarm);
        }

        public CctvZoneClientData CctvZone
        {
            get
            {
                return this.Tag as CctvZoneClientData;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string CctvZoneName
        {
            get
            {
                return CctvZone.Name;
            }

        }

        [GridControlRowInfoData(Searchable = true)]
        public int CctvZoneStructs
        {
            get
            {
                return CctvZone.StructNumber;
            }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is GridControlDataAdmCctvZone)
            {
                result = CctvZone.Code == (obj as GridControlDataAdmCctvZone).CctvZone.Code;
            }
            return result;
        }

        #region GridControlDataAdm
        public override System.Windows.Forms.Form GetForm(AdministrationRibbonForm form, ClientData data, FormBehavior behaviour)
        {
            return new ZoneForm(form, data as CctvZoneClientData, behaviour);
        }

        public override string GetDeletedMessage
        {
            get { return ResourceLoader.GetString("DeleteCctvZone"); }
        }

        public override string GetNotFoundMessage
        {
            get { return ResourceLoader.GetString2("DeleteCctvZoneData"); }
        }

        public override bool ShowPreview
        {
            get { return false; }
        }

        public override bool PageSearch
        {
            get { return false; }
        }

        public override bool InitializeCollections
        {
            get { return false; }
        }

        public override bool Refresh
        {
            get { return true; }
        }

        public override string GroupText
        {
            get { return ResourceLoader.GetString("CctvZone"); }
        }

        public override string Query
        {
            get { return SmartCadHqls.GetAllCctvZone; }
        }

        protected override System.Drawing.Image ViewImage
        {
            get
            {
                return ResourceLoader.GetImage("$Image.DepartmentZone");
            }
        }

        public override string PageText
        {
            get
            {
                return ResourceLoader.GetString("Cctv");
            }
        }

        protected override int PageOrder
        {
            get { return 6; }
        }

        protected override UserResourceClientData Resource
        {
            get
            {
                return CctvZoneClientData.Resource;
            }
        }

        protected override string ViewToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ViewCCTVZones");
            }
        }

        protected override string AddToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_CreateZone");
            }
        }

        protected override string DeleteToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_DeleteZone");
            }
        }

        protected override string EditToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ModifyZone");
            }
        }

        protected override int GroupOrder
        {
            get
            {
                return 0;
            }
        }
        #endregion
    }
}
