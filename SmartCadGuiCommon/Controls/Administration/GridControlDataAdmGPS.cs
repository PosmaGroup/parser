﻿using SmartCadControls;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadCore.Core;
using SmartCadCore.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartCadGuiCommon.Controls
{
    public class GridControlDataAdmGPS : GridControlDataAdm
    {
        public GridControlDataAdmGPS(GPSClientData gps)
            : base(gps)
        {
            ApplicationOwner.Add(SmartCadApplications.SmartCadParser);
        }

        public GPSClientData GPS
        {
            get
            {
                return this.Tag as GPSClientData;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string IdentifierM
        {
            get
            {
                return GPS.Name;
            }

        }

        [GridControlRowInfoData(Searchable = true)]
        public string Brand
        {
            get
            {
                return GPS.Type.Brand;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string Model
        {
            get
            {
                return GPS.Type.Model;
            }
        }


        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is GridControlDataAdmGPS)
            {
                result = GPS.Code == (obj as GridControlDataAdmGPS).GPS.Code;
            }
            return result;
        }

        #region Override GridControlDataAdm

        public override System.Windows.Forms.Form GetForm(AdministrationRibbonForm form, ClientData data, FormBehavior behaviour)
        {
            return new GPSForm(form, data as GPSClientData, behaviour);
        }

        public override string GetDeletedMessage
        {
            get { return ResourceLoader.GetString2("DeleteGPSMessage"); }
        }

        public override string GetNotFoundMessage
        {
            get { return ResourceLoader.GetString2("DeleteGPSData"); }
        }

        public override bool ShowPreview
        {
            get { return false; }
        }

        public override bool PageSearch
        {
            get { return false; }
        }

        public override bool InitializeCollections
        {
            get { return false; }
        }

        public override bool Refresh
        {
            get { return true; }
        }

        public override string GroupText
        {
            get { return ResourceLoader.GetString2("GPS"); }
        }

        public override string Query
        {
            get { return SmartCadHqls.GetGPS; }
        }

        protected override System.Drawing.Image ViewImage
        {
            get
            {
                return ResourceLoader.GetImage("$Image.GPS");
            }
        }

        public override string PageText
        {
            get { return ResourceLoader.GetString("MobileUnits"); }
        }

        protected override int PageOrder
        {
            get { return 4; }
        }

        protected override UserResourceClientData Resource
        {
            get { return GPSClientData.Resource; }
        }

        protected override string ViewToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ViewGPS");
            }
        }

        protected override string AddToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_CreateGPS");
            }
        }

        protected override string DeleteToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_DeleteGPS");
            }
        }

        protected override string EditToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ModifyGPS");
            }
        }

        protected override int GroupOrder
        {
            get
            {
                return 1;
            }
        }
        #endregion
    }
}
