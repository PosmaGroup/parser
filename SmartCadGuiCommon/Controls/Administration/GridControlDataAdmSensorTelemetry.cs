﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Core;
using SmartCadCore.Common;

using SmartCadControls;

using SmartCadCore.Enums;

namespace SmartCadGuiCommon.Controls
{
    public class GridControlDataAdmSensorTelemetry : GridControlDataAdm
    {
        public GridControlDataAdmSensorTelemetry(DataloggerClientData datalogger)
            : base(datalogger)
        {
            ApplicationOwner.Add(SmartCadApplications.SmartCadAlarm);
        }

        public DataloggerClientData Datalogger
        {
            get
            {
                return this.Tag as DataloggerClientData;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string SensorTelemetryName
        {
            get
            {
                return Datalogger.Name;
            }

        }

        [GridControlRowInfoData(Searchable = true)]
        public int SensorTelemetryCount
        {
            get
            {
                return Datalogger.SensorTelemetrys.Count;
           
            }
        }

      

       
        [GridControlRowInfoData(Searchable = true)]
        public string CCTVZone
        {
            get
            {
                string zone = string.Empty;
                if (Datalogger.StructClientData != null && Datalogger.StructClientData.CctvZone != null)
                {
                    zone = Datalogger.StructClientData.CctvZone.Name;
                }
                return zone;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string SensorTelemetryStruct
        {
            get
            {
                if (Datalogger.StructClientData != null)
                    return Datalogger.StructClientData.Name;
                else
                    return string.Empty;
            }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is GridControlDataAdmSensorTelemetry)
            {
                result = Datalogger.Code == (obj as GridControlDataAdmSensorTelemetry).Datalogger.Code;
            }
            return result;
        }

        #region GridControlDataAdm
        public override System.Windows.Forms.Form GetForm(AdministrationRibbonForm form, ClientData data, FormBehavior behaviour)
        {
            return new DataloggerForm(form, data as DataloggerClientData, behaviour);
        }

        public override string GetDeletedMessage
        {
            get { return ResourceLoader.GetString2("DeleteSensorTelemetryMessage"); }
        }

        public override string GetNotFoundMessage
        {
            get { return ResourceLoader.GetString2("DeleteSensorTelemetryData"); }
        }

        public override bool ShowPreview
        {
            get { return false; }
        }

        public override bool PageSearch
        {
            get { return false; }
        }

        public override bool InitializeCollections
        {
            get { return false; }
        }

        public override bool Refresh
        {
            get { return true; }
        }

        public override string GroupText
        {
            get { return ResourceLoader.GetString2("SensorTelemetrys"); }
        }

        public override string Query
        {
            get { return SmartCadHqls.GetAllDataloggers; }
        }
        protected override System.Drawing.Image ViewImage
        {
            get
            {
                return ResourceLoader.GetImage("$Image.Telemetry");
            }
        }

        public override string PageText
        {
            get
            {
                return ResourceLoader.GetString("Cctv");
            }
        }

        protected override int PageOrder
        {
            get { return 6; }
        }

        protected override UserResourceClientData Resource
        {
            get
            {
                return SensorTelemetryClientData.Resource;
            }
        }

        protected override string ViewToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ViewSensorTelemetry");
            }
        }

        protected override string AddToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_CreateSensorTelemetry");
            }
        }

        protected override string DeleteToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_DeleteSensorTelemetry");
            }
        }

        protected override string EditToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ModifySensorTelemetry");
            }
        }

        protected override int GroupOrder
        {
            get
            {
                return 3;
            }
        }

        #endregion
    }
}
