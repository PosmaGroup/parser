﻿using SmartCadControls;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadCore.Core;
using SmartCadCore.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace SmartCadGuiCommon.Controls
{
    public class GridControlDataAdmIncidentType : GridControlDataAdm
    {
        public GridControlDataAdmIncidentType(IncidentTypeClientData incidentType)
            : base(incidentType)
        {
            ApplicationOwner.Add(SmartCadApplications.SmartCadAdministration);
        }

        public IncidentTypeClientData IncidentType
        {
            get
            {
                return this.Tag as IncidentTypeClientData;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string IncidentTypeName
        {
            get
            {
                return IncidentType.FriendlyName;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string IncidentTypeCustomCode
        {
            get
            {
                return IncidentType.CustomCode;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string IncidentTypeNoEmergency
        {
            get
            {
                if (IncidentType.NoEmergency == true)
                    return ResourceLoader.GetString2("NotEmergency");
                else
                    return ResourceLoader.GetString2("Emergency");
            }
        }
        
        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is GridControlDataAdmIncidentType)
            {
                result = IncidentType.Code == (obj as GridControlDataAdmIncidentType).IncidentType.Code;
            }
            return result;
        }

        #region GridControlDataAdm
        public override System.Windows.Forms.Form GetForm(AdministrationRibbonForm form, ClientData data, FormBehavior behaviour)
        {
            return new IncidentTypeForm(form, data as IncidentTypeClientData, behaviour);
        }

        public override string GetDeletedMessage
        {
            get { return ResourceLoader.GetString2("DeleteIncidentType"); }
        }

        public override string GetNotFoundMessage
        {
            get { return ResourceLoader.GetString2("DeleteIncidentTypeData"); }
        }

        public override bool ShowPreview
        {
            get { return false; }
        }

        public override bool PageSearch
        {
            get { return false; }
        }

        public override bool InitializeCollections
        {
            get { return true; }
        }

        public override bool Refresh
        {
            get { return true; }
        }

        public override string GroupText
        {
            get { return ResourceLoader.GetString2("IncidentTypes"); }
        }

        public override string Query
        {
            get { return SmartCadHqls.GetIncidentTypes; }
        }

        protected override System.Drawing.Image ViewImage
        {
            get
            {
                return ResourceLoader.GetImage("IconIncidentType");
            }
        }

        public override string PageText
        {
            get { return ResourceLoader.GetString("FirstLevel"); }
        }

        protected override int PageOrder
        {
            get { return 2; }
        }

        protected override UserResourceClientData Resource
        {
            get { return IncidentTypeClientData.Resource; }
        }

        protected override string ViewToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ViewIncidentType");
            }
        }

        protected override string AddToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_CreateIncidentType");
            }
        }

        protected override string DeleteToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_DeleteIncidentType");
            }
        }

        protected override string EditToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ModifyIncidentType");
            }
        }

        protected override int GroupOrder
        {
            get
            {
                return 0;
            }
        }
        #endregion
    }
}
