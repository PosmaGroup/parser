﻿using SmartCadControls;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadCore.Core;
using SmartCadCore.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace SmartCadGuiCommon.Controls
{
    public class GridControlDataAdmQuestionDispatchReport : GridControlDataAdm
    {
        public GridControlDataAdmQuestionDispatchReport(QuestionDispatchReportClientData question)
            : base(question)
        {
            ApplicationOwner.Add(SmartCadApplications.SmartCadDispatch);
        }

        public QuestionDispatchReportClientData Question
        {
            get
            {
                return this.Tag as QuestionDispatchReportClientData;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string Name
        {
            get
            {
                return Question.Text;
            }

        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is GridControlDataAdmQuestionDispatchReport)
            {
                result = Question.Code == (obj as GridControlDataAdmQuestionDispatchReport).Question.Code;
            }
            return result;
        }

        protected override int GroupOrder
        {
            get
            {
                return 3;
            }
        }

        #region Override GridControlDataAdm

        public override System.Windows.Forms.Form GetForm(AdministrationRibbonForm form, ClientData data, FormBehavior behaviour)
        {
            return new QuestionDispatchReportForm(form, data as QuestionDispatchReportClientData, behaviour);
        }

        public override string GetDeletedMessage
        {
            get { return ResourceLoader.GetString2("DeleteQuestion"); }
        }

        public override string GetNotFoundMessage
        {
            get { return ResourceLoader.GetString2("DeleteQuestionData"); }
        }

        public override bool ShowPreview
        {
            get { return false; }
        }

        public override bool PageSearch
        {
            get { return true; }
        }

        public override bool InitializeCollections
        {
            get { return true; }
        }

        public override bool Refresh
        {
            get { return true; }
        }

        public override string GroupText
        {
            get { return ResourceLoader.GetString2("Questions"); }
        }

        public override string Query
        {
            get { return SmartCadHqls.GetQuestionsDispatchReport; }
        }
        protected override System.Drawing.Image ViewImage
        {
            get
            {
                return ResourceLoader.GetImage("$Image.DispatchReportQuestion");
            }
        }

        public override string PageText
        {
            get
            {
                return ResourceLoader.GetString("Dispatch");
            }
        }

        protected override int PageOrder
        {
            get { return 5; }
        }

        protected override UserResourceClientData Resource
        {
            get
            {
                return QuestionDispatchReportClientData.Resource;
            }
        }

        protected override string ViewToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ViewQuestionDispatchReport");
            }
        }

        protected override string AddToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_CreateQuestionDispatchReport");
            }
        }

        protected override string DeleteToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_DeleteQuestionDispatchReport");
            }
        }

        protected override string EditToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ModifyQuestionDispatchReport");
            }
        }
        #endregion
    }
}
