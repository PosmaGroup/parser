﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;




using System.Drawing;
using Smartmatic.SmartCad.Service;
using DevExpress.XtraBars;
using SmartCadControls;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadCore.Core;
using SmartCadCore.Enums;

namespace SmartCadGuiCommon.Controls
{
    public class GridControlDataAdmUnit : GridControlDataAdm
    {
        public GridControlDataAdmUnit(UnitClientData unit)
            : base(unit)
        {
            ApplicationOwner.Add(SmartCadApplications.SmartCadAdministration);
        }

        public UnitClientData Unit
        {
            get
            {
                return this.Tag as UnitClientData;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string DepartmentType
        {
            get
            {
                return Unit.DepartmentType.Name;//.DepartmentStation.DepartmentZone.DepartmentType.Name;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string UnitCustomCode
        {
            get
            {
                return Unit.CustomCode;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string UnitType
        {
            get
            {
                return Unit.Type.Name;
            }
        }

        //[GridControlRowInfoData(Searchable = true)]
        //public int DeviceID
        //{
        //    get
        //    {
        //        return Unit.GPSCode;
        //    }
        //}

        [GridControlRowInfoData(Searchable = true)]
        public string DeviceID
        {
            get
            {
                return Unit.IdGPS;
            }
        }
        
        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is GridControlDataAdmUnit)
            {
                result = Unit.Code == (obj as GridControlDataAdmUnit).Unit.Code;
            }
            return result;
        }

        protected override int GroupOrder
        {
            get
            {
                return 2;
            }
        }

        #region GridControlDataAdm
        public override System.Windows.Forms.Form GetForm(AdministrationRibbonForm form, ClientData data, FormBehavior behaviour)
        {
            return new UnitForm(form, data as UnitClientData, behaviour);
        }

        public override string GetDeletedMessage
        {
            get { return ResourceLoader.GetString2("DeleteUnit"); }
        }

        public override string GetNotFoundMessage
        {
            get { return ResourceLoader.GetString2("DeleteUnitData"); }
        }

        public override bool ShowPreview
        {
            get { return false; }
        }

        public override bool PageSearch
        {
            get { return true; }
        }

        public override bool InitializeCollections
        {
            get { return true; }
        }

        public override bool Refresh
        {
            get { return true; }
        }

        public override string GroupText
        {
            get { return ResourceLoader.GetString2("UNITS"); }
        }

        public override string Query
        {
            get { return SmartCadHqls.GetUnitOrderByCode; }
        }

        protected override System.Drawing.Image ViewImage
        {
            get
            {
                return ResourceLoader.GetImage("$Image.Unit");
            }
        }

        public override string PageText
        {
            get { return ResourceLoader.GetString("MobileUnits"); }
        }

        protected override int PageOrder
        {
            get { return 4; }
        }

        protected override UserResourceClientData Resource
        {
            get { return UnitClientData.Resource; }
        }

        protected override string ViewToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ViewUnit");
            }
        }

        protected override string AddToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_CreateUnit");
            }
        }

        protected override string DeleteToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_DeleteUnit");
            }
        }

        protected override string EditToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ModifyUnit");
            }
        }

        protected override List<DevExpress.XtraBars.BarButtonItem> CreateBasicButtons()
        {
            Items = base.CreateBasicButtons();

            //COMMENTED FOR BATAAN VERSION 3.3.1 AA
            //Items.Add(CreateButton(UnitRouteCaption, null, UnitRouteImage, UnitRouteClick, false, Resource, UserActionClientData.Update, System.Windows.Forms.Shortcut.CtrlH, UnitRouteToolTip));

            return Items;
        }

        #region UnitRoute
        private string UnitRouteCaption { get { return ResourceLoader.GetString2("AssignRoutes"); } }

        private Image UnitRouteImage { get { return ResourceLoader.GetImage("$Image.UnitRoute"); } }

        private string UnitRouteToolTip { get { return ResourceLoader.GetString2("ToolTip_AssignRoutes"); } }

        private void UnitRouteClick(object sender, ItemClickEventArgs e)
        {
           ClientData selected = (GridControl.SelectedItems[0] as GridControlDataAdm).Tag;
            if (Refresh)
                selected = ServerServiceClient.GetInstance().RefreshClient(selected, InitializeCollections);
            if (selected != null)
            {
                WorkShiftRouteForm form = new WorkShiftRouteForm(AdministrationForm, (UnitClientData)selected, FormBehavior.Edit);
                form.ShowDialog();
                //EnableOptions(e.Item as BarButtonItem);
            }     
        }
        #endregion
        #endregion
    }
}
