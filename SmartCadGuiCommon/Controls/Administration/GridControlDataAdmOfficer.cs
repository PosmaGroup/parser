﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Core;
using SmartCadCore.Common;
using SmartCadControls;
using SmartCadCore.Enums;

namespace SmartCadGuiCommon.Controls
{
    public class GridControlDataAdmOfficer : GridControlDataAdm
    {
        public GridControlDataAdmOfficer(OfficerClientData officer)
            : base(officer)
        {
            ApplicationOwner.Add(SmartCadApplications.SmartCadAdministration);
        }

        public OfficerClientData Officer
        {
            get
            {
                return this.Tag as OfficerClientData;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string DepartmentType
        {
            get
            {
                return Officer.DepartmentType.Name;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string OfficerLastName
        {
            get
            {
                return Officer.LastName;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string OfficerName
        {
            get
            {
                return Officer.FirstName;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string OfficerID
        {
            get
            {
                return Officer.PersonID;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string OfficerBadge
        {
            get
            {
                return Officer.Badge;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string Position
        {
            get
            {
                string position = string.Empty;
                if (Officer.Position != null)
                {
                    position = Officer.Position.Name;
                }
                return position;
            }
        }
        
        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is GridControlDataAdmOfficer)
            {
                result = Officer.Code == (obj as GridControlDataAdmOfficer).Officer.Code;
            }
            return result;
        }

        protected override int GroupOrder
        {
            get
            {
                return 2;
            }
        }

        #region GridControlDataAdm
        public override System.Windows.Forms.Form GetForm(AdministrationRibbonForm form, ClientData data, FormBehavior behaviour)
        {
            return new OfficerForm(form, data as OfficerClientData, behaviour);
        }

        public override string GetDeletedMessage
        {
            get { return ResourceLoader.GetString2("DeleteOfficer"); }
        }

        public override string GetNotFoundMessage
        {
            get { return ResourceLoader.GetString2("DeleteOfficerData"); }
        }

        public override bool ShowPreview
        {
            get { return false; }
        }

        public override bool PageSearch
        {
            get { return true; }
        }

        public override bool InitializeCollections
        {
            get { return true; }
        }

        public override bool Refresh
        {
            get { return true; }
        }

        public override string GroupText
        {
            get { return ResourceLoader.GetString2("Officers"); }
        }

        public override string Query
        {
            get { return SmartCadHqls.GetOfficers; }
        }

        protected override System.Drawing.Image ViewImage
        {
            get
            {
                return ResourceLoader.GetImage("$Image.Officer");
            }
        }

        public override string PageText
        {
            get
            {
                return ResourceLoader.GetString("Dispatch");
            }
        }

        protected override int PageOrder
        {
            get { return 5; }
        }

        protected override UserResourceClientData Resource
        {
            get
            {
                return OfficerClientData.Resource;
            }
        }

        protected override string ViewToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ViewOfficer");
            }
        }

        protected override string AddToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_CreateOfficer");
            }
        }

        protected override string DeleteToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_DeleteOfficer");
            }
        }

        protected override string EditToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ModifyOfficer");
            }
        }
        #endregion
    }
}
