﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.ClientData;
using SmartCadCore.Core;
using SmartCadCore.Common;
using SmartCadControls;
using SmartCadCore.Enums;

namespace SmartCadGuiCommon.Controls
{
    public class GridControlDataAdmRule : GridControlDataAdm
    {
        public GridControlDataAdmRule(CameraClientData camera): base(camera) { }
/*        public GridControlDataAdmRule(RuleClientData rule)
            : base(rule)
        {
            ApplicationOwner.Add(SmartCadApplications.SmartCadCCTV);
        }

        public RuleClientData Rules
        {
            get
            {
                return this.Tag as RuleClientData;
            }
        }
        */
        [GridControlRowInfoData(Searchable = true)]
        public string Camera
        {
            get
            {
           //     return Rules.Camera;
                return "cualquier cosa";
            }

        }

        [GridControlRowInfoData(Searchable = true)]
        public string Rule
        {
            get
            {
             //   return Rules.Rule;
                return "cualquier cosa";
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string Value
        {
            get
            {
                //return Rules.Value;
                return "cualquier cosa";
            }
        }
        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is GridControlDataAdmRule)
            {
               // result = Rules.Code == (obj as GridControlDataAdmRule).Rules.Code;
            }
            return result;
        }

        #region GridControlDataAdm
        public override System.Windows.Forms.Form GetForm(AdministrationRibbonForm form,ClientData data, FormBehavior behaviour)
        {
            return new RulesForm(/*form, data as RuleClientData, behaviour*/);
        }

        public override string GetDeletedMessage
        {
            get { return ResourceLoader.GetString("DeleteRule"); }
        }

        public override string GetNotFoundMessage
        {
            get { return ResourceLoader.GetString2("DeleteStructData"); }
        }

        public override bool ShowPreview
        {
            get { return false; }
        }

        public override bool PageSearch
        {
            get { return true; }
        }

        public override bool InitializeCollections
        {
            get { return true; }
        }

        public override bool Refresh
        {
            get { return true; }
        }

        public override string GroupText
        {
            get { return ResourceLoader.GetString("AVDescription"); }
        }

        public override string Query
        {
            get { return SmartCadHqls.GetAllRules; }
        }

        protected override System.Drawing.Image ViewImage
        {
            get
            {
                //return ResourceLoader.GetImage("$Image.AVRules");
                return ResourceLoader.GetImage("$Image.Camera");
            }
        }

        public override string PageText
        {
            get
            {
                return ResourceLoader.GetString("Cctv");
            }
        }

        protected override int PageOrder
        {
            get { return 6; }
        }

        protected override UserResourceClientData Resource
        {
            get
            {
                //return RuleClientData.Resource;
                return null;
            }
        }

        protected override string ViewToolTip
        {
            get
            {
                return ResourceLoader.GetString("TootTip_ViewAVideoRules");
            }
        }

        protected override string AddToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_CreateRule");
            }
        }

        protected override string DeleteToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_DeleteRule");
            }
        }

        protected override string EditToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ModifyRule");
            }
        }

        protected override int GroupOrder
        {
            get
            {
                return 1;
            }
        }
        #endregion
    }
}
