using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using DevExpress.XtraGrid.Views.Base;
using Smartmatic.SmartCad.Service;
using DevExpress.XtraGrid.Views.Layout;
using DevExpress.XtraGrid.Views.Grid;
using System.Threading;
using DevExpress.XtraGrid.Columns;
using SmartCadCore.Common;
using SmartCadCore.Core;
using SmartCadCore.ClientData;

namespace SmartCadGuiCommon.Controls
{
    
    public partial class NewDispatchOrderGridIndicatorControl : UserControl
    {        
        private BindingList<NewDispatchOrderGridControlData> dataSource;
        public NewDispatchOrderGridIndicatorControl()
        {
            InitializeComponent();
            LoadLanguage();
        }

      

        private void LoadLanguage()
        {
            gridColumnIndicator.Caption = ResourceLoader.GetString2("IncidentType");
            gridColumnValue.Caption = ResourceLoader.GetString2("RealValue"); 
            gridColumnPercentValue.Caption = ResourceLoader.GetString2("Percent");            
        }
        public BindingList<NewDispatchOrderGridControlData> DataSource
        {
            get
            {
                return dataSource;
            }
            set
            {
                dataSource = value;
                FormUtil.InvokeRequired(this.gridControl1, delegate
                {
                    this.gridControl1.DataSource = dataSource;
                });
            }
        }
        public void Load(IList list)
        {
            BindingList<NewDispatchOrderGridControlData> listValue = new BindingList<NewDispatchOrderGridControlData>();            
            foreach (IndicatorResultValuesClientData value in list)
            {
                NewDispatchOrderGridControlData data = new NewDispatchOrderGridControlData(value);
                if (listValue.Contains(data) == false)
                {
                    listValue.Add(data);
                }
            }
            DataSource = listValue;

          
        }
        public void AddOrUpdate(IndicatorResultValuesClientData result)
        {
            if (DataSource == null)
            {
                DataSource = new BindingList<NewDispatchOrderGridControlData>();
            }

            NewDispatchOrderGridControlData indicatorGridControlData = new NewDispatchOrderGridControlData(result);
            int index = dataSource.IndexOf(indicatorGridControlData);
            if (index > -1)
            {
                FormUtil.InvokeRequired(this.gridControl1, delegate
               {
                   dataSource[index] = new NewDispatchOrderGridControlData(result);
               });
            }
            else
            {
                FormUtil.InvokeRequired(this.gridControl1, delegate
               {
                   dataSource.Add(new NewDispatchOrderGridControlData(result));
               });
            }

        }

        private void gridView1_DragObjectOver(object sender, DragObjectOverEventArgs e)
        {
            GridColumn column = e.DragObject as GridColumn;
            if (column != null && e.DropInfo.Index < 0)
            {
                e.DropInfo.Valid = false;
            }

        }
        
         
    }

    public class NewDispatchOrderGridControlData
    {
        private string realValue;
        private string percentValue;      
        private string name;

       public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }
        public string RealValue
        {
            get
            {
                return realValue;
            }
            set 
            {
                realValue = value;
            }
        }
        public string PercentValue
        {
            get
            {
                return percentValue;
            }
            set
            {
                percentValue = value;
            }
        }

        public NewDispatchOrderGridControlData(IndicatorResultValuesClientData indicatorResultValuesClientData)
        {
            List<double> values = ApplicationUtil.ConvertIndicatorResultValuesFromString(
                indicatorResultValuesClientData.ResultValue);

            realValue = values[0].ToString();
            if (values.Count > 1)
            {
                percentValue = values[1].ToString();
            }
            name = indicatorResultValuesClientData.Description;
           
        }
         
        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is NewDispatchOrderGridControlData)
            {
                result = this.name == ((NewDispatchOrderGridControlData)obj).name;
            }
            return result;
        }
    }


   


}
