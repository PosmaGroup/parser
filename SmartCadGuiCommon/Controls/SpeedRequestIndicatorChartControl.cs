using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Collections;
using DevExpress.XtraCharts;
using Smartmatic.SmartCad.Service;
using SmartCadCore.Core;
using SmartCadCore.ClientData;

namespace SmartCadGuiCommon.Controls
{
    public partial class SpeedRequestIndicatorChartControl : DevExpress.XtraEditors.XtraUserControl
    {        
        public SpeedRequestIndicatorChartControl()
        {
            InitializeComponent();
        }

        public void Load()
        {
            IList indicators = ServerServiceClient.GetInstance().SearchClientObjects(
                   SmartCadHqls.GetCustomHql(SmartCadHqls.GetAllIndicatorsResultByindicatorCode,
                   23,
                   2,
                    ApplicationUtil.GetDataBaseFormattedDate(DateTime.Today)));
            if (indicators.Count > 0)
            {
                FormUtil.InvokeRequired(this.chartControl1, delegate
               {
                   this.chartControl1.Series.BeginUpdate();
                   this.chartControl1.Series[0].LegendText =
                       (indicators[0] as IndicatorResultClientData).IndicatorName;
                   foreach (IndicatorResultClientData indicatorResult in indicators)
                   {
                       foreach (IndicatorResultValuesClientData data in indicatorResult.Values)
                       {
                           if (data.CustomCode == ServerServiceClient.GetInstance().OperatorClient.Code)
                           {
                               SeriesPoint sp1 =
                                           new SeriesPoint(data.Date, new double[] { 
                                Convert.ToDouble(data.ResultValue) });
                               this.chartControl1.Series[0].Points.Add(sp1);
                           }
                       }
                   }
                   this.chartControl1.Series.EndUpdate();
               });
            }
        }

        public void AddOrUpdate(IList dataList)
        {                                            
            FormUtil.InvokeRequired(this.chartControl1, delegate
            {               
               // this.chartControl1.Series[0].Points.Clear();
                if (dataList.Count > 0)
                {
                    this.chartControl1.Series.BeginUpdate();
                    this.chartControl1.Series[0].LegendText = 
                        (dataList[0] as IndicatorResultValuesClientData).IndicatorName;

                    foreach (IndicatorResultValuesClientData data in dataList)
                    {
                        SeriesPoint sp1 = 
                            new SeriesPoint(data.Date, new double[] { 
                                Convert.ToDouble(data.ResultValue) });
                        this.chartControl1.Series[0].Points.Add(sp1);
                    }

                    this.chartControl1.Series.EndUpdate();
                }              
            });

        }
    }
}
