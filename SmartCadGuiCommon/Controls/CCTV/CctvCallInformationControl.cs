using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Smartmatic.SmartCad.Service;
using System.Collections;
using SmartCadControls.Controls;
using SmartCadCore.Common;
using SmartCadCore.ClientData;
using SmartCadCore.Enums;
using SmartCadCore.Core;

namespace SmartCadGuiCommon.Controls
{

    public partial class CctvCallInformationControl: HeaderPanelControl
    {
        #region Fields

       
        private AddressClientData addressData;
        private FrontClientStateEnum frontClientState = FrontClientStateEnum.WaitingForIncident;      
        protected CallRingingEventArgs incomingCallEventArgs;
        private CctvZoneClientData cctvZoneSelected;     

        #endregion

        public CctvCallInformationControl()
        {
            InitializeComponent();
            this.groupControlBody.Text = ResourceLoader.GetString2("CctvDataHeaderText");
			LoadLanguage();
        }

		private void LoadLanguage()
		{
			layoutControlItemCity.Text = ResourceLoader.GetString2("City");
			layoutControlItemZoneName.Text = ResourceLoader.GetString2("ZoneName")+ ":";
			layoutControlItemStreet.Text = ResourceLoader.GetString2("StreetName") + ":";
			layoutControlItemCamera.Text = ResourceLoader.GetString2("Camera") + ":*";
			layoutControlItemStruct.Text = ResourceLoader.GetString2("Struct") + ":*";
			layoutControlItemStructType.Text = ResourceLoader.GetString2("StructType") + ":*";
			layoutControlItemZone.Text = ResourceLoader.GetString2("CCTVZone") + ":*";
			layoutControlItemTown.Text = ResourceLoader.GetString2("Town") + ":";
            // Analytical Video Controls Associated
            layoutControlItemAlarmDate.Text = ResourceLoader.GetString2("AlarmDateTime") + ":*";
            layoutControlItemAlarmRule.Text = ResourceLoader.GetString2("AlarmRule") + ":*"; 
            
		}

        #region Properties
       
        public FrontClientStateEnum FrontClientState
        {
            get
            {
                return this.frontClientState;
            }
            set
            {
                this.frontClientState = value;
                if (value == FrontClientStateEnum.WaitingForIncident)
                    SetFrontClientWaitingForCallState();
                else if (value == FrontClientStateEnum.RegisteringIncident)
                    SetFrontClientRegisteringCallState();
            }
        }
        public object CcvtZone
        {
            get
            {
                return this.comboBoxExCcvtZone.SelectedItem;
            }
            set
            {
                if(value != null)
                this.comboBoxExCcvtZone.Items.Add(value);
            }
        }       
        public object Struct
        {
            get
            {
                return this.comboBoxExStruct.SelectedItem;
            }
            set
            {
                if (value != null)
                this.comboBoxExStruct.Items.Add(value);
            }
        }        
        public object StructType
        {
            get
            {
                return this.comboBoxExStructType.SelectedItem;
            }
            set
            {
                if (value != null)
                    this.comboBoxExStructType.Items.Add(value);
            }
        }
        public object Camera
        {
            get
            {
                return this.comboBoxExCamera.SelectedItem;
            }
            set
            {
                if (value != null)
                this.comboBoxExCamera.Items.Add(value);
            }
        }       

        [RefreshProperties(RefreshProperties.All),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
        Browsable(false)]
        public AddressClientData CallerAddress
        {
            get
            {
                if (null != this.addressData)
                {
                    this.addressData.Zone = this.textBoxExZone.Text;
                    this.addressData.Street = this.textBoxExStreet.Text;
                    this.addressData.Reference = this.textBoxExCity.Text;
                    this.addressData.More = this.textBoxExTown.Text;                   
                }
                else
                {
                    this.addressData = new AddressClientData(
                        this.textBoxExZone.Text,
                        this.textBoxExStreet.Text,
                        this.textBoxExCity.Text,
                        this.textBoxExTown.Text,                        
                        false);
                }

                return addressData;
            }
            set
            {
                addressData = value;

                if (null != this.addressData)
                {
                    this.textBoxExZone.Text = this.addressData.Zone;
                    this.textBoxExStreet.Text = this.addressData.Street;
                    this.textBoxExCity.Text = this.addressData.Reference;
                    this.textBoxExTown.Text = this.addressData.More;               
                }
            }
        }

		public override bool Active
		{
			get
			{
				return base.Active;
			}
			set
			{
				base.Active = value;
				Image img;

				img = pictureBoxNumber.Image;

				pictureBoxNumber.Image = ResourceLoader.GetImage(GetNumberImageName());

				if (img != null)
					img.Dispose();
				if (Active)
				{                 
					this.pictureBoxNumber.LookAndFeel.Style = GetStyle(ActiveColor);
					this.groupControlBody.LookAndFeel.Style = GetStyle(ActiveColor);
					this.layoutControl1.LookAndFeel.Style = GetStyle(ActiveColor);
				}
				else
				{
					this.pictureBoxNumber.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Skin;
					this.groupControlBody.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Skin;
					this.layoutControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Skin;
					this.pictureBoxNumber.LookAndFeel.SkinName = InactiveColor;
					this.groupControlBody.LookAndFeel.SkinName = InactiveColor;
					this.layoutControl1.LookAndFeel.SkinName = InactiveColor;
				}
			}
		}

        #endregion        
        
        public void ChangeSelectedCamera(CameraClientData camera)
        {
            comboBoxExCcvtZone.SelectedValueChanged -= comboBoxExCcvtZone_SelectedValueChanged;
            comboBoxExCcvtZone.SelectedItem = camera.StructClientData.CctvZone;
            comboBoxExCcvtZone.SelectedValueChanged += comboBoxExCcvtZone_SelectedValueChanged;

            comboBoxExStruct.SelectedValueChanged -= comboBoxExStruct_SelectedValueChanged;
            comboBoxExStruct.Items.Clear();
            comboBoxExStruct.Items.Add(camera.StructClientData);

            comboBoxExStruct.SelectedValueChanged -= comboBoxExStruct_SelectedValueChanged;
            comboBoxExStruct.SelectedValueChanged += comboBoxExStruct_SelectedValueChanged;
            
            comboBoxExStruct.SelectedItem = camera.StructClientData;
            comboBoxExStruct.SelectedValueChanged += comboBoxExStruct_SelectedValueChanged;

            comboBoxExStructType.SelectedValueChanged -= comboBoxExStructType_SelectedValueChanged;
            comboBoxExStructType.SelectedItem = camera.StructClientData.Type;
            comboBoxExStructType.SelectedValueChanged += comboBoxExStructType_SelectedValueChanged;

            comboBoxExCamera.SelectedValueChanged -= comboBoxExCamera_SelectedValueChanged;
            comboBoxExCamera.Items.Clear();
            comboBoxExCamera.Items.Add(camera);
            comboBoxExCamera.SelectedItem = camera;
            textBoxExCity.Text = camera.StructClientData.State;
            textBoxExTown.Text = camera.StructClientData.Town;
            textBoxExZone.Text = camera.StructClientData.ZoneSecRef;
            textBoxExStreet.Text = camera.StructClientData.Street;
            comboBoxExCamera.SelectedValueChanged += comboBoxExCamera_SelectedValueChanged;

        }

        public void ChangeSelectedCameraAlarm(CameraClientDataAlarm camera)
        {
            comboBoxExCcvtZone.SelectedValueChanged -= comboBoxExCcvtZone_SelectedValueChanged;
            comboBoxExCcvtZone.SelectedItem = camera.StructClientData.CctvZone;
            comboBoxExCcvtZone.SelectedValueChanged += comboBoxExCcvtZone_SelectedValueChanged;

            comboBoxExStruct.SelectedValueChanged -= comboBoxExStruct_SelectedValueChanged;
            comboBoxExStruct.Items.Clear();
            comboBoxExStruct.Items.Add(camera.StructClientData);
            comboBoxExStruct.SelectedItem = camera.StructClientData;
            comboBoxExStruct.SelectedValueChanged += comboBoxExStruct_SelectedValueChanged;

            comboBoxExStructType.SelectedValueChanged -= comboBoxExStructType_SelectedValueChanged;
            comboBoxExStructType.SelectedItem = camera.StructClientData.Type;
            comboBoxExStructType.SelectedValueChanged += comboBoxExStructType_SelectedValueChanged;

            comboBoxExCamera.SelectedValueChanged -= comboBoxExCamera_SelectedValueChanged;
            comboBoxExCamera.Items.Clear();
            comboBoxExCamera.Items.Add((CameraClientDataAlarm)camera);
            comboBoxExCamera.SelectedItem = camera;
            textBoxExCity.Text = camera.StructClientData.State;
            textBoxExTown.Text = camera.StructClientData.Town;
            textBoxExZone.Text = camera.StructClientData.ZoneSecRef;
            textBoxExStreet.Text = camera.StructClientData.Street;
            if (camera.AlarmDate != null) { 
            textBoxExAlarmDate.Text = camera.AlarmDate.ToString();
            textBoxExAlarmRule.Text = camera.AlarmRule;
            }
            comboBoxExCamera.SelectedValueChanged += comboBoxExCamera_SelectedValueChanged;

        }

        private void comboBoxExCcvtZone_SelectedValueChanged(object sender, EventArgs e)
        {          
            cctvZoneSelected = (CctvZoneClientData)(sender as ComboBox).SelectedItem;
            if (cctvZoneSelected != null)
            {
                comboBoxExStruct.Items.Clear();
                comboBoxExCamera.Items.Clear();
                ResetAddresControls();

                IList structs = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(
                    SmartCadHqls.GetStructsByCctvZoneName, cctvZoneSelected.Name));
                foreach (StructClientData structData in structs)
                {
                    comboBoxExStruct.Items.Add(structData);
                }
                
            }
        }

        private void comboBoxExStructType_SelectedValueChanged(object sender, EventArgs e)
        {
            if (cctvZoneSelected != null)
            {
                StructTypeClientData structTypeSelected;
                structTypeSelected = (StructTypeClientData)(sender as ComboBox).SelectedItem;
                if (structTypeSelected != null)
                {
                    comboBoxExStruct.Items.Clear();
                    comboBoxExCamera.Items.Clear();
                    ResetAddresControls();
                    IList structs = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(
                        SmartCadHqls.GetStructsByStructTypeAndCctvZone, structTypeSelected.Code, cctvZoneSelected.Code));
                    foreach (StructClientData structData in structs)
                    {
                        comboBoxExStruct.Items.Add(structData);
                    }
                }
                
            }
        }

        private void comboBoxExStruct_SelectedValueChanged(object sender, EventArgs e)
        {
            if (comboBoxExStruct.SelectedIndex > -1)
            {
                StructClientData structSelected;
                structSelected = (StructClientData)(sender as ComboBox).SelectedItem;
                if (structSelected != null)
                {
                    ResetAddresControls();
                    this.textBoxExCity.Text = structSelected.State;
                    this.textBoxExTown.Text = structSelected.Town;
                    this.textBoxExZone.Text = structSelected.ZoneSecRef;
                    this.textBoxExStreet.Text = structSelected.Street;
                    comboBoxExCamera.Items.Clear();
                  
                    int operCode = (int)ServerServiceClient.GetInstance().OperatorClient.Code;

                    IList cameras = ServerServiceClient.GetInstance().SearchClientObjects(
                        SmartCadHqls.GetCustomHql
                        (@"SELECT ObjectData.Device 
                                                FROM OperatorDeviceData ObjectData 
                                                WHERE ObjectData.User.Code = {0} and ObjectData.Device.Struct.Code = {1}", operCode, structSelected.Code));

                    foreach (CameraClientData cameraData in cameras)
                    {
                        comboBoxExCamera.Items.Add(cameraData);
                    }
                    if (comboBoxExCamera.Items.Count > 0) comboBoxExCamera.SelectedIndex = 0;
                    else comboBoxExCamera.SelectedIndex = -1;
                    EventHandler<GeoPointEventArgs> handler = SendGeoPointStruct;
                    if (handler != null)
                    {
                        handler(this, new GeoPointEventArgs(new GeoPoint(structSelected.Lon, structSelected.Lat)));
                    }
                }
            }
            else {
                comboBoxExCamera.Items.Clear();
            }

        }

        private void comboBoxExCamera_SelectedValueChanged(object sender, EventArgs e)
        {
            if((CameraClientData)(sender as ComboBox).SelectedItem != null) { 
                CameraClientData cameraSelected;
                cameraSelected = (CameraClientData)(sender as ComboBox).SelectedItem;
                if (cameraSelected != null)
                {
                    if (cameraSelected.StructClientData != null)
                    {
                        this.textBoxExCity.Text = cameraSelected.StructClientData.State;
                        this.textBoxExTown.Text = cameraSelected.StructClientData.Town;
                        this.textBoxExZone.Text = cameraSelected.StructClientData.ZoneSecRef;
                        this.textBoxExStreet.Text = cameraSelected.StructClientData.Street;
                    }
                }
            }else
            {
                CameraClientDataAlarm cameraSelected;
                cameraSelected = (CameraClientDataAlarm)(sender as ComboBox).SelectedItem;
                if (cameraSelected != null)
                {
                    if (cameraSelected.StructClientData != null)
                    {
                        this.textBoxExCity.Text = cameraSelected.StructClientData.State;
                        this.textBoxExTown.Text = cameraSelected.StructClientData.Town;
                        this.textBoxExZone.Text = cameraSelected.StructClientData.ZoneSecRef;
                        this.textBoxExStreet.Text = cameraSelected.StructClientData.Street;
                    }
                }
            }
        }

        private void ResetAddresControls()
        {           
            this.textBoxExCity.Text = string.Empty;
            this.textBoxExTown.Text = string.Empty;
            this.textBoxExZone.Text = string.Empty;
            this.textBoxExStreet.Text = string.Empty;

            this.textBoxExAlarmDate.Text = string.Empty;
            this.textBoxExAlarmRule.Text = string.Empty;
        }

        private void SetFrontClientUpdatingCallState()
        {
            this.textBoxExStreet.Enabled = false;
            this.textBoxExZone.Enabled = false;
        }

        private void SetFrontClientRegisteringCallState()
        {
            //this.comboBoxExCcvtZone.Enabled = true;
            //this.comboBoxExStructType.Enabled = true;
            //this.comboBoxExStruct.Enabled = true;
            //this.comboBoxExCamera.Enabled = true;
            //this.pictureBoxNumber.Enabled = true;
        }

        private void SetFrontClientWaitingForCallState()
        {
            this.comboBoxExCcvtZone.Enabled = false;
            this.comboBoxExStructType.Enabled = false;
            this.comboBoxExStruct.Enabled = false;
            this.comboBoxExCamera.Enabled = false;
            this.pictureBoxNumber.Enabled = false;
        }

        public void CleanControl()
        {
            FormUtil.InvokeRequired(this, new MethodInvoker(delegate
            {
               
                this.textBoxExStreet.Text = "";
                this.textBoxExZone.Text = "";
                this.textBoxExCity.Text = "";
                this.textBoxExTown.Text = "";
                this.comboBoxExCamera.SelectedItem = null;
                this.comboBoxExStruct.SelectedItem = null;
                this.comboBoxExStructType.SelectedItem = null;
                this.comboBoxExCcvtZone.SelectedItem = null;
                this.textBoxExAlarmDate.Text = string.Empty;
                this.textBoxExAlarmRule.Text = string.Empty;
            }));
        }

        public void ResetCombobox()
        {
            this.comboBoxExCamera.Items.Clear();
            this.comboBoxExStruct.Items.Clear();                    
        }

        public void AddComboBoxItem(object item) 
        {
            if (item is CameraClientData)
            {
                comboBoxExCamera.Items.Add((CameraClientData)item);
            }
            else if (item is CctvZoneClientData)
            {
                comboBoxExCcvtZone.Items.Add((CctvZoneClientData)item);
            }
            else if (item is  StructClientData)
            {
                comboBoxExStruct.Items.Add((StructClientData)item);  
            }
            else if(item is CameraClientDataAlarm)
            {
                comboBoxExCamera.Items.Add((CameraClientDataAlarm)item);
            }

        }

        public void UpdateComboBoxItem(object item)
        {
            int index = -1;

            if (item is CameraClientData)
            {
                comboBoxExCamera.SelectedIndex = -1;
                index = comboBoxExCamera.Items.IndexOf((CameraClientData)item);

                if (index > -1)
                    comboBoxExCamera.Items[index] = (CameraClientData)item;
            }
            else if (item is CameraClientDataAlarm)
            {
                comboBoxExCamera.SelectedIndex = -1;
                index = comboBoxExCamera.Items.IndexOf((CameraClientDataAlarm)item);

                if (index > -1)
                    comboBoxExCamera.Items[index] = (CameraClientDataAlarm)item;
            }
            else if (item is CctvZoneClientData)
            {
                comboBoxExCcvtZone.SelectedIndex = -1;
                comboBoxExStructType.SelectedIndex = -1;
                comboBoxExStruct.SelectedIndex = -1;
                comboBoxExCamera.SelectedIndex = -1;

                index = comboBoxExCcvtZone.Items.IndexOf((CctvZoneClientData)item);
                if (index > -1)
                    comboBoxExCcvtZone.Items[index] = (CctvZoneClientData)item;
            }
            else if (item is StructClientData)
            {
                comboBoxExStruct.SelectedIndex = -1;
                comboBoxExCamera.SelectedIndex = -1;
                StructClientData st = (StructClientData)item;

                if ((comboBoxExCcvtZone.SelectedItem != null && st.CctvZone.Code != ((CctvZoneClientData)comboBoxExCcvtZone.SelectedItem).Code) || (comboBoxExStructType.SelectedItem != null && st.Type.Name != comboBoxExStructType.SelectedItem.ToString()))
                {
                    comboBoxExCcvtZone.SelectedIndex = -1;
                    comboBoxExStructType.SelectedIndex = -1;
                    comboBoxExStruct.Items.Clear();
                    comboBoxExCamera.Items.Clear();
                
                   }
                else 
                {
                    index = comboBoxExStruct.Items.IndexOf(st);
                    if (index > -1)
                        comboBoxExStruct.Items[index] = st;
                
                }
            }

        }

        public void DeleteComboBoxItem(object item)
        {
            if (item is CameraClientData)
            {
                comboBoxExCamera.Items.Remove((CameraClientData)item);
            }
            else if (item is CameraClientDataAlarm)
            {
                comboBoxExCamera.Items.Remove((CameraClientDataAlarm)item);
            }
            else if (item is CctvZoneClientData)
            {
                comboBoxExCcvtZone.Items.Remove((CctvZoneClientData)item);
                comboBoxExStruct.Items.Clear();
                comboBoxExCamera.Items.Clear();
            }
            else if (item is StructClientData)
            {
                comboBoxExStruct.Items.Remove((StructClientData)item);
                comboBoxExCamera.Items.Clear();
            }

        }

        [Browsable(true)]
        public event EventHandler<GeoPointEventArgs> SendGeoPointStruct;

  
    }
}
