﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Smartmatic.SmartCad.Service;
using System.Threading;
using DevExpress.XtraLayout;
using System.IO;
using System.Xml;
using System.Linq;
//using SmartCadControls.Controls;
using SmartCadCore.ClientData;
using Smartmatic.SmartCad.Vms;
using SmartCadControls.Controls;
using SmartCadGuiCommon.Classes;
using SmartCadGuiCommon.CCTV.Nodes;
using SlimDX.DirectInput;
using SmartCadCore.Core;

namespace SmartCadGuiCommon.Controls
{
    public delegate void SelectionChanged(CameraPanelControl selectedCamera);

    public partial class TemplatePanelControl : DevExpress.XtraEditors.XtraUserControl, IPlayBackPanel
    {
        public event SelectionChanged SelectionChanged;

        public CctvTemplateNode Template { get; set; }
        public Smartmatic.SmartCad.Vms.PlayModes Mode { get; set; }
        public  List<CameraPanelControl> CameraPanels = new List<CameraPanelControl>();
        private bool changed = false;
        private ConfigurationClientData config = null;
        private Joystick Joy;
        public static List<Form> unpinCameras = new List<Form>();
        public TemplatePanelControl listCamerasinTemplates;

        public TemplatePanelControl()
        {
            InitializeComponent();
            this.Text = "";
            dragDropLayoutControl1.CalcHitInfo = false;
            dragDropLayoutControl1.ControlCreated += dragDropLayoutControl1_ControlCreated;
            ((ILayoutControl)dragDropLayoutControl1.layoutControl).OptionsView.AllowHotTrack = true;
            ((ILayoutControl)dragDropLayoutControl1.layoutControl).OptionsCustomizationForm.AllowHandleDeleteKey = true;
            ((ILayoutControl)dragDropLayoutControl1.layoutControl).OptionsCustomizationForm.EnableUndoManager = true;
        }

        public TemplatePanelControl(CctvTemplateNode template, Smartmatic.SmartCad.Vms.PlayModes mode, ConfigurationClientData config)
            : this(template, mode, config, true)
        {
        }

        public TemplatePanelControl(CctvTemplateNode template, Smartmatic.SmartCad.Vms.PlayModes mode, ConfigurationClientData config, bool unping)
            : this()
        {
            this.config = config;

            Mode = mode;
            Template = template;
            Name = Template.Name;
            barStaticText.Caption = Template.Text;
            if (Mode != Smartmatic.SmartCad.Vms.PlayModes.Live)
            {
                barManager1.ShowCloseButton = false;
                barButtonUnping.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;//.Enabled = false;
            }
            else
            {
                barButtonUnping.Visibility = unping ? DevExpress.XtraBars.BarItemVisibility.Always :
                    DevExpress.XtraBars.BarItemVisibility.Never;
                bar2.Visible = unping;
            }

            if (Template.RenderMethod != null)
                RestoreLayoutControlFromXML(dragDropLayoutControl1.layoutControl, Template.RenderMethod);
        }

        public void CameraPanelControlTemplates()
        {
            foreach (var cameraPanel in CameraPanels)
            {
                cameraPanel.CameraPanelControlTemplates();
                break;
            }
        }


        private void dragDropLayoutControl1_ControlCreated(object sender, DragDropControlEventArgs e)
        {
            if (e.Item != null)
            {
                System.GC.Collect();
                ((ILayoutControl)dragDropLayoutControl1.layoutControl).EnableCustomizationMode = true;

                CameraPanelControl selectedCamera;
                System.Windows.Forms.Control.ControlCollection collection = dragDropLayoutControl1.layoutControl.Controls;
                int cont = GetTypeCameraControl(collection);
                CameraPanelControl panel = new CameraPanelControl(e.Item.Tag as CameraClientData, PlayModes.Live, config, cont);
                panel.SelectedChanged += panel_SelectedChanged;
                CameraPanels.Add(panel);
                //if(DateTimeValueChanged!=null)
                //    panel.DateTimeValueChanged += (VmsControlEx.DateTimeValueChangedEventArgs)DateTimeValueChanged.GetInvocationList()[0];
                e.Item.Control = panel;
                e.Item.TextVisible = false;

                changed = true;
            }
        }

        private void ConnectJoy()
        {

            DirectInput input = new DirectInput();
            IList<SlimDX.DirectInput.DeviceInstance> deviceList = input.GetDevices(SlimDX.DirectInput.DeviceType.Joystick, DeviceEnumerationFlags.AttachedOnly);

            //DeviceList deviceList = Manager.GetDevices(DeviceType.Joystick, EnumDevicesFlags.AttachedOnly);

            SlimDX.DirectInput.DeviceInstance deviceInstance = null;
            for (int i = 0; i < deviceList.Count; i++)
            {
                //deviceList.MoveNext();
                deviceInstance = (SlimDX.DirectInput.DeviceInstance)deviceList[i];
                Joy = new Joystick(input, deviceInstance.ProductGuid);
                SlimDX.DirectInput.JoystickState state = Joy.GetCurrentState();
                break;
            }
        }

        private int GetTypeCameraControl(ControlCollection collection)
        {
            int cont = 0;
            foreach (var item in collection)
            {
                if (item.GetType() == typeof(CameraPanelControl))
                    cont++;
            }

            return cont;
        }

        void panel_SelectedChanged(CameraPanelControl camera)
        {
            if (SelectionChanged != null)
                SelectionChanged(camera);
        }

        private void barManager1_CloseButtonClick(object sender, EventArgs e)
        {
            if (Parent.Parent is Form && Parent.Parent.Text == "Template")
                Parent.Parent.Dispose();
            else
            {
                this.Dispose();
                this.Parent = null;
                CctvFrontClientFormDevX.cameraVisualizeForm.SelectedTemplate = null;

            }
        }

        private void barButtonUnping_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Parent != null) Parent.Controls.Clear();

            Form floatingForm = new Form();
            floatingForm.FormBorderStyle = FormBorderStyle.SizableToolWindow;
            floatingForm.Margin = new System.Windows.Forms.Padding(0);
            LayoutControl layout = new LayoutControl();
            layout.Margin = new System.Windows.Forms.Padding(0);
            layout.Clear();

            TemplatePanelControl panelControl = new TemplatePanelControl(Template, Mode, config, false);
            if (SelectionChanged != null)
                panelControl.SelectionChanged += (SelectionChanged)SelectionChanged.GetInvocationList()[0];

            LayoutControlItem dragItem = layout.AddItem();
            dragItem.Control = panelControl;
            dragItem.Name = Name;
            dragItem.Text = "";
            dragItem.TextVisible = false;
            dragItem.Padding = new DevExpress.XtraLayout.Utils.Padding(0);
            dragItem.ParentName = "Customization";

            layout.GetGroupAtPoint(new Point(1, 1)).GroupBordersVisible = false;
            layout.GetGroupAtPoint(new Point(1, 1)).Padding = new DevExpress.XtraLayout.Utils.Padding(0);

            floatingForm.Size = new System.Drawing.Size(700, 500);
            floatingForm.StartPosition = FormStartPosition.CenterScreen;
            floatingForm.Controls.Add(layout);
            layout.Dock = DockStyle.Fill;
            floatingForm.Text = "Template";
            floatingForm.Show();
   //         this.ParentForm.AddOwnedForm(floatingForm);

            unpinCameras.Add(floatingForm);
            listCamerasinTemplates = panelControl;
        }


        public TemplatePanelControl listCamerasinTemplatesUnpin()
        {
            return listCamerasinTemplates;
        }

        public void ClearlistCamerasinTemplatesUnpin()
        {
            listCamerasinTemplates = new TemplatePanelControl();
        }

        public List<Form> unpinList()
        {
            return unpinCameras;
        }

        public void ClearunpinList()
        {
            unpinCameras = new List<Form>();
        }

        public void SetPlayMode(PlayModes playMode)
        {

        }

        public void DisposePanelControl()
        {
            foreach (CameraPanelControl camera in CameraPanels)
            {
                camera.CameraPanelControlTemplates(camera.vmsControl);
                camera.Controls.Clear();

            } 
        }

        public void ClearConnectionsEditButton()
        {
            CameraPanelControl camera = new CameraPanelControl();
                camera.CameraPanelControlTemplates();
                camera.Controls.Clear();
            
        }


        public void ClearControlPanel()
        {
            this.Controls.Clear();
        }

        public void StartEdit()
        {
            ((ILayoutControl)dragDropLayoutControl1.layoutControl).EnableCustomizationMode = true;
            barManager1.ShowCloseButton = false;
            barButtonUnping.Enabled = false;
        }

        public void EndEdit()
        {
            ((ILayoutControl)dragDropLayoutControl1.layoutControl).EnableCustomizationMode = false;
            barManager1.ShowCloseButton = true;
            barButtonUnping.Enabled = true;
        }

        public bool HasChanged()
        {
            return changed ? true : ((ILayoutControl)dragDropLayoutControl1.layoutControl).UndoManager.IsUndoAllowed;
        }

        public string GetRenderMethod()
        {
            return GetRenderMethod(this.dragDropLayoutControl1.layoutControl);
        }

        private string GetRenderMethod(LayoutControl layout)
        {
            string son = "";
            try
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    layout.SaveLayoutToStream(ms);
                    ms.Position = 0;
                    using (MemoryStream msTotal = InsertControlsInXML(ms))
                    {
                        msTotal.Position = 0;
                        StreamReader sr = new StreamReader(msTotal);
                        son = sr.ReadToEnd();

                        sr.Close();
                        sr.Dispose();
                    }
                }
            }
            catch { }
            return son;
        }

        private MemoryStream InsertControlsInXML(Stream s)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(s);
            XmlNode firstChild = doc.FirstChild.ChildNodes[0];
            foreach (BaseLayoutItem baseLayoutItem in dragDropLayoutControl1.layoutControl.Items)
            {
                if (baseLayoutItem is LayoutControlItem)
                {
                    LayoutControlItem item = (LayoutControlItem)baseLayoutItem;
                    if (item.Control != null)
                    {
                        XmlNode node = doc.CreateNode(XmlNodeType.Element, "Control", "");
                        XmlAttribute control = doc.CreateAttribute("type");
                        XmlAttribute name = doc.CreateAttribute("name");
                        name.Value = item.Control.Name;
                        XmlAttribute itemName = doc.CreateAttribute("itemName");
                        itemName.Value = item.Name;
                        XmlAttribute itemTex = doc.CreateAttribute("itemText");
                        itemTex.Value = item.Text;
                        node.Attributes.Append(itemTex);
                        node.Attributes.Append(itemName);
                        node.Attributes.Append(control);
                        node.Attributes.Append(name);
                        doc.FirstChild.InsertBefore(node, firstChild);
                    }
                }
            }
            MemoryStream ms = new MemoryStream();
            doc.Save(ms);
            return ms;
        }

        public void RestoreLayoutControlFromXML(LayoutControl layout, string render)
        {
            try
            {
                MemoryStream ms = GetMemoryStreamWithXML(layout, render);
                layout.BeginUpdate();
                layout.RestoreLayoutFromStream(ms);
                ms.Close();
                layout.EndUpdate();
                ms.Dispose();
            }
            catch (Exception e)
            {

            }
        }

        private MemoryStream GetMemoryStreamWithXML(LayoutControl layout, string render)
        {
            byte[] byteArray = Encoding.UTF8.GetBytes(render);
            MemoryStream ms = new MemoryStream(byteArray);
            ms.Position = 0;
            XmlDocument doc = new XmlDocument();
            doc.Load(ms);
            XmlNode firstChild = doc.FirstChild;
            layout.BeginUpdate();
            layout.GetGroupAtPoint(new Point(1, 1)).GroupBordersVisible = false;
            layout.GetGroupAtPoint(new Point(1, 1)).Padding = new DevExpress.XtraLayout.Utils.Padding(0);
            foreach (XmlNode node in firstChild.ChildNodes)
            {
                if (node.Name == "Control")
                {
                    LayoutControlItem item = layout.AddItem();
                    item.Padding = new DevExpress.XtraLayout.Utils.Padding(0);
                    item.Name = node.Attributes["itemName"].Value;
                    foreach (CctvCameraNode camera in Template.Cameras)
                    {
                        if (camera.Name == node.Attributes["itemName"].Value)
                        {
                            node.Attributes["itemText"].Value = camera.Text;
                            item.Control = new CameraPanelControl(camera.Camera, Mode, config, 1);
                            item.Control.Disposed += Camera_Disposed;
                            CameraPanels.Add((CameraPanelControl)item.Control);
                            ((CameraPanelControl)item.Control).SelectedChanged += panel_SelectedChanged;
                            ((CameraPanelControl)item.Control).DateTimeValueChanged += control_DateTimeValueChanged;
                            break;
                        }
                    }
                    if (item.Control == null) // No access
                        item.Control = new CameraPanelControl(new CameraClientData() { Name = node.Attributes["name"].Value }, Mode, config, 1);
                    item.Control.Name = node.Attributes["name"].Value;
                    item.Control.Text = "";

                    //layout.BeginUpdate();
                    //layout.Controls.Add(item.Control);
                    //layout.EndUpdate();
                    //layout.Invalidate();
                }
            }
            MemoryStream finalStream = new MemoryStream();
            doc.Save(finalStream);
            finalStream.Position = 0;
            layout.EndUpdate();
            return finalStream;
        }

        void control_DateTimeValueChanged(DateTime dateTime)
        {
            if (DateTimeValueChanged != null)
            {
                DateTimeValueChanged(dateTime);
            }
        }

        void Camera_Disposed(object sender, EventArgs e)
        {
            CameraPanels.Remove(sender as CameraPanelControl);
            changed = true;
        }

        public List<string> GetCamerasName()
        {
            List<string> cameras = new List<string>();

            foreach (LayoutControlItem item in dragDropLayoutControl1.layoutControl.Items.OfType<LayoutControlItem>())
            {
                cameras.Add(item.Name);
            }

            return cameras;
        }

        public bool ContainsCamera(string cameraName)
        {
            return dragDropLayoutControl1.layoutControl.Items.OfType<LayoutControlItem>().SingleOrDefault(
                    item => item.Text == cameraName) != null;
        }

        public void ClearVMSControl()
        {
            foreach (var cameraPanel in CameraPanels)
            {
                cameraPanel.ClearVMSControl();
            }
        }

        public void ClearVMSControlPlayback()
        {
            foreach (var cameraPanel in CameraPanels)
            {
                cameraPanel.ClearVMSControlPlayback();
                //break;
            }
        }


        #region IPlayBackPanel Members

        public event Smartmatic.SmartCad.Vms.VmsControlEx.DateTimeValueChangedEventHandler DateTimeValueChanged;

        public double PlaySpeed
        {
            set
            {
                foreach (CameraPanelControl cameraPanel in CameraPanels)
                {
                    cameraPanel.PlaySpeed = value;
                }
            }
        }

        public int Volume
        {
            set
            {
                foreach (CameraPanelControl cameraPanel in CameraPanels)
                {
                    cameraPanel.Volume = value;
                }
            }
        }

        public bool Mute
        {
            set
            {
                foreach (CameraPanelControl cameraPanel in CameraPanels)
                {
                    cameraPanel.Mute = value;
                }
            }
        }

        public int PlayDirection
        {
            set
            {
                foreach (CameraPanelControl cameraPanel in CameraPanels)
                {
                    cameraPanel.PlayDirection = value;
                }
            }
        }

        public DateTime StartDate
        {
            set
            {
                foreach (CameraPanelControl cameraPanel in CameraPanels)
                {
                    cameraPanel.StartDate = value;
                }
            }
        }

        public DateTime EndDate
        {
            set
            {
                foreach (CameraPanelControl cameraPanel in CameraPanels)
                {
                    cameraPanel.EndDate = value;
                }
            }
        }

        public void StopPlayBack()
        {
            foreach (CameraPanelControl cameraPanel in CameraPanels)
            {
                cameraPanel.StopPlayBack();
            }
        }

        public void StartPlayBack()
        {
            foreach (CameraPanelControl cameraPanel in CameraPanels)
            {
                cameraPanel.StartPlayBack();
            }
        }

        public void ExportVideo(DateTime startDate, DateTime endDate, string path, int format, int timestamp)
        {
            foreach (CameraPanelControl cameraPanel in CameraPanels)
            {
                cameraPanel.ExportVideo(startDate, endDate, path, format, timestamp);
            }
        }

        public void InitStopPlayback()
        {
            foreach (CameraPanelControl cameraPanel in CameraPanels)
            {
                cameraPanel.InitStopPlayback();
            }
        }

        #endregion

    }
}
