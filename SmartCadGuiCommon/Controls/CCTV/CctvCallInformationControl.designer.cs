
using SmartCadControls.Controls;

namespace SmartCadGuiCommon.Controls
{
    partial class CctvCallInformationControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupControlBody = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.comboBoxExCcvtZone = new ComboBoxEx();
            this.pictureBoxNumber = new DevExpress.XtraEditors.PictureEdit();
            this.textBoxExTown = new TextBoxEx();
            this.comboBoxExStruct = new ComboBoxEx();
            this.comboBoxExCamera = new ComboBoxEx();
            this.comboBoxExStructType = new ComboBoxEx();
            this.textBoxExCity = new TextBoxEx();
            this.textBoxExZone = new TextBoxEx();
            this.textBoxExStreet = new TextBoxEx();
            this.textBoxExAlarmDate = new TextBoxEx();
            this.textBoxExAlarmRule = new TextBoxEx();
        
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemZone = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemStructType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemZoneName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemStreet = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItemStruct = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemCamera = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemCity = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemTown = new DevExpress.XtraLayout.LayoutControlItem();
            // Analytical Video Controls Associated
            this.layoutControlItemAlarmDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemAlarmRule = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlBody)).BeginInit();
            this.groupControlBody.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemZone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemStructType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemZoneName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemStreet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemStruct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCamera)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTown)).BeginInit();
            // Analytical Video Controls Associated
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAlarmDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAlarmRule)).BeginInit();
            this.SuspendLayout();
            
            

            // 
            // groupControlBody
            // 
            this.groupControlBody.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(252)))), ((int)(((byte)(226)))));
            this.groupControlBody.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(252)))), ((int)(((byte)(226)))));
            this.groupControlBody.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(94)))), ((int)(((byte)(50)))));
            this.groupControlBody.Appearance.Options.UseBackColor = true;
            this.groupControlBody.Appearance.Options.UseBorderColor = true;
            this.groupControlBody.AppearanceCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(115)))), ((int)(((byte)(172)))), ((int)(((byte)(68)))));
            this.groupControlBody.AppearanceCaption.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(94)))), ((int)(((byte)(50)))));
            this.groupControlBody.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControlBody.AppearanceCaption.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.groupControlBody.AppearanceCaption.Options.UseBackColor = true;
            this.groupControlBody.AppearanceCaption.Options.UseFont = true;
            this.groupControlBody.Controls.Add(this.layoutControl1);
            this.groupControlBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControlBody.Location = new System.Drawing.Point(0, 0);
            this.groupControlBody.LookAndFeel.SkinName = "Blue";
            this.groupControlBody.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.groupControlBody.LookAndFeel.UseDefaultLookAndFeel = false;
            this.groupControlBody.Name = "groupControlBody";
            this.groupControlBody.Size = new System.Drawing.Size(466,155); 
            this.groupControlBody.TabIndex = 41;
            this.groupControlBody.Text = "groupControlBody";
            // 
            // layoutControl1
            // 
            this.layoutControl1.AllowCustomizationMenu = false;
            this.layoutControl1.Appearance.DisabledLayoutGroupCaption.ForeColor = System.Drawing.SystemColors.GrayText;
            this.layoutControl1.Appearance.DisabledLayoutGroupCaption.Options.UseForeColor = true;
            this.layoutControl1.Appearance.DisabledLayoutItem.ForeColor = System.Drawing.SystemColors.GrayText;
            this.layoutControl1.Appearance.DisabledLayoutItem.Options.UseForeColor = true;
            this.layoutControl1.BackColor = System.Drawing.Color.White;
            this.layoutControl1.Controls.Add(this.comboBoxExCcvtZone);
            this.layoutControl1.Controls.Add(this.pictureBoxNumber);
            this.layoutControl1.Controls.Add(this.textBoxExTown);
            this.layoutControl1.Controls.Add(this.comboBoxExStruct);
            this.layoutControl1.Controls.Add(this.comboBoxExCamera);
            this.layoutControl1.Controls.Add(this.comboBoxExStructType);
            this.layoutControl1.Controls.Add(this.textBoxExCity);
            this.layoutControl1.Controls.Add(this.textBoxExZone);
            this.layoutControl1.Controls.Add(this.textBoxExStreet);
            // Analytical Video
            this.layoutControl1.Controls.Add(this.textBoxExAlarmDate);
            this.layoutControl1.Controls.Add(this.textBoxExAlarmRule); 
            //
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 19);
            this.layoutControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.layoutControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(462, 134);
            this.layoutControl1.TabIndex = 49;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // comboBoxExCcvtZone
            // 
            this.comboBoxExCcvtZone.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxExCcvtZone.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.comboBoxExCcvtZone.FormattingEnabled = true;
            this.comboBoxExCcvtZone.Location = new System.Drawing.Point(202, 6);
            this.comboBoxExCcvtZone.Name = "comboBoxExCcvtZone";
            this.comboBoxExCcvtZone.Size = new System.Drawing.Size(60, 21);
            this.comboBoxExCcvtZone.Sorted = true;
            this.comboBoxExCcvtZone.TabIndex = 41;
            this.comboBoxExCcvtZone.Enabled = false;
            this.comboBoxExCcvtZone.SelectedValueChanged += new System.EventHandler(this.comboBoxExCcvtZone_SelectedValueChanged);
            // 
            // pictureBoxNumber
            // 
            this.pictureBoxNumber.Location = new System.Drawing.Point(6, 6);
            this.pictureBoxNumber.Name = "pictureBoxNumber";
            this.pictureBoxNumber.Properties.ReadOnly = true;
            this.pictureBoxNumber.Properties.ShowMenu = false;
            this.pictureBoxNumber.Size = new System.Drawing.Size(41, 41);
            this.pictureBoxNumber.StyleController = this.layoutControl1;
            this.pictureBoxNumber.TabIndex = 0;
            // 
            // textBoxExTown
            // 
            this.textBoxExTown.AllowsLetters = true;
            this.textBoxExTown.AllowsNumbers = true;
            this.textBoxExTown.AllowsPunctuation = true;
            this.textBoxExTown.AllowsSeparators = true;
            this.textBoxExTown.AllowsSymbols = true;
            this.textBoxExTown.AllowsWhiteSpaces = true;
            this.textBoxExTown.Enabled = false;
            this.textBoxExTown.ExtraAllowedChars = "";
            this.textBoxExTown.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExTown.Location = new System.Drawing.Point(417, 36);
            this.textBoxExTown.MaxLength = 100;
            this.textBoxExTown.Name = "textBoxExTown";
            this.textBoxExTown.NonAllowedCharacters = "";
            this.textBoxExTown.RegularExpresion = "";
            this.textBoxExTown.Size = new System.Drawing.Size(39, 20);
            this.textBoxExTown.TabIndex = 46;
            // 
            // comboBoxExStruct
            // 
            this.comboBoxExStruct.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxExStruct.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.comboBoxExStruct.FormattingEnabled = true;
            this.comboBoxExStruct.Location = new System.Drawing.Point(202, 66);
            this.comboBoxExStruct.Name = "comboBoxExStruct";
            this.comboBoxExStruct.Size = new System.Drawing.Size(60, 21);
            this.comboBoxExStruct.Sorted = true;
            this.comboBoxExStruct.TabIndex = 43;
            this.comboBoxExStruct.Enabled = false;
            this.comboBoxExStruct.SelectedValueChanged += new System.EventHandler(this.comboBoxExStruct_SelectedValueChanged);
            // 
            // comboBoxExCamera
            // 
            this.comboBoxExCamera.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxExCamera.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.comboBoxExCamera.FormattingEnabled = true;
            this.comboBoxExCamera.Location = new System.Drawing.Point(202, 96);
            this.comboBoxExCamera.Name = "comboBoxExCamera";
            this.comboBoxExCamera.Size = new System.Drawing.Size(60, 21);
            this.comboBoxExCamera.Sorted = true;
            this.comboBoxExCamera.TabIndex = 44;
            this.comboBoxExCamera.Enabled = false;
            this.comboBoxExCamera.SelectedValueChanged += new System.EventHandler(this.comboBoxExCamera_SelectedValueChanged);

            // 
            // textBoxExAlarmDate
            // 
            this.textBoxExAlarmDate.AllowsLetters = true;
            this.textBoxExAlarmDate.AllowsNumbers = true;
            this.textBoxExAlarmDate.AllowsPunctuation = true;
            this.textBoxExAlarmDate.AllowsSeparators = true;
            this.textBoxExAlarmDate.AllowsSymbols = true;
            this.textBoxExAlarmDate.AllowsWhiteSpaces = true;
            this.textBoxExAlarmDate.Enabled = false;
            this.textBoxExAlarmDate.ExtraAllowedChars = "";
            this.textBoxExAlarmDate.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExAlarmDate.Location = new System.Drawing.Point(202, 156);
            this.textBoxExAlarmDate.MaxLength = 40;
            this.textBoxExAlarmDate.Name = "textBoxExAlarmDate";
            this.textBoxExAlarmDate.NonAllowedCharacters = "";
            this.textBoxExAlarmDate.RegularExpresion = "";
            this.textBoxExAlarmDate.Size = new System.Drawing.Size(39, 20); 
            this.textBoxExAlarmDate.TabIndex = 49;
            
            // 
            // comboBoxExStructType
            // 
            this.comboBoxExStructType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxExStructType.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.comboBoxExStructType.FormattingEnabled = true;
            this.comboBoxExStructType.Location = new System.Drawing.Point(202, 36);
            this.comboBoxExStructType.Name = "comboBoxExStructType";
            this.comboBoxExStructType.Size = new System.Drawing.Size(60, 21);
            this.comboBoxExStructType.Sorted = true;
            this.comboBoxExStructType.TabIndex = 42;
            this.comboBoxExStructType.Enabled = false;
            this.comboBoxExStructType.SelectedValueChanged += new System.EventHandler(this.comboBoxExStructType_SelectedValueChanged);
            // 
            // textBoxExCity
            // 
            this.textBoxExCity.AllowsLetters = true;
            this.textBoxExCity.AllowsNumbers = true;
            this.textBoxExCity.AllowsPunctuation = true;
            this.textBoxExCity.AllowsSeparators = true;
            this.textBoxExCity.AllowsSymbols = true;
            this.textBoxExCity.AllowsWhiteSpaces = true;
            this.textBoxExCity.Enabled = false;
            this.textBoxExCity.ExtraAllowedChars = "";
            this.textBoxExCity.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExCity.Location = new System.Drawing.Point(417, 6);
            this.textBoxExCity.MaxLength = 40;
            this.textBoxExCity.Name = "textBoxExCity";
            this.textBoxExCity.NonAllowedCharacters = "";
            this.textBoxExCity.RegularExpresion = "";
            this.textBoxExCity.Size = new System.Drawing.Size(39, 20);
            this.textBoxExCity.TabIndex = 45;
            // 
            // textBoxExZone
            // 
            this.textBoxExZone.AllowsLetters = true;
            this.textBoxExZone.AllowsNumbers = true;
            this.textBoxExZone.AllowsPunctuation = true;
            this.textBoxExZone.AllowsSeparators = true;
            this.textBoxExZone.AllowsSymbols = true;
            this.textBoxExZone.AllowsWhiteSpaces = true;
            this.textBoxExZone.Enabled = false;
            this.textBoxExZone.ExtraAllowedChars = "";
            this.textBoxExZone.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExZone.Location = new System.Drawing.Point(417, 66);
            this.textBoxExZone.MaxLength = 40;
            this.textBoxExZone.Name = "textBoxExZone";
            this.textBoxExZone.NonAllowedCharacters = "";
            this.textBoxExZone.RegularExpresion = "";
            this.textBoxExZone.Size = new System.Drawing.Size(39, 20);
            this.textBoxExZone.TabIndex = 47;
            // 
            // textBoxExStreet
            // 
            this.textBoxExStreet.AllowsLetters = true;
            this.textBoxExStreet.AllowsNumbers = true;
            this.textBoxExStreet.AllowsPunctuation = true;
            this.textBoxExStreet.AllowsSeparators = true;
            this.textBoxExStreet.AllowsSymbols = true;
            this.textBoxExStreet.AllowsWhiteSpaces = true;
            this.textBoxExStreet.Enabled = false;
            this.textBoxExStreet.ExtraAllowedChars = "";
            this.textBoxExStreet.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExStreet.Location = new System.Drawing.Point(417, 96);
            this.textBoxExStreet.MaxLength = 40;
            this.textBoxExStreet.Name = "textBoxExStreet";
            this.textBoxExStreet.NonAllowedCharacters = "";
            this.textBoxExStreet.RegularExpresion = "";
            this.textBoxExStreet.Size = new System.Drawing.Size(39, 20);
            this.textBoxExStreet.TabIndex = 48;

            // 
            // textBoxExAlarmRule
            // 
            this.textBoxExAlarmRule.AllowsLetters = true;
            this.textBoxExAlarmRule.AllowsNumbers = true;
            this.textBoxExAlarmRule.AllowsPunctuation = true;
            this.textBoxExAlarmRule.AllowsSeparators = true;
            this.textBoxExAlarmRule.AllowsSymbols = true;
            this.textBoxExAlarmRule.AllowsWhiteSpaces = true;
            this.textBoxExAlarmRule.Enabled = false;
            this.textBoxExAlarmRule.ExtraAllowedChars = "";
            this.textBoxExAlarmRule.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExAlarmRule.Location = new System.Drawing.Point(417, 126);
            this.textBoxExAlarmRule.MaxLength = 40;
            this.textBoxExAlarmRule.Name = "textBoxExAlarmRule";
            this.textBoxExAlarmRule.NonAllowedCharacters = "";
            this.textBoxExAlarmRule.RegularExpresion = "";
            this.textBoxExAlarmRule.Size = new System.Drawing.Size(39, 20);
            this.textBoxExAlarmRule.TabIndex = 50;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(252)))), ((int)(((byte)(226)))));
            this.layoutControlGroup1.AppearanceGroup.Options.UseBackColor = true;
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemZone,
            this.layoutControlItemStructType,
            this.layoutControlItemZoneName,
            this.layoutControlItemStreet,
            this.layoutControlItem9,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItemStruct,
            this.layoutControlItemCamera,
            this.layoutControlItemCity,
            this.layoutControlItemTown,
            this.layoutControlItemAlarmDate,
            this.layoutControlItemAlarmRule
            });

            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(462, 134); // 462, 134
            this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItemZone
            // 
            this.layoutControlItemZone.Control = this.comboBoxExCcvtZone;
            this.layoutControlItemZone.CustomizationFormText = "layoutControlItemZone";
            this.layoutControlItemZone.Location = new System.Drawing.Point(51, 0);
            this.layoutControlItemZone.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItemZone.MinSize = new System.Drawing.Size(118, 30);
            this.layoutControlItemZone.Name = "layoutControlItemZone";
            this.layoutControlItemZone.Size = new System.Drawing.Size(215, 30);
            this.layoutControlItemZone.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemZone.Text = "layoutControlItemZone";
            this.layoutControlItemZone.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemZone.TextSize = new System.Drawing.Size(140, 20);
            // 
            // layoutControlItemStructType
            // 
            this.layoutControlItemStructType.Control = this.comboBoxExStructType;
            this.layoutControlItemStructType.CustomizationFormText = "layoutControlItemStructType";
            this.layoutControlItemStructType.Location = new System.Drawing.Point(51, 30);
            this.layoutControlItemStructType.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItemStructType.MinSize = new System.Drawing.Size(129, 30);
            this.layoutControlItemStructType.Name = "layoutControlItemStructType";
            this.layoutControlItemStructType.Size = new System.Drawing.Size(215, 30);
            this.layoutControlItemStructType.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemStructType.Text = "layoutControlItemStructType";
            this.layoutControlItemStructType.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemStructType.TextSize = new System.Drawing.Size(140, 20);
            // 
            // layoutControlItemZoneName
            // 
            this.layoutControlItemZoneName.Control = this.textBoxExZone;
            this.layoutControlItemZoneName.CustomizationFormText = "layoutControlItemZoneName";
            this.layoutControlItemZoneName.Location = new System.Drawing.Point(266, 60);
            this.layoutControlItemZoneName.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItemZoneName.MinSize = new System.Drawing.Size(100, 30);
            this.layoutControlItemZoneName.Name = "layoutControlItemZoneName";
            this.layoutControlItemZoneName.Size = new System.Drawing.Size(194, 30);
            this.layoutControlItemZoneName.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemZoneName.Text = "layoutControlItemZoneName";
            this.layoutControlItemZoneName.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemZoneName.TextSize = new System.Drawing.Size(140, 20);
            // 
            // layoutControlItemStreet
            // 
            this.layoutControlItemStreet.Control = this.textBoxExStreet;
            this.layoutControlItemStreet.CustomizationFormText = "layoutControlItemStreet";
            this.layoutControlItemStreet.Location = new System.Drawing.Point(266, 90);
            this.layoutControlItemStreet.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItemStreet.MinSize = new System.Drawing.Size(100, 30);
            this.layoutControlItemStreet.Name = "layoutControlItemStreet";
            this.layoutControlItemStreet.Size = new System.Drawing.Size(194, 30);
            this.layoutControlItemStreet.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemStreet.Text = "layoutControlItemStreet";
            this.layoutControlItemStreet.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemStreet.TextSize = new System.Drawing.Size(140, 20);
            // 
            // layoutControlItemAlarmRule
            // 
            this.layoutControlItemAlarmRule.Control = this.textBoxExAlarmRule;
            this.layoutControlItemAlarmRule.CustomizationFormText = "layoutControlItemAlarmRule";
            this.layoutControlItemAlarmRule.Location = new System.Drawing.Point(266, 110); // 51,240
            this.layoutControlItemAlarmRule.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItemAlarmRule.MinSize = new System.Drawing.Size(100, 30);
            this.layoutControlItemAlarmRule.Name = "layoutControlItemAlarmRule";
            this.layoutControlItemAlarmRule.Size = new System.Drawing.Size(194, 30); //215,30
            this.layoutControlItemAlarmRule.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemAlarmRule.Text = "layoutControlItemAlarmRule";
            this.layoutControlItemAlarmRule.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemAlarmRule.TextSize = new System.Drawing.Size(140, 20);

            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.pictureBoxNumber;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(51, 51);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(51, 51);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(51, 51);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Text = "layoutControlItem9";
            this.layoutControlItem9.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextToControlDistance = 0;
            this.layoutControlItem9.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 51);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(51, 169);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AppearanceItemCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.emptySpaceItem2.AppearanceItemCaption.Options.UseBackColor = true;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 120);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(460, 12);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItemStruct
            // 
            this.layoutControlItemStruct.Control = this.comboBoxExStruct;
            this.layoutControlItemStruct.CustomizationFormText = "layoutControlItemStruct";
            this.layoutControlItemStruct.Location = new System.Drawing.Point(51, 60);
            this.layoutControlItemStruct.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItemStruct.MinSize = new System.Drawing.Size(100, 30);
            this.layoutControlItemStruct.Name = "layoutControlItemStruct";
            this.layoutControlItemStruct.Size = new System.Drawing.Size(215, 30);
            this.layoutControlItemStruct.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemStruct.Text = "layoutControlItemStruct";
            this.layoutControlItemStruct.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemStruct.TextSize = new System.Drawing.Size(140, 20);
            // 
            // layoutControlItemCamera
            // 
            this.layoutControlItemCamera.Control = this.comboBoxExCamera;
            this.layoutControlItemCamera.CustomizationFormText = "layoutControlItemCamera";
            this.layoutControlItemCamera.Location = new System.Drawing.Point(51, 90);
            this.layoutControlItemCamera.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItemCamera.MinSize = new System.Drawing.Size(100, 30);
            this.layoutControlItemCamera.Name = "layoutControlItemCamera";
            this.layoutControlItemCamera.Size = new System.Drawing.Size(215, 30);
            this.layoutControlItemCamera.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemCamera.Text = "layoutControlItemCamera";
            this.layoutControlItemCamera.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemCamera.TextSize = new System.Drawing.Size(140, 20);
            //       
            // layoutControlItemAlarmDate
            //

            this.layoutControlItemAlarmDate.Control = this.textBoxExAlarmDate;
            this.layoutControlItemAlarmDate.CustomizationFormText = "layoutControlItemAlarmDate";
            this.layoutControlItemAlarmDate.Location = new System.Drawing.Point(51, 110); 
            this.layoutControlItemAlarmDate.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItemAlarmDate.MinSize = new System.Drawing.Size(100, 30);
            this.layoutControlItemAlarmDate.Name = "layoutControlItemAlarmDate";
            this.layoutControlItemAlarmDate.Size = new System.Drawing.Size(215, 30);
            this.layoutControlItemAlarmDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemAlarmDate.Text = "layoutControlItemAlarmDate";
            this.layoutControlItemAlarmDate.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemAlarmDate.TextSize = new System.Drawing.Size(140, 20);
            // 
            // layoutControlItemCity
            // 
            this.layoutControlItemCity.Control = this.textBoxExCity;
            this.layoutControlItemCity.CustomizationFormText = "layoutControlItemCity";
            this.layoutControlItemCity.Location = new System.Drawing.Point(266, 0);
            this.layoutControlItemCity.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItemCity.MinSize = new System.Drawing.Size(100, 30);
            this.layoutControlItemCity.Name = "layoutControlItemCity";
            this.layoutControlItemCity.Size = new System.Drawing.Size(194, 30);
            this.layoutControlItemCity.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemCity.Text = "layoutControlItemCity";
            this.layoutControlItemCity.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemCity.TextSize = new System.Drawing.Size(140, 20);
            // 
            // layoutControlItemTown
            // 
            this.layoutControlItemTown.Control = this.textBoxExTown;
            this.layoutControlItemTown.CustomizationFormText = "layoutControlItemTown";
            this.layoutControlItemTown.Location = new System.Drawing.Point(266, 30);
            this.layoutControlItemTown.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItemTown.MinSize = new System.Drawing.Size(100, 30);
            this.layoutControlItemTown.Name = "layoutControlItemTown";
            this.layoutControlItemTown.Size = new System.Drawing.Size(194, 30);
            this.layoutControlItemTown.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemTown.Text = "layoutControlItemTown";
            this.layoutControlItemTown.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemTown.TextSize = new System.Drawing.Size(140, 20);
            // 
            // CctvCallInformationControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupControlBody);
            this.MinimumSize = new System.Drawing.Size(466, 155);
            this.Name = "CctvCallInformationControl";
            this.Size = new System.Drawing.Size(466, 155);
            ((System.ComponentModel.ISupportInitialize)(this.groupControlBody)).EndInit();
            this.groupControlBody.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemZone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemStructType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemZoneName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemStreet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemStruct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCamera)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTown)).EndInit();
            //Analytical Video Controls Associated
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAlarmDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAlarmRule)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TextBoxEx textBoxExZone;
        private TextBoxEx textBoxExStreet;
		private TextBoxEx textBoxExCity;
        private TextBoxEx textBoxExTown;
        //Analytical Video Controls
        private TextBoxEx textBoxExAlarmDate;
        private TextBoxEx textBoxExAlarmRule;

        private ComboBoxEx comboBoxExCamera;
        private ComboBoxEx comboBoxExStruct;
        private ComboBoxEx comboBoxExCcvtZone;
        private ComboBoxEx comboBoxExStructType;
		private DevExpress.XtraEditors.GroupControl groupControlBody;
		private DevExpress.XtraEditors.PictureEdit pictureBoxNumber;
		private DevExpress.XtraLayout.LayoutControl layoutControl1;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemZone;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTown;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemStructType;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemCity;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemCamera;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemStruct;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemZoneName;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemStreet;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        // Analytical Video Controls Associated
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemAlarmDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemAlarmRule;
    }
}
