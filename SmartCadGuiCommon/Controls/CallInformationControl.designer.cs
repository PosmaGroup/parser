using SmartCadControls.Controls;

namespace SmartCadGuiCommon.Controls
{
    partial class CallInformationControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxExLineOwnerName = new TextBoxEx();
            this.textBoxExCallerPhoneNumber = new TextBoxEx();
            this.checkBoxAnomimous = new CheckBoxEx2();
            this.textBoxExZone = new TextBoxEx();
            this.textBoxExStreet = new TextBoxEx();
            this.textBoxExReference = new TextBoxEx();
            this.textBoxExMore = new TextBoxEx();
            this.textBoxExLinePhoneNumber = new TextBoxEx();
            this.groupControlBody = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.pictureBoxNumber = new DevExpress.XtraEditors.PictureEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemLinePhoneNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemCallerPhoneNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemLineOwnerNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemZone = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemStreet = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemAnonimous = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemReference = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemMore = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlBody)).BeginInit();
            this.groupControlBody.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLinePhoneNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCallerPhoneNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLineOwnerNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemZone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemStreet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAnonimous)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemReference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemMore)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxExLineOwnerName
            // 
            this.textBoxExLineOwnerName.AllowsLetters = true;
            this.textBoxExLineOwnerName.AllowsNumbers = false;
            this.textBoxExLineOwnerName.AllowsPunctuation = false;
            this.textBoxExLineOwnerName.AllowsSeparators = true;
            this.textBoxExLineOwnerName.AllowsSymbols = false;
            this.textBoxExLineOwnerName.AllowsWhiteSpaces = true;
            this.textBoxExLineOwnerName.Enabled = false;
            this.textBoxExLineOwnerName.ExtraAllowedChars = ".-\'";
            this.textBoxExLineOwnerName.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExLineOwnerName.Location = new System.Drawing.Point(157, 50);
            this.textBoxExLineOwnerName.MaxLength = 40;
            this.textBoxExLineOwnerName.Name = "textBoxExLineOwnerName";
            this.textBoxExLineOwnerName.NonAllowedCharacters = "";
            this.textBoxExLineOwnerName.RegularExpresion = "";
            this.textBoxExLineOwnerName.Size = new System.Drawing.Size(88, 20);
            this.textBoxExLineOwnerName.TabIndex = 2;
            this.textBoxExLineOwnerName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxExLineOwnerName_KeyPress);
            // 
            // textBoxExCallerPhoneNumber
            // 
            this.textBoxExCallerPhoneNumber.AllowsLetters = false;
            this.textBoxExCallerPhoneNumber.AllowsNumbers = true;
            this.textBoxExCallerPhoneNumber.AllowsPunctuation = false;
            this.textBoxExCallerPhoneNumber.AllowsSeparators = false;
            this.textBoxExCallerPhoneNumber.AllowsSymbols = false;
            this.textBoxExCallerPhoneNumber.AllowsWhiteSpaces = false;
            this.textBoxExCallerPhoneNumber.Enabled = false;
            this.textBoxExCallerPhoneNumber.ExtraAllowedChars = "/-x()X";
            this.textBoxExCallerPhoneNumber.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExCallerPhoneNumber.Location = new System.Drawing.Point(157, 26);
            this.textBoxExCallerPhoneNumber.MaxLength = 17;
            this.textBoxExCallerPhoneNumber.Name = "textBoxExCallerPhoneNumber";
            this.textBoxExCallerPhoneNumber.NonAllowedCharacters = "";
            this.textBoxExCallerPhoneNumber.RegularExpresion = "";
            this.textBoxExCallerPhoneNumber.Size = new System.Drawing.Size(88, 20);
            this.textBoxExCallerPhoneNumber.TabIndex = 1;
            this.textBoxExCallerPhoneNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxExCallerPhoneNumber_KeyPress);
            // 
            // checkBoxAnomimous
            // 
            this.checkBoxAnomimous.BackColor = System.Drawing.Color.Transparent;
            this.checkBoxAnomimous.Enabled = false;
            this.checkBoxAnomimous.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxAnomimous.Location = new System.Drawing.Point(259, 50);
            this.checkBoxAnomimous.Name = "checkBoxAnomimous";
            this.checkBoxAnomimous.NotVisibleText = "";
            this.checkBoxAnomimous.Size = new System.Drawing.Size(20, 20);
            this.checkBoxAnomimous.TabIndex = 3;
            this.checkBoxAnomimous.UseVisualStyleBackColor = false;
            this.checkBoxAnomimous.CheckedChanged += new System.EventHandler(this.checkBoxAnomimous_CheckedChanged);
            // 
            // textBoxExZone
            // 
            this.textBoxExZone.AllowsLetters = true;
            this.textBoxExZone.AllowsNumbers = true;
            this.textBoxExZone.AllowsPunctuation = true;
            this.textBoxExZone.AllowsSeparators = true;
            this.textBoxExZone.AllowsSymbols = true;
            this.textBoxExZone.AllowsWhiteSpaces = true;
            this.textBoxExZone.Enabled = false;
            this.textBoxExZone.ExtraAllowedChars = "";
            this.textBoxExZone.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExZone.Location = new System.Drawing.Point(319, 2);
            this.textBoxExZone.MaxLength = 40;
            this.textBoxExZone.Name = "textBoxExZone";
            this.textBoxExZone.NonAllowedCharacters = "";
            this.textBoxExZone.RegularExpresion = "";
            this.textBoxExZone.Size = new System.Drawing.Size(110, 20);
            this.textBoxExZone.TabIndex = 4;
            this.textBoxExZone.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxExZone_KeyPress);
            // 
            // textBoxExStreet
            // 
            this.textBoxExStreet.AllowsLetters = true;
            this.textBoxExStreet.AllowsNumbers = true;
            this.textBoxExStreet.AllowsPunctuation = true;
            this.textBoxExStreet.AllowsSeparators = true;
            this.textBoxExStreet.AllowsSymbols = true;
            this.textBoxExStreet.AllowsWhiteSpaces = true;
            this.textBoxExStreet.Enabled = false;
            this.textBoxExStreet.ExtraAllowedChars = "";
            this.textBoxExStreet.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExStreet.Location = new System.Drawing.Point(319, 26);
            this.textBoxExStreet.MaxLength = 40;
            this.textBoxExStreet.Name = "textBoxExStreet";
            this.textBoxExStreet.NonAllowedCharacters = "";
            this.textBoxExStreet.RegularExpresion = "";
            this.textBoxExStreet.Size = new System.Drawing.Size(110, 20);
            this.textBoxExStreet.TabIndex = 5;
            this.textBoxExStreet.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxExStreet_KeyPress);
            // 
            // textBoxExReference
            // 
            this.textBoxExReference.AllowsLetters = true;
            this.textBoxExReference.AllowsNumbers = true;
            this.textBoxExReference.AllowsPunctuation = true;
            this.textBoxExReference.AllowsSeparators = true;
            this.textBoxExReference.AllowsSymbols = true;
            this.textBoxExReference.AllowsWhiteSpaces = true;
            this.textBoxExReference.Enabled = false;
            this.textBoxExReference.ExtraAllowedChars = "";
            this.textBoxExReference.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExReference.Location = new System.Drawing.Point(508, 2);
            this.textBoxExReference.MaxLength = 40;
            this.textBoxExReference.Name = "textBoxExReference";
            this.textBoxExReference.NonAllowedCharacters = "";
            this.textBoxExReference.RegularExpresion = "";
            this.textBoxExReference.Size = new System.Drawing.Size(119, 20);
            this.textBoxExReference.TabIndex = 6;
            this.textBoxExReference.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxExReference_KeyPress);
            // 
            // textBoxExMore
            // 
            this.textBoxExMore.AllowsLetters = true;
            this.textBoxExMore.AllowsNumbers = true;
            this.textBoxExMore.AllowsPunctuation = true;
            this.textBoxExMore.AllowsSeparators = true;
            this.textBoxExMore.AllowsSymbols = true;
            this.textBoxExMore.AllowsWhiteSpaces = true;
            this.textBoxExMore.Enabled = false;
            this.textBoxExMore.ExtraAllowedChars = "";
            this.textBoxExMore.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExMore.Location = new System.Drawing.Point(508, 26);
            this.textBoxExMore.MaxLength = 100;
            this.textBoxExMore.Name = "textBoxExMore";
            this.textBoxExMore.NonAllowedCharacters = "";
            this.textBoxExMore.RegularExpresion = "";
            this.textBoxExMore.Size = new System.Drawing.Size(119, 20);
            this.textBoxExMore.TabIndex = 7;
            this.textBoxExMore.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxExMore_KeyPress);
            // 
            // textBoxExLinePhoneNumber
            // 
            this.textBoxExLinePhoneNumber.AllowsLetters = false;
            this.textBoxExLinePhoneNumber.AllowsNumbers = true;
            this.textBoxExLinePhoneNumber.AllowsPunctuation = false;
            this.textBoxExLinePhoneNumber.AllowsSeparators = false;
            this.textBoxExLinePhoneNumber.AllowsSymbols = false;
            this.textBoxExLinePhoneNumber.AllowsWhiteSpaces = false;
            this.textBoxExLinePhoneNumber.Enabled = false;
            this.textBoxExLinePhoneNumber.ExtraAllowedChars = "/-x()X";
            this.textBoxExLinePhoneNumber.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExLinePhoneNumber.Location = new System.Drawing.Point(157, 2);
            this.textBoxExLinePhoneNumber.MaxLength = 17;
            this.textBoxExLinePhoneNumber.Name = "textBoxExLinePhoneNumber";
            this.textBoxExLinePhoneNumber.NonAllowedCharacters = "";
            this.textBoxExLinePhoneNumber.RegularExpresion = "";
            this.textBoxExLinePhoneNumber.Size = new System.Drawing.Size(88, 20);
            this.textBoxExLinePhoneNumber.TabIndex = 0;
            this.textBoxExLinePhoneNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxExLinePhoneNumber_KeyPress);
            // 
            // groupControlBody
            // 
            this.groupControlBody.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(94)))), ((int)(((byte)(50)))));
            this.groupControlBody.Appearance.ForeColor = System.Drawing.Color.Black;
            this.groupControlBody.Appearance.Options.UseBorderColor = true;
            this.groupControlBody.Appearance.Options.UseForeColor = true;
            this.groupControlBody.AppearanceCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(115)))), ((int)(((byte)(172)))), ((int)(((byte)(68)))));
            this.groupControlBody.AppearanceCaption.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(94)))), ((int)(((byte)(50)))));
            this.groupControlBody.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControlBody.AppearanceCaption.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.groupControlBody.AppearanceCaption.Options.UseBackColor = true;
            this.groupControlBody.AppearanceCaption.Options.UseFont = true;
            this.groupControlBody.Controls.Add(this.layoutControl1);
            this.groupControlBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControlBody.Location = new System.Drawing.Point(0, 0);
            this.groupControlBody.LookAndFeel.SkinName = "Blue";
            this.groupControlBody.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.groupControlBody.LookAndFeel.UseDefaultLookAndFeel = false;
            this.groupControlBody.Margin = new System.Windows.Forms.Padding(0);
            this.groupControlBody.Name = "groupControlBody";
            this.groupControlBody.Size = new System.Drawing.Size(633, 96);
            this.groupControlBody.TabIndex = 0;
            // 
            // layoutControl1
            // 
            this.layoutControl1.AllowCustomizationMenu = false;
            this.layoutControl1.Appearance.DisabledLayoutGroupCaption.ForeColor = System.Drawing.SystemColors.GrayText;
            this.layoutControl1.Appearance.DisabledLayoutGroupCaption.Options.UseForeColor = true;
            this.layoutControl1.Appearance.DisabledLayoutItem.ForeColor = System.Drawing.SystemColors.GrayText;
            this.layoutControl1.Appearance.DisabledLayoutItem.Options.UseForeColor = true;
            this.layoutControl1.Controls.Add(this.pictureBoxNumber);
            this.layoutControl1.Controls.Add(this.textBoxExLinePhoneNumber);
            this.layoutControl1.Controls.Add(this.textBoxExMore);
            this.layoutControl1.Controls.Add(this.textBoxExReference);
            this.layoutControl1.Controls.Add(this.textBoxExStreet);
            this.layoutControl1.Controls.Add(this.textBoxExZone);
            this.layoutControl1.Controls.Add(this.textBoxExCallerPhoneNumber);
            this.layoutControl1.Controls.Add(this.textBoxExLineOwnerName);
            this.layoutControl1.Controls.Add(this.checkBoxAnomimous);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 19);
            this.layoutControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.layoutControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsFocus.AllowFocusControlOnActivatedTabPage = true;
            this.layoutControl1.OptionsFocus.EnableAutoTabOrder = false;
            this.layoutControl1.OptionsFocus.MoveFocusDirection = DevExpress.XtraLayout.MoveFocusDirection.DownThenAcross;
            this.layoutControl1.OptionsView.ItemBorderColor = System.Drawing.Color.Transparent;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(629, 75);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // pictureBoxNumber
            // 
            this.pictureBoxNumber.Location = new System.Drawing.Point(2, 2);
            this.pictureBoxNumber.Name = "pictureBoxNumber";
            this.pictureBoxNumber.Size = new System.Drawing.Size(41, 41);
            this.pictureBoxNumber.StyleController = this.layoutControl1;
            this.pictureBoxNumber.TabIndex = 8;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(252)))), ((int)(((byte)(226)))));
            this.layoutControlGroup1.AppearanceGroup.Options.UseBackColor = true;
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemLinePhoneNumber,
            this.layoutControlItemCallerPhoneNumber,
            this.layoutControlItemLineOwnerNumber,
            this.layoutControlItemZone,
            this.layoutControlItemStreet,
            this.layoutControlItemAnonimous,
            this.layoutControlItemReference,
            this.layoutControlItemMore,
            this.layoutControlItem1,
            this.emptySpaceItem4,
            this.emptySpaceItem3,
            this.emptySpaceItem2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.OptionsItemText.TextToControlDistance = 0;
            //this.layoutControlGroup1.Size = new System.Drawing.Size(629, 75);
            this.layoutControlGroup1.Size = new System.Drawing.Size(629, 100);
            this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItemLinePhoneNumber
            // 
            this.layoutControlItemLinePhoneNumber.Control = this.textBoxExLinePhoneNumber;
            this.layoutControlItemLinePhoneNumber.CustomizationFormText = "layoutControlItemLinePhoneNumber";
            this.layoutControlItemLinePhoneNumber.Location = new System.Drawing.Point(55, 0);
            this.layoutControlItemLinePhoneNumber.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItemLinePhoneNumber.MinSize = new System.Drawing.Size(118, 24);
            this.layoutControlItemLinePhoneNumber.Name = "layoutControlItemLinePhoneNumber";
            this.layoutControlItemLinePhoneNumber.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItemLinePhoneNumber.Size = new System.Drawing.Size(192, 24);
            this.layoutControlItemLinePhoneNumber.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemLinePhoneNumber.Text = "LinePhoneNumber";
            this.layoutControlItemLinePhoneNumber.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItemLinePhoneNumber.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemLinePhoneNumber.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItemLinePhoneNumber.TextToControlDistance = 0;
            // 
            // layoutControlItemCallerPhoneNumber
            // 
            this.layoutControlItemCallerPhoneNumber.Control = this.textBoxExCallerPhoneNumber;
            this.layoutControlItemCallerPhoneNumber.CustomizationFormText = "layoutControlItemCallerPhoneNumber";
            this.layoutControlItemCallerPhoneNumber.Location = new System.Drawing.Point(55, 24);
            this.layoutControlItemCallerPhoneNumber.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItemCallerPhoneNumber.MinSize = new System.Drawing.Size(118, 24);
            this.layoutControlItemCallerPhoneNumber.Name = "layoutControlItemCallerPhoneNumber";
            this.layoutControlItemCallerPhoneNumber.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItemCallerPhoneNumber.Size = new System.Drawing.Size(192, 24);
            this.layoutControlItemCallerPhoneNumber.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemCallerPhoneNumber.Text = "CallerPhoneNumber";
            this.layoutControlItemCallerPhoneNumber.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItemCallerPhoneNumber.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemCallerPhoneNumber.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItemCallerPhoneNumber.TextToControlDistance = 0;
            // 
            // layoutControlItemLineOwnerNumber
            // 
            this.layoutControlItemLineOwnerNumber.Control = this.textBoxExLineOwnerName;
            this.layoutControlItemLineOwnerNumber.CustomizationFormText = "layoutControlItemLineOwnerNumber";
            this.layoutControlItemLineOwnerNumber.Location = new System.Drawing.Point(55, 48);
            this.layoutControlItemLineOwnerNumber.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItemLineOwnerNumber.MinSize = new System.Drawing.Size(118, 24);
            this.layoutControlItemLineOwnerNumber.Name = "layoutControlItemLineOwnerNumber";
            this.layoutControlItemLineOwnerNumber.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItemLineOwnerNumber.Size = new System.Drawing.Size(192, 27);
            this.layoutControlItemLineOwnerNumber.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemLineOwnerNumber.Text = "LineOwnerNumber";
            this.layoutControlItemLineOwnerNumber.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItemLineOwnerNumber.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemLineOwnerNumber.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItemLineOwnerNumber.TextToControlDistance = 0;
            // 
            // layoutControlItemZone
            // 
            this.layoutControlItemZone.Control = this.textBoxExZone;
            this.layoutControlItemZone.CustomizationFormText = "layoutControlItemZone";
            this.layoutControlItemZone.Location = new System.Drawing.Point(247, 0);
            this.layoutControlItemZone.Name = "layoutControlItemZone";
            this.layoutControlItemZone.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItemZone.Size = new System.Drawing.Size(184, 24);
            this.layoutControlItemZone.Spacing = new DevExpress.XtraLayout.Utils.Padding(10, 0, 0, 0);
            this.layoutControlItemZone.Text = "Zone";
            this.layoutControlItemZone.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItemZone.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemZone.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItemZone.TextToControlDistance = 0;
            // 
            // layoutControlItemStreet
            // 
            this.layoutControlItemStreet.Control = this.textBoxExStreet;
            this.layoutControlItemStreet.CustomizationFormText = "layoutControlItemStreet";
            this.layoutControlItemStreet.Location = new System.Drawing.Point(247, 24);
            this.layoutControlItemStreet.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItemStreet.MinSize = new System.Drawing.Size(128, 24);
            this.layoutControlItemStreet.Name = "layoutControlItemStreet";
            this.layoutControlItemStreet.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItemStreet.Size = new System.Drawing.Size(184, 24);
            this.layoutControlItemStreet.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemStreet.Spacing = new DevExpress.XtraLayout.Utils.Padding(10, 0, 0, 0);
            this.layoutControlItemStreet.Text = "Street";
            this.layoutControlItemStreet.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItemStreet.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemStreet.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItemStreet.TextToControlDistance = 0;
            // 
            // layoutControlItemAnonimous
            // 
            this.layoutControlItemAnonimous.Control = this.checkBoxAnomimous;
            this.layoutControlItemAnonimous.CustomizationFormText = "layoutControlItemAnonimous";
            this.layoutControlItemAnonimous.Location = new System.Drawing.Point(247, 48);
            this.layoutControlItemAnonimous.MaxSize = new System.Drawing.Size(104, 24);
            this.layoutControlItemAnonimous.MinSize = new System.Drawing.Size(104, 24);
            this.layoutControlItemAnonimous.Name = "layoutControlItemAnonimous";
            this.layoutControlItemAnonimous.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItemAnonimous.Size = new System.Drawing.Size(104, 27);
            this.layoutControlItemAnonimous.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemAnonimous.Spacing = new DevExpress.XtraLayout.Utils.Padding(10, 0, 0, 0);
            this.layoutControlItemAnonimous.Text = "Anonimous";
            this.layoutControlItemAnonimous.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItemAnonimous.TextLocation = DevExpress.Utils.Locations.Right;
            this.layoutControlItemAnonimous.TextSize = new System.Drawing.Size(65, 20);
            this.layoutControlItemAnonimous.TextToControlDistance = 5;
            this.layoutControlItemAnonimous.Click += new System.EventHandler(this.layoutControlItemAnonimous_Click);
            // 
            // layoutControlItemReference
            // 
            this.layoutControlItemReference.Control = this.textBoxExReference;
            this.layoutControlItemReference.CustomizationFormText = "layoutControlItemReference";
            this.layoutControlItemReference.Location = new System.Drawing.Point(431, 0);
            this.layoutControlItemReference.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItemReference.MinSize = new System.Drawing.Size(128, 24);
            this.layoutControlItemReference.Name = "layoutControlItemReference";
            this.layoutControlItemReference.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItemReference.Size = new System.Drawing.Size(198, 24);
            this.layoutControlItemReference.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemReference.Spacing = new DevExpress.XtraLayout.Utils.Padding(10, 0, 0, 0);
            this.layoutControlItemReference.Text = "Reference";
            this.layoutControlItemReference.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItemReference.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemReference.TextSize = new System.Drawing.Size(65, 20);
            this.layoutControlItemReference.TextToControlDistance = 0;
            // 
            // layoutControlItemMore
            // 
            this.layoutControlItemMore.Control = this.textBoxExMore;
            this.layoutControlItemMore.CustomizationFormText = "layoutControlItemMore";
            this.layoutControlItemMore.Location = new System.Drawing.Point(431, 24);
            this.layoutControlItemMore.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItemMore.MinSize = new System.Drawing.Size(128, 24);
            this.layoutControlItemMore.Name = "layoutControlItemMore";
            this.layoutControlItemMore.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItemMore.Size = new System.Drawing.Size(198, 24);
            this.layoutControlItemMore.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemMore.Spacing = new DevExpress.XtraLayout.Utils.Padding(10, 0, 0, 0);
            this.layoutControlItemMore.Text = "More";
            this.layoutControlItemMore.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItemMore.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemMore.TextSize = new System.Drawing.Size(65, 20);
            this.layoutControlItemMore.TextToControlDistance = 0;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.pictureBoxNumber;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(45, 45);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(45, 45);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem1.Size = new System.Drawing.Size(45, 45);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 45);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(45, 30);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(351, 48);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(278, 27);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(45, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(10, 75);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // CallInformationControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupControlBody);
            this.Name = "CallInformationControl";
            this.Size = new System.Drawing.Size(633, 96);
            this.Load += new System.EventHandler(this.CallInformationControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControlBody)).EndInit();
            this.groupControlBody.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLinePhoneNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCallerPhoneNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLineOwnerNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemZone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemStreet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAnonimous)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemReference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemMore)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

		private DevExpress.XtraEditors.GroupControl groupControlBody;
		private TextBoxEx textBoxExLineOwnerName;
        private TextBoxEx textBoxExCallerPhoneNumber;
        private CheckBoxEx2 checkBoxAnomimous;
        private TextBoxEx textBoxExZone;
        private TextBoxEx textBoxExStreet;
        private TextBoxEx textBoxExReference;
        private TextBoxEx textBoxExMore;
        private TextBoxEx textBoxExLinePhoneNumber;
		private DevExpress.XtraLayout.LayoutControl layoutControl1;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemLinePhoneNumber;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemCallerPhoneNumber;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemLineOwnerNumber;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemZone;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemStreet;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemAnonimous;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemReference;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemMore;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
		private DevExpress.XtraEditors.PictureEdit pictureBoxNumber;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
    }
}
