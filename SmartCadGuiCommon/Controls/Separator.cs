using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SmartCadGuiCommon.Controls
{
	/// <summary>
	/// Summary description for Separator.
	/// </summary>
	[Designer(typeof (SeparatorControlDesigner))]
	public class Separator : UserControl
	{
		private Color transparentColor = Color.White;

		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private Container components = null;

		public Separator()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitForm call
			base.TabStop = false;
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			// 
			// Separator
			// 
			this.BackColor = System.Drawing.Color.Silver;
			this.Name = "Separator";
			this.Size = new System.Drawing.Size(368, 2);
			this.Paint += new System.Windows.Forms.PaintEventHandler(this.Separator_Paint);

		}

		#endregion

		private void Separator_Paint(object sender, PaintEventArgs e)
		{
			SolidBrush brush = new SolidBrush(BackColor);
			Pen pencil = new Pen(brush, 1);
			e.Graphics.DrawLine(pencil, 0, 0, Width, 0);
			pencil.Color = transparentColor;
			e.Graphics.DrawLine(pencil, 0, 1, Width, 1);
		}

		public Color TransparentColor
		{
			get { return transparentColor; }
			set { transparentColor = value; }
		}

		new public Size Size
		{
			get { return base.Size; }
			set { base.Size = new Size(value.Width, 2); }
		}

		[Browsable(false),
			EditorBrowsable(EditorBrowsableState.Never),
			DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		new public bool TabStop
		{
			get { return base.TabStop; }
		}
	}
}