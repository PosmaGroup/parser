﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraLayout;
using DevExpress.XtraLayout.Dragging;
using DevExpress.XtraLayout.Customization;
using SmartCadControls.Controls;

namespace SmartCadGuiCommon.Controls
{
    public partial class DragDropTemplateControl : DragDropLayoutControl
    {
        public CameraPanelControl SelectedCamera { get; set; }
        public DragDropTemplateControl()
            :base(true)
        {
        }
    }
}
