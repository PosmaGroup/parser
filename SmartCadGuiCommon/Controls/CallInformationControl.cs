using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Smartmatic.SmartCad.Service;
using SmartCadControls.Controls;
using SmartCadCore.Common;
using SmartCadCore.Core;
using SmartCadCore.Enums;
using SmartCadCore.ClientData;

namespace SmartCadGuiCommon.Controls
{
    public partial class CallInformationControl : HeaderPanelControl
    {
        #region Fields

        public DefaultFrontClientFormDevX defaultFrontClientFormDevX;

        private AddressClientData addressData;

        public event EventHandler<EventArgs> TextUpdated;
        public event EventHandler<EventArgs> CallPickedUp;
        public event EventHandler<EventArgs> CallHangedUp;
        protected CallRingingEventArgs incomingCallEventArgs;
        private bool isAnonimousCallActivated;
        private DateTime receivedCallTime;
        private FrontClientStateEnum frontClientState;

        #endregion

		#region Constructors

		public CallInformationControl()
        {
            InitializeComponent();
			LoadLanguage();
		}

		#endregion

		#region Properties

        public FrontClientStateEnum FrontClientState
        {
            get
            {
                return this.frontClientState;
            }
            set
            {
                this.frontClientState = value;
                if (value == FrontClientStateEnum.WaitingForIncident)
                    SetFrontClientWaitingForCallState();
                else if (value == FrontClientStateEnum.RegisteringIncident)
                    SetFrontClientRegisteringCallState();
                else if (value == FrontClientStateEnum.UpdatingIncident)
                    SetFrontClientUpdatingCallState();
            }
        }

        public string LineTelephoneNumber
        {
            get
            {
                return textBoxExLinePhoneNumber.Text;
            }
            set
            {
                textBoxExLinePhoneNumber.Text = value;
            }
        }

        public string CallerNumber
        {
            get
            {
                return this.textBoxExCallerPhoneNumber.Text;
            }
            set
            {
                this.textBoxExCallerPhoneNumber.Text = value;
            }
        }

        public string LineName
        {
            get
            {
                return textBoxExLineOwnerName.Text;
            }
            set
            {
                textBoxExLineOwnerName.Text = value;
            }
        }

        [RefreshProperties(RefreshProperties.All),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
        Browsable(false)]
        public AddressClientData CallerAddress
        {
            get
            {
                if (null != this.addressData)
                {
                    this.addressData.Zone = this.textBoxExZone.Text;
                    this.addressData.Street = this.textBoxExStreet.Text;
                    this.addressData.Reference = this.textBoxExReference.Text;
                    this.addressData.More = this.textBoxExMore.Text;
                }
                else
                {
                    this.addressData = new AddressClientData(
                        this.textBoxExZone.Text,
                        this.textBoxExStreet.Text,
                        this.textBoxExReference.Text,
                        this.textBoxExMore.Text,
                        false);
                }

                return addressData;
            }
            set
            {
                if (value != null)
                    addressData = new AddressClientData(value.Zone, value.Street, value.Reference, value.More, value.IsSynchronized);
                if (null != this.addressData)
                {
                    this.textBoxExZone.Text = this.addressData.Zone;
                    this.textBoxExStreet.Text = this.addressData.Street;
                    this.textBoxExReference.Text = this.addressData.Reference;
                    this.textBoxExMore.Text = this.addressData.More;
                }
            }
        }

        public bool Anonymous
        {
            get
            {
                return this.checkBoxAnomimous.Checked;
            }
            set
            {
                this.checkBoxAnomimous.Checked = value;
            }
        }

        public bool IsAnonimousCallActivated
        {
            get
            {
                return this.isAnonimousCallActivated;
            }
            set
            {
                this.checkBoxAnomimous.Checked = value;
            }
        }

        public DateTime ReceivedCallTime
        {
            get
            {
                return this.receivedCallTime;
            }
        }

		public override bool Active
		{
			get
			{
				return base.Active;
			}
			set
			{
				base.Active = value;

				Image img;

				img = pictureBoxNumber.Image;

				pictureBoxNumber.Image = ResourceLoader.GetImage(GetNumberImageName());

				if (img != null)
					img.Dispose();
				if (Active)
				{
                    this.pictureBoxNumber.LookAndFeel.Style = GetStyle(ActiveColor);
                    this.groupControlBody.LookAndFeel.Style = GetStyle(ActiveColor);
                    this.layoutControl1.LookAndFeel.Style = GetStyle(ActiveColor);
                                     
                }
				else
				{
               
					this.pictureBoxNumber.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Skin;
					this.groupControlBody.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Skin;
					this.layoutControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Skin;
					this.pictureBoxNumber.LookAndFeel.SkinName = InactiveColor;
					this.groupControlBody.LookAndFeel.SkinName = InactiveColor;
					this.layoutControl1.LookAndFeel.SkinName = InactiveColor;
                }
            
			}
		}
        #endregion

		private void LoadLanguage()
		{
			layoutControlItemAnonimous.Text = ResourceLoader.GetString2("Anonymous");
			layoutControlItemLinePhoneNumber.Text = ResourceLoader.GetString2("Phone") + ":";
			layoutControlItemCallerPhoneNumber.Text = ResourceLoader.GetString2("OtherPhone") + ":";
            layoutControlItemLineOwnerNumber.Text = ResourceLoader.GetString2("OwnerNumber") + ":*";
			layoutControlItemZone.Text = ResourceLoader.GetString2("ZoneSector") + ":";
			layoutControlItemStreet.Text = ResourceLoader.GetString2("Street") + ":";
            layoutControlItemReference.Text = ResourceLoader.GetString2("HouseBuilding");
			layoutControlItemMore.Text = ResourceLoader.GetString2("More") + ":";
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
		}

		private void SetFrontClientUpdatingCallState()
        {
            this.textBoxExMore.Enabled = false;
            this.textBoxExReference.Enabled = false;
            this.textBoxExStreet.Enabled = false;
            this.textBoxExZone.Enabled = false;
            this.pictureBoxNumber.Enabled = false;
            this.textBoxExCallerPhoneNumber.Enabled = false;
            this.textBoxExLineOwnerName.Enabled = false;
            this.textBoxExLinePhoneNumber.Enabled = false;
            this.checkBoxAnomimous.Enabled = false;
        }

        private void SetFrontClientRegisteringCallState()
        {
            this.textBoxExMore.Enabled = true;
            this.textBoxExReference.Enabled = true;
            this.textBoxExStreet.Enabled = true;
            this.textBoxExZone.Enabled = true;
            pictureBoxNumber.Enabled = true;
            this.textBoxExCallerPhoneNumber.Enabled = true;
            this.textBoxExLineOwnerName.Enabled = true;
            this.textBoxExLinePhoneNumber.Enabled = false;
            this.checkBoxAnomimous.Enabled = true;
        }

        private void SetFrontClientWaitingForCallState()
        {
            this.textBoxExMore.Enabled = false;
            this.textBoxExReference.Enabled = false;
            this.textBoxExStreet.Enabled = false;
            this.textBoxExZone.Enabled = false;
            pictureBoxNumber.Enabled = false;
            this.textBoxExCallerPhoneNumber.Enabled = false;
            this.textBoxExLineOwnerName.Enabled = false;
            this.textBoxExLinePhoneNumber.Enabled = false;
            this.checkBoxAnomimous.Enabled = false;
        }

        public void CleanControl()
        {
            FormUtil.InvokeRequired(this, new MethodInvoker(delegate
            {
                this.textBoxExMore.Text = "";
                this.textBoxExReference.Text = "";
                this.textBoxExStreet.Text = "";
                this.textBoxExZone.Text = "";

                defaultFrontClientFormDevX.TelephoneStatus = TelephoneStatusType.None;
                this.textBoxExCallerPhoneNumber.Text = "";
                this.textBoxExLineOwnerName.Text = "";
                this.textBoxExLineOwnerName.Tag = null;
                this.textBoxExLinePhoneNumber.Text = "";
                this.checkBoxAnomimous.Checked = false;
            }));
        }

        public void WhenIncomingCallEventArgs(CallRingingEventArgs e)
        {
            incomingCallEventArgs = (CallRingingEventArgs)e;

            defaultFrontClientFormDevX.TelephoneStatus = TelephoneStatusType.Incoming;
            
            this.receivedCallTime = ServerServiceClient.GetInstance().GetTime();
        }

        public void WhenEstablishedCallEventArgs(CallEstablishedEventArgs e)
        {
            defaultFrontClientFormDevX.TelephoneStatus = TelephoneStatusType.Active;

            if (CallPickedUp != null)
                CallPickedUp(this, EventArgs.Empty);
        }

        public void WhenEndingCallEventArgs(TelephonyActionEventArgs e)
        {
            FormUtil.InvokeRequired(textBoxExLinePhoneNumber, delegate
                {
                    textBoxExLinePhoneNumber.Enabled = false;

                });

            if (defaultFrontClientFormDevX.TelephoneStatus == TelephoneStatusType.Active)
                defaultFrontClientFormDevX.TelephoneStatus = TelephoneStatusType.Dropped;
            else if (defaultFrontClientFormDevX.TelephoneStatus == TelephoneStatusType.Incoming)
                defaultFrontClientFormDevX.TelephoneStatus = TelephoneStatusType.None;

            if (CallHangedUp != null)
                CallHangedUp(this, EventArgs.Empty);
        }

        private void checkBoxAnomimous_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxAnomimous.Checked == true)
            {
                textBoxExLineOwnerName.Enabled = false;
                textBoxExLineOwnerName.Tag = textBoxExLineOwnerName.Text;
                textBoxExLineOwnerName.Text = "";
                this.isAnonimousCallActivated = true;
                layoutControlItemLineOwnerNumber.Text = layoutControlItemLineOwnerNumber.Text.Replace("*", "");

            }
            else
            {
                textBoxExLineOwnerName.Enabled = true;
                if (textBoxExLineOwnerName.Tag != null)
                {
                    textBoxExLineOwnerName.Text = textBoxExLineOwnerName.Tag as string;
                    textBoxExLineOwnerName.Tag = null;
                }
                this.isAnonimousCallActivated = false;
                layoutControlItemLineOwnerNumber.Text = layoutControlItemLineOwnerNumber.Text + "*";

            }
        }

        public new bool Focus()
        {
            return this.textBoxExLineOwnerName.Focus();
        }

        private void textBoxExZone_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.Equals('\r'))
            {
                e.Handled = true;
            }
        }

        private void textBoxExStreet_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.Equals('\r'))
            {
                e.Handled = true;
            }
        }

        private void textBoxExReference_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.Equals('\r'))
            {
                e.Handled = true;
            }
        }

        private void textBoxExMore_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.Equals('\r'))
            {
                e.Handled = true;
            }
        }

        private void textBoxExLinePhoneNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.Equals('\r'))
            {
                e.Handled = true;
            }
        }

        private void textBoxExCallerPhoneNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.Equals('\r'))
            {
                e.Handled = true;
            }
        }

        private void textBoxExLineOwnerName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.Equals('\r'))
            {
                e.Handled = true;
            }
        }

        private void CallInformationControl_Load(object sender, EventArgs e)
        {
            this.groupControlBody.Text = ResourceLoader.GetString2("CallId");
            textBoxExCallerPhoneNumber.TextChanged += new EventHandler(t_TextChanged);        
            textBoxExLineOwnerName.TextChanged += new EventHandler(t_TextChanged);
            textBoxExZone.TextChanged += new EventHandler(t_TextChanged);
            textBoxExStreet.TextChanged += new EventHandler(t_TextChanged);
            textBoxExReference.TextChanged += new EventHandler(t_TextChanged);
            textBoxExMore.TextChanged += new EventHandler(t_TextChanged);
        }

        private void layoutControlItemAnonimous_Click(object sender, EventArgs e)
        {
            ((CheckBox)layoutControlItemAnonimous.Control).Checked = !((CheckBox)layoutControlItemAnonimous.Control).Checked;
        }

        void t_TextChanged(object sender, EventArgs e)
        {
            TextBoxEx txtBox = sender as TextBoxEx;
            PhoneReportCallerClientData phoneReportCaller = new PhoneReportCallerClientData();
            AddressClientData addressData = new AddressClientData();
            if (txtBox.Name == "textBoxExCallerPhoneNumber") phoneReportCaller.AdditionalNumber = txtBox.Text;
            else if (txtBox.Name == "textBoxExLineOwnerName") phoneReportCaller.Name = txtBox.Text;
            else if (txtBox.Name == "textBoxExZone") addressData.Zone = txtBox.Text;
            else if (txtBox.Name == "textBoxExStreet") addressData.Street = txtBox.Text;
            else if (txtBox.Name == "textBoxExReference") addressData.Reference = txtBox.Text;
            else if (txtBox.Name == "textBoxExMore") addressData.More = txtBox.Text;

            if (this.TextUpdated != null)
                this.TextUpdated(this, EventArgs.Empty);

        }
    }
}
