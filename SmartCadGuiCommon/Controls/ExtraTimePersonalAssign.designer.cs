﻿using DevExpress.XtraEditors;
using SmartCadControls;
namespace SmartCadGuiCommon.Controls
{
    partial class ExtraTimePersonalAssignForm: XtraForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControlExtraTime = new DevExpress.XtraLayout.LayoutControl();
            this.selectUnSelectOperators = new SelectUnSelectControlDevx();
            this.gridControlExtras = new GridControlEx();
            this.gridViewProgramExtras = new GridViewEx();
            this.gridColumnVariationName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnOperScheduleDay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnOperScheduleDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnOperScheduleStart = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnOperScheduleEnd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.textBoxExName = new DevExpress.XtraEditors.TextEdit();
            this.ButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.gridControlDate = new GridControlEx();
            this.gridViewDate = new GridViewEx();
            this.gridColumnScheduleDay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnScheduleDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.gridColumnScheduleStart = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTimeEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit();
            this.gridColumnScheduleEnd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.buttonOk = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroupOperators = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupextrasAssigned = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupData = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemDates = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemName = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlExtraTime)).BeginInit();
            this.layoutControlExtraTime.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExtras)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewProgramExtras)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxExName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupOperators)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupextrasAssigned)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControlExtraTime
            // 
            this.layoutControlExtraTime.AllowCustomizationMenu = false;
            this.layoutControlExtraTime.Controls.Add(this.selectUnSelectOperators);
            this.layoutControlExtraTime.Controls.Add(this.gridControlExtras);
            this.layoutControlExtraTime.Controls.Add(this.textBoxExName);
            this.layoutControlExtraTime.Controls.Add(this.ButtonCancel);
            this.layoutControlExtraTime.Controls.Add(this.gridControlDate);
            this.layoutControlExtraTime.Controls.Add(this.buttonOk);
            this.layoutControlExtraTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlExtraTime.Location = new System.Drawing.Point(0, 0);
            this.layoutControlExtraTime.Name = "layoutControlExtraTime";
            this.layoutControlExtraTime.Root = this.layoutControlGroup1;
            this.layoutControlExtraTime.Size = new System.Drawing.Size(622, 716);
            this.layoutControlExtraTime.TabIndex = 25;
            this.layoutControlExtraTime.Text = "layoutControl1";
            // 
            // selectUnSelectOperators
            // 
            this.selectUnSelectOperators.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectUnSelectOperators.Appearance.Options.UseFont = true;
            this.selectUnSelectOperators.Location = new System.Drawing.Point(7, 236);
            this.selectUnSelectOperators.Name = "selectUnSelectOperators";
            this.selectUnSelectOperators.Size = new System.Drawing.Size(608, 208);
            this.selectUnSelectOperators.TabIndex = 8;
            // 
            // gridControlExtras
            // 
            this.gridControlExtras.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridControlExtras.EnableAutoFilter = false;
            this.gridControlExtras.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExtras.Location = new System.Drawing.Point(7, 478);
            this.gridControlExtras.LookAndFeel.SkinName = "Blue";
            this.gridControlExtras.MainView = this.gridViewProgramExtras;
            this.gridControlExtras.Name = "gridControlExtras";
            this.gridControlExtras.Size = new System.Drawing.Size(608, 195);
            this.gridControlExtras.TabIndex = 6;
            this.gridControlExtras.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewProgramExtras});
            this.gridControlExtras.ViewTotalRows = false;
            // 
            // gridViewProgramExtras
            // 
            this.gridViewProgramExtras.AllowFocusedRowChanged = true;
            this.gridViewProgramExtras.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewProgramExtras.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewProgramExtras.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewProgramExtras.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewProgramExtras.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewProgramExtras.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewProgramExtras.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewProgramExtras.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewProgramExtras.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnVariationName,
            this.gridColumnOperScheduleDay,
            this.gridColumnOperScheduleDate,
            this.gridColumnOperScheduleStart,
            this.gridColumnOperScheduleEnd});
            this.gridViewProgramExtras.EnablePreviewLineForFocusedRow = false;
            this.gridViewProgramExtras.GridControl = this.gridControlExtras;
            this.gridViewProgramExtras.GroupCount = 1;
            this.gridViewProgramExtras.Name = "gridViewProgramExtras";
            this.gridViewProgramExtras.OptionsCustomization.AllowFilter = false;
            this.gridViewProgramExtras.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewProgramExtras.OptionsView.ColumnAutoWidth = false;
            this.gridViewProgramExtras.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewProgramExtras.OptionsView.ShowGroupPanel = false;
            this.gridViewProgramExtras.OptionsView.ShowIndicator = false;
            this.gridViewProgramExtras.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumnVariationName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewProgramExtras.ViewTotalRows = false;
            // 
            // gridColumnVariationName
            // 
            this.gridColumnVariationName.Caption = "Nombre";
            this.gridColumnVariationName.FieldName = "Name";
            this.gridColumnVariationName.Name = "gridColumnVariationName";
            this.gridColumnVariationName.OptionsColumn.AllowEdit = false;
            this.gridColumnVariationName.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumnVariationName.Visible = true;
            this.gridColumnVariationName.VisibleIndex = 0;
            this.gridColumnVariationName.Width = 119;
            // 
            // gridColumnOperScheduleDay
            // 
            this.gridColumnOperScheduleDay.Caption = "Dia";
            this.gridColumnOperScheduleDay.Name = "gridColumnOperScheduleDay";
            this.gridColumnOperScheduleDay.Visible = true;
            this.gridColumnOperScheduleDay.VisibleIndex = 0;
            // 
            // gridColumnOperScheduleDate
            // 
            this.gridColumnOperScheduleDate.Caption = "Fecha";
            this.gridColumnOperScheduleDate.FieldName = "Date";
            this.gridColumnOperScheduleDate.Name = "gridColumnOperScheduleDate";
            this.gridColumnOperScheduleDate.OptionsColumn.AllowEdit = false;
            this.gridColumnOperScheduleDate.Visible = true;
            this.gridColumnOperScheduleDate.VisibleIndex = 1;
            this.gridColumnOperScheduleDate.Width = 67;
            // 
            // gridColumnOperScheduleStart
            // 
            this.gridColumnOperScheduleStart.Caption = "Inicio";
            this.gridColumnOperScheduleStart.FieldName = "Start";
            this.gridColumnOperScheduleStart.Name = "gridColumnOperScheduleStart";
            this.gridColumnOperScheduleStart.OptionsColumn.AllowEdit = false;
            this.gridColumnOperScheduleStart.Visible = true;
            this.gridColumnOperScheduleStart.VisibleIndex = 2;
            this.gridColumnOperScheduleStart.Width = 71;
            // 
            // gridColumnOperScheduleEnd
            // 
            this.gridColumnOperScheduleEnd.Caption = "Fin";
            this.gridColumnOperScheduleEnd.FieldName = "End";
            this.gridColumnOperScheduleEnd.Name = "gridColumnOperScheduleEnd";
            this.gridColumnOperScheduleEnd.OptionsColumn.AllowEdit = false;
            this.gridColumnOperScheduleEnd.Visible = true;
            this.gridColumnOperScheduleEnd.VisibleIndex = 3;
            this.gridColumnOperScheduleEnd.Width = 64;
            // 
            // textBoxExName
            // 
            this.textBoxExName.Enabled = false;
            this.textBoxExName.Location = new System.Drawing.Point(126, 27);
            this.textBoxExName.Name = "textBoxExName";
            this.textBoxExName.Properties.AllowFocused = false;
            this.textBoxExName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textBoxExName.Properties.Appearance.Options.UseBackColor = true;
            this.textBoxExName.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White;
            this.textBoxExName.Properties.AppearanceDisabled.BackColor2 = System.Drawing.Color.White;
            this.textBoxExName.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.textBoxExName.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.textBoxExName.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.textBoxExName.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White;
            this.textBoxExName.Properties.AppearanceReadOnly.Options.UseBackColor = true;
            this.textBoxExName.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.textBoxExName.Size = new System.Drawing.Size(489, 22);
            this.textBoxExName.StyleController = this.layoutControlExtraTime;
            this.textBoxExName.TabIndex = 0;
            this.textBoxExName.TabStop = false;
            // 
            // ButtonCancel
            // 
            this.ButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.ButtonCancel.Location = new System.Drawing.Point(538, 682);
            this.ButtonCancel.Name = "ButtonCancel";
            this.ButtonCancel.Size = new System.Drawing.Size(82, 32);
            this.ButtonCancel.StyleController = this.layoutControlExtraTime;
            this.ButtonCancel.TabIndex = 4;
            this.ButtonCancel.Text = "Cancelar";
            // 
            // gridControlDate
            // 
            this.gridControlDate.EnableAutoFilter = false;
            this.gridControlDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlDate.Location = new System.Drawing.Point(126, 53);
            this.gridControlDate.LookAndFeel.SkinName = "Blue";
            this.gridControlDate.MainView = this.gridViewDate;
            this.gridControlDate.Name = "gridControlDate";
            this.gridControlDate.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTimeEdit1,
            this.repositoryItemDateEdit1});
            this.gridControlDate.Size = new System.Drawing.Size(489, 149);
            this.gridControlDate.TabIndex = 1;
            this.gridControlDate.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewDate,
            this.gridView1});
            this.gridControlDate.ViewTotalRows = false;
            // 
            // gridViewDate
            // 
            this.gridViewDate.AllowFocusedRowChanged = true;
            this.gridViewDate.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewDate.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewDate.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewDate.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewDate.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewDate.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewDate.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewDate.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewDate.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnScheduleDay,
            this.gridColumnScheduleDate,
            this.gridColumnScheduleStart,
            this.gridColumnScheduleEnd});
            this.gridViewDate.EnablePreviewLineForFocusedRow = false;
            this.gridViewDate.GridControl = this.gridControlDate;
            this.gridViewDate.Name = "gridViewDate";
            this.gridViewDate.OptionsCustomization.AllowFilter = false;
            this.gridViewDate.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewDate.OptionsView.ColumnAutoWidth = false;
            this.gridViewDate.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewDate.OptionsView.ShowGroupPanel = false;
            this.gridViewDate.OptionsView.ShowIndicator = false;
            this.gridViewDate.ViewTotalRows = false;
            // 
            // gridColumnScheduleDay
            // 
            this.gridColumnScheduleDay.Caption = "Dia";
            this.gridColumnScheduleDay.FieldName = "DayOfWeek";
            this.gridColumnScheduleDay.Name = "gridColumnScheduleDay";
            this.gridColumnScheduleDay.OptionsColumn.AllowEdit = false;
            this.gridColumnScheduleDay.Visible = true;
            this.gridColumnScheduleDay.VisibleIndex = 0;
            this.gridColumnScheduleDay.Width = 104;
            // 
            // gridColumnScheduleDate
            // 
            this.gridColumnScheduleDate.Caption = "Fecha";
            this.gridColumnScheduleDate.ColumnEdit = this.repositoryItemDateEdit1;
            this.gridColumnScheduleDate.FieldName = "Date";
            this.gridColumnScheduleDate.Name = "gridColumnScheduleDate";
            this.gridColumnScheduleDate.OptionsColumn.AllowEdit = false;
            this.gridColumnScheduleDate.Visible = true;
            this.gridColumnScheduleDate.VisibleIndex = 1;
            this.gridColumnScheduleDate.Width = 101;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            this.repositoryItemDateEdit1.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // gridColumnScheduleStart
            // 
            this.gridColumnScheduleStart.Caption = "Inicio";
            this.gridColumnScheduleStart.ColumnEdit = this.repositoryItemTimeEdit1;
            this.gridColumnScheduleStart.FieldName = "BeginTime";
            this.gridColumnScheduleStart.Name = "gridColumnScheduleStart";
            this.gridColumnScheduleStart.OptionsColumn.AllowEdit = false;
            this.gridColumnScheduleStart.Visible = true;
            this.gridColumnScheduleStart.VisibleIndex = 2;
            this.gridColumnScheduleStart.Width = 103;
            // 
            // repositoryItemTimeEdit1
            // 
            this.repositoryItemTimeEdit1.AutoHeight = false;
            this.repositoryItemTimeEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemTimeEdit1.Mask.EditMask = "HH:mm";
            this.repositoryItemTimeEdit1.Name = "repositoryItemTimeEdit1";
            // 
            // gridColumnScheduleEnd
            // 
            this.gridColumnScheduleEnd.Caption = "Fin";
            this.gridColumnScheduleEnd.ColumnEdit = this.repositoryItemTimeEdit1;
            this.gridColumnScheduleEnd.FieldName = "EndTime";
            this.gridColumnScheduleEnd.Name = "gridColumnScheduleEnd";
            this.gridColumnScheduleEnd.OptionsColumn.AllowEdit = false;
            this.gridColumnScheduleEnd.Visible = true;
            this.gridColumnScheduleEnd.VisibleIndex = 3;
            this.gridColumnScheduleEnd.Width = 101;
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gridControlDate;
            this.gridView1.Name = "gridView1";
            // 
            // buttonOk
            // 
            this.buttonOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOk.Enabled = true;
            this.buttonOk.Location = new System.Drawing.Point(452, 682);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(82, 32);
            this.buttonOk.StyleController = this.layoutControlExtraTime;
            this.buttonOk.TabIndex = 3;
            this.buttonOk.Text = "Accept";
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupOperators,
            this.layoutControlGroupextrasAssigned,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlGroupData,
            this.emptySpaceItem3});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(622, 716);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroupOperators
            // 
            this.layoutControlGroupOperators.CustomizationFormText = "layoutControlGroupOperators";
            this.layoutControlGroupOperators.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.layoutControlGroupOperators.Location = new System.Drawing.Point(0, 209);
            this.layoutControlGroupOperators.Name = "layoutControlGroupOperators";
            this.layoutControlGroupOperators.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupOperators.Size = new System.Drawing.Size(622, 242);
            this.layoutControlGroupOperators.Text = "layoutControlGroupOperators";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.selectUnSelectOperators;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(612, 212);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlGroupextrasAssigned
            // 
            this.layoutControlGroupextrasAssigned.CustomizationFormText = "layoutControlGroupextrasAssigned";
            this.layoutControlGroupextrasAssigned.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem8});
            this.layoutControlGroupextrasAssigned.Location = new System.Drawing.Point(0, 451);
            this.layoutControlGroupextrasAssigned.Name = "layoutControlGroupextrasAssigned";
            this.layoutControlGroupextrasAssigned.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupextrasAssigned.Size = new System.Drawing.Size(622, 229);
            this.layoutControlGroupextrasAssigned.Text = "layoutControlGroupextrasAssigned";
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.gridControlExtras;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(612, 199);
            this.layoutControlItem8.Text = "layoutControlItem8";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.buttonOk;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(450, 680);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Text = "layoutControlItem9";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextToControlDistance = 0;
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.ButtonCancel;
            this.layoutControlItem10.CustomizationFormText = "layoutControlItem10";
            this.layoutControlItem10.Location = new System.Drawing.Point(536, 680);
            this.layoutControlItem10.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem10.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem10.Text = "layoutControlItem10";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextToControlDistance = 0;
            this.layoutControlItem10.TextVisible = false;
            // 
            // layoutControlGroupData
            // 
            this.layoutControlGroupData.CustomizationFormText = "layoutControlGroupData";
            this.layoutControlGroupData.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemDates,
            this.layoutControlItemName});
            this.layoutControlGroupData.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupData.Name = "layoutControlGroupData";
            this.layoutControlGroupData.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupData.Size = new System.Drawing.Size(622, 209);
            this.layoutControlGroupData.Text = "layoutControlGroupData";
            // 
            // layoutControlItemDates
            // 
            this.layoutControlItemDates.Control = this.gridControlDate;
            this.layoutControlItemDates.CustomizationFormText = "layoutControlItemDates";
            this.layoutControlItemDates.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItemDates.Name = "layoutControlItemDates";
            this.layoutControlItemDates.Size = new System.Drawing.Size(612, 153);
            this.layoutControlItemDates.Text = "layoutControlItemDates";
            this.layoutControlItemDates.TextSize = new System.Drawing.Size(115, 13);
            // 
            // layoutControlItemName
            // 
            this.layoutControlItemName.Control = this.textBoxExName;
            this.layoutControlItemName.CustomizationFormText = "layoutControlItemName";
            this.layoutControlItemName.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemName.Name = "layoutControlItemName";
            this.layoutControlItemName.Size = new System.Drawing.Size(612, 26);
            this.layoutControlItemName.Text = "layoutControlItemName";
            this.layoutControlItemName.TextSize = new System.Drawing.Size(115, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 680);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(450, 36);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ExtraTimePersonalAssignForm
            // 
            this.AcceptButton = this.buttonOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.ButtonCancel;
            this.ClientSize = new System.Drawing.Size(622, 716);
            this.Controls.Add(this.layoutControlExtraTime);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(630, 750);
            this.Name = "ExtraTimePersonalAssignForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Participantes de tiempo extra";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ExtraTimePersonalAssignForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlExtraTime)).EndInit();
            this.layoutControlExtraTime.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExtras)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewProgramExtras)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxExName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupOperators)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupextrasAssigned)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private GridControlEx gridControlExtras;
        private GridViewEx gridViewProgramExtras;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnVariationName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnOperScheduleDate;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnOperScheduleStart;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnOperScheduleEnd;
        private DevExpress.XtraEditors.TextEdit textBoxExName;
        private GridControlEx gridControlDate;
        private GridViewEx gridViewDate;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnScheduleDay;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnScheduleDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnScheduleStart;
        private DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit repositoryItemTimeEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnScheduleEnd;
        private DevExpress.XtraEditors.SimpleButton ButtonCancel;
        private DevExpress.XtraEditors.SimpleButton buttonOk;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnOperScheduleDay;
        private DevExpress.XtraLayout.LayoutControl layoutControlExtraTime;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemName;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDates;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupOperators;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupextrasAssigned;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupData;
        private SelectUnSelectControlDevx selectUnSelectOperators;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;

    }
}