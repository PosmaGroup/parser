using SmartCadControls;
namespace SmartCadGuiCommon.Controls
{
    partial class IndicatorGridControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode3 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode4 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode5 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode6 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode7 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode8 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode9 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode10 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode11 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode12 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode13 = new DevExpress.XtraGrid.GridLevelNode();
            this.gridViewD01 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnD01Description = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnD01Time = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl1 = new GridControlEx();
            this.gridViewD04 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnD04Description = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnD04Time = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridViewD08 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnD08Description = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnD08RealValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnD08Percentage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridViewD14 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnD14Description = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnD14RealValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnD14Percentage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridViewD15 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnD15Description = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnD15RealValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnD15Percentage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridViewD30 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnD30Description = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnD30RealValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnD30Percentage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridViewD31 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnD31Description = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnD31RealValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnD31Percentage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridViewD27 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnD27Description = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnD27Time = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridViewD28 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnD28Description = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnD28Time = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridViewD25 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnD25Description = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnD25Time = new DevExpress.XtraGrid.Columns.GridColumn();
            this.bandedGridView1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumnD42Description = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumnD42AssignedValue = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumnD42AssignedPercentage = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumnD42PendingValue = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumnD42PendingPercentage = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumnD42DelayedValue = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumnD42DelayedPercentage = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand5 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumnD42InProgressValue = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumnD42InProgressPercentage = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand19 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumnD42NewValue = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumnD42NewPercentage = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand6 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridColumnD42TotalValue = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridColumnD42TotalValuePercentage = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridView2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand7 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumnD43Description = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand8 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumnD43AssignedValue = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumnD43AssignedPercentage = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand9 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumnD43PendingValue = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumnD43PendingPercentage = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand10 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumnD43DelayedValue = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumnD43DelayedPercentage = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand11 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumnD43InProgressValue = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumnD43InProgressPercentage = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand20 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumnD43NewValue = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumnD43NewPercentage = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand12 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumnD43TotalValue = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumnD43TotalPercentage = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridView3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand13 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumnD44Description = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand14 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumnD44AssignedValue = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumnD44AssignedPercentage = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand15 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumnD44PendingValue = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumnD44PendingPercentage = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand16 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumnD44DelayedValue = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumnD44DelayedPercentage = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand17 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumnD44InProgressValue = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumnD44InProgressPercentage = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand21 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumnD44NewValue = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumnD44NewPercentage = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand18 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumnD44TotalValue = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.bandedGridColumnD44TotalValuePercentage = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridView1 = new GridViewEx();
            this.gridColumnIndicator = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnCondition = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemPictureEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.gridColumnRealValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnTrend = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTimeEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewD01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewD04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewD08)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewD14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewD15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewD30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewD31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewD27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewD28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewD25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // gridViewD01
            // 
            this.gridViewD01.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnD01Description,
            this.gridColumnD01Time});
            this.gridViewD01.GridControl = this.gridControl1;
            this.gridViewD01.GroupFormat = "[#image]{1} {2}";
            this.gridViewD01.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "totalTime", this.gridColumnD01Time, "SUM={0}")});
            this.gridViewD01.Name = "gridViewD01";
            this.gridViewD01.OptionsBehavior.AutoPopulateColumns = false;
            this.gridViewD01.OptionsBehavior.Editable = false;
            this.gridViewD01.OptionsCustomization.AllowFilter = false;
            this.gridViewD01.OptionsMenu.EnableColumnMenu = false;
            this.gridViewD01.OptionsMenu.EnableFooterMenu = false;
            this.gridViewD01.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridViewD01.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewD01.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewD01.OptionsView.ShowFooter = true;
            this.gridViewD01.OptionsView.ShowGroupPanel = false;
            this.gridViewD01.OptionsView.ShowIndicator = false;
            this.gridViewD01.DragObjectOver += new DevExpress.XtraGrid.Views.Base.DragObjectOverEventHandler(this.gridView_DragObjectOver);
            // 
            // gridColumnD01Description
            // 
            this.gridColumnD01Description.Caption = "gridColumn1";
            this.gridColumnD01Description.FieldName = "Description";
            this.gridColumnD01Description.Name = "gridColumnD01Description";
            this.gridColumnD01Description.OptionsColumn.AllowEdit = false;
            this.gridColumnD01Description.Visible = true;
            this.gridColumnD01Description.VisibleIndex = 0;
            // 
            // gridColumnD01Time
            // 
            this.gridColumnD01Time.Caption = "gridColumn2";
            this.gridColumnD01Time.FieldName = "Time";
            this.gridColumnD01Time.Name = "gridColumnD01Time";
            this.gridColumnD01Time.OptionsColumn.AllowEdit = false;
            this.gridColumnD01Time.SummaryItem.DisplayFormat = "{0}";
            this.gridColumnD01Time.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom;
            this.gridColumnD01Time.Visible = true;
            this.gridColumnD01Time.VisibleIndex = 1;
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EnableAutoFilter = false;
            this.gridControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            gridLevelNode1.LevelTemplate = this.gridViewD01;
            gridLevelNode1.RelationName = "D01";
            gridLevelNode2.LevelTemplate = this.gridViewD04;
            gridLevelNode2.RelationName = "D04";
            gridLevelNode3.LevelTemplate = this.gridViewD08;
            gridLevelNode3.RelationName = "D08";
            gridLevelNode4.LevelTemplate = this.gridViewD14;
            gridLevelNode4.RelationName = "D14";
            gridLevelNode5.LevelTemplate = this.gridViewD15;
            gridLevelNode5.RelationName = "D15";
            gridLevelNode6.LevelTemplate = this.gridViewD30;
            gridLevelNode6.RelationName = "D30";
            gridLevelNode7.LevelTemplate = this.gridViewD31;
            gridLevelNode7.RelationName = "D31";
            gridLevelNode8.LevelTemplate = this.gridViewD27;
            gridLevelNode8.RelationName = "D27";
            gridLevelNode9.LevelTemplate = this.gridViewD28;
            gridLevelNode9.RelationName = "D28";
            gridLevelNode10.LevelTemplate = this.gridViewD25;
            gridLevelNode10.RelationName = "D25";
            gridLevelNode11.LevelTemplate = this.bandedGridView1;
            gridLevelNode11.RelationName = "D42";
            gridLevelNode12.LevelTemplate = this.bandedGridView2;
            gridLevelNode12.RelationName = "D43";
            gridLevelNode13.LevelTemplate = this.bandedGridView3;
            gridLevelNode13.RelationName = "D44";
            this.gridControl1.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1,
            gridLevelNode2,
            gridLevelNode3,
            gridLevelNode4,
            gridLevelNode5,
            gridLevelNode6,
            gridLevelNode7,
            gridLevelNode8,
            gridLevelNode9,
            gridLevelNode10,
            gridLevelNode11,
            gridLevelNode12,
            gridLevelNode13});
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.LookAndFeel.SkinName = "Blue";
            this.gridControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPictureEdit1,
            this.repositoryItemTimeEdit1});
            this.gridControl1.Size = new System.Drawing.Size(472, 404);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewD04,
            this.gridViewD08,
            this.gridViewD14,
            this.gridViewD15,
            this.gridViewD30,
            this.gridViewD31,
            this.gridViewD27,
            this.gridViewD28,
            this.gridViewD25,
            this.bandedGridView1,
            this.bandedGridView2,
            this.bandedGridView3,
            this.gridView1,
            this.gridViewD01});
            this.gridControl1.ViewTotalRows = false;
            // 
            // gridViewD04
            // 
            this.gridViewD04.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnD04Description,
            this.gridColumnD04Time});
            this.gridViewD04.GridControl = this.gridControl1;
            this.gridViewD04.GroupFormat = "[#image]{1} {2}";
            this.gridViewD04.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "totalTime", this.gridColumnD04Time, "SUM={0}")});
            this.gridViewD04.Name = "gridViewD04";
            this.gridViewD04.OptionsBehavior.AutoPopulateColumns = false;
            this.gridViewD04.OptionsBehavior.Editable = false;
            this.gridViewD04.OptionsCustomization.AllowFilter = false;
            this.gridViewD04.OptionsMenu.EnableColumnMenu = false;
            this.gridViewD04.OptionsMenu.EnableFooterMenu = false;
            this.gridViewD04.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridViewD04.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewD04.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewD04.OptionsView.ShowFooter = true;
            this.gridViewD04.OptionsView.ShowGroupPanel = false;
            this.gridViewD04.OptionsView.ShowIndicator = false;
            this.gridViewD04.DragObjectOver += new DevExpress.XtraGrid.Views.Base.DragObjectOverEventHandler(this.gridView_DragObjectOver);
            // 
            // gridColumnD04Description
            // 
            this.gridColumnD04Description.Caption = "gridColumn1";
            this.gridColumnD04Description.FieldName = "Description";
            this.gridColumnD04Description.Name = "gridColumnD04Description";
            this.gridColumnD04Description.OptionsColumn.AllowEdit = false;
            this.gridColumnD04Description.Visible = true;
            this.gridColumnD04Description.VisibleIndex = 0;
            // 
            // gridColumnD04Time
            // 
            this.gridColumnD04Time.Caption = "gridColumn2";
            this.gridColumnD04Time.FieldName = "Time";
            this.gridColumnD04Time.Name = "gridColumnD04Time";
            this.gridColumnD04Time.OptionsColumn.AllowEdit = false;
            this.gridColumnD04Time.SummaryItem.DisplayFormat = "{0}";
            this.gridColumnD04Time.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom;
            this.gridColumnD04Time.Visible = true;
            this.gridColumnD04Time.VisibleIndex = 1;
            // 
            // gridViewD08
            // 
            this.gridViewD08.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnD08Description,
            this.gridColumnD08RealValue,
            this.gridColumnD08Percentage});
            this.gridViewD08.GridControl = this.gridControl1;
            this.gridViewD08.GroupFormat = "[#image]{1} {2}";
            this.gridViewD08.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "totalRealValue", this.gridColumnD08RealValue, "SUM={0}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "totalPercentage", this.gridColumnD08Percentage, "SUM={0}")});
            this.gridViewD08.Name = "gridViewD08";
            this.gridViewD08.OptionsBehavior.AutoPopulateColumns = false;
            this.gridViewD08.OptionsBehavior.Editable = false;
            this.gridViewD08.OptionsCustomization.AllowFilter = false;
            this.gridViewD08.OptionsMenu.EnableColumnMenu = false;
            this.gridViewD08.OptionsMenu.EnableFooterMenu = false;
            this.gridViewD08.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridViewD08.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewD08.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewD08.OptionsView.ShowFooter = true;
            this.gridViewD08.OptionsView.ShowGroupPanel = false;
            this.gridViewD08.OptionsView.ShowIndicator = false;
            this.gridViewD08.DragObjectOver += new DevExpress.XtraGrid.Views.Base.DragObjectOverEventHandler(this.gridView_DragObjectOver);
            // 
            // gridColumnD08Description
            // 
            this.gridColumnD08Description.Caption = "gridColumn1";
            this.gridColumnD08Description.FieldName = "Description";
            this.gridColumnD08Description.Name = "gridColumnD08Description";
            this.gridColumnD08Description.OptionsColumn.AllowEdit = false;
            this.gridColumnD08Description.Visible = true;
            this.gridColumnD08Description.VisibleIndex = 0;
            // 
            // gridColumnD08RealValue
            // 
            this.gridColumnD08RealValue.Caption = "gridColumn2";
            this.gridColumnD08RealValue.FieldName = "RealValue";
            this.gridColumnD08RealValue.Name = "gridColumnD08RealValue";
            this.gridColumnD08RealValue.OptionsColumn.AllowEdit = false;
            this.gridColumnD08RealValue.SummaryItem.DisplayFormat = "{0}";
            this.gridColumnD08RealValue.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.gridColumnD08RealValue.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.gridColumnD08RealValue.Visible = true;
            this.gridColumnD08RealValue.VisibleIndex = 1;
            // 
            // gridColumnD08Percentage
            // 
            this.gridColumnD08Percentage.Caption = "gridColumn1";
            this.gridColumnD08Percentage.DisplayFormat.FormatString = "p";
            this.gridColumnD08Percentage.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumnD08Percentage.FieldName = "Percentage";
            this.gridColumnD08Percentage.Name = "gridColumnD08Percentage";
            this.gridColumnD08Percentage.OptionsColumn.AllowEdit = false;
            this.gridColumnD08Percentage.SummaryItem.DisplayFormat = "{0}";
            this.gridColumnD08Percentage.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom;
            this.gridColumnD08Percentage.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.gridColumnD08Percentage.Visible = true;
            this.gridColumnD08Percentage.VisibleIndex = 2;
            // 
            // gridViewD14
            // 
            this.gridViewD14.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnD14Description,
            this.gridColumnD14RealValue,
            this.gridColumnD14Percentage});
            this.gridViewD14.GridControl = this.gridControl1;
            this.gridViewD14.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "totalRealValue", this.gridColumnD14RealValue, "SUM={0}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "totalPercentage", this.gridColumnD14Percentage, "SUM={0}")});
            this.gridViewD14.Name = "gridViewD14";
            this.gridViewD14.OptionsBehavior.AutoPopulateColumns = false;
            this.gridViewD14.OptionsBehavior.Editable = false;
            this.gridViewD14.OptionsCustomization.AllowFilter = false;
            this.gridViewD14.OptionsMenu.EnableColumnMenu = false;
            this.gridViewD14.OptionsMenu.EnableFooterMenu = false;
            this.gridViewD14.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridViewD14.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewD14.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewD14.OptionsView.ShowFooter = true;
            this.gridViewD14.OptionsView.ShowGroupPanel = false;
            this.gridViewD14.OptionsView.ShowIndicator = false;
            this.gridViewD14.DragObjectOver += new DevExpress.XtraGrid.Views.Base.DragObjectOverEventHandler(this.gridView_DragObjectOver);
            // 
            // gridColumnD14Description
            // 
            this.gridColumnD14Description.Caption = "gridColumn1";
            this.gridColumnD14Description.FieldName = "Description";
            this.gridColumnD14Description.Name = "gridColumnD14Description";
            this.gridColumnD14Description.Visible = true;
            this.gridColumnD14Description.VisibleIndex = 0;
            // 
            // gridColumnD14RealValue
            // 
            this.gridColumnD14RealValue.Caption = "gridColumn1";
            this.gridColumnD14RealValue.FieldName = "RealValue";
            this.gridColumnD14RealValue.Name = "gridColumnD14RealValue";
            this.gridColumnD14RealValue.SummaryItem.DisplayFormat = "{0}";
            this.gridColumnD14RealValue.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.gridColumnD14RealValue.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.gridColumnD14RealValue.Visible = true;
            this.gridColumnD14RealValue.VisibleIndex = 1;
            // 
            // gridColumnD14Percentage
            // 
            this.gridColumnD14Percentage.Caption = "gridColumn2";
            this.gridColumnD14Percentage.DisplayFormat.FormatString = "p";
            this.gridColumnD14Percentage.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumnD14Percentage.FieldName = "Percentage";
            this.gridColumnD14Percentage.Name = "gridColumnD14Percentage";
            this.gridColumnD14Percentage.SummaryItem.DisplayFormat = "{0}";
            this.gridColumnD14Percentage.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom;
            this.gridColumnD14Percentage.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.gridColumnD14Percentage.Visible = true;
            this.gridColumnD14Percentage.VisibleIndex = 2;
            // 
            // gridViewD15
            // 
            this.gridViewD15.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnD15Description,
            this.gridColumnD15RealValue,
            this.gridColumnD15Percentage});
            this.gridViewD15.GridControl = this.gridControl1;
            this.gridViewD15.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "totalRealValue", this.gridColumnD15RealValue, "SUM={0}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "totalPercentage", this.gridColumnD15Percentage, "SUM={0}")});
            this.gridViewD15.Name = "gridViewD15";
            this.gridViewD15.OptionsBehavior.AutoPopulateColumns = false;
            this.gridViewD15.OptionsBehavior.Editable = false;
            this.gridViewD15.OptionsCustomization.AllowFilter = false;
            this.gridViewD15.OptionsMenu.EnableColumnMenu = false;
            this.gridViewD15.OptionsMenu.EnableFooterMenu = false;
            this.gridViewD15.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridViewD15.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewD15.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewD15.OptionsView.ShowFooter = true;
            this.gridViewD15.OptionsView.ShowGroupPanel = false;
            this.gridViewD15.OptionsView.ShowIndicator = false;
            this.gridViewD15.DragObjectOver += new DevExpress.XtraGrid.Views.Base.DragObjectOverEventHandler(this.gridView_DragObjectOver);
            // 
            // gridColumnD15Description
            // 
            this.gridColumnD15Description.Caption = "gridColumn1";
            this.gridColumnD15Description.FieldName = "Description";
            this.gridColumnD15Description.Name = "gridColumnD15Description";
            this.gridColumnD15Description.Visible = true;
            this.gridColumnD15Description.VisibleIndex = 0;
            // 
            // gridColumnD15RealValue
            // 
            this.gridColumnD15RealValue.Caption = "gridColumn1";
            this.gridColumnD15RealValue.FieldName = "RealValue";
            this.gridColumnD15RealValue.Name = "gridColumnD15RealValue";
            this.gridColumnD15RealValue.SummaryItem.DisplayFormat = "{0}";
            this.gridColumnD15RealValue.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.gridColumnD15RealValue.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.gridColumnD15RealValue.Visible = true;
            this.gridColumnD15RealValue.VisibleIndex = 1;
            // 
            // gridColumnD15Percentage
            // 
            this.gridColumnD15Percentage.Caption = "gridColumn2";
            this.gridColumnD15Percentage.DisplayFormat.FormatString = "p";
            this.gridColumnD15Percentage.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumnD15Percentage.FieldName = "Percentage";
            this.gridColumnD15Percentage.Name = "gridColumnD15Percentage";
            this.gridColumnD15Percentage.SummaryItem.DisplayFormat = "{0}";
            this.gridColumnD15Percentage.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom;
            this.gridColumnD15Percentage.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.gridColumnD15Percentage.Visible = true;
            this.gridColumnD15Percentage.VisibleIndex = 2;
            // 
            // gridViewD30
            // 
            this.gridViewD30.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnD30Description,
            this.gridColumnD30RealValue,
            this.gridColumnD30Percentage});
            this.gridViewD30.GridControl = this.gridControl1;
            this.gridViewD30.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "totalRealValue", this.gridColumnD30RealValue, "SUM={0}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "totalPercentage", this.gridColumnD30Percentage, "SUM={0}")});
            this.gridViewD30.Name = "gridViewD30";
            this.gridViewD30.OptionsBehavior.AutoPopulateColumns = false;
            this.gridViewD30.OptionsBehavior.Editable = false;
            this.gridViewD30.OptionsCustomization.AllowFilter = false;
            this.gridViewD30.OptionsMenu.EnableColumnMenu = false;
            this.gridViewD30.OptionsMenu.EnableFooterMenu = false;
            this.gridViewD30.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridViewD30.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewD30.OptionsView.ShowFooter = true;
            this.gridViewD30.OptionsView.ShowGroupPanel = false;
            this.gridViewD30.OptionsView.ShowIndicator = false;
            this.gridViewD30.DragObjectOver += new DevExpress.XtraGrid.Views.Base.DragObjectOverEventHandler(this.gridView_DragObjectOver);
            // 
            // gridColumnD30Description
            // 
            this.gridColumnD30Description.Caption = "gridColumn1";
            this.gridColumnD30Description.FieldName = "Description";
            this.gridColumnD30Description.Name = "gridColumnD30Description";
            this.gridColumnD30Description.Visible = true;
            this.gridColumnD30Description.VisibleIndex = 0;
            // 
            // gridColumnD30RealValue
            // 
            this.gridColumnD30RealValue.Caption = "gridColumn2";
            this.gridColumnD30RealValue.FieldName = "RealValue";
            this.gridColumnD30RealValue.Name = "gridColumnD30RealValue";
            this.gridColumnD30RealValue.SummaryItem.DisplayFormat = "{0}";
            this.gridColumnD30RealValue.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.gridColumnD30RealValue.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.gridColumnD30RealValue.Visible = true;
            this.gridColumnD30RealValue.VisibleIndex = 1;
            // 
            // gridColumnD30Percentage
            // 
            this.gridColumnD30Percentage.Caption = "gridColumn3";
            this.gridColumnD30Percentage.DisplayFormat.FormatString = "p";
            this.gridColumnD30Percentage.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumnD30Percentage.FieldName = "Percentage";
            this.gridColumnD30Percentage.Name = "gridColumnD30Percentage";
            this.gridColumnD30Percentage.SummaryItem.DisplayFormat = "{0}";
            this.gridColumnD30Percentage.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom;
            this.gridColumnD30Percentage.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.gridColumnD30Percentage.Visible = true;
            this.gridColumnD30Percentage.VisibleIndex = 2;
            // 
            // gridViewD31
            // 
            this.gridViewD31.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnD31Description,
            this.gridColumnD31RealValue,
            this.gridColumnD31Percentage});
            this.gridViewD31.GridControl = this.gridControl1;
            this.gridViewD31.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "totalRealValue", this.gridColumnD31RealValue, "SUM={0}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "totalPercentage", this.gridColumnD31Percentage, "SUM={0}")});
            this.gridViewD31.Name = "gridViewD31";
            this.gridViewD31.OptionsBehavior.AutoPopulateColumns = false;
            this.gridViewD31.OptionsBehavior.Editable = false;
            this.gridViewD31.OptionsCustomization.AllowFilter = false;
            this.gridViewD31.OptionsMenu.EnableColumnMenu = false;
            this.gridViewD31.OptionsMenu.EnableFooterMenu = false;
            this.gridViewD31.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridViewD31.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewD31.OptionsView.ShowFooter = true;
            this.gridViewD31.OptionsView.ShowGroupPanel = false;
            this.gridViewD31.OptionsView.ShowIndicator = false;
            this.gridViewD31.DragObjectOver += new DevExpress.XtraGrid.Views.Base.DragObjectOverEventHandler(this.gridView_DragObjectOver);
            // 
            // gridColumnD31Description
            // 
            this.gridColumnD31Description.Caption = "gridColumn1";
            this.gridColumnD31Description.FieldName = "Description";
            this.gridColumnD31Description.Name = "gridColumnD31Description";
            this.gridColumnD31Description.Visible = true;
            this.gridColumnD31Description.VisibleIndex = 0;
            // 
            // gridColumnD31RealValue
            // 
            this.gridColumnD31RealValue.Caption = "gridColumn2";
            this.gridColumnD31RealValue.FieldName = "RealValue";
            this.gridColumnD31RealValue.Name = "gridColumnD31RealValue";
            this.gridColumnD31RealValue.SummaryItem.DisplayFormat = "{0}";
            this.gridColumnD31RealValue.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.gridColumnD31RealValue.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.gridColumnD31RealValue.Visible = true;
            this.gridColumnD31RealValue.VisibleIndex = 1;
            // 
            // gridColumnD31Percentage
            // 
            this.gridColumnD31Percentage.Caption = "gridColumn3";
            this.gridColumnD31Percentage.DisplayFormat.FormatString = "p";
            this.gridColumnD31Percentage.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumnD31Percentage.FieldName = "Percentage";
            this.gridColumnD31Percentage.Name = "gridColumnD31Percentage";
            this.gridColumnD31Percentage.SummaryItem.DisplayFormat = "{0}";
            this.gridColumnD31Percentage.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom;
            this.gridColumnD31Percentage.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.gridColumnD31Percentage.Visible = true;
            this.gridColumnD31Percentage.VisibleIndex = 2;
            // 
            // gridViewD27
            // 
            this.gridViewD27.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnD27Description,
            this.gridColumnD27Time});
            this.gridViewD27.GridControl = this.gridControl1;
            this.gridViewD27.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "totalTime", this.gridColumnD27Time, "SUM={0}")});
            this.gridViewD27.Name = "gridViewD27";
            this.gridViewD27.OptionsBehavior.AutoPopulateColumns = false;
            this.gridViewD27.OptionsBehavior.Editable = false;
            this.gridViewD27.OptionsCustomization.AllowFilter = false;
            this.gridViewD27.OptionsMenu.EnableColumnMenu = false;
            this.gridViewD27.OptionsMenu.EnableFooterMenu = false;
            this.gridViewD27.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridViewD27.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewD27.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewD27.OptionsView.ShowFooter = true;
            this.gridViewD27.OptionsView.ShowGroupPanel = false;
            this.gridViewD27.OptionsView.ShowIndicator = false;
            this.gridViewD27.DragObjectOver += new DevExpress.XtraGrid.Views.Base.DragObjectOverEventHandler(this.gridView_DragObjectOver);
            // 
            // gridColumnD27Description
            // 
            this.gridColumnD27Description.Caption = "gridColumn1";
            this.gridColumnD27Description.FieldName = "Description";
            this.gridColumnD27Description.Name = "gridColumnD27Description";
            this.gridColumnD27Description.Visible = true;
            this.gridColumnD27Description.VisibleIndex = 0;
            // 
            // gridColumnD27Time
            // 
            this.gridColumnD27Time.Caption = "gridColumn2";
            this.gridColumnD27Time.FieldName = "Time";
            this.gridColumnD27Time.Name = "gridColumnD27Time";
            this.gridColumnD27Time.SummaryItem.DisplayFormat = "{0}";
            this.gridColumnD27Time.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom;
            this.gridColumnD27Time.Visible = true;
            this.gridColumnD27Time.VisibleIndex = 1;
            // 
            // gridViewD28
            // 
            this.gridViewD28.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnD28Description,
            this.gridColumnD28Time});
            this.gridViewD28.GridControl = this.gridControl1;
            this.gridViewD28.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "totalTime", this.gridColumnD28Time, "SUM={0}")});
            this.gridViewD28.Name = "gridViewD28";
            this.gridViewD28.OptionsBehavior.AutoPopulateColumns = false;
            this.gridViewD28.OptionsBehavior.Editable = false;
            this.gridViewD28.OptionsCustomization.AllowFilter = false;
            this.gridViewD28.OptionsMenu.EnableColumnMenu = false;
            this.gridViewD28.OptionsMenu.EnableFooterMenu = false;
            this.gridViewD28.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridViewD28.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewD28.OptionsView.ShowFooter = true;
            this.gridViewD28.OptionsView.ShowGroupPanel = false;
            this.gridViewD28.OptionsView.ShowIndicator = false;
            this.gridViewD28.DragObjectOver += new DevExpress.XtraGrid.Views.Base.DragObjectOverEventHandler(this.gridView_DragObjectOver);
            // 
            // gridColumnD28Description
            // 
            this.gridColumnD28Description.Caption = "gridColumn1";
            this.gridColumnD28Description.FieldName = "Description";
            this.gridColumnD28Description.Name = "gridColumnD28Description";
            this.gridColumnD28Description.Visible = true;
            this.gridColumnD28Description.VisibleIndex = 0;
            // 
            // gridColumnD28Time
            // 
            this.gridColumnD28Time.Caption = "gridColumn2";
            this.gridColumnD28Time.FieldName = "Time";
            this.gridColumnD28Time.Name = "gridColumnD28Time";
            this.gridColumnD28Time.SummaryItem.DisplayFormat = "{0}";
            this.gridColumnD28Time.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom;
            this.gridColumnD28Time.Visible = true;
            this.gridColumnD28Time.VisibleIndex = 1;
            // 
            // gridViewD25
            // 
            this.gridViewD25.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnD25Description,
            this.gridColumnD25Time});
            this.gridViewD25.GridControl = this.gridControl1;
            this.gridViewD25.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "totalTime", this.gridColumnD25Time, "SUM={0}")});
            this.gridViewD25.Name = "gridViewD25";
            this.gridViewD25.OptionsBehavior.AutoPopulateColumns = false;
            this.gridViewD25.OptionsBehavior.Editable = false;
            this.gridViewD25.OptionsCustomization.AllowFilter = false;
            this.gridViewD25.OptionsMenu.EnableColumnMenu = false;
            this.gridViewD25.OptionsMenu.EnableFooterMenu = false;
            this.gridViewD25.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridViewD25.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewD25.OptionsView.ShowFooter = true;
            this.gridViewD25.OptionsView.ShowGroupPanel = false;
            this.gridViewD25.OptionsView.ShowIndicator = false;
            this.gridViewD25.DragObjectOver += new DevExpress.XtraGrid.Views.Base.DragObjectOverEventHandler(this.gridView_DragObjectOver);
            // 
            // gridColumnD25Description
            // 
            this.gridColumnD25Description.Caption = "gridColumn1";
            this.gridColumnD25Description.FieldName = "Description";
            this.gridColumnD25Description.Name = "gridColumnD25Description";
            this.gridColumnD25Description.Visible = true;
            this.gridColumnD25Description.VisibleIndex = 0;
            // 
            // gridColumnD25Time
            // 
            this.gridColumnD25Time.Caption = "gridColumn2";
            this.gridColumnD25Time.FieldName = "Time";
            this.gridColumnD25Time.Name = "gridColumnD25Time";
            this.gridColumnD25Time.SummaryItem.DisplayFormat = "{0}";
            this.gridColumnD25Time.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom;
            this.gridColumnD25Time.Visible = true;
            this.gridColumnD25Time.VisibleIndex = 1;
            // 
            // bandedGridView1
            // 
            this.bandedGridView1.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand1,
            this.gridBand2,
            this.gridBand3,
            this.gridBand4,
            this.gridBand5,
            this.gridBand19,
            this.gridBand6});
            this.bandedGridView1.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.gridColumnD42Description,
            this.gridColumnD42AssignedValue,
            this.gridColumnD42AssignedPercentage,
            this.gridColumnD42PendingValue,
            this.gridColumnD42PendingPercentage,
            this.gridColumnD42DelayedValue,
            this.gridColumnD42DelayedPercentage,
            this.gridColumnD42InProgressValue,
            this.gridColumnD42InProgressPercentage,
            this.gridColumnD42TotalValue,
            this.gridColumnD42TotalValuePercentage,
            this.gridColumnD42NewValue,
            this.gridColumnD42NewPercentage});
            this.bandedGridView1.GridControl = this.gridControl1;
            this.bandedGridView1.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "totalAssignedValue", this.gridColumnD42AssignedValue, "SUM={0}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "totalInProgressValue", this.gridColumnD42InProgressValue, "SUM={0}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Custom, "totalInProgressPercentage", this.gridColumnD42InProgressPercentage, "SUM={0}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "totalPendingValue", this.gridColumnD42PendingValue, "SUM={0}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "totalDelayedValue", this.gridColumnD42DelayedValue, "SUM={0}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "totalValue", this.gridColumnD42TotalValue, "SUM={0}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "totalValuePercentage", this.gridColumnD42TotalValuePercentage, "SUM={0}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "totalAssignedPercentage", this.gridColumnD42AssignedPercentage, "SUM={0}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "totalPendingPercentage", this.gridColumnD42PendingPercentage, "SUM={0}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "totalDelayedPercentage", this.gridColumnD42DelayedPercentage, "SUM={0}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "totalNewValue", this.gridColumnD42NewValue, "SUM={0}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "totalNewPercentage", this.gridColumnD42NewPercentage, "SUM={0}")});
            this.bandedGridView1.Name = "bandedGridView1";
            this.bandedGridView1.OptionsBehavior.AutoPopulateColumns = false;
            this.bandedGridView1.OptionsBehavior.Editable = false;
            this.bandedGridView1.OptionsCustomization.AllowFilter = false;
            this.bandedGridView1.OptionsMenu.EnableColumnMenu = false;
            this.bandedGridView1.OptionsMenu.EnableFooterMenu = false;
            this.bandedGridView1.OptionsMenu.EnableGroupPanelMenu = false;
            this.bandedGridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.bandedGridView1.OptionsView.ShowFooter = true;
            this.bandedGridView1.OptionsView.ShowGroupPanel = false;
            this.bandedGridView1.OptionsView.ShowIndicator = false;
            this.bandedGridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumnD42InProgressPercentage, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.bandedGridView1.DragObjectOver += new DevExpress.XtraGrid.Views.Base.DragObjectOverEventHandler(this.gridView_DragObjectOver);
            // 
            // gridBand1
            // 
            this.gridBand1.Caption = "Incidente";
            this.gridBand1.Columns.Add(this.gridColumnD42Description);
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.Width = 84;
            // 
            // gridColumnD42Description
            // 
            this.gridColumnD42Description.Caption = "gridColumn1";
            this.gridColumnD42Description.FieldName = "Description";
            this.gridColumnD42Description.Name = "gridColumnD42Description";
            this.gridColumnD42Description.OptionsColumn.AllowEdit = false;
            this.gridColumnD42Description.Visible = true;
            this.gridColumnD42Description.Width = 84;
            // 
            // gridBand2
            // 
            this.gridBand2.Caption = "Nueva";
            this.gridBand2.Columns.Add(this.gridColumnD42AssignedValue);
            this.gridBand2.Columns.Add(this.gridColumnD42AssignedPercentage);
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.Width = 168;
            // 
            // gridColumnD42AssignedValue
            // 
            this.gridColumnD42AssignedValue.Caption = "gridColumn2";
            this.gridColumnD42AssignedValue.FieldName = "AssignedValue";
            this.gridColumnD42AssignedValue.Name = "gridColumnD42AssignedValue";
            this.gridColumnD42AssignedValue.OptionsColumn.AllowEdit = false;
            this.gridColumnD42AssignedValue.SummaryItem.DisplayFormat = "{0}";
            this.gridColumnD42AssignedValue.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.gridColumnD42AssignedValue.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.gridColumnD42AssignedValue.Visible = true;
            this.gridColumnD42AssignedValue.Width = 84;
            // 
            // gridColumnD42AssignedPercentage
            // 
            this.gridColumnD42AssignedPercentage.Caption = "gridColumn3";
            this.gridColumnD42AssignedPercentage.FieldName = "AssignedPercentage";
            this.gridColumnD42AssignedPercentage.Name = "gridColumnD42AssignedPercentage";
            this.gridColumnD42AssignedPercentage.OptionsColumn.AllowEdit = false;
            this.gridColumnD42AssignedPercentage.SummaryItem.DisplayFormat = "{0}";
            this.gridColumnD42AssignedPercentage.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom;
            this.gridColumnD42AssignedPercentage.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.gridColumnD42AssignedPercentage.Visible = true;
            this.gridColumnD42AssignedPercentage.Width = 84;
            // 
            // gridBand3
            // 
            this.gridBand3.Caption = "Pendientez1";
            this.gridBand3.Columns.Add(this.gridColumnD42PendingValue);
            this.gridBand3.Columns.Add(this.gridColumnD42PendingPercentage);
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.Width = 168;
            // 
            // gridColumnD42PendingValue
            // 
            this.gridColumnD42PendingValue.Caption = "gridColumn4";
            this.gridColumnD42PendingValue.FieldName = "PendingValue";
            this.gridColumnD42PendingValue.Name = "gridColumnD42PendingValue";
            this.gridColumnD42PendingValue.OptionsColumn.AllowEdit = false;
            this.gridColumnD42PendingValue.SummaryItem.DisplayFormat = "{0}";
            this.gridColumnD42PendingValue.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.gridColumnD42PendingValue.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.gridColumnD42PendingValue.Visible = true;
            this.gridColumnD42PendingValue.Width = 84;
            // 
            // gridColumnD42PendingPercentage
            // 
            this.gridColumnD42PendingPercentage.Caption = "gridColumn5";
            this.gridColumnD42PendingPercentage.FieldName = "PendingPercentage";
            this.gridColumnD42PendingPercentage.Name = "gridColumnD42PendingPercentage";
            this.gridColumnD42PendingPercentage.OptionsColumn.AllowEdit = false;
            this.gridColumnD42PendingPercentage.SummaryItem.DisplayFormat = "{0}";
            this.gridColumnD42PendingPercentage.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom;
            this.gridColumnD42PendingPercentage.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.gridColumnD42PendingPercentage.Visible = true;
            this.gridColumnD42PendingPercentage.Width = 84;
            // 
            // gridBand4
            // 
            this.gridBand4.Caption = "Retrasada";
            this.gridBand4.Columns.Add(this.gridColumnD42DelayedValue);
            this.gridBand4.Columns.Add(this.gridColumnD42DelayedPercentage);
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.Width = 168;
            // 
            // gridColumnD42DelayedValue
            // 
            this.gridColumnD42DelayedValue.Caption = "gridColumn6";
            this.gridColumnD42DelayedValue.FieldName = "DelayedValue";
            this.gridColumnD42DelayedValue.Name = "gridColumnD42DelayedValue";
            this.gridColumnD42DelayedValue.OptionsColumn.AllowEdit = false;
            this.gridColumnD42DelayedValue.SummaryItem.DisplayFormat = "{0}";
            this.gridColumnD42DelayedValue.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.gridColumnD42DelayedValue.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.gridColumnD42DelayedValue.Visible = true;
            this.gridColumnD42DelayedValue.Width = 84;
            // 
            // gridColumnD42DelayedPercentage
            // 
            this.gridColumnD42DelayedPercentage.Caption = "gridColumn7";
            this.gridColumnD42DelayedPercentage.FieldName = "DelayedPercentage";
            this.gridColumnD42DelayedPercentage.Name = "gridColumnD42DelayedPercentage";
            this.gridColumnD42DelayedPercentage.OptionsColumn.AllowEdit = false;
            this.gridColumnD42DelayedPercentage.SummaryItem.DisplayFormat = "{0}";
            this.gridColumnD42DelayedPercentage.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom;
            this.gridColumnD42DelayedPercentage.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.gridColumnD42DelayedPercentage.Visible = true;
            this.gridColumnD42DelayedPercentage.Width = 84;
            // 
            // gridBand5
            // 
            this.gridBand5.Caption = "En proceso";
            this.gridBand5.Columns.Add(this.gridColumnD42InProgressValue);
            this.gridBand5.Columns.Add(this.gridColumnD42InProgressPercentage);
            this.gridBand5.Name = "gridBand5";
            this.gridBand5.Width = 168;
            // 
            // gridColumnD42InProgressValue
            // 
            this.gridColumnD42InProgressValue.Caption = "gridColumn8";
            this.gridColumnD42InProgressValue.FieldName = "InProgressValue";
            this.gridColumnD42InProgressValue.Name = "gridColumnD42InProgressValue";
            this.gridColumnD42InProgressValue.OptionsColumn.AllowEdit = false;
            this.gridColumnD42InProgressValue.SummaryItem.DisplayFormat = "{0}";
            this.gridColumnD42InProgressValue.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.gridColumnD42InProgressValue.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.gridColumnD42InProgressValue.Visible = true;
            this.gridColumnD42InProgressValue.Width = 84;
            // 
            // gridColumnD42InProgressPercentage
            // 
            this.gridColumnD42InProgressPercentage.Caption = "gridColumn9";
            this.gridColumnD42InProgressPercentage.FieldName = "InProgressPercentage";
            this.gridColumnD42InProgressPercentage.Name = "gridColumnD42InProgressPercentage";
            this.gridColumnD42InProgressPercentage.OptionsColumn.AllowEdit = false;
            this.gridColumnD42InProgressPercentage.SummaryItem.DisplayFormat = "{0}";
            this.gridColumnD42InProgressPercentage.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom;
            this.gridColumnD42InProgressPercentage.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.gridColumnD42InProgressPercentage.Visible = true;
            this.gridColumnD42InProgressPercentage.Width = 84;
            // 
            // gridBand19
            // 
            this.gridBand19.Caption = "No atendida";
            this.gridBand19.Columns.Add(this.gridColumnD42NewValue);
            this.gridBand19.Columns.Add(this.gridColumnD42NewPercentage);
            this.gridBand19.Name = "gridBand19";
            this.gridBand19.Width = 150;
            // 
            // gridColumnD42NewValue
            // 
            this.gridColumnD42NewValue.Caption = "gridColumn12";
            this.gridColumnD42NewValue.FieldName = "NewValue";
            this.gridColumnD42NewValue.Name = "gridColumnD42NewValue";
            this.gridColumnD42NewValue.OptionsColumn.AllowEdit = false;
            this.gridColumnD42NewValue.SummaryItem.DisplayFormat = "{0}";
            this.gridColumnD42NewValue.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.gridColumnD42NewValue.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.gridColumnD42NewValue.Visible = true;
            // 
            // gridColumnD42NewPercentage
            // 
            this.gridColumnD42NewPercentage.Caption = "gridColumn13";
            this.gridColumnD42NewPercentage.FieldName = "NewPercentage";
            this.gridColumnD42NewPercentage.Name = "gridColumnD42NewPercentage";
            this.gridColumnD42NewPercentage.OptionsColumn.AllowEdit = false;
            this.gridColumnD42NewPercentage.SummaryItem.DisplayFormat = "{0}";
            this.gridColumnD42NewPercentage.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom;
            this.gridColumnD42NewPercentage.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.gridColumnD42NewPercentage.Visible = true;
            // 
            // gridBand6
            // 
            this.gridBand6.Caption = "Total";
            this.gridBand6.Columns.Add(this.gridColumnD42TotalValue);
            this.gridBand6.Columns.Add(this.gridColumnD42TotalValuePercentage);
            this.gridBand6.Name = "gridBand6";
            this.gridBand6.Width = 170;
            // 
            // gridColumnD42TotalValue
            // 
            this.gridColumnD42TotalValue.Caption = "gridColumn10";
            this.gridColumnD42TotalValue.FieldName = "TotalValue";
            this.gridColumnD42TotalValue.Name = "gridColumnD42TotalValue";
            this.gridColumnD42TotalValue.OptionsColumn.AllowEdit = false;
            this.gridColumnD42TotalValue.SummaryItem.DisplayFormat = "{0}";
            this.gridColumnD42TotalValue.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.gridColumnD42TotalValue.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.gridColumnD42TotalValue.Visible = true;
            this.gridColumnD42TotalValue.Width = 95;
            // 
            // gridColumnD42TotalValuePercentage
            // 
            this.gridColumnD42TotalValuePercentage.Caption = "gridColumn11";
            this.gridColumnD42TotalValuePercentage.FieldName = "TotalValuePercentage";
            this.gridColumnD42TotalValuePercentage.Name = "gridColumnD42TotalValuePercentage";
            this.gridColumnD42TotalValuePercentage.OptionsColumn.AllowEdit = false;
            this.gridColumnD42TotalValuePercentage.SummaryItem.DisplayFormat = "{0}";
            this.gridColumnD42TotalValuePercentage.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom;
            this.gridColumnD42TotalValuePercentage.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.gridColumnD42TotalValuePercentage.Visible = true;
            // 
            // bandedGridView2
            // 
            this.bandedGridView2.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand7,
            this.gridBand8,
            this.gridBand9,
            this.gridBand10,
            this.gridBand11,
            this.gridBand20,
            this.gridBand12});
            this.bandedGridView2.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.bandedGridColumnD43Description,
            this.bandedGridColumnD43AssignedValue,
            this.bandedGridColumnD43AssignedPercentage,
            this.bandedGridColumnD43PendingValue,
            this.bandedGridColumnD43PendingPercentage,
            this.bandedGridColumnD43DelayedValue,
            this.bandedGridColumnD43DelayedPercentage,
            this.bandedGridColumnD43InProgressValue,
            this.bandedGridColumnD43InProgressPercentage,
            this.bandedGridColumnD43TotalValue,
            this.bandedGridColumnD43TotalPercentage,
            this.bandedGridColumnD43NewValue,
            this.bandedGridColumnD43NewPercentage});
            this.bandedGridView2.GridControl = this.gridControl1;
            this.bandedGridView2.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "totalAssignedValue", this.bandedGridColumnD43AssignedValue, "SUM={0}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "totalAssignedPercentage", this.bandedGridColumnD43AssignedPercentage, "SUM={0}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "totalPendingValue", this.bandedGridColumnD43PendingValue, "SUM={0}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "totalPendingPercentage", this.bandedGridColumnD43PendingPercentage, "SUM={0}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "totalDelayedValue", this.bandedGridColumnD43DelayedValue, "SUM={0}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "totalDelayedPercentage", this.bandedGridColumnD43DelayedPercentage, "SUM={0}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "totalInProgressValue", this.bandedGridColumnD43InProgressValue, "SUM={0}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "totalInProgressPercentage", this.bandedGridColumnD43InProgressPercentage, "SUM={0}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "totalValue", this.bandedGridColumnD43TotalValue, "SUM={0}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "totalValuePercentage", this.bandedGridColumnD43TotalPercentage, "SUM={0}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "totalNewValue", this.bandedGridColumnD43NewValue, "SUM={0}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "totalNewPercentage", this.bandedGridColumnD43NewPercentage, "SUM={0}")});
            this.bandedGridView2.Name = "bandedGridView2";
            this.bandedGridView2.OptionsBehavior.AutoPopulateColumns = false;
            this.bandedGridView2.OptionsBehavior.Editable = false;
            this.bandedGridView2.OptionsCustomization.AllowFilter = false;
            this.bandedGridView2.OptionsMenu.EnableColumnMenu = false;
            this.bandedGridView2.OptionsMenu.EnableFooterMenu = false;
            this.bandedGridView2.OptionsMenu.EnableGroupPanelMenu = false;
            this.bandedGridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.bandedGridView2.OptionsView.ShowFooter = true;
            this.bandedGridView2.OptionsView.ShowGroupPanel = false;
            this.bandedGridView2.OptionsView.ShowIndicator = false;
            this.bandedGridView2.DragObjectOver += new DevExpress.XtraGrid.Views.Base.DragObjectOverEventHandler(this.gridView_DragObjectOver);
            // 
            // gridBand7
            // 
            this.gridBand7.Caption = "Incidente";
            this.gridBand7.Columns.Add(this.bandedGridColumnD43Description);
            this.gridBand7.Name = "gridBand7";
            this.gridBand7.Width = 75;
            // 
            // bandedGridColumnD43Description
            // 
            this.bandedGridColumnD43Description.Caption = "bandedGridColumn1";
            this.bandedGridColumnD43Description.FieldName = "Description";
            this.bandedGridColumnD43Description.Name = "bandedGridColumnD43Description";
            this.bandedGridColumnD43Description.OptionsColumn.AllowEdit = false;
            this.bandedGridColumnD43Description.Visible = true;
            // 
            // gridBand8
            // 
            this.gridBand8.Caption = "Nueva";
            this.gridBand8.Columns.Add(this.bandedGridColumnD43AssignedValue);
            this.gridBand8.Columns.Add(this.bandedGridColumnD43AssignedPercentage);
            this.gridBand8.Name = "gridBand8";
            this.gridBand8.Width = 150;
            // 
            // bandedGridColumnD43AssignedValue
            // 
            this.bandedGridColumnD43AssignedValue.Caption = "bandedGridColumn2";
            this.bandedGridColumnD43AssignedValue.FieldName = "AssignedValue";
            this.bandedGridColumnD43AssignedValue.Name = "bandedGridColumnD43AssignedValue";
            this.bandedGridColumnD43AssignedValue.OptionsColumn.AllowEdit = false;
            this.bandedGridColumnD43AssignedValue.SummaryItem.DisplayFormat = "{0}";
            this.bandedGridColumnD43AssignedValue.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.bandedGridColumnD43AssignedValue.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.bandedGridColumnD43AssignedValue.Visible = true;
            // 
            // bandedGridColumnD43AssignedPercentage
            // 
            this.bandedGridColumnD43AssignedPercentage.Caption = "bandedGridColumn3";
            this.bandedGridColumnD43AssignedPercentage.FieldName = "AssignedPercentage";
            this.bandedGridColumnD43AssignedPercentage.Name = "bandedGridColumnD43AssignedPercentage";
            this.bandedGridColumnD43AssignedPercentage.OptionsColumn.AllowEdit = false;
            this.bandedGridColumnD43AssignedPercentage.SummaryItem.DisplayFormat = "{0}";
            this.bandedGridColumnD43AssignedPercentage.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom;
            this.bandedGridColumnD43AssignedPercentage.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.bandedGridColumnD43AssignedPercentage.Visible = true;
            // 
            // gridBand9
            // 
            this.gridBand9.Caption = "Pendientez2";
            this.gridBand9.Columns.Add(this.bandedGridColumnD43PendingValue);
            this.gridBand9.Columns.Add(this.bandedGridColumnD43PendingPercentage);
            this.gridBand9.Name = "gridBand9";
            this.gridBand9.Width = 150;
            // 
            // bandedGridColumnD43PendingValue
            // 
            this.bandedGridColumnD43PendingValue.Caption = "bandedGridColumn4";
            this.bandedGridColumnD43PendingValue.FieldName = "PendingValue";
            this.bandedGridColumnD43PendingValue.Name = "bandedGridColumnD43PendingValue";
            this.bandedGridColumnD43PendingValue.OptionsColumn.AllowEdit = false;
            this.bandedGridColumnD43PendingValue.SummaryItem.DisplayFormat = "{0}";
            this.bandedGridColumnD43PendingValue.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.bandedGridColumnD43PendingValue.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.bandedGridColumnD43PendingValue.Visible = true;
            // 
            // bandedGridColumnD43PendingPercentage
            // 
            this.bandedGridColumnD43PendingPercentage.Caption = "bandedGridColumn5";
            this.bandedGridColumnD43PendingPercentage.FieldName = "PendingPercentage";
            this.bandedGridColumnD43PendingPercentage.Name = "bandedGridColumnD43PendingPercentage";
            this.bandedGridColumnD43PendingPercentage.OptionsColumn.AllowEdit = false;
            this.bandedGridColumnD43PendingPercentage.SummaryItem.DisplayFormat = "{0}";
            this.bandedGridColumnD43PendingPercentage.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom;
            this.bandedGridColumnD43PendingPercentage.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.bandedGridColumnD43PendingPercentage.Visible = true;
            // 
            // gridBand10
            // 
            this.gridBand10.Caption = "Retrasada";
            this.gridBand10.Columns.Add(this.bandedGridColumnD43DelayedValue);
            this.gridBand10.Columns.Add(this.bandedGridColumnD43DelayedPercentage);
            this.gridBand10.Name = "gridBand10";
            this.gridBand10.Width = 150;
            // 
            // bandedGridColumnD43DelayedValue
            // 
            this.bandedGridColumnD43DelayedValue.Caption = "bandedGridColumn6";
            this.bandedGridColumnD43DelayedValue.FieldName = "DelayedValue";
            this.bandedGridColumnD43DelayedValue.Name = "bandedGridColumnD43DelayedValue";
            this.bandedGridColumnD43DelayedValue.OptionsColumn.AllowEdit = false;
            this.bandedGridColumnD43DelayedValue.SummaryItem.DisplayFormat = "{0}";
            this.bandedGridColumnD43DelayedValue.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.bandedGridColumnD43DelayedValue.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.bandedGridColumnD43DelayedValue.Visible = true;
            // 
            // bandedGridColumnD43DelayedPercentage
            // 
            this.bandedGridColumnD43DelayedPercentage.Caption = "bandedGridColumn7";
            this.bandedGridColumnD43DelayedPercentage.FieldName = "DelayedPercentage";
            this.bandedGridColumnD43DelayedPercentage.Name = "bandedGridColumnD43DelayedPercentage";
            this.bandedGridColumnD43DelayedPercentage.OptionsColumn.AllowEdit = false;
            this.bandedGridColumnD43DelayedPercentage.SummaryItem.DisplayFormat = "{0}";
            this.bandedGridColumnD43DelayedPercentage.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom;
            this.bandedGridColumnD43DelayedPercentage.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.bandedGridColumnD43DelayedPercentage.Visible = true;
            // 
            // gridBand11
            // 
            this.gridBand11.Caption = "En proceso";
            this.gridBand11.Columns.Add(this.bandedGridColumnD43InProgressValue);
            this.gridBand11.Columns.Add(this.bandedGridColumnD43InProgressPercentage);
            this.gridBand11.Name = "gridBand11";
            this.gridBand11.Width = 150;
            // 
            // bandedGridColumnD43InProgressValue
            // 
            this.bandedGridColumnD43InProgressValue.Caption = "bandedGridColumn8";
            this.bandedGridColumnD43InProgressValue.FieldName = "InProgressValue";
            this.bandedGridColumnD43InProgressValue.Name = "bandedGridColumnD43InProgressValue";
            this.bandedGridColumnD43InProgressValue.OptionsColumn.AllowEdit = false;
            this.bandedGridColumnD43InProgressValue.SummaryItem.DisplayFormat = "{0}";
            this.bandedGridColumnD43InProgressValue.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.bandedGridColumnD43InProgressValue.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.bandedGridColumnD43InProgressValue.Visible = true;
            // 
            // bandedGridColumnD43InProgressPercentage
            // 
            this.bandedGridColumnD43InProgressPercentage.Caption = "bandedGridColumn9";
            this.bandedGridColumnD43InProgressPercentage.FieldName = "InProgressPercentage";
            this.bandedGridColumnD43InProgressPercentage.Name = "bandedGridColumnD43InProgressPercentage";
            this.bandedGridColumnD43InProgressPercentage.OptionsColumn.AllowEdit = false;
            this.bandedGridColumnD43InProgressPercentage.SummaryItem.DisplayFormat = "{0}";
            this.bandedGridColumnD43InProgressPercentage.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom;
            this.bandedGridColumnD43InProgressPercentage.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.bandedGridColumnD43InProgressPercentage.Visible = true;
            // 
            // gridBand20
            // 
            this.gridBand20.Caption = "No atendida";
            this.gridBand20.Columns.Add(this.bandedGridColumnD43NewValue);
            this.gridBand20.Columns.Add(this.bandedGridColumnD43NewPercentage);
            this.gridBand20.Name = "gridBand20";
            this.gridBand20.Width = 150;
            // 
            // bandedGridColumnD43NewValue
            // 
            this.bandedGridColumnD43NewValue.Caption = "bandedGridColumn12";
            this.bandedGridColumnD43NewValue.FieldName = "NewValue";
            this.bandedGridColumnD43NewValue.Name = "bandedGridColumnD43NewValue";
            this.bandedGridColumnD43NewValue.OptionsColumn.AllowEdit = false;
            this.bandedGridColumnD43NewValue.SummaryItem.DisplayFormat = "{0}";
            this.bandedGridColumnD43NewValue.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.bandedGridColumnD43NewValue.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.bandedGridColumnD43NewValue.Visible = true;
            // 
            // bandedGridColumnD43NewPercentage
            // 
            this.bandedGridColumnD43NewPercentage.Caption = "bandedGridColumn13";
            this.bandedGridColumnD43NewPercentage.FieldName = "NewPercentage";
            this.bandedGridColumnD43NewPercentage.Name = "bandedGridColumnD43NewPercentage";
            this.bandedGridColumnD43NewPercentage.OptionsColumn.AllowEdit = false;
            this.bandedGridColumnD43NewPercentage.SummaryItem.DisplayFormat = "{0}";
            this.bandedGridColumnD43NewPercentage.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom;
            this.bandedGridColumnD43NewPercentage.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.bandedGridColumnD43NewPercentage.Visible = true;
            // 
            // gridBand12
            // 
            this.gridBand12.Caption = "Total";
            this.gridBand12.Columns.Add(this.bandedGridColumnD43TotalValue);
            this.gridBand12.Columns.Add(this.bandedGridColumnD43TotalPercentage);
            this.gridBand12.Name = "gridBand12";
            this.gridBand12.Width = 150;
            // 
            // bandedGridColumnD43TotalValue
            // 
            this.bandedGridColumnD43TotalValue.Caption = "bandedGridColumn10";
            this.bandedGridColumnD43TotalValue.FieldName = "TotalValue";
            this.bandedGridColumnD43TotalValue.Name = "bandedGridColumnD43TotalValue";
            this.bandedGridColumnD43TotalValue.OptionsColumn.AllowEdit = false;
            this.bandedGridColumnD43TotalValue.SummaryItem.DisplayFormat = "{0}";
            this.bandedGridColumnD43TotalValue.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.bandedGridColumnD43TotalValue.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.bandedGridColumnD43TotalValue.Visible = true;
            // 
            // bandedGridColumnD43TotalPercentage
            // 
            this.bandedGridColumnD43TotalPercentage.Caption = "bandedGridColumn11";
            this.bandedGridColumnD43TotalPercentage.FieldName = "TotalValuePercentage";
            this.bandedGridColumnD43TotalPercentage.Name = "bandedGridColumnD43TotalPercentage";
            this.bandedGridColumnD43TotalPercentage.OptionsColumn.AllowEdit = false;
            this.bandedGridColumnD43TotalPercentage.SummaryItem.DisplayFormat = "{0}";
            this.bandedGridColumnD43TotalPercentage.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom;
            this.bandedGridColumnD43TotalPercentage.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.bandedGridColumnD43TotalPercentage.Visible = true;
            // 
            // bandedGridView3
            // 
            this.bandedGridView3.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand13,
            this.gridBand14,
            this.gridBand15,
            this.gridBand16,
            this.gridBand17,
            this.gridBand21,
            this.gridBand18});
            this.bandedGridView3.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.bandedGridColumnD44Description,
            this.bandedGridColumnD44AssignedValue,
            this.bandedGridColumnD44AssignedPercentage,
            this.bandedGridColumnD44PendingValue,
            this.bandedGridColumnD44PendingPercentage,
            this.bandedGridColumnD44DelayedValue,
            this.bandedGridColumnD44DelayedPercentage,
            this.bandedGridColumnD44InProgressValue,
            this.bandedGridColumnD44InProgressPercentage,
            this.bandedGridColumnD44TotalValue,
            this.bandedGridColumnD44TotalValuePercentage,
            this.bandedGridColumnD44NewValue,
            this.bandedGridColumnD44NewPercentage});
            this.bandedGridView3.GridControl = this.gridControl1;
            this.bandedGridView3.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "totalAssignedValue", this.bandedGridColumnD44AssignedValue, "SUM={0}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "totalAssignedPercentage", this.bandedGridColumnD44AssignedPercentage, "SUM={0}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "totalPendingValue", this.bandedGridColumnD44PendingValue, "SUM={0}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "totalPendingPercentage", this.bandedGridColumnD44PendingPercentage, "SUM={0}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "totalDelayedValue", this.bandedGridColumnD44DelayedValue, "SUM={0}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "totalDelayedPercentage", this.bandedGridColumnD44DelayedPercentage, "SUM={0}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "totalInProgressValue", this.bandedGridColumnD44InProgressValue, "SUM={0}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "totalInProgressPercentage", this.bandedGridColumnD44InProgressPercentage, "SUM={0}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "totalValue", this.bandedGridColumnD44TotalValue, "SUM={0}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "totalValuePercentage", this.bandedGridColumnD44TotalValuePercentage, "SUM={0}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "totalNewValue", this.bandedGridColumnD44NewValue, "SUM={0}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "totalNewPercentage", this.bandedGridColumnD44NewPercentage, "SUM={0}")});
            this.bandedGridView3.Name = "bandedGridView3";
            this.bandedGridView3.OptionsBehavior.AutoPopulateColumns = false;
            this.bandedGridView3.OptionsBehavior.Editable = false;
            this.bandedGridView3.OptionsCustomization.AllowFilter = false;
            this.bandedGridView3.OptionsMenu.EnableColumnMenu = false;
            this.bandedGridView3.OptionsMenu.EnableFooterMenu = false;
            this.bandedGridView3.OptionsMenu.EnableGroupPanelMenu = false;
            this.bandedGridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.bandedGridView3.OptionsView.ShowFooter = true;
            this.bandedGridView3.OptionsView.ShowGroupPanel = false;
            this.bandedGridView3.OptionsView.ShowIndicator = false;
            this.bandedGridView3.DragObjectOver += new DevExpress.XtraGrid.Views.Base.DragObjectOverEventHandler(this.gridView_DragObjectOver);
            // 
            // gridBand13
            // 
            this.gridBand13.Caption = "Nueva";
            this.gridBand13.Columns.Add(this.bandedGridColumnD44Description);
            this.gridBand13.Name = "gridBand13";
            this.gridBand13.Width = 75;
            // 
            // bandedGridColumnD44Description
            // 
            this.bandedGridColumnD44Description.Caption = "bandedGridColumn1";
            this.bandedGridColumnD44Description.FieldName = "Description";
            this.bandedGridColumnD44Description.Name = "bandedGridColumnD44Description";
            this.bandedGridColumnD44Description.OptionsColumn.AllowEdit = false;
            this.bandedGridColumnD44Description.Visible = true;
            // 
            // gridBand14
            // 
            this.gridBand14.Caption = "Nueva";
            this.gridBand14.Columns.Add(this.bandedGridColumnD44AssignedValue);
            this.gridBand14.Columns.Add(this.bandedGridColumnD44AssignedPercentage);
            this.gridBand14.Name = "gridBand14";
            this.gridBand14.Width = 150;
            // 
            // bandedGridColumnD44AssignedValue
            // 
            this.bandedGridColumnD44AssignedValue.Caption = "bandedGridColumn2";
            this.bandedGridColumnD44AssignedValue.FieldName = "AssignedValue";
            this.bandedGridColumnD44AssignedValue.Name = "bandedGridColumnD44AssignedValue";
            this.bandedGridColumnD44AssignedValue.OptionsColumn.AllowEdit = false;
            this.bandedGridColumnD44AssignedValue.SummaryItem.DisplayFormat = "{0}";
            this.bandedGridColumnD44AssignedValue.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.bandedGridColumnD44AssignedValue.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.bandedGridColumnD44AssignedValue.Visible = true;
            // 
            // bandedGridColumnD44AssignedPercentage
            // 
            this.bandedGridColumnD44AssignedPercentage.Caption = "bandedGridColumn3";
            this.bandedGridColumnD44AssignedPercentage.FieldName = "AssignedPercentage";
            this.bandedGridColumnD44AssignedPercentage.Name = "bandedGridColumnD44AssignedPercentage";
            this.bandedGridColumnD44AssignedPercentage.OptionsColumn.AllowEdit = false;
            this.bandedGridColumnD44AssignedPercentage.SummaryItem.DisplayFormat = "{0}";
            this.bandedGridColumnD44AssignedPercentage.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom;
            this.bandedGridColumnD44AssignedPercentage.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.bandedGridColumnD44AssignedPercentage.Visible = true;
            // 
            // gridBand15
            // 
            this.gridBand15.Caption = "Pendientez3";
            this.gridBand15.Columns.Add(this.bandedGridColumnD44PendingValue);
            this.gridBand15.Columns.Add(this.bandedGridColumnD44PendingPercentage);
            this.gridBand15.Name = "gridBand15";
            this.gridBand15.Width = 150;
            // 
            // bandedGridColumnD44PendingValue
            // 
            this.bandedGridColumnD44PendingValue.Caption = "bandedGridColumn4";
            this.bandedGridColumnD44PendingValue.FieldName = "PendingValue";
            this.bandedGridColumnD44PendingValue.Name = "bandedGridColumnD44PendingValue";
            this.bandedGridColumnD44PendingValue.OptionsColumn.AllowEdit = false;
            this.bandedGridColumnD44PendingValue.SummaryItem.DisplayFormat = "{0}";
            this.bandedGridColumnD44PendingValue.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.bandedGridColumnD44PendingValue.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.bandedGridColumnD44PendingValue.Visible = true;
            // 
            // bandedGridColumnD44PendingPercentage
            // 
            this.bandedGridColumnD44PendingPercentage.Caption = "bandedGridColumn5";
            this.bandedGridColumnD44PendingPercentage.FieldName = "PendingPercentage";
            this.bandedGridColumnD44PendingPercentage.Name = "bandedGridColumnD44PendingPercentage";
            this.bandedGridColumnD44PendingPercentage.OptionsColumn.AllowEdit = false;
            this.bandedGridColumnD44PendingPercentage.SummaryItem.DisplayFormat = "{0}";
            this.bandedGridColumnD44PendingPercentage.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom;
            this.bandedGridColumnD44PendingPercentage.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.bandedGridColumnD44PendingPercentage.Visible = true;
            // 
            // gridBand16
            // 
            this.gridBand16.Caption = "Retrasada";
            this.gridBand16.Columns.Add(this.bandedGridColumnD44DelayedValue);
            this.gridBand16.Columns.Add(this.bandedGridColumnD44DelayedPercentage);
            this.gridBand16.Name = "gridBand16";
            this.gridBand16.Width = 150;
            // 
            // bandedGridColumnD44DelayedValue
            // 
            this.bandedGridColumnD44DelayedValue.Caption = "bandedGridColumn6";
            this.bandedGridColumnD44DelayedValue.FieldName = "DelayedValue";
            this.bandedGridColumnD44DelayedValue.Name = "bandedGridColumnD44DelayedValue";
            this.bandedGridColumnD44DelayedValue.OptionsColumn.AllowEdit = false;
            this.bandedGridColumnD44DelayedValue.SummaryItem.DisplayFormat = "{0}";
            this.bandedGridColumnD44DelayedValue.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.bandedGridColumnD44DelayedValue.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.bandedGridColumnD44DelayedValue.Visible = true;
            // 
            // bandedGridColumnD44DelayedPercentage
            // 
            this.bandedGridColumnD44DelayedPercentage.Caption = "bandedGridColumn7";
            this.bandedGridColumnD44DelayedPercentage.FieldName = "DelayedPercentage";
            this.bandedGridColumnD44DelayedPercentage.Name = "bandedGridColumnD44DelayedPercentage";
            this.bandedGridColumnD44DelayedPercentage.OptionsColumn.AllowEdit = false;
            this.bandedGridColumnD44DelayedPercentage.SummaryItem.DisplayFormat = "{0}";
            this.bandedGridColumnD44DelayedPercentage.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom;
            this.bandedGridColumnD44DelayedPercentage.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.bandedGridColumnD44DelayedPercentage.Visible = true;
            // 
            // gridBand17
            // 
            this.gridBand17.Caption = "En proceso";
            this.gridBand17.Columns.Add(this.bandedGridColumnD44InProgressValue);
            this.gridBand17.Columns.Add(this.bandedGridColumnD44InProgressPercentage);
            this.gridBand17.Name = "gridBand17";
            this.gridBand17.Width = 150;
            // 
            // bandedGridColumnD44InProgressValue
            // 
            this.bandedGridColumnD44InProgressValue.Caption = "bandedGridColumn8";
            this.bandedGridColumnD44InProgressValue.FieldName = "InProgressValue";
            this.bandedGridColumnD44InProgressValue.Name = "bandedGridColumnD44InProgressValue";
            this.bandedGridColumnD44InProgressValue.OptionsColumn.AllowEdit = false;
            this.bandedGridColumnD44InProgressValue.SummaryItem.DisplayFormat = "{0}";
            this.bandedGridColumnD44InProgressValue.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.bandedGridColumnD44InProgressValue.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.bandedGridColumnD44InProgressValue.Visible = true;
            // 
            // bandedGridColumnD44InProgressPercentage
            // 
            this.bandedGridColumnD44InProgressPercentage.Caption = "bandedGridColumn9";
            this.bandedGridColumnD44InProgressPercentage.FieldName = "InProgressPercentage";
            this.bandedGridColumnD44InProgressPercentage.Name = "bandedGridColumnD44InProgressPercentage";
            this.bandedGridColumnD44InProgressPercentage.OptionsColumn.AllowEdit = false;
            this.bandedGridColumnD44InProgressPercentage.SummaryItem.DisplayFormat = "{0}";
            this.bandedGridColumnD44InProgressPercentage.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom;
            this.bandedGridColumnD44InProgressPercentage.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.bandedGridColumnD44InProgressPercentage.Visible = true;
            // 
            // gridBand21
            // 
            this.gridBand21.Caption = "No atendida";
            this.gridBand21.Columns.Add(this.bandedGridColumnD44NewValue);
            this.gridBand21.Columns.Add(this.bandedGridColumnD44NewPercentage);
            this.gridBand21.Name = "gridBand21";
            this.gridBand21.Width = 150;
            // 
            // bandedGridColumnD44NewValue
            // 
            this.bandedGridColumnD44NewValue.Caption = "bandedGridColumn12";
            this.bandedGridColumnD44NewValue.FieldName = "NewValue";
            this.bandedGridColumnD44NewValue.Name = "bandedGridColumnD44NewValue";
            this.bandedGridColumnD44NewValue.OptionsColumn.AllowEdit = false;
            this.bandedGridColumnD44NewValue.SummaryItem.DisplayFormat = "{0}";
            this.bandedGridColumnD44NewValue.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.bandedGridColumnD44NewValue.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.bandedGridColumnD44NewValue.Visible = true;
            // 
            // bandedGridColumnD44NewPercentage
            // 
            this.bandedGridColumnD44NewPercentage.Caption = "bandedGridColumn13";
            this.bandedGridColumnD44NewPercentage.FieldName = "NewPercentage";
            this.bandedGridColumnD44NewPercentage.Name = "bandedGridColumnD44NewPercentage";
            this.bandedGridColumnD44NewPercentage.OptionsColumn.AllowEdit = false;
            this.bandedGridColumnD44NewPercentage.SummaryItem.DisplayFormat = "{0}";
            this.bandedGridColumnD44NewPercentage.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom;
            this.bandedGridColumnD44NewPercentage.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.bandedGridColumnD44NewPercentage.Visible = true;
            // 
            // gridBand18
            // 
            this.gridBand18.Caption = "Total";
            this.gridBand18.Columns.Add(this.bandedGridColumnD44TotalValue);
            this.gridBand18.Columns.Add(this.bandedGridColumnD44TotalValuePercentage);
            this.gridBand18.Name = "gridBand18";
            this.gridBand18.Width = 150;
            // 
            // bandedGridColumnD44TotalValue
            // 
            this.bandedGridColumnD44TotalValue.Caption = "bandedGridColumn10";
            this.bandedGridColumnD44TotalValue.FieldName = "TotalValue";
            this.bandedGridColumnD44TotalValue.Name = "bandedGridColumnD44TotalValue";
            this.bandedGridColumnD44TotalValue.OptionsColumn.AllowEdit = false;
            this.bandedGridColumnD44TotalValue.SummaryItem.DisplayFormat = "{0}";
            this.bandedGridColumnD44TotalValue.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.bandedGridColumnD44TotalValue.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.bandedGridColumnD44TotalValue.Visible = true;
            // 
            // bandedGridColumnD44TotalValuePercentage
            // 
            this.bandedGridColumnD44TotalValuePercentage.Caption = "bandedGridColumn11";
            this.bandedGridColumnD44TotalValuePercentage.FieldName = "TotalValuePercentage";
            this.bandedGridColumnD44TotalValuePercentage.Name = "bandedGridColumnD44TotalValuePercentage";
            this.bandedGridColumnD44TotalValuePercentage.OptionsColumn.AllowEdit = false;
            this.bandedGridColumnD44TotalValuePercentage.SummaryItem.DisplayFormat = "{0}";
            this.bandedGridColumnD44TotalValuePercentage.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Custom;
            this.bandedGridColumnD44TotalValuePercentage.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.bandedGridColumnD44TotalValuePercentage.Visible = true;
            // 
            // gridView1
            // 
            this.gridView1.AllowFocusedRowChanged = true;
            this.gridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnIndicator,
            this.gridColumnCondition,
            this.gridColumnRealValue,
            this.gridColumnTrend,
            this.gridColumnCategory});
            this.gridView1.EnablePreviewLineForFocusedRow = true;
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 1;
            this.gridView1.GroupFormat = "[#image]{1} {2}";
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsCustomization.AllowFilter = false;
            this.gridView1.OptionsDetail.ShowDetailTabs = false;
            this.gridView1.OptionsDetail.SmartDetailExpandButtonMode = DevExpress.XtraGrid.Views.Grid.DetailExpandButtonMode.CheckAllDetails;
            this.gridView1.OptionsMenu.EnableColumnMenu = false;
            this.gridView1.OptionsMenu.EnableFooterMenu = false;
            this.gridView1.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.AutoCalcPreviewLineCount = true;
            this.gridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.OptionsView.ShowPreview = true;
            this.gridView1.PreviewFieldName = "Description";
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumnCategory, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.ViewTotalRows = false;
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView1_FocusedRowChanged);
            this.gridView1.DragObjectOver += new DevExpress.XtraGrid.Views.Base.DragObjectOverEventHandler(this.gridView_DragObjectOver);
            // 
            // gridColumnIndicator
            // 
            this.gridColumnIndicator.Caption = "Indicadores";
            this.gridColumnIndicator.FieldName = "Name";
            this.gridColumnIndicator.Name = "gridColumnIndicator";
            this.gridColumnIndicator.OptionsColumn.AllowEdit = false;
            this.gridColumnIndicator.OptionsColumn.ReadOnly = true;
            this.gridColumnIndicator.Visible = true;
            this.gridColumnIndicator.VisibleIndex = 0;
            // 
            // gridColumnCondition
            // 
            this.gridColumnCondition.Caption = "Condicin";
            this.gridColumnCondition.ColumnEdit = this.repositoryItemPictureEdit1;
            this.gridColumnCondition.FieldName = "Threshold";
            this.gridColumnCondition.Name = "gridColumnCondition";
            this.gridColumnCondition.OptionsColumn.AllowEdit = false;
            this.gridColumnCondition.OptionsColumn.ReadOnly = true;
            this.gridColumnCondition.Visible = true;
            this.gridColumnCondition.VisibleIndex = 1;
            // 
            // repositoryItemPictureEdit1
            // 
            this.repositoryItemPictureEdit1.Name = "repositoryItemPictureEdit1";
            // 
            // gridColumnRealValue
            // 
            this.gridColumnRealValue.Caption = "Valor Real";
            this.gridColumnRealValue.FieldName = "RealValue";
            this.gridColumnRealValue.Name = "gridColumnRealValue";
            this.gridColumnRealValue.OptionsColumn.AllowEdit = false;
            this.gridColumnRealValue.OptionsColumn.ReadOnly = true;
            this.gridColumnRealValue.Visible = true;
            this.gridColumnRealValue.VisibleIndex = 2;
            // 
            // gridColumnTrend
            // 
            this.gridColumnTrend.Caption = "Trend";
            this.gridColumnTrend.ColumnEdit = this.repositoryItemPictureEdit1;
            this.gridColumnTrend.FieldName = "Trend";
            this.gridColumnTrend.Name = "gridColumnTrend";
            this.gridColumnTrend.OptionsColumn.AllowEdit = false;
            this.gridColumnTrend.OptionsColumn.ReadOnly = true;
            this.gridColumnTrend.Visible = true;
            this.gridColumnTrend.VisibleIndex = 3;
            // 
            // gridColumnCategory
            // 
            this.gridColumnCategory.FieldName = "Type";
            this.gridColumnCategory.Name = "gridColumnCategory";
            this.gridColumnCategory.OptionsColumn.AllowEdit = false;
            this.gridColumnCategory.OptionsColumn.ReadOnly = true;
            this.gridColumnCategory.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumnCategory.Visible = true;
            this.gridColumnCategory.VisibleIndex = 4;
            // 
            // repositoryItemTimeEdit1
            // 
            this.repositoryItemTimeEdit1.AutoHeight = false;
            this.repositoryItemTimeEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemTimeEdit1.Name = "repositoryItemTimeEdit1";
            this.repositoryItemTimeEdit1.NullText = " ";
            this.repositoryItemTimeEdit1.ReadOnly = true;
            // 
            // IndicatorGridControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControl1);
            this.Name = "IndicatorGridControl";
            this.Size = new System.Drawing.Size(472, 404);
            ((System.ComponentModel.ISupportInitialize)(this.gridViewD01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewD04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewD08)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewD14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewD15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewD30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewD31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewD27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewD28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewD25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal GridViewEx gridView1;
        internal DevExpress.XtraGrid.Columns.GridColumn gridColumnIndicator;
        internal DevExpress.XtraGrid.Columns.GridColumn gridColumnCondition;
        internal DevExpress.XtraGrid.Columns.GridColumn gridColumnRealValue;
        internal DevExpress.XtraGrid.Columns.GridColumn gridColumnTrend;
        internal DevExpress.XtraGrid.Columns.GridColumn gridColumnCategory;
        internal GridControlEx gridControl1;
        internal DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnD01Description;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnD01Time;
        private DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit repositoryItemTimeEdit1;
        internal DevExpress.XtraGrid.Views.Grid.GridView gridViewD01;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnD04Description;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnD04Time;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnD08Description;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnD08RealValue;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnD08Percentage;
        internal DevExpress.XtraGrid.Views.Grid.GridView gridViewD04;
        internal DevExpress.XtraGrid.Views.Grid.GridView gridViewD08;
        internal DevExpress.XtraGrid.Views.Grid.GridView gridViewD14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnD14Description;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnD14Percentage;
        internal DevExpress.XtraGrid.Views.Grid.GridView gridViewD15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnD15Description;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnD15Percentage;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnD14RealValue;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnD15RealValue;
        internal DevExpress.XtraGrid.Views.Grid.GridView gridViewD30;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnD30Description;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnD30RealValue;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnD30Percentage;
        internal DevExpress.XtraGrid.Views.Grid.GridView gridViewD31;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnD31Description;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnD31RealValue;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnD31Percentage;
        internal DevExpress.XtraGrid.Views.Grid.GridView gridViewD27;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnD27Description;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnD27Time;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewD28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnD28Description;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnD28Time;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewD25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnD25Description;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnD25Time;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bandedGridView1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumnD42Description;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumnD42AssignedValue;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumnD42AssignedPercentage;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumnD42PendingValue;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumnD42PendingPercentage;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumnD42DelayedValue;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumnD42DelayedPercentage;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumnD42InProgressValue;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumnD42InProgressPercentage;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumnD42TotalValue;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand5;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumnD42TotalValuePercentage;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bandedGridView2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand7;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnD43Description;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnD43AssignedValue;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnD43AssignedPercentage;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnD43PendingValue;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnD43PendingPercentage;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnD43DelayedValue;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnD43DelayedPercentage;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnD43InProgressValue;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnD43InProgressPercentage;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnD43TotalValue;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnD43TotalPercentage;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand8;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand9;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand10;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand11;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand12;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bandedGridView3;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand13;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnD44Description;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnD44AssignedValue;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnD44AssignedPercentage;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnD44PendingValue;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnD44PendingPercentage;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnD44DelayedValue;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnD44DelayedPercentage;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnD44InProgressValue;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnD44InProgressPercentage;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnD44TotalValue;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnD44TotalValuePercentage;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand14;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand15;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand16;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand17;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand18;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumnD42NewValue;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn gridColumnD42NewPercentage;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand19;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand20;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnD43NewValue;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnD43NewPercentage;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand21;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnD44NewValue;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumnD44NewPercentage;
    }
}
