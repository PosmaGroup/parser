namespace SmartCadGuiCommon.Controls
{
    partial class QuestionsEndReportControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControlQuestionEndReport = new DevExpress.XtraLayout.LayoutControl();
            this.layoutControlGroupQuestionEndReport = new DevExpress.XtraLayout.LayoutControlGroup();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlQuestionEndReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupQuestionEndReport)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControlQuestionEndReport
            // 
            this.layoutControlQuestionEndReport.AllowCustomizationMenu = false;
            this.layoutControlQuestionEndReport.Appearance.ControlDisabled.BackColor = System.Drawing.Color.White;
            this.layoutControlQuestionEndReport.Appearance.ControlDisabled.Options.UseBackColor = true;
            this.layoutControlQuestionEndReport.Appearance.DisabledLayoutGroupCaption.ForeColor = System.Drawing.Color.White;
            this.layoutControlQuestionEndReport.Appearance.DisabledLayoutGroupCaption.Options.UseForeColor = true;
            this.layoutControlQuestionEndReport.Appearance.DisabledLayoutItem.ForeColor = System.Drawing.Color.Black;
            this.layoutControlQuestionEndReport.Appearance.DisabledLayoutItem.Options.UseForeColor = true;
            this.layoutControlQuestionEndReport.AutoScroll = false;
            this.layoutControlQuestionEndReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlQuestionEndReport.Location = new System.Drawing.Point(0, 0);
            this.layoutControlQuestionEndReport.Margin = new System.Windows.Forms.Padding(1);
            this.layoutControlQuestionEndReport.Name = "layoutControlQuestionEndReport";
            this.layoutControlQuestionEndReport.Root = this.layoutControlGroupQuestionEndReport;
            this.layoutControlQuestionEndReport.Size = new System.Drawing.Size(150, 150);
            this.layoutControlQuestionEndReport.TabIndex = 0;
            this.layoutControlQuestionEndReport.Text = "layoutControl1";
            // 
            // layoutControlGroupQuestionEndReport
            // 
            this.layoutControlGroupQuestionEndReport.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroupQuestionEndReport.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupQuestionEndReport.Name = "layoutControlGroupQuestionEndReport";
            this.layoutControlGroupQuestionEndReport.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupQuestionEndReport.Size = new System.Drawing.Size(150, 150);
            this.layoutControlGroupQuestionEndReport.Text = "layoutControlGroupQuestionEndReport";
            // 
            // QuestionsEndReportControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.layoutControlQuestionEndReport);
            this.Name = "QuestionsEndReportControl";
            this.Load += new System.EventHandler(this.QuestionsEndReportControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlQuestionEndReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupQuestionEndReport)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControlQuestionEndReport;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupQuestionEndReport;
    }
}
