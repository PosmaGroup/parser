﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using Smartmatic.SmartCad.Service;
using System.Reflection;
using System.ServiceModel;
using DevExpress.XtraEditors;
using SmartCadControls.SyncBoxes;
using SmartCadCore.ClientData;
using SmartCadCore.Core;
using SmartCadCore.Common;
using SmartCadControls;
using SmartCadControls.Controls;

namespace SmartCadGuiCommon.Controls
{
    public partial class ExtraTimePersonalAssignForm : XtraForm
    {
        #region Fields
        private GridControlSynBox dateSynBox;
        private GridControlSynBox extrasSynBox;
        WorkShiftVariationClientData selectedVariation = null;
        private bool isTimeOff;       
        private object syncCommited = new object();
        private DepartmentTypeClientData department = new DepartmentTypeClientData();
        private List<OperatorClientData> operatorsDifferentApplication = new List<OperatorClientData>();
        #endregion

        public ExtraTimePersonalAssignForm()
        {
            InitializeComponent();
            InitializeDataGrid();
            dateSynBox = new GridControlSynBox(gridControlDate);
            extrasSynBox = new GridControlSynBox(gridControlExtras);
        }

        public ExtraTimePersonalAssignForm(WorkShiftVariationClientData variation, bool isTimeOff, Form MdiParent)
        :this()
        {
            selectUnSelectOperators.LayoutControlItem = 1;
            selectUnSelectOperators.Parameters_Changed += new SelectUnSelectControlDevx.ControlParametersChanged(ExtraTimePersonalAssignParameters_Change);
            selectUnSelectOperators.CheckActive += new SelectUnSelectControlDevx.ControlCheckActive(ExtraTimePersonalAssignParameters_CheckWsActive);
            selectUnSelectOperators.SelectionChanged += new SelectUnSelectControlDevx.GridControlSelectionChanged(selectUnSelectOperators_SelectionChanged);
            this.Tag = MdiParent;
            if (this.Tag != null)
            {
                (this.Tag as SupervisionForm).SupervisionCommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(ExtraTimePersonalAssignForm_SupervisionCommittedChanges);
                if ((this.Tag as SupervisionForm).SelectedDepartment != null)
                    department = (this.Tag as SupervisionForm).SelectedDepartment;
            }
            this.isTimeOff = isTimeOff;         
            this.SelectedVariation = variation;
            LoadLanguage();
        }

      

        private void InitializeDataGrid()
        {
            /****** GridControlExtras ***************/
            gridControlExtras.EnableAutoFilter = true;
            gridControlExtras.Type = typeof(GridControlDataWSVSchedule);
            gridControlExtras.ViewTotalRows = true;
            gridViewProgramExtras.Columns[0].GroupIndex = 0;
            gridViewProgramExtras.Columns[1].Width = 200;

            /****** GridControlDate ******************/
            gridControlDate.EnableAutoFilter = true;
            gridControlDate.Type = typeof(GridControlDataWSVSchedule);
            gridControlDate.ViewTotalRows = true;

            selectUnSelectOperators.InitializeGrids(typeof(GridControlDataOperatorWS));
        }

        public WorkShiftVariationClientData SelectedVariation
        {
            get
            {
                return selectedVariation;
            }
            set
            {
                Clear();
                selectedVariation = value;
                IList operatorsAssigned = new ArrayList();
                IList operatorsNotAssigned = new ArrayList();

                if (selectedVariation != null)
                {
                    textBoxExName.Text = selectedVariation.Name;

                    DateTime dateNow = (DateTime)ServerServiceClient.GetInstance().GetTime();

                    BindingList<GridControlDataWSVSchedule> dataSource = new BindingList<GridControlDataWSVSchedule>();
                    foreach (WorkShiftScheduleVariationClientData sc in selectedVariation.Schedules)
                    {
                        if (sc.End > dateNow)
                              dataSource.Add(new GridControlDataWSVSchedule(sc));
                        
                    }
                    gridViewDate.BeginUpdate();
                    gridControlDate.DataSource = dataSource;
                    gridViewDate.EndUpdate();

                    if (selectedVariation.Operators == null)
                        selectedVariation.Operators = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetOperatorsByWorkShiftVariation, selectedVariation.Code));

                    if (selectedVariation.Operators != null)
                    {
                        IList roles = new ArrayList();
                        string application = ((SupervisionForm)this.Tag).SupervisedApplicationName;

                        if (application == UserApplicationClientData.FirstLevel.Name)
                        {
                            roles = (IList)ServerServiceClient.GetInstance().SearchBasicObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetRolesCodeByAccessWithoutGeneralSupervisor, "UserApplicationData+Basic+FirstLevel", "FirstLevelSupervisor"));

                            foreach (OperatorClientData ope in selectedVariation.Operators)
                            {
                                if (roles.Contains(ope.RoleCode) == true)
                                    operatorsAssigned.Add(new GridControlDataOperatorWS(ope));
                                else
                                {
                                    operatorsDifferentApplication.Add(ope);
                                }
                            }

                        }
                        else if (application == UserApplicationClientData.Dispatch.Name)
                        {
                            roles = (IList)ServerServiceClient.GetInstance().SearchBasicObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetRolesCodeByAccessWithoutGeneralSupervisor, "UserApplicationData+Basic+Dispatch", "DispatchSupervisor"));

                            foreach (OperatorClientData ope in selectedVariation.Operators)
                            {
                                if (roles.Contains(ope.RoleCode) == true && (ope.DepartmentTypes != null && ope.DepartmentTypes.Contains(department) == true))
                                    operatorsAssigned.Add(new GridControlDataOperatorWS(ope));
                                else
                                {
                                    operatorsDifferentApplication.Add(ope);
                                }
                            }
                        }
                    }
                }

                operatorsNotAssigned = GetNotAssignedOperators();

                selectUnSelectOperators.InitializeData(operatorsNotAssigned, operatorsAssigned);

                ExtraTimePersonalAssignParameters_Change(null, null);
            }
        }
      
        
        private void Clear()
        {
            gridControlExtras.DataSource = null;
            gridControlDate.DataSource = null;
            selectUnSelectOperators.gridControlExNotSelected.ClearData();
            selectUnSelectOperators.gridControlExSelected.ClearData();            
        }

        protected override void OnLoad(EventArgs e)
        {   
            if (isTimeOff == false)
            {
                this.layoutControlExtraTime.Text = ResourceLoader.GetString2("ExtraTime");
                gridViewProgramExtras.Columns[0].Caption = ResourceLoader.GetString2("WorkShift");
                this.layoutControlGroupextrasAssigned.Text = ResourceLoader.GetString2("CreateWorkShiftApply");
                Text = ResourceLoader.GetString2("WorkShiftPersonalAssignFormText");
                buttonOk.Text = ResourceLoader.GetString2("VariationEditButtonOkText");
                ButtonCancel.Text = ResourceLoader.GetString2("$Message.Cancel");
                Icon = ResourceLoader.GetIcon("$Icon.Personal");
            }
            else
            {
                layoutControlExtraTime.Text = ResourceLoader.GetString2("FreeTimeFormGroupText");
                gridViewProgramExtras.Columns[0].Caption = ResourceLoader.GetString2("VacationOrPermission");
                layoutControlGroupextrasAssigned.Text = ResourceLoader.GetString2("CreateVariationVacationApply");
                Text = ResourceLoader.GetString2("VacationPersonalAssignFormText");
                buttonOk.Text = ResourceLoader.GetString2("VariationEditButtonOkText");
                ButtonCancel.Text = ResourceLoader.GetString2("$Message.Cancel");
                Icon = ResourceLoader.GetIcon("$Icon.Personal");
            }
       }

        void ExtraTimePersonalAssignForm_SupervisionCommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
        {
            try
            {
                if (e.Objects != null && e.Objects.Count > 0)
                {
                    lock (syncCommited)
                    {
                        try
                        {
                            if (ServerServiceClient.GetInstance().OperatorClient == null)
                            { }
                        }
                        catch
                        { }

                        if (e.Objects[0] is WorkShiftVariationClientData)
                        {
                            WorkShiftVariationClientData variation = (WorkShiftVariationClientData)e.Objects[0];

                            if (selectedVariation.Code == variation.Code)
                            {
                                try
                                {
                                    if (e.Action == CommittedDataAction.Update)
                                    {
                                        if (selectedVariation != null)
                                        {
                                            FormUtil.InvokeRequired(this, delegate
                                            {
                                                SelectedVariation = variation;
                                               // SelectedVariation = (WorkShiftVariationClientData)ServerServiceClient.GetInstance().SearchClientObject(SmartCadHqls.GetCustomHql(SmartCadHqls.GetWorkShiftVariationWithSchedulesByCode, selectedVariation.Code));
                                            });
                                        }
                                    }
                                    else if (e.Action == CommittedDataAction.Delete)
                                    {
                                        this.Dispose();
                                    }
                                }
                                catch { }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }

        }

        private void ExtraTimePersonalAssignParameters_Change(object sender, System.EventArgs e)
        {
            //if (this.selectUnSelectOperators.gridControlExSelected.Items.Count > 0)
                //buttonOk.Enabled = true;
            //else
                //buttonOk.Enabled = false;
        }

        public static bool CheckActive(string strArr, string wsName)
        {
            bool retval = false;
            string[] rawStr = null;

            if (strArr != null)
                rawStr = strArr.Split(',');

            if (rawStr.Count() > 0 && wsName != null)
            {
                if (rawStr.Contains(wsName))
                    retval = true;
            }

            return retval;
        }

        private bool ExtraTimePersonalAssignParameters_CheckWsActive(object sender, System.EventArgs e, int OptCode)
        { 
            bool WSActive = false;

            if (OptCode > 0)
            {
                foreach (GridControlDataOperatorWS gridData in selectUnSelectOperators.gridControlExSelected.Items)
                {
                    if (gridData.Operator.Code == OptCode)
                    {                        
                        foreach (WorkShiftVariationClientData ws in gridData.Operator.WorkShifts)
                        {
                            if (selectedVariation.Code == ws.Code)
                            {
                                if (ws.Schedules == null)
                                {
                                    ws.Schedules = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetWorkShiftScheduleVariationsByWSVCode, ws.Code));
                                }
                                foreach (WorkShiftScheduleVariationClientData wssv in ws.Schedules)
                                {
                                    DateTime DateTimeNow = ServerServiceClient.GetInstance().GetTime();
                                    DateTime lastDate = wssv.End;
                                    DateTime startDate = wssv.Start;
                                    if (lastDate > DateTimeNow && startDate <= DateTimeNow)
                                    {
                                        WSActive = true;
                                    }
                                }
                            }                            
                        }
                    }                    
                }
            }

            return WSActive;
        }

        private void LoadLanguage() 
        {
            /****************  DataGrids **************/

            gridColumnOperScheduleDay.Caption = ResourceLoader.GetString2("Day");
            gridColumnOperScheduleDate.Caption = ResourceLoader.GetString2("VariationDate");
            gridColumnOperScheduleStart.Caption = ResourceLoader.GetString2("VariationBegin");
            gridColumnOperScheduleEnd.Caption = ResourceLoader.GetString2("VariationEnd");

            gridColumnScheduleDay.Caption = ResourceLoader.GetString2("Day");
            gridColumnScheduleDate.Caption = ResourceLoader.GetString2("VariationDate");
            gridColumnScheduleStart.Caption = ResourceLoader.GetString2("VariationBegin");
            gridColumnScheduleEnd.Caption = ResourceLoader.GetString2("VariationEnd");

            /******************* Labels **********************/
            this.layoutControlItemName.Text = ResourceLoader.GetString2("Name")+":";
            if (isTimeOff)
                this.layoutControlItemDates.Text = ResourceLoader.GetString2("VacationDetails") + ":";
            else
                this.layoutControlItemDates.Text = ResourceLoader.GetString2("WorkShiftDetails") + ":";

            layoutControlGroupData.Text = ResourceLoader.GetString2("Data");

            
            /******************** GroupBox ****************************/
            this.layoutControlGroupOperators.Text = ResourceLoader.GetString2("VariationPersonalAssign");
        }


      

        /// <summary>
        /// Validate that remove operators or supervisors has assignings.
        /// </summary>
        /// <returns>True if its validate, otherwise false</returns>
        private bool ValidateScheduleVariations()
        {
            bool result = true;
            ArrayList codes = new ArrayList();
            
            //Filter those supervisors or operators remove.
            if (selectedVariation.Operators != null)
            {                
                foreach (OperatorClientData clientData in selectedVariation.Operators)
                {
                    if (selectUnSelectOperators.gridControlExSelected.Items.Contains(new GridControlDataOperatorWS(clientData)) == false && operatorsDifferentApplication.Contains(clientData) == false)
                        codes.Add(clientData.Code);
                }
            }

            if (codes.Count > 0)
            {
                StringBuilder builder = new StringBuilder();
                builder.Append("(");
                foreach (int code in codes)
                {
                    builder.Append(code);
                    builder.Append(",");
                }
                builder.Remove(builder.Length -1, 1);
                builder.Append(")");

				string hql = SmartCadHqls.GetAmmountOfAssignsByOperator;
                long count = (long)ServerServiceClient.GetInstance().SearchBasicObject(SmartCadHqls.GetCustomHql(hql, selectedVariation.Code, builder.ToString()));
                if (count > 0)
                {
                    DialogResult dialog = MessageForm.Show(ResourceLoader.GetString2("SupervisorsOperatorsWithAssign"), MessageFormType.WarningQuestion);
                    if (dialog == DialogResult.No)
                    {
                        DialogResult = DialogResult.None;
                        SelectedVariation = selectedVariation;
                        result = false;
                    }
                }
            }
            return result;
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValidateScheduleVariations() == true)
                {
                    BackgroundProcessForm processForm = new BackgroundProcessForm(this, new MethodInfo[1] { GetType().GetMethod("buttonOk_Click1", BindingFlags.NonPublic | BindingFlags.Instance) }, new object[1][] { new object[0] });
                    processForm.CanThrowError = true;
                    processForm.ShowDialog();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (FaultException ex)
            {
                SelectedVariation = (WorkShiftVariationClientData)ServerServiceClient.GetInstance().SearchClientObject(SmartCadHqls.GetCustomHql(SmartCadHqls.GetWorkShiftVariationWithSchedulesByCode, selectedVariation.Code));
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, MessageFormType.Error);
            }
            catch (Exception ex)
            {
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, ex);
            }
        }

        private void buttonOk_Click1()
        {
            if (selectedVariation != null)
            {
                selectedVariation.Operators = new ArrayList();

                foreach (GridControlDataOperatorWS gridData in selectUnSelectOperators.gridControlExSelected.Items)
                {
                    selectedVariation.Operators.Add(gridData.Operator as OperatorClientData);
                }

                foreach (OperatorClientData oper in operatorsDifferentApplication)
                {
                    selectedVariation.Operators.Add(oper);
                }

                ServerServiceClient.GetInstance().SaveOrUpdateClientData(selectedVariation);
            }
        }

        #region ListsBoxes


        private IList GetNotAssignedOperators()
        {
           
            this.selectUnSelectOperators.gridControlExSelected.ClearData();
            ArrayList operators = new ArrayList();
            IList dataSource = new ArrayList();

            if (((SupervisionForm)this.Tag).SupervisedApplicationName == UserApplicationClientData.FirstLevel.Name)
            {
                operators = (ArrayList)ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetFirstLevelOperatorsWithWorkShifts);
            }
            else if (((SupervisionForm)this.Tag).SupervisedApplicationName == UserApplicationClientData.Dispatch.Name)
            {
                operators = (ArrayList)ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(
                                                                                SmartCadHqls.GetDispatchOperatorsWithWorkShifts,
                                                                                department.Code));
            }                                 

            if (operators != null)
            {
                foreach (OperatorClientData oper in operators)
                {
                    if (this.selectedVariation == null || this.selectedVariation.Operators.Contains(oper) == false)
                        dataSource.Add(new GridControlDataOperatorWS(oper));
                }
            }

            return dataSource;
      
        }

        private void selectUnSelectOperators_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (sender != null) {

                GridViewEx gridView = (GridViewEx)sender;

                if (gridView.FocusedRowHandle > -1)
                {
                    ShowOperatorVariations((OperatorClientData)((GridControlDataOperatorWS)gridView.GetRow(gridView.FocusedRowHandle)).Operator);
                }
                else 
                {
                    this.gridControlExtras.DataSource = null;
                }
            
            }
        }

        #endregion

        private void ShowOperatorVariations(OperatorClientData oper)
        {
            IList grids = new ArrayList();
            gridControlExtras.DataSource = null;
            if (oper != null)
            {
                IList schedules = new ArrayList();
                IList args = new ArrayList();
                args.Add(oper.Code);

                if (isTimeOff == false)
                {
                    this.layoutControlGroupextrasAssigned.Text = ResourceLoader.GetString2("CreateWorkShiftApply") + " " + oper.FirstName;
                    schedules = (IList)ServerServiceClient.GetInstance().OperatorScheduleManagerMethod("GetWorkTimeByOperator", args);
                }
                else
                {
                    layoutControlGroupextrasAssigned.Text = ResourceLoader.GetString2("CreateVariationVacationApply") + " " + oper.FirstName;
                    schedules = (IList)ServerServiceClient.GetInstance().OperatorScheduleManagerMethod("GetTimeOffByOperator", args);
                }

                if (schedules != null)
                {
                    //We keep the codes of those variation that has not ended.
                    List<int> variationCodes = new List<int>();
                    foreach (WorkShiftScheduleVariationClientData schedule in schedules)
                    {
                        grids.Add(new GridControlDataWSVSchedule(schedule));
                        if (schedule.End > ServerServiceClient.GetInstance().GetTime() &&
                            variationCodes.Contains(schedule.WorkShiftVariationCode) == false)
                            variationCodes.Add(schedule.WorkShiftVariationCode);
                    }
                    //Remove all the parts of the schedules that has ended.
                    for (int index = grids.Count - 1; index >= 0; index--)
                    {
                        GridControlDataWSVSchedule data = (GridControlDataWSVSchedule)grids[index];
                        int variationCode = ((WorkShiftScheduleVariationClientData)data.Tag).WorkShiftVariationCode;
                        if (variationCodes.Contains(variationCode) == false)
                            grids.RemoveAt(index);
                    }

                    gridControlExtras.BeginUpdate();
                    gridControlExtras.DataSource = grids;
                    gridControlExtras.EndUpdate();
                }
            }
        }


        private void ExtraTimePersonalAssignForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.Tag != null)
            {
                (this.Tag as SupervisionForm).SupervisionCommittedChanges -= new EventHandler<CommittedObjectDataCollectionEventArgs>(ExtraTimePersonalAssignForm_SupervisionCommittedChanges);
            }
        }

        private void selectUnSelectOperators_Load(object sender, EventArgs e)
        {

        }      
    }


    public class GridControlDataOperatorWS : GridControlData
    {
        public GridControlDataOperatorWS(OperatorClientData oper)
            : base(oper)
        {
        }

        public OperatorClientData Operator
        {
            get
            {
                return this.Tag as OperatorClientData;
            }
        }

        [GridControlRowInfoData(Width=80, Searchable = true)]
        public string FirstName
        {
            get
            {
                return Operator.FirstName;
            }
        }

        [GridControlRowInfoData(Width=80, Searchable = true)]
        public string LastName
        {
            get
            {
                return Operator.LastName;
            }
        }

        [GridControlRowInfoData(Searchable = false)]
        public string WorkShifts
        {
            get
            {
                string result = string.Empty;
                if (Operator.WorkShifts != null)
                {
                    foreach (WorkShiftVariationClientData ws in Operator.WorkShifts)
                    {
                        if (ws.Schedules == null)
                        {
                            ws.Schedules = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetWorkShiftScheduleVariationsByWSVCode, ws.Code));                            
                        }
                        foreach (WorkShiftScheduleVariationClientData wssv in ws.Schedules)
                        {
                            DateTime DateTimeNow = ServerServiceClient.GetInstance().GetTime();
                            DateTime lastDate = wssv.End;
                            if (lastDate > DateTimeNow)
                            {
                                bool checkWsName = ExtraTimePersonalAssignForm.CheckActive(result, ws.Name);
                                if (checkWsName == false)
                                {
                                    if (ws.Type != WorkShiftVariationClientData.WorkShiftType.TimeOff)
                                        result += ws.Name + ", ";
                                }                                
                            }
                        }
                    }

                    if (result != string.Empty)
                        result = result.Substring(0, result.Length - 2);
                }
                return result;
            }
        }

        
        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is GridControlDataOperatorWS)
            {
                result = Operator.Equals((obj as GridControlDataOperatorWS).Operator);
            }
            return result;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }        
    }
}
