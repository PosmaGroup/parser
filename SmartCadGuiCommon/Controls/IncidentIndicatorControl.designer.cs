namespace SmartCadGuiCommon.Controls
{
    partial class IncidentIndicatorControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraCharts.Series series2 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.PieSeriesLabel pieSeriesLabel3 = new DevExpress.XtraCharts.PieSeriesLabel();
            DevExpress.XtraCharts.PiePointOptions piePointOptions2 = new DevExpress.XtraCharts.PiePointOptions();
            DevExpress.XtraCharts.PieSeriesView pieSeriesView3 = new DevExpress.XtraCharts.PieSeriesView();
            DevExpress.XtraCharts.PieSeriesLabel pieSeriesLabel4 = new DevExpress.XtraCharts.PieSeriesLabel();
            DevExpress.XtraCharts.PieSeriesView pieSeriesView4 = new DevExpress.XtraCharts.PieSeriesView();
            this.chartControl1 = new DevExpress.XtraCharts.ChartControl();
            this.layoutControlIncidentIndicator = new DevExpress.XtraLayout.LayoutControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.incidentGridIndicatorControl1 = new IncidentGridIndicatorControl();
            this.layoutControlItemIncidentGrid = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemChart = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlIncidentIndicator)).BeginInit();
            this.layoutControlIncidentIndicator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemIncidentGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemChart)).BeginInit();
            this.SuspendLayout();
            // 
            // chartControl1
            // 
            this.chartControl1.Location = new System.Drawing.Point(282, 3);
            this.chartControl1.Name = "chartControl1";
            pieSeriesLabel3.LineVisible = true;
            pieSeriesLabel3.Visible = false;
            series2.Label = pieSeriesLabel3;
            piePointOptions2.ValueNumericOptions.Format = DevExpress.XtraCharts.NumericFormat.Percent;
            piePointOptions2.ValueNumericOptions.Precision = 0;
            series2.LegendPointOptions = piePointOptions2;
            series2.Name = "Series 1";
            series2.SynchronizePointOptions = false;
            pieSeriesView3.RuntimeExploding = false;
            series2.View = pieSeriesView3;
            this.chartControl1.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series2};
            pieSeriesLabel4.LineVisible = true;
            this.chartControl1.SeriesTemplate.Label = pieSeriesLabel4;
            pieSeriesView4.RuntimeExploding = false;
            this.chartControl1.SeriesTemplate.View = pieSeriesView4;
            this.chartControl1.Size = new System.Drawing.Size(276, 143);
            this.chartControl1.TabIndex = 2;
            // 
            // layoutControlIncidentIndicator
            // 
            this.layoutControlIncidentIndicator.Appearance.DisabledLayoutGroupCaption.ForeColor = System.Drawing.SystemColors.GrayText;
            this.layoutControlIncidentIndicator.Appearance.DisabledLayoutGroupCaption.Options.UseForeColor = true;
            this.layoutControlIncidentIndicator.Appearance.DisabledLayoutItem.ForeColor = System.Drawing.SystemColors.GrayText;
            this.layoutControlIncidentIndicator.Appearance.DisabledLayoutItem.Options.UseForeColor = true;
            this.layoutControlIncidentIndicator.Controls.Add(this.incidentGridIndicatorControl1);
            this.layoutControlIncidentIndicator.Controls.Add(this.chartControl1);
            this.layoutControlIncidentIndicator.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlIncidentIndicator.Location = new System.Drawing.Point(0, 0);
            this.layoutControlIncidentIndicator.Name = "layoutControlIncidentIndicator";
            this.layoutControlIncidentIndicator.Root = this.layoutControlGroup1;
            this.layoutControlIncidentIndicator.Size = new System.Drawing.Size(560, 148);
            this.layoutControlIncidentIndicator.TabIndex = 3;
            this.layoutControlIncidentIndicator.Text = "layoutControl1";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemIncidentGrid,
            this.layoutControlItemChart});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(560, 148);
            this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // incidentGridIndicatorControl1
            // 
            this.incidentGridIndicatorControl1.DataSource = null;
            this.incidentGridIndicatorControl1.Location = new System.Drawing.Point(3, 3);
            this.incidentGridIndicatorControl1.Name = "incidentGridIndicatorControl1";
            this.incidentGridIndicatorControl1.Size = new System.Drawing.Size(276, 143);
            this.incidentGridIndicatorControl1.TabIndex = 0;
            // 
            // layoutControlItemIncidentGrid
            // 
            this.layoutControlItemIncidentGrid.Control = this.incidentGridIndicatorControl1;
            this.layoutControlItemIncidentGrid.CustomizationFormText = "layoutControlItemIncidentGrid";
            this.layoutControlItemIncidentGrid.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemIncidentGrid.Name = "layoutControlItemIncidentGrid";
            this.layoutControlItemIncidentGrid.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItemIncidentGrid.Size = new System.Drawing.Size(279, 146);
            this.layoutControlItemIncidentGrid.Text = "layoutControlItemIncidentGrid";
            this.layoutControlItemIncidentGrid.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemIncidentGrid.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemIncidentGrid.TextToControlDistance = 0;
            this.layoutControlItemIncidentGrid.TextVisible = false;
            // 
            // layoutControlItemChart
            // 
            this.layoutControlItemChart.Control = this.chartControl1;
            this.layoutControlItemChart.CustomizationFormText = "layoutControlItemChart";
            this.layoutControlItemChart.Location = new System.Drawing.Point(279, 0);
            this.layoutControlItemChart.Name = "layoutControlItemChart";
            this.layoutControlItemChart.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItemChart.Size = new System.Drawing.Size(279, 146);
            this.layoutControlItemChart.Text = "layoutControlItemChart";
            this.layoutControlItemChart.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemChart.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemChart.TextToControlDistance = 0;
            this.layoutControlItemChart.TextVisible = false;
            // 
            // IncidentIndicatorControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControlIncidentIndicator);
            this.LookAndFeel.SkinName = "Blue";
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "IncidentIndicatorControl";
            this.Size = new System.Drawing.Size(560, 148);
            ((System.ComponentModel.ISupportInitialize)(pieSeriesLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlIncidentIndicator)).EndInit();
            this.layoutControlIncidentIndicator.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemIncidentGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemChart)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal IncidentGridIndicatorControl incidentGridIndicatorControl1;
        private DevExpress.XtraCharts.ChartControl chartControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControlIncidentIndicator;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemIncidentGrid;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemChart;
    }
}
