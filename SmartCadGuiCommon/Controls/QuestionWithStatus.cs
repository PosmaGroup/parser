﻿using SmartCadCore.ClientData;
using SmartCadFirstLevel.Gui;
using SmartCadGuiCommon.Enums;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadGuiCommon.Controls
{
    #region QuestionWithStatus
    public class QuestionWithStatus
    {
        #region Fields

        QuestionsControlDevX questionsControl;
        QuestionClientData question;
        QuestionStatusEnum questionStatus;
        bool isPersisted;
        Color statusColor;

        #endregion

        #region Constructors

        public QuestionWithStatus(QuestionClientData question, QuestionStatusEnum questionStatus, QuestionsControlDevX questionsControl)
        {
            this.question = question;
            this.questionStatus = questionStatus;
            this.questionsControl = questionsControl;
            this.isPersisted = false;
        }

        #endregion

        #region Properties

        public bool IsPersisted
        {
            get
            {
                return this.isPersisted;
            }
            set
            {
                this.isPersisted = value;
            }
        }

        public QuestionClientData Question
        {
            get
            {
                return question;
            }
            set
            {
                question = value;
            }
        }

        public QuestionStatusEnum QuestionStatus
        {
            get
            {
                return questionStatus;
            }
            set
            {
                questionStatus = value;
                if (value == QuestionStatusEnum.NotAsked)
                    this.statusColor = questionsControl.NotAskedStatusColor;
                if (value == QuestionStatusEnum.Asking)
                    this.statusColor = questionsControl.AskingStatusColor;
                if (value == QuestionStatusEnum.Asked)
                    this.statusColor = questionsControl.AskedStatusColor;
            }
        }

        public Color StatusColor
        {
            get
            {
                return statusColor;
            }
            set
            {
                statusColor = value;
            }
        }

        #endregion
    }

#endregion
}
