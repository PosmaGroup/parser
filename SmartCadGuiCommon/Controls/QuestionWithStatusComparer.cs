﻿using SmartCadCore.ClientData;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadGuiCommon.Controls
{
    #region QuestionWithStatusComparer

    public class QuestionWithStatusComparer : IComparer
    {
        #region IComparer Members

        public int Compare(object x, object y)
        {
            QuestionWithStatus qws1 = (QuestionWithStatus)x;
            QuestionWithStatus qws2 = (QuestionWithStatus)y;

            if (qws1.Question.RequirementType.HasValue == false)
                return 1;

            if (qws2.Question.RequirementType.HasValue == false)
                return -1;

            if (qws1.Question.RequirementType.Value == RequirementTypeClientEnum.None && qws2.Question.RequirementType.Value == RequirementTypeClientEnum.None)
                return qws1.Question.ShortText.CompareTo(qws2.Question.ShortText);

            int result = qws2.Question.RequirementType.Value.CompareTo(qws1.Question.RequirementType.Value);
            if (result == 0)
                result = qws1.Question.ShortText.CompareTo(qws2.Question.ShortText);

            return result;
        }

        #endregion
    }

    #endregion
}
