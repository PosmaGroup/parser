using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraCharts;
using System.Collections;
using SmartCadCore.Core;
using SmartCadCore.Common;
using SmartCadCore.ClientData;

namespace SmartCadGuiCommon.Controls
{
    public partial class OperatorIndicatorControl2 : DevExpress.XtraEditors.XtraUserControl
    {
       
        public OperatorIndicatorControl2()
        {
            
            InitializeComponent();
            this.operatorGridIndicatorControl2.operatorIndicatorEvent2 += new OperatorIndicatorCharEventHandler2(OperatorGridIndicatorControl_OperatorIndicatorEvent);
            this.chartControl1.CustomDrawSeriesPoint +=new CustomDrawSeriesPointEventHandler(chartControl1_CustomDrawSeriesPoint);
            
            
        }
        private void OperatorGridIndicatorControl_OperatorIndicatorEvent(object sender,
         OperatorIndicatorChartEventArgs2 e)
        {
            bool isEmpty = true;
            if (e.List.Count > 0)
            {
                FormUtil.InvokeRequired(this.chartControl1, delegate
                {
                    this.chartControl1.Series.BeginUpdate();
                    this.chartControl1.Series[0].Points.Clear();
                    this.chartControl1.Series[1].Points.Clear();
                    for (int i = 0; i < e.List.Count; i++)
                    {

                       OperatorGridControlData2 indicator = (e.List[i] as OperatorGridControlData2);
                       

                        //PieSeriesLabel label = chartControl1.Series[0].Label as PieSeriesLabel;
                        //if (label != null)
                        //{
                        //    label.Position = PieSeriesLabelPosition.Radial;
                        //    label.Antialiasing = true;
                        //}

                        PiePointOptions options = chartControl1.Series[0].PointOptions as PiePointOptions;
                        if (options != null)
                        {
                            options.PercentOptions.ValueAsPercent = true;
                            SetNumericOptions(chartControl1.Series[0], NumericFormat.Percent, 0);
                        }

                        SeriesPoint sp = new SeriesPoint();
                        sp.Argument = indicator.Name;
                        sp.Values = new double[] { Convert.ToDouble(indicator.RealValue) };                        
                        this.chartControl1.Series[0].Points.Add(sp);
                        if ((e.List[i] as OperatorGridControlData2).ListResults != null)
                        {
                            
                            for (int j = 0; j < (e.List[i] as OperatorGridControlData2).ListResults.Count; j++)
                            {
                                OperatorGridControlData2 internetlIndicator = (e.List[i] as OperatorGridControlData2).ListResults[j];                                
                                //label = chartControl1.Series[1].Label as PieSeriesLabel;
                                //if (label != null)
                                //{
                                    //label.Position = PieSeriesLabelPosition.Radial;
                                    //label.Antialiasing = true;
                                //}

                                options = chartControl1.Series[1].PointOptions as PiePointOptions;
                                if (options != null)
                                {
                                    options.PercentOptions.ValueAsPercent = true;
                                    SetNumericOptions(chartControl1.Series[1], NumericFormat.Percent, 0);
                                }

                                sp = new SeriesPoint();
                                sp.Argument = internetlIndicator.Name;
                                sp.Values = new double[] { Convert.ToDouble(internetlIndicator.RealValue) };
                                this.chartControl1.Series[1].Points.Add(sp);

                               
                            }
                            isEmpty = true;
                            foreach (SeriesPoint point in this.chartControl1.Series[1].Points)
                            {
                                if (point.Values[0] > 0)
                                {
                                    isEmpty = false;
                                }
                            }
                            if (isEmpty)
                            {
                                this.chartControl1.Series[1].Visible = false;
                            }
                            else
                            {
                                this.chartControl1.Series[1].Visible = true;                                
                            }
                        }
                      

                    }
                  isEmpty = true;
                    foreach (SeriesPoint point in this.chartControl1.Series[0].Points)
                    {
                        if (point.Values[0] > 0)
                        {
                            isEmpty = false;
                        }
                    }
                    if (isEmpty)
                    {
                        this.chartControl1.Series[0].Visible = false;
                    }
                    else
                    {
                        this.chartControl1.Series[0].Visible = true;
                    }
                    this.chartControl1.Series.EndUpdate();

                });
            }
        }


        private void chartControl1_CustomDrawSeriesPoint(object sender, DevExpress.XtraCharts.CustomDrawSeriesPointEventArgs e)
        {


            if (e.SeriesPoint.Argument == ResourceLoader.GetString2("IndicatorD20Name"))
            {
                e.LegendDrawOptions.Color = e.SeriesDrawOptions.Color = OperatorStatusClientData.Ready.Color;
            }
            else if (e.SeriesPoint.Argument == ResourceLoader.GetString2("IndicatorD34Name"))
            {
                e.LegendDrawOptions.Color = e.SeriesDrawOptions.Color = OperatorStatusClientData.Busy.Color;
            }
            else if (e.SeriesPoint.Argument == ResourceLoader.GetString2("IndicatorD35Name"))
            {
                e.LegendDrawOptions.Color = e.SeriesDrawOptions.Color = OperatorStatusClientData.Absent.Color;
            }
            else if (e.SeriesPoint.Argument == ResourceLoader.GetString2("IndicatorD36Name"))
            {
                e.LegendDrawOptions.Color = e.SeriesDrawOptions.Color = OperatorStatusClientData.Reunion.Color;
            }
            else if (e.SeriesPoint.Argument == ResourceLoader.GetString2("IndicatorD37Name"))
            {
                e.LegendDrawOptions.Color = e.SeriesDrawOptions.Color = OperatorStatusClientData.Rest.Color;
            }
            else if (e.SeriesPoint.Argument == ResourceLoader.GetString2("IndicatorD38Name"))
            {
                e.LegendDrawOptions.Color = e.SeriesDrawOptions.Color = OperatorStatusClientData.Bathroom.Color;
            }
            else if (e.SeriesPoint.Argument == ResourceLoader.GetString2("IndicatorPN01Name"))
            {
                e.LegendDrawOptions.Color = e.SeriesDrawOptions.Color = OperatorStatusClientData.Ready.Color;
            }
            else if (e.SeriesPoint.Argument == ResourceLoader.GetString2("IndicatorPN03Name"))
            {
                e.LegendDrawOptions.Color = e.SeriesDrawOptions.Color = OperatorStatusClientData.Busy.Color;
            }
            else if (e.SeriesPoint.Argument == ResourceLoader.GetString2("IndicatorPN15Name"))
            {
                e.LegendDrawOptions.Color = e.SeriesDrawOptions.Color = OperatorStatusClientData.Absent.Color;
            }
            else if (e.SeriesPoint.Argument == ResourceLoader.GetString2("IndicatorPN16Name"))
            {
                e.LegendDrawOptions.Color = e.SeriesDrawOptions.Color = OperatorStatusClientData.Reunion.Color;
            }
            else if (e.SeriesPoint.Argument == ResourceLoader.GetString2("IndicatorPN17Name"))
            {
                e.LegendDrawOptions.Color = e.SeriesDrawOptions.Color = OperatorStatusClientData.Rest.Color;
            }
            else if (e.SeriesPoint.Argument == ResourceLoader.GetString2("IndicatorPN18Name"))
            {
                e.LegendDrawOptions.Color = e.SeriesDrawOptions.Color = OperatorStatusClientData.Bathroom.Color;
            }

            ((PieDrawOptions)e.SeriesDrawOptions).FillStyle.FillMode = FillMode.Solid;


        }
        private void SetNumericOptions(Series series, NumericFormat format, int precision)
        {
            series.PointOptions.ValueNumericOptions.Format = format;
            series.PointOptions.ValueNumericOptions.Precision = precision;
        }

    }
}
