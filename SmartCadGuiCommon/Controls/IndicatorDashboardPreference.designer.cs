
namespace SmartCadGuiCommon.Controls
{
    partial class IndicatorDashboardPreference
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IndicatorDashboardPreference));
            this.radioGroupSupervisor = new DevExpress.XtraEditors.RadioGroup();
            this.layoutControlDashboardPreference = new DevExpress.XtraLayout.LayoutControl();
            this.indicatorDashboardPreferenceClass1 = new IndicatorDashboardPreferenceClass();
            this.simpleButtonApply = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.indicatorDashboardPreferenceClass3 = new IndicatorDashboardPreferenceClass();
            this.indicatorDashboardPreferenceClass2 = new IndicatorDashboardPreferenceClass();
            this.indicatorDashboardPreferenceClassSystem = new IndicatorDashboardPreferenceClass();
            this.simpleButtonAccept = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.tabbedControlGroupPreferences = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroupSystem = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupDepartment = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupMyGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupOperator = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupSupervisor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlDashboardPreference)).BeginInit();
            this.layoutControlDashboardPreference.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroupPreferences)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupSystem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupDepartment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupMyGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupOperator)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            this.SuspendLayout();
            // 
            // radioGroupSupervisor
            // 
            this.radioGroupSupervisor.Location = new System.Drawing.Point(2, 2);
            this.radioGroupSupervisor.Name = "radioGroupSupervisor";
            this.radioGroupSupervisor.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.radioGroupSupervisor.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.radioGroupSupervisor.Properties.Appearance.Options.UseBackColor = true;
            this.radioGroupSupervisor.Properties.Appearance.Options.UseFont = true;
            this.radioGroupSupervisor.Properties.AppearanceFocused.BackColor = System.Drawing.Color.Transparent;
            this.radioGroupSupervisor.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.radioGroupSupervisor.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.radioGroupSupervisor.Properties.Columns = 2;
            this.radioGroupSupervisor.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Supervisor de primer nivel"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Supervisor de despacho")});
            this.radioGroupSupervisor.Size = new System.Drawing.Size(372, 33);
            this.radioGroupSupervisor.StyleController = this.layoutControlDashboardPreference;
            this.radioGroupSupervisor.TabIndex = 0;
            this.radioGroupSupervisor.SelectedIndexChanged += new System.EventHandler(this.radioGroupSupervisor_SelectedIndexChanged);
            // 
            // layoutControlDashboardPreference
            // 
            this.layoutControlDashboardPreference.AllowCustomizationMenu = false;
            this.layoutControlDashboardPreference.Controls.Add(this.simpleButtonApply);
            this.layoutControlDashboardPreference.Controls.Add(this.simpleButtonCancel);
            this.layoutControlDashboardPreference.Controls.Add(this.radioGroupSupervisor);
            this.layoutControlDashboardPreference.Controls.Add(this.indicatorDashboardPreferenceClassSystem);
            this.layoutControlDashboardPreference.Controls.Add(this.simpleButtonAccept);
            this.layoutControlDashboardPreference.Controls.Add(this.indicatorDashboardPreferenceClass1);
            this.layoutControlDashboardPreference.Controls.Add(this.indicatorDashboardPreferenceClass3);
            this.layoutControlDashboardPreference.Controls.Add(this.indicatorDashboardPreferenceClass2);
            this.layoutControlDashboardPreference.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlDashboardPreference.Location = new System.Drawing.Point(0, 0);
            this.layoutControlDashboardPreference.Name = "layoutControlDashboardPreference";
            this.layoutControlDashboardPreference.Root = this.layoutControlGroup1;
            this.layoutControlDashboardPreference.Size = new System.Drawing.Size(508, 482);
            this.layoutControlDashboardPreference.TabIndex = 4;
            this.layoutControlDashboardPreference.Text = "layoutControl1";
            // 
            // indicatorDashboardPreferenceClass1
            // 
            this.indicatorDashboardPreferenceClass1.Location = new System.Drawing.Point(6, 63);
            this.indicatorDashboardPreferenceClass1.Name = "indicatorDashboardPreferenceClass1";
            this.indicatorDashboardPreferenceClass1.Size = new System.Drawing.Size(496, 377);
            this.indicatorDashboardPreferenceClass1.TabIndex = 1;
            // 
            // simpleButtonApply
            // 
            this.simpleButtonApply.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonApply.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButtonApply.Appearance.Options.UseFont = true;
            this.simpleButtonApply.Enabled = false;
            this.simpleButtonApply.Location = new System.Drawing.Point(424, 448);
            this.simpleButtonApply.Name = "simpleButtonApply";
            this.simpleButtonApply.Size = new System.Drawing.Size(82, 32);
            this.simpleButtonApply.StyleController = this.layoutControlDashboardPreference;
            this.simpleButtonApply.TabIndex = 2;
            this.simpleButtonApply.Text = "Button3";
            this.simpleButtonApply.Click += new System.EventHandler(this.simpleButtonApply_Click);
            // 
            // simpleButtonCancel
            // 
            this.simpleButtonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonCancel.Location = new System.Drawing.Point(338, 448);
            this.simpleButtonCancel.Name = "simpleButtonCancel";
            this.simpleButtonCancel.Size = new System.Drawing.Size(82, 32);
            this.simpleButtonCancel.StyleController = this.layoutControlDashboardPreference;
            this.simpleButtonCancel.TabIndex = 1;
            this.simpleButtonCancel.Text = "Button2";
            // 
            // indicatorDashboardPreferenceClass3
            // 
            this.indicatorDashboardPreferenceClass3.Location = new System.Drawing.Point(6, 63);
            this.indicatorDashboardPreferenceClass3.Name = "indicatorDashboardPreferenceClass3";
            this.indicatorDashboardPreferenceClass3.Size = new System.Drawing.Size(496, 377);
            this.indicatorDashboardPreferenceClass3.TabIndex = 1;
            // 
            // indicatorDashboardPreferenceClass2
            // 
            this.indicatorDashboardPreferenceClass2.Location = new System.Drawing.Point(6, 63);
            this.indicatorDashboardPreferenceClass2.Name = "indicatorDashboardPreferenceClass2";
            this.indicatorDashboardPreferenceClass2.Size = new System.Drawing.Size(496, 377);
            this.indicatorDashboardPreferenceClass2.TabIndex = 1;
            // 
            // indicatorDashboardPreferenceClassSystem
            // 
            this.indicatorDashboardPreferenceClassSystem.Location = new System.Drawing.Point(6, 63);
            this.indicatorDashboardPreferenceClassSystem.Name = "indicatorDashboardPreferenceClassSystem";
            this.indicatorDashboardPreferenceClassSystem.Size = new System.Drawing.Size(496, 377);
            this.indicatorDashboardPreferenceClassSystem.TabIndex = 0;
            // 
            // simpleButtonAccept
            // 
            this.simpleButtonAccept.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonAccept.Location = new System.Drawing.Point(252, 448);
            this.simpleButtonAccept.Name = "simpleButtonAccept";
            this.simpleButtonAccept.Size = new System.Drawing.Size(82, 32);
            this.simpleButtonAccept.StyleController = this.layoutControlDashboardPreference;
            this.simpleButtonAccept.TabIndex = 0;
            this.simpleButtonAccept.Text = "Button1";
            this.simpleButtonAccept.Click += new System.EventHandler(this.simpleButtonApply_Click);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.tabbedControlGroupPreferences});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(508, 482);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.radioGroupSupervisor;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(376, 37);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(376, 37);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(376, 37);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.simpleButtonAccept;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(250, 446);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.simpleButtonCancel;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(336, 446);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.simpleButtonApply;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(422, 446);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(376, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(132, 37);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 446);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(250, 36);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // tabbedControlGroupPreferences
            // 
            this.tabbedControlGroupPreferences.CustomizationFormText = "tabbedControlGroupPreferences";
            this.tabbedControlGroupPreferences.Location = new System.Drawing.Point(0, 37);
            this.tabbedControlGroupPreferences.Name = "tabbedControlGroupPreferences";
            this.tabbedControlGroupPreferences.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.tabbedControlGroupPreferences.SelectedTabPage = this.layoutControlGroupSystem;
            this.tabbedControlGroupPreferences.SelectedTabPageIndex = 0;
            this.tabbedControlGroupPreferences.Size = new System.Drawing.Size(508, 409);
            this.tabbedControlGroupPreferences.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupSystem,
            this.layoutControlGroupDepartment,
            this.layoutControlGroupMyGroup,
            this.layoutControlGroupOperator});
            this.tabbedControlGroupPreferences.Text = "tabbedControlGroupPreferences";
            this.tabbedControlGroupPreferences.SelectedPageChanged += new DevExpress.XtraLayout.LayoutTabPageChangedEventHandler(this.tabbedControlGroupPreferences_SelectedPageChanged);
            // 
            // layoutControlGroupSystem
            // 
            this.layoutControlGroupSystem.AppearanceGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroupSystem.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroupSystem.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlGroupSystem.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlGroupSystem.AppearanceTabPage.Header.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlGroupSystem.AppearanceTabPage.Header.Options.UseFont = true;
            this.layoutControlGroupSystem.AppearanceTabPage.HeaderActive.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlGroupSystem.AppearanceTabPage.HeaderActive.Options.UseFont = true;
            this.layoutControlGroupSystem.AppearanceTabPage.HeaderDisabled.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlGroupSystem.AppearanceTabPage.HeaderDisabled.Options.UseFont = true;
            this.layoutControlGroupSystem.AppearanceTabPage.HeaderHotTracked.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlGroupSystem.AppearanceTabPage.HeaderHotTracked.Options.UseFont = true;
            this.layoutControlGroupSystem.AppearanceTabPage.PageClient.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlGroupSystem.AppearanceTabPage.PageClient.Options.UseFont = true;
            this.layoutControlGroupSystem.CustomizationFormText = "layoutControlGroupSystem";
            this.layoutControlGroupSystem.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.layoutControlGroupSystem.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupSystem.Name = "layoutControlGroupSystem";
            this.layoutControlGroupSystem.Size = new System.Drawing.Size(498, 379);
            this.layoutControlGroupSystem.Text = "layoutControlGroupSystem";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.indicatorDashboardPreferenceClassSystem;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem2.Size = new System.Drawing.Size(498, 379);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlGroupDepartment
            // 
            this.layoutControlGroupDepartment.AppearanceGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlGroupDepartment.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroupDepartment.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlGroupDepartment.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlGroupDepartment.AppearanceTabPage.Header.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlGroupDepartment.AppearanceTabPage.Header.Options.UseFont = true;
            this.layoutControlGroupDepartment.AppearanceTabPage.HeaderActive.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlGroupDepartment.AppearanceTabPage.HeaderActive.Options.UseFont = true;
            this.layoutControlGroupDepartment.AppearanceTabPage.HeaderDisabled.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlGroupDepartment.AppearanceTabPage.HeaderDisabled.Options.UseFont = true;
            this.layoutControlGroupDepartment.AppearanceTabPage.HeaderHotTracked.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlGroupDepartment.AppearanceTabPage.HeaderHotTracked.Options.UseFont = true;
            this.layoutControlGroupDepartment.AppearanceTabPage.PageClient.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlGroupDepartment.AppearanceTabPage.PageClient.Options.UseFont = true;
            this.layoutControlGroupDepartment.CustomizationFormText = "layoutControlGroupDepartment";
            this.layoutControlGroupDepartment.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem6});
            this.layoutControlGroupDepartment.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupDepartment.Name = "layoutControlGroupDepartment";
            this.layoutControlGroupDepartment.Size = new System.Drawing.Size(498, 379);
            this.layoutControlGroupDepartment.Text = "layoutControlGroupDepartment";
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.indicatorDashboardPreferenceClass1;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem6.Size = new System.Drawing.Size(498, 379);
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlGroupMyGroup
            // 
            this.layoutControlGroupMyGroup.AppearanceGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlGroupMyGroup.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroupMyGroup.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlGroupMyGroup.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlGroupMyGroup.AppearanceTabPage.Header.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlGroupMyGroup.AppearanceTabPage.Header.Options.UseFont = true;
            this.layoutControlGroupMyGroup.AppearanceTabPage.HeaderActive.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlGroupMyGroup.AppearanceTabPage.HeaderActive.Options.UseFont = true;
            this.layoutControlGroupMyGroup.AppearanceTabPage.HeaderDisabled.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlGroupMyGroup.AppearanceTabPage.HeaderDisabled.Options.UseFont = true;
            this.layoutControlGroupMyGroup.AppearanceTabPage.HeaderHotTracked.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlGroupMyGroup.AppearanceTabPage.HeaderHotTracked.Options.UseFont = true;
            this.layoutControlGroupMyGroup.AppearanceTabPage.PageClient.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlGroupMyGroup.AppearanceTabPage.PageClient.Options.UseFont = true;
            this.layoutControlGroupMyGroup.CustomizationFormText = "layoutControlGroupMyGroup";
            this.layoutControlGroupMyGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem7});
            this.layoutControlGroupMyGroup.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupMyGroup.Name = "layoutControlGroupMyGroup";
            this.layoutControlGroupMyGroup.Size = new System.Drawing.Size(498, 379);
            this.layoutControlGroupMyGroup.Text = "layoutControlGroupMyGroup";
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.indicatorDashboardPreferenceClass2;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem7.Size = new System.Drawing.Size(498, 379);
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlGroupOperator
            // 
            this.layoutControlGroupOperator.AppearanceGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlGroupOperator.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroupOperator.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlGroupOperator.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlGroupOperator.AppearanceTabPage.Header.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlGroupOperator.AppearanceTabPage.Header.Options.UseFont = true;
            this.layoutControlGroupOperator.AppearanceTabPage.HeaderActive.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlGroupOperator.AppearanceTabPage.HeaderActive.Options.UseFont = true;
            this.layoutControlGroupOperator.AppearanceTabPage.HeaderDisabled.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlGroupOperator.AppearanceTabPage.HeaderDisabled.Options.UseFont = true;
            this.layoutControlGroupOperator.AppearanceTabPage.HeaderHotTracked.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlGroupOperator.AppearanceTabPage.HeaderHotTracked.Options.UseFont = true;
            this.layoutControlGroupOperator.AppearanceTabPage.PageClient.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.layoutControlGroupOperator.AppearanceTabPage.PageClient.Options.UseFont = true;
            this.layoutControlGroupOperator.CustomizationFormText = "layoutControlGroupOperator";
            this.layoutControlGroupOperator.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem8});
            this.layoutControlGroupOperator.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupOperator.Name = "layoutControlGroupOperator";
            this.layoutControlGroupOperator.Size = new System.Drawing.Size(498, 379);
            this.layoutControlGroupOperator.Text = "layoutControlGroupOperator";
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.indicatorDashboardPreferenceClass3;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem8.Size = new System.Drawing.Size(498, 379);
            this.layoutControlItem8.Text = "layoutControlItem8";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            // 
            // IndicatorDashboardPreference
            // 
            this.AcceptButton = this.simpleButtonAccept;
            this.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.simpleButtonCancel;
            this.ClientSize = new System.Drawing.Size(508, 482);
            this.Controls.Add(this.layoutControlDashboardPreference);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(516, 516);
            this.Name = "IndicatorDashboardPreference";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "IndicatorDashboardPreference";
            this.Load += new System.EventHandler(this.IndicatorDashboardPreference_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupSupervisor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlDashboardPreference)).EndInit();
            this.layoutControlDashboardPreference.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroupPreferences)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupSystem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupDepartment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupMyGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupOperator)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.RadioGroup radioGroupSupervisor;
        private DevExpress.XtraEditors.SimpleButton simpleButtonApply;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCancel;
        private DevExpress.XtraEditors.SimpleButton simpleButtonAccept;
        private IndicatorDashboardPreferenceClass indicatorDashboardPreferenceClassSystem;
        private IndicatorDashboardPreferenceClass indicatorDashboardPreferenceClass1;
        private IndicatorDashboardPreferenceClass indicatorDashboardPreferenceClass2;
        private IndicatorDashboardPreferenceClass indicatorDashboardPreferenceClass3;
        private DevExpress.XtraLayout.LayoutControl layoutControlDashboardPreference;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroupPreferences;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupDepartment;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupSystem;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupMyGroup;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupOperator;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
    }
}