using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.ServiceModel;
using System.Collections;
using Smartmatic.SmartCad.Service;
using System.Reflection;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraEditors;
using SmartCadCore.Common;
using SmartCadCore.Core;
using SmartCadCore.ClientData;


namespace SmartCadGuiCommon
{
    public partial class TrainingCourseScheduleForm : XtraForm
    {
        private TrainingCourseGridData selectedCourse;
        private TrainingCourseScheduleGridData selectedSchedule;
        private TrainingCoursePartsGridData selectedPart;
        private BindingList<TrainingCourseScheduleGridData> schedules = new BindingList<TrainingCourseScheduleGridData>();
        private BindingList<TrainingCoursePartsGridData> parts = new BindingList<TrainingCoursePartsGridData>();
        private ArrayList schedulesToDelete = new ArrayList();
        private ArrayList partsToDelete = new ArrayList();
        private object syncCommited = new object();
        private bool buttonPressed = false;
        private string invalidMessage = string.Empty;
        private DateTime realStartSelectedSchedule = DateTime.MaxValue;
        private DateTime realEndSelectedSchedule = DateTime.MinValue;
        private bool noOperators = false;
        private System.Threading.Timer timer = null;

        public TrainingCourseScheduleForm()
        {
            InitializeComponent();
            LoadLanguage();
        }

        private void LoadLanguage()
        {
            this.Text = ResourceLoader.GetString2("TrainingCourseScheduleForm");

            this.layoutControlGroupCourseInf.Text = ResourceLoader.GetString2("TrainingCourseInformation");
            this.layoutControlItemName.Text = ResourceLoader.GetString2("Name") + ":";

            this.layoutControlGroupSchedules.Text = ResourceLoader.GetString2("Schedules");
            gridColumnName.Caption = ResourceLoader.GetString2("Name");
            gridColumnStart.Caption = ResourceLoader.GetString2("StartDate");
            gridColumnEnd.Caption = ResourceLoader.GetString2("EndDate");
            gridColumnTrainer.Caption = ResourceLoader.GetString2("Trainer");
            gridColumnStatus.Caption = ResourceLoader.GetString2("Status");

            this.layoutControlGroupParts.Text = ResourceLoader.GetString2("Intervals");
            gridColumnRoom.Caption = ResourceLoader.GetString2("LocationColumn");
            gridColumnPartsStart.Caption = ResourceLoader.GetString2("StartDate");
            gridColumnPartsEnd.Caption = ResourceLoader.GetString2("EndDate");

            toolStripButtonAddSchedule.ToolTipText = ResourceLoader.GetString2("AddSchedule");
            toolStripButtonRemoveSchedule.ToolTipText = ResourceLoader.GetString2("DeleteSchedule");
            toolStripButtonAdd.ToolTipText = ResourceLoader.GetString2("AddSchedulePart");
            toolStripButtonDelete.ToolTipText = ResourceLoader.GetString2("DeleteSchedulePart");

            buttonExOk.Text = ResourceLoader.GetString2("Accept");
            buttonExCancel.Text = ResourceLoader.GetString2("Cancel");
            buttonExApply.Text = ResourceLoader.GetString2("Apply");
        }

        public TrainingCourseScheduleForm(TrainingCourseGridData course, BindingList<TrainingCourseScheduleGridData> list, TrainingCourseScheduleGridData selected)
            : this()
        {
            selectedCourse = course;
            selectedSchedule = selected;
            schedules = new BindingList<TrainingCourseScheduleGridData>(list);
            LoadSchedules();
            this.textBoxExCourse.Text = course.Name;

            gridViewSchedules.BeginInit();
            gridControlSchedules.DataSource = schedules;
            gridViewSchedules.EndInit();
			gridControlSchedules.ViewTotalRows = true;
			gridControlParts.ViewTotalRows = true;

            gridViewParts.Columns["StartDate"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            gridViewParts.Columns["StartDate"].DisplayFormat.FormatString = "MM/dd/yyyy HH:mm";
            gridViewParts.Columns["EndDate"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            gridViewParts.Columns["EndDate"].DisplayFormat.FormatString = "MM/dd/yyyy HH:mm";

            gridViewSchedules.Columns["StartDate"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            gridViewSchedules.Columns["StartDate"].DisplayFormat.FormatString = "MM/dd/yyyy";
            gridViewSchedules.Columns["EndDate"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            gridViewSchedules.Columns["EndDate"].DisplayFormat.FormatString = "MM/dd/yyyy";

         //   LoadSchedulesParts();
         
            if (selected != null)
            {
                int index = schedules.IndexOf(selected);
                if (index > -1)
                    gridViewSchedules.FocusedRowHandle = index;
            }
        }

        private void LoadSchedules()
        {
            IList list = (IList)ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetTrainingCourseScheduleByCourseCodeWithScheduleParts, selectedCourse.Data.Code));
            schedules.Clear();
            schedulesToDelete.Clear();
            partsToDelete.Clear();
            foreach (TrainingCourseScheduleClientData client in list)
            {
                schedules.Add(new TrainingCourseScheduleGridData(client));
            }
        }

        private void LoadSchedulesParts()
        {
            foreach(TrainingCourseScheduleGridData gridData in schedules)
            {
                IList list = (IList)ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetTrainingCourseSchedulePartsByScheduleCode, gridData.Data.Code));
                gridData.Data.Parts = list;
            }
        }

        private void Clear()
        {
            schedules.Clear();
        }

        private int codeTemp = -1;
        private void toolStripButtonAdd_Click(object sender, EventArgs e)
        {
            if (gridViewSchedules.SelectedRowsCount > 0 && gridViewSchedules.FocusedRowHandle != DevExpress.XtraGrid.GridControl.AutoFilterRowHandle && selectedSchedule != null)
            {
                selectedSchedule = gridViewSchedules.GetFocusedRow() as TrainingCourseScheduleGridData;
                TrainingCourseSchedulePartsClientData schedulePart = new TrainingCourseSchedulePartsClientData();
                if (selectedSchedule.StartDate > ServerServiceClient.GetInstance().GetTime())
                    schedulePart.Start = selectedSchedule.StartDate;
                else
                    schedulePart.Start = ServerServiceClient.GetInstance().GetTime();
                schedulePart.End = selectedSchedule.EndDate;
                schedulePart.Code = codeTemp;
                codeTemp--;
                TrainingCoursePartsGridData gridData = new TrainingCoursePartsGridData(schedulePart);
                parts.Add(gridData);
                int index = parts.IndexOf(gridData);
                if (index > -1)
                    gridViewParts.FocusedRowHandle = index;
                selectedSchedule.Data.Parts.Add(schedulePart);
                selectedPart = gridData;
                ButtonActivation(null, null);
                ApplyButtonEnable();
            }
        }

        private void toolStripButtonDelete_Click(object sender, EventArgs e)
        {
            if (gridViewParts.SelectedRowsCount > 0 && gridViewParts.FocusedRowHandle != DevExpress.XtraGrid.GridControl.AutoFilterRowHandle)
            {
                selectedSchedule = gridViewSchedules.GetFocusedRow() as TrainingCourseScheduleGridData;
                 selectedPart = gridViewParts.GetFocusedRow() as TrainingCoursePartsGridData;
                buttonExApply.Enabled = true;
                selectedSchedule.Data.Parts.Remove(selectedPart.Data);
                if (selectedPart.Data.Code > 0)
                    partsToDelete.Add(selectedPart);
                parts.Remove(selectedPart);
                repositoryItemSpinEditMinimumAttendance.MaxValue = selectedSchedule.Data.Parts.Count;
                toolStripButtonDelete.Enabled =
                   (selectedPart != null) && (selectedPart.StartDate > ServerServiceClient.GetInstance().GetTime() ||
                    selectedPart.Data.Code < 0 ||
                    noOperators == true);
                //&&
                //   parts.Count > 1;
              //  gridViewParts.ClearSelection();
            }
            ButtonActivation(null, null);
            ApplyButtonEnable();
            
            
        }

        private void ButtonActivation(object sender, EventArgs e)
        {
            if (CheckScheduleRequiredFields() == false || CheckSchedulePartsRequiredFields() == false)
            {
                buttonExOk.Enabled = false;
            }
            else
            {
                buttonExOk.Enabled = true;
            }
            buttonExApply.Enabled = CheckScheduleRequiredFields() == true && CheckSchedulePartsRequiredFields() == true;
        }

        private void ApplyButtonEnable()
        {
            buttonExApply.Enabled = CheckScheduleRequiredFields() == true && 
                CheckSchedulePartsRequiredFields() == true && 
                CheckDataChanges() == true;
        }

        private bool CheckDataChanges()
        {
            return true;
        }

        private bool CheckScheduleRequiredFields() 
        {
            if (schedules.Count > 0)
            {
                foreach (TrainingCourseScheduleGridData gridData in schedules)
                {
                    if (string.IsNullOrEmpty(gridData.Name) ||
                        gridData.Data.Parts.Count == 0 ||
                        gridData.StartDate == gridData.EndDate)
                        return false;
                }
            }
            else 
            {
                return false;
            }

            return true;

        }

        private bool CheckSchedulePartsRequiredFields()
        {
            if (parts.Count > 0)
            {
                foreach (TrainingCoursePartsGridData gridData in parts)
                {
                    if (string.IsNullOrEmpty(gridData.Room))
                        return false;
                }
            }
            else 
            {
                return false;
            }
            return true;
        }

        private void buttonExOk_Click(object sender, EventArgs e)
        {
            buttonPressed = true;
            if (buttonExApply.Enabled == true)
            {
                if (ValidateSchedule() == false)
                {
                    DialogResult = DialogResult.None;
                    MessageForm.Show(invalidMessage, MessageFormType.Error);
                    return;
                }
                try
                {
                    BackgroundProcessForm processForm = new BackgroundProcessForm(this, new MethodInfo[1] { GetType().GetMethod("buttonExOK_Click1", BindingFlags.NonPublic | BindingFlags.Instance) }, new object[1][] { new object[0] });
                    processForm.CanThrowError = true;
                    processForm.ShowDialog();
                    buttonExApply.Enabled = false;                    
                }
                catch (FaultException ex)
                {
                    buttonPressed = false;
                    LoadSchedules();
                    DialogResult = DialogResult.None;
                    MessageForm.Show(ex.Message, MessageFormType.Error);
                }
                catch (Exception ex)
                {
                    buttonPressed = false;
                    DialogResult = DialogResult.None;
                    MessageForm.Show(ex.Message, ex);
                }
            }
          
        }

        private void buttonExOK_Click1()
        {
            for (int i = 0; i < partsToDelete.Count && schedulesToDelete.Count > 0; i++)
            {
                TrainingCoursePartsGridData part = partsToDelete[i] as TrainingCoursePartsGridData;
                foreach (TrainingCourseScheduleGridData gridData in schedulesToDelete)
                {
                    if (gridData.Data.Parts.Contains(part.Data) == true)
                    {
                        partsToDelete.RemoveAt(i--);
                    }
                }
            }
            foreach (TrainingCoursePartsGridData part in partsToDelete)
            {
                ServerServiceClient.GetInstance().DeleteClientObject(part.Data);
            }

            foreach (TrainingCourseScheduleGridData schedule in schedulesToDelete)
            {
                ServerServiceClient.GetInstance().DeleteClientObject(schedule.Data);
            }
            partsToDelete.Clear();
            schedulesToDelete.Clear();
            BindingList<TrainingCourseScheduleGridData> aux = new BindingList<TrainingCourseScheduleGridData>(schedules);
            for (int i = 0; i < aux.Count; i++)
            {
                TrainingCourseScheduleGridData gridScheduleData = aux[i];
                TrainingCourseScheduleClientData schedule = gridScheduleData.Data;
                if (schedule.Code < 0)
                    schedule.Code = 0;

                foreach (TrainingCourseSchedulePartsClientData part in schedule.Parts)
                {
                    if (part.Code < 0)
                    {
                        part.Code = 0;
                    }
                }
                schedule = (TrainingCourseScheduleClientData)ServerServiceClient.GetInstance().SaveOrUpdateClientDataWithReturn(schedule);

                FormUtil.InvokeRequired(this, delegate
                {
                    ReLoadSchedules(schedule, i);

                });
            }
        }

        private void ReLoadSchedules(TrainingCourseScheduleClientData savedSchedule, int index)
        {
            TrainingCourseScheduleGridData gridData = new TrainingCourseScheduleGridData(savedSchedule);

            if (selectedSchedule.Data.TrainingCourseCode == selectedCourse.Data.Code)
            {
                if (index > -1)
                {
                    schedules[index] = gridData;
                }
            }            
        }

        private bool ValidateScheduleParts(TrainingCourseScheduleGridData gridData)
        {
            bool result = true;
            IList parts = gridData.Data.Parts;
            if (gridData.StartDate >= gridData.EndDate)
                result = false;
            for (int i = 0; i < parts.Count && result == true; i++)
            {
                TrainingCourseSchedulePartsClientData part1 = parts[i] as TrainingCourseSchedulePartsClientData;

                if (part1.Start.AddSeconds(-part1.Start.Second) >= part1.End.AddSeconds(-part1.End.Second))
                    result = false;

                for (int j = i + 1; j < parts.Count && result == true; j++)
                {
                    TrainingCourseSchedulePartsClientData part2 = parts[j] as TrainingCourseSchedulePartsClientData;
                    if (!((part1.Start < part2.End && part1.End <= part2.Start) || (part1.Start >= part2.End && part1.End > part2.End)))
                        result = false;
                }
            }
            for (int i = 0; i < parts.Count && result == true; i++)
            {
                TrainingCourseSchedulePartsClientData part1 = parts[i] as TrainingCourseSchedulePartsClientData;
                if ((part1.Start.AddSeconds(1) < gridData.Data.Start) ||
                    (gridData.Data.End < part1.End))
                    result = false;
            }
            return result;
        }

        private bool ValidateSchedule() 
        {
            List<string> names = new List<string>();
            for (int i = 0; i < schedules.Count; i ++)
            {
                TrainingCourseScheduleGridData schedule = schedules[i] as TrainingCourseScheduleGridData;

                if (names.Contains(schedule.Name) == true || schedule.StartDate > schedule.EndDate)
                {
                    invalidMessage = ResourceLoader.GetString2("NoValidTrainingCourseSchedule");
                    int index = schedules.IndexOf(schedule);
                    if (index > -1)
                        gridViewSchedules.FocusedRowHandle = index;
                    return false;
                }
                else if (ValidateScheduleParts(schedule) == false)
                {
                    invalidMessage = ResourceLoader.GetString2("NoValidTrainingCourseScheduleParts", schedule.Name);
                    int index = schedules.IndexOf(schedule);
                    if (index > -1)
                        gridViewSchedules.FocusedRowHandle = index;
                    return false;
                }
                names.Add(schedule.Name);
            }
            return true;
        }

        private void buttonExApply_Click(object sender, EventArgs e)
        {
            buttonExOk_Click(null, null);
        }

        private void toolStripButtonAddSchedule_Click(object sender, EventArgs e)
        {
            TrainingCourseScheduleClientData schedule = new TrainingCourseScheduleClientData();
            schedule.Start = ServerServiceClient.GetInstance().GetTime().Date;
            schedule.End = ServerServiceClient.GetInstance().GetTime().Date.AddDays(1).AddSeconds(-1);
            schedule.Code = codeTemp;
            schedule.Parts = new ArrayList();
            schedule.TrainingCourseCode = selectedCourse.Data.Code;
            codeTemp--;
            TrainingCourseScheduleGridData gridData = new TrainingCourseScheduleGridData(schedule);
            schedules.Add(gridData);
            int index = schedules.IndexOf(gridData);
            if (index > -1)
                gridViewSchedules.FocusedRowHandle = index;
            ButtonActivation(null, null);
            ApplyButtonEnable();
        }

        private void toolStripButtonRemoveSchedule_Click(object sender, EventArgs e)
        {
            if (gridViewSchedules.SelectedRowsCount > 0 && gridViewSchedules.FocusedRowHandle != DevExpress.XtraGrid.GridControl.AutoFilterRowHandle)
            {
                selectedSchedule = gridViewSchedules.GetFocusedRow() as TrainingCourseScheduleGridData;

                buttonExApply.Enabled = true;
                if (selectedSchedule.Data.Code > 0)
                    schedulesToDelete.Add(selectedSchedule);
                schedules.Remove(selectedSchedule);
            }
            ButtonActivation(null, null);
            ApplyButtonEnable();
        }

        private void gridViewSchedules_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (e.FocusedRowHandle > -1)
            {
                //timer.Change(5000, 5000);
                selectedSchedule = gridViewSchedules.GetFocusedRow() as TrainingCourseScheduleGridData;
                parts.Clear();
                realEndSelectedSchedule = DateTime.MinValue;
                realStartSelectedSchedule = DateTime.MaxValue;
                foreach (TrainingCourseSchedulePartsClientData schedulePart in selectedSchedule.Data.Parts)
                {
                    TrainingCoursePartsGridData gridData = new TrainingCoursePartsGridData(schedulePart);
                    parts.Add(gridData);

                    if (schedulePart.End > realEndSelectedSchedule)
                        realEndSelectedSchedule = schedulePart.End;
                    if (schedulePart.Start < realStartSelectedSchedule)
                        realStartSelectedSchedule = schedulePart.Start;
                }

                IList result = (IList)ServerServiceClient.GetInstance().SearchBasicObjects(
                    SmartCadHqls.GetCustomHql(
                    SmartCadHqls.GetCountTrainingCourseScheduleEvaluatingorEvaluated,
                    selectedSchedule.Data.Code, 
                    ApplicationUtil.GetDataBaseFormattedDate(ServerServiceClient.GetInstance().GetTime())));
                noOperators = false;
                if (result != null && result.Count > 0)
                {
                    noOperators = (long)result[0] == 0;
                }

                toolStripButtonRemoveSchedule.Enabled = realStartSelectedSchedule > ServerServiceClient.GetInstance().GetTime() || selectedSchedule.Data.Code < 0 || noOperators == true;
                toolStripButtonAdd.Enabled = realEndSelectedSchedule > ServerServiceClient.GetInstance().GetTime() || selectedSchedule.Data.Code < 0;

                if (selectedSchedule.Data.TrainingCourseOperators == null)
                {
                    selectedSchedule.Data.TrainingCourseOperators = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetOperatorsTrainingCourseByScheduleCode, selectedSchedule.Data.Code));   
                }                

                if (realStartSelectedSchedule < ServerServiceClient.GetInstance().GetTime() &&
                    ServerServiceClient.GetInstance().GetTime() < realEndSelectedSchedule &&
                    selectedSchedule.Data.Code >= 0)
                {
                    AllowEditRow(gridViewSchedules, realEndSelectedSchedule == DateTime.MinValue);

                    if (selectedSchedule.Data.TrainingCourseOperators.Count <= 0)
                    {
                        gridColumnName.OptionsColumn.AllowEdit = true;
                        gridColumnStart.OptionsColumn.AllowEdit = true;
                        gridColumnStatus.OptionsColumn.AllowEdit = true;
                        gridColumnEnd.OptionsColumn.AllowEdit = true;
                        gridColumnTrainer.OptionsColumn.AllowEdit = true;
                    }
                    else
                    {
                        gridColumnName.OptionsColumn.AllowEdit = false;
                        gridColumnStart.OptionsColumn.AllowEdit = false;
                        gridColumnStatus.OptionsColumn.AllowEdit = false;
                        gridColumnEnd.OptionsColumn.AllowEdit = true;
                        gridColumnTrainer.OptionsColumn.AllowEdit = true;
                    }                    
                }
                //else
                //{
                //    AllowEditRow(gridViewSchedules,  realEndSelectedSchedule == DateTime.MinValue);
                //}

                repositoryItemDateEditStart.MinValue = ServerServiceClient.GetInstance().GetTime().Date;
                repositoryItemDateEditEnd.MinValue = selectedSchedule.StartDate;

                if (gridControlParts.DataSource == null)
                {
                    gridControlParts.BeginInit();
                    gridControlParts.DataSource = parts;
                    gridControlParts.EndInit();
                    gridViewParts.BestFitColumns();
                }
            }
            else
            {
                selectedSchedule = null;
                parts.Clear();
            }
        }

        private void gridViewSchedules_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column == gridColumnStart)
            {
                repositoryItemDateEditPartStart.MinValue = (DateTime)e.Value;
                repositoryItemDateEditPartEnd.MinValue = (DateTime)e.Value;
                repositoryItemDateEditEnd.MinValue = (DateTime)e.Value;
                if (selectedSchedule.EndDate < selectedSchedule.StartDate)
                {
                    DateTime newDT = (DateTime)e.Value;
                    newDT = newDT.Date.AddDays(1).AddSeconds(-1);
                    selectedSchedule.EndDate = newDT;
                }
            }
            else if (e.Column == gridColumnEnd)
            {
                repositoryItemDateEditPartStart.MaxValue = (DateTime)e.Value;
                repositoryItemDateEditPartEnd.MaxValue = (DateTime)e.Value;
            }

            ButtonActivation(null, null);
        }

        private void gridViewParts_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Value != null)
            {
                if (e.Column == gridColumnRoom)
                    selectedPart.Room = e.Value.ToString();
                else if (e.Column == gridColumnPartsStart)
                {
                    repositoryItemDateEditPartEnd.MinValue = ((DateTime)e.Value);
                    if (selectedPart.EndDate <= selectedPart.StartDate)
                        selectedPart.EndDate = ((DateTime)e.Value).AddSeconds(1);
                }
                if (selectedPart.EndDate.Date > realEndSelectedSchedule.Date)
                    realEndSelectedSchedule = selectedPart.EndDate;
                if (selectedPart.StartDate.Date < realStartSelectedSchedule.Date)
                    realStartSelectedSchedule = selectedPart.StartDate;
                gridViewSchedules.RefreshRow(gridViewSchedules.FocusedRowHandle);
            }
            
            ButtonActivation(null, null);
        }

        private void gridViewParts_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (e.FocusedRowHandle > -1)
            {
                selectedPart = gridViewParts.GetFocusedRow() as TrainingCoursePartsGridData;
                
                toolStripButtonDelete.Enabled = 
                    (selectedPart.StartDate > ServerServiceClient.GetInstance().GetTime() || 
                     selectedPart.Data.Code < 0 || 
                     noOperators == true) && 
                    parts.Count >= 1;

                if (selectedPart.StartDate < ServerServiceClient.GetInstance().GetTime() &&
                    ServerServiceClient.GetInstance().GetTime() < selectedPart.EndDate &&
                    selectedPart.Data.Code >= 0 && noOperators == false)
                {
                    gridColumnRoom.OptionsColumn.AllowEdit = false;
                    gridColumnPartsEnd.OptionsColumn.AllowEdit = false;
                    gridColumnPartsStart.OptionsColumn.AllowEdit = false;
                }
                else
                {
                   AllowEditRow(gridViewParts, selectedPart.EndDate > ServerServiceClient.GetInstance().GetTime() || selectedPart.Data.Code < 0);
                  
                }
                if (selectedPart.Data.TrainingCourseSchedule == null || selectedPart.Data.TrainingCourseSchedule.TrainingCourseOperators == null || selectedPart.Data.TrainingCourseSchedule.TrainingCourseOperators.Count == 0)
                    AllowEditRow(gridViewParts, true);
                repositoryItemDateEditPartEnd.MinValue = selectedPart.StartDate;

                if (selectedSchedule.StartDate > ServerServiceClient.GetInstance().GetTime())
                    repositoryItemDateEditPartStart.MinValue = selectedSchedule.StartDate;
                else
                    repositoryItemDateEditPartStart.MinValue = ServerServiceClient.GetInstance().GetTime();

                repositoryItemDateEditPartEnd.MinValue = selectedPart.StartDate;

                repositoryItemDateEditPartStart.MaxValue = selectedSchedule.EndDate;
                repositoryItemDateEditPartEnd.MaxValue = selectedSchedule.EndDate;
            }
            else
                selectedPart = null;
        }

        private void gridViewSchedules_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Value != null)
            {
                if (e.Column == gridColumnEnd && e.Value is DateTime)
                {
                    try
                    {
                        toolStripButtonAdd.Enabled = Convert.ToDateTime(e.Value) > ServerServiceClient.GetInstance().GetTime();
                    }
                    catch
                    {
                        toolStripButtonAdd.Enabled = false;
                    }
                }
            }
        }

        private void gridViewParts_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {

        }

        private void AllowEditRow(GridView view, bool editable)
        {
            foreach (GridColumn column in view.Columns)
            {
                if (column.FieldName != "Status")
               column.OptionsColumn.AllowEdit = editable;
            }
        }

        private void TrainingCourseScheduleForm_Load(object sender, EventArgs e)
        {
            timer = new System.Threading.Timer(new System.Threading.TimerCallback(OnTimer));
            timer.Change(5000, 5000);
            ServerServiceClient.GetInstance().CommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(TrainingForm_SupervisionCommittedChanges);
        }

        private void TrainingCourseScheduleForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (timer != null)
                timer.Dispose();
            ServerServiceClient.GetInstance().CommittedChanges -= new EventHandler<CommittedObjectDataCollectionEventArgs>(TrainingForm_SupervisionCommittedChanges);
        }

        private void OnTimer(object state)
        {
            FormUtil.InvokeRequired(gridControlSchedules, delegate
            {              
                for (int i = 0; i < gridViewSchedules.RowCount; i++)
                   ((GridView)gridControlSchedules.MainView).SetRowCellValue(i, gridViewSchedules.Columns["Status"], ((TrainingCourseScheduleGridData)gridViewSchedules.GetRow(i)).Status);                  
                
            });
        }

        void TrainingForm_SupervisionCommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
        {
            try
            {
                if (e.Objects != null && e.Objects.Count > 0)
                {
                    lock (syncCommited)
                    {
                        if (e.Objects[0] is TrainingCourseClientData)
                        {
                            TrainingCourseClientData course = (TrainingCourseClientData)e.Objects[0];
                            if (e.Action == CommittedDataAction.Delete && course.Code == selectedCourse.Data.Code && buttonPressed == false)
                            {
                                MessageForm.Show(ResourceLoader.GetString2("DeleteFormTrainingCourseScheduleData"), MessageFormType.Information);
                                DialogResult = DialogResult.Cancel;
                                this.Close();
                            }
                        }
                        else if (e.Objects[0] is TrainingCourseScheduleClientData)
                        {
                            FormUtil.InvokeRequired(gridControlSchedules,
                               delegate
                               {
                                   TrainingCourseScheduleClientData courseSchedule = (TrainingCourseScheduleClientData)e.Objects[0];                                                                      
                                   TrainingCourseScheduleGridData tcgd = new TrainingCourseScheduleGridData(courseSchedule);                                   

                                   if (selectedCourse.Data.Code == tcgd.Data.TrainingCourseCode)
                                   {
                                       if (e.Action == CommittedDataAction.Save ||
                                           e.Action == CommittedDataAction.Update)
                                       {
                                           if (schedules.Contains(tcgd) == false)
                                               schedules.Add(tcgd);
                                           else
                                           {
                                               int index = schedules.IndexOf(tcgd);
                                               schedules[index] = tcgd;
                                           }
                                       }
                                       else
                                       {
                                           if (schedules.Contains(tcgd) == true)
                                               schedules.Remove(tcgd);
                                       }
                                   }                                   

                                   if (selectedSchedule.Data.Code == tcgd.Data.Code)
                                   {
                                       if (buttonPressed == false)
                                           MessageForm.Show(ResourceLoader.GetString2("UpdateFormTrainingCourseScheduleData"), MessageFormType.Information);


                                       gridViewSchedules_FocusedRowChanged(
                                           gridViewSchedules,
                                           new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs(
                                               gridViewSchedules.FocusedRowHandle,
                                               gridViewSchedules.FocusedRowHandle));

                                   }
                               });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }
    }
}
