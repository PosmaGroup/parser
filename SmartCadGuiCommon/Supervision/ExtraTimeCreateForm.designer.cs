using SmartCadControls;
using SmartCadControls.Controls;
using SmartCadGuiCommon.Controls;
namespace SmartCadGuiCommon
{
    partial class ExtraTimeCreateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ExtraTimeCreateForm));
            this.comboBoxExMotive = new DevExpress.XtraEditors.ComboBoxEdit();
            this.layoutControlExtraTime = new DevExpress.XtraLayout.LayoutControl();
            this.comboBoxEditType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.ButtonEmpty = new DevExpress.XtraEditors.SimpleButton();
            this.ButtonDelete = new DevExpress.XtraEditors.SimpleButton();
            this.ButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.textBoxExName = new TextBoxEx();
            this.dateIntervalsControl1 = new DateIntervalsControl();
            this.ButtonAdd = new DevExpress.XtraEditors.SimpleButton();
            this.textBoxExDes = new TextBoxEx();
            this.gridControlSelected = new GridControlEx();
            this.gridViewSelection = new GridViewEx();
            this.gridColumnScheduleDay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnScheduleDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.gridColumnScheduleStart = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTimeEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit();
            this.gridColumnScheduleEnd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.buttonOk = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroupExtraTimeInfo = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemDesc = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemMotive = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupDates = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemSelection = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.styleController1 = new DevExpress.XtraEditors.StyleController(this.components);
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.toolTipController = new DevExpress.Utils.ToolTipController(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxExMotive.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlExtraTime)).BeginInit();
            this.layoutControlExtraTime.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSelected)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSelection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupExtraTimeInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemMotive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupDates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemSelection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.styleController1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // comboBoxExMotive
            // 
            this.comboBoxExMotive.Enabled = false;
            this.comboBoxExMotive.Location = new System.Drawing.Point(144, 81);
            this.comboBoxExMotive.Name = "comboBoxExMotive";
            this.comboBoxExMotive.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxExMotive.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxExMotive.Size = new System.Drawing.Size(293, 20);
            this.comboBoxExMotive.StyleController = this.layoutControlExtraTime;
            this.comboBoxExMotive.TabIndex = 3;
            this.comboBoxExMotive.SelectedIndexChanged += new System.EventHandler(this.comboBoxExMotive_SelectedIndexChanged);
            // 
            // layoutControlExtraTime
            // 
            this.layoutControlExtraTime.AllowCustomizationMenu = false;
            this.layoutControlExtraTime.Controls.Add(this.comboBoxEditType);
            this.layoutControlExtraTime.Controls.Add(this.ButtonEmpty);
            this.layoutControlExtraTime.Controls.Add(this.ButtonDelete);
            this.layoutControlExtraTime.Controls.Add(this.ButtonCancel);
            this.layoutControlExtraTime.Controls.Add(this.comboBoxExMotive);
            this.layoutControlExtraTime.Controls.Add(this.textBoxExName);
            this.layoutControlExtraTime.Controls.Add(this.dateIntervalsControl1);
            this.layoutControlExtraTime.Controls.Add(this.ButtonAdd);
            this.layoutControlExtraTime.Controls.Add(this.textBoxExDes);
            this.layoutControlExtraTime.Controls.Add(this.gridControlSelected);
            this.layoutControlExtraTime.Controls.Add(this.buttonOk);
            this.layoutControlExtraTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlExtraTime.Location = new System.Drawing.Point(2, 2);
            this.layoutControlExtraTime.Name = "layoutControlExtraTime";
            this.layoutControlExtraTime.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(142, 117, 431, 350);
            this.layoutControlExtraTime.Root = this.layoutControlGroup1;
            this.layoutControlExtraTime.Size = new System.Drawing.Size(951, 519);
            this.layoutControlExtraTime.TabIndex = 5;
            this.layoutControlExtraTime.Text = "layoutControl1";
            // 
            // comboBoxEditType
            // 
            this.comboBoxEditType.Location = new System.Drawing.Point(144, 55);
            this.comboBoxEditType.Name = "comboBoxEditType";
            this.comboBoxEditType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEditType.Size = new System.Drawing.Size(293, 20);
            this.comboBoxEditType.StyleController = this.layoutControlExtraTime;
            this.comboBoxEditType.TabIndex = 6;
            this.comboBoxEditType.SelectedIndexChanged += new System.EventHandler(this.comboBoxEditType_SelectedIndexChanged);
            // 
            // ButtonEmpty
            // 
            this.ButtonEmpty.Enabled = false;
            this.ButtonEmpty.Location = new System.Drawing.Point(785, 427);
            this.ButtonEmpty.Name = "ButtonEmpty";
            this.ButtonEmpty.Size = new System.Drawing.Size(73, 26);
            this.ButtonEmpty.StyleController = this.layoutControlExtraTime;
            this.ButtonEmpty.TabIndex = 4;
            this.ButtonEmpty.Text = "Limpiar";
            this.ButtonEmpty.Click += new System.EventHandler(this.ButtonEmpty_Click);
            // 
            // ButtonDelete
            // 
            this.ButtonDelete.Enabled = false;
            this.ButtonDelete.Location = new System.Drawing.Point(868, 427);
            this.ButtonDelete.Name = "ButtonDelete";
            this.ButtonDelete.Size = new System.Drawing.Size(73, 26);
            this.ButtonDelete.StyleController = this.layoutControlExtraTime;
            this.ButtonDelete.TabIndex = 5;
            this.ButtonDelete.Text = "Quitar";
            this.ButtonDelete.Click += new System.EventHandler(this.ButtonDelete_Click);
            // 
            // ButtonCancel
            // 
            this.ButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.ButtonCancel.Location = new System.Drawing.Point(872, 483);
            this.ButtonCancel.Name = "ButtonCancel";
            this.ButtonCancel.Size = new System.Drawing.Size(74, 31);
            this.ButtonCancel.StyleController = this.layoutControlExtraTime;
            this.ButtonCancel.TabIndex = 4;
            // 
            // textBoxExName
            // 
            this.textBoxExName.AllowsLetters = true;
            this.textBoxExName.AllowsNumbers = true;
            this.textBoxExName.AllowsPunctuation = true;
            this.textBoxExName.AllowsSeparators = true;
            this.textBoxExName.AllowsSymbols = true;
            this.textBoxExName.AllowsWhiteSpaces = true;
            this.textBoxExName.ExtraAllowedChars = "";
            this.textBoxExName.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExName.Location = new System.Drawing.Point(144, 29);
            this.textBoxExName.MaxLength = 60;
            this.textBoxExName.Name = "textBoxExName";
            this.textBoxExName.NonAllowedCharacters = "";
            this.textBoxExName.RegularExpresion = "";
            this.textBoxExName.Size = new System.Drawing.Size(293, 20);
            this.textBoxExName.TabIndex = 1;
            this.textBoxExName.TextChanged += new System.EventHandler(this.textBoxExName_TextChanged);
            // 
            // dateIntervalsControl1
            // 
            this.dateIntervalsControl1.Location = new System.Drawing.Point(7, 137);
            this.dateIntervalsControl1.Name = "dateIntervalsControl1";
            this.dateIntervalsControl1.Size = new System.Drawing.Size(485, 268);
            toolTipTitleItem1.Text = "Calendar";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Use the Control or Shift keys to select more than one day when you mouseover on t" +
                "he calendar.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.toolTipController.SetSuperTip(this.dateIntervalsControl1, superToolTip1);
            this.dateIntervalsControl1.TabIndex = 0;
            this.dateIntervalsControl1.Parameters_Change += new DateIntervalsControl.DateIntervalsParameters_Change(this.dateIntervalsControl1_Parameters_Change);
            // 
            // ButtonAdd
            // 
            this.ButtonAdd.Location = new System.Drawing.Point(410, 424);
            this.ButtonAdd.Name = "ButtonAdd";
            this.ButtonAdd.Size = new System.Drawing.Size(82, 32);
            this.ButtonAdd.StyleController = this.layoutControlExtraTime;
            this.ButtonAdd.TabIndex = 1;
            this.ButtonAdd.Text = "Agregar";
            this.ButtonAdd.Click += new System.EventHandler(this.ButtonAdd_Click);
            // 
            // textBoxExDes
            // 
            this.textBoxExDes.AllowsLetters = true;
            this.textBoxExDes.AllowsNumbers = true;
            this.textBoxExDes.AllowsPunctuation = true;
            this.textBoxExDes.AllowsSeparators = true;
            this.textBoxExDes.AllowsSymbols = true;
            this.textBoxExDes.AllowsWhiteSpaces = true;
            this.textBoxExDes.ExtraAllowedChars = "";
            this.textBoxExDes.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExDes.Location = new System.Drawing.Point(586, 29);
            this.textBoxExDes.MaxLength = 300;
            this.textBoxExDes.Multiline = true;
            this.textBoxExDes.Name = "textBoxExDes";
            this.textBoxExDes.NonAllowedCharacters = "";
            this.textBoxExDes.RegularExpresion = "";
            this.textBoxExDes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxExDes.Size = new System.Drawing.Size(355, 72);
            this.textBoxExDes.TabIndex = 5;
            // 
            // gridControlSelected
            // 
            this.gridControlSelected.EnableAutoFilter = false;
            this.gridControlSelected.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlSelected.Location = new System.Drawing.Point(496, 153);
            this.gridControlSelected.LookAndFeel.SkinName = "Blue";
            this.gridControlSelected.MainView = this.gridViewSelection;
            this.gridControlSelected.Name = "gridControlSelected";
            this.gridControlSelected.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTimeEdit1,
            this.repositoryItemDateEdit1});
            this.gridControlSelected.Size = new System.Drawing.Size(448, 267);
            this.gridControlSelected.TabIndex = 3;
            this.gridControlSelected.TabStop = false;
            this.gridControlSelected.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewSelection});
            this.gridControlSelected.ViewTotalRows = true;
            // 
            // gridViewSelection
            // 
            this.gridViewSelection.AllowFocusedRowChanged = true;
            this.gridViewSelection.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewSelection.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewSelection.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewSelection.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewSelection.Appearance.FooterPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewSelection.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewSelection.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewSelection.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewSelection.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewSelection.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewSelection.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnScheduleDay,
            this.gridColumnScheduleDate,
            this.gridColumnScheduleStart,
            this.gridColumnScheduleEnd});
            this.gridViewSelection.EnablePreviewLineForFocusedRow = false;
            this.gridViewSelection.GridControl = this.gridControlSelected;
            this.gridViewSelection.Name = "gridViewSelection";
            this.gridViewSelection.OptionsCustomization.AllowFilter = false;
            this.gridViewSelection.OptionsMenu.EnableFooterMenu = false;
            this.gridViewSelection.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewSelection.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewSelection.OptionsView.ShowFooter = true;
            this.gridViewSelection.OptionsView.ShowGroupPanel = false;
            this.gridViewSelection.OptionsView.ShowIndicator = false;
            this.gridViewSelection.ViewTotalRows = true;
            this.gridViewSelection.DragObjectOver += new DevExpress.XtraGrid.Views.Base.DragObjectOverEventHandler(this.gridView_DragObjectOver);
            // 
            // gridColumnScheduleDay
            // 
            this.gridColumnScheduleDay.Caption = "Dia";
            this.gridColumnScheduleDay.FieldName = "DayOfWeek";
            this.gridColumnScheduleDay.Name = "gridColumnScheduleDay";
            this.gridColumnScheduleDay.OptionsColumn.AllowEdit = false;
            this.gridColumnScheduleDay.Visible = true;
            this.gridColumnScheduleDay.VisibleIndex = 0;
            // 
            // gridColumnScheduleDate
            // 
            this.gridColumnScheduleDate.Caption = "Fecha";
            this.gridColumnScheduleDate.ColumnEdit = this.repositoryItemDateEdit1;
            this.gridColumnScheduleDate.FieldName = "Date";
            this.gridColumnScheduleDate.Name = "gridColumnScheduleDate";
            this.gridColumnScheduleDate.OptionsColumn.AllowEdit = false;
            this.gridColumnScheduleDate.Visible = true;
            this.gridColumnScheduleDate.VisibleIndex = 1;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            this.repositoryItemDateEdit1.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // gridColumnScheduleStart
            // 
            this.gridColumnScheduleStart.Caption = "Inicio";
            this.gridColumnScheduleStart.ColumnEdit = this.repositoryItemTimeEdit1;
            this.gridColumnScheduleStart.FieldName = "BeginTime";
            this.gridColumnScheduleStart.Name = "gridColumnScheduleStart";
            this.gridColumnScheduleStart.OptionsColumn.AllowEdit = false;
            this.gridColumnScheduleStart.Visible = true;
            this.gridColumnScheduleStart.VisibleIndex = 2;
            // 
            // repositoryItemTimeEdit1
            // 
            this.repositoryItemTimeEdit1.AutoHeight = false;
            this.repositoryItemTimeEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemTimeEdit1.Mask.EditMask = "HH:mm";
            this.repositoryItemTimeEdit1.Name = "repositoryItemTimeEdit1";
            // 
            // gridColumnScheduleEnd
            // 
            this.gridColumnScheduleEnd.Caption = "Fin";
            this.gridColumnScheduleEnd.ColumnEdit = this.repositoryItemTimeEdit1;
            this.gridColumnScheduleEnd.FieldName = "EndTime";
            this.gridColumnScheduleEnd.Name = "gridColumnScheduleEnd";
            this.gridColumnScheduleEnd.OptionsColumn.AllowEdit = false;
            this.gridColumnScheduleEnd.Visible = true;
            this.gridColumnScheduleEnd.VisibleIndex = 3;
            // 
            // buttonOk
            // 
            this.buttonOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOk.Location = new System.Drawing.Point(780, 483);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(82, 31);
            this.buttonOk.StyleController = this.layoutControlExtraTime;
            this.buttonOk.TabIndex = 3;
            this.buttonOk.Text = "Accept";
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupExtraTimeInfo,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlGroupDates,
            this.emptySpaceItem3});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(951, 519);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroupExtraTimeInfo
            // 
            this.layoutControlGroupExtraTimeInfo.CustomizationFormText = "layoutControlGroupExtraTimeInfo";
            this.layoutControlGroupExtraTimeInfo.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemName,
            this.layoutControlItemType,
            this.layoutControlItemDesc,
            this.layoutControlItemMotive});
            this.layoutControlGroupExtraTimeInfo.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupExtraTimeInfo.Name = "layoutControlGroupExtraTimeInfo";
            this.layoutControlGroupExtraTimeInfo.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupExtraTimeInfo.Size = new System.Drawing.Size(951, 110);
            this.layoutControlGroupExtraTimeInfo.Text = "layoutControlGroupExtraTimeInfo";
            // 
            // layoutControlItemName
            // 
            this.layoutControlItemName.Control = this.textBoxExName;
            this.layoutControlItemName.CustomizationFormText = "layoutControlItemDesc";
            this.layoutControlItemName.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemName.Name = "layoutControlItemName";
            this.layoutControlItemName.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 4, 3);
            this.layoutControlItemName.Size = new System.Drawing.Size(437, 27);
            this.layoutControlItemName.Text = "layoutControlItemName";
            this.layoutControlItemName.TextSize = new System.Drawing.Size(130, 13);
            // 
            // layoutControlItemType
            // 
            this.layoutControlItemType.Control = this.comboBoxEditType;
            this.layoutControlItemType.CustomizationFormText = "layoutControlItemType";
            this.layoutControlItemType.Location = new System.Drawing.Point(0, 27);
            this.layoutControlItemType.Name = "layoutControlItemType";
            this.layoutControlItemType.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 3, 3);
            this.layoutControlItemType.Size = new System.Drawing.Size(437, 26);
            this.layoutControlItemType.Text = "layoutControlItemType";
            this.layoutControlItemType.TextSize = new System.Drawing.Size(130, 13);
            // 
            // layoutControlItemDesc
            // 
            this.layoutControlItemDesc.Control = this.textBoxExDes;
            this.layoutControlItemDesc.CustomizationFormText = "layoutControlItemName";
            this.layoutControlItemDesc.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.layoutControlItemDesc.Location = new System.Drawing.Point(437, 0);
            this.layoutControlItemDesc.MinSize = new System.Drawing.Size(151, 29);
            this.layoutControlItemDesc.Name = "layoutControlItemDesc";
            this.layoutControlItemDesc.Padding = new DevExpress.XtraLayout.Utils.Padding(10, 5, 4, 4);
            this.layoutControlItemDesc.Size = new System.Drawing.Size(504, 80);
            this.layoutControlItemDesc.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemDesc.Text = "layoutControlItemDesc";
            this.layoutControlItemDesc.TextSize = new System.Drawing.Size(130, 13);
            // 
            // layoutControlItemMotive
            // 
            this.layoutControlItemMotive.Control = this.comboBoxExMotive;
            this.layoutControlItemMotive.CustomizationFormText = "layoutControlItemMotive";
            this.layoutControlItemMotive.Location = new System.Drawing.Point(0, 53);
            this.layoutControlItemMotive.Name = "layoutControlItemMotive";
            this.layoutControlItemMotive.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 3, 4);
            this.layoutControlItemMotive.Size = new System.Drawing.Size(437, 27);
            this.layoutControlItemMotive.Text = "layoutControlItemMotive";
            this.layoutControlItemMotive.TextSize = new System.Drawing.Size(130, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.buttonOk;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(775, 463);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(54, 47);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 20, 5);
            this.layoutControlItem6.Size = new System.Drawing.Size(92, 56);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.ButtonCancel;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(867, 463);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(26, 45);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 20, 5);
            this.layoutControlItem7.Size = new System.Drawing.Size(84, 56);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlGroupDates
            // 
            this.layoutControlGroupDates.CustomizationFormText = "layoutControlGroupDates";
            this.layoutControlGroupDates.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItemSelection,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.emptySpaceItem2,
            this.layoutControlItem3,
            this.emptySpaceItem1,
            this.emptySpaceItem4});
            this.layoutControlGroupDates.Location = new System.Drawing.Point(0, 110);
            this.layoutControlGroupDates.Name = "layoutControlGroupDates";
            this.layoutControlGroupDates.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupDates.Size = new System.Drawing.Size(951, 353);
            this.layoutControlGroupDates.Text = "layoutControlGroupDates";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dateIntervalsControl1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(489, 272);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(489, 272);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(489, 272);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItemSelection
            // 
            this.layoutControlItemSelection.Control = this.gridControlSelected;
            this.layoutControlItemSelection.CustomizationFormText = "layoutControlItemSelection";
            this.layoutControlItemSelection.Location = new System.Drawing.Point(489, 0);
            this.layoutControlItemSelection.Name = "layoutControlItemSelection";
            this.layoutControlItemSelection.Size = new System.Drawing.Size(452, 287);
            this.layoutControlItemSelection.Text = "layoutControlItemSelection";
            this.layoutControlItemSelection.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItemSelection.TextSize = new System.Drawing.Size(130, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.ButtonEmpty;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(775, 287);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(83, 36);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(83, 36);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem4.Size = new System.Drawing.Size(83, 36);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.ButtonDelete;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(858, 287);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(83, 36);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(83, 36);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem5.Size = new System.Drawing.Size(83, 36);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(489, 287);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(286, 36);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.ButtonAdd;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(403, 287);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 287);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(403, 36);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 272);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(489, 15);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 463);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(110, 55);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 30, 5);
            this.emptySpaceItem3.Size = new System.Drawing.Size(775, 56);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // styleController1
            // 
            this.styleController1.LookAndFeel.SkinName = "Lilian";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.layoutControlExtraTime);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.LookAndFeel.SkinName = "Blue";
            this.panelControl1.Margin = new System.Windows.Forms.Padding(2);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(955, 523);
            this.panelControl1.TabIndex = 0;
            // 
            // ExtraTimeCreateForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(955, 523);
            this.Controls.Add(this.panelControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(951, 540);
            this.Name = "ExtraTimeCreateForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Agregar variacion";
            this.Load += new System.EventHandler(this.ExtraTimeCreateForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxExMotive.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlExtraTime)).EndInit();
            this.layoutControlExtraTime.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSelected)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSelection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupExtraTimeInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemMotive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupDates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemSelection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.styleController1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private TextBoxEx textBoxExName;
        private TextBoxEx textBoxExDes;
        private GridControlEx gridControlSelected;
        private GridViewEx gridViewSelection;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnScheduleDate;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnScheduleStart;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnScheduleEnd;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit repositoryItemTimeEdit1;
        private DevExpress.XtraEditors.SimpleButton buttonOk;
        private DevExpress.XtraEditors.SimpleButton ButtonCancel;
        private DevExpress.XtraEditors.SimpleButton ButtonEmpty;
        private DevExpress.XtraEditors.SimpleButton ButtonDelete;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxExMotive;
        private DevExpress.XtraEditors.SimpleButton ButtonAdd;
        private DevExpress.XtraEditors.StyleController styleController1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnScheduleDay;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DateIntervalsControl dateIntervalsControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControlExtraTime;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemName;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDesc;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemMotive;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupExtraTimeInfo;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemSelection;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupDates;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditType;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemType;
        private DevExpress.Utils.ToolTipController toolTipController;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
    }
}