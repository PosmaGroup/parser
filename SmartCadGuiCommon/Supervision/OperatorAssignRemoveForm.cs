using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Smartmatic.SmartCad.Service;
using DevExpress.XtraEditors.Repository;
using System.ServiceModel;
using System.Reflection;
using System.Collections;
using DevExpress.XtraScheduler;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadCore.Core;
using SmartCadControls.Controls;
using SmartCadControls;


namespace SmartCadGuiCommon
{
    public partial class OperatorAssignRemoveForm : DevExpress.XtraEditors.XtraForm
    {

        private OperatorClientData selectedOperator;
        int itemsCheckByRange;
        int itemsCheckByOper;
        private Appointment selectedApp;
      

        public OperatorAssignRemoveForm()
        {
            InitializeComponent();
            InitializeDataGrids();
            LoadLanguage();
        }

        public OperatorAssignRemoveForm(OperatorClientData oper, Appointment app) 
        :this()
        {
            selectedOperator = oper;
            this.selectedApp = app;
            FillOperatorsAssigned();
        }


        private void InitializeDataGrids() 
        {
            gridControlExAssignments.Type = typeof(GridControlDataOperatorAssignEx);
            gridControlExAssignments.ViewTotalRows = true;
            gridControlExAssignments.EnableAutoFilter = true;
            gridViewExAssignments.OptionsBehavior.Editable = true;
            gridViewExAssignments.Columns["Checked"].OptionsColumn.ShowCaption = false;
            gridViewExAssignments.Columns["Checked"].Width = 15;
            gridViewExAssignments.Columns["Checked"].OptionsColumn.AllowEdit = true;
            gridViewExAssignments.Columns["Checked"].VisibleIndex = 0;
            gridViewExAssignments.Columns["Checked"].OptionsColumn.AllowMove = false;
            gridViewExAssignments.Columns["Operator"].OptionsColumn.AllowEdit = false;
            gridViewExAssignments.Columns["Supervisor"].OptionsColumn.AllowEdit = false;
            gridViewExAssignments.Columns["Date"].OptionsColumn.AllowEdit = false;
            gridViewExAssignments.Columns["StartTime"].OptionsColumn.AllowEdit = false;
            gridViewExAssignments.Columns["EndTime"].OptionsColumn.AllowEdit = false;
            gridViewExAssignments.Columns["Day"].OptionsColumn.AllowEdit = false;
            gridViewExAssignments.OptionsView.ColumnAutoWidth = true;

            gridControlExOperators.Type = typeof(GridControlDataOperatorsAssigned);
            gridControlExOperators.ViewTotalRows = true;
            gridControlExOperators.EnableAutoFilter = true;
            gridViewExOperators.OptionsBehavior.Editable = true;
            gridViewExOperators.Columns["Checked"].OptionsColumn.ShowCaption = false;
            gridViewExOperators.Columns["Checked"].Width = 15;
            gridViewExOperators.Columns["Checked"].OptionsColumn.AllowEdit = true;
            gridViewExOperators.Columns["Checked"].VisibleIndex = 0;
            gridViewExOperators.Columns["Checked"].OptionsColumn.AllowMove = false;
            gridViewExOperators.Columns["FirstName"].OptionsColumn.AllowEdit = false;
            gridViewExOperators.Columns["LastName"].OptionsColumn.AllowEdit = false;
            gridViewExOperators.Columns["WorkShiftName"].OptionsColumn.AllowEdit = false;
            gridViewExOperators.OptionsView.ColumnAutoWidth = true;

        }

        private void LoadLanguage()   
        {
            layoutControlItemName.Text = ResourceLoader.GetString2("Name") + ":";
            layoutControlItemStart.Text = ResourceLoader.GetString2("StartDate") + ":";
            layoutControlItemEnd.Text = ResourceLoader.GetString2("EndDate") + ":";
            simpleButtonAccept.Text = ResourceLoader.GetString2("Accept");
            simpleButtonCancel.Text = ResourceLoader.GetString2("Cancel");
            checkEditAll.Text = ResourceLoader.GetString2("SelectAll");
            checkEditAllOper.Text = ResourceLoader.GetString2("SelectAll");
            layoutControlGroupOperator.Text = ResourceLoader.GetString2("Data");
            layoutControlGroupOperatorsAssigned.Text = ResourceLoader.GetString2("Assignments");
            radioButtonByRange.Text = ResourceLoader.GetString2("ByRange");
       
        }

        private void OperatorAssignRemoveForm_Load(object sender, EventArgs e)
        {
            radioButtonByRange.Checked = true;
            DateTime now = ServerServiceClient.GetInstance().GetTimeFromDB();
            dateEditStart.Properties.MinValue = now;

            if (selectedApp != null)
            {
                if (selectedApp.Start >= now)
                dateEditStart.DateTime = selectedApp.Start;
                else
                dateEditStart.DateTime = now;
                
                dateEditEnd.DateTime = selectedApp.End;
                simpleButtonSearch.PerformClick();
            }
            else
            {
                dateEditStart.DateTime = now;
                dateEditEnd.DateTime = now;
            }
            textEditName.Text = selectedOperator.FirstName + " " + selectedOperator.LastName;

            if (selectedOperator.IsSupervisor == true)
            {
                gridViewExAssignments.Columns["Supervisor"].Visible = false;
                radioButtonByOperator.Text = ResourceLoader.GetString2("ByOperator");
                this.Text = ResourceLoader.GetString2("RemoveSupervisorAssignments");
            }
            else
            {
                gridViewExAssignments.Columns["Operator"].Visible = false;
                radioButtonByOperator.Text = ResourceLoader.GetString2("BySupervisor");
                this.Text = ResourceLoader.GetString2("RemoveOperatorAssignments");
            }
        }


        private void FillOperatorsAssigned() 
        {
            string hql = string.Empty;
            DateTime now = ServerServiceClient.GetInstance().GetTimeFromDB();
            BindingList<GridControlDataOperatorsAssigned> dataSource = new BindingList<GridControlDataOperatorsAssigned>();

            if (selectedOperator.IsSupervisor == true)
				hql = SmartCadHqls.GetWorkShiftVariationWithOperatorsAssignForTheGivenSupervisor;
            else
				hql = SmartCadHqls.GetWorkShiftVariationWithOperatorsAssignForTheGivenOperator;


            IList wsList = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(hql, (int)WorkShiftVariationClientData.WorkShiftType.TimeOff, selectedOperator.Code, ApplicationUtil.GetDataBaseFormattedDate(now)));

            foreach (WorkShiftVariationClientData ws in wsList)
            {   
                foreach (OperatorClientData oper in ws.Operators)
	    		     dataSource.Add(new GridControlDataOperatorsAssigned(oper, ws.Name, ws.Code));
            }

            gridControlExOperators.BeginInit();
            gridControlExOperators.DataSource = dataSource;
            gridControlExOperators.EndInit();

        }


        private void dateEditStart_EditValueChanged(object sender, EventArgs e)
        {
            dateEditEnd.Properties.MinValue = dateEditStart.DateTime;
        }

        private void simpleButtonSearch_Click(object sender, EventArgs e)
        {
            string hql = string.Empty;
            IList assigns;
            BindingList<GridControlDataOperatorAssignEx> dataSource = new BindingList<GridControlDataOperatorAssignEx>();

            if (selectedOperator.IsSupervisor == true)
                hql = @"SELECT assign FROM OperatorAssignData assign WHERE assign.Supervisor.Code = {0} AND ((assign.StartDate BETWEEN '{1}' AND '{2}') OR (assign.EndDate BETWEEN '{1}' AND '{2}'))";
            else
                hql = @"SELECT assign FROM OperatorAssignData assign WHERE assign.SupervisedOperator.Code = {0} AND ((assign.StartDate BETWEEN '{1}' AND '{2}') OR (assign.EndDate BETWEEN '{1}' AND '{2}'))";

            assigns = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(hql, selectedOperator.Code, ApplicationUtil.GetNHibernateFormattedDate(dateEditStart.DateTime), ApplicationUtil.GetNHibernateFormattedDate(dateEditEnd.DateTime)));
            
            if (assigns != null && assigns.Count > 0)
            {
                foreach (OperatorAssignClientData operAssign in assigns)
                {
                    GridControlDataOperatorAssignEx gridData = new GridControlDataOperatorAssignEx(operAssign); 
                    if (operAssign.StartDate.Date == dateEditStart.DateTime.Date) 
                    {
                        if (dateEditStart.DateTime.TimeOfDay > operAssign.StartDate.TimeOfDay)
                            gridData.StartTime = dateEditStart.DateTime;
                        else
                            gridData.StartTime = operAssign.StartDate;
                    }

                    if (operAssign.EndDate.Date == dateEditEnd.DateTime.Date)
                    {
                        if (dateEditEnd.DateTime.TimeOfDay < operAssign.EndDate.TimeOfDay)
                            gridData.EndTime = dateEditEnd.DateTime;
                        else
                            gridData.EndTime = operAssign.EndDate;
                    }

                    dataSource.Add(gridData);

                }

                gridControlExAssignments.BeginUpdate();
                gridControlExAssignments.DataSource = dataSource;
                gridControlExAssignments.EndUpdate();
            }
            else
                CleanGridControl();
            

         

        }

        private void CleanGridControl()
        {
            FormUtil.InvokeRequired(this, delegate
            {
                gridControlExAssignments.BeginUpdate();
                gridControlExAssignments.DataSource = null;
                gridControlExAssignments.EndUpdate();

            });
        }

     

        private void checkEditAll_CheckedChanged(object sender, EventArgs e)
        {
            gridViewExAssignments.CellValueChanging -= new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(gridViewExAssignments_CellValueChanging);

            gridViewExAssignments.BeginUpdate();
            foreach (GridControlDataOperatorAssignEx gridData in gridControlExAssignments.Items)
                  gridData.Checked = checkEditAll.Checked;
            gridViewExAssignments.EndUpdate();

            if (checkEditAll.Checked == true)
            {
                itemsCheckByRange = gridControlExAssignments.Items.Count;
                if (itemsCheckByRange > 0)
                    simpleButtonAccept.Enabled = true;
                else
                    simpleButtonAccept.Enabled = false;
            }
            else
            {
                simpleButtonAccept.Enabled = false;
                itemsCheckByRange = 0;
            }

            gridViewExAssignments.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(gridViewExAssignments_CellValueChanging);


        }

        private void checkEditAllOper_CheckedChanged(object sender, EventArgs e)
        {   
            gridViewExOperators.CellValueChanging -= new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(gridViewExOperators_CellValueChanging);

            gridViewExOperators.BeginUpdate();
            foreach (GridControlDataOperatorsAssigned gridData in gridControlExOperators.Items)
                gridData.Checked = checkEditAllOper.Checked;
            gridViewExOperators.EndUpdate();

            if (checkEditAllOper.Checked == true)
            {
                itemsCheckByOper = gridControlExOperators.Items.Count;
                if (itemsCheckByOper > 0)
                    simpleButtonAccept.Enabled = true;
                else
                    simpleButtonAccept.Enabled = false;
            }
            else
            {
                simpleButtonAccept.Enabled = false;
                itemsCheckByOper = 0;
            }

            gridViewExOperators.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(gridViewExOperators_CellValueChanging);

        }

        private void gridViewExAssignments_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            checkEditAll.CheckedChanged -= new EventHandler(checkEditAll_CheckedChanged);

            if ((bool)e.Value == true)
                itemsCheckByRange++;
            else
                itemsCheckByRange--;

            if (itemsCheckByRange > 0 && radioButtonByRange.Checked == true)
                simpleButtonAccept.Enabled = true;
            else
                simpleButtonAccept.Enabled = false;


            if (itemsCheckByRange > 0 && itemsCheckByRange == gridControlExAssignments.Items.Count)
                checkEditAll.CheckState = CheckState.Checked;
            else if (itemsCheckByRange == 0)
                checkEditAll.CheckState = CheckState.Unchecked;


            checkEditAll.CheckedChanged += new EventHandler(checkEditAll_CheckedChanged);
        }


        private void gridViewExOperators_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {

            checkEditAllOper.CheckedChanged -= new EventHandler(checkEditAllOper_CheckedChanged);

            if ((bool)e.Value == true)
                itemsCheckByOper++;
            else
                itemsCheckByOper--;

            if (itemsCheckByOper > 0 && radioButtonByOperator.Checked == true)
                simpleButtonAccept.Enabled = true;
            else
                simpleButtonAccept.Enabled = false;


            if (itemsCheckByOper > 0 && itemsCheckByOper == gridControlExOperators.Items.Count)
                checkEditAllOper.CheckState = CheckState.Checked;
            else if (itemsCheckByOper == 0)
                checkEditAllOper.CheckState = CheckState.Unchecked;


            checkEditAllOper.CheckedChanged += new EventHandler(checkEditAllOper_CheckedChanged);
        
        }

        private void radioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonByRange.Checked == true) 
            {
                gridControlExOperators.Enabled = false;
                checkEditAllOper.Enabled = false;

                dateEditStart.Enabled = true;
                dateEditEnd.Enabled = true;
                simpleButtonSearch.Enabled = true;
                gridControlExAssignments.Enabled = true;
                checkEditAll.Enabled = true;

                if (itemsCheckByRange > 0)
                    simpleButtonAccept.Enabled = true;
                else
                    simpleButtonAccept.Enabled = false;

                layoutControlGroupByOperator.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                layoutControlGroupByRange.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;

            }
            else if (radioButtonByOperator.Checked == true) 
            {
                dateEditStart.Enabled = false;
                dateEditEnd.Enabled = false;
                simpleButtonSearch.Enabled = false;
                gridControlExAssignments.Enabled = false;
                checkEditAll.Enabled = false;

                gridControlExOperators.Enabled = true;
                checkEditAllOper.Enabled = true;


                if (itemsCheckByOper > 0)
                    simpleButtonAccept.Enabled = true;
                else
                    simpleButtonAccept.Enabled = false;


                layoutControlGroupByRange.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                layoutControlGroupByOperator.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            }
        }


        private ArrayList GetCheckedGridControlDataOperatorAssigns()
        {
            ArrayList result = new ArrayList();

            foreach (GridControlDataOperatorAssignEx gridData in gridControlExAssignments.Items)
            {
                if (gridData.Checked == true)
                    result.Add(gridData);
            }

            return result;
        }

        private IList GetCheckedOperatorsOrSupervisors()
        {
            IList result = new ArrayList();

            foreach (GridControlDataOperatorsAssigned gridData in gridControlExOperators.Items)
            {
                if (gridData.Checked == true)
                    result.Add(gridData);
            }

            return result;
        }

        private void simpleButtonAccept_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult;

            if (radioButtonByRange.Checked == true)
                dialogResult = MessageForm.Show(ResourceLoader.GetString2("RemoveSelectedAssignment"), MessageFormType.Question);
            else
            {
                if (selectedOperator.IsSupervisor == true)
                    dialogResult = MessageForm.Show(ResourceLoader.GetString2("RemoveAssignSupervisorMessage"), MessageFormType.Question);
                else
                    dialogResult = MessageForm.Show(ResourceLoader.GetString2("RemoveAssignOperatorMessage"), MessageFormType.Question);
            }

            //----- PREVIOUS CODE ------
            //if (radioButtonByRange.Checked == true)
            //   dialogResult = MessageForm.Show("Esta seguro que desea remover todas las asignaciones seleccionadas?", MessageFormType.Question);
            //else
            //{
            //    if (selectedOperator.IsSupervisor == true)
            //        dialogResult = MessageForm.Show("Esta seguro que desea remover todas las asignaciones de los operadores seleccionados?", MessageFormType.Question);
            //    else
            //        dialogResult = MessageForm.Show("Esta seguro que desea remover todas las asignaciones de los supervisores seleccionados?", MessageFormType.Question);
            //}

            if (dialogResult != DialogResult.Yes)
                return;

            try
            {
                BackgroundProcessForm processForm = new BackgroundProcessForm(this, new MethodInfo[1] { GetType().GetMethod("simpleButtonAccept_Click_1", BindingFlags.NonPublic | BindingFlags.Instance) }, new object[1][] { new object[0] });
                processForm.CanThrowError = true;
                processForm.ShowDialog();
            }
            catch (FaultException ex)
            {
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, MessageFormType.Error);
            }
            catch (Exception ex)
            {
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, ex);

            }
        }

        private object[] GetOperatorAssignsToModify()
        {
            object[] result = new object[2];
            IList assignsToDelete = new ArrayList();
            IList assignsToSave = new ArrayList();
            OperatorAssignClientData operAssign;
            OperatorAssignClientData operAssignAux;
            ArrayList checkedGridControlData = GetCheckedGridControlDataOperatorAssigns();

            foreach (GridControlDataOperatorAssignEx gridData in checkedGridControlData)
            {
                operAssign = new OperatorAssignClientData();
                operAssign = gridData.Tag as OperatorAssignClientData;

                if (gridData.StartTime <= operAssign.StartDate && gridData.EndTime >= operAssign.EndDate) 
                {
                    assignsToDelete.Add(operAssign);
                }
                else if (gridData.StartTime > operAssign.StartDate && gridData.EndTime < operAssign.EndDate)
                {
                    operAssignAux = new OperatorAssignClientData();
                    operAssignAux.SupervisedOperatorCode = operAssign.SupervisedOperatorCode;
                    operAssignAux.SupervisedScheduleVariation = operAssign.SupervisedScheduleVariation;
                    operAssignAux.SupervisorScheduleVariation = operAssign.SupervisorScheduleVariation;
                    operAssignAux.StartDate = gridData.EndTime;
                    operAssignAux.EndDate = operAssign.EndDate;
                    operAssignAux.SupervisorCode = operAssign.SupervisorCode;
                    assignsToSave.Add(operAssignAux);

                    operAssign.EndDate = gridData.StartTime;
                    assignsToSave.Add(operAssign);
                
                }
                else if (gridData.StartTime > operAssign.StartDate && gridData.EndTime >= operAssign.EndDate)
                {
                    operAssign.EndDate = gridData.StartTime;
                    assignsToSave.Add(operAssign);
                }
                else if (gridData.StartTime <= operAssign.StartDate && gridData.EndTime < operAssign.EndDate)
                {
                    operAssign.StartDate = gridData.EndTime;
                    assignsToSave.Add(operAssign);
                }
   
            }

            result[0] = assignsToSave;
            result[1] = assignsToDelete;
            
            return result; 
        }

        private void simpleButtonAccept_Click_1()
        {
            ArrayList assignsToDelete = new ArrayList();
            ArrayList assignsToSave = new ArrayList();

            if (radioButtonByRange.Checked == true)
            {
                object[] list = GetOperatorAssignsToModify();

                assignsToSave = (ArrayList)list[0];
                assignsToDelete = (ArrayList)list[1];

            }
            else
            {
                DateTime now = ServerServiceClient.GetInstance().GetTimeFromDB();
                IList assigns;
                IList operators = GetCheckedOperatorsOrSupervisors();

                string hql = string.Empty;
                if (selectedOperator.IsSupervisor == false)
                {
                    hql = SmartCadHqls.GetOperatorAssignsByWorkShiftCodeAndOperatorCode;

                    foreach (GridControlDataOperatorsAssigned oper in operators)
                    {
                        assigns = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(hql, oper.Code, selectedOperator.Code, oper.WorkShiftCode, ApplicationUtil.GetDataBaseFormattedDate(now)));
                        assignsToDelete.AddRange(assigns);
                    }
                }
                else
                {
                    hql = SmartCadHqls.GetOperatorAssignsByWorkShiftCodeAndSupervisorCode;

                    foreach (GridControlDataOperatorsAssigned oper in operators)
                    {
                        assigns = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(hql, selectedOperator.Code, oper.Code, oper.WorkShiftCode, ApplicationUtil.GetDataBaseFormattedDate(now)));
                        assignsToDelete.AddRange(assigns);
                    }
                }

            }
            if (assignsToSave.Count > 0)
                ServerServiceClient.GetInstance().SaveOrUpdateClientData(assignsToSave);

            if (assignsToDelete.Count > 0)
                ServerServiceClient.GetInstance().DeleteClientObjectCollection(assignsToDelete);

        }

        private void simpleButtonDetail_Click(object sender, EventArgs e)
        {
            if (gridControlExAssignments.SelectedItems.Count > 0) 
            {
                GridControlDataOperatorAssignEx gridData = gridControlExAssignments.SelectedItems[0] as GridControlDataOperatorAssignEx;

                OperatorAssignDetailForm detail = null;
                if (selectedOperator.IsSupervisor == false)
                     detail = new OperatorAssignDetailForm(selectedOperator, gridData.OperatorAssign.SupervisedScheduleVariation);
                else
                     detail = new OperatorAssignDetailForm(selectedOperator, gridData.OperatorAssign.SupervisorScheduleVariation);


                detail.ShowDialog();
            }

        }

       

       
    }

    public class GridControlDataOperatorAssignEx : GridControlData
    {

        private bool check;

        public GridControlDataOperatorAssignEx(OperatorAssignClientData operAssign)
            : base(operAssign)
        {

        }

        public OperatorAssignClientData OperatorAssign
        {
            get { return this.Tag as OperatorAssignClientData; }
        }

        public int Code
        {
            get { return OperatorAssign.Code; }
        }


        [GridControlRowInfoData(Visible = true, RepositoryType = typeof(RepositoryItemCheckEdit), Searchable = false)]
        public bool Checked
        {
            get { return check; }
            set { check = value; }
        }

        [GridControlRowInfoData(Visible = false)]
        public string Day
        {
            get
            {
                return ResourceLoader.GetString2(OperatorAssign.StartDate.DayOfWeek.ToString());
            }
        }

        [GridControlRowInfoData(Visible = true, DisplayFormat = "MM/dd/yyyy")]
        public DateTime Date
        {
            get
            {
                return OperatorAssign.StartDate.Date;
            }
        }


        [GridControlRowInfoData(Visible = true, DisplayFormat = "HH:mm")]
        public DateTime StartTime
        {
            get;
            set;


        }

        [GridControlRowInfoData(Visible = true, DisplayFormat = "HH:mm")]
        public DateTime EndTime
        {
            get;
            set;
        }

        [GridControlRowInfoData(Visible = true)]
        public string Supervisor
        {
            get { return OperatorAssign.SupFirstName + " " + OperatorAssign.SupLastName; }
        }

        [GridControlRowInfoData(Visible = true)]
        public string Operator
        {
            get { return OperatorAssign.OperFirstName + " " + OperatorAssign.OperLastName; }
        }


        public override bool Equals(object obj)
        {
            bool result = false;
            GridControlDataOperatorAssignEx var = obj as GridControlDataOperatorAssignEx;
            if (var != null)
            {
                OperatorAssignClientData oad = (OperatorAssignClientData)var.OperatorAssign;
                if (this.OperatorAssign.Equals(oad) == true)
                    result = true;

            }
            return result;

        }


    }

    public class GridControlDataOperatorsAssigned : GridControlData
    {
        private string workShiftName;
        private int workShiftCode;
        private bool check;

        public GridControlDataOperatorsAssigned(OperatorClientData data, string wsName, int wsCode)
            : base(data)
        {
            workShiftName = wsName;
            workShiftCode = wsCode;
        }

        public OperatorClientData Operator
        {
            get { return this.Tag as OperatorClientData; }

        }

        public int Code
        {
            get { return Operator.Code; }
        }

        [GridControlRowInfoData(Visible = true, RepositoryType = typeof(RepositoryItemCheckEdit), Searchable = false)]
        public bool Checked
        {
            get { return check; }
            set { check = value; }
        }

        [GridControlRowInfoData(Visible = true, Width = 150)]
        public string FirstName
        {
            get { return Operator.FirstName; }
        }

        [GridControlRowInfoData(Visible = true, Width = 150)]
        public string LastName
        {
            get { return Operator.LastName; }
        }

        [GridControlRowInfoData(Visible = true, Width = 150)]
        public string WorkShiftName
        {
            get
            {
                return workShiftName;
            }
        }

        public int WorkShiftCode
        {
            get
            {
                return workShiftCode;
            }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            GridControlDataOperatorsAssigned var = obj as GridControlDataOperatorsAssigned;
            if (var != null)
            {
                if (this.Code == var.Code)
                    result = true;
            }
            return result;
        }

    }
}