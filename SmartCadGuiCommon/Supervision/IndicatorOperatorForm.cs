﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using Smartmatic.SmartCad.Service;
using SmartCadCore.Common;
using SmartCadCore.ClientData;
using SmartCadCore.Core;

namespace SmartCadGuiCommon
{
    public partial class IndicatorOperatorForm : Form
    {
        private string indicatorCode;

        public IndicatorOperatorForm()
        {
            InitializeComponent();
            LoadLanguage();
        }

        private void LoadLanguage()
        {
            this.Text = ResourceLoader.GetString2("IndicatorOperatorForm");
            this.gridColumnOperator.Caption = ResourceLoader.GetString2("Operator");
            this.gridColumnValue.Caption = ResourceLoader.GetString2("RealValue");
            this.gridColumnThreshold.Caption = ResourceLoader.GetString2("Threshold");
            this.gridColumnTrend.Caption = ResourceLoader.GetString2("Trend");
        }

        public string IndicatorCode
        {
            get
            {
                return indicatorCode;
            }
            set
            {
                indicatorCode = value;
            }
        }

        public string IndicatorName
        {
            set
            {
                this.groupControlIndicator.Text = value;
            }
        }

        public void LoadData()
        {
            IndicatorResultClientData result = (IndicatorResultClientData)ServerServiceClient.GetInstance().SearchClientObject(
                SmartCadHqls.GetCustomHql(
                SmartCadHqls.GetOperatorIndicatorResultByIndicatorIndicatorClass, 
                IndicatorClassClientData.Operator.Code, 
                IndicatorCode));

            if (result != null)
            {
                List<IndicatorOperatorGridData> values = new List<IndicatorOperatorGridData>();
                foreach (IndicatorResultValuesClientData client in result.Values)
                {
                    try
                    {
                        if (Convert.ToDouble(client.ResultValue) > 0d)
                        {
                            values.Add(new IndicatorOperatorGridData(client));
                        }
                    }
                    catch
                    {
                        //Invalid conversion
                    }
                }
                gridControlIndicatorOperator.BeginInit();
                gridControlIndicatorOperator.DataSource = values;
                gridControlIndicatorOperator.EndInit();
            }
            else
            {
                MessageForm.Show(ResourceLoader.GetString2("NoDetailInformationavailable"), MessageFormType.Information);
                this.Close();
            }
        }

        private void IndicatorOperatorForm_Load(object sender, EventArgs e)
        {
            LoadData();
        }
    }

    public class IndicatorOperatorGridData
    {
        IndicatorResultValuesClientData data;

        public IndicatorOperatorGridData(IndicatorResultValuesClientData data)
        {
            this.data = data;
        }

        public string Operator
        {
            get
            {
                return data.Information;
            }
        }

        public string Value
        {
            get
            {
                if (data.MeasureUnit == "seg")
                {                    
                    return ApplicationUtil.GetTimeSpanCustomString(TimeSpan.FromSeconds(double.Parse(data.ResultValue)));
                }
                else if (data.MeasureUnit == "%")
                {
                    return ((double.Parse(data.ResultValue) * 100) + "%");
                }
                else
                {
                    return data.ResultValue;
                }
            }
        }

        public Image Threshold
        {
            get
            {
                return ResourceLoader.GetImage("ImageThreshold"  + data.Threshold.ToString());
            }
        }

        public Image Trend
        {
            get
            {
                //return ResourceLoader.GetImage("ImageTrend" + data.Trend.ToString());
                if (ResourceLoader.GetImage("TrendImage" + data.Trend.ToString()) != null)
                {
                    return ResourceLoader.GetImage("TrendImage" + data.Trend.ToString());
                }
                else
                {
                    return ResourceLoader.GetImage("$Image.Transparent");
                }


            }
        }
    }
}
