using SmartCadControls;
using SmartCadControls.Controls;
using System.Windows.Forms;
namespace SmartCadGuiCommon
{
    partial class TrainingCourseScheduleForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TrainingCourseScheduleForm));
            this.textBoxExCourse = new TextBoxEx();
			this.gridControlSchedules = new GridControlEx();
			this.gridViewSchedules = new GridViewEx();
            this.gridColumnName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnStart = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEditStart = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.gridColumnEnd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEditEnd = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.gridColumnTrainer = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEditMinimumAttendance = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonAddSchedule = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonRemoveSchedule = new System.Windows.Forms.ToolStripButton();
			this.gridControlParts = new GridControlEx();
			this.gridViewParts = new GridViewEx();
            this.gridColumnRoom = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnPartsStart = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEditPartStart = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.gridColumnPartsEnd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEditPartEnd = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonAdd = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonDelete = new System.Windows.Forms.ToolStripButton();
            this.buttonExOk = new DevExpress.XtraEditors.SimpleButton();
            this.TrainingCourseScheduleFormConvertedLayout = new DevExpress.XtraLayout.LayoutControl();
            this.buttonExApply = new DevExpress.XtraEditors.SimpleButton();
            this.buttonExCancel = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupCourseInf = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupSchedules = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupParts = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutConverter1 = new DevExpress.XtraLayout.Converter.LayoutConverter(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSchedules)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSchedules)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditStart.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditEnd.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditMinimumAttendance)).BeginInit();
            this.toolStrip2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlParts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewParts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditPartStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditPartStart.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditPartEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditPartEnd.VistaTimeProperties)).BeginInit();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TrainingCourseScheduleFormConvertedLayout)).BeginInit();
            this.TrainingCourseScheduleFormConvertedLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupCourseInf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupSchedules)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupParts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxExCourse
            // 
            this.textBoxExCourse.AllowsLetters = true;
            this.textBoxExCourse.AllowsNumbers = true;
            this.textBoxExCourse.AllowsPunctuation = true;
            this.textBoxExCourse.AllowsSeparators = true;
            this.textBoxExCourse.AllowsSymbols = true;
            this.textBoxExCourse.AllowsWhiteSpaces = true;
            this.textBoxExCourse.BackColor = System.Drawing.Color.White;
            this.textBoxExCourse.ExtraAllowedChars = "";
            this.textBoxExCourse.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExCourse.Location = new System.Drawing.Point(129, 28);
            this.textBoxExCourse.Name = "textBoxExCourse";
            this.textBoxExCourse.NonAllowedCharacters = "";
            this.textBoxExCourse.ReadOnly = true;
            this.textBoxExCourse.RegularExpresion = "";
            this.textBoxExCourse.Size = new System.Drawing.Size(504, 20);
            this.textBoxExCourse.TabIndex = 4;
            this.textBoxExCourse.Enabled = true;
            this.textBoxExCourse.TextChanged += new System.EventHandler(this.ButtonActivation);
            // 
            // gridControlSchedules
            // 
            this.gridControlSchedules.Location = new System.Drawing.Point(10, 109);
            this.gridControlSchedules.MainView = this.gridViewSchedules;
            this.gridControlSchedules.Name = "gridControlSchedules";
            this.gridControlSchedules.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateEditStart,
            this.repositoryItemSpinEditMinimumAttendance,
            this.repositoryItemDateEditEnd});
            this.gridControlSchedules.Size = new System.Drawing.Size(623, 154);
            this.gridControlSchedules.TabIndex = 2;
            this.gridControlSchedules.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewSchedules});
            // 
            // gridViewSchedules
            // 
            this.gridViewSchedules.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnName,
            this.gridColumnStart,
            this.gridColumnEnd,
            this.gridColumnTrainer,
            this.gridColumnStatus});
            this.gridViewSchedules.GridControl = this.gridControlSchedules;
            this.gridViewSchedules.GroupFormat = "[#image]{1} {2}";
            this.gridViewSchedules.Name = "gridViewSchedules";
            this.gridViewSchedules.OptionsCustomization.AllowColumnMoving = false;
            this.gridViewSchedules.OptionsCustomization.AllowFilter = false;
            this.gridViewSchedules.OptionsMenu.EnableColumnMenu = false;
            this.gridViewSchedules.OptionsMenu.EnableFooterMenu = false;
            this.gridViewSchedules.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridViewSchedules.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewSchedules.OptionsView.ShowAutoFilterRow = true;
            this.gridViewSchedules.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewSchedules.OptionsView.ShowGroupPanel = false;
            this.gridViewSchedules.OptionsView.ShowIndicator = false;
            this.gridViewSchedules.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewSchedules_FocusedRowChanged);
            this.gridViewSchedules.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewSchedules_CellValueChanged);
            this.gridViewSchedules.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewSchedules_CellValueChanging);
            // 
            // gridColumnName
            // 
            this.gridColumnName.Caption = "gridColumn1";
            this.gridColumnName.FieldName = "Name";
            this.gridColumnName.Name = "gridColumnName";
            this.gridColumnName.OptionsFilter.AllowFilter = false;
            this.gridColumnName.Visible = true;
            this.gridColumnName.VisibleIndex = 0;
            // 
            // gridColumnStart
            // 
            this.gridColumnStart.Caption = "gridColumn2";
            this.gridColumnStart.ColumnEdit = this.repositoryItemDateEditStart;
            this.gridColumnStart.FieldName = "StartDate";
            this.gridColumnStart.Name = "gridColumnStart";
            this.gridColumnStart.OptionsFilter.AllowFilter = false;
            this.gridColumnStart.UnboundType = DevExpress.Data.UnboundColumnType.DateTime;
            this.gridColumnStart.Visible = true;
            this.gridColumnStart.VisibleIndex = 1;
            // 
            // repositoryItemDateEditStart
            // 
            this.repositoryItemDateEditStart.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemDateEditStart.AutoHeight = false;
            this.repositoryItemDateEditStart.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEditStart.Name = "repositoryItemDateEditStart";
            this.repositoryItemDateEditStart.ShowToday = false;
            this.repositoryItemDateEditStart.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // gridColumnEnd
            // 
            this.gridColumnEnd.Caption = "gridColumn3";
            this.gridColumnEnd.ColumnEdit = this.repositoryItemDateEditEnd;
            this.gridColumnEnd.FieldName = "EndDate";
            this.gridColumnEnd.Name = "gridColumnEnd";
            this.gridColumnEnd.OptionsFilter.AllowFilter = false;
            this.gridColumnEnd.UnboundType = DevExpress.Data.UnboundColumnType.DateTime;
            this.gridColumnEnd.Visible = true;
            this.gridColumnEnd.VisibleIndex = 2;
            // 
            // repositoryItemDateEditEnd
            // 
            this.repositoryItemDateEditEnd.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemDateEditEnd.AutoHeight = false;
            this.repositoryItemDateEditEnd.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEditEnd.Name = "repositoryItemDateEditEnd";
            this.repositoryItemDateEditEnd.ShowToday = false;
            this.repositoryItemDateEditEnd.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // gridColumnTrainer
            // 
            this.gridColumnTrainer.Caption = "gridColumn1";
            this.gridColumnTrainer.FieldName = "Trainer";
            this.gridColumnTrainer.Name = "gridColumnTrainer";
            this.gridColumnTrainer.OptionsFilter.AllowFilter = false;
            this.gridColumnTrainer.Visible = true;
            this.gridColumnTrainer.VisibleIndex = 3;
            // 
            // gridColumnStatus
            // 
            this.gridColumnStatus.Caption = "gridColumn4";
            this.gridColumnStatus.FieldName = "Status";
            this.gridColumnStatus.Name = "gridColumnStatus";
            this.gridColumnStatus.OptionsColumn.AllowEdit = false;
            this.gridColumnStatus.OptionsColumn.ReadOnly = true;
            this.gridColumnStatus.OptionsFilter.AllowFilter = false;
            this.gridColumnStatus.Visible = true;
            this.gridColumnStatus.VisibleIndex = 4;
            // 
            // repositoryItemSpinEditMinimumAttendance
            // 
            this.repositoryItemSpinEditMinimumAttendance.AutoHeight = false;
            this.repositoryItemSpinEditMinimumAttendance.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEditMinimumAttendance.IsFloatValue = false;
            this.repositoryItemSpinEditMinimumAttendance.Mask.EditMask = "N00";
            this.repositoryItemSpinEditMinimumAttendance.MaxValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.repositoryItemSpinEditMinimumAttendance.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.repositoryItemSpinEditMinimumAttendance.Name = "repositoryItemSpinEditMinimumAttendance";
            // 
            // toolStrip2
            // 
            this.toolStrip2.AutoSize = false;
            this.toolStrip2.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonAddSchedule,
            this.toolStripButtonRemoveSchedule});
            this.toolStrip2.Location = new System.Drawing.Point(10, 83);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(623, 25);
            this.toolStrip2.TabIndex = 1;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // toolStripButtonAddSchedule
            // 
            this.toolStripButtonAddSchedule.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonAddSchedule.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonAddSchedule.Image")));
            this.toolStripButtonAddSchedule.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonAddSchedule.Name = "toolStripButtonAddSchedule";
            this.toolStripButtonAddSchedule.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonAddSchedule.Text = "toolStripButtonAdd";
            this.toolStripButtonAddSchedule.ToolTipText = "Agregar horario";
            this.toolStripButtonAddSchedule.Click += new System.EventHandler(this.toolStripButtonAddSchedule_Click);
            // 
            // toolStripButtonRemoveSchedule
            // 
            this.toolStripButtonRemoveSchedule.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonRemoveSchedule.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonRemoveSchedule.Image")));
            this.toolStripButtonRemoveSchedule.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonRemoveSchedule.Name = "toolStripButtonRemoveSchedule";
            this.toolStripButtonRemoveSchedule.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonRemoveSchedule.Text = "toolStripButtonDelete";
            this.toolStripButtonRemoveSchedule.ToolTipText = "Eliminar horario";
            this.toolStripButtonRemoveSchedule.Click += new System.EventHandler(this.toolStripButtonRemoveSchedule_Click);
            // 
            // gridControlParts
            // 
            this.gridControlParts.Location = new System.Drawing.Point(10, 324);
            this.gridControlParts.MainView = this.gridViewParts;
            this.gridControlParts.Name = "gridControlParts";
            this.gridControlParts.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateEditPartStart,
            this.repositoryItemDateEditPartEnd});
            this.gridControlParts.Size = new System.Drawing.Size(623, 149);
            this.gridControlParts.TabIndex = 3;
            this.gridControlParts.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewParts});
            // 
            // gridViewParts
            // 
            this.gridViewParts.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnRoom,
            this.gridColumnPartsStart,
            this.gridColumnPartsEnd});
            this.gridViewParts.GridControl = this.gridControlParts;
            this.gridViewParts.GroupFormat = "[#image]{1} {2}";
            this.gridViewParts.Name = "gridViewParts";
            this.gridViewParts.OptionsCustomization.AllowColumnMoving = false;
            this.gridViewParts.OptionsCustomization.AllowFilter = false;
            this.gridViewParts.OptionsMenu.EnableColumnMenu = false;
            this.gridViewParts.OptionsMenu.EnableFooterMenu = false;
            this.gridViewParts.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridViewParts.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewParts.OptionsView.ShowAutoFilterRow = true;
            this.gridViewParts.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewParts.OptionsView.ShowGroupPanel = false;
            this.gridViewParts.OptionsView.ShowIndicator = false;
            this.gridViewParts.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewParts_FocusedRowChanged);
            this.gridViewParts.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewParts_CellValueChanged);
            this.gridViewParts.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewParts_CellValueChanging);
            // 
            // gridColumnRoom
            // 
            this.gridColumnRoom.Caption = "gridColumn1";
            this.gridColumnRoom.FieldName = "Room";
            this.gridColumnRoom.Name = "gridColumnRoom";
            this.gridColumnRoom.OptionsFilter.AllowFilter = false;
            this.gridColumnRoom.Visible = true;
            this.gridColumnRoom.VisibleIndex = 0;
            // 
            // gridColumnPartsStart
            // 
            this.gridColumnPartsStart.Caption = "gridColumn2";
            this.gridColumnPartsStart.ColumnEdit = this.repositoryItemDateEditPartStart;
            this.gridColumnPartsStart.FieldName = "StartDate";
            this.gridColumnPartsStart.Name = "gridColumnPartsStart";
            this.gridColumnPartsStart.OptionsFilter.AllowFilter = false;
            this.gridColumnPartsStart.Visible = true;
            this.gridColumnPartsStart.VisibleIndex = 1;
            // 
            // repositoryItemDateEditPartStart
            // 
            this.repositoryItemDateEditPartStart.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemDateEditPartStart.AutoHeight = false;
            this.repositoryItemDateEditPartStart.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEditPartStart.DisplayFormat.FormatString = "MM/dd/yyyy HH:mm";
            this.repositoryItemDateEditPartStart.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.repositoryItemDateEditPartStart.EditFormat.FormatString = "MM/dd/yyyy HH:mm";
            this.repositoryItemDateEditPartStart.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.repositoryItemDateEditPartStart.Mask.EditMask = "MM/dd/yyyy HH:mm";
            this.repositoryItemDateEditPartStart.Name = "repositoryItemDateEditPartStart";
            this.repositoryItemDateEditPartStart.ShowToday = false;
            this.repositoryItemDateEditPartStart.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // gridColumnPartsEnd
            // 
            this.gridColumnPartsEnd.Caption = "gridColumn3";
            this.gridColumnPartsEnd.ColumnEdit = this.repositoryItemDateEditPartEnd;
            this.gridColumnPartsEnd.FieldName = "EndDate";
            this.gridColumnPartsEnd.Name = "gridColumnPartsEnd";
            this.gridColumnPartsEnd.OptionsFilter.AllowFilter = false;
            this.gridColumnPartsEnd.Visible = true;
            this.gridColumnPartsEnd.VisibleIndex = 2;
            // 
            // repositoryItemDateEditPartEnd
            // 
            this.repositoryItemDateEditPartEnd.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemDateEditPartEnd.AutoHeight = false;
            this.repositoryItemDateEditPartEnd.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEditPartEnd.DisplayFormat.FormatString = "MM/dd/yyyy HH:mm";
            this.repositoryItemDateEditPartEnd.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.repositoryItemDateEditPartEnd.EditFormat.FormatString = "MM/dd/yyyy HH:mm";
            this.repositoryItemDateEditPartEnd.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.repositoryItemDateEditPartEnd.Mask.EditMask = "MM/dd/yyyy HH:mm";
            this.repositoryItemDateEditPartEnd.Name = "repositoryItemDateEditPartEnd";
            this.repositoryItemDateEditPartEnd.ShowToday = false;
            this.repositoryItemDateEditPartEnd.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonAdd,
            this.toolStripButtonDelete});
            this.toolStrip1.Location = new System.Drawing.Point(10, 298);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(623, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButtonAdd
            // 
            this.toolStripButtonAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonAdd.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonAdd.Image")));
            this.toolStripButtonAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonAdd.Name = "toolStripButtonAdd";
            this.toolStripButtonAdd.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonAdd.Text = "toolStripButtonAdd";
            this.toolStripButtonAdd.ToolTipText = "Agregar intervalo";
            this.toolStripButtonAdd.Click += new System.EventHandler(this.toolStripButtonAdd_Click);
            // 
            // toolStripButtonDelete
            // 
            this.toolStripButtonDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonDelete.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonDelete.Image")));
            this.toolStripButtonDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonDelete.Name = "toolStripButtonDelete";
            this.toolStripButtonDelete.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonDelete.Text = "toolStripButtonDelete";
            this.toolStripButtonDelete.ToolTipText = "Eliminar intervalo";
            this.toolStripButtonDelete.Click += new System.EventHandler(this.toolStripButtonDelete_Click);
            // 
            // buttonExOk
            // 
            this.buttonExOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonExOk.Enabled = false;
            this.buttonExOk.Location = new System.Drawing.Point(389, 487);
            this.buttonExOk.Name = "buttonExOk";
            this.buttonExOk.Size = new System.Drawing.Size(75, 25);
            this.buttonExOk.StyleController = this.TrainingCourseScheduleFormConvertedLayout;
            this.buttonExOk.TabIndex = 2;
            this.buttonExOk.Text = "Accept";
            this.buttonExOk.Click += new System.EventHandler(this.buttonExOk_Click);
            // 
            // TrainingCourseScheduleFormConvertedLayout
            // 
            this.TrainingCourseScheduleFormConvertedLayout.AllowCustomizationMenu = false;
            this.TrainingCourseScheduleFormConvertedLayout.Appearance.DisabledLayoutGroupCaption.ForeColor = System.Drawing.SystemColors.GrayText;
            this.TrainingCourseScheduleFormConvertedLayout.Appearance.DisabledLayoutGroupCaption.Options.UseForeColor = true;
            this.TrainingCourseScheduleFormConvertedLayout.Appearance.DisabledLayoutItem.ForeColor = System.Drawing.SystemColors.GrayText;
            this.TrainingCourseScheduleFormConvertedLayout.Appearance.DisabledLayoutItem.Options.UseForeColor = true;
            this.TrainingCourseScheduleFormConvertedLayout.Controls.Add(this.gridControlParts);
            this.TrainingCourseScheduleFormConvertedLayout.Controls.Add(this.gridControlSchedules);
            this.TrainingCourseScheduleFormConvertedLayout.Controls.Add(this.toolStrip1);
            this.TrainingCourseScheduleFormConvertedLayout.Controls.Add(this.textBoxExCourse);
            this.TrainingCourseScheduleFormConvertedLayout.Controls.Add(this.toolStrip2);
            this.TrainingCourseScheduleFormConvertedLayout.Controls.Add(this.buttonExApply);
            this.TrainingCourseScheduleFormConvertedLayout.Controls.Add(this.buttonExCancel);
            this.TrainingCourseScheduleFormConvertedLayout.Controls.Add(this.buttonExOk);
            this.TrainingCourseScheduleFormConvertedLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TrainingCourseScheduleFormConvertedLayout.Location = new System.Drawing.Point(0, 0);
            this.TrainingCourseScheduleFormConvertedLayout.Name = "TrainingCourseScheduleFormConvertedLayout";
            this.TrainingCourseScheduleFormConvertedLayout.Root = this.layoutControlGroup1;
            this.TrainingCourseScheduleFormConvertedLayout.Size = new System.Drawing.Size(642, 518);
            this.TrainingCourseScheduleFormConvertedLayout.TabIndex = 5;
            // 
            // buttonExApply
            // 
            this.buttonExApply.Enabled = false;
            this.buttonExApply.Location = new System.Drawing.Point(561, 487);
            this.buttonExApply.Name = "buttonExApply";
            this.buttonExApply.Size = new System.Drawing.Size(75, 25);
            this.buttonExApply.StyleController = this.TrainingCourseScheduleFormConvertedLayout;
            this.buttonExApply.TabIndex = 4;
            this.buttonExApply.Text = "Aplicar";
            this.buttonExApply.Click += new System.EventHandler(this.buttonExApply_Click);
            // 
            // buttonExCancel
            // 
            this.buttonExCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonExCancel.Location = new System.Drawing.Point(475, 487);
            this.buttonExCancel.Name = "buttonExCancel";
            this.buttonExCancel.Size = new System.Drawing.Size(75, 25);
            this.buttonExCancel.StyleController = this.TrainingCourseScheduleFormConvertedLayout;
            this.buttonExCancel.TabIndex = 3;
            this.buttonExCancel.Text = "Cancelar";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlGroupCourseInf,
            this.layoutControlGroupSchedules,
            this.layoutControlGroupParts,
            this.emptySpaceItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(642, 518);
            this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.buttonExApply;
            this.layoutControlItem6.CustomizationFormText = "buttonExApplyitem";
            this.layoutControlItem6.Location = new System.Drawing.Point(554, 480);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem6.Name = "buttonExApplyitem";
            this.layoutControlItem6.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "buttonExApplyitem";
            this.layoutControlItem6.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.buttonExCancel;
            this.layoutControlItem7.CustomizationFormText = "buttonExCancelitem";
            this.layoutControlItem7.Location = new System.Drawing.Point(468, 480);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem7.Name = "buttonExCancelitem";
            this.layoutControlItem7.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "buttonExCancelitem";
            this.layoutControlItem7.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.buttonExOk;
            this.layoutControlItem8.CustomizationFormText = "buttonExOkitem";
            this.layoutControlItem8.Location = new System.Drawing.Point(382, 480);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem8.Name = "buttonExOkitem";
            this.layoutControlItem8.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "buttonExOkitem";
            this.layoutControlItem8.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlGroupCourseInf
            // 
            this.layoutControlGroupCourseInf.CustomizationFormText = "layoutControlGroupCourseInf";
            this.layoutControlGroupCourseInf.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemName});
            this.layoutControlGroupCourseInf.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupCourseInf.Name = "layoutControlGroupCourseInf";
            this.layoutControlGroupCourseInf.Size = new System.Drawing.Size(640, 55);
            this.layoutControlGroupCourseInf.Text = "layoutControlGroupCourseInf";
            // 
            // layoutControlItemName
            // 
            this.layoutControlItemName.Control = this.textBoxExCourse;
            this.layoutControlItemName.CustomizationFormText = "layoutControlItemName";
            this.layoutControlItemName.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemName.Name = "layoutControlItemName";
            this.layoutControlItemName.Size = new System.Drawing.Size(634, 31);
            this.layoutControlItemName.Text = "layoutControlItemName";
            this.layoutControlItemName.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemName.TextSize = new System.Drawing.Size(114, 20);
            // 
            // layoutControlGroupSchedules
            // 
            this.layoutControlGroupSchedules.CustomizationFormText = "layoutControlGroupSchedules";
            this.layoutControlGroupSchedules.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5,
            this.layoutControlItem2});
            this.layoutControlGroupSchedules.Location = new System.Drawing.Point(0, 55);
            this.layoutControlGroupSchedules.Name = "layoutControlGroupSchedules";
            this.layoutControlGroupSchedules.Size = new System.Drawing.Size(640, 215);
            this.layoutControlGroupSchedules.Text = "layoutControlGroupSchedules";
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.toolStrip2;
            this.layoutControlItem5.CustomizationFormText = "toolStrip2item";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(0, 31);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(111, 31);
            this.layoutControlItem5.Name = "toolStrip2item";
            this.layoutControlItem5.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 0);
            this.layoutControlItem5.Size = new System.Drawing.Size(634, 31);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "toolStrip2item";
            this.layoutControlItem5.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.gridControlSchedules;
            this.layoutControlItem2.CustomizationFormText = "gridControlSchedulesitem";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 31);
            this.layoutControlItem2.Name = "gridControlSchedulesitem";
            this.layoutControlItem2.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 0, 5);
            this.layoutControlItem2.Size = new System.Drawing.Size(634, 160);
            this.layoutControlItem2.Text = "gridControlSchedulesitem";
            this.layoutControlItem2.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlGroupParts
            // 
            this.layoutControlGroupParts.CustomizationFormText = "layoutControlGroupParts";
            this.layoutControlGroupParts.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.layoutControlItem1});
            this.layoutControlGroupParts.Location = new System.Drawing.Point(0, 270);
            this.layoutControlGroupParts.Name = "layoutControlGroupParts";
            this.layoutControlGroupParts.Size = new System.Drawing.Size(640, 210);
            this.layoutControlGroupParts.Text = "layoutControlGroupParts";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.toolStrip1;
            this.layoutControlItem3.CustomizationFormText = "toolStrip1item";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(0, 31);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(111, 31);
            this.layoutControlItem3.Name = "toolStrip1item";
            this.layoutControlItem3.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 0);
            this.layoutControlItem3.Size = new System.Drawing.Size(634, 31);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "toolStrip1item";
            this.layoutControlItem3.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridControlParts;
            this.layoutControlItem1.CustomizationFormText = "gridControlPartsitem";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 31);
            this.layoutControlItem1.Name = "gridControlPartsitem";
            this.layoutControlItem1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 0, 5);
            this.layoutControlItem1.Size = new System.Drawing.Size(634, 155);
            this.layoutControlItem1.Text = "gridControlPartsitem";
            this.layoutControlItem1.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 480);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(382, 36);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // TrainingCourseScheduleForm
            // 
            this.AcceptButton = this.buttonExOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonExCancel;
            this.ClientSize = new System.Drawing.Size(642, 518);
            this.Controls.Add(this.TrainingCourseScheduleFormConvertedLayout);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(650, 552);
            this.Name = "TrainingCourseScheduleForm";
            this.Text = "Horarios del curso de entrenamiento";
            this.Load += new System.EventHandler(this.TrainingCourseScheduleForm_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TrainingCourseScheduleForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSchedules)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSchedules)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditStart.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditEnd.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditMinimumAttendance)).EndInit();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlParts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewParts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditPartStart.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditPartStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditPartEnd.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditPartEnd)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TrainingCourseScheduleFormConvertedLayout)).EndInit();
            this.TrainingCourseScheduleFormConvertedLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupCourseInf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupSchedules)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupParts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TextBoxEx textBoxExCourse;
        private DevExpress.XtraEditors.SimpleButton buttonExOk;
        private DevExpress.XtraEditors.SimpleButton buttonExCancel;
        private DevExpress.XtraEditors.SimpleButton buttonExApply;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButtonAdd;
        private System.Windows.Forms.ToolStripButton toolStripButtonDelete;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton toolStripButtonAddSchedule;
        private System.Windows.Forms.ToolStripButton toolStripButtonRemoveSchedule;
		private GridControlEx gridControlSchedules;
        private GridViewEx gridViewSchedules;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnStart;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnEnd;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnStatus;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEditStart;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditMinimumAttendance;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEditEnd;
		private GridControlEx gridControlParts;
		private GridViewEx gridViewParts;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnRoom;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnPartsStart;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEditPartStart;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnPartsEnd;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEditPartEnd;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnTrainer;
        private DevExpress.XtraLayout.LayoutControl TrainingCourseScheduleFormConvertedLayout;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupCourseInf;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemName;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupSchedules;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupParts;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.Converter.LayoutConverter layoutConverter1;
    }
}