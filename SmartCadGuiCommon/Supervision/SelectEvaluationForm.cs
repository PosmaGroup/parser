using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Smartmatic.SmartCad.Service;
using System.Collections;
using SmartCadCore.ClientData;
using SmartCadCore.Core;
using SmartCadCore.Common;

namespace SmartCadGuiCommon
{
    public partial class SelectEvaluationForm : Form
    {

        private EvaluationClientData operatorEvaluation;
        
        public EvaluationClientData OperatorEvaluation
        {
            get { return operatorEvaluation; }
            set { operatorEvaluation = value; }
        }

        public SelectEvaluationForm()
        {
            InitializeComponent();
            LoadLanguage();
        }

        public SelectEvaluationForm(int operatorCode, string supervisedApplicationName) : this()
        {
            ServerServiceClient server = ServerServiceClient.GetInstance();
            string hql = SmartCadHqls.GetCustomHql(SmartCadHqls.GetOperatorByCode,operatorCode);
            OperatorClientData ope = (OperatorClientData)(server.SearchClientObjects(hql)[0]);

            OperatorCategoryHistoryClientData category = GetActualCategory(ope);
            
            
            //get application by user
            IList listAppByUser = GetApplicationsByUser(operatorCode);

            //get evaluations
            hql = @"SELECT eva FROM EvaluationData eva left join fetch eva.SetEvaluationUserApplications left join fetch eva.SetEvaluationDepartamentsType WHERE eva.OperatorCategory.Code = " + category.CategoryCode;
            IList listEvaluations = server.SearchClientObjects(hql);
            foreach (EvaluationClientData evaluation in listEvaluations)
            {
                
                bool foundApp = false;
                foundApp = EvaluationApplyToUserApplications(listAppByUser, evaluation);

                bool foundDep = false;
                
                if (supervisedApplicationName == UserApplicationClientData.FirstLevel.Name) 
                    foundDep = true;
                else
                    foundDep = EvaluationApplyToUserDepartaments(ope, evaluation);
                
                if (foundApp && foundDep)
                {
                    this.listBoxControlEvaluations.Items.Add(evaluation);
                }
            }
            checkedListBoxEvaluation_SelectedIndexChanged(null, null);
        }

        private void LoadLanguage()
        {
            layoutControlItemEvaluations.Text = ResourceLoader.GetString2("EvaluationToApply") + ":";
            buttonOK.Text = ResourceLoader.GetString2("Accept");
            buttonCancel.Text = ResourceLoader.GetString2("Cancel");
            this.Text = ResourceLoader.GetString2("EvaluationForm");
        }
        
        private static bool EvaluationApplyToUserDepartaments(OperatorClientData ope, EvaluationClientData evaluation)
        {
            bool retval = false;
            foreach (DepartmentTypeClientData dep in ope.DepartmentTypes)
            {
                foreach (EvaluationDepartmentTypeClientData depEva in evaluation.Departments)
                {
                    if (dep.Code == depEva.Department.Code)
                    {
                        retval = true;
                        break;
                    }
                }
                if (retval)
                    break;
            }
            return retval;
        }

        private bool EvaluationApplyToUserApplications(IList listAppByUser, EvaluationClientData evaluation)
        {
            bool foundApp = false;
            foreach (UserApplicationClientData app in listAppByUser)
            {
                foreach (EvaluationUserApplicationClientData appEva in evaluation.Applications)
                {
                    if (app.Code == appEva.Application.Code)
                    {
                        foundApp = true;
                        break;
                    }
                }
                if (foundApp)
                    break;
            }
            return foundApp;
        }

        private IList GetApplicationsByUser(int operatorCode)
        {
            string hql = @"SELECT distinct userApp
                            FROM OperatorData ope
                                INNER JOIN  ope.Role.Profiles pro
                                INNER JOIN  pro.Accesses acc
                                INNER JOIN  acc.UserApplication userApp 
                            WHERE 
                                ope.Code = " + operatorCode;
            return ServerServiceClient.GetInstance().SearchClientObjects(hql);
        }

        private OperatorCategoryHistoryClientData GetActualCategory(OperatorClientData ope)
        {
            foreach (OperatorCategoryHistoryClientData opecat in ope.CategoryList)
            {
                if (opecat.EndDate == DateTime.MinValue)
                {
                    return opecat;
                }
            }
            return null;
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {

        }

        private void checkedListBoxEvaluation_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.buttonOK.Enabled = this.listBoxControlEvaluations.SelectedIndex != -1;
            if (this.buttonOK.Enabled) 
            {
                OperatorEvaluation = this.listBoxControlEvaluations.SelectedItem as EvaluationClientData;
            }
        }

        private void panelControl1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}