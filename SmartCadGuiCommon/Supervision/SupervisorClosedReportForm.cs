using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Smartmatic.SmartCad.Service;
using System.Reflection;
using System.ServiceModel;
using System.Collections;
using DevExpress.XtraEditors;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadCore.Core;

namespace SmartCadGuiCommon
{
    public partial class SupervisorClosedReportForm : XtraForm
    {
        private SupervisorCloseReportClientData report;
        private SupervisorCloseReportMessageClientData lastMessage;

        public SupervisorClosedReportForm()
        {
            InitializeComponent();
            LoadLanguage();
            LoadData();
            this.Activated += new EventHandler(SupervisorClosedReportForm_Activated);
        }

        void SupervisorClosedReportForm_Activated(object sender, EventArgs e)
        {
            if (this.MdiParent != null)
                (this.MdiParent as SupervisionForm).RibbonControl.SelectedPage = (this.MdiParent as SupervisionForm).RibbonPageOperation;
    
        }

        private void LoadLanguage()
        {
            this.Text = ResourceLoader.GetString2("SupervisorClosedReportForm");
            this.layoutControlGroupCloseMessages.Text = ResourceLoader.GetString2("ClosedSystemMessages") + ":";
            this.layoutControlGroupNewMessage.Text = ResourceLoader.GetString2("Observations") + ":";
            this.simpleButtonAdd.Text = ResourceLoader.GetString2("Add");

            this.RibbonPageOperation.Text = ResourceLoader.GetString2("Operation");
            this.ribbonPageGroupFinalReport.Text = ResourceLoader.GetString2("CloseReport");
            this.ribbonPageGroupOptions.Text = ResourceLoader.GetString2("GeneralOptions");

            this.barButtonItemEndReport.Caption = ResourceLoader.GetString2("Finalize");
            this.barButtonItemRedo.Caption = ResourceLoader.GetString2("Redo");
            this.barButtonItemUndo.Caption = ResourceLoader.GetString2("Undo");
            this.barButtonItemCut.Caption = ResourceLoader.GetString2("Cut");
            this.barButtonItemCopy.Caption = ResourceLoader.GetString2("Copy");
            this.barButtonItemPaste.Caption = ResourceLoader.GetString2("Paste");

            this.barButtonItemPrint.Caption = ResourceLoader.GetString2("Print");
            this.barButtonItemSave.Caption = ResourceLoader.GetString2("Save");
            this.barButtonItemSend.Caption = ResourceLoader.GetString2("Send");
            this.barButtonItemUpdate.Caption = ResourceLoader.GetString2("Update");

            #region Ribbon bar ToolTips
            this.barButtonItemPrint.SuperTip = SupervisionForm.BuildToolTip("ToolTip_Print");
            this.barButtonItemSave.SuperTip = SupervisionForm.BuildToolTip("ToolTip_Save");
            
            this.barButtonItemCut.SuperTip = SupervisionForm.BuildToolTip("ToolTip_Cut");
            this.barButtonItemCopy.SuperTip = SupervisionForm.BuildToolTip("ToolTip_Copy");
            this.barButtonItemPaste.SuperTip = SupervisionForm.BuildToolTip("ToolTip_Paste");
            this.barButtonItemUndo.SuperTip = SupervisionForm.BuildToolTip("ToolTip_Undo");
            this.barButtonItemRedo.SuperTip = SupervisionForm.BuildToolTip("ToolTip_Redo");
            this.barButtonItemEndReport.SuperTip = SupervisionForm.BuildToolTip("ToolTip_FinalizeClosingReport");
            this.barButtonItemEndReport.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N));

            this.barButtonItemPrint.SuperTip = SupervisionForm.BuildToolTip("ToolTip_Print");
            this.barButtonItemSave.SuperTip = SupervisionForm.BuildToolTip("ToolTip_Save");
            this.barButtonItemSend.SuperTip = SupervisionForm.BuildToolTip("ToolTip_Send");
            this.barButtonItemUpdate.SuperTip = SupervisionForm.BuildToolTip("ToolTip_Update");

            #endregion
        }

        private void LoadData()
        {
            IList list = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetCurrentCloseReportBySession, ServerServiceClient.GetInstance().CurrentSession.Code));
            if (list.Count > 0)
            {
                report = (SupervisorCloseReportClientData)list[0];
                barButtonItemEndReport.Enabled = !report.Finished;
                richTextBoxCloseMessage.Enabled = !report.Finished;
                barButtonItemEndReport.Enabled = !report.Finished;
                foreach (SupervisorCloseReportMessageClientData scrmcd in report.Messages)
                {
                    PrintMessage(scrmcd.Time, scrmcd.Message);
                }
            }
            else
            {
                report = new SupervisorCloseReportClientData();
                report.SessionCode = ServerServiceClient.GetInstance().CurrentSession.Code;
                report.Messages = new ArrayList();
            }
            InitPaste();
        }

        private void simpleButtonAdd_Click(object sender, EventArgs e)
        {
            lastMessage = new SupervisorCloseReportMessageClientData();
            lastMessage.Message = richTextBoxCloseMessage.Text;
            lastMessage.Time = ServerServiceClient.GetInstance().GetTime();
            if (report.Code == 0)
            {
                report.Messages.Add(lastMessage);
            }
            else
            {
                lastMessage.SupervisorCloseReportCode = report.Code;
            }
            Save(true);
            barButtonItemEndReport.Enabled = true;
        }
            
        private void richTextBoxCloseMessage_TextChanged(object sender, EventArgs e)
        {
            simpleButtonAdd.Enabled = richTextBoxCloseMessage.Text.Trim().Length > 0;
        }

        private void Save(bool printMessage)
        {
            if (simpleButtonAdd.Enabled == true || report.Finished == true)
            {                
                try
                {
                    BackgroundProcessForm processForm = new BackgroundProcessForm(this, new MethodInfo[1] { GetType().GetMethod("Save_Help", BindingFlags.NonPublic | BindingFlags.Instance) }, new object[1][] { new object[0] });
                    processForm.CanThrowError = true;
                    processForm.ShowDialog();

                    if (printMessage == true)
                    {
                        PrintMessage(lastMessage.Time, lastMessage.Message);
                        richTextBoxCloseMessage.Focus();
                        richTextBoxCloseMessage.Clear();
                    }
                }
                catch (FaultException ex)
                {
                    LoadData();
                    DialogResult = DialogResult.None;
                    MessageForm.Show(ex.Message, MessageFormType.Error);
                }
                catch (Exception ex)
                {
                    DialogResult = DialogResult.None;
                    MessageForm.Show(ex.Message, ex);
                }
            }
        }

        private void Save_Help()
        {
            if (report.Code == 0 || report.Finished == true)
            {
                ServerServiceClient.GetInstance().SaveOrUpdateClientData(report);
                IList list = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetCurrentCloseReportBySessionWithoutMessages, ServerServiceClient.GetInstance().CurrentSession.Code));
                if (list.Count > 0)
                {
                    report = (SupervisorCloseReportClientData)list[0];
                }
            }
            else
                ServerServiceClient.GetInstance().SaveOrUpdateClientData(lastMessage);
        }

        private void PrintMessage(DateTime date, string message)
        {
            int initIndex = richTextBoxCloseMessages.Text.Length;
            richTextBoxCloseMessages.AppendText(date.ToString("MM/dd/yyyy HH:mm") + ": ");
            richTextBoxCloseMessages.Select(initIndex, richTextBoxCloseMessages.Text.Length);
            richTextBoxCloseMessages.SelectionFont = new Font(richTextBoxCloseMessages.Font.FontFamily, richTextBoxCloseMessages.Font.Size, FontStyle.Bold);
            richTextBoxCloseMessages.AppendText(Environment.NewLine);
            richTextBoxCloseMessages.AppendText(message.Trim());
            richTextBoxCloseMessages.AppendText(Environment.NewLine);
            richTextBoxCloseMessages.AppendText(Environment.NewLine);
 
            richTextBoxCloseMessages.ScrollToCaret();
        }

        private bool CancelClosing()
        {
            bool result = false;
            if ((simpleButtonAdd.Enabled == true)&&(report.Finished == false))
            {
                DialogResult dr = MessageForm.Show(ResourceLoader.GetString2("ClosingSupervisorCloseReportForm"), MessageFormType.YesNoCancel);
                if (dr == DialogResult.Yes)
                {
                    simpleButtonAdd.PerformClick();
                }
                else if (dr == DialogResult.Cancel)
                {
                    result = true;
                }
            }
            return result;
        }

        private void SupervisorClosedReportForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = CancelClosing();
        }

        private void barButtonItemEndReport_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (simpleButtonAdd.Enabled == true)
            {
                DialogResult dr = MessageForm.Show(ResourceLoader.GetString2("FinishedObservationSupervisorCloseReportForm"), MessageFormType.Question);
                if (dr == DialogResult.Yes)
                {
                    simpleButtonAdd.PerformClick();
                }

            }

            DialogResult dr1 = MessageForm.Show(ResourceLoader.GetString2("FinishedSupervisorCloseReportForm"), MessageFormType.Question);
            if (dr1 == DialogResult.Yes)
            {
                report.Finished = true;
                richTextBoxCloseMessage.Enabled = !report.Finished;
                barButtonItemEndReport.Enabled = !report.Finished;
                Save(false);
                (this.ParentForm as SupervisionForm).barButtonItemReport.Enabled = !report.Finished;
                this.Close();
            }
        }

        private void barButtonItemRedo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            richTextBoxCloseMessage.Redo();
            InitRedo();
            InitUndo();
        }

        private void barButtonItemUndo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            richTextBoxCloseMessage.Undo();
            InitRedo();
            InitUndo();
        }

        private void PrintPreviewBarItemCut_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            richTextBoxCloseMessage.Cut();
            InitPaste();
        }

        private void PrintPreviewBarItemCopy_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (richTextBoxCloseMessage.SelectionLength > 0)
                richTextBoxCloseMessage.Copy();
            else
                richTextBoxCloseMessages.Copy();
            InitPaste();
        }

        private void PrintPreviewBarItemPaste_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            richTextBoxCloseMessage.Paste(DataFormats.GetFormat("Text"));
            InitRedo();
        }

        private void barButtonItemSend_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }

        private void barButtonItemPrint_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }

        private void barButtonItemSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }

        private void richTextBoxCloseMessage_SelectionChanged(object sender, EventArgs e)
        {
            InitEdit(richTextBoxCloseMessage.SelectionLength > 0);
        }

        private void richTextBoxCloseMessages_SelectionChanged(object sender, EventArgs e)
        {
            InitEdit(richTextBoxCloseMessages.SelectionLength > 0);
        }

        private void InitEdit(bool enabled)
        {
            barButtonItemCut.Enabled = enabled;
            barButtonItemCopy.Enabled = enabled;
            InitRedo();
            InitUndo();
        }

        private void InitRedo()
        {
            barButtonItemRedo.Enabled = richTextBoxCloseMessage.CanRedo;
        }

        private void InitUndo()
        {
            barButtonItemUndo.Enabled = richTextBoxCloseMessage.CanUndo;
        }

        private void InitPaste()
        {
            bool enabledPase = richTextBoxCloseMessage.CanPaste(DataFormats.GetFormat("Text"));
            barButtonItemPaste.Enabled = enabledPase;
        }
              
    }
}
