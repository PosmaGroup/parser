using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors.Controls;
using System.Collections;
using Smartmatic.SmartCad.Service;
using System.Reflection;
using System.ServiceModel;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadCore.Core;

namespace SmartCadGuiCommon
{
    public partial class ForecastOperationForm : Form
    {
        IndicatorGroupForecastClientData data;
        ForecastOperationFormOperation operation = ForecastOperationFormOperation.Create;
        IndicatorGroupForecastClientData replace;
        SupervisionForm parent;
        private int indexDispatch;
        private int indexCalls;
        private int indexFirstLevel;
        private bool isCollapseCallsFirtsLevel = false;
        private int forecastSeparationTime;
        private bool buttonOkPressed;

        private Dictionary<int, List<int>> IndicatorsValues;
        List<int> listParentsRowsHandles; 

        public ForecastOperationForm(IndicatorGroupForecastClientData data, ForecastOperationFormOperation operation, SupervisionForm parent,int timeSeparationForecast)
        {            
            InitializeComponent();
            listParentsRowsHandles = new List<int>();
            this.data = data;
            this.operation = operation;
            this.parent = parent;
            this.forecastSeparationTime = timeSeparationForecast;
            this.IndicatorsValues = new Dictionary<int, List<int>>();
            buttonOkPressed = false;
            LoadLanguage();

            LoadData();
            LoadEvents();
            gridControlForecastDetails.CellToolTipNeeded += new DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventHandler(gridControlForecastDetails_CellToolTipNeeded);

        }

        void gridControlForecastDetails_CellToolTipNeeded(object sender, DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventArgs e)
        {
            if (e.Info == null) 
            {
                string infoText = string.Empty;
                GridHitInfo info = this.gridViewForecastDetail.CalcHitInfo(e.ControlMousePosition);

                if (info.InRowCell == true && info.Column != null && (info.Column.FieldName == "High" || info.Column.FieldName == "Low")) 
                {
                    infoText = ResourceLoader.GetString2("SetOrSelectValue");
                    e.Info = new DevExpress.Utils.ToolTipControlInfo(new DevExpress.XtraGrid.Views.Base.CellToolTipInfo(info.RowHandle, info.Column, "cell"), infoText);
                                    
                }
            }    
        }

        private void LoadLanguage()
        {
            this.layoutControlGroupInfo.Text = ResourceLoader.GetString2("Data");
            this.layoutControlGroupParameters.Text = ResourceLoader.GetString2("Parameters");
            this.layoutControlItemName.Text = ResourceLoader.GetString2("Name")+" *";
            this.layoutControlItemDescription.Text = ResourceLoader.GetString2("$Message.Description") + " *";
            this.layoutControlItemDepartment.Text = ResourceLoader.GetString2("Department");
            this.layoutControlItemFrom.Text = ResourceLoader.GetString2("StartDate") + " *";
            this.layoutControlItemTo.Text = ResourceLoader.GetString2("EndDate") +" *";

            this.gridColumnIndicator.Caption = ResourceLoader.GetString2("Indicators");
            this.gridColumnIndicatorType.Caption = ResourceLoader.GetString2("IndicatorType");
            this.gridColumnPostivePattern.Caption = ResourceLoader.GetString2("PositivePattern");
            this.gridColumnLow.Caption = ResourceLoader.GetString2("ThresholdLow");
            this.gridColumnHigh.Caption = ResourceLoader.GetString2("ThresholdHigh");
            this.gridColumnMeasureUnit.Caption = ResourceLoader.GetString2("MeasureUnit");

            this.simpleButtonCancel.Text = ResourceLoader.GetString2("Cancel");
            this.simpleButtonApply.Text = ResourceLoader.GetString2("Create");
        }

        private void LoadData()
        {
            IList departmentList = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetDepartmentsType);
            comboBoxEditDepartment.Properties.Items.AddRange(departmentList);
            comboBoxEditDepartment.Properties.Items.Insert(0, ResourceLoader.GetString2("All"));
            comboBoxEditDepartment.SelectedIndex = 0;
            dateEditFrom.Properties.MinValue = ServerServiceClient.GetInstance().GetTime();

            if (operation == ForecastOperationFormOperation.Create)
            {
                this.Text = ResourceLoader.GetString2("CreateForecastOperationForm");
                this.Icon = ResourceLoader.GetIcon("$Icon.Create");
                dateEditFrom.DateTime = DateTime.Now;
                dateEditTo.DateTime = DateTime.Now.AddMinutes(forecastSeparationTime);
                data.Code = 0;
                data.General = false;
                data.FactoryData = false;
            }
            else if (operation == ForecastOperationFormOperation.Update)
            {
                this.Text = ResourceLoader.GetString2("ChangeForecastOperationForm");
                this.Icon = ResourceLoader.GetIcon("$Icon.Update");
                textEditName.Text = data.Name;
                memoEditDescription.Text = data.Description;
                dateEditFrom.DateTime = data.Start;
                dateEditFrom.Enabled = data.Start > ServerServiceClient.GetInstance().GetTime();
                dateEditTo.DateTime = data.End;
                simpleButtonApply.Enabled = true;
                data.FactoryData = false;
                this.simpleButtonApply.Text = ResourceLoader.GetString2("Accept");
            }
            else if (operation == ForecastOperationFormOperation.Duplicate)
            {
                if (replace == null)
                {
                    replace = data;
                    data = new IndicatorGroupForecastClientData();
                }
                data.FactoryData = false;
                this.Text = ResourceLoader.GetString2("DuplicateForecastOperationForm");
                this.Icon = ResourceLoader.GetIcon("$Icon.Duplicate");
                textEditName.Text = replace.Name + "_" + ResourceLoader.GetString2("DuplicateNameCopy");
                memoEditDescription.Text = replace.Description;
                dateEditFrom.DateTime = replace.Start;
                dateEditTo.DateTime = replace.End;
                simpleButtonApply.Enabled = true;
                data.General = false;
                data.FactoryData = false;
                data.ForecastValues = new ArrayList();
                foreach (IndicatorForecastClientData forecast in replace.ForecastValues)
                {
                    forecast.IndicatorGroupForecastCode = 0;
                    forecast.Code = 0;
                    forecast.Version = 0;
                    data.ForecastValues.Add(forecast);
                }
            }
            else if (operation == ForecastOperationFormOperation.Replace)
            {
                if (replace == null)
                {
                    replace = data;
                    data = new IndicatorGroupForecastClientData();
                }
                data.FactoryData = false;
                this.Text = ResourceLoader.GetString2("ReplaceForecastOperationForm");
                this.Icon = ResourceLoader.GetIcon("$Icon.Replace");
                data = (IndicatorGroupForecastClientData)data.Clone();
                textEditName.Text = replace.Name + "_" + ResourceLoader.GetString2("DuplicateNameCopy");
                memoEditDescription.Text = replace.Description;
                comboBoxEditDepartment.Enabled = false;
                dateEditFrom.DateTime = DateTime.Now;
                dateEditTo.Enabled = false;
                
                if (replace.General == true)
                {
                    data.General = true;
                    dateEditTo.DateTime = DateTime.MaxValue;
                    dateEditFrom.Properties.MaxValue = DateTime.MaxValue;
                }
                else
                {
                    data.General = false;
                    dateEditTo.DateTime = replace.End;
                    dateEditFrom.Properties.MaxValue = data.End;
                }
                data.ForecastValues = new ArrayList();
                foreach (IndicatorForecastClientData forecast in replace.ForecastValues)
                {
                    forecast.IndicatorGroupForecastCode = 0;
                    forecast.Code = 0;
                    forecast.Version = 0;
                    data.ForecastValues.Add(forecast);
                }
            }
            else if (operation == ForecastOperationFormOperation.Restore)
            {
                replace = data;
                this.Text = ResourceLoader.GetString2("RestoreForecastOperationForm");
                this.Icon = ResourceLoader.GetIcon("$Icon.Restore");
                data = (IndicatorGroupForecastClientData)data.Clone();
                this.gridColumnHigh.OptionsColumn.AllowEdit = false;
                this.gridColumnLow.OptionsColumn.AllowEdit = false;
                memoEditDescription.Text = replace.Description;
                data.FactoryData = true;
                dateEditFrom.DateTime = DateTime.Now;
                dateEditTo.DateTime = DateTime.MaxValue;
                dateEditFrom.Properties.MaxValue = DateTime.MaxValue;
                dateEditTo.Enabled = false;
                dateEditFrom.Enabled = false;
                textEditName.Text = ResourceLoader.GetString2("PredeterminatedValues") + "_" + DateTime.Now.ToString("yyyyMMddHHmm");
            }

            IndicatorGroupForecastClientData generalFactory = null;
            if ((operation == ForecastOperationFormOperation.Create) || (operation == ForecastOperationFormOperation.Restore))
            {
                string hql = SmartCadHqls.GetIndicatorGroupForecastFactoryWithValues;
                IList list = ServerServiceClient.GetInstance().SearchClientObjects(hql);
                if (list.Count < 1)
                {
                    MessageForm.Show("No hay un indicador general de fabrica definido", MessageFormType.Error);
                    this.Close();
                }
                else
                {
                    generalFactory = (IndicatorGroupForecastClientData)list[0];
                }
            }
            else
            {
                generalFactory = data;
            }


            List<IndicatorForecastDetailGridData> forecastDetailGridData = new List<IndicatorForecastDetailGridData>();
            foreach (IndicatorForecastClientData item in generalFactory.ForecastValues)
            {
                
                if (operation != ForecastOperationFormOperation.Update)
                    item.Code = 0;
                IndicatorForecastDetailGridData gridData = new IndicatorForecastDetailGridData(item);
                gridData.SetFactoryValues(item.Low, item.High);
                if (item.Code == 0 || forecastDetailGridData.Contains(gridData) == false)
                    forecastDetailGridData.Add(gridData);
            }

            gridControlForecastDetails.BeginInit();
            gridControlForecastDetails.DataSource = forecastDetailGridData;
            gridControlForecastDetails.EndInit();
            gridViewForecastDetail.ExpandAllGroups();
            gridViewForecastDetail.BestFitColumns();

            //for (int i = 0; i < gridViewForecastDetail.RowCount; i++)
            //{
            //    int prh = gridViewForecastDetail.GetParentRowHandle(i);
            //    if (!listParentsRowsHandles.Contains(prh))
            //    {
            //        listParentsRowsHandles.Add(prh);
            //        IndicatorForecastDetailGridData aux = gridViewForecastDetail.GetRow(i) as IndicatorForecastDetailGridData;
            //        if (aux != null)
            //        {
            //            if (aux.Data.Indicator.TypeName == IndicatorTypeData.Calls.Name)
            //                indexCalls = prh;
            //            else if (aux.Data.Indicator.TypeName == IndicatorTypeData.Dispatch.Name)
            //                indexDispatch = prh;
            //            else if (aux.Data.Indicator.TypeName == IndicatorTypeData.FirstLevel.Name)
            //                indexFirstLevel = prh;
            //        }
            //    }
            //}
            comboBoxEditDepartment.SelectedItem = 0;

            //if(operation == ForecastOperationFormOperation.Update)
            //    CheckComboBox();
        }

        private void CheckComboBox()
        {
            if (data.ForecastValues != null)
            {
                IndicatorForecastClientData ifcd = data.ForecastValues[0] as IndicatorForecastClientData;
                if (ifcd.DepartmentType != null)
                {
                    comboBoxEditDepartment.EditValue = ifcd.DepartmentType;
                }
                else
                {
                    comboBoxEditDepartment.SelectedItem = 0;
                }
            }
            else
            {
                comboBoxEditDepartment.SelectedItem = 0;
            }
        }

        private void LoadEvents()
        {
            parent.SupervisionCommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(parent_SupervisionCommittedChanges);
        }

        void parent_SupervisionCommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
        {
            try
            {
                if (e.Objects != null && e.Objects.Count > 0)
                {
                    try
                    {
                        if (e.Objects[0] is IndicatorGroupForecastClientData)
                        {
                            foreach (IndicatorGroupForecastClientData newData in e.Objects)
                            {
                                //IndicatorGroupForecastClientData newData = (IndicatorGroupForecastClientData)e.Objects[0];
                                if (operation != ForecastOperationFormOperation.Create && data.Code == newData.Code)
                                {
                                    if (e.Action != CommittedDataAction.Delete && buttonOkPressed == false)
                                    {
                                        MessageForm.Show(ResourceLoader.GetString2("UpdateForecastOperationForm"), MessageFormType.Information);
                                        data = newData;
                                        LoadData();
                                    }
                                    else
                                    {
                                        if (e.Action == CommittedDataAction.Delete)
                                        {
                                            MessageForm.Show(ResourceLoader.GetString2("DeleteForecastOperationForm"), MessageFormType.Information);
                                            Close();
                                        }
                                    }
                                }
                            }
                        }
                        if (e.Objects[0] is ApplicationPreferenceClientData)
                        {
                            ApplicationPreferenceClientData preference = e.Objects[0] as ApplicationPreferenceClientData;
                            if (e.Action == CommittedDataAction.Update)
                            {
                                if (preference.Name.Equals("TimeIntervalForecastSeparation"))
                                {
                                    forecastSeparationTime = int.Parse(preference.Value);
                                }
                            }
                        }
                    }
                    catch
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        private void dateEditFrom_EditValueChanged(object sender, EventArgs e)
        {
            if (dateEditTo.Enabled == true)
            {
                dateEditTo.Properties.MinValue = dateEditFrom.DateTime;
                if (dateEditTo.DateTime.Subtract(dateEditFrom.DateTime).TotalMinutes < forecastSeparationTime)
                    dateEditTo.DateTime = dateEditFrom.DateTime.AddMinutes(forecastSeparationTime);
            }
            textEditName_EditValueChanged(sender, e);
        }

        private void gridViewForecastDetail_ValidatingEditor(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            IndicatorForecastDetailGridData value = (IndicatorForecastDetailGridData)gridViewForecastDetail.GetFocusedRow();
            if (value.MeasureUnit == ResourceLoader.GetString2("%") &&
                (int.Parse(e.Value.ToString()) < 0 || int.Parse(e.Value.ToString()) > 100))
            {
                e.Valid = false;
                e.ErrorText = ResourceLoader.GetString2("InvalidValueColumnPercentage"); 
            }
        }

        private void gridViewForecastDetail_InvalidValueException(object sender, DevExpress.XtraEditors.Controls.InvalidValueExceptionEventArgs e)
        {
            e.ExceptionMode = ExceptionMode.DisplayError;
            e.WindowCaption = ResourceLoader.GetString2("Error");

            // Destroying the editor and discarding the changes made within the edited cell
            gridViewForecastDetail.HideEditor();
        }

        private void gridViewForecastDetail_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column == gridColumnLow)
            {
                repositoryItemSpinEditHigh.MinValue = int.Parse(e.Value.ToString());
            }
            else if (e.Column == gridColumnHigh)
            {
                repositoryItemSpinEditLow.MaxValue = int.Parse(e.Value.ToString());
            }
            textEditName_EditValueChanged(sender, e);
        }

        private void gridViewForecastDetail_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (e.FocusedRowHandle >= 0)
            {
                IndicatorForecastDetailGridData data = (IndicatorForecastDetailGridData)gridViewForecastDetail.GetFocusedRow();
                repositoryItemSpinEditHigh.MinValue = data.Low;
                repositoryItemSpinEditLow.MaxValue = data.High;
                repositoryItemSpinEditLow.MinValue = 1;
                if (data.MeasureUnit == ResourceLoader.GetString2("%"))
                {
                    repositoryItemSpinEditHigh.MaxValue = 100;
                }
                else
                {
                    repositoryItemSpinEditHigh.MaxValue = 100000;
                }
            }
            else if (e.FocusedRowHandle < 0)
            {
                gridViewForecastDetail.FocusedRowHandle = e.PrevFocusedRowHandle == GridControl.InvalidRowHandle ? 0 : e.PrevFocusedRowHandle;
            }
        }

        private void textEditName_EditValueChanged(object sender, EventArgs e)
        {
            
            if (textEditName.Text.Trim() != string.Empty &&
                memoEditDescription.Text != string.Empty &&
                dateEditFrom.DateTime < dateEditTo.DateTime &&
                ( operation == ForecastOperationFormOperation.Restore || dateEditFrom.DateTime >= DateTime.Now) &&
                CheckValues())
            {
                if (simpleButtonApply.Enabled == false)
                    simpleButtonApply.Enabled = true;
            }
            else 
            {
                if (simpleButtonApply.Enabled == true)
                    simpleButtonApply.Enabled = false;
            }
        }

        private bool CheckValues()
        {
            if (operation == ForecastOperationFormOperation.Replace)
            {

                List<IndicatorForecastDetailGridData> list = (List<IndicatorForecastDetailGridData>)gridViewForecastDetail.DataSource;
                if (list == null)
                    return false;
                bool retval = false;
                foreach (IndicatorForecastDetailGridData item in list)
                {
                    if (item.Low != item.LowFactory || item.High != item.HighFactory)
                    {
                        retval = true;
                        break;
                    }
                }
                return retval;
            }
            else
            {
                return true;
            }
        }

        private void Save()
        {
            try
            {
                BackgroundProcessForm processForm = new BackgroundProcessForm(this, new MethodInfo[1] { GetType().GetMethod("Save_Help", BindingFlags.NonPublic | BindingFlags.Instance) }, new object[1][] { new object[0] });
                processForm.CanThrowError = true;
                processForm.ShowDialog();
            }
            catch (FaultException ex)
            {
                string name = data.Name;
                if (operation == ForecastOperationFormOperation.Update)
                {
                    IndicatorGroupForecastClientData clientAux = (IndicatorGroupForecastClientData)ServerServiceClient.GetInstance().RefreshClient(data);
                    if (clientAux != null)
                    {
                        data = clientAux;
                        name = data.Name;
                    }
                }
                else if (operation == ForecastOperationFormOperation.Replace)
                {
                    IndicatorGroupForecastClientData clientAux = (IndicatorGroupForecastClientData)ServerServiceClient.GetInstance().RefreshClient(replace);
                    if (clientAux != null)
                    {
                        replace = clientAux;
                        name = replace.Name;
                    }
                }
                else if (operation == ForecastOperationFormOperation.Restore)
                {
                    name = replace.Name;
                }
                //LoadData();
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, MessageFormType.Error);
                if (ex.Message.CompareTo(ResourceLoader.GetString2("ErrorTimeSeparationForecast")) == 0)
                {
                    this.dateEditTo.Focus();
                }
                if ((ex.Message.CompareTo(ResourceLoader.GetString2("ErrorMessageModifyForecastActive")) == 0 ||
                    ex.Message.CompareTo(ResourceLoader.GetString2("InvalidForecastUpdated", name)) == 0) ||
                    (ex.Message.CompareTo(ResourceLoader.GetString2("ErrorTimeSeparationForecast")) == 0 &&
                    operation == ForecastOperationFormOperation.Replace))
                {
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                DialogResult = DialogResult.None;
                MessageForm.Show(ex.Message, ex);
            }
        }

        private void Save_Help()
        {

            if (operation == ForecastOperationFormOperation.Update)
            {
                if (data.Start < DateTime.Now)
                {
                    throw new FaultException(ResourceLoader.GetString2("ErrorMessageModifyForecastActive"));
                }
            }
            data.Name = textEditName.Text;
            data.Description = memoEditDescription.Text;
            
            data.Start = new DateTime(dateEditFrom.DateTime.Year,
                                        dateEditFrom.DateTime.Month,
                                        dateEditFrom.DateTime.Day,
                                        dateEditFrom.DateTime.Hour,
                                        dateEditFrom.DateTime.Minute,
                                        2);
            
            data.End = new DateTime(dateEditTo.DateTime.Year,
                                        dateEditTo.DateTime.Month,
                                        dateEditTo.DateTime.Day,
                                        dateEditTo.DateTime.Hour,
                                        dateEditTo.DateTime.Minute,
                                        0);
            if (data.Start.AddMinutes(this.forecastSeparationTime) > data.End.AddSeconds(5))
                throw new FaultException(ResourceLoader.GetString2("ErrorTimeSeparationForecast", new string[]{this.forecastSeparationTime+""}));
            
            if (operation != ForecastOperationFormOperation.Restore && data.Start < DateTime.Now)
                throw new FaultException(ResourceLoader.GetString2("ErrorMessageDateTimeTOBad"));
            
                data.ForecastValues = new ArrayList();
                List<IndicatorForecastDetailGridData> list = (List<IndicatorForecastDetailGridData>)gridControlForecastDetails.DataSource;
                foreach (IndicatorForecastDetailGridData item in list)
                {
                    if (comboBoxEditDepartment.SelectedIndex > 0)
                    {
                        item.Data.DepartmentType = (DepartmentTypeClientData)comboBoxEditDepartment.SelectedItem;
                    }
                    if (data.Code != 0)
                        item.Data.IndicatorGroupForecastCode = data.Code;
                    data.ForecastValues.Add(item.Data);
                }
            

            if (operation == ForecastOperationFormOperation.Replace || operation == ForecastOperationFormOperation.Restore)
            {
                replace.End = new DateTime(dateEditFrom.DateTime.Year,
                                        dateEditFrom.DateTime.Month,
                                        dateEditFrom.DateTime.Day,
                                        dateEditFrom.DateTime.Hour,
                                        dateEditFrom.DateTime.Minute,
                                        0);
                ArrayList saveList = new ArrayList();
                saveList.Add(replace);
                saveList.Add(data);
                ServerServiceClient.GetInstance().SaveOrUpdateClientData(saveList);
            }
            else
            {
                ServerServiceClient.GetInstance().SaveOrUpdateClientData(data);
            }
            
        }

        private void simpleButtonApply_Click(object sender, EventArgs e)
        {
            if (simpleButtonApply.Enabled == true)
            {
                buttonOkPressed = true;
                Save();
                if (((SimpleButton)sender).Name == simpleButtonApply.Name)
                    simpleButtonApply.Enabled = false;
            }
        }

        private void comboBoxEditDepartment_EditValueChanged(object sender, EventArgs e)
        {
            string all = ResourceLoader.GetString2("All");
            if (this.comboBoxEditDepartment.SelectedItem is DepartmentTypeClientData)
            {
                this.gridViewForecastDetail.CollapseGroupRow(indexCalls);
                this.gridViewForecastDetail.CollapseGroupRow(indexFirstLevel);
                isCollapseCallsFirtsLevel = true;
            }
            else
            {
                this.gridViewForecastDetail.ExpandAllGroups();
                isCollapseCallsFirtsLevel = false;
            }
        }

        private void gridViewForecastDetail_GroupRowExpanding(object sender, DevExpress.XtraGrid.Views.Base.RowAllowEventArgs e)
        {
            if(e.RowHandle == indexCalls || e.RowHandle == indexFirstLevel)
            {
                e.Allow = !isCollapseCallsFirtsLevel;
            }
            else
                e.Allow = true;
        }

        private void simpleButtonCancel_Click(object sender, EventArgs e)
        {

        }

        private void ForecastOperationForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            parent.SupervisionCommittedChanges -= new EventHandler<CommittedObjectDataCollectionEventArgs>(parent_SupervisionCommittedChanges);
        }

        private void gridViewForecastDetail_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            
            textEditName_EditValueChanged(sender, e);
        }
    }

    public enum ForecastOperationFormOperation
    { 
        Create,
        Update,
        Duplicate,
        Replace,
        Restore
    }
}