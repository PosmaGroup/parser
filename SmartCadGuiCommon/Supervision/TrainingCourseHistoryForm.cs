using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using Smartmatic.SmartCad.Service;
using SmartCadCore.ClientData;

using SmartCadCore.Core;
using SmartCadCore.Enums;
using SmartCadGuiCommon.Filters;
using SmartCadGuiCommon.SyncBoxes;



namespace SmartCadGuiCommon
{
    public partial class TrainingCourseHistoryForm : Form
    {
        private TrainingCourseScheduleFilter trainingCourseScheduleFilter;

        public TrainingCourseHistoryForm()
        {
            InitializeComponent();
            InitializeDataGrid();
        }

        public TrainingCourseHistoryForm(TrainingCourseClientData selectedCourse)
            :this()
        {
            SelectedCourse = selectedCourse;
        }


        private TrainingCourseClientData selectedCourse;
        public TrainingCourseClientData SelectedCourse
        {
            get
            {
                return selectedCourse;
            }
            set
            {
                selectedCourse = value;

                if (selectedCourse != null) {

                    LoadHistorySchedules();
                }
            }
        }

        private void InitializeDataGrid() 
        {
            dataGridExCourses.Type = typeof(GridTrainingCourseScheduleData);
            //TODOF
            //GridTrainingCourseScheduleData.DataGrid = dataGridExCourses;
            dataGridExCourses.AllowDrop = false;

            dataGridExCourses.Columns["Name"].Width = 135;
            dataGridExCourses.Columns["Start"].Width = 80;
            dataGridExCourses.Columns["End"].Width = 80;
            dataGridExCourses.Columns["Trainer"].Width = 135;
            

        }

        private void LoadHistorySchedules() 
        {
            string DateNHibernateFormat = "yyyyMMdd HH:mm";
            string date = ServerServiceClient.GetInstance().GetTimeFromDB().ToString(DateNHibernateFormat);

            IList schedules = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetHistorySchedulesByCourseCode, selectedCourse.Code.ToString(), date));

            foreach (TrainingCourseScheduleClientData sc in schedules)
            {
                sc.TrainingCourse = selectedCourse;
                GridTrainingCourseScheduleData gridData = new GridTrainingCourseScheduleData(sc);
                //TODOF
                //dataGridExCourses.AddData(gridData);
            }
        }

        private void dataGridExCourses_DoubleClick(object sender, EventArgs e)
        {
            if (dataGridExCourses.SelectedItemsCount > 0)
            {
                TrainingCourseEvaluationForm form = new TrainingCourseEvaluationForm(dataGridExCourses.SelectedItems[0].Tag as TrainingCourseScheduleClientData, FormBehavior.View);

                form.ShowDialog();

            }            
        }

        private void toolStripButtonEval_Click(object sender, EventArgs e)
        {
            if (dataGridExCourses.SelectedItemsCount > 0) 
            {
                TrainingCourseEvaluationForm form = new TrainingCourseEvaluationForm(dataGridExCourses.SelectedItems[0].Tag as TrainingCourseScheduleClientData, FormBehavior.View);

                form.ShowDialog();

            }
        }

        private void toolStripButtonSearch_Click(object sender, EventArgs e)
        {
            trainingCourseScheduleFilter = new TrainingCourseScheduleFilter(dataGridExCourses, new ArrayList());
            if (string.IsNullOrEmpty(this.toolStripTextBoxSearch.Text.Trim()) == false)
            {
                trainingCourseScheduleFilter.FilterByText(this.toolStripTextBoxSearch.Text.Trim());
            }
            else
                trainingCourseScheduleFilter.Filter(false);
        }

        private void dataGridExCourses_SelectionWillChange(object sender, EventArgs e)
        {
            if (dataGridExCourses.SelectedItemsCount > 0)
                toolStripButtonEval.Enabled = true;
            else
                toolStripButtonEval.Enabled = false;
        }    
 
    }
}