using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;
using Smartmatic.SmartCad.Service;
using System.Threading;
using System.ServiceModel;
using System.Collections;
using System.Xml.Xsl;
using System.IO;
using System.Xml;
using DevExpress.XtraBars;
using System.Reflection;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using SmartCadCore.ClientData;
using SmartCadCore.Core;
using SmartCadCore.Common;
using SmartCadGuiCommon.Util;
using SmartCadCore.Enums;

using SmartCadGuiCommon;
using SmartCadGuiCommon.Controls;

namespace SmartCadGuiCommon
{
    public partial class SupervisionForm : XtraForm
    {        
        #region Fields

        private DefaultSupervisionForm defaultSupervisionForm;
        private IncidentNotificationDistributionForm notificationDistributionForm;
        private TrainingForm trainingForm;
        private ExtraTimeForm extraTimeForm;
        private ExtraTimeForm freeTimeForm;
        private PersonalFileForm personalFileForm;
        private ConsultExtraTimeVacationsForms consultExtraTimeform;
        private ConsultExtraTimeVacationsForms consultVacationform;
  		public OperatorChatForm operatorChatForm;
        public List<RemoteControlForm> remoteControlFormList;
        private const int SINGLE_SELECTION = 1;
        private SkinEngine skinEngine;
        private const float FONT_SIZE = 9.75f;
        private System.Threading.Timer globalTimer;
        private DateTime syncServerDateTime;
        private TimeSpan sinceLoggedIn;
        private int GLOBAL_TIMER_PERIOD;
        private TimeSpan ADD_TIMESPAN;
        internal Dictionary<string, ApplicationPreferenceClientData> globalApplicationPreferences;
        private bool isFirstLevelSupervisor;
        private bool isDispatchSupervisor;
        private DepartmentTypeClientData selectedDepartment;
        private string appName;
        private IndicatorDashboardPreference indicatorDashboardPreferenceForm;
        private SupervisorClosedReportForm supervisorClosedReportForm;
        private SupervisorCloseReportReaderForm supervisorClosedReportReaderForm;
        private ForescastForm forecastForm;
        private IndicatorsForm indicatorsForm;
        private const int INDEX_GROUP_BUTTON_ITEM = 2;
        private OperatorAssignForm operatorAssignForm;
        private IList globalIndicatorClassDashboards;
        private IList globalIndicators;
        private bool isGeneralSupervisor;
        private string labelUserCaption = "";
        private string labelConnectedCaption = "";

        #region Synchronization objects


        private object syncCommited = new object();

        #endregion
        #endregion

        public IList GlobalIndicatorClassDashboards
        {
            get
            {
                return globalIndicatorClassDashboards;
            }
            set
            {
                globalIndicatorClassDashboards = value;
            }
        }

        public IList GlobalIndicators
        {
            get
            {
                return globalIndicators;
            }
            set
            {
                globalIndicators = value;
            }
        }

        public DepartmentTypeClientData SelectedDepartment
        {
            get { return selectedDepartment; }
            set
            {
                selectedDepartment = value;
                if (selectedDepartment == null)
                {
                    SupervisedApplicationName = UserApplicationClientData.FirstLevel.Name;
                    ApplicationUtil.SupervisionApplicationName = UserApplicationClientData.FirstLevel.Name;
                    if (defaultSupervisionForm != null)
                    {
                        defaultSupervisionForm.SelectedDepartmentType = null;
                    }
                }
                else
                {
                    SupervisedApplicationName = UserApplicationClientData.Dispatch.Name;
                    ApplicationUtil.SupervisionApplicationName = UserApplicationClientData.Dispatch.Name;
                    if (defaultSupervisionForm != null)
                    {
                        defaultSupervisionForm.SelectedDepartmentType = selectedDepartment;
                    }
                }                
            }
        }

        public event EventHandler<CommittedObjectDataCollectionEventArgs> SupervisionCommittedChanges;
        
        public SupervisionForm(bool general, bool firstLevel, bool dispatch, IList classDashboardList, IList indicatorList)
        {
            var applications = ServerServiceClient.GetInstance().GetActiveApplications();

            InitializeComponent();
            if (!applications.Contains(SmartCadApplications.SmartCadFirstLevel) && !applications.Contains(SmartCadApplications.SmartCadDispatch)) 
            {
                ribbonPageGroupAccessProfile.Visible = false;
            }
            if (!applications.Contains(SmartCadApplications.SmartCadFirstLevel)) 
            {
                barCheckItemFirstLevel.Visibility = BarItemVisibility.Never;
            }
            if (!applications.Contains(SmartCadApplications.SmartCadDispatch)) 
            {
                barButtonItemDispatch.Visibility = BarItemVisibility.Never;
            }
            

            this.labelLogo.BackColor = Color.FromArgb(191, 219, 255);
            GlobalIndicatorClassDashboards = classDashboardList;
            GlobalIndicators = indicatorList;
            this.RibbonControl.MinimizedChanged += new EventHandler(RibbonControl_MinimizedChanged);

            LoadLanguage();
            ArrayList deparmentList = new ArrayList();
            SetSkin();

            SupervisedApplicationName = UserApplicationClientData.FirstLevel.Name;
            
            if (dispatch)
            {
                deparmentList.AddRange(ServerServiceClient.GetInstance().SearchClientObjectsMaxRows(
                    SmartCadHqls.GetCustomHql(SmartCadHqls.GetDepartmentsTypeByOperatorCode,
                    ServerServiceClient.GetInstance().OperatorClient.Code), true));

                DepartmentTypeClientData defaultDeparment = deparmentList[0] as DepartmentTypeClientData;
                BarButtonItem selectedItem = null;
                
                foreach (DepartmentTypeClientData dept in deparmentList)
                {
                    BarButtonItem item = new BarButtonItem(this.RibbonControl.Manager, dept.Name);
                    item.Tag = dept;
                    item.ItemClick += new ItemClickEventHandler(item_ItemClick);
                    item.ButtonStyle = BarButtonStyle.Check;
                    item.GroupIndex = INDEX_GROUP_BUTTON_ITEM;
                    popupMenuDispatch.AddItem(item);
                    if (dept.Code == defaultDeparment.Code)
                    {
                        selectedItem = item;
                    }
                }
                
                if (!firstLevel)
                {
                    SelectedDepartment = defaultDeparment;
                    selectedItem.Visibility = BarItemVisibility.Never;
                    barButtonItemDispatch.Down = true;
                    popupMenuDispatch.HidePopup();                    
                    selectedItem.PerformClick();
                    selectedItem.Visibility = BarItemVisibility.Always;
                }
            }

            isFirstLevelSupervisor = firstLevel;
            isDispatchSupervisor = dispatch;
			isGeneralSupervisor = general;

            if (this.IsGeneralSupervisor == false)
            {
                this.barButtonItemCreateExtraTime.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                this.barButtonItemDeleteExtraTime.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                this.barButtonItemModifyExtraTime.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                this.barButtonItemPersonalAssignExtraTime.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                this.barButtonItemCreateVacations.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                this.barButtonItemDeleteVacations.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                this.barButtonItemModifyVacations.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                this.barButtonItemPersonalAssignVacations.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            }
            else
            {
                this.barButtonItemCreateExtraTime.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
                this.barButtonItemDeleteExtraTime.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
                this.barButtonItemModifyExtraTime.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
                this.barButtonItemPersonalAssignExtraTime.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
                this.barButtonItemCreateVacations.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
                this.barButtonItemDeleteVacations.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
                this.barButtonItemModifyVacations.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
                this.barButtonItemPersonalAssignVacations.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
            }

            defaultSupervisionForm = new DefaultSupervisionForm(skinEngine, SupervisedApplicationName, SelectedDepartment, firstLevel, dispatch, isGeneralSupervisor);
            defaultSupervisionForm.MdiParent = this;
            defaultSupervisionForm.GlobalIndicatorClassDashboards = GlobalIndicatorClassDashboards;
            defaultSupervisionForm.GlobalIndicators = GlobalIndicators;            
            xtraTabbedMdiManager.Pages[defaultSupervisionForm].ShowCloseButton = DevExpress.Utils.DefaultBoolean.False;
            LoadGlobalApplicationPreferences();
            EnableButtonIncidentAssignment();
            #region ReadOnly values from ApplicationPreferences
            GLOBAL_TIMER_PERIOD = ApplicationGuiUtil.GLOBAL_TIME_PERIOD;
            ADD_TIMESPAN = new TimeSpan(0, 0, 0, 0, GLOBAL_TIMER_PERIOD);
            #endregion
            ServerServiceClient.GetInstance().CommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(SupervisionForm_CommittedChanges);
        }

        void RibbonControl_MinimizedChanged(object sender, EventArgs e)
        {
            if (this.RibbonControl.Minimized == true)
            {
                this.RibbonControl.Minimized = false;
            }
        }

        private void LoadLanguage()
        {
            this.Text = ResourceLoader.GetString2("SupervisionForm");
            this.labelLogo.Image = ResourceLoader.GetImage("$Image.AdministrationLogo");
            this.Icon = ResourceLoader.GetIcon("SupervisionIcon");
            this.barButtonItemRemoteControl.Caption = ResourceLoader.GetString2("Monitoring");
            #region Monitoring
            RibbonPageMonitoring.Text = ResourceLoader.GetString("Monitoring");
            ribbonPageGroupPerformance.Text = ResourceLoader.GetString("Performance");
            commandBarItemIndicators.Caption = ResourceLoader.GetString2("Indicators");
            barButtonItemConfigureIndicators.Caption = ResourceLoader.GetString2("Preferences");
            barButtonItemOperAffectIndicators.Caption = ResourceLoader.GetString2("OperatorsAffectIndicators");
            barButtonItemForecast.Caption = ResourceLoader.GetString2("Forecast");
            barButtonItemAddForecast.Caption = ResourceLoader.GetString2("Create");
            barButtonItemDeleteForecast.Caption = ResourceLoader.GetString2("$Message.Delete");
            barButtonItemUpdateForecast.Caption = ResourceLoader.GetString2("Modify");
            barButtonItemDuplicate.Caption = ResourceLoader.GetString2("Duplicate");
            barButtonItemReplace.Caption = ResourceLoader.GetString2("Replace");
            barButtonItemFinalize.Caption = ResourceLoader.GetString2("Finalize");
            BarButtonItemHistoricGraphic.Caption = ResourceLoader.GetString2("HistoricGraphics");
            BarButtonItemHistoryDetails.Caption = ResourceLoader.GetString2("HistoricDetails");
            barCheckItemViewSelected.Caption = ResourceLoader.GetString("ViewSelected");
            barCheckItemViewAll.Caption = ResourceLoader.GetString("ViewAll");
            ribbonPageGroupOperator.Text = ResourceLoader.GetString2("OperatorsActivities");
            barButtonItemOperatorActivities.Caption = ResourceLoader.GetString2("SeeActivities");

            BarButtonItemSelectRoom.Caption = ResourceLoader.GetString2("ViewRoom");
            barEditItemSelectRoom.Caption = ResourceLoader.GetString2("$Message.Nombre") + ":";
            ribbonPageGroupGeneralOptMonitor.Text = ResourceLoader.GetString2("GeneralOptions");
            this.barButtonItemOperatorPerformance.Caption = ResourceLoader.GetString2("ViewPerformance");
            this.barSubItemChartControl.Caption = ResourceLoader.GetString2("ConfigureGraphic");
            
            this.barCheckItemDownTreshold.Caption = ResourceLoader.GetString("DownTreshold");
            this.barCheckItemUpTreshold.Caption = ResourceLoader.GetString("UpTreshold");
            this.barCheckItemTrend.Caption = ResourceLoader.GetString("Trend");
            
            this.categoraToolStripMenuItem1.Text = ResourceLoader.GetString2("Category");

			this.barButtonItemChat.Caption = ResourceLoader.GetString2("Chat");

            this.barButtonItemReConnect.Caption = ResourceLoader.GetString2("RemoteControlReconnect");
            this.barCheckItemScaledView.Caption = ResourceLoader.GetString2("RemoteControlScaleView");
            this.barCheckItemTakeControl.Caption = ResourceLoader.GetString2("RmoteControlTakeControl");
			
            #endregion

            #region Operation
            RibbonPageOperation.Text = ResourceLoader.GetString2("Operation");

            ribbonPageGroupDistribution.Text = ResourceLoader.GetString2("Distribution");
            barItemNotificationDistribution.Caption = ResourceLoader.GetString2("NotificationDistribution");

            ribbonPageGroupAssignment.Text = ResourceLoader.GetString2("AssignmentByWorkshift");
            barButtonItemOperatorAssign.Caption = ResourceLoader.GetString2("Assignment");

            this.barButtonItemAssign.Caption = ResourceLoader.GetString2("Assign");
            this.barButtonItemRemove.Caption = ResourceLoader.GetString2("Delete");
            this.barButtonItemOpen.Caption = ResourceLoader.GetString2("Details");

            barButtonItemExtraTime.Caption = ResourceLoader.GetString2("WorkShifts");
            barButtonItemCreateExtraTime.Caption = ResourceLoader.GetString2("$Message.Crear");
            barButtonItemDeleteExtraTime.Caption = ResourceLoader.GetString2("$Message.Delete");
            barButtonItemModifyExtraTime.Caption = ResourceLoader.GetString2("Modify");
            ribbonPageGroupExtraTime.Text = ResourceLoader.GetString2("WorkShifts");
            barButtonItemConsultExtraTime.Caption = ResourceLoader.GetString2("View");
            barButtonItemVacationsAndAbsences.Caption = ResourceLoader.GetString2("VacationsAndPermissions");
            barButtonItemCreateVacations.Caption = ResourceLoader.GetString2("$Message.Crear");
            barButtonItemDeleteVacations.Caption = ResourceLoader.GetString2("$Message.Delete");
            barButtonItemModifyVacations.Caption = ResourceLoader.GetString2("Modify");
            ribbonPageGroupVacationAndAbsence.Text = ResourceLoader.GetString2("VacationsAndPermissions");
            barButtonItemConsultVacations.Caption = ResourceLoader.GetString2("View");

            barButtonItemPersonalAssignExtraTime.Caption = ResourceLoader.GetString2("Personal");
            barButtonItemPersonalAssignVacations.Caption = ResourceLoader.GetString2("Personal");

            ribbonPageGroupPersonalFile.Text = ResourceLoader.GetString2("PersonalRecord");
            printPreviewBarItemPersonalfile.Caption = ResourceLoader.GetString2("SeePersonalRecord");
            barButtonItemEvaluateOperator.Caption = ResourceLoader.GetString2("EvaluateOperator");
            printPreviewBarItemModifyCategory.Caption = ResourceLoader.GetString2("ModifyCategory");
            printPreviewBarItemPersonalFileObs.Caption = ResourceLoader.GetString2("AddObservation");

            ribbonPageGroupTraining.Text = ResourceLoader.GetString2("Training");
            barButtonItemCourses.Caption = ResourceLoader.GetString2("Courses");
            barItemCreateCourses.Caption = ResourceLoader.GetString2("$Message.Crear");
            barItemDeleteCourses.Caption = ResourceLoader.GetString2("$Message.Delete");
            barButtonItemModifyCourses.Caption = ResourceLoader.GetString2("Modify");
            barButtonTrainingCoursesSchedule.Caption = ResourceLoader.GetString2("Schedules");
            barButtonOperatorTrainingAssign.Caption = ResourceLoader.GetString2("Partaker");

            ribbonPageGroupFinalReport.Text = ResourceLoader.GetString2("CloseReport");
            barButtonItemCloseReportsReader.Caption = ResourceLoader.GetString2("VisualizeReports");
            barButtonItemReport.Caption = ResourceLoader.GetString2("Report");
            barButtonItemEndReport.Caption = ResourceLoader.GetString2("Finalize");
            barButtonItemRedo.Caption = ResourceLoader.GetString2("Redo");
            barButtonItemUndo.Caption = ResourceLoader.GetString2("Undo");
            barButtonItemCut.Caption = ResourceLoader.GetString2("Cut");
            barButtonItemCopy.Caption = ResourceLoader.GetString2("Copy");
            barButtonItemPaste.Caption = ResourceLoader.GetString2("Paste");
            ribbonPageGroupOptions.Text = ResourceLoader.GetString2("GeneralOptions");
            #endregion

            #region Tools
            RibbonPageTools.Text = ResourceLoader.GetString2("Tools");

            ribbonPageGroupPreferences.Text = ResourceLoader.GetString2("Preferences");
            barButtonItemPreferences.Caption = ResourceLoader.GetString2("IndicatorPanel");

            ribbonPageGroupAccessProfile.Text = ResourceLoader.GetString2("AccessProfile");
            barCheckItemFirstLevel.Caption = ResourceLoader.GetString2("FirstLevel");
            barButtonItemDispatch.Caption = ResourceLoader.GetString2("Dispatch");

            barMdiChildrenListItemSelect.Caption = ResourceLoader.GetString2("Selecting");
            #endregion

            #region Footer
            labelUserCaption = ResourceLoader.GetString2("User") + ": ";
            labelConnectedCaption = ResourceLoader.GetString2("Connected") + ": ";
            #endregion

            #region GeneralOptions
            this.barButtonItemPrint.Caption = ResourceLoader.GetString2("Print");
            this.barButtonItemSave.Caption = ResourceLoader.GetString2("Save");
            this.barButtonItemSend.Caption = ResourceLoader.GetString2("Send");
            this.barButtonItemUpdate.Caption = ResourceLoader.GetString2("Update");
            this.BarButtonItemSendMonitor.Caption = ResourceLoader.GetString2("Send");
            this.barButtonItemRefreshGraphic.Caption = ResourceLoader.GetString2("Update");

            #endregion
            #region Ribbon bar ToolTips
            this.commandBarItemIndicators.SuperTip = BuildToolTip("ToolTip_ViewIndicators");
            this.barButtonItemOperAffectIndicators.SuperTip = BuildToolTip("ToolTip_KeyIndicatorDetails");
            this.barButtonItemForecast.SuperTip = BuildToolTip("ToolTip_ViewForecast");
            this.barButtonItemAddForecast.SuperTip = BuildToolTip("ToolTip_CreateForecast");
            this.barButtonItemDeleteForecast.SuperTip = BuildToolTip("ToolTip_DeleteForecast");
            this.barButtonItemUpdateForecast.SuperTip = BuildToolTip("ToolTip_ModifyForecast");
            this.barButtonItemDuplicate.SuperTip = BuildToolTip("ToolTip_DuplicateForecast");
            this.barButtonItemReplace.SuperTip = BuildToolTip("ToolTip_ReplaceForecast");
            this.barButtonItemFinalize.SuperTip = BuildToolTip("ToolTip_FinalizeForecast");
            this.barButtonItemOperatorActivities.SuperTip = BuildToolTip("ToolTip_ViewOperatorActivities");
            this.barButtonItemOperatorPerformance.SuperTip = BuildToolTip("ToolTip_ViewOperatorPerformance");
            this.BarButtonItemSelectRoom.SuperTip = BuildToolTip("ToolTip_ViewRoom");
            this.barCheckItemViewSelected.SuperTip = BuildToolTip("ToolTip_ViewSelectedOperator");
            this.barCheckItemViewAll.SuperTip = BuildToolTip("ToolTip_AllOperators");
            this.barEditItemSelectRoom.SuperTip = BuildToolTip("ToolTip_RoomName");
            this.barButtonItemPrint.SuperTip = BuildToolTip("ToolTip_Print");
            this.barButtonItemSave.SuperTip = BuildToolTip("ToolTip_Save");
            this.BarButtonItemSendMonitor.SuperTip = BuildToolTip("ToolTip_Send");
            this.barButtonItemRefreshGraphic.SuperTip = BuildToolTip("ToolTip_Update");
            this.printPreviewBarItemPersonalfile.SuperTip = BuildToolTip("ToolTip_ViewPersonalFile");
            this.barButtonItemEvaluateOperator.SuperTip = BuildToolTip("ToolTip_EvaluateOperator");
            this.printPreviewBarItemModifyCategory.SuperTip = BuildToolTip("ToolTip_ModifyOperatorCategory");
            this.printPreviewBarItemPersonalFileObs.SuperTip = BuildToolTip("ToolTip_AddOperatorObservation");
            this.barButtonItemOperatorAssign.SuperTip = BuildToolTip("ToolTip_AssignByWorkshift");
            this.barButtonItemExtraTime.SuperTip = BuildToolTip("ToolTip_ViewWorkShift");
            this.barButtonItemCreateExtraTime.SuperTip = BuildToolTip("ToolTip_CreateWorkShift");
            this.barButtonItemDeleteExtraTime.SuperTip = BuildToolTip("ToolTip_DeleteWorkShift");
            this.barButtonItemModifyExtraTime.SuperTip = BuildToolTip("ToolTip_ModifyWorkShift");
            this.barButtonItemConsultExtraTime.SuperTip = BuildToolTip("ToolTip_RequestWorkShift");
            this.barButtonItemPersonalAssignExtraTime.SuperTip = BuildToolTip("ToolTip_PersonalWorkShift");

            this.barButtonItemVacationsAndAbsences.SuperTip = BuildToolTip("ToolTip_ViewVacationsAndAbsences");
            this.barButtonItemCreateVacations.SuperTip = BuildToolTip("ToolTip_CreateVacations");
            this.barButtonItemDeleteVacations.SuperTip = BuildToolTip("ToolTip_DeleteVacations");
            this.barButtonItemModifyVacations.SuperTip = BuildToolTip("ToolTip_ModifyVacations");
            this.barButtonItemConsultVacations.SuperTip = BuildToolTip("ToolTip_ConsultVacations");
            this.barButtonItemPersonalAssignVacations.SuperTip = BuildToolTip("ToolTip_PersonalVacations");


            this.barButtonItemCourses.SuperTip = BuildToolTip("ToolTip_ViewTrainningCourse");
            this.barItemCreateCourses.SuperTip = BuildToolTip("ToolTip_CreateTrainningCourse");
            this.barItemDeleteCourses.SuperTip = BuildToolTip("ToolTip_DeleteTrainningCourse");
            this.barButtonItemModifyCourses.SuperTip = BuildToolTip("ToolTip_ModifyTrainningCourse");
            this.barButtonTrainingCoursesSchedule.SuperTip = BuildToolTip("ToolTip_CreateScheduleToTrainningCourse");
            this.barButtonOperatorTrainingAssign.SuperTip = BuildToolTip("ToolTip_AssignPeopleToCourse");

            this.barItemNotificationDistribution.SuperTip = BuildToolTip("ToolTip_NotificationDistribution");
            this.barButtonItemCloseReportsReader.SuperTip = BuildToolTip("ToolTip_ViewClosingReports");
            this.barButtonItemReport.SuperTip = BuildToolTip("ToolTip_MakeClosingReports");
            this.barButtonItemCut.SuperTip = BuildToolTip("ToolTip_Cut");
            this.barButtonItemCopy.SuperTip = BuildToolTip("ToolTip_CopySupervision");

            this.barButtonItemPaste.SuperTip = BuildToolTip("ToolTip_Paste");
            this.barButtonItemUndo.SuperTip = BuildToolTip("ToolTip_Undo");
            this.barButtonItemRedo.SuperTip = BuildToolTip("ToolTip_Redo");
            this.barButtonItemEndReport.SuperTip = BuildToolTip("ToolTip_FinalizeClosingReport");
          
            this.barButtonItemPrint.SuperTip = BuildToolTip("ToolTip_Print");
            this.barButtonItemSave.SuperTip = BuildToolTip("ToolTip_Save");
            this.barButtonItemSend.SuperTip = BuildToolTip("ToolTip_Send");
            this.barButtonItemUpdate.SuperTip = BuildToolTip("ToolTip_Update");

            this.barButtonItemPreferences.SuperTip = BuildToolTip("ToolTip_DashboardPrefrences");
            this.ribbonPageGroupAccessProfile.SuperTip = BuildToolTip("ToolTip_AccessProfileGroup");
            this.barCheckItemFirstLevel.SuperTip = BuildToolTip("ToolTip_CheckFirstLevel");
            this.barButtonItemDispatch.SuperTip = BuildToolTip("ToolTip_CheckDispatch");
            this.barButtonItemHelp.SuperTip = BuildToolTip("ToolTip_HelpOnline");
            this.barButtonItemExit.SuperTip = BuildToolTip("ToolTip_Exit");
            

         
            #endregion

        }

        public static SuperToolTip BuildToolTip(string keyToolTip)
        {
            SuperToolTip toolTip = null;
            string data = ResourceLoader.GetString2(keyToolTip);
            string[] dataElements = ApplicationUtil.GetSplittedResult(data);
            if (dataElements.Length > 0)
            {
                toolTip = new DevExpress.Utils.SuperToolTip();
                ToolTipTitleItem titleItem = new ToolTipTitleItem();
                titleItem.Text = dataElements[0];
                toolTip.Items.Add(titleItem);
            }
            if (dataElements.Length > 1)
            {
                ToolTipItem toolTipItem = new ToolTipItem();
                toolTipItem.Text = dataElements[1];
                toolTip.Items.Add(toolTipItem);
            }
            return toolTip;
        }

        void item_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (e.Item != null)
            {
                bool applyChange = true;
                //bool applyChange = false;
                Form[] childArr = new Form[this.MdiChildren.Length];
                int i = 0;
                foreach (Form child in this.MdiChildren)
                {
                    childArr[i++] = child;
                }
                for (i = 0; i < childArr.Length; i++)
                {
                    Form child = (Form)childArr[i];
                    if (!child.Equals(defaultSupervisionForm))
                    {
                        child.Close();
                        if (child != null && child.IsDisposed == false)
                        {
                            applyChange = applyChange && false;
                        }
                    }
                    else
                    {
                        if (((DefaultSupervisionForm)child).tabbedControlGroupMonitorActivities.SelectedTabPage == ((DefaultSupervisionForm)child).layoutControlGroupRoom)
                        {
                            ((DefaultSupervisionForm)child).ShowOperatorsInRoom(true);
                        }
                    }
                }
                if (operatorChatForm != null)
                {
                    operatorChatForm.Close();
                }
                if (applyChange == true)
                {
                    barButtonItemDispatch.Down = true;
                    (e.Item as BarButtonItem).Down = true;
                    barButtonItemDispatch.Caption = e.Item.Caption;
                    barButtonItemDispatch.Tag = e.Item.Tag;
                    SelectedDepartment = e.Item.Tag as DepartmentTypeClientData;
                    ribbonPageGroupDistribution.Visible = true;
                    if ((indicatorsForm != null) && (IsGeneralSupervisor) && (indicatorsForm.IsDisposed == false))
                    {
                        indicatorsForm.SetGeneralDispatchSupervisorMode();
                    }
                }
            }        
        }

        internal bool IsGeneralSupervisor
        {
            get
            {
                return isGeneralSupervisor;
            }
        }

        internal bool IsFirstLevelSupervisor
        {
            get
            {
                return isFirstLevelSupervisor;
            }
        }

        internal bool IsDispatchSupervisor
        {
            get
            {
                return isDispatchSupervisor;
            }
        }

        internal SupervisorType SelectedSupervisorType
        {
            get
            {
                if (IsGeneralSupervisor == true)
                    return SupervisorType.General;
                else if (isFirstLevelSupervisor == true)
                    return SupervisorType.FirstLevel;
                else
                    return SupervisorType.Dispatch;
            }
        }

        private void SupervisionForm_Load(object sender, EventArgs e)
        {
         
            this.Location = new Point(0, 0);
            this.Width = Screen.PrimaryScreen.WorkingArea.Width;
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
            CustomizeForm();  
            this.labelUser.Caption = labelUserCaption + ServerServiceClient.GetInstance().OperatorClient.FirstName
                + " " + ServerServiceClient.GetInstance().OperatorClient.LastName;

            this.labelDate.Caption = ApplicationUtil.GetFormattedDate(ServerServiceClient.GetInstance().GetTime());

            syncServerDateTime = ServerServiceClient.GetInstance().GetTimeFromDB();
            sinceLoggedIn = TimeSpan.Zero;
            this.labelConnected.Caption = labelConnectedCaption + "00:00";

            globalTimer = ApplicationUtil.RecurrentExecuteMethod(
                new SimpleDelegate(
                delegate
                {
                    timer_Tick(null, null);
                }), null, null, GLOBAL_TIMER_PERIOD, GLOBAL_TIMER_PERIOD);
            defaultSupervisionForm.Show();
			RibbonControl.SelectedPage = RibbonPageMonitoring;
            this.barButtonItemOperAffectIndicators.ItemClick += new ItemClickEventHandler(barButtonItemOperAffectIndicators_ItemClick);
            ApplicationUtil.SupervisionApplicationName = SupervisedApplicationName;
        }

        private string indicatorOperatorFormCustomCode;
        private string indicatorOperatorFormName;
        IndicatorOperatorForm indicatorOperatorForm = null;
        private void indicatorsForm_IndicatorOperatorFormChangedEvent(object sender, 
            IndicatorOperatorFormChangeEventArgs e)
        {
            if (e.Show == true)
            {
                this.barButtonItemOperAffectIndicators.Enabled = true;
                indicatorOperatorFormCustomCode = e.CustomCde;
                indicatorOperatorFormName = e.Name;
            }
            else
            {
                this.barButtonItemOperAffectIndicators.Enabled = false;
                indicatorOperatorFormCustomCode = string.Empty;
                indicatorOperatorFormName = string.Empty;
            
            }
        }

        private void barButtonItemOperAffectIndicators_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (indicatorOperatorForm == null || indicatorOperatorForm.IsDisposed == true)
            {
                indicatorOperatorForm = new IndicatorOperatorForm();
                indicatorOperatorForm.IndicatorCode = indicatorOperatorFormCustomCode;
                indicatorOperatorForm.IndicatorName = indicatorOperatorFormName;
                indicatorOperatorForm.ShowDialog();
                indicatorOperatorForm = null;
            }           
        }

        private void LoadGlobalApplicationPreferences()
        {
            try
            {
                globalApplicationPreferences = new Dictionary<string, ApplicationPreferenceClientData>();
                foreach (ApplicationPreferenceClientData applicationPreference in 
                    ServerServiceClient.GetInstance().SearchClientObjectsMaxRows(
                    SmartCadHqls.GetAllApplicationPreferences, true))
                {
                    this.globalApplicationPreferences.Add(applicationPreference.Name, applicationPreference);
                }
            }
            catch (Exception ex)
            {
                MessageForm.Show(ex.Message, ex);
            }
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            syncServerDateTime = syncServerDateTime.AddMilliseconds((double)GLOBAL_TIMER_PERIOD);
            sinceLoggedIn = sinceLoggedIn.Add(ADD_TIMESPAN);
			FormUtil.InvokeRequired(ribbonStatusBar1, delegate
            {
                this.labelDate.Caption = ApplicationUtil.GetFormattedDate(ServerServiceClient.GetInstance().GetTime());
                this.labelConnected.Caption = labelConnectedCaption + Math.Truncate(sinceLoggedIn.TotalHours).ToString().PadLeft(2, '0') + ":" + sinceLoggedIn.Minutes.ToString().PadLeft(2, '0');
                ribbonStatusBar1.Refresh();
            });
        }

        public string SupervisedApplicationName
        {
            set
            {
                appName = value;
                ServerServiceClient.GetInstance().SetSupervisedApplicationName(appName);
                if (defaultSupervisionForm != null)
                    defaultSupervisionForm.SupervisedApplicationName = appName;
                appName = value;
            }
            get
            {                
                return appName;
            }
        }

        /// <summary>
        /// This method enable/disable menus, buttons, etc according to user profile when the form is loading.
        /// </summary>
        private void CustomizeForm()
        {
            ribbonPageGroupAccessProfile.Visible = isDispatchSupervisor;
            if (!isFirstLevelSupervisor)
            {
                barCheckItemFirstLevel.Enabled = false;
                barCheckItemFirstLevel.Visibility = BarItemVisibility.Never;
            }
            ribbonPageGroupPreferences.Visible = IsGeneralSupervisor;
            ribbonPageGroupAssignment.Visible = IsGeneralSupervisor;

        //    barButtonItemReport.Enabled = IsGeneralSupervisor;
            barButtonItemPreferences.Enabled = IsGeneralSupervisor;
            RibbonPageTools.Visible = IsGeneralSupervisor || 
                (isDispatchSupervisor &&
                ServerServiceClient.GetInstance().OperatorClient.DepartmentTypes.Count > 1);
            BarItemVisibility visibility = IsGeneralSupervisor ? BarItemVisibility.Always : BarItemVisibility.Never;
            barButtonItemForecast.Visibility = visibility;
            barButtonItemAddForecast.Visibility = visibility;
            barButtonItemDeleteForecast.Visibility = visibility;
            barButtonItemUpdateForecast.Visibility = visibility;
            barButtonItemDuplicate.Visibility = visibility;
            barButtonItemReplace.Visibility = visibility;
            barButtonItemFinalize.Visibility = visibility;
        }


        private void SetSkin()
        {
            try
            {
                string skinFile = SmartCadConfiguration.SkinsFolder + "/Clasic.xml";

                skinEngine = SkinEngine.Load(skinFile);

                ProfessionalColorTable colorTable = new CustomToolStripColors(
                    skinEngine.Colors["ToolStripGradientBegin"],
                    skinEngine.Colors["ToolStripGradientMiddle"],
                    skinEngine.Colors["ToolStripGradientEnd"],

                    skinEngine.Colors["MenuStripGradientBegin"],
                    skinEngine.Colors["MenuStripGradientEnd"],

                    skinEngine.Colors["StatusStripGradientBegin"],
                    skinEngine.Colors["StatusStripGradientEnd"]
                );
                
                skinEngine.ApplyChanges();
            }
            catch (Exception ex)
            {
                MessageForm.Show(ResourceLoader.GetString2("$Message.Error"), ResourceLoader.GetString2("ErrorLoadingSkin"), ex);
            }
        }

        private void toolStripButtonExp_Click(object sender, EventArgs e)
        {
            if (defaultSupervisionForm.SelectedOperator != null && (personalFileForm == null || personalFileForm.IsDisposed == true))
            {
                int code = ServerServiceClient.GetInstance().OperatorClient.Code;
                personalFileForm = new PersonalFileForm(code, defaultSupervisionForm.SelectedOperator, SupervisedApplicationName);
            }
            if (personalFileForm != null && personalFileForm.IsDisposed == false)
            {
                personalFileForm.MdiParent = this;
                personalFileForm.Activate();
				if (selectedDepartment == null)
					personalFileForm.FirstLevel = true;
				else
					personalFileForm.FirstLevel = false;
                personalFileForm.Show();
            }
        }

        void SupervisionForm_CommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
        {
            try
            {
                if (e.Objects != null && e.Objects.Count > 0)
                {
                    lock (syncCommited)
                    {
                        if (e.Objects[0] is ApplicationPreferenceClientData)
                        {
                            ApplicationPreferenceClientData preference = e.Objects[0] as ApplicationPreferenceClientData;
                            if (e.Action == CommittedDataAction.Update)
                            {
                                if (globalApplicationPreferences.ContainsKey(preference.Name))
                                {
                                    globalApplicationPreferences[preference.Name] = preference;
                                }
                                else
                                {
                                    globalApplicationPreferences.Add(preference.Name, preference);
                                }
                                EnableButtonIncidentAssignment();
                            }
                        }
                        else if (e.Objects[0] is DepartmentTypeClientData)
                        {
                            if (e.Action == CommittedDataAction.Update)
                            {
                                DepartmentTypeClientData dep = e.Objects[0] as DepartmentTypeClientData;
                                if (selectedDepartment != null && selectedDepartment.Code == dep.Code)
                                {
                                    selectedDepartment = dep;
                                    FormUtil.InvokeRequired(defaultSupervisionForm,
                                    delegate
                                    {
                                        if (defaultSupervisionForm != null)
                                        {
                                            defaultSupervisionForm.SelectedDepartmentType = selectedDepartment;
                                        }
                                    });
                                }

                                FormUtil.InvokeRequired(defaultSupervisionForm,
                                delegate
                                {
                                    foreach (BarButtonItemLink itemLink in popupMenuDispatch.ItemLinks)
                                    {
                                        BarButtonItem item = itemLink.Item;
                                        if (item != null && item.Tag.Equals(dep))
                                        {
                                            item.Caption = dep.Name;
                                            item.Tag = dep;
                                            if ((selectedDepartment != null && selectedDepartment.Code == dep.Code) ||
                                                (barButtonItemDispatch.Tag != null && 
                                                (barButtonItemDispatch.Tag as DepartmentTypeClientData).Equals(dep)))
                                            {
                                                barButtonItemDispatch.Caption = item.Caption;
                                            }
                                            break;
                                        }
                                    }
                                });

                            }
                        }
                        try
                        {
                            if (ServerServiceClient.GetInstance().OperatorClient == null)
                            { }
                        }
                        catch
                        { }
         
                        if (SupervisionCommittedChanges != null)
                        {
                            Delegate[] test = SupervisionCommittedChanges.GetInvocationList();
                            for(int i = test.Length - 1; i >= 0; i--)
                            {
                                try
                                {
                                    test[i].DynamicInvoke(null, e);
                                }
                                catch
                                { }
                            }
                        }                        
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        private void EnableButtonIncidentAssignment()
        {
            if (this.globalApplicationPreferences.ContainsKey("CheckLogInMode") == true)
            {
                bool check = true;
                check = Convert.ToBoolean(globalApplicationPreferences["CheckLogInMode"].Value);
                FormUtil.InvokeRequired(this,
                delegate
                {
                    barItemNotificationDistribution.Enabled = check || IsGeneralSupervisor;
                });
            }
        }

        public bool EnsuredLogout = false;

        private bool EnsureLogout()
        {
            DialogResult dialogResult = MessageForm.Show(ResourceLoader.GetString2("$Message.ExitApplication"), MessageFormType.Question);

            return dialogResult == DialogResult.Yes;
        }

        private void SupervisionForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (CheckCloseReport() == true)
            {
                ServerServiceClient.GetInstance().CommittedChanges -= new EventHandler<CommittedObjectDataCollectionEventArgs>(SupervisionForm_CommittedChanges);
                ServerServiceClient.GetInstance().CloseSession();
            }
            else
                e.Cancel = true;
        }

        private bool CheckCloseReport()
        {
            bool result = false;
            IList list = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetCurrentCloseReportBySessionWithoutMessages, ServerServiceClient.GetInstance().CurrentSession.Code));
            if (list.Count == 0)
            {
                DialogResult dr = MessageForm.Show(ResourceLoader.GetString2("ClosingSupervisorFormWithoutCloseReport"), MessageFormType.WarningQuestion);
                if (dr == DialogResult.Yes)
                    result = true;
            }
            else
            {
              
                SupervisorCloseReportClientData report = (SupervisorCloseReportClientData)list[0];
                if (report.Finished == false)
                {
                    DialogResult dr = MessageForm.Show(ResourceLoader.GetString2("ClosingSupervisorFormFinishedCloseReport"), MessageFormType.Question);
                    if (dr == DialogResult.Yes)
                    {
                        report.Finished = true;
                        report.End = this.syncServerDateTime;
                        ServerServiceClient.GetInstance().DeleteClientObject(report);
                        result = true;
                    }
                }
                else 
                {
                    if (EnsuredLogout == true)
                    {
                        report.End = this.syncServerDateTime;
                        ServerServiceClient.GetInstance().SaveOrUpdateClientData(report);
                        result = true;
                    }
                }
            }
            return result;
        }

        private void TestGridControlActions()
        {
            Random rnd = new Random();
            IndicatorResultClientData result = new IndicatorResultClientData();
            result.Code = 2;
            result.IndicatorName = "Indicador 2";
            result.IndicatorMeasureUnit = "seg";
            result.Values = new ArrayList();
            IndicatorResultValuesClientData values = new IndicatorResultValuesClientData();
            values.IndicatorClassFriendlyName = "Indicador " + result.Code;
            values.ResultValue = rnd.Next(100).ToString();
            values.Threshold = -1;
            result.Values.Add(values);

            int option = rnd.Next(11);
            if (option % 3 == 0)
            {
                //LoadIndicatorResults();
            }
            else if (option % 3 == 1)
            {
                //FormUtil.InvokeRequired(this,
                //delegate
                //{
                //    indicatorDashBoardControl.AddOrUpdateItem(result, IndicatorDashBoardControl.Category.System);
                //    indicatorDashBoardControl1.AddOrUpdateItem(result, IndicatorDashBoardControl.Category.Group);
                //    indicatorDashBoardControl2.AddOrUpdateItem(result, IndicatorDashBoardControl.Category.Operator);
                //});
            }
            else
            {
                result = new IndicatorResultClientData();
                result.IndicatorName = "Indicador 2";
                //FormUtil.InvokeRequired(this,
                //delegate
                //{
                //    indicatorDashBoardControl.DeleteResultItem(result, IndicatorDashBoardControl.Category.System);
                //    indicatorDashBoardControl1.DeleteResultItem(result, IndicatorDashBoardControl.Category.Operator);
                //    indicatorDashBoardControl2.DeleteResultItem(result, IndicatorDashBoardControl.Category.Group);
                //});
            }
        }

        private void PrintPreviewBarNotificationDistribution_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (SelectedDepartment != null)
            {
                if (notificationDistributionForm == null || notificationDistributionForm.IsDisposed == true)
                {
                    notificationDistributionForm = new IncidentNotificationDistributionForm(SelectedDepartment, IsGeneralSupervisor);
                }
                notificationDistributionForm.MdiParent = this;
                notificationDistributionForm.Activate();
                notificationDistributionForm.Show();
            }
        }

   
        private void barButtonItemExit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }



        private void barButtonItemOperatorAssign_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (operatorAssignForm == null || operatorAssignForm.IsDisposed == true)
            {
                BackgroundProcessForm processForm = new BackgroundProcessForm(this,
                  new MethodInfo[1]
                { 
                    GetType().GetMethod("ShowOperatorAssignForm", BindingFlags.NonPublic | BindingFlags.Instance) 
                },
                  new object[1][] { new object[0] { } });
                processForm.CanThrowError = true;
                processForm.ShowDialog();
            }

            operatorAssignForm.Activate();
            operatorAssignForm.Show();
        }

     
        private void ShowOperatorAssignForm()
        {
            int timeToDelete = int.Parse(globalApplicationPreferences["TimeToDeleteWorkShift"].Value);
            FormUtil.InvokeRequired(this, delegate
            {
                if (SelectedDepartment != null)
                    operatorAssignForm = new OperatorAssignForm(SupervisedApplicationName, SelectedDepartment, timeToDelete);
                else
                    operatorAssignForm = new OperatorAssignForm(SupervisedApplicationName, null, timeToDelete);

                operatorAssignForm.MdiParent = this;
            });
        }


        private void PrintPreviewBarItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (defaultSupervisionForm.SelectedOperator != null && (personalFileForm == null || personalFileForm.IsDisposed == true))
            {
                int code = ServerServiceClient.GetInstance().OperatorClient.Code;
                personalFileForm = new PersonalFileForm(code, defaultSupervisionForm.SelectedOperator, SupervisedApplicationName);
            }
			else if (defaultSupervisionForm.SelectedOperator != null &&
				(personalFileForm != null || personalFileForm.IsDisposed == false) && 
				personalFileForm.operatorCode!= defaultSupervisionForm.SelectedOperator.Code)
			{
				int code = ServerServiceClient.GetInstance().OperatorClient.Code;
				personalFileForm.Close();
				personalFileForm = new PersonalFileForm(code, defaultSupervisionForm.SelectedOperator, SupervisedApplicationName);
			}
            if (personalFileForm != null && personalFileForm.IsDisposed == false)
            {
                personalFileForm.MdiParent = this;
                personalFileForm.Activate();
				if (selectedDepartment == null)
					personalFileForm.FirstLevel = true;
				else
					personalFileForm.FirstLevel = false;
               
                personalFileForm.Show();
            }
        }

        private void BarButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            defaultSupervisionForm.Activate();
            defaultSupervisionForm.ShowOperatorActivitiesTabPage();
        }

        private void barButtonRefreshChart_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //defaultSupervisionForm.LoadSessionStatusHistoryChart();
        }

        private void barButtonItemCourses_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (trainingForm == null || trainingForm.IsDisposed == true)
            {
                if (this.SelectedDepartment != null)
                trainingForm = new TrainingForm(this.SupervisedApplicationName, this.SelectedDepartment);
                else
                    trainingForm = new TrainingForm(this.SupervisedApplicationName, null);
                
            }
            trainingForm.MdiParent = this;
            trainingForm.Activate();
            trainingForm.Show();
        }

        private void barButtonItemSendPersonalFile_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }

        private void BarButtonItemEvaluateOperator_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }

        private void PrintPreviewBarItemModifyCategory_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }

        private void PrintPreviewBarItemPersonalFileObs_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }

        private void BarButtonTrainingCoursesSchedule_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            /*if (trainingForm.SelectedCourse != null) {

                TrainingCourseScheduleForm form;
            
                if (trainingForm.SelectedCourseSchedule != null)
                    form = new TrainingCourseScheduleForm((TrainingCourseClientData)trainingForm.SelectedCourse, (TrainingCourseScheduleClientData)trainingForm.SelectedCourseSchedule);
                else
                    form = new TrainingCourseScheduleForm((TrainingCourseClientData)trainingForm.SelectedCourse, null);

                DialogResult res = form.ShowDialog();

                if (res == DialogResult.Cancel)
                {
                    TrainingCourseClientData course = new TrainingCourseClientData();
                    course.Code = ((TrainingCourseClientData)trainingForm.SelectedCourse).Code;
                    course = (TrainingCourseClientData)ServerServiceClient.GetInstance().RefreshClient(course);
                    trainingForm.trainingCourseSyncBox2.Sync(course, CommittedDataAction.Update);

                }
            }*/
        }


        private void BarButtonItemHistoricTraining_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //if (trainingForm.SelectedCourse != null)
            //{
            //    TrainingCourseHistoryForm form = new TrainingCourseHistoryForm((TrainingCourseClientData)trainingForm.SelectedCourse);
            //    form.ShowDialog();
            //}
        }

        private void PrintPreviewBarItemCreateCourses_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            TrainingCreateForm form = new TrainingCreateForm(null, FormBehavior.Create);

            form.ShowDialog();
        }

        private void BarButtonOperatorTrainingAssign_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //if (trainingForm.SelectedCourse != null)
            //{
            //    TrainingCourseAssignForm form;

            //    if (trainingForm.SelectedCourseSchedule != null)
            //        form = new TrainingCourseAssignForm((TrainingCourseClientData)trainingForm.SelectedCourse, (TrainingCourseScheduleClientData)trainingForm.SelectedCourseSchedule);
            //    else
            //        form = new TrainingCourseAssignForm((TrainingCourseClientData)trainingForm.SelectedCourse, null);

            //    form.ShowDialog();

            //}
        }

        private void barButtonItemEvaluateOperatorInCourse_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //if (trainingForm.SelectedCourse != null && trainingForm.SelectedCourseSchedule != null)
            //{
            //    TrainingCourseScheduleClientData schedule = new TrainingCourseScheduleClientData();
            //    schedule = (TrainingCourseScheduleClientData)trainingForm.SelectedCourseSchedule;

            //    if (schedule.TrainingCourseOperators.Count > 0)
            //    {
            //        TrainingCourseEvaluationForm form = new TrainingCourseEvaluationForm((TrainingCourseScheduleClientData)trainingForm.SelectedCourseSchedule, FormBehavior.Edit);

            //        form.ShowDialog();
            //    }
            //    else
            //    {
            //        MessageForm.Show(ResourceLoader.GetString2("ScheduleCanNotBeEvaluated"), MessageFormType.Information);

            //    }
            //}

        }

        private void BarButtonItemHistoryDetails_ItemClick(object sender, ItemClickEventArgs e)
        {
        }

        private void barButtonItemPreferences_ItemClick(object sender, ItemClickEventArgs e)
        {
            indicatorDashboardPreferenceForm = new IndicatorDashboardPreference(this);
            indicatorDashboardPreferenceForm.Activate();
            indicatorDashboardPreferenceForm.ShowDialog();
            indicatorDashboardPreferenceForm = null;
        }

        private void barCheckItemFirstLevel_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //TODO: HAbilitar  el cambio de aplicaciones            
            BarCheckItem checkItem = e.Item as BarCheckItem;
            if (checkItem != null && checkItem.Checked)
            {
                bool applyChange = true;
                Form[] childArr = new Form[this.MdiChildren.Length];
                int i = 0;
                foreach (Form child in this.MdiChildren)
                {
                    childArr[i++] = child;
                }
                for (i = 0; i < childArr.Length; i++)
                {
                    Form child = (Form)childArr[i];
                    if (!child.Equals(defaultSupervisionForm))
                    {
                        child.Close();
                        if (child != null && child.IsDisposed == false)
                        {
                            applyChange = applyChange && false;
                        }
                    }
                }
                if (applyChange == true)
                {
                    foreach (BarButtonItemLink itemLink in popupMenuDispatch.ItemLinks)
                    {
                        BarButtonItem item = itemLink.Item;
                        if (item != null)
                        {
                            if (item.Down)
                            {
                                item.Toggle();
                            }
                        }
                    }
                    SelectedDepartment = null;
                    ribbonPageGroupDistribution.Visible = false;
                    if ((indicatorsForm != null) && (indicatorsForm.IsDisposed == false))
                    {
                        indicatorsForm.SetGeneralFirtsLevelSupervisorMode();
                    }
                    this.barButtonItemDispatch.Caption = ResourceLoader.GetString2("Dispatch");
                }
            }
            ApplicationUtil.SupervisionApplicationName = SupervisedApplicationName;
        }

        private void barButtonItemDispatch_DownChanged(object sender, ItemClickEventArgs e)
        {
            //TODO: Para el caso de despacho, con un menu bar, aadir un popup control
            BarButtonItem buttonItem = e.Item as BarButtonItem;
            if (buttonItem != null && buttonItem.Down)
            {
                buttonItem.DropDownControl.ShowPopup(this.RibbonControl.Manager, Cursor.Position);
                ApplicationUtil.SupervisionApplicationName = SupervisedApplicationName;
            }
        }

        private void barButtonItemDispatch_ItemPress(object sender, ItemClickEventArgs e)
        {
            BarButtonItem buttonItem = e.Item as BarButtonItem;
            if (buttonItem != null && buttonItem.Down)
            {
                buttonItem.DropDownControl.ShowPopup(this.RibbonControl.Manager, Cursor.Position);                
            }
        }

        private void popupMenuDispatch_CloseUp(object sender, EventArgs e)
        {
            if (SelectedDepartment == null)
            {
                barCheckItemFirstLevel.Checked = true;
            }
        }

        private void barButtonItemReport_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (supervisorClosedReportForm == null || supervisorClosedReportForm.IsDisposed == true)
            {
                supervisorClosedReportForm = new SupervisorClosedReportForm();
            }
            supervisorClosedReportForm.MdiParent = this;
            supervisorClosedReportForm.Activate();
            supervisorClosedReportForm.Show();
        }

        private void PrintPreviewBarItemCut_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }
              

        private void barButtonItemCloseReportsReader_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (supervisorClosedReportReaderForm == null || supervisorClosedReportReaderForm.IsDisposed == true)
            {
                string depName = string.Empty;
                if (SelectedDepartment != null) depName = SelectedDepartment.Name;
                supervisorClosedReportReaderForm = new SupervisorCloseReportReaderForm(SelectedSupervisorType, SupervisedApplicationName, depName);
            }
            supervisorClosedReportReaderForm.MdiParent = this;
            supervisorClosedReportReaderForm.Activate();
            supervisorClosedReportReaderForm.Show();
        }

        private void commandBarItemIndicators_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (indicatorsForm == null || indicatorsForm.IsDisposed == true)
            {
                indicatorsForm = new IndicatorsForm();
                this.indicatorsForm.IndicatorOperatorFormChangedEvent +=
                new IndicatorOperatorFormChangeEventHandler(indicatorsForm_IndicatorOperatorFormChangedEvent);
            }
            indicatorsForm.MdiParent = this;
            indicatorsForm.Activate();
            indicatorsForm.Show();
        }

        private void barButtonItemExtraTime_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (extraTimeForm == null || extraTimeForm.IsDisposed == true)
            {
                int timeToDeleteWorkShift = int.Parse(globalApplicationPreferences["TimeToDeleteWorkShift"].Value);
                extraTimeForm = new ExtraTimeForm(IsGeneralSupervisor, false, timeToDeleteWorkShift);                
            }
            RibbonControl.ForceInitialize();
            extraTimeForm.MdiParent = this;
            extraTimeForm.Activate();
            extraTimeForm.Show();
            FormUtil.InvokeRequired(this,
                delegate
                {
                    this.ribbonPageGroupExtraTime.ItemLinks.Remove(this.barButtonItemConsultExtraTime);
                    this.ribbonPageGroupExtraTime.ItemLinks.Add(this.barButtonItemConsultExtraTime);
                });
            FormUtil.InvokeRequired(this,
                delegate
                {
                    this.ribbonPageGroupVacationAndAbsence.ItemLinks.Remove(this.barButtonItemConsultVacations);
                    this.ribbonPageGroupVacationAndAbsence.ItemLinks.Add(this.barButtonItemConsultVacations);
                });            
        }

        private void barButtonItemDeleteExtraTime_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void barButtonItemModifyExtraTime_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void barButtonItemVacationsAndAbsences_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (freeTimeForm == null || freeTimeForm.IsDisposed == true)
            {
                int timeToDeleteVacation = int.Parse(globalApplicationPreferences["TimeToDeleteVacation"].Value);
                freeTimeForm = new ExtraTimeForm(IsGeneralSupervisor, true, timeToDeleteVacation);                
            }
            RibbonControl.ForceInitialize();
            freeTimeForm.MdiParent = this;
            freeTimeForm.Activate();
            freeTimeForm.Show();
            FormUtil.InvokeRequired(this,
                delegate
                {
                    this.ribbonPageGroupVacationAndAbsence.ItemLinks.Remove(this.barButtonItemConsultVacations);
                    this.ribbonPageGroupVacationAndAbsence.ItemLinks.Add(this.barButtonItemConsultVacations);
                });
            FormUtil.InvokeRequired(this,
                delegate
                {
                    this.ribbonPageGroupExtraTime.ItemLinks.Remove(this.barButtonItemConsultExtraTime);
                    this.ribbonPageGroupExtraTime.ItemLinks.Add(this.barButtonItemConsultExtraTime);
                });            
        }

        private void barButtonItemCreateVacations_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void barButtonItemDeleteVacations_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void barButtonItemModifyVacations_ItemClick(object sender, ItemClickEventArgs e)
        {

        }
		
        private void barButtonItemForecast_ItemClick(object sender, ItemClickEventArgs e)
        {
			if (forecastForm == null || forecastForm.IsDisposed == true)
			{
				int separation = int.Parse(globalApplicationPreferences["TimeIntervalForecastSeparation"].Value);
				forecastForm = new ForescastForm(this, separation);
			}
			forecastForm.MdiParent = this;
			forecastForm.Activate();
			forecastForm.Show();
        }

		public void OpenChatForm()
		{
			if (operatorChatForm == null || operatorChatForm.IsDisposed == true)
			{
				if (IsGeneralSupervisor == true)
					operatorChatForm = new OperatorChatForm(SupervisedApplicationName, ChatOperatorType.GeneralSupervisor);
				else
					operatorChatForm = new OperatorChatForm(SupervisedApplicationName, ChatOperatorType.SpecificSupervisor);
                operatorChatForm.Tag = defaultSupervisionForm;
			}
            if (selectedDepartment != null)
			    operatorChatForm.DepartmentTypeCode = selectedDepartment.Code;
			operatorChatForm.Activate();
			operatorChatForm.Show();
		}


        public void barButtonItemConsultVacations_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (consultVacationform == null || consultVacationform.IsDisposed == true)
            {
                consultVacationform = new ConsultExtraTimeVacationsForms(SupervisedApplicationName, defaultSupervisionForm.SelectedDepartmentType, ConsultExtraTimeVacationsForms.ModeConsult.VacationsPermisions, this.IsGeneralSupervisor);
            }
            RibbonControl.ForceInitialize();
            consultVacationform.MdiParent = this;
            consultVacationform.Activate();
            consultVacationform.Show();            
        }

        public void barButtonItemConsultExtraTime_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (consultExtraTimeform == null || consultExtraTimeform.IsDisposed == true)
            {
                consultExtraTimeform = new ConsultExtraTimeVacationsForms(SupervisedApplicationName, defaultSupervisionForm.SelectedDepartmentType, ConsultExtraTimeVacationsForms.ModeConsult.ExtraTime, this.IsGeneralSupervisor);
            }
            RibbonControl.ForceInitialize();
            consultExtraTimeform.MdiParent = this;
            consultExtraTimeform.Activate();
            consultExtraTimeform.Show();            
        }     
        
        private void BarButtonItemSelectRoom_ItemClick(object sender, ItemClickEventArgs e)
        {
            defaultSupervisionForm.Activate();
            defaultSupervisionForm.ShowRoomTabPage();
        }

        private void barCheckItemUpTreshold_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            commandBarItemIndicators_ItemClick(sender, e);
            this.indicatorsForm.indicatorControlSystem.ShowExtraResultSeries(ExtraSerieTypes.UpTreshold,
                (sender as BarCheckItem).Checked, DateTime.MinValue);
            this.indicatorsForm.indicatorControlGroup.ShowExtraResultSeries(ExtraSerieTypes.UpTreshold,
                (sender as BarCheckItem).Checked, DateTime.MinValue);
            this.indicatorsForm.indicatorControlDepartment.ShowExtraResultSeries(ExtraSerieTypes.UpTreshold,
                (sender as BarCheckItem).Checked, DateTime.MinValue);
        }

        private void barCheckItemDownTreshold_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            commandBarItemIndicators_ItemClick(sender, e);
            this.indicatorsForm.indicatorControlSystem.ShowExtraResultSeries(ExtraSerieTypes.DownTreshold,
                (sender as BarCheckItem).Checked, DateTime.MinValue);
            this.indicatorsForm.indicatorControlGroup.ShowExtraResultSeries(ExtraSerieTypes.DownTreshold,
                (sender as BarCheckItem).Checked, DateTime.MinValue);
            this.indicatorsForm.indicatorControlDepartment.ShowExtraResultSeries(ExtraSerieTypes.DownTreshold,
                (sender as BarCheckItem).Checked, DateTime.MinValue);
        }

        private void barCheckItemTrend_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            commandBarItemIndicators_ItemClick(sender, e);
            this.indicatorsForm.indicatorControlSystem.ShowExtraResultSeries(ExtraSerieTypes.Trend, 
                (sender as BarCheckItem).Checked, DateTime.MinValue);
            this.indicatorsForm.indicatorControlGroup.ShowExtraResultSeries(ExtraSerieTypes.Trend,
                (sender as BarCheckItem).Checked, DateTime.MinValue);
            this.indicatorsForm.indicatorControlDepartment.ShowExtraResultSeries(ExtraSerieTypes.Trend,
                (sender as BarCheckItem).Checked, DateTime.MinValue);
        }

        private void barButtonItemOperatorPerformance_ItemClick(object sender, ItemClickEventArgs e)
        {
            defaultSupervisionForm.Activate();
            defaultSupervisionForm.ShowPerformanceOperatorTabPage();
        }

        private void repositoryItemRadioGroup3_SelectedIndexChanged(object sender, EventArgs e)
        {
            defaultSupervisionForm.Activate();
            defaultSupervisionForm.ShowPerformanceOperatorTabPage();
            RadioGroup rg = sender as RadioGroup;
            if (sender != null)
            {
                bool option = Convert.ToBoolean(rg.Text);
                this.defaultSupervisionForm.ShowPrimaryIndicators(option);                
            }
        }

        private void RibbonControl_Click(object sender, EventArgs e)
        {

        }


		void barButtonItemChat_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
			OpenChatForm();
		}

        private void barButtonItemHelp_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                ApplicationUtil.LaunchHelp(SmartCadConfiguration.DistFolder + "/Help/SUPERVISION Module.chm");
            }
            catch (Exception ex)
            {
                MessageForm.Show(ResourceLoader.GetString2("$Message.HelpNotInstalled"), ex);
            }
        }

        private void barButtonItemDispatch_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void SupervisionForm_MdiChildActivate(object sender, EventArgs e)
        {
            if (this.ActiveMdiChild is ConsultExtraTimeVacationsForms ||
                this.ActiveMdiChild is OperatorAssignForm)
            {
                barButtonItemPrint.Enabled = true;
                barButtonItemSave.Enabled = true;
            }
            else
            {
                barButtonItemPrint.Enabled = false;
                barButtonItemSave.Enabled = false;
            }
        }

        private void barButtonItemPrint_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (this.ActiveMdiChild is ConsultExtraTimeVacationsForms)
            {
                ((ConsultExtraTimeVacationsForms)this.ActiveMdiChild).printableComponentLink.PrintDlg();
            }
            else if (this.ActiveMdiChild is OperatorAssignForm)
            {
                ((OperatorAssignForm)this.ActiveMdiChild).schedulerControl.printableComponentLink.PrintDlg();
            }
        }

        private void barButtonItemSave_ItemClick(object sender, ItemClickEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.DefaultExt = "jpg";
            saveFileDialog.Filter = "jpeg files (*.jpg)|*.jpg";
            DialogResult resSave = saveFileDialog.ShowDialog();
            if (resSave == DialogResult.OK && !string.IsNullOrEmpty(saveFileDialog.FileName))
            {
                if (saveFileDialog.FilterIndex == 1)
                {
                    if (this.ActiveMdiChild is ConsultExtraTimeVacationsForms)
                    {
                        ((ConsultExtraTimeVacationsForms)this.ActiveMdiChild).printableComponentLink.ExportToImage(saveFileDialog.FileName);
                    }
                    else if (this.ActiveMdiChild is OperatorAssignForm)
                    {
                        ((OperatorAssignForm)this.ActiveMdiChild).schedulerControl.printableComponentLink.ExportToImage(saveFileDialog.FileName);
                    }
                }
            }
        }

    
    }
}
