﻿

using SmartCadControls;
namespace SmartCadGuiCommon
{
    partial class ConsultExtraTimeVacationsForms
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraScheduler.TimeRuler timeRuler3 = new DevExpress.XtraScheduler.TimeRuler();
            DevExpress.XtraScheduler.TimeRuler timeRuler4 = new DevExpress.XtraScheduler.TimeRuler();
            DevExpress.XtraScheduler.TimeRuler timeRuler1 = new DevExpress.XtraScheduler.TimeRuler();
            DevExpress.XtraScheduler.TimeRuler timeRuler2 = new DevExpress.XtraScheduler.TimeRuler();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConsultExtraTimeVacationsForms));
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanel1 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.gridControlOperators = new GridControlEx();
            this.gridViewOperators = new GridViewEx();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.dateNavigator1 = new DevExpress.XtraScheduler.DateNavigator();
            this.schedulerControlDefaultView = new DevExpress.XtraScheduler.SchedulerControl();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.viewNavigatorBar1 = new DevExpress.XtraScheduler.UI.ViewNavigatorBar();
            this.viewNavigatorBackwardItem1 = new DevExpress.XtraScheduler.UI.ViewNavigatorBackwardItem();
            this.viewNavigatorForwardItem1 = new DevExpress.XtraScheduler.UI.ViewNavigatorForwardItem();
            this.viewNavigatorTodayItem1 = new DevExpress.XtraScheduler.UI.ViewNavigatorTodayItem();
            this.viewNavigatorZoomInItem1 = new DevExpress.XtraScheduler.UI.ViewNavigatorZoomInItem();
            this.viewNavigatorZoomOutItem1 = new DevExpress.XtraScheduler.UI.ViewNavigatorZoomOutItem();
            this.viewSelectorBar1 = new DevExpress.XtraScheduler.UI.ViewSelectorBar();
            this.viewSelectorItem1 = new DevExpress.XtraScheduler.UI.ViewSelectorItem();
            this.viewSelectorItem2 = new DevExpress.XtraScheduler.UI.ViewSelectorItem();
            this.viewSelectorItem3 = new DevExpress.XtraScheduler.UI.ViewSelectorItem();
            this.viewSelectorItem4 = new DevExpress.XtraScheduler.UI.ViewSelectorItem();
            this.viewSelectorItem5 = new DevExpress.XtraScheduler.UI.ViewSelectorItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.schedulerStorage1 = new DevExpress.XtraScheduler.SchedulerStorage(this.components);
            this.toolTipController = new DevExpress.Utils.ToolTipController(this.components);
            this.comboBoxWorkShift = new DevExpress.XtraEditors.ComboBoxEdit();
            this.radioGroupFilter = new DevExpress.XtraEditors.RadioGroup();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            this.layoutControlGroupCalendar = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupFilter = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemDataGrid = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupCombo = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItemComboWs = new DevExpress.XtraLayout.LayoutControlItem();
            this.viewNavigator1 = new DevExpress.XtraScheduler.UI.ViewNavigator(this.components);
            this.viewSelector1 = new DevExpress.XtraScheduler.UI.ViewSelector(this.components);
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.schedulerControlCustomView = new DevExpress.XtraScheduler.SchedulerControl();
            this.RibbonControl = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.barButtonItemCreateExtraTime = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemDeleteExtraTime = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemModifyExtraTime = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemCreateVacations = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemDeleteVacations = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemModifyVacations = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemConsultVacations = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemConsultExtraTime = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemPersonalAssignVacations = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemPersonalAssignExtraTime = new DevExpress.XtraBars.BarButtonItem();
            this.RibbonPageOperation = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPage();
            this.ribbonPageGroupExtraTime = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupVacationAndAbsence = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.printingSystem = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.printableComponentLink = new DevExpress.XtraPrinting.PrintableComponentLink(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.dockPanel1.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlOperators)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewOperators)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateNavigator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerControlDefaultView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerStorage1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxWorkShift.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupCalendar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDataGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupCombo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemComboWs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewNavigator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewSelector1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerControlCustomView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RibbonControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem)).BeginInit();
            this.SuspendLayout();
            // 
            // dockManager1
            // 
            this.dockManager1.DockingOptions.HideImmediatelyOnAutoHide = true;
            this.dockManager1.DockModeVS2005FadeSpeed = 500;
            this.dockManager1.Form = this;
            this.dockManager1.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanel1});
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "System.Windows.Forms.StatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar"});
            // 
            // dockPanel1
            // 
            this.dockPanel1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(239)))));
            this.dockPanel1.Appearance.Options.UseBackColor = true;
            this.dockPanel1.Controls.Add(this.dockPanel1_Container);
            this.dockPanel1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanel1.ID = new System.Guid("bdcf900d-de07-4368-bbda-f1a8091d93b9");
            this.dockPanel1.Location = new System.Drawing.Point(0, 31);
            this.dockPanel1.Name = "dockPanel1";
            this.dockPanel1.Options.AllowDockBottom = false;
            this.dockPanel1.Options.AllowDockFill = false;
            this.dockPanel1.Options.AllowDockTop = false;
            this.dockPanel1.Options.AllowFloating = false;
            this.dockPanel1.Options.FloatOnDblClick = false;
            this.dockPanel1.Options.ShowCloseButton = false;
            this.dockPanel1.Options.ShowMaximizeButton = false;
            this.dockPanel1.OriginalSize = new System.Drawing.Size(389, 200);
            this.dockPanel1.Size = new System.Drawing.Size(389, 761);
            this.dockPanel1.Text = "Herramientas";
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.layoutControl1);
            this.dockPanel1_Container.Location = new System.Drawing.Point(4, 23);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(381, 734);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // layoutControl1
            // 
            this.layoutControl1.AllowCustomizationMenu = false;
            this.layoutControl1.Controls.Add(this.panelControl2);
            this.layoutControl1.Controls.Add(this.dateNavigator1);
            this.layoutControl1.Controls.Add(this.comboBoxWorkShift);
            this.layoutControl1.Controls.Add(this.radioGroupFilter);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(381, 734);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            this.layoutControl1.GroupExpandChanged += new DevExpress.XtraLayout.Utils.LayoutGroupEventHandler(this.layoutControl1_GroupExpandChanged);
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.gridControlOperators);
            this.panelControl2.Controls.Add(this.checkEdit1);
            this.panelControl2.Location = new System.Drawing.Point(7, 458);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(367, 269);
            this.panelControl2.TabIndex = 8;
            // 
            // gridControlOperators
            // 
            this.gridControlOperators.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControlOperators.EnableAutoFilter = true;
            this.gridControlOperators.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlOperators.Location = new System.Drawing.Point(5, 31);
            this.gridControlOperators.MainView = this.gridViewOperators;
            this.gridControlOperators.Name = "gridControlOperators";
            this.gridControlOperators.Size = new System.Drawing.Size(357, 233);
            this.gridControlOperators.TabIndex = 8;
            this.gridControlOperators.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewOperators});
            this.gridControlOperators.ViewTotalRows = true;
            // 
            // gridViewOperators
            // 
            this.gridViewOperators.AllowFocusedRowChanged = true;
            this.gridViewOperators.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewOperators.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewOperators.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewOperators.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewOperators.Appearance.FooterPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewOperators.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewOperators.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewOperators.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewOperators.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewOperators.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewOperators.EnablePreviewLineForFocusedRow = false;
            this.gridViewOperators.GridControl = this.gridControlOperators;
            this.gridViewOperators.GroupFormat = " [#image]{1} {2}";
            this.gridViewOperators.Name = "gridViewOperators";
            this.gridViewOperators.OptionsMenu.EnableFooterMenu = false;
            this.gridViewOperators.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewOperators.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewOperators.OptionsView.ShowFooter = true;
            this.gridViewOperators.ViewTotalRows = true;
            this.gridViewOperators.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridViewOperators_CustomRowCellEdit);
            this.gridViewOperators.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewOperators_CellValueChanging);
            // 
            // checkEdit1
            // 
            this.checkEdit1.Location = new System.Drawing.Point(5, 6);
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.AllowGrayed = true;
            this.checkEdit1.Properties.Caption = "Seleccionar todos";
            this.checkEdit1.Size = new System.Drawing.Size(126, 19);
            this.checkEdit1.TabIndex = 9;
            this.checkEdit1.CheckStateChanged += new System.EventHandler(this.checkEdit1_CheckStateChanged);
            // 
            // dateNavigator1
            // 
            this.dateNavigator1.HotDate = null;
            this.dateNavigator1.Location = new System.Drawing.Point(7, 27);
            this.dateNavigator1.Name = "dateNavigator1";
            this.dateNavigator1.SchedulerControl = this.schedulerControlDefaultView;
            this.dateNavigator1.Size = new System.Drawing.Size(367, 310);
            toolTipTitleItem1.Text = "Calendario";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Para escoger mas de un dia sobre el calendario mantenga presionado el boton princ" +
                "ipal del raton y arrastre para seleccionar";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.toolTipController.SetSuperTip(this.dateNavigator1, superToolTip1);
            this.dateNavigator1.TabIndex = 4;
            // 
            // schedulerControlDefaultView
            // 
            this.schedulerControlDefaultView.ActiveViewType = DevExpress.XtraScheduler.SchedulerViewType.Month;
            this.schedulerControlDefaultView.Appearance.Appointment.Options.UseTextOptions = true;
            this.schedulerControlDefaultView.Appearance.Appointment.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            this.schedulerControlDefaultView.Location = new System.Drawing.Point(102, 29);
            this.schedulerControlDefaultView.MenuManager = this.barManager1;
            this.schedulerControlDefaultView.Name = "schedulerControlDefaultView";
            this.schedulerControlDefaultView.OptionsCustomization.AllowAppointmentCopy = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.schedulerControlDefaultView.OptionsCustomization.AllowAppointmentCreate = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.schedulerControlDefaultView.OptionsCustomization.AllowAppointmentDelete = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.schedulerControlDefaultView.OptionsCustomization.AllowAppointmentDrag = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.schedulerControlDefaultView.OptionsCustomization.AllowAppointmentDragBetweenResources = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.schedulerControlDefaultView.OptionsCustomization.AllowAppointmentEdit = DevExpress.XtraScheduler.UsedAppointmentType.Custom;
            this.schedulerControlDefaultView.OptionsCustomization.AllowAppointmentMultiSelect = false;
            this.schedulerControlDefaultView.OptionsCustomization.AllowAppointmentResize = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.schedulerControlDefaultView.OptionsCustomization.AllowInplaceEditor = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.schedulerControlDefaultView.Size = new System.Drawing.Size(497, 242);
            this.schedulerControlDefaultView.Start = new System.DateTime(2008, 12, 1, 0, 0, 0, 0);
            this.schedulerControlDefaultView.Storage = this.schedulerStorage1;
            this.schedulerControlDefaultView.TabIndex = 0;
            this.schedulerControlDefaultView.Text = "schedulerControl1";
            this.schedulerControlDefaultView.ToolTipController = this.toolTipController;
            this.schedulerControlDefaultView.Views.DayView.AppointmentDisplayOptions.EndTimeVisibility = DevExpress.XtraScheduler.AppointmentTimeVisibility.Never;
            this.schedulerControlDefaultView.Views.DayView.AppointmentDisplayOptions.StartTimeVisibility = DevExpress.XtraScheduler.AppointmentTimeVisibility.Never;
            this.schedulerControlDefaultView.Views.DayView.TimeRulers.Add(timeRuler3);
            this.schedulerControlDefaultView.Views.MonthView.AppointmentDisplayOptions.EndTimeVisibility = DevExpress.XtraScheduler.AppointmentTimeVisibility.Never;
            this.schedulerControlDefaultView.Views.MonthView.AppointmentDisplayOptions.StartTimeVisibility = DevExpress.XtraScheduler.AppointmentTimeVisibility.Never;
            this.schedulerControlDefaultView.Views.TimelineView.AppointmentDisplayOptions.EndTimeVisibility = DevExpress.XtraScheduler.AppointmentTimeVisibility.Never;
            this.schedulerControlDefaultView.Views.TimelineView.AppointmentDisplayOptions.StartTimeVisibility = DevExpress.XtraScheduler.AppointmentTimeVisibility.Never;
            this.schedulerControlDefaultView.Views.WeekView.AppointmentDisplayOptions.EndTimeVisibility = DevExpress.XtraScheduler.AppointmentTimeVisibility.Never;
            this.schedulerControlDefaultView.Views.WeekView.AppointmentDisplayOptions.StartTimeVisibility = DevExpress.XtraScheduler.AppointmentTimeVisibility.Never;
            this.schedulerControlDefaultView.Views.WorkWeekView.AppointmentDisplayOptions.EndTimeVisibility = DevExpress.XtraScheduler.AppointmentTimeVisibility.Never;
            this.schedulerControlDefaultView.Views.WorkWeekView.AppointmentDisplayOptions.StartTimeVisibility = DevExpress.XtraScheduler.AppointmentTimeVisibility.Never;
            this.schedulerControlDefaultView.Views.WorkWeekView.TimeRulers.Add(timeRuler4);
            this.schedulerControlDefaultView.InitAppointmentDisplayText += new DevExpress.XtraScheduler.AppointmentDisplayTextEventHandler(this.schedulerControlDefaultView_InitAppointmentDisplayText);
            this.schedulerControlDefaultView.PreparePopupMenu += new DevExpress.XtraScheduler.PreparePopupMenuEventHandler(this.schedulerControlDefaultView_PreparePopupMenu);
            this.schedulerControlDefaultView.EditAppointmentFormShowing += new DevExpress.XtraScheduler.AppointmentFormEventHandler(this.schedulerControlDefaultView_EditAppointmentFormShowing);
            // 
            // barManager1
            // 
            this.barManager1.AllowCustomization = false;
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.viewNavigatorBar1,
            this.viewSelectorBar1});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.viewNavigatorBackwardItem1,
            this.viewNavigatorForwardItem1,
            this.viewNavigatorTodayItem1,
            this.viewNavigatorZoomInItem1,
            this.viewNavigatorZoomOutItem1,
            this.viewSelectorItem1,
            this.viewSelectorItem2,
            this.viewSelectorItem3,
            this.viewSelectorItem4,
            this.viewSelectorItem5});
            this.barManager1.MaxItemId = 34;
            // 
            // viewNavigatorBar1
            // 
            this.viewNavigatorBar1.DockCol = 0;
            this.viewNavigatorBar1.DockRow = 0;
            this.viewNavigatorBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.viewNavigatorBar1.FloatLocation = new System.Drawing.Point(349, 139);
            this.viewNavigatorBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.viewNavigatorBackwardItem1, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.viewNavigatorForwardItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.viewNavigatorTodayItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.viewNavigatorZoomInItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.viewNavigatorZoomOutItem1)});
            this.viewNavigatorBar1.OptionsBar.AllowQuickCustomization = false;
            this.viewNavigatorBar1.OptionsBar.DisableClose = true;
            this.viewNavigatorBar1.OptionsBar.DisableCustomization = true;
            this.viewNavigatorBar1.OptionsBar.DrawDragBorder = false;
            // 
            // viewNavigatorBackwardItem1
            // 
            this.viewNavigatorBackwardItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("viewNavigatorBackwardItem1.Glyph")));
            this.viewNavigatorBackwardItem1.GroupIndex = 1;
            this.viewNavigatorBackwardItem1.Id = 20;
            this.viewNavigatorBackwardItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("viewNavigatorBackwardItem1.LargeGlyph")));
            this.viewNavigatorBackwardItem1.Name = "viewNavigatorBackwardItem1";
            // 
            // viewNavigatorForwardItem1
            // 
            this.viewNavigatorForwardItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("viewNavigatorForwardItem1.Glyph")));
            this.viewNavigatorForwardItem1.GroupIndex = 1;
            this.viewNavigatorForwardItem1.Id = 21;
            this.viewNavigatorForwardItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("viewNavigatorForwardItem1.LargeGlyph")));
            this.viewNavigatorForwardItem1.Name = "viewNavigatorForwardItem1";
            // 
            // viewNavigatorTodayItem1
            // 
            this.viewNavigatorTodayItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("viewNavigatorTodayItem1.Glyph")));
            this.viewNavigatorTodayItem1.GroupIndex = 1;
            this.viewNavigatorTodayItem1.Id = 22;
            this.viewNavigatorTodayItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("viewNavigatorTodayItem1.LargeGlyph")));
            this.viewNavigatorTodayItem1.Name = "viewNavigatorTodayItem1";
            // 
            // viewNavigatorZoomInItem1
            // 
            this.viewNavigatorZoomInItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("viewNavigatorZoomInItem1.Glyph")));
            this.viewNavigatorZoomInItem1.GroupIndex = 1;
            this.viewNavigatorZoomInItem1.Id = 23;
            this.viewNavigatorZoomInItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("viewNavigatorZoomInItem1.LargeGlyph")));
            this.viewNavigatorZoomInItem1.Name = "viewNavigatorZoomInItem1";
            // 
            // viewNavigatorZoomOutItem1
            // 
            this.viewNavigatorZoomOutItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("viewNavigatorZoomOutItem1.Glyph")));
            this.viewNavigatorZoomOutItem1.GroupIndex = 1;
            this.viewNavigatorZoomOutItem1.Id = 24;
            this.viewNavigatorZoomOutItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("viewNavigatorZoomOutItem1.LargeGlyph")));
            this.viewNavigatorZoomOutItem1.Name = "viewNavigatorZoomOutItem1";
            // 
            // viewSelectorBar1
            // 
            this.viewSelectorBar1.DockCol = 1;
            this.viewSelectorBar1.DockRow = 0;
            this.viewSelectorBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.viewSelectorBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.viewSelectorItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.viewSelectorItem2),
            new DevExpress.XtraBars.LinkPersistInfo(this.viewSelectorItem3),
            new DevExpress.XtraBars.LinkPersistInfo(this.viewSelectorItem4),
            new DevExpress.XtraBars.LinkPersistInfo(this.viewSelectorItem5)});
            this.viewSelectorBar1.OptionsBar.AllowQuickCustomization = false;
            this.viewSelectorBar1.OptionsBar.DisableClose = true;
            this.viewSelectorBar1.OptionsBar.DisableCustomization = true;
            this.viewSelectorBar1.OptionsBar.DrawDragBorder = false;
            // 
            // viewSelectorItem1
            // 
            this.viewSelectorItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("viewSelectorItem1.Glyph")));
            this.viewSelectorItem1.GroupIndex = 1;
            this.viewSelectorItem1.Id = 25;
            this.viewSelectorItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("viewSelectorItem1.LargeGlyph")));
            this.viewSelectorItem1.Name = "viewSelectorItem1";
            this.viewSelectorItem1.SchedulerViewType = DevExpress.XtraScheduler.SchedulerViewType.Day;
            // 
            // viewSelectorItem2
            // 
            this.viewSelectorItem2.Glyph = ((System.Drawing.Image)(resources.GetObject("viewSelectorItem2.Glyph")));
            this.viewSelectorItem2.GroupIndex = 1;
            this.viewSelectorItem2.Id = 26;
            this.viewSelectorItem2.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("viewSelectorItem2.LargeGlyph")));
            this.viewSelectorItem2.Name = "viewSelectorItem2";
            this.viewSelectorItem2.SchedulerViewType = DevExpress.XtraScheduler.SchedulerViewType.WorkWeek;
            // 
            // viewSelectorItem3
            // 
            this.viewSelectorItem3.Glyph = ((System.Drawing.Image)(resources.GetObject("viewSelectorItem3.Glyph")));
            this.viewSelectorItem3.GroupIndex = 1;
            this.viewSelectorItem3.Id = 27;
            this.viewSelectorItem3.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("viewSelectorItem3.LargeGlyph")));
            this.viewSelectorItem3.Name = "viewSelectorItem3";
            this.viewSelectorItem3.SchedulerViewType = DevExpress.XtraScheduler.SchedulerViewType.Week;
            // 
            // viewSelectorItem4
            // 
            this.viewSelectorItem4.Checked = true;
            this.viewSelectorItem4.Glyph = ((System.Drawing.Image)(resources.GetObject("viewSelectorItem4.Glyph")));
            this.viewSelectorItem4.GroupIndex = 1;
            this.viewSelectorItem4.Id = 28;
            this.viewSelectorItem4.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("viewSelectorItem4.LargeGlyph")));
            this.viewSelectorItem4.Name = "viewSelectorItem4";
            this.viewSelectorItem4.SchedulerViewType = DevExpress.XtraScheduler.SchedulerViewType.Month;
            // 
            // viewSelectorItem5
            // 
            this.viewSelectorItem5.Glyph = ((System.Drawing.Image)(resources.GetObject("viewSelectorItem5.Glyph")));
            this.viewSelectorItem5.GroupIndex = 1;
            this.viewSelectorItem5.Id = 29;
            this.viewSelectorItem5.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("viewSelectorItem5.LargeGlyph")));
            this.viewSelectorItem5.Name = "viewSelectorItem5";
            this.viewSelectorItem5.SchedulerViewType = DevExpress.XtraScheduler.SchedulerViewType.Timeline;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1015, 31);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 792);
            this.barDockControlBottom.Size = new System.Drawing.Size(1015, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 31);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 761);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1015, 31);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 761);
            // 
            // schedulerStorage1
            // 
            this.schedulerStorage1.Appointments.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentCustomFieldMapping("Color", ""));
            this.schedulerStorage1.Appointments.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentCustomFieldMapping("AType", ""));
            this.schedulerStorage1.Appointments.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentCustomFieldMapping("WorkShiftVariationCode", "", DevExpress.XtraScheduler.FieldValueType.Integer));
            this.schedulerStorage1.Appointments.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentCustomFieldMapping("WorkShiftCode", "", DevExpress.XtraScheduler.FieldValueType.Integer));
            this.schedulerStorage1.Appointments.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentCustomFieldMapping("WorkShiftVariationScheduleCode", "", DevExpress.XtraScheduler.FieldValueType.Integer));
            this.schedulerStorage1.Appointments.Labels.Add(new DevExpress.XtraScheduler.AppointmentLabel(System.Drawing.Color.CornflowerBlue, "WorkShift", "&WorkShift"));
            this.schedulerStorage1.Appointments.Labels.Add(new DevExpress.XtraScheduler.AppointmentLabel(System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(204)))), ((int)(((byte)(111))))), "ExtraTime", "&ExtraTime"));
            this.schedulerStorage1.Appointments.Labels.Add(new DevExpress.XtraScheduler.AppointmentLabel(System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(196)))), ((int)(((byte)(121))))), "Vacation", "&Vacation"));
            this.schedulerStorage1.FilterAppointment += new DevExpress.XtraScheduler.PersistentObjectCancelEventHandler(this.schedulerStorage1_FilterAppointment);
            // 
            // comboBoxWorkShift
            // 
            this.comboBoxWorkShift.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxWorkShift.Location = new System.Drawing.Point(146, 412);
            this.comboBoxWorkShift.Name = "comboBoxWorkShift";
            this.comboBoxWorkShift.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxWorkShift.Properties.Sorted = true;
            this.comboBoxWorkShift.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxWorkShift.Size = new System.Drawing.Size(228, 20);
            this.comboBoxWorkShift.StyleController = this.layoutControl1;
            this.comboBoxWorkShift.TabIndex = 7;
            this.comboBoxWorkShift.EditValueChanged += new System.EventHandler(this.comboBoxWorkShift_EditValueChanged);
            // 
            // radioGroupFilter
            // 
            this.radioGroupFilter.Location = new System.Drawing.Point(7, 376);
            this.radioGroupFilter.Name = "radioGroupFilter";
            this.radioGroupFilter.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Turnos"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "Personal")});
            this.radioGroupFilter.Properties.MouseWheel += new System.Windows.Forms.MouseEventHandler(this.radioGroupFilter_Properties_MouseWheel);
            this.radioGroupFilter.Size = new System.Drawing.Size(367, 32);
            this.radioGroupFilter.StyleController = this.layoutControl1;
            this.radioGroupFilter.TabIndex = 9;
            this.radioGroupFilter.SelectedIndexChanged += new System.EventHandler(this.radioGroupFilter_SelectedIndexChanged);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.splitterItem1,
            this.layoutControlGroupCalendar,
            this.layoutControlGroupFilter});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(381, 734);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            this.splitterItem1.CustomizationFormText = "splitterItem1";
            this.splitterItem1.Location = new System.Drawing.Point(0, 344);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(381, 5);
            // 
            // layoutControlGroupCalendar
            // 
            this.layoutControlGroupCalendar.CustomizationFormText = "layoutControlGroup4";
            this.layoutControlGroupCalendar.ExpandButtonLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroupCalendar.ExpandButtonVisible = true;
            this.layoutControlGroupCalendar.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.layoutControlGroupCalendar.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupCalendar.Name = "layoutControlGroupCalendar";
            this.layoutControlGroupCalendar.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupCalendar.Size = new System.Drawing.Size(381, 344);
            this.layoutControlGroupCalendar.Text = "Calendario";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dateNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(371, 314);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlGroupFilter
            // 
            this.layoutControlGroupFilter.CustomizationFormText = "Filtros";
            this.layoutControlGroupFilter.ExpandButtonLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroupFilter.ExpandButtonVisible = true;
            this.layoutControlGroupFilter.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemDataGrid,
            this.layoutControlItem2,
            this.layoutControlGroupCombo});
            this.layoutControlGroupFilter.Location = new System.Drawing.Point(0, 349);
            this.layoutControlGroupFilter.Name = "layoutControlGroupFilter";
            this.layoutControlGroupFilter.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupFilter.Size = new System.Drawing.Size(381, 385);
            this.layoutControlGroupFilter.Text = "Filtros";
            // 
            // layoutControlItemDataGrid
            // 
            this.layoutControlItemDataGrid.Control = this.panelControl2;
            this.layoutControlItemDataGrid.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItemDataGrid.Location = new System.Drawing.Point(0, 82);
            this.layoutControlItemDataGrid.Name = "layoutControlItemDataGrid";
            this.layoutControlItemDataGrid.Size = new System.Drawing.Size(371, 273);
            this.layoutControlItemDataGrid.Text = "layoutControlItemDataGrid";
            this.layoutControlItemDataGrid.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemDataGrid.TextToControlDistance = 0;
            this.layoutControlItemDataGrid.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.radioGroupFilter;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(0, 36);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(61, 36);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(371, 36);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "Filtro :";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlGroupCombo
            // 
            this.layoutControlGroupCombo.CustomizationFormText = "layoutControlGroupCombo";
            this.layoutControlGroupCombo.GroupBordersVisible = false;
            this.layoutControlGroupCombo.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.layoutControlItemComboWs});
            this.layoutControlGroupCombo.Location = new System.Drawing.Point(0, 36);
            this.layoutControlGroupCombo.Name = "layoutControlGroupCombo";
            this.layoutControlGroupCombo.Size = new System.Drawing.Size(371, 46);
            this.layoutControlGroupCombo.Text = "layoutControlGroupCombo";
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 31);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(371, 15);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItemComboWs
            // 
            this.layoutControlItemComboWs.Control = this.comboBoxWorkShift;
            this.layoutControlItemComboWs.CustomizationFormText = "layoutControlItemComboWs";
            this.layoutControlItemComboWs.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemComboWs.MaxSize = new System.Drawing.Size(0, 31);
            this.layoutControlItemComboWs.MinSize = new System.Drawing.Size(201, 31);
            this.layoutControlItemComboWs.Name = "layoutControlItemComboWs";
            this.layoutControlItemComboWs.Size = new System.Drawing.Size(371, 31);
            this.layoutControlItemComboWs.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemComboWs.Text = "layoutControlItemComboWs";
            this.layoutControlItemComboWs.TextSize = new System.Drawing.Size(135, 13);
            // 
            // viewNavigator1
            // 
            this.viewNavigator1.BarManager = this.barManager1;
            this.viewNavigator1.SchedulerControl = this.schedulerControlDefaultView;
            // 
            // viewSelector1
            // 
            this.viewSelector1.BarManager = this.barManager1;
            this.viewSelector1.SchedulerControl = this.schedulerControlDefaultView;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.schedulerControlCustomView);
            this.panelControl1.Controls.Add(this.schedulerControlDefaultView);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(389, 31);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(626, 761);
            this.panelControl1.TabIndex = 4;
            // 
            // schedulerControlCustomView
            // 
            this.schedulerControlCustomView.Appearance.Appointment.Options.UseTextOptions = true;
            this.schedulerControlCustomView.Appearance.Appointment.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            this.schedulerControlCustomView.Location = new System.Drawing.Point(103, 360);
            this.schedulerControlCustomView.MenuManager = this.barManager1;
            this.schedulerControlCustomView.Name = "schedulerControlCustomView";
            this.schedulerControlCustomView.OptionsCustomization.AllowAppointmentCopy = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.schedulerControlCustomView.OptionsCustomization.AllowAppointmentCreate = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.schedulerControlCustomView.OptionsCustomization.AllowAppointmentDelete = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.schedulerControlCustomView.OptionsCustomization.AllowAppointmentDrag = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.schedulerControlCustomView.OptionsCustomization.AllowAppointmentDragBetweenResources = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.schedulerControlCustomView.OptionsCustomization.AllowAppointmentEdit = DevExpress.XtraScheduler.UsedAppointmentType.Custom;
            this.schedulerControlCustomView.OptionsCustomization.AllowAppointmentMultiSelect = false;
            this.schedulerControlCustomView.OptionsCustomization.AllowAppointmentResize = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.schedulerControlCustomView.Size = new System.Drawing.Size(496, 244);
            this.schedulerControlCustomView.Start = new System.DateTime(2008, 10, 14, 0, 0, 0, 0);
            this.schedulerControlCustomView.Storage = this.schedulerStorage1;
            this.schedulerControlCustomView.TabIndex = 1;
            this.schedulerControlCustomView.Text = "schedulerControl1";
            this.schedulerControlCustomView.ToolTipController = this.toolTipController;
            this.schedulerControlCustomView.Views.DayView.TimeRulers.Add(timeRuler1);
            this.schedulerControlCustomView.Views.WorkWeekView.TimeRulers.Add(timeRuler2);
            // 
            // RibbonControl
            // 
            this.RibbonControl.Dock = System.Windows.Forms.DockStyle.None;
            this.RibbonControl.ExpandCollapseItem.Id = 0;
            this.RibbonControl.ExpandCollapseItem.Name = "";
            this.RibbonControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RibbonControl.Images = this.imageList;
            this.RibbonControl.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.RibbonControl.ExpandCollapseItem,
            this.barButtonItemCreateExtraTime,
            this.barButtonItemDeleteExtraTime,
            this.barButtonItemModifyExtraTime,
            this.barButtonItemCreateVacations,
            this.barButtonItemDeleteVacations,
            this.barButtonItemModifyVacations,
            this.barButtonItemConsultVacations,
            this.barButtonItemConsultExtraTime,
            this.barButtonItemPersonalAssignVacations,
            this.barButtonItemPersonalAssignExtraTime});
            this.RibbonControl.ItemsVertAlign = DevExpress.Utils.VertAlignment.Top;
            this.RibbonControl.LargeImages = this.imageList;
            this.RibbonControl.Location = new System.Drawing.Point(400, 650);
            this.RibbonControl.MaxItemId = 263;
            this.RibbonControl.MdiMergeStyle = DevExpress.XtraBars.Ribbon.RibbonMdiMergeStyle.Always;
            this.RibbonControl.Name = "RibbonControl";
            this.RibbonControl.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.RibbonPageOperation});
            this.RibbonControl.ShowToolbarCustomizeItem = false;
            this.RibbonControl.Size = new System.Drawing.Size(309, 120);
            this.RibbonControl.Toolbar.ShowCustomizeItem = false;
            this.RibbonControl.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "logout.gif");
            // 
            // barButtonItemCreateExtraTime
            // 
            this.barButtonItemCreateExtraTime.Caption = "Crear";
            this.barButtonItemCreateExtraTime.Enabled = false;
            this.barButtonItemCreateExtraTime.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemCreateExtraTime.Glyph")));
            this.barButtonItemCreateExtraTime.Id = 251;
            this.barButtonItemCreateExtraTime.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemCreateExtraTime.Name = "barButtonItemCreateExtraTime";
            this.barButtonItemCreateExtraTime.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // barButtonItemDeleteExtraTime
            // 
            this.barButtonItemDeleteExtraTime.Caption = "Eliminar";
            this.barButtonItemDeleteExtraTime.Enabled = false;
            this.barButtonItemDeleteExtraTime.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemDeleteExtraTime.Glyph")));
            this.barButtonItemDeleteExtraTime.Id = 252;
            this.barButtonItemDeleteExtraTime.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemDeleteExtraTime.Name = "barButtonItemDeleteExtraTime";
            this.barButtonItemDeleteExtraTime.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // barButtonItemModifyExtraTime
            // 
            this.barButtonItemModifyExtraTime.Caption = "Modificar";
            this.barButtonItemModifyExtraTime.Enabled = false;
            this.barButtonItemModifyExtraTime.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemModifyExtraTime.Glyph")));
            this.barButtonItemModifyExtraTime.Id = 253;
            this.barButtonItemModifyExtraTime.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemModifyExtraTime.Name = "barButtonItemModifyExtraTime";
            this.barButtonItemModifyExtraTime.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // barButtonItemCreateVacations
            // 
            this.barButtonItemCreateVacations.Caption = "Crear";
            this.barButtonItemCreateVacations.Enabled = false;
            this.barButtonItemCreateVacations.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemCreateVacations.Glyph")));
            this.barButtonItemCreateVacations.Id = 255;
            this.barButtonItemCreateVacations.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemCreateVacations.Name = "barButtonItemCreateVacations";
            this.barButtonItemCreateVacations.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // barButtonItemDeleteVacations
            // 
            this.barButtonItemDeleteVacations.Caption = "Eliminar";
            this.barButtonItemDeleteVacations.Enabled = false;
            this.barButtonItemDeleteVacations.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemDeleteVacations.Glyph")));
            this.barButtonItemDeleteVacations.Id = 256;
            this.barButtonItemDeleteVacations.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemDeleteVacations.Name = "barButtonItemDeleteVacations";
            this.barButtonItemDeleteVacations.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // barButtonItemModifyVacations
            // 
            this.barButtonItemModifyVacations.Caption = "Modificar";
            this.barButtonItemModifyVacations.Enabled = false;
            this.barButtonItemModifyVacations.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemModifyVacations.Glyph")));
            this.barButtonItemModifyVacations.Id = 257;
            this.barButtonItemModifyVacations.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemModifyVacations.Name = "barButtonItemModifyVacations";
            this.barButtonItemModifyVacations.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // barButtonItemConsultVacations
            // 
            this.barButtonItemConsultVacations.Caption = "Consulta";
            this.barButtonItemConsultVacations.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemConsultVacations.Glyph")));
            this.barButtonItemConsultVacations.Id = 259;
            this.barButtonItemConsultVacations.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemConsultVacations.Name = "barButtonItemConsultVacations";
            this.barButtonItemConsultVacations.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemConsultVacations.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemConsultVacations_ItemClick);
            // 
            // barButtonItemConsultExtraTime
            // 
            this.barButtonItemConsultExtraTime.Caption = "Consulta";
            this.barButtonItemConsultExtraTime.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemConsultExtraTime.Glyph")));
            this.barButtonItemConsultExtraTime.Id = 260;
            this.barButtonItemConsultExtraTime.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemConsultExtraTime.Name = "barButtonItemConsultExtraTime";
            this.barButtonItemConsultExtraTime.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemConsultExtraTime.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemConsultExtraTime_ItemClick);
            // 
            // barButtonItemPersonalAssignVacations
            // 
            this.barButtonItemPersonalAssignVacations.Caption = "Personal";
            this.barButtonItemPersonalAssignVacations.Enabled = false;
            this.barButtonItemPersonalAssignVacations.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPersonalAssignVacations.Glyph")));
            this.barButtonItemPersonalAssignVacations.Id = 261;
            this.barButtonItemPersonalAssignVacations.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemPersonalAssignVacations.Name = "barButtonItemPersonalAssignVacations";
            this.barButtonItemPersonalAssignVacations.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // barButtonItemPersonalAssignExtraTime
            // 
            this.barButtonItemPersonalAssignExtraTime.Caption = "Personal";
            this.barButtonItemPersonalAssignExtraTime.Enabled = false;
            this.barButtonItemPersonalAssignExtraTime.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPersonalAssignExtraTime.Glyph")));
            this.barButtonItemPersonalAssignExtraTime.Id = 262;
            this.barButtonItemPersonalAssignExtraTime.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemPersonalAssignExtraTime.Name = "barButtonItemPersonalAssignExtraTime";
            this.barButtonItemPersonalAssignExtraTime.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // RibbonPageOperation
            // 
            this.RibbonPageOperation.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupExtraTime,
            this.ribbonPageGroupVacationAndAbsence});
            this.RibbonPageOperation.Name = "RibbonPageOperation";
            this.RibbonPageOperation.Text = "Operacion";
            // 
            // ribbonPageGroupExtraTime
            // 
            this.ribbonPageGroupExtraTime.AllowMinimize = false;
            this.ribbonPageGroupExtraTime.AllowTextClipping = false;
            this.ribbonPageGroupExtraTime.Name = "ribbonPageGroupExtraTime";
            this.ribbonPageGroupExtraTime.ShowCaptionButton = false;
            this.ribbonPageGroupExtraTime.Text = "Tiempo extra";
            // 
            // ribbonPageGroupVacationAndAbsence
            // 
            this.ribbonPageGroupVacationAndAbsence.AllowMinimize = false;
            this.ribbonPageGroupVacationAndAbsence.AllowTextClipping = false;
            this.ribbonPageGroupVacationAndAbsence.Name = "ribbonPageGroupVacationAndAbsence";
            this.ribbonPageGroupVacationAndAbsence.ShowCaptionButton = false;
            this.ribbonPageGroupVacationAndAbsence.Text = "Permisos y vacaciones";
            // 
            // printingSystem
            // 
            this.printingSystem.ExportOptions.Image.Format = System.Drawing.Imaging.ImageFormat.Jpeg;
            this.printingSystem.Links.AddRange(new object[] {
            this.printableComponentLink});
            // 
            // printableComponentLink
            // 
            this.printableComponentLink.Component = this.schedulerControlDefaultView;
            // 
            // 
            // 
            this.printableComponentLink.ImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("printableComponentLink.ImageCollection.ImageStream")));
            this.printableComponentLink.Landscape = true;
            this.printableComponentLink.PrintingSystem = this.printingSystem;
            this.printableComponentLink.PrintingSystemBase = this.printingSystem;
            // 
            // ConsultExtraTimeVacationsForms
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1015, 792);
            this.Controls.Add(this.RibbonControl);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.dockPanel1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "ConsultExtraTimeVacationsForms";
            this.Text = "ConsultExtraTimeVacationsForms";
            this.Load += new System.EventHandler(this.ConsultExtraTimeVacationsForms_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.dockPanel1.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlOperators)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewOperators)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateNavigator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerControlDefaultView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerStorage1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxWorkShift.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupCalendar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDataGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupCombo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemComboWs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewNavigator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewSelector1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.schedulerControlCustomView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RibbonControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraScheduler.SchedulerStorage schedulerStorage1;
        private DevExpress.XtraScheduler.UI.ViewNavigator viewNavigator1;
        private DevExpress.XtraScheduler.UI.ViewSelector viewSelector1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        internal DevExpress.XtraScheduler.SchedulerControl schedulerControlDefaultView;
        private DevExpress.XtraScheduler.UI.ViewNavigatorBar viewNavigatorBar1;
        private DevExpress.XtraScheduler.UI.ViewNavigatorBackwardItem viewNavigatorBackwardItem1;
        private DevExpress.XtraScheduler.UI.ViewNavigatorForwardItem viewNavigatorForwardItem1;
        private DevExpress.XtraScheduler.UI.ViewNavigatorTodayItem viewNavigatorTodayItem1;
        private DevExpress.XtraScheduler.UI.ViewNavigatorZoomInItem viewNavigatorZoomInItem1;
        private DevExpress.XtraScheduler.UI.ViewNavigatorZoomOutItem viewNavigatorZoomOutItem1;
        private DevExpress.XtraScheduler.UI.ViewSelectorBar viewSelectorBar1;
        private DevExpress.XtraScheduler.UI.ViewSelectorItem viewSelectorItem1;
        private DevExpress.XtraScheduler.UI.ViewSelectorItem viewSelectorItem2;
        private DevExpress.XtraScheduler.UI.ViewSelectorItem viewSelectorItem3;
        private DevExpress.XtraScheduler.UI.ViewSelectorItem viewSelectorItem4;
        private DevExpress.XtraScheduler.UI.ViewSelectorItem viewSelectorItem5;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel1;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private GridControlEx gridControlOperators;
        private GridViewEx gridViewOperators;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxWorkShift;
        private DevExpress.XtraScheduler.DateNavigator dateNavigator1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.SplitterItem splitterItem1;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupCalendar;
        private DevExpress.XtraScheduler.SchedulerControl schedulerControlCustomView;
        
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.RadioGroup radioGroupFilter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupFilter;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDataGrid;
        internal DevExpress.XtraBars.Ribbon.RibbonControl RibbonControl;
        private System.Windows.Forms.ImageList imageList;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemCreateExtraTime;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemDeleteExtraTime;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemModifyExtraTime;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemCreateVacations;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemDeleteVacations;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemModifyVacations;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemConsultVacations;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemConsultExtraTime;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemPersonalAssignVacations;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemPersonalAssignExtraTime;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPage RibbonPageOperation;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupExtraTime;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupVacationAndAbsence;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemComboWs;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupCombo;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.Utils.ToolTipController toolTipController;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem;
        internal DevExpress.XtraPrinting.PrintableComponentLink printableComponentLink;

    }
}