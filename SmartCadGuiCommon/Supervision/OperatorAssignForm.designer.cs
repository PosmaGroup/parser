
using SmartCadControls;
using SmartCadGuiCommon.Controls;
namespace SmartCadGuiCommon
{
    partial class OperatorAssignForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OperatorAssignForm));
            this.layoutControlOperatorAssign = new DevExpress.XtraLayout.LayoutControl();
            this.RibbonControl = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.barButtonItemRemove = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.barButtonItemAssign = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemSend = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemSave = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemPrint = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemUpdate = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem2 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem3 = new DevExpress.XtraBars.BarStaticItem();
            this.barButtonItemOpen = new DevExpress.XtraBars.BarButtonItem();
            this.RibbonPageOperation = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPage();
            this.ribbonPageGroupAssignment = new DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup();
            this.schedulerStorage1 = new DevExpress.XtraScheduler.SchedulerStorage(this.components);
            this.radioButtonSupervised = new System.Windows.Forms.RadioButton();
            this.radioButtonPartially = new System.Windows.Forms.RadioButton();
            this.radioButtonNotSupervised = new System.Windows.Forms.RadioButton();
            this.radioButtonAll = new System.Windows.Forms.RadioButton();
            this.gridControlExAssignedOperators = new GridControlEx();
            this.gridViewExAssignedOperators = new GridViewEx();
            this.gridControlExSupervisors = new GridControlEx();
            this.gridViewExSupervisors = new GridViewEx();
            this.gridControlExOperators = new GridControlEx();
            this.gridViewExOperators = new GridViewEx();
            this.gridViewEx2 = new GridViewEx();
            this.schedulerControl = new OperatorAssignSchedulerControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            this.layoutControlGroupOperators = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemOperators = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupSupervisors = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemSupervisors = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupAssignedOperators = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemAssignedOperators = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupScheduler = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemScheduler = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlOperatorAssign)).BeginInit();
            this.layoutControlOperatorAssign.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RibbonControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerStorage1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExAssignedOperators)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExAssignedOperators)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExSupervisors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExSupervisors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExOperators)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExOperators)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEx2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupOperators)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemOperators)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupSupervisors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemSupervisors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupAssignedOperators)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAssignedOperators)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupScheduler)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemScheduler)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControlOperatorAssign
            // 
            this.layoutControlOperatorAssign.AllowCustomizationMenu = false;
            this.layoutControlOperatorAssign.Controls.Add(this.radioButtonSupervised);
            this.layoutControlOperatorAssign.Controls.Add(this.radioButtonPartially);
            this.layoutControlOperatorAssign.Controls.Add(this.radioButtonNotSupervised);
            this.layoutControlOperatorAssign.Controls.Add(this.radioButtonAll);
            this.layoutControlOperatorAssign.Controls.Add(this.gridControlExAssignedOperators);
            this.layoutControlOperatorAssign.Controls.Add(this.gridControlExSupervisors);
            this.layoutControlOperatorAssign.Controls.Add(this.gridControlExOperators);
            this.layoutControlOperatorAssign.Controls.Add(this.schedulerControl);
            this.layoutControlOperatorAssign.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlOperatorAssign.Location = new System.Drawing.Point(0, 0);
            this.layoutControlOperatorAssign.Name = "layoutControlOperatorAssign";
            this.layoutControlOperatorAssign.Root = this.layoutControlGroup1;
            this.layoutControlOperatorAssign.Size = new System.Drawing.Size(1262, 753);
            this.layoutControlOperatorAssign.TabIndex = 0;
            this.layoutControlOperatorAssign.Text = "layoutControl1";
            // 
            // RibbonControl
            // 
            this.RibbonControl.Dock = System.Windows.Forms.DockStyle.None;
            this.RibbonControl.ExpandCollapseItem.Id = 0;
            this.RibbonControl.ExpandCollapseItem.Name = "";
            this.RibbonControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RibbonControl.Images = this.imageList2;
            this.RibbonControl.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.RibbonControl.ExpandCollapseItem,
            this.barButtonItemRemove,
            this.barButtonItemAssign,
            this.barButtonItemSend,
            this.barButtonItemSave,
            this.barButtonItemPrint,
            this.barButtonItemUpdate,
            this.barStaticItem1,
            this.barStaticItem2,
            this.barStaticItem3,
            this.barButtonItemOpen});
            this.RibbonControl.ItemsVertAlign = DevExpress.Utils.VertAlignment.Top;
            this.RibbonControl.LargeImages = this.imageList2;
            this.RibbonControl.Location = new System.Drawing.Point(511, 317);
            this.RibbonControl.Margin = new System.Windows.Forms.Padding(2);
            this.RibbonControl.MaxItemId = 262;
            this.RibbonControl.MdiMergeStyle = DevExpress.XtraBars.Ribbon.RibbonMdiMergeStyle.Always;
            this.RibbonControl.Name = "RibbonControl";
            this.RibbonControl.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.RibbonPageOperation});
            this.RibbonControl.ShowToolbarCustomizeItem = false;
            this.RibbonControl.Size = new System.Drawing.Size(165, 120);
            this.RibbonControl.Toolbar.ShowCustomizeItem = false;
            this.RibbonControl.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            // 
            // imageList2
            // 
            this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList2.Images.SetKeyName(0, "logout.gif");
            // 
            // barButtonItemRemove
            // 
            this.barButtonItemRemove.Caption = "Eliminar";
            this.barButtonItemRemove.Enabled = false;
            this.barButtonItemRemove.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemRemove.Glyph")));
            this.barButtonItemRemove.Id = 68;
            this.barButtonItemRemove.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemRemove.Name = "barButtonItemRemove";
            this.barButtonItemRemove.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemRemove.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemRemove_ItemClick);
            // 
            // barButtonItemAssign
            // 
            this.barButtonItemAssign.Caption = "Asignar";
            this.barButtonItemAssign.Enabled = false;
            this.barButtonItemAssign.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemAssign.Glyph")));
            this.barButtonItemAssign.Id = 170;
            this.barButtonItemAssign.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemAssign.Name = "barButtonItemAssign";
            this.barButtonItemAssign.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemAssign.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemAssign_ItemClick);
            // 
            // barButtonItemSend
            // 
            this.barButtonItemSend.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemSend.Glyph")));
            this.barButtonItemSend.Id = 229;
            this.barButtonItemSend.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Enter));
            this.barButtonItemSend.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemSend.Name = "barButtonItemSend";
            this.barButtonItemSend.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // barButtonItemSave
            // 
            this.barButtonItemSave.Glyph = global::SmartCadGuiCommon.PrintRibbonControllerResources.RibbonPrintPreview_Save;
            this.barButtonItemSave.Id = 230;
            this.barButtonItemSave.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S));
            this.barButtonItemSave.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemSave.Name = "barButtonItemSave";
            this.barButtonItemSave.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // barButtonItemPrint
            // 
            this.barButtonItemPrint.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPrint.Glyph")));
            this.barButtonItemPrint.Id = 231;
            this.barButtonItemPrint.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P));
            this.barButtonItemPrint.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemPrint.Name = "barButtonItemPrint";
            this.barButtonItemPrint.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // barButtonItemUpdate
            // 
            this.barButtonItemUpdate.Enabled = false;
            this.barButtonItemUpdate.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemUpdate.Glyph")));
            this.barButtonItemUpdate.Id = 228;
            this.barButtonItemUpdate.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F5);
            this.barButtonItemUpdate.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemUpdate.Name = "barButtonItemUpdate";
            this.barButtonItemUpdate.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "Imprimir";
            this.barStaticItem1.Id = 258;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem2
            // 
            this.barStaticItem2.Caption = "Exportar como PDF";
            this.barStaticItem2.Id = 259;
            this.barStaticItem2.Name = "barStaticItem2";
            this.barStaticItem2.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem3
            // 
            this.barStaticItem3.Caption = "Exportar como HTML";
            this.barStaticItem3.Id = 260;
            this.barStaticItem3.Name = "barStaticItem3";
            this.barStaticItem3.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barButtonItemOpen
            // 
            this.barButtonItemOpen.Caption = "Abrir";
            this.barButtonItemOpen.Enabled = false;
            this.barButtonItemOpen.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemOpen.Glyph")));
            this.barButtonItemOpen.Id = 261;
            this.barButtonItemOpen.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemOpen.Name = "barButtonItemOpen";
            this.barButtonItemOpen.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemOpen.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemOpen_ItemClick);
            // 
            // RibbonPageOperation
            // 
            this.RibbonPageOperation.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupAssignment});
            this.RibbonPageOperation.Name = "RibbonPageOperation";
            this.RibbonPageOperation.Text = "Operacion";
            // 
            // ribbonPageGroupAssignment
            // 
            this.ribbonPageGroupAssignment.AllowMinimize = false;
            this.ribbonPageGroupAssignment.AllowTextClipping = false;
            this.ribbonPageGroupAssignment.ItemLinks.Add(this.barButtonItemOpen);
            this.ribbonPageGroupAssignment.ItemLinks.Add(this.barButtonItemAssign);
            this.ribbonPageGroupAssignment.ItemLinks.Add(this.barButtonItemRemove);
            this.ribbonPageGroupAssignment.Kind = DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroupKind.Print;
            this.ribbonPageGroupAssignment.Name = "ribbonPageGroupAssignment";
            this.ribbonPageGroupAssignment.ShowCaptionButton = false;
            this.ribbonPageGroupAssignment.Text = "Asignacion";
            // 
            // radioButtonSupervised
            // 
            this.radioButtonSupervised.Location = new System.Drawing.Point(270, 30);
            this.radioButtonSupervised.Name = "radioButtonSupervised";
            this.radioButtonSupervised.Size = new System.Drawing.Size(91, 20);
            this.radioButtonSupervised.TabIndex = 11;
            this.radioButtonSupervised.TabStop = true;
            this.radioButtonSupervised.Text = "Supervisados";
            this.radioButtonSupervised.UseVisualStyleBackColor = true;
            this.radioButtonSupervised.CheckedChanged += new System.EventHandler(this.radioButtonFilterOperators_CheckedChanged);
            // 
            // radioButtonPartially
            // 
            this.radioButtonPartially.Location = new System.Drawing.Point(115, 30);
            this.radioButtonPartially.Name = "radioButtonPartially";
            this.radioButtonPartially.Size = new System.Drawing.Size(153, 20);
            this.radioButtonPartially.TabIndex = 10;
            this.radioButtonPartially.TabStop = true;
            this.radioButtonPartially.Text = "Parcialmente supervisados";
            this.radioButtonPartially.UseVisualStyleBackColor = true;
            this.radioButtonPartially.CheckedChanged += new System.EventHandler(this.radioButtonFilterOperators_CheckedChanged);
            // 
            // radioButtonNotSupervised
            // 
            this.radioButtonNotSupervised.Location = new System.Drawing.Point(8, 30);
            this.radioButtonNotSupervised.Name = "radioButtonNotSupervised";
            this.radioButtonNotSupervised.Size = new System.Drawing.Size(105, 20);
            this.radioButtonNotSupervised.TabIndex = 9;
            this.radioButtonNotSupervised.TabStop = true;
            this.radioButtonNotSupervised.Text = "No supervisados";
            this.radioButtonNotSupervised.UseVisualStyleBackColor = true;
            this.radioButtonNotSupervised.CheckedChanged += new System.EventHandler(this.radioButtonFilterOperators_CheckedChanged);
            // 
            // radioButtonAll
            // 
            this.radioButtonAll.Location = new System.Drawing.Point(363, 30);
            this.radioButtonAll.Name = "radioButtonAll";
            this.radioButtonAll.Size = new System.Drawing.Size(59, 20);
            this.radioButtonAll.TabIndex = 8;
            this.radioButtonAll.TabStop = true;
            this.radioButtonAll.Text = "Todos";
            this.radioButtonAll.UseVisualStyleBackColor = true;
            this.radioButtonAll.CheckedChanged += new System.EventHandler(this.radioButtonFilterOperators_CheckedChanged);
            // 
            // gridControlExAssignedOperators
            // 
            this.gridControlExAssignedOperators.EnableAutoFilter = true;
            this.gridControlExAssignedOperators.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExAssignedOperators.Location = new System.Drawing.Point(7, 508);
            this.gridControlExAssignedOperators.MainView = this.gridViewExAssignedOperators;
            this.gridControlExAssignedOperators.Name = "gridControlExAssignedOperators";
            this.gridControlExAssignedOperators.Size = new System.Drawing.Size(445, 238);
            this.gridControlExAssignedOperators.TabIndex = 7;
            this.gridControlExAssignedOperators.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExAssignedOperators});
            this.gridControlExAssignedOperators.ViewTotalRows = true;
            // 
            // gridViewExAssignedOperators
            // 
            this.gridViewExAssignedOperators.AllowFocusedRowChanged = true;
            this.gridViewExAssignedOperators.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewExAssignedOperators.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExAssignedOperators.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewExAssignedOperators.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewExAssignedOperators.Appearance.FooterPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewExAssignedOperators.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewExAssignedOperators.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewExAssignedOperators.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExAssignedOperators.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewExAssignedOperators.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewExAssignedOperators.EnablePreviewLineForFocusedRow = false;
            this.gridViewExAssignedOperators.GridControl = this.gridControlExAssignedOperators;
            this.gridViewExAssignedOperators.Name = "gridViewExAssignedOperators";
            this.gridViewExAssignedOperators.OptionsMenu.EnableFooterMenu = false;
            this.gridViewExAssignedOperators.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExAssignedOperators.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewExAssignedOperators.OptionsView.ShowFooter = true;
            this.gridViewExAssignedOperators.OptionsView.ShowGroupPanel = false;
            this.gridViewExAssignedOperators.ViewTotalRows = true;
            // 
            // gridControlExSupervisors
            // 
            this.gridControlExSupervisors.EnableAutoFilter = true;
            this.gridControlExSupervisors.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExSupervisors.Location = new System.Drawing.Point(7, 267);
            this.gridControlExSupervisors.MainView = this.gridViewExSupervisors;
            this.gridControlExSupervisors.Name = "gridControlExSupervisors";
            this.gridControlExSupervisors.Size = new System.Drawing.Size(445, 207);
            this.gridControlExSupervisors.TabIndex = 6;
            this.gridControlExSupervisors.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExSupervisors});
            this.gridControlExSupervisors.ViewTotalRows = true;
            // 
            // gridViewExSupervisors
            // 
            this.gridViewExSupervisors.AllowFocusedRowChanged = true;
            this.gridViewExSupervisors.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewExSupervisors.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExSupervisors.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewExSupervisors.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewExSupervisors.Appearance.FooterPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewExSupervisors.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewExSupervisors.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewExSupervisors.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExSupervisors.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewExSupervisors.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewExSupervisors.EnablePreviewLineForFocusedRow = false;
            this.gridViewExSupervisors.GridControl = this.gridControlExSupervisors;
            this.gridViewExSupervisors.Name = "gridViewExSupervisors";
            this.gridViewExSupervisors.OptionsDetail.EnableMasterViewMode = false;
            this.gridViewExSupervisors.OptionsMenu.EnableFooterMenu = false;
            this.gridViewExSupervisors.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExSupervisors.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewExSupervisors.OptionsView.ShowFooter = true;
            this.gridViewExSupervisors.OptionsView.ShowGroupPanel = false;
            this.gridViewExSupervisors.ViewTotalRows = true;
            this.gridViewExSupervisors.SelectionWillChange += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewExSupervisors_SelectionWillChange);
            // 
            // gridControlExOperators
            // 
            this.gridControlExOperators.EnableAutoFilter = true;
            this.gridControlExOperators.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExOperators.Location = new System.Drawing.Point(7, 57);
            this.gridControlExOperators.MainView = this.gridViewExOperators;
            this.gridControlExOperators.Name = "gridControlExOperators";
            this.gridControlExOperators.Size = new System.Drawing.Size(445, 176);
            this.gridControlExOperators.TabIndex = 5;
            this.gridControlExOperators.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExOperators,
            this.gridViewEx2});
            this.gridControlExOperators.ViewTotalRows = true;
            // 
            // gridViewExOperators
            // 
            this.gridViewExOperators.AllowFocusedRowChanged = true;
            this.gridViewExOperators.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewExOperators.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExOperators.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewExOperators.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewExOperators.Appearance.FooterPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewExOperators.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewExOperators.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewExOperators.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExOperators.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewExOperators.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewExOperators.EnablePreviewLineForFocusedRow = false;
            this.gridViewExOperators.GridControl = this.gridControlExOperators;
            this.gridViewExOperators.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.None, "", null, "")});
            this.gridViewExOperators.Name = "gridViewExOperators";
            this.gridViewExOperators.OptionsMenu.EnableFooterMenu = false;
            this.gridViewExOperators.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExOperators.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewExOperators.OptionsView.ShowFooter = true;
            this.gridViewExOperators.OptionsView.ShowGroupPanel = false;
            this.gridViewExOperators.ViewTotalRows = true;
            this.gridViewExOperators.SelectionWillChange += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewExOperators_SelectionWillChange);
            // 
            // gridViewEx2
            // 
            this.gridViewEx2.AllowFocusedRowChanged = true;
            this.gridViewEx2.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewEx2.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewEx2.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewEx2.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewEx2.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewEx2.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewEx2.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewEx2.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewEx2.EnablePreviewLineForFocusedRow = false;
            this.gridViewEx2.GridControl = this.gridControlExOperators;
            this.gridViewEx2.Name = "gridViewEx2";
            this.gridViewEx2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewEx2.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewEx2.ViewTotalRows = false;
            // 
            // schedulerControl
            // 
            this.schedulerControl.EnabledAssign = false;
            this.schedulerControl.EnabledRemove = false;
            this.schedulerControl.IsOperatorSelected = false;
            this.schedulerControl.IsOperatorView = true;
            this.schedulerControl.Location = new System.Drawing.Point(471, 27);
            this.schedulerControl.Name = "schedulerControl";
            this.schedulerControl.Size = new System.Drawing.Size(784, 719);
            this.schedulerControl.TabIndex = 4;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.splitterItem1,
            this.layoutControlGroupOperators,
            this.layoutControlGroupSupervisors,
            this.layoutControlGroupAssignedOperators,
            this.layoutControlGroupScheduler});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(1262, 753);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            this.splitterItem1.CustomizationFormText = "splitterItem1";
            this.splitterItem1.Location = new System.Drawing.Point(459, 0);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(5, 753);
            // 
            // layoutControlGroupOperators
            // 
            this.layoutControlGroupOperators.CustomizationFormText = "layoutControlGroupOperators";
            this.layoutControlGroupOperators.ExpandButtonMode = DevExpress.Utils.Controls.ExpandButtonMode.Inverted;
            this.layoutControlGroupOperators.ExpandButtonVisible = true;
            this.layoutControlGroupOperators.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.layoutControlItem4,
            this.layoutControlItem3,
            this.layoutControlItem2,
            this.layoutControlItemOperators,
            this.layoutControlItem1});
            this.layoutControlGroupOperators.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupOperators.Name = "layoutControlGroupOperators";
            this.layoutControlGroupOperators.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupOperators.Size = new System.Drawing.Size(459, 240);
            this.layoutControlGroupOperators.Text = "layoutControlGroupOperators";
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(418, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(31, 30);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.radioButtonSupervised;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(264, 0);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(93, 30);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(93, 30);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 5, 5);
            this.layoutControlItem4.Size = new System.Drawing.Size(93, 30);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.radioButtonPartially;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(109, 0);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(155, 30);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(155, 30);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 5, 5);
            this.layoutControlItem3.Size = new System.Drawing.Size(155, 30);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.radioButtonNotSupervised;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(109, 30);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(109, 30);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 1, 5, 5);
            this.layoutControlItem2.Size = new System.Drawing.Size(109, 30);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItemOperators
            // 
            this.layoutControlItemOperators.Control = this.gridControlExOperators;
            this.layoutControlItemOperators.CustomizationFormText = "layoutControlItemOperators";
            this.layoutControlItemOperators.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItemOperators.Name = "layoutControlItemOperators";
            this.layoutControlItemOperators.Size = new System.Drawing.Size(449, 180);
            this.layoutControlItemOperators.Text = "layoutControlItemOperators";
            this.layoutControlItemOperators.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemOperators.TextToControlDistance = 0;
            this.layoutControlItemOperators.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.radioButtonAll;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(357, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(61, 30);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(61, 30);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 5, 5);
            this.layoutControlItem1.Size = new System.Drawing.Size(61, 30);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlGroupSupervisors
            // 
            this.layoutControlGroupSupervisors.CustomizationFormText = "layoutControlGroupSupervisors";
            this.layoutControlGroupSupervisors.ExpandButtonMode = DevExpress.Utils.Controls.ExpandButtonMode.Inverted;
            this.layoutControlGroupSupervisors.ExpandButtonVisible = true;
            this.layoutControlGroupSupervisors.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemSupervisors});
            this.layoutControlGroupSupervisors.Location = new System.Drawing.Point(0, 240);
            this.layoutControlGroupSupervisors.Name = "layoutControlGroupSupervisors";
            this.layoutControlGroupSupervisors.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupSupervisors.Size = new System.Drawing.Size(459, 241);
            this.layoutControlGroupSupervisors.Text = "layoutControlGroupSupervisors";
            // 
            // layoutControlItemSupervisors
            // 
            this.layoutControlItemSupervisors.Control = this.gridControlExSupervisors;
            this.layoutControlItemSupervisors.CustomizationFormText = "layoutControlItemSupervisors";
            this.layoutControlItemSupervisors.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemSupervisors.Name = "layoutControlItemSupervisors";
            this.layoutControlItemSupervisors.Size = new System.Drawing.Size(449, 211);
            this.layoutControlItemSupervisors.Text = "layoutControlItemSupervisors";
            this.layoutControlItemSupervisors.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemSupervisors.TextToControlDistance = 0;
            this.layoutControlItemSupervisors.TextVisible = false;
            // 
            // layoutControlGroupAssignedOperators
            // 
            this.layoutControlGroupAssignedOperators.CustomizationFormText = "layoutControlGroupAssignedOperators";
            this.layoutControlGroupAssignedOperators.ExpandButtonMode = DevExpress.Utils.Controls.ExpandButtonMode.Inverted;
            this.layoutControlGroupAssignedOperators.ExpandButtonVisible = true;
            this.layoutControlGroupAssignedOperators.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemAssignedOperators});
            this.layoutControlGroupAssignedOperators.Location = new System.Drawing.Point(0, 481);
            this.layoutControlGroupAssignedOperators.Name = "layoutControlGroupAssignedOperators";
            this.layoutControlGroupAssignedOperators.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupAssignedOperators.Size = new System.Drawing.Size(459, 272);
            this.layoutControlGroupAssignedOperators.Text = "layoutControlGroupAssignedOperators";
            // 
            // layoutControlItemAssignedOperators
            // 
            this.layoutControlItemAssignedOperators.Control = this.gridControlExAssignedOperators;
            this.layoutControlItemAssignedOperators.CustomizationFormText = "layoutControlItemAssignedOperators";
            this.layoutControlItemAssignedOperators.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemAssignedOperators.Name = "layoutControlItemAssignedOperators";
            this.layoutControlItemAssignedOperators.Size = new System.Drawing.Size(449, 242);
            this.layoutControlItemAssignedOperators.Text = "layoutControlItemAssignedOperators";
            this.layoutControlItemAssignedOperators.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemAssignedOperators.TextToControlDistance = 0;
            this.layoutControlItemAssignedOperators.TextVisible = false;
            // 
            // layoutControlGroupScheduler
            // 
            this.layoutControlGroupScheduler.CustomizationFormText = "layoutControlGroupScheduler";
            this.layoutControlGroupScheduler.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemScheduler});
            this.layoutControlGroupScheduler.Location = new System.Drawing.Point(464, 0);
            this.layoutControlGroupScheduler.Name = "layoutControlGroupScheduler";
            this.layoutControlGroupScheduler.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupScheduler.Size = new System.Drawing.Size(798, 753);
            this.layoutControlGroupScheduler.Text = "layoutControlGroupScheduler";
            // 
            // layoutControlItemScheduler
            // 
            this.layoutControlItemScheduler.Control = this.schedulerControl;
            this.layoutControlItemScheduler.CustomizationFormText = "layoutControlItemScheduler";
            this.layoutControlItemScheduler.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemScheduler.Name = "layoutControlItemScheduler";
            this.layoutControlItemScheduler.Size = new System.Drawing.Size(788, 723);
            this.layoutControlItemScheduler.Text = "layoutControlItemScheduler";
            this.layoutControlItemScheduler.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemScheduler.TextToControlDistance = 0;
            this.layoutControlItemScheduler.TextVisible = false;
            // 
            // OperatorAssignForm
            // 
            this.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1262, 753);
            this.Controls.Add(this.RibbonControl);
            this.Controls.Add(this.layoutControlOperatorAssign);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "OperatorAssignForm";
            this.Text = "OperatorAssignForm";
            this.Activated += new System.EventHandler(this.OperatorAssignForm_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OperatorAssignForm_FormClosing);
            this.Load += new System.EventHandler(this.OperatorAssignForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlOperatorAssign)).EndInit();
            this.layoutControlOperatorAssign.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RibbonControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerStorage1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExAssignedOperators)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExAssignedOperators)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExSupervisors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExSupervisors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExOperators)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExOperators)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEx2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupOperators)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemOperators)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupSupervisors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemSupervisors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupAssignedOperators)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemAssignedOperators)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupScheduler)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemScheduler)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControlOperatorAssign;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        public OperatorAssignSchedulerControl schedulerControl;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemScheduler;
        private GridControlEx gridControlExAssignedOperators;
        private GridViewEx gridViewExAssignedOperators;
        private GridControlEx gridControlExSupervisors;
        private GridViewEx gridViewExSupervisors;
        private GridControlEx gridControlExOperators;
        private GridViewEx gridViewExOperators;
        private GridViewEx gridViewEx2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemOperators;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemSupervisors;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemAssignedOperators;
        private DevExpress.XtraLayout.SplitterItem splitterItem1;
        internal DevExpress.XtraBars.Ribbon.RibbonControl RibbonControl;
        private System.Windows.Forms.ImageList imageList2;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewBarItem barButtonItemRemove;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemAssign;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemSend;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemSave;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemPrint;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemUpdate;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItem2;
        private DevExpress.XtraBars.BarStaticItem barStaticItem3;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPage RibbonPageOperation;
        internal DevExpress.XtraPrinting.Preview.PrintPreviewRibbonPageGroup ribbonPageGroupAssignment;
        private DevExpress.XtraBars.BarButtonItem barButtonItemOpen;
        private System.Windows.Forms.RadioButton radioButtonNotSupervised;
        private System.Windows.Forms.RadioButton radioButtonAll;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private System.Windows.Forms.RadioButton radioButtonSupervised;
        private System.Windows.Forms.RadioButton radioButtonPartially;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupOperators;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupSupervisors;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupAssignedOperators;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupScheduler;
        private DevExpress.XtraScheduler.SchedulerStorage schedulerStorage1;
    }
}