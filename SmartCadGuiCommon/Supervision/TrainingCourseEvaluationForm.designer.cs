using SmartCadControls;
using SmartCadControls.Controls;
namespace SmartCadGuiCommon
{
    partial class TrainingCourseEvaluationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            DatagGridDefaultGroup datagGridDefaultGroup1 = new DatagGridDefaultGroup();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TrainingCourseEvaluationForm));
            this.buttonExCancel = new SimpleButtonEx();
            this.buttonExOk = new SimpleButtonEx();
            this.groupBoxEvaluation = new DevExpress.XtraEditors.GroupControl();
            this.textBoxExTotal = new TextBoxEx();
            this.textBoxExPending = new TextBoxEx();
            this.labelExPending = new LabelEx();
            this.labelExTotal = new LabelEx();
            this.dataGridExEvaluation = new DataGridEx();
            this.toolStripEvaluation = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonSearch = new System.Windows.Forms.ToolStripButton();
            this.toolStripTextBoxSearch = new System.Windows.Forms.ToolStripTextBox();
            this.groupBoxEvaluationCriteria = new DevExpress.XtraEditors.GroupControl();
            this.textBoxExBase = new TextBoxEx();
            this.textBoxExTotalAtt = new TextBoxEx();
            this.textBoxExMinApp = new TextBoxEx();
            this.textBoxExMinAtt = new TextBoxEx();
            this.labelExMinApp = new LabelEx();
            this.labelExTotalAtt = new LabelEx();
            this.labelExQualification = new LabelEx();
            this.labelExMinAtt = new LabelEx();
            this.groupBoxCourseData = new DevExpress.XtraEditors.GroupControl();
            this.textBoxCourseName = new System.Windows.Forms.TextBox();
            this.labelCourseName = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.groupBoxEvaluation)).BeginInit();
            this.groupBoxEvaluation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridExEvaluation)).BeginInit();
            this.toolStripEvaluation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupBoxEvaluationCriteria)).BeginInit();
            this.groupBoxEvaluationCriteria.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupBoxCourseData)).BeginInit();
            this.groupBoxCourseData.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonExCancel
            // 
            this.buttonExCancel.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExCancel.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonExCancel.Appearance.Options.UseFont = true;
            this.buttonExCancel.Appearance.Options.UseForeColor = true;
            this.buttonExCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonExCancel.FontFormat = FontFormat.GetFormat("ButtonFormat");
            this.buttonExCancel.Location = new System.Drawing.Point(297, 455);
            this.buttonExCancel.Name = "buttonExCancel";
            this.buttonExCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonExCancel.TabIndex = 7;
            this.buttonExCancel.Text = "Cancelar";
            // 
            // buttonExOk
            // 
            this.buttonExOk.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExOk.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonExOk.Appearance.Options.UseFont = true;
            this.buttonExOk.Appearance.Options.UseForeColor = true;
            this.buttonExOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonExOk.FontFormat = FontFormat.GetFormat("ButtonFormat");
            this.buttonExOk.Location = new System.Drawing.Point(216, 455);
            this.buttonExOk.Name = "buttonExOk";
            this.buttonExOk.Size = new System.Drawing.Size(75, 23);
            this.buttonExOk.TabIndex = 6;
            this.buttonExOk.Text = "Accept";
            this.buttonExOk.Click += new System.EventHandler(this.buttonExOk_Click);
            // 
            // groupBoxEvaluation
            // 
            this.groupBoxEvaluation.Controls.Add(this.textBoxExTotal);
            this.groupBoxEvaluation.Controls.Add(this.textBoxExPending);
            this.groupBoxEvaluation.Controls.Add(this.labelExPending);
            this.groupBoxEvaluation.Controls.Add(this.labelExTotal);
            this.groupBoxEvaluation.Controls.Add(this.dataGridExEvaluation);
            this.groupBoxEvaluation.Controls.Add(this.toolStripEvaluation);
            this.groupBoxEvaluation.Location = new System.Drawing.Point(6, 149);
            this.groupBoxEvaluation.Name = "groupBoxEvaluation";
            this.groupBoxEvaluation.Size = new System.Drawing.Size(371, 300);
            this.groupBoxEvaluation.TabIndex = 5;
            this.groupBoxEvaluation.Text = "Evaluacion";
            // 
            // textBoxExTotal
            // 
            this.textBoxExTotal.AllowsLetters = true;
            this.textBoxExTotal.AllowsNumbers = true;
            this.textBoxExTotal.AllowsPunctuation = true;
            this.textBoxExTotal.AllowsSeparators = true;
            this.textBoxExTotal.AllowsSymbols = true;
            this.textBoxExTotal.AllowsWhiteSpaces = true;
            this.textBoxExTotal.Enabled = false;
            this.textBoxExTotal.ExtraAllowedChars = "";
            this.textBoxExTotal.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExTotal.Location = new System.Drawing.Point(196, 239);
            this.textBoxExTotal.Name = "textBoxExTotal";
            this.textBoxExTotal.NonAllowedCharacters = "";
            this.textBoxExTotal.RegularExpresion = "";
            this.textBoxExTotal.Size = new System.Drawing.Size(50, 20);
            this.textBoxExTotal.TabIndex = 9;
            // 
            // textBoxExPending
            // 
            this.textBoxExPending.AllowsLetters = true;
            this.textBoxExPending.AllowsNumbers = true;
            this.textBoxExPending.AllowsPunctuation = true;
            this.textBoxExPending.AllowsSeparators = true;
            this.textBoxExPending.AllowsSymbols = true;
            this.textBoxExPending.AllowsWhiteSpaces = true;
            this.textBoxExPending.Enabled = false;
            this.textBoxExPending.ExtraAllowedChars = "";
            this.textBoxExPending.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExPending.Location = new System.Drawing.Point(196, 269);
            this.textBoxExPending.Name = "textBoxExPending";
            this.textBoxExPending.NonAllowedCharacters = "";
            this.textBoxExPending.RegularExpresion = "";
            this.textBoxExPending.Size = new System.Drawing.Size(50, 20);
            this.textBoxExPending.TabIndex = 8;
            // 
            // labelExPending
            // 
            this.labelExPending.AutoSize = true;
            this.labelExPending.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExPending.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExPending.Location = new System.Drawing.Point(6, 272);
            this.labelExPending.Name = "labelExPending";
            this.labelExPending.Size = new System.Drawing.Size(176, 13);
            this.labelExPending.TabIndex = 4;
            this.labelExPending.Text = "Operadores pendientes por evaluar:";
            // 
            // labelExTotal
            // 
            this.labelExTotal.AutoSize = true;
            this.labelExTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExTotal.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExTotal.Location = new System.Drawing.Point(6, 242);
            this.labelExTotal.Name = "labelExTotal";
            this.labelExTotal.Size = new System.Drawing.Size(117, 13);
            this.labelExTotal.TabIndex = 3;
            this.labelExTotal.Text = "Operadores evaluados:";
            // 
            // dataGridExEvaluation
            // 
            this.dataGridExEvaluation.AllowDrop = true;
            this.dataGridExEvaluation.AllowEditing = false;
            this.dataGridExEvaluation.AllowUserToAddRows = false;
            this.dataGridExEvaluation.AllowUserToDeleteRows = false;
            this.dataGridExEvaluation.AllowUserToOrderColumns = true;
            this.dataGridExEvaluation.AllowUserToResizeRows = false;
            this.dataGridExEvaluation.AllowUserToSortColumns = true;
            this.dataGridExEvaluation.BackgroundColor = System.Drawing.Color.White;
            this.dataGridExEvaluation.ColumnHeadersBackColor = System.Drawing.Color.Empty;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.ButtonFace;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Tahoma", 8.25F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridExEvaluation.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridExEvaluation.ColumnHeadersHeight = 25;
            this.dataGridExEvaluation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridExEvaluation.Editing = false;
            this.dataGridExEvaluation.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dataGridExEvaluation.EnabledSelectionHandler = true;
            this.dataGridExEvaluation.EnableHeadersVisualStyles = false;
            this.dataGridExEvaluation.EnableSystemShortCuts = false;
            this.dataGridExEvaluation.Grouping = false;
            datagGridDefaultGroup1.Collapsed = false;
            datagGridDefaultGroup1.Column = null;
            datagGridDefaultGroup1.Height = 34;
            datagGridDefaultGroup1.ItemCount = 0;
            datagGridDefaultGroup1.Text = "";
            datagGridDefaultGroup1.Value = null;
            this.dataGridExEvaluation.GroupTemplate = datagGridDefaultGroup1;
            this.dataGridExEvaluation.Location = new System.Drawing.Point(3, 45);
            this.dataGridExEvaluation.MultiSelect = false;
            this.dataGridExEvaluation.Name = "dataGridExEvaluation";
            this.dataGridExEvaluation.SelectedItemBackColor = System.Drawing.Color.Empty;
            this.dataGridExEvaluation.SelectedItemForeColor = System.Drawing.Color.Empty;
            this.dataGridExEvaluation.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridExEvaluation.Size = new System.Drawing.Size(365, 186);
            this.dataGridExEvaluation.SortedColumnColor = System.Drawing.Color.Empty;
            this.dataGridExEvaluation.TabIndex = 1;
            this.dataGridExEvaluation.Type = null;
            this.dataGridExEvaluation.VirtualMode = true;
            this.dataGridExEvaluation.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridExEvaluation_CellValueChanged);
            this.dataGridExEvaluation.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dataGridExEvaluation_EditingControlShowing);
            // 
            // toolStripEvaluation
            // 
            this.toolStripEvaluation.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonSearch,
            this.toolStripTextBoxSearch});
            this.toolStripEvaluation.Location = new System.Drawing.Point(2, 20);
            this.toolStripEvaluation.Name = "toolStripEvaluation";
            this.toolStripEvaluation.Size = new System.Drawing.Size(367, 25);
            this.toolStripEvaluation.TabIndex = 0;
            this.toolStripEvaluation.Text = "toolStrip1";
            // 
            // toolStripButtonSearch
            // 
            this.toolStripButtonSearch.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButtonSearch.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonSearch.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonSearch.Image")));
            this.toolStripButtonSearch.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonSearch.Name = "toolStripButtonSearch";
            this.toolStripButtonSearch.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonSearch.Click += new System.EventHandler(this.toolStripButtonSearch_Click);
            // 
            // toolStripTextBoxSearch
            // 
            this.toolStripTextBoxSearch.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripTextBoxSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.toolStripTextBoxSearch.Name = "toolStripTextBoxSearch";
            this.toolStripTextBoxSearch.Size = new System.Drawing.Size(100, 25);
            // 
            // groupBoxEvaluationCriteria
            // 
            this.groupBoxEvaluationCriteria.Controls.Add(this.textBoxExBase);
            this.groupBoxEvaluationCriteria.Controls.Add(this.textBoxExTotalAtt);
            this.groupBoxEvaluationCriteria.Controls.Add(this.textBoxExMinApp);
            this.groupBoxEvaluationCriteria.Controls.Add(this.textBoxExMinAtt);
            this.groupBoxEvaluationCriteria.Controls.Add(this.labelExMinApp);
            this.groupBoxEvaluationCriteria.Controls.Add(this.labelExTotalAtt);
            this.groupBoxEvaluationCriteria.Controls.Add(this.labelExQualification);
            this.groupBoxEvaluationCriteria.Controls.Add(this.labelExMinAtt);
            this.groupBoxEvaluationCriteria.Location = new System.Drawing.Point(6, 62);
            this.groupBoxEvaluationCriteria.Name = "groupBoxEvaluationCriteria";
            this.groupBoxEvaluationCriteria.Size = new System.Drawing.Size(371, 81);
            this.groupBoxEvaluationCriteria.TabIndex = 4;
            this.groupBoxEvaluationCriteria.Text = "Criterios de evaluacion";
            // 
            // textBoxExBase
            // 
            this.textBoxExBase.AllowsLetters = true;
            this.textBoxExBase.AllowsNumbers = true;
            this.textBoxExBase.AllowsPunctuation = true;
            this.textBoxExBase.AllowsSeparators = true;
            this.textBoxExBase.AllowsSymbols = true;
            this.textBoxExBase.AllowsWhiteSpaces = true;
            this.textBoxExBase.Enabled = false;
            this.textBoxExBase.ExtraAllowedChars = "";
            this.textBoxExBase.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExBase.Location = new System.Drawing.Point(117, 25);
            this.textBoxExBase.Name = "textBoxExBase";
            this.textBoxExBase.NonAllowedCharacters = "";
            this.textBoxExBase.RegularExpresion = "";
            this.textBoxExBase.Size = new System.Drawing.Size(50, 20);
            this.textBoxExBase.TabIndex = 7;
            // 
            // textBoxExTotalAtt
            // 
            this.textBoxExTotalAtt.AllowsLetters = true;
            this.textBoxExTotalAtt.AllowsNumbers = true;
            this.textBoxExTotalAtt.AllowsPunctuation = true;
            this.textBoxExTotalAtt.AllowsSeparators = true;
            this.textBoxExTotalAtt.AllowsSymbols = true;
            this.textBoxExTotalAtt.AllowsWhiteSpaces = true;
            this.textBoxExTotalAtt.Enabled = false;
            this.textBoxExTotalAtt.ExtraAllowedChars = "";
            this.textBoxExTotalAtt.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExTotalAtt.Location = new System.Drawing.Point(117, 56);
            this.textBoxExTotalAtt.Name = "textBoxExTotalAtt";
            this.textBoxExTotalAtt.NonAllowedCharacters = "";
            this.textBoxExTotalAtt.RegularExpresion = "";
            this.textBoxExTotalAtt.Size = new System.Drawing.Size(50, 20);
            this.textBoxExTotalAtt.TabIndex = 6;
            // 
            // textBoxExMinApp
            // 
            this.textBoxExMinApp.AllowsLetters = true;
            this.textBoxExMinApp.AllowsNumbers = true;
            this.textBoxExMinApp.AllowsPunctuation = true;
            this.textBoxExMinApp.AllowsSeparators = true;
            this.textBoxExMinApp.AllowsSymbols = true;
            this.textBoxExMinApp.AllowsWhiteSpaces = true;
            this.textBoxExMinApp.Enabled = false;
            this.textBoxExMinApp.ExtraAllowedChars = "";
            this.textBoxExMinApp.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExMinApp.Location = new System.Drawing.Point(300, 25);
            this.textBoxExMinApp.Name = "textBoxExMinApp";
            this.textBoxExMinApp.NonAllowedCharacters = "";
            this.textBoxExMinApp.RegularExpresion = "";
            this.textBoxExMinApp.Size = new System.Drawing.Size(50, 20);
            this.textBoxExMinApp.TabIndex = 5;
            // 
            // textBoxExMinAtt
            // 
            this.textBoxExMinAtt.AllowsLetters = true;
            this.textBoxExMinAtt.AllowsNumbers = true;
            this.textBoxExMinAtt.AllowsPunctuation = true;
            this.textBoxExMinAtt.AllowsSeparators = true;
            this.textBoxExMinAtt.AllowsSymbols = true;
            this.textBoxExMinAtt.AllowsWhiteSpaces = true;
            this.textBoxExMinAtt.Enabled = false;
            this.textBoxExMinAtt.ExtraAllowedChars = "";
            this.textBoxExMinAtt.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExMinAtt.Location = new System.Drawing.Point(300, 56);
            this.textBoxExMinAtt.Name = "textBoxExMinAtt";
            this.textBoxExMinAtt.NonAllowedCharacters = "";
            this.textBoxExMinAtt.RegularExpresion = "";
            this.textBoxExMinAtt.Size = new System.Drawing.Size(50, 20);
            this.textBoxExMinAtt.TabIndex = 4;
            // 
            // labelExMinApp
            // 
            this.labelExMinApp.AutoSize = true;
            this.labelExMinApp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExMinApp.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExMinApp.Location = new System.Drawing.Point(193, 28);
            this.labelExMinApp.Name = "labelExMinApp";
            this.labelExMinApp.Size = new System.Drawing.Size(101, 13);
            this.labelExMinApp.TabIndex = 3;
            this.labelExMinApp.Text = "Minimo aprobatorio:";
            // 
            // labelExTotalAtt
            // 
            this.labelExTotalAtt.AutoSize = true;
            this.labelExTotalAtt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExTotalAtt.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExTotalAtt.Location = new System.Drawing.Point(6, 59);
            this.labelExTotalAtt.Name = "labelExTotalAtt";
            this.labelExTotalAtt.Size = new System.Drawing.Size(104, 13);
            this.labelExTotalAtt.TabIndex = 2;
            this.labelExTotalAtt.Text = "Total de asistencias:";
            // 
            // labelExQualification
            // 
            this.labelExQualification.AutoSize = true;
            this.labelExQualification.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExQualification.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExQualification.Location = new System.Drawing.Point(6, 28);
            this.labelExQualification.Name = "labelExQualification";
            this.labelExQualification.Size = new System.Drawing.Size(105, 13);
            this.labelExQualification.TabIndex = 1;
            this.labelExQualification.Text = "Base de calificacion:";
            // 
            // labelExMinAtt
            // 
            this.labelExMinAtt.AutoSize = true;
            this.labelExMinAtt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExMinAtt.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExMinAtt.Location = new System.Drawing.Point(193, 59);
            this.labelExMinAtt.Name = "labelExMinAtt";
            this.labelExMinAtt.Size = new System.Drawing.Size(95, 13);
            this.labelExMinAtt.TabIndex = 0;
            this.labelExMinAtt.Text = "Asistencia minima:";
            // 
            // groupBoxCourseData
            // 
            this.groupBoxCourseData.Controls.Add(this.textBoxCourseName);
            this.groupBoxCourseData.Controls.Add(this.labelCourseName);
            this.groupBoxCourseData.Location = new System.Drawing.Point(6, 5);
            this.groupBoxCourseData.Name = "groupBoxCourseData";
            this.groupBoxCourseData.Size = new System.Drawing.Size(371, 51);
            this.groupBoxCourseData.TabIndex = 8;
            this.groupBoxCourseData.Text = "Datos del curso";
            // 
            // textBoxCourseName
            // 
            this.textBoxCourseName.Location = new System.Drawing.Point(78, 24);
            this.textBoxCourseName.Name = "textBoxCourseName";
            this.textBoxCourseName.ReadOnly = true;
            this.textBoxCourseName.Size = new System.Drawing.Size(272, 21);
            this.textBoxCourseName.TabIndex = 1;
            // 
            // labelCourseName
            // 
            this.labelCourseName.AutoSize = true;
            this.labelCourseName.Location = new System.Drawing.Point(6, 24);
            this.labelCourseName.Name = "labelCourseName";
            this.labelCourseName.Size = new System.Drawing.Size(51, 13);
            this.labelCourseName.TabIndex = 0;
            this.labelCourseName.Text = "Nombre :";
            // 
            // TrainingCourseEvaluationForm
            // 
            this.AcceptButton = this.buttonExOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonExCancel;
            this.ClientSize = new System.Drawing.Size(384, 485);
            this.Controls.Add(this.groupBoxCourseData);
            this.Controls.Add(this.buttonExCancel);
            this.Controls.Add(this.buttonExOk);
            this.Controls.Add(this.groupBoxEvaluation);
            this.Controls.Add(this.groupBoxEvaluationCriteria);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TrainingCourseEvaluationForm";
            this.Text = "Evaluacion del curso de entrenamiento";
            this.Load += new System.EventHandler(this.TrainingCourseEvaluationForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupBoxEvaluation)).EndInit();
            this.groupBoxEvaluation.ResumeLayout(false);
            this.groupBoxEvaluation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridExEvaluation)).EndInit();
            this.toolStripEvaluation.ResumeLayout(false);
            this.toolStripEvaluation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupBoxEvaluationCriteria)).EndInit();
            this.groupBoxEvaluationCriteria.ResumeLayout(false);
            this.groupBoxEvaluationCriteria.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupBoxCourseData)).EndInit();
            this.groupBoxCourseData.ResumeLayout(false);
            this.groupBoxCourseData.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private SimpleButtonEx buttonExCancel;
        private SimpleButtonEx buttonExOk;
        private DevExpress.XtraEditors.GroupControl groupBoxEvaluation;
        private DataGridEx dataGridExEvaluation;
        private System.Windows.Forms.ToolStrip toolStripEvaluation;
        private TextBoxEx textBoxExBase;
        private TextBoxEx textBoxExTotalAtt;
        private TextBoxEx textBoxExMinApp;
        private TextBoxEx textBoxExMinAtt;
        private LabelEx labelExMinApp;
        private LabelEx labelExTotalAtt;
        private LabelEx labelExQualification;
        private LabelEx labelExMinAtt;
        private LabelEx labelExPending;
        private LabelEx labelExTotal;
        private TextBoxEx textBoxExTotal;
        private TextBoxEx textBoxExPending;
        private System.Windows.Forms.ToolStripButton toolStripButtonSearch;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBoxSearch;
        private DevExpress.XtraEditors.GroupControl groupBoxCourseData;
        private System.Windows.Forms.TextBox textBoxCourseName;
        private System.Windows.Forms.Label labelCourseName;
        private DevExpress.XtraEditors.GroupControl groupBoxEvaluationCriteria;

    }
}