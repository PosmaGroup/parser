﻿using SmartCadControls;
namespace SmartCadGuiCommon
{
    partial class AppointmentFormEx
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButtonOk = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.gridControlOperators = new GridControlEx();
            this.gridViewOperators = new GridViewEx();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.labelName = new DevExpress.XtraEditors.LabelControl();
            this.textName = new DevExpress.XtraEditors.LabelControl();
            this.textBoxDescription = new DevExpress.XtraEditors.MemoEdit();
            this.labelDateStart = new DevExpress.XtraEditors.LabelControl();
            this.labelDescription = new DevExpress.XtraEditors.LabelControl();
            this.textDateStart = new DevExpress.XtraEditors.LabelControl();
            this.textType = new DevExpress.XtraEditors.LabelControl();
            this.textTimeStart = new DevExpress.XtraEditors.LabelControl();
            this.labelImage = new DevExpress.XtraEditors.LabelControl();
            this.labelDateEnd = new DevExpress.XtraEditors.LabelControl();
            this.textTimeEnd = new DevExpress.XtraEditors.LabelControl();
            this.textDateEnd = new DevExpress.XtraEditors.LabelControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlOperators)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewOperators)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.simpleButtonOk);
            this.panelControl1.Controls.Add(this.layoutControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(632, 331);
            this.panelControl1.TabIndex = 0;
            // 
            // simpleButtonOk
            // 
            this.simpleButtonOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.simpleButtonOk.Location = new System.Drawing.Point(545, 303);
            this.simpleButtonOk.Name = "simpleButtonOk";
            this.simpleButtonOk.Size = new System.Drawing.Size(75, 23);
            this.simpleButtonOk.TabIndex = 27;
            this.simpleButtonOk.Text = "Accept";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.gridControlOperators);
            this.layoutControl1.Controls.Add(this.panelControl2);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl1.Location = new System.Drawing.Point(2, 2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(628, 295);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // gridControlOperators
            // 
            this.gridControlOperators.EnableAutoFilter = false;
            this.gridControlOperators.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlOperators.Location = new System.Drawing.Point(285, 27);
            this.gridControlOperators.MainView = this.gridViewOperators;
            this.gridControlOperators.Name = "gridControlOperators";
            this.gridControlOperators.Size = new System.Drawing.Size(336, 261);
            this.gridControlOperators.TabIndex = 5;
            this.gridControlOperators.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewOperators});
            this.gridControlOperators.ViewTotalRows = false;
            // 
            // gridViewOperators
            // 
            this.gridViewOperators.AllowFocusedRowChanged = true;
            this.gridViewOperators.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewOperators.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewOperators.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewOperators.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewOperators.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewOperators.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewOperators.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewOperators.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewOperators.EnablePreviewLineForFocusedRow = false;
            this.gridViewOperators.GridControl = this.gridControlOperators;
            this.gridViewOperators.GroupFormat = " [#image]{1} {2}";
            this.gridViewOperators.Name = "gridViewOperators";
            this.gridViewOperators.OptionsCustomization.AllowFilter = false;
            this.gridViewOperators.OptionsDetail.EnableMasterViewMode = false;
            this.gridViewOperators.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewOperators.OptionsSelection.UseIndicatorForSelection = false;
            this.gridViewOperators.OptionsView.ShowDetailButtons = false;
            this.gridViewOperators.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewOperators.OptionsView.ShowGroupPanel = false;
            this.gridViewOperators.OptionsView.ShowIndicator = false;
            this.gridViewOperators.ViewTotalRows = false;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.labelName);
            this.panelControl2.Controls.Add(this.textName);
            this.panelControl2.Controls.Add(this.textBoxDescription);
            this.panelControl2.Controls.Add(this.labelDateStart);
            this.panelControl2.Controls.Add(this.labelDescription);
            this.panelControl2.Controls.Add(this.textDateStart);
            this.panelControl2.Controls.Add(this.textType);
            this.panelControl2.Controls.Add(this.textTimeStart);
            this.panelControl2.Controls.Add(this.labelImage);
            this.panelControl2.Controls.Add(this.labelDateEnd);
            this.panelControl2.Controls.Add(this.textTimeEnd);
            this.panelControl2.Controls.Add(this.textDateEnd);
            this.panelControl2.Location = new System.Drawing.Point(7, 27);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(264, 261);
            this.panelControl2.TabIndex = 4;
            // 
            // labelName
            // 
            this.labelName.Location = new System.Drawing.Point(5, 5);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(44, 13);
            this.labelName.TabIndex = 14;
            this.labelName.Text = "Nombre :";
            // 
            // textName
            // 
            this.textName.Location = new System.Drawing.Point(79, 5);
            this.textName.Name = "textName";
            this.textName.Size = new System.Drawing.Size(63, 13);
            this.textName.TabIndex = 15;
            this.textName.Text = "labelControl2";
            // 
            // textBoxDescription
            // 
            this.textBoxDescription.Enabled = false;
            this.textBoxDescription.Location = new System.Drawing.Point(5, 137);
            this.textBoxDescription.Name = "textBoxDescription";
            this.textBoxDescription.Size = new System.Drawing.Size(255, 119);
            this.textBoxDescription.TabIndex = 25;
            // 
            // labelDateStart
            // 
            this.labelDateStart.Location = new System.Drawing.Point(5, 61);
            this.labelDateStart.Name = "labelDateStart";
            this.labelDateStart.Size = new System.Drawing.Size(53, 13);
            this.labelDateStart.TabIndex = 16;
            this.labelDateStart.Text = "Comienzo :";
            // 
            // labelDescription
            // 
            this.labelDescription.Location = new System.Drawing.Point(5, 118);
            this.labelDescription.Name = "labelDescription";
            this.labelDescription.Size = new System.Drawing.Size(61, 13);
            this.labelDescription.TabIndex = 24;
            this.labelDescription.Text = "Descripcion :";
            // 
            // textDateStart
            // 
            this.textDateStart.Location = new System.Drawing.Point(79, 61);
            this.textDateStart.Name = "textDateStart";
            this.textDateStart.Size = new System.Drawing.Size(63, 13);
            this.textDateStart.TabIndex = 17;
            this.textDateStart.Text = "labelControl4";
            // 
            // textType
            // 
            this.textType.AllowHtmlString = true;
            this.textType.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.textType.Location = new System.Drawing.Point(79, 33);
            this.textType.Name = "textType";
            this.textType.Size = new System.Drawing.Size(69, 14);
            this.textType.TabIndex = 23;
            this.textType.Text = "labelControl10";
            // 
            // textTimeStart
            // 
            this.textTimeStart.Location = new System.Drawing.Point(148, 61);
            this.textTimeStart.Name = "textTimeStart";
            this.textTimeStart.Size = new System.Drawing.Size(63, 13);
            this.textTimeStart.TabIndex = 18;
            this.textTimeStart.Text = "labelControl5";
            // 
            // labelImage
            // 
            this.labelImage.Location = new System.Drawing.Point(5, 33);
            this.labelImage.Name = "labelImage";
            this.labelImage.Size = new System.Drawing.Size(27, 13);
            this.labelImage.TabIndex = 22;
            this.labelImage.Text = "Tipo :";
            // 
            // labelDateEnd
            // 
            this.labelDateEnd.Location = new System.Drawing.Point(5, 90);
            this.labelDateEnd.Name = "labelDateEnd";
            this.labelDateEnd.Size = new System.Drawing.Size(21, 13);
            this.labelDateEnd.TabIndex = 19;
            this.labelDateEnd.Text = "Fin :";
            // 
            // textTimeEnd
            // 
            this.textTimeEnd.Location = new System.Drawing.Point(148, 90);
            this.textTimeEnd.Name = "textTimeEnd";
            this.textTimeEnd.Size = new System.Drawing.Size(63, 13);
            this.textTimeEnd.TabIndex = 21;
            this.textTimeEnd.Text = "labelControl8";
            // 
            // textDateEnd
            // 
            this.textDateEnd.Location = new System.Drawing.Point(79, 90);
            this.textDateEnd.Name = "textDateEnd";
            this.textDateEnd.Size = new System.Drawing.Size(63, 13);
            this.textDateEnd.TabIndex = 20;
            this.textDateEnd.Text = "labelControl7";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup3});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(628, 295);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup2.Size = new System.Drawing.Size(278, 295);
            this.layoutControlGroup2.Text = "Tiempo extra";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.panelControl2;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(268, 265);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.layoutControlGroup3.Location = new System.Drawing.Point(278, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup3.Size = new System.Drawing.Size(350, 295);
            this.layoutControlGroup3.Text = "Operadores";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.gridControlOperators;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(340, 265);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // AppointmentFormEx
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(632, 331);
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AppointmentFormEx";
            this.Text = "AppointmentFormEx";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlOperators)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewOperators)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.SimpleButton simpleButtonOk;
        private GridControlEx gridControlOperators;
        private GridViewEx gridViewOperators;
        private DevExpress.XtraEditors.PanelControl panelControl2;
		private DevExpress.XtraEditors.LabelControl labelName;
        private DevExpress.XtraEditors.LabelControl textName;
        private DevExpress.XtraEditors.MemoEdit textBoxDescription;
        private DevExpress.XtraEditors.LabelControl labelDateStart;
        private DevExpress.XtraEditors.LabelControl labelDescription;
        private DevExpress.XtraEditors.LabelControl textDateStart;
        private DevExpress.XtraEditors.LabelControl textType;
        private DevExpress.XtraEditors.LabelControl textTimeStart;
        private DevExpress.XtraEditors.LabelControl labelImage;
        private DevExpress.XtraEditors.LabelControl labelDateEnd;
        private DevExpress.XtraEditors.LabelControl textTimeEnd;
        private DevExpress.XtraEditors.LabelControl textDateEnd;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
    }
}