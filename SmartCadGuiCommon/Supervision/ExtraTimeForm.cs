using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using Smartmatic.SmartCad.Service;
using System.Reflection;
using System.ServiceModel;
using DevExpress.XtraGrid.Columns;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadCore.Core;
using SmartCadCore.Enums;
using SmartCadGuiCommon.Controls;
using SmartCadControls;
using SmartCadGuiCommon;


namespace SmartCadGuiCommon
{
    public partial class ExtraTimeForm : DevExpress.XtraEditors.XtraForm
    {
        private BindingList<GridControlDataWorkShiftVariation> dataSource;
        private bool isGeneralSupervisor;
        private object syncCommited = new object();
        private Timer timerUpdateInformation = new Timer();
        private int timeToDelete = 0;
        private bool isTimeOff = false;

        //is true when variation is a extra time, otherwise is false.
        public ExtraTimeForm(bool isGeneralSupervisor, bool isTimeOff, int timeToDeleteVariation)
        {
            timeToDelete = timeToDeleteVariation;
            this.isTimeOff = isTimeOff;

            InitializeComponent();
            this.isGeneralSupervisor = isGeneralSupervisor;
            if (this.isGeneralSupervisor == false)
            {
                this.barButtonItemCreateExtraTime.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                this.barButtonItemDeleteExtraTime.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                this.barButtonItemModifyExtraTime.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                this.barButtonItemPersonalAssignExtraTime.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                this.barButtonItemCreateVacations.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                this.barButtonItemDeleteVacations.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                this.barButtonItemModifyVacations.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                this.barButtonItemPersonalAssignVacations.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            }
            else
            {
                this.barButtonItemCreateExtraTime.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
                this.barButtonItemDeleteExtraTime.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
                this.barButtonItemModifyExtraTime.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
                this.barButtonItemPersonalAssignExtraTime.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
                this.barButtonItemCreateVacations.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
                this.barButtonItemDeleteVacations.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
                this.barButtonItemModifyVacations.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
                this.barButtonItemPersonalAssignVacations.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
            }
            InitializeGridControl();
            LoadLanguage();

            //Cargo los horarios de trabajo
            FillWorkShifts(); 
     
        }
        
        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            ExtraTimeCreateForm createForm = new ExtraTimeCreateForm();
            createForm.ShowDialog();
        }

        private WorkShiftVariationClientData selectedWorkShiftVariation;
        
        public WorkShiftVariationClientData SelectedWorkShiftVariation
        {
            get
            {
                return selectedWorkShiftVariation;
            }
            set
            {                
                selectedWorkShiftVariation = value;
            }
        }

        private void InitializeGridControl()
        {
            gridControlExtraTime.Type = typeof(GridControlDataWorkShiftVariation);
            gridControlExtraTime.EnableAutoFilter = true;
            gridControlExtraTime.ViewTotalRows = true;

            gridControlDetails.Type = typeof(WorkShiftScheduleVariationGridData);
            gridControlDetails.ViewTotalRows = true;

            gridControlOperators.Type = typeof(OperatorVariationGridData);
            gridControlOperators.EnableAutoFilter = true;
            gridControlOperators.ViewTotalRows = true;

			gridViewExtraTime.Columns["Type"].Visible = !isTimeOff;
            gridViewExtraTime.Columns["MotiveName"].Visible = isTimeOff;

        }

        private void LoadLanguage()
        {
            switch (isTimeOff)
            {
                case false:
                    Text = ResourceLoader.GetString2("WorkShiftFormText");
                    this.layoutControlGroupExtraTime.Text = ResourceLoader.GetString2("WorkShiftGroupBoxText");
                    break;
                case true:
                    Text = ResourceLoader.GetString2("FreeTimeFormText");
                    layoutControlGroupExtraTime.Text = ResourceLoader.GetString2("FreeTimeGroupBoxText");
                    break;
                default:
                    break;
            }

            this.layoutControlGroupDetails.Text = ResourceLoader.GetString2("VariationDetails");
            this.layoutControlGroupOperators.Text = ResourceLoader.GetString2("Personal");

            barButtonItemPersonalAssignExtraTime.Caption = ResourceLoader.GetString2("Personal");
            barButtonItemPersonalAssignVacations.Caption = ResourceLoader.GetString2("Personal");
            RibbonPageOperation.Text = ResourceLoader.GetString2("Operation");
            ribbonPageGroupExtraTime.Text = ResourceLoader.GetString2("WorkShifts");
            ribbonPageGroupVacationAndAbsence.Text = ResourceLoader.GetString2("VacationsAndPermissions");
            barButtonItemCreateExtraTime.Caption = ResourceLoader.GetString2("$Message.Crear");
            barButtonItemDeleteExtraTime.Caption = ResourceLoader.GetString2("$Message.Delete");
            barButtonItemModifyExtraTime.Caption = ResourceLoader.GetString2("Modify");
            barButtonItemCreateVacations.Caption = ResourceLoader.GetString2("$Message.Crear");
            barButtonItemDeleteVacations.Caption = ResourceLoader.GetString2("$Message.Delete");
            barButtonItemModifyVacations.Caption = ResourceLoader.GetString2("Modify");
            barButtonItemConsultExtraTime.Caption = ResourceLoader.GetString2("View");
            barButtonItemConsultVacations.Caption = ResourceLoader.GetString2("View");

            #region Ribbon bar ToolTips
            this.barButtonItemCreateExtraTime.SuperTip = SupervisionForm.BuildToolTip("ToolTip_CreateWorkShift");
            this.barButtonItemDeleteExtraTime.SuperTip = SupervisionForm.BuildToolTip("ToolTip_DeleteWorkShift");
            this.barButtonItemModifyExtraTime.SuperTip = SupervisionForm.BuildToolTip("ToolTip_ModifyWorkShift");
            this.barButtonItemConsultExtraTime.SuperTip = SupervisionForm.BuildToolTip("ToolTip_RequestWorkShift");

            this.barButtonItemCreateVacations.SuperTip = SupervisionForm.BuildToolTip("ToolTip_CreateVacations");
            this.barButtonItemDeleteVacations.SuperTip = SupervisionForm.BuildToolTip("ToolTip_DeleteVacations");
            this.barButtonItemModifyVacations.SuperTip = SupervisionForm.BuildToolTip("ToolTip_ModifyVacations");
            this.barButtonItemConsultVacations.SuperTip = SupervisionForm.BuildToolTip("ToolTip_ConsultVacations");

            #endregion
        }

        protected override void OnLoad(EventArgs e)
        {
           (this.MdiParent as SupervisionForm).SupervisionCommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(ExtraTimeForm_SupervisionCommittedChanges);

            timerUpdateInformation = new Timer();
            timerUpdateInformation.Tick += new EventHandler(timerUpdateInformation_Tick);
            timerUpdateInformation.Interval = 10000;
            timerUpdateInformation.Start();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
            if ((this.MdiParent as SupervisionForm) != null)
                (this.MdiParent as SupervisionForm).SupervisionCommittedChanges -= new EventHandler<CommittedObjectDataCollectionEventArgs>(ExtraTimeForm_SupervisionCommittedChanges);
        }

        void ExtraTimeForm_SupervisionCommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
        {
            try
            {
                if (e.Objects != null && e.Objects.Count > 0)
                {
                    lock (syncCommited)
                    {
                        try
                        {
                            if (ServerServiceClient.GetInstance().OperatorClient == null)
                            { }
                        }
                        catch
                        { }
                        if (e.Objects[0] is ApplicationPreferenceClientData)
                        {
                            ApplicationPreferenceClientData preference = e.Objects[0] as ApplicationPreferenceClientData;
                            if (e.Action == CommittedDataAction.Update)
                            {
                                if (isTimeOff == false && preference.Name.Equals("TimeToDeleteWorkShift"))
                                {
                                    timeToDelete = int.Parse(preference.Value);
                                }
                                else if (isTimeOff == true && preference.Name.Equals("TimeToDeleteVacation"))
                                {
                                    timeToDelete = int.Parse(preference.Value);
                                }
                            }
                        }
                        if (e.Objects[0] is WorkShiftVariationClientData)
                        {
                           
                            WorkShiftVariationClientData variation = (WorkShiftVariationClientData)e.Objects[0];
                            if (variation.ObjectType == WorkShiftVariationClientData.ObjectRelatedType.Operators)
                            {
                                if ((isTimeOff == false && variation.Type != WorkShiftVariationClientData.WorkShiftType.TimeOff) || (isTimeOff == true && variation.Type == WorkShiftVariationClientData.WorkShiftType.TimeOff))
                                {

                                    GridControlDataWorkShiftVariation gridData = new GridControlDataWorkShiftVariation(variation);
                                                                       
                                    int index = 0;

                                    
                                    try
                                    {
                                        FormUtil.InvokeRequired(this, delegate
                                           {
                                               if (e.Action == CommittedDataAction.Save)
                                                   dataSource.Add(gridData);
                                               else
                                               {
                                                   index = dataSource.IndexOf(gridData);

                                                   if (index >= 0)
                                                   {
                                                       if (e.Action == CommittedDataAction.Delete)
                                                       {
                                                           dataSource.RemoveAt(index);
                                                          
                                                       }
                                                       else if (e.Action == CommittedDataAction.Update)
                                                       {
                                                           dataSource[index] = gridData;
                                                 
                                                       }
                                                   }
                                               }
                                           });
                                    }
                                    finally
                                    {
                                        FormUtil.InvokeRequired(this, delegate
                                        {
                                           
                                            gridViewExtraTime_FocusedRowChanged(gridViewExtraTime, new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs(gridViewExtraTime.FocusedRowHandle, gridViewExtraTime.FocusedRowHandle));
                                        });
                                        

                                    }

                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }

        }
      
        private void FillWorkShifts() 
        {
            IList list;

            if (isTimeOff == true)
            {
                list = ServerServiceClient.GetInstance().SearchClientObjects(
                    SmartCadHqls.GetCustomHql(
                    SmartCadHqls.GetTimeOff, (int)WorkShiftVariationClientData.WorkShiftType.TimeOff,
                    ApplicationUtil.GetDataBaseFormattedDate(ServerServiceClient.GetInstance().GetTime().AddMinutes(timeToDelete * -1))));
            }
            else 
            {
                list = ServerServiceClient.GetInstance().SearchClientObjects(
                    SmartCadHqls.GetCustomHql(
                    SmartCadHqls.GetWorkShift, (int)WorkShiftVariationClientData.WorkShiftType.WorkShift, (int)WorkShiftVariationClientData.WorkShiftType.ExtraTime,
                    ApplicationUtil.GetDataBaseFormattedDate(ServerServiceClient.GetInstance().GetTime().AddMinutes(timeToDelete * -1))));
            }

            dataSource = new BindingList<GridControlDataWorkShiftVariation>();
            if (list != null) 
            {
                foreach (WorkShiftVariationClientData client in list)
                {
                    dataSource.Add(new GridControlDataWorkShiftVariation(client));
                }
                gridControlExtraTime.BeginInit();
                gridControlExtraTime.DataSource = dataSource;
                gridControlExtraTime.EndInit();
            }
        }


        private void gridViewExtraTime_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (e.FocusedRowHandle >= 0)
            {
                ArrayList list = new ArrayList();
                ArrayList listOper = new ArrayList();

                GridControlDataWorkShiftVariation gridData = gridViewExtraTime.GetRow(e.FocusedRowHandle) as GridControlDataWorkShiftVariation;

                if (gridData != null)
                {
                    SelectedWorkShiftVariation = gridData.Data as WorkShiftVariationClientData;

                    if (selectedWorkShiftVariation.Schedules == null)
                        selectedWorkShiftVariation.Schedules = (IList)ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetWorkShiftScheduleVariationsByWSVCode, selectedWorkShiftVariation.Code));

                    foreach (WorkShiftScheduleVariationClientData schedule in selectedWorkShiftVariation.Schedules)
                    {
                        list.Add(new WorkShiftScheduleVariationGridData(schedule));
                    }

                    FormUtil.InvokeRequired(this, delegate
                    {
                        gridControlDetails.BeginInit();
                        gridControlDetails.DataSource = list;
                        gridControlDetails.EndInit();
                    });


                    if (selectedWorkShiftVariation.Operators == null)
                        selectedWorkShiftVariation.Operators = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetOperatorsByWorkShiftVariation, selectedWorkShiftVariation.Code));

                    foreach (OperatorClientData oper in selectedWorkShiftVariation.Operators)
                    {
                        listOper.Add(new OperatorVariationGridData(oper));
                    }


                    FormUtil.InvokeRequired(this, delegate
                    {
                        gridControlOperators.BeginInit();
                        gridControlOperators.DataSource = listOper;
                        gridControlOperators.EndInit();
                    });
                }

                else
                {
                    FormUtil.InvokeRequired(this, delegate
                   {
                       SelectedWorkShiftVariation = null;

                       gridControlDetails.DataSource = null;
                       gridControlOperators.DataSource = null;
                   });
                }

            }
            else
            {
                FormUtil.InvokeRequired(this, delegate
                {
                   SelectedWorkShiftVariation = null;
                   gridControlDetails.DataSource = null;
                   gridControlOperators.DataSource = null;
                });
            }
            ButtonsActivation();
        }


        //YT: No se llama en ningun lado
        //private void FillVariationDetailsAndOperators(WorkShiftVariationClientData variation) 
        //{
        //    ArrayList list = new ArrayList();
        //    ArrayList listOper = new ArrayList();

        //    foreach (WorkShiftScheduleVariationClientData schedule in variation.Schedules)
        //    {
        //        list.Add(new WorkShiftScheduleVariationGridData(schedule));
        //    }

        //    FormUtil.InvokeRequired(this, delegate
        //    {
        //        gridControlDetails.DataSource = new ArrayList(list);
        //        gridViewDetails.RefreshData();

        //    });
            
        //    if (variation.WorkShiftCode == 0)
        //    {
        //        foreach (OperatorClientData oper in variation.Operators)
        //        {
        //            listOper.Add(new OperatorVariationGridData(oper));
        //        }
        //    }
        //    else
        //    {
        //        foreach (OperatorClientData oper in (IList)ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetOperatorByWorkShiftCode, variation.WorkShiftCode)))
        //        {
        //            listOper.Add(new OperatorVariationGridData(oper));
        //        }
        //    }

        //    FormUtil.InvokeRequired(this, delegate
        //    {
        //        gridControlOperators.DataSource = new ArrayList(listOper);
        //        gridViewOperators.RefreshData();

        //    });
            
        //}

        private void ExtraTimeForm_Activated(object sender, EventArgs e)
        {
            ButtonsActivation();
    
            if (this.MdiParent != null)
                (this.MdiParent as SupervisionForm).RibbonControl.SelectedPage = (this.MdiParent as SupervisionForm).RibbonPageOperation;
    
        }

        private void ButtonsActivation() 
        {
            if (selectedWorkShiftVariation != null && selectedWorkShiftVariation.Schedules!= null)
            {
                DateTime realEndDate = DateTime.MinValue;
                foreach (WorkShiftScheduleVariationClientData schedule in selectedWorkShiftVariation.Schedules)
                {
                    if (schedule.End > realEndDate)
                        realEndDate = schedule.End;
                }
                bool active = realEndDate > DateTime.Now;

                if (isTimeOff == false)
                {
                    barButtonItemCreateExtraTime.Enabled = true;
                    barButtonItemDeleteExtraTime.Enabled = active;
                    barButtonItemModifyExtraTime.Enabled = active;
                    barButtonItemPersonalAssignExtraTime.Enabled = active;
                    barButtonItemDeleteVacations.Enabled = false;
                    barButtonItemModifyVacations.Enabled = false;
                    barButtonItemCreateVacations.Enabled = false;
                    barButtonItemPersonalAssignVacations.Enabled= false;
                }
                else
                {

                    barButtonItemDeleteVacations.Enabled = active;
                    barButtonItemModifyVacations.Enabled = active;
                    barButtonItemCreateVacations.Enabled = true;
                    barButtonItemPersonalAssignVacations.Enabled = active;
                    barButtonItemCreateExtraTime.Enabled = false;
                    barButtonItemDeleteExtraTime.Enabled = false;
                    barButtonItemModifyExtraTime.Enabled = false;
                    barButtonItemPersonalAssignExtraTime.Enabled = false;
                }                
            }
            else
            {
                if (isTimeOff == false)
                {
                    barButtonItemCreateExtraTime.Enabled = true;
                    barButtonItemDeleteExtraTime.Enabled = false;
                    barButtonItemModifyExtraTime.Enabled = false;
                    barButtonItemPersonalAssignExtraTime.Enabled = false;
                }
                else
                {
                    barButtonItemCreateVacations.Enabled = true;
                    barButtonItemDeleteVacations.Enabled = false;
                    barButtonItemModifyVacations.Enabled = false;
                    barButtonItemPersonalAssignVacations.Enabled = false;
                }                    
                

            }
        }

        private void barButtonItemCreateExtraTime_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ExtraTimeCreateForm form;

            form = new ExtraTimeCreateForm(null, FormBehavior.Create, isTimeOff);

            form.ShowDialog();
        }

        private void barButtonItemDeleteExtraTime_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridViewExtraTime.SelectedRowsCount > 0)
            {
                ArrayList objectsToDelete = new ArrayList();
                int[] selectedRows = gridViewExtraTime.GetSelectedRows();
                for (int i = 0; i < selectedRows.Length; i++) 
                {
                    objectsToDelete.Add(gridViewExtraTime.GetRow(selectedRows[i]));
                }
                string message = string.Empty;

                if (objectsToDelete.Count == 1)
                    message = ResourceLoader.GetString("DeleteWorkShiftMessage");
                else
                    message = ResourceLoader.GetString2("DeleteMWorkShiftMessage", objectsToDelete.Count.ToString());


                if (MessageForm.Show(message, MessageFormType.Question) == DialogResult.Yes)
                {
                    BackgroundProcessForm processForm = null;
                    try
                    {
                        if (objectsToDelete.Count > 0)
                        {
                            processForm = new BackgroundProcessForm(this, new MethodInfo[1] { GetType().GetMethod("DeleteObjects", BindingFlags.NonPublic | BindingFlags.Instance) }, new object[1][] { new object[1] { objectsToDelete } });
                            processForm.CanThrowError = true;
                            processForm.ShowDialog();
                            //ButtonsActivation();
                        }
                        else
                        {
                            MessageForm.Show(ResourceLoader.GetString2("DeleteWorkShiftData"), MessageFormType.Information);
                        }

                    }
                    catch (FaultException ex)
                    {
                        if (ex.Code != null && ex.Code.Name != null
                            && ex.Code.Name.Equals(typeof(DatabaseObjectException).Name))
                        {
                            MessageForm.Show(ex.Message, MessageFormType.Information);
                        }
                    }
                    catch (Exception ex)
                    {
                        processForm.Close();
                        MessageForm.Show(ex.Message, ex);
                    }
                }
            }
        }

        private void DeleteObjects(ArrayList objectsToDelete)
        {
            for (int i = 0; i < objectsToDelete.Count; i++)
            {
                GridControlDataWorkShiftVariation gridData = (GridControlDataWorkShiftVariation)objectsToDelete[i];
                WorkShiftVariationClientData workShiftVariation = ((WorkShiftVariationClientData)(gridData.Data));

                ServerServiceClient.GetInstance().DeleteClientObject(workShiftVariation);
            }
        }

        private void barButtonItemModifyExtraTime_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            gridViewExtraTime_DoubleClick(sender, null);
        }

        private void barButtonItemCreateVacations_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ExtraTimeCreateForm form;

            form = new ExtraTimeCreateForm(null, FormBehavior.Create, isTimeOff);

            form.ShowDialog();
        }

        private void barButtonItemDeleteVacations_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridViewExtraTime.SelectedRowsCount > 0)
            {
                ArrayList objectsToDelete = new ArrayList();
                int[] selectedRows = gridViewExtraTime.GetSelectedRows();
                for (int i = 0; i < selectedRows.Length; i++)
                {
                    objectsToDelete.Add(gridViewExtraTime.GetRow(selectedRows[i]));
                }
                string message = string.Empty;

                if (objectsToDelete.Count == 1)
                    message = ResourceLoader.GetString("DeleteVacationMessage");
                else
                    message = ResourceLoader.GetString2("DeleteMVacationMessage", objectsToDelete.Count.ToString());


                if (MessageForm.Show(message, MessageFormType.Question) == DialogResult.Yes)
                {
                    BackgroundProcessForm processForm = null;
                    try
                    {
                        if (objectsToDelete.Count > 0)
                        {
                            processForm = new BackgroundProcessForm(this, new MethodInfo[1] { GetType().GetMethod("DeleteObjects", BindingFlags.NonPublic | BindingFlags.Instance) }, new object[1][] { new object[1] { objectsToDelete } });
                            processForm.CanThrowError = true;
                            processForm.ShowDialog();
                        }
                        else
                        {
                            MessageForm.Show(ResourceLoader.GetString2("DeleteVacationData"), MessageFormType.Information);
                        }

                    }
                    catch (FaultException ex)
                    {
                        if (ex.Code != null && ex.Code.Name != null
                            && ex.Code.Name.Equals(typeof(DatabaseObjectException).Name))
                        {
                            MessageForm.Show(ex.Message, MessageFormType.Information);
                        }
                    }
                    catch (Exception ex)
                    {
                        processForm.Close();
                        MessageForm.Show(ex.Message, ex);
                    }
                }
            }
        }

        private void barButtonItemModifyVacations_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            gridViewExtraTime_DoubleClick(sender, null);
        }

        private void gridViewExtraTime_DoubleClick(object sender, EventArgs e)
        {
            if (selectedWorkShiftVariation != null && isGeneralSupervisor) 
            {
                WorkShiftVariationClientData aux = new WorkShiftVariationClientData();
                aux.Code = selectedWorkShiftVariation.Code;
                aux = (WorkShiftVariationClientData)ServerServiceClient.GetInstance().SearchClientObject(SmartCadHqls.GetCustomHql(SmartCadHqls.GetWorkShiftVariationWithSchedulesByCode, aux.Code));

                if (aux != null)
                {
                    ExtraTimeCreateForm form;
                    form = new ExtraTimeCreateForm(aux as WorkShiftVariationClientData, FormBehavior.Edit, isTimeOff);
                    DialogResult res = form.ShowDialog();

                    if (DialogResult == DialogResult.OK)
                        selectedWorkShiftVariation = form.SelectedVariation;
                }
            }
        }


        private void barButtonItemPersonalAssign_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (selectedWorkShiftVariation != null)
            {
                WorkShiftVariationClientData newWSV = new WorkShiftVariationClientData();
                newWSV.Code= selectedWorkShiftVariation.Code;
				newWSV = (WorkShiftVariationClientData)ServerServiceClient.GetInstance().SearchClientObject(SmartCadHqls.GetCustomHql(SmartCadHqls.GetWorkShiftVariationWithSchedulesByCode, newWSV.Code));

                if (newWSV != null)
                {
                    //ExtraTimePersonalAssignForm form = new ExtraTimePersonalAssignForm(selectedWorkShiftVariation.Clone() as WorkShiftVariationClientData, isTimeOff, this.MdiParent);
                    ExtraTimePersonalAssignForm form = new ExtraTimePersonalAssignForm(newWSV as WorkShiftVariationClientData, isTimeOff, this.MdiParent);

                    DialogResult res = form.ShowDialog();

                    if (res == DialogResult.OK)
                         SelectedWorkShiftVariation = form.SelectedVariation;
                    else
                    {
                        SelectedWorkShiftVariation = (WorkShiftVariationClientData)ServerServiceClient.GetInstance().RefreshClient(selectedWorkShiftVariation);
                    }
                }
            }
        }

        private void ExtraTimeForm_Load(object sender, EventArgs e)
        {
            
            barButtonItemConsultVacations.Enabled = true;
            barButtonItemConsultExtraTime.Enabled = true;
        }

        void timerUpdateInformation_Tick(object sender, EventArgs e)
        {
            FormUtil.InvokeRequired(this, new MethodInvoker(UpdateForm));
        }

        void UpdateForm()
        {
            gridControlExtraTime.BeginUpdate();
            BindingList<GridControlDataWorkShiftVariation> temp = (BindingList<GridControlDataWorkShiftVariation>)gridControlExtraTime.DataSource;
            for (int i = temp.Count - 1; i >= 0; i--)
            {
                WorkShiftVariationClientData client = temp[i].Data;
                int count = 0;
                for (int j = client.Schedules.Count - 1; j >= 0; j--)
                {
                    WorkShiftScheduleVariationClientData schedule = (WorkShiftScheduleVariationClientData)client.Schedules[j];
                    if (schedule.End.AddMinutes(timeToDelete) <= ServerServiceClient.GetInstance().GetTime())
                    {
                        count++;
                    }
                }
                if (count == client.Schedules.Count)
                    temp.RemoveAt(i);
            }
            gridControlExtraTime.EndUpdate();

            gridViewExtraTime_FocusedRowChanged(
                gridViewExtraTime,
                new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs(
                    gridViewExtraTime.FocusedRowHandle,
                    gridViewExtraTime.FocusedRowHandle));
        }

        private void barButtonItemConsultExtraTime_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (this.MdiParent != null)
            {
                (this.MdiParent as SupervisionForm).barButtonItemConsultExtraTime_ItemClick(null, null);
            }
        }

        private void barButtonItemConsultVacations_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (this.MdiParent != null)
            {
                (this.MdiParent as SupervisionForm).barButtonItemConsultVacations_ItemClick(null, null);
            }
        }

        private void ExtraTimeForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            //This has to be the last line of code.
            if (e.Cancel == false)
            {
                if (timerUpdateInformation != null)
                {
                    timerUpdateInformation.Stop();
                    timerUpdateInformation.Dispose();
                }
            }
        }
        
    }

    #region GridData

    public class GridControlDataWorkShiftVariation : GridControlDataAdm
    {
        WorkShiftVariationClientData data = null;

        public GridControlDataWorkShiftVariation(WorkShiftVariationClientData data):base(data)
        {
            this.data = data;
        }

        public WorkShiftVariationClientData Data {

            get { return data; }
            set { data = value; }
        
        }

        public int Code
        {
            get { return data.Code; }
        }

        [GridControlRowInfoData(Visible = true)]
        public string Name
        {
            get { return data.Name; }
        }

        [GridControlRowInfoData(Visible = false)]
        public string MotiveName
        {
            get { return data.MotiveName;}
        }
        
        [GridControlRowInfoData(Visible = true)]
        public string Description
        {
            get { return data.Description; }
        }

        [GridControlRowInfoData(Visible = false)]
        public string Type
        {
            get { return ResourceLoader.GetString2(data.Type.ToString());}
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            GridControlDataWorkShiftVariation var = obj as GridControlDataWorkShiftVariation;
            if (var != null)
			{           
                if (this.Code == var.Code)
                    result = true;
            }
            return result;
        }

        protected override int GroupOrder
        {
            get
            {
                return 3;
            }
        }

		#region Override GridControlDataAdm

		public override System.Windows.Forms.Form GetForm(AdministrationRibbonForm form, ClientData data, FormBehavior behaviour)
		{
			return new ExtraTimeCreateForm(data as WorkShiftVariationClientData, behaviour,null);
		}

		public override string GetDeletedMessage
		{
			get { return ResourceLoader.GetString2("DeleteWorkShiftMessage"); }
		}

		public override string GetNotFoundMessage
		{
			get { return ResourceLoader.GetString2("DeleteWorkShiftData"); }
		}

		public override bool ShowPreview
		{
			get { return false; }
		}

		public override bool PageSearch
		{
			get { return false; }
		}

		public override bool InitializeCollections
		{
			get { return true; }
		}

		public override bool Refresh
		{
			get { return false; }
		}

		public override string GroupText
		{
			get { return ResourceLoader.GetString2("WorkShift"); }
		}

		public override string Query
		{
			get { return SmartCadHqls.GetUnitWorkShift; }
		}

        protected override System.Drawing.Image ViewImage
        {
            get
            {
                return ResourceLoader.GetImage("IconCalendar");
            }
        }

        //BATAAN VERSION AA
        public override bool Visible
        {
            get
            {
                return false;
            }
        }

        public override string PageText
        {
            get { return ResourceLoader.GetString("MobileUnits"); }
        }

        protected override int PageOrder
        {
            get { return 4; }
        }

        protected override UserResourceClientData Resource
        {
            get { return WorkShiftVariationClientData.Resource; }
        }

        protected override string ViewToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ViewUnitWorkShift");
            }
        }

        protected override string AddToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_UnitWorkShift");
            }
        }

        protected override string DeleteToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_DeleteUnitWorkShift");
            }
        }

        protected override string EditToolTip
        {
            get
            {
                return ResourceLoader.GetString("ToolTip_ModifyUnitWorkShift");
            }
        }
		#endregion
    }

    public class WorkShiftScheduleVariationGridData
    {
        WorkShiftScheduleVariationClientData data = null;

        public WorkShiftScheduleVariationGridData(WorkShiftScheduleVariationClientData data)
        {
            this.data = data;
        }

        public WorkShiftScheduleVariationClientData Data
        {
            get { return data; }
            set { data = value; }
        }

        [GridControlRowInfoData(Visible = true)]
        public string Day
        {
            get
            {
                return ResourceLoader.GetString2(data.Start.DayOfWeek.ToString());
            }
        }

        [GridControlRowInfoData(Visible = true)]
        public string Date
        {
            get { 
                return data.Start.ToShortDateString();
            }
        }

        [GridControlRowInfoData(Visible = true)]
        public string StartTime
        {
            get {
                return data.Start.ToString("HH:mm");
            }
        }

        [GridControlRowInfoData(Visible = true)]
        public string EndTime
        {
            get {

                //if (data.End.Hour == 23 && data.End.Minute == 59)
                //{
                //    return new DateTime(data.End.Year, data.End.Month, data.End.Day, 0, 0, 0).AddDays(1).ToString("HH:mm");
                //}
                //else
                return data.End.ToString("HH:mm");
               
            }
        }

    }


    public class OperatorVariationGridData
    {
        OperatorClientData data = null;

        public OperatorVariationGridData(OperatorClientData data)
        {
            this.data = data;
        }

        public OperatorClientData Data
        {
            get { return data; }
            set { data = value; }
        }

        [GridControlRowInfoData(Visible = true)]
        public string FirstName
        {
            get
            {
                return data.FirstName;
            }
        }

        [GridControlRowInfoData(Visible = true)]
        public string LastName
        {
            get
            {
                return data.LastName;
            }
        }

        [GridControlRowInfoData(Visible = true)]
        public string SRole
        {
            get
            {
                if (data.DepartmentTypes != null && data.DepartmentTypes.Count > 0)
                {
                    string departs = ": ";
                    foreach (DepartmentTypeClientData dep in data.DepartmentTypes)
                    {
                        departs += dep.Name + ", ";
                    }
                    departs = departs.Remove(departs.Length - 2, 2);
                    return data.RoleFriendlyName + departs;
                }
                return data.RoleFriendlyName;
            }
        }
       
    }

    #endregion
}
