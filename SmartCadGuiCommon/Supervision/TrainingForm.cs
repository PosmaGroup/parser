using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using Smartmatic.SmartCad.Service;
using System.ServiceModel;
using System.Reflection;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using SmartCadCore.ClientData;

using SmartCadCore.Core;
using SmartCadCore.Common;
using SmartCadCore.Enums;
using SmartCadControls;
using SmartCadGuiCommon.SyncBoxes;


namespace SmartCadGuiCommon
{
    public partial class TrainingForm : DevExpress.XtraEditors.XtraForm
    {
        public TrainingCourseSyncBox2 trainingCourseSyncBox2;
        private object syncCommited = new object();
  
        private BindingList<TrainingCourseGridData> trainingCourseList;
        private BindingList<TrainingCourseScheduleGridData> trainingCourseSchedulesList;
        private BindingList<TrainingCoursePartsGridData> trainingCoursePartsList;
        private BindingList<TrainingCourseOperatorsGridData> trainingCourseOperatorsList;

        private TrainingCourseClientData selectedTrainigCourse;
        private TrainingCourseScheduleClientData selectedTrainingCourseSchedule;

        private TrainingCreateForm createForm = null;
        private System.Threading.Timer timer = null;
        private string applicationName = string.Empty;
        private DepartmentTypeClientData department = new DepartmentTypeClientData();

        public TrainingForm(string appName, DepartmentTypeClientData dep)
        {
            InitializeComponent();
            InitializeGridControl();
            LoadLanguage();
          
            LoadTrainingCourse();
            applicationName = appName;
            
            if (dep != null)
                department = dep;
        }

        private void LoadTrainingCourse()
        {
            trainingCourseList = new BindingList<TrainingCourseGridData>();
            //Build courses list with schedules with operators and parts
            IList courses = (IList)ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetTrainingCourses);
            
            if (courses != null && courses.Count > 0)
            {

                IList schedulesWithOpers = (IList)ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetTrainingCourseScheduleWithOperators);
                IList schedulesWithParts = (IList)ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetTrainingCourseScheduleWithParts);

                foreach (TrainingCourseClientData course in courses)
                {
                    if (trainingCourseList.Contains(new TrainingCourseGridData(course)) == false)
                    {
                        foreach (TrainingCourseScheduleClientData schedule in course.Schedules)
                        {
                            for (int i = 0; i < schedulesWithOpers.Count; i++)
                            {
                                TrainingCourseScheduleClientData sWithOpers = (TrainingCourseScheduleClientData)schedulesWithOpers[i];
                                if (schedule.Code == sWithOpers.Code)
                                {
                                    TrainingCourseScheduleClientData sWithParts = (TrainingCourseScheduleClientData)schedulesWithParts[i];
                                    schedule.TrainingCourseOperators = sWithOpers.TrainingCourseOperators;
                                    schedule.Parts = sWithParts.Parts;
                                    schedule.RealStart = sWithParts.RealStart;
                                    schedule.RealEnd = sWithParts.RealEnd;
                                    break;
                                }
                            }
                        }
                        TrainingCourseGridData gridData = new TrainingCourseGridData(course);
                        trainingCourseList.Add(gridData);
                    }
                }
            }

            gridControlTrainingCourse.BeginInit();
            gridControlTrainingCourse.DataSource = trainingCourseList;
            gridControlTrainingCourse.EndInit();

        }

        private void InitializeGridControl()
        {
            this.gridControlOperators.Type = typeof(TrainingCourseOperatorsGridData);
            gridControlOperators.EnableAutoFilter = true;
            gridControlOperators.ViewTotalRows = true;

            this.gridControlParts.Type = typeof(TrainingCoursePartsGridData);
            gridControlParts.EnableAutoFilter = true;
            gridControlParts.ViewTotalRows = true;
            gridViewParts.Columns["StartDate"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            gridViewParts.Columns["StartDate"].DisplayFormat.FormatString = "MM/dd/yyyy HH:mm";
            gridViewParts.Columns["EndDate"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            gridViewParts.Columns["EndDate"].DisplayFormat.FormatString = "MM/dd/yyyy HH:mm";

            this.gridControlSchedules.Type = typeof(TrainingCourseScheduleGridData);
            gridControlSchedules.EnableAutoFilter = true;
            gridControlSchedules.ViewTotalRows = true;

            this.gridControlTrainingCourse.Type = typeof(TrainingCourseGridData);
            gridControlTrainingCourse.EnableAutoFilter = true;
            gridControlTrainingCourse.ViewTotalRows = true;
        }


        void TrainingForm_Load(object sender, System.EventArgs e)
        {
            (this.MdiParent as SupervisionForm).SupervisionCommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(TrainingForm_SupervisionCommittedChanges);
            this.Activated += new EventHandler(TrainingForm_Activated);

            timer = new System.Threading.Timer(new System.Threading.TimerCallback(OnTimer));
            timer.Change(5000, 5000);
        }

        void TrainingForm_Activated(object sender, EventArgs e)
        {
            if (this.MdiParent != null)
                (this.MdiParent as SupervisionForm).RibbonControl.SelectedPage = (this.MdiParent as SupervisionForm).RibbonPageOperation;
        }


        protected override void OnFormClosed(FormClosedEventArgs e)
        {
            base.OnFormClosed(e);
            if (timer != null)
                timer.Dispose();
         
            (this.MdiParent as SupervisionForm).SupervisionCommittedChanges -= new EventHandler<CommittedObjectDataCollectionEventArgs>(TrainingForm_SupervisionCommittedChanges);

        }

        private void OnTimer(object state)
        {
            FormUtil.InvokeRequired(gridControlSchedules, delegate
            {
                gridViewSchedules.RefreshData();
            });
        }

        private void LoadLanguage()
        {
            this.Text = ResourceLoader.GetString2("TrainingForm_Title");
            RibbonPageOperation.Text = ResourceLoader.GetString2("Operation");
            barItemCreateCourses.Caption = ResourceLoader.GetString2("$Message.Crear");
            barItemDeleteCourses.Caption = ResourceLoader.GetString2("$Message.Delete");
            barButtonItemModifyCourses.Caption = ResourceLoader.GetString2("Modify");
            barButtonTrainingCoursesSchedule.Caption = ResourceLoader.GetString2("Schedules");
            barButtonOperatorTrainingAssign.Caption = ResourceLoader.GetString2("Partaker");
            ribbonPageGroupTraining.Text = ResourceLoader.GetString2("Training");
            this.layoutControlGroupTrainingCourse.Text = ResourceLoader.GetString2("TrainingCourses");
            gridColumnName.Caption = ResourceLoader.GetString2("Name");
            gridColumnContact.Caption = ResourceLoader.GetString2("ContactPerson");
            gridColumnContactTelephone.Caption = ResourceLoader.GetString2("ContactTelephone");

            this.layoutControlGroupSchedules.Text = ResourceLoader.GetString2("Schedules");
            gridColumnScheduleName.Caption = ResourceLoader.GetString2("Name");
            gridColumnScheduleStart.Caption = ResourceLoader.GetString2("StartDate");
            gridColumnScheduleEnd.Caption = ResourceLoader.GetString2("EndDate");
            gridColumnTrainer.Caption = ResourceLoader.GetString2("Trainer");
            gridColumnStatus.Caption = ResourceLoader.GetString2("Status");

            this.layoutControlGroupParts.Text = ResourceLoader.GetString2("Intervals");
            gridColumnRoom.Caption = ResourceLoader.GetString2("Room");
            gridColumnPartStart.Caption = ResourceLoader.GetString2("StartDate");
            gridColumnPartEnd.Caption = ResourceLoader.GetString2("EndDate");

  this.layoutControlGroupOperators.Text = ResourceLoader.GetString2("TrainingCourseOperators");
            gridColumnOperatorName.Caption = ResourceLoader.GetString2("Name");
            gridColumnCategory.Caption = ResourceLoader.GetString2("Category");
            gridColumnWorkShift.Caption = ResourceLoader.GetString2("WorkShift");

            #region Ribbon bar ToolTips
            this.barItemCreateCourses.SuperTip = SupervisionForm.BuildToolTip("ToolTip_CreateTrainningCourse");
            this.barItemDeleteCourses.SuperTip = SupervisionForm.BuildToolTip("ToolTip_DeleteTrainningCourse");
            this.barButtonItemModifyCourses.SuperTip = SupervisionForm.BuildToolTip("ToolTip_ModifyTrainningCourse");
            this.barButtonTrainingCoursesSchedule.SuperTip = SupervisionForm.BuildToolTip("ToolTip_CreateScheduleToTrainningCourse");
            this.barButtonOperatorTrainingAssign.SuperTip = SupervisionForm.BuildToolTip("ToolTip_AssignPeopleToCourse");

            #endregion
        }

        void TrainingForm_SupervisionCommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
        {
            try
            {
                if (e.Objects != null && e.Objects.Count > 0)
                {
                    lock (syncCommited)
                    {
                        try
                        {
                            if (ServerServiceClient.GetInstance().OperatorClient == null)
                            { }
                        }
                        catch
                        { }
                        if (e.Objects[0] is TrainingCourseClientData)
                        {
                            TrainingCourseClientData course = (TrainingCourseClientData)e.Objects[0];

                            TrainingCourseGridData tcgd = new TrainingCourseGridData(course);

                            FormUtil.InvokeRequired(this, delegate
                            {
                                if (e.Action != CommittedDataAction.Delete)
                                {
                                    int index = trainingCourseList.IndexOf(tcgd);

                                    if (index > -1)
                                        trainingCourseList[index] = tcgd;
                                    else
                                        trainingCourseList.Add(tcgd);


                                    if (createForm != null && createForm.ButtonOkPressed == false && createForm.SelectedCourse != null && course.Code == createForm.SelectedCourse.Code)
                                    {
                                        MessageForm.Show(ResourceLoader.GetString2("UpdateFormTrainingCourseData"), MessageFormType.Information);
                                        createForm.SelectedCourse = course;
                                    }
                                }
                                else
                                {
                                    if (trainingCourseList.Contains(tcgd) == true)
                                        trainingCourseList.Remove(tcgd);


                                    if (createForm != null && createForm.ButtonOkPressed == false && createForm.SelectedCourse != null && course.Code == createForm.SelectedCourse.Code)
                                    {
                                        MessageForm.Show(ResourceLoader.GetString2("DeleteFormTrainingCourseData"), MessageFormType.Information);
                                        createForm.DialogResult = DialogResult.Abort;
                                        createForm.Close();
                                    }
                                }

                            });

                            FormUtil.InvokeRequired(gridControlTrainingCourse,
                                delegate()
                                {
                                    gridViewTrainingCourse_FocusedRowChanged(
                                            gridViewTrainingCourse,
                                            new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs(
                                                gridViewTrainingCourse.FocusedRowHandle,
                                                gridViewTrainingCourse.FocusedRowHandle));
                                });

                        }
                        else if (e.Objects[0] is TrainingCourseScheduleClientData)
                        {
                            TrainingCourseScheduleClientData schedule = (TrainingCourseScheduleClientData)e.Objects[0];

                            FormUtil.InvokeRequired(this, delegate
                            {
                                TrainingCourseClientData course = new TrainingCourseClientData();
                                course.Code = schedule.TrainingCourseCode;
                                int indexCourse = trainingCourseList.IndexOf(new TrainingCourseGridData(course));

                                if (indexCourse > -1)
                                {
                                    course = trainingCourseList[indexCourse].Data as TrainingCourseClientData;

                                    int indexSchedule = course.Schedules.IndexOf(schedule);

                                    if (e.Action != CommittedDataAction.Delete)
                                    {
                                        if (indexSchedule > -1)
                                            course.Schedules[indexSchedule] = schedule;
                                        else
                                            course.Schedules.Add(schedule);
                                    }
                                    else
                                        course.Schedules.RemoveAt(indexSchedule);


                                    trainingCourseList[indexCourse] = new TrainingCourseGridData(course);
                                }

                            });

                            FormUtil.InvokeRequired(gridControlTrainingCourse,
                            delegate
                            {
                                gridViewTrainingCourse_FocusedRowChanged(
                                           gridViewTrainingCourse,
                                           new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs(
                                               gridViewTrainingCourse.FocusedRowHandle,
                                               gridViewTrainingCourse.FocusedRowHandle));
                            });

                        }
                        else if (e.Objects[0] is OperatorTrainingCourseClientData)
                        {                           
                            TrainingCourseClientData course = null;
                            TrainingCourseScheduleClientData schedule = null;

                            FormUtil.InvokeRequired(this, delegate
                            {
                                foreach (OperatorTrainingCourseClientData operatorTraining in e.Objects)
                                {
                                    course = new TrainingCourseClientData();
                                    course.Code = operatorTraining.CourseCode;
                                    int indexCourse = trainingCourseList.IndexOf(new TrainingCourseGridData(course));

                                    if (indexCourse > -1)
                                    {
                                        course = trainingCourseList[indexCourse].Data as TrainingCourseClientData;

                                        schedule = new TrainingCourseScheduleClientData();
                                        schedule.Code = operatorTraining.CourseScheduleCode;

                                        int indexSchedule = course.Schedules.IndexOf(schedule);

                                        if (indexSchedule > -1)
                                        {
                                            schedule = course.Schedules[indexSchedule] as TrainingCourseScheduleClientData;
                                            int indexOperator;
                                            try
                                            {
                                                indexOperator = schedule.TrainingCourseOperators.IndexOf(operatorTraining);
                                            }
                                            catch
                                            {
                                                indexOperator = 0;
                                                schedule.TrainingCourseOperators = (IList)ServerServiceClient.GetInstance().SearchClientObjects(
                                                                                    SmartCadHqls.GetCustomHql(
                                                                                    SmartCadHqls.GetOperatorsTrainingCourseByScheduleCode,
                                                                                    schedule.Code));
                                            }

                                            if (e.Action != CommittedDataAction.Delete)
                                            {
                                                if (indexOperator > -1)
                                                    schedule.TrainingCourseOperators[indexOperator] = operatorTraining;
                                                else
                                                    schedule.TrainingCourseOperators.Add(operatorTraining);
                                            }
                                            else
                                                schedule.TrainingCourseOperators.RemoveAt(indexOperator);


                                            trainingCourseList[indexCourse] = new TrainingCourseGridData(course);

                                        }
                                    }

                                }

                            });

                           FormUtil.InvokeRequired(gridControlTrainingCourse,
                           delegate
                           {
                               gridViewTrainingCourse_FocusedRowChanged(
                                          gridViewTrainingCourse,
                                          new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs(
                                              gridViewTrainingCourse.FocusedRowHandle,
                                              gridViewTrainingCourse.FocusedRowHandle));
                           });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }

        }

        private void toolStripButtonActivation() 
        {          
            if (gridViewTrainingCourse.SelectedRowsCount > 0 && selectedTrainigCourse != null)
            {
                barButtonTrainingCoursesSchedule.Enabled = true;

                if (gridViewSchedules.SelectedRowsCount > 0 && selectedTrainingCourseSchedule != null && new TrainingCourseScheduleGridData(selectedTrainingCourseSchedule).Status == ResourceLoader.GetString2("NotStarted"))
                {
                    barButtonOperatorTrainingAssign.Enabled = true;
                }
                else
                {
                    barButtonOperatorTrainingAssign.Enabled = false;
                }
            }
            else 
            {
                barButtonItemModifyCourses.Enabled = false;
                barItemDeleteCourses.Enabled = false;
                barButtonOperatorTrainingAssign.Enabled = false;
                barButtonTrainingCoursesSchedule.Enabled = false;
            }
        }

        public void DeleteTrainingCourse() 
        {
            if (gridViewTrainingCourse.SelectedRowsCount > 0 && selectedTrainigCourse != null)
            {
                ArrayList objectsToDelete = new ArrayList();
                objectsToDelete.Add(gridViewTrainingCourse.GetFocusedRow());

                string message = string.Empty;

                if (objectsToDelete.Count == 1)
                    message = ResourceLoader.GetString("DeleteTrainingCourseMessage");
                else
                    message = ResourceLoader.GetString2("DeleteTrainingCoursesMessage", objectsToDelete.Count.ToString());


                if (MessageForm.Show(message, MessageFormType.Question) == DialogResult.Yes)
                {
                    BackgroundProcessForm processForm = null;
                    try
                    {
                        if (objectsToDelete.Count > 0)
                        {
                            ArrayList operatorsTrainingToDelete = new ArrayList();
                            if (selectedTrainigCourse != null)
                            {
                                if (selectedTrainingCourseSchedule != null)
                                {
                                    foreach (OperatorTrainingCourseClientData schedule in selectedTrainingCourseSchedule.TrainingCourseOperators)
                                    {
                                        if (operatorsTrainingToDelete.Contains(schedule) == false && (selectedTrainingCourseSchedule.RealStart >= ServerServiceClient.GetInstance().GetTime() && selectedTrainingCourseSchedule.RealStart != DateTime.MaxValue))
                                        {
                                            operatorsTrainingToDelete.Add(schedule);
                                        }
                                    }
                                }
                            }
                            ServerServiceClient.GetInstance().DeleteClientObjectCollection(operatorsTrainingToDelete); 

                            processForm = new BackgroundProcessForm(this, new MethodInfo[1] { GetType().GetMethod("DeleteObjects", BindingFlags.NonPublic | BindingFlags.Instance) }, new object[1][] { new object[1] { objectsToDelete } });
                            processForm.CanThrowError = true;
                            processForm.ShowDialog();
                        }
                        else
                        {
                            MessageForm.Show(ResourceLoader.GetString2("DeleteTrainingCourseData"), MessageFormType.Information);
                        }

                    }
                    catch (FaultException ex)
                    {
                        if (ex.Code != null && ex.Code.Name != null
                            && ex.Code.Name.Equals(typeof(DatabaseObjectException).Name))
                        {
                            MessageForm.Show(ex.Message, MessageFormType.Information);
                        }
                    }
                    catch (Exception ex)
                    {
                        processForm.Close();
                        MessageForm.Show(ex.Message, ex);
                    }
                }
            }
        }

        private void DeleteObjects(ArrayList objectsToDelete)
        {
            for (int i = 0; i < objectsToDelete.Count; i++)
            {
                TrainingCourseGridData item = (TrainingCourseGridData)objectsToDelete[i];
                ServerServiceClient.GetInstance().DeleteClientObject(item.Data);
            }
        }
       
        protected override void OnClosing(CancelEventArgs e)
        {
            if (this.ParentForm != null)
            {
                SupervisionForm form = this.ParentForm as SupervisionForm;
                form.barItemCreateCourses.Enabled = false;
                form.barButtonItemModifyCourses.Enabled = false;
                form.barItemDeleteCourses.Enabled = false;
                form.barButtonOperatorTrainingAssign.Enabled = false;
                form.barButtonTrainingCoursesSchedule.Enabled = false;
            }
        }

        private void barItemCreateCourses_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            createForm = new TrainingCreateForm(null, FormBehavior.Create);
            createForm.ShowDialog();
            createForm.BringToFront();
            createForm = null;
        }

        private void barItemDeleteCourses_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DeleteTrainingCourse();
        }

        private void barButtonItemModifyCourses_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridViewTrainingCourse.SelectedRowsCount > 0 && selectedTrainigCourse != null)
            {
                TrainingCourseGridData tcgd = gridViewTrainingCourse.GetFocusedRow() as TrainingCourseGridData;
                createForm = new TrainingCreateForm((TrainingCourseClientData)tcgd.Data.Clone(), FormBehavior.Edit);
                if (CheckUpdateTrainingCourse(tcgd.Data.Code) == true)
                {
                    createForm.SetReadOnly(true);
                }
                createForm.ShowDialog();
                createForm.BringToFront();
                createForm = null;
            }
        }

        private bool CheckUpdateTrainingCourse(int code)
        {
            bool result = false;
            long count = (long)((IList)ServerServiceClient.GetInstance().SearchBasicObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetCountTrainingCourseScheduleStartedWithOperators, code, ApplicationUtil.GetDataBaseFormattedDate(ServerServiceClient.GetInstance().GetTime()))))[0];
            if (count > 0)
                result = true;
            return result;
        }

        private void barButtonTrainingCoursesSchedule_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridViewTrainingCourse.SelectedRowsCount > 0 && selectedTrainigCourse != null)
            {
                TrainingCourseScheduleForm form;

                TrainingCourseGridData tcgd = (TrainingCourseGridData)gridViewTrainingCourse.GetFocusedRow();
                BindingList<TrainingCourseScheduleGridData> aux = new BindingList<TrainingCourseScheduleGridData>();
                if (gridViewSchedules.SelectedRowsCount > 0 && selectedTrainingCourseSchedule != null)
                {
                    TrainingCourseScheduleClientData tcsgdClient = (gridViewSchedules.GetFocusedRow() as TrainingCourseScheduleGridData).Data.Clone() as TrainingCourseScheduleClientData;
                    tcsgdClient.Code = (gridViewSchedules.GetFocusedRow() as TrainingCourseScheduleGridData).Data.Code;

                    TrainingCourseScheduleGridData tcsgd = new TrainingCourseScheduleGridData(tcsgdClient);

                    foreach (TrainingCourseScheduleGridData item in trainingCourseSchedulesList)
                    {
                        TrainingCourseScheduleClientData itemClient = item.Data.Clone() as TrainingCourseScheduleClientData;
                        itemClient.Code = item.Data.Code;
                        aux.Add(new TrainingCourseScheduleGridData(itemClient));
                    }
                    form = new TrainingCourseScheduleForm(tcgd, aux, tcsgd);
                }
                else
                {
                    foreach (TrainingCourseScheduleGridData item in trainingCourseSchedulesList)
                    {
                        TrainingCourseScheduleClientData itemClient = item.Data.Clone() as TrainingCourseScheduleClientData;
                        itemClient.Code = item.Data.Code;
                        aux.Add(new TrainingCourseScheduleGridData(itemClient));
                    }
                    form = new TrainingCourseScheduleForm(tcgd, aux, null);
                }
                DialogResult res = form.ShowDialog();
            }
        }

       
     
        private void barButtonOperatorTrainingAssign_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridViewTrainingCourse.SelectedRowsCount > 0 && selectedTrainigCourse != null)
            {
                TrainingCourseAssignForm form;

                TrainingCourseGridData tcgd = gridViewTrainingCourse.GetFocusedRow() as TrainingCourseGridData;
          
                if (gridViewSchedules.SelectedRowsCount > 0 && selectedTrainingCourseSchedule != null)
                {
                    TrainingCourseScheduleGridData tcsgd = gridViewSchedules.GetFocusedRow() as TrainingCourseScheduleGridData;
                    form = new TrainingCourseAssignForm(tcgd.Data, tcsgd.Data, applicationName, department);
                }
                else
                    form = new TrainingCourseAssignForm(tcgd.Data, null, applicationName, department);
        
                form.ShowDialog();
            }
        }

        private void barButtonItemEvaluateOperatorInCourse_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridViewTrainingCourse.SelectedRowsCount > 0 && gridViewSchedules.SelectedRowsCount > 0)
            {
                TrainingCourseScheduleGridData tcsgd = gridViewSchedules.GetFocusedRow() as TrainingCourseScheduleGridData;

                if (gridViewOperators.RowCount > 0)
                {
                    TrainingCourseClientData aux=new TrainingCourseClientData();
                    aux.Code =  tcsgd.Data.TrainingCourseCode;
                    tcsgd.Data.TrainingCourse = (TrainingCourseClientData)ServerServiceClient.GetInstance().RefreshClient(aux);
                    TrainingCourseEvaluationForm form = new TrainingCourseEvaluationForm(tcsgd.Data, FormBehavior.Edit);
                    form.ShowDialog();
                }
                else
                {
                    MessageForm.Show(ResourceLoader.GetString2("ScheduleCanNotBeEvaluated"), MessageFormType.Information);
                }
            }
        }

        private void barButtonItemHistoricTraining_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridViewTrainingCourse.SelectedRowsCount > 0 && selectedTrainigCourse != null)
            {
                TrainingCourseGridData tcgd = gridViewTrainingCourse.GetFocusedRow() as TrainingCourseGridData;
                TrainingCourseHistoryForm form = new TrainingCourseHistoryForm(tcgd.Data);
                form.ShowDialog();
            }
        }

        private void gridViewTrainingCourse_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (e.FocusedRowHandle > -1)
            {

                selectedTrainigCourse = ((TrainingCourseGridData)gridControlTrainingCourse.SelectedItems[0]).Data as TrainingCourseClientData;

                //Cargo los schedules si es necesario
                if (selectedTrainigCourse.Schedules == null)
                {
                    selectedTrainigCourse.Schedules = (IList)ServerServiceClient.GetInstance().SearchClientObjects(
                                                                                                SmartCadHqls.GetCustomHql(
                                                                                                SmartCadHqls.GetTrainingCourseScheduleByCourseCode,
                                                                                                selectedTrainigCourse.Code));
                }

                trainingCourseSchedulesList = new BindingList<TrainingCourseScheduleGridData>();
                foreach (TrainingCourseScheduleClientData schedule in selectedTrainigCourse.Schedules)
                {
                    trainingCourseSchedulesList.Add(new TrainingCourseScheduleGridData(schedule));
                }


                gridControlSchedules.BeginUpdate();
                gridControlSchedules.ClearData();
                gridControlSchedules.DataSource = trainingCourseSchedulesList;
                gridControlSchedules.EndUpdate();

           
                IList result = (IList)ServerServiceClient.GetInstance().SearchBasicObjects(
                                                               SmartCadHqls.GetCustomHql(SmartCadHqls.GetCountTrainingCourseScheduleStartedWithOperators,
                                                               selectedTrainigCourse.Code,
                                                               ApplicationUtil.GetDataBaseFormattedDate(ServerServiceClient.GetInstance().GetTime())));

               
                barItemDeleteCourses.Enabled = true;
                barButtonItemModifyCourses.Enabled = true;
                if (result != null && result.Count > 0)
                {
                    barItemDeleteCourses.Enabled = (long)result[0] == 0;
                    barButtonItemModifyCourses.Enabled = (long)result[0] == 0;
                }

                if (gridControlSchedules.Items.Count > 0)
                {
                    gridViewSchedules_FocusedRowChanged(
                                              gridViewSchedules,
                                              new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs(
                                                  gridViewSchedules.FocusedRowHandle,
                                                  gridViewSchedules.FocusedRowHandle));
                }
            }
            else
            {
                selectedTrainigCourse = null;

                gridControlSchedules.BeginUpdate();
                gridControlSchedules.DataSource = null;
                gridControlSchedules.EndUpdate();

                gridViewSchedules.FocusedRowHandle = -1;
            }

           
            toolStripButtonActivation();
        }

        private void gridViewSchedules_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (e.FocusedRowHandle > -1)
            {
                selectedTrainingCourseSchedule = ((TrainingCourseScheduleGridData)gridControlSchedules.SelectedItems[0]).Data as TrainingCourseScheduleClientData;

                if (timer != null)
                    timer.Change(5000, 5000);

               // Cargo las partes si es necesario
                //if (selectedTrainingCourseSchedule.Parts == null)
                {
                    selectedTrainingCourseSchedule.Parts = (IList)ServerServiceClient.GetInstance().SearchClientObjects(
                                                                        SmartCadHqls.GetCustomHql(
                                                                        SmartCadHqls.GetTrainingCourseSchedulePartsByScheduleCode,
                                                                        selectedTrainingCourseSchedule.Code));
                }

                trainingCoursePartsList = new BindingList<TrainingCoursePartsGridData>();
                foreach (TrainingCourseSchedulePartsClientData part in selectedTrainingCourseSchedule.Parts)
                {
                    trainingCoursePartsList.Add(new TrainingCoursePartsGridData(part));
                }

            
                gridControlParts.BeginUpdate();
                gridControlParts.DataSource = trainingCoursePartsList;
                gridControlParts.EndUpdate();

                //Cargo los operadores si es necesario
                if (selectedTrainingCourseSchedule.TrainingCourseOperators == null)
                {
                    selectedTrainingCourseSchedule.TrainingCourseOperators = (IList)ServerServiceClient.GetInstance().SearchClientObjects(
                                                                                    SmartCadHqls.GetCustomHql(
                                                                                    SmartCadHqls.GetOperatorsTrainingCourseByScheduleCode,
                                                                                    selectedTrainingCourseSchedule.Code));
                }

                trainingCourseOperatorsList = new BindingList<TrainingCourseOperatorsGridData>();
                foreach (OperatorTrainingCourseClientData ope in selectedTrainingCourseSchedule.TrainingCourseOperators)
                {
                    TrainingCourseOperatorsGridData gridData = new TrainingCourseOperatorsGridData(ope);
                    trainingCourseOperatorsList.Add(gridData);
                }
                                
                gridControlOperators.BeginUpdate();
                gridControlOperators.DataSource = trainingCourseOperatorsList;
                gridControlOperators.EndUpdate();

            }
            else
            {
                selectedTrainingCourseSchedule = null;

                gridControlParts.BeginUpdate();
                gridControlParts.DataSource = null;
                gridControlParts.EndUpdate();

                gridControlOperators.BeginUpdate();
                gridControlOperators.DataSource = null;
                gridControlOperators.EndUpdate();
            
            }
            toolStripButtonActivation();
        }

        private void gridControlTrainingCourse_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                GridHitInfo info = gridViewTrainingCourse.CalcHitInfo(e.X, e.Y);
                if (info.InRow == true)
                {
                    barButtonItemModifyCourses.PerformClick();
                }
            }
        }

        private void gridControlSchedules_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                GridHitInfo info = gridViewSchedules.CalcHitInfo(e.X, e.Y);
                if (info.InRow == true)
                {
                    barButtonTrainingCoursesSchedule.PerformClick();
                }
            }
        }
    }

    public class TrainingCourseGridData
    {
        private TrainingCourseClientData data;


        public TrainingCourseGridData()
        {
      
        }
        public TrainingCourseGridData(TrainingCourseClientData data)
        {
            this.data = data;
        }

        [GridControlRowInfoData(Visible = true, Width = 550)]
        public string Name
        {
            get { return data.Name; }
            set { data.Name = value; }
        }

        [GridControlRowInfoData(Visible = true, Width = 360)]
        public string ContactPerson
        {
            get { return data.ContactPerson; }
            set { data.ContactPerson = value; }
        }

        [GridControlRowInfoData(Visible = true, Width = 350)]
        public string ContactTelephone
        {
            get { return data.PhoneContactPerson; }
            set { data.PhoneContactPerson = value;}
        }


        public TrainingCourseClientData Data
        {
            get { return data; }
            set { data = value; }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is TrainingCourseGridData)
            {
                result = this.Data.Code == ((TrainingCourseGridData)obj).Data.Code;
            }
            return result;
        }      

    }

    public class TrainingCourseScheduleGridData
    { 
        private TrainingCourseScheduleClientData data;
        private string status;


        public TrainingCourseScheduleGridData() 
        {
        
        }

        public TrainingCourseScheduleGridData(TrainingCourseScheduleClientData data)
        {
            this.data = data;
        }

        public int Code
        {
            get { return data.Code; }
            set { data.Code = value; }
        }

        [GridControlRowInfoData(Visible = true, Width = 360)]
        public string Name
        {
            get { return data.Name; }
            set { data.Name = value.Trim(); }
        }

        [GridControlRowInfoData(Visible = true, Width = 205, DisplayFormat = "MM/dd/yyyy")]
        public DateTime StartDate
        {
            get { return data.Start; }
            set { data.Start = value; }
        }

        [GridControlRowInfoData(Visible = true, Width = 205, DisplayFormat = "MM/dd/yyyy")]
        public DateTime EndDate
        {
            get { return data.End; }
            set { data.End = value; }
        }

        [GridControlRowInfoData(Visible = true, Width = 290)]
        public string Trainer
        {
            get { return data.Trainer; }
            set { data.Trainer = value.Trim(); }
        }

        [GridControlRowInfoData(Visible = true, Width = 200)]
        public string Status
        {
            get { return GetStatus(); }
            set { status = value; }
        }

        public TrainingCourseScheduleClientData Data
        {
            get { return data; }
            set { data = value; }
        }

        private string GetStatus()
        {
            string status = string.Empty;
            if (data.RealStart > ServerServiceClient.GetInstance().GetTime() && data.RealStart != DateTime.MaxValue)
            {
                status = ResourceLoader.GetString2("NotStarted");
            }
            else if (data.RealEnd < ServerServiceClient.GetInstance().GetTime() && data.RealEnd != DateTime.MinValue)
            {
                status = ResourceLoader.GetString2("Finished");
            }
            else
            {
                if (data.RealStart != DateTime.MaxValue && data.RealEnd != DateTime.MinValue)
                    status = ResourceLoader.GetString2("Started");
            }
            return status;
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is TrainingCourseScheduleGridData)
            {
                result = this.Code == ((TrainingCourseScheduleGridData)obj).Code;
                //result = this.Name == ((TrainingCourseScheduleGridData)obj).Name &&
                //         ApplicationUtil.CompareDateTime(this.Start, ((TrainingCourseScheduleGridData)obj).Start) &&
                //         ApplicationUtil.CompareDateTime(this.End, ((TrainingCourseScheduleGridData)obj).End); 
            }
            return result;
        }
        
       
    }

    public class TrainingCoursePartsGridData
    {
        TrainingCourseSchedulePartsClientData data;
        private bool changed = false;

        public TrainingCoursePartsGridData(TrainingCourseSchedulePartsClientData data)
        {
            this.data = data;
        }

        [GridControlRowInfoData(Visible = true, Width = 212)]
        public string Room
        {
            get { return data.Room; }
            set { data.Room = value.Trim(); changed = true; }
        }

        [GridControlRowInfoData(Visible = true, Width = 190)]
        public DateTime StartDate
        {
            get { return data.Start; }
            set { data.Start = value; changed = true; }
        }

        [GridControlRowInfoData(Visible = true, Width = 190)]
        public DateTime EndDate
        {
            get { return data.End; }
            set { data.End = value; changed = true; }
        }

        public TrainingCourseSchedulePartsClientData Data
        {
            get { return data; }
        }

      
        public bool Changed
        {
            get { return changed; }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is TrainingCoursePartsGridData)
            {
                result = this.Room == ((TrainingCoursePartsGridData)obj).Room &&
                         ApplicationUtil.CompareDateTime(this.StartDate, ((TrainingCoursePartsGridData)obj).StartDate) &&
                         ApplicationUtil.CompareDateTime(this.EndDate, ((TrainingCoursePartsGridData)obj).EndDate); 
            }
            return result;
        }
    }

    public class TrainingCourseOperatorsGridData
    {
        OperatorTrainingCourseClientData data;

        public TrainingCourseOperatorsGridData(OperatorTrainingCourseClientData data)
        {
            this.data = data;
        }

        [GridControlRowInfoData(Visible = true, Width = 250)]
        public string Name
        {
            get { return data.OperatorFirstName + " " + data.OperatorLastName; }
        }

        [GridControlRowInfoData(Visible = true, Width = 200)]
        public string Category
        {
            get { return data.OperatorCategory; }
        }

        [GridControlRowInfoData(Visible = true, Width = 210)]
        public string WorkShift
        {
            get { return data.OperatorWorkShift; }
        }

        public OperatorTrainingCourseClientData Data
        {
            get { return data; }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is TrainingCourseOperatorsGridData)
            {
                result = this.Data.OperatorCode == ((TrainingCourseOperatorsGridData)obj).Data.OperatorCode;
            }
            return result;
        }
    }
}