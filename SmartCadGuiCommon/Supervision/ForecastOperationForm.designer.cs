using SmartCadControls;
namespace SmartCadGuiCommon
{
    partial class ForecastOperationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.simpleButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlForecast = new DevExpress.XtraLayout.LayoutControl();
            this.textEditName = new DevExpress.XtraEditors.TextEdit();
            this.memoEditDescription = new DevExpress.XtraEditors.MemoEdit();
            this.simpleButtonApply = new DevExpress.XtraEditors.SimpleButton();
            this.dateEditTo = new DevExpress.XtraEditors.DateEdit();
            this.comboBoxEditDepartment = new DevExpress.XtraEditors.ComboBoxEdit();
            this.dateEditFrom = new DevExpress.XtraEditors.DateEdit();
            this.gridControlForecastDetails = new GridControlEx();
            this.gridViewForecastDetail = new GridViewEx();
            this.gridColumnIndicator = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnIndicatorType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnPostivePattern = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnLow = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEditLow = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridColumnHigh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEditHigh = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridColumnMeasureUnit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroupInfo = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroupParameters = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemDepartment = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemFrom = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItemTo = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlForecast)).BeginInit();
            this.layoutControlForecast.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTo.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditDepartment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFrom.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlForecastDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewForecastDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditLow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditHigh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupParameters)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDepartment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButtonCancel
            // 
            this.simpleButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonCancel.Location = new System.Drawing.Point(908, 672);
            this.simpleButtonCancel.Name = "simpleButtonCancel";
            this.simpleButtonCancel.Size = new System.Drawing.Size(82, 32);
            this.simpleButtonCancel.StyleController = this.layoutControlForecast;
            this.simpleButtonCancel.TabIndex = 8;
            this.simpleButtonCancel.Text = "Button2";
            this.simpleButtonCancel.Click += new System.EventHandler(this.simpleButtonCancel_Click);
            // 
            // layoutControlForecast
            // 
            this.layoutControlForecast.AllowCustomizationMenu = false;
            this.layoutControlForecast.Controls.Add(this.textEditName);
            this.layoutControlForecast.Controls.Add(this.simpleButtonCancel);
            this.layoutControlForecast.Controls.Add(this.memoEditDescription);
            this.layoutControlForecast.Controls.Add(this.simpleButtonApply);
            this.layoutControlForecast.Controls.Add(this.dateEditTo);
            this.layoutControlForecast.Controls.Add(this.comboBoxEditDepartment);
            this.layoutControlForecast.Controls.Add(this.dateEditFrom);
            this.layoutControlForecast.Controls.Add(this.gridControlForecastDetails);
            this.layoutControlForecast.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlForecast.Location = new System.Drawing.Point(0, 0);
            this.layoutControlForecast.Name = "layoutControlForecast";
            this.layoutControlForecast.Root = this.layoutControlGroup1;
            this.layoutControlForecast.Size = new System.Drawing.Size(992, 706);
            this.layoutControlForecast.TabIndex = 9;
            this.layoutControlForecast.Text = "layoutControl1";
            // 
            // textEditName
            // 
            this.textEditName.EditValue = "";
            this.textEditName.Location = new System.Drawing.Point(7, 43);
            this.textEditName.Name = "textEditName";
            this.textEditName.Size = new System.Drawing.Size(200, 20);
            this.textEditName.StyleController = this.layoutControlForecast;
            this.textEditName.TabIndex = 2;
            this.textEditName.EditValueChanged += new System.EventHandler(this.textEditName_EditValueChanged);
            // 
            // memoEditDescription
            // 
            this.memoEditDescription.Location = new System.Drawing.Point(211, 43);
            this.memoEditDescription.Name = "memoEditDescription";
            this.memoEditDescription.Size = new System.Drawing.Size(266, 84);
            this.memoEditDescription.StyleController = this.layoutControlForecast;
            this.memoEditDescription.TabIndex = 0;
            this.memoEditDescription.EditValueChanged += new System.EventHandler(this.textEditName_EditValueChanged);
            // 
            // simpleButtonApply
            // 
            this.simpleButtonApply.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.simpleButtonApply.Enabled = false;
            this.simpleButtonApply.Location = new System.Drawing.Point(822, 672);
            this.simpleButtonApply.Name = "simpleButtonApply";
            this.simpleButtonApply.Size = new System.Drawing.Size(82, 32);
            this.simpleButtonApply.StyleController = this.layoutControlForecast;
            this.simpleButtonApply.TabIndex = 7;
            this.simpleButtonApply.Text = "Button1";
            this.simpleButtonApply.Click += new System.EventHandler(this.simpleButtonApply_Click);
            // 
            // dateEditTo
            // 
            this.dateEditTo.EditValue = null;
            this.dateEditTo.Location = new System.Drawing.Point(885, 51);
            this.dateEditTo.Name = "dateEditTo";
            this.dateEditTo.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dateEditTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditTo.Properties.DisplayFormat.FormatString = "MM/dd/yyyy HH:mm";
            this.dateEditTo.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.dateEditTo.Properties.EditFormat.FormatString = "MM/dd/yyyy HH:mm";
            this.dateEditTo.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.dateEditTo.Properties.Mask.EditMask = "MM/dd/yyyy HH:mm";
            this.dateEditTo.Properties.ShowToday = false;
            this.dateEditTo.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditTo.Size = new System.Drawing.Size(68, 20);
            this.dateEditTo.StyleController = this.layoutControlForecast;
            this.dateEditTo.TabIndex = 3;
            this.dateEditTo.EditValueChanged += new System.EventHandler(this.textEditName_EditValueChanged);
            // 
            // comboBoxEditDepartment
            // 
            this.comboBoxEditDepartment.Enabled = false;
            this.comboBoxEditDepartment.Location = new System.Drawing.Point(667, 27);
            this.comboBoxEditDepartment.Name = "comboBoxEditDepartment";
            this.comboBoxEditDepartment.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditDepartment.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEditDepartment.Size = new System.Drawing.Size(66, 20);
            this.comboBoxEditDepartment.StyleController = this.layoutControlForecast;
            this.comboBoxEditDepartment.TabIndex = 1;
            this.comboBoxEditDepartment.Visible = false;
            this.comboBoxEditDepartment.EditValueChanged += new System.EventHandler(this.comboBoxEditDepartment_EditValueChanged);
            // 
            // dateEditFrom
            // 
            this.dateEditFrom.EditValue = null;
            this.dateEditFrom.Location = new System.Drawing.Point(885, 27);
            this.dateEditFrom.Name = "dateEditFrom";
            this.dateEditFrom.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dateEditFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditFrom.Properties.DisplayFormat.FormatString = "MM/dd/yyyy HH:mm";
            this.dateEditFrom.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.dateEditFrom.Properties.EditFormat.FormatString = "MM/dd/yyyy HH:mm";
            this.dateEditFrom.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.dateEditFrom.Properties.Mask.EditMask = "MM/dd/yyyy HH:mm";
            this.dateEditFrom.Properties.ShowToday = false;
            this.dateEditFrom.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditFrom.Size = new System.Drawing.Size(68, 20);
            this.dateEditFrom.StyleController = this.layoutControlForecast;
            this.dateEditFrom.TabIndex = 2;
            this.dateEditFrom.EditValueChanged += new System.EventHandler(this.dateEditFrom_EditValueChanged);
            // 
            // gridControlForecastDetails
            // 
            this.gridControlForecastDetails.EnableAutoFilter = false;
            this.gridControlForecastDetails.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlForecastDetails.Location = new System.Drawing.Point(2, 136);
            this.gridControlForecastDetails.MainView = this.gridViewForecastDetail;
            this.gridControlForecastDetails.Name = "gridControlForecastDetails";
            this.gridControlForecastDetails.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemSpinEditLow,
            this.repositoryItemSpinEditHigh});
            this.gridControlForecastDetails.Size = new System.Drawing.Size(988, 532);
            this.gridControlForecastDetails.TabIndex = 4;
            this.gridControlForecastDetails.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewForecastDetail});
            this.gridControlForecastDetails.ViewTotalRows = false;
            // 
            // gridViewForecastDetail
            // 
            this.gridViewForecastDetail.AllowFocusedRowChanged = true;
            this.gridViewForecastDetail.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewForecastDetail.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewForecastDetail.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewForecastDetail.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewForecastDetail.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewForecastDetail.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewForecastDetail.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewForecastDetail.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewForecastDetail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnIndicator,
            this.gridColumnIndicatorType,
            this.gridColumnPostivePattern,
            this.gridColumnLow,
            this.gridColumnHigh,
            this.gridColumnMeasureUnit});
            this.gridViewForecastDetail.EnablePreviewLineForFocusedRow = false;
            this.gridViewForecastDetail.GridControl = this.gridControlForecastDetails;
            this.gridViewForecastDetail.GroupCount = 1;
            this.gridViewForecastDetail.GroupFormat = "[#image]{1} {2}";
            this.gridViewForecastDetail.Name = "gridViewForecastDetail";
            this.gridViewForecastDetail.OptionsCustomization.AllowColumnMoving = false;
            this.gridViewForecastDetail.OptionsMenu.EnableColumnMenu = false;
            this.gridViewForecastDetail.OptionsMenu.EnableFooterMenu = false;
            this.gridViewForecastDetail.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridViewForecastDetail.OptionsPrint.AutoWidth = false;
            this.gridViewForecastDetail.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewForecastDetail.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewForecastDetail.OptionsView.ShowGroupPanel = false;
            this.gridViewForecastDetail.OptionsView.ShowIndicator = false;
            this.gridViewForecastDetail.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumnIndicatorType, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewForecastDetail.ViewTotalRows = false;
            this.gridViewForecastDetail.GroupRowExpanding += new DevExpress.XtraGrid.Views.Base.RowAllowEventHandler(this.gridViewForecastDetail_GroupRowExpanding);
            this.gridViewForecastDetail.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewForecastDetail_FocusedRowChanged);
            this.gridViewForecastDetail.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewForecastDetail_CellValueChanged);
            this.gridViewForecastDetail.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewForecastDetail_CellValueChanging);
            this.gridViewForecastDetail.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gridViewForecastDetail_ValidatingEditor);
            this.gridViewForecastDetail.InvalidValueException += new DevExpress.XtraEditors.Controls.InvalidValueExceptionEventHandler(this.gridViewForecastDetail_InvalidValueException);
            // 
            // gridColumnIndicator
            // 
            this.gridColumnIndicator.Caption = "gridColumn1";
            this.gridColumnIndicator.FieldName = "Indicator";
            this.gridColumnIndicator.Name = "gridColumnIndicator";
            this.gridColumnIndicator.OptionsColumn.AllowEdit = false;
            this.gridColumnIndicator.OptionsFilter.AllowAutoFilter = false;
            this.gridColumnIndicator.OptionsFilter.AllowFilter = false;
            this.gridColumnIndicator.Visible = true;
            this.gridColumnIndicator.VisibleIndex = 0;
            this.gridColumnIndicator.Width = 450;
            // 
            // gridColumnIndicatorType
            // 
            this.gridColumnIndicatorType.Caption = "gridColumn2";
            this.gridColumnIndicatorType.FieldName = "IndicatorType";
            this.gridColumnIndicatorType.Name = "gridColumnIndicatorType";
            this.gridColumnIndicatorType.OptionsColumn.AllowEdit = false;
            this.gridColumnIndicatorType.OptionsFilter.AllowAutoFilter = false;
            this.gridColumnIndicatorType.OptionsFilter.AllowFilter = false;
            this.gridColumnIndicatorType.Visible = true;
            this.gridColumnIndicatorType.VisibleIndex = 1;
            // 
            // gridColumnPostivePattern
            // 
            this.gridColumnPostivePattern.Caption = "gridColumn3";
            this.gridColumnPostivePattern.FieldName = "PositivePattern";
            this.gridColumnPostivePattern.Name = "gridColumnPostivePattern";
            this.gridColumnPostivePattern.OptionsColumn.AllowEdit = false;
            this.gridColumnPostivePattern.OptionsFilter.AllowAutoFilter = false;
            this.gridColumnPostivePattern.OptionsFilter.AllowFilter = false;
            this.gridColumnPostivePattern.Visible = true;
            this.gridColumnPostivePattern.VisibleIndex = 1;
            this.gridColumnPostivePattern.Width = 200;
            // 
            // gridColumnLow
            // 
            this.gridColumnLow.Caption = "gridColumn4";
            this.gridColumnLow.ColumnEdit = this.repositoryItemSpinEditLow;
            this.gridColumnLow.FieldName = "Low";
            this.gridColumnLow.MinWidth = 100;
            this.gridColumnLow.Name = "gridColumnLow";
            this.gridColumnLow.OptionsFilter.AllowAutoFilter = false;
            this.gridColumnLow.OptionsFilter.AllowFilter = false;
            this.gridColumnLow.Visible = true;
            this.gridColumnLow.VisibleIndex = 2;
            this.gridColumnLow.Width = 110;
            // 
            // repositoryItemSpinEditLow
            // 
            this.repositoryItemSpinEditLow.AutoHeight = false;
            this.repositoryItemSpinEditLow.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEditLow.IsFloatValue = false;
            this.repositoryItemSpinEditLow.Mask.EditMask = "N00";
            this.repositoryItemSpinEditLow.Name = "repositoryItemSpinEditLow";
            // 
            // gridColumnHigh
            // 
            this.gridColumnHigh.Caption = "gridColumn5";
            this.gridColumnHigh.ColumnEdit = this.repositoryItemSpinEditHigh;
            this.gridColumnHigh.FieldName = "High";
            this.gridColumnHigh.MinWidth = 100;
            this.gridColumnHigh.Name = "gridColumnHigh";
            this.gridColumnHigh.OptionsFilter.AllowAutoFilter = false;
            this.gridColumnHigh.OptionsFilter.AllowFilter = false;
            this.gridColumnHigh.Visible = true;
            this.gridColumnHigh.VisibleIndex = 3;
            this.gridColumnHigh.Width = 110;
            // 
            // repositoryItemSpinEditHigh
            // 
            this.repositoryItemSpinEditHigh.AutoHeight = false;
            this.repositoryItemSpinEditHigh.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEditHigh.IsFloatValue = false;
            this.repositoryItemSpinEditHigh.Mask.EditMask = "N00";
            this.repositoryItemSpinEditHigh.Name = "repositoryItemSpinEditHigh";
            // 
            // gridColumnMeasureUnit
            // 
            this.gridColumnMeasureUnit.Caption = "gridColumn6";
            this.gridColumnMeasureUnit.FieldName = "MeasureUnit";
            this.gridColumnMeasureUnit.Name = "gridColumnMeasureUnit";
            this.gridColumnMeasureUnit.OptionsColumn.AllowEdit = false;
            this.gridColumnMeasureUnit.OptionsFilter.AllowAutoFilter = false;
            this.gridColumnMeasureUnit.OptionsFilter.AllowFilter = false;
            this.gridColumnMeasureUnit.Visible = true;
            this.gridColumnMeasureUnit.VisibleIndex = 4;
            this.gridColumnMeasureUnit.Width = 140;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupInfo,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.emptySpaceItem1,
            this.layoutControlGroupParameters});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(992, 706);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroupInfo
            // 
            this.layoutControlGroupInfo.CustomizationFormText = "layoutControlGroupInfo";
            this.layoutControlGroupInfo.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemName,
            this.layoutControlItemDescription,
            this.emptySpaceItem2,
            this.emptySpaceItem4});
            this.layoutControlGroupInfo.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupInfo.Name = "layoutControlGroupInfo";
            this.layoutControlGroupInfo.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupInfo.Size = new System.Drawing.Size(512, 134);
            this.layoutControlGroupInfo.Text = "layoutControlGroupInfo";
            // 
            // layoutControlItemName
            // 
            this.layoutControlItemName.Control = this.textEditName;
            this.layoutControlItemName.CustomizationFormText = "layoutControlItemName";
            this.layoutControlItemName.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemName.Name = "layoutControlItemName";
            this.layoutControlItemName.Size = new System.Drawing.Size(204, 40);
            this.layoutControlItemName.Text = "layoutControlItemName";
            this.layoutControlItemName.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItemName.TextSize = new System.Drawing.Size(144, 13);
            // 
            // layoutControlItemDescription
            // 
            this.layoutControlItemDescription.Control = this.memoEditDescription;
            this.layoutControlItemDescription.CustomizationFormText = "layoutControlItemDescription";
            this.layoutControlItemDescription.Location = new System.Drawing.Point(204, 0);
            this.layoutControlItemDescription.MaxSize = new System.Drawing.Size(0, 104);
            this.layoutControlItemDescription.MinSize = new System.Drawing.Size(155, 104);
            this.layoutControlItemDescription.Name = "layoutControlItemDescription";
            this.layoutControlItemDescription.Size = new System.Drawing.Size(270, 104);
            this.layoutControlItemDescription.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemDescription.Text = "layoutControlItemDescription";
            this.layoutControlItemDescription.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItemDescription.TextSize = new System.Drawing.Size(144, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 40);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(204, 64);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(474, 0);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(28, 104);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridControlForecastDetails;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 134);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(111, 31);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(992, 536);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.simpleButtonApply;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(820, 670);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.simpleButtonCancel;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(906, 670);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 670);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(820, 36);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroupParameters
            // 
            this.layoutControlGroupParameters.CustomizationFormText = "layoutControlGroupParameters";
            this.layoutControlGroupParameters.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemDepartment,
            this.layoutControlItemFrom,
            this.emptySpaceItem3,
            this.layoutControlItemTo,
            this.emptySpaceItem5});
            this.layoutControlGroupParameters.Location = new System.Drawing.Point(512, 0);
            this.layoutControlGroupParameters.Name = "layoutControlGroupParameters";
            this.layoutControlGroupParameters.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupParameters.Size = new System.Drawing.Size(480, 134);
            this.layoutControlGroupParameters.Text = "layoutControlGroupParameters";
            // 
            // layoutControlItemDepartment
            // 
            this.layoutControlItemDepartment.Control = this.comboBoxEditDepartment;
            this.layoutControlItemDepartment.CustomizationFormText = "layoutControlItemDepartment";
            this.layoutControlItemDepartment.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemDepartment.Name = "layoutControlItemDepartment";
            this.layoutControlItemDepartment.Size = new System.Drawing.Size(218, 48);
            this.layoutControlItemDepartment.Text = "layoutControlItemDepartment";
            this.layoutControlItemDepartment.TextSize = new System.Drawing.Size(144, 13);
            // 
            // layoutControlItemFrom
            // 
            this.layoutControlItemFrom.Control = this.dateEditFrom;
            this.layoutControlItemFrom.CustomizationFormText = "layoutControlItemFrom";
            this.layoutControlItemFrom.Location = new System.Drawing.Point(218, 0);
            this.layoutControlItemFrom.Name = "layoutControlItemFrom";
            this.layoutControlItemFrom.Size = new System.Drawing.Size(220, 24);
            this.layoutControlItemFrom.Text = "layoutControlItemFrom";
            this.layoutControlItemFrom.TextSize = new System.Drawing.Size(144, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 48);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(438, 56);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItemTo
            // 
            this.layoutControlItemTo.Control = this.dateEditTo;
            this.layoutControlItemTo.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItemTo.Location = new System.Drawing.Point(218, 24);
            this.layoutControlItemTo.Name = "layoutControlItemTo";
            this.layoutControlItemTo.Size = new System.Drawing.Size(220, 24);
            this.layoutControlItemTo.Text = "layoutControlItemTo";
            this.layoutControlItemTo.TextSize = new System.Drawing.Size(144, 13);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(438, 0);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(32, 104);
            this.emptySpaceItem5.Text = "emptySpaceItem5";
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ForecastOperationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.simpleButtonCancel;
            this.ClientSize = new System.Drawing.Size(992, 706);
            this.Controls.Add(this.layoutControlForecast);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(1000, 740);
            this.Name = "ForecastOperationForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ForecastOperationForm";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ForecastOperationForm_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlForecast)).EndInit();
            this.layoutControlForecast.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEditName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTo.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditDepartment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFrom.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlForecastDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewForecastDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditLow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditHigh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupParameters)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDepartment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal GridControlEx gridControlForecastDetails;
        private GridViewEx gridViewForecastDetail;
        internal DevExpress.XtraGrid.Columns.GridColumn gridColumnIndicator;
        internal DevExpress.XtraGrid.Columns.GridColumn gridColumnIndicatorType;
        internal DevExpress.XtraGrid.Columns.GridColumn gridColumnPostivePattern;
        internal DevExpress.XtraGrid.Columns.GridColumn gridColumnLow;
        internal DevExpress.XtraGrid.Columns.GridColumn gridColumnHigh;
        internal DevExpress.XtraGrid.Columns.GridColumn gridColumnMeasureUnit;
        internal DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditLow;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditHigh;
        private DevExpress.XtraEditors.SimpleButton simpleButtonApply;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCancel;
        internal DevExpress.XtraEditors.TextEdit textEditName;
        internal DevExpress.XtraEditors.MemoEdit memoEditDescription;
        internal DevExpress.XtraEditors.DateEdit dateEditTo;
        internal DevExpress.XtraEditors.DateEdit dateEditFrom;
        internal DevExpress.XtraEditors.ComboBoxEdit comboBoxEditDepartment;
        private DevExpress.XtraLayout.LayoutControl layoutControlForecast;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemName;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDescription;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupInfo;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDepartment;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemFrom;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTo;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupParameters;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;

    }
}