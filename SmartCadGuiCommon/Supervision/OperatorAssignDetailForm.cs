using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraScheduler;
using System.Collections;
using Smartmatic.SmartCad.Service;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadCore.Core;
using SmartCadControls;
using SmartCadControls.Controls;

namespace SmartCadGuiCommon
{
    public partial class OperatorAssignDetailForm : DevExpress.XtraEditors.XtraForm
    {
        public OperatorAssignDetailForm()
        {
            InitializeComponent();
            InitializeDataGrids();
            LoadLanguage();

        }

        public OperatorAssignDetailForm(Appointment app) 
        :this(){

            ShowAppointmentInformation(app);
         
        }

        public OperatorAssignDetailForm(OperatorClientData oper, WorkShiftScheduleVariationClientData schedule)
            : this()
        {

            ShowAssignmentDetail(oper, schedule);

            layoutControlGroupWithoutSupervision.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            this.MinimumSize = new Size(this.MinimumSize.Width, this.MinimumSize.Height - 150);
            this.Size = this.MinimumSize;

        }

        private void LoadLanguage()
        {
            layoutControlItemName.Text = ResourceLoader.GetString2("Name") + ":";
            layoutControlItemWSName.Text = ResourceLoader.GetString2("Schedule") + ":";
            layoutControlItemBegin.Text = ResourceLoader.GetString2("StartDate") + ":";
            layoutControlItemEnd.Text = ResourceLoader.GetString2("EndDate") + ":";
            layoutControlGroupInf.Text = ResourceLoader.GetString2("Data");
            layoutControlGroupAssignments.Text = ResourceLoader.GetString2("TimeAssigned");
            simpleButtonAccept.Text = ResourceLoader.GetString2("Accept");
            layoutControlGroupWithoutSupervision.Text = ResourceLoader.GetString2("TimeNotAssigned");

            this.Text = ResourceLoader.GetString2("AssignmentDetail");

        }

        private void InitializeDataGrids() 
        {
            gridControlExAssignments.Type = typeof(GridControlDataOperatorAssignDetail);
            gridControlExAssignments.ViewTotalRows = true;
            gridControlExAssignments.EnableAutoFilter = true;
            gridViewExAssignments.ViewTotalRows = false;
            gridViewExAssignments.OptionsView.ColumnAutoWidth = true;

            this.gridControlExWithoutSupervision.Type = typeof(IntervalScheduleGridData);
            gridControlExWithoutSupervision.ViewTotalRows = true;
            gridControlExWithoutSupervision.EnableAutoFilter = true;
            gridViewExWithoutSupervision.ViewTotalRows = false;
            gridViewExWithoutSupervision.OptionsView.ColumnAutoWidth = true;
        
        }

		private void ShowAppointmentInformation(Appointment app)
		{			
            OperatorClientData oper = new OperatorClientData();
            oper.Code = (int)app.CustomFields["OperatorCode"];
            oper = (OperatorClientData)ServerServiceClient.GetInstance().RefreshClient(oper);

            WorkShiftScheduleVariationClientData schedule = new WorkShiftScheduleVariationClientData();
            schedule.Code = (int)app.CustomFields["ScheduleCode"];
            schedule = (WorkShiftScheduleVariationClientData)ServerServiceClient.GetInstance().RefreshClient(schedule);
            
			labelControlName.Text = oper.FirstName + " " + oper.LastName;
			labelControlWorkShift.Text = app.Subject;
            labelControlBegin.Text = app.Start.ToString("MM/dd/yyyy  HH:mm");
            labelControlEnd.Text = app.End.ToString("MM/dd/yyyy  HH:mm");

            if (oper.IsSupervisor == false)
                gridViewExAssignments.Columns["Operator"].Visible = false; 
            else
                gridViewExAssignments.Columns["Supervisor"].Visible = false;
            
            IList assigns = FillOperatorAssigns(oper, schedule.Code);
            
            FillNotAssignedTime(assigns, schedule);
            
            
		}


        private void ShowAssignmentDetail(OperatorClientData oper, WorkShiftScheduleVariationClientData schedule) 
        {                        
            labelControlName.Text = oper.FirstName + " " + oper.LastName;
            labelControlWorkShift.Text = schedule.WorkShiftVariationName;
            labelControlBegin.Text = schedule.Start.ToString("MM/dd/yyyy  HH:mm");
            labelControlEnd.Text = schedule.End.ToString("MM/dd/yyyy  HH:mm");

            if (oper.IsSupervisor == false)
                gridViewExAssignments.Columns["Operator"].Visible = false;
            else
                gridViewExAssignments.Columns["Supervisor"].Visible = false;

            FillOperatorAssigns(oper, schedule.Code);
                    
        }

        private IList FillOperatorAssigns(OperatorClientData oper, int scheduleCode) 
        {   
            IList assigns; 
            if (oper.IsSupervisor == false)    
                assigns = (IList)ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(@"SELECT assign FROM OperatorAssignData assign WHERE assign.SupervisedOperator.Code = {0} AND assign.SupervisedScheduleVariation.Code = {1} order by assign.StartDate", oper.Code, scheduleCode));
            else
                assigns = (IList)ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(@"SELECT assign FROM OperatorAssignData assign WHERE assign.Supervisor.Code = {0} AND assign.SupervisorScheduleVariation.Code = {1} order by assign.StartDate", oper.Code, scheduleCode));
            
            gridControlExAssignments.SetDataSource(assigns);

            return assigns;
        }

        private void FillNotAssignedTime(IList assigns, WorkShiftScheduleVariationClientData schedule)
        {
            BindingList<IntervalScheduleGridData> dataSource = new BindingList<IntervalScheduleGridData>();
            DateTime start = schedule.Start;

            foreach (OperatorAssignClientData operAssign in assigns)
            {
                if (operAssign.StartDate > start)
                    dataSource.Add(new IntervalScheduleGridData(start, operAssign.StartDate));
                
                start = operAssign.EndDate;
            }

            if (start != schedule.End)
                dataSource.Add(new IntervalScheduleGridData(start, schedule.End));

            gridViewExWithoutSupervision.BeginUpdate();
            gridControlExWithoutSupervision.DataSource = dataSource;
            gridViewExWithoutSupervision.EndUpdate();


        }
    }


    public class IntervalScheduleGridData 
    {

        private string date; 
        private string startTime;
        private string endTime;


        public IntervalScheduleGridData() 
        {
        
        }

         public IntervalScheduleGridData(DateTime start, DateTime end) 
        {
            date = start.Date.ToString("MM/dd/yyyy");
            startTime = start.ToString("HH:mm"); ;
            endTime = end.ToString("HH:mm"); ;
        }

         [GridControlRowInfoData(Visible = true)]
        public string Date 
        {

            get {
                return date;
            }
            set {
                date = value;
            }
        }

         [GridControlRowInfoData(Visible = true)]
        public string StartTime
        {

            get
            {
                return  startTime;
            }
            set
            {
                startTime = value;
            }
        }

         [GridControlRowInfoData(Visible = true)]
        public string EndTime
        {

            get
            {
                return endTime;
            }
            set
            {
                endTime = value;
            }
        }
    
    
    }

    public class GridControlDataOperatorAssignDetail : GridControlData
    {

        
        public GridControlDataOperatorAssignDetail(OperatorAssignClientData operAssign)
            : base(operAssign)
        {

        }

        public OperatorAssignClientData OperatorAssign
        {
            get { return this.Tag as OperatorAssignClientData; }
        }

        public int Code
        {
            get { return OperatorAssign.Code; }
        }

               
              
        [GridControlRowInfoData(Visible = true)]
        public string Date
        {
            get
            {
                return OperatorAssign.StartDate.ToString("MM/dd/yyyy");
            }
        }


        [GridControlRowInfoData(Visible = true)]
        public string StartTime
        {
            get
            {
                return OperatorAssign.StartDate.ToString("HH:mm");
            }
        }

        [GridControlRowInfoData(Visible = true)]
        public string EndTime
        {
            get
            {
                return OperatorAssign.EndDate.ToString("HH:mm");
            }
        }

        [GridControlRowInfoData(Visible = true)]
        public string Supervisor
        {
            get { return OperatorAssign.SupFirstName + " " + OperatorAssign.SupLastName; }
        }

        [GridControlRowInfoData(Visible = true)]
        public string Operator
        {
            get { return OperatorAssign.OperFirstName + " " + OperatorAssign.OperLastName; }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            GridControlDataOperatorAssignDetail var = obj as GridControlDataOperatorAssignDetail;
            if (var != null)
            {
                OperatorAssignClientData oad = (OperatorAssignClientData)var.OperatorAssign;
                if (this.OperatorAssign.Equals(oad) == true)
                    result = true;

            }
            return result;

        }

    }



}