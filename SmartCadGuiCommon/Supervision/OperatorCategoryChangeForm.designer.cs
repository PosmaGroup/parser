using SmartCadControls.Controls;
namespace SmartCadGuiCommon
{
    partial class OperatorCategoryChangeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OperatorCategoryChangeForm));
            this.buttonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.buttonOK = new DevExpress.XtraEditors.SimpleButton();
            this.textBoxDesc = new DevExpress.XtraEditors.MemoEdit();
            this.layoutControlCategoryChange = new DevExpress.XtraLayout.LayoutControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroupInfo = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroupDescription = new DevExpress.XtraLayout.LayoutControlGroup();
            this.labelNextCategory = new LabelEx();
            this.labelDate = new LabelEx();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemSince = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemCategory = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemDes = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxDesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCategoryChange)).BeginInit();
            this.layoutControlCategoryChange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemSince)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDes)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonCancel
            // 
            this.buttonCancel.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCancel.Appearance.Options.UseFont = true;
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(268, 207);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 25);
            this.buttonCancel.StyleController = this.layoutControlCategoryChange;
            this.buttonCancel.TabIndex = 11;
            this.buttonCancel.Text = "Cancelar";
            // 
            // buttonOK
            // 
            this.buttonOK.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOK.Appearance.Options.UseFont = true;
            this.buttonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOK.Location = new System.Drawing.Point(182, 207);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 25);
            this.buttonOK.StyleController = this.layoutControlCategoryChange;
            this.buttonOK.TabIndex = 10;
            this.buttonOK.Text = "Accept";
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // textBoxDesc
            // 
            this.textBoxDesc.Enabled = false;
            this.textBoxDesc.Location = new System.Drawing.Point(10, 112);
            this.textBoxDesc.Name = "textBoxDesc";
            this.textBoxDesc.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textBoxDesc.Properties.Appearance.BackColor2 = System.Drawing.Color.White;
            this.textBoxDesc.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxDesc.Properties.Appearance.Options.UseBackColor = true;
            this.textBoxDesc.Properties.Appearance.Options.UseFont = true;
            this.textBoxDesc.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White;
            this.textBoxDesc.Properties.AppearanceDisabled.BackColor2 = System.Drawing.Color.White;
            this.textBoxDesc.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.textBoxDesc.Properties.AppearanceFocused.BackColor = System.Drawing.Color.White;
            this.textBoxDesc.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.White;
            this.textBoxDesc.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.textBoxDesc.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White;
            this.textBoxDesc.Properties.AppearanceReadOnly.BackColor2 = System.Drawing.Color.White;
            this.textBoxDesc.Properties.AppearanceReadOnly.Options.UseBackColor = true;
            this.textBoxDesc.Size = new System.Drawing.Size(330, 81);
            this.textBoxDesc.StyleController = this.layoutControlCategoryChange;
            this.textBoxDesc.TabIndex = 0;
            // 
            // layoutControlCategoryChange
            // 
            this.layoutControlCategoryChange.AllowCustomizationMenu = false;
            this.layoutControlCategoryChange.Appearance.DisabledLayoutGroupCaption.ForeColor = System.Drawing.SystemColors.GrayText;
            this.layoutControlCategoryChange.Appearance.DisabledLayoutGroupCaption.Options.UseForeColor = true;
            this.layoutControlCategoryChange.Appearance.DisabledLayoutItem.ForeColor = System.Drawing.SystemColors.GrayText;
            this.layoutControlCategoryChange.Appearance.DisabledLayoutItem.Options.UseForeColor = true;
            this.layoutControlCategoryChange.Controls.Add(this.buttonCancel);
            this.layoutControlCategoryChange.Controls.Add(this.textBoxDesc);
            this.layoutControlCategoryChange.Controls.Add(this.buttonOK);
            this.layoutControlCategoryChange.Controls.Add(this.labelNextCategory);
            this.layoutControlCategoryChange.Controls.Add(this.labelDate);
            this.layoutControlCategoryChange.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlCategoryChange.Location = new System.Drawing.Point(0, 0);
            this.layoutControlCategoryChange.Name = "layoutControlCategoryChange";
            this.layoutControlCategoryChange.Root = this.layoutControlGroup1;
            this.layoutControlCategoryChange.Size = new System.Drawing.Size(349, 238);
            this.layoutControlCategoryChange.TabIndex = 1;
            this.layoutControlCategoryChange.Text = "layoutControl1";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem1,
            this.emptySpaceItem1,
            this.layoutControlGroupInfo,
            this.layoutControlGroupDescription});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(349, 238);
            this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 200);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(175, 36);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroupInfo
            // 
            this.layoutControlGroupInfo.AppearanceGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroupInfo.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroupInfo.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroupInfo.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlGroupInfo.CustomizationFormText = "layoutControlGroupInfo";
            this.layoutControlGroupInfo.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemSince,
            this.layoutControlItemCategory});
            this.layoutControlGroupInfo.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupInfo.Name = "layoutControlGroupInfo";
            this.layoutControlGroupInfo.Size = new System.Drawing.Size(347, 85);
            this.layoutControlGroupInfo.Text = "layoutControlGroupInfo";
            // 
            // layoutControlGroupDescription
            // 
            this.layoutControlGroupDescription.AppearanceGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroupDescription.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroupDescription.CustomizationFormText = "layoutControlGroupDescription";
            this.layoutControlGroupDescription.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemDes});
            this.layoutControlGroupDescription.Location = new System.Drawing.Point(0, 85);
            this.layoutControlGroupDescription.Name = "layoutControlGroupDescription";
            this.layoutControlGroupDescription.Size = new System.Drawing.Size(347, 115);
            this.layoutControlGroupDescription.Text = "layoutControlGroupDescription";
            // 
            // labelNextCategory
            // 
            this.labelNextCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNextCategory.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelNextCategory.Location = new System.Drawing.Point(138, 58);
            this.labelNextCategory.Name = "labelNextCategory";
            this.labelNextCategory.Size = new System.Drawing.Size(202, 20);
            this.labelNextCategory.TabIndex = 2;
            this.labelNextCategory.Text = "labelNextCategory";
            this.labelNextCategory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelDate
            // 
            this.labelDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDate.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelDate.Location = new System.Drawing.Point(138, 27);
            this.labelDate.Name = "labelDate";
            this.labelDate.Size = new System.Drawing.Size(202, 20);
            this.labelDate.TabIndex = 1;
            this.labelDate.Text = "labelDate";
            this.labelDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.buttonCancel;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(261, 200);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.buttonOK;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(175, 200);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(86, 36);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(86, 36);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItemSince
            // 
            this.layoutControlItemSince.Control = this.labelDate;
            this.layoutControlItemSince.CustomizationFormText = "layoutControlItemSince";
            this.layoutControlItemSince.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemSince.Name = "layoutControlItemSince";
            this.layoutControlItemSince.Size = new System.Drawing.Size(341, 31);
            this.layoutControlItemSince.Text = "layoutControlItemSince";
            this.layoutControlItemSince.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemSince.TextSize = new System.Drawing.Size(123, 20);
            // 
            // layoutControlItemCategory
            // 
            this.layoutControlItemCategory.Control = this.labelNextCategory;
            this.layoutControlItemCategory.CustomizationFormText = "layoutControlItemCategory";
            this.layoutControlItemCategory.Location = new System.Drawing.Point(0, 31);
            this.layoutControlItemCategory.Name = "layoutControlItemCategory";
            this.layoutControlItemCategory.Size = new System.Drawing.Size(341, 31);
            this.layoutControlItemCategory.Text = "layoutControlItemCategory";
            this.layoutControlItemCategory.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemCategory.TextSize = new System.Drawing.Size(123, 20);
            // 
            // layoutControlItemDes
            // 
            this.layoutControlItemDes.Control = this.textBoxDesc;
            this.layoutControlItemDes.CustomizationFormText = "layoutControlItemDes";
            this.layoutControlItemDes.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemDes.Name = "layoutControlItemDes";
            this.layoutControlItemDes.Size = new System.Drawing.Size(341, 92);
            this.layoutControlItemDes.Text = "layoutControlItemDes";
            this.layoutControlItemDes.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemDes.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemDes.TextToControlDistance = 0;
            this.layoutControlItemDes.TextVisible = false;
            // 
            // OperatorCategoryChangeForm
            // 
            this.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(349, 238);
            this.Controls.Add(this.layoutControlCategoryChange);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(357, 272);
            this.Name = "OperatorCategoryChangeForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "OperatorCategoryChangeForm";
            ((System.ComponentModel.ISupportInitialize)(this.textBoxDesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCategoryChange)).EndInit();
            this.layoutControlCategoryChange.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemSince)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDes)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton buttonCancel;
        private DevExpress.XtraEditors.SimpleButton buttonOK;
        private DevExpress.XtraEditors.MemoEdit textBoxDesc;
        private LabelEx labelNextCategory;
        private LabelEx labelDate;
        private DevExpress.XtraLayout.LayoutControl layoutControlCategoryChange;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupInfo;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemSince;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemCategory;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupDescription;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDes;

    }
}