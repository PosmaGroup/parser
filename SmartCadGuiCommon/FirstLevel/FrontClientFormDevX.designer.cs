namespace SmartCadGuiCommon
{
	partial class FrontClientFormDevX
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrontClientFormDevX));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.alertControl1 = new DevExpress.XtraBars.Alerter.AlertControl(this.components);
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barButtonItemHelp = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemStatus = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemOpenMap = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemChat = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemPrint = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemSave = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.barEditItemProgress = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemProgressBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.barButtonItemTelephone = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemCreateNewIncident = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemAddToIncident = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemFinalize = new DevExpress.XtraBars.BarButtonItem();
            this.barEditItemChronometer = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemTimeEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit();
            this.barButtonGroupTelephone = new DevExpress.XtraBars.BarButtonGroup();
            this.barButtonItemPhoneReboot = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemVirtualCall = new DevExpress.XtraBars.BarButtonItem();
            this.barCheckItemIncompleteCall = new DevExpress.XtraBars.BarCheckItem();
            this.barStaticItemTimeBar = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChonometer = new DevExpress.XtraBars.BarStaticItem();
            this.barButtonItemParkCall = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemHistory = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemLogo = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemCallBack = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroupCallInfo = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupTelephone = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupTool = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupUser = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupGeneralOptions = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupApplications = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonStatusBar1 = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.labelUser = new DevExpress.XtraBars.BarStaticItem();
            this.labelConnected = new DevExpress.XtraBars.BarStaticItem();
            this.labelDate = new DevExpress.XtraBars.BarStaticItem();
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.xtraTabbedMdiManager1 = new DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(this.components);
            this.labelLogo = new System.Windows.Forms.Label();
            this.ribbonPage2 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.barButtonItemStartRegIncident = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemCancelIncident = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem5 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem6 = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPageGroupTools = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.barButtonItem7 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem8 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem9 = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // alertControl1
            // 
            this.alertControl1.LookAndFeel.SkinName = "Blue";
            this.alertControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.alertControl1.AlertClick += new DevExpress.XtraBars.Alerter.AlertClickEventHandler(this.alertControl1_AlertClick);
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.ExpandCollapseItem.Name = "";
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.barButtonItemHelp,
            this.barButtonItemStatus,
            this.barButtonItemOpenMap,
            this.barButtonItemChat,
            this.barButtonItemPrint,
            this.barButtonItemSave,
            this.barButtonItemRefresh,
            this.barEditItemProgress,
            this.barButtonItemTelephone,
            this.barButtonItemCreateNewIncident,
            this.barButtonItemAddToIncident,
            this.barButtonItemFinalize,
            this.barEditItemChronometer,
            this.barButtonGroupTelephone,
            this.barButtonItemPhoneReboot,
            this.barButtonItemVirtualCall,
            this.barCheckItemIncompleteCall,
            this.barStaticItemTimeBar,
            this.barStaticItemChonometer,
            this.barButtonItemParkCall,
            this.barButtonItemHistory,
            this.barButtonItemLogo,
            this.barButtonItemCallBack});
            this.ribbonControl1.ItemsVertAlign = DevExpress.Utils.VertAlignment.Top;
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 42;
            this.ribbonControl1.MdiMergeStyle = DevExpress.XtraBars.Ribbon.RibbonMdiMergeStyle.Always;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.PageHeaderItemLinks.Add(this.barButtonItemHelp);
            this.ribbonControl1.PageHeaderItemLinks.Add(this.barButtonItemLogo);
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbonControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTimeEdit1,
            this.repositoryItemProgressBar1});
            this.ribbonControl1.ShowToolbarCustomizeItem = false;
            this.ribbonControl1.Size = new System.Drawing.Size(1370, 119);
            this.ribbonControl1.StatusBar = this.ribbonStatusBar1;
            this.ribbonControl1.Toolbar.ShowCustomizeItem = false;
            this.ribbonControl1.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            this.ribbonControl1.Click += new System.EventHandler(this.ribbonControl1_Click);
            // 
            // barButtonItemHelp
            // 
            this.barButtonItemHelp.Caption = "Help";
            this.barButtonItemHelp.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemHelp.Glyph")));
            this.barButtonItemHelp.Id = 0;
            this.barButtonItemHelp.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F1);
            this.barButtonItemHelp.Name = "barButtonItemHelp";
            this.barButtonItemHelp.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemHelp_ItemClick);
            // 
            // barButtonItemStatus
            // 
            this.barButtonItemStatus.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.barButtonItemStatus.Caption = "ItemStatus";
            this.barButtonItemStatus.Enabled = false;
            this.barButtonItemStatus.Id = 3;
            this.barButtonItemStatus.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemStatus.Name = "barButtonItemStatus";
            this.barButtonItemStatus.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItemStatus.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemStatus_ItemClick);
            // 
            // barButtonItemOpenMap
            // 
            this.barButtonItemOpenMap.Caption = "ItemOpenMap";
            this.barButtonItemOpenMap.Id = 4;
            this.barButtonItemOpenMap.ItemShortcut = new DevExpress.XtraBars.BarShortcut(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
                | System.Windows.Forms.Keys.M));
            this.barButtonItemOpenMap.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemOpenMap.LargeGlyph")));
            this.barButtonItemOpenMap.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemOpenMap.Name = "barButtonItemOpenMap";
            this.barButtonItemOpenMap.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItemOpenMap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemOpenMap_ItemClick);
            // 
            // barButtonItemChat
            // 
            this.barButtonItemChat.Caption = "ItemChat";
            this.barButtonItemChat.Id = 5;
            this.barButtonItemChat.ItemShortcut = new DevExpress.XtraBars.BarShortcut(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
                | System.Windows.Forms.Keys.C));
            this.barButtonItemChat.LargeGlyph = global::SmartCadGuiCommon.Properties.ResourcesGui.chat;
            this.barButtonItemChat.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemChat.Name = "barButtonItemChat";
            this.barButtonItemChat.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItemChat.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemChat_ItemClick);
            // 
            // barButtonItemPrint
            // 
            this.barButtonItemPrint.Caption = "ItemPrint";
            this.barButtonItemPrint.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPrint.Glyph")));
            this.barButtonItemPrint.Id = 6;
            this.barButtonItemPrint.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P));
            this.barButtonItemPrint.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemPrint.Name = "barButtonItemPrint";
            // 
            // barButtonItemSave
            // 
            this.barButtonItemSave.Caption = "ItemSave";
            this.barButtonItemSave.Glyph = global::SmartCadGuiCommon.PrintRibbonControllerResources.RibbonPrintPreview_Save;
            this.barButtonItemSave.Id = 7;
            this.barButtonItemSave.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S));
            this.barButtonItemSave.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemSave.Name = "barButtonItemSave";
            // 
            // barButtonItemRefresh
            // 
            this.barButtonItemRefresh.Caption = "ItemRefresh";
            this.barButtonItemRefresh.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemRefresh.Glyph")));
            this.barButtonItemRefresh.Id = 8;
            this.barButtonItemRefresh.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F5);
            this.barButtonItemRefresh.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemRefresh.Name = "barButtonItemRefresh";
            // 
            // barEditItemProgress
            // 
            this.barEditItemProgress.CanOpenEdit = false;
            this.barEditItemProgress.Edit = this.repositoryItemProgressBar1;
            this.barEditItemProgress.Id = 11;
            this.barEditItemProgress.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barEditItemProgress.Name = "barEditItemProgress";
            this.barEditItemProgress.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barEditItemProgress.Width = 120;
            // 
            // repositoryItemProgressBar1
            // 
            this.repositoryItemProgressBar1.Appearance.BorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.repositoryItemProgressBar1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.repositoryItemProgressBar1.EndColor = System.Drawing.Color.Lime;
            this.repositoryItemProgressBar1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            this.repositoryItemProgressBar1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.repositoryItemProgressBar1.Name = "repositoryItemProgressBar1";
            this.repositoryItemProgressBar1.ProgressViewStyle = DevExpress.XtraEditors.Controls.ProgressViewStyle.Solid;
            this.repositoryItemProgressBar1.StartColor = System.Drawing.Color.Lime;
            this.repositoryItemProgressBar1.Step = 1;
            // 
            // barButtonItemTelephone
            // 
            this.barButtonItemTelephone.Caption = "ItemTelephone";
            this.barButtonItemTelephone.Enabled = false;
            this.barButtonItemTelephone.Id = 12;
            this.barButtonItemTelephone.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemTelephone.LargeGlyph")));
            this.barButtonItemTelephone.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemTelephone.Name = "barButtonItemTelephone";
            this.barButtonItemTelephone.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonItemCreateNewIncident
            // 
            this.barButtonItemCreateNewIncident.Caption = "ItemCreateNewIncident";
            this.barButtonItemCreateNewIncident.Enabled = false;
            this.barButtonItemCreateNewIncident.Id = 13;
            this.barButtonItemCreateNewIncident.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemCreateNewIncident.LargeGlyph")));
            this.barButtonItemCreateNewIncident.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemCreateNewIncident.Name = "barButtonItemCreateNewIncident";
            this.barButtonItemCreateNewIncident.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonItemAddToIncident
            // 
            this.barButtonItemAddToIncident.Caption = "ItemAddToIncident";
            this.barButtonItemAddToIncident.Enabled = false;
            this.barButtonItemAddToIncident.Id = 14;
            this.barButtonItemAddToIncident.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemAddToIncident.LargeGlyph")));
            this.barButtonItemAddToIncident.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemAddToIncident.Name = "barButtonItemAddToIncident";
            this.barButtonItemAddToIncident.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonItemFinalize
            // 
            this.barButtonItemFinalize.Caption = "ItemFinalize";
            this.barButtonItemFinalize.Enabled = false;
            this.barButtonItemFinalize.Id = 15;
            this.barButtonItemFinalize.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemFinalize.LargeGlyph")));
            this.barButtonItemFinalize.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemFinalize.Name = "barButtonItemFinalize";
            this.barButtonItemFinalize.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItemFinalize.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemFinalize_ItemClick);
            // 
            // barEditItemChronometer
            // 
            this.barEditItemChronometer.CanOpenEdit = false;
            this.barEditItemChronometer.CaptionAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.barEditItemChronometer.Edit = this.repositoryItemTimeEdit1;
            this.barEditItemChronometer.EditValue = "00:00:00";
            this.barEditItemChronometer.Id = 17;
            this.barEditItemChronometer.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barEditItemChronometer.Name = "barEditItemChronometer";
            this.barEditItemChronometer.Width = 90;
            // 
            // repositoryItemTimeEdit1
            // 
            this.repositoryItemTimeEdit1.AllowFocused = false;
            this.repositoryItemTimeEdit1.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemTimeEdit1.Appearance.Font = new System.Drawing.Font("MS Reference Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.repositoryItemTimeEdit1.Appearance.Options.UseFont = true;
            this.repositoryItemTimeEdit1.AutoHeight = false;
            this.repositoryItemTimeEdit1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.repositoryItemTimeEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "", -1, true, false, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, false)});
            this.repositoryItemTimeEdit1.Mask.EditMask = "";
            this.repositoryItemTimeEdit1.Name = "repositoryItemTimeEdit1";
            // 
            // barButtonGroupTelephone
            // 
            this.barButtonGroupTelephone.Caption = "GroupTelephone";
            this.barButtonGroupTelephone.Enabled = false;
            this.barButtonGroupTelephone.Id = 18;
            this.barButtonGroupTelephone.Name = "barButtonGroupTelephone";
            this.barButtonGroupTelephone.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            // 
            // barButtonItemPhoneReboot
            // 
            this.barButtonItemPhoneReboot.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Left;
            this.barButtonItemPhoneReboot.Caption = "PhoneReboot";
            this.barButtonItemPhoneReboot.Enabled = false;
            this.barButtonItemPhoneReboot.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPhoneReboot.Glyph")));
            this.barButtonItemPhoneReboot.Id = 19;
            this.barButtonItemPhoneReboot.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemPhoneReboot.Name = "barButtonItemPhoneReboot";
            this.barButtonItemPhoneReboot.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemPhoneReboot.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.barButtonItemPhoneReboot.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemPhoneReboot_ItemClick);
            // 
            // barButtonItemVirtualCall
            // 
            this.barButtonItemVirtualCall.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Left;
            this.barButtonItemVirtualCall.Caption = "VirtualCall";
            this.barButtonItemVirtualCall.Enabled = false;
            this.barButtonItemVirtualCall.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemVirtualCall.Glyph")));
            this.barButtonItemVirtualCall.Id = 20;
            this.barButtonItemVirtualCall.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemVirtualCall.Name = "barButtonItemVirtualCall";
            this.barButtonItemVirtualCall.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemVirtualCall.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemVirtualCall_ItemClick);
            // 
            // barCheckItemIncompleteCall
            // 
            this.barCheckItemIncompleteCall.Caption = "barCheckItemIncompleteCall";
            this.barCheckItemIncompleteCall.Enabled = false;
            this.barCheckItemIncompleteCall.Id = 22;
            this.barCheckItemIncompleteCall.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barCheckItemIncompleteCall.LargeGlyph")));
            this.barCheckItemIncompleteCall.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barCheckItemIncompleteCall.Name = "barCheckItemIncompleteCall";
            this.barCheckItemIncompleteCall.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barStaticItemTimeBar
            // 
            this.barStaticItemTimeBar.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barStaticItemTimeBar.Caption = "Barra de tiempo";
            this.barStaticItemTimeBar.Enabled = false;
            this.barStaticItemTimeBar.Id = 25;
            this.barStaticItemTimeBar.LeftIndent = 33;
            this.barStaticItemTimeBar.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barStaticItemTimeBar.Name = "barStaticItemTimeBar";
            this.barStaticItemTimeBar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.Caption;
            this.barStaticItemTimeBar.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChonometer
            // 
            this.barStaticItemChonometer.Caption = "Cronometro";
            this.barStaticItemChonometer.Enabled = false;
            this.barStaticItemChonometer.Id = 26;
            this.barStaticItemChonometer.LeftIndent = 18;
            this.barStaticItemChonometer.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barStaticItemChonometer.Name = "barStaticItemChonometer";
            this.barStaticItemChonometer.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.Caption;
            this.barStaticItemChonometer.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // barButtonItemParkCall
            // 
            this.barButtonItemParkCall.Caption = "Park call";
            this.barButtonItemParkCall.Enabled = false;
            this.barButtonItemParkCall.Glyph = global::SmartCadGuiCommon.PrintRibbonControllerResources.RibbonPrintPreview_ClosePreview;
            this.barButtonItemParkCall.Id = 29;
            this.barButtonItemParkCall.Name = "barButtonItemParkCall";
            this.barButtonItemParkCall.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.barButtonItemParkCall.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemParkCall_ItemClick);
            // 
            // barButtonItemHistory
            // 
            this.barButtonItemHistory.Caption = "ItemHistory";
            this.barButtonItemHistory.Id = 30;
            this.barButtonItemHistory.ItemShortcut = new DevExpress.XtraBars.BarShortcut(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
                | System.Windows.Forms.Keys.I));
            this.barButtonItemHistory.LargeGlyph = global::SmartCadGuiCommon.Properties.ResourcesGui.Historial;
            this.barButtonItemHistory.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemHistory.Name = "barButtonItemHistory";
            this.barButtonItemHistory.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItemHistory.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemHistory_ItemClick);
            // 
            // barButtonItemLogo
            // 
            this.barButtonItemLogo.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemLogo.Glyph")));
            this.barButtonItemLogo.Id = 40;
            this.barButtonItemLogo.Name = "barButtonItemLogo";
            this.barButtonItemLogo.SmallWithoutTextWidth = 57;
            // 
            // barButtonItemCallBack
            // 
            this.barButtonItemCallBack.Caption = "ItemCallBack";
            this.barButtonItemCallBack.Enabled = false;
            this.barButtonItemCallBack.Id = 41;
            this.barButtonItemCallBack.LargeGlyph = global::SmartCadGuiCommon.Properties.ResourcesGui.Call;
            this.barButtonItemCallBack.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemCallBack.Name = "barButtonItemCallBack";
            this.barButtonItemCallBack.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupCallInfo,
            this.ribbonPageGroupTelephone,
            this.ribbonPageGroupTool,
            this.ribbonPageGroupUser,
            this.ribbonPageGroupGeneralOptions,
            this.ribbonPageGroupApplications});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "ribbonPage1";
            // 
            // ribbonPageGroupCallInfo
            // 
            this.ribbonPageGroupCallInfo.AllowMinimize = false;
            this.ribbonPageGroupCallInfo.AllowTextClipping = false;
            this.ribbonPageGroupCallInfo.ItemLinks.Add(this.barButtonItemTelephone, true);
            this.ribbonPageGroupCallInfo.ItemLinks.Add(this.barCheckItemIncompleteCall, true);
            this.ribbonPageGroupCallInfo.ItemLinks.Add(this.barButtonItemCallBack);
            this.ribbonPageGroupCallInfo.ItemLinks.Add(this.barButtonItemCreateNewIncident);
            this.ribbonPageGroupCallInfo.ItemLinks.Add(this.barButtonItemAddToIncident);
            this.ribbonPageGroupCallInfo.ItemLinks.Add(this.barButtonItemFinalize);
            this.ribbonPageGroupCallInfo.ItemLinks.Add(this.barEditItemProgress, true, "", "", true);
            this.ribbonPageGroupCallInfo.ItemLinks.Add(this.barStaticItemTimeBar, false, "", "", true);
            this.ribbonPageGroupCallInfo.ItemLinks.Add(this.barEditItemChronometer, true, "", "", true);
            this.ribbonPageGroupCallInfo.ItemLinks.Add(this.barStaticItemChonometer, false, "", "", true);
            this.ribbonPageGroupCallInfo.Name = "ribbonPageGroupCallInfo";
            this.ribbonPageGroupCallInfo.ShowCaptionButton = false;
            this.ribbonPageGroupCallInfo.Text = "ribbonPageGroupCallInfo";
            // 
            // ribbonPageGroupTelephone
            // 
            this.ribbonPageGroupTelephone.AllowMinimize = false;
            this.ribbonPageGroupTelephone.AllowTextClipping = false;
            this.ribbonPageGroupTelephone.ItemLinks.Add(this.barButtonItemPhoneReboot);
            this.ribbonPageGroupTelephone.ItemLinks.Add(this.barButtonItemVirtualCall);
            this.ribbonPageGroupTelephone.ItemLinks.Add(this.barButtonItemParkCall);
            this.ribbonPageGroupTelephone.Name = "ribbonPageGroupTelephone";
            this.ribbonPageGroupTelephone.ShowCaptionButton = false;
            this.ribbonPageGroupTelephone.Text = "ribbonGroupTelephone";
            // 
            // ribbonPageGroupTool
            // 
            this.ribbonPageGroupTool.AllowMinimize = false;
            this.ribbonPageGroupTool.AllowTextClipping = false;
            this.ribbonPageGroupTool.ItemLinks.Add(this.barButtonItemChat);
            this.ribbonPageGroupTool.ItemLinks.Add(this.barButtonItemHistory);
            this.ribbonPageGroupTool.Name = "ribbonPageGroupTool";
            this.ribbonPageGroupTool.ShowCaptionButton = false;
            this.ribbonPageGroupTool.Text = "ribbonPageGroupTool";
            // 
            // ribbonPageGroupUser
            // 
            this.ribbonPageGroupUser.AllowMinimize = false;
            this.ribbonPageGroupUser.AllowTextClipping = false;
            this.ribbonPageGroupUser.ItemLinks.Add(this.barButtonItemStatus);
            this.ribbonPageGroupUser.Name = "ribbonPageGroupUser";
            this.ribbonPageGroupUser.ShowCaptionButton = false;
            this.ribbonPageGroupUser.Text = "ribbonPageGroupUser";
            // 
            // ribbonPageGroupGeneralOptions
            // 
            this.ribbonPageGroupGeneralOptions.AllowMinimize = false;
            this.ribbonPageGroupGeneralOptions.AllowTextClipping = false;
            this.ribbonPageGroupGeneralOptions.ItemLinks.Add(this.barButtonItemPrint);
            this.ribbonPageGroupGeneralOptions.ItemLinks.Add(this.barButtonItemSave);
            this.ribbonPageGroupGeneralOptions.ItemLinks.Add(this.barButtonItemRefresh);
            this.ribbonPageGroupGeneralOptions.Name = "ribbonPageGroupGeneralOptions";
            this.ribbonPageGroupGeneralOptions.ShowCaptionButton = false;
            this.ribbonPageGroupGeneralOptions.Text = "ribbonPageGroupGeneralOptions";
            // 
            // ribbonPageGroupApplications
            // 
            this.ribbonPageGroupApplications.AllowMinimize = false;
            this.ribbonPageGroupApplications.AllowTextClipping = false;
            this.ribbonPageGroupApplications.ItemLinks.Add(this.barButtonItemOpenMap);
            this.ribbonPageGroupApplications.Name = "ribbonPageGroupApplications";
            this.ribbonPageGroupApplications.ShowCaptionButton = false;
            this.ribbonPageGroupApplications.Text = "ribbonPageGroupApplications";
            // 
            // ribbonStatusBar1
            // 
            this.ribbonStatusBar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ribbonStatusBar1.ItemLinks.Add(this.labelUser);
            this.ribbonStatusBar1.ItemLinks.Add(this.labelConnected);
            this.ribbonStatusBar1.ItemLinks.Add(this.labelDate);
            this.ribbonStatusBar1.Location = new System.Drawing.Point(0, 719);
            this.ribbonStatusBar1.Name = "ribbonStatusBar1";
            this.ribbonStatusBar1.Ribbon = this.ribbonControl1;
            this.ribbonStatusBar1.Size = new System.Drawing.Size(1370, 23);
            // 
            // labelUser
            // 
            this.labelUser.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUser.Appearance.Options.UseFont = true;
            this.labelUser.Caption = "Usuario";
            this.labelUser.Id = 26;
            this.labelUser.Name = "labelUser";
            this.labelUser.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // labelConnected
            // 
            this.labelConnected.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelConnected.Appearance.Options.UseFont = true;
            this.labelConnected.Caption = "Connected";
            this.labelConnected.Id = 25;
            this.labelConnected.Name = "labelConnected";
            this.labelConnected.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // labelDate
            // 
            this.labelDate.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.labelDate.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDate.Appearance.Options.UseFont = true;
            this.labelDate.Caption = "Date";
            this.labelDate.Id = 27;
            this.labelDate.Name = "labelDate";
            this.labelDate.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // defaultLookAndFeel1
            // 
            this.defaultLookAndFeel1.LookAndFeel.SkinName = "Blue";
            // 
            // xtraTabbedMdiManager1
            // 
            this.xtraTabbedMdiManager1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.xtraTabbedMdiManager1.BorderStylePage = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.xtraTabbedMdiManager1.ClosePageButtonShowMode = DevExpress.XtraTab.ClosePageButtonShowMode.InActiveTabPageHeader;
            this.xtraTabbedMdiManager1.HeaderButtons = ((DevExpress.XtraTab.TabButtons)(((DevExpress.XtraTab.TabButtons.Prev | DevExpress.XtraTab.TabButtons.Next) 
            | DevExpress.XtraTab.TabButtons.Default)));
            this.xtraTabbedMdiManager1.HeaderButtonsShowMode = DevExpress.XtraTab.TabButtonShowMode.WhenNeeded;
            this.xtraTabbedMdiManager1.MdiParent = this;
            this.xtraTabbedMdiManager1.SetNextMdiChildMode = DevExpress.XtraTabbedMdi.SetNextMdiChildMode.TabControl;
            // 
            // labelLogo
            // 
            this.labelLogo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLogo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.labelLogo.Image = ((System.Drawing.Image)(resources.GetObject("labelLogo.Image")));
            this.labelLogo.Location = new System.Drawing.Point(1313, 2);
            this.labelLogo.Name = "labelLogo";
            this.labelLogo.Size = new System.Drawing.Size(57, 22);
            this.labelLogo.TabIndex = 2;
            // 
            // ribbonPage2
            // 
            this.ribbonPage2.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1,
            this.ribbonPageGroup2,
            this.ribbonPageGroup3,
            this.ribbonPageGroupTools});
            this.ribbonPage2.Name = "ribbonPage2";
            this.ribbonPage2.Text = "ribbonPage1";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.AllowMinimize = false;
            this.ribbonPageGroup1.AllowTextClipping = false;
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItemStartRegIncident);
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItem1, true);
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItemCancelIncident);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.ShowCaptionButton = false;
            this.ribbonPageGroup1.Text = "ribbonPageGroupCallInfo";
            // 
            // barButtonItemStartRegIncident
            // 
            this.barButtonItemStartRegIncident.Caption = "ItemStartRegIncident";
            this.barButtonItemStartRegIncident.Enabled = false;
            this.barButtonItemStartRegIncident.Id = 12;
            this.barButtonItemStartRegIncident.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemStartRegIncident.LargeGlyph")));
            this.barButtonItemStartRegIncident.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemStartRegIncident.Name = "barButtonItemStartRegIncident";
            this.barButtonItemStartRegIncident.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "ItemCreateNewIncident";
            this.barButtonItem1.Enabled = false;
            this.barButtonItem1.Id = 13;
            this.barButtonItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.LargeGlyph")));
            this.barButtonItem1.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonItemCancelIncident
            // 
            this.barButtonItemCancelIncident.Caption = "ItemCancelIncident";
            this.barButtonItemCancelIncident.Enabled = false;
            this.barButtonItemCancelIncident.Id = 15;
            this.barButtonItemCancelIncident.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemCancelIncident.LargeGlyph")));
            this.barButtonItemCancelIncident.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemCancelIncident.Name = "barButtonItemCancelIncident";
            this.barButtonItemCancelIncident.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.AllowMinimize = false;
            this.ribbonPageGroup2.AllowTextClipping = false;
            this.ribbonPageGroup2.ItemLinks.Add(this.barButtonItem2);
            this.ribbonPageGroup2.ItemLinks.Add(this.barButtonItem3);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.ShowCaptionButton = false;
            this.ribbonPageGroup2.Text = "Applications";
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "ItemChat";
            this.barButtonItem2.Id = 5;
            this.barButtonItem2.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem2.LargeGlyph")));
            this.barButtonItem2.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItem2.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "History";
            this.barButtonItem3.Id = 21;
            this.barButtonItem3.LargeGlyph = global::SmartCadGuiCommon.Properties.ResourcesGui.Historial;
            this.barButtonItem3.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItem3.Name = "barButtonItem3";
            this.barButtonItem3.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.AllowMinimize = false;
            this.ribbonPageGroup3.AllowTextClipping = false;
            this.ribbonPageGroup3.ItemLinks.Add(this.barButtonItem4);
            this.ribbonPageGroup3.ItemLinks.Add(this.barButtonItem5);
            this.ribbonPageGroup3.ItemLinks.Add(this.barButtonItem6);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.ShowCaptionButton = false;
            this.ribbonPageGroup3.Text = "ribbonPageGroupGeneralOptions";
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "ItemPrint";
            this.barButtonItem4.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem4.Glyph")));
            this.barButtonItem4.Id = 6;
            this.barButtonItem4.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItem4.Name = "barButtonItem4";
            // 
            // barButtonItem5
            // 
            this.barButtonItem5.Caption = "ItemSave";
            this.barButtonItem5.Glyph = global::SmartCadGuiCommon.PrintRibbonControllerResources.RibbonPrintPreview_Save;
            this.barButtonItem5.Id = 7;
            this.barButtonItem5.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItem5.Name = "barButtonItem5";
            // 
            // barButtonItem6
            // 
            this.barButtonItem6.Caption = "ItemRefresh";
            this.barButtonItem6.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem6.Glyph")));
            this.barButtonItem6.Id = 8;
            this.barButtonItem6.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItem6.Name = "barButtonItem6";
            // 
            // ribbonPageGroupTools
            // 
            this.ribbonPageGroupTools.AllowMinimize = false;
            this.ribbonPageGroupTools.AllowTextClipping = false;
            this.ribbonPageGroupTools.ItemLinks.Add(this.barButtonItem7);
            this.ribbonPageGroupTools.Name = "ribbonPageGroupTools";
            this.ribbonPageGroupTools.ShowCaptionButton = false;
            this.ribbonPageGroupTools.Text = "ribbonPageGroupTools";
            // 
            // barButtonItem7
            // 
            this.barButtonItem7.Caption = "ItemOpenMap";
            this.barButtonItem7.Id = 4;
            this.barButtonItem7.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem7.LargeGlyph")));
            this.barButtonItem7.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItem7.Name = "barButtonItem7";
            this.barButtonItem7.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonItem8
            // 
            this.barButtonItem8.Caption = "barButtonItemHelp";
            this.barButtonItem8.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem8.Glyph")));
            this.barButtonItem8.Id = 0;
            this.barButtonItem8.Name = "barButtonItem8";
            // 
            // barButtonItem9
            // 
            this.barButtonItem9.Caption = "barButtonItem4";
            this.barButtonItem9.Id = 24;
            this.barButtonItem9.Name = "barButtonItem9";
            this.barButtonItem9.SmallWithoutTextWidth = 70;
            // 
            // FrontClientFormDevX
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1370, 742);
            this.Controls.Add(this.ribbonStatusBar1);
            this.Controls.Add(this.labelLogo);
            this.Controls.Add(this.ribbonControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(1278, 726);
            this.Name = "FrontClientFormDevX";
            this.Text = "FrontClientFormDevX";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrontClientFormDevX_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrontClientFormDevX_FormClosed);
            this.Load += new System.EventHandler(this.FrontClientFormDevX_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

		private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
		private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupUser;
		private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
        private DevExpress.XtraTabbedMdi.XtraTabbedMdiManager xtraTabbedMdiManager1;
		private System.Windows.Forms.Label labelLogo;
		private DevExpress.XtraBars.BarButtonItem barButtonItemOpenMap;
		private DevExpress.XtraBars.BarButtonItem barButtonItemChat;
		private DevExpress.XtraBars.BarButtonItem barButtonItemPrint;
		private DevExpress.XtraBars.BarButtonItem barButtonItemSave;
		private DevExpress.XtraBars.BarButtonItem barButtonItemRefresh;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupTool;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupApplications;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupGeneralOptions;
		private DevExpress.XtraBars.BarEditItem barEditItemProgress;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupCallInfo;
		private DevExpress.XtraBars.BarButtonItem barButtonItemAddToIncident;
		private DevExpress.XtraBars.BarButtonItem barButtonItemFinalize;
		private DevExpress.XtraBars.BarEditItem barEditItemChronometer;
		public DevExpress.XtraBars.BarButtonItem barButtonItemStatus;
		public DevExpress.XtraBars.BarButtonItem barButtonItemTelephone;
		public DevExpress.XtraBars.BarButtonItem barButtonItemCreateNewIncident;
		private DevExpress.XtraBars.Alerter.AlertControl alertControl1;
		private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar repositoryItemProgressBar1;
		private DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit repositoryItemTimeEdit1;
		public DevExpress.XtraBars.BarButtonItem barButtonItemHelp;
		private DevExpress.XtraBars.BarButtonGroup barButtonGroupTelephone;
		public DevExpress.XtraBars.BarButtonItem barButtonItemPhoneReboot;
		public DevExpress.XtraBars.BarButtonItem barButtonItemVirtualCall;
		private DevExpress.XtraBars.BarCheckItem barCheckItemIncompleteCall;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupTelephone;
		private DevExpress.XtraBars.BarStaticItem barStaticItemTimeBar;
		private DevExpress.XtraBars.BarStaticItem barStaticItemChonometer;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemParkCall;
        private DevExpress.XtraBars.BarButtonItem barButtonItemHistory;
        private DevExpress.XtraBars.BarButtonItem barButtonItemLogo;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar1;
        private DevExpress.XtraBars.BarStaticItem labelUser;
        private DevExpress.XtraBars.BarStaticItem labelConnected;
        private DevExpress.XtraBars.BarStaticItem labelDate;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage2;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        public DevExpress.XtraBars.BarButtonItem barButtonItemStartRegIncident;
        public DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItemCancelIncident;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraBars.BarButtonItem barButtonItem5;
        private DevExpress.XtraBars.BarButtonItem barButtonItem6;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupTools;
        private DevExpress.XtraBars.BarButtonItem barButtonItem7;
        public DevExpress.XtraBars.BarButtonItem barButtonItem8;
        private DevExpress.XtraBars.BarButtonItem barButtonItem9;
        public DevExpress.XtraBars.BarButtonItem barButtonItemCallBack;
	}
}