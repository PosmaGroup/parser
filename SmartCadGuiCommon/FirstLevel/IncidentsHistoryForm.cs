using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Collections;
using Smartmatic.SmartCad.Service;
using System.Reflection;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using System.Threading;
using System.IO;
using System.Xml.Xsl;
using System.Xml;
using System.Net;
using DevExpress.XtraTab;
using SmartCadControls.SyncBoxes;
using SmartCadCore.ClientData;
using SmartCadControls.Controls;
using SmartCadCore.Core;
using SmartCadCore.Common;
using SmartCadControls;
using SmartCadFirstLevel.Gui.SyncBoxes;

namespace SmartCadGuiCommon
{
    public partial class IncidentsHistoryForm : DevExpress.XtraEditors.XtraForm
    {
        System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PlayBackForm));
        private XslCompiledTransform xslCompiledTransform;
        private int selectedIncidentCode;
        private GridControlSynBox incidentSyncBox;
        ApplicationPreferenceClientData FrontClientIncidentCodePrefix = null;

        public IncidentsHistoryForm()
        {
            InitializeComponent();
        }

        private void IncidentsHistoryForm_Load(object sender, EventArgs e)
        {
            searchFilterBar1.AddFilter(new IncidentStatusFilter());
            searchFilterBar1.AddFilter(new DatesFilter());
            searchFilterBar1.AddFilter(new IncidentTypeFilter(UserApplicationClientData.FirstLevel));
            searchFilterBar1.SearchRequest += searchFilterBar1_SearchRequest;

            XmlReaderSettings xmlReaderSettings = new XmlReaderSettings() { ProhibitDtd = false };
            xslCompiledTransform = new XslCompiledTransform();
            xslCompiledTransform.Load(XmlReader.Create(SmartCadConfiguration.IncidentXslt, xmlReaderSettings));

            gridControlExPreviousIncidents.Type = typeof(GridIncidentData);
            incidentSyncBox = new GridControlSynBox(gridControlExPreviousIncidents);

            gridControlExPreviousIncidents.ViewTotalRows = true;
            gridControlExPreviousIncidents.EnableAutoFilter = true;
            gridViewExPreviousIncidents.ViewTotalRows = true;
            gridControlExPreviousIncidents.AllowDrop = false;

            this.Text = ResourceLoader.GetString2("IncidentsHistoryForm");
            this.dockPanel1.Text = ResourceLoader.GetString2("SearchingToolsMaps");
            this.dockPanelGrid.Text = ResourceLoader.GetString2("Incidents");

            FrontClientIncidentCodePrefix = (ApplicationPreferenceClientData)ServerServiceClient.GetInstance().SearchClientObject(
                SmartCadHqls.GetCustomHql(SmartCadHqls.GetApplicationPreferenceByName, "FrontClientIncidentCodePrefix"));
        }

        void searchFilterBar1_SearchRequest(Dictionary<string, Dictionary<string, List<object>>> filters)
        {
            BackgroundProcessForm processForm = new BackgroundProcessForm(
                ResourceLoader.GetString2("Searching"),
                this,
                new MethodInfo[1] {
                    GetType().GetMethod("BackgroundSearch", BindingFlags.NonPublic | BindingFlags.Instance) 
                },
                new object[1][] { new object[] { new object[] { filters } } }
            );
            processForm.CanThrowError = true;
            processForm.ShowDialog();
        }

        private void BackgroundSearch(object[] objs)
        {
            Dictionary<string, Dictionary<string, List<object>>> filters = (Dictionary<string, Dictionary<string, List<object>>>)objs[0];
            string HQL = @"SELECT inc FROM IncidentData inc 
                                      LEFT JOIN FETCH inc.SetReportBaseList child 
                                      LEFT JOIN FETCH child.SetIncidentTypes WHERE
                                      inc.CustomCode like '" + FrontClientIncidentCodePrefix.Value + "%' AND";
            //SET Incident Status filter
            if (filters["IncidentStatusFilter"].Count == 1)
            {
                if (filters["IncidentStatusFilter"].ContainsKey("Open"))
                {
                    HQL += " inc.Status.CustomCode = 'Open' AND";
                }
                else
                {
                    HQL += " inc.Status.CustomCode = 'Closed' AND";
                }
            }
            //SET Incident date interval filter
            string start = ApplicationUtil.GetDataBaseFormattedDate_1(((DateTime)filters["DatesFilter"]["StartDate"][0]));
            string end = ApplicationUtil.GetDataBaseFormattedDate_1(((DateTime)filters["DatesFilter"]["EndDate"][0]));

            HQL += @" inc.StartDate BETWEEN '" + start + " 00:00:00'" +
                    " AND '" + end + " 23:59:59'";

            //SET Incident Type filter
            if (filters["IncidentTypeFilter"]["IncidentType"].Count > 0)
            {
                HQL += @" AND inc.Code in 
                   ( SELECT reportBase.Incident.Code
                        FROM ReportBaseData reportBase 
                        LEFT JOIN reportBase.SetIncidentTypes types
                        WHERE types.Name IN (";
                foreach (object type in filters["IncidentTypeFilter"]["IncidentType"])
                {
                    HQL += String.Format("'{0}', ", type);
                }
                HQL = HQL.Remove(HQL.Length - 2);
                HQL += "))";
            }

            IList incidents = ServerServiceClient.GetInstance().SearchClientObjectsMaxRows(HQL, true);

            gridControlExPreviousIncidents.ClearData();
            incidentSyncBox.Sync(incidents);
        }


        private void gridControlExPreviousIncidents_CellToolTipNeeded(object sender, DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventArgs e)
        {
            if (e.Info == null)
            {
                string infoText = string.Empty;
                GridHitInfo info = gridViewExPreviousIncidents.CalcHitInfo(e.ControlMousePosition);
                if (info.RowHandle > -1 && info.RowHandle != GridControlEx.InvalidRowHandle)
                {
                    IncidentClientData incidentData = (IncidentClientData)((GridIncidentData)gridViewExPreviousIncidents.GetRow(info.RowHandle)).Tag;

                    if (incidentData != null)
                        foreach (IncidentTypeClientData incidentType in incidentData.IncidentTypes)
                            if (incidentType.NoEmergency != true)
                                infoText += incidentType.FriendlyName + ", ";

                    if (infoText.Trim() != "")
                        infoText = infoText.Substring(0, infoText.Length - 2);

                    e.Info = new DevExpress.Utils.ToolTipControlInfo(new DevExpress.XtraGrid.Views.Base.CellToolTipInfo(info.RowHandle, info.Column, "cell"), infoText);
                }
            }
        }

        private void gridViewExPreviousIncidents_SelectionWillChange(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            FormUtil.InvokeRequired(this,
                delegate
                {
                    RemovePlayers();

                    if (textBoxExIncidentdetails.IsDisposed == false)
                        textBoxExIncidentdetails.DocumentText = String.Format("<span style='HEIGHT:100%;FONT-SIZE: 10pt; FONT-FAMILY: Sans-Serif'>{0}</span>", ResourceLoader.GetString2("NoIncidentSelected"));
                });
        }

        private void gridViewExPreviousIncidents_SingleSelectionChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            FormUtil.InvokeRequired(this,
                delegate
                {
                    if (gridControlExPreviousIncidents.SelectedItems.Count > 0)
                    {
                        IncidentClientData selectedIncident = (IncidentClientData)((GridIncidentData)gridControlExPreviousIncidents.SelectedItems[0]).Tag;

                        ArrayList parameters = new ArrayList();
                        parameters.Add(selectedIncident.Code);
                        parameters.Add(selectedIncident.IncidentTypes);

                        SelectedIncidentTask selectedIncidentTask = new SelectedIncidentTask(parameters);
                        ArrayList tasks = new ArrayList();
                        tasks.Add(selectedIncidentTask);

                        ThreadPool.QueueUserWorkItem(delegate(object state)
                        {
                            try
                            {
                                SelectedIncidentHelp(selectedIncidentTask);
                            }
                            catch (Exception ex)
                            {
                                MessageForm.Show(ex.Message, ex);
                            }
                        });

                        ThreadPool.QueueUserWorkItem(delegate(object state)
                        {
                            try
                            {
                                if (selectedIncident.PhoneReports != null && selectedIncident.PhoneReports.Count > 0)
                                {
                                    //GetFirstLevelCalls
                                    foreach (PhoneReportClientData phoneReport in selectedIncident.PhoneReports)
                                    {
                                        AddAudioTab(phoneReport.CustomCode, "Call #"+(phoneReport.OrderOfCall+1), selectedIncident.CustomCode);
                                    }


                                    selectedIncident.IncidentNotifications = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetIncidentNotificationsByIncidentCustomCode, selectedIncident.CustomCode));

                                    //GetDispatchCalls
                                    int i = 1;
                                    foreach (IncidentNotificationClientData notif in selectedIncident.IncidentNotifications)
                                    {
                                        AddAudioTab(notif.CustomCode, notif.DepartmentType.Name + " Call #", notif.CustomCode);
                                    }
                                }
                            }
                            catch { }
                        });

                    }
                    else
                    {
                        textBoxExIncidentdetails.DocumentText = String.Format("<span style='HEIGHT:100%;FONT-SIZE: 10pt; FONT-FAMILY: Sans-Serif'>{0}</span>", ResourceLoader.GetString2("NoIncidentSelected"));
                    }
                });
        }

        private void AddAudioTab(string customCode, string tabTitle, string exportFileName)
        {
            IList reportAudios = ServerServiceClient.GetInstance().SearchClientObjects(
                                            SmartCadHqls.GetCustomHql(SmartCadHqls.GetAudioByPhoneReportCustomCode, customCode));
            if (reportAudios != null && reportAudios.Count > 0)
            {
                int i = 1;
                foreach (PhoneReportAudioClientData reportAudio in reportAudios)
                {
                    if (File.Exists(reportAudio.StorageFolder + reportAudio.FileName))
                    {
                        FormUtil.InvokeRequired(this, delegate
                        {
                            XtraTabPage newPage = new XtraTabPage();
                            if (tabTitle.EndsWith("#"))
                            {
                                newPage.Text = tabTitle + i++;
                            }
                            else
                            {
                                newPage.Text = (i == 1) ? tabTitle : ("Recall #" + (i-1));
                                i++;
                            }
                            groupControlPlayBack.TabPages.Add(newPage);

                            CallAudioPlayer callPlayer = new CallAudioPlayer(
                                reportAudio.StorageFolder + reportAudio.FileName,
                                exportFileName);
                            callPlayer.Dock = DockStyle.Bottom;

                            newPage.Controls.Add(callPlayer);
                            callPlayer.Init();
                        });
                    }
                    else
                    {
                        FormUtil.InvokeRequired(this, delegate
                        {
                            MessageForm.Show(ResourceLoader.GetString2("FileNotFoundAt", reportAudio.StorageFolder), MessageFormType.Error);
                        });
                    }
                }
            }
        }

        private void SelectedIncidentHelp(SelectedIncidentTask selectedIncidentTask)
        {
            if (selectedIncidentTask != null)
            {
                ArrayList tasks = new ArrayList();
                tasks.Add(selectedIncidentTask);
                bool sameIncident = false;
                if (selectedIncidentCode == (int)selectedIncidentTask.Parameters[0])
                    sameIncident = true;
                selectedIncidentCode = (int)selectedIncidentTask.Parameters[0];
                try
                {
                    IList result = ServerServiceClient.GetInstance().RunTasks(tasks);
                    if (result != null && result.Count > 0 && (result[0] as IList) != null)
                    {
                        IList tempList = (IList)result[0];
                        if (tempList != null && tempList.Count > 0)
                        {
                            SelectedIncidentTaskResult selectedIncidentLightData = tempList[0] as SelectedIncidentTaskResult;
                            if (selectedIncidentLightData != null)
                            {
                                FillIncidentDetails(selectedIncidentLightData, sameIncident);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    SmartLogger.Print(ex);
                }
            }
        }

        private void FillIncidentDetails(SelectedIncidentTaskResult selectedIncidentTask, bool sameIncident)
        {
            try
            {
                string strXml = ApplicationUtil.BuildHtml(selectedIncidentTask.Xml, xslCompiledTransform, "incident");
                strXml = ApplicationUtil.RecoverBlanks(strXml);

                FileStream fs = File.Open("incident_firstlevel.html", FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite);
                byte[] byteXML = Encoding.Unicode.GetBytes(strXml);
                fs.Write(byteXML, 0, byteXML.Length);
                fs.Flush();
                fs.Close();

                FormUtil.InvokeRequired(textBoxExIncidentdetails, () =>
                    textBoxExIncidentdetails.Navigate(new FileInfo("incident_firstlevel.html").FullName, sameIncident));
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        private void IncidentsHistoryForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            RemovePlayers();
        }

        private void RemovePlayers()
        {
            foreach (XtraTabPage tab in groupControlPlayBack.TabPages)
            {
                ((CallAudioPlayer)tab.Controls[0]).Disable();
                tab.Controls.Clear();
            }
            groupControlPlayBack.TabPages.Clear();
        }
    }

}
