﻿using SmartCadCore.ClientData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadGuiCommon.CCTV.Nodes
{
    public class CctvCameraNode : CctvNode
    {
        //The Name prop represents the camera name
        public CameraClientData Camera { get; set; }

        public CctvCameraNode(CameraClientData camera)
            : base(camera.CustomId, camera.Name)
        {
            Camera = camera;
        }

        public CctvCameraNode(string cameraName, string cameraCustomName)
            : base(cameraName, cameraCustomName)
        {
        }

        public override bool Equals(object obj)
        {
            if (obj is CctvCameraNode)
            {
                return this.Name == ((CctvCameraNode)obj).Name;
            }
            return false;
        }
    }
}
