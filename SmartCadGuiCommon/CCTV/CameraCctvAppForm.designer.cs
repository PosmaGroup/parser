using SmartCadControls.Controls;
using Smartmatic.SmartCad.Vms;
namespace SmartCadGuiCommon
{
    partial class CameraCctvAppForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CameraCctvAppForm));
            this.vmsControlEx1 = new VmsControlEx();
            this.SuspendLayout();
            // 
            // vmsControlEx1
            // 
            this.vmsControlEx1.audio = false;
            this.vmsControlEx1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.vmsControlEx1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vmsControlEx1.inputs = null;
            this.vmsControlEx1.Location = new System.Drawing.Point(0, 0);
            this.vmsControlEx1.MinimumSize = new System.Drawing.Size(346, 255);
            this.vmsControlEx1.Name = "vmsControlEx1";
            this.vmsControlEx1.outputs = null;
            this.vmsControlEx1.PTZ = false;
            this.vmsControlEx1.Size = new System.Drawing.Size(703, 397);
            this.vmsControlEx1.speaker = false;
            this.vmsControlEx1.TabIndex = 0;
            // 
            // CameraCctvAppForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(703, 397);
            this.Controls.Add(this.vmsControlEx1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CameraCctvAppForm";
            this.Text = "Camera";
            this.Deactivate += new System.EventHandler(this.CameraCctvAppForm_Deactivate);
            this.Load += new System.EventHandler(this.CameraForm_Load);
            this.Activated += new System.EventHandler(this.CameraCctvAppForm_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CameraCctvAppForm_FormClosing);
            this.ResumeLayout(false);

        }

        #endregion

        private VmsControlEx vmsControlEx1;

    }
}