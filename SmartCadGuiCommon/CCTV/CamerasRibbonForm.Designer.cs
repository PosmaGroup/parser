using SmartCadControls;
namespace SmartCadGuiCommon
{
	partial class CamerasRibbonForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CamerasRibbonForm));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barButtonItemCascade = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemArrange = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemTileHorizontal = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemTileVertical = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemStartIncident = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.xtraTabbedMdiManager1 = new DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(this.components);
            this.dockManager = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.panelContainer1 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanelControls = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.groupControlOutput = new DevExpress.XtraEditors.GroupControl();
            this.simpleButtonOutput = new DevExpress.XtraEditors.SimpleButton();
            this.imageListBoxControlOutputs = new DevExpress.XtraEditors.ImageListBoxControl();
            this.groupControlPanTiltZoom = new DevExpress.XtraEditors.GroupControl();
            this.simpleButtonZoomOut = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonBottomRight = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonBottom = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonZoomIn = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonBottomLeft = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonMiddleRight = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonMiddle = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonMiddleLeft = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonUpperRight = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonUpper = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonUpperLeft = new DevExpress.XtraEditors.SimpleButton();
            this.groupControlInput = new DevExpress.XtraEditors.GroupControl();
            this.imageListBoxControlInputs = new DevExpress.XtraEditors.ImageListBoxControl();
            this.groupControlAudioIN = new DevExpress.XtraEditors.GroupControl();
            this.labelControlVolume = new DevExpress.XtraEditors.LabelControl();
            this.simpleButtonSpeak = new DevExpress.XtraEditors.SimpleButton();
            this.trackBarControlVolume = new DevExpress.XtraEditors.TrackBarControl();
            this.muteCheckBox = new System.Windows.Forms.CheckBox();
            this.dockPanelPlayback = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel2_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.simpleButtonExport = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl1AudioPlayback = new DevExpress.XtraEditors.GroupControl();
            this.labelControlVulmePlayback = new DevExpress.XtraEditors.LabelControl();
            this.trackBarControlPlayback = new DevExpress.XtraEditors.TrackBarControl();
            this.checkBoxMutePlayback = new System.Windows.Forms.CheckBox();
            this.groupControlPlayback = new DevExpress.XtraEditors.GroupControl();
            this.simpleButtonGoto = new DevExpress.XtraEditors.SimpleButton();
            this.checkButtonBack = new DevExpress.XtraEditors.CheckButton();
            this.checkButtonPlay = new DevExpress.XtraEditors.CheckButton();
            this.dateEditplayback = new DevExpress.XtraEditors.DateEdit();
            this.simpleButtonStop = new DevExpress.XtraEditors.SimpleButton();
            this.labelControlPlaybackSpeed = new DevExpress.XtraEditors.LabelControl();
            this.zoomTrackBarControlSpeed = new DevExpress.XtraEditors.ZoomTrackBarControl();
            this.dockPanelEvents = new DevExpress.XtraBars.Docking.DockPanel();
            this.controlContainer1 = new DevExpress.XtraBars.Docking.ControlContainer();
            this.gridControlExEvents = new GridControlEx();
            this.gridViewExEvents = new GridViewEx();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tmrUpdateStick = new System.Windows.Forms.Timer(this.components);
            this.barButtonItemStartRegIncident = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager)).BeginInit();
            this.panelContainer1.SuspendLayout();
            this.dockPanelControls.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlOutput)).BeginInit();
            this.groupControlOutput.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageListBoxControlOutputs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlPanTiltZoom)).BeginInit();
            this.groupControlPanTiltZoom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlInput)).BeginInit();
            this.groupControlInput.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageListBoxControlInputs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlAudioIN)).BeginInit();
            this.groupControlAudioIN.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarControlVolume)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarControlVolume.Properties)).BeginInit();
            this.dockPanelPlayback.SuspendLayout();
            this.dockPanel2_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1AudioPlayback)).BeginInit();
            this.groupControl1AudioPlayback.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarControlPlayback)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarControlPlayback.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlPlayback)).BeginInit();
            this.groupControlPlayback.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditplayback.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditplayback.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.zoomTrackBarControlSpeed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.zoomTrackBarControlSpeed.Properties)).BeginInit();
            this.dockPanelEvents.SuspendLayout();
            this.controlContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExEvents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExEvents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(241)))));
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.ExpandCollapseItem.Name = "";
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.barButtonItemCascade,
            this.barButtonItemArrange,
            this.barButtonItemTileHorizontal,
            this.barButtonItemTileVertical,
            this.barButtonItemStartIncident});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 5;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbonControl1.ShowToolbarCustomizeItem = false;
            this.ribbonControl1.Size = new System.Drawing.Size(792, 119);
            this.ribbonControl1.Toolbar.ShowCustomizeItem = false;
            this.ribbonControl1.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            // 
            // barButtonItemCascade
            // 
            this.barButtonItemCascade.Caption = "Cascade";
            this.barButtonItemCascade.Id = 0;
            this.barButtonItemCascade.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemCascade.LargeGlyph")));
            this.barButtonItemCascade.Name = "barButtonItemCascade";
            this.barButtonItemCascade.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItemCascade.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemCascade_ItemClick);
            // 
            // barButtonItemArrange
            // 
            this.barButtonItemArrange.Caption = "Arrange";
            this.barButtonItemArrange.Id = 1;
            this.barButtonItemArrange.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemArrange.LargeGlyph")));
            this.barButtonItemArrange.Name = "barButtonItemArrange";
            this.barButtonItemArrange.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItemArrange.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemArrange_ItemClick);
            // 
            // barButtonItemTileHorizontal
            // 
            this.barButtonItemTileHorizontal.Caption = "Tile horizontal";
            this.barButtonItemTileHorizontal.Id = 2;
            this.barButtonItemTileHorizontal.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemTileHorizontal.LargeGlyph")));
            this.barButtonItemTileHorizontal.Name = "barButtonItemTileHorizontal";
            this.barButtonItemTileHorizontal.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItemTileHorizontal.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemTileHorizontal_ItemClick);
            // 
            // barButtonItemTileVertical
            // 
            this.barButtonItemTileVertical.Caption = "Tile vertical";
            this.barButtonItemTileVertical.Id = 3;
            this.barButtonItemTileVertical.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemTileVertical.LargeGlyph")));
            this.barButtonItemTileVertical.Name = "barButtonItemTileVertical";
            this.barButtonItemTileVertical.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItemTileVertical.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemTileVertical_ItemClick);
            // 
            // barButtonItemStartIncident
            // 
            this.barButtonItemStartIncident.Caption = "Start incident";
            this.barButtonItemStartIncident.Id = 4;
            this.barButtonItemStartIncident.LargeGlyph = global::SmartCadGuiCommon.Properties.Resources.Iniciar;
            this.barButtonItemStartIncident.Name = "barButtonItemStartIncident";
            this.barButtonItemStartIncident.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.barButtonItemStartIncident.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemStartIncident_ItemClick);
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "ribbonPage1";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.AllowMinimize = false;
            this.ribbonPageGroup1.AllowTextClipping = false;
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItemStartIncident);
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItemCascade);
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItemArrange);
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItemTileHorizontal);
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItemTileVertical);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.ShowCaptionButton = false;
            this.ribbonPageGroup1.Text = "ribbonPageGroup1";
            // 
            // xtraTabbedMdiManager1
            // 
            this.xtraTabbedMdiManager1.MdiParent = this;
            // 
            // dockManager
            // 
            this.dockManager.Form = this;
            this.dockManager.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.panelContainer1,
            this.dockPanelEvents});
            this.dockManager.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "System.Windows.Forms.StatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            // 
            // panelContainer1
            // 
            this.panelContainer1.ActiveChild = this.dockPanelControls;
            this.panelContainer1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panelContainer1.Appearance.Options.UseBackColor = true;
            this.panelContainer1.Controls.Add(this.dockPanelControls);
            this.panelContainer1.Controls.Add(this.dockPanelPlayback);
            this.panelContainer1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Right;
            this.panelContainer1.FloatVertical = true;
            this.panelContainer1.ID = new System.Guid("bbbed589-eed3-427b-8734-2cc39ecd2d88");
            this.panelContainer1.Location = new System.Drawing.Point(592, 119);
            this.panelContainer1.Name = "panelContainer1";
            this.panelContainer1.Options.AllowDockBottom = false;
            this.panelContainer1.Options.AllowDockTop = false;
            this.panelContainer1.Options.AllowFloating = false;
            this.panelContainer1.Options.FloatOnDblClick = false;
            this.panelContainer1.Options.ShowCloseButton = false;
            this.panelContainer1.Options.ShowMaximizeButton = false;
            this.panelContainer1.OriginalSize = new System.Drawing.Size(200, 200);
            this.panelContainer1.Size = new System.Drawing.Size(200, 352);
            this.panelContainer1.Tabbed = true;
            this.panelContainer1.TabsPosition = DevExpress.XtraBars.Docking.TabsPosition.Top;
            this.panelContainer1.Text = "panelContainer1";
            // 
            // dockPanelControls
            // 
            this.dockPanelControls.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.dockPanelControls.Appearance.Options.UseBackColor = true;
            this.dockPanelControls.Controls.Add(this.dockPanel1_Container);
            this.dockPanelControls.Dock = DevExpress.XtraBars.Docking.DockingStyle.Fill;
            this.dockPanelControls.FloatVertical = true;
            this.dockPanelControls.ID = new System.Guid("4f87d711-68e7-4660-9297-3b976d6554e1");
            this.dockPanelControls.Location = new System.Drawing.Point(3, 50);
            this.dockPanelControls.Name = "dockPanelControls";
            this.dockPanelControls.Options.AllowDockBottom = false;
            this.dockPanelControls.Options.AllowDockTop = false;
            this.dockPanelControls.Options.AllowFloating = false;
            this.dockPanelControls.Options.FloatOnDblClick = false;
            this.dockPanelControls.Options.ShowCloseButton = false;
            this.dockPanelControls.Options.ShowMaximizeButton = false;
            this.dockPanelControls.OriginalSize = new System.Drawing.Size(192, 497);
            this.dockPanelControls.Size = new System.Drawing.Size(194, 499);
            this.dockPanelControls.TabsPosition = DevExpress.XtraBars.Docking.TabsPosition.Top;
            this.dockPanelControls.Text = "Live";
            this.dockPanelControls.Enter += new System.EventHandler(this.dockPanelControls_Enter);
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.groupControlOutput);
            this.dockPanel1_Container.Controls.Add(this.groupControlPanTiltZoom);
            this.dockPanel1_Container.Controls.Add(this.groupControlInput);
            this.dockPanel1_Container.Controls.Add(this.groupControlAudioIN);
            this.dockPanel1_Container.Location = new System.Drawing.Point(0, 0);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(194, 499);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // groupControlOutput
            // 
            this.groupControlOutput.Controls.Add(this.simpleButtonOutput);
            this.groupControlOutput.Controls.Add(this.imageListBoxControlOutputs);
            this.groupControlOutput.Enabled = false;
            this.groupControlOutput.Location = new System.Drawing.Point(3, 366);
            this.groupControlOutput.Name = "groupControlOutput";
            this.groupControlOutput.Size = new System.Drawing.Size(170, 134);
            this.groupControlOutput.TabIndex = 5;
            this.groupControlOutput.Text = "groupControlOutput";
            // 
            // simpleButtonOutput
            // 
            this.simpleButtonOutput.Location = new System.Drawing.Point(40, 106);
            this.simpleButtonOutput.Name = "simpleButtonOutput";
            this.simpleButtonOutput.Size = new System.Drawing.Size(90, 25);
            this.simpleButtonOutput.TabIndex = 12;
            this.simpleButtonOutput.Text = "TriggerOutput";
            this.simpleButtonOutput.Click += new System.EventHandler(this.simpleButtonOutput_Click);
            // 
            // imageListBoxControlOutputs
            // 
            this.imageListBoxControlOutputs.Location = new System.Drawing.Point(5, 23);
            this.imageListBoxControlOutputs.Name = "imageListBoxControlOutputs";
            this.imageListBoxControlOutputs.Size = new System.Drawing.Size(160, 77);
            this.imageListBoxControlOutputs.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            this.imageListBoxControlOutputs.TabIndex = 1;
            this.imageListBoxControlOutputs.SelectedIndexChanged += new System.EventHandler(this.imageListBoxControlOutputs_SelectedIndexChanged);
            // 
            // groupControlPanTiltZoom
            // 
            this.groupControlPanTiltZoom.Controls.Add(this.simpleButtonZoomOut);
            this.groupControlPanTiltZoom.Controls.Add(this.simpleButtonBottomRight);
            this.groupControlPanTiltZoom.Controls.Add(this.simpleButtonBottom);
            this.groupControlPanTiltZoom.Controls.Add(this.simpleButtonZoomIn);
            this.groupControlPanTiltZoom.Controls.Add(this.simpleButtonBottomLeft);
            this.groupControlPanTiltZoom.Controls.Add(this.simpleButtonMiddleRight);
            this.groupControlPanTiltZoom.Controls.Add(this.simpleButtonMiddle);
            this.groupControlPanTiltZoom.Controls.Add(this.simpleButtonMiddleLeft);
            this.groupControlPanTiltZoom.Controls.Add(this.simpleButtonUpperRight);
            this.groupControlPanTiltZoom.Controls.Add(this.simpleButtonUpper);
            this.groupControlPanTiltZoom.Controls.Add(this.simpleButtonUpperLeft);
            this.groupControlPanTiltZoom.Enabled = false;
            this.groupControlPanTiltZoom.Location = new System.Drawing.Point(3, 3);
            this.groupControlPanTiltZoom.Name = "groupControlPanTiltZoom";
            this.groupControlPanTiltZoom.Size = new System.Drawing.Size(170, 130);
            this.groupControlPanTiltZoom.TabIndex = 1;
            this.groupControlPanTiltZoom.Text = "groupControlPanTiltZoom";
            // 
            // simpleButtonZoomOut
            // 
            this.simpleButtonZoomOut.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonZoomOut.Image")));
            this.simpleButtonZoomOut.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButtonZoomOut.Location = new System.Drawing.Point(127, 81);
            this.simpleButtonZoomOut.Name = "simpleButtonZoomOut";
            this.simpleButtonZoomOut.Size = new System.Drawing.Size(30, 30);
            this.simpleButtonZoomOut.TabIndex = 10;
            this.simpleButtonZoomOut.Click += new System.EventHandler(this.PTZControlClicked);
            // 
            // simpleButtonBottomRight
            // 
            this.simpleButtonBottomRight.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonBottomRight.Image")));
            this.simpleButtonBottomRight.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButtonBottomRight.Location = new System.Drawing.Point(79, 95);
            this.simpleButtonBottomRight.Name = "simpleButtonBottomRight";
            this.simpleButtonBottomRight.Size = new System.Drawing.Size(30, 30);
            this.simpleButtonBottomRight.TabIndex = 8;
            this.simpleButtonBottomRight.Click += new System.EventHandler(this.PTZControlClicked);
            // 
            // simpleButtonBottom
            // 
            this.simpleButtonBottom.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonBottom.Image")));
            this.simpleButtonBottom.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButtonBottom.Location = new System.Drawing.Point(43, 95);
            this.simpleButtonBottom.Name = "simpleButtonBottom";
            this.simpleButtonBottom.Size = new System.Drawing.Size(30, 30);
            this.simpleButtonBottom.TabIndex = 7;
            this.simpleButtonBottom.Click += new System.EventHandler(this.PTZControlClicked);
            // 
            // simpleButtonZoomIn
            // 
            this.simpleButtonZoomIn.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonZoomIn.Image")));
            this.simpleButtonZoomIn.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButtonZoomIn.Location = new System.Drawing.Point(127, 45);
            this.simpleButtonZoomIn.Name = "simpleButtonZoomIn";
            this.simpleButtonZoomIn.Size = new System.Drawing.Size(30, 30);
            this.simpleButtonZoomIn.TabIndex = 9;
            this.simpleButtonZoomIn.Click += new System.EventHandler(this.PTZControlClicked);
            // 
            // simpleButtonBottomLeft
            // 
            this.simpleButtonBottomLeft.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonBottomLeft.Image")));
            this.simpleButtonBottomLeft.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButtonBottomLeft.Location = new System.Drawing.Point(7, 95);
            this.simpleButtonBottomLeft.Name = "simpleButtonBottomLeft";
            this.simpleButtonBottomLeft.Size = new System.Drawing.Size(30, 30);
            this.simpleButtonBottomLeft.TabIndex = 6;
            this.simpleButtonBottomLeft.Click += new System.EventHandler(this.PTZControlClicked);
            // 
            // simpleButtonMiddleRight
            // 
            this.simpleButtonMiddleRight.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonMiddleRight.Image")));
            this.simpleButtonMiddleRight.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButtonMiddleRight.Location = new System.Drawing.Point(79, 59);
            this.simpleButtonMiddleRight.Name = "simpleButtonMiddleRight";
            this.simpleButtonMiddleRight.Size = new System.Drawing.Size(30, 30);
            this.simpleButtonMiddleRight.TabIndex = 5;
            this.simpleButtonMiddleRight.Click += new System.EventHandler(this.PTZControlClicked);
            // 
            // simpleButtonMiddle
            // 
            this.simpleButtonMiddle.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonMiddle.Image")));
            this.simpleButtonMiddle.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButtonMiddle.Location = new System.Drawing.Point(43, 59);
            this.simpleButtonMiddle.Name = "simpleButtonMiddle";
            this.simpleButtonMiddle.Size = new System.Drawing.Size(30, 30);
            this.simpleButtonMiddle.TabIndex = 4;
            this.simpleButtonMiddle.Click += new System.EventHandler(this.PTZControlClicked);
            // 
            // simpleButtonMiddleLeft
            // 
            this.simpleButtonMiddleLeft.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonMiddleLeft.Image")));
            this.simpleButtonMiddleLeft.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButtonMiddleLeft.Location = new System.Drawing.Point(7, 59);
            this.simpleButtonMiddleLeft.Name = "simpleButtonMiddleLeft";
            this.simpleButtonMiddleLeft.Size = new System.Drawing.Size(30, 30);
            this.simpleButtonMiddleLeft.TabIndex = 3;
            this.simpleButtonMiddleLeft.Click += new System.EventHandler(this.PTZControlClicked);
            // 
            // simpleButtonUpperRight
            // 
            this.simpleButtonUpperRight.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonUpperRight.Image")));
            this.simpleButtonUpperRight.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButtonUpperRight.Location = new System.Drawing.Point(79, 23);
            this.simpleButtonUpperRight.Name = "simpleButtonUpperRight";
            this.simpleButtonUpperRight.Size = new System.Drawing.Size(30, 30);
            this.simpleButtonUpperRight.TabIndex = 2;
            this.simpleButtonUpperRight.Click += new System.EventHandler(this.PTZControlClicked);
            // 
            // simpleButtonUpper
            // 
            this.simpleButtonUpper.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonUpper.Image")));
            this.simpleButtonUpper.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButtonUpper.Location = new System.Drawing.Point(43, 23);
            this.simpleButtonUpper.Name = "simpleButtonUpper";
            this.simpleButtonUpper.Size = new System.Drawing.Size(30, 30);
            this.simpleButtonUpper.TabIndex = 1;
            this.simpleButtonUpper.Click += new System.EventHandler(this.PTZControlClicked);
            // 
            // simpleButtonUpperLeft
            // 
            this.simpleButtonUpperLeft.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonUpperLeft.Image")));
            this.simpleButtonUpperLeft.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButtonUpperLeft.Location = new System.Drawing.Point(7, 23);
            this.simpleButtonUpperLeft.Name = "simpleButtonUpperLeft";
            this.simpleButtonUpperLeft.Size = new System.Drawing.Size(30, 30);
            this.simpleButtonUpperLeft.TabIndex = 0;
            this.simpleButtonUpperLeft.Click += new System.EventHandler(this.PTZControlClicked);
            // 
            // groupControlInput
            // 
            this.groupControlInput.Controls.Add(this.imageListBoxControlInputs);
            this.groupControlInput.Enabled = false;
            this.groupControlInput.Location = new System.Drawing.Point(3, 255);
            this.groupControlInput.Name = "groupControlInput";
            this.groupControlInput.Size = new System.Drawing.Size(170, 105);
            this.groupControlInput.TabIndex = 4;
            this.groupControlInput.Text = "groupControlInput";
            // 
            // imageListBoxControlInputs
            // 
            this.imageListBoxControlInputs.Location = new System.Drawing.Point(5, 23);
            this.imageListBoxControlInputs.Name = "imageListBoxControlInputs";
            this.imageListBoxControlInputs.Size = new System.Drawing.Size(160, 77);
            this.imageListBoxControlInputs.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            this.imageListBoxControlInputs.TabIndex = 0;
            this.imageListBoxControlInputs.SelectedIndexChanged += new System.EventHandler(this.imageListBoxControlInputs_SelectedIndexChanged);
            // 
            // groupControlAudioIN
            // 
            this.groupControlAudioIN.Controls.Add(this.labelControlVolume);
            this.groupControlAudioIN.Controls.Add(this.simpleButtonSpeak);
            this.groupControlAudioIN.Controls.Add(this.trackBarControlVolume);
            this.groupControlAudioIN.Controls.Add(this.muteCheckBox);
            this.groupControlAudioIN.Enabled = false;
            this.groupControlAudioIN.Location = new System.Drawing.Point(3, 139);
            this.groupControlAudioIN.Name = "groupControlAudioIN";
            this.groupControlAudioIN.Size = new System.Drawing.Size(170, 110);
            this.groupControlAudioIN.TabIndex = 2;
            this.groupControlAudioIN.Text = "groupControlAudioIN";
            // 
            // labelControlVolume
            // 
            this.labelControlVolume.Location = new System.Drawing.Point(5, 24);
            this.labelControlVolume.Name = "labelControlVolume";
            this.labelControlVolume.Size = new System.Drawing.Size(34, 13);
            this.labelControlVolume.TabIndex = 12;
            this.labelControlVolume.Text = "Volume";
            // 
            // simpleButtonSpeak
            // 
            this.simpleButtonSpeak.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonSpeak.Image")));
            this.simpleButtonSpeak.Location = new System.Drawing.Point(127, 74);
            this.simpleButtonSpeak.Name = "simpleButtonSpeak";
            this.simpleButtonSpeak.Size = new System.Drawing.Size(30, 30);
            this.simpleButtonSpeak.TabIndex = 11;
            this.simpleButtonSpeak.MouseDown += new System.Windows.Forms.MouseEventHandler(this.simpleButtonSpeak_MouseDown);
            this.simpleButtonSpeak.MouseUp += new System.Windows.Forms.MouseEventHandler(this.simpleButtonSpeak_MouseUp);
            // 
            // trackBarControlVolume
            // 
            this.trackBarControlVolume.EditValue = 10;
            this.trackBarControlVolume.Location = new System.Drawing.Point(7, 43);
            this.trackBarControlVolume.Name = "trackBarControlVolume";
            this.trackBarControlVolume.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.trackBarControlVolume.Size = new System.Drawing.Size(158, 45);
            this.trackBarControlVolume.TabIndex = 4;
            this.trackBarControlVolume.Value = 10;
            this.trackBarControlVolume.EditValueChanged += new System.EventHandler(this.trackBarControlVolume_ValueChanged);
            // 
            // muteCheckBox
            // 
            this.muteCheckBox.AutoSize = true;
            this.muteCheckBox.Location = new System.Drawing.Point(5, 87);
            this.muteCheckBox.Name = "muteCheckBox";
            this.muteCheckBox.Size = new System.Drawing.Size(50, 17);
            this.muteCheckBox.TabIndex = 3;
            this.muteCheckBox.Text = "Mute";
            this.muteCheckBox.UseVisualStyleBackColor = true;
            this.muteCheckBox.CheckedChanged += new System.EventHandler(this.muteCheckBox_CheckedChanged);
            // 
            // dockPanelPlayback
            // 
            this.dockPanelPlayback.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.dockPanelPlayback.Appearance.Options.UseBackColor = true;
            this.dockPanelPlayback.Controls.Add(this.dockPanel2_Container);
            this.dockPanelPlayback.Dock = DevExpress.XtraBars.Docking.DockingStyle.Fill;
            this.dockPanelPlayback.FloatVertical = true;
            this.dockPanelPlayback.ID = new System.Guid("9fced63a-4f2b-4161-afa1-167cc7e3a817");
            this.dockPanelPlayback.Location = new System.Drawing.Point(3, 50);
            this.dockPanelPlayback.Name = "dockPanelPlayback";
            this.dockPanelPlayback.Options.AllowDockBottom = false;
            this.dockPanelPlayback.Options.AllowDockTop = false;
            this.dockPanelPlayback.Options.AllowFloating = false;
            this.dockPanelPlayback.Options.FloatOnDblClick = false;
            this.dockPanelPlayback.Options.ShowCloseButton = false;
            this.dockPanelPlayback.Options.ShowMaximizeButton = false;
            this.dockPanelPlayback.OriginalSize = new System.Drawing.Size(192, 497);
            this.dockPanelPlayback.Size = new System.Drawing.Size(194, 499);
            this.dockPanelPlayback.Text = "Playback";
            this.dockPanelPlayback.Enter += new System.EventHandler(this.dockPanelPlayback_Enter);
            // 
            // dockPanel2_Container
            // 
            this.dockPanel2_Container.Controls.Add(this.groupControl1);
            this.dockPanel2_Container.Controls.Add(this.groupControl1AudioPlayback);
            this.dockPanel2_Container.Controls.Add(this.groupControlPlayback);
            this.dockPanel2_Container.Location = new System.Drawing.Point(0, 0);
            this.dockPanel2_Container.Name = "dockPanel2_Container";
            this.dockPanel2_Container.Size = new System.Drawing.Size(194, 499);
            this.dockPanel2_Container.TabIndex = 0;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.simpleButtonExport);
            this.groupControl1.Location = new System.Drawing.Point(3, 249);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(170, 53);
            this.groupControl1.TabIndex = 4;
            this.groupControl1.Text = "groupControlExport";
            // 
            // simpleButtonExport
            // 
            this.simpleButtonExport.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButtonExport.Location = new System.Drawing.Point(11, 23);
            this.simpleButtonExport.Name = "simpleButtonExport";
            this.simpleButtonExport.Size = new System.Drawing.Size(56, 25);
            this.simpleButtonExport.TabIndex = 14;
            this.simpleButtonExport.Text = "Export";
            this.simpleButtonExport.Click += new System.EventHandler(this.simpleButtonExport_Click);
            // 
            // groupControl1AudioPlayback
            // 
            this.groupControl1AudioPlayback.Controls.Add(this.labelControlVulmePlayback);
            this.groupControl1AudioPlayback.Controls.Add(this.trackBarControlPlayback);
            this.groupControl1AudioPlayback.Controls.Add(this.checkBoxMutePlayback);
            this.groupControl1AudioPlayback.Location = new System.Drawing.Point(3, 133);
            this.groupControl1AudioPlayback.Name = "groupControl1AudioPlayback";
            this.groupControl1AudioPlayback.Size = new System.Drawing.Size(170, 110);
            this.groupControl1AudioPlayback.TabIndex = 3;
            this.groupControl1AudioPlayback.Text = "groupControl1";
            // 
            // labelControlVulmePlayback
            // 
            this.labelControlVulmePlayback.Location = new System.Drawing.Point(5, 24);
            this.labelControlVulmePlayback.Name = "labelControlVulmePlayback";
            this.labelControlVulmePlayback.Size = new System.Drawing.Size(34, 13);
            this.labelControlVulmePlayback.TabIndex = 12;
            this.labelControlVulmePlayback.Text = "Volume";
            // 
            // trackBarControlPlayback
            // 
            this.trackBarControlPlayback.EditValue = 10;
            this.trackBarControlPlayback.Location = new System.Drawing.Point(7, 43);
            this.trackBarControlPlayback.Name = "trackBarControlPlayback";
            this.trackBarControlPlayback.Size = new System.Drawing.Size(158, 45);
            this.trackBarControlPlayback.TabIndex = 4;
            this.trackBarControlPlayback.Value = 10;
            this.trackBarControlPlayback.EditValueChanged += new System.EventHandler(this.trackBarControlVolume_ValueChanged);
            // 
            // checkBoxMutePlayback
            // 
            this.checkBoxMutePlayback.AutoSize = true;
            this.checkBoxMutePlayback.Location = new System.Drawing.Point(5, 87);
            this.checkBoxMutePlayback.Name = "checkBoxMutePlayback";
            this.checkBoxMutePlayback.Size = new System.Drawing.Size(50, 17);
            this.checkBoxMutePlayback.TabIndex = 3;
            this.checkBoxMutePlayback.Text = "Mute";
            this.checkBoxMutePlayback.UseVisualStyleBackColor = true;
            this.checkBoxMutePlayback.CheckedChanged += new System.EventHandler(this.muteCheckBox_CheckedChanged);
            // 
            // groupControlPlayback
            // 
            this.groupControlPlayback.Controls.Add(this.simpleButtonGoto);
            this.groupControlPlayback.Controls.Add(this.checkButtonBack);
            this.groupControlPlayback.Controls.Add(this.checkButtonPlay);
            this.groupControlPlayback.Controls.Add(this.dateEditplayback);
            this.groupControlPlayback.Controls.Add(this.simpleButtonStop);
            this.groupControlPlayback.Controls.Add(this.labelControlPlaybackSpeed);
            this.groupControlPlayback.Controls.Add(this.zoomTrackBarControlSpeed);
            this.groupControlPlayback.Location = new System.Drawing.Point(3, 3);
            this.groupControlPlayback.Name = "groupControlPlayback";
            this.groupControlPlayback.Size = new System.Drawing.Size(170, 124);
            this.groupControlPlayback.TabIndex = 5;
            this.groupControlPlayback.Text = "groupControlPlayback";
            // 
            // simpleButtonGoto
            // 
            this.simpleButtonGoto.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonGoto.Image")));
            this.simpleButtonGoto.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButtonGoto.Location = new System.Drawing.Point(140, 21);
            this.simpleButtonGoto.Name = "simpleButtonGoto";
            this.simpleButtonGoto.Size = new System.Drawing.Size(25, 25);
            this.simpleButtonGoto.TabIndex = 22;
            this.simpleButtonGoto.Click += new System.EventHandler(this.simpleButtonGoto_Click);
            // 
            // checkButtonBack
            // 
            this.checkButtonBack.Image = ((System.Drawing.Image)(resources.GetObject("checkButtonBack.Image")));
            this.checkButtonBack.Location = new System.Drawing.Point(42, 93);
            this.checkButtonBack.Name = "checkButtonBack";
            this.checkButtonBack.Size = new System.Drawing.Size(25, 25);
            this.checkButtonBack.TabIndex = 21;
            this.checkButtonBack.Click += new System.EventHandler(this.checkButtonBack_CheckedChanged);
            // 
            // checkButtonPlay
            // 
            this.checkButtonPlay.Image = ((System.Drawing.Image)(resources.GetObject("checkButtonPlay.Image")));
            this.checkButtonPlay.Location = new System.Drawing.Point(104, 93);
            this.checkButtonPlay.Name = "checkButtonPlay";
            this.checkButtonPlay.Size = new System.Drawing.Size(25, 25);
            this.checkButtonPlay.TabIndex = 20;
            this.checkButtonPlay.Click += new System.EventHandler(this.checkButtonPlay_CheckedChanged);
            // 
            // dateEditplayback
            // 
            this.dateEditplayback.EditValue = null;
            this.dateEditplayback.Location = new System.Drawing.Point(6, 24);
            this.dateEditplayback.Name = "dateEditplayback";
            this.dateEditplayback.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dateEditplayback.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, false, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, false)});
            this.dateEditplayback.Properties.DisplayFormat.FormatString = "MM/dd/yyyy HH:mm:ss";
            this.dateEditplayback.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.dateEditplayback.Properties.EditFormat.FormatString = "MM/dd/yyyy HH:mm:ss";
            this.dateEditplayback.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.dateEditplayback.Properties.Mask.EditMask = "MM/dd/yyyy HH:mm:ss";
            this.dateEditplayback.Properties.ShowDropDown = DevExpress.XtraEditors.Controls.ShowDropDown.Never;
            this.dateEditplayback.Properties.ShowPopupShadow = false;
            this.dateEditplayback.Properties.ShowToday = false;
            this.dateEditplayback.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditplayback.Size = new System.Drawing.Size(128, 20);
            this.dateEditplayback.TabIndex = 17;
            // 
            // simpleButtonStop
            // 
            this.simpleButtonStop.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonStop.Image")));
            this.simpleButtonStop.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButtonStop.Location = new System.Drawing.Point(73, 93);
            this.simpleButtonStop.Name = "simpleButtonStop";
            this.simpleButtonStop.Size = new System.Drawing.Size(25, 25);
            this.simpleButtonStop.TabIndex = 13;
            this.simpleButtonStop.Click += new System.EventHandler(this.simpleButtonStop_Click);
            // 
            // labelControlPlaybackSpeed
            // 
            this.labelControlPlaybackSpeed.Location = new System.Drawing.Point(6, 50);
            this.labelControlPlaybackSpeed.Name = "labelControlPlaybackSpeed";
            this.labelControlPlaybackSpeed.Size = new System.Drawing.Size(107, 13);
            this.labelControlPlaybackSpeed.TabIndex = 1;
            this.labelControlPlaybackSpeed.Text = "labelControlPlaySpeed";
            // 
            // zoomTrackBarControlSpeed
            // 
            this.zoomTrackBarControlSpeed.EditValue = 4;
            this.zoomTrackBarControlSpeed.Location = new System.Drawing.Point(5, 69);
            this.zoomTrackBarControlSpeed.Name = "zoomTrackBarControlSpeed";
            this.zoomTrackBarControlSpeed.Properties.LargeChange = 1;
            this.zoomTrackBarControlSpeed.Properties.Maximum = 8;
            this.zoomTrackBarControlSpeed.Properties.ScrollThumbStyle = DevExpress.XtraEditors.Repository.ScrollThumbStyle.ArrowDownRight;
            this.zoomTrackBarControlSpeed.Size = new System.Drawing.Size(160, 20);
            this.zoomTrackBarControlSpeed.TabIndex = 0;
            this.zoomTrackBarControlSpeed.Value = 4;
            this.zoomTrackBarControlSpeed.EditValueChanged += new System.EventHandler(this.zoomTrackBarControlSpeed_EditValueChanged);
            // 
            // dockPanelEvents
            // 
            this.dockPanelEvents.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.dockPanelEvents.Appearance.Options.UseBackColor = true;
            this.dockPanelEvents.Controls.Add(this.controlContainer1);
            this.dockPanelEvents.Dock = DevExpress.XtraBars.Docking.DockingStyle.Bottom;
            this.dockPanelEvents.ID = new System.Guid("13f6c90e-f13b-4135-8cd4-b5e49a87ce90");
            this.dockPanelEvents.Location = new System.Drawing.Point(0, 471);
            this.dockPanelEvents.Name = "dockPanelEvents";
            this.dockPanelEvents.Options.AllowDockLeft = false;
            this.dockPanelEvents.Options.AllowDockRight = false;
            this.dockPanelEvents.Options.AllowDockTop = false;
            this.dockPanelEvents.Options.AllowFloating = false;
            this.dockPanelEvents.Options.FloatOnDblClick = false;
            this.dockPanelEvents.Options.ShowCloseButton = false;
            this.dockPanelEvents.Options.ShowMaximizeButton = false;
            this.dockPanelEvents.OriginalSize = new System.Drawing.Size(200, 200);
            this.dockPanelEvents.Size = new System.Drawing.Size(792, 200);
            this.dockPanelEvents.Text = "Events";
            // 
            // controlContainer1
            // 
            this.controlContainer1.Controls.Add(this.gridControlExEvents);
            this.controlContainer1.Location = new System.Drawing.Point(3, 29);
            this.controlContainer1.Name = "controlContainer1";
            this.controlContainer1.Size = new System.Drawing.Size(586, 168);
            this.controlContainer1.TabIndex = 0;
            // 
            // gridControlExEvents
            // 
            this.gridControlExEvents.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlExEvents.EnableAutoFilter = true;
            this.gridControlExEvents.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExEvents.Location = new System.Drawing.Point(0, 0);
            this.gridControlExEvents.MainView = this.gridViewExEvents;
            this.gridControlExEvents.Name = "gridControlExEvents";
            this.gridControlExEvents.Size = new System.Drawing.Size(586, 168);
            this.gridControlExEvents.TabIndex = 5;
            this.gridControlExEvents.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExEvents});
            this.gridControlExEvents.ViewTotalRows = false;
            // 
            // gridViewExEvents
            // 
            this.gridViewExEvents.AllowFocusedRowChanged = true;
            this.gridViewExEvents.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewExEvents.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExEvents.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewExEvents.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewExEvents.Appearance.FooterPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewExEvents.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewExEvents.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewExEvents.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExEvents.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewExEvents.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewExEvents.EnablePreviewLineForFocusedRow = false;
            this.gridViewExEvents.GridControl = this.gridControlExEvents;
            this.gridViewExEvents.Name = "gridViewExEvents";
            this.gridViewExEvents.OptionsMenu.EnableFooterMenu = false;
            this.gridViewExEvents.OptionsNavigation.UseTabKey = false;
            this.gridViewExEvents.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExEvents.OptionsView.ShowDetailButtons = false;
            this.gridViewExEvents.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewExEvents.OptionsView.ShowFooter = true;
            this.gridViewExEvents.ViewTotalRows = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(457, 170);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem1";
            this.layoutControlItem2.Size = new System.Drawing.Size(457, 170);
            this.layoutControlItem2.Text = "layoutControlItem1";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // tmrUpdateStick
            // 
            this.tmrUpdateStick.Interval = 1;
            this.tmrUpdateStick.Tick += new System.EventHandler(this.tmrUpdateStick_Tick);
            // 
            // barButtonItemStartRegIncident
            // 
            this.barButtonItemStartRegIncident.Caption = "ItemStartRegIncident";
            this.barButtonItemStartRegIncident.Id = 12;
            this.barButtonItemStartRegIncident.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemStartRegIncident.Name = "barButtonItemStartRegIncident";
            this.barButtonItemStartRegIncident.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // CamerasRibbonForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(792, 671);
            this.Controls.Add(this.panelContainer1);
            this.Controls.Add(this.dockPanelEvents);
            this.Controls.Add(this.ribbonControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MinimumSize = new System.Drawing.Size(808, 709);
            this.Name = "CamerasRibbonForm";
            this.Text = "CamerasRibbonForm";
            this.Activated += new System.EventHandler(this.CamerasRibbonForm_Activated);
            this.Deactivate += new System.EventHandler(this.CamerasRibbonForm_Deactivate);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CamerasRibbonForm_FormClosing);
            this.Load += new System.EventHandler(this.CamerasRibbonForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager)).EndInit();
            this.panelContainer1.ResumeLayout(false);
            this.dockPanelControls.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControlOutput)).EndInit();
            this.groupControlOutput.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imageListBoxControlOutputs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlPanTiltZoom)).EndInit();
            this.groupControlPanTiltZoom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControlInput)).EndInit();
            this.groupControlInput.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imageListBoxControlInputs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlAudioIN)).EndInit();
            this.groupControlAudioIN.ResumeLayout(false);
            this.groupControlAudioIN.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarControlVolume.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarControlVolume)).EndInit();
            this.dockPanelPlayback.ResumeLayout(false);
            this.dockPanel2_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1AudioPlayback)).EndInit();
            this.groupControl1AudioPlayback.ResumeLayout(false);
            this.groupControl1AudioPlayback.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarControlPlayback.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarControlPlayback)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlPlayback)).EndInit();
            this.groupControlPlayback.ResumeLayout(false);
            this.groupControlPlayback.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditplayback.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditplayback.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.zoomTrackBarControlSpeed.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.zoomTrackBarControlSpeed)).EndInit();
            this.dockPanelEvents.ResumeLayout(false);
            this.controlContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExEvents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExEvents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

		private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
		private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
		private DevExpress.XtraBars.BarButtonItem barButtonItemCascade;
		private DevExpress.XtraBars.BarButtonItem barButtonItemArrange;
		private DevExpress.XtraBars.BarButtonItem barButtonItemTileHorizontal;
		private DevExpress.XtraBars.BarButtonItem barButtonItemTileVertical;
        private DevExpress.XtraTabbedMdi.XtraTabbedMdiManager xtraTabbedMdiManager1;
        private DevExpress.XtraBars.Docking.DockManager dockManager;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        public DevExpress.XtraEditors.GroupControl groupControlOutput;
        private DevExpress.XtraEditors.SimpleButton simpleButtonOutput;
        public DevExpress.XtraEditors.GroupControl groupControlPanTiltZoom;
        private DevExpress.XtraEditors.SimpleButton simpleButtonZoomOut;
        private DevExpress.XtraEditors.SimpleButton simpleButtonBottomRight;
        private DevExpress.XtraEditors.SimpleButton simpleButtonBottom;
        private DevExpress.XtraEditors.SimpleButton simpleButtonZoomIn;
        private DevExpress.XtraEditors.SimpleButton simpleButtonBottomLeft;
        private DevExpress.XtraEditors.SimpleButton simpleButtonMiddleRight;
        private DevExpress.XtraEditors.SimpleButton simpleButtonMiddle;
        private DevExpress.XtraEditors.SimpleButton simpleButtonMiddleLeft;
        private DevExpress.XtraEditors.SimpleButton simpleButtonUpperRight;
        private DevExpress.XtraEditors.SimpleButton simpleButtonUpper;
        private DevExpress.XtraEditors.SimpleButton simpleButtonUpperLeft;
        public DevExpress.XtraEditors.GroupControl groupControlInput;
        public DevExpress.XtraEditors.GroupControl groupControlAudioIN;
        private DevExpress.XtraEditors.LabelControl labelControlVolume;
        public DevExpress.XtraEditors.TrackBarControl trackBarControlVolume;
        public System.Windows.Forms.CheckBox muteCheckBox;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel2_Container;
        private DevExpress.XtraEditors.GroupControl groupControl1AudioPlayback;
        private DevExpress.XtraEditors.LabelControl labelControlVulmePlayback;
        public DevExpress.XtraEditors.TrackBarControl trackBarControlPlayback;
        public System.Windows.Forms.CheckBox checkBoxMutePlayback;
        private DevExpress.XtraEditors.GroupControl groupControlPlayback;
        private DevExpress.XtraEditors.SimpleButton simpleButtonGoto;
        private DevExpress.XtraEditors.SimpleButton simpleButtonStop;
        private DevExpress.XtraEditors.LabelControl labelControlPlaybackSpeed;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelEvents;
        private DevExpress.XtraBars.Docking.ControlContainer controlContainer1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private GridControlEx gridControlExEvents;
        private GridViewEx gridViewExEvents;
        public System.Windows.Forms.Timer tmrUpdateStick;
        public DevExpress.XtraEditors.SimpleButton simpleButtonSpeak;
        public DevExpress.XtraEditors.ImageListBoxControl imageListBoxControlOutputs;
        public DevExpress.XtraEditors.ImageListBoxControl imageListBoxControlInputs;
        public DevExpress.XtraBars.Docking.DockPanel dockPanelControls;
        public DevExpress.XtraBars.Docking.DockPanel panelContainer1;
        public DevExpress.XtraBars.Docking.DockPanel dockPanelPlayback;
        public DevExpress.XtraEditors.CheckButton checkButtonBack;
        public DevExpress.XtraEditors.CheckButton checkButtonPlay;
        public DevExpress.XtraEditors.DateEdit dateEditplayback;
        public DevExpress.XtraEditors.ZoomTrackBarControl zoomTrackBarControlSpeed;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButtonExport;
        private DevExpress.XtraBars.BarButtonItem barButtonItemStartIncident;
        public DevExpress.XtraBars.BarButtonItem barButtonItemStartRegIncident;
	}
}