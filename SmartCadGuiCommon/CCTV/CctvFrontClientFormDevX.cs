using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Smartmatic.SmartCad.Service;
using System.Net;

using System.Collections;
using DevExpress.XtraBars;
using System.IO;
using System.Xml;
using SmartCadCore.Core;
using SmartCadCore.Common;
using SmartCadCore.ClientData;
using SmartCadCore.Enums;
using SmartCadGuiCommon;
using SmartCadGuiCommon.Classes;
using SmartCadGuiCommon.Controls.Administration;
using SmartCadGuiCommon.CCTV.Nodes;
using VideoOS.Platform;
using Smartmatic.SmartCad.Vms;
using SmartCadGuiCommon.Util;
using SmartCadGuiCommon.Controls;

namespace SmartCadGuiCommon
{
	public partial class CctvFrontClientFormDevX : XtraForm
	{
		#region Fields

		DefaultCctvFrontClientFormDevX defaultCctvFrontClientFormDevX;
        public static CameraVisualizeForm cameraVisualizeForm;
        CameraAlarmsForm cameraAlarmsForm;
        PlayBackForm playBackForm;
        CctvIncidentsHistoryForm historyForm;
        CctvAlarmsForm alarmForm;
        OperatorChatForm chatForm;
        private ConfigurationClientData config = null;
		private ApplicationMessageClientData alertControlAmcd = null;
<<<<<<< HEAD
        private bool viewAlarm = true; //this value set the VA funtions ON
=======
        private bool viewAlarm = true  ; //this value set the VA funtions ON   
>>>>>>> cfce0f157aed6a5ff665701816811b689b60a574
        public BindingList<CctvCameraNode> Cameras = new BindingList<CctvCameraNode>();
        public BindingList<CctvTemplateNode> Templates = new BindingList<CctvTemplateNode>();
        VmsControlEx vmsControlExit ;
        private DateTime syncServerDateTime;
        private TimeSpan sinceLoggedIn;
        private const int GLOBAL_TIMER_PERIOD = 10000;
        private readonly TimeSpan ADD_TIMESPAN;
        public System.Threading.Timer globalTimer;
        private string labelUserCaption = "";
        private string labelConnectedCaption = "";
        private VmsSingletonUtil VmsControl;
        ServerServiceClient serverServiceClient = null;
        CameraPanelControl cameraPanelControlAccess;
        TemplatePanelControl templatePanelControlAccess;
		#endregion

		#region Properties

        public bool ValueViewAlarm(){
            return viewAlarm;
        }
		public ServerServiceClient ServerServiceClient
		{
			set
			{
				defaultCctvFrontClientFormDevX.ServerServiceClient = value;
			}
		}
		
		public NetworkCredential NetworkCredential
		{
			set
			{
				defaultCctvFrontClientFormDevX.NetworkCredential = value;
			}
		}

		public ConfigurationClientData ConfigurationClient
		{
			set
			{
				defaultCctvFrontClientFormDevX.ConfigurationClient = value;
			}
		}

		public string ExtNumber
		{
			set
			{
				defaultCctvFrontClientFormDevX.ExtNumber = value;
			}
		}

        #endregion

		public CctvFrontClientFormDevX()
		{
            var applications = ServerServiceClient.GetInstance().GetActiveApplications();

            InitializeComponent();

            if (!applications.Contains(SmartCadApplications.SmartCadMap)) 
            {
                ribbonPageGroupTools.Visible = false;
            }

            if (!applications.Contains(SmartCadApplications.SmartCadSupervision)) 
            {
                barButtonItemChat.Visibility = BarItemVisibility.Never;
            }
            WindowState = FormWindowState.Maximized;

            defaultCctvFrontClientFormDevX = new DefaultCctvFrontClientFormDevX();// { MdiParent = this };

			LoadLanguage();
			ServerServiceClient.GetInstance().CommittedChanges += FrontClientFormDevX_CommittedChanges;
            ADD_TIMESPAN = new TimeSpan(0, 0, 0, 0, GLOBAL_TIMER_PERIOD);

            VmsControl = new VmsSingletonUtil();

            config = ServerServiceClient.GetInstance().GetConfiguration();
            vmsControlExit = VmsControlEx.GetInstance(config.VmsDllName);
            cameraPanelControlAccess = new CameraPanelControl() ;
            templatePanelControlAccess = new TemplatePanelControl();
        }

        private void LoadCamerasAndTemplates()
        {
            #region Load Cameras
            int operCode = (int)ServerServiceClient.GetInstance().OperatorClient.Code;
            IList userCameras = ServerServiceClient.GetInstance().SearchClientObjects(
                SmartCadHqls.GetCustomHql(SmartCadHqls.GetCamerasByOperatorCode, operCode));

            foreach (CameraClientData clientData in userCameras)
            {
                Cameras.Add(new CctvCameraNode(clientData));
            }
            #endregion

            #region Load Camera Templates
            string[] files = null;
            try
            {
                files = Directory.GetFiles(SmartCadConfiguration.CameraTemplates, "*.xml");
            }
            catch { }

            foreach (string fileName in files)
            {
                StreamReader sr = new StreamReader(fileName);
                string renderMethod = sr.ReadToEnd();
                sr.Close();

                CctvTemplateNode template = new CctvTemplateNode(fileName, Path.GetFileNameWithoutExtension(fileName), renderMethod);
                AddCamerasToTemplate(template);
                Templates.Add(template);
            }
            #endregion
        }

        private void FillStatusStrip()
        {
            try
            {
                syncServerDateTime = ServerServiceClient.GetInstance().GetTime();
                sinceLoggedIn = TimeSpan.Zero;
                this.labelConnected.Caption = "00:00";

                labelDate.Caption = ApplicationUtil.GetFormattedDate(serverServiceClient.GetTime());
                globalTimer = ApplicationUtil.RecurrentExecuteMethod(
                    new SimpleDelegate(
                    delegate
                    {
                        globalTimer_Tick(null, null);
                    }), null, null, GLOBAL_TIMER_PERIOD, GLOBAL_TIMER_PERIOD);

                labelUser.Caption = labelUserCaption + serverServiceClient.OperatorClient.FirstName
                    + " " + serverServiceClient.OperatorClient.LastName;
                labelConnected.Caption = labelConnectedCaption + "00:00";
            }
            catch { }
        }

        private void globalTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                syncServerDateTime = syncServerDateTime.AddMilliseconds((double)GLOBAL_TIMER_PERIOD);
                sinceLoggedIn = sinceLoggedIn.Add(ADD_TIMESPAN);
                FormUtil.InvokeRequired(this, delegate
                {
                    labelDate.Caption = ApplicationUtil.GetFormattedDate(ServerServiceClient.GetInstance().GetTime());
                    labelConnected.Caption = labelConnectedCaption + Math.Truncate(sinceLoggedIn.TotalHours).ToString().PadLeft(2, '0') + ":" + sinceLoggedIn.Minutes.ToString().PadLeft(2, '0');
                });

            }
            catch {
                Console.WriteLine("Error X ROJA");
            }
        }

        public void DeleteTemplate(CctvTemplateNode template)
        {
            playBackForm.DeleteTemplate(template);
        }

        public void AddNewTemplate(CctvTemplateNode template)
        {
            playBackForm.AddNewTemplate(template);
        }

        public void UpdateTemplate(CctvTemplateNode template)
        {
            playBackForm.DeleteTemplate(template);
            playBackForm.AddNewTemplate(template);
        }

        private void AddCamerasToTemplate(CctvTemplateNode template)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(template.Name);
                XmlNode firstChild = doc.FirstChild;
                foreach (XmlNode node in firstChild.ChildNodes)
                {
                    if (node.Name == "Control")
                    {
                        foreach (CctvCameraNode camera in Cameras as BindingList<CctvCameraNode>)
                        {
                            if (camera.Name == node.Attributes["itemName"].Value)
                            {
                                template.Cameras.Add(camera);
                                break;
                            }
                        }
                    }
                }
            }
            catch
            { }
        }

		private void LoadLanguage()
		{
            Text = ResourceLoader.GetString2("CCTVModule");
            barButtonItemOpenMap.Glyph = ResourceLoader.GetImage("$Image.OpenMap");
            barButtonItemChat.Caption = ResourceLoader.GetString2("Chat");
            barButtonItemOpenMap.LargeGlyph = ResourceLoader.GetImage("$Image.OpenMap");

			barButtonItemChat.Caption = ResourceLoader.GetString2("Chat");
			barButtonItemOpenMap.Caption = ResourceLoader.GetString2("Open");
			barButtonItemPrint.Caption = ResourceLoader.GetString2("Print");
			barButtonItemRefresh.Caption = ResourceLoader.GetString2("Refresh");
			barButtonItemSave.Caption = ResourceLoader.GetString2("Save");
			barButtonItemCancelIncident.Caption = ResourceLoader.GetString2("Cancel");
            barButtonItemCreateNewIncident.Caption = ResourceLoader.GetString2("Register");
            barButtonItemHistory.Caption = ResourceLoader.GetString2("IncidentsHistoryForm");
            barButtonItemAlarm.Caption = ResourceLoader.GetString2("AlarmsForm");
			ribbonPageGroupCallInfo.Text = ResourceLoader.GetString2("Incidents");
			ribbonPageGroupGeneralOptions.Text = ResourceLoader.GetString2("GeneralOptions");
			ribbonPageGroupApplications.Text = ResourceLoader.GetString2("Applications");
			ribbonPageGroupTools.Text = ResourceLoader.GetString2("Maps");
			barButtonItemStartRegIncident.Caption = ResourceLoader.GetString2("New");
			//Text = defaultCctvFrontClientFormDevX.Text;
			ribbonPage1.Text = ResourceLoader.GetString2("Cctv");
            barButtonItemCancelVA_Alarm.Caption = ResourceLoader.GetString2("Dismiss");
            ribbonPageGroupAlarm.Text = ResourceLoader.GetString2("Alarms");

            #region Footer
            labelUserCaption = ResourceLoader.GetString2("User") + ": ";
            labelConnectedCaption = ResourceLoader.GetString2("Connected") + ": ";
            #endregion
		}
		
		public void FrontClientFormDevX_CommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
		{
			try
			{
				if (e != null && e.Objects != null && e.Objects.Count > 0)
				{
					if (e.Objects[0] is ApplicationMessageClientData)
					{
						ApplicationMessageClientData amcd = ((ApplicationMessageClientData)e.Objects[0]);
						if (amcd.ToOperators.Contains(ServerServiceClient.GetInstance().OperatorClient))
						{
							if (chatForm != null && chatForm.IsDisposed == false)
							{
                                FormUtil.InvokeRequired(this, () =>
								{
									chatForm.OpenNewTab(amcd.Operator, amcd);
									chatForm.Activate();
								});
							}
							else
							{
								alertControlAmcd = amcd;
								FormUtil.InvokeRequired(this, () =>
								{
									alertControl1.Show(this, "Message from: " + amcd.Operator.Login, amcd.Message);
								});
							}
						}
					}
					else if (e.Objects[0] is ApplicationPreferenceClientData)
                    {
                        #region ApplicationPreferenceClientData
                        ApplicationPreferenceClientData preference = e.Objects[0] as ApplicationPreferenceClientData;
						if (e.Action == CommittedDataAction.Update)
						{
							if (preference.Name.Equals("AlarmTimeLimit"))
							{
								FormUtil.InvokeRequired(this, () =>
                                {
									repositoryItemProgressBar1.Maximum = int.Parse(preference.Value);
								});
							}
                            else if (preference.Name.Equals("CCTVIncidentCodePrefix"))
                            {
                                historyForm.CCTVIncidentCodePrefix.Value = preference.Value;
                            }
                            else if (preference.Name.Equals("CctvVideoSecondsBefore"))
                            {
                                FormUtil.InvokeRequired(this, () =>
                                {
                                    historyForm.CctvVideoSecondsBefore.Value = preference.Value;
                                    //cameraAlarmsForm.CctvVideoSecondsBefore.Value = preference.Value;
                                });
                            }
                            else if (preference.Name.Equals("CctvVideoSecondsAfter"))
                            {
                                FormUtil.InvokeRequired(this, () =>
                                {
                                    historyForm.CctvVideoSecondsAfter.Value = preference.Value;
                                    //cameraAlarmsForm.CctvVideoSecondsAfter.Value = preference.Value;
                                });
                            }
                        }
                        #endregion
                    }
                    else if (e.Objects[0] is Item)
                    {
                        CameraClientData camera = ((CameraClientData)e.Objects[0]);
                        FormUtil.InvokeRequired(this, () =>
                        {
                            foreach (CctvCameraNode data in Cameras)
                            {
                                if (data.Camera.Code == camera.Code)
                                {
                                    data.Camera = camera;
                                    cameraVisualizeForm.LoadCameras();
                                    if (cameraVisualizeForm.SelectedCamera != null && (cameraVisualizeForm.SelectedCamera.Camera.Code) == camera.Code)
                                        cameraVisualizeForm.SelectedCamera.Camera = camera;
                                    playBackForm.LoadCameras();
                                    //cameraAlarmsForm.LoadInitialData();
                                    //if (cameraAlarmsForm.SelectedAlarm != null && cameraAlarmsForm.SelectedAlarm.Camera.Code == camera.Code)
                                    //    cameraAlarmsForm.SelectedAlarm.Camera = camera;
                                    break;
                                }
                            }
                        });
                    }
				}
			}
			catch
			{
			}
		}

		public void SetPassword(string p)
		{
			defaultCctvFrontClientFormDevX.SetPassword(p);
            ServerServiceClient.GetInstance().OperatorClient.Password = p;
		}

        public void LoadInitialData()
        {
            serverServiceClient = ServerServiceClient.GetInstance();

            cameraVisualizeForm = new CameraVisualizeForm() { MdiParent = this, ServerServiceClient = serverServiceClient };
            defaultCctvFrontClientFormDevX.MdiParent = this;

            //COMMENTED BATAAN VERSION 3.3.1 AA
            //cameraAlarmsForm = new CameraAlarmsForm() { MdiParent = this, ServerServiceClient = serverServiceClient };
            playBackForm = new PlayBackForm() { MdiParent = this, ServerServiceClient = serverServiceClient, VmsControl = VmsControl };

            //////URDANETA 
            if(viewAlarm)
            alarmForm = new CctvAlarmsForm() { MdiParent = this, VmsControl = VmsControl };
            
            LoadCamerasAndTemplates();
            cameraVisualizeForm.LoadInitialData();
            defaultCctvFrontClientFormDevX.LoadInitialData();

            //COMMENTED BATAAN VERSION 3.3.1 AA
            //cameraAlarmsForm.LoadInitialData();
            playBackForm.LoadInitialData();
            

            CheckAccessToMap();

            xtraTabbedMdiManager1.Pages[cameraVisualizeForm].ShowCloseButton = DevExpress.Utils.DefaultBoolean.False;
            xtraTabbedMdiManager1.Pages[defaultCctvFrontClientFormDevX].ShowCloseButton = DevExpress.Utils.DefaultBoolean.False;

            //COMMENTED BATAAN VERSION 3.3.1 AA
            //xtraTabbedMdiManager1.Pages[cameraAlarmsForm].ShowCloseButton = DevExpress.Utils.DefaultBoolean.False;
            xtraTabbedMdiManager1.Pages[playBackForm].ShowCloseButton = DevExpress.Utils.DefaultBoolean.False;

            FillStatusStrip();
        }

		private void FrontClientFormDevX_Load(object sender, EventArgs e)
		{
            playBackForm.Show();

            //COMMENTED BATAAN VERSION 3.3.1 AA
            //cameraAlarmsForm.Show();
            defaultCctvFrontClientFormDevX.Show();
            //cameraVisualizeForm.Show();

			repositoryItemProgressBar1.Step = 1;
			repositoryItemProgressBar1.Maximum = int.Parse(defaultCctvFrontClientFormDevX.GlobalApplicationPreference["AlarmTimeLimit"].Value);

            xtraTabbedMdiManager1.SelectedPageChanged += new EventHandler(xtraTabbedMdiManager1_SelectedPageChanged);

            //////URDANETA
            if(viewAlarm)
            alarmForm.Show();
            //

           cameraVisualizeForm.Show();
            
		}

        void xtraTabbedMdiManager1_SelectedPageChanged(object sender, EventArgs e)
        {
            //Se verifica si la pesta�a en donde esta posicionado es distinta a la de alarmas
            if (viewAlarm)
            { 
                if (xtraTabbedMdiManager1.SelectedPage.Text != alarmForm.Text)
                {
                    alarmForm.globalTimer.Dispose();
                }
            }

            if (xtraTabbedMdiManager1.SelectedPage.Text == playBackForm.Text)
            {
                
                // Metodo para leer lista de exportar
                //verifyExportersVideos();
                

                if (playBackForm.CurrentPanel != null)
                {
                    playBackForm.CurrentPanel.CameraPanelControlTemplates();           
                    playBackForm.CurrentPanel.ClearControlPanel();
                    playBackForm.SetPlayBackSpeed();
                    CheckUnpin();
                }
                else {


                    //CheckUnpin
                    
                    if (cameraVisualizeForm.SelectedCamera != null)
                    {
                        cameraVisualizeForm.SelectedCamera.CameraPanelControlTemplates();
                        CheckUnpin();
                    }

                    if (cameraVisualizeForm.SelectedTemplate != null)
                    {
                        cameraVisualizeForm.SelectedTemplate.PanelControl.CameraPanelControlTemplates();
                        cameraVisualizeForm.SelectedTemplate.PanelControl.ClearVMSControl();
                        CheckUnpin();
                    }

                }

                barButtonItemStartRegIncident.Enabled = false;
                //URDANETA
                if(viewAlarm != false){
                alarmForm.barButtonItemCancelVA_Alarm.Enabled = false;
                alarmForm.barButtonItemCancelVA_Alarm.Refresh();
                alarmForm.resetValuesFromPanel();
                }
            }
            if (xtraTabbedMdiManager1.SelectedPage.Text == defaultCctvFrontClientFormDevX.Text)
            {

                if (selectedTrigger != null)
                {
                    CheckUnpinRegister();
                    //CheckUnpin(); 
                }
                else
                {
                    
                    if (cameraVisualizeForm.SelectedCamera != null)
                    {
                        cameraVisualizeForm.SelectedCamera.CameraPanelControlTemplates();
                        CheckUnpin();
                    }

                    if (cameraVisualizeForm.SelectedTemplate != null)
                    {
                        cameraVisualizeForm.SelectedTemplate.PanelControl.CameraPanelControlTemplates();
                        CheckUnpin();
                    }
                }
                

                barButtonItemStartRegIncident.Enabled = false;
                //URDANETA
                if (viewAlarm != false)
                {
                    alarmForm.barButtonItemCancelVA_Alarm.Enabled = false;
                    alarmForm.barButtonItemCancelVA_Alarm.Refresh();
                    alarmForm.resetValuesFromPanel();
                }
                //if (cameraVisualizeForm.SelectedCamera != null)
                //{

                //    cameraVisualizeForm.SelectedCamera.CameraPanelControlTemplates();
                //}

                //if (cameraVisualizeForm.SelectedTemplate != null)
                //{
                //    cameraVisualizeForm.SelectedTemplate.PanelControl.CameraPanelControlTemplates();
                //}
            }
            else if (xtraTabbedMdiManager1.SelectedPage.Text == cameraVisualizeForm.Text)
            {

                CheckUnpin();

                if (defaultCctvFrontClientFormDevX.CctvClientState == CctvClientStateEnum.RegisteringCctvIncident)
                { 
                    barButtonItemStartRegIncident.Enabled = false;
                    //URDANETA
                    if (viewAlarm != false)
                    {
                        alarmForm.barButtonItemCancelVA_Alarm.Enabled = false;
                        alarmForm.barButtonItemCancelVA_Alarm.Refresh();
                        alarmForm.resetValuesFromPanel();
                    }
                    if (cameraVisualizeForm.SelectedCamera != null && cameraVisualizeForm.SelectedTemplate == null)
                    {
                        barButtonItemStartRegIncident.Enabled = false;
                        //URDANETA
                        if (viewAlarm != false)
                        {
                            alarmForm.barButtonItemCancelVA_Alarm.Enabled = false;
                            alarmForm.barButtonItemCancelVA_Alarm.Refresh();
                            alarmForm.resetValuesFromPanel();
                        }
                        cameraVisualizeForm.StartPanel();
                        cameraVisualizeForm.SelectedCamera.SetPlayMode(PlayModes.Live);
                    }
                    else
                    {
                        if (cameraVisualizeForm.SelectedTemplate != null)
                        {
                            cameraVisualizeForm.SaveTemplate();
                            barButtonItemStartRegIncident.Enabled = false;
                            //URDANETA
                            if (viewAlarm != false)
                            {
                                alarmForm.barButtonItemCancelVA_Alarm.Enabled = false;
                                alarmForm.barButtonItemCancelVA_Alarm.Refresh();
                                alarmForm.resetValuesFromPanel();
                            }
                            cameraVisualizeForm.StartTemplate();
                        }
                    }
                }
                else
                {
                    if (cameraVisualizeForm.SelectedCamera != null && cameraVisualizeForm.SelectedTemplate == null)
                    {
                        barButtonItemStartRegIncident.Enabled = false;
                        //URDANETA
                        if (viewAlarm != false)
                        {
                            alarmForm.barButtonItemCancelVA_Alarm.Enabled = false;
                            alarmForm.barButtonItemCancelVA_Alarm.Refresh();
                            alarmForm.resetValuesFromPanel();
                        }
                        cameraVisualizeForm.StartPanel();
                        cameraVisualizeForm.SelectedCamera.SetPlayMode(PlayModes.Live);
                    }
                    else
                    {
                        if (cameraVisualizeForm.SelectedTemplate != null)
                        {
                            cameraVisualizeForm.SaveTemplate();
                            barButtonItemStartRegIncident.Enabled = false;
                            //URDANETA
                            if (viewAlarm != false)
                            {
                                alarmForm.barButtonItemCancelVA_Alarm.Enabled = false;
                                alarmForm.barButtonItemCancelVA_Alarm.Refresh();
                                alarmForm.resetValuesFromPanel();
                            }
                            cameraVisualizeForm.StartTemplate();
                            
                        }
                    }

                    if (cameraVisualizeForm.SelectedCamera == null && cameraVisualizeForm.SelectedTemplate == null && playBackForm.CurrentPanel != null)
                    {
                        playBackForm.CurrentPanel.CameraPanelControlTemplates();
                        playBackForm.CurrentPanel.ClearControlPanel();
                    }

                }
            }else if (viewAlarm)
            { 
                if(xtraTabbedMdiManager1.SelectedPage.Text == alarmForm.Text)
                {
                    if (cameraVisualizeForm.SelectedCamera != null)
                    {
                        cameraVisualizeForm.SelectedCamera.CameraPanelControlTemplates();
                        CheckUnpin();
                    }

                    if (cameraVisualizeForm.SelectedTemplate != null)
                    {
                        cameraVisualizeForm.SelectedTemplate.PanelControl.CameraPanelControlTemplates();
                        CheckUnpin();
                    }

                    if (cameraVisualizeForm.SelectedCamera == null && cameraVisualizeForm.SelectedTemplate == null && playBackForm.CurrentPanel != null)
                    {
                        playBackForm.CurrentPanel.CameraPanelControlTemplates();
                        playBackForm.CurrentPanel.ClearControlPanel();
                    }

                    alarmForm.BackgroundSearch(null);
                    alarmForm.ExecuteSyncro();
                    barButtonItemStartRegIncident.Enabled = false;
                    //URDANETA
                    alarmForm.barButtonItemCancelVA_Alarm.Enabled = false;
                    alarmForm.barButtonItemCancelVA_Alarm.Refresh();
                    alarmForm.resetValuesFromPanel();
                }
       //         alarmForm.BackgroundSearch();
                alarmForm.ExecuteSyncro();
  
               

                barButtonItemStartRegIncident.Enabled = false;
                //URDANETA
                alarmForm.barButtonItemCancelVA_Alarm.Enabled = false;
                alarmForm.barButtonItemCancelVA_Alarm.Refresh();
                alarmForm.resetValuesFromPanel();
            }
            else if(historyForm != null)
            {

                // Metodo para leer lista de exportar
                //verifyExportersVideos();
                barButtonItemStartRegIncident.Enabled = false;
                //URDANETA
                if (viewAlarm != false)
                {
                    alarmForm.barButtonItemCancelVA_Alarm.Enabled = false;
                    alarmForm.barButtonItemCancelVA_Alarm.Refresh();
                    alarmForm.resetValuesFromPanel();
                }

                if (xtraTabbedMdiManager1.SelectedPage.Text == historyForm.Text)
                {
                    if (cameraVisualizeForm.SelectedCamera != null)
                    {
                        cameraVisualizeForm.SelectedCamera.CameraPanelControlTemplates();
                        CheckUnpin();
                    }

                    if (cameraVisualizeForm.SelectedTemplate != null)
                    {
                        cameraVisualizeForm.SelectedTemplate.PanelControl.CameraPanelControlTemplates();
                        CheckUnpin();
                    }

                    if (historyForm.gridControlExPreviousIncidents.Items.Count != 0)  historyForm.gridControlExPreviousIncidents.ClearData();

                    if (playBackForm.CurrentPanel != null)
                    {
                        playBackForm.CurrentPanel.CameraPanelControlTemplates();
                        playBackForm.CurrentPanel.ClearVMSControlPlayback();
                        historyForm.VmsControl.vmsControl = null;
                    }

                }
                    
            }
            //COMMENTED BATAAN VERSION 3.3.1 AA
            //else if (xtraTabbedMdiManager1.SelectedPage.Text == cameraAlarmsForm.Text)
            //{
            //    if (defaultCctvFrontClientFormDevX.CctvClientState == CctvClientStateEnum.RegisteringCctvIncident)
            //        barButtonItemStartRegIncident.Enabled = false;
            //    else
            //    {
            //        if (cameraAlarmsForm.SelectedAlarm != null)
            //            barButtonItemStartRegIncident.Enabled = true;
            //        else
            //            barButtonItemStartRegIncident.Enabled = false;
            //    }
            //}
        }

        private void FrontClientFormDevX_FormClosing(object sender, FormClosingEventArgs e)
        {
            //check if a call has entered or if an incident is being typed
            //if (e.CloseReason == CloseReason.None)
            //{

            //    ServerServiceClient.GetInstance().CommittedChanges -= FrontClientFormDevX_CommittedChanges;
            //    Application.Exit();
            //}
            //else
            //{
            //    e.Cancel = true;
            //}
            ServerServiceClient.GetInstance().CommittedChanges -= FrontClientFormDevX_CommittedChanges;
            

            if (!EnsureLogout())
            {
                e.Cancel = true;
            }
            else 
            {
                if (!vmsControlExit.ReleaseConnectionExport())
                {
                    MessageForm.Show(ResourceLoader.GetString2("VideoExporting"), MessageFormType.Information);
                    e.Cancel = true;
                }
                else
                {
                    e.Cancel = false;
                }
            }

        }

        private static bool EnsureLogout()
		{
			DialogResult dialogResult = MessageForm.Show(ResourceLoader.GetString2("$Message.ExitApplication"), MessageFormType.Question);

			return dialogResult == DialogResult.Yes;
		}

		private void CheckAccessToMap()
		{
			if (ServerServiceClient.GetInstance().CheckAccessClient(UserResourceClientData.Application,
				UserActionClientData.Basic, UserApplicationClientData.Map, false) == true)
				barButtonItemOpenMap.Enabled = true;
			else
				barButtonItemOpenMap.Enabled = false;
		}

		private void alertControl1_AlertClick(object sender, DevExpress.XtraBars.Alerter.AlertClickEventArgs e)
		{
			if (chatForm == null || chatForm.IsDisposed == true)
			{
				chatForm = new OperatorChatForm(UserApplicationClientData.Cctv.Name, ChatOperatorType.Operator);
			}
			chatForm.WindowState = FormWindowState.Normal;
			chatForm.Activate();
			chatForm.Show();
			chatForm.BringToFront();
		}        

		private void barButtonItemChat_ItemClick(object sender, ItemClickEventArgs e)
		{
			if (chatForm == null || chatForm.IsDisposed == true)
				chatForm = new OperatorChatForm(UserApplicationClientData.Cctv.Name,ChatOperatorType.Operator);
			
			chatForm.Activate();
			chatForm.Show();
		}

		private void barButtonItemExit_ItemClick(object sender, ItemClickEventArgs e)
		{
			this.Close();
		}

		public void barButtonItemHelp_ItemClick(object sender, ItemClickEventArgs e)
		{
			try
            {
                ApplicationUtil.LaunchHelp(SmartCadConfiguration.DistFolder + "/Help/CCTV Module.chm");
            }
            catch (Exception ex)
            {
                MessageForm.Show(ResourceLoader.GetString2("$Message.HelpNotInstalled"), ex);
            }
        }

		private void barButtonItemOpenMap_ItemClick(object sender, ItemClickEventArgs e)
		{
			try
            {
                ApplicationUtil.LaunchApplicationWithArguments(
                        SmartCadConfiguration.SmartCadMapFilename,
						new object[] { ServerServiceClient.GetInstance().OperatorClient.Login,
                            defaultCctvFrontClientFormDevX.password, 
                            ApplicationUtil.CheckPrimaryScreenBounds(Left),
                            new SendActivateLayerEventArgs(ShapeType.Incident),
                            new SendActivateLayerEventArgs(ShapeType.Struct)
                        });
            }
            catch (Exception ex)
            {
                MessageForm.Show(ex.Message, ex);
            }
		}

        private object selectedTrigger;

        public void FisnishAlarm()
        {
            if (selectedTrigger is GridControlDataAlarm) cameraAlarmsForm.FisnishAlarm(cameraAlarmsForm.SelectedAlarm);
        }

        public void ReleaseAlarm()
        {
            if(selectedTrigger is GridControlDataAlarm) cameraAlarmsForm.ReleaseAlarm(cameraAlarmsForm.SelectedAlarm);
        }


        private void barButtonItemStartRegIncident_ItemClick(object sender, ItemClickEventArgs e)
        {
            //if (xtraTabbedMdiManager1.SelectedPage.Text == cameraAlarmsForm.Text)
            //{
            //    if (cameraAlarmsForm.SelectedAlarm != null)
            //    {
            //        if (cameraAlarmsForm.SelectedAlarm.Camera.StructClientData == null)
            //        {
            //            MessageForm.Show(ResourceLoader.GetString2("CannotStartIncidentWithoutStruct"), MessageFormType.Error);
            //            return;
            //        }

            //        selectedTrigger = cameraAlarmsForm.SelectedAlarm;
            //        cameraAlarmsForm.TakeAlarm(cameraAlarmsForm.SelectedAlarm);
            //        defaultCctvFrontClientFormDevX.barButtonItemStartRegIncident_ItemClick(cameraAlarmsForm.SelectedAlarm, null);
            //    }
            //}
            //else
            //{
            if (xtraTabbedMdiManager1.SelectedPage.Text == cameraVisualizeForm.Text)
            {
                if (cameraVisualizeForm.SelectedCamera != null)
                {
                    if (cameraVisualizeForm.SelectedCamera.Camera.StructClientData == null)
                    {
                        MessageForm.Show(ResourceLoader.GetString2("CannotStartIncidentWithoutStruct"), MessageFormType.Error);
                        return;
                    }
                
                    selectedTrigger = cameraVisualizeForm.SelectedCamera.Camera;
                    defaultCctvFrontClientFormDevX.barButtonItemStartRegIncident_ItemClick(
                        cameraVisualizeForm.SelectedCamera.Camera, null);
                }
            }else
            {
                if (((CctvAlarmsForm)this.ActiveMdiChild).gridControlAlarmIn.SelectedItems.Count > 0)
                {
                    GridCctvAlertCamData selectedAlarm = (GridCctvAlertCamData)((CctvAlarmsForm)this.ActiveMdiChild).gridControlAlarmIn.SelectedItems[0];
                    //VAAlertCamClientData report = ((VAAlertCamClientData)selectedAlarm.Tag);
                    //CameraClientDataAlarm camera = new CameraClientDataAlarm();
                    //camera.Ip = report.Camera.Ip;
                    //camera.IpServer = report.Camera.IpServer;
                    //camera.Login = report.Camera.Login;
                    //camera.Name = report.Camera.Name;
                    //camera.Port = report.Camera.Port;
                    //camera.StructClientData = report.Camera.StructClientData;
                    //camera.Type = report.Camera.Type;
                    //camera.Version = report.Camera.Version;
                    //camera.Code = report.Camera.Code;
                    //camera.ConnectionType = report.Camera.ConnectionType;
                    //camera.CustomId = report.Camera.CustomId;
                    //camera.AlarmDate = report.AlertDate;
                    //camera.AlarmRule = report.Rule.Name;
                    defaultCctvFrontClientFormDevX.barButtonItemStartRegIncidentAlarm_ItemClick(
                            selectedAlarm, null);
                }
            }
            xtraTabbedMdiManager1.SelectedPage = xtraTabbedMdiManager1.Pages[defaultCctvFrontClientFormDevX];
        }

        private void barButtonItemHistory_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (historyForm == null || historyForm.IsDisposed == true)
            {
                historyForm = new CctvIncidentsHistoryForm() { VmsControl = VmsControl };
            }
            historyForm.MdiParent = this;
            historyForm.Activate();
            historyForm.Show();
        }

        private void barButtonItemAlarm_ItemClick(object sender, ItemClickEventArgs e)
        {
            if(viewAlarm)
            { 
                if (alarmForm == null || alarmForm.IsDisposed == true)
                {
                    alarmForm = new CctvAlarmsForm() { VmsControl = VmsControl };
                }
                alarmForm.MdiParent = this;
                alarmForm.Activate();
                alarmForm.Show();
            }
        }

        private void labelUser_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void verifyExportersVideos()
        {
            if (vmsControlExit.VerifyConnectionExport() && vmsControlExit.TotalExported() != 0)
            {
                MessageForm.Show(ResourceLoader.GetString2("VideoExported") + vmsControlExit.TotalExported(), MessageFormType.Information);
            }
        }


        public void CheckUnpin()
        {
            CameraPanelControl auxCamera;
            CctvTemplateNode auxTemplate;

            if (cameraVisualizeForm.SelectedCamera != null)
            {
                auxCamera = cameraVisualizeForm.SelectedCamera;
                foreach (Form form in cameraVisualizeForm.SelectedCamera.unpinList())
                {
                    form.Close();
                }

                if (cameraVisualizeForm.SelectedCamera == null) cameraVisualizeForm.SelectedCamera = auxCamera;
                cameraPanelControlAccess.ClearunpinList();
            }
            if (cameraVisualizeForm.SelectedTemplate != null)
            {
                auxTemplate = cameraVisualizeForm.SelectedTemplate;
                List <Form> tempCameraListUnpin = cameraPanelControlAccess.unpinList();

                foreach (Form form in cameraVisualizeForm.SelectedTemplate.PanelControl.unpinList())
                {
                    form.Close();
                }

                if (cameraVisualizeForm.SelectedTemplate == null) cameraVisualizeForm.SelectedTemplate = auxTemplate;
                templatePanelControlAccess.ClearunpinList();


                if (tempCameraListUnpin.Count > 0)
                {
                   foreach (Form form in tempCameraListUnpin)
                   {
                     form.Close();
                   }

                   cameraPanelControlAccess.ClearunpinList();
                }

            }

            //if (tempListForms.Count > 0) 
            //{
            //    foreach (Form form in tempListForms)
            //    {
            //        form.Close();
            //    }
            //    cameraPanelControlAccess.ClearunpinList();
            //}

        }

        public void CheckUnpinRegister()
        {
            if (cameraVisualizeForm.SelectedTemplate != null)
            {
                Form auxCamera = new Form();
                int countValidation = 0;
                if (cameraVisualizeForm.SelectedCamera != null) {
                    int totalCameras= cameraVisualizeForm.SelectedCamera.unpinList().Count;
                        if (cameraVisualizeForm.SelectedCamera.unpinList().Count != 0)
                        {
                            foreach (Form form in cameraVisualizeForm.SelectedCamera.unpinList())
                            {
                               
                               if(form.AccessibleDescription != "true")
                                {
                                    countValidation++;
                                    form.Close();
                                }else
                                {
                                    auxCamera = form;
                                }
                                
                            }
        
                            cameraPanelControlAccess.ClearunpinList();
                            cameraVisualizeForm.SelectedCamera.AddunpinList(auxCamera);
                            
                            if (cameraVisualizeForm.SelectedTemplate != null)
                            {
                                if (totalCameras == countValidation)
                                {
                                    methodForTemplatesUnpin();
        
                                }
                                else { 
        
                                    foreach (Form form in cameraVisualizeForm.SelectedTemplate.PanelControl.unpinList())
                                    {
                                        form.Close();
                                    }
                                    templatePanelControlAccess.ClearunpinList();
                                }
                                
                            }
        
        
                        }
                        else { 
        
                            if (cameraVisualizeForm.SelectedTemplate.PanelControl.unpinList().Count > 0)
                            {
        
                                methodForTemplatesUnpin();
        
                            }
                        }

                }
                

                //foreach (Form form in cameraVisualizeForm.SelectedTemplate.PanelControl.unpinList().Count)
                //{
                //    foreach (Control control in )
                //        {
                //            control
                //            if (form.AccessibleName != selectedTrigger.ToString()) form.Close();
                //        }


                //    if (form.AccessibleName != selectedTrigger.ToString()) form.Close();
                //}
                templatePanelControlAccess.ClearlistCamerasinTemplatesUnpin();
               // templatePanelControlAccess.ClearunpinList();
            }

            //foreach (Form form in cameraVisualizeForm.OwnedForms)
            //{
            //    if (form.AccessibleName != selectedTrigger.ToString()) form.Close();
            //}

        }


        public void methodForTemplatesUnpin()
        {
            TemplatePanelControl template = cameraVisualizeForm.SelectedTemplate.PanelControl.listCamerasinTemplatesUnpin();
            List<int> lista = new List<int> { };
            int count = 0;
            int count2 = 0;
            bool firstTime = true;

            foreach (CameraPanelControl cameraInTemplate in template.CameraPanels)
            {
                if (selectedTrigger.ToString() != cameraInTemplate.Camera.Name)
                {
                    lista.Add(count);
                }
                count += 1;
            }



            foreach (int numero in lista)
            {
                if (firstTime)
                {
                    template.CameraPanels[numero].Dispose();
                    firstTime = false;
                }
                else
                {
                    template.CameraPanels[numero - count2].Dispose();
                }
                count2++;
            }
        }

       
        public void viewAlarmAgentVI()
        {
                viewAlarm = true;
        }
	}
}