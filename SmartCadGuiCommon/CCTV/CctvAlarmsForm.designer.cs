
using DevExpress.XtraReports.UI;
using SmartCadControls;
using SmartCadControls.Controls;
using SmartCadCore.Common;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
namespace SmartCadGuiCommon
{
    partial class CctvAlarmsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PlayBackForm));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.groupControlVideo = new DevExpress.XtraEditors.GroupControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabAlarms = new System.Windows.Forms.TabControl(); // Tabla de Alarmas
            this.tabDataloggerAlarms = new System.Windows.Forms.TabPage();
            this.gridControlAlarmIn = new SmartCadControls.GridControlEx();
            this.gridViewDataloggerAlarms = new SmartCadControls.GridViewEx();
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barButtonItemPrint = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemSave = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemCancelVA_Alarm = new DevExpress.XtraBars.BarButtonItem();
            this.checkButtonPlay = new DevExpress.XtraEditors.CheckButton();
            this.checkButtonBack = new DevExpress.XtraEditors.CheckButton();
            this.simpleButtonStop = new DevExpress.XtraEditors.SimpleButton();
            this.checkButtonMute = new DevExpress.XtraEditors.CheckButton();
            this.trackBarControlVolume = new DevExpress.XtraEditors.TrackBarControl();
            this.zoomTrackBarControlSpeed = new DevExpress.XtraEditors.ZoomTrackBarControl();
            this.toolTipController1 = new DevExpress.Utils.ToolTipController(this.components);
            this.groupControlAudioControl = new DevExpress.XtraEditors.GroupControl();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.DateEditEndDate = new DevExpress.XtraEditors.DateEdit();
            this.DateEditStartDate = new DevExpress.XtraEditors.DateEdit();
            this.labelVideoTime = new System.Windows.Forms.Label();
            this.labelControlPlaybackSpeed = new DevExpress.XtraEditors.LabelControl();
            this.ribbonPageGroupGeneralOptions = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupAlarm = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.controlContainerVideo = new DevExpress.XtraBars.Docking.ControlContainer();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
           

            this.tabAlarms.SuspendLayout();
            this.tabDataloggerAlarms.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlAlarmIn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDataloggerAlarms)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlAudioControl)).BeginInit();
            this.groupControlAudioControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarControlVolume)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarControlVolume.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.zoomTrackBarControlSpeed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.zoomTrackBarControlSpeed.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlVideo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditStartDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditStartDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.controlContainerVideo.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabAlarms
            // Tabla de Alarmas 
            this.tabAlarms.Controls.Add(this.tabDataloggerAlarms);
            this.tabAlarms.Location = new System.Drawing.Point(4, 4);
            //this.tabAlarms.MinimumSize = new System.Drawing.Size(1350, 300); //Para mi
            this.tabAlarms.MinimumSize = new System.Drawing.Size(1600, 300); //Para ellos
            //this.tabAlarms.MaximumSize = new System.Drawing.Size(1600, 300); //Para ellos
            this.tabAlarms.Name = "tabAlarms";
            this.tabAlarms.SelectedIndex = 0;
            //this.tabAlarms.Size = new System.Drawing.Size(1650, 323);
            this.tabAlarms.TabIndex = 2;
            // 
            //tabDataloggerAlarms
            // 
            this.tabDataloggerAlarms.Controls.Add(this.gridControlAlarmIn);
            this.tabDataloggerAlarms.Location = new System.Drawing.Point(4, 22);
            this.tabDataloggerAlarms.Name = "tabDataloggerAlarms";
            this.tabDataloggerAlarms.Padding = new System.Windows.Forms.Padding(2);
            this.tabDataloggerAlarms.Size = new System.Drawing.Size(554, 297);
            this.tabDataloggerAlarms.TabIndex = 1;
            this.tabDataloggerAlarms.Text = ResourceLoader.GetString2("DataloggerAlarms");
            this.tabDataloggerAlarms.UseVisualStyleBackColor = true;
            // 
            // gridControlAlarmIn
            // 
            this.gridControlAlarmIn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlAlarmIn.EnableAutoFilter = false;
            this.gridControlAlarmIn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlAlarmIn.Location = new System.Drawing.Point(2, 2);
            this.gridControlAlarmIn.MainView = this.gridViewDataloggerAlarms;
            this.gridControlAlarmIn.Margin = new System.Windows.Forms.Padding(2);
            this.gridControlAlarmIn.Name = "gridControlAlarmIn";
            this.gridControlAlarmIn.Size = new System.Drawing.Size(550, 293);
            this.gridControlAlarmIn.TabIndex = 8;
            this.gridControlAlarmIn.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewDataloggerAlarms});
            this.gridControlAlarmIn.ViewTotalRows = false;
            //ScrollBar vScrollBar1 = new VScrollBar();
            //vScrollBar1.Dock = DockStyle.Left;
            //this.gridControlAlarmIn.Controls.Add(vScrollBar1);
            // 
            this.gridViewDataloggerAlarms.AllowFocusedRowChanged = true;
            this.gridViewDataloggerAlarms.Appearance.FocusedCell.BackColor = System.Drawing.Color.Transparent;
            this.gridViewDataloggerAlarms.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewDataloggerAlarms.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewDataloggerAlarms.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewDataloggerAlarms.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewDataloggerAlarms.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewDataloggerAlarms.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewDataloggerAlarms.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewDataloggerAlarms.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewDataloggerAlarms.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gridViewDataloggerAlarms.Appearance.Row.Options.UseBackColor = true;
            this.gridViewDataloggerAlarms.EnablePreviewLineForFocusedRow = false;
            this.gridViewDataloggerAlarms.GridControl = this.gridControlAlarmIn;
            this.gridViewDataloggerAlarms.GroupFormat = "[#image]{1} {2}";
            this.gridViewDataloggerAlarms.Name = "gridViewDataloggerAlarms";
            this.gridViewDataloggerAlarms.OptionsBehavior.Editable = false;
            this.gridViewDataloggerAlarms.OptionsMenu.EnableColumnMenu = false;
            this.gridViewDataloggerAlarms.OptionsMenu.EnableFooterMenu = false;
            this.gridViewDataloggerAlarms.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridViewDataloggerAlarms.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewDataloggerAlarms.OptionsView.ShowAutoFilterRow = true;
            this.gridViewDataloggerAlarms.OptionsView.ShowDetailButtons = false;
            this.gridViewDataloggerAlarms.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewDataloggerAlarms.OptionsView.ShowGroupPanel = false;
            this.gridViewDataloggerAlarms.OptionsView.ShowIndicator = false;
            this.gridViewDataloggerAlarms.ViewTotalRows = false;
            //this.gridViewDataloggerAlarms.SelectionWillChange += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewExPreviousAlarms_SelectionWillChange);
            this.gridViewDataloggerAlarms.RowCellClick += this.gridViewExPreviousAlarms_SelectionWillChange2;
           
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.splitContainerControl1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(434, 119);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(740, 623);
            this.layoutControl1.TabIndex = 9;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // splitContainerControl1
            // Este contiene el Datalogger Alarm junto con la tabla de Alarmas
            this.splitContainerControl1.Horizontal = false; // El desplazamiento del grid es vertical, por eso es FALSE.
            this.splitContainerControl1.Location = new System.Drawing.Point(4, 4);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.tabAlarms);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.controlContainerVideo);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(732, 615);
            this.splitContainerControl1.SplitterPosition = 334;
            this.splitContainerControl1.TabIndex = 8;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // splitContainerControl2
            // Este contiene el Video de la Alarma y los controles del Video
            this.splitContainerControl2.Horizontal = true; // El desplazamiento del Grid es Horizontal por eso es TRUE.
            this.splitContainerControl2.Location = new System.Drawing.Point(4, 4);
            this.splitContainerControl2.Name = "splitContainerControl1";
            this.splitContainerControl2.Panel1.Controls.Add(this.groupControlAudioControl);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.groupControlVideo);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(1582, 313);
            this.splitContainerControl2.SplitterPosition = 154;
            this.splitContainerControl2.TabIndex = 8;
            this.splitContainerControl2.Text = "splitContainerControl1";
            // 
            // groupControlVideo
            //
            this.groupControlVideo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControlVideo.Location = new System.Drawing.Point(155, 0);
            this.groupControlVideo.Name = "groupControlVideo";
            this.groupControlVideo.Size = new System.Drawing.Size(1450, 317);
            this.groupControlVideo.TabIndex = 7;
            this.groupControlVideo.Text = "Video";
            // 
            // controlContainerVideo
            // Este es el panel que reemplazo el splitContainerControl2 que es el que contiene las secciones de los controles del video y el video
            this.controlContainerVideo.Controls.Add(this.groupControlVideo);
            this.controlContainerVideo.Controls.Add(this.groupControlAudioControl);
            this.controlContainerVideo.Dock = DockStyle.Fill;
            this.controlContainerVideo.Location = new System.Drawing.Point(3, 29);
            this.controlContainerVideo.Name = "dockPanel2_Container";
            this.controlContainerVideo.Size = new System.Drawing.Size(195, 591);
            this.controlContainerVideo.TabIndex = 0;




            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(740, 623);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            
          
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem1.Control = this.splitContainerControl1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(736, 619);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
           
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ApplicationButtonAnimationLength = 1;
            this.ribbonControl1.ExpandCollapseItem.AllowRightClickInMenu = false;
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.ExpandCollapseItem.Name = "";
            this.ribbonControl1.GroupAnimationLength = 1;
            this.ribbonControl1.ItemAnimationLength = 1;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.barButtonItemPrint,
            this.barButtonItemSave,
            this.barButtonItemRefresh,
            this.barButtonItemCancelVA_Alarm});///boton de disscard
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 45;
            this.ribbonControl1.MdiMergeStyle = DevExpress.XtraBars.Ribbon.RibbonMdiMergeStyle.Always;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.PageAnimationLength = 1;
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbonControl1.ShowExpandCollapseButton = DevExpress.Utils.DefaultBoolean.True;
            this.ribbonControl1.ShowToolbarCustomizeItem = false;
            this.ribbonControl1.Size = new System.Drawing.Size(1174, 119);
            this.ribbonControl1.Toolbar.ShowCustomizeItem = false;
            this.ribbonControl1.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            //this.ribbonControl1.Controls.Add(vScrollBar1);
            // 
            // barButtonItemPrint
            // 
            this.barButtonItemPrint.Caption = "ItemPrint";
            this.barButtonItemPrint.Enabled = false;
            this.barButtonItemPrint.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPrint.Glyph")));
            this.barButtonItemPrint.Id = 6;
            this.barButtonItemPrint.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemPrint.Name = "barButtonItemPrint";
            //this.barButtonItemPrint.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemPrint_ItemClick);
            // 
            // barButtonItemSave
            // 
            this.barButtonItemSave.Caption = "ItemSave";
            this.barButtonItemSave.Enabled = false;
            this.barButtonItemSave.Glyph = global::SmartCadGuiCommon.PrintRibbonControllerResources.RibbonPrintPreview_Save;
            this.barButtonItemSave.Id = 7;
            this.barButtonItemSave.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemSave.Name = "barButtonItemSave";
            //this.barButtonItemSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemSave_ItemClick);
            // 
            // barButtonItemRefresh
            // 
            this.barButtonItemRefresh.Caption = "ItemRefresh";
            this.barButtonItemRefresh.Enabled = false;
            this.barButtonItemRefresh.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemRefresh.Glyph")));
            this.barButtonItemRefresh.Id = 8;
            this.barButtonItemRefresh.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemRefresh.Name = "barButtonItemRefresh";
            this.barButtonItemRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemRefresh_ItemClick);
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupGeneralOptions,
            this.ribbonPageGroupAlarm});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "ribbonPage1";
            // 
            // ribbonPageGroupGeneralOptions
            // 
            this.ribbonPageGroupGeneralOptions.AllowMinimize = false;
            this.ribbonPageGroupGeneralOptions.AllowTextClipping = false;
            this.ribbonPageGroupGeneralOptions.ItemLinks.Add(this.barButtonItemPrint);
            this.ribbonPageGroupGeneralOptions.ItemLinks.Add(this.barButtonItemSave);
            this.ribbonPageGroupGeneralOptions.ItemLinks.Add(this.barButtonItemRefresh);
            this.ribbonPageGroupGeneralOptions.Name = "ribbonPageGroupGeneralOptions";
            this.ribbonPageGroupGeneralOptions.ShowCaptionButton = false;
            this.ribbonPageGroupGeneralOptions.Text = "ribbonPageGroupGeneralOptions"; 
            //
            // ribbonPageGroupAlarm
            //
            this.ribbonPageGroupAlarm.AllowMinimize = false;
            this.ribbonPageGroupAlarm.AllowTextClipping = false;
            this.ribbonPageGroupAlarm.ItemLinks.Add(this.barButtonItemCancelVA_Alarm);
            this.ribbonPageGroupAlarm.Name = "ribbonPageGroupAlarm";
            this.ribbonPageGroupAlarm.ShowCaptionButton = false;
            this.ribbonPageGroupAlarm.Text = "ribbonPageGroupAlarm";

            // 
            // checkButtonPlay
            // 
            this.checkButtonPlay.Enabled = false;
            this.checkButtonPlay.Image = global::SmartCadGuiCommon.Properties.Resources.Play;
            this.checkButtonPlay.Location = new System.Drawing.Point(87, 62);
            this.checkButtonPlay.Name = "checkButtonPlay";
            this.checkButtonPlay.Size = new System.Drawing.Size(25, 25);
            this.checkButtonPlay.TabIndex = 54;
            this.checkButtonPlay.Click += new System.EventHandler(this.simpleButtonPlay_Click);
            // 
            // groupControlAudioControl
            // Se le a�adio el dock pero a left para que estuviera siempre a la izquierda y se modificara el tama�o dependiendo del cambio del tama�o que se haga
            this.groupControlAudioControl.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupControlAudioControl.Controls.Add(this.checkButtonPlay);
            this.groupControlAudioControl.Controls.Add(this.checkButtonBack);
            this.groupControlAudioControl.Controls.Add(this.simpleButtonStop);
            this.groupControlAudioControl.Controls.Add(this.checkButtonMute);
            this.groupControlAudioControl.Controls.Add(this.labelControlPlaybackSpeed);
            //this.groupControlAudioControl.Controls.Add(this.labelVideoTime);
            this.groupControlAudioControl.Controls.Add(this.trackBarControlVolume);
            this.groupControlAudioControl.Controls.Add(this.zoomTrackBarControlSpeed);
            this.groupControlAudioControl.Location = new System.Drawing.Point(0, 0);
            this.groupControlAudioControl.Name = "groupControlAudioControl";
            this.groupControlAudioControl.Size = new System.Drawing.Size(154, 334);
            this.groupControlAudioControl.TabIndex = 11;
            // 
            // panelControl1
            // 
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(255, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(945, 671);
            this.panelControl1.TabIndex = 10;
            //this.panelControl1.Controls.Add(vScrollBar1);
            // 
            // DateEditEndDate
            // 
            this.DateEditEndDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DateEditEndDate.EditValue = null;
            this.DateEditEndDate.Location = new System.Drawing.Point(87, 58);
            this.DateEditEndDate.Name = "DateEditEndDate";
            this.DateEditEndDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.DateEditEndDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, false, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject6, "", null, null, false)});
            this.DateEditEndDate.Properties.DisplayFormat.FormatString = "MM/dd/yyyy HH:mm:ss.fff";
            this.DateEditEndDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.DateEditEndDate.Properties.EditFormat.FormatString = "MM/dd/yyyy HH:mm:ss.fff";
            this.DateEditEndDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.DateEditEndDate.Properties.Mask.EditMask = "MM/dd/yyyy HH:mm:ss.fff";
            this.DateEditEndDate.Properties.ShowDropDown = DevExpress.XtraEditors.Controls.ShowDropDown.Never;
            this.DateEditEndDate.Properties.ShowPopupShadow = false;
            this.DateEditEndDate.Properties.ShowToday = false;
            this.DateEditEndDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DateEditEndDate.Size = new System.Drawing.Size(146, 20);
            this.DateEditEndDate.TabIndex = 45;
            // 
            // DateEditStartDate
            // 
            this.DateEditStartDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DateEditStartDate.EditValue = null;
            this.DateEditStartDate.Location = new System.Drawing.Point(87, 32);
            this.DateEditStartDate.Name = "DateEditStartDate";
            this.DateEditStartDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.DateEditStartDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, false, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject7, "", null, null, false)});
            this.DateEditStartDate.Properties.DisplayFormat.FormatString = "MM/dd/yyyy HH:mm:ss.fff";
            this.DateEditStartDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.DateEditStartDate.Properties.EditFormat.FormatString = "MM/dd/yyyy HH:mm:ss.fff";
            this.DateEditStartDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.DateEditStartDate.Properties.Mask.EditMask = "MM/dd/yyyy HH:mm:ss.fff";
            this.DateEditStartDate.Properties.ShowDropDown = DevExpress.XtraEditors.Controls.ShowDropDown.Never;
            this.DateEditStartDate.Properties.ShowPopupShadow = false;
            this.DateEditStartDate.Properties.ShowToday = false;
            this.DateEditStartDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DateEditStartDate.Size = new System.Drawing.Size(146, 20);
            this.DateEditStartDate.TabIndex = 44;
            // 
            // labelVideoTime
            // 
            this.labelVideoTime.AutoSize = true;
            this.labelVideoTime.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelVideoTime.Location = new System.Drawing.Point(33, 31);
            this.labelVideoTime.Name = "labelVideoTime";
            this.labelVideoTime.Size = new System.Drawing.Size(154, 18);
            this.labelVideoTime.TabIndex = 56;
            this.labelVideoTime.Text = "05/02/2012 15:25:16";
            // 
            // checkButtonBack
            // 
            this.checkButtonBack.Enabled = false;
            this.checkButtonBack.Image = ((System.Drawing.Image)(resources.GetObject("checkButtonBack.Image")));
            this.checkButtonBack.Location = new System.Drawing.Point(25, 62);
            this.checkButtonBack.Name = "checkButtonBack";
            this.checkButtonBack.Size = new System.Drawing.Size(25, 25);
            this.checkButtonBack.TabIndex = 22;
            this.checkButtonBack.Click += new System.EventHandler(this.simpleButtonBack_Click);
            // 
            // simpleButtonStop
            // 
            this.simpleButtonStop.Enabled = false;
            this.simpleButtonStop.Image = ((System.Drawing.Image)(resources.GetObject("simpleButtonStop.Image")));
            this.simpleButtonStop.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButtonStop.Location = new System.Drawing.Point(56, 62);
            this.simpleButtonStop.Name = "simpleButtonStop";
            this.simpleButtonStop.Size = new System.Drawing.Size(25, 25);
            this.simpleButtonStop.TabIndex = 47;
            this.simpleButtonStop.ToolTipController = this.toolTipController1;
            this.simpleButtonStop.Click += new System.EventHandler(this.simpleButtonStop_Click);
            // 
            // checkButtonMute
            // 
            this.checkButtonMute.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.checkButtonMute.Enabled = false;
            this.checkButtonMute.Image = global::SmartCadGuiCommon.Properties.Resources.soundSmall;
            this.checkButtonMute.Location = new System.Drawing.Point(120, 99);
            this.checkButtonMute.Name = "checkButtonMute";
            this.checkButtonMute.Size = new System.Drawing.Size(25, 25);
            this.checkButtonMute.TabIndex = 60;
            this.checkButtonMute.CheckedChanged += new System.EventHandler(this.checkButtonMute_CheckedChanged);
            // 
            // labelControlPlaybackSpeed
            // 
            this.labelControlPlaybackSpeed.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControlPlaybackSpeed.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelControlPlaybackSpeed.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControlPlaybackSpeed.Location = new System.Drawing.Point(25, 89);
            this.labelControlPlaybackSpeed.Name = "labelControlPlaybackSpeed";
            this.labelControlPlaybackSpeed.Size = new System.Drawing.Size(107, 13);
            this.labelControlPlaybackSpeed.TabIndex = 59;
            this.labelControlPlaybackSpeed.Text = "Speed: 1X";

            //prueba
            this.splitContainerControl2.BackColor = Color.BlueViolet;
            Graphics g = this.labelControlPlaybackSpeed.CreateGraphics();
            g.DrawLine(Pens.Cyan, new Point(10, 10), new Point(150, 150));

            //finprueba
            // 
            // trackBarControlVolume
            // 
            this.trackBarControlVolume.EditValue = 10;
            this.trackBarControlVolume.Enabled = false;
            this.trackBarControlVolume.Location = new System.Drawing.Point(120, 18);
            this.trackBarControlVolume.Name = "trackBarControlVolume";
            this.trackBarControlVolume.Properties.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trackBarControlVolume.Properties.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackBarControlVolume.Size = new System.Drawing.Size(45, 84);
            this.trackBarControlVolume.TabIndex = 14;
            this.trackBarControlVolume.Value = 10;
            this.trackBarControlVolume.EditValueChanged += new System.EventHandler(this.trackBarControlPlayback_EditValueChanged);
            // 
            // zoomTrackBarControlSpeed
            // 
            this.zoomTrackBarControlSpeed.EditValue = 4;
            this.zoomTrackBarControlSpeed.Enabled = false;
            this.zoomTrackBarControlSpeed.Location = new System.Drawing.Point(17, 103);
            this.zoomTrackBarControlSpeed.Name = "zoomTrackBarControlSpeed";
            this.zoomTrackBarControlSpeed.Properties.LargeChange = 1;
            this.zoomTrackBarControlSpeed.Properties.Maximum = 8;
            this.zoomTrackBarControlSpeed.Properties.ScrollThumbStyle = DevExpress.XtraEditors.Repository.ScrollThumbStyle.ArrowDownRight;
            this.zoomTrackBarControlSpeed.Size = new System.Drawing.Size(100, 20);
            this.zoomTrackBarControlSpeed.TabIndex = 58;
            this.zoomTrackBarControlSpeed.Value = 4;
            this.zoomTrackBarControlSpeed.EditValueChanged += new System.EventHandler(this.zoomTrackBarControlSpeed_EditValueChanged);
            // 
            // barButtonItemCancelVA_Alarm
            // 
            this.barButtonItemCancelVA_Alarm.Caption = "barButtonItemCancelAlarm";
            this.barButtonItemCancelVA_Alarm.Enabled = false;
            this.barButtonItemCancelVA_Alarm.Refresh();///URDANETA
            this.barButtonItemCancelVA_Alarm.Id = 41;
            this.barButtonItemCancelVA_Alarm.LargeGlyph = global::SmartCadGuiCommon.Properties.Resources.CancelAlarm1;
            this.barButtonItemCancelVA_Alarm.Name = "barButtonItemCancelVA_Alarm";
            this.barButtonItemCancelVA_Alarm.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemCancelVA_Alarm_ItemClick);
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // CctvAlarmsHistoryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1174, 742);
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.ribbonControl1);
            this.Controls.Add(this.panelControl1);
            this.Name = "CctvAlarmsForm";
            this.Text = "AlarmsForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CctvAlarmsForm_FormClosing);
            this.Load += new System.EventHandler(this.CctvAlarmsForm_Load);
            this.ControlBox = false;
            this.tabAlarms.ResumeLayout(false);
            this.tabDataloggerAlarms.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlAlarmIn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDataloggerAlarms)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditEndDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditEndDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditStartDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEditStartDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControlAudioControl)).EndInit();
            this.groupControlAudioControl.ResumeLayout(false);
            this.groupControlAudioControl.PerformLayout();
            this.controlContainerVideo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.trackBarControlVolume.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarControlVolume)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.zoomTrackBarControlSpeed.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.zoomTrackBarControlSpeed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlVideo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        public DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.XtraBars.BarButtonItem barButtonItemPrint;
        private DevExpress.XtraBars.BarButtonItem barButtonItemSave;
        private DevExpress.XtraBars.BarButtonItem barButtonItemRefresh;
        public DevExpress.XtraBars.BarButtonItem barButtonItemCancelVA_Alarm;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupGeneralOptions;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        public DevExpress.XtraEditors.DateEdit DateEditEndDate;
        public DevExpress.XtraEditors.DateEdit DateEditStartDate;
        public DevExpress.XtraEditors.GroupControl groupControlVideo;
        private System.Windows.Forms.Label labelVideoTime;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        public DevExpress.XtraEditors.CheckButton checkButtonPlay;
        public DevExpress.XtraEditors.CheckButton checkButtonBack;
        private DevExpress.XtraEditors.SimpleButton simpleButtonStop;
        public DevExpress.XtraEditors.CheckButton checkButtonMute;
        public DevExpress.XtraEditors.TrackBarControl trackBarControlVolume;
        private DevExpress.XtraEditors.LabelControl labelControlPlaybackSpeed;
        public DevExpress.XtraEditors.ZoomTrackBarControl zoomTrackBarControlSpeed;
        private DevExpress.Utils.ToolTipController toolTipController1;
        public System.Windows.Forms.TabControl tabAlarms;
        private System.Windows.Forms.TabPage tabDataloggerAlarms;
        public GridControlEx gridControlAlarmIn;
        public GridViewEx gridViewDataloggerAlarms;
        public DevExpress.XtraEditors.GroupControl groupControlAudioControl;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupAlarm;
        // Este es lo que se reemplazo por el split
        private DevExpress.XtraBars.Docking.ControlContainer controlContainerVideo;
       
    }
}