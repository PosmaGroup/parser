using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Smartmatic.SmartCad.Map;
using System.Collections;
using Smartmatic.SmartCad.Service;
using System.Reflection;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using System.Threading;
using System.IO;
using System.Xml.Xsl;
using System.Xml;
using Smartmatic.SmartCad.Vms;
using DevExpress.XtraLayout;
using SmartCadControls.SyncBoxes;
using SmartCadCore.ClientData;
using SmartCadCore.Core;
using SmartCadControls.Controls;
using SmartCadCore.Common;
using SmartCadControls;
using SmartCadFirstLevel.Gui.SyncBoxes;
using SmartCadGuiCommon.Controls;
using SmartCadGuiCommon.Util;

namespace SmartCadGuiCommon
{
    public partial class CctvIncidentsHistoryForm : DevExpress.XtraEditors.XtraForm
    {

        private XslCompiledTransform xslCompiledTransform;
        private int selectedIncidentCode;
        private GridControlSynBox incidentSyncBox;
        public ApplicationPreferenceClientData CCTVIncidentCodePrefix = null;
        public ApplicationPreferenceClientData CCTVIncidentVACodePrefix = null;
        public ApplicationPreferenceClientData CctvVideoSecondsBefore = null;
        public ApplicationPreferenceClientData CctvVideoSecondsAfter = null;
        private Thread vmsThread = null;
        private DateTime videoStartDate;
        private DateTime videoEndDate;
        private ConfigurationClientData config = null;
        public CameraPanelControl CameraPanel;
        public VmsSingletonUtil VmsControl;

        private string p;

        public CctvIncidentsHistoryForm()
        {
            InitializeComponent();
            CCTVIncidentCodePrefix = (ApplicationPreferenceClientData)ServerServiceClient.GetInstance().SearchClientObject(
               SmartCadHqls.GetCustomHql(SmartCadHqls.GetApplicationPreferenceByName, "CCTVIncidentCodePrefix"));
            CCTVIncidentVACodePrefix = (ApplicationPreferenceClientData)ServerServiceClient.GetInstance().SearchClientObject(
               SmartCadHqls.GetCustomHql(SmartCadHqls.GetApplicationPreferenceByName, "VACodePrefix"));
            CctvVideoSecondsBefore = (ApplicationPreferenceClientData)ServerServiceClient.GetInstance().SearchClientObject(
                SmartCadHqls.GetCustomHql(SmartCadHqls.GetApplicationPreferenceByName, "CctvVideoSecondsBefore"));
            CctvVideoSecondsAfter = (ApplicationPreferenceClientData)ServerServiceClient.GetInstance().SearchClientObject(
                SmartCadHqls.GetCustomHql(SmartCadHqls.GetApplicationPreferenceByName, "CctvVideoSecondsAfter"));

            config = ServerServiceClient.GetInstance().GetConfiguration();
            VmsControl = new VmsSingletonUtil();
        }

        private void CctvIncidentsHistoryForm_Load(object sender, EventArgs e)
        {
            searchFilterBar1.AddFilter(new IncidentStatusFilter());
            searchFilterBar1.AddFilter(new DatesFilter());
            searchFilterBar1.AddFilter(new IncidentTypeFilter(UserApplicationClientData.Cctv));
            searchFilterBar1.SearchRequest += searchFilterBar1_SearchRequest;

            XmlReaderSettings xmlReaderSettings = new XmlReaderSettings() { ProhibitDtd = false };
            xslCompiledTransform = new XslCompiledTransform();
            xslCompiledTransform.Load(XmlReader.Create(SmartCadConfiguration.CctvIncidentXslt, xmlReaderSettings));

            gridControlExPreviousIncidents.Type = typeof(GridCctvIncidentData);
            incidentSyncBox = new GridControlSynBox(gridControlExPreviousIncidents);
            
            gridControlExPreviousIncidents.ViewTotalRows = true;
            gridControlExPreviousIncidents.EnableAutoFilter = true;
            gridViewExPreviousIncidents.ViewTotalRows = true;
            gridControlExPreviousIncidents.AllowDrop = false;
            LoadLanguage();
        }

        void LoadLanguage()
        {
            Text = ResourceLoader.GetString2("IncidentsHistoryForm");
            dockPanel1.Text = ResourceLoader.GetString2("SearchingToolsMaps");
            dockPanelGrid.Text = ResourceLoader.GetString2("Incidents");
            barButtonItemPrint.Caption = ResourceLoader.GetString2("Print");
            barButtonItemRefresh.Caption = ResourceLoader.GetString2("Refresh");
            barButtonItemSave.Caption = ResourceLoader.GetString2("Save");
            ribbonPageGroupGeneralOptions.Text = ResourceLoader.GetString2("GeneralOptions");
            ribbonPage1.Text = ResourceLoader.GetString2("Cctv");
            groupControlDetails.Text = ResourceLoader.GetString2("Incident details");
            groupControlVideo.Text = ResourceLoader.GetString2("Incident Video");
            barButtonItemCancelTelemetryAlarm.Caption = ResourceLoader.GetString2("Dismiss");
            ribbonPageGroupAlarm.Text = ResourceLoader.GetString2("Alarms");
        }

        void searchFilterBar1_SearchRequest(Dictionary<string, Dictionary<string, List<object>>> filters)
        {
            BackgroundProcessForm processForm = new BackgroundProcessForm(
                ResourceLoader.GetString2("Searching"),
                this,
                new MethodInfo[1] {
                    GetType().GetMethod("BackgroundSearch", BindingFlags.NonPublic | BindingFlags.Instance) 
                },
                new object[1][] { new object[] { new object[] { filters } } }
            );
            processForm.CanThrowError = true;
            processForm.ShowDialog();
        }

        private void BackgroundSearch(object[] objs)
        {
            Dictionary<string, Dictionary<string, List<object>>> filters = (Dictionary<string, Dictionary<string, List<object>>>)objs[0];
            
            string HQL = @"SELECT inc FROM IncidentData inc 
                                      LEFT JOIN FETCH inc.SetReportBaseList child 
                                      LEFT JOIN FETCH child.SetIncidentTypes WHERE
                                        ( inc.CustomCode like '" + CCTVIncidentCodePrefix.Value + "%' OR" +
                                        //La siguiente linea se va a cambiar por el nuevo prefix cuando en la base de datos ya este creado el nuevo prefix que se va a utilizar
                                        " inc.CustomCode like '" + CCTVIncidentVACodePrefix.Value + "%') AND";
            //SET Incident Status filter
            if (filters["IncidentStatusFilter"].Count == 1)
            {
                if (filters["IncidentStatusFilter"].ContainsKey("Open"))
                {
                    HQL += " inc.Status.CustomCode = 'Open' AND";
                }
                else
                {
                    HQL += " inc.Status.CustomCode = 'Closed' AND";
                }
            }
            //SET Incident date interval filter
            string start = ApplicationUtil.GetDataBaseFormattedDate_1(((DateTime)filters["DatesFilter"]["StartDate"][0]));
            string end = ApplicationUtil.GetDataBaseFormattedDate_1(((DateTime)filters["DatesFilter"]["EndDate"][0]));

            HQL += @" inc.StartDate BETWEEN '" + start + " 00:00:00'" +
                    " AND '" + end + " 23:59:59'";
            
            //SET Incident Type filter
            if (filters["IncidentTypeFilter"]["IncidentType"].Count > 0)
            {
                HQL += @" AND inc.Code in 
                   ( SELECT reportBase.Incident.Code
                        FROM ReportBaseData reportBase 
                        LEFT JOIN reportBase.SetIncidentTypes types
                        WHERE types.Name IN (";
                foreach (object type in filters["IncidentTypeFilter"]["IncidentType"])
                {
                    HQL += String.Format("'{0}', ", type);
                }
                HQL = HQL.Remove(HQL.Length - 2);
                HQL += "))";
            }

            IList incidents = ServerServiceClient.GetInstance().SearchClientObjectsMaxRows(HQL, true);

            gridControlExPreviousIncidents.ClearData();
            incidentSyncBox.Sync(incidents);
        }


        private void gridControlExPreviousIncidents_CellToolTipNeeded(object sender, DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventArgs e)
        {
            if (e.Info == null)
            {
                string infoText = string.Empty;
                GridHitInfo info = gridViewExPreviousIncidents.CalcHitInfo(e.ControlMousePosition);
                if (info.RowHandle > -1 && info.RowHandle != GridControlEx.InvalidRowHandle)
                {
                    IncidentClientData incidentData = (IncidentClientData)((GridCctvIncidentData)gridViewExPreviousIncidents.GetRow(info.RowHandle)).Tag;

                    if (incidentData != null)
                        foreach (IncidentTypeClientData incidentType in incidentData.IncidentTypes)
                            if (incidentType.NoEmergency != true)
                                infoText += incidentType.FriendlyName + ", ";

                    if (infoText.Trim() != "")
                        infoText = infoText.Substring(0, infoText.Length - 2);

                    e.Info = new DevExpress.Utils.ToolTipControlInfo(new DevExpress.XtraGrid.Views.Base.CellToolTipInfo(info.RowHandle, info.Column, "cell"), infoText);
                }
            }
        }

        private void gridViewExPreviousIncidents_SelectionWillChange(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            barButtonItemPrint.Enabled = (e.FocusedRowHandle > -1);
            barButtonItemSave.Enabled = (e.FocusedRowHandle > -1);
            barButtonItemRefresh.Enabled = (e.FocusedRowHandle > -1);


            if (e.FocusedRowHandle > -1)//(gridControlExPreviousIncidents.SelectedItems.Count > 0)
            {
                FormUtil.InvokeRequired(this,
                delegate
                {
                    IncidentClientData selectedIncident = (IncidentClientData)((GridCctvIncidentData)gridControlExPreviousIncidents.SelectedItems[0]).Tag;

                    ArrayList parameters = new ArrayList();
                    parameters.Add(selectedIncident.Code);
                    parameters.Add(selectedIncident.IncidentTypes);

                    SelectedIncidentTask selectedIncidentTask = new SelectedIncidentTask(parameters);
                    ArrayList tasks = new ArrayList();
                    tasks.Add(selectedIncidentTask);

                    ThreadPool.QueueUserWorkItem(delegate(object state)
                    {
                        try
                        {
                            SelectedIncidentHelp(selectedIncidentTask);
                        }
                        catch (Exception ex)
                        {
                            MessageForm.Show(ex.Message, ex);
                        }
                    });

                    if (selectedIncident.CctvReports != null && selectedIncident.CctvReports.Count > 0)
                    {
                        try
                        {
                            CctvReportClientData report = ((CctvReportClientData)selectedIncident.CctvReports[0]);
                            if (groupControlVideo.Controls.Count > 0)
                                ((CameraPanelControl)groupControlVideo.Controls[0]).StopPlayBack();
                            groupControlVideo.Controls.Clear();
                           
                            videoStartDate = report.StartDate.AddSeconds(-int.Parse(CctvVideoSecondsBefore.Value));
                            videoEndDate = report.StartDate.AddSeconds(int.Parse(CctvVideoSecondsAfter.Value));
                            if (this.CameraPanel != null)   this.CameraPanel.CameraPanelControlTemplates();
                            CameraPanelControl panel = new CameraPanelControl(report.Camera, PlayModes.PlayBack_SingleSequence, config, false, videoStartDate, videoEndDate,VmsControl);
                            CameraPanel = panel;
                            panel.EndDate = videoEndDate;
                            panel.StartDate = videoStartDate;
                            panel.DateTimeValueChanged += new VmsControlEx.DateTimeValueChangedEventHandler(panel_DateTimeValueChanged);
                            groupControlVideo.Controls.Add(panel);
                            panel.Dock = DockStyle.Fill;

                            panel.StartPlayBack();
                        }
                        catch { }
                    }
                });
            }
            else
            {
                textBoxExIncidentdetails.DocumentText = String.Format("<span style='HEIGHT:100%;FONT-SIZE: 10pt; FONT-FAMILY: Sans-Serif'>{0}</span>", ResourceLoader.GetString2("NoIncidentSelected"));
               
                if (groupControlVideo.Controls.Count > 0)
                    ((CameraPanelControl)groupControlVideo.Controls[0]).StopPlayBack();
                try { groupControlVideo.Controls.Clear(); }
                catch { }
            }
        }

        void panel_DateTimeValueChanged(DateTime dateTime)
        {
            if (dateTime >= videoEndDate && groupControlVideo.Controls.Count > 0)
            {
                ((CameraPanelControl)groupControlVideo.Controls[0]).StartDate = videoStartDate;
            }
        }

        private void SelectedIncidentHelp(SelectedIncidentTask selectedIncidentTask)
        {
            if (selectedIncidentTask != null)
            {
                ArrayList tasks = new ArrayList();
                tasks.Add(selectedIncidentTask);
                bool sameIncident = false;
                if (selectedIncidentCode == (int)selectedIncidentTask.Parameters[0])
                    sameIncident = true;
                selectedIncidentCode = (int)selectedIncidentTask.Parameters[0];
                try
                {
                    IList result = ServerServiceClient.GetInstance().RunTasks(tasks);
                    if (result != null && result.Count > 0 && (result[0] as IList) != null)
                    {
                        IList tempList = (IList)result[0];
                        if (tempList != null && tempList.Count > 0)
                        {
                            SelectedIncidentTaskResult selectedIncidentLightData = tempList[0] as SelectedIncidentTaskResult;
                            if (selectedIncidentLightData != null)
                            {
                                FillIncidentDetails(selectedIncidentLightData, sameIncident);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    SmartLogger.Print(ex);
                }
            }
        }

        private void FillIncidentDetails(SelectedIncidentTaskResult selectedIncidentTask, bool sameIncident)
        {
            try
            {
                string strXml = ApplicationUtil.BuildHtml(selectedIncidentTask.Xml, xslCompiledTransform, "Cctvincident");
                strXml = ApplicationUtil.RecoverBlanks(strXml);

                FileStream fs = File.Open("incident_cctv.html", FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite);
                byte[] byteXML = Encoding.Unicode.GetBytes(strXml);
                fs.Write(byteXML, 0, byteXML.Length);
                fs.Flush();
                fs.Close();

                FormUtil.InvokeRequired(textBoxExIncidentdetails, () =>
                    textBoxExIncidentdetails.Navigate(new FileInfo("incident_cctv.html").FullName, sameIncident));
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        private void barButtonItemPrint_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            textBoxExIncidentdetails.ShowPrintDialog();
        }

        private void barButtonItemSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            textBoxExIncidentdetails.ShowSaveAsDialog();
        }

        private void CctvIncidentsHistoryForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            //groupControlVideo.Controls.Clear();

            //e.Cancel = true;
            //if (vmsThread != null)
            //    vmsThread.Abort();
        }
    }
}