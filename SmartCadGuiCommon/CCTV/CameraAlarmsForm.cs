﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Smartmatic.SmartCad.Vms;
using System.Threading;
using Smartmatic.SmartCad.Service;
using SmartCadCore.ClientData;
using SmartCadCore.Core;
using SmartCadControls;
using SmartCadCore.Common;
using SmartCadControls.Controls;
using SmartCadGuiCommon.Controls.Administration;
using SmartCadGuiCommon.Classes;
using SmartCadGuiCommon.Controls;
using SmartCadGuiCommon.CCTV.Nodes;

namespace SmartCadGuiCommon
{
    public partial class CameraAlarmsForm : DevExpress.XtraEditors.XtraForm
    {
        private ConfigurationClientData config = null;
        private static readonly DateTime _dt1970 = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
        private ServerServiceClient serverServiceClient;
        public VmsAlarmManager AlarmManager;
        List<CameraClientData> cameras = new List<CameraClientData>();
        public ApplicationPreferenceClientData CctvVideoSecondsBefore = null;
        public ApplicationPreferenceClientData CctvVideoSecondsAfter = null;
        private Thread vmsThread = null;
        private DateTime videoStartDate;
        private DateTime videoEndDate;
        
        public GridControlDataAlarm SelectedAlarm { get; set; }

        /// <summary>
        /// Set the current selected camera. Use "camera" == null to clear selection.
        /// </summary>
        /// <param name="camera"></param>
        void SelectAlarm(GridControlDataAlarm alarm)
        {
            if ((CctvFrontClientFormDevX)this.MdiParent != null)
            {
                if (alarm == null)
                    ((CctvFrontClientFormDevX)this.MdiParent).barButtonItemStartRegIncident.Enabled = false;
                else
                    ((CctvFrontClientFormDevX)this.MdiParent).barButtonItemStartRegIncident.Enabled = true;
            }
            SelectedAlarm = alarm;
        }

        void Panel_Disposed(object sender, EventArgs e)
        {
            try
            {
                ((CameraPanelControl)sender).DateTimeValueChanged -= panel_DateTimeValueChanged;
            }
            catch { }
            SelectedAlarm = null;
        }

        public ServerServiceClient ServerServiceClient
        {
            get
            {
                return serverServiceClient;
            }
            set
            {
                serverServiceClient = value;
            }
        }

        public CameraAlarmsForm()
        {
            InitializeComponent();
            CctvVideoSecondsBefore = (ApplicationPreferenceClientData)ServerServiceClient.GetInstance().SearchClientObject(
                SmartCadHqls.GetCustomHql(SmartCadHqls.GetApplicationPreferenceByName, "CctvVideoSecondsBefore"));
            CctvVideoSecondsAfter = (ApplicationPreferenceClientData)ServerServiceClient.GetInstance().SearchClientObject(
                SmartCadHqls.GetCustomHql(SmartCadHqls.GetApplicationPreferenceByName, "CctvVideoSecondsAfter"));

            InitGrid();
            config = ServerServiceClient.GetInstance().GetConfiguration();
            if (config.VmsDllName != "")
            {
                AlarmManager = VmsAlarmManager.GetInstance(config.VmsDllName);
            }
        }

        private void CameraEventsForm_Load(object sender, EventArgs e)
        {
            LoadLanguage();

            AlarmManager.Connect(config.VmsEventsServerIp);
            AlarmManager.AlarmsReceived += new AlarmsReceived(AlarmManager_AlarmsReceived); 
        }

        public void LoadInitialData()
        {
            #region Load Cameras
            foreach (CctvCameraNode camera in ((CctvFrontClientFormDevX)this.MdiParent).Cameras)
            {
                cameras.Add(camera.Camera);
            }
            #endregion
        }

        void AlarmManager_AlarmsReceived(List<VmsAlarm> newAlarms, List<VmsAlarm> closedAlarms)
        {
            if (newAlarms.Count > 0)
            {
                List<GridControlDataAlarm> toAdd = new List<GridControlDataAlarm>();
                foreach (VmsAlarm alarm in newAlarms)
                {
                    foreach (CameraClientData camera in cameras)
                    {
                        if (alarm.CameraId == camera.CustomId)
                        {
                            toAdd.Add(new GridControlDataAlarm(alarm, camera));
                            break;
                        }
                    }
                }
                GridControlEvents.AddOrUpdateList(toAdd);
            }

            if (GridControlEvents.DataSource != null && closedAlarms.Count > 0)
            {
                List<GridControlDataAlarm> toDelete = new List<GridControlDataAlarm>();
                foreach (VmsAlarm alarm in closedAlarms)
                {
                    toDelete.Add(new GridControlDataAlarm(alarm, null));
                    if(SelectedAlarm != null && SelectedAlarm.Alarm.AlarmId == alarm.AlarmId)
                    {
                        groupControlVideo.Controls.Clear();
                        //SelectedAlarm = null;
                    }
                }
                GridControlEvents.DeleteList(toDelete);
            }
        }

        //private void tmrUpdateStick_Tick(object sender, EventArgs e)
        //{
        //    syncServerDateTime = syncServerDateTime.AddMilliseconds((double)GLOBAL_TIMER_PERIOD);
        //    sinceLoggedIn = sinceLoggedIn.Add(ADD_TIMESPAN);
        //    FormUtil.InvokeRequired(this, delegate
        //    {
        //        toolStripStatusLabelGeneralDate.Text = ApplicationUtil.GetFormattedDate(ServerServiceClient.GetInstance().GetTime());
        //        this.toolStripStatusLabelConnectedTime.Text = Math.Truncate(sinceLoggedIn.TotalHours).ToString().PadLeft(2, '0') + ":" + sinceLoggedIn.Minutes.ToString().PadLeft(2, '0');
        //    });
        //}

        //private void FillStatusStrip()
        //{
        //    toolStripStatusLabelUserName.Text = serverServiceClient.OperatorClient.FirstName + " " + serverServiceClient.OperatorClient.LastName;
        //}

        /// <summary>
        /// Initialize Grid Control EX of Alarms
        /// </summary>
        private void InitGrid()
        {
            GridControlEvents.EnableAutoFilter = true;
            GridControlEvents.Type = typeof(GridControlDataAlarm);
            GridControlEvents.ViewTotalRows = true;
            GridControlEvents.AllowDrop = false;
            GridViewEvents.ViewTotalRows = true;
            GridViewEvents.BestFitColumns();
            GridViewEvents.FocusedRowHandle = GridControlEx.AutoFilterRowHandle;
            GridViewEvents.SelectRow(GridControlEx.AutoFilterRowHandle);
        }

        private void LoadLanguage()
        {
            this.Text = ResourceLoader.GetString2("Alerts");
            dockPanel1.Text = ResourceLoader.GetString2("Alerts");
            groupControlVideo.Text = ResourceLoader.GetString2("Alert Video");

            barButtonItemPrint.Caption = ResourceLoader.GetString2("Print");
            barButtonItemRefresh.Caption = ResourceLoader.GetString2("Refresh");
            barButtonItemSave.Caption = ResourceLoader.GetString2("Save");
            ribbonPageGroupGeneralOptions.Text = ResourceLoader.GetString2("GeneralOptions");
            ribbonPage1.Text = ResourceLoader.GetString2("Cctv");

            #region Footer
            //toolStripStatusLABEL_TEXT.Text = ResourceLoader.GetString2("User") + ":";
            //toolStripStatusLabelConnected.Text = ResourceLoader.GetString2("Connected") + ":";
            #endregion
        }
        
        private void GridViewEvents_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (GridViewEvents.GetFocusedRow() != null)
            {
                GridControlDataAlarm gridData = (GridControlDataAlarm)GridViewEvents.GetFocusedRow();
                SelectAlarm(gridData);

                if (gridData.Camera != null)
                {
                    videoStartDate = gridData.DateTime.AddSeconds(-int.Parse(CctvVideoSecondsBefore.Value));
                    videoEndDate = gridData.DateTime.AddSeconds(int.Parse(CctvVideoSecondsAfter.Value));

                    if (groupControlVideo.Controls.Count > 0)
                    {
                        CameraPanelControl selectedPanel = (CameraPanelControl)groupControlVideo.Controls[0];
                        selectedPanel.DateTimeValueChanged -= panel_DateTimeValueChanged;
                        if (selectedPanel.Camera.CustomId == gridData.Camera.CustomId)
                        {
                            selectedPanel.PlayDirection = 0;
                            selectedPanel.StartDate = gridData.DateTime;
                            return;
                        }
                    }

                    groupControlVideo.Controls.Clear();
                    //CameraPanelControl panel = new CameraPanelControl(gridData.Camera, PlayModes.PlayBack_SingleSequence, config);
                    //panel.StartDate = videoStartDate;
                    //panel.DateTimeValueChanged += panel_DateTimeValueChanged;
                    //groupControlVideo.Controls.Add(panel);
                    //panel.Dock = DockStyle.Fill;
                    //panel.Disposed += Panel_Disposed;

                    //panel.StartPlayBack();
                }
            }
            else
            {
                groupControlVideo.Controls.Clear();
                SelectAlarm(null);
            }
        }

        void panel_DateTimeValueChanged(DateTime dateTime)
        {
            if (groupControlVideo.Controls.Count > 0)
            {
                if (dateTime >= videoEndDate)
                {
                    ((CameraPanelControl)groupControlVideo.Controls[0]).StartDate = videoStartDate;
                }
            }
        }
        
        private void CameraEventsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
            }
            else
            {
                AlarmManager.Disconnect();
            }
        }

        private void toolStripMenuItemCancel_Click(object sender, EventArgs e)
        {
            //CODE TO DELETE ALL THE INCOMMING ALERTS. AA
            //foreach (GridControlDataAlarm grid in (BindingList<GridControlDataAlarm>)GridControlEvents.DataSource)
            //{
            //    AlarmManager.UpdateAlarm(grid.Alarm, 11);
            //}

            GridControlDataAlarm gridData = (GridControlDataAlarm)GridViewEvents.GetFocusedRow();
            if(gridData!=null)
                AlarmManager.UpdateAlarm(gridData.Alarm,11);

            GridViewEvents.BeginUpdate();
            GridViewEvents.DeleteSelectedRows();
            GridViewEvents.EndUpdate();
        }

        public void TakeAlarm(GridControlDataAlarm gridControlDataAlarm)
        {
            //In progress
            AlarmManager.UpdateAlarm(gridControlDataAlarm.Alarm, 4);
        }

        public void ReleaseAlarm(GridControlDataAlarm gridControlDataAlarm)
        {
            //New
            AlarmManager.UpdateAlarm(gridControlDataAlarm.Alarm, 1);
        }

        public void FisnishAlarm(GridControlDataAlarm gridControlDataAlarm)
        {
            //Closed
            AlarmManager.UpdateAlarm(gridControlDataAlarm.Alarm, 11);
        }

        private void dockPanel1_Click(object sender, EventArgs e)
        {

        }

        private void dockPanel1_Container_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}