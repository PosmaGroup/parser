
using SmartCadControls;
using SmartCadControls.Controls;

namespace SmartCadGuiCommon
{
    partial class CctvIncidentsHistoryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CctvIncidentsHistoryForm));
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanel1 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.searchFilterBar1 = new SearchFilterBar();
            this.dockPanelGrid = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel2_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.gridControlExPreviousIncidents = new GridControlEx();
            this.gridViewExPreviousIncidents = new GridViewEx();
            this.textBoxExIncidentdetails = new SearchableWebBrowser();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.groupControlDetails = new DevExpress.XtraEditors.GroupControl();
            this.groupControlVideo = new DevExpress.XtraEditors.GroupControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barButtonItemPrint = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemSave = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.barButtonItemCancelTelemetryAlarm = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPageGroupGeneralOptions = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupAlarm = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.dockPanel1.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            this.dockPanelGrid.SuspendLayout();
            this.dockPanel2_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExPreviousIncidents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExPreviousIncidents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlDetails)).BeginInit();
            this.groupControlDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlVideo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(795, 690);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(50, 20);
            this.layoutControlItem1.TextToControlDistance = 5;
            // 
            // dockManager1
            // 
            this.dockManager1.Form = this;
            this.dockManager1.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanel1,
            this.dockPanelGrid});
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "System.Windows.Forms.StatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            // 
            // dockPanel1
            // 
            this.dockPanel1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.dockPanel1.Appearance.Options.UseBackColor = true;
            this.dockPanel1.Controls.Add(this.dockPanel1_Container);
            this.dockPanel1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanel1.ID = new System.Guid("471da6ac-a016-472d-9140-2353b31e4409");
            this.dockPanel1.Location = new System.Drawing.Point(201, 119);
            this.dockPanel1.Name = "dockPanel1";
            this.dockPanel1.Options.AllowDockFill = false;
            this.dockPanel1.Options.AllowDockTop = false;
            this.dockPanel1.Options.ShowCloseButton = false;
            this.dockPanel1.Options.ShowMaximizeButton = false;
            this.dockPanel1.OriginalSize = new System.Drawing.Size(233, 200);
            this.dockPanel1.Size = new System.Drawing.Size(233, 623);
            this.dockPanel1.Text = "Options";
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.searchFilterBar1);
            this.dockPanel1_Container.Location = new System.Drawing.Point(3, 29);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(227, 591);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // searchFilterBar1
            // 
            this.searchFilterBar1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.searchFilterBar1.Location = new System.Drawing.Point(0, 0);
            this.searchFilterBar1.Name = "searchFilterBar1";
            this.searchFilterBar1.Size = new System.Drawing.Size(227, 591);
            this.searchFilterBar1.TabIndex = 0;
            // 
            // dockPanelGrid
            // 
            this.dockPanelGrid.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.dockPanelGrid.Appearance.Options.UseBackColor = true;
            this.dockPanelGrid.Controls.Add(this.dockPanel2_Container);
            this.dockPanelGrid.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanelGrid.ID = new System.Guid("fd96e0ca-f576-47b8-8598-de62ee6865c5");
            this.dockPanelGrid.Location = new System.Drawing.Point(0, 119);
            this.dockPanelGrid.Name = "dockPanelGrid";
            this.dockPanelGrid.Options.ShowCloseButton = false;
            this.dockPanelGrid.OriginalSize = new System.Drawing.Size(201, 200);
            this.dockPanelGrid.Size = new System.Drawing.Size(201, 623);
            this.dockPanelGrid.Text = "dockPanelGrid";
            // 
            // dockPanel2_Container
            // 
            this.dockPanel2_Container.Controls.Add(this.gridControlExPreviousIncidents);
            this.dockPanel2_Container.Location = new System.Drawing.Point(3, 29);
            this.dockPanel2_Container.Name = "dockPanel2_Container";
            this.dockPanel2_Container.Size = new System.Drawing.Size(195, 591);
            this.dockPanel2_Container.TabIndex = 0;
            // 
            // gridControlExPreviousIncidents
            // 
            this.gridControlExPreviousIncidents.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlExPreviousIncidents.EnableAutoFilter = true;
            this.gridControlExPreviousIncidents.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExPreviousIncidents.Location = new System.Drawing.Point(0, 0);
            this.gridControlExPreviousIncidents.MainView = this.gridViewExPreviousIncidents;
            this.gridControlExPreviousIncidents.Name = "gridControlExPreviousIncidents";
            this.gridControlExPreviousIncidents.Size = new System.Drawing.Size(195, 591);
            this.gridControlExPreviousIncidents.TabIndex = 4;
            this.gridControlExPreviousIncidents.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExPreviousIncidents});
            this.gridControlExPreviousIncidents.ViewTotalRows = true;
            this.gridControlExPreviousIncidents.CellToolTipNeeded += new DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventHandler(this.gridControlExPreviousIncidents_CellToolTipNeeded);
            // 
            // gridViewExPreviousIncidents
            // 
            this.gridViewExPreviousIncidents.AllowFocusedRowChanged = true;
            this.gridViewExPreviousIncidents.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewExPreviousIncidents.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExPreviousIncidents.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewExPreviousIncidents.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewExPreviousIncidents.Appearance.FooterPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewExPreviousIncidents.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewExPreviousIncidents.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewExPreviousIncidents.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExPreviousIncidents.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewExPreviousIncidents.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewExPreviousIncidents.EnablePreviewLineForFocusedRow = false;
            this.gridViewExPreviousIncidents.GridControl = this.gridControlExPreviousIncidents;
            this.gridViewExPreviousIncidents.Name = "gridViewExPreviousIncidents";
            this.gridViewExPreviousIncidents.OptionsMenu.EnableFooterMenu = false;
            this.gridViewExPreviousIncidents.OptionsNavigation.UseTabKey = false;
            this.gridViewExPreviousIncidents.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExPreviousIncidents.OptionsView.ShowDetailButtons = false;
            this.gridViewExPreviousIncidents.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewExPreviousIncidents.OptionsView.ShowFooter = true;
            this.gridViewExPreviousIncidents.ViewTotalRows = true;
            this.gridViewExPreviousIncidents.SelectionWillChange += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewExPreviousIncidents_SelectionWillChange);
            // 
            // textBoxExIncidentdetails
            // 
            this.textBoxExIncidentdetails.AllowWebBrowserDrop = false;
            this.textBoxExIncidentdetails.AutoSize = true;
            this.textBoxExIncidentdetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxExIncidentdetails.DocumentText = "<HTML></HTML>\0";
            this.textBoxExIncidentdetails.IsWebBrowserContextMenuEnabled = true;
            this.textBoxExIncidentdetails.Location = new System.Drawing.Point(2, 22);
            this.textBoxExIncidentdetails.Margin = new System.Windows.Forms.Padding(0);
            this.textBoxExIncidentdetails.Name = "textBoxExIncidentdetails";
            this.textBoxExIncidentdetails.Size = new System.Drawing.Size(728, 396);
            this.textBoxExIncidentdetails.TabIndex = 6;
            this.textBoxExIncidentdetails.WebBrowserShortcutsEnabled = false;
            this.textBoxExIncidentdetails.XmlText = "<HTML></HTML>\0";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.splitContainerControl1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(434, 119);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(740, 623);
            this.layoutControl1.TabIndex = 9;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(4, 4);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.groupControlDetails);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.groupControlVideo);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(732, 615);
            this.splitContainerControl1.SplitterPosition = 420;
            this.splitContainerControl1.TabIndex = 8;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // groupControlDetails
            // 
            this.groupControlDetails.Controls.Add(this.textBoxExIncidentdetails);
            this.groupControlDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControlDetails.Location = new System.Drawing.Point(0, 0);
            this.groupControlDetails.Name = "groupControlDetails";
            this.groupControlDetails.Size = new System.Drawing.Size(732, 420);
            this.groupControlDetails.TabIndex = 7;
            this.groupControlDetails.Text = "IncidentDetails";
            // 
            // groupControlVideo
            // 
            this.groupControlVideo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControlVideo.Location = new System.Drawing.Point(0, 0);
            this.groupControlVideo.Name = "groupControlVideo";
            this.groupControlVideo.Size = new System.Drawing.Size(732, 189);
            this.groupControlVideo.TabIndex = 7;
            this.groupControlVideo.Text = "Video";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(740, 623);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.splitContainerControl1;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(736, 619);
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ApplicationButtonAnimationLength = 1;
            this.ribbonControl1.ExpandCollapseItem.AllowRightClickInMenu = false;
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.ExpandCollapseItem.Name = "";
            this.ribbonControl1.GroupAnimationLength = 1;
            this.ribbonControl1.ItemAnimationLength = 1;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.barButtonItemPrint,
            this.barButtonItemSave,
            this.barButtonItemRefresh,
            this.barButtonItemCancelTelemetryAlarm});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 45;
            this.ribbonControl1.MdiMergeStyle = DevExpress.XtraBars.Ribbon.RibbonMdiMergeStyle.Always;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.PageAnimationLength = 1;
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbonControl1.ShowExpandCollapseButton = DevExpress.Utils.DefaultBoolean.True;
            this.ribbonControl1.ShowToolbarCustomizeItem = false;
            this.ribbonControl1.Size = new System.Drawing.Size(1174, 119);
            this.ribbonControl1.Toolbar.ShowCustomizeItem = false;
            this.ribbonControl1.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            // 
            // barButtonItemPrint
            // 
            this.barButtonItemPrint.Caption = "ItemPrint";
            this.barButtonItemPrint.Enabled = false;
            this.barButtonItemPrint.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPrint.Glyph")));
            this.barButtonItemPrint.Id = 6;
            this.barButtonItemPrint.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemPrint.Name = "barButtonItemPrint";
            this.barButtonItemPrint.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemPrint_ItemClick);
            // 
            // barButtonItemSave
            // 
            this.barButtonItemSave.Caption = "ItemSave";
            this.barButtonItemSave.Enabled = false;
            this.barButtonItemSave.Glyph = global::SmartCadGuiCommon.PrintRibbonControllerResources.RibbonPrintPreview_Save;
            this.barButtonItemSave.Id = 7;
            this.barButtonItemSave.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemSave.Name = "barButtonItemSave";
            this.barButtonItemSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemSave_ItemClick);
            // 
            // barButtonItemRefresh
            // 
            this.barButtonItemRefresh.Caption = "ItemRefresh";
            this.barButtonItemRefresh.Enabled = false;
            this.barButtonItemRefresh.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemRefresh.Glyph")));
            this.barButtonItemRefresh.Id = 8;
            this.barButtonItemRefresh.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemRefresh.Name = "barButtonItemRefresh";
            // 
            // barButtonItemCancelTelemetryAlarm
            // 
            this.barButtonItemCancelTelemetryAlarm.Caption = "barButtonItemCancelAlarm";
            this.barButtonItemCancelTelemetryAlarm.Enabled = false;
            this.barButtonItemCancelTelemetryAlarm.Id = 41;
            this.barButtonItemCancelTelemetryAlarm.LargeGlyph = global::SmartCadGuiCommon.Properties.Resources.CancelAlarm1;
            this.barButtonItemCancelTelemetryAlarm.Name = "barButtonItemCancelTelemetryAlarm";
            //this.barButtonItemCancelTelemetryAlarm.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemCancelTelemetryAlarm_ItemClick);
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupGeneralOptions});
            //this.ribbonPageGroupAlarm});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "ribbonPage1";
            // 
            // ribbonPageGroupGeneralOptions
            // 
            this.ribbonPageGroupGeneralOptions.AllowMinimize = false;
            this.ribbonPageGroupGeneralOptions.AllowTextClipping = false;
            this.ribbonPageGroupGeneralOptions.ItemLinks.Add(this.barButtonItemPrint);
            this.ribbonPageGroupGeneralOptions.ItemLinks.Add(this.barButtonItemSave);
            this.ribbonPageGroupGeneralOptions.ItemLinks.Add(this.barButtonItemRefresh);
            this.ribbonPageGroupGeneralOptions.Name = "ribbonPageGroupGeneralOptions";
            this.ribbonPageGroupGeneralOptions.ShowCaptionButton = false;
            this.ribbonPageGroupGeneralOptions.Text = "ribbonPageGroupGeneralOptions";
            // 
            // ribbonPageGroupAlarm
            //
            this.ribbonPageGroupAlarm.AllowMinimize = false;
            this.ribbonPageGroupAlarm.AllowTextClipping = false;
            //this.ribbonPageGroupAlarm.ItemLinks.Add(this.barButtonItemCancelTelemetryAlarm);
            this.ribbonPageGroupAlarm.Name = "ribbonPageGroupAlarm";
            this.ribbonPageGroupAlarm.ShowCaptionButton = false;
            this.ribbonPageGroupAlarm.Text = "ribbonPageGroupAlarm";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // CctvIncidentsHistoryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1174, 742);
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.dockPanel1);
            this.Controls.Add(this.dockPanelGrid);
            this.Controls.Add(this.ribbonControl1);
            this.Name = "CctvIncidentsHistoryForm";
            this.Text = "IncidentsHistoryForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CctvIncidentsHistoryForm_FormClosing);
            this.Load += new System.EventHandler(this.CctvIncidentsHistoryForm_Load);
            this.ControlBox = false;
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.dockPanel1.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            this.dockPanelGrid.ResumeLayout(false);
            this.dockPanel2_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExPreviousIncidents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExPreviousIncidents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControlDetails)).EndInit();
            this.groupControlDetails.ResumeLayout(false);
            this.groupControlDetails.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlVideo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel1;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private SearchFilterBar searchFilterBar1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelGrid;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel2_Container;
        public GridControlEx gridControlExPreviousIncidents;
        private GridViewEx gridViewExPreviousIncidents;
        private SearchableWebBrowser textBoxExIncidentdetails;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        public DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.XtraBars.BarButtonItem barButtonItemPrint;
        private DevExpress.XtraBars.BarButtonItem barButtonItemSave;
        private DevExpress.XtraBars.BarButtonItem barButtonItemRefresh;
        public DevExpress.XtraBars.BarButtonItem barButtonItemCancelTelemetryAlarm;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupGeneralOptions;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupAlarm;
        public DevExpress.XtraEditors.GroupControl groupControlVideo;
        private DevExpress.XtraEditors.GroupControl groupControlDetails;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
    }
}