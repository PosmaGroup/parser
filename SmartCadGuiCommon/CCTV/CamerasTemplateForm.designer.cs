using SmartCadControls;
using SmartCadGuiCommon.Controls;
namespace Smartmatic.SmartCad.Gui
{
    partial class CamerasTemplateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.dragDropLayoutControl1 = new DragDropTemplateControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.textEditName = new DevExpress.XtraEditors.TextEdit();
            this.panelControlGrid = new DevExpress.XtraEditors.PanelControl();
            this.gridControlExCameras = new GridControlEx();
            this.gridViewExCameras = new GridViewEx();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemGrid = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemTextEdit = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlGrid)).BeginInit();
            this.panelControlGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExCameras)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExCameras)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTextEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.dragDropLayoutControl1);
            this.layoutControl1.Controls.Add(this.simpleButton1);
            this.layoutControl1.Controls.Add(this.textEditName);
            this.layoutControl1.Controls.Add(this.panelControlGrid);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1078, 732);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // dragDropLayoutControl1
            // 
            this.dragDropLayoutControl1.AllowDrop = true;
            this.dragDropLayoutControl1.Location = new System.Drawing.Point(307, 12);
            this.dragDropLayoutControl1.Name = "dragDropLayoutControl1";
            this.dragDropLayoutControl1.SelectedCamera = null;
            this.dragDropLayoutControl1.Size = new System.Drawing.Size(759, 708);
            this.dragDropLayoutControl1.TabIndex = 0;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(225, 12);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(78, 22);
            this.simpleButton1.StyleController = this.layoutControl1;
            this.simpleButton1.TabIndex = 7;
            this.simpleButton1.Text = "simpleButton1";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // textEditName
            // 
            this.textEditName.Location = new System.Drawing.Point(94, 12);
            this.textEditName.Name = "textEditName";
            this.textEditName.Size = new System.Drawing.Size(127, 20);
            this.textEditName.StyleController = this.layoutControl1;
            this.textEditName.TabIndex = 6;
            // 
            // panelControlGrid
            // 
            this.panelControlGrid.Controls.Add(this.gridControlExCameras);
            this.panelControlGrid.Location = new System.Drawing.Point(12, 38);
            this.panelControlGrid.Name = "panelControlGrid";
            this.panelControlGrid.Size = new System.Drawing.Size(291, 682);
            this.panelControlGrid.TabIndex = 5;
            // 
            // gridControlExCameras
            // 
            this.gridControlExCameras.AllowDrop = true;
            this.gridControlExCameras.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlExCameras.EnableAutoFilter = true;
            this.gridControlExCameras.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExCameras.Location = new System.Drawing.Point(2, 2);
            this.gridControlExCameras.MainView = this.gridViewExCameras;
            this.gridControlExCameras.Name = "gridControlExCameras";
            this.gridControlExCameras.Size = new System.Drawing.Size(287, 678);
            this.gridControlExCameras.TabIndex = 6;
            this.gridControlExCameras.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExCameras});
            this.gridControlExCameras.ViewTotalRows = true;
            this.gridControlExCameras.MouseMove += new System.Windows.Forms.MouseEventHandler(this.gridControlExCameras_MouseMove);
            // 
            // gridViewExCameras
            // 
            this.gridViewExCameras.AllowFocusedRowChanged = true;
            this.gridViewExCameras.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewExCameras.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExCameras.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewExCameras.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewExCameras.Appearance.FooterPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewExCameras.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewExCameras.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewExCameras.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExCameras.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewExCameras.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewExCameras.EnablePreviewLineForFocusedRow = false;
            this.gridViewExCameras.GridControl = this.gridControlExCameras;
            this.gridViewExCameras.Name = "gridViewExCameras";
            this.gridViewExCameras.OptionsMenu.EnableFooterMenu = false;
            this.gridViewExCameras.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExCameras.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewExCameras.OptionsView.ShowFooter = true;
            this.gridViewExCameras.ViewTotalRows = true;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemGrid,
            this.layoutControlItemTextEdit,
            this.layoutControlItem1,
            this.layoutControlItem2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1078, 732);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItemGrid
            // 
            this.layoutControlItemGrid.Control = this.panelControlGrid;
            this.layoutControlItemGrid.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItemGrid.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItemGrid.Name = "layoutControlItemGrid";
            this.layoutControlItemGrid.Size = new System.Drawing.Size(295, 686);
            this.layoutControlItemGrid.Text = "layoutControlItemGrid";
            this.layoutControlItemGrid.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemGrid.TextToControlDistance = 0;
            this.layoutControlItemGrid.TextVisible = false;
            // 
            // layoutControlItemTextEdit
            // 
            this.layoutControlItemTextEdit.Control = this.textEditName;
            this.layoutControlItemTextEdit.CustomizationFormText = "Template Name:";
            this.layoutControlItemTextEdit.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemTextEdit.Name = "layoutControlItemTextEdit";
            this.layoutControlItemTextEdit.Size = new System.Drawing.Size(213, 26);
            this.layoutControlItemTextEdit.Text = "Template Name:";
            this.layoutControlItemTextEdit.TextSize = new System.Drawing.Size(78, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.simpleButton1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(213, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(82, 26);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.dragDropLayoutControl1;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(295, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(763, 712);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // CamerasTemplateForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1078, 732);
            this.Controls.Add(this.layoutControl1);
            this.Name = "CamerasTemplateForm";
            this.Text = "CamerasTemplateForm";
            this.Load += new System.EventHandler(this.CamerasLayoutForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEditName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlGrid)).EndInit();
            this.panelControlGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExCameras)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExCameras)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTextEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.PanelControl panelControlGrid;
        private GridControlEx gridControlExCameras;
        private GridViewEx gridViewExCameras;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemGrid;
        private DevExpress.XtraEditors.TextEdit textEditName;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTextEdit;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DragDropTemplateControl dragDropLayoutControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
    }
}