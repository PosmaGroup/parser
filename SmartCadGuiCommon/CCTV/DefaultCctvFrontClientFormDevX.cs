using Microsoft.Win32;
using SmartCadControls;
using SmartCadControls.Controls;
using SmartCadControls.Filters;
using SmartCadControls.Filters;
using SmartCadControls.SyncBoxes;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadCore.Core;
using SmartCadCore.Enums;
using SmartCadFirstLevel.Gui.SyncBoxes;
using SmartCadGuiCommon.Controls;
using SmartCadGuiCommon.Enums;
using SmartCadGuiCommon.Services;
using SmartCadGuiCommon.Util;
using Smartmatic.SmartCad.Gui;
using Smartmatic.SmartCad.Service;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Printing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Xsl;

namespace SmartCadGuiCommon
{

    //public enum CctvClientStateEnum
    //{
    //    None,
    //    WaitingForCctvIncident,
    //    RegisteringCctvIncident
    //}

	public partial class DefaultCctvFrontClientFormDevX : DevExpress.XtraEditors.XtraForm
    {
        #region Fields
        private ServerServiceClient serverServiceClient;
        private CctvReportClientData globalCctvReport;
        private bool isActiveCall = false;
        private bool incidentAdded = false;
        private bool callEntering = false;
        private ImageList activeStepImages;
        private ImageList inactiveStepImages;
        private FilterBase incidentsCurrentFilter;
        private ToolStripMenuItem toolStripMenuItem;
        private SkinEngine skinEngine;
        private System.Threading.Timer answersUpdatedTimer;
        private EventArgs answersUpdatedTimerEventArgs;
        private Dictionary<int, IncidentTypeClientData> globalIncidentTypes;
        private Dictionary<int,DepartmentTypeClientData> globalDepartmentTypes;
        private Dictionary<string, IncidentNotificationPriorityClientData> globalNotificationPriorities;
        private IList globalIncidents;
        private Dictionary<string, ApplicationPreferenceClientData> globalApplicationPreference;
        private object syncAssociatedInfo;
        private IList updatedAnswers = new ArrayList();
        private XslCompiledTransform xslCompiledTransform;       
        private ConfigurationClientData configurationClient;        
        private GridControlSynBox incidentSyncBox;        
        private NetworkCredential networkCredential;          
        private int selectedIncidentCode = -1;        
        private System.Threading.Timer refreshIncidentReport;
        public string password;
        private object IEFooter;
        private string objectValue;
        public string ExtNumber;
        private const string SourceDirectory = "Software\\Microsoft\\Internet Explorer\\PageSetup";
        private bool searchByText = false;
        private bool popUpShow = true;       
        private object sync = new object();
        private ArrayList IncidentChanges = new ArrayList();
        private ArrayList QuestionChanges = new ArrayList();
		private ArrayList DepartmentTypeChanges = new ArrayList();
        private CctvClientStateEnum cctvClientState = CctvClientStateEnum.None;
        private Dictionary<int, IncidentTypeClientData> incidentTypeActionsUpdate;
        private Dictionary<int, IncidentTypeClientData> incidentTypeActionDelete;
        private Dictionary<int, IncidentTypeClientData> incidentTypeActionAdd;
        private Dictionary<int, QuestionClientData> questionActionsUpdate;
        private Dictionary<int, QuestionClientData> questionActionDelete;
        private Dictionary<int, QuestionClientData> questionActionAdd;
        private GridCctvAlertCamData AlarmDataSelect;

        #endregion

        #region Properties       

        public CctvClientStateEnum CctvClientState
        {
            get
            {
                return this.cctvClientState;
            }
            set
            {
                this.cctvClientState = value;
                if (value == CctvClientStateEnum.WaitingForCctvIncident)
                    SetFrontClientWaitingForCallState();
                else if (value == CctvClientStateEnum.RegisteringCctvIncident)
                    SetFrontClientRegisteringCallState();                
            }
        }

        public bool IsActiveCall
        {
            get
            {
                return this.isActiveCall;
            }
            set
            {
                this.isActiveCall = value;
            }
        }

        public IList GlobalIncidents
        {
            get
            {
                return globalIncidents;
            }
            set
            {
                globalIncidents = value;
            }
        }

        public CctvReportClientData GlobalCctvReport
        {
            get
            {
                return globalCctvReport;
            }
            set
            {
                globalCctvReport = value;
            }
        }

        object syncGlobalIncidentTypes = new object();
        public Dictionary<int, IncidentTypeClientData> GlobalIncidentTypes
        {
            get
            {
                lock (syncGlobalIncidentTypes)
                {
                    return globalIncidentTypes;
                }
            }
            set
            {
                lock (sync)
                {
                    globalIncidentTypes = value;

                    foreach (IncidentTypeClientData itcd in incidentTypeActionAdd.Values)
                    {
                        if (globalIncidentTypes.ContainsKey(itcd.Code) == false)
                        {
                            if (itcd.NoEmergency == false)
                            {
                                globalIncidentTypes.Add(itcd.Code, itcd);
                            }
                        }
                            //globalIncidentTypes.Add(itcd.Code, itcd);
                    }
                    incidentTypeActionAdd.Clear();

                    foreach (IncidentTypeClientData itcd in incidentTypeActionsUpdate.Values)
                    {
                        if (globalIncidentTypes.ContainsKey(itcd.Code) == true)
                            globalIncidentTypes[itcd.Code] = itcd;
                    }
                    incidentTypeActionsUpdate.Clear();

                    foreach (IncidentTypeClientData itcd in incidentTypeActionDelete.Values)
                    {
                        if (globalIncidentTypes.ContainsKey(itcd.Code) == true)
                            globalIncidentTypes.Remove(itcd.Code);
                    }
                    incidentTypeActionDelete.Clear();

                    foreach (QuestionClientData qcd in questionActionAdd.Values)
                    {
                        AddQuestion(qcd);
                    }
                    questionActionAdd.Clear();

                    foreach (QuestionClientData qcd in questionActionsUpdate.Values)
                    {
                        UpdateQuestion(qcd);
                    }
                    questionActionsUpdate.Clear();

                    foreach (QuestionClientData qcd in questionActionDelete.Values)
                    {
                        DeleteQuestion(qcd);
                    }
                    questionActionDelete.Clear();

                    questionsCctvControl1.GlobalIncidentTypes = globalIncidentTypes;
                }
            }
        }

        public Dictionary<int, DepartmentTypeClientData> GlobalDepartmentTypes
        {
            get
            {
                return this.globalDepartmentTypes;
            }
            set
            {
                this.globalDepartmentTypes = value;
                this.departmentsInvolvedControl.departmentTypesControl.GlobalDepartmentTypes = globalDepartmentTypes;
            }
        }

        public Dictionary<string, IncidentNotificationPriorityClientData> GlobalNotificationPriorities
        {
            get
            {
                return this.globalNotificationPriorities;
            }
            set
            {
                
                this.globalNotificationPriorities = value;
                this.departmentsInvolvedControl.departmentTypesControl.GlobalNotificationPriorities = globalNotificationPriorities;
            }
        }

        public Dictionary<string,ApplicationPreferenceClientData> GlobalApplicationPreference
        {
            get
            {
                return globalApplicationPreference;
            }
            set
            {
                globalApplicationPreference = value;
            }
        }

        public ServerServiceClient ServerServiceClient
        {
            get
            {
                return serverServiceClient;
            }
            set
            {
                serverServiceClient = value;
            }
        }
              

        internal QuestionsControlDevX QuestionsCctvControl1
        {
            get
            {
                return this.questionsCctvControl1;
            }
        }

        public NetworkCredential NetworkCredential
        {
            get
            {
                return networkCredential;
            }

            set
            {
                networkCredential = value;
            }
        }

        public ConfigurationClientData ConfigurationClient
        {
            get
            {
                return configurationClient;
            }
            set
            {
                configurationClient = value;
            }
        }

        public void SetPassword(string passwd)
        {
            this.password = passwd;
        }

        #endregion

        #region Constructors

		public DefaultCctvFrontClientFormDevX()
        {
            InitializeComponent();
            globalIncidents = new ArrayList();
            globalDepartmentTypes = new Dictionary<int, DepartmentTypeClientData>();
            globalNotificationPriorities = new Dictionary<string, IncidentNotificationPriorityClientData>();
            globalIncidentTypes = new Dictionary<int, IncidentTypeClientData>();
            
            syncAssociatedInfo = new object();
            refreshIncidentReport = new System.Threading.Timer(new TimerCallback(OnRefreshIncidentReportTimer));

            incidentTypeActionsUpdate = new Dictionary<int, IncidentTypeClientData>();
            incidentTypeActionDelete = new Dictionary<int, IncidentTypeClientData>();
            incidentTypeActionAdd = new Dictionary<int, IncidentTypeClientData>();
            questionActionsUpdate = new Dictionary<int, QuestionClientData>();
            questionActionDelete = new Dictionary<int, QuestionClientData>();
            questionActionAdd = new Dictionary<int, QuestionClientData>();

            ServerServiceClient.GetInstance().CommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(serverService_CommittedChanges);
        }

        #endregion

        public void LoadInitialData()
        {
            #region Loading Incident Types and questions
			IList clientIncidentTypes = ServerServiceClient.GetInstance().SearchClientObjectsMaxRows(SmartCadHqls.GetCustomHql(SmartCadHqls.IncidentTypesWithDepartmentTypesQuestions, UserApplicationClientData.Cctv.Code), true);
            foreach (IncidentTypeClientData incidentTypeClient in clientIncidentTypes)
            {
                if (globalIncidentTypes.ContainsKey(incidentTypeClient.Code))
                {
                    if (globalIncidentTypes[incidentTypeClient.Code].Version < incidentTypeClient.Version)
                    {
                        globalIncidentTypes[incidentTypeClient.Code] = incidentTypeClient;
                    }
                }
                else
                {
                    if (incidentTypeClient.NoEmergency == false)
                    {
                        globalIncidentTypes.Add(incidentTypeClient.Code, incidentTypeClient);
                    }
                }
            }
            this.GlobalIncidentTypes = globalIncidentTypes;
            #endregion

            #region Loading DepartmentTypes
            IList tempDepartmentTypes = ServerServiceClient.GetInstance().SearchClientObjects(
                     SmartCadHqls.GetDepartmentsTypeWithStationsAssociated, true);
            
            foreach (DepartmentTypeClientData departmentType in tempDepartmentTypes)
            {
                if (globalDepartmentTypes.ContainsKey(departmentType.Code))
                {
                    if (globalDepartmentTypes[departmentType.Code].Version < departmentType.Version)
                    {
                        globalDepartmentTypes[departmentType.Code] = departmentType;
                    }
                }
                else
                {
                    globalDepartmentTypes.Add(departmentType.Code, departmentType);
                }
            }
            this.GlobalDepartmentTypes = globalDepartmentTypes;
            #endregion

            #region Loading IncidentNotificationPriorities
            IList tempNotificationPriorities = ServerServiceClient.GetInstance().SearchClientObjectsMaxRows(SmartCadHqls.GetIncidentNotificationPriorities, true);            
            foreach (IncidentNotificationPriorityClientData notificationPriority in tempNotificationPriorities)
            {
                if (globalNotificationPriorities.ContainsKey(notificationPriority.CustomCode))
                {
                    if (globalNotificationPriorities[notificationPriority.CustomCode].Version < notificationPriority.Version)
                    {
                        globalNotificationPriorities[notificationPriority.CustomCode] = notificationPriority;
                    }
                }
                else
                {
                    globalNotificationPriorities.Add(notificationPriority.CustomCode, notificationPriority);
                }
            }
            GlobalNotificationPriorities = globalNotificationPriorities;
            #endregion

            #region Loading ApplicationPreferences
            GlobalApplicationPreference = new Dictionary<string, ApplicationPreferenceClientData>();
            foreach (ApplicationPreferenceClientData preference in 
                ServerServiceClient.GetInstance().SearchClientObjectsMaxRows(
                SmartCadHqls.GetAllApplicationPreferences, true))
            {
                GlobalApplicationPreference.Add(preference.Name, preference);
            }
            #endregion

            #region Loading Incidents
            IList incidentList = ServerServiceClient.GetInstance().SearchClientObjectsMaxRows(
                SmartCadHqls.GetCustomHql(
                SmartCadHqls.NewIncidentRuleEvaluate, UserApplicationClientData.Cctv.Code), true);
            int index = -1;
            foreach (IncidentClientData incidentClientData in incidentList)
            {
                if (incidentClientData.SourceIncidentApp == SourceIncidentAppEnum.Cctv)
                {
                    index = globalIncidents.IndexOf(incidentClientData);
                    if (index != -1 && (globalIncidents[index] as IncidentClientData).Version < incidentClientData.Version)
                    {
                        globalIncidents[index] = incidentClientData;
                    }
                    else if (index == -1)
                    {
                        globalIncidents.Add(incidentClientData);
                    }
                }
            }
            this.GlobalIncidents = globalIncidents;
            
            #endregion
        }

        private void CctvFrontClientForm_Load(object sender, EventArgs e)
        {
            SetSkin();

            XmlReaderSettings xmlReaderSettings = new XmlReaderSettings();
            xmlReaderSettings.ProhibitDtd = false;

            xslCompiledTransform = new XslCompiledTransform();
            xslCompiledTransform.Load(XmlReader.Create(SmartCadConfiguration.CctvIncidentXslt, xmlReaderSettings));            
           
            departmentsInvolvedControl.departmentTypesControl.Left = 
                (departmentsInvolvedControl.Size.Width - departmentsInvolvedControl.departmentTypesControl.Size.Width)/2;                      
            
            //Subscribing to events needed.
            questionsCctvControl1.AnswerUpdated += new EventHandler<EventArgs>(questionsControl_AnswerUpdated);

            questionsCctvControl1.IncidentTypesSelected += new EventHandler<EventArgs>(questionsCctvControl1_IncidentTypeSelected);           
            this.departmentsInvolvedControl.departmentTypesControl.DepartmentTypesSelected += new EventHandler<EventArgs>(departmentTypesControl2_DepartmentTypesSelected);
            questionsCctvControl1.Enter += new EventHandler(questionsCctvControl1_Enter);
            this.departmentsInvolvedControl.Enter += new EventHandler(departmentsInvolvedControl_Enter);         
            cctvCallInformationControl1.Enter +=new EventHandler(callReceiverControl1_Enter);            
            KeyDown += new KeyEventHandler(CctvFrontClientForm_KeyDown);

			
            //Filling needed information...
            LoadStepImages();

            //Setting up the FrontClient and Nortel Configuration...
            this.CctvClientState = CctvClientStateEnum.WaitingForCctvIncident;
            
            //Setting up the FrontClient and Nortel Configuration...
            this.CctvClientState = CctvClientStateEnum.WaitingForCctvIncident;
            ArrayList parameters = new ArrayList();
            parameters.Add("OPEN");      

            configurationClient = ServerServiceClient.GetInstance().GetConfiguration();
            answersUpdatedTimer = new System.Threading.Timer(new TimerCallback(OnAnswerUpdateTimer));
            CleanForm();
            questionsCctvControl1.removeIncident += new QuestionsControlDevX.RemoveIncident(questionsCctvControl1_removeIncident);
            FillCctvZones();            
            FillStructsType(); 
            departmentsInvolvedControl.departmentTypesControl.gridViewExDepartmentType.ClearSelection();
			LoadLanguage();
			questionsCctvControl1.clientMode = FrontClientMode.Cctv;

            
            //barButtonItemStartRegIncident.LargeGlyph = DevExpress.XtraBars
            //barButtonItemStartRegIncident.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;


			gridControlExIncidentList.Type = typeof(GridIncidentCctvData);
			incidentSyncBox = new GridControlSynBox(gridControlExIncidentList);
			incidentSyncBox.Sync(globalIncidents);
			gridControlExIncidentList.ViewTotalRows = true;
			gridControlExIncidentList.EnableAutoFilter = true;
			gridViewExIncidentList.OptionsView.ShowAutoFilterRow = true;
			gridViewExIncidentList.ViewTotalRows = true;
			gridControlExIncidentList.AllowDrop = false;
			gridControlExIncidentList.CellToolTipNeeded += new DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventHandler(gridControlExIncidentList_CellToolTipNeeded);
			gridViewExIncidentList.SelectionWillChange += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(gridViewEx1_SelectionWillChange);
            gridViewExIncidentList.SingleSelectionChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(gridViewExIncidentList_SingleSelectionChanged);
        }

        void departmentsInvolvedControl_Enter(object sender, EventArgs e)
        {
            if (this.CctvClientState != CctvClientStateEnum.WaitingForCctvIncident)
            {
                ChangeCurrentStep(3, false);
            }
        }


		void gridControlExIncidentList_CellToolTipNeeded(object sender, DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventArgs e)
		{
			if (e.Info ==null)
			{
				DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo info = ((GridViewEx)gridControlExIncidentList.MainView).CalcHitInfo(e.ControlMousePosition);
				if (info.RowHandle > -1)
				{
					e.Info = new DevExpress.Utils.ToolTipControlInfo();
					e.Info.Object = gridControlExIncidentList;

					if (this.gridViewExIncidentList.GetRow(info.RowHandle) != null)
					{
						IncidentClientData incidentData = (IncidentClientData)((GridIncidentCctvData)((IList)gridControlExIncidentList.DataSource)[info.RowHandle]).Tag;

						string toolTipText = "";

						if (incidentData != null)
						{
							foreach (IncidentTypeClientData incidentType in incidentData.IncidentTypes)
							{
								if (incidentType.NoEmergency != true)
									toolTipText = toolTipText + incidentType.FriendlyName + ", ";
							}
						}
						if (toolTipText.Trim() != "")
						{
							toolTipText = toolTipText.Substring(0, toolTipText.Length - 2);
						}
						e.Info.Text = toolTipText;
					}
				}
			}
		}

		private void LoadLanguage()
		{
			barButtonItemPrint.Caption = ResourceLoader.GetString2("Print");
			barButtonItemRefresh.Caption = ResourceLoader.GetString2("Refresh");
			barButtonItemSave.Caption = ResourceLoader.GetString2("Save"); 
			barButtonItemCancelIncident.Caption = ResourceLoader.GetString2("Cancel");
			barButtonItemCreateNewIncident.Caption = ResourceLoader.GetString2("Register");
			barButtonItemStartRegIncident.Caption = ResourceLoader.GetString2("New");
			ribbonPageGroupCallInfo.Text = ResourceLoader.GetString2("Incidents");
			ribbonPageGroupGeneralOptions.Text = ResourceLoader.GetString2("GeneralOptions");
			ribbonPage1.Text = ResourceLoader.GetString2("Cctv");
            this.Text = ResourceLoader.GetString2("DefaultCctvFrontClientFormDevX");
            layoutControlDefaultCCTVFrontClient.Text = ResourceLoader.GetString2("DefaultCctvFrontClientFormDevX");
			layoutControlGroup3.Text = ResourceLoader.GetString2("IncidentList");
			layoutControlGroup4.Text = ResourceLoader.GetString2("IncidentDetails");
			dockPanel1.Text = ResourceLoader.GetString2("HeaderRegIncidents");
            barButtonItemCancelTelemetryAlarm.Caption = ResourceLoader.GetString2("Dismiss");
            ribbonPageGroupAlarm.Text = ResourceLoader.GetString2("Alarms");
            //            this.Text = ResourceLoader.GetString2("CCTVModule");

        }
        private void FillCctvZones()
        {
            foreach (CctvZoneClientData cctvZone in
                ServerServiceClient.GetInstance().SearchClientObjects(typeof(CctvZoneClientData)))           
            {                
                this.cctvCallInformationControl1.CcvtZone = cctvZone;
            }
        }
       
        private void FillStructsType()
        {
            foreach (StructTypeClientData structData in
                    ServerServiceClient.GetInstance().SearchClientObjects(typeof(StructTypeClientData)))          
            {
                this.cctvCallInformationControl1.StructType = structData;
            }
        }
       
        private void questionsCctvControl1_removeIncident(IList selectedIncidentTypes)
        {

            IncidentTypeDepartmentTypeClientData department;
            IncidentTypeClientData incidentType;
            ArrayList updateIncident = new ArrayList();
            foreach (IncidentTypeClientData incident in selectedIncidentTypes)
            {
                if (GlobalCctvReport.IncidentTypesCodes != null)
                    GlobalCctvReport.IncidentTypesCodes.Remove(incident.Code);
                               
            }
            for (int j = 0; j < departmentsInvolvedControl.departmentTypesControl.gridViewExDepartmentType.RowCount; j++)
            {
                if (departmentsInvolvedControl.departmentTypesControl.gridViewExDepartmentType.GetRowCellValue(j,"Prioridad") == null)
                    departmentsInvolvedControl.departmentTypesControl.gridViewExDepartmentType.SetRowCellValue(j," ",false);
            }
            if (globalCctvReport.IncidentTypesCodes != null)
            {
                foreach (int incidentCode in GlobalCctvReport.IncidentTypesCodes)
                {
                    for (int k = 0; k < this.questionsCctvControl1.SelectedIncidentTypes.Count; k++)
                    {
                        incidentType = (IncidentTypeClientData)this.questionsCctvControl1.SelectedIncidentTypes[k];
                        if (incidentType.Code == incidentCode)
                            updateIncident.Add(incidentType);
                    }
                }
            }
            foreach (IncidentTypeClientData incident in updateIncident)
            {
                for (int i = 0; i < incident.IncidentTypeDepartmentTypeClients.Count; i++)
                {
                    department = (IncidentTypeDepartmentTypeClientData)incident.IncidentTypeDepartmentTypeClients[i];
                    for (int j = 0; j < departmentsInvolvedControl.departmentTypesControl.gridViewExDepartmentType.RowCount; j++)
                    {
                        if (department.DepartmentType.Code == (((GridDepartmentTypeData)((IList)departmentsInvolvedControl.departmentTypesControl.gridControlExDepartmentType.DataSource)[j]).Tag as DepartmentTypeClientData).Code)
                        {
                            departmentsInvolvedControl.departmentTypesControl.gridViewExDepartmentType.SetRowCellValue(j," ", true);
                        }
                    }
                }
            }
			this.questionsCctvControl1.UpdateTextBoxSelectedIncidentTypes(updateIncident);
        }
		private void gridViewEx1_SelectionWillChange(object sender, EventArgs e)
        {
           
			barButtonItemSave.Enabled = false;
			barButtonItemPrint.Enabled = false;
			barButtonItemRefresh.Enabled = false;           
           
            if (textBoxExIncidentdetails.IsDisposed == false)
                textBoxExIncidentdetails.DocumentText = "<span style='HEIGHT:100%;FONT-SIZE: 10pt; FONT-FAMILY: Sans-Serif'>" + ResourceLoader.GetString2("NoIncidentSelected") + "</span>";                        
   
            FormUtil.InvokeRequired(this,
               delegate
               {
                   this.layoutControlGroup4.Text = ResourceLoader.GetString2("IncidentDetails");
               });
        }
        private void OnRefreshIncidentReport()
        {
            refreshIncidentReport.Change(1000, Timeout.Infinite);
        }
        private void OnRefreshIncidentReportTimer(object state)
        {
            refreshIncidentReport.Change(Timeout.Infinite, Timeout.Infinite);

            ThreadPool.QueueUserWorkItem(delegate
            {
                try
                {
                    RefreshIncidentReport();
                }
                catch (Exception ex)
                {
                    SmartLogger.Print(ex);
                }
            });
        }
        private void OnAnswerUpdateTimer(object state)
        {
            answersUpdatedTimer.Change(Timeout.Infinite, Timeout.Infinite);          
        }              
       
        private void CctvFrontClientForm_KeyDown(object sender, KeyEventArgs e)
        {
            //SHORTCUTS...
            if (e.Control == true && !(e.Modifiers == (Keys.Control | Keys.Alt)))
            {
                if ((e.KeyValue == 49 || e.KeyCode == Keys.NumPad1) && this.cctvClientState != CctvClientStateEnum.WaitingForCctvIncident)//GOES TO STEP ONE(1)...
                {
                    ChangeCurrentStep(1, true);
                }
                else if ((e.KeyValue == 50 || e.KeyCode == Keys.NumPad2) && this.cctvClientState != CctvClientStateEnum.WaitingForCctvIncident)//GOES TO STEP TWO(2)...
                {
                    ChangeCurrentStep(2, true);
                }
                else if ((e.KeyValue == 51 || e.KeyCode == Keys.NumPad3) && this.cctvClientState != CctvClientStateEnum.WaitingForCctvIncident)//GOES TO STEP THREE(3)...
                {
                    ChangeCurrentStep(3, true);
                }
                else if ((e.KeyValue == 52 || e.KeyCode == Keys.NumPad4) && this.cctvClientState != CctvClientStateEnum.WaitingForCctvIncident)//GOES TO STEP FOUR(4)...
                {
                    ChangeCurrentStep(4, true);
                }
                else if (e.KeyCode == Keys.A && this.cctvClientState == CctvClientStateEnum.RegisteringCctvIncident)//TO CHECK OR UNCHECK THE IS ANONIMOUS CALL CHECBOX
                {
                              
                }
                else if ((e.KeyCode == Keys.Enter || e.KeyCode == Keys.Space))
                {
                    e.SuppressKeyPress = true;
                }
            }
           
            else if (e.KeyCode == Keys.F1)//CALLS ONLINE HELP...
            {
				((CctvFrontClientFormDevX)ParentForm).barButtonItemHelp_ItemClick(null, null);
            }
          
            else if (e.KeyCode == Keys.F6)//SETS THE SIMILAR INCIDENT FILTER...
            {
        
            }
            else if (e.KeyCode == Keys.F7)//SETS THE SAME NUMBER INCIDENT FILTER...
            {
                
            }
            else if (e.KeyCode == Keys.Enter && e.Shift == false && e.Control == false && e.Alt == false)
            {
                if (this.questionsCctvControl1.ContainsFocus && this.questionsCctvControl1.Active && !this.questionsCctvControl1.IsPanelIncidentTypeActive)
                {
                    this.questionsCctvControl1.SetNextQuestion();
                }
            }
            else if (e.KeyCode == Keys.F8)//SETS THE OPEN INCIDENTS FILTER...
            {
                
            }
            else if (e.KeyCode == Keys.F9)//CREATE A NEW INCIDENT WITH A NEW PHONE REPORTS...
            {
                if (CctvClientState != CctvClientStateEnum.WaitingForCctvIncident)
                    barButtonItemCreateNewIncident_ItemClick(null, null);
            }           
            else if (sender == null && e.Alt == true && !(e.Modifiers == (Keys.Control | Keys.Alt))
               && e.KeyCode == Keys.F4 && this.Disposing == false)
            {
                this.Close();
            }        
        }       
       
        public void callReceiverControl1_Enter(object sender, EventArgs e)
        {
            if (this.CctvClientState != CctvClientStateEnum.WaitingForCctvIncident)
            {
                ChangeCurrentStep(1, false);
            }
        }
       
        private void questionsCctvControl1_Enter(object sender, EventArgs e)
        {
            if (this.CctvClientState != CctvClientStateEnum.WaitingForCctvIncident)
            {
                ChangeCurrentStep(2, false);
            }
        }
        private void LoadStepImages()
        {
            this.inactiveStepImages = new ImageList();
            this.activeStepImages = new ImageList();
            string imageName = "$Image.";
            for (int i = 1; i < 5; i++)
            {
                Image image = ResourceLoader.GetImage(imageName + i + "Encendido");
                if (activeStepImages.ImageSize.Height != image.Size.Height ||
                    activeStepImages.ImageSize.Width != image.Size.Width)
                {
                    activeStepImages.ImageSize = image.Size;
                }
                activeStepImages.Images.Add(image);
            }
            for (int i = 1; i < 5; i++)
            {
                Image image = ResourceLoader.GetImage(imageName + i + "Apagado");
                if (inactiveStepImages.ImageSize.Height != image.Size.Height ||
                    inactiveStepImages.ImageSize.Width != image.Size.Width)
                {
                    inactiveStepImages.ImageSize = image.Size;
                }
                inactiveStepImages.Images.Add(image);
            }
        }
        private void departmentTypesControl2_DepartmentTypesSelected(object sender, EventArgs e)
        {
            DepartmentTypesControl control = (DepartmentTypesControl)sender;
            if(this.GlobalCctvReport != null)
				this.GlobalCctvReport.ReportBaseDepartmentTypesClient = control.SelectedDepartmentTypesWithPriority;
        }               
        private bool ValidateQuestions(RequirementTypeClientEnum requirementType)
        {
            //returns true if there are invalid questions...
            bool toReturn = false;
            if (GlobalCctvReport.IncidentTypesCodes != null)
            {
                int index = 0;
                while (toReturn != true && index < GlobalCctvReport.IncidentTypesCodes.Count)
                {
                    IncidentTypeClientData incidentType = globalIncidentTypes[(int)GlobalCctvReport.IncidentTypesCodes[index]];
                    foreach (IncidentTypeQuestionClientData incidentQuestion in incidentType.IncidentTypeQuestions)
                    {
                        if (incidentQuestion.Question != null)
                        {
                            if (incidentQuestion.Question.RequirementType == requirementType)
                            {
                                int questionIndex = 0;
                                bool found = false;
                                while ((questionIndex < this.questionsCctvControl1.QuestionsWithAnswers.Count) && (found != true))
                                {
                                    if (((ReportAnswerClientData)this.questionsCctvControl1.QuestionsWithAnswers[questionIndex]).QuestionCode.Equals(incidentQuestion.Question.Code) == true)
                                    {
                                        found = true;
                                    }
                                    questionIndex++;
                                }
                                if (found != true)
                                {
                                    toReturn = true;
                                }
                            }
                        }
                    }
                    index++;
                }
            }
            return toReturn;
        }   
        private bool ValidateRequiredQuestions()
        {
            return ValidateQuestions(RequirementTypeClientEnum.Required);
        }
        private bool ValidateIncidentTypes()
        {
            if (GlobalCctvReport.IncidentTypesCodes != null && GlobalCctvReport.IncidentTypesCodes.Count > 0)
                return false;
            else
                return true;
        }
        private bool ValidateDepartmentTypes()
        {
            bool toReturn = false;
            if (GlobalCctvReport.ReportBaseDepartmentTypesClient != null)
            {
                int index = 0;
                while (toReturn != true && index < GlobalCctvReport.ReportBaseDepartmentTypesClient.Count)
                {
                    ReportBaseDepartmentTypeClientData cctvReportDepartmentType =
                        GlobalCctvReport.ReportBaseDepartmentTypesClient[index] as ReportBaseDepartmentTypeClientData;
                    if (cctvReportDepartmentType.PriorityCode == 0)
                    {
                        toReturn = true;
                    }
                    index++;
                }
            }
            else
            {
                GlobalCctvReport.ReportBaseDepartmentTypesClient = new ArrayList();
            }
            return toReturn;
        }        
        private void UpdateQuestionsCctvControl1(ArrayList Changes)
        {
            if ((Changes != null) && (Changes.Count > 0))
            {
                if (Changes[0] is IncidentTypeClientData)
                {
                    this.questionsCctvControl1.UpdateIncidentTClientData(Changes);
                }
                else if (Changes[0] is QuestionClientData)
                {
                    this.questionsCctvControl1.UpdateIncidentQuestions(Changes);
                }
            }
        }
        private void UpdatedDepartmentInvolvedControl()
        {
            for (int i = 0; i < this.departmentsInvolvedControl.departmentTypesControl.gridViewExDepartmentType.RowCount; i++)
                if ((this.departmentsInvolvedControl.departmentTypesControl.gridViewExDepartmentType.GetRowCellValue(i,"Prioridad") == null)||
                    (this.departmentsInvolvedControl.departmentTypesControl.gridViewExDepartmentType.GetRowCellValue(i,"Prioridad").ToString() == string.Empty))
                    this.departmentsInvolvedControl.departmentTypesControl.gridViewExDepartmentType.SetRowCellValue(i," ",false);
        }        
        private bool ValidateInterface()
        {
            bool toReturn = true;
            
            if (ValidateChangedIncidentype() == false)
            {
                if (ValidateIncidentTypes() == false)
                {
                    DialogResult result = MessageForm.Show(ResourceLoader.GetString2("DeletedOrModifiedIncidentTipes"), MessageFormType.Question);
                    if (result == DialogResult.No)
                    {
                        popUpShow = false;
                        this.UpdateQuestionsCctvControl1(IncidentChanges);
                        UpdatedDepartmentInvolvedControl();
                        this.departmentsInvolvedControl.departmentTypesControl.IncidentTypes = IncidentChanges;
                        IncidentChanges.Clear();                      
                        toReturn = false;
                        this.questionsCctvControl1.SetFrontClientChangedIncidentTypeState();
                        this.questionsCctvControl1.Focus();
                    }
                }
                else
                {
                    MessageForm.Show(ResourceLoader.GetString2("DeletedIncidentTypes"), MessageFormType.Information);
                    toReturn = false;
                    this.questionsCctvControl1.SetFrontClientChangedIncidentTypeState();
                    this.questionsCctvControl1.Focus();
                }
            }
			  else if (ValidateChangedQuestions() == false)
            {
                DialogResult result = MessageForm.Show(ResourceLoader.GetString2("UpdatedRequiredQuestionsWishToContinue"), MessageFormType.Question);
                if (result == DialogResult.No)
                {
                    this.UpdateQuestionsCctvControl1(QuestionChanges);
                    QuestionChanges.Clear();
                    toReturn = false;
                    this.questionsCctvControl1.Focus();
                }
            }
            else if (ValidateRecommendedChangedQuestions() == false)
            {
                DialogResult result = MessageForm.Show(ResourceLoader.GetString2("UpdatedRequiredQuestions"), MessageFormType.Information);
                toReturn = false;
                this.questionsCctvControl1.PressedButtonOk();
                this.questionsCctvControl1.Focus();
            }
            else if (ValidateAddedQuestions() == false)
            {
                DialogResult result = MessageForm.Show(ResourceLoader.GetString2("NewRequiredQuestions"), MessageFormType.Information);
                toReturn = false;
                this.questionsCctvControl1.PressedButtonOk();
                this.questionsCctvControl1.Focus();
            }
            else if (ValidateCctvCallInformation() == false)
            {
                DialogResult result = MessageForm.Show(ResourceLoader.GetString2("CctvBadIncident"), MessageFormType.Error);
                toReturn = false;                
                this.cctvCallInformationControl1.Focus();
            }
			else if (ValidateDepartmentTypeChanges() == false)
			{
				if (ValidateDepartmentTypes() == false)
				{
					DialogResult dialog = MessageForm.Show(ResourceLoader.GetString2("DepartmentTypeUpdated"), MessageFormType.Question);
					if (dialog == DialogResult.No)
					{
						popUpShow = false;
						toReturn = false;
					}
					else if (GetSelectedDepartment().Count() == 0)
					{
						MessageForm.Show(ResourceLoader.GetString2("DepartmentTypeUpdatedCheckSelection"), MessageFormType.Error);
						popUpShow = false;
						toReturn = false;
					}
				}
			}
			if (toReturn == true)
            {
                if (ValidateIncidentTypes() == true)
                {
                    toReturn = false;
                    MessageForm.Show(ResourceLoader.GetString2("CctvReportWithoutIncidentType"), MessageFormType.Error);
                    this.questionsCctvControl1.SetFrontClientChangedIncidentTypeState();
                    this.questionsCctvControl1.Focus();
                    
                }
                else if (ValidateRequiredQuestions() == true)
                {
                    toReturn = false;
                    MessageForm.Show(ResourceLoader.GetString2("CctvRemainingNotAnsweredRequiredQuestions"), MessageFormType.Error);
                    this.questionsCctvControl1.Focus();
                    this.questionsCctvControl1.SelectNextNotAskedQuestion(new VistaButtonEx());
                }               
                else if (ValidateDepartmentTypes() == true)
                {
                    toReturn = false;
                    MessageForm.Show(ResourceLoader.GetString2("CctvDepartmentsWithoutPriority"), MessageFormType.Error);                   

                    this.departmentsInvolvedControl.departmentTypesControl.SetFocus("radioButtonExPriority1");
                }
				else if (ValidateCctvCallInformation() == false)
				{
					DialogResult result = MessageForm.Show(ResourceLoader.GetString2("CctvBadIncident"), MessageFormType.Error);
					toReturn = false;
					this.cctvCallInformationControl1.Focus();
				}              
            }
            if ((questionActionDelete.Count > 0) && (toReturn == true))
            {
                questionsCctvControl1.ShowPanelSelected();
            }
            return toReturn;
        }

		private bool ValidateDepartmentTypeChanges()
		{
			bool result = true;
			foreach (DepartmentTypeClientData item in DepartmentTypeChanges)
			{
				departmentsInvolvedControl.departmentTypesControl.DeleteDepartmentWithPriority(item);
				departmentsInvolvedControl.departmentTypesControl.gridControlExDepartmentType.BeginInit();
				departmentsInvolvedControl.departmentTypesControl.gridControlExDepartmentType.DeleteItem(new GridDepartmentTypeData(item));
				departmentsInvolvedControl.departmentTypesControl.gridControlExDepartmentType.EndInit();
				result = false;
			}
			DepartmentTypeChanges.Clear();
			return result;
		}

		private bool ValidateCctvCallInformation()
        {
            if ((cctvCallInformationControl1.CcvtZone == null) ||
                (cctvCallInformationControl1.StructType == null) ||
                (cctvCallInformationControl1.Struct == null) ||
                (cctvCallInformationControl1.Camera == null))
                return false;
            else
                return true;
        }

        private bool ValidateChangedQuestions()
        {
            bool validate = true;
            if (GlobalCctvReport.Answers != null)
            {
                for (int i = 0; i < GlobalCctvReport.Answers.Count; i++)
                {
                    ReportAnswerClientData pracd = GlobalCctvReport.Answers[i] as ReportAnswerClientData;
                    if (questionActionDelete.ContainsKey(pracd.QuestionCode) == true)
                    {
                        if (validate == true)
                            validate = false;

                        QuestionClientData deletedQuestion = questionActionDelete[pracd.QuestionCode];
                        GlobalCctvReport.Answers.RemoveAt(i);
                        questionsCctvControl1.DeleteQuestion(deletedQuestion);
                        i--;
                    }
                    else
                    {
                        if (questionActionsUpdate.ContainsKey(pracd.QuestionCode) == true)
                        {
                            QuestionClientData updatedQuestion = questionActionsUpdate[pracd.QuestionCode];
                         
                            if (validate == true)
                                validate = false;

                            questionsCctvControl1.UpdateQuestion(updatedQuestion);
                        }
                    }
                }
            }
            return validate;
        }

        private bool ValidateRecommendedChangedQuestions()
        {
            bool validate = true;
            foreach (QuestionClientData qcd in questionActionsUpdate.Values)
            {
                foreach (IncidentTypeQuestionClientData itqcd in qcd.IncidentTypes)
                {
                    for (int i = 0; i < questionsCctvControl1.SelectedIncidentTypes.Count; i++)
                    {
                        IncidentTypeClientData incidentTypeClientData = questionsCctvControl1.SelectedIncidentTypes[i] as IncidentTypeClientData;
                        if (incidentTypeClientData.Code == itqcd.IncidentType.Code)
                        {
                            if (validate == true)
                            {
                                validate = false;
                                break;
                            }

                            questionsCctvControl1.UpdateQuestion(qcd);
                        }
                    }
                    if (validate == false)
                        break;
                }
                if (validate == false)
                    break;
            }
            if (validate == false)
            {
                questionActionsUpdate.Clear();
            }            
            return validate;
        }

        private bool ValidateAddedQuestions()
        {
            bool validate = true;
            foreach (QuestionClientData qcd in questionActionAdd.Values)
            {
                foreach (IncidentTypeQuestionClientData itqcd in qcd.IncidentTypes)
                {
                    for (int i = 0; i < questionsCctvControl1.SelectedIncidentTypes.Count; i++)
                    {
                        IncidentTypeClientData incidentTypeClientData = questionsCctvControl1.SelectedIncidentTypes[i] as IncidentTypeClientData;
                        if (incidentTypeClientData.Code == itqcd.IncidentType.Code)
                        {
                            if (validate == true)
                            {
                                validate = false;
                                break;
                            }
                        }
                    }
                    if (validate == false)
                        break;
                }
                if (validate == false)
                    break;
            }
            if (validate == false)
                questionActionAdd.Clear();
            return validate;
        }

        private bool ValidateChangedIncidentype()
        {
            bool validate = true;
            for (int i = 0; i < questionsCctvControl1.SelectedIncidentTypes.Count; i++)
            {
                IncidentTypeClientData incidentTypeClientData = questionsCctvControl1.SelectedIncidentTypes[i] as IncidentTypeClientData;
                if (GlobalIncidentTypes.ContainsKey(incidentTypeClientData.Code) == false)
                {
                    if (validate == true)
                        validate = false;

                    questionsCctvControl1.DeleteIncidentType(incidentTypeClientData);
                    CleanIncidentTypeInformationFromGlobalCctvReport(incidentTypeClientData);
                }
                else
                {
                    IncidentTypeClientData updatedIncidentTypeClientData = GlobalIncidentTypes[incidentTypeClientData.Code] as IncidentTypeClientData;
                    if ((updatedIncidentTypeClientData.Version > incidentTypeClientData.Version) && (popUpShow == true))
                    {
                        if (validate == true)
                            validate = false;

                        questionsCctvControl1.UpdateIncidentType(updatedIncidentTypeClientData);
                    }
                }
            }
            return validate;
        }

        private void CleanIncidentTypeInformationFromGlobalCctvReport(IncidentTypeClientData incidentTypeClientData)
        {
            //If the IncidentType has been deleted while using it. Before we save the phone report we need to 
            // delete the incident report and its answers from it.

            GlobalCctvReport.IncidentTypesCodes.Remove(incidentTypeClientData.Code);
            for (int k = 0; GlobalCctvReport.Answers != null && k < GlobalCctvReport.Answers.Count; k++)
            {
                ReportAnswerClientData pracd = GlobalCctvReport.Answers[k] as ReportAnswerClientData;
                bool deleteAnswer = true;

                foreach (IncidentTypeClientData itcd in questionsCctvControl1.SelectedIncidentTypes)
                {
                    foreach (IncidentTypeQuestionClientData prqt in itcd.IncidentTypeQuestions)
                    {
                        if (prqt.Question.Code == pracd.QuestionCode)
                        {
                            deleteAnswer = false;
                            break;
                        }
                    }
                    if (deleteAnswer == false)
                        break;
                }

                if (deleteAnswer == true)
                {
                    GlobalCctvReport.Answers.RemoveAt(k);
                    k--;
                }
            }
        }
  
        private void ChangeCurrentStep(int stepNumber, bool needFocus)
        {
            switch (stepNumber)
            {
                case 0:                   
                    this.cctvCallInformationControl1.Active = false;
                    this.questionsCctvControl1.Active = false;
                    this.departmentsInvolvedControl.Active = false;                  
                    break;
                case 1:                    
                    this.cctvCallInformationControl1.Active = false;
                    if(needFocus == true)
                        this.cctvCallInformationControl1.Focus();
                    this.questionsCctvControl1.Active = false;
                    this.departmentsInvolvedControl.Active = false;                    
                    break;
                case 2:                  
                    this.cctvCallInformationControl1.Active = true;
                    this.questionsCctvControl1.Active = true;
                    if (needFocus == true)
                        this.questionsCctvControl1.Focus();
                    this.departmentsInvolvedControl.Active = false;                                        
                    break;
                case 3:                  
                    this.cctvCallInformationControl1.Active = false;
                    this.questionsCctvControl1.Active = false;
                    this.departmentsInvolvedControl.Active = true;
                    if(needFocus == true)
                        this.departmentsInvolvedControl.Focus();                                        
                    break;
                case 4:                   
                    this.cctvCallInformationControl1.Active = false;
                    this.questionsCctvControl1.Active = false;
                    this.departmentsInvolvedControl.Active = false;                                       
                    break;
            }
        }

        private void GenerateNewCctvReport(object sender)
        {
            //if (sender is GridControlDataAlarm)
            //{
            //    GridControlDataAlarm alarm = sender as GridControlDataAlarm;
            //    this.GlobalCctvReport = new CctvReportClientData();
            //    this.GlobalCctvReport.Camera = alarm.Camera;
            //    this.GlobalCctvReport.StartDate = alarm.DateTime;
            //    this.GlobalCctvReport.CustomCode = Guid.NewGuid().ToString();
            //    this.GlobalCctvReport.OperatorLogin = ServerServiceClient.OperatorClient.Login;

            //    cctvCallInformationControl1.ChangeSelectedCamera(alarm.Camera);
            //    cctvCallInformationControl1.Enabled = false;
            //    ChangeCurrentStep(2, true);
            //}
            //else
            if (sender is CameraClientData)
            {
                CameraClientData camera = sender as CameraClientData;

                this.GlobalCctvReport = new CctvReportClientData();
                camera.StructClientData.ZoneSecRef = camera.StructClientData.CctvZone.Name;
                this.GlobalCctvReport.Camera = camera;
                this.GlobalCctvReport.StartDate = DateTime.Now;
                this.GlobalCctvReport.CustomCode = Guid.NewGuid().ToString();
                this.GlobalCctvReport.OperatorLogin = ServerServiceClient.OperatorClient.Login;

                cctvCallInformationControl1.ChangeSelectedCamera(camera);
                //cctvCallInformationControl1.Enabled = false;
                ChangeCurrentStep(2, true);
            }
            else
            {
                this.GlobalCctvReport = new CctvReportClientData();
                this.GlobalCctvReport.CustomCode = Guid.NewGuid().ToString();
                this.GlobalCctvReport.OperatorLogin = ServerServiceClient.OperatorClient.Login;
                cctvCallInformationControl1.Enabled = true;

                ChangeCurrentStep(1, true);
            }
        }

        private void GenerateNewCctvReportAlarm(object sender)
        {
            //if (sender is GridControlDataAlarm)
            //{
            //    GridControlDataAlarm alarm = sender as GridControlDataAlarm;
            //    this.GlobalCctvReport = new CctvReportClientData();
            //    this.GlobalCctvReport.Camera = alarm.Camera;
            //    this.GlobalCctvReport.StartDate = alarm.DateTime;
            //    this.GlobalCctvReport.CustomCode = Guid.NewGuid().ToString();
            //    this.GlobalCctvReport.OperatorLogin = ServerServiceClient.OperatorClient.Login;

            //    cctvCallInformationControl1.ChangeSelectedCamera(alarm.Camera);
            //    cctvCallInformationControl1.Enabled = false;
            //    ChangeCurrentStep(2, true);
            //}
            //else
            if (sender is GridCctvAlertCamData)
            {
                this.AlarmDataSelect = sender as GridCctvAlertCamData;
                VAAlertCamClientData report = ((VAAlertCamClientData)AlarmDataSelect.Tag);
                IList userCameras = ServerServiceClient.GetInstance().SearchClientObjects(
                SmartCadHqls.GetCustomHql(SmartCadHqls.GetCamerasByName, AlarmDataSelect.Camera));
                CameraClientData cameraData = userCameras[0] as CameraClientData;
                CameraClientDataAlarm camera = new CameraClientDataAlarm();
                camera.Ip = report.Camera.Ip;
                camera.IpServer = report.Camera.IpServer;
                camera.Login = report.Camera.Login;
                camera.Name = report.Camera.Name;
                camera.Port = report.Camera.Port;
                camera.StructClientData = cameraData.StructClientData;
                camera.Type = report.Camera.Type;
                camera.StructClientData.ZoneSecRef = report.Camera.StructClientData.ZoneSecRef;
                camera.Version = report.Camera.Version;
                camera.Code = report.Camera.Code;
                camera.ConnectionType = report.Camera.ConnectionType;
                camera.CustomId = report.Camera.CustomId;
                camera.AlarmDate = report.AlertDate;
                camera.AlarmRule = report.Rule.Name;
                //CameraClientDataAlarm camera = sender as CameraClientDataAlarm;

                this.GlobalCctvReport = new CctvReportClientData();

                this.GlobalCctvReport.Camera = camera;
                this.GlobalCctvReport.StartDate = report.AlertDate;
                this.GlobalCctvReport.CustomCode = Guid.NewGuid().ToString();
                this.GlobalCctvReport.OperatorLogin = ServerServiceClient.OperatorClient.Login;
                

                cctvCallInformationControl1.ChangeSelectedCameraAlarm(camera);
                //cctvCallInformationControl1.Enabled = false;
                ChangeCurrentStep(2, true);
            }
            else
            {
                this.GlobalCctvReport = new CctvReportClientData();
                this.GlobalCctvReport.CustomCode = Guid.NewGuid().ToString();
                this.GlobalCctvReport.OperatorLogin = ServerServiceClient.OperatorClient.Login;
                cctvCallInformationControl1.Enabled = true;

                ChangeCurrentStep(1, true);
            }
        }

        private void questionsCctvControl1_IncidentTypeSelected(object sender, EventArgs e)
        {
            QuestionsControlDevX questionCtrl = (QuestionsControlDevX)sender;
            IList selectedIncidentTypes = questionCtrl.SelectedIncidentTypes;
            if (HasIncidentTypesListChanged(selectedIncidentTypes) == true)
            {
                this.GlobalCctvReport.IncidentTypesCodes = new ArrayList();
                foreach (IncidentTypeClientData incidentType in selectedIncidentTypes)
                    this.GlobalCctvReport.IncidentTypesCodes.Add(incidentType.Code);
                if (selectedIncidentTypes.Count == 1 && (selectedIncidentTypes[0] as IncidentTypeClientData) != null &&
                    (selectedIncidentTypes[0] as IncidentTypeClientData).NoEmergency == true)
                    this.departmentsInvolvedControl.departmentTypesControl.CleanControl();
                else
                    this.departmentsInvolvedControl.departmentTypesControl.IncidentTypes = selectedIncidentTypes;
            }                   
            questionActionAdd.Clear();
            questionActionsUpdate.Clear();
            questionActionDelete.Clear();
        }

        private bool HasIncidentTypesListChanged(IList newList)
        {
            bool result = false;
            if (GlobalCctvReport.IncidentTypesCodes != null)
            {
                if (newList.Count != globalCctvReport.IncidentTypesCodes.Count)
                    result = true;
                else if (result == false)
                {
                    for (int i = 0; i < newList.Count && result == false; i++)
                    {
                        IncidentTypeClientData newIncidentType = newList[i] as IncidentTypeClientData;
                        if (GlobalCctvReport.IncidentTypesCodes.Contains(newIncidentType.Code) == false)
                            result = true;
                    }
                }
            }
            else
                result = true;

            return result;
        }
        
		public void serverService_CommittedChanges(object sender, CommittedObjectDataCollectionEventArgs args)
        {
            try
            {
                if (args != null && args.Objects != null && args.Objects.Count > 0)
                {
                    if (args.Objects[0] is IncidentClientData)//if it's an incident...
                    {
                        if (((IncidentClientData)args.Objects[0]).Status.Name.Equals(IncidentStatusClientData.Closed.Name) == false
                            && args.Action != CommittedDataAction.Delete)
                        {
                            SmartCadContext context = args.Context as SmartCadContext;
                            bool select = false;
                            if (context != null)
                            {
                                if (ServerServiceClient.GetInstance().OperatorClient != null &&
                                    ServerServiceClient.GetInstance().OperatorClient.Login == context.Operator)
                                {
                                    select = true;
                                }
                            }
                            gridControlExIncidentList.AddOrUpdateItem(new GridIncidentCctvData(args.Objects[0] as IncidentClientData), select);
                            
                            this.GlobalIncidents.Add(args.Objects[0] as IncidentClientData);
						}
						else if (((IncidentClientData)args.Objects[0]).Status.Name.Equals(IncidentStatusClientData.Closed.Name))
						{
							gridControlExIncidentList.DeleteItem(new GridIncidentCctvData(args.Objects[0] as IncidentClientData));
							globalIncidents.Remove(args.Objects[0] as IncidentClientData);
						}
                    }
                    else if (args.Objects[0] is CctvReportClientData)// if it's a phone report... gotta check if it is already added...
                    {
						IncidentClientData incidentData = new IncidentClientData();//null;
                        CctvReportClientData cctvReport = args.Objects[0] as CctvReportClientData;
						int index = ((IList)gridControlExIncidentList.DataSource).IndexOf(new GridIncidentCctvData(incidentData));
						
						if(index>=0)
                        {
							incidentData = (IncidentClientData)((GridIncidentCctvData)((IList)gridControlExIncidentList.DataSource)[index]).Tag;
                            bool found = false;
                            for (int i = 0; i < incidentData.CctvReports.Count && found != true; i++)
                            {
                                CctvReportClientData cctvReportTemp = incidentData.CctvReports[i] as CctvReportClientData;
                                if (cctvReportTemp.CustomCode == cctvReport.CustomCode)
                                {
                                    found = true;
                                    cctvReportTemp = cctvReport;
                                }
                            }
                            if (found == false)
                            {
                                foreach (int incidentTypeCode in cctvReport.IncidentTypesCodes)
                                {
                                    bool alreadyAdded = false;
                                    IncidentTypeClientData itcd = GlobalIncidentTypes[incidentTypeCode];
                                    foreach (IncidentTypeClientData incidentType in incidentData.IncidentTypes)
                                    {
                                        if (incidentType.Code == itcd.Code)
                                            alreadyAdded = true;
                                    }

                                    if (alreadyAdded == false)
                                        incidentData.IncidentTypes.Add(itcd);
                                }
                                incidentData.CctvReports.Add(cctvReport);
                                incidentSyncBox.Sync(new GridIncidentCctvData(incidentData), args.Action);
                            }
                        }
                    }
                    else if (args.Objects[0] is IncidentTypeClientData)
                    {
                        IncidentTypeClientData incidentTypeClientData = args.Objects[0] as IncidentTypeClientData;
                        if (args.Action == CommittedDataAction.Save)
                        {
                            if (incidentTypeClientData.NoEmergency == false)
                            {
                                AddIncindetType(incidentTypeClientData);
                            }
                        }
                        else if (args.Action == CommittedDataAction.Update)
                        {
                            popUpShow = true;

                            UpdateIncidentType(incidentTypeClientData);
                            for (int i = 0; i < IncidentChanges.Count; i++)
                            {
                                if ((IncidentChanges[i] as IncidentTypeClientData).Code == incidentTypeClientData.Code)
                                    IncidentChanges.RemoveAt(i);
                            }
                            IncidentChanges.Add(incidentTypeClientData);
                        }
                        else if (args.Action == CommittedDataAction.Delete)
                        {
                            DeleteIncidentType(incidentTypeClientData);
                        }
                        //Actualizar tipod de incident en datagrid.
                        FormUtil.InvokeRequired(this, delegate
                        {
                            gridControlExIncidentList.BeginUpdate();

                            if (gridControlExIncidentList.DataSource != null)
                            {
                                ArrayList list = new ArrayList(gridControlExIncidentList.DataSource as IList);
                                foreach (GridIncidentCctvData item in list)
                                {
                                    IncidentClientData icd = item.Tag as IncidentClientData;
                                    for (int i = 0; i < icd.IncidentTypes.Count; i++)
                                    {
                                        IncidentTypeClientData itcd = icd.IncidentTypes[i] as IncidentTypeClientData;
                                        if (itcd.Code == incidentTypeClientData.Code)
                                        {
                                            icd.IncidentTypes[i] = incidentTypeClientData;
                                            int index = ((IList)gridControlExIncidentList.DataSource).IndexOf(new GridIncidentCctvData(icd));
                                            if (index >= 0)
                                                ((IList)gridControlExIncidentList.DataSource)[index] = new GridIncidentCctvData(icd);
                                            break;
                                        }
                                    }
                                }
                            }

                            gridControlExIncidentList.EndUpdate();

                        });
                    }
                    else if (args.Objects[0] is QuestionClientData)
                    {
                        QuestionClientData questionClientDara = args.Objects[0] as QuestionClientData;
                        if (args.Action == CommittedDataAction.Save)
                        {
                            AddQuestion(questionClientDara);
                        }
                        else if (args.Action == CommittedDataAction.Update)
                        {
                            UpdateQuestion(questionClientDara);
                            for (int i = 0; i < QuestionChanges.Count; i++)
                            {
                                if ((QuestionChanges[i] as QuestionClientData).Code == questionClientDara.Code)
                                    QuestionChanges.RemoveAt(i);
                            }
                            QuestionChanges.Add(questionClientDara);
                        }
                        else if (args.Action == CommittedDataAction.Delete)
                        {
                            DeleteQuestion(questionClientDara);
                        }
                    }
                    else if (args.Objects[0] is CameraClientData)
                    {

                        FormUtil.InvokeRequired(this, delegate
                        {
                            CameraClientData camera = (CameraClientData)args.Objects[0];

                            if (args.Action == CommittedDataAction.Update)
                            {
                                cctvCallInformationControl1.UpdateComboBoxItem(camera);
                            }
                            else if (args.Action == CommittedDataAction.Delete)
                            {
                                cctvCallInformationControl1.DeleteComboBoxItem(camera);
                            }

                        });

                    }
                    else if (args.Objects[0] is StructClientData)
                    {
                        FormUtil.InvokeRequired(this, delegate
                        {
                            StructClientData structClient = (StructClientData)args.Objects[0];

                            if (args.Action == CommittedDataAction.Save)
                            {
                                if (cctvCallInformationControl1.CcvtZone != null && structClient.CctvZone.Code == ((CctvZoneClientData)cctvCallInformationControl1.CcvtZone).Code)
                                {
                                    cctvCallInformationControl1.AddComboBoxItem(structClient);
                                }
                            }
                            else if (args.Action == CommittedDataAction.Update)
                            {
                                cctvCallInformationControl1.UpdateComboBoxItem(structClient);
                            }
                            else if (args.Action == CommittedDataAction.Delete)
                            {
                                cctvCallInformationControl1.DeleteComboBoxItem(structClient);
                            }

                        });
                    }
                    else if (args.Objects[0] is CctvZoneClientData)
                    {
                        FormUtil.InvokeRequired(this, delegate
                        {
                            CctvZoneClientData cctvZone = (CctvZoneClientData)args.Objects[0];

                            if (args.Action == CommittedDataAction.Save)
                            {
                                cctvCallInformationControl1.AddComboBoxItem(cctvZone);

                            }
                            else if (args.Action == CommittedDataAction.Update)
                            {
                                cctvCallInformationControl1.UpdateComboBoxItem(cctvZone);
                            }
                            else if (args.Action == CommittedDataAction.Delete)
                            {
                                cctvCallInformationControl1.DeleteComboBoxItem(cctvZone);
                            }

                        });
                    }
                    else if (args.Objects[0] is ApplicationPreferenceClientData)
                    {
                        ApplicationPreferenceClientData preference = args.Objects[0] as ApplicationPreferenceClientData;
                        if (args.Action == CommittedDataAction.Update)
                        {
                            if (globalApplicationPreference.ContainsKey(preference.Name))
                            {
                                globalApplicationPreference[preference.Name] = preference;
                            }
                            else
                            {
                                globalApplicationPreference.Add(preference.Name, preference);
                            }
                        }
                    }
                    else if (args.Objects[0] is DepartmentTypeClientData)
                    {
                        #region  DepartmentTypeClientData
                        if (args.Action == CommittedDataAction.Delete)
                        {
                            DepartmentTypeClientData data =(DepartmentTypeClientData)args.Objects[0];
							GridDepartmentTypeData itemData = new GridDepartmentTypeData(data);
							if (GetSelectedDepartment().Contains(itemData))
								DepartmentTypeChanges.Add(data);
							else
							{
								FormUtil.InvokeRequired(this,
									  delegate
									  {
										  departmentsInvolvedControl.departmentTypesControl.gridControlExDepartmentType.BeginInit();
										  departmentsInvolvedControl.departmentTypesControl.gridControlExDepartmentType.DeleteItem(itemData);
										  departmentsInvolvedControl.departmentTypesControl.gridControlExDepartmentType.EndInit();
									  });
							}
                        }
                        else if (args.Action == CommittedDataAction.Save || args.Action == CommittedDataAction.Update)
                        {
                            GridDepartmentTypeData data = new GridDepartmentTypeData((DepartmentTypeClientData)args.Objects[0]);
                            FormUtil.InvokeRequired(this,
                                delegate
                                {
                                    departmentsInvolvedControl.departmentTypesControl.gridControlExDepartmentType.BeginInit();
                                    departmentsInvolvedControl.departmentTypesControl.gridControlExDepartmentType.AddOrUpdateItem(data);
                                    departmentsInvolvedControl.departmentTypesControl.gridControlExDepartmentType.EndInit();
                                });
                        }
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

		private IEnumerable<GridDepartmentTypeData> GetSelectedDepartment()
		{
			return departmentsInvolvedControl.departmentTypesControl.gridControlExDepartmentType.Items.Cast<GridDepartmentTypeData>().Where(
									dep => dep.DepartmentTypeCheck);
		}
               
        #region IncidentType Actions
        private void DeleteIncidentType(IncidentTypeClientData incidentTypeClientData)
        {
            if (GlobalIncidents != null && GlobalIncidentTypes.ContainsKey(incidentTypeClientData.Code) == true)
            {
                GlobalIncidentTypes.Remove(incidentTypeClientData.Code);
                if (cctvClientState == CctvClientStateEnum.WaitingForCctvIncident)
                {
                    FormUtil.InvokeRequired(this, delegate
                    {
                        CleanForm(false);
                    });
                }
            }
            else if (incidentTypeActionDelete.ContainsKey(incidentTypeClientData.Code) == false)
            {
                incidentTypeActionDelete.Add(incidentTypeClientData.Code, incidentTypeClientData);
            }
        }
        private void UpdateIncidentType(IncidentTypeClientData incidentTypeClientData)
        {
            if (GlobalIncidents != null && GlobalIncidentTypes.ContainsKey(incidentTypeClientData.Code) == true)
            {
                GlobalIncidentTypes[incidentTypeClientData.Code] = incidentTypeClientData;
                if (cctvClientState == CctvClientStateEnum.WaitingForCctvIncident && callEntering == false)
                {
                    FormUtil.InvokeRequired(this, delegate
                    {
                        CleanForm(false);
                    });
                }
            }
            else
            {
                if (incidentTypeActionsUpdate.ContainsKey(incidentTypeClientData.Code) == true)
                {
                    incidentTypeActionsUpdate[incidentTypeClientData.Code] = incidentTypeClientData;
                }
                else
                {
                    incidentTypeActionsUpdate.Add(incidentTypeClientData.Code, incidentTypeClientData);
                }
            }
        }
        private void AddIncindetType(IncidentTypeClientData incidentTypeClientData)
        {
            if (GlobalIncidents != null)
                if (incidentTypeClientData.NoEmergency == false)
                {
                    GlobalIncidentTypes.Add(incidentTypeClientData.Code, incidentTypeClientData);
                }
                //GlobalIncidentTypes.Add(incidentTypeClientData.Code, incidentTypeClientData);
            if (cctvClientState == CctvClientStateEnum.WaitingForCctvIncident && callEntering == false)
            {
                FormUtil.InvokeRequired(this, delegate
                {
                    CleanForm(false);
                });
            }
            else
            {
                if (incidentTypeActionAdd.ContainsKey(incidentTypeClientData.Code) == false)
                {
                    incidentTypeActionAdd.Add(incidentTypeClientData.Code, incidentTypeClientData);
                }
            }
        }
        #endregion

        #region Question Actions
        private void DeleteQuestion(QuestionClientData questionClientData)
        {
            if (GlobalIncidents != null)
            {
                foreach (IncidentTypeQuestionClientData itqcd in questionClientData.IncidentTypes)
                {
                    if (GlobalIncidentTypes.ContainsKey(itqcd.IncidentType.Code) == true)
                    {
                        IncidentTypeClientData itcd = GlobalIncidentTypes[itqcd.IncidentType.Code] as IncidentTypeClientData;
                        for (int i = 0; i < itcd.IncidentTypeQuestions.Count; i++)
                        {
                            IncidentTypeQuestionClientData itqcd2 = itcd.IncidentTypeQuestions[i] as IncidentTypeQuestionClientData;
                            if (itqcd2.Code == itqcd.Code)
                            {
                                itcd.IncidentTypeQuestions.RemoveAt(i);
                                i--;
                            }
                        }
                    }
                }
            }
            if (cctvClientState == CctvClientStateEnum.WaitingForCctvIncident)
            {
                FormUtil.InvokeRequired(this, delegate
                {
                    CleanForm(false);
                });
            }
            else
            {
                if (questionActionDelete.ContainsKey(questionClientData.Code) == false)
                {
                    questionActionDelete.Add(questionClientData.Code, questionClientData);
                }
            }
        }
        private void UpdateQuestion(QuestionClientData questionClientData)
        {
            if (GlobalIncidents != null)
            {
                foreach (IncidentTypeClientData itcd in GlobalIncidentTypes.Values)
                {
                    for (int i = 0; i < itcd.IncidentTypeQuestions.Count; i++)
                    {
                        IncidentTypeQuestionClientData itqcd = itcd.IncidentTypeQuestions[i] as IncidentTypeQuestionClientData;
                        if (itqcd.Question.Code == questionClientData.Code)
                        {
                            itcd.IncidentTypeQuestions.RemoveAt(i);
                            i--;
                        }
                    }
                }
                foreach (IncidentTypeQuestionClientData itqcd in questionClientData.IncidentTypes)
                {
                    if (GlobalIncidentTypes.ContainsKey(itqcd.IncidentType.Code) == true)
                    {
                        IncidentTypeClientData itcd = GlobalIncidentTypes[itqcd.IncidentType.Code] as IncidentTypeClientData;
                        itqcd.Question = questionClientData;
                        itcd.IncidentTypeQuestions.Add(itqcd);
                    }
                }
            }
            if (cctvClientState == CctvClientStateEnum.WaitingForCctvIncident && callEntering == false)
            {
                FormUtil.InvokeRequired(this, delegate
                {
                     CleanForm(false);
                });
            }
            else
            {
                if (questionClientData.RequirementType == RequirementTypeClientEnum.Recommended && 
                    questionActionsUpdate.ContainsKey(questionClientData.Code) == false)
                {
                    questionActionsUpdate.Add(questionClientData.Code, questionClientData);
                }
                else
                {
                    if (questionClientData.RequirementType == RequirementTypeClientEnum.Recommended &&
                        questionActionsUpdate[questionClientData.Code] != null)
                        questionActionsUpdate[questionClientData.Code] = questionClientData;
                }
            }

            if (questionActionAdd.ContainsKey(questionClientData.Code))
            {
                questionActionAdd[questionClientData.Code] = questionClientData;
                questionActionsUpdate.Remove(questionClientData.Code);
            }
        }
        private void AddQuestion(QuestionClientData questionClientData)
        {
            foreach (IncidentTypeQuestionClientData itqcd in questionClientData.IncidentTypes)
            {
                if (GlobalIncidents != null && GlobalIncidentTypes.ContainsKey(itqcd.IncidentType.Code) == true)
                {
                    IncidentTypeClientData itcd = GlobalIncidentTypes[itqcd.IncidentType.Code] as IncidentTypeClientData;
                    itqcd.Question = questionClientData;
                    itcd.IncidentTypeQuestions.Add(itqcd);
                }
            }
            if (cctvClientState == CctvClientStateEnum.WaitingForCctvIncident && callEntering == false)
            {
                FormUtil.InvokeRequired(this, delegate
                {
                    CleanForm(false);
                });
            }
            else
            {
                if (questionClientData.RequirementType == RequirementTypeClientEnum.Recommended &&
                    questionActionAdd.ContainsKey(questionClientData.Code) == false)
                {
                    questionActionAdd.Add(questionClientData.Code, questionClientData);
                }
                else
                {
                    if (questionClientData.RequirementType == RequirementTypeClientEnum.Recommended &&
                        questionActionAdd[questionClientData.Code] != null)
                        questionActionAdd[questionClientData.Code] = questionClientData;
                }
            }
        }

        private void questionsControl_AnswerUpdated(object sender, EventArgs e)
        {
            if (CctvClientState != CctvClientStateEnum.RegisteringCctvIncident)
            {
                updatedAnswers = new ArrayList(questionsCctvControl1.QuestionsWithAnswers);
                foreach (ReportAnswerClientData cctvReportAnswer in updatedAnswers)
                {
                    cctvReportAnswer.ReportCode = GlobalCctvReport.Code;
                }
            }
            else
            {
                this.GlobalCctvReport.Answers = new ArrayList(questionsCctvControl1.QuestionsWithAnswers);
                foreach (ReportAnswerClientData cctvReportAnswer in GlobalCctvReport.Answers)
                {
                    cctvReportAnswer.ReportCode = GlobalCctvReport.Code;
                }
            }

            answersUpdatedTimerEventArgs = e;
            answersUpdatedTimer.Change(1000, Timeout.Infinite);
        }
        #endregion
               
        private IList CheckCctvReportUpdates(IList newAnswers)
        {
            //No borrar, se utilizara despues.
            IList answersToAdd = new ArrayList();

            if (GlobalCctvReport != null)
            {
                if (newAnswers != null)
                {
                    foreach (ReportAnswerClientData cctvReportAnswer in newAnswers)
                    {
                        bool found = false;
                        int i = 0;
                        while (found == false && i < GlobalCctvReport.Answers.Count)
                        {
                            ReportAnswerClientData tempCctvReportAnswer = GlobalCctvReport.Answers[i] as ReportAnswerClientData;
                            if (tempCctvReportAnswer.QuestionCode == cctvReportAnswer.QuestionCode)
                            {
                                found = true;
                            }
                            i++;
                        }
                        if (found == false)
                        {
                            cctvReportAnswer.ReportCode = GlobalCctvReport.Code;
                            answersToAdd.Add(cctvReportAnswer);
                        }
                    }
                }
            }
            return answersToAdd;
        }

        private bool IsEmergencyIncident(CctvReportClientData newCctvReport)
        {
            bool result = true;
            if (newCctvReport.IncidentTypesCodes.Count == 1)
            {
                IncidentTypeClientData incidentType = globalIncidentTypes[(int)newCctvReport.IncidentTypesCodes[0]];
                if(incidentType.NoEmergency == true)
                    result = false;
            }
            return result;
        }

        private void GenerateNewIncident()
        {
            IncidentClientData incident = new IncidentClientData();
            incident.SourceIncidentApp = SourceIncidentAppEnum.Cctv;
            incident.CctvReports = new ArrayList();
            incident.StartDate = DateTime.Now;
            incident.EndDate = DateTime.Now;
			ApplicationPreferenceClientData prefix = globalApplicationPreference["CCTVIncidentCodePrefix"];
			ApplicationPreferenceClientData isSuffix = globalApplicationPreference["IsIncidentCodeSuffix"];
			incident.CustomCode = serverServiceClient.OperatorClient.Code + serverServiceClient.GetTime().ToString("yyMMddHHmmss");
			if(isSuffix.Value.Equals(false.ToString()))
				incident.CustomCode = prefix.Value +  incident.CustomCode;
			else
				incident.CustomCode+=prefix.Value;
            incident.Address = new AddressClientData(
                    (cctvCallInformationControl1.Struct as StructClientData).ZoneSecRef,
                    (cctvCallInformationControl1.Struct as StructClientData).Street,
                    null, null, true);
           
            incident.Address.State = (cctvCallInformationControl1.Struct as StructClientData).State;
            incident.Address.Town = (cctvCallInformationControl1.Struct as StructClientData).Town;
            incident.Address.Lat = (cctvCallInformationControl1.Struct as StructClientData).Lat;
            incident.Address.Lon = (cctvCallInformationControl1.Struct as StructClientData).Lon;            
            incident.IsEmergency = IsEmergencyIncident(GlobalCctvReport);            
            if (incident.IsEmergency == true)
                incident.Status = IncidentStatusClientData.Open;
            else
                incident.Status = IncidentStatusClientData.Closed;                     
            foreach (ReportBaseDepartmentTypeClientData reportBaseDepartmentType in
                this.GlobalCctvReport.ReportBaseDepartmentTypesClient)
            {
                reportBaseDepartmentType.ReportBaseCode = this.GlobalCctvReport.Code;
            }
            if (GlobalCctvReport.ReportBaseDepartmentTypesClient == null || GlobalCctvReport.ReportBaseDepartmentTypesClient.Count == 0)
                incident.Status = IncidentStatusClientData.Closed;            
            //GlobalCctvReport.CctvReportCameraClient.Address = incident.Address;
            GlobalCctvReport.Camera = 
                (cctvCallInformationControl1.Camera as CameraClientData);

            this.GlobalCctvReport.IncidentCode = incident.Code;
            if (GlobalCctvReport.Answers == null)
                GlobalCctvReport.Answers = new ArrayList();
            incident.CctvReports.Add(this.GlobalCctvReport);
            ServerServiceClient.GetInstance().SaveOrUpdateClientData(incident);
            updatedAnswers.Clear();
            EndRecordNewIncident();
            
        }

        private void GenerateNewIncidentAlarm()
        {
            IncidentClientData incident = new IncidentClientData();
            incident.SourceIncidentApp = SourceIncidentAppEnum.Cctv;
            incident.CctvReports = new ArrayList();
            incident.StartDate = (cctvCallInformationControl1.Camera as CameraClientDataAlarm).AlarmDate;
            incident.EndDate = (cctvCallInformationControl1.Camera as CameraClientDataAlarm).AlarmDate;
            
            //ApplicationPreferenceClientData prefix = globalApplicationPreference["CCTVIncidentCodePrefix"];
            ApplicationPreferenceClientData isSuffix = globalApplicationPreference["IsIncidentCodeSuffix"];
            ApplicationPreferenceClientData prefixVA = globalApplicationPreference["VACodePrefix"];
            incident.CustomCode = serverServiceClient.OperatorClient.Code + serverServiceClient.GetTime().ToString("yyMMddHHmmss");
            if (isSuffix.Value.Equals(false.ToString()))
                //Lo que esta comentado se va a descomentar cuando en la base de datos ya este creado el nuevo prefix que se va a utilizar
                //URDANETA
                incident.CustomCode = prefixVA.Value + incident.CustomCode;
            else
                incident.CustomCode += prefixVA.Value;
            incident.Address = new AddressClientData(
                    (cctvCallInformationControl1.Struct as StructClientData).ZoneSecRef,
                    (cctvCallInformationControl1.Struct as StructClientData).Street,
                    null, null, true);

            incident.Address.State = (cctvCallInformationControl1.Struct as StructClientData).State;
            incident.Address.Town = (cctvCallInformationControl1.Struct as StructClientData).Town;
            incident.Address.Lat = (cctvCallInformationControl1.Struct as StructClientData).Lat;
            incident.Address.Lon = (cctvCallInformationControl1.Struct as StructClientData).Lon;
            incident.IsEmergency = IsEmergencyIncident(GlobalCctvReport);
            if (incident.IsEmergency == true)
                incident.Status = IncidentStatusClientData.Open;
            else
                incident.Status = IncidentStatusClientData.Closed;
            foreach (ReportBaseDepartmentTypeClientData reportBaseDepartmentType in
                this.GlobalCctvReport.ReportBaseDepartmentTypesClient)
            {
                reportBaseDepartmentType.ReportBaseCode = this.GlobalCctvReport.Code;
            }
            if (GlobalCctvReport.ReportBaseDepartmentTypesClient == null || GlobalCctvReport.ReportBaseDepartmentTypesClient.Count == 0)
                incident.Status = IncidentStatusClientData.Closed;
            //GlobalCctvReport.CctvReportCameraClient.Address = incident.Address;
            GlobalCctvReport.Camera = (cctvCallInformationControl1.Camera as CameraClientData);
            this.GlobalCctvReport.IncidentCode = incident.Code;
            if (GlobalCctvReport.Answers == null)
                GlobalCctvReport.Answers = new ArrayList();
            incident.CctvReports.Add(this.GlobalCctvReport);
            ServerServiceClient.GetInstance().SaveOrUpdateClientData(incident);
            updatedAnswers.Clear();
            EndRecordNewIncident();

        }

        private void EndRecordNewIncident()
        {
            //CHECKING FOR UNSAVED CHANGES IN PHONEREPORT...
            if (updatedAnswers.Count > 0)
            {
                if (EnsureUpdateCctvReport() == true)
                {
                    GlobalCctvReport.Answers = updatedAnswers;
                }
            }
            if (ValidateChangedQuestions() == false)
            {
                DialogResult dialogResult = MessageForm.Show(ResourceLoader.GetString2("QuestionsWereDeleted"), MessageFormType.Information);
            }           
            ServerServiceClient.GetInstance().FinalizeCctvIncident(GlobalCctvReport);          
            this.CctvClientState = CctvClientStateEnum.WaitingForCctvIncident;
            //SEND CLEAR TO MAPS
            ApplicationServiceClient.Current(UserApplicationClientData.Map).InsertAddressDataInGis(
                new AddressClientData(string.Empty, string.Empty, string.Empty, string.Empty, false));
            questionActionsUpdate.Clear();
            questionActionDelete.Clear();
            questionActionAdd.Clear();
            CleanForm();         
        }  

        private void FillIncidentDetails(SelectedIncidentTaskResult selectedIncidentTask, bool sameIncident)
        {
            try
            {
                string strXml = ApplicationUtil.BuildHtml(selectedIncidentTask.Xml, xslCompiledTransform, "Cctvincident");
                strXml = ApplicationUtil.RecoverBlanks(strXml);

                FileStream fs = File.Open("incident_cctv.html", FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite);
                byte[] byteXML = Encoding.Unicode.GetBytes(strXml);
                fs.Write(byteXML, 0, byteXML.Length);
                fs.Flush();
                fs.Close();
                FormUtil.InvokeRequired(this,
               delegate
               {
                   this.layoutControlGroup4.Text =  ResourceLoader.GetString2("IncidentDetails");
                   this.layoutControlGroup4.Text += ResourceLoader.GetString2("UpdatedAt");
                   this.layoutControlGroup4.Text += serverServiceClient.GetTime().ToString("G", ApplicationUtil.GetCurrentCulture());
               });
             
                FormUtil.InvokeRequired(textBoxExIncidentdetails, delegate
                {
                    textBoxExIncidentdetails.Navigate(new FileInfo("incident_cctv.html").FullName, sameIncident); 
                });
            }
            catch(Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }        

        private void FillCallsAndDispatchs(SelectedIncidentTaskResult selectedIncidentTask)
        {
            if (selectedIncidentTask != null)
            {
                foreach (CctvReportClientData phoneReportLight in selectedIncidentTask.TotalPhoneReports)
                {
					GridControlDataAssociatedCallData item = new GridControlDataAssociatedCallData(phoneReportLight);
                }
                foreach (DispatchOrderClientData dispatchOrderLight in selectedIncidentTask.TotalDispatchOrders)
                {
					GridControlDataAssociatedDispatchOrder item = new GridControlDataAssociatedDispatchOrder(dispatchOrderLight);
                }
            }
        }

        private string GetCctvReportIncidentTypes(CctvReportClientData cctvReport)
        {
            string incidentTypeStr = "";
            int counter = 0;
            int incTypeLength = cctvReport.IncidentTypesCodes.Count;
            foreach (int incidentTypeCode in cctvReport.IncidentTypesCodes)
            {
                incidentTypeStr += globalIncidentTypes[incidentTypeCode].CustomCode;
                counter++;
                if (counter < incTypeLength)
                    incidentTypeStr += ", ";

            }
            return incidentTypeStr;
        }
        
		private bool EnsureUpdateCctvReport()
        {
            DialogResult dialogResult = MessageForm.Show(ResourceLoader.GetString2("ChangesOnReport"), MessageFormType.Question);

            return dialogResult == DialogResult.Yes;
        }
        
		private void CleanForm()
        {
            CleanForm(true);
        }

        private void CleanForm(bool clearSelection)
        {
            this.questionsCctvControl1.CleanControls();            
            this.cctvCallInformationControl1.CleanControl();
            this.departmentsInvolvedControl.departmentTypesControl.CleanControl();
            this.incidentAdded = false;
            callEntering = false;
            IsActiveCall = false;        
            if (clearSelection == true)
				this.gridViewExIncidentList.ClearSelection();          
            
			this.GlobalCctvReport = null;
            ChangeCurrentStep(0, false);
        }

        private void CheckButtons(ButtonEx pressedButton)
        {            
            pressedButton.BackColor = SystemColors.ButtonHighlight;
        } 
              
        private void buttonExOpenIncidentsFilter_Click(object sender, EventArgs e)
        {            
            ButtonEx pressedButton = (ButtonEx)sender;
            if (pressedButton.BackColor != SystemColors.ButtonHighlight)
            {
                searchByText = false;
                CheckButtons(pressedButton);              
                ArrayList parameters = new ArrayList();
                parameters.Add("OPEN_INCIDENTS");
                incidentsCurrentFilter.Parameters = parameters;
                incidentsCurrentFilter.Filter(true);

				if (gridControlExIncidentList.SelectedItems.Count == 0)
                    ClearTextBox();
            }
        }

        private void ClearTextBox()
        {
            layoutControlGroup4.Text = ResourceLoader.GetString2("IncidentDetails");
            textBoxExIncidentdetails.DocumentText = "<span style='HEIGHT:100%;FONT-SIZE: 10pt; FONT-FAMILY: Sans-Serif'>" + ResourceLoader.GetString2("NoIncidentSelected") + "</span>";            
			barButtonItemPrint.Enabled = false;
			barButtonItemSave.Enabled = false;
			barButtonItemRefresh.Enabled = false;
        }                
                     
        private void SelectedIncidentHelp(SelectedIncidentTask selectedIncidentTask, GeoPoint point)
        {
            if (point != null)
            {
                ApplicationServiceClient.Current(UserApplicationClientData.Map).DisplayGeoPointInGis(point);
            }
            if (selectedIncidentTask != null)
            {
                ArrayList tasks = new ArrayList();
                tasks.Add(selectedIncidentTask);
                bool sameIncident = false;
                if (selectedIncidentCode == (int)selectedIncidentTask.Parameters[0])
                    sameIncident = true;
                selectedIncidentCode = (int)selectedIncidentTask.Parameters[0];
                try
                {
                    IList result = serverServiceClient.RunTasks(tasks);
                    if (result != null && result.Count > 0 && (result[0] as IList) != null)
                    {
                        IList tempList = (IList)result[0];
                        if (tempList != null && tempList.Count > 0)
                        {
                            SelectedIncidentTaskResult selectedIncidentLightData = tempList[0] as SelectedIncidentTaskResult;
                            if (selectedIncidentLightData != null)
                            {
                                FillIncidentDetails(selectedIncidentLightData, sameIncident);
                                FillCallsAndDispatchs(selectedIncidentLightData);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    SmartLogger.Print(ex);
                }
            }
        }

        private void RefreshIncidentReport()
        {
            if (selectedIncidentCode != -1)
            {
                ArrayList parameters = new ArrayList();
                parameters.Add(selectedIncidentCode);
                SelectedIncidentTask selectedIncidentTask = new SelectedIncidentTask(parameters);
                ArrayList tasks = new ArrayList();
                tasks.Add(selectedIncidentTask);
                try
                {
                    IList result = serverServiceClient.RunTasks(tasks);
                    if (result != null && result.Count > 0 && (result[0] as IList) != null)
                    {
                        IList tempList = (IList)result[0];
                        if (tempList != null && tempList.Count > 0)
                        {
                            SelectedIncidentTaskResult selectedIncidentLightData = tempList[0] as SelectedIncidentTaskResult;
                            if (selectedIncidentLightData != null)
                            {
                                FillIncidentDetails(selectedIncidentLightData, true);
                                FillCallsAndDispatchs(selectedIncidentLightData);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    SmartLogger.Print(ex);
                }
            }
        }    
          
        private void SetSkin()
        {
            try
            {
                string skinFile = SmartCadConfiguration.SkinsFolder + "/Clasic.xml";
                skinEngine = SkinEngine.Load(skinFile);
                ProfessionalColorTable colorTable = new CustomToolStripColors(
                    skinEngine.Colors["ToolStripGradientBegin"],
                    skinEngine.Colors["ToolStripGradientMiddle"],
                    skinEngine.Colors["ToolStripGradientEnd"],
                    skinEngine.Colors["MenuStripGradientBegin"],
                    skinEngine.Colors["MenuStripGradientEnd"],
                    skinEngine.Colors["StatusStripGradientBegin"],
                    skinEngine.Colors["StatusStripGradientEnd"]
                );

                
                skinEngine.AddElement("Step1Control", this.cctvCallInformationControl1);
                skinEngine.AddElement("Step2Control", this.questionsCctvControl1);
                skinEngine.AddElement("Step3LeftControl", this.departmentsInvolvedControl);               
                skinEngine.AddCompleteElement(this);
                skinEngine.ApplyChanges();
            }
            catch (Exception ex)
            {
                MessageForm.Show(ResourceLoader.GetString2("$Message.Error"), ResourceLoader.GetString2("ErrorLoadingSkin"), ex);
            }
        }

        private void CctvFrontClientForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            ServerServiceClient.GetInstance().CommittedChanges -= new EventHandler<CommittedObjectDataCollectionEventArgs>(serverService_CommittedChanges);
            
            ServerServiceClient.GetInstance().CloseSession();
        }

        private void SetFrontClientWaitingForCallState()
        {
            FormUtil.InvokeRequired(this, delegate
            {               
                this.cctvCallInformationControl1.FrontClientState =FrontClientStateEnum.WaitingForIncident;
                this.questionsCctvControl1.CctvClientState = CctvClientStateEnum.WaitingForCctvIncident;
                this.departmentsInvolvedControl.FrontClientState = FrontClientStateEnum.WaitingForIncident;              
               // this.barButtonItemStartRegIncident.Enabled = true;
                this.barButtonItemCreateNewIncident.Enabled = false;
				this.barButtonItemCancelIncident.Enabled = false; 
            });
        }

        private void SetFrontClientRegisteringCallState()
        {
            FormUtil.InvokeRequired(this, delegate
            {
                this.cctvCallInformationControl1.FrontClientState = FrontClientStateEnum.RegisteringIncident;
                this.questionsCctvControl1.CctvClientState = CctvClientStateEnum.RegisteringCctvIncident;
                this.departmentsInvolvedControl.FrontClientState = FrontClientStateEnum.RegisteringIncident;
                this.barButtonItemStartRegIncident.Enabled = false;
                this.barButtonItemCreateNewIncident.Enabled = true;
				this.barButtonItemCancelIncident.Enabled = true;
               
            }); 
        }    
   
        private void dataGridCctvPrevIncidents_KeyDown(object sender, KeyEventArgs e)
        {          
            if (e.KeyCode == Keys.F5)
            {
                barButtonItemRefresh_ItemClick(null, null);
            }
            else
            {
                CctvFrontClientForm_KeyDown(null, e);
            }
        }

        private void textBoxExIncidentdetails_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            KeyEventArgs keyEvent = new KeyEventArgs(e.KeyData);
            CctvFrontClientForm_KeyDown(null, keyEvent);
        }
        
		private void questionAnswerTextControl_Enter(object sender, EventArgs e)
        {
            if (this.CctvClientState != CctvClientStateEnum.WaitingForCctvIncident)
                ChangeCurrentStep(3, false);
        }              
        
		private void toolStripMenuItemHelpOnLine_Click(object sender, EventArgs e)
        {
            try
            {
                ApplicationUtil.LaunchHelp(SmartCadConfiguration.DistFolder + "/Help/CCTV Module.chm");
            }
            catch (Exception ex)
            {
                MessageForm.Show(ResourceLoader.GetString2("$Message.HelpNotInstalled"), ex);
            }
        }    
     
        private void questionAnswerTextControl_QuestionAnswerTextControlPreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            KeyEventArgs keyEvent = new KeyEventArgs(e.KeyData);
            CctvFrontClientForm_KeyDown(null, keyEvent);
        }
        
        private void frontClientButton_MouseLeave(object sender, EventArgs e)
        {
            toolTipMain.Hide((Button)sender);
        }

        private void contextMenuStripIncidents_Opening(object sender, CancelEventArgs e)
        {
			if (gridControlExIncidentList.SelectedItems.Count > 0)
                barButtonItemRefresh.Enabled = true;
            else
            {
                e.Cancel = true;
                barButtonItemRefresh.Enabled = false;
            }
        }
                
        private void cctvCallInformationControl1_SendGeoPointStruct(object sender, GeoPointEventArgs e)
        {
            ApplicationServiceClient.Current(UserApplicationClientData.Map).DisplayGeoPointInGis(e.Point);
        }

		public void barButtonItemStartRegIncident_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
			//barButtonItemStartRegIncident.LargeGlyph = ResourceLoader.GetImage("$Image.RecordOn");
			this.cctvCallInformationControl1.ResetCombobox();
			GenerateNewCctvReport(sender);
			this.CctvClientState = CctvClientStateEnum.RegisteringCctvIncident;
			//ChangeCurrentStep(1, true);
		}

        public void barButtonItemStartRegIncidentAlarm_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //barButtonItemStartRegIncident.LargeGlyph = ResourceLoader.GetImage("$Image.RecordOn");
            this.cctvCallInformationControl1.ResetCombobox();
            GenerateNewCctvReportAlarm(sender);
            this.CctvClientState = CctvClientStateEnum.RegisteringCctvIncident;
            //ChangeCurrentStep(1, true);
        }

        public void barButtonItemCreateNewIncident_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
			if (ValidateInterface() == true)
			{
                this.Show();
				this.questionsCctvControl1.Focus();
                if (AlarmDataSelect == null)
                {
                    GenerateNewIncident();

                }else
                {
                    GenerateNewIncidentAlarm();
                    ServerServiceClient.GetInstance().DeleteAlarmObject(((ClientData)AlarmDataSelect.Tag));
                }
                AlarmDataSelect = null;
                CleanForm();
				departmentsInvolvedControl.departmentTypesControl.gridViewExDepartmentType.ClearSelection();
				this.CctvClientState = CctvClientStateEnum.WaitingForCctvIncident;
				//barButtonItemStartRegIncident.LargeGlyph = ResourceLoader.GetImage("$Image.RecordOff");

                //((CctvFrontClientFormDevX)this.MdiParent).FisnishAlarm();
			}
		}

        public void barButtonItemCancelIncident_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
			DialogResult dialog = MessageForm.Show("CancelIncidentRegistrationCCTV", MessageFormType.WarningQuestion);
			if (dialog.Equals(DialogResult.No))
			{
				return;
			}
			updatedAnswers.Clear();
			CleanForm();
            AlarmDataSelect = null;
            departmentsInvolvedControl.departmentTypesControl.gridViewExDepartmentType.ClearSelection();
			this.CctvClientState = CctvClientStateEnum.WaitingForCctvIncident;

            //((CctvFrontClientFormDevX)this.MdiParent).ReleaseAlarm();
			//barButtonItemStartRegIncident.LargeGlyph = ResourceLoader.GetImage("$Image.RecordOff");
		}

		private void barButtonItemPrint_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
            textBoxExIncidentdetails.ShowPrintDialog();
		}

		private void barButtonItemSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
			textBoxExIncidentdetails.ShowSaveAsDialog();
		}

		private void barButtonItemRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
			if (gridControlExIncidentList.SelectedItems.Count > 0)
                OnRefreshIncidentReport();
		}

		private void dockPanel1_DockChanged(object sender, EventArgs e)
		{
			if (dockPanel1.Visible == true && this.Width < this.ParentForm.MinimumSize.Width + dockPanel1.Width)
				this.ParentForm.Width += dockPanel1.Width;
		}
		
		void gridViewExIncidentList_SingleSelectionChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
		{
			this.selectedIncidentCode = -1;
			if (gridControlExIncidentList.SelectedItems.Count > 0)
            {
				IncidentClientData selectedIncident = ((GridIncidentCctvData)gridControlExIncidentList.SelectedItems[0]).Tag as IncidentClientData;
                
                ArrayList parameters = new ArrayList();
                parameters.Add(selectedIncident.Code);
                parameters.Add(selectedIncident.IncidentTypes);

                SelectedIncidentTask selectedIncidentTask = new SelectedIncidentTask(parameters);
                ArrayList tasks = new ArrayList();
                tasks.Add(selectedIncidentTask);
                
                GeoPoint point = null;
                AddressClientData address = selectedIncident.Address;
                if (address != null && address.Lon != 0 && address.Lat != 0)
                {
                    point = new GeoPoint(address.Lon, address.Lat);
                }                
                ThreadPool.QueueUserWorkItem(delegate(object state)
                {
                    try
                    {                       
                        SelectedIncidentHelp(selectedIncidentTask, point);
                    }
                    catch (Exception ex)
                    {
                        MessageForm.Show(ex.Message, ex);
                    }
                });

				barButtonItemSave.Enabled = true;
				barButtonItemPrint.Enabled = true;
				barButtonItemRefresh.Enabled = true;
            }
            else
            {
				barButtonItemSave.Enabled = false;
				barButtonItemPrint.Enabled = false;
				barButtonItemRefresh.Enabled = false;                                        
                textBoxExIncidentdetails.DocumentText = "<span style='HEIGHT:100%;FONT-SIZE: 10pt; FONT-FAMILY: Sans-Serif'>" + ResourceLoader.GetString2("NoIncidentSelected") + "</span>";                                
            }
		}

		private void DefaultCctvFrontClientFormDevX_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
            }
            else if (CctvClientState == CctvClientStateEnum.WaitingForCctvIncident) //&& EnsureLogout())
            {
                cctvCallInformationControl1.Enter -= callReceiverControl1_Enter;
                ServerServiceClient.GetInstance().CommittedChanges -= serverService_CommittedChanges;
                ServerServiceClient.GetInstance().CommittedChanges -= ((CctvFrontClientFormDevX)this.MdiParent).FrontClientFormDevX_CommittedChanges;
                //this.CleanForm();
               // this.Controls.Clear();
               // this.Dispose(true);
            }
            else
            {
                e.Cancel = true;
            }

        }

        private static bool EnsureLogout()
        {
            DialogResult dialogResult = MessageForm.Show(ResourceLoader.GetString2("$Message.ExitApplication"), MessageFormType.Question);

            return dialogResult == DialogResult.Yes;
        }
    }    
}
