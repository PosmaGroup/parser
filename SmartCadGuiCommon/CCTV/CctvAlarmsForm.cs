using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Smartmatic.SmartCad.Map;
using System.Collections;
using Smartmatic.SmartCad.Service;
using System.Reflection;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using System.Threading;
using System.IO;
using System.Xml.Xsl;
using System.Xml;
using Smartmatic.SmartCad.Vms;
using DevExpress.XtraLayout;
using SmartCadControls.SyncBoxes;
using SmartCadCore.ClientData;
using SmartCadCore.Core;
using SmartCadControls.Controls;
using SmartCadCore.Common;
using SmartCadControls;
using SmartCadFirstLevel.Gui.SyncBoxes;
using SmartCadGuiCommon.Classes;
using SmartCadGuiCommon.Controls;
using SmartCadGuiCommon.Util;
using Smartmatic.SmartCad.VideoAnalitica.Objects;
using Smartmatic.SmartCad.VideoAnalitica.AgentVI;
using System.Drawing.Drawing2D;
using System.Windows.Media; 

namespace SmartCadGuiCommon
{

    
    public partial class CctvAlarmsForm : DevExpress.XtraEditors.XtraForm
    {
        //public int elements_selected = -1;
        private ServerServiceClient serverServiceClient;
        private XslCompiledTransform xslCompiledTransform;
        private int selectedAlarmCode;
        private GridControlSynBox AlarmSyncBox;
        public ApplicationPreferenceClientData CCTVAlarmCodePrefix = null;
        public ApplicationPreferenceClientData CctvVideoSecondsBefore = null;
        public ApplicationPreferenceClientData CctvVideoSecondsAfter = null;
        private Dictionary<string, ApplicationPreferenceClientData> globalApplicationPreference;
        private DateTime videoStartDate;
        private DateTime videoEndDate;
        private ConfigurationClientData config = null;
        public CameraPanelControl CameraPanel;
        public VmsSingletonUtil VmsControl;
        public Dictionary<int, float> speed = new Dictionary<int, float>();
        public System.Threading.Timer globalTimer;
     //   private static System.Threading.Timer timer;
        private const int GLOBAL_TIMER_PERIOD = 30000;
        private CctvFrontClientFormDevX cctvfront;
        

        IPlayBackPanel currentPanel;
        public IPlayBackPanel CurrentPanel
        {
            get { return currentPanel; }
            set
            {
                currentPanel = value;
                panelControl1.Controls.Clear();
                panelControl1.Controls.Add(currentPanel as Control);
                ((Control)currentPanel).Dock = DockStyle.Fill;
                currentPanel.DateTimeValueChanged += CurrentPanel_DateTimeValueChanged;
                ((Control)currentPanel).Disposed += CurrentPanel_Disposed;
            }
        }

        void CurrentPanel_Disposed(object sender, EventArgs e)
        {
            currentPanel = null;
            //simpleButtonGoto.Enabled = false;
            //simpleButtonGoto.Enabled = true;
        }

        System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PlayBackForm));

        private string p;

        public CctvAlarmsForm()
        {
            InitializeComponent();
            InitSpeed();
            CCTVAlarmCodePrefix = (ApplicationPreferenceClientData)ServerServiceClient.GetInstance().SearchClientObject(
               SmartCadHqls.GetCustomHql(SmartCadHqls.GetApplicationPreferenceByName, "CCTVAlarmCodePrefix"));
            CctvVideoSecondsBefore = (ApplicationPreferenceClientData)ServerServiceClient.GetInstance().SearchClientObject(
                SmartCadHqls.GetCustomHql(SmartCadHqls.GetApplicationPreferenceByName, "CctvVideoSecondsBefore"));
            CctvVideoSecondsAfter = (ApplicationPreferenceClientData)ServerServiceClient.GetInstance().SearchClientObject(
                SmartCadHqls.GetCustomHql(SmartCadHqls.GetApplicationPreferenceByName, "CctvVideoSecondsAfter"));
            //DateEditStartDate.EditValue = DateTime.Now.Subtract(new TimeSpan(0, 1, 0));
            //labelVideoTime.Text = DateEditStartDate.EditValue.ToString();
            //DateEditEndDate.EditValue = DateTime.Now;
            config = ServerServiceClient.GetInstance().GetConfiguration();
            VmsControl = new VmsSingletonUtil();


        }

        private void CctvAlarmsForm_Load(object sender, EventArgs e)
        {

            XmlReaderSettings xmlReaderSettings = new XmlReaderSettings() { ProhibitDtd = false };
            xslCompiledTransform = new XslCompiledTransform();
            xslCompiledTransform.Load(XmlReader.Create(SmartCadConfiguration.CctvAlarmXslt, xmlReaderSettings));

            this.gridControlAlarmIn.EnableAutoFilter = true;
            this.gridControlAlarmIn.Type = typeof(GridCctvAlertCamData);
            AlarmSyncBox = new GridControlSynBox(gridControlAlarmIn);
            LoadLanguage();
        }

        void LoadLanguage()
        {
            Text = ResourceLoader.GetString2("Alarms");
            barButtonItemPrint.Caption = ResourceLoader.GetString2("Print");
            barButtonItemRefresh.Caption = ResourceLoader.GetString2("Refresh");
            barButtonItemSave.Caption = ResourceLoader.GetString2("Save");
            ribbonPageGroupGeneralOptions.Text = ResourceLoader.GetString2("GeneralOptions");
            checkButtonPlay.ToolTipTitle = ResourceLoader.GetString2("StartVideo");
            simpleButtonStop.ToolTipTitle = ResourceLoader.GetString2("StopVideo");
            checkButtonBack.ToolTipTitle = "Rewind";
            ribbonPage1.Text = ResourceLoader.GetString2("Cctv");
            groupControlVideo.Text = ResourceLoader.GetString2("Alarm Video");
            groupControlAudioControl.Text = ResourceLoader.GetString2("VideoControls");
            barButtonItemCancelVA_Alarm.Caption = ResourceLoader.GetString2("Dismiss");
            ribbonPageGroupAlarm.Text = ResourceLoader.GetString2("Alarms");
        }

        public void ExecuteSyncro()
        {
           
          /*  
            globalTimer = ApplicationUtil.RecurrentExecuteMethod(
                new SimpleDelegate(
                delegate
                {
                    globalTimer_Tick(null, null);
                }), null, null, GLOBAL_TIMER_PERIOD, GLOBAL_TIMER_PERIOD);
            */
            var autoEvent = new AutoResetEvent(false);
            globalTimer = new System.Threading.Timer(new TimerCallback(BackgroundSearch), autoEvent, 0, GLOBAL_TIMER_PERIOD);
            GC.KeepAlive(globalTimer);

        }
        private void globalTimer_Tick(object sender, EventArgs e)
        {
    /*        globalTimer.Dispose();
            BackgroundSearch();
            ExecuteSyncro();
     * */
        }
        void CurrentPanel_DateTimeValueChanged(DateTime dateTime)
        {
            labelVideoTime.Text = dateTime.ToShortDateString() + " " + dateTime.ToLongTimeString();
            //if (dateTime.CompareTo(DateEditEndDate.DateTime) > -1)
            //    simpleButtonStop_Click(null, null);
            if (DateEditStartDate.DateTime.CompareTo(dateTime) > 0)
                simpleButtonStop_Click(null, null);
      //  }


            Alert[] EventsFromServer;
            
           // IObj.Connect();
          //  EventsFromServer = IObj.getAlerts();

//            }

        }

        public void BackgroundSearch(object state)
        {
            int operCode = (int)ServerServiceClient.GetInstance().OperatorClient.Code;
             
            IList AlertsList = ServerServiceClient.GetInstance().SearchClientObjectsMaxRows(SmartCadHqls.GetCustomHql(SmartCadHqls.GetVAAlertByCameraOperatorId, operCode), true);

             //if (AlertsList.Count == 0)
             //AlertsList = null;
            //if (AlertsList != null)
            //{
                if (AlarmSyncBox.Update(AlertsList))
                {
                    if (groupControlVideo.Controls.Count > 0)
                        ((CameraPanelControl)groupControlVideo.Controls[0]).StopPlayBack();
                    Action act = () => groupControlVideo.Controls.Clear();
                    groupControlVideo.Invoke(act);
                }
            //}    
          barButtonItemRefresh.Enabled = true;
            ////checkButtonPlay.Image = global::SmartCadGuiCommon.Properties.Resources.Play;
            //Action act1 = () => checkButtonPlay.Image = global::SmartCadGuiCommon.Properties.Resources.Play;
            //checkButtonPlay.Invoke(act1);
            //
            ////checkButtonBack.Image = ((System.Drawing.Image)(resources.GetObject("checkButtonBack.Image")));
            //Action act2 = () => checkButtonBack.Image = ((System.Drawing.Image)(resources.GetObject("checkButtonBack.Image")));
            //checkButtonBack.Invoke(act2);
            //
            ////checkButtonBack.Enabled = false;
            //Action act3 = () => checkButtonBack.Enabled = false;
            //checkButtonBack.Invoke(act3);
            //
            ////checkButtonBack.Checked = false;
            //Action act4 = () => checkButtonBack.Checked = false;
            //checkButtonBack.Invoke(act4);
            //
            ////checkButtonMute.Enabled = false;
            //Action act5 = () => checkButtonMute.Enabled = false;
            //checkButtonMute.Invoke(act5);
            //
            ////checkButtonPlay.Enabled = false;
            //Action act6 = () => checkButtonPlay.Enabled = false;
            //checkButtonPlay.Invoke(act6);
            //
            ////checkButtonPlay.Checked = false;
            //Action act7 = () => checkButtonPlay.Checked = false;
            //checkButtonPlay.Invoke(act7);
            //
            ////simpleButtonStop.Enabled = false;
            //Action act8 = () => simpleButtonStop.Enabled = false;
            //simpleButtonStop.Invoke(act8);
            //
            ////labelControlPlaybackSpeed.Enabled = false;
            //Action act9 = () => labelControlPlaybackSpeed.Enabled = false;
            //labelControlPlaybackSpeed.Invoke(act9);
            //
            ////trackBarControlVolume.Enabled = false;
            //Action act10 = () => trackBarControlVolume.Enabled = false;
            //trackBarControlVolume.Invoke(act10);
            //
            ////zoomTrackBarControlSpeed.Enabled = false;
            //Action act11 = () => zoomTrackBarControlSpeed.Enabled = false;
            //zoomTrackBarControlSpeed.Invoke(act11);

        }

        /*private void gridViewExPreviousAlarms_SelectionWillChange(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            
            
            // Prueba de descartes
            //barButtonItemPrint.Enabled = (e.FocusedRowHandle > -1);
            //barButtonItemSave.Enabled = (e.FocusedRowHandle > -1);
            //barButtonItemRefresh.Enabled = (e.FocusedRowHandle > -1);
            ((CctvFrontClientFormDevX)this.MdiParent).barButtonItemStartRegIncident.Enabled = (e.FocusedRowHandle > -1);
            barButtonItemCancelVA_Alarm.Enabled = (e.FocusedRowHandle > -1);

            checkButtonBack.Checked = true;
            checkButtonBack.Image = ((System.Drawing.Image)(resources.GetObject("checkButtonBack.Image")));
            checkButtonBack.Checked = false;
            checkButtonBack.Enabled = (e.FocusedRowHandle > -1);
            checkButtonMute.Enabled = (e.FocusedRowHandle > -1);
            checkButtonPlay.Checked = true;
            checkButtonPlay.Image = global::SmartCadGuiCommon.Properties.Resources.Play;
            checkButtonPlay.Checked = false;
            checkButtonPlay.Enabled = (e.FocusedRowHandle > -1);
            simpleButtonStop.Enabled = (e.FocusedRowHandle > -1);
            labelControlPlaybackSpeed.Enabled = (e.FocusedRowHandle > -1);
            trackBarControlVolume.Enabled = (e.FocusedRowHandle > -1);
            zoomTrackBarControlSpeed.Enabled = (e.FocusedRowHandle > -1);

            if (e.FocusedRowHandle > -1 && gridControlAlarmIn.SelectedItems.Count > 0)
            {
                //FormUtil.InvokeRequired(this,
                //delegate
                //{
                GridCctvAlertCamData selectedAlarm = ((GridCctvAlertCamData)gridControlAlarmIn.SelectedItems[0]);
                
                
                        try
                        {
                            VAAlertCamClientData report = ((VAAlertCamClientData)selectedAlarm.Tag);
                            if (groupControlVideo.Controls.Count > 0)
                                ((CameraPanelControl)groupControlVideo.Controls[0]).StopPlayBack();
                            groupControlVideo.Controls.Clear();

                            videoStartDate = selectedAlarm.Date.AddSeconds(-int.Parse(CctvVideoSecondsBefore.Value));
                            videoEndDate = selectedAlarm.Date.AddSeconds(int.Parse(CctvVideoSecondsAfter.Value));
                            if (this.CameraPanel != null) this.CameraPanel.CameraPanelControlTemplates();
                            CurrentPanel = new CameraPanelControl(report.Camera, PlayModes.PlayBack_SingleSequence, config, false, videoStartDate, videoEndDate, VmsControl);
                            CurrentPanel.EndDate = videoEndDate;
                            CurrentPanel.StartDate = videoStartDate;
                            CurrentPanel.DateTimeValueChanged += new VmsControlEx.DateTimeValueChangedEventHandler(panel_DateTimeValueChanged);
                            DateEditStartDate.EditValue = videoStartDate;
                            DateEditEndDate.EditValue = videoEndDate;
                            labelVideoTime.Text = videoStartDate.ToShortDateString() + " " + videoStartDate.ToLongTimeString();
                            CameraPanel = (CameraPanelControl)CurrentPanel;
                           
                        
                            groupControlVideo.Controls.Add(CameraPanel);
                           
                            CameraPanel.Dock = DockStyle.Fill;
                            CurrentPanel.PlayDirection = 0;
                            CurrentPanel.PlaySpeed = 0;
                            CurrentPanel.InitStopPlayback();
                           
                          
                            
                            //panel.StartPlayBack();
                        }
                        catch { }
                //});
            }
            else
            {

                if (groupControlVideo.Controls.Count > 0)
                    ((CameraPanelControl)groupControlVideo.Controls[0]).StopPlayBack();
                try { groupControlVideo.Controls.Clear(); }
                catch { }
            }
        }
        */

        private void gridViewExPreviousAlarms_SelectionWillChange2(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            ((CctvFrontClientFormDevX)this.MdiParent).barButtonItemStartRegIncident.Enabled = (e.RowHandle > -1);
            barButtonItemCancelVA_Alarm.Enabled = (e.RowHandle > -1);
            //elements_selected = e.RowHandle;
            //DialogResult dialogResult2 = MessageBox.Show("estas parado en la fila: " + gridControlAlarmIn.SelectedItems.Count);
            checkButtonBack.Checked = true;
            checkButtonBack.Image = ((System.Drawing.Image)(resources.GetObject("checkButtonBack.Image")));
            checkButtonBack.Checked = false;
            checkButtonBack.Enabled = (e.RowHandle > -1);
            checkButtonMute.Enabled = (e.RowHandle > -1);
            checkButtonPlay.Checked = true;
            checkButtonPlay.Image = global::SmartCadGuiCommon.Properties.Resources.Play;
            checkButtonPlay.Checked = false;
            checkButtonPlay.Enabled = (e.RowHandle > -1);
            simpleButtonStop.Enabled = (e.RowHandle > -1);
            labelControlPlaybackSpeed.Enabled = (e.RowHandle > -1);
            trackBarControlVolume.Enabled = (e.RowHandle > -1);
            zoomTrackBarControlSpeed.Enabled = (e.RowHandle > -1);

            if (e.RowHandle > -1 && gridControlAlarmIn.SelectedItems.Count > 0)
            {
                //FormUtil.InvokeRequired(this,
                //delegate
                //{
                GridCctvAlertCamData selectedAlarm = ((GridCctvAlertCamData)gridControlAlarmIn.SelectedItems[0]);


                try
                {
                    VAAlertCamClientData report = ((VAAlertCamClientData)selectedAlarm.Tag);
                    if (groupControlVideo.Controls.Count > 0)
                        ((CameraPanelControl)groupControlVideo.Controls[0]).StopPlayBack();
                    groupControlVideo.Controls.Clear();

                    videoStartDate = selectedAlarm.Date.AddSeconds(-int.Parse(CctvVideoSecondsBefore.Value));
                    videoEndDate = selectedAlarm.Date.AddSeconds(int.Parse(CctvVideoSecondsAfter.Value));
                    if (this.CameraPanel != null) this.CameraPanel.CameraPanelControlTemplates();
                    CurrentPanel = new CameraPanelControl(report.Camera, PlayModes.PlayBack_SingleSequence, config, false, videoStartDate, videoEndDate, VmsControl);
                    CurrentPanel.EndDate = videoEndDate;
                    CurrentPanel.StartDate = videoStartDate;
                    CurrentPanel.DateTimeValueChanged += new VmsControlEx.DateTimeValueChangedEventHandler(panel_DateTimeValueChanged);
                    DateEditStartDate.EditValue = videoStartDate;
                    DateEditEndDate.EditValue = videoEndDate;
                    labelVideoTime.Text = videoStartDate.ToShortDateString() + " " + videoStartDate.ToLongTimeString();
                    CameraPanel = (CameraPanelControl)CurrentPanel;


                    groupControlVideo.Controls.Add(CameraPanel);

                    CameraPanel.Dock = DockStyle.Fill;
                    CurrentPanel.PlayDirection = 0;
                    CurrentPanel.PlaySpeed = 0;
                    CurrentPanel.InitStopPlayback();



                    //panel.StartPlayBack();
                }
                catch { }
                //});
            }
            else
            {

                if (groupControlVideo.Controls.Count > 0)
                    ((CameraPanelControl)groupControlVideo.Controls[0]).StopPlayBack();
                try { groupControlVideo.Controls.Clear(); }
                catch { }
            }
        }

        private void InitSpeed()
        {
            speed.Add(0, 0);
            speed.Add(1, 0.25f);
            speed.Add(2, 0.50f);
            speed.Add(3, 0.75f);
            speed.Add(4, 1.00f);
            speed.Add(5, 2.00f);
            speed.Add(6, 5.00f);
            speed.Add(7, 10.00f);
            speed.Add(8, 20.00f);

            zoomTrackBarControlSpeed.Value = 4;
        }

        public void SetPlayBackSpeed()
        {

        }

        private void simpleButtonPlay_Click(object sender, EventArgs e)
        {
            if (checkButtonPlay.Checked)
            {
                checkButtonPlay.Image = global::SmartCadGuiCommon.Properties.Resources.Play;
                checkButtonBack.Enabled = true;
                checkButtonBack.Checked = false;

                CurrentPanel.StopPlayBack();
                //CurrentPanel.StartDate = DateTime.Parse(labelVideoTime.Text);
            }
            else
            {
                checkButtonPlay.Image = global::SmartCadGuiCommon.Properties.Resources.pausa;
                checkButtonBack.Enabled = false;
                checkButtonBack.Checked = false;

                CurrentPanel.PlayDirection = 0;
                try
                {
                    //CurrentPanel.StartDate = DateTime.Parse(labelVideoTime.Text);
                    CurrentPanel.StartPlayBack();

                }
                catch { }
            }
        }

        private void simpleButtonBack_Click(object sender, EventArgs e)
        {
            if (checkButtonBack.Checked)
            {
                checkButtonBack.Image = ((System.Drawing.Image)(resources.GetObject("checkButtonBack.Image")));
                checkButtonPlay.Enabled = true;
                checkButtonPlay.Checked = false;

                CurrentPanel.StopPlayBack();
                //CurrentPanel.StartDate = DateTime.Parse(labelVideoTime.Text);
            }
            else
            {
                checkButtonBack.Image = global::SmartCadGuiCommon.Properties.Resources.pausa;
                checkButtonPlay.Enabled = false;
                checkButtonPlay.Checked = false;
                CurrentPanel.PlayDirection = 1;
                try
                {
                    CurrentPanel.StartDate = DateTime.Parse(labelVideoTime.Text);
                    CurrentPanel.StartPlayBack();

                }
                catch { }
                //CurrentPanel.PlayDirection = -1;
                //CurrentPanel.StartPlayBack();
            }
        }

        private void simpleButtonStop_Click(object sender, EventArgs e)
        {
            checkButtonPlay.Checked = false;
            checkButtonBack.Checked = false;
            checkButtonPlay.Enabled = true;
            checkButtonBack.Enabled = true;
            checkButtonPlay.Image = global::SmartCadGuiCommon.Properties.Resources.Play;
            checkButtonBack.Image = ((System.Drawing.Image)(resources.GetObject("checkButtonBack.Image")));

            labelVideoTime.Text = DateEditStartDate.DateTime.ToShortDateString() + " " + DateEditStartDate.DateTime.ToLongTimeString();
            if (CurrentPanel != null)
            {
                CurrentPanel.InitStopPlayback();
                CurrentPanel.EndDate = DateEditEndDate.DateTime;
                CurrentPanel.StartDate = DateEditStartDate.DateTime;

            }
        }

        private void trackBarControlPlayback_EditValueChanged(object sender, EventArgs e)
        {
            CurrentPanel.Volume = trackBarControlVolume.Value;
        }

        private void zoomTrackBarControlSpeed_EditValueChanged(object sender, EventArgs e)
        {
            ZoomTrackBarControl control = (DevExpress.XtraEditors.ZoomTrackBarControl)sender;
            if (control.Value == 0)
                this.labelControlPlaybackSpeed.Text = ResourceLoader.GetString2("Pause");
            else
            {
                this.labelControlPlaybackSpeed.Text = ResourceLoader.GetString2("Speed") + " : " + speed[control.Value].ToString() + "x";
            }
            CurrentPanel.PlaySpeed = speed[control.Value];
        }

        private void checkButtonMute_CheckedChanged(object sender, EventArgs e)
        {
            CurrentPanel.Mute = checkButtonMute.Checked;
            if (checkButtonMute.Checked)
            {
                this.checkButtonMute.Image = global::SmartCadGuiCommon.Properties.Resources.soundMuteSmall;
            }
            else
            {
                this.checkButtonMute.Image = global::SmartCadGuiCommon.Properties.Resources.soundSmall;
            }
        }

        private void simpleButtonGoto_EnabledChanged(object sender, EventArgs e)
        {
            //if (simpleButtonGoto.Enabled == false)
            //{
                checkButtonPlay.Checked = false;
                checkButtonBack.Checked = false;
                checkButtonPlay.Enabled = false;
                checkButtonBack.Enabled = false;
                simpleButtonStop.Enabled = false;
                //checkButtonMute.Enabled = false;
                //trackBarControlVolume.Enabled = false;
                //zoomTrackBarControlSpeed.Enabled = false;
                checkButtonPlay.Image = global::SmartCadGuiCommon.Properties.Resources.Play;
                checkButtonBack.Image = ((System.Drawing.Image)(resources.GetObject("checkButtonBack.Image")));

                labelVideoTime.Text = DateEditStartDate.DateTime.ToShortDateString() + " " + DateEditStartDate.DateTime.ToLongTimeString();
            //}
        }

        void panel_DateTimeValueChanged(DateTime dateTime)
        {
            if (dateTime >= videoEndDate && groupControlVideo.Controls.Count > 0)
            {
                ((CameraPanelControl)groupControlVideo.Controls[0]).StartDate = videoStartDate;
            }
        }

        private void SelectedAlarmHelp(SelectedIncidentTask selectedAlarmTask)
        {
            if (selectedAlarmTask != null)
            {
                ArrayList tasks = new ArrayList();
                tasks.Add(selectedAlarmTask);
                bool sameAlarm = false;
                if (selectedAlarmCode == (int)selectedAlarmTask.Parameters[0])
                    sameAlarm = true;
                selectedAlarmCode = (int)selectedAlarmTask.Parameters[0];
                try
                {
                    IList result = ServerServiceClient.GetInstance().RunTasks(tasks);
                    if (result != null && result.Count > 0 && (result[0] as IList) != null)
                    {
                        IList tempList = (IList)result[0];
                        if (tempList != null && tempList.Count > 0)
                        {
                            SelectedIncidentTaskResult selectedAlarmLightData = tempList[0] as SelectedIncidentTaskResult;
                            if (selectedAlarmLightData != null)
                            {
                                FillAlarmDetails(selectedAlarmLightData, sameAlarm);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    SmartLogger.Print(ex);
                }
            }
        }

        private void FillAlarmDetails(SelectedIncidentTaskResult selectedAlarmTask, bool sameIncident)
        {
            try
            {
                string strXml = ApplicationUtil.BuildHtml(selectedAlarmTask.Xml, xslCompiledTransform, "Cctvincident");
                strXml = ApplicationUtil.RecoverBlanks(strXml);

                FileStream fs = File.Open("incident_cctv.html", FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite);
                byte[] byteXML = Encoding.Unicode.GetBytes(strXml);
                fs.Write(byteXML, 0, byteXML.Length);
                fs.Flush();
                fs.Close();
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        
       
        private void dismissing_alarm()
        {
                ServerServiceClient.GetInstance().DeleteAlarmObject(((ClientData)((GridCctvAlertCamData)gridControlAlarmIn.SelectedItems[0]).Tag));
                BackgroundSearch(null);
        }
        private void barButtonItemRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            BackgroundSearch(null);
        }

        
        //
        public void barButtonItemCancelVA_Alarm_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //DialogResult dialogResult1 = MessageBox.Show("estas parado en la fila: " + gridControlAlarmIn.SelectedItems.Count);

            if (gridControlAlarmIn.SelectedItems.Count > 0)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to dismiss the alarm?", "Warning", MessageBoxButtons.OKCancel);
                if (dialogResult == DialogResult.OK)
                {

                    //cctvfront.barButtonItemStartRegIncident.Enabled = false;
                    //cctvfront.barButtonItemStartRegIncident.Refresh();
                    ((CctvFrontClientFormDevX)this.MdiParent).barButtonItemStartRegIncident.Enabled = false;
                    barButtonItemCancelVA_Alarm.Enabled = false;
                    barButtonItemCancelVA_Alarm.Refresh();
                    resetValuesFromPanel();
                    dismissing_alarm();

               }
            }

            else {
                DialogResult dialogResult = MessageBox.Show("An alarm must be selected to dismiss", "Warning", MessageBoxButtons.OKCancel);
            }
        }

        public void resetValuesFromPanel()
        {

            checkButtonBack.Checked = false;
            checkButtonBack.Enabled = false;
            checkButtonMute.Enabled = false;
            checkButtonPlay.Checked = false;
            checkButtonPlay.Enabled = false;
            simpleButtonStop.Enabled = false;
            labelControlPlaybackSpeed.Enabled = false;
            trackBarControlVolume.Enabled = false;
            zoomTrackBarControlSpeed.Enabled = false;
            groupControlVideo.Controls.Clear();

        }

        private void CctvAlarmsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            //groupControlVideo.Controls.Clear();

            //e.Cancel = true;
            //if (vmsThread != null)
            //    vmsThread.Abort();
        }

        private void groupControl1_CustomDrawCaption(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawLine(Pens.Coral, new Point(10, 10), new Point(150, 150));
        }
    }
}