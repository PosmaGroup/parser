namespace SmartCadGuiCommon
{
	
	
	partial class CctvFrontClientFormDevX
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
        CctvAlarmsForm AlarmForm = new CctvAlarmsForm();

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CctvFrontClientFormDevX));
            this.alertControl1 = new DevExpress.XtraBars.Alerter.AlertControl(this.components);
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barButtonItemHelp = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemOpenMap = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemChat = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemPrint = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemSave = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemStartRegIncident = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemCreateNewIncident = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemCancelIncident = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemHistory = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemAlarm = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemCancelVA_Alarm = new DevExpress.XtraBars.BarButtonItem();
            this.labelConnected = new DevExpress.XtraBars.BarStaticItem();
            this.labelUser = new DevExpress.XtraBars.BarStaticItem();
            this.labelDate = new DevExpress.XtraBars.BarStaticItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroupCallInfo = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupApplications = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupGeneralOptions = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupTools = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupAlarm = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.repositoryItemTimeEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit();
            this.repositoryItemProgressBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.repositoryItemPictureEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.ribbonStatusBar1 = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.xtraTabbedMdiManager1 = new DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(this.components);
            this.labelLogo = new System.Windows.Forms.Label();
            this.barButtonItemExit = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // alertControl1
            // 
            this.alertControl1.LookAndFeel.SkinName = "Blue";
            this.alertControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.alertControl1.AlertClick += new DevExpress.XtraBars.Alerter.AlertClickEventHandler(this.alertControl1_AlertClick);
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ApplicationButtonAnimationLength = 1;
            this.ribbonControl1.ExpandCollapseItem.AllowRightClickInMenu = false;
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.ExpandCollapseItem.Name = "";
            this.ribbonControl1.GroupAnimationLength = 1;
            this.ribbonControl1.ItemAnimationLength = 1;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.barButtonItemHelp,
            this.barButtonItemOpenMap,
            this.barButtonItemChat,
            this.barButtonItemPrint,
            this.barButtonItemSave,
            this.barButtonItemRefresh,
            this.barButtonItemStartRegIncident,
            this.barButtonItemCreateNewIncident,
            this.barButtonItemCancelIncident,
            this.barButtonItemHistory,
            this.barButtonItemAlarm,
            this.barButtonItem4,
            this.labelConnected,
            this.barButtonItemCancelVA_Alarm,
            this.labelUser,
            this.labelDate});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 45;
            this.ribbonControl1.MdiMergeStyle = DevExpress.XtraBars.Ribbon.RibbonMdiMergeStyle.Always;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.PageAnimationLength = 1;
            this.ribbonControl1.PageHeaderItemLinks.Add(this.barButtonItemHelp, true);
            this.ribbonControl1.PageHeaderItemLinks.Add(this.barButtonItem4);
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbonControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTimeEdit1,
            this.repositoryItemProgressBar1,
            this.repositoryItemPictureEdit1});
            this.ribbonControl1.ShowExpandCollapseButton = DevExpress.Utils.DefaultBoolean.True;
            this.ribbonControl1.ShowToolbarCustomizeItem = false;
            this.ribbonControl1.Size = new System.Drawing.Size(1280, 119);
            this.ribbonControl1.StatusBar = this.ribbonStatusBar1;
            this.ribbonControl1.Toolbar.ShowCustomizeItem = false;
            this.ribbonControl1.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            // 
            // barButtonItemHelp
            // 
            this.barButtonItemHelp.Caption = "barButtonItemHelp";
            this.barButtonItemHelp.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemHelp.Glyph")));
            this.barButtonItemHelp.Id = 0;
            this.barButtonItemHelp.Name = "barButtonItemHelp";
            this.barButtonItemHelp.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemHelp_ItemClick);
            // 
            // barButtonItemOpenMap
            // 
            this.barButtonItemOpenMap.Caption = "ItemOpenMap";
            this.barButtonItemOpenMap.Id = 4;
            this.barButtonItemOpenMap.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemOpenMap.LargeGlyph")));
            this.barButtonItemOpenMap.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemOpenMap.Name = "barButtonItemOpenMap";
            this.barButtonItemOpenMap.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItemOpenMap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemOpenMap_ItemClick);
            // 
            // barButtonItemChat
            // 
            this.barButtonItemChat.Caption = "ItemChat";
            this.barButtonItemChat.Id = 5;
            this.barButtonItemChat.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemChat.LargeGlyph")));
            this.barButtonItemChat.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemChat.Name = "barButtonItemChat";
            this.barButtonItemChat.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItemChat.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.barButtonItemChat.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemChat_ItemClick);
            // 
            // barButtonItemPrint
            // 
            this.barButtonItemPrint.Caption = "ItemPrint";
            this.barButtonItemPrint.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPrint.Glyph")));
            this.barButtonItemPrint.Id = 6;
            this.barButtonItemPrint.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemPrint.Name = "barButtonItemPrint";
            // 
            // barButtonItemSave
            // 
            this.barButtonItemSave.Caption = "ItemSave";
            this.barButtonItemSave.Glyph = global::SmartCadGuiCommon.PrintRibbonControllerResources.RibbonPrintPreview_Save;
            this.barButtonItemSave.Id = 7;
            this.barButtonItemSave.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemSave.Name = "barButtonItemSave";
            // 
            // barButtonItemRefresh
            // 
            this.barButtonItemRefresh.Caption = "ItemRefresh";
            this.barButtonItemRefresh.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemRefresh.Glyph")));
            this.barButtonItemRefresh.Id = 8;
            this.barButtonItemRefresh.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemRefresh.Name = "barButtonItemRefresh";
            // 
            // barButtonItemStartRegIncident
            // 
            this.barButtonItemStartRegIncident.Caption = "ItemStartRegIncident";
            this.barButtonItemStartRegIncident.Enabled = false;
            this.barButtonItemStartRegIncident.Id = 12;
            this.barButtonItemStartRegIncident.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemStartRegIncident.LargeGlyph")));
            this.barButtonItemStartRegIncident.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemStartRegIncident.Name = "barButtonItemStartRegIncident";
            this.barButtonItemStartRegIncident.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItemStartRegIncident.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemStartRegIncident_ItemClick);
            // 
            // barButtonItemCreateNewIncident
            // 
            this.barButtonItemCreateNewIncident.Caption = "ItemCreateNewIncident";
            this.barButtonItemCreateNewIncident.Enabled = false;
            this.barButtonItemCreateNewIncident.Id = 13;
            this.barButtonItemCreateNewIncident.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemCreateNewIncident.LargeGlyph")));
            this.barButtonItemCreateNewIncident.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemCreateNewIncident.Name = "barButtonItemCreateNewIncident";
            this.barButtonItemCreateNewIncident.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonItemCancelIncident
            // 
            this.barButtonItemCancelIncident.Caption = "ItemCancelIncident";
            this.barButtonItemCancelIncident.Enabled = false;
            this.barButtonItemCancelIncident.Id = 15;
            this.barButtonItemCancelIncident.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemCancelIncident.LargeGlyph")));
            this.barButtonItemCancelIncident.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemCancelIncident.Name = "barButtonItemCancelIncident";
            this.barButtonItemCancelIncident.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonItemHistory
            // 
            this.barButtonItemHistory.Caption = "History";
            this.barButtonItemHistory.Id = 21;
            this.barButtonItemHistory.LargeGlyph = global::SmartCadGuiCommon.Properties.Resources.Historial;
            this.barButtonItemHistory.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemHistory.Name = "barButtonItemHistory";
            this.barButtonItemHistory.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItemHistory.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemHistory_ItemClick);
            //
            // barButtonItemAlarm
            // Arreglarlo
            this.barButtonItemAlarm.Caption = "Alarm";
            this.barButtonItemAlarm.Id = 90;
            this.barButtonItemAlarm.LargeGlyph = global::SmartCadGuiCommon.Properties.Resources.Alerta;
            this.barButtonItemAlarm.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemAlarm.Name = "barButtonItemAlarm";
            this.barButtonItemAlarm.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItemAlarm.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemAlarm_ItemClick);
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "barButtonItem4";
            this.barButtonItem4.Id = 24;
            this.barButtonItem4.Name = "barButtonItem4";
            this.barButtonItem4.SmallWithoutTextWidth = 70;
            // 
            // labelConnected
            // 
            this.labelConnected.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelConnected.Appearance.Options.UseFont = true;
            this.labelConnected.Caption = "Connected";
            this.labelConnected.Id = 25;
            this.labelConnected.Name = "labelConnected";
            this.labelConnected.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // labelUser
            // 
            this.labelUser.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUser.Appearance.Options.UseFont = true;
            this.labelUser.Caption = "Usuario";
            this.labelUser.Id = 26;
            this.labelUser.Name = "labelUser";
            this.labelUser.TextAlignment = System.Drawing.StringAlignment.Near;
            this.labelUser.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.labelUser_ItemClick);
            // 
            // labelDate
            // 
            this.labelDate.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.labelDate.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDate.Appearance.Options.UseFont = true;
            this.labelDate.Caption = "Date";
            this.labelDate.Id = 27;
            this.labelDate.Name = "labelDate";
            this.labelDate.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barButtonItemCancelVA_Alarm
            // 
            this.barButtonItemCancelVA_Alarm.Caption = "barButtonItemCancelAlarm";
            this.barButtonItemCancelVA_Alarm.Enabled = false;
            this.barButtonItemCancelVA_Alarm.Refresh();//URDANETA
            this.barButtonItemCancelVA_Alarm.Id = 41;
            this.barButtonItemCancelVA_Alarm.LargeGlyph = global::SmartCadGuiCommon.Properties.Resources.CancelAlarm1;
            this.barButtonItemCancelVA_Alarm.Name = "barButtonItemCancelVA_Alarm";
            this.barButtonItemCancelVA_Alarm.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(AlarmForm.barButtonItemCancelVA_Alarm_ItemClick);
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupCallInfo,
            this.ribbonPageGroupApplications,
            this.ribbonPageGroupGeneralOptions,
            this.ribbonPageGroupTools});
            //this.ribbonPageGroupAlarm});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "ribbonPage1";
            // 
            // ribbonPageGroupCallInfo
            // 
            this.ribbonPageGroupCallInfo.AllowMinimize = false;
            this.ribbonPageGroupCallInfo.AllowTextClipping = false;
            this.ribbonPageGroupCallInfo.ItemLinks.Add(this.barButtonItemStartRegIncident);
            this.ribbonPageGroupCallInfo.ItemLinks.Add(this.barButtonItemCreateNewIncident, true);
            this.ribbonPageGroupCallInfo.ItemLinks.Add(this.barButtonItemCancelIncident);
            this.ribbonPageGroupCallInfo.Name = "ribbonPageGroupCallInfo";
            this.ribbonPageGroupCallInfo.ShowCaptionButton = false;
            this.ribbonPageGroupCallInfo.Text = "ribbonPageGroupCallInfo";
            // 
            // ribbonPageGroupApplications
            // 
            this.ribbonPageGroupApplications.AllowMinimize = false;
            this.ribbonPageGroupApplications.AllowTextClipping = false;
            this.ribbonPageGroupApplications.ItemLinks.Add(this.barButtonItemChat);
            this.ribbonPageGroupApplications.ItemLinks.Add(this.barButtonItemHistory);
            this.ribbonPageGroupApplications.Name = "ribbonPageGroupApplications";
            this.ribbonPageGroupApplications.ShowCaptionButton = false;
            this.ribbonPageGroupApplications.Text = "Applications";
            // 
            // ribbonPageGroupGeneralOptions
            // 
            this.ribbonPageGroupGeneralOptions.AllowMinimize = false;
            this.ribbonPageGroupGeneralOptions.AllowTextClipping = false;
            this.ribbonPageGroupGeneralOptions.ItemLinks.Add(this.barButtonItemPrint);
            this.ribbonPageGroupGeneralOptions.ItemLinks.Add(this.barButtonItemSave);
            this.ribbonPageGroupGeneralOptions.ItemLinks.Add(this.barButtonItemRefresh);
            this.ribbonPageGroupGeneralOptions.Name = "ribbonPageGroupGeneralOptions";
            this.ribbonPageGroupGeneralOptions.ShowCaptionButton = false;
            this.ribbonPageGroupGeneralOptions.Text = "ribbonPageGroupGeneralOptions";
            // 
            // ribbonPageGroupAlarm
            //
            this.ribbonPageGroupAlarm.AllowMinimize = false;
            this.ribbonPageGroupAlarm.AllowTextClipping = false;
            this.ribbonPageGroupAlarm.ItemLinks.Add(this.barButtonItemCancelVA_Alarm);
            this.ribbonPageGroupAlarm.Name = "ribbonPageGroupAlarm";
            this.ribbonPageGroupAlarm.ShowCaptionButton = false;
            this.ribbonPageGroupAlarm.Text = "ribbonPageGroupAlarm";
            // 
            // ribbonPageGroupTools
            // 
            this.ribbonPageGroupTools.AllowMinimize = false;
            this.ribbonPageGroupTools.AllowTextClipping = false;
            this.ribbonPageGroupTools.ItemLinks.Add(this.barButtonItemOpenMap);
            this.ribbonPageGroupTools.Name = "ribbonPageGroupTools";
            this.ribbonPageGroupTools.ShowCaptionButton = false;
            this.ribbonPageGroupTools.Text = "ribbonPageGroupTools";
            this.ribbonPageGroupTools.Visible = false;
            // 
            // repositoryItemTimeEdit1
            // 
            this.repositoryItemTimeEdit1.AllowFocused = false;
            this.repositoryItemTimeEdit1.AutoHeight = false;
            this.repositoryItemTimeEdit1.DisplayFormat.FormatString = "mm:ss";
            this.repositoryItemTimeEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemTimeEdit1.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.None;
            this.repositoryItemTimeEdit1.Mask.EditMask = "";
            this.repositoryItemTimeEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Custom;
            this.repositoryItemTimeEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTimeEdit1.Name = "repositoryItemTimeEdit1";
            this.repositoryItemTimeEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // repositoryItemProgressBar1
            // 
            this.repositoryItemProgressBar1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemProgressBar1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemProgressBar1.Name = "repositoryItemProgressBar1";
            this.repositoryItemProgressBar1.StartColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            // 
            // repositoryItemPictureEdit1
            // 
            this.repositoryItemPictureEdit1.Name = "repositoryItemPictureEdit1";
            // 
            // ribbonStatusBar1
            // 
            this.ribbonStatusBar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ribbonStatusBar1.ItemLinks.Add(this.labelUser);
            this.ribbonStatusBar1.ItemLinks.Add(this.labelConnected);
            this.ribbonStatusBar1.ItemLinks.Add(this.labelDate);
            this.ribbonStatusBar1.Location = new System.Drawing.Point(0, 727);
            this.ribbonStatusBar1.Name = "ribbonStatusBar1";
            this.ribbonStatusBar1.Ribbon = this.ribbonControl1;
            this.ribbonStatusBar1.Size = new System.Drawing.Size(1280, 23);
            // 
            // defaultLookAndFeel1
            // 
            this.defaultLookAndFeel1.LookAndFeel.SkinName = "Blue";
            // 
            // xtraTabbedMdiManager1
            // 
            this.xtraTabbedMdiManager1.MdiParent = this;
            // 
            // labelLogo
            // 
            this.labelLogo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLogo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.labelLogo.ForeColor = System.Drawing.Color.Transparent;
            this.labelLogo.Image = ((System.Drawing.Image)(resources.GetObject("labelLogo.Image")));
            this.labelLogo.Location = new System.Drawing.Point(1215, 1);
            this.labelLogo.Name = "labelLogo";
            this.labelLogo.Size = new System.Drawing.Size(61, 23);
            this.labelLogo.TabIndex = 2;
            // 
            // barButtonItemExit
            // 
            this.barButtonItemExit.Caption = "barButtonItemExit";
            this.barButtonItemExit.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemExit.Glyph")));
            this.barButtonItemExit.Id = 1;
            this.barButtonItemExit.Name = "barButtonItemExit";
            this.barButtonItemExit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemExit_ItemClick);
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "barButtonItem3";
            this.barButtonItem3.Id = 20;
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "barButtonItem2";
            this.barButtonItem2.Id = 19;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Id = 18;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // CctvFrontClientFormDevX
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1280, 750);
            this.Controls.Add(this.ribbonStatusBar1);
            this.Controls.Add(this.labelLogo);
            this.Controls.Add(this.ribbonControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(1278, 726);
            this.Name = "CctvFrontClientFormDevX";
            this.Text = "SmartCAD - CCTV Module";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrontClientFormDevX_FormClosing);
            this.Load += new System.EventHandler(this.FrontClientFormDevX_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

		private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
		private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
		private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
        private DevExpress.XtraTabbedMdi.XtraTabbedMdiManager xtraTabbedMdiManager1;
		private System.Windows.Forms.Label labelLogo;
		private DevExpress.XtraBars.BarButtonItem barButtonItemOpenMap;
		private DevExpress.XtraBars.BarButtonItem barButtonItemChat;
		private DevExpress.XtraBars.BarButtonItem barButtonItemPrint;
		private DevExpress.XtraBars.BarButtonItem barButtonItemSave;
		private DevExpress.XtraBars.BarButtonItem barButtonItemRefresh;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupApplications;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupTools;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupGeneralOptions;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupCallInfo;
		private DevExpress.XtraBars.BarButtonItem barButtonItemCancelIncident;
		public DevExpress.XtraBars.BarButtonItem barButtonItemStartRegIncident;
		public DevExpress.XtraBars.BarButtonItem barButtonItemCreateNewIncident;
		private DevExpress.XtraBars.Alerter.AlertControl alertControl1;
		private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar repositoryItemProgressBar1;
		private DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit repositoryItemTimeEdit1;
        public DevExpress.XtraBars.BarButtonItem barButtonItemHelp;
        private DevExpress.XtraBars.BarButtonItem barButtonItemExit;
        private DevExpress.XtraBars.BarButtonItem barButtonItemHistory;
        private DevExpress.XtraBars.BarButtonItem barButtonItemAlarm;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        public DevExpress.XtraBars.BarButtonItem barButtonItemCancelVA_Alarm;
        
        private DevExpress.XtraBars.BarStaticItem labelConnected;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar1;
        private DevExpress.XtraBars.BarStaticItem labelUser;
        private DevExpress.XtraBars.BarStaticItem labelDate;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupAlarm;
    }
}