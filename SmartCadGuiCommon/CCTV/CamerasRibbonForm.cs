using System;
using System.Windows.Forms;
using SmartCadCore.Common;
using System.Collections.Generic;
using DevExpress.Utils;


using System.Collections;
using SmartCadCore.ClientData;
using SmartCadControls.Controls;
using Smartmatic.SmartCad.Service;
using SmartCadControls;
using SmartCadControls.Util;
using Smartmatic.SmartCad.Vms;
using SlimDX.DirectInput;


namespace SmartCadGuiCommon
{
	public partial class CamerasRibbonForm : DevExpress.XtraEditors.XtraForm
	{
        #region fields
        public Dictionary<int, float> speed = new Dictionary<int, float>();
        private bool Status = false;
        private string OutName = string.Empty;
        public VmsControlEx VMSControl = new VmsControlEx();
        public EventManager alarmManager;
        private Joystick Joy;
        private JoystickManager JoyMan;
        internal bool joyActive = false;
        #endregion

        public CamerasRibbonForm()
		{
			InitializeComponent();
        }
        
        #region Arrange Buttons
        private void barButtonItemCascade_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
			xtraTabbedMdiManager1.MdiParent = null;
			this.LayoutMdi(MdiLayout.Cascade);			
		}

		private void barButtonItemArrange_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
			xtraTabbedMdiManager1.MdiParent = this;
		}

		private void barButtonItemTileHorizontal_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
			xtraTabbedMdiManager1.MdiParent = null;
			LayoutMdi(MdiLayout.TileHorizontal);
		}

		private void barButtonItemTileVertical_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
			xtraTabbedMdiManager1.MdiParent = null;
			LayoutMdi(MdiLayout.TileVertical);
        }
        #endregion

        private void CamerasRibbonForm_Load(object sender, EventArgs e)
		{
            #region Joystick
            ConnectJoy();

            if (Joy != null)
            {
                joyActive = true;
            }
            JoyMan = new JoystickManager();
            JoyMan.Init();
            JoyMan.JoystickAddedEvent += new EventHandler(JoyMan_JoystickAddedEvent);
            JoyMan.JoystickRemovedEvent += new EventHandler(JoyMan_JoystickRemovedEvent);

            #endregion
            InitGrid();
            InitSpeed();
			LoadLanguage();
            InitImageList();
            OperatorClientData oper = ServerServiceClient.GetInstance().OperatorClient;
            ConfigurationClientData configuration = ServerServiceClient.GetInstance().GetConfiguration();
            if (configuration.VmsServerIp != "" && configuration.VmsDllName != "")
            {
                bool isLoad = false;
                this.alarmManager = EventManager.GetInstance(configuration.VmsDllName);

                if (oper != null)
                {
                    alarmManager.Initialize(configuration.VmsServerIp, oper.Login, oper.Password);
                    if (alarmManager.Connect() == 1)
                        isLoad = true;
                }

                if (isLoad == false)
                {
                    alarmManager.Initialize(configuration.VmsServerIp, configuration.VmsLogin, configuration.VmsPassword);
                    alarmManager.Connect();
                }

                this.alarmManager.Start();
                this.alarmManager.AlarmEventManager += new EventManager.AlarmEvent(alarmManager_AlarmEventManager);
            }
            else
                this.Close();

            System.Threading.Thread th = new System.Threading.Thread(new System.Threading.ThreadStart(CreateAlarms));
            th.Start();
        }

        private void CreateAlarms()
        {
            System.Threading.Thread.Sleep(30000);
            ClientEventData evento = new ClientEventData();
            evento.CameraId = "Camera 1 on Axis 214 Camera (10.0.13.92)";
            evento.EventTime = DateTime.Now;

            this.gridControlExEvents.AddOrUpdateItem(new GridControlDataEvents(evento));
        }

        void CamerasRibbonForm_FormClosing(object sender, System.Windows.Forms.FormClosingEventArgs e)
        {
            alarmManager.Stop();
        }

        void alarmManager_AlarmEventManager(object sender, EventManager.AlarmArgs args)
        {
            ClientEventData evento = new ClientEventData();
            evento.CameraId = args.AlarmData.CameraId;
            evento.DeviceEventName = args.AlarmData.DeviceEventName;
            evento.EventTime = args.AlarmData.EventTime;

            this.gridControlExEvents.AddOrUpdateItem(new GridControlDataEvents(evento));
        }

        void VMSControl_CInput(object sender, IOArgs args)
        {
            IOData data = (IOData)args.IData;
            foreach (DevExpress.XtraEditors.Controls.ImageListBoxItem item in this.imageListBoxControlInputs.Items)
            {
                if (data.Name.Equals(item.Value.ToString()))
                {
                    int index = 0;
                    index = this.imageListBoxControlInputs.SelectedIndex;
                    this.imageListBoxControlInputs.Items.Remove(item);
                    DevExpress.XtraEditors.Controls.ImageListBoxItem newItem = new DevExpress.XtraEditors.Controls.ImageListBoxItem();
                    newItem.Value = data.Name;
                    this.imageListBoxControlInputs.Items.Add(newItem, data.Status);
                    this.imageListBoxControlInputs.SelectedIndex = index;
                    break;
                }
            }
        }

        void VMSControl_COutput(object sender, IOArgs args)
        {
            IOData data = (IOData)args.IData;
            foreach (DevExpress.XtraEditors.Controls.ImageListBoxItem item in this.imageListBoxControlOutputs.Items)
            {
                if (data.Name.Equals(item.Value.ToString()))
                {
                    int index = 0;
                    index = this.imageListBoxControlOutputs.SelectedIndex;
                    this.imageListBoxControlOutputs.Items.Remove(item);
                    DevExpress.XtraEditors.Controls.ImageListBoxItem newItem = new DevExpress.XtraEditors.Controls.ImageListBoxItem();
                    newItem.Value = data.Name;
                    this.imageListBoxControlOutputs.Items.Add(newItem, data.Status);
                    this.imageListBoxControlOutputs.SelectedIndex = index;
                    break;
                }
            }
        }

        //private void ConnectJoy()
        //{
        //    DeviceList deviceList = Manager.GetDevices(DeviceType.Joystick, EnumDevicesFlags.AttachedOnly);
        //    DeviceInstance deviceInstance;
        //    for (int i = 0; i < deviceList.Count; i++)
        //    {
        //        deviceList.MoveNext();
        //        deviceInstance = (DeviceInstance)deviceList.Current;
        //        Joy = new JoystickDevice(new Device(deviceInstance.ProductGuid));
        //        JoystickState state = Joy.GetJoystickState();
        //        break;
        //    }
        //}

        private void ConnectJoy()
        {

            DirectInput input = new DirectInput();
            IList<DeviceInstance> deviceList = input.GetDevices(SlimDX.DirectInput.DeviceType.Joystick, DeviceEnumerationFlags.AttachedOnly);

            //DeviceList deviceList = Manager.GetDevices(DeviceType.Joystick, EnumDevicesFlags.AttachedOnly);

            DeviceInstance deviceInstance = null;
            for (int i = 0; i < deviceList.Count; i++)
            {
                //deviceList.MoveNext();
                deviceInstance = (DeviceInstance)deviceList[i];
                Joy = new Joystick(input, deviceInstance.ProductGuid);
                SlimDX.DirectInput.JoystickState state = Joy.GetCurrentState();
                break;
            }
        }

        void JoyMan_JoystickRemovedEvent(object sender, EventArgs e)
        {
            Joy.Dispose();
            joyActive = false;
        }

        void JoyMan_JoystickAddedEvent(object sender, EventArgs e)
        {
            ConnectJoy();
            joyActive = true;

        }

        private void tmrUpdateStick_Tick(object sender, EventArgs e)
        {
            //if (joyActive)
            //{
            //    ArrayList joystickAxisList = Joy.JoystickAxisList as ArrayList;
            //    int X = 0;
            //    int Y = 0;
            //    int Z = 0;
            //    foreach (JoystickAxisInput input in joystickAxisList)
            //    {
            //        int value = input.Value;
            //        #region AxisX
            //        if (input.ToString() == "AxisX")
            //        {
            //            X = value;
            //        }
            //        #endregion
            //        #region AxisY
            //        if (input.ToString() == "AxisY")
            //        {
            //            Y = value;
            //        }
            //        #endregion
            //        #region AxisZ
            //        if (input.ToString() == "AxisZ")
            //        {
            //            Z = value;
            //        }
            //        #endregion
            //    }
            //    VMSControl.PTZMove(X, Y, Z);
            //}
        }

		private void LoadLanguage()
		{
			barButtonItemArrange.Caption = ResourceLoader.GetString2("Arrange");
			barButtonItemCascade.Caption = ResourceLoader.GetString2("Cascade");
			barButtonItemTileHorizontal.Caption = ResourceLoader.GetString2("TileHorizontal");
			barButtonItemTileVertical.Caption = ResourceLoader.GetString2("TileVertical");
			Text = ResourceLoader.GetString2("Cameras");
            this.ribbonPage1.Text = ResourceLoader.GetString2("ViewWindows");
            this.ribbonPageGroup1.Text = ResourceLoader.GetString2("WindowsOrder");
            #region live feed
            this.groupControlPanTiltZoom.Text = ResourceLoader.GetString2("PTZControls");
            this.dockPanelControls.Text = ResourceLoader.GetString2("Management");
            this.groupControlAudioIN.Text = ResourceLoader.GetString2("AudioControlIN");
            this.muteCheckBox.Text = ResourceLoader.GetString2("Mute");
            this.dockPanelEvents.Text = ResourceLoader.GetString2("DeviceEventsName");
            this.groupControlInput.Text = ResourceLoader.GetString2("groupControlInput");
            this.groupControlOutput.Text = ResourceLoader.GetString2("groupControlOutput");
            this.simpleButtonOutput.Text = ResourceLoader.GetString2("OutputOn");
            this.labelControlVolume.Text = ResourceLoader.GetString2("Volume");
            #endregion
            #region playback
            this.dockPanelPlayback.Text = ResourceLoader.GetString2("Playback");
            this.labelControlPlaybackSpeed.Text = ResourceLoader.GetString2("Speed") + " : " + speed[4].ToString() + "x";
            this.groupControlPlayback.Text = ResourceLoader.GetString2("PlaybackControls");
            this.groupControl1AudioPlayback.Text = ResourceLoader.GetString2("AudioControlIN");
            this.labelControlVulmePlayback.Text = ResourceLoader.GetString2("Volume");
            this.checkBoxMutePlayback.Text = ResourceLoader.GetString2("Mute");
            this.groupControl1.Text = ResourceLoader.GetString2("ExportButton");
            this.simpleButtonExport.Text = ResourceLoader.GetString2("ExportButton");
            #endregion
		}

        private void InitGrid()
        {
            gridControlExEvents.EnableAutoFilter = true;

            gridControlExEvents.Type = typeof(GridControlDataEvents);
            gridControlExEvents.ViewTotalRows = true;
            gridViewExEvents.ViewTotalRows = true;
            gridControlExEvents.AllowDrop = false;
            gridViewExEvents.FocusedRowHandle = GridControlEx.AutoFilterRowHandle;
            gridViewExEvents.SelectRow(GridControlEx.AutoFilterRowHandle);

        }

        private void InitSpeed()
        {
            speed.Add(0, 0);
            speed.Add(1, 0.25f);
            speed.Add(2, 0.50f);
            speed.Add(3, 0.75f);
            speed.Add(4, 1.00f);
            speed.Add(5, 2.00f);
            speed.Add(6, 5.00f);
            speed.Add(7, 10.00f);
            speed.Add(8, 20.00f);
        }

        private void InitImageList()
        {
            ImageList images = new ImageList();
            images.Images.Add(ResourceLoader.GetIcon("$Icon.IOReady"));
            images.Images.Add(ResourceLoader.GetIcon("$Icon.IOInuse"));
            this.imageListBoxControlInputs.ImageList = images;
            this.imageListBoxControlOutputs.ImageList = images;
        }

        private void dockManager_Sizing(object sender, DevExpress.XtraBars.Docking.SizingEventArgs e)
        {
            e.Cancel = true;
        }

        private void PTZControlClicked(object sender, EventArgs e)
        {
            string controlName = ((Control)sender).Name;
            VmsControlEx.PtzMoveRelativeTypes moveTo = VmsControlEx.PtzMoveRelativeTypes.Down;

            if (controlName.Equals(simpleButtonUpperLeft.Name))
            {
                moveTo = VmsControlEx.PtzMoveRelativeTypes.UpLeft;
            }
            else if (controlName.Equals(simpleButtonUpper.Name))
            {
                moveTo = VmsControlEx.PtzMoveRelativeTypes.Up;
            }
            else if (controlName.Equals(simpleButtonUpperRight.Name))
            {
                moveTo = VmsControlEx.PtzMoveRelativeTypes.UpRight;
            }
            else if (controlName.Equals(simpleButtonMiddleLeft.Name))
            {
                moveTo = VmsControlEx.PtzMoveRelativeTypes.Left;
            }
            else if (controlName.Equals(simpleButtonMiddle.Name))
            {
                moveTo = VmsControlEx.PtzMoveRelativeTypes.Home;
            }
            else if (controlName.Equals(simpleButtonMiddleRight.Name))
            {
                moveTo = VmsControlEx.PtzMoveRelativeTypes.Right;
            }
            else if (controlName.Equals(simpleButtonBottomLeft.Name))
            {
                moveTo = VmsControlEx.PtzMoveRelativeTypes.DownLeft;
            }
            else if (controlName.Equals(simpleButtonBottom.Name))
            {
                moveTo = VmsControlEx.PtzMoveRelativeTypes.Down;
            }
            else if (controlName.Equals(simpleButtonBottomRight.Name))
            {
                moveTo = VmsControlEx.PtzMoveRelativeTypes.DownRight;
            }
            else if (controlName.Equals(simpleButtonZoomIn.Name))
            {
                moveTo = VmsControlEx.PtzMoveRelativeTypes.ZoomIn;
            }
            else if (controlName.Equals(simpleButtonZoomOut.Name))
            {
                moveTo = VmsControlEx.PtzMoveRelativeTypes.ZoomOut;
            }
            if (!string.IsNullOrEmpty(controlName))
            {
                VMSControl.Ptz_do(moveTo);
            }

        }

        public virtual void muteCheckBox_CheckedChanged(object sender, EventArgs e)
        {
             VMSControl.mute(this.muteCheckBox.Checked);
        }

        private void trackBarControlVolume_ValueChanged(object sender, EventArgs e)
        {
            VMSControl.volume(trackBarControlVolume.Value);
        }

        private void simpleButtonSpeak_MouseDown(object sender, MouseEventArgs e)
        {
            this.simpleButtonSpeak.Image = ResourceLoader.GetImage("$Image.talking");
           // VMSControl.speaking(true);
        }

        private void simpleButtonSpeak_MouseUp(object sender, MouseEventArgs e)
        {
           // VMSControl.speaking(false);
            this.simpleButtonSpeak.Image = ResourceLoader.GetImage("$Image.nottalking");
        }

        private void imageListBoxControlInputs_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.imageListBoxControlInputs.SelectedIndex != -1 && this.imageListBoxControlInputs.Items.Count > 0)
            {
                SuperToolTip SuperTip = new SuperToolTip();
                ToolTipItem item = new ToolTipItem();
                ToolTipTitleItem title = new ToolTipTitleItem();
                title.Text = ResourceLoader.GetString2("Input" + " :");
                item.Text = this.imageListBoxControlInputs.SelectedItem.ToString();
                SuperTip.Items.Add(title);
                SuperTip.Items.Add(item);
                this.imageListBoxControlInputs.SuperTip = SuperTip;
            }
        }

        private void imageListBoxControlOutputs_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.imageListBoxControlOutputs.SelectedIndex != -1 && this.imageListBoxControlOutputs.Items.Count > 0)
            {
                SuperToolTip SuperTip = new SuperToolTip();
                ToolTipItem item = new ToolTipItem();
                ToolTipTitleItem title = new ToolTipTitleItem();
                title.Text = ResourceLoader.GetString2("Output" + " :");
                item.Text = this.imageListBoxControlOutputs.SelectedItem.ToString();
                SuperTip.Items.Add(title);
                SuperTip.Items.Add(item);
                this.imageListBoxControlOutputs.SuperTip = SuperTip;

                DevExpress.XtraEditors.Controls.ImageListBoxItem itemimg = new DevExpress.XtraEditors.Controls.ImageListBoxItem();
                itemimg = (DevExpress.XtraEditors.Controls.ImageListBoxItem)this.imageListBoxControlOutputs.SelectedItem;
                if (itemimg.ImageIndex == 0)
                {
                    this.simpleButtonOutput.Text = ResourceLoader.GetString2("OutputOn");
                    Status = true;
                    OutName = itemimg.Value.ToString();
                }
                else
                {
                    this.simpleButtonOutput.Text = ResourceLoader.GetString2("OutputOff");
                    Status = false;
                    OutName = itemimg.Value.ToString();
                }
            }
        }

        private void zoomTrackBarControlSpeed_EditValueChanged(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.ZoomTrackBarControl control = (DevExpress.XtraEditors.ZoomTrackBarControl)sender;
            if (control.Value == 0)
                this.labelControlPlaybackSpeed.Text = ResourceLoader.GetString2("Pause");
            else
            {
                this.labelControlPlaybackSpeed.Text = ResourceLoader.GetString2("Speed") + " : " + speed[control.Value].ToString() + "x";
            }
            this.VMSControl.SetSpeedPlayback(speed[control.Value]);
        }

        private void dockPanelControls_Enter(object sender, EventArgs e)
        {
            this.VMSControl.Playmode(PlayModes.Live);
            this.dockPanelEvents.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Visible;
            this.trackBarControlVolume.Value = this.VMSControl.GetVolume();
            this.muteCheckBox.Checked = this.VMSControl.GetMute();
        }

        private void dockPanelPlayback_Enter(object sender, EventArgs e)
        {
            try
            {
                this.VMSControl.Playmode(PlayModes.PlayBack_MultiSequences);
                this.trackBarControlPlayback.Value = this.VMSControl.GetVolume();
                this.checkBoxMutePlayback.Checked = this.VMSControl.GetMute();


                this.dateEditplayback.DateTime = DateTime.Now.AddMinutes(-5);
                this.VMSControl.SetPlaybackDate(this.dateEditplayback.DateTime);
                this.simpleButtonGoto.PerformClick();
                this.checkButtonBack.Checked = false;
                this.checkButtonPlay.Checked = false;
                this.dockPanelEvents.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            }
            catch { }
        }

        private void simpleButtonStop_Click(object sender, EventArgs e)
        {
            if (this.checkButtonPlay.Checked)
                this.checkButtonPlay.Checked = false;

            if (this.checkButtonBack.Checked)
                this.checkButtonBack.Checked = false;

            this.VMSControl.StopPlayback();
        }

        private void checkButtonPlay_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.checkButtonPlay.Checked)
            {
                this.VMSControl.SetPlayDirection(0);
                this.checkButtonBack.Checked = false;
            }
            else
            {
                this.simpleButtonStop.PerformClick();
            }

        }

        private void checkButtonBack_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.checkButtonBack.Checked)
            {
                this.VMSControl.SetPlayDirection(1);
                this.checkButtonPlay.Checked = false;
            }
            else
            {
                this.simpleButtonStop.PerformClick();
            }
        }

        private void simpleButtonGoto_Click(object sender, EventArgs e)
        {
            this.simpleButtonStop.PerformClick();
            this.VMSControl.SetPlaybackDate(this.dateEditplayback.DateTime);
        }

        void CamerasRibbonForm_Deactivate(object sender, System.EventArgs e)
        {
            this.tmrUpdateStick.Enabled = false;
            
            if (VMSControl != null)
                this.VMSControl.PTZMove(5000, 5000, 5000);
        }

        void CamerasRibbonForm_Activated(object sender, System.EventArgs e)
        {
            if (this.VMSControl != null)
                if (this.VMSControl.feed)
                {
                    this.tmrUpdateStick.Enabled = true;
                }
        }

        internal void eventsuscribe()
        {
            this.VMSControl.CInput += new VmsControlEx.ChangeInput(VMSControl_CInput);
            this.VMSControl.COutput += new VmsControlEx.ChangeOutput(VMSControl_COutput);
        }

        private void simpleButtonOutput_Click(object sender, EventArgs e)
        {
            this.VMSControl.TriggerOutput(Status, OutName);
        }

        private void simpleButtonExport_Click(object sender, EventArgs e)
        {
            //ExportVideo form = new ExportVideo(VMSControl);
            //form.ShowDialog();
        }

        private void barButtonItemStartIncident_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }



    }
    
}