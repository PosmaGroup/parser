
using DevExpress.Utils;
using DevExpress.XtraEditors;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using Smartmatic.SmartCad.Gui;
using Smartmatic.SmartCad.Service;
using Smartmatic.SmartCad.Vms;
using System;
using System.Collections;
using System.IO;
using System.Xml;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using System.Collections;
using Smartmatic.SmartCad.Service;
using System.Globalization;
using SmartCadCore.Common;
using Smartmatic.SmartCad.Vms;
using SmartCadCore.ClientData;
using SmartCadGuiCommon.Classes;
using SmartCadFirstLevel.Gui;
using SmartCadGuiCommon.CCTV.Nodes;
using SmartCadGuiCommon.Controls;
using SmartCadGuiCommon.Util;

namespace SmartCadGuiCommon
{
    public partial class PlayBackForm : DevExpress.XtraEditors.XtraForm
    {

        #region Fields

        private static readonly DateTime _dt1970 = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
        private ServerServiceClient serverServiceClient;
        private CameraClientData camera = new CameraClientData();
        public Dictionary<int, float> speed = new Dictionary<int, float>();
        ConfigurationClientData config = null;
        public CctvNode selectedRow = null;
        public VmsSingletonUtil VmsControl;
        public VmsSingletonPlaybackUtil VmsControlPlayBack;

        IPlayBackPanel currentPanel;
        public IPlayBackPanel CurrentPanel
        {
            get { return currentPanel; }
            set
            {
                currentPanel = value;
                panelControl1.Controls.Clear();
                panelControl1.Controls.Add(currentPanel as Control);
                ((Control)currentPanel).Dock = DockStyle.Fill;
                currentPanel.DateTimeValueChanged += CurrentPanel_DateTimeValueChanged;
                ((Control)currentPanel).Disposed += CurrentPanel_Disposed;
            }
        }

        void CurrentPanel_Disposed(object sender, EventArgs e)
        {
            currentPanel = null;
            simpleButtonGoto.Enabled = false;
            simpleButtonGoto.Enabled = true;
        }

        #endregion

        System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PlayBackForm));

        #region Public Methods

        public ServerServiceClient ServerServiceClient
        {
            get
            {
                return serverServiceClient;
            }
            set
            {
                serverServiceClient = value;
            }
        }

        #endregion

        public PlayBackForm()
        {
            InitializeComponent();
            InitGrid();
            LoadLanguage();
            InitSpeed();
            DateEditStartDate.EditValue = DateTime.Now.Subtract(new TimeSpan(0, 1, 0));
            labelVideoTime.Text = DateEditStartDate.EditValue.ToString();
            DateEditEndDate.EditValue = DateTime.Now;
            config = ServerServiceClient.GetInstance().GetConfiguration();
            VmsControl = new VmsSingletonUtil();
            VmsControlPlayBack = new VmsSingletonPlaybackUtil();
        }

        private void InitGrid()
        {
            gridControlCameras.EnableAutoFilter = true;
            gridControlCameras.AllowDrop = true;
            gridViewCameras.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            gridViewCameras.OptionsView.ShowAutoFilterRow = true;
            gridViewCameras.OptionsCustomization.AllowFilter = false;

            gridControlTemplates.EnableAutoFilter = true;
            gridViewTemplates.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            gridViewTemplates.OptionsView.ShowAutoFilterRow = true;
            gridViewTemplates.OptionsCustomization.AllowFilter = false;
        }


        private void LoadLanguage() 
        {
            //dockPanelControls.Text = ResourceLoader.GetString2("CameraPlaybackForm");
            Text = ResourceLoader.GetString2("CameraPlaybackForm");
            groupControlAudioControl.Text = ResourceLoader.GetString2("VideoControls");
            groupControlPlayback.Text = ResourceLoader.GetString2("PlaybackControls");
            simpleButtonStop.ToolTipTitle = ResourceLoader.GetString2("StopVideo");
            checkButtonPlay.ToolTipTitle = ResourceLoader.GetString2("StartVideo");
            checkButtonBack.ToolTipTitle = "Rewind";
            lbStartDate.Text = ResourceLoader.GetString2("StartDate");
            lbEndDate.Text = ResourceLoader.GetString2("EndDate");
            groupControlExport.Text = ResourceLoader.GetString2("ExportButton");
            simpleButtonExport.Text = ResourceLoader.GetString2("ExportButton");
            simpleButtonExport.ToolTipTitle = ResourceLoader.GetString2("ExportVideo");

            barButtonItemPrint.Caption = ResourceLoader.GetString2("Print");
            barButtonItemRefresh.Caption = ResourceLoader.GetString2("Refresh");
            barButtonItemSave.Caption = ResourceLoader.GetString2("Save");
            ribbonPageGroupGeneralOptions.Text = ResourceLoader.GetString2("GeneralOptions");
            ribbonPage1.Text = ResourceLoader.GetString2("Cctv");
            barButtonItemCancelTelemetryAlarm.Caption = ResourceLoader.GetString2("Dismiss");
            ribbonPageGroupAlarm.Text = ResourceLoader.GetString2("Alarms");

        }

        public void LoadInitialData()
        {
            #region Load Cameras
            LoadCameras();
            #endregion

            #region Load Camera Templates
            BindingList<CctvTemplateNode> templates = new BindingList<CctvTemplateNode>();
            foreach (CctvTemplateNode template in ((CctvFrontClientFormDevX)this.MdiParent).Templates)
            {
                CctvTemplateNode newT = new CctvTemplateNode(template.Name, template.Text, template.RenderMethod);
                foreach (CctvCameraNode camera in template.Cameras)
                {
                    newT.Cameras.Add(new CctvCameraNode(camera.Camera));
                }
                templates.Add(newT);
            }
            gridControlTemplates.BeginUpdate();
            gridControlTemplates.DataSource = templates;
            gridControlTemplates.EndUpdate();
            #endregion
        }

        public void LoadCameras()
        {
            BindingList<CctvCameraNode> cameras = new BindingList<CctvCameraNode>();
            foreach (CctvCameraNode camera in ((CctvFrontClientFormDevX)this.MdiParent).Cameras)
            {
                cameras.Add(new CctvCameraNode(camera.Camera));
            }
            gridControlCameras.BeginUpdate();
            gridControlCameras.DataSource = cameras;
            gridControlCameras.EndUpdate();
        }
        private void InitSpeed()
        {
            speed.Add(0, 0);
            speed.Add(1, 0.25f);
            speed.Add(2, 0.50f);
            speed.Add(3, 0.75f);
            speed.Add(4, 1.00f);
            speed.Add(5, 2.00f);
            speed.Add(6, 5.00f);
            speed.Add(7, 10.00f);
            speed.Add(8, 20.00f);

            zoomTrackBarControlSpeed.Value = 4;
        }

       
        private void PlayBackForm_Load(object sender, EventArgs e)
        {
        }

        #region Video Playback Button Controls

        void CurrentPanel_DateTimeValueChanged(DateTime dateTime)
        {
            labelVideoTime.Text = dateTime.ToShortDateString() + " " + dateTime.ToLongTimeString();
            //if (dateTime.CompareTo(DateEditEndDate.DateTime) > -1)
            //    simpleButtonStop_Click(null, null);
            if (DateEditStartDate.DateTime.CompareTo(dateTime) > 0)
                simpleButtonStop_Click(null, null);
        }

        public void SetPlayBackSpeed()
        {

        }

        public void StartPanel()
        {
            if (selectedRow is CctvCameraNode)
            {
                // CurrentPanel.DisposePanelControl();
                CurrentPanel = new CameraPanelControl(((CctvCameraNode)selectedRow).Camera, PlayModes.PlayBack_SingleSequence, config, false, DateEditStartDate.DateTime, DateEditEndDate.DateTime, VmsControl);
            }
            else if (selectedRow is CctvTemplateNode)
            {
            //    if (CurrentPanel == null || ((CctvTemplateNode)selectedRow).Name != CurrentPanel.Name)
                    CurrentPanel = new TemplatePanelControl((CctvTemplateNode)selectedRow, PlayModes.PlayBack_SingleSequence, config);
            }
            CurrentPanel.PlayDirection = 0;
            CurrentPanel.PlaySpeed = 0;
            CurrentPanel.StopPlayBack();//speed[zoomTrackBarControlSpeed.Value];
            CurrentPanel.StartDate = DateEditStartDate.DateTime;
        }


        private void simpleButtonGoto_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            simpleButtonStop_Click(null, null);

            if (selectedRow is CctvCameraNode)
            {
                //if (/*CurrentPanel == null ||*/ ((CctvCameraNode)selectedRow).Camera.Name != CurrentPanel.Name)

                if (VmsControl.vmsControl != null)
                {
                    VmsControl.vmsControl.PlaybackDate = DateEditStartDate.DateTime;
                    VmsControl.vmsControl.EndPlayBackDate = DateEditEndDate.DateTime;
                }

                CurrentPanel = new CameraPanelControl(((CctvCameraNode)selectedRow).Camera, PlayModes.PlayBack_SingleSequence, config, false, DateEditStartDate.DateTime, DateEditEndDate.DateTime, VmsControlPlayBack);

            }
            else if (selectedRow is CctvTemplateNode)
            {
           //     if (CurrentPanel == null || ((CctvTemplateNode)selectedRow).Name != CurrentPanel.Name)
                if (VmsControl.vmsControl != null)
                {
                    VmsControl.vmsControl.PlaybackDate = DateEditStartDate.DateTime;
                    VmsControl.vmsControl.EndPlayBackDate = DateEditEndDate.DateTime;
                }
                CurrentPanel = new TemplatePanelControl((CctvTemplateNode)selectedRow, PlayModes.PlayBack_SingleSequence, config);
            }
            else
            {
                this.Cursor = Cursors.Default;
                return;
            }

            //barButtonItemPrint.Enabled = true;
            barButtonItemSave.Enabled = true;
            simpleButtonGoto.Enabled = true;
            this.Cursor = Cursors.Default;

            CurrentPanel.PlayDirection = 0;
            CurrentPanel.PlaySpeed = 0;
            CurrentPanel.StartDate = DateEditStartDate.DateTime;
            CurrentPanel.EndDate = DateEditEndDate.DateTime;
            CurrentPanel.StopPlayBack();//speed[zoomTrackBarControlSpeed.Value];

            
            simpleButtonStop.Enabled = true;
            checkButtonMute.Enabled = true;
            trackBarControlVolume.Enabled = true;
            zoomTrackBarControlSpeed.Enabled = true;


            labelVideoTime.Text = DateEditStartDate.DateTime.ToShortDateString() + " " + DateEditStartDate.DateTime.ToLongTimeString();
            this.Cursor = Cursors.Default;
            simpleButtonExport.Enabled = true;
        }

        private void simpleButtonExport_Click(object sender, EventArgs e)
        {
            if (CurrentPanel != null)
            {
                ExportVideo form = new ExportVideo(CurrentPanel, DateEditStartDate.DateTime, DateEditEndDate.DateTime);
                form.ShowDialog();

            }
        }

        private void simpleButtonPlay_Click(object sender, EventArgs e)
        {
            if (checkButtonPlay.Checked)
            {
                checkButtonPlay.Image = global::SmartCadGuiCommon.Properties.Resources.Play;
                checkButtonBack.Enabled = true;
                checkButtonBack.Checked = false;

                CurrentPanel.StopPlayBack();
                CurrentPanel.StartDate = DateTime.Parse(labelVideoTime.Text);
            }
            else
            {
                checkButtonPlay.Image = global::SmartCadGuiCommon.Properties.Resources.pausa;
                checkButtonBack.Enabled = false;
                checkButtonBack.Checked = false;

                CurrentPanel.PlayDirection = 0;
                try
                {
                    CurrentPanel.StartDate = DateTime.Parse(labelVideoTime.Text);
                    CurrentPanel.StartPlayBack();

                }
                catch { }
            }
        }

        private void simpleButtonBack_Click(object sender, EventArgs e)
        {
            if (checkButtonBack.Checked)
            {
                checkButtonBack.Image = ((System.Drawing.Image)(resources.GetObject("checkButtonBack.Image")));
                checkButtonPlay.Enabled = true;
                checkButtonPlay.Checked = false;

                CurrentPanel.StopPlayBack();
                CurrentPanel.StartDate = DateTime.Parse(labelVideoTime.Text);
            }
            else
            {
                checkButtonBack.Image = global::SmartCadGuiCommon.Properties.Resources.pausa;
                checkButtonPlay.Enabled = false;
                checkButtonPlay.Checked = false;
                CurrentPanel.PlayDirection = 1;
                CurrentPanel.StartPlayBack();
            }
        }

        private void simpleButtonStop_Click(object sender, EventArgs e)
        { 
            checkButtonPlay.Checked = false;
            checkButtonBack.Checked = false;
            checkButtonPlay.Enabled = true;
            checkButtonBack.Enabled = true;
            checkButtonPlay.Image = global::SmartCadGuiCommon.Properties.Resources.Play;
            checkButtonBack.Image = ((System.Drawing.Image)(resources.GetObject("checkButtonBack.Image")));

            labelVideoTime.Text = DateEditStartDate.DateTime.ToShortDateString() + " " + DateEditStartDate.DateTime.ToLongTimeString();
            if (CurrentPanel != null)
            {
                CurrentPanel.StopPlayBack();
                CurrentPanel.EndDate = DateEditEndDate.DateTime;
                CurrentPanel.StartDate = DateEditStartDate.DateTime;

            }
        }

        #endregion

        private void gridViewCameras_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (gridViewCameras.SelectedRowsCount > 0)
            {
                simpleButtonGoto.Enabled = true;
                selectedRow = gridViewCameras.GetFocusedRow() as CctvNode;
            }
            else
                simpleButtonGoto.Enabled = false;
        }

        private void gridViewTemplates_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (gridViewTemplates.SelectedRowsCount > 0)
            {
                simpleButtonGoto.Enabled = true;
                selectedRow = gridViewTemplates.GetFocusedRow() as CctvNode;
            }
            else
                simpleButtonGoto.Enabled = false;
        }

        private void zoomTrackBarControlSpeed_EditValueChanged(object sender, EventArgs e)
        {
            ZoomTrackBarControl control = (DevExpress.XtraEditors.ZoomTrackBarControl)sender;
            if (control.Value == 0)
                this.labelControlPlaybackSpeed.Text = ResourceLoader.GetString2("Pause");
            else
            {
                this.labelControlPlaybackSpeed.Text = ResourceLoader.GetString2("Speed") + " : " + speed[control.Value].ToString() + "x";
            }
           CurrentPanel.PlaySpeed = speed[control.Value];
        }

        private void trackBarControlPlayback_EditValueChanged(object sender, EventArgs e)
        {
           CurrentPanel.Volume = trackBarControlVolume.Value;
        }

        private void checkButtonMute_CheckedChanged(object sender, EventArgs e)
        {
            CurrentPanel.Mute = checkButtonMute.Checked;
            if (checkButtonMute.Checked)
            {
                this.checkButtonMute.Image = global::SmartCadGuiCommon.Properties.Resources.soundMuteSmall;
            }
            else
            {
                this.checkButtonMute.Image = global::SmartCadGuiCommon.Properties.Resources.soundSmall;
            }
        }

        private void PlayBackForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
            }
        }

        private void simpleButtonGoto_EnabledChanged(object sender, EventArgs e)
        {
            if (simpleButtonGoto.Enabled == false)
            {
                checkButtonPlay.Checked = false;
                checkButtonBack.Checked = false;
                checkButtonPlay.Enabled = false;
                checkButtonBack.Enabled = false;
                simpleButtonStop.Enabled = false;
                checkButtonMute.Enabled = false;
                trackBarControlVolume.Enabled = false;
                zoomTrackBarControlSpeed.Enabled = false;
                checkButtonPlay.Image = global::SmartCadGuiCommon.Properties.Resources.Play;
                checkButtonBack.Image = ((System.Drawing.Image)(resources.GetObject("checkButtonBack.Image")));

                labelVideoTime.Text = DateEditStartDate.DateTime.ToShortDateString() + " " + DateEditStartDate.DateTime.ToLongTimeString();
            }
        }

        private void gridControlCameras_Enter(object sender, EventArgs e)
        {
            selectedRow = gridViewCameras.GetFocusedRow() as CctvNode;
        }

        private void gridControlTemplates_Enter(object sender, EventArgs e)
        {
            selectedRow = gridViewTemplates.GetFocusedRow() as CctvNode;
        }

        private void gridControlTemplates_Click(object sender, EventArgs e)
        {
            selectedRow = gridViewTemplates.GetFocusedRow() as CctvNode;
        }

        private void barButtonItemPrint_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            
        }

        private void barButtonItemSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            SaveFileDialog fDialog = new SaveFileDialog();
            fDialog.Filter = "AVI Video (*.avi)|*.avi|JPEG Image (*.jpg)|*.jpg";
            fDialog.FilterIndex = 1;
            if (fDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK && fDialog.FileName != "")
            {
                ///aqui invoco la exportacion

                CurrentPanel.ExportVideo(DateEditStartDate.DateTime, DateEditEndDate.DateTime, fDialog.FileName,
                    fDialog.FilterIndex-1, 0);
            }
        }

        internal void DeleteTemplate(CctvTemplateNode template)
        {
            gridControlTemplates.BeginUpdate();
            ((BindingList<CctvTemplateNode>)gridControlTemplates.DataSource).Remove(template);
            gridControlTemplates.EndUpdate();
        }

        internal void AddNewTemplate(CctvTemplateNode template)
        {
            gridControlTemplates.BeginUpdate();
            ((BindingList<CctvTemplateNode>)gridControlTemplates.DataSource).Add(template);
            gridControlTemplates.EndUpdate();
        }
    }
}