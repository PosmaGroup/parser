﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Collections;
using Smartmatic.SmartCad.Service;
using DevExpress.XtraEditors.Repository;
using System.IO;
using SmartCadControls.Controls;
using SmartCadCore.Common;
using SmartCadCore.ClientData;
using SmartCadControls;
using SmartCadCore.Core;

namespace SmartCadGuiCommon.SyncBoxes
{
    public class GridControlDataOperator : GridControlData
    {
        private double []percentageSchedule;
        private static Image noImage = ResourceLoader.GetImage("$Image.Transparent");
        private string scheduleName = string.Empty;

        public GridControlDataOperator(OperatorClientData oper)
            : base(oper)
        {
            //Inicialmente es 100%, color verde
            percentageSchedule = new double[2] {0, 3};
        }

        private static string supervisedApplicationName;

        public static string SupervisedApplicationName
        {
            get { return supervisedApplicationName; }
            set { supervisedApplicationName = value; }
        }

        public OperatorClientData OperatorClient
        {
            get
            {
                return this.Tag as OperatorClientData;
            }
        }

        [GridControlRowInfoData(Visible = true, Width = 80,
            RepositoryType = typeof(RepositoryItemPictureEdit), Searchable = false)]
        public Image Photo
        {
            get
            {
                Image photo = noImage;
                if (OperatorClient.Picture != null && OperatorClient.Picture.Length > 0)
                {
                    try
                    {
                        MemoryStream ms = new MemoryStream(OperatorClient.Picture);
                        photo = Image.FromStream(ms);
                    }
                    catch
                    { }
                }
                return photo;
            }
        }

        [GridControlRowInfoData(Visible = true, Width = 80, Searchable = true)]
        public string FullFirstName
        {
            get
            {
                return OperatorClient.FirstName;
            }
        }

        [GridControlRowInfoData(Visible = true, Width = 80, Searchable = true)]
        public string FullLastName
        {
            get
            {
                return OperatorClient.LastName;
            }
        }

        [GridControlRowInfoData(Visible = false, Searchable = true)]
        public string OperatorStatus
        {
            get
            {
                return this.Status;
            }
        }

        [GridControlRowInfoData(Visible = true, Width = 80, Searchable = true)]
        public string Status
        {
            get
            {
                return ApplicationUtil.GetOperatorStatus(OperatorClient,supervisedApplicationName).StatusFriendlyName;
            }
        }

        /// <summary>
        /// Es un campo tipo arreglo de dos posiciones:
        /// La posicion 0 es el porcentaje de adherencia al cronograma y 
        /// la posicion 1 es el indice de la imagen en el imageList a mostrar como semaforo.
        /// Para ello 0 es sin imagen, 1 es verde, 2 amarillo y 3 rojo.
        /// </summary>
        public double[] PercentageSchedule
        {
            get
            {
                return percentageSchedule;
            }
            set
            {
                percentageSchedule = value;
            }
        }

        [GridControlRowInfoData(Visible = true, Width = 80, MeasureUnit = "%", RealFieldName = "PercentageSchedule",
            RepositoryType = typeof(RepositoryItemImageComboBox), Searchable = false)]
        public int PercentageScheduleCode
        {
            get
            {
                return this.Tag.Code;
            }
        }

        [GridControlRowInfoData(Visible = true, Width = 80, Searchable = true)]
        public string Room
        {
            get
            {
                return ((OperatorClientData)this.Tag).RoomName;
            }
        }

        [GridControlRowInfoData(Visible = true, Width = 80, Searchable = true)]
        public string SitNumber
        {
            get
            {
                string sit = string.Empty;
                if (OperatorClient.LastSessions != null)
                {
                    SessionHistoryClientData lastSession = ApplicationUtil.GetLastSession(OperatorClient.LastSessions, SupervisedApplicationName);
                    if (lastSession != null && lastSession.IsLoggedIn.Value == true)
                    {
                        sit = lastSession.NumberSeat;
                    }
                }
                return sit;
            }
        }

        [GridControlRowInfoData(Visible = true, Width = 80, Searchable = true)]
        public string Category
        {
            get
            {
                OperatorCategoryHistoryClientData category = new OperatorCategoryHistoryClientData();
                if (OperatorClient.CategoryList != null)
                {
                    bool found = false;
                    for (int i = 0; i < OperatorClient.CategoryList.Count && found == false; i++)
                    {
                        OperatorCategoryHistoryClientData ochcd = (OperatorCategoryHistoryClientData)OperatorClient.CategoryList[i];
                        if (ochcd.EndDate == DateTime.MinValue)
                        {
                            category = ochcd;
                            found = true;
                        }
                    }
                }
                return category.CategoryFriendlyName;
            }
        }

        [GridControlRowInfoData(Visible = true, Width = 80, Searchable = true)]
        public string Schedule
        {
            get
            {
                DateTime now = ServerServiceClient.GetInstance().GetTime();
                bool flag = false;
                try
                {
                    foreach (WorkShiftVariationClientData WorkS in OperatorClient.WorkShifts)
                    {
                        foreach (WorkShiftScheduleVariationClientData Schedule in WorkS.Schedules)
                            if (Schedule.Start <= now && now <= Schedule.End)
                            {
                                scheduleName = (string)WorkS.Name;
                                flag = true;
                                break;
                            }
                        if (flag)
                            break;
                    }
                }
                catch
                {
                    scheduleName = string.Empty;
                }

                if (string.IsNullOrEmpty(scheduleName) == true)
                    scheduleName = string.Empty;

                return scheduleName;
            }
            set 
            {
                scheduleName = value;
            }
        }

        [GridControlRowInfoData(Visible = false, Width = 80, Searchable = true)]
        public string Supervisor
        {
            get
            {
                OperatorAssignClientData assign = GetCurrentAssignedSupervisor();
                if (assign != null)
                    return assign.SupFirstName + " " + assign.SupLastName;
                return string.Empty;
            }
        }

        //private string GetCurrentWorkShift(int operatorCode)
        //{
        //    DateTime now = SmartCadDatabase.GetTimeFromBD();

        //    string wsName = (string)ServerServiceClient.GetInstance().SearchBasicObject(SmartCadHqls.GetCustomHql(SmartCadHqls.GetCurrentWorkShiftNameByOperatorCode, operatorCode, (int)WorkShiftVariationClientData.WorkShiftType.TimeOff, ApplicationUtil.GetDataBaseFormattedDate(now)));

        //    if (wsName != null)
        //        return wsName;
        //    else
        //        return string.Empty;
        //}

        private OperatorAssignClientData GetCurrentAssignedSupervisor()
        {
            OperatorAssignClientData assign = null;
            DateTime now = ServerServiceClient.GetInstance().GetTime();
            //DateTime time = ApplicationUtil.SCHEDULE_REFERENCE_DATE;
            //time = time.AddHours(now.Hour).AddMinutes(now.Minute).AddSeconds(now.Second).AddMilliseconds(now.Millisecond);
            if (OperatorClient.Supervisors != null && OperatorClient.WorkShifts != null && OperatorClient.WorkShifts.Count > 0)
            {
                for (int i = 0; i < OperatorClient.Supervisors.Count && assign == null; i++)
                {
                    OperatorAssignClientData temp = OperatorClient.Supervisors[i] as OperatorAssignClientData;
                    if ((int)now.DayOfWeek == (int)temp.StartDate.DayOfWeek && temp.StartDate <= now && now <= temp.EndDate)
                    {
                        assign = temp;
                    }
                }
            }
            return assign;
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is GridControlDataOperator)
            {
                result = OperatorClient.Equals((obj as GridControlDataOperator).OperatorClient);
            }
            return result;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
