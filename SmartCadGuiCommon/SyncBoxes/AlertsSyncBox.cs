using System;
using System.Collections.Generic;
using System.Text;

using System.Drawing;
using System.Collections;
using SmartCadControls;
using SmartCadCore.ClientData;
using SmartCadControls.Controls;

namespace SmartCadGuiCommon
{
    public class GridAlertCamData : GridControlData
    {
        private VAAlertCamClientData alerts;
        private bool visible = true;

        
        public GridAlertCamData(VAAlertCamClientData alerts)
            : base(alerts)
        {
            this.alerts = alerts;
        }

		[GridControlRowInfoData(Searchable = true)]
        public string Camera
        {
            get
            {
                return alerts.Camera.Ip;
            }
        }

		[GridControlRowInfoData(Searchable = true)]
        public string Street
        {
            get
            {
                return alerts.Camera.StructClientData.Street;
            }
        }

		[GridControlRowInfoData(Searchable = true)]
        public string Rule
        {
            get
            {
                return alerts.Rule.Name;
            }
        }



        [GridControlRowInfoData(Searchable = true, DisplayFormat = "MM/dd/yyyy HH:mm:ss")]
        public DateTime Date 
        {
            get 
            {
                return alerts.AlertDate;
            }
        
        }

        [GridControlRowInfoData(Searchable = true)]
        public int Status
        {
            get
            {
                return alerts.Status;
            }
        }

        
        [GridControlRowInfoData(Visible=false)]
        public bool Visible
        {
            get
            {
                return visible;
            }
            set
            {
                visible = value;   
            }
        }

		public override bool Equals(object obj)
		{
            if (obj is GridAlertCamData == false)
				return false;
            GridAlertCamData data = obj as GridAlertCamData;
            if (data.alerts.Code == alerts.Code || data.alerts.Code.Equals(alerts.Code))
				return true;
			return false;
		}
    }

    public class GridCctvAlertCamData : GridControlData
    {
        private VAAlertCamClientData alert;
        private bool visible = true;


        [GridControlRowInfoData(Searchable = true)]
        public string Camera
        {
            get
            {
                string varReturn = "";
                if (alert.Camera != null)
                    varReturn = alert.Camera.Ip;
                return varReturn;
            }
        }

        public GridCctvAlertCamData(VAAlertCamClientData alert)
            : base(alert)
        {
            this.alert = alert;
        }

        [GridControlRowInfoData(Searchable = true)]
        public string Street
        {
            get
            {
                string varReturn = "";
                if (alert.Camera != null)
                    varReturn = alert.Camera.StructClientData.Street;
                    //varReturn = "";
                return varReturn;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string Zone
        {
            get
            {
                string varReturn = "";
                if (alert.Camera != null)
                    varReturn = alert.Camera.StructClientData.ZoneSecRef;
                    //varReturn = "";
                return varReturn;
            }
        }
        
        [GridControlRowInfoData(Searchable = true)]                                   
        public string Rule
        {
            get
            {
                return alert.Rule.Name;
            }
        }
       

        [GridControlRowInfoData(Searchable = true)]
        public int ID
        {
            get
            {
                return alert.Code;
            }

        }

        [GridControlRowInfoData(Searchable = true, DisplayFormat = "MM/dd/yyyy HH:mm:ss")]
        public DateTime Date
        {
            get
            {
                return alert.AlertDate;
            }
        }
        
        
        public int Status
        {
            get
            {
                return alert.Status;
            }
        }

        [GridControlRowInfoData(Visible = false)]
        public bool Visible
        {
            get
            {
                return visible;
            }
            set
            {
                visible = value;
            }
        }

        public override bool Equals(object obj)
        {
            if (obj is GridCctvAlertCamData == false)
                return false;
            GridCctvAlertCamData data = obj as GridCctvAlertCamData;
            if (data.alert.Code == alert.Code || data.alert.Code.Equals(alert.Code))
                return true;
            return false;
        }
    }
}
