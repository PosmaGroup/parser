﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadControls.Controls;
using SmartCadCore.ClientData;
using SmartCadControls;

namespace SmartCadGuiCommon
{
    public class GridControlDataNotificationInProgress : GridControlData
    {
        public GridControlDataNotificationInProgress(IncidentNotificationClientData incidentNotification)
            : base(incidentNotification)
        { }

        public IncidentNotificationClientData IncidentNotification
        {
            get
            {
                return this.Tag as IncidentNotificationClientData;
            }
        }

        public string CreationDate
        {
            get
            {
                return IncidentNotification.CreationDate.ToString("s");
            }
        }

        [GridControlRowInfoData(Width = 80, Searchable = true)]
        public string Priority
        {
            get
            {
                return IncidentNotification.Priority.ToString();
            }
        }

        [GridControlRowInfoData(Width = 80, Searchable = true)]
        public string Department
        {
            get
            {
                return IncidentNotification.DepartmentType.Name;
            }
        }

        [GridControlRowInfoData(Width = 80, Searchable = true)]
        public string IncidentNotificationType
        {
            get
            {
                int count = 0;
                StringBuilder sb = new StringBuilder();
                if (IncidentNotification.IncidentTypeCustomCodes != null)
                {
                    foreach (string temp in IncidentNotification.IncidentTypeCustomCodes)
                    {
                        sb.Append(temp);
                        if (++count < IncidentNotification.IncidentTypeCustomCodes.Count)
                        {
                            sb.Append(", ");
                        }
                        else
                        {
                            sb.Append(".");
                        }
                    }
                }
                return sb.ToString();
            }
        }

        [GridControlRowInfoData(Width = 80, Searchable = true)]
        public string DepartmentZone
        {
            get
            {
                return IncidentNotification.DepartmentStation.DepartmentZone.Name;
            }
        }

        [GridControlRowInfoData(Width = 80, Searchable = true)]
        public string DepartmentStation
        {
            get
            {
                return IncidentNotification.DepartmentStation.Name;
            }
        }

        [GridControlRowInfoData(Width = 80, Searchable = true)]
        public string Incident
        {
            get
            {
                return IncidentNotification.IncidentCustomCode;
            }
        }

        [GridControlRowInfoData(Width = 80, Searchable = true)]
        public string Status
        {
            get
            {
                return IncidentNotification.Status.FriendlyName;
            }
        }

        [GridControlRowInfoData(Width = 80, Searchable = true)]
        public int AssignedUnits
        {
            get
            {
                return IncidentNotification.AssignedUnits;
            }
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is GridControlDataNotificationInProgress)
            {
                result = IncidentNotification.Equals((obj as GridControlDataNotificationInProgress).IncidentNotification);
            }
            return result;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
