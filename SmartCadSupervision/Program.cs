﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;
using Smartmatic.SmartCad.Service;
using System.Net;
using System.Collections;
using System.Globalization;
using SmartCadCore.Core;
using SmartCadGuiCommon.Util;
using SmartCadCore.Common;
using SmartCadCore.ClientData;
using SmartCadGuiCommon;
using System.ServiceModel;
using System.IO;

namespace SmartCadSupervision
{
    static class Program
    {
        //public static IDictionary<string, ApplicationPreferenceClientData> globalApplicationPreference;
        private static IList globalIndicatorClassDashboards;
        private static IList globalIndicators;
        private static string ApplicationName = "Supervision";
        
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            {
                try
                {
                    
                    Application.EnableVisualStyles();
                    Application.SetCompatibleTextRenderingDefault(false);
#if ! DEBUG
	                DevExpress.Data.CurrencyDataController.DisableThreadingProblemsDetection = true; 
#endif
                    Application.ThreadException += new ThreadExceptionEventHandler(ApplicationGuiUtil.Application_ThreadException);
                    ApplicationUtil.ActivateProcess(Process.GetCurrentProcess());

                    SmartCadConfiguration.Load();

                    ResourceLoader.UpdatedLanguage(new CultureInfo(SmartCadConfiguration.SmartCadSection.CultureElement.CultureLanguageElement.Name
                        + "-" + SmartCadConfiguration.SmartCadSection.CultureElement.CultureCountryElement.Name));

                    object result = ServerServiceClient.GetServerService();

                    if (result == null)
                    {
                        MessageForm.Show(ResourceLoader.GetString2("UnableConnectToServer"), MessageFormType.Error);
                        return;
                    }

                    ConfigurationClientData cfg = ServerServiceClient.GetInstance().GetConfiguration();
                    SmartCadConfiguration.SaveConfigurationInClient(cfg);
                    if (!ApplicationUtil.VerifyLanguage(cfg))
                    {
                        byte[] dll = ServerServiceClient.GetInstance().CheckLanguages(cfg.Language);
                        DirectoryInfo directory = Directory.CreateDirectory(SmartCadConfiguration.LanguageDirectoryClient + "\\" + cfg.Language);
                        using (FileStream fileDll = new FileStream(directory.FullName + "\\Smartmatic.SmartCad.Resources.resources.dll", FileMode.Create, FileAccess.Write))
                        {
                            fileDll.Write(dll, 0, dll.Length);
                        }
                    }

                    ResourceLoader.UpdatedLanguage(new CultureInfo(cfg.Language));

                    ServerServiceClient.GetInstance().SetApplication(UserApplicationClientData.UserApplications.Supervision.ToString());
                    SmartCadLoadInitialData.LoadInitialClientData();

                    System.Threading.Timer checkServer = ApplicationGuiUtil.StartCheckServer();

                    ApplicationGuiUtil.CheckErrorDetailButton();
                    ServerServiceClient.GetInstance().ActivateApplication(ApplicationName, ApplicationUtil.GetMACAddress());
                    bool success = false;

                    do
                    {
                        LoginForm login = new LoginForm(UserApplicationClientData.Supervision);
                        
                        if (login.ShowDialog() == DialogResult.OK)
                        {
#if !DEBUG
                        SplashForm.ShowSplash();
#endif
                         
                            LoadGlobalData();
                            
                            NetworkCredential networkCredential = login.NetworkCredential;
                            ImpersonateUser impersonateUser = new ImpersonateUser();
                            impersonateUser.Impersonate(
                                networkCredential.Domain,
                                networkCredential.UserName,
                                networkCredential.Password
                            );

                            Environment.SetEnvironmentVariable("TMP", SmartCadConfiguration.DistFolder + "Temp");

                            ServerServiceClient serverServiceClient = login.ServerService;

                            success = true;

                            bool isFirstLevelSupervisor = CheckTypeSupervisor(serverServiceClient.OperatorClient.RoleCode, true);
                            bool isDispatchSupervisor = CheckTypeSupervisor(serverServiceClient.OperatorClient.RoleCode, false);
							bool isGeneralSupervisor = CheckTypeSupervisor(serverServiceClient.OperatorClient.RoleCode, null);
                            /*
                             * Por defecto, si es supervisor general entra como supervisor de primer nivel.
                             * Es decir, si es primer nivel o supervisor general, incialmente entra por primer nivel: (isFirstLevelSupervisor && !isDispatchSupervisor) || (isFirstLevelSupervisor && isDispatchSupervisor) => isFirstLevel
                             * caso contrario(sólo supervisor de despacho), entra por supervisor de despacho
                             */
                                                       
                            SupervisionForm supervisionForm = new SupervisionForm(isGeneralSupervisor, isFirstLevelSupervisor, isDispatchSupervisor, 
                                globalIndicatorClassDashboards, globalIndicators);

#if !DEBUG
                        SplashForm.CloseSplash();
#endif

                            Application.Run(supervisionForm);
                        }
                        else
                        {
                            ServerServiceClient.GetInstance().CloseSession();
                            success = true;
                        }
                    }
                    while (!success);


                }
                catch (EndpointNotFoundException ex)
                {
                    SplashForm.CloseSplash();

                    MessageForm.Show(ResourceLoader.GetString2("UnableConnectToServer"), ex);
                }
                catch (Exception ex)
                {
                    SplashForm.CloseSplash();

                    MessageForm.Show(ResourceLoader.GetString2("GenericErrorApplication"), ex);
                }
            }           
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="roleCode"></param>
        /// <param name="checkFirstLevel">if true, this methods check if the role is FirstLevel Supervisor</param>
        /// <returns></returns>
        private static bool CheckTypeSupervisor(int roleCode, bool? checkFirstLevel)
        {
            bool result = false;
			string accessedRole = string.Empty;

			if (checkFirstLevel.HasValue == true)
				accessedRole = checkFirstLevel.Value ? "FirstLevelSupervisorName" : "DispatchSupervisorName";
			else
				accessedRole = "GeneralSupervisorName";
            IList temp = ServerServiceClient.GetInstance().SearchBasicObjects(
                SmartCadHqls.GetCustomHql(SmartCadHqls.IsAccessInRole, roleCode, accessedRole)) as IList;
            if (temp.Count > 0)
            {
                result = (long)temp[0] >= 1;
            }
            return result;
        }

        private static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            if (e.Exception.GetType() == typeof(CommunicationObjectFaultedException))
            {
                MessageForm.Show(ResourceLoader.GetString2("CommunicationObjectFaultedExceptionMessage"), MessageFormType.Error);
                Process.GetCurrentProcess().Kill();
            }
            else
            {
                MessageForm.Show(e.Exception.Message, e.Exception);
            }
        }

        private static void LoadGlobalData()
        {
            //YT: No esta en uso, las preferencias se estan cargando en el constructor de SupervisionForm
            //globalApplicationPreference = new Dictionary<string, ApplicationPreferenceClientData>();
            //foreach (ApplicationPreferenceClientData preference in ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetAllApplicationPreferences))) 
            //{
            //    globalApplicationPreference.Add(preference.Name, preference);
            //}
            globalIndicatorClassDashboards = ServerServiceClient.GetInstance().SearchClientObjects(
                 SmartCadHqls.GetCustomHql(SmartCadHqls.GetAllIndicatorsClassDashboards));
            globalIndicators = ServerServiceClient.GetInstance().SearchClientObjects(
                 SmartCadHqls.GetCustomHql(
                     SmartCadHqls.GetIndicatorWithShowDashboardTrue));
        }
        
    }
}