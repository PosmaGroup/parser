﻿using SmartCadControls.Controls;

namespace Smartmatic.SmartCad.Telephony.Avaya
{
    partial class AvayaConfigurationControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxExCMName = new TextBoxEx();
            this.labelExCMName = new LabelEx();
            this.labelExAESIp = new LabelEx();
            this.groupControlCMServer = new DevExpress.XtraEditors.GroupControl();
            this.textBoxExCMIp = new TextBoxEx();
            this.labelExCMIp = new LabelEx();
            this.groupControlAESServer = new DevExpress.XtraEditors.GroupControl();
            this.labelExSecure = new LabelEx();
            this.checkEditSecure = new DevExpress.XtraEditors.CheckEdit();
            this.textBoxExAESLogin = new TextBoxEx();
            this.textBoxExAESPassword = new TextBoxEx();
            this.labelExAESLogin = new LabelEx();
            this.labelExAESPassword = new LabelEx();
            this.labelControlSeparatorMain = new DevExpress.XtraEditors.LabelControl();
            this.textBoxExAESIp = new TextBoxEx();
            this.textBoxExAESPort = new TextBoxEx();
            this.groupControlTelephony = new DevExpress.XtraEditors.GroupControl();
            this.groupControlMediaGateway = new DevExpress.XtraEditors.GroupControl();
            this.labelExMGIp = new LabelEx();
            this.textBoxExMediaGateway = new TextBoxEx();
            this.labelExService = new LabelEx();
            this.labelExServer = new LabelEx();
            this.textBoxExService = new TextBoxEx();
            this.textBoxExServer = new TextBoxEx();
            this.textBoxExHost = new TextBoxEx();
            this.labelExHost = new LabelEx();
            this.groupControlStatistics = new DevExpress.XtraEditors.GroupControl();
            this.labelExProtocol = new LabelEx();
            this.textBoxExProtocol = new TextBoxEx();
            this.labelExDBLocale = new LabelEx();
            this.textBoxExDBLocale = new TextBoxEx();
            this.textBoxExCLocale = new TextBoxEx();
            this.labelExCLocale = new LabelEx();
            this.labelExDriver = new LabelEx();
            this.textBoxExDriver = new TextBoxEx();
            this.labelExPwd = new LabelEx();
            this.textBoxExPwd = new TextBoxEx();
            this.textBoxExDatabase = new TextBoxEx();
            this.textBoxExUid = new TextBoxEx();
            this.labelExDatabase = new LabelEx();
            this.labelExUid = new LabelEx();
            this.tabControl = new DevExpress.XtraTab.XtraTabControl();
            this.tabPageSoftphone = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabPageStatistics = new DevExpress.XtraTab.XtraTabPage();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlCMServer)).BeginInit();
            this.groupControlCMServer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlAESServer)).BeginInit();
            this.groupControlAESServer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSecure.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlTelephony)).BeginInit();
            this.groupControlTelephony.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlMediaGateway)).BeginInit();
            this.groupControlMediaGateway.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlStatistics)).BeginInit();
            this.groupControlStatistics.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl)).BeginInit();
            this.tabControl.SuspendLayout();
            this.tabPageSoftphone.SuspendLayout();
            this.xtraTabPageStatistics.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxExCMName
            // 
            this.textBoxExCMName.AllowsLetters = true;
            this.textBoxExCMName.AllowsNumbers = true;
            this.textBoxExCMName.AllowsPunctuation = true;
            this.textBoxExCMName.AllowsSeparators = false;
            this.textBoxExCMName.AllowsSymbols = false;
            this.textBoxExCMName.AllowsWhiteSpaces = false;
            this.textBoxExCMName.ExtraAllowedChars = "";
            this.textBoxExCMName.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExCMName.Location = new System.Drawing.Point(105, 29);
            this.textBoxExCMName.MaxLength = 15;
            this.textBoxExCMName.Name = "textBoxExCMName";
            this.textBoxExCMName.NonAllowedCharacters = "\'";
            this.textBoxExCMName.RegularExpresion = "";
            this.textBoxExCMName.Size = new System.Drawing.Size(255, 20);
            this.textBoxExCMName.TabIndex = 1;
            this.textBoxExCMName.TextChanged += new System.EventHandler(this.textBoxEx_TextChanged);
            // 
            // labelExCMName
            // 
            this.labelExCMName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExCMName.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExCMName.Location = new System.Drawing.Point(5, 32);
            this.labelExCMName.Name = "labelExCMName";
            this.labelExCMName.Size = new System.Drawing.Size(85, 17);
            this.labelExCMName.TabIndex = 0;
            this.labelExCMName.Text = "Switch Name";
            // 
            // labelExAESIp
            // 
            this.labelExAESIp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExAESIp.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExAESIp.Location = new System.Drawing.Point(5, 29);
            this.labelExAESIp.Name = "labelExAESIp";
            this.labelExAESIp.Size = new System.Drawing.Size(64, 16);
            this.labelExAESIp.TabIndex = 0;
            this.labelExAESIp.Text = "ServerIp";
            // 
            // groupControlCMServer
            // 
            this.groupControlCMServer.Controls.Add(this.textBoxExCMIp);
            this.groupControlCMServer.Controls.Add(this.labelExCMIp);
            this.groupControlCMServer.Controls.Add(this.labelExCMName);
            this.groupControlCMServer.Controls.Add(this.textBoxExCMName);
            this.groupControlCMServer.Location = new System.Drawing.Point(5, 141);
            this.groupControlCMServer.Name = "groupControlCMServer";
            this.groupControlCMServer.Size = new System.Drawing.Size(365, 85);
            this.groupControlCMServer.TabIndex = 1;
            this.groupControlCMServer.Text = "CM Server";
            // 
            // textBoxExCMIp
            // 
            this.textBoxExCMIp.AllowsLetters = true;
            this.textBoxExCMIp.AllowsNumbers = true;
            this.textBoxExCMIp.AllowsPunctuation = true;
            this.textBoxExCMIp.AllowsSeparators = false;
            this.textBoxExCMIp.AllowsSymbols = false;
            this.textBoxExCMIp.AllowsWhiteSpaces = false;
            this.textBoxExCMIp.ExtraAllowedChars = "";
            this.textBoxExCMIp.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExCMIp.Location = new System.Drawing.Point(105, 57);
            this.textBoxExCMIp.MaxLength = 15;
            this.textBoxExCMIp.Name = "textBoxExCMIp";
            this.textBoxExCMIp.NonAllowedCharacters = "\'";
            this.textBoxExCMIp.RegularExpresion = "";
            this.textBoxExCMIp.Size = new System.Drawing.Size(255, 20);
            this.textBoxExCMIp.TabIndex = 5;
            this.textBoxExCMIp.TextChanged += new System.EventHandler(this.textBoxEx_TextChanged);
            // 
            // labelExCMIp
            // 
            this.labelExCMIp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExCMIp.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExCMIp.Location = new System.Drawing.Point(5, 60);
            this.labelExCMIp.Name = "labelExCMIp";
            this.labelExCMIp.Size = new System.Drawing.Size(85, 17);
            this.labelExCMIp.TabIndex = 4;
            this.labelExCMIp.Text = "Switch Ip";
            // 
            // groupControlAESServer
            // 
            this.groupControlAESServer.Controls.Add(this.labelExSecure);
            this.groupControlAESServer.Controls.Add(this.checkEditSecure);
            this.groupControlAESServer.Controls.Add(this.textBoxExAESLogin);
            this.groupControlAESServer.Controls.Add(this.textBoxExAESPassword);
            this.groupControlAESServer.Controls.Add(this.labelExAESLogin);
            this.groupControlAESServer.Controls.Add(this.labelExAESPassword);
            this.groupControlAESServer.Controls.Add(this.labelControlSeparatorMain);
            this.groupControlAESServer.Controls.Add(this.labelExAESIp);
            this.groupControlAESServer.Controls.Add(this.textBoxExAESIp);
            this.groupControlAESServer.Controls.Add(this.textBoxExAESPort);
            this.groupControlAESServer.Location = new System.Drawing.Point(5, 26);
            this.groupControlAESServer.Name = "groupControlAESServer";
            this.groupControlAESServer.Size = new System.Drawing.Size(365, 109);
            this.groupControlAESServer.TabIndex = 0;
            this.groupControlAESServer.Text = "AES Server";
            // 
            // labelExSecure
            // 
            this.labelExSecure.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExSecure.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExSecure.Location = new System.Drawing.Point(289, 29);
            this.labelExSecure.Name = "labelExSecure";
            this.labelExSecure.Size = new System.Drawing.Size(49, 19);
            this.labelExSecure.TabIndex = 16;
            this.labelExSecure.Text = "Secure";
            // 
            // checkEditSecure
            // 
            this.checkEditSecure.Location = new System.Drawing.Point(339, 27);
            this.checkEditSecure.Name = "checkEditSecure";
            this.checkEditSecure.Properties.Caption = "";
            this.checkEditSecure.Size = new System.Drawing.Size(25, 19);
            this.checkEditSecure.TabIndex = 15;
            // 
            // textBoxExAESLogin
            // 
            this.textBoxExAESLogin.AllowsLetters = true;
            this.textBoxExAESLogin.AllowsNumbers = true;
            this.textBoxExAESLogin.AllowsPunctuation = true;
            this.textBoxExAESLogin.AllowsSeparators = true;
            this.textBoxExAESLogin.AllowsSymbols = true;
            this.textBoxExAESLogin.AllowsWhiteSpaces = true;
            this.textBoxExAESLogin.ExtraAllowedChars = "";
            this.textBoxExAESLogin.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExAESLogin.Location = new System.Drawing.Point(105, 54);
            this.textBoxExAESLogin.MaxLength = 100;
            this.textBoxExAESLogin.Name = "textBoxExAESLogin";
            this.textBoxExAESLogin.NonAllowedCharacters = "\'";
            this.textBoxExAESLogin.RegularExpresion = "";
            this.textBoxExAESLogin.Size = new System.Drawing.Size(255, 20);
            this.textBoxExAESLogin.TabIndex = 11;
            this.textBoxExAESLogin.TextChanged += new System.EventHandler(this.textBoxEx_TextChanged);
            // 
            // textBoxExAESPassword
            // 
            this.textBoxExAESPassword.AllowsLetters = true;
            this.textBoxExAESPassword.AllowsNumbers = true;
            this.textBoxExAESPassword.AllowsPunctuation = true;
            this.textBoxExAESPassword.AllowsSeparators = true;
            this.textBoxExAESPassword.AllowsSymbols = true;
            this.textBoxExAESPassword.AllowsWhiteSpaces = true;
            this.textBoxExAESPassword.ExtraAllowedChars = "";
            this.textBoxExAESPassword.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExAESPassword.Location = new System.Drawing.Point(105, 82);
            this.textBoxExAESPassword.MaxLength = 63;
            this.textBoxExAESPassword.Name = "textBoxExAESPassword";
            this.textBoxExAESPassword.NonAllowedCharacters = "\'";
            this.textBoxExAESPassword.PasswordChar = '*';
            this.textBoxExAESPassword.RegularExpresion = "";
            this.textBoxExAESPassword.Size = new System.Drawing.Size(255, 20);
            this.textBoxExAESPassword.TabIndex = 13;
            this.textBoxExAESPassword.TextChanged += new System.EventHandler(this.textBoxEx_TextChanged);
            // 
            // labelExAESLogin
            // 
            this.labelExAESLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExAESLogin.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExAESLogin.Location = new System.Drawing.Point(5, 57);
            this.labelExAESLogin.Name = "labelExAESLogin";
            this.labelExAESLogin.Size = new System.Drawing.Size(88, 16);
            this.labelExAESLogin.TabIndex = 10;
            this.labelExAESLogin.Text = "Login";
            // 
            // labelExAESPassword
            // 
            this.labelExAESPassword.AutoSize = true;
            this.labelExAESPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExAESPassword.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExAESPassword.Location = new System.Drawing.Point(5, 85);
            this.labelExAESPassword.Name = "labelExAESPassword";
            this.labelExAESPassword.Size = new System.Drawing.Size(53, 13);
            this.labelExAESPassword.TabIndex = 12;
            this.labelExAESPassword.Text = "Password";
            // 
            // labelControlSeparatorMain
            // 
            this.labelControlSeparatorMain.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControlSeparatorMain.Location = new System.Drawing.Point(204, 28);
            this.labelControlSeparatorMain.Name = "labelControlSeparatorMain";
            this.labelControlSeparatorMain.Size = new System.Drawing.Size(5, 16);
            this.labelControlSeparatorMain.TabIndex = 2;
            this.labelControlSeparatorMain.Text = ":";
            // 
            // textBoxExAESIp
            // 
            this.textBoxExAESIp.AllowsLetters = true;
            this.textBoxExAESIp.AllowsNumbers = true;
            this.textBoxExAESIp.AllowsPunctuation = true;
            this.textBoxExAESIp.AllowsSeparators = false;
            this.textBoxExAESIp.AllowsSymbols = false;
            this.textBoxExAESIp.AllowsWhiteSpaces = false;
            this.textBoxExAESIp.ExtraAllowedChars = "";
            this.textBoxExAESIp.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExAESIp.Location = new System.Drawing.Point(105, 27);
            this.textBoxExAESIp.MaxLength = 15;
            this.textBoxExAESIp.Name = "textBoxExAESIp";
            this.textBoxExAESIp.NonAllowedCharacters = "\'";
            this.textBoxExAESIp.RegularExpresion = "";
            this.textBoxExAESIp.Size = new System.Drawing.Size(93, 20);
            this.textBoxExAESIp.TabIndex = 1;
            this.textBoxExAESIp.TextChanged += new System.EventHandler(this.textBoxEx_TextChanged);
            // 
            // textBoxExAESPort
            // 
            this.textBoxExAESPort.AllowsLetters = false;
            this.textBoxExAESPort.AllowsNumbers = true;
            this.textBoxExAESPort.AllowsPunctuation = true;
            this.textBoxExAESPort.AllowsSeparators = false;
            this.textBoxExAESPort.AllowsSymbols = false;
            this.textBoxExAESPort.AllowsWhiteSpaces = false;
            this.textBoxExAESPort.ExtraAllowedChars = "";
            this.textBoxExAESPort.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExAESPort.Location = new System.Drawing.Point(219, 27);
            this.textBoxExAESPort.MaxLength = 15;
            this.textBoxExAESPort.Name = "textBoxExAESPort";
            this.textBoxExAESPort.NonAllowedCharacters = "\'";
            this.textBoxExAESPort.RegularExpresion = "";
            this.textBoxExAESPort.Size = new System.Drawing.Size(54, 20);
            this.textBoxExAESPort.TabIndex = 3;
            this.textBoxExAESPort.TextChanged += new System.EventHandler(this.textBoxEx_TextChanged);
            // 
            // groupControlTelephony
            // 
            this.groupControlTelephony.Controls.Add(this.groupControlMediaGateway);
            this.groupControlTelephony.Controls.Add(this.groupControlCMServer);
            this.groupControlTelephony.Controls.Add(this.groupControlAESServer);
            this.groupControlTelephony.Location = new System.Drawing.Point(1, 1);
            this.groupControlTelephony.Name = "groupControlTelephony";
            this.groupControlTelephony.Size = new System.Drawing.Size(375, 295);
            this.groupControlTelephony.TabIndex = 7;
            this.groupControlTelephony.Text = "groupControlTelephony";
            // 
            // groupControlMediaGateway
            // 
            this.groupControlMediaGateway.Controls.Add(this.labelExMGIp);
            this.groupControlMediaGateway.Controls.Add(this.textBoxExMediaGateway);
            this.groupControlMediaGateway.Location = new System.Drawing.Point(5, 232);
            this.groupControlMediaGateway.Name = "groupControlMediaGateway";
            this.groupControlMediaGateway.Size = new System.Drawing.Size(365, 59);
            this.groupControlMediaGateway.TabIndex = 2;
            this.groupControlMediaGateway.Text = "Media Gateway";
            // 
            // labelExMGIp
            // 
            this.labelExMGIp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExMGIp.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExMGIp.Location = new System.Drawing.Point(5, 32);
            this.labelExMGIp.Name = "labelExMGIp";
            this.labelExMGIp.Size = new System.Drawing.Size(85, 17);
            this.labelExMGIp.TabIndex = 0;
            this.labelExMGIp.Text = "Ip";
            // 
            // textBoxExMediaGateway
            // 
            this.textBoxExMediaGateway.AllowsLetters = true;
            this.textBoxExMediaGateway.AllowsNumbers = true;
            this.textBoxExMediaGateway.AllowsPunctuation = true;
            this.textBoxExMediaGateway.AllowsSeparators = false;
            this.textBoxExMediaGateway.AllowsSymbols = false;
            this.textBoxExMediaGateway.AllowsWhiteSpaces = false;
            this.textBoxExMediaGateway.ExtraAllowedChars = "";
            this.textBoxExMediaGateway.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExMediaGateway.Location = new System.Drawing.Point(105, 29);
            this.textBoxExMediaGateway.MaxLength = 15;
            this.textBoxExMediaGateway.Name = "textBoxExMediaGateway";
            this.textBoxExMediaGateway.NonAllowedCharacters = "\'";
            this.textBoxExMediaGateway.RegularExpresion = "";
            this.textBoxExMediaGateway.Size = new System.Drawing.Size(255, 20);
            this.textBoxExMediaGateway.TabIndex = 1;
            // 
            // labelExService
            // 
            this.labelExService.AutoSize = true;
            this.labelExService.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExService.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExService.Location = new System.Drawing.Point(7, 119);
            this.labelExService.Name = "labelExService";
            this.labelExService.Size = new System.Drawing.Size(43, 13);
            this.labelExService.TabIndex = 8;
            this.labelExService.Text = "Service";
            // 
            // labelExServer
            // 
            this.labelExServer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExServer.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExServer.Location = new System.Drawing.Point(7, 91);
            this.labelExServer.Name = "labelExServer";
            this.labelExServer.Size = new System.Drawing.Size(88, 16);
            this.labelExServer.TabIndex = 6;
            this.labelExServer.Text = "Server";
            // 
            // textBoxExService
            // 
            this.textBoxExService.AllowsLetters = true;
            this.textBoxExService.AllowsNumbers = true;
            this.textBoxExService.AllowsPunctuation = true;
            this.textBoxExService.AllowsSeparators = true;
            this.textBoxExService.AllowsSymbols = true;
            this.textBoxExService.AllowsWhiteSpaces = true;
            this.textBoxExService.ExtraAllowedChars = "";
            this.textBoxExService.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExService.Location = new System.Drawing.Point(110, 117);
            this.textBoxExService.MaxLength = 63;
            this.textBoxExService.Name = "textBoxExService";
            this.textBoxExService.NonAllowedCharacters = "\'";
            this.textBoxExService.RegularExpresion = "";
            this.textBoxExService.Size = new System.Drawing.Size(260, 20);
            this.textBoxExService.TabIndex = 9;
            // 
            // textBoxExServer
            // 
            this.textBoxExServer.AllowsLetters = true;
            this.textBoxExServer.AllowsNumbers = true;
            this.textBoxExServer.AllowsPunctuation = true;
            this.textBoxExServer.AllowsSeparators = true;
            this.textBoxExServer.AllowsSymbols = true;
            this.textBoxExServer.AllowsWhiteSpaces = true;
            this.textBoxExServer.ExtraAllowedChars = "";
            this.textBoxExServer.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExServer.Location = new System.Drawing.Point(110, 88);
            this.textBoxExServer.MaxLength = 100;
            this.textBoxExServer.Name = "textBoxExServer";
            this.textBoxExServer.NonAllowedCharacters = "\'";
            this.textBoxExServer.RegularExpresion = "";
            this.textBoxExServer.Size = new System.Drawing.Size(260, 20);
            this.textBoxExServer.TabIndex = 7;
            // 
            // textBoxExHost
            // 
            this.textBoxExHost.AllowsLetters = true;
            this.textBoxExHost.AllowsNumbers = true;
            this.textBoxExHost.AllowsPunctuation = true;
            this.textBoxExHost.AllowsSeparators = false;
            this.textBoxExHost.AllowsSymbols = false;
            this.textBoxExHost.AllowsWhiteSpaces = false;
            this.textBoxExHost.ExtraAllowedChars = "";
            this.textBoxExHost.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExHost.Location = new System.Drawing.Point(110, 60);
            this.textBoxExHost.MaxLength = 15;
            this.textBoxExHost.Name = "textBoxExHost";
            this.textBoxExHost.NonAllowedCharacters = "\'";
            this.textBoxExHost.RegularExpresion = "";
            this.textBoxExHost.Size = new System.Drawing.Size(260, 20);
            this.textBoxExHost.TabIndex = 5;
            // 
            // labelExHost
            // 
            this.labelExHost.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExHost.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExHost.Location = new System.Drawing.Point(7, 61);
            this.labelExHost.Name = "labelExHost";
            this.labelExHost.Size = new System.Drawing.Size(88, 16);
            this.labelExHost.TabIndex = 4;
            this.labelExHost.Text = "Host";
            // 
            // groupControlStatistics
            // 
            this.groupControlStatistics.Controls.Add(this.labelExProtocol);
            this.groupControlStatistics.Controls.Add(this.textBoxExProtocol);
            this.groupControlStatistics.Controls.Add(this.labelExDBLocale);
            this.groupControlStatistics.Controls.Add(this.textBoxExDBLocale);
            this.groupControlStatistics.Controls.Add(this.textBoxExCLocale);
            this.groupControlStatistics.Controls.Add(this.labelExCLocale);
            this.groupControlStatistics.Controls.Add(this.labelExDriver);
            this.groupControlStatistics.Controls.Add(this.textBoxExDriver);
            this.groupControlStatistics.Controls.Add(this.labelExPwd);
            this.groupControlStatistics.Controls.Add(this.textBoxExPwd);
            this.groupControlStatistics.Controls.Add(this.textBoxExDatabase);
            this.groupControlStatistics.Controls.Add(this.textBoxExUid);
            this.groupControlStatistics.Controls.Add(this.labelExDatabase);
            this.groupControlStatistics.Controls.Add(this.labelExUid);
            this.groupControlStatistics.Controls.Add(this.labelExHost);
            this.groupControlStatistics.Controls.Add(this.textBoxExHost);
            this.groupControlStatistics.Controls.Add(this.textBoxExServer);
            this.groupControlStatistics.Controls.Add(this.textBoxExService);
            this.groupControlStatistics.Controls.Add(this.labelExServer);
            this.groupControlStatistics.Controls.Add(this.labelExService);
            this.groupControlStatistics.Location = new System.Drawing.Point(1, 1);
            this.groupControlStatistics.Name = "groupControlStatistics";
            this.groupControlStatistics.Size = new System.Drawing.Size(375, 308);
            this.groupControlStatistics.TabIndex = 6;
            this.groupControlStatistics.Text = "groupControlStatistic";
            // 
            // labelExProtocol
            // 
            this.labelExProtocol.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExProtocol.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExProtocol.Location = new System.Drawing.Point(8, 228);
            this.labelExProtocol.Name = "labelExProtocol";
            this.labelExProtocol.Size = new System.Drawing.Size(88, 16);
            this.labelExProtocol.TabIndex = 24;
            this.labelExProtocol.Text = "Protocol";
            // 
            // textBoxExProtocol
            // 
            this.textBoxExProtocol.AllowsLetters = true;
            this.textBoxExProtocol.AllowsNumbers = true;
            this.textBoxExProtocol.AllowsPunctuation = true;
            this.textBoxExProtocol.AllowsSeparators = false;
            this.textBoxExProtocol.AllowsSymbols = false;
            this.textBoxExProtocol.AllowsWhiteSpaces = false;
            this.textBoxExProtocol.ExtraAllowedChars = "";
            this.textBoxExProtocol.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExProtocol.Location = new System.Drawing.Point(110, 226);
            this.textBoxExProtocol.MaxLength = 15;
            this.textBoxExProtocol.Name = "textBoxExProtocol";
            this.textBoxExProtocol.NonAllowedCharacters = "\'";
            this.textBoxExProtocol.RegularExpresion = "";
            this.textBoxExProtocol.Size = new System.Drawing.Size(260, 20);
            this.textBoxExProtocol.TabIndex = 25;
            // 
            // labelExDBLocale
            // 
            this.labelExDBLocale.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExDBLocale.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExDBLocale.Location = new System.Drawing.Point(6, 282);
            this.labelExDBLocale.Name = "labelExDBLocale";
            this.labelExDBLocale.Size = new System.Drawing.Size(88, 15);
            this.labelExDBLocale.TabIndex = 22;
            this.labelExDBLocale.Text = "Database Locale";
            // 
            // textBoxExDBLocale
            // 
            this.textBoxExDBLocale.AllowsLetters = true;
            this.textBoxExDBLocale.AllowsNumbers = true;
            this.textBoxExDBLocale.AllowsPunctuation = true;
            this.textBoxExDBLocale.AllowsSeparators = false;
            this.textBoxExDBLocale.AllowsSymbols = false;
            this.textBoxExDBLocale.AllowsWhiteSpaces = false;
            this.textBoxExDBLocale.ExtraAllowedChars = "";
            this.textBoxExDBLocale.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExDBLocale.Location = new System.Drawing.Point(110, 279);
            this.textBoxExDBLocale.MaxLength = 15;
            this.textBoxExDBLocale.Name = "textBoxExDBLocale";
            this.textBoxExDBLocale.NonAllowedCharacters = "\'";
            this.textBoxExDBLocale.RegularExpresion = "";
            this.textBoxExDBLocale.Size = new System.Drawing.Size(260, 20);
            this.textBoxExDBLocale.TabIndex = 23;
            // 
            // textBoxExCLocale
            // 
            this.textBoxExCLocale.AllowsLetters = true;
            this.textBoxExCLocale.AllowsNumbers = true;
            this.textBoxExCLocale.AllowsPunctuation = true;
            this.textBoxExCLocale.AllowsSeparators = true;
            this.textBoxExCLocale.AllowsSymbols = true;
            this.textBoxExCLocale.AllowsWhiteSpaces = true;
            this.textBoxExCLocale.ExtraAllowedChars = "";
            this.textBoxExCLocale.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExCLocale.Location = new System.Drawing.Point(109, 253);
            this.textBoxExCLocale.MaxLength = 63;
            this.textBoxExCLocale.Name = "textBoxExCLocale";
            this.textBoxExCLocale.NonAllowedCharacters = "\'";
            this.textBoxExCLocale.RegularExpresion = "";
            this.textBoxExCLocale.Size = new System.Drawing.Size(260, 20);
            this.textBoxExCLocale.TabIndex = 21;
            // 
            // labelExCLocale
            // 
            this.labelExCLocale.AutoSize = true;
            this.labelExCLocale.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExCLocale.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExCLocale.Location = new System.Drawing.Point(7, 256);
            this.labelExCLocale.Name = "labelExCLocale";
            this.labelExCLocale.Size = new System.Drawing.Size(68, 13);
            this.labelExCLocale.TabIndex = 20;
            this.labelExCLocale.Text = "Client Locale";
            // 
            // labelExDriver
            // 
            this.labelExDriver.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExDriver.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExDriver.Location = new System.Drawing.Point(8, 33);
            this.labelExDriver.Name = "labelExDriver";
            this.labelExDriver.Size = new System.Drawing.Size(88, 16);
            this.labelExDriver.TabIndex = 18;
            this.labelExDriver.Text = "Host";
            // 
            // textBoxExDriver
            // 
            this.textBoxExDriver.AllowsLetters = true;
            this.textBoxExDriver.AllowsNumbers = true;
            this.textBoxExDriver.AllowsPunctuation = true;
            this.textBoxExDriver.AllowsSeparators = true;
            this.textBoxExDriver.AllowsSymbols = true;
            this.textBoxExDriver.AllowsWhiteSpaces = true;
            this.textBoxExDriver.ExtraAllowedChars = "";
            this.textBoxExDriver.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExDriver.Location = new System.Drawing.Point(109, 34);
            this.textBoxExDriver.MaxLength = 0;
            this.textBoxExDriver.Name = "textBoxExDriver";
            this.textBoxExDriver.NonAllowedCharacters = "\'";
            this.textBoxExDriver.RegularExpresion = "";
            this.textBoxExDriver.Size = new System.Drawing.Size(260, 20);
            this.textBoxExDriver.TabIndex = 19;
            // 
            // labelExPwd
            // 
            this.labelExPwd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExPwd.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExPwd.Location = new System.Drawing.Point(7, 200);
            this.labelExPwd.Name = "labelExPwd";
            this.labelExPwd.Size = new System.Drawing.Size(88, 16);
            this.labelExPwd.TabIndex = 16;
            this.labelExPwd.Text = "Pwd";
            // 
            // textBoxExPwd
            // 
            this.textBoxExPwd.AllowsLetters = true;
            this.textBoxExPwd.AllowsNumbers = true;
            this.textBoxExPwd.AllowsPunctuation = true;
            this.textBoxExPwd.AllowsSeparators = false;
            this.textBoxExPwd.AllowsSymbols = false;
            this.textBoxExPwd.AllowsWhiteSpaces = false;
            this.textBoxExPwd.ExtraAllowedChars = "";
            this.textBoxExPwd.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExPwd.Location = new System.Drawing.Point(109, 198);
            this.textBoxExPwd.MaxLength = 15;
            this.textBoxExPwd.Name = "textBoxExPwd";
            this.textBoxExPwd.NonAllowedCharacters = "\'";
            this.textBoxExPwd.PasswordChar = '*';
            this.textBoxExPwd.RegularExpresion = "";
            this.textBoxExPwd.Size = new System.Drawing.Size(260, 20);
            this.textBoxExPwd.TabIndex = 17;
            // 
            // textBoxExDatabase
            // 
            this.textBoxExDatabase.AllowsLetters = true;
            this.textBoxExDatabase.AllowsNumbers = true;
            this.textBoxExDatabase.AllowsPunctuation = true;
            this.textBoxExDatabase.AllowsSeparators = true;
            this.textBoxExDatabase.AllowsSymbols = true;
            this.textBoxExDatabase.AllowsWhiteSpaces = true;
            this.textBoxExDatabase.ExtraAllowedChars = "";
            this.textBoxExDatabase.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExDatabase.Location = new System.Drawing.Point(110, 144);
            this.textBoxExDatabase.MaxLength = 100;
            this.textBoxExDatabase.Name = "textBoxExDatabase";
            this.textBoxExDatabase.NonAllowedCharacters = "\'";
            this.textBoxExDatabase.RegularExpresion = "";
            this.textBoxExDatabase.Size = new System.Drawing.Size(260, 20);
            this.textBoxExDatabase.TabIndex = 13;
            // 
            // textBoxExUid
            // 
            this.textBoxExUid.AllowsLetters = true;
            this.textBoxExUid.AllowsNumbers = true;
            this.textBoxExUid.AllowsPunctuation = true;
            this.textBoxExUid.AllowsSeparators = true;
            this.textBoxExUid.AllowsSymbols = true;
            this.textBoxExUid.AllowsWhiteSpaces = true;
            this.textBoxExUid.ExtraAllowedChars = "";
            this.textBoxExUid.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExUid.Location = new System.Drawing.Point(110, 171);
            this.textBoxExUid.MaxLength = 63;
            this.textBoxExUid.Name = "textBoxExUid";
            this.textBoxExUid.NonAllowedCharacters = "\'";
            this.textBoxExUid.RegularExpresion = "";
            this.textBoxExUid.Size = new System.Drawing.Size(260, 20);
            this.textBoxExUid.TabIndex = 15;
            // 
            // labelExDatabase
            // 
            this.labelExDatabase.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExDatabase.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExDatabase.Location = new System.Drawing.Point(8, 147);
            this.labelExDatabase.Name = "labelExDatabase";
            this.labelExDatabase.Size = new System.Drawing.Size(88, 16);
            this.labelExDatabase.TabIndex = 12;
            this.labelExDatabase.Text = "Database";
            // 
            // labelExUid
            // 
            this.labelExUid.AutoSize = true;
            this.labelExUid.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExUid.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExUid.Location = new System.Drawing.Point(8, 175);
            this.labelExUid.Name = "labelExUid";
            this.labelExUid.Size = new System.Drawing.Size(23, 13);
            this.labelExUid.TabIndex = 14;
            this.labelExUid.Text = "Uid";
            // 
            // tabControl
            // 
            this.tabControl.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.tabControl.Location = new System.Drawing.Point(3, 3);
            this.tabControl.Name = "tabControl";
            this.tabControl.Padding = new System.Windows.Forms.Padding(2);
            this.tabControl.SelectedTabPage = this.tabPageSoftphone;
            this.tabControl.Size = new System.Drawing.Size(383, 341);
            this.tabControl.TabIndex = 8;
            this.tabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tabPageSoftphone,
            this.xtraTabPageStatistics});
            // 
            // tabPageSoftphone
            // 
            this.tabPageSoftphone.Controls.Add(this.groupControlTelephony);
            this.tabPageSoftphone.Name = "tabPageSoftphone";
            this.tabPageSoftphone.Padding = new System.Windows.Forms.Padding(2);
            this.tabPageSoftphone.Size = new System.Drawing.Size(378, 316);
            this.tabPageSoftphone.Text = "SoftPhone";
            // 
            // xtraTabPageStatistics
            // 
            this.xtraTabPageStatistics.Controls.Add(this.groupControlStatistics);
            this.xtraTabPageStatistics.Name = "xtraTabPageStatistics";
            this.xtraTabPageStatistics.Size = new System.Drawing.Size(378, 316);
            this.xtraTabPageStatistics.Text = "Statistics";
            // 
            // AvayaConfigurationControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabControl);
            this.Name = "AvayaConfigurationControl";
            this.Size = new System.Drawing.Size(388, 348);
            ((System.ComponentModel.ISupportInitialize)(this.groupControlCMServer)).EndInit();
            this.groupControlCMServer.ResumeLayout(false);
            this.groupControlCMServer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlAESServer)).EndInit();
            this.groupControlAESServer.ResumeLayout(false);
            this.groupControlAESServer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSecure.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlTelephony)).EndInit();
            this.groupControlTelephony.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControlMediaGateway)).EndInit();
            this.groupControlMediaGateway.ResumeLayout(false);
            this.groupControlMediaGateway.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlStatistics)).EndInit();
            this.groupControlStatistics.ResumeLayout(false);
            this.groupControlStatistics.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl)).EndInit();
            this.tabControl.ResumeLayout(false);
            this.tabPageSoftphone.ResumeLayout(false);
            this.xtraTabPageStatistics.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal TextBoxEx textBoxExCMName;
        private LabelEx labelExCMName;
        private LabelEx labelExAESIp;
        private DevExpress.XtraEditors.GroupControl groupControlCMServer;
        private DevExpress.XtraEditors.GroupControl groupControlAESServer;
        private DevExpress.XtraEditors.LabelControl labelControlSeparatorMain;
        internal TextBoxEx textBoxExAESIp;
        internal TextBoxEx textBoxExAESPort;
        private DevExpress.XtraEditors.GroupControl groupControlTelephony;
        internal TextBoxEx textBoxExCMIp;
        private LabelEx labelExCMIp;
        internal TextBoxEx textBoxExAESLogin;
        internal TextBoxEx textBoxExAESPassword;
        private LabelEx labelExAESLogin;
        private LabelEx labelExAESPassword;
        private LabelEx labelExSecure;
        private DevExpress.XtraEditors.CheckEdit checkEditSecure;
        private LabelEx labelExService;
        private LabelEx labelExServer;
        internal TextBoxEx textBoxExService;
        internal TextBoxEx textBoxExServer;
        internal TextBoxEx textBoxExHost;
        private LabelEx labelExHost;
        private DevExpress.XtraEditors.GroupControl groupControlStatistics;
        private LabelEx labelExPwd;
        internal TextBoxEx textBoxExPwd;
        internal TextBoxEx textBoxExDatabase;
        internal TextBoxEx textBoxExUid;
        private LabelEx labelExDatabase;
        private LabelEx labelExUid;
        private DevExpress.XtraEditors.GroupControl groupControlMediaGateway;
        private LabelEx labelExMGIp;
        internal TextBoxEx textBoxExMediaGateway;
        private LabelEx labelExDriver;
        internal TextBoxEx textBoxExDriver;
        private LabelEx labelExProtocol;
        internal TextBoxEx textBoxExProtocol;
        private LabelEx labelExDBLocale;
        internal TextBoxEx textBoxExDBLocale;
        internal TextBoxEx textBoxExCLocale;
        private LabelEx labelExCLocale;
        private DevExpress.XtraTab.XtraTabControl tabControl;
        private DevExpress.XtraTab.XtraTabPage tabPageSoftphone;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageStatistics;


    }
}
