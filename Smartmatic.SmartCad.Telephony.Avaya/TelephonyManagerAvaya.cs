﻿using System;
using System.Collections;
using Avaya.ApplicationEnablement.DMCC;
using System.Net.Sockets;
using System.Threading;
using System.Net;
using System.Linq;
using Codecs;
using System.IO;
using Smartmatic.SmartCad.Service;
using System.Runtime.InteropServices;
using LumiSoft.Net.RTP;
using LumiSoft.Net.Media;
using LumiSoft.Net.Media.Codec.Audio;
using System.Collections.Generic;
using NAudio.Wave;
using System.Diagnostics;
using SmartCadCore.Core;
using SmartCadCore.Common;

namespace Smartmatic.SmartCad.Telephony.Avaya
{
    /// <summary>
    /// Provide an interface to operate with an Avaya PBX.
    /// </summary>
    /// <className>TelephonyManagerAvaya</className>
    /// <author email="arturo.aguero@smartmatic.com">Arturo Aguero</author>
    /// <date>2012/06/21</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    public class TelephonyManagerAvaya : TelephonyManagerAbstract
    {
        /// <summary>
        /// Indicate if the caller is in the ready state or busy state
        /// </summary>
        private bool IsReady { get; set; }

        ServiceProvider serviceProvider;
        Device currentDevice = null;
        string currentDeviceId;
        string currentCallId = "";
        string callingDeviceId = "";
        string currentDisplay = "";
        string waitingCallId = null;
        string waitingDeviceId=null;

        string RtpIp_Receive = "";//local ip address
        int RtpPort_Receive = 2050;

        string RtpIp_ToSend = "";//remote ip
        int RtpPort_ToSend;
        int RtpPacketSize_ToSend = 20;

        string RtcpIp = "127.0.0.1";
        int RtcpPort = 2053;
        Media.MediaMode mediaMode = Media.MediaMode.CLIENT_MODE;
        Phone.MediaInfo.DependencyMode dependencyMode = Phone.MediaInfo.DependencyMode.Main;
        string protocolVersion = ServiceProvider.DmccProtocolVersion.PROTOCOL_VERSION_5_2;

        private bool shuttingDown = false;
        private INetworkChatCodec codec = new MuLawChatCodec(); //Mulaw is g711u, should be the default in avaya.


        public enum TelephonyCallState
        {
            NoCall = 0,
            Originated,
            Alerting,
            Established,
            Held,
            Conferenced,
            Failed,
            LoggingIn,
            LoggingOut,
            AutoIn,
            DoNotDisturb,
            Initializing,
            DisableAutoIn
        }

        private TelephonyCallState currentCallState;
        public TelephonyCallState CurrentCallState
        {
            get { return currentCallState; }
            set
            {
                if(value == TelephonyCallState.NoCall)
                    currentDisplay = "";
                currentCallState = value;
            }
        }

        protected string AESServerIp
        {
            get { return configurationProperties[AvayaProperties.AES_IP].ToString(); }
        }
        protected string AESServerPort
        {
            get { return configurationProperties[AvayaProperties.AES_PORT].ToString(); }
        }
        protected string AESLogin
        {
            get { return configurationProperties[AvayaProperties.AES_LOGIN].ToString(); }
        }
        string aesPassword = "";
        protected string AESPassword
        {
            get
            {
                if (aesPassword == "")
                {
                    //Encrypted password
                    aesPassword = CryptoUtil.DecryptWithRijndael(configurationProperties[AvayaProperties.AES_PASSWORD].ToString()).TrimEnd('\0');
                    //Plane password
                    // aesPassword = configurationProperties[AvayaProperties.AES_PASSWORD].ToString();
                }
                return aesPassword;
            }
        }
        protected string CMServerIp
        {
            get { return configurationProperties[AvayaProperties.CM_IP].ToString(); }
        }
        protected string CMServerName
        {
            get { return configurationProperties[AvayaProperties.CM_NAME].ToString(); }
        }
        protected string MediaGatewayIp
        {
            get { return configurationProperties[AvayaProperties.MEDIA_GATEWAY].ToString(); }
        }
       
        protected string Queue
        {
            get { return configurationProperties[CommonProperties.Queue].ToString(); }
        }

        public string PhoneReportCustomCode { get; set; }


        bool recordCalls = false;
        bool useSecureConex = true;

        public TelephonyManagerAvaya(Hashtable properties)
            : base(properties)
        {
            try
            {
                useSecureConex = bool.Parse((string)properties[AvayaProperties.SECURE_SOCKET]);
                int AEServerPort = Convert.ToInt32(properties[AvayaProperties.AES_PORT]);
                string AEServerIp = (string)properties[AvayaProperties.AES_IP];
                recordCalls = (bool)properties[CommonProperties.RecordCalls];
            }
            catch (Exception ex)
            {
                HandleException(new TelephonyException(ResourceLoader.GetString2("TelphonyServerNotConfigured"), ex));
            }

            Init();
            Login(AgentId, AgentPassword, true);
        }

        public override string TestCTIState()
        {
            return "";
        }

        /// PASO 1 - Inicia la session con el DMCC
        protected override void Init()
        {
            try
            {
                Console.WriteLine("Initializing...");
                CurrentCallState = TelephonyCallState.Initializing;

                serviceProvider = new ServiceProvider();
                InitializeCallbacks();

                string SessionName = AgentId;
                int SessionCleanupDelay = 90; // Seconds
                int SessionDuration = 14400; // Seconds

                bool EnableAutoKeepAlive = true; // if this is set to false then you will have to call ResetApplicationSession every SessionDuration seconds
                bool AllowCertificateNameMismatch = true;
                Console.WriteLine("Starting...");
                int i = serviceProvider.StartApplicationSession(AESServerIp, int.Parse(AESServerPort), SessionName,
                            AESLogin, AESPassword, SessionCleanupDelay, SessionDuration,
                            protocolVersion,
                            useSecureConex,
                            serviceProvider, EnableAutoKeepAlive, AllowCertificateNameMismatch);
                Console.WriteLine("Started.");
            }
            catch (System.Net.Sockets.SocketException)
            {
                HandleException(new ApplicationException(ResourceLoader.GetString2("UnavailableTelephonyServer")));
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
                throw ex;
            }
        }

        private void HandleException(Exception ex)
        {
            SmartLogger.Print(ex);
            currentCallState = TelephonyCallState.Failed;
            throw ApplicationUtil.NewFaultException(ex);
        }

        private void HandleExceptionEvent(Exception ex)
        {
            SmartLogger.Print(ex);
            currentCallState = TelephonyCallState.Failed;
            OnTelephonyError(new TelephonyErrorEventArgs(ex.Message,AgentId,Extension, Queue, DateTime.Now.TimeOfDay));
        }

        #region INIT
        
        // PASO 1.1 - Inicializa los callbacks
        void InitializeCallbacks()
        {
            serviceProvider.OnServerConnectionDownEvent += serviceProvider_OnServerConnectionDownEvent;
            serviceProvider.OnStartApplicationSessionResponse += ServiceProvider_OnStartApplicationSessionResponse;
            serviceProvider.getThirdPartyCallController.OnDeliveredEvent += new DeliveredEventHandler(getThirdPartyCallController_OnDeliveredEvent);
            serviceProvider.getThirdPartyCallController.OnEstablishedEvent += new EstablishedEventHandler(getThirdPartyCallController_OnEstablishedEvent);
            serviceProvider.getThirdPartyCallController.OnRetrievedEvent += new RetrievedEventHandler(getThirdPartyCallController_OnRetrievedEvent);
            serviceProvider.getThirdPartyCallController.OnConnectionClearedEvent += new ConnectionClearedEventHandler(getThirdPartyCallController_OnConnectionClearedEvent);
            serviceProvider.getThirdPartyCallController.OnMakeCallResponse += new MakeCallResponseHandler(getThirdPartyCallController_OnMakeCallResponse);
            serviceProvider.getThirdPartyCallController.OnFailedEvent += new FailedEventHandler(getThirdPartyCallController_OnFailedEvent);
            serviceProvider.getThirdPartyCallController.OnDivertedEvent += new DivertedEventHandler(getThirdPartyCallController_OnDivertedEvent);
            serviceProvider.getThirdPartyCallController.OnGenerateDigitsResponse += new GenerateDigitsResponseHandler(getThirdPartyCallController_OnGenerateDigitsResponse);
            serviceProvider.getThirdPartyCallController.OnDoNotDisturbEvent += getThirdPartyCallController_OnDoNotDisturbEvent;
            serviceProvider.getThirdPartyCallController.OnAgentLoggedOffEvent += getThirdPartyCallController_OnAgentLoggedOffEvent;
            serviceProvider.getThirdPartyCallController.OnClearConnectionResponse += getThirdPartyCallController_OnClearConnectionResponse;
        }

        void getThirdPartyCallController_OnClearConnectionResponse(object sender, ThirdPartyCallController.ClearConnectionResponseArgs e)
        {
        }

        void serviceProvider_OnServerConnectionDownEvent(object sender, ServiceProvider.ServerConnectionDownEventArgs e)
        {
            if (e.getReason != "")
                SmartLogger.Print(e.getReason);
        }

        void getThirdPartyCallController_OnAgentLoggedOffEvent(object sender, ThirdPartyCallController.AgentLoggedOffEventArgs e)
        {
            CurrentCallState = TelephonyCallState.NoCall;
        }

        void getThirdPartyCallController_OnDoNotDisturbEvent(object sender, ThirdPartyCallController.DoNotDisturbEventArgs e)
        {
            OnDoNotDisturbChanged(new DoNotDisturbChangedEventArgs(true));
        }

        /// PASO 2 - Avisa que ya se inicio la session
        string sessionId;
        void ServiceProvider_OnStartApplicationSessionResponse(object sender, ServiceProvider.StartApplicationSessionResponseArgs e)
        {
            sessionId = e.getSessionId;
            if (e.getError != "")
            {
               HandleExceptionEvent(new ApplicationException(ResourceLoader.GetString2("UnavailableTelephonyServer")));
            }

            GetDeviceId();
        }

        /// PASO 2.1 - Toma control de la extension
        private void GetDeviceId()
        {
            currentDevice = serviceProvider.GetNewDevice();

            // Device events
            currentDevice.OnGetDeviceIdResponse += new GetDeviceIdResponseHandler(currentDevice_OnGetDeviceIdResponse);

            //// Phone events
            currentDevice.getPhone.OnRegisterTerminalResponse += currentDevice_getPhone_OnRegisterTerminalResponse;
            currentDevice.getPhone.OnRingerStatusUpdatedEvent += PhoneServicesClass_OnRingerStatusUpdatedEvent;
            currentDevice.getPhone.OnGetDisplayResponse += new GetDisplayResponseHandler(getPhone_OnGetDisplayResponse);

            //// Media events
            currentDevice.getPhone.getMedia.OnStartMonitorResponse += new MediaStartMonitorResponseHandler(getMedia_OnStartMonitorResponse);
            currentDevice.getPhone.getMedia.OnMediaStartedEvent += new MediaStartedEventHandler(getMedia_OnMediaStartedEvent);
            currentDevice.getPhone.getMedia.OnMediaStoppedEvent += new MediaStoppedEventHandler(getMedia_OnMediaStoppedEvent);
            
            bool ControllableByOtherSessions = true;
            // The SwitchName or SwitchIpInterface may be null (but not both)
            int rc = currentDevice.GetDeviceId(Extension, CMServerName, CMServerIp, ControllableByOtherSessions, null);

            if (rc == -1)
            {
                HandleException( new ApplicationException(ResourceLoader.GetString2("TerminalInUse")));
            }
        }

        ///PASO 3 - Avisa que ya se tiene el control, se inician los monitores y se registra la ext.
        void currentDevice_OnGetDeviceIdResponse(object sender, Device.GetDeviceIdResponseArgs e)
        {
            try
            {
                if (e.getError != "")
                {
                    HandleExceptionEvent(new ApplicationException(ResourceLoader.GetString2("TerminalInUse")));
                }
                   
                currentDeviceId = e.getDevice.getDeviceIdAsString;

                StartPhoneMonitors();
                StartThirdPartyMonitors();
                StartMediaMonitors();


                //int acdSplit = serviceProvider.getThirdPartyCallController.GetAcdSplit(currentDeviceId, null);

                RtpIp_Receive = GetLocalIp();
                RtcpIp = RtpIp_Receive;
                
                // Now we want to actually register the terminal 
                Phone.LoginInfo loginInfo = new Phone.LoginInfo()
                    {
                        ExtensionPassword = ExtensionPassword,
                        ForceLogin = true
                    };

                Phone.MediaInfo mediaInfo = new Phone.MediaInfo()
                    {
                        MediaControl = mediaMode,
                        RequestedDependencyMode = dependencyMode,
                        RtpIpAddress = RtpIp_Receive,
                        RtpIpPort = RtpPort_Receive.ToString(),
                        RtcpIpAddress = RtcpIp,
                        RtcpIpPort = RtcpPort.ToString()
                    };

                currentDevice.getPhone.RegisterTerminal(loginInfo, mediaInfo, Extension);
            }
            catch (Exception exc) { }
        }

        private string GetLocalIp()
        {
            string localIP = "?";
            IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (System.Net.IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    localIP = ip.ToString();
                    break;
                }
            }
            return localIP;
        }

        private void StartPhoneMonitors()
        {
            Phone.PhoneEvents PhoneMonitorEvents;
            PhoneMonitorEvents = new Phone.PhoneEvents(false);

            // Configure the events in the monitor that we want to be notified about
            PhoneMonitorEvents.DisplayUpdatedEvent = true;
            PhoneMonitorEvents.HookswitchEvent = true;
            PhoneMonitorEvents.LampModeEvent = false;
            PhoneMonitorEvents.RingerStatusEvent = false;
            PhoneMonitorEvents.TerminalUnregisteredEvent = true;

            // Start the monitor for the phone 
            currentDevice.getPhone.OnStartMonitorResponse += getPhone_OnStartMonitorResponse;
            currentDevice.getPhone.OnTerminalUnregisteredEvent += getPhone_OnTerminalUnregisteredEvent;
            currentDevice.getPhone.OnUnregisterTerminalResponse += getPhone_OnUnregisterTerminalResponse;
            currentDevice.getPhone.StartMonitor(PhoneMonitorEvents, "Phone").ToString();
        }

        void getPhone_OnUnregisterTerminalResponse(object sender, Phone.UnregisterTerminalResponseArgs e)
        {
            currentDevice.getPhone.getMedia.StopMonitor(mediaMonitorId, null);
            currentDevice.getPhone.StopMonitor(phoneMonitorId, null);
            currentDevice.ReleaseDeviceId(null);
            currentDevice = null;
            serviceProvider.getThirdPartyCallController.StopMonitor(thirdPartyMonitorId, null);
            serviceProvider.StopApplicationSession("Application Request", null);
        }

        string phoneMonitorId;

        void getPhone_OnStartMonitorResponse(object sender, Phone.StartMonitorResponseArgs e)
        {
            phoneMonitorId = e.getMonitorId;
        }
        void getPhone_OnTerminalUnregisteredEvent(object sender, Phone.TerminalUnregisteredEventArgs e)
        {
            currentDevice = null;
            OnTerminalUnregistered(new TerminalUnRegisteredEventArgs());
        }

        string thirdPartyMonitorId;
        private void StartThirdPartyMonitors()
        {
            //Esto es lo que me habilita para recibir las llamadas. Monitorea el thirdParty.
            ThirdPartyCallController.ThirdPartyCallControlEvents CallControlEvents = new ThirdPartyCallController.ThirdPartyCallControlEvents(true);
            serviceProvider.getThirdPartyCallController.StartMonitor(currentDeviceId, CallControlEvents, Extension).ToString();
            serviceProvider.getThirdPartyCallController.OnStartMonitorResponse += getThirdPartyCallController_OnStartMonitorResponse;
            serviceProvider.getThirdPartyCallController.OnSetAgentStateResponse += new SetAgentStateResponseHandler(getThirdPartyCallController_OnSetAgentStateResponse);
            serviceProvider.getThirdPartyCallController.OnGetAcdSplitResponse += getThirdPartyCallController_OnGetAcdSplitResponse;
            serviceProvider.OnStartApplicationSessionResponse +=serviceProvider_OnStartApplicationSessionResponse;
        }

        private void serviceProvider_OnStartApplicationSessionResponse(object sender, ServiceProvider.StartApplicationSessionResponseArgs e)
        {
            serviceProvider.ShutDown(ServiceProvider.ServiceProviderObjectDeactivatedEventArgs.ServiceProviderObjectDeactivatedReason.REQUEST_FROM_APPLICATION, "");
            Thread.Sleep(100);
            serviceProvider = null;
        }

        void getThirdPartyCallController_OnGetAcdSplitResponse(object sender, ThirdPartyCallController.GetAcdSplitResponseArgs e)
        {
            int loged = e.getAgentsLoggedIn;
            int available = e.getAvailableAgents;
            int callinqueue = e.getCallsInQueue;

        }

        void getThirdPartyCallController_OnStartMonitorResponse(object sender, ThirdPartyCallController.StartMonitorResponseArgs e)
        {
            thirdPartyMonitorId = e.getMonitorId;
        }


        private void StartMediaMonitors()
        {
            //Inicia el monitor del audio
            Media.MediaEvents MediaEvents = new Media.MediaEvents(false);
            MediaEvents.MediaStartedEvent = true;
            MediaEvents.PlayingEvent = true;
            MediaEvents.PlayingStoppedEvent = true;
            MediaEvents.PlayingSuspendedEvent = true;
            MediaEvents.RecordingEvent = true;
            MediaEvents.RecordingSuspendedEvent = true;
            MediaEvents.TonesDetectedEvent = true;
            MediaEvents.TonesRetrievedEvent = true;

            currentDevice.getPhone.getMedia.StartMonitor(MediaEvents, "Media");
        }

        ///PASO 4 - Avisa que ya se registo!
        void currentDevice_getPhone_OnRegisterTerminalResponse(object sender, Phone.RegisterTerminalResponseArgs e)
        {
            if (e.getError != "" || e.getCode != "1")
            {
                Close();
                HandleExceptionEvent(new ApplicationException(ResourceLoader.GetString2("ExtensionNotProperlyConfigured")));
            }
            else
            {
                CurrentCallState = TelephonyCallState.NoCall;
           }
        }
        #endregion

        #region EVENTS


        #region Third Party Events

        //6.1
        //Avisa si se Logueo en la cola
        void getThirdPartyCallController_OnSetAgentStateResponse(object sender, ThirdPartyCallController.SetAgentStateResponseArgs e)
        {
            if (e.getError != "")
            {
                CurrentCallState = TelephonyCallState.Failed;
            }
            else
            {
                CurrentCallState = TelephonyCallState.NoCall;
                OnAgentRegistered(new AgentRegisteredEventArgs(Extension, AgentId, Queue, TimeSpan.FromTicks(DateTime.Today.Ticks)));
                OnAgentLogin(new AgentLoginEventArgs(Extension, AgentId, Queue, TimeSpan.FromTicks(DateTime.Today.Ticks)));
            }
        }

        /// Avisa que hay una llamada entrante
        void getThirdPartyCallController_OnDeliveredEvent(object sender, ThirdPartyCallController.DeliveredEventEventArgs e)
        {
            if (e.getAlertingDeviceId.StartsWith(currentDeviceId))
            {
                if (IsReady && currentCallState== TelephonyCallState.NoCall)
                {
                    callingDeviceId = e.getCallingDeviceId;
                    currentCallId = e.getConnectionId.getCallId;
                    RtpIp_ToSend = RtpIp_ToSend_temp;
                    RtpPort_ToSend = RtpPort_ToSend_temp;

                    CurrentCallState = TelephonyCallState.Alerting;
                    OnCallRinging(new CallRingingEventArgs(callingDeviceId, AgentId, "", TimeSpan.FromTicks(DateTime.Now.Ticks)));
                }
                else
                {
                    RtpIp_ToSend = RtpIp_ToSend_temp;
                    RtpPort_ToSend = RtpPort_ToSend_temp;

                    waitingDeviceId = e.getCallingDeviceId;
                    waitingCallId = e.getConnectionId.getCallId;
                }
            }
        }

        //Se redirecciono la llamada a la cola
        void getThirdPartyCallController_OnDivertedEvent(object sender, ThirdPartyCallController.DivertedEventEventArgs e)
        {
            OnCallReleased(new CallReleasedEventArgs("", AgentId, Queue, TimeSpan.FromTicks(DateTime.Today.Ticks)));
            OnAgentNotReady(new AgentNotReadyEventArgs(Extension, AgentId, Queue, TimeSpan.FromTicks(DateTime.Today.Ticks), ""));
            DropCall();
            CurrentCallState = TelephonyCallState.NoCall;
        }

        //Se Atendio la llamada
        void getThirdPartyCallController_OnEstablishedEvent(object sender, ThirdPartyCallController.EstablishedEventArgs e)
        {
            waitingCallId = null;
            CurrentCallState = TelephonyCallState.Established;

            if (e.getCallingDeviceId == currentDeviceId)
            {
                RtpPort_ToSend = RtpPort_ToSend_temp;
                StartMediaThreads();
                if(string.IsNullOrEmpty(AgentId))
                    OnCallMade(new CallMadeEventArgs());
            }
            else
            {
                OnCallEstablished(new CallEstablishedEventArgs("", AgentId, Queue, TimeSpan.FromTicks(DateTime.Today.Ticks)));
            }
        }

        //Se cayo o tranque la llamada
        void getThirdPartyCallController_OnConnectionClearedEvent(object sender, ThirdPartyCallController.ConnectionClearedEventArgs e)
        {
            if (CurrentCallState == TelephonyCallState.LoggingIn)
            {
                #region LoggingIn
                int state = serviceProvider.getThirdPartyCallController.GetAgentLogin(currentDeviceId, null);
                if (state == 9)
                {
                    CurrentCallState = TelephonyCallState.Failed;
                }
                else
                {
                    CurrentCallState = TelephonyCallState.AutoIn;
                    OnAgentRegistered(new AgentRegisteredEventArgs(Extension, AgentId, Queue, TimeSpan.FromTicks(DateTime.Today.Ticks)));
                    OnAgentLogin(new AgentLoginEventArgs(Extension, AgentId, Queue, TimeSpan.FromTicks(DateTime.Today.Ticks)));
                    EnableAutoIn();
                }
                #endregion
            }
            else if (CurrentCallState == TelephonyCallState.AutoIn)
            {
                CurrentCallState = TelephonyCallState.NoCall;
            }
            else if (CurrentCallState == TelephonyCallState.DisableAutoIn)
            {
                CurrentCallState = TelephonyCallState.DoNotDisturb;
            }
            else if (currentCallState == TelephonyCallState.DoNotDisturb)
            {
                #region DoNotDisturb

                if (waitingCallId != null)
                {
                    waitingCallId = null;
                    waitingDeviceId = null;
                    waitingRtpIp_ToSend = null;
                    waitingRtpPort_ToSend = -1;
                    OnCallAbandoned(new CallAbandonedEventArgs("", AgentId, Queue, TimeSpan.FromTicks(DateTime.Today.Ticks)));
                }
                #endregion
            }
            else if (currentCallState == TelephonyCallState.Alerting)
            {
                #region Alerting
                currentCallState = TelephonyCallState.NoCall;

                waitingCallId = null;
                waitingDeviceId = null;
                waitingRtpIp_ToSend = null;
                waitingRtpPort_ToSend = -1;

                OnCallAbandoned(new CallAbandonedEventArgs("", AgentId, Queue, TimeSpan.FromTicks(DateTime.Today.Ticks)));
                StopMediaThreads(true);

                #endregion
            }
            else if (currentCallState == TelephonyCallState.Established)
            {
                #region Established
                currentCallState = TelephonyCallState.DoNotDisturb;

                if (e.getReleasingDeviceId == currentDeviceId)
                {
                    OnCallReleased(new CallReleasedEventArgs("", AgentId, Queue, TimeSpan.FromTicks(DateTime.Today.Ticks)));
                }
                else
                {
                    OnCallAbandoned(new CallAbandonedEventArgs("", AgentId, Queue, TimeSpan.FromTicks(DateTime.Today.Ticks)));
                }
                StopMediaThreads();
                #endregion
            }
            else if (currentCallState == TelephonyCallState.NoCall)
            {
                #region NoCall
                CurrentCallState = TelephonyCallState.NoCall;
                if (e.getReleasingDeviceId == currentDeviceId)
                {
                    OnCallReleased(new CallReleasedEventArgs("", AgentId, Queue, TimeSpan.FromTicks(DateTime.Today.Ticks)));
                }
                else
                {
                    OnCallAbandoned(new CallAbandonedEventArgs("", AgentId, Queue, TimeSpan.FromTicks(DateTime.Today.Ticks)));
                }
                StopMediaThreads();
                #endregion
            }
        }

        //Recupera la llamada luego de que estaba en HOLD
        void getThirdPartyCallController_OnRetrievedEvent(object sender, ThirdPartyCallController.RetrievedEventArgs e)
        {

        }

        private void PhoneServicesClass_OnRingerStatusUpdatedEvent(object sender, Phone.RingerStatusUpdatedEventArgs e)
        {

        }

        //Avisa si hubo un error al intentar llamar
        //public ThirdPartyCallController.CallIdentifier CallIdentifier { get; set; }
        void getThirdPartyCallController_OnMakeCallResponse(object sender, ThirdPartyCallController.MakeCallResponseArgs e)
        {
            //CallIdentifier = e.getCallingDeviceConnectionId;
            if (e.getCallingDeviceConnectionId != null)
            {
                callingDeviceId = e.getCallingDeviceConnectionId.getDeviceIdAsString;
                currentCallId = e.getCallingDeviceConnectionId.getCallId;
            }
            if (e.getError != "")
            {
                //if (e.getError.ToLower().Contains("resource busy")) 
                    DropCall();
                SmartLogger.Print(ResourceLoader.GetString2("InvalidCall"));
            }
        }

        /// <summary>
        /// Get the event response for the Generate digits, this is use to send dtmf tone.
        /// Example: After calling the radio extention the frecuency code has to be given.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void getThirdPartyCallController_OnGenerateDigitsResponse(object sender, ThirdPartyCallController.GenerateDigitsResponseArgs e)
        {
            if (e.getError != "")
            {
                SmartLogger.Print(ResourceLoader.GetString2("InvalidDigitsSend"));
            }
            OnDigitsReceived(new DigitsReceivedEventArgs());
        }

        void getThirdPartyCallController_OnFailedEvent(object sender, ThirdPartyCallController.FailedEventArgs e)
        {
            DropCall();
        }
        #endregion

        #region Media Events
        string mediaMonitorId;
        void getMedia_OnStartMonitorResponse(object sender, Media.StartMonitorResponseArgs e)
        {
            mediaMonitorId = e.getMonitorId;
        }

        void getPhone_OnGetDisplayResponse(object sender, Phone.GetDisplayResponseArgs e)
        {
            try
            {
                // Save the display
                currentDisplay = e.getContentsOfDisplay.Trim();
            }
            catch (Exception exc) { }
        }

        private void getMedia_OnMediaStartedEvent(object sender, Media.MediaStartedEventArgs e)
        {
            //if (currentCallState == TelephonyCallState.Established
            //    || currentCallState == TelephonyCallState.Alerting
            //    || currentCallState == TelephonyCallState.NoCall
            //    || currentCallState == TelephonyCallState.DoNotDisturb)
            //{
                RtpPacketSize_ToSend = e.getPacketSize;
                RtpIp_ToSend_temp = e.getRtpIpAddress.getIpAddress;
                RtpPort_ToSend_temp = e.getRtpIpAddress.getPort;
            //}
        }

        void getMedia_OnMediaStoppedEvent(object sender, Media.MediaStoppedEventArgs e)
        {
            StopMediaThreads();
        }
        #endregion

        #endregion

        #region INTERNAL METHODS
        private void StartMediaThreads()
        {
            try
            {
                StopMediaThreads();

                StartLumisoft();

                Console.WriteLine("IP to transmit {0} - {1}", RtpIp_ToSend, RtpPort_ToSend);
            }
            catch (Exception exc)
            {
            }
        }

        void StopMediaThreads(bool discardAudio = false)
        {
            try
            {
                //STOP SPEAKERS
                shuttingDown = true;

                StopLumisoft(discardAudio);
            }
            catch (Exception e) { }
        }

        void DropCall()
        {
            try
            {
                Console.WriteLine("Dropping Call");
                serviceProvider.getThirdPartyCallController.ClearConnection
                    (new ThirdPartyCallController.CallIdentifier(currentDeviceId.ToString(), currentCallId), null);
            }
            catch (Exception ex)
            {
            }
        }

        void RejectCall()
        {
            try
            {
                serviceProvider.getThirdPartyCallController.ClearCall(
                    new ThirdPartyCallController.CallIdentifier(currentDeviceId.ToString(), currentCallId), null);
            }
            catch (Exception ex)
            {
            }
        }
        #endregion

        public override void Login(string agentId, string agentPassword, bool isReady)
        {
            if (!string.IsNullOrEmpty(AgentId) && !string.IsNullOrEmpty(AgentPassword))
            {
                while (CurrentCallState == TelephonyCallState.Initializing)
                {
                    Thread.Sleep(50);
                }
                if (CurrentCallState == TelephonyCallState.Failed)
                {
                    Logout();
                    Close();
                    HandleException(new ApplicationException(ResourceLoader.GetString2("ExtensionNotProperlyConfigured")));
                }

                try
                {
                    CurrentCallState = TelephonyCallState.LoggingIn;
                    MakeCall("*22" + agentId + AgentPassword);

                    IsReady = isReady;

                    while (CurrentCallState == TelephonyCallState.LoggingIn)
                    {
                        Thread.Sleep(10);
                    }
                    if (CurrentCallState == TelephonyCallState.Failed) throw new Exception();
                }
                catch (Exception exc)
                {
                    Logout();
                    Close();
                    HandleException(new TelephonyException(ResourceLoader.GetString2("UnableLoginInTerminal"), exc));
                }
            }
        }

        private void MakeCall(string number, bool callingOutside = false)
        {
            Console.WriteLine("Calling " + number);
            if (CurrentCallState != TelephonyCallState.Established)
            {
                callingDeviceId = number;
                int extLength = currentDeviceId.Split(':')[0].Length;
                string directoryNumber = number + currentDeviceId.Substring(extLength, currentDeviceId.Length - extLength);

                serviceProvider.getThirdPartyCallController.MakeCall(currentDeviceId, directoryNumber, null);

                if (callingOutside)
                {
                    StartMediaThreads();
                }
            }
            else
            {
                serviceProvider.getThirdPartyCallController.GenerateDigits(new ThirdPartyCallController.CallIdentifier(currentDeviceId.ToString(), currentCallId), number, null);
            }
        }

        public override void Call(string number)
        {
            MakeCall(number, true);
        }
        
        public override void Logout()
        {
            if (!string.IsNullOrEmpty(AgentId) && !string.IsNullOrEmpty(AgentPassword))
            {
                try
                {

                    CurrentCallState = TelephonyCallState.LoggingOut;
                    MakeCall("*23");
                    int i = 0;
                    while (CurrentCallState == TelephonyCallState.LoggingOut && i++ < 20)
                    {
                        Thread.Sleep(50);
                    }
                }
                catch (Exception ex)
                {
                    SmartLogger.Print(ex);
                }
            }
        }

        public override void Hold()
        {
            try
            {
                if (currentCallId != "")
                {
                    serviceProvider.getThirdPartyCallController.HoldCall(
                        new ThirdPartyCallController.CallIdentifier(currentDeviceId, currentCallId), null);
                    CurrentCallState = TelephonyCallState.Held;
                }
            }
            catch (Exception exc) { }
        }

        public override void SetReadyStatus(bool ready)
        {
            if (ready)
            {
                if (waitingCallId != null)
                {
                    Console.WriteLine("Se puso en RINGING");
                    CurrentCallState = TelephonyCallState.Alerting;
                    currentCallId = waitingCallId;
                    callingDeviceId = waitingDeviceId;
                    RtpIp_ToSend  = waitingRtpIp_ToSend;
                    RtpPort_ToSend = waitingRtpPort_ToSend;

                    OnCallRinging(new CallRingingEventArgs(callingDeviceId, AgentId, "",
                        TimeSpan.FromTicks(DateTime.Now.Ticks)));

                    waitingCallId = null;
                }
                else
                {
                    CurrentCallState = TelephonyCallState.AutoIn;
                    EnableAutoIn();
                    IsReady = true;
                    Console.WriteLine("Se puso en Ready");
                }
            }
            else
            {
                if (waitingCallId == null || IsReady)
                {
                    CurrentCallState = TelephonyCallState.DisableAutoIn;
                    DisableAutoIn();
                }
                IsReady = false;
                Console.WriteLine("Se puso en NO Ready");
            }
        }

        private void EnableAutoIn()
        {
            try
            {
                //This feature should be changed in version 6.1
                MakeCall("*20");
            }
            catch (Exception e)
            {
            }
        }

        private void DisableAutoIn()
        {
            try
            {
                //This feature should be changed in version 6.1
                MakeCall("*21");
                //StopMediaThreads();
            }
            catch (Exception e)
            {
            }
        }

        public override bool GetReadyStatus()
        {
            return IsReady;
        }

        public override void Answer()
        {
            try
            {
                if (waitingCallId != null)
                {
                    currentCallId = waitingCallId;
                    callingDeviceId = waitingDeviceId;
                    RtpIp_ToSend = waitingRtpIp_ToSend;
                    RtpPort_ToSend = waitingRtpPort_ToSend;
                    waitingCallId = null; 
                    callingDeviceId = null;


                    Console.WriteLine("Answering waiting call, IP to transmit {0} - {1}", RtpIp_ToSend, RtpPort_ToSend);
                }
                StartMediaThreads();

                serviceProvider.getThirdPartyCallController.AnswerCall(new ThirdPartyCallController.CallIdentifier(
                    currentDeviceId, currentCallId), null);
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        public override void Release()
        {
            DropCall();
        }

        //PENDING
        public override void Transfer()
        {
        }

        //PENDING
        public override void ParkCall(string extensionParking, string parkId)
        {
        }

        //PENDING
        public override void Conference()
        {
        }

        public override void Close()
        {
            try
            {
                currentDevice.getPhone.UnregisterTerminal(null);
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        public override void RecoverConnection()
        {
            try
            {
                currentDevice = null;
                DropCall();
                CurrentCallState = TelephonyCallState.Initializing;
                GetDeviceId();
                Login(AgentId, AgentPassword, true);



                ///RETREIVE CALL
                //if (currentCallId != "")
                //{
                //    serviceProvider.getThirdPartyCallController.RetrieveCall(
                //        new ThirdPartyCallController.CallIdentifier(currentDeviceId, currentCallId), null);
                //    CurrentCallState = TelephonyCallState.Established;
                //}
            }
            catch (Exception exc) { }
        }

        public override void SetPhoneReport(string customCode){
            PhoneReportCustomCode = customCode;
        }

        #region Lumisoft

        private RTP_MultimediaSession m_pRtpSession = null;
        private RTP_MultimediaSession m_pRtpLOCALSession = null;
        
        private Dictionary<int, AudioCodec> m_pAudioCodecs = null;
        private AudioCodec m_pActiveCodec = null;

        private RTP_Session m_pSession = null;
        private AudioIn_RTP m_pAudioInRTP = null;
        private RTP_SendStream m_pSendStream = null;
        private AudioOut_RTP m_pAudioOut = null;

        NAudio.Wave.WaveFileWriter audioFile = null;
        NAudio.Wave.WaveFileWriter microAudioFile = null;
        string incommingAudioFileName;
        
        private void StartLumisoft()
        {
            m_pAudioCodecs = new Dictionary<int, AudioCodec>();
            m_pAudioCodecs.Add(0, new PCMU());
            m_pAudioCodecs.Add(8, new PCMA());

            m_pActiveCodec = new PCMA();

            m_pRtpSession = new RTP_MultimediaSession(RTP_Utils.GenerateCNAME());
            m_pRtpSession.CreateSession(new RTP_Address(System.Net.IPAddress.Parse(RtpIp_Receive), 
                RtpPort_Receive, RtcpPort), new RTP_Clock(0, 8000));

            m_pRtpSession.Sessions[0].NewReceiveStream += m_pRtpSession_NewReceiveStream;
            m_pRtpSession.Sessions[0].Payload = 0;
            m_pRtpSession.Sessions[0].Start();
           
        }
        private void m_pRtpLOCALSession_NewReceiveStream(object sender, RTP_ReceiveStreamEventArgs e)
        {
            e.Stream.Closed += new EventHandler(LOCALStream_Closed);
            e.Stream.PacketReceived += new EventHandler<RTP_PacketEventArgs>(LOCALStream_PacketReceived);
        }

        private void LOCALStream_Closed(object sender, EventArgs e)
        {
            ((RTP_ReceiveStream)sender).PacketReceived -= LOCALStream_PacketReceived;
            ((RTP_ReceiveStream)sender).Closed -= LOCALStream_Closed;
        }

        private void LOCALStream_PacketReceived(object sender, RTP_PacketEventArgs e)
        {
            byte[] decoded = codec.Decode(e.Packet.Data, 0, e.Packet.Data.Length);
            microAudioFile.Write(decoded, 0, decoded.Length);
        }

        private void StopLumisoft(bool discardAudio)
        {
            try
            {
                if (m_pAudioInRTP != null)
                {
                    m_pAudioInRTP.Dispose();
                    m_pAudioInRTP = null;
                }
            }
            catch { }

            try
            {
                if (m_pSendStream != null)
                {
                    m_pSendStream.Close();
                    m_pSendStream = null;
                }
            }
            catch { }

            try
            {
                if (m_pRtpSession != null)
                {
                    m_pRtpSession.Dispose();
                    m_pRtpSession = null;
                }
            }
            catch { }

            if (recordCalls)
            {
                try
                {
                    if (m_pRtpLOCALSession != null)
                    {
                        m_pRtpLOCALSession.Dispose();
                        m_pRtpLOCALSession = null;
                    }
                }
                catch { }

                try
                {
                    if (audioFile != null)
                    {
                        audioFile.Close();
                        audioFile.Dispose();
                        audioFile = null;
                    }
                }
                catch { }

                if (microAudioFile != null)
                {
                    try
                    {
                        microAudioFile.Close();
                        microAudioFile.Dispose();
                        microAudioFile = null;
                    }
                    catch { }

                    if (discardAudio)
                    {
                        new Thread(new ThreadStart(DeleteAudioFiles)).Start();
                    }
                    else
                    {
                        new Thread(new ThreadStart(MergeTwoAudioFiles)).Start();
                    }
                }
            }
        }

        

        #region method m_pRtpSession_NewSendStream

        #endregion

        #region method m_pRtpSession_NewReceiveStream

        /// <summary>
        /// This method is called when RTP session gets new receive stream.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event data.</param>
        private void m_pRtpSession_NewReceiveStream(object sender, RTP_ReceiveStreamEventArgs e)
        {
            if (m_pRtpSession.Sessions[0].Targets.Count() == 0)
            {
                RTP_Source remote = e.Stream.Session.Members.Where(m => m.IsLocal == false).FirstOrDefault();
                m_pRtpSession.Sessions[0].AddTarget(new RTP_Address(remote.RtpEP.Address,
                    remote.RtpEP.Port, remote.RtpEP.Port + 1));

                if (remote.RtpEP.Port != RtpPort_ToSend)
                {
                    m_pRtpSession.Sessions[0].AddTarget(new RTP_Address(remote.RtpEP.Address,
                       RtpPort_ToSend, RtpPort_ToSend + 1));
                }
                

                m_pSendStream = e.Stream.Session.CreateSendStream();
                m_pAudioInRTP = new AudioIn_RTP(AudioIn.Devices[0], RtpPacketSize_ToSend, m_pAudioCodecs, m_pSendStream);
                m_pAudioInRTP.Start();


                if (recordCalls)
                {
                    lock (locker)
                    {
                        //records incomming and local audio in separated audio files
                        incommingAudioFileName = SmartCadConfiguration.AudioRecordingFolder + "/" + DateTime.Now.ToString("HHmmssfff_ddMMyyyy");
                        audioFile = new NAudio.Wave.WaveFileWriter(incommingAudioFileName + ".wav", codec.RecordFormat);
                        microAudioFile = new NAudio.Wave.WaveFileWriter(incommingAudioFileName + "_mic.wav", codec.RecordFormat);
                        //Re-send the audio to it self to be recordered
                        m_pRtpSession.Sessions[0].AddTarget(new RTP_Address(System.Net.IPAddress.Parse(RtpIp_Receive),
                             RtpPort_Receive + 10, RtpPort_Receive + 11));

                        //Creates a session to receive it locally
                        m_pRtpLOCALSession = new RTP_MultimediaSession(RTP_Utils.GenerateCNAME());
                        m_pRtpLOCALSession.CreateSession(new RTP_Address(System.Net.IPAddress.Parse(RtpIp_Receive),
                                                        RtpPort_Receive + 10, RtpPort_Receive + 11), new RTP_Clock(0, 8000));
                        m_pRtpLOCALSession.Sessions[0].NewReceiveStream += m_pRtpLOCALSession_NewReceiveStream;
                        m_pRtpLOCALSession.Sessions[0].Payload = 0;
                        m_pRtpLOCALSession.Sessions[0].Start();
                    }
                }

                if (m_pAudioOut != null)
                {
                    m_pAudioOut.Dispose();
                }

                if (recordCalls)
                {
                    e.Stream.Closed += new EventHandler(Stream_Closed);
                    e.Stream.PacketReceived += Stream_PacketReceived;
                }

                m_pAudioOut = new AudioOut_RTP(AudioOut.Devices[0], e.Stream, m_pAudioCodecs);
                m_pAudioOut.Start();
            }
        }

        void Stream_PacketReceived(object sender, RTP_PacketEventArgs e)
        {
            byte[] decoded = codec.Decode(e.Packet.Data, 0, e.Packet.Data.Length);
            audioFile.Write(decoded, 0, decoded.Length);
        }
       
        void Stream_Closed(object sender, EventArgs e)
        {
            ((RTP_ReceiveStream)sender).PacketReceived -= Stream_PacketReceived;
            ((RTP_ReceiveStream)sender).Closed -= Stream_Closed;
            if(!shuttingDown)
                StopMediaThreads();
        }

        #endregion

        Object locker = new Object();
        //The voice incomming and the microphone are recorded in separated files
        //here these files are meerged in one single stereo file.
        private void MergeTwoAudioFiles()
        {
            try
            {
                lock (locker)
                {
                    string audioFileNameSafeCopy = incommingAudioFileName;
                    string phoneReportCustomCodeSafeCopy = PhoneReportCustomCode;
                    incommingAudioFileName = "";
                    PhoneReportCustomCode = "";

                    if (audioFileNameSafeCopy != "" && File.Exists(audioFileNameSafeCopy + ".wav")
                        && phoneReportCustomCodeSafeCopy != "")
                    {
                        Console.WriteLine("Merging Audio Files " + audioFileNameSafeCopy);

                        WaveMixerStream32 mixer = new WaveMixerStream32();
                        mixer.AutoStop = true;
                        mixer.AddInputStream(new WaveChannel32(new WaveFileReader(audioFileNameSafeCopy + ".wav"), 1, 0));
                        mixer.AddInputStream(new WaveChannel32(new WaveFileReader(audioFileNameSafeCopy + "_mic.wav"), 1, 0));
                        Wave32To16Stream wavmixer = new Wave32To16Stream(mixer);
                        WaveFileWriter.CreateWaveFile(audioFileNameSafeCopy + "_mixed.wav", wavmixer);
                        wavmixer.Dispose();
                        mixer.Dispose();

                        File.Delete(audioFileNameSafeCopy + ".wav");
                        File.Delete(audioFileNameSafeCopy + "_mic.wav");

                        OnCallRecorded(new CallRecordedEventArgs(phoneReportCustomCodeSafeCopy));
                    }
                }
            }
            catch { /*Keep going*/}
        }

        private void DeleteAudioFiles()
        {
            try
            {
                lock (locker)
                {
                    Console.WriteLine("Deleting Audio Files " + incommingAudioFileName);

                    File.Delete(incommingAudioFileName + ".wav");
                    File.Delete(incommingAudioFileName + "_mic.wav");
                }
            }
            catch { /*Keep going*/}
        }

        #endregion


        public int RtpPort_ToSend_temp { get; set; }

        public string RtpIp_ToSend_temp { get; set; }

        public int waitingRtpPort_ToSend { get; set; }

        public string waitingRtpIp_ToSend { get; set; }
    }

    public class RTPPacket
    {
        /// <summary>
        /// Konstruktor
        /// </summary>
        public RTPPacket()
        {

        }
        /// <summary>
        /// Konstuktor
        /// </summary>
        /// <param name="_data"></param>
        public RTPPacket(byte[] data)
        {
            Parse(data);
        }

        //Attribute
        public static int MinHeaderLength = 12;
        public int HeaderLength = MinHeaderLength;
        public int Version = 0;
        public bool Padding = false;
        public bool Extension = false;
        public int CSRCCount = 0;
        public bool Marker = false;
        public int PayloadType = 0;
        public UInt16 SequenceNumber = 0;
        public uint Timestamp = 0;
        public uint SourceId = 0;
        public Byte[] Data;

        /// <summary>
        /// Parse
        /// </summary>
        /// <param name="linearData"></param>
        private void Parse(Byte[] data)
        {
            if (data.Length >= MinHeaderLength)
            {
                Version = ValueFromByte(data[0], 6, 2);
                Padding = Convert.ToBoolean(ValueFromByte(data[0], 5, 1));
                Extension = Convert.ToBoolean(ValueFromByte(data[0], 4, 1));
                CSRCCount = ValueFromByte(data[0], 0, 4);
                Marker = Convert.ToBoolean(ValueFromByte(data[1], 7, 1));
                PayloadType = ValueFromByte(data[1], 0, 7);
                HeaderLength = MinHeaderLength + (CSRCCount * 4);

                //Sequence Nummer
                Byte[] seqNum = new Byte[2];
                seqNum[0] = data[3];
                seqNum[1] = data[2];
                SequenceNumber = System.BitConverter.ToUInt16(seqNum, 0);

                //TimeStamp
                Byte[] timeStmp = new Byte[4];
                timeStmp[0] = data[7];
                timeStmp[1] = data[6];
                timeStmp[2] = data[5];
                timeStmp[3] = data[4];
                Timestamp = System.BitConverter.ToUInt32(timeStmp, 0);

                //SourceId
                Byte[] srcId = new Byte[4];
                srcId[0] = data[8];
                srcId[1] = data[9];
                srcId[2] = data[10];
                srcId[3] = data[11];
                SourceId = System.BitConverter.ToUInt32(srcId, 0);

                //Daten kopieren
                Data = new Byte[data.Length - HeaderLength];
                Array.Copy(data, HeaderLength, this.Data, 0, data.Length - HeaderLength);
            }
        }
        /// <summary>
        /// GetValueFromByte
        /// </summary>
        /// <param name="value"></param>
        /// <param name="startPos"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        private Int32 ValueFromByte(Byte value, int startPos, int length)
        {
            Byte mask = 0;
            //Maske erstellen
            for (int i = 0; i < length; i++)
            {
                mask = (Byte)(mask | 0x1 << startPos + i);
            }

            //Ergebnis
            Byte result = (Byte)((value & mask) >> startPos);
            //Fertig
            return Convert.ToInt32(result);
        }
        /// <summary>
        /// ToBytes
        /// </summary>
        /// <returns></returns>
        public Byte[] ToBytes()
        {
            //Ergebnis
            Byte[] bytes = new Byte[RTPPacket.MinHeaderLength];

            //Byte 0
            bytes[0] = (Byte)(Version << 6);
            bytes[0] |= (Byte)(Convert.ToInt32(Padding) << 5);
            bytes[0] |= (Byte)(Convert.ToInt32(Extension) << 4);
            bytes[0] |= (Byte)(Convert.ToInt32(CSRCCount));

            //Byte 1
            bytes[1] = (Byte)(Convert.ToInt32(Marker) << 7);
            bytes[1] |= (Byte)(Convert.ToInt32(PayloadType));

            //Byte 2 + 3
            Byte[] bytesSequenceNumber = BitConverter.GetBytes(SequenceNumber);
            bytes[2] = bytesSequenceNumber[1];
            bytes[3] = bytesSequenceNumber[0];

            //Byte 4 bis 7
            Byte[] bytesTimeStamp = BitConverter.GetBytes(Timestamp);
            bytes[4] = bytesTimeStamp[3];
            bytes[5] = bytesTimeStamp[2];
            bytes[6] = bytesTimeStamp[1];
            bytes[7] = bytesTimeStamp[0];

            //Byte 8 bis 11
            Byte[] bytesSourceId = BitConverter.GetBytes(SourceId);
            bytes[8] = bytesSourceId[3];
            bytes[9] = bytesSourceId[2];
            bytes[10] = bytesSourceId[1];
            bytes[11] = bytesSourceId[0];

            //Fertig
            return bytes;
        }
    }
}
