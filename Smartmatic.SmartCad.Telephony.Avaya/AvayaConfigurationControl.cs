﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SmartCadCore.Common;
using SmartCadCore.Core;

namespace Smartmatic.SmartCad.Telephony.Avaya
{ 
    public partial class AvayaConfigurationControl : UserControl, IConfigurationControl
    {
        public AvayaConfigurationControl()
        {
            InitializeComponent();
            LoadLanguage();
        }

        void LoadLanguage()
        {
            groupControlTelephony.Text = ResourceLoader.GetString2("TelephonyConfiguration");
            groupControlCMServer.Text = ResourceLoader.GetString2("Communication Manager");
            groupControlAESServer.Text = ResourceLoader.GetString2("Application Enablement Services");
            groupControlStatistics.Text = ResourceLoader.GetString2("CMSStatisticConfiguration");
            groupControlMediaGateway.Text = ResourceLoader.GetString2("Media Gateway");
            labelExAESIp.Text = ResourceLoader.GetString2("Server") + ":*";
            labelExAESLogin.Text = ResourceLoader.GetString2("UserName") + ":*";
            labelExAESPassword.Text = ResourceLoader.GetString2("Password") + ":*";
            labelExCMName.Text = ResourceLoader.GetString2("Switch Name") + ":*";
            labelExCMIp.Text = ResourceLoader.GetString2("IP Address") + ":*";
            labelExSecure.Text = ResourceLoader.GetString2("Secure Socket");
            labelExMGIp.Text = ResourceLoader.GetString2("Ip") + ":";
            labelExDriver.Text = ResourceLoader.GetString2("Driver") + ":";
            labelExHost.Text = ResourceLoader.GetString2("Host") + ":";
            labelExServer.Text = ResourceLoader.GetString2("Server") + ":";
            labelExService.Text = ResourceLoader.GetString2("Service") + ":";
            labelExDatabase.Text = ResourceLoader.GetString2("Database") + ":";
            labelExUid.Text = ResourceLoader.GetString2("PersonId") + ":";
            labelExPwd.Text = ResourceLoader.GetString2("Password") + ":";   
        }

        #region IConfigurationControl Members

        public event Parameters_ChangeEvent FulFilled_Change;

        public void SetProperties(Dictionary<string, string> properties)
        {
            foreach (string propName in properties.Keys)
            {
                switch (propName)
                {
                    case AvayaProperties.AES_IP:
                        textBoxExAESIp.Text = properties[propName];
                        break;
                    case AvayaProperties.AES_PORT:
                        textBoxExAESPort.Text = properties[propName];
                        break;
                    case AvayaProperties.AES_LOGIN:
                        textBoxExAESLogin.Text = properties[propName];
                        break;
                    case AvayaProperties.AES_PASSWORD:
                        if(!string.IsNullOrEmpty(properties[propName].Trim()))
                            textBoxExAESPassword.Text = CryptoUtil.DecryptWithRijndael(properties[propName]).TrimEnd('\0');
                        break;
                    case AvayaProperties.CM_IP:
                        textBoxExCMIp.Text = properties[propName];
                        break;
                    case AvayaProperties.CM_NAME:
                        textBoxExCMName.Text = properties[propName];
                        break;
                    case AvayaProperties.SECURE_SOCKET:
                        checkEditSecure.Checked = (properties[propName] == true.ToString());
                        break;
                    case AvayaProperties.MEDIA_GATEWAY:
                        textBoxExMediaGateway.Text = properties[propName];
                        break;
                    default:
                        break;
                }
            }
        }

        public Dictionary<string, string> GetProperties()
        {
            Dictionary<string, string> props = new Dictionary<string, string>();

            props.Add(AvayaProperties.AES_IP, textBoxExAESIp.Text);
            props.Add(AvayaProperties.AES_PORT, textBoxExAESPort.Text);
            props.Add(AvayaProperties.AES_LOGIN, textBoxExAESLogin.Text);
            props.Add(AvayaProperties.SECURE_SOCKET, checkEditSecure.Checked.ToString());

            string encryptedPass = "";
            if (string.IsNullOrEmpty(textBoxExAESPassword.Text.Trim()) == false)
                encryptedPass = CryptoUtil.EncryptWithRijndael(textBoxExAESPassword.Text);
            props.Add(AvayaProperties.AES_PASSWORD, encryptedPass);

            props.Add(AvayaProperties.CM_IP, textBoxExCMIp.Text);
            props.Add(AvayaProperties.CM_NAME, textBoxExCMName.Text);
            props.Add(CommonProperties.Queue, "");
            props.Add(AvayaProperties.MEDIA_GATEWAY, textBoxExMediaGateway.Text);

            return props;
        }

        public void SetStatisticProperties(string[] statisticProperties)
        {
            if (!string.IsNullOrEmpty(statisticProperties[3]))
            {
                string[] properties = statisticProperties[3].Split(';');
                textBoxExDriver.Text = properties[0].Split('=')[1].Replace("{", "").Replace("}", "");
                textBoxExHost.Text = properties[1].Split('=')[1];
                textBoxExServer.Text = properties[2].Split('=')[1];
                textBoxExService.Text = properties[3].Split('=')[1];
                textBoxExProtocol.Text = properties[4].Split('=')[1];
                textBoxExDatabase.Text = properties[5].Split('=')[1];
                textBoxExUid.Text = properties[6].Split('=')[1];
                textBoxExPwd.Text = properties[7].Split('=')[1];
                textBoxExCLocale.Text = properties[8].Split('=')[1];
                textBoxExDBLocale.Text = properties[9].Split('=')[1];
            }
        }

        public string[] GetStatisticProperties()
        {
            string connectionString =
                "Driver={"+ textBoxExDriver.Text + "};" +
                "Host=" + textBoxExHost.Text + ";" +
                "Server=" + textBoxExServer.Text + ";" +
                "Service=" + textBoxExService.Text + ";" +
                "Protocol=" + textBoxExProtocol.Text + ";" +
                "Database=" + textBoxExDatabase.Text + ";" +
                "Uid=" + textBoxExUid.Text + ";" +
                "Pwd=" + textBoxExPwd.Text + ";" +
                "CLIENT_LOCALE=" + textBoxExCLocale.Text + ";" +
                "DB_LOCALE=" + textBoxExDBLocale.Text + ";";
            
            string[] props = new string[8];
            props[0] = "";
            props[1] = "";
            props[2] = "";
            props[3] = connectionString;
            props[4] = "";
            props[5] = "";

            return props;
        }
        #endregion

        bool isFullFilled = false;
        private void textBoxEx_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBoxExAESIp.Text.Trim()) ||
                string.IsNullOrEmpty(textBoxExAESPort.Text.Trim()) ||
                string.IsNullOrEmpty(textBoxExAESLogin.Text.Trim()) ||
                string.IsNullOrEmpty(textBoxExAESPassword.Text.Trim()) ||
                string.IsNullOrEmpty(textBoxExCMIp.Text.Trim()) ||
                string.IsNullOrEmpty(textBoxExCMName.Text.Trim()))
            {
                if (isFullFilled && FulFilled_Change != null)
                {
                    FulFilled_Change(false);
                }
                isFullFilled = false;
            }
            else
            {
                if (!isFullFilled && FulFilled_Change != null)
                {
                    FulFilled_Change(true);
                }
                isFullFilled = true;
            }
        }
    }
}
