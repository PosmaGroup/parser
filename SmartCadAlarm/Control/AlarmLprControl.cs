﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SmartCadCore.Common;

namespace SmartCadAlarm.Controls
{
    
	public partial class AlarmLprControl: UserControl
	{
        public AlarmLprControl()
		{
			InitializeComponent();

            Text = ResourceLoader.GetString2("Lpr");
		}

        private void textEditEditPlate_KeyPress(object sender, KeyPressEventArgs e)
        {
           if(e.KeyChar==13)
           {
               this.labelControlReadPlate.Text = this.textEditEditPlate.Text;
           }
        }
	}
}
