using SmartCadAlarm.Controls;
using SmartCadControls.Controls;


namespace SmartCadAlarm.Controls
{
    partial class AlarmInformationControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupControlBody = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.textBoxExDate = new TextBoxEx();
            this.textBoxExEvent = new TextBoxEx();
            this.textBoxExDatalogger = new TextBoxEx();
            this.textBoxExSensor = new TextBoxEx();
            this.pictureBoxNumber = new DevExpress.XtraEditors.PictureEdit();
            this.textBoxExTown = new TextBoxEx();
            this.textBoxExCity = new TextBoxEx();
            this.textBoxExStreet = new TextBoxEx();
            this.textBoxExStruct = new TextBoxEx();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemStreet = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItemCity = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemTown = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemSensor = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlDatalogger = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlEvent = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemStructName = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlBody)).BeginInit();
            this.groupControlBody.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemStreet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemSensor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlDatalogger)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlEvent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemStructName)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControlBody
            // 
            this.groupControlBody.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(252)))), ((int)(((byte)(226)))));
            this.groupControlBody.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(252)))), ((int)(((byte)(226)))));
            this.groupControlBody.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(94)))), ((int)(((byte)(50)))));
            this.groupControlBody.Appearance.Options.UseBackColor = true;
            this.groupControlBody.Appearance.Options.UseBorderColor = true;
            this.groupControlBody.AppearanceCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(115)))), ((int)(((byte)(172)))), ((int)(((byte)(68)))));
            this.groupControlBody.AppearanceCaption.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(94)))), ((int)(((byte)(50)))));
            this.groupControlBody.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControlBody.AppearanceCaption.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.groupControlBody.AppearanceCaption.Options.UseBackColor = true;
            this.groupControlBody.AppearanceCaption.Options.UseFont = true;
            this.groupControlBody.Controls.Add(this.layoutControl1);
            this.groupControlBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControlBody.Location = new System.Drawing.Point(0, 0);
            this.groupControlBody.LookAndFeel.SkinName = "Blue";
            this.groupControlBody.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.groupControlBody.LookAndFeel.UseDefaultLookAndFeel = false;
            this.groupControlBody.Name = "groupControlBody";
            this.groupControlBody.Size = new System.Drawing.Size(466, 155);
            this.groupControlBody.TabIndex = 41;
            this.groupControlBody.Text = "groupControlBody";
            // 
            // layoutControl1
            // 
            this.layoutControl1.AllowCustomizationMenu = false;
            this.layoutControl1.Appearance.DisabledLayoutGroupCaption.ForeColor = System.Drawing.SystemColors.GrayText;
            this.layoutControl1.Appearance.DisabledLayoutGroupCaption.Options.UseForeColor = true;
            this.layoutControl1.Appearance.DisabledLayoutItem.ForeColor = System.Drawing.SystemColors.GrayText;
            this.layoutControl1.Appearance.DisabledLayoutItem.Options.UseForeColor = true;
            this.layoutControl1.BackColor = System.Drawing.Color.White;
            this.layoutControl1.Controls.Add(this.textBoxExDate);
            this.layoutControl1.Controls.Add(this.textBoxExEvent);
            this.layoutControl1.Controls.Add(this.textBoxExDatalogger);
            this.layoutControl1.Controls.Add(this.textBoxExSensor);
            this.layoutControl1.Controls.Add(this.pictureBoxNumber);
            this.layoutControl1.Controls.Add(this.textBoxExTown);
            this.layoutControl1.Controls.Add(this.textBoxExCity);
            this.layoutControl1.Controls.Add(this.textBoxExStreet);
            this.layoutControl1.Controls.Add(this.textBoxExStruct);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 19);
            this.layoutControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.layoutControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(462, 134);
            this.layoutControl1.TabIndex = 49;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // textBoxExDate
            // 
            this.textBoxExDate.AllowsLetters = true;
            this.textBoxExDate.AllowsNumbers = true;
            this.textBoxExDate.AllowsPunctuation = true;
            this.textBoxExDate.AllowsSeparators = true;
            this.textBoxExDate.AllowsSymbols = true;
            this.textBoxExDate.AllowsWhiteSpaces = true;
            this.textBoxExDate.Enabled = false;
            this.textBoxExDate.ExtraAllowedChars = "";
            this.textBoxExDate.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExDate.Location = new System.Drawing.Point(205, 96);
            this.textBoxExDate.MaxLength = 40;
            this.textBoxExDate.Name = "textBoxExDate";
            this.textBoxExDate.NonAllowedCharacters = "";
            this.textBoxExDate.RegularExpresion = "";
            this.textBoxExDate.Size = new System.Drawing.Size(46, 20);
            this.textBoxExDate.TabIndex = 52;
            // 
            // textBoxExEvent
            // 
            this.textBoxExEvent.AllowsLetters = true;
            this.textBoxExEvent.AllowsNumbers = true;
            this.textBoxExEvent.AllowsPunctuation = true;
            this.textBoxExEvent.AllowsSeparators = true;
            this.textBoxExEvent.AllowsSymbols = true;
            this.textBoxExEvent.AllowsWhiteSpaces = true;
            this.textBoxExEvent.Enabled = false;
            this.textBoxExEvent.ExtraAllowedChars = "";
            this.textBoxExEvent.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExEvent.Location = new System.Drawing.Point(205, 66);
            this.textBoxExEvent.MaxLength = 40;
            this.textBoxExEvent.Name = "textBoxExEvent";
            this.textBoxExEvent.NonAllowedCharacters = "";
            this.textBoxExEvent.RegularExpresion = "";
            this.textBoxExEvent.Size = new System.Drawing.Size(46, 20);
            this.textBoxExEvent.TabIndex = 51;
            // 
            // textBoxExDatalogger
            // 
            this.textBoxExDatalogger.AllowsLetters = true;
            this.textBoxExDatalogger.AllowsNumbers = true;
            this.textBoxExDatalogger.AllowsPunctuation = true;
            this.textBoxExDatalogger.AllowsSeparators = true;
            this.textBoxExDatalogger.AllowsSymbols = true;
            this.textBoxExDatalogger.AllowsWhiteSpaces = true;
            this.textBoxExDatalogger.Enabled = false;
            this.textBoxExDatalogger.ExtraAllowedChars = "";
            this.textBoxExDatalogger.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExDatalogger.Location = new System.Drawing.Point(205, 6);
            this.textBoxExDatalogger.MaxLength = 40;
            this.textBoxExDatalogger.Name = "textBoxExDatalogger";
            this.textBoxExDatalogger.NonAllowedCharacters = "";
            this.textBoxExDatalogger.RegularExpresion = "";
            this.textBoxExDatalogger.Size = new System.Drawing.Size(46, 20);
            this.textBoxExDatalogger.TabIndex = 50;
            // 
            // textBoxExSensor
            // 
            this.textBoxExSensor.AllowsLetters = true;
            this.textBoxExSensor.AllowsNumbers = true;
            this.textBoxExSensor.AllowsPunctuation = true;
            this.textBoxExSensor.AllowsSeparators = true;
            this.textBoxExSensor.AllowsSymbols = true;
            this.textBoxExSensor.AllowsWhiteSpaces = true;
            this.textBoxExSensor.Enabled = false;
            this.textBoxExSensor.ExtraAllowedChars = "";
            this.textBoxExSensor.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExSensor.Location = new System.Drawing.Point(205, 36);
            this.textBoxExSensor.MaxLength = 40;
            this.textBoxExSensor.Name = "textBoxExSensor";
            this.textBoxExSensor.NonAllowedCharacters = "";
            this.textBoxExSensor.RegularExpresion = "";
            this.textBoxExSensor.Size = new System.Drawing.Size(46, 20);
            this.textBoxExSensor.TabIndex = 49;
            // 
            // pictureBoxNumber
            // 
            this.pictureBoxNumber.Location = new System.Drawing.Point(6, 6);
            this.pictureBoxNumber.Name = "pictureBoxNumber";
            this.pictureBoxNumber.Properties.ReadOnly = true;
            this.pictureBoxNumber.Properties.ShowMenu = false;
            this.pictureBoxNumber.Size = new System.Drawing.Size(41, 41);
            this.pictureBoxNumber.StyleController = this.layoutControl1;
            this.pictureBoxNumber.TabIndex = 0;
            // 
            // textBoxExTown
            // 
            this.textBoxExTown.AllowsLetters = true;
            this.textBoxExTown.AllowsNumbers = true;
            this.textBoxExTown.AllowsPunctuation = true;
            this.textBoxExTown.AllowsSeparators = true;
            this.textBoxExTown.AllowsSymbols = true;
            this.textBoxExTown.AllowsWhiteSpaces = true;
            this.textBoxExTown.Enabled = false;
            this.textBoxExTown.ExtraAllowedChars = "";
            this.textBoxExTown.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExTown.Location = new System.Drawing.Point(409, 6);
            this.textBoxExTown.MaxLength = 100;
            this.textBoxExTown.Name = "textBoxExTown";
            this.textBoxExTown.NonAllowedCharacters = "";
            this.textBoxExTown.RegularExpresion = "";
            this.textBoxExTown.Size = new System.Drawing.Size(47, 20);
            this.textBoxExTown.TabIndex = 46;
            // 
            // textBoxExCity
            // 
            this.textBoxExCity.AllowsLetters = true;
            this.textBoxExCity.AllowsNumbers = true;
            this.textBoxExCity.AllowsPunctuation = true;
            this.textBoxExCity.AllowsSeparators = true;
            this.textBoxExCity.AllowsSymbols = true;
            this.textBoxExCity.AllowsWhiteSpaces = true;
            this.textBoxExCity.Enabled = false;
            this.textBoxExCity.ExtraAllowedChars = "";
            this.textBoxExCity.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExCity.Location = new System.Drawing.Point(409, 36);
            this.textBoxExCity.MaxLength = 40;
            this.textBoxExCity.Name = "textBoxExCity";
            this.textBoxExCity.NonAllowedCharacters = "";
            this.textBoxExCity.RegularExpresion = "";
            this.textBoxExCity.Size = new System.Drawing.Size(47, 20);
            this.textBoxExCity.TabIndex = 45;
            // 
            // textBoxExStreet
            // 
            this.textBoxExStreet.AllowsLetters = true;
            this.textBoxExStreet.AllowsNumbers = true;
            this.textBoxExStreet.AllowsPunctuation = true;
            this.textBoxExStreet.AllowsSeparators = true;
            this.textBoxExStreet.AllowsSymbols = true;
            this.textBoxExStreet.AllowsWhiteSpaces = true;
            this.textBoxExStreet.Enabled = false;
            this.textBoxExStreet.ExtraAllowedChars = "";
            this.textBoxExStreet.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExStreet.Location = new System.Drawing.Point(409, 66);
            this.textBoxExStreet.MaxLength = 40;
            this.textBoxExStreet.Name = "textBoxExStreet";
            this.textBoxExStreet.NonAllowedCharacters = "";
            this.textBoxExStreet.RegularExpresion = "";
            this.textBoxExStreet.Size = new System.Drawing.Size(47, 20);
            this.textBoxExStreet.TabIndex = 48;
            // 
            // textBoxExStruct
            // 
            this.textBoxExStruct.AllowsLetters = true;
            this.textBoxExStruct.AllowsNumbers = true;
            this.textBoxExStruct.AllowsPunctuation = true;
            this.textBoxExStruct.AllowsSeparators = true;
            this.textBoxExStruct.AllowsSymbols = true;
            this.textBoxExStruct.AllowsWhiteSpaces = true;
            this.textBoxExStruct.Enabled = false;
            this.textBoxExStruct.ExtraAllowedChars = "";
            this.textBoxExStruct.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExStruct.Location = new System.Drawing.Point(409, 96);
            this.textBoxExStruct.MaxLength = 40;
            this.textBoxExStruct.Name = "textBoxExStruct";
            this.textBoxExStruct.NonAllowedCharacters = "";
            this.textBoxExStruct.RegularExpresion = "";
            this.textBoxExStruct.Size = new System.Drawing.Size(47, 20);
            this.textBoxExStruct.TabIndex = 47;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(252)))), ((int)(((byte)(226)))));
            this.layoutControlGroup1.AppearanceGroup.Options.UseBackColor = true;
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemStreet,
            this.layoutControlItem9,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItemTown,
            this.layoutControlDatalogger,
            this.layoutControlEvent,
            this.layoutControlItemDate,
            this.layoutControlItemStructName,
            this.layoutControlItemSensor,
            this.layoutControlItemCity});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(462, 134);
            this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItemStreet
            // 
            this.layoutControlItemStreet.Control = this.textBoxExStreet;
            this.layoutControlItemStreet.CustomizationFormText = "layoutControlItemStreet";
            this.layoutControlItemStreet.Location = new System.Drawing.Point(255, 60);
            this.layoutControlItemStreet.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItemStreet.MinSize = new System.Drawing.Size(100, 30);
            this.layoutControlItemStreet.Name = "layoutControlItemStreet";
            this.layoutControlItemStreet.Size = new System.Drawing.Size(205, 30);
            this.layoutControlItemStreet.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemStreet.Text = "layoutControlItemStreet";
            this.layoutControlItemStreet.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemStreet.TextSize = new System.Drawing.Size(143, 20);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.pictureBoxNumber;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(51, 51);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(51, 51);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(51, 51);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Text = "layoutControlItem9";
            this.layoutControlItem9.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextToControlDistance = 0;
            this.layoutControlItem9.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 51);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(51, 69);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AppearanceItemCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.emptySpaceItem2.AppearanceItemCaption.Options.UseBackColor = true;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 120);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(460, 12);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItemCity
            // 
            this.layoutControlItemCity.Control = this.textBoxExCity;
            this.layoutControlItemCity.CustomizationFormText = "layoutControlItemCity";
            this.layoutControlItemCity.Location = new System.Drawing.Point(255, 30);
            this.layoutControlItemCity.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItemCity.MinSize = new System.Drawing.Size(100, 30);
            this.layoutControlItemCity.Name = "layoutControlItemCity";
            this.layoutControlItemCity.Size = new System.Drawing.Size(205, 30);
            this.layoutControlItemCity.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemCity.Text = "layoutControlItemCity";
            this.layoutControlItemCity.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemCity.TextSize = new System.Drawing.Size(143, 20);
            // 
            // layoutControlItemTown
            // 
            this.layoutControlItemTown.Control = this.textBoxExTown;
            this.layoutControlItemTown.CustomizationFormText = "layoutControlItemTown";
            this.layoutControlItemTown.Location = new System.Drawing.Point(255, 0);
            this.layoutControlItemTown.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItemTown.MinSize = new System.Drawing.Size(100, 30);
            this.layoutControlItemTown.Name = "layoutControlItemTown";
            this.layoutControlItemTown.Size = new System.Drawing.Size(205, 30);
            this.layoutControlItemTown.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemTown.Text = "layoutControlItemTown";
            this.layoutControlItemTown.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemTown.TextSize = new System.Drawing.Size(143, 20);
            // 
            // layoutControlItemSensor
            // 
            this.layoutControlItemSensor.Control = this.textBoxExSensor;
            this.layoutControlItemSensor.CustomizationFormText = "layoutControlItemSensor";
            this.layoutControlItemSensor.Location = new System.Drawing.Point(51, 30);
            this.layoutControlItemSensor.Name = "layoutControlItemSensor";
            this.layoutControlItemSensor.Size = new System.Drawing.Size(204, 30);
            this.layoutControlItemSensor.Text = "layoutControlItemSensor";
            this.layoutControlItemSensor.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemSensor.TextSize = new System.Drawing.Size(143, 20);
            // 
            // layoutControlDatalogger
            // 
            this.layoutControlDatalogger.Control = this.textBoxExDatalogger;
            this.layoutControlDatalogger.CustomizationFormText = "layoutControlDatalogger";
            this.layoutControlDatalogger.Location = new System.Drawing.Point(51, 0);
            this.layoutControlDatalogger.Name = "layoutControlDatalogger";
            this.layoutControlDatalogger.Size = new System.Drawing.Size(204, 30);
            this.layoutControlDatalogger.Text = "layoutControlDatalogger";
            this.layoutControlDatalogger.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlDatalogger.TextSize = new System.Drawing.Size(143, 20);
            // 
            // layoutControlEvent
            // 
            this.layoutControlEvent.Control = this.textBoxExEvent;
            this.layoutControlEvent.CustomizationFormText = "layoutControlEvent";
            this.layoutControlEvent.Location = new System.Drawing.Point(51, 60);
            this.layoutControlEvent.Name = "layoutControlEvent";
            this.layoutControlEvent.Size = new System.Drawing.Size(204, 30);
            this.layoutControlEvent.Text = "layoutControlEvent";
            this.layoutControlEvent.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlEvent.TextSize = new System.Drawing.Size(143, 20);
            // 
            // layoutControlItemDate
            // 
            this.layoutControlItemDate.Control = this.textBoxExDate;
            this.layoutControlItemDate.CustomizationFormText = "layoutControlItemDate";
            this.layoutControlItemDate.Location = new System.Drawing.Point(51, 90);
            this.layoutControlItemDate.Name = "layoutControlItemDate";
            this.layoutControlItemDate.Size = new System.Drawing.Size(204, 30);
            this.layoutControlItemDate.Text = "layoutControlItemDate";
            this.layoutControlItemDate.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemDate.TextSize = new System.Drawing.Size(143, 20);
            // 
            // layoutControlItemStructName
            // 
            this.layoutControlItemStructName.Control = this.textBoxExStruct;
            this.layoutControlItemStructName.CustomizationFormText = "layoutControlItemStructName";
            this.layoutControlItemStructName.Location = new System.Drawing.Point(255, 90);
            this.layoutControlItemStructName.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItemStructName.MinSize = new System.Drawing.Size(105, 30);
            this.layoutControlItemStructName.Name = "layoutControlItemStructName";
            this.layoutControlItemStructName.Size = new System.Drawing.Size(205, 30);
            this.layoutControlItemStructName.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemStructName.Text = "layoutControlItemStructName";
            this.layoutControlItemStructName.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItemStructName.TextSize = new System.Drawing.Size(143, 20);
            // 
            // AlarmInformationControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupControlBody);
            this.MinimumSize = new System.Drawing.Size(466, 155);
            this.Name = "AlarmInformationControl";
            this.Size = new System.Drawing.Size(466, 155);
            ((System.ComponentModel.ISupportInitialize)(this.groupControlBody)).EndInit();
            this.groupControlBody.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemStreet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemTown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemSensor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlDatalogger)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlEvent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemStructName)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TextBoxEx textBoxExStruct;
        private TextBoxEx textBoxExStreet;
		private TextBoxEx textBoxExCity;
        private TextBoxEx textBoxExTown;
		private DevExpress.XtraEditors.GroupControl groupControlBody;
		private DevExpress.XtraEditors.PictureEdit pictureBoxNumber;
		private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemTown;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemCity;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemStructName;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemStreet;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private TextBoxEx textBoxExDate;
        private TextBoxEx textBoxExEvent;
        private TextBoxEx textBoxExDatalogger;
        private TextBoxEx textBoxExSensor;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemSensor;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlDatalogger;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlEvent;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDate;
		
    }
}
