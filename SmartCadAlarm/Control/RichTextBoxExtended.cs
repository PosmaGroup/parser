using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.Threading;
using System.Text;
using System.IO;
using SmartCadCore.Common;


namespace SmartCadAlarm.Controls
{
   	#region StampActions
	public enum StampActions
	{
		EditedBy = 1,
		DateTime = 2,
		Custom = 4
	}
	#endregion

    #region FontColor
    public enum FontColor
    {
        Black,
        Red,
        Green,
        Blue,
        Yellow,
        Gray,
        Maroon,
        Olive,
        Teal,
        Navy,
        Purple,
        White,
        Silver,
        Lime,
        Aqua
    }
    #endregion

	/// <summary>
	/// An extended RichTextBox that contains a toolbar.
	/// </summary>
	public class RichTextBoxExtended : System.Windows.Forms.UserControl
	{
		//Used for looping
		private RichTextBox rtbTemp = new RichTextBox();
        //Used for mantening

        #region Windows Generated

        private System.Windows.Forms.RichTextBox rtb1;
        private System.Windows.Forms.ImageList imgList1;
		private System.Windows.Forms.OpenFileDialog ofd1;
        private System.Windows.Forms.SaveFileDialog sfd1;
        private System.ComponentModel.IContainer components;
        private ToolStrip toolStripMain;
        private ToolStripButton toolStripButtonSave;
        private ToolStripButton toolStripButtonOpen;
        private ToolStripSeparator toolStripSeparator1;
        private ToolStripComboBox toolStripComboBoxFont;
        private ToolStripComboBox toolStripComboBoxFontSize;
        private ToolStripDropDownButton toolStripDropDownButtonColor;
        private ToolStripSeparator toolStripSeparator2;
        private ToolStripButton toolStripButtonBold;
        private ToolStripButton toolStripButtonItalic;
        private ToolStripButton toolStripButtonUnderline;
        private ToolStripButton toolStripButtonStrikeout;
        private ToolStripSeparator toolStripSeparator3;
        private ToolStripButton toolStripButtonUndo;
        private ToolStripButton toolStripButtonRedo;
        private ToolStripSeparator toolStripSeparator5;
        private ToolStripButton toolStripButtonCut;
        private ToolStripButton toolStripButtonCopy;
        private ToolStripButton toolStripButtonPaste;
        private ToolStripButton toolStripButtonCenter;
        private ToolStripButton toolStripButtonRight;
        private ToolStripSeparator toolStripSeparator4;
        private ToolStripButton toolStripButtonLeft;
        private ToolTip toolTipMain;
        private ToolStripButton toolStripButtonColor;
        private ToolStripButton toolStripButtonStamp;
        private MenuItem menuItemCut;
        private MenuItem menuItemCopy;
        private MenuItem menuItemPaste;
        private ContextMenu contextMenu;

		public RichTextBoxExtended()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();
            //Fill font list;
            FillFontList();
            //Fill font size list;
            FillFontSizeList();
            //Fill the color menu item
            FillColorList();
			//Update the graphics on the toolbar
			UpdateToolbar();

           menuItemCut = new MenuItem("Cortar", new EventHandler(contextMenu_cutItem_Click));
           menuItemCopy = new MenuItem("Copiar", new EventHandler(contextMenu_copyItem_Click));
            menuItemPaste = new MenuItem("Pegar", new EventHandler(contextMenu_pasteItem_Click));
           
           
            
		}

        private void FillFontList()
        {
            int maxlen = 0;

            bool found = false;
            foreach (FontFamily fontFamily in FontFamily.Families)
            {
                if (fontFamily.IsStyleAvailable(FontStyle.Regular) &&
                    fontFamily.IsStyleAvailable(FontStyle.Bold) &&
                    fontFamily.IsStyleAvailable(FontStyle.Italic) &&
                    fontFamily.IsStyleAvailable(FontStyle.Strikeout) &&
                    fontFamily.IsStyleAvailable(FontStyle.Underline))
                {
                    ToolStripMenuItem tsmi = new ToolStripMenuItem(fontFamily.Name);
                    toolStripComboBoxFont.Items.Add(tsmi);
                    maxlen = Math.Max(maxlen, tsmi.Text.Length);
                 
                    if (tsmi.ToString()  == "Microsoft Sans Serif")
                    {
                        toolStripComboBoxFont.SelectedItem = tsmi;
                        rtb1.Font = new Font(tsmi.ToString(), ((float)10.5), rtb1.Font.Style);
                        found = true; 
                    }
                }    
            }
            if (maxlen > 0)
                toolStripComboBoxFont.DropDownWidth = maxlen * 7;

            if (found == false && toolStripComboBoxFont.Items.Count > 0)
            {
                toolStripComboBoxFont.SelectedItem = toolStripComboBoxFont.Items[0];
                rtb1.Font = new Font(toolStripComboBoxFont.Items[0].ToString(), ((float)10.5), rtb1.Font.Style);
            }
        }

        private void FillFontSizeList()
        {
            for (int i = 8, j = 1; i <= 72; i = i + j)
            {
                ToolStripMenuItem tsmi = new ToolStripMenuItem(i.ToString());
                toolStripComboBoxFontSize.Items.Add(tsmi);
                if (i == 12)
                    j = 2;
                else if (i == 28)
                    j = 8;
                else if (i == 36)
                    j = 12;
                else if (i == 48)
                    j = 24;
            }
            toolStripComboBoxFontSize.SelectedIndex = 2;
        }

        private void FillColorList()
        {
            
            foreach (string color in Enum.GetNames(typeof(FontColor)))
            {
                ToolStripMenuItem tsmi = new ToolStripMenuItem(color);
                tsmi.Tag = Color.FromName(color);
                tsmi.Text = ResourceLoader.GetString2(color);
                Bitmap bitmap = new Bitmap(16, 16);
                SolidBrush brush = new SolidBrush(Color.FromName(color));
                Graphics.FromImage(bitmap).FillRectangle(brush, 0, 0, 16, 16);
                brush.Dispose();
                tsmi.Image = bitmap;

                tsmi.ImageScaling = ToolStripItemImageScaling.None;
                tsmi.Click += new EventHandler(toolStripDropDownButtonColor_Click);
                toolStripDropDownButtonColor.DropDownItems.Add(tsmi);
            }
            toolStripDropDownButtonColor_Click(toolStripDropDownButtonColor.DropDownItems[0], new EventArgs());
        }

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

        public RichTextBox InnerRichTextBox
        {
            get
            {
                return rtb1;
            }
        }
		#endregion

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RichTextBoxExtended));
            this.imgList1 = new System.Windows.Forms.ImageList(this.components);
            this.rtb1 = new System.Windows.Forms.RichTextBox();
            this.contextMenu = new System.Windows.Forms.ContextMenu();
            this.menuItemCut = new System.Windows.Forms.MenuItem();
            this.menuItemCopy = new System.Windows.Forms.MenuItem();
            this.menuItemPaste = new System.Windows.Forms.MenuItem();
            this.ofd1 = new System.Windows.Forms.OpenFileDialog();
            this.sfd1 = new System.Windows.Forms.SaveFileDialog();
            this.toolStripMain = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonSave = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonOpen = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripComboBoxFont = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripComboBoxFontSize = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripButtonColor = new System.Windows.Forms.ToolStripButton();
            this.toolStripDropDownButtonColor = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonBold = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonItalic = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonUnderline = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonStrikeout = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonLeft = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonCenter = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonRight = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonUndo = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonRedo = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonCut = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonCopy = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonPaste = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonStamp = new System.Windows.Forms.ToolStripButton();
            this.toolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.toolStripMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // imgList1
            // 
            this.imgList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imgList1.ImageStream")));
            this.imgList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imgList1.Images.SetKeyName(0, "");
            this.imgList1.Images.SetKeyName(1, "");
            this.imgList1.Images.SetKeyName(2, "");
            this.imgList1.Images.SetKeyName(3, "");
            this.imgList1.Images.SetKeyName(4, "");
            this.imgList1.Images.SetKeyName(5, "");
            this.imgList1.Images.SetKeyName(6, "");
            this.imgList1.Images.SetKeyName(7, "");
            this.imgList1.Images.SetKeyName(8, "");
            this.imgList1.Images.SetKeyName(9, "");
            this.imgList1.Images.SetKeyName(10, "");
            this.imgList1.Images.SetKeyName(11, "");
            this.imgList1.Images.SetKeyName(12, "");
            this.imgList1.Images.SetKeyName(13, "");
            this.imgList1.Images.SetKeyName(14, "");
            this.imgList1.Images.SetKeyName(15, "");
            this.imgList1.Images.SetKeyName(16, "");
            this.imgList1.Images.SetKeyName(17, "");
            this.imgList1.Images.SetKeyName(18, "");
            this.imgList1.Images.SetKeyName(19, "");
            this.imgList1.Images.SetKeyName(20, "");
            // 
            // rtb1
            // 
            this.rtb1.AutoWordSelection = true;
            this.rtb1.ContextMenu = this.contextMenu;
            this.rtb1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtb1.Location = new System.Drawing.Point(0, 25);
            this.rtb1.MaxLength = 8500;
            this.rtb1.Name = "rtb1";
            this.rtb1.Size = new System.Drawing.Size(616, 199);
            this.rtb1.TabIndex = 1;
            this.rtb1.Text = "";
            this.rtb1.LinkClicked += new System.Windows.Forms.LinkClickedEventHandler(this.rtb1_LinkClicked);
            this.rtb1.SelectionChanged += new System.EventHandler(this.rtb1_SelectionChanged);
            this.rtb1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rtb1_KeyDown);
            this.rtb1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.rtb1_KeyPress);
            // 
            // contextMenu
            // 
            this.contextMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItemCut,
            this.menuItemCopy,
            this.menuItemPaste});
            this.contextMenu.Popup += new System.EventHandler(this.contextMenu_Popup);
            // 
            // menuItemCut
            // 
            this.menuItemCut.Index = 0;
            this.menuItemCut.Text = "Cut";
            this.menuItemCut.Click += new System.EventHandler(this.contextMenu_cutItem_Click);
            // 
            // menuItemCopy
            // 
            this.menuItemCopy.Index = 1;
            this.menuItemCopy.Text = "Copy";
            this.menuItemCopy.Click += new System.EventHandler(this.contextMenu_copyItem_Click);
            // 
            // menuItemPaste
            // 
            this.menuItemPaste.Index = 2;
            this.menuItemPaste.Text = "Paste";
            this.menuItemPaste.Click += new System.EventHandler(this.contextMenu_pasteItem_Click);
            // 
            // ofd1
            // 
            this.ofd1.DefaultExt = "rtf";
            this.ofd1.Filter = "Rich Text Files|*.rtf|Plain Text File|*.txt";
            this.ofd1.Title = "Abrir archivo";
            // 
            // sfd1
            // 
            this.sfd1.DefaultExt = "rtf";
            this.sfd1.Filter = "Rich Text File|*.rtf|Plain Text File|*.txt";
            this.sfd1.Title = "Guardar como";
            // 
            // toolStripMain
            // 
            this.toolStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonSave,
            this.toolStripButtonOpen,
            this.toolStripSeparator1,
            this.toolStripComboBoxFont,
            this.toolStripComboBoxFontSize,
            this.toolStripButtonColor,
            this.toolStripDropDownButtonColor,
            this.toolStripSeparator2,
            this.toolStripButtonBold,
            this.toolStripButtonItalic,
            this.toolStripButtonUnderline,
            this.toolStripButtonStrikeout,
            this.toolStripSeparator3,
            this.toolStripButtonLeft,
            this.toolStripButtonCenter,
            this.toolStripButtonRight,
            this.toolStripSeparator4,
            this.toolStripButtonUndo,
            this.toolStripButtonRedo,
            this.toolStripSeparator5,
            this.toolStripButtonCut,
            this.toolStripButtonCopy,
            this.toolStripButtonPaste,
            this.toolStripButtonStamp});
            this.toolStripMain.Location = new System.Drawing.Point(0, 0);
            this.toolStripMain.Name = "toolStripMain";
            this.toolStripMain.Size = new System.Drawing.Size(616, 25);
            this.toolStripMain.TabIndex = 3;
            this.toolStripMain.TabStop = true;
            this.toolStripMain.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.toolStripMain_ItemClicked);
            // 
            // toolStripButtonSave
            // 
            this.toolStripButtonSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonSave.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonSave.Image")));
            this.toolStripButtonSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonSave.Name = "toolStripButtonSave";
            this.toolStripButtonSave.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonSave.ToolTipText = "Guardar";
            // 
            // toolStripButtonOpen
            // 
            this.toolStripButtonOpen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonOpen.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonOpen.Image")));
            this.toolStripButtonOpen.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonOpen.Name = "toolStripButtonOpen";
            this.toolStripButtonOpen.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonOpen.ToolTipText = "Open";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripComboBoxFont
            // 
            this.toolStripComboBoxFont.AutoToolTip = true;
            this.toolStripComboBoxFont.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toolStripComboBoxFont.Name = "toolStripComboBoxFont";
            this.toolStripComboBoxFont.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.toolStripComboBoxFont.Size = new System.Drawing.Size(121, 25);
            this.toolStripComboBoxFont.ToolTipText = "Change the font face";
            this.toolStripComboBoxFont.SelectedIndexChanged += new System.EventHandler(this.toolStripComboBoxFont_SelectedIndexChanged);
            this.toolStripComboBoxFont.Enter += new System.EventHandler(this.toolStripComboBoxFont_Enter);
            // 
            // toolStripComboBoxFontSize
            // 
            this.toolStripComboBoxFontSize.AutoSize = false;
            this.toolStripComboBoxFontSize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toolStripComboBoxFontSize.DropDownWidth = 60;
            this.toolStripComboBoxFontSize.MaxLength = 2;
            this.toolStripComboBoxFontSize.Name = "toolStripComboBoxFontSize";
            this.toolStripComboBoxFontSize.Size = new System.Drawing.Size(35, 23);
            this.toolStripComboBoxFontSize.ToolTipText = "Change the font size";
            this.toolStripComboBoxFontSize.SelectedIndexChanged += new System.EventHandler(this.toolStripComboBoxFontSize_SelectedIndexChanged);
            // 
            // toolStripButtonColor
            // 
            this.toolStripButtonColor.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonColor.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonColor.Image")));
            this.toolStripButtonColor.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonColor.Name = "toolStripButtonColor";
            this.toolStripButtonColor.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonColor.ToolTipText = "Change the text color";
            // 
            // toolStripDropDownButtonColor
            // 
            this.toolStripDropDownButtonColor.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripDropDownButtonColor.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toolStripDropDownButtonColor.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButtonColor.Margin = new System.Windows.Forms.Padding(1, 1, 0, 2);
            this.toolStripDropDownButtonColor.Name = "toolStripDropDownButtonColor";
            this.toolStripDropDownButtonColor.Size = new System.Drawing.Size(13, 22);
            this.toolStripDropDownButtonColor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toolStripDropDownButtonColor.ToolTipText = "Change the text color";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButtonBold
            // 
            this.toolStripButtonBold.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonBold.Image = global::SmartCadAlarm.Util.XRDesignRibbonControllerResources.RibbonUserDesigner_FontBold;
            this.toolStripButtonBold.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonBold.Name = "toolStripButtonBold";
            this.toolStripButtonBold.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonBold.ToolTipText = "Make the selected text bold";
            // 
            // toolStripButtonItalic
            // 
            this.toolStripButtonItalic.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonItalic.Image = global::SmartCadAlarm.Util.XRDesignRibbonControllerResources.RibbonUserDesigner_FontItalic;
            this.toolStripButtonItalic.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonItalic.Name = "toolStripButtonItalic";
            this.toolStripButtonItalic.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonItalic.ToolTipText = "Italicize the selected text";
            // 
            // toolStripButtonUnderline
            // 
            this.toolStripButtonUnderline.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonUnderline.Image = global::SmartCadAlarm.Util.XRDesignRibbonControllerResources.RibbonUserDesigner_FontUnderline;
            this.toolStripButtonUnderline.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonUnderline.Name = "toolStripButtonUnderline";
            this.toolStripButtonUnderline.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonUnderline.ToolTipText = "Underline the selected text";
            // 
            // toolStripButtonStrikeout
            // 
            this.toolStripButtonStrikeout.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonStrikeout.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonStrikeout.Image")));
            this.toolStripButtonStrikeout.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonStrikeout.Name = "toolStripButtonStrikeout";
            this.toolStripButtonStrikeout.Size = new System.Drawing.Size(23, 22);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButtonLeft
            // 
            this.toolStripButtonLeft.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonLeft.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonLeft.Image")));
            this.toolStripButtonLeft.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonLeft.Name = "toolStripButtonLeft";
            this.toolStripButtonLeft.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonLeft.ToolTipText = "Align text to the left";
            // 
            // toolStripButtonCenter
            // 
            this.toolStripButtonCenter.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonCenter.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonCenter.Image")));
            this.toolStripButtonCenter.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonCenter.Name = "toolStripButtonCenter";
            this.toolStripButtonCenter.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonCenter.Text = "toolStripButton1";
            this.toolStripButtonCenter.ToolTipText = "Center text";
            // 
            // toolStripButtonRight
            // 
            this.toolStripButtonRight.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonRight.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonRight.Image")));
            this.toolStripButtonRight.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonRight.Name = "toolStripButtonRight";
            this.toolStripButtonRight.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonRight.ToolTipText = "Align text to the right";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButtonUndo
            // 
            this.toolStripButtonUndo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonUndo.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonUndo.Image")));
            this.toolStripButtonUndo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonUndo.Name = "toolStripButtonUndo";
            this.toolStripButtonUndo.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonUndo.ToolTipText = "Undo";
            // 
            // toolStripButtonRedo
            // 
            this.toolStripButtonRedo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonRedo.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonRedo.Image")));
            this.toolStripButtonRedo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonRedo.Name = "toolStripButtonRedo";
            this.toolStripButtonRedo.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonRedo.ToolTipText = "Repeat";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButtonCut
            // 
            this.toolStripButtonCut.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonCut.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonCut.Image")));
            this.toolStripButtonCut.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonCut.Name = "toolStripButtonCut";
            this.toolStripButtonCut.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonCut.ToolTipText = "Cortar";
            // 
            // toolStripButtonCopy
            // 
            this.toolStripButtonCopy.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonCopy.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonCopy.Image")));
            this.toolStripButtonCopy.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonCopy.Name = "toolStripButtonCopy";
            this.toolStripButtonCopy.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonCopy.ToolTipText = "Copiar";
            // 
            // toolStripButtonPaste
            // 
            this.toolStripButtonPaste.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonPaste.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonPaste.Image")));
            this.toolStripButtonPaste.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonPaste.Name = "toolStripButtonPaste";
            this.toolStripButtonPaste.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonPaste.ToolTipText = "Pegar";
            // 
            // toolStripButtonStamp
            // 
            this.toolStripButtonStamp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonStamp.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonStamp.Image")));
            this.toolStripButtonStamp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonStamp.Name = "toolStripButtonStamp";
            this.toolStripButtonStamp.Size = new System.Drawing.Size(23, 22);
            // 
            // RichTextBoxExtended
            // 
            this.Controls.Add(this.rtb1);
            this.Controls.Add(this.toolStripMain);
            this.Name = "RichTextBoxExtended";
            this.Size = new System.Drawing.Size(616, 224);
            this.toolStripMain.ResumeLayout(false);
            this.toolStripMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		#region Selection Change event	
		[Description("Occurs when the selection is changed"),
		Category("Behavior")]
		// Raised in tb1 SelectionChanged event so that user can do useful things
		public event System.EventHandler SelChanged;
		#endregion

		#region Stamp Event Stuff
		[Description("Occurs when the stamp button is clicked"), 
		 Category("Behavior")]
		public event System.EventHandler Stamp;
        
		/// <summary>
		/// OnStamp event
		/// </summary>
		protected virtual void OnStamp(EventArgs e)
		{
			if(Stamp != null)
				Stamp(this, e);

			switch(StampAction)
			{
				case StampActions.EditedBy:
				{
					StringBuilder stamp = new StringBuilder(""); //holds our stamp text
					if(rtb1.Text.Length > 0) stamp.Append("\r\n\r\n"); //add two lines for space
					stamp.Append("Edited by "); 
					//use the CurrentPrincipal name if one exsist else use windows logon username
					if(Thread.CurrentPrincipal == null || Thread.CurrentPrincipal.Identity == null || Thread.CurrentPrincipal.Identity.Name.Length <= 0)
						stamp.Append(Environment.UserName);
					else
						stamp.Append(Thread.CurrentPrincipal.Identity.Name);
					stamp.Append(" on " + DateTime.Now.ToLongDateString() + "\r\n");
			
					rtb1.SelectionLength = 0; //unselect everything basicly
					rtb1.SelectionStart = rtb1.Text.Length; //start new selection at the end of the text
					rtb1.SelectionColor = this.StampColor; //make the selection blue
					rtb1.SelectionFont = new Font(rtb1.SelectionFont, FontStyle.Bold); //set the selection font and style
					rtb1.AppendText(stamp.ToString()); //add the stamp to the richtextbox
					rtb1.Focus(); //set focus back on the richtextbox
				} break; //end edited by stamp
				case StampActions.DateTime:
				{
					StringBuilder stamp = new StringBuilder(""); //holds our stamp text
					if(rtb1.Text.Length > 0) stamp.Append("\r\n\r\n"); //add two lines for space
					stamp.Append(DateTime.Now.ToLongDateString() + "\r\n");
					rtb1.SelectionLength = 0; //unselect everything basicly
					rtb1.SelectionStart = rtb1.Text.Length; //start new selection at the end of the text
					rtb1.SelectionColor = this.StampColor; //make the selection blue
					rtb1.SelectionFont = new Font(rtb1.SelectionFont, FontStyle.Bold); //set the selection font and style
					rtb1.AppendText(stamp.ToString()); //add the stamp to the richtextbox
					rtb1.Focus(); //set focus back on the richtextbox
				} break;
			} //end select
		}
		#endregion

		#region Update Toolbar
		/// <summary>
		///     Update the toolbar button statuses
		/// </summary>
		public void UpdateToolbar()
		{
            // Get the font, fontsize and style to apply to the toolbar buttons
			Font fnt = GetFontDetails();
			// Set font style buttons to the styles applying to the entire selection
			FontStyle style = fnt.Style;
			
			//Set all the style buttons using the gathered style
			toolStripButtonBold.Checked = fnt.Bold; //bold button
            toolStripButtonItalic.Checked = fnt.Italic; //italic button
            toolStripButtonUnderline.Checked = fnt.Underline; //underline button
            toolStripButtonStrikeout.Checked = fnt.Strikeout; //strikeout button
            toolStripButtonLeft.Checked = (rtb1.SelectionAlignment == HorizontalAlignment.Left); //justify left
            toolStripButtonCenter.Checked = (rtb1.SelectionAlignment == HorizontalAlignment.Center); //justify center
            toolStripButtonRight.Checked = (rtb1.SelectionAlignment == HorizontalAlignment.Right); //justify right
	
			//Check the correct color
            foreach (ToolStripMenuItem tsmi in toolStripDropDownButtonColor.DropDownItems)
            {
                tsmi.Checked = rtb1.SelectionColor.Equals(tsmi.Tag);
                if (tsmi.Checked == true)
                {
                    toolStripButtonColor.Image = tsmi.Image;
                    toolStripButtonColor.Tag = tsmi.Tag;
                }
            }
            //Check the correct font
            toolStripComboBoxFont.SelectedIndexChanged -= new EventHandler(toolStripComboBoxFont_SelectedIndexChanged);
            foreach (ToolStripMenuItem tsmi in toolStripComboBoxFont.Items)
                tsmi.Checked = (fnt.Name == tsmi.Text);
            toolStripComboBoxFont.Text = fnt.FontFamily.Name;
            toolStripComboBoxFont.SelectedIndexChanged += new EventHandler(toolStripComboBoxFont_SelectedIndexChanged);
            
            //Check the correct font size
            toolStripComboBoxFontSize.SelectedIndexChanged -= new EventHandler(toolStripComboBoxFontSize_SelectedIndexChanged);
            foreach (ToolStripMenuItem tsmi in toolStripComboBoxFontSize.Items)
                tsmi.Checked = ((int)fnt.SizeInPoints == float.Parse(tsmi.Text));
            toolStripComboBoxFontSize.Text = ((int)fnt.SizeInPoints).ToString();
            toolStripComboBoxFontSize.SelectedIndexChanged += new EventHandler(toolStripComboBoxFontSize_SelectedIndexChanged);

            if (rtb1.CanUndo)
            {
                if (rtb1.TextLength > 0)
                {
                    toolStripButtonUndo.Enabled = true;
                }
            }
            else
            {
                toolStripButtonUndo.Enabled = false; 
            }

            if (rtb1.CanRedo)
            {
                if (rtb1.TextLength > 0)
                {
                    toolStripButtonRedo.Enabled = true;
                }
            }
            else
            {
                toolStripButtonRedo.Enabled = false;
            }
		}
		#endregion

		#region Update Toolbar Seperators
		private void UpdateToolbarSeperators()
		{
			//Save & Open
            if (!toolStripButtonSave.Visible && !toolStripButtonOpen.Visible)
                toolStripSeparator1.Visible = false;
			else
                toolStripSeparator1.Visible = true;

			//Font & Font Size
            if (!toolStripComboBoxFont.Visible && !toolStripComboBoxFontSize.Visible && !toolStripDropDownButtonColor.Visible)
                toolStripSeparator2.Visible = false;
			else
                toolStripSeparator2.Visible = true;

			//Bold, Italic, Underline, & Strikeout
            if (!toolStripButtonBold.Visible && !toolStripButtonItalic.Visible && !toolStripButtonUnderline.Visible && !toolStripButtonStrikeout.Visible)
                toolStripSeparator3.Visible = false;
			else
                toolStripSeparator3.Visible = true;

			//Left, Center, & Right
            if (!toolStripButtonLeft.Visible && !toolStripButtonCenter.Visible && !toolStripButtonRight.Visible)
                toolStripSeparator4.Visible = false;
			else
                toolStripSeparator4.Visible = true;

			//Undo & Redo
            if (!toolStripButtonUndo.Visible && !toolStripButtonRedo.Visible)
                toolStripSeparator5.Visible = false;
			else
                toolStripSeparator5.Visible = true;
		}
#endregion

		#region RichTextBox Selection Change
		/// <summary>
		///		Change the toolbar buttons when new text is selected
		///		and raise event SelChanged
		/// </summary>
		private void rtb1_SelectionChanged(object sender, System.EventArgs e)
		{
            //Save state of toolStripButtonUnderline
            ToolStripButton toolStripButton = this.toolStripButtonUnderline; 
            bool isUnderline = toolStripButton.Checked;
            
            //Update the toolbar buttons
            UpdateToolbar();

            if (((RichTextBox)(sender)).SelectedText == "")
            {
                ChangeFontStyle(FontStyle.Underline, isUnderline);
            }
           
			//Send the SelChangedEvent
			if (SelChanged != null)
				SelChanged(this, e);
		}
#endregion

		#region Color Click
		/// <summary>
		///     Change the richtextbox color
		/// </summary>
        void toolStripDropDownButtonColor_Click(object sender, EventArgs e)
        {
            //set the richtextbox color based on the name of the menu item
            ToolStripMenuItem toolStripMenuItem = sender as ToolStripMenuItem;
            toolStripButtonColor.Image = toolStripMenuItem.Image;
            toolStripButtonColor.Tag = toolStripMenuItem.Tag;
            ChangeFontColor((Color)(toolStripMenuItem.Tag));
            rtb1.Focus();
        }
		#endregion

		#region Font Click
		/// <summary>
		///     Change the richtextbox font
		/// </summary>
        private void toolStripComboBoxFont_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Set the font for the entire selection
            ChangeFont(((ToolStripComboBox)sender).Text);
            rtb1.Focus();
        }


        private void toolStripComboBoxFont_Enter(object sender, EventArgs e) {
            toolStripComboBoxFont.ToolTipText = null;
        
        }
		#endregion

		#region Font Size Click
		/// <summary>
		///     Change the richtextbox font size
		/// </summary>
        private void toolStripComboBoxFontSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            //set the richtextbox font size based on the name of the menu item
            ChangeFontSize(float.Parse(((ToolStripComboBox)sender).Text));
            rtb1.Focus();
        }
		#endregion

		#region Link Clicked
		/// <summary>
		/// Starts the default browser if a link is clicked
		/// </summary>
		private void rtb1_LinkClicked(object sender, System.Windows.Forms.LinkClickedEventArgs e)
		{
			System.Diagnostics.Process.Start(e.LinkText);
		}
		#endregion

		#region Public Properties
		/// <summary>
		///     The toolbar that is contained with-in the RichTextBoxExtened control
		/// </summary>
		[Description("The internal toolbar control"),
		Category("Internal Controls"),
		DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public ToolStrip Toolbar
		{
			get { return toolStripMain; }
		}

		/// <summary>
		///     The RichTextBox that is contained with-in the RichTextBoxExtened control
		/// </summary>
		[Description("The internal richtextbox control"),
		Category("Internal Controls"),
		DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public RichTextBox RichTextBox
		{
			get	{ return rtb1; }
		}

		/// <summary>
		///     Show the save button or not
		/// </summary>
		[Description("Show the save button or not"),
		Category("Appearance")]
		public bool ShowSave
		{
            get { return toolStripButtonSave.Visible; }
            set { toolStripButtonSave.Visible = value; UpdateToolbarSeperators(); }
		}

		/// <summary>
		///    Show the open button or not 
		/// </summary>
		[Description("Show the open button or not"),
		Category("Appearance")]
		public bool ShowOpen
		{
            get { return toolStripButtonOpen.Visible; }
            set { toolStripButtonOpen.Visible = value; UpdateToolbarSeperators(); }
		}

		/// <summary>
		///     Show the stamp button or not
		/// </summary>
		[Description("Show the stamp button or not"),
		 Category("Appearance")]
		public bool ShowStamp
		{
            get { return toolStripButtonStamp.Visible; }
            set { toolStripButtonStamp.Visible = value; }
		}
		
		/// <summary>
		///     Show the color button or not
		/// </summary>
		[Description("Show the color button or not"),
		Category("Appearance")]
		public bool ShowColors
		{
            get { return toolStripDropDownButtonColor.Visible; }
            set { toolStripDropDownButtonColor.Visible = value; }
		}

		/// <summary>
		///     Show the undo button or not
		/// </summary>
		[Description("Show the undo button or not"),
		Category("Appearance")]
		public bool ShowUndo
		{
            get { return toolStripButtonUndo.Visible; }
            set { toolStripButtonUndo.Visible = value; UpdateToolbarSeperators(); }
		}

		/// <summary>
		///     Show the redo button or not
		/// </summary>
		[Description("Show the redo button or not"),
		Category("Appearance")]
		public bool ShowRedo
		{
            get { return toolStripButtonRedo.Visible; }
            set { toolStripButtonRedo.Visible = value; UpdateToolbarSeperators(); }
		}

		/// <summary>
		///     Show the bold button or not
		/// </summary>
		[Description("Show the bold button or not"),
		Category("Appearance")]
		public bool ShowBold
		{
            get { return toolStripButtonBold.Visible; }
            set { toolStripButtonBold.Visible = value; UpdateToolbarSeperators(); }
		}

		/// <summary>
		///     Show the italic button or not
		/// </summary>
		[Description("Show the italic button or not"),
		Category("Appearance")]
		public bool ShowItalic
		{
            get { return toolStripButtonItalic.Visible; }
            set { toolStripButtonItalic.Visible = value; UpdateToolbarSeperators(); }
		}

		/// <summary>
		///     Show the underline button or not
		/// </summary>
		[Description("Show the underline button or not"),
		Category("Appearance")]
		public bool ShowUnderline
		{
            get { return toolStripButtonUnderline.Visible; }
            set { toolStripButtonUnderline.Visible = value; UpdateToolbarSeperators(); }
		}

		/// <summary>
		///     Show the strikeout button or not
		/// </summary>
		[Description("Show the strikeout button or not"),
		Category("Appearance")]
		public bool ShowStrikeout
		{
            get { return toolStripButtonStrikeout.Visible; }
            set { toolStripButtonStrikeout.Visible = value; UpdateToolbarSeperators(); }
		}

		/// <summary>
		///     Show the left justify button or not
		/// </summary>
		[Description("Show the left justify button or not"),
		Category("Appearance")]
		public bool ShowLeftJustify
		{
            get { return toolStripButtonLeft.Visible; }
            set { toolStripButtonLeft.Visible = value; UpdateToolbarSeperators(); }
		}

		/// <summary>
		///     Show the right justify button or not
		/// </summary>
		[Description("Show the right justify button or not"),
		Category("Appearance")]
		public bool ShowRightJustify
		{
            get { return toolStripButtonRight.Visible; }
            set { toolStripButtonRight.Visible = value; UpdateToolbarSeperators(); }
		}

		/// <summary>
		///     Show the center justify button or not
		/// </summary>
		[Description("Show the center justify button or not"),
		Category("Appearance")]
		public bool ShowCenterJustify
		{
            get { return toolStripButtonCenter.Visible; }
            set { toolStripButtonCenter.Visible = value; UpdateToolbarSeperators(); }
		}

		/// <summary>
		///     Determines how the stamp button will respond
		/// </summary>
		StampActions m_StampAction = StampActions.EditedBy;
		[Description("Determines how the stamp button will respond"),
		Category("Behavior")]
		public StampActions StampAction
		{
			get { return m_StampAction; }
			set { m_StampAction = value; }
		}
		
		/// <summary>
		///     Color of the stamp text
		/// </summary>
		Color m_StampColor = Color.Blue;

		[Description("Color of the stamp text"),
		Category("Appearance")]
		public Color StampColor
		{
			get { return m_StampColor; }
			set { m_StampColor = value; }
		}
			
		/// <summary>
		///     Show the font button or not
		/// </summary>
		[Description("Show the font button or not"),
		Category("Appearance")]
		public bool ShowFont
		{
            get { return toolStripComboBoxFont.Visible; }
            set { toolStripComboBoxFont.Visible = value; }
		}

		/// <summary>
		///     Show the font size button or not
		/// </summary>
		[Description("Show the font size button or not"),
		Category("Appearance")]
		public bool ShowFontSize
		{
            get { return toolStripComboBoxFontSize.Visible; }
            set { toolStripComboBoxFontSize.Visible = value; }
		}

		/// <summary>
		///     Show the cut button or not
		/// </summary>
		[Description("Show the cut button or not"),
		Category("Appearance")]
		public bool ShowCut
		{
            get { return toolStripButtonCut.Visible; }
            set { toolStripButtonCut.Visible = value; }
		}

		/// <summary>
		///     Show the copy button or not
		/// </summary>
		[Description("Show the copy button or not"),
		Category("Appearance")]
		public bool ShowCopy
		{
            get { return toolStripButtonCopy.Visible; }
            set { toolStripButtonCopy.Visible = value; }
		}

		/// <summary>
		///     Show the paste button or not
		/// </summary>
		[Description("Show the paste button or not"),
		Category("Appearance")]
		public bool ShowPaste
		{
            get { return toolStripButtonPaste.Visible; }
            set { toolStripButtonPaste.Visible = value; }
		}

		/// <summary>
		///     Detect URLs with-in the richtextbox
		/// </summary>
		[Description("Detect URLs with-in the richtextbox"),
		Category("Behavior")]
		public bool DetectURLs
		{
			get { return rtb1.DetectUrls; }
			set { rtb1.DetectUrls = value; }
		}

		/// <summary>
		/// Determines if the TAB key moves to the next control or enters a TAB character in the richtextbox
		/// </summary>
		[Description("Determines if the TAB key moves to the next control or enters a TAB character in the richtextbox"),
		Category("Behavior")]
		public bool AcceptsTab
		{
			get { return rtb1.AcceptsTab; }
			set { rtb1.AcceptsTab = value; }
		}

		/// <summary>
		/// Determines if auto word selection is enabled
		/// </summary>
		[Description("Determines if auto word selection is enabled"),
		Category("Behavior")]
		public bool AutoWordSelection
		{
			get { return rtb1.AutoWordSelection; }
			set { rtb1.AutoWordSelection = value; }
		}

		/// <summary>
		/// Determines if this control can be edited
		/// </summary>
		[Description("Determines if this control can be edited"),
		Category("Behavior")]
		public bool ReadOnly
		{
			get { return rtb1.ReadOnly; }
			set 
			{
				toolStripMain.Visible = !value;
				rtb1.ReadOnly = value;
			}
		}

		private bool _showToolBarText;

		/// <summary>
		/// Determines if the buttons on the toolbar will show there text or not
		/// </summary>
		[Description("Determines if the buttons on the toolbar will show there text or not"),
		Category("Behavior")]
		public bool ShowToolBarText
		{
			get	{ return _showToolBarText; }
			set 
			{
				_showToolBarText = value;

				if(_showToolBarText)
				{
                    toolStripButtonSave.Text = "Save";
                    toolStripButtonOpen.Text = "Open";
                    toolStripButtonBold.Text = "Bold";
                    toolStripComboBoxFont.Text = "Font";
                    toolStripComboBoxFontSize.Text = "Font Size";
                    toolStripDropDownButtonColor.Text = "Font Color";
                    toolStripButtonItalic.Text = "Italic";
                    toolStripButtonStrikeout.Text = "Strikeout";
                    toolStripButtonUnderline.Text = "Underline";
                    toolStripButtonLeft.Text = "Left";
                    toolStripButtonCenter.Text = "Center";
                    toolStripButtonRight.Text = "Right";
                    toolStripButtonUndo.Text = "Undo";
                    toolStripButtonRedo.Text = "Redo";
                    toolStripButtonCut.Text = "Cut";
                    toolStripButtonCopy.Text = "Copy";
                    toolStripButtonPaste.Text = "Paste";
                    toolStripButtonStamp.Text = "Stamp";
				}
				else
				{
                    toolStripButtonSave.Text = "";
                    toolStripButtonOpen.Text = "";
                    toolStripButtonBold.Text = "";
                    toolStripComboBoxFont.Text = "";
                    toolStripComboBoxFontSize.Text = "";
                    toolStripDropDownButtonColor.Text = "";
                    toolStripButtonItalic.Text = "";
                    toolStripButtonStrikeout.Text = "";
                    toolStripButtonUnderline.Text = "";
                    toolStripButtonLeft.Text = "";
                    toolStripButtonCenter.Text = "";
                    toolStripButtonRight.Text = "";
                    toolStripButtonUndo.Text = "";
                    toolStripButtonRedo.Text = "";
                    toolStripButtonCut.Text = "";
                    toolStripButtonCopy.Text = "";
                    toolStripButtonPaste.Text = "";
                    toolStripButtonStamp.Text = "";
				}

				this.Invalidate();
				this.Update();
			}
		}

        /// <summary>
        ///     Show the save button or not
        /// </summary>
        [Description("Gets or Sets the maximum number of characters the user can type or paste in the control."),
        Category("Appearance")]
        public int MaxLength
        {
            get { return rtb1.MaxLength; }
            set { rtb1.MaxLength = value; }
        }
		#endregion

		#region Change font
		/// <summary>
		///     Change the richtextbox font for the current selection
		/// </summary>
		public void ChangeFont(string fontFamily)
		{
			//This method should handle cases that occur when multiple fonts/styles are selected
			// Parameters:-
			// fontFamily - the font to be applied, eg "Courier New"

			// Reason: The reason this method and the others exist is because
			// setting these items via the selection font doen't work because
			// a null selection font is returned for a selection with more 
			// than one font!
			
			int rtb1start = rtb1.SelectionStart;				
			int len = rtb1.SelectionLength; 
			int rtbTempStart = 0;						

			// If len <= 1 and there is a selection font, amend and return
			if (len <= 1 && rtb1.SelectionFont != null)
			{
				rtb1.SelectionFont =
					new Font(fontFamily, rtb1.SelectionFont.Size, rtb1.SelectionFont.Style);
				return;
			}

			// Step through the selected text one char at a time
			rtbTemp.Rtf = rtb1.SelectedRtf;
			for(int i = 0; i < len; ++i) 
			{ 
				rtbTemp.Select(rtbTempStart + i, 1); 
				rtbTemp.SelectionFont = new Font(fontFamily, rtbTemp.SelectionFont.Size, rtbTemp.SelectionFont.Style);
			}

			// Replace & reselect
			rtbTemp.Select(rtbTempStart,len);
			rtb1.SelectedRtf = rtbTemp.SelectedRtf;
            rtb1.Select(rtb1start, len);
            rtb1.Focus();
			return;
		}
		#endregion

		#region Change font style
		/// <summary>
		///     Change the richtextbox style for the current selection
		/// </summary>
		public void ChangeFontStyle(FontStyle style, bool add)
		{
            try
            {
                this.rtb1.SelectionChanged -= new System.EventHandler(this.rtb1_SelectionChanged);

                //This method should handle cases that occur when multiple fonts/styles are selected
                // Parameters:-
                //	style - eg FontStyle.Bold
                //	add - IF true then add else remove

                // throw error if style isn't: bold, italic, strikeout or underline
                if (style != FontStyle.Bold
                    && style != FontStyle.Italic
                    && style != FontStyle.Strikeout
                    && style != FontStyle.Underline)
                    throw new System.InvalidProgramException("Invalid style parameter to ChangeFontStyle");

                int rtb1start = rtb1.SelectionStart;
                int len = rtb1.SelectionLength;

                if (style == FontStyle.Underline)
                    add = UnderlineFont(style, rtb1.SelectionFont.Underline, add);

                if (add == true)
                    rtb1.SelectionFont = new Font(rtb1.SelectionFont, rtb1.SelectionFont.Style | style);
                else
                    rtb1.SelectionFont = new Font(rtb1.SelectionFont, rtb1.SelectionFont.Style & ~style);

                rtb1.Select(rtb1start, len);

                this.rtb1.SelectionChanged += new System.EventHandler(this.rtb1_SelectionChanged);
            }
            catch { }
		}

        public bool UnderlineFont(FontStyle style, bool underline, bool add)
        {
            try
            {
                int i = 1;
                int rtb1start = rtb1.SelectionStart;
                int len = rtb1.SelectionLength;

                if (rtb1.Text.Length > 0)
                {
                    if (underline == false && add == true)
                    {
                        if (len == 0)
                        {
                            rtbTemp.Rtf = rtb1.Rtf;
                            rtbTemp.Select(rtb1start - i, 1);

                            if (isBlank(rtbTemp.SelectedText) == false)
                            {
                                rtbTemp.Select(rtb1start - ++i, 1);
                                if (isBlank(rtbTemp.SelectedText) == true)
                                {
                                    try
                                    {
                                        while (isBlank(rtbTemp.SelectedText, false) == true)
                                        {
                                            rtbTemp.Rtf = rtb1.Rtf;
                                            rtbTemp.Select(rtb1start - ++i, 1);
                                        }
                                    }
                                    catch { }
                                    rtb1.Select(rtb1start - i , i);
                                }
                                else
                                {
                                    rtb1.Select(rtb1start -1, 1);
                                }
                                toolStripButtonUnderline.Checked = true; //underline button
                            }
                            else
                            {
                                if (rtbTemp.SelectedText == " " && rtbTemp.SelectionFont.Underline == false)
                                {
                                    add = false;
                                }
                                toolStripButtonUnderline.Checked = true; //underline button
                            }
                        }
                        else
                        {
                            rtbTemp.Rtf = rtb1.Rtf;
                            rtbTemp.Select(rtb1start + len - i, 1);

                            if (isBlank(rtbTemp.SelectedText) == true)
                            {
                                try
                                {
                                    while (isBlank(rtbTemp.SelectedText, false) == true && i <= len)
                                    {
                                        rtbTemp.Rtf = rtb1.Rtf;
                                        rtbTemp.Select(rtb1start + len - ++i, 1);
                                    }
                                }
                                catch { }
                                rtb1.Select(rtb1start - i, len + i);
                            }
                        }
                    }
                    else if (underline == true && add == true)
                    {
                        rtbTemp.Rtf = rtb1.Rtf;
                        rtbTemp.Select(rtb1start + len + i, 1);

                        if (rtbTemp.SelectedText == "" || rtbTemp.SelectedText == "\n")
                        {
                            rtbTemp.Select(rtb1start + len, 1);
                            if (rtbTemp.SelectedText == "" || rtbTemp.SelectedText == "\n") 
                            {
                                rtbTemp.Select(rtb1start + len -1, 1);   
                                if (isBlank(rtbTemp.SelectedText) == true)
                                {
                                    try
                                    {
                                        while (isBlank(rtbTemp.SelectedText, false) == true)
                                        {
                                            rtbTemp.Rtf = rtb1.Rtf;
                                            rtbTemp.Select(rtb1start + len - ++i, 1);
                                        }
                                    }
                                    catch { }
                                    rtb1.Select(rtb1start - i + 1, i + 1);
                                    add = false;
                                    toolStripButtonUnderline.Checked = true; //underline button
                                }
                                else
                                {
                                    rtbTemp.Select(rtb1start + len - ++i, 1);
                                    if (isBlank(rtbTemp.SelectedText) == true)
                                    {
                                        try
                                        {
                                            while (isBlank(rtbTemp.SelectedText, false) == true)
                                            {
                                                rtbTemp.Rtf = rtb1.Rtf;
                                                rtbTemp.Select(rtb1start + len - ++i, 1);
                                            }
                                        }
                                        catch { }
                                        rtb1.Select(rtb1start - i + 1, i);
                                    }
                                }
                            }
                        }
                        else if (rtbTemp.SelectedText == " " && rtbTemp.SelectionFont.Underline == false)
                        {
                            add = false;
                        }
                    }
                }
            }catch{}
            return add;
        }
		#endregion

		#region Change font size
		/// <summary>
		///     Change the richtextbox font size for the current selection
		/// </summary>
		public void ChangeFontSize(float fontSize)
		{
			//This method should handle cases that occur when multiple fonts/styles are selected
			// Parameters:-
			// fontSize - the fontsize to be applied, eg 33.5
			
			if (fontSize <= 0.0)
				throw new System.InvalidProgramException("Invalid font size parameter to ChangeFontSize");
			
			int rtb1start = rtb1.SelectionStart;				
			int len = rtb1.SelectionLength; 
			int rtbTempStart = 0;

			// If len <= 1 and there is a selection font, amend and return
			if (len <= 1 && rtb1.SelectionFont != null)
			{
				rtb1.SelectionFont =
					new Font(rtb1.SelectionFont.FontFamily, fontSize, rtb1.SelectionFont.Style);
				return;
			}
			
			// Step through the selected text one char at a time
			rtbTemp.Rtf = rtb1.SelectedRtf;
			for(int i = 0; i < len; ++i) 
			{ 
				rtbTemp.Select(rtbTempStart + i, 1); 
				rtbTemp.SelectionFont = new Font(rtbTemp.SelectionFont.FontFamily, fontSize, rtbTemp.SelectionFont.Style);
			}

			// Replace & reselect
			rtbTemp.Select(rtbTempStart,len);
			rtb1.SelectedRtf = rtbTemp.SelectedRtf;
			rtb1.Select(rtb1start,len);
            rtb1.Focus();
			return;
		}
		#endregion

		#region Change font color
		/// <summary>
		///     Change the richtextbox font color for the current selection
		/// </summary>
		public void ChangeFontColor(Color newColor)
		{
			//This method should handle cases that occur when multiple fonts/styles are selected
			// Parameters:-
			//	newColor - eg Color.Red
			
			int rtb1start = rtb1.SelectionStart;				
			int len = rtb1.SelectionLength; 
			int rtbTempStart = 0;			
			
			//if len <= 1 and there is a selection font then just handle and return
			if(len <= 1 && rtb1.SelectionFont != null)
			{
				rtb1.SelectionColor = newColor;
				return;
			}
			
			// Step through the selected text one char at a time	
			rtbTemp.Rtf = rtb1.SelectedRtf;
			for(int i = 0; i < len; ++i) 
			{ 
				rtbTemp.Select(rtbTempStart + i, 1); 

				//change color
				rtbTemp.SelectionColor = newColor;
			}

			// Replace & reselect
			rtbTemp.Select(rtbTempStart,len);
			rtb1.SelectedRtf = rtbTemp.SelectedRtf;
			rtb1.Select(rtb1start,len);
            rtb1.Focus();
			return;
		}
		#endregion

		#region Get Font Details
		/// <summary>
		///     Returns a Font with:
		///     1) The font applying to the entire selection, if none is the default font. 
		///     2) The font size applying to the entire selection, if none is the size of the default font.
		///     3) A style containing the attributes that are common to the entire selection, default regular.
		/// </summary>		
		/// 
		public Font GetFontDetails()
		{
			//This method should handle cases that occur when multiple fonts/styles are selected
			
			int rtb1start = rtb1.SelectionStart;				
			int len = rtb1.SelectionLength; 
			int rtbTempStart = 0;

			if (true)						
			{
				// Return the selection or default font
				if (rtb1.SelectionFont != null)
					return rtb1.SelectionFont;
				else
					return rtb1.Font;
			}

			// Step through the selected text one char at a time	
			// after setting defaults from first char
			rtbTemp.Rtf = rtb1.SelectedRtf;
		
			//Turn everything on so we can turn it off one by one
			FontStyle replystyle =			
				FontStyle.Bold | FontStyle.Italic | FontStyle.Strikeout | FontStyle.Underline;
			
			// Set reply font, size and style to that of first char in selection.
			rtbTemp.Select(rtbTempStart, 1);
			string replyfont = rtbTemp.SelectionFont.Name;
			float replyfontsize = rtbTemp.SelectionFont.Size;
			replystyle = replystyle & rtbTemp.SelectionFont.Style;
			
			// Search the rest of the selection
			for(int i = 1; i < len; ++i)				
			{ 
				rtbTemp.Select(rtbTempStart + i, 1); 
				
				// Check reply for different style
				replystyle = replystyle & rtbTemp.SelectionFont.Style;
				
				// Check font
				if (replyfont != rtbTemp.SelectionFont.FontFamily.Name)
					replyfont = "";

				// Check font size
				if (replyfontsize != rtbTemp.SelectionFont.Size)
					replyfontsize = (float)0.0;
			}

			// Now set font and size if more than one font or font size was selected
			if (replyfont == "")
				replyfont = rtbTemp.Font.FontFamily.Name;

			if (replyfontsize == 0.0)
				replyfontsize = rtbTemp.Font.Size;

			// generate reply font
			Font reply 
				= new Font(replyfont, replyfontsize, replystyle);
			
			return reply;
		}
		#endregion

		#region Keyboard Handler
		private void rtb1_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if (e.Modifiers == Keys.Control)
			{
                ToolStripButton toolStripButton = null;
                
				switch (e.KeyCode)
				{
                    //case Keys.C:
                    //    toolStripButton = this.toolStripButtonCopy;
                    //    break;
                    //case Keys.V:
                    //    toolStripButton = this.toolStripButtonPaste;
                    //    break;
                    //case Keys.X:
                    //    toolStripButton = this.toolStripButtonCut;
                    //    break;
					case Keys.B:
                        toolStripButton = this.toolStripButtonBold;
						break;
					case Keys.I:
                        toolStripButton = this.toolStripButtonItalic;
						break;
					case Keys.U:
                        toolStripButton = this.toolStripButtonUnderline;
						break;
                    case Keys.Z:
                        toolStripButton = this.toolStripButtonUndo;
                        break;
                    case Keys.Y:
                        toolStripButton = this.toolStripButtonRedo;
                        break;
                    case Keys.A:// Ignore
                    case Keys.J:// Ignore
                        e.SuppressKeyPress = true;
                        break;
                    case Keys.E:// Overwrite
                        rtb1.SelectAll();
                        e.SuppressKeyPress = true;
                        break;
				}

                if (toolStripButton != null)
				{
                    //if (e.KeyCode != Keys.S) toolStripButton.Checked = !toolStripButton.Checked;
                    toolStripMain_ItemClicked(null, new ToolStripItemClickedEventArgs(toolStripButton));
				}
			}
			
			//Insert a tab if the tab key was pressed.
			/* NOTE: This was needed because in rtb1_KeyPress I tell the richtextbox not
			 * to handle tab events.  I do that because CTRL+I inserts a tab for some
			 * strange reason.  What was MicroSoft thinking?
			 * Richard Parsons 02/08/2007
			 */
			if (e.KeyCode == Keys.Tab)
				rtb1.SelectedText = "\t";           

		}

		private void rtb1_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if((int)e.KeyChar == 9)
				e.Handled = true; // Stops Ctrl+I from inserting a tab (char HT) into the richtextbox
            try
            {
                if ((Char.IsLetterOrDigit(e.KeyChar) 
                    || Char.IsPunctuation(e.KeyChar)
                    || Char.IsWhiteSpace(e.KeyChar)
                    || Char.IsSymbol(e.KeyChar)) && rtb1.TextLength > rtb1.MaxLength)
                {
                    MessageForm.Show(ResourceLoader.GetString2("MaxNumberOfCaracter", rtb1.MaxLength.ToString()), MessageFormType.Error);
                }
            }
            catch { }
		}
		#endregion

        #region Toolbar Button Click
        private void toolStripMain_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (e.ClickedItem is ToolStripButton)
            {
                // true if style to be added
                // false to remove style
                bool add = ((ToolStripButton)e.ClickedItem).Checked;
                //Switch based on the tag of the button pressed
                switch (e.ClickedItem.Name.ToLower())
                {
                    case "toolstripbuttoncolor":
                        ChangeFontColor((Color)(toolStripButtonColor.Tag));
                        break;
                    case "toolstripbuttonbold":
                        ChangeFontStyle(FontStyle.Bold, !add);
                        break;
                    case "toolstripbuttonitalic":
                        ChangeFontStyle(FontStyle.Italic, !add);
                        break;
                    case "toolstripbuttonunderline":
                        ChangeFontStyle(FontStyle.Underline, !add);
                        break;
                    case "toolstripbuttonstrikeout":
                        ChangeFontStyle(FontStyle.Strikeout, !add);
                        break;
                    case "toolstripbuttonleft":
                        //change horizontal alignment to left
                        toolStripButtonCenter.Checked = false;
                        toolStripButtonRight.Checked = false;
                        rtb1.SelectionAlignment = HorizontalAlignment.Left;
                        break;
                    case "toolstripbuttoncenter":
                        //change horizontal alignment to center
                        toolStripButtonLeft.Checked = false;
                        toolStripButtonRight.Checked = false;
                        rtb1.SelectionAlignment = HorizontalAlignment.Center;
                        break;
                    case "toolstripbuttonright":
                        //change horizontal alignment to right
                        toolStripButtonLeft.Checked = false;
                        toolStripButtonCenter.Checked = false;
                        rtb1.SelectionAlignment = HorizontalAlignment.Right;
                        break;
                    case "toolstripbuttonstamp":
                        OnStamp(new EventArgs()); //send stamp event
                        break;
                    case "color":
                        rtb1.SelectionColor = Color.Black;
                        break;
                    case "toolstripbuttonundo":
                        if (rtb1.CanUndo)
                        {
                            toolStripButtonUndo.Checked = true;
                        }
                        rtb1.Undo();
                        UpdateToolbar();
                        toolStripButtonUndo.Checked = false;
                        return;
                    case "toolstripbuttonredo":
                        if (rtb1.CanRedo)
                        {
                            toolStripButtonRedo.Checked = true;
                        }
                        rtb1.Redo();
                        UpdateToolbar();
                        toolStripButtonRedo.Checked = false;
                        return;
                    case "toolstripbuttonopen":
                        try
                        {
                            if (ofd1.ShowDialog() == DialogResult.OK && ofd1.FileName.Length > 0)
                            {
                                FileInfo file = new FileInfo(ofd1.FileName);
                                if (file.Length <= rtb1.MaxLength)
                                {
                                    if (System.IO.Path.GetExtension(ofd1.FileName).ToLower().Equals(".rtf"))
                                        rtb1.LoadFile(ofd1.FileName, RichTextBoxStreamType.RichText);
                                    else
                                        rtb1.LoadFile(ofd1.FileName, RichTextBoxStreamType.PlainText);
                                }
                                else
                                {
                                    throw new Exception(ResourceLoader.GetString2("ErrorLoadingFile", ofd1.FileName));
                                }
                            }
                        }
                        catch (ArgumentException ae)
                        {
                            if (ae.Message == "Invalid file format.")
                                MessageForm.Show(ResourceLoader.GetString2("ErrorLoadingFile", ofd1.FileName), ae);
                        }
                        catch (FileNotFoundException ex)
                        {
                            MessageForm.Show(ResourceLoader.GetString2("ErrorLoadingFile", ofd1.FileName), ex);
                        }
                        catch (IOException ex)
                        {
                            MessageForm.Show(ResourceLoader.GetString2("ErrorLoadingFile", ofd1.FileName), ex);
                        }                        
                        catch (Exception ex)
                        {
                            MessageForm.Show(ex.Message, ex);
                        }
                        break;
                    case "toolstripbuttonsave":
                        if (sfd1.ShowDialog() == DialogResult.OK && sfd1.FileName.Length > 0)
                            if (System.IO.Path.GetExtension(sfd1.FileName).ToLower().Equals(".rtf"))
                                rtb1.SaveFile(sfd1.FileName);
                            else
                                rtb1.SaveFile(sfd1.FileName, RichTextBoxStreamType.PlainText);
                        break;
                    case "toolstripbuttoncut":
                        {
                            if (rtb1.SelectedText.Length <= 0) break;
                            rtb1.Cut();
                            break;
                        }
                    case "toolstripbuttoncopy":
                        {
                            if (rtb1.SelectedText.Length <= 0) break;
                            rtb1.Copy();
                            break;
                        }
                    case "toolstripbuttonpaste":
                        {
                            try
                            {
                                rtb1.Paste();
                            }
                            catch
                            {
                                MessageBox.Show("ErrorPaste");
                            }
                            break;
                        }
                } //end select
                ((ToolStripButton)e.ClickedItem).Checked = !add;
            }
        }
 
        #endregion

        public static bool isBlank(string text)
        {
            return isBlank(text, true);
        }

        public static bool isBlank(string text, bool canBeEnter)
        {
            bool result = false;
           
            if (text == " " ||
                text == "\t" ||
                text == "\r")
            {
                result = true;
            }
            if (canBeEnter == true)
                return (result || (text == "\n"));
            else
                return (result && (text != "\n"));
        }

    

        protected virtual void contextMenu_cutItem_Click(object sender, EventArgs e)
        {
            this.rtb1.Cut();
        }

        protected virtual void contextMenu_copyItem_Click(object sender, EventArgs e)
        {
            this.rtb1.Copy();
        }

        protected virtual void contextMenu_pasteItem_Click(object sender, EventArgs e)
        {
            if ((rtb1.TextLength + Clipboard.GetText().Length) > rtb1.MaxLength)
            {
                MessageForm.Show(ResourceLoader.GetString2("MaxNumberOfCaracter", rtb1.MaxLength.ToString()), MessageFormType.Error);
            }
            this.rtb1.Paste();            
        }

        private void contextMenu_Popup(object sender, EventArgs e)
        {
            if (!Focused)
            {
                Focus();
            }
            if (this.rtb1.SelectedText.Length == 0)
            {
                menuItemCopy.Enabled = false;
                menuItemCut.Enabled = false;
               
            }
            else
            {
                menuItemCopy.Enabled = true;
                menuItemCut.Enabled = true;
              
            }
           
           
            IDataObject iData = Clipboard.GetDataObject();
            if (!iData.GetDataPresent(DataFormats.Text))
            {
                menuItemPaste.Enabled = false;
            }
            else
            {
                menuItemPaste.Enabled = true;
           
            }

           
        }

	} //end class
} //end namespace

