﻿namespace SmartCadAlarm.Controls
{
    partial class AlarmLprControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanelLprControls = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.pictureEditPlateImage = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEditContourImage = new DevExpress.XtraEditors.PictureEdit();
            this.groupControlEditPlate = new DevExpress.XtraEditors.GroupControl();
            this.textEditEditPlate = new DevExpress.XtraEditors.TextEdit();
            this.groupControlFindPlateResult = new DevExpress.XtraEditors.GroupControl();
            this.pictureEditFindPlateResult = new DevExpress.XtraEditors.PictureEdit();
            this.groupControlReadPlate = new DevExpress.XtraEditors.GroupControl();
            this.labelControlReadPlate = new DevExpress.XtraEditors.LabelControl();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupPlateImage = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupContourImage = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlMapControl = new DevExpress.XtraLayout.LayoutControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.dockPanelLprControls.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEditPlateImage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEditContourImage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlEditPlate)).BeginInit();
            this.groupControlEditPlate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditEditPlate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlFindPlateResult)).BeginInit();
            this.groupControlFindPlateResult.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEditFindPlateResult.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlReadPlate)).BeginInit();
            this.groupControlReadPlate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupPlateImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupContourImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlMapControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // dockManager1
            // 
            this.dockManager1.Form = this;
            this.dockManager1.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanelLprControls});
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "System.Windows.Forms.StatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            // 
            // dockPanelLprControls
            // 
            this.dockPanelLprControls.Controls.Add(this.dockPanel1_Container);
            this.dockPanelLprControls.Dock = DevExpress.XtraBars.Docking.DockingStyle.Bottom;
            this.dockPanelLprControls.FloatVertical = true;
            this.dockPanelLprControls.ID = new System.Guid("56c809dc-45fb-4cb2-815a-06f242a8d0b8");
            this.dockPanelLprControls.Location = new System.Drawing.Point(0, 411);
            this.dockPanelLprControls.Name = "dockPanelLprControls";
            this.dockPanelLprControls.OriginalSize = new System.Drawing.Size(200, 200);
            this.dockPanelLprControls.Size = new System.Drawing.Size(1264, 200);
            this.dockPanelLprControls.Text = "dockPanelControls";
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.layoutControl2);
            this.dockPanel1_Container.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dockPanel1_Container.Location = new System.Drawing.Point(4, 23);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(1256, 173);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.pictureEditPlateImage);
            this.layoutControl2.Controls.Add(this.pictureEditContourImage);
            this.layoutControl2.Controls.Add(this.groupControlEditPlate);
            this.layoutControl2.Controls.Add(this.groupControlFindPlateResult);
            this.layoutControl2.Controls.Add(this.groupControlReadPlate);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(1256, 173);
            this.layoutControl2.TabIndex = 11;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // pictureEditPlateImage
            // 
            this.pictureEditPlateImage.Location = new System.Drawing.Point(491, 44);
            this.pictureEditPlateImage.Name = "pictureEditPlateImage";
            this.pictureEditPlateImage.Size = new System.Drawing.Size(304, 105);
            this.pictureEditPlateImage.StyleController = this.layoutControl2;
            this.pictureEditPlateImage.TabIndex = 13;
            // 
            // pictureEditContourImage
            // 
            this.pictureEditContourImage.Location = new System.Drawing.Point(24, 44);
            this.pictureEditContourImage.Name = "pictureEditContourImage";
            this.pictureEditContourImage.Size = new System.Drawing.Size(439, 105);
            this.pictureEditContourImage.StyleController = this.layoutControl2;
            this.pictureEditContourImage.TabIndex = 12;
            // 
            // groupControlEditPlate
            // 
            this.groupControlEditPlate.Controls.Add(this.textEditEditPlate);
            this.groupControlEditPlate.Location = new System.Drawing.Point(1029, 104);
            this.groupControlEditPlate.Name = "groupControlEditPlate";
            this.groupControlEditPlate.Size = new System.Drawing.Size(215, 57);
            this.groupControlEditPlate.TabIndex = 7;
            this.groupControlEditPlate.Text = "groupControlEditPlate";
            // 
            // textEditEditPlate
            // 
            this.textEditEditPlate.Location = new System.Drawing.Point(5, 45);
            this.textEditEditPlate.Name = "textEditEditPlate";
            this.textEditEditPlate.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEditEditPlate.Properties.Appearance.Options.UseFont = true;
            this.textEditEditPlate.Size = new System.Drawing.Size(201, 49);
            this.textEditEditPlate.TabIndex = 0;
            this.textEditEditPlate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textEditEditPlate_KeyPress);
            // 
            // groupControlFindPlateResult
            // 
            this.groupControlFindPlateResult.Controls.Add(this.pictureEditFindPlateResult);
            this.groupControlFindPlateResult.Location = new System.Drawing.Point(811, 12);
            this.groupControlFindPlateResult.Name = "groupControlFindPlateResult";
            this.groupControlFindPlateResult.Size = new System.Drawing.Size(433, 88);
            this.groupControlFindPlateResult.TabIndex = 6;
            this.groupControlFindPlateResult.Text = "groupControlFindPlateResult";
            // 
            // pictureEditFindPlateResult
            // 
            this.pictureEditFindPlateResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureEditFindPlateResult.Location = new System.Drawing.Point(2, 22);
            this.pictureEditFindPlateResult.Name = "pictureEditFindPlateResult";
            this.pictureEditFindPlateResult.Size = new System.Drawing.Size(429, 64);
            this.pictureEditFindPlateResult.TabIndex = 1;
            // 
            // groupControlReadPlate
            // 
            this.groupControlReadPlate.Controls.Add(this.labelControlReadPlate);
            this.groupControlReadPlate.Location = new System.Drawing.Point(811, 104);
            this.groupControlReadPlate.Name = "groupControlReadPlate";
            this.groupControlReadPlate.Size = new System.Drawing.Size(214, 57);
            this.groupControlReadPlate.TabIndex = 8;
            this.groupControlReadPlate.Text = "groupControlReadPlate";
            // 
            // labelControlReadPlate
            // 
            this.labelControlReadPlate.Appearance.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControlReadPlate.Location = new System.Drawing.Point(41, 48);
            this.labelControlReadPlate.Name = "labelControlReadPlate";
            this.labelControlReadPlate.Size = new System.Drawing.Size(120, 42);
            this.labelControlReadPlate.TabIndex = 0;
            this.labelControlReadPlate.Text = "ABC123";
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem6,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlGroupPlateImage,
            this.layoutControlGroupContourImage});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup1";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1256, 173);
            this.layoutControlGroup2.Text = "layoutControlGroup1";
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.groupControlFindPlateResult;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem6.Location = new System.Drawing.Point(799, 0);
            this.layoutControlItem6.Name = "layoutControlItem3";
            this.layoutControlItem6.Size = new System.Drawing.Size(437, 92);
            this.layoutControlItem6.Text = "layoutControlItem3";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.groupControlEditPlate;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem9.Location = new System.Drawing.Point(1017, 92);
            this.layoutControlItem9.Name = "layoutControlItem5";
            this.layoutControlItem9.Size = new System.Drawing.Size(219, 61);
            this.layoutControlItem9.Text = "layoutControlItem5";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextToControlDistance = 0;
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.groupControlReadPlate;
            this.layoutControlItem10.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem10.Location = new System.Drawing.Point(799, 92);
            this.layoutControlItem10.Name = "layoutControlItem4";
            this.layoutControlItem10.Size = new System.Drawing.Size(218, 61);
            this.layoutControlItem10.Text = "layoutControlItem4";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextToControlDistance = 0;
            this.layoutControlItem10.TextVisible = false;
            // 
            // layoutControlGroupPlateImage
            // 
            this.layoutControlGroupPlateImage.CustomizationFormText = "layoutControlGroupPlateImage";
            this.layoutControlGroupPlateImage.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.layoutControlGroupPlateImage.Location = new System.Drawing.Point(467, 0);
            this.layoutControlGroupPlateImage.Name = "layoutControlGroupPlateImage";
            this.layoutControlGroupPlateImage.Size = new System.Drawing.Size(332, 153);
            this.layoutControlGroupPlateImage.Text = "layoutControlGroupPlateImage";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.pictureEditPlateImage;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(308, 109);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlGroupContourImage
            // 
            this.layoutControlGroupContourImage.CustomizationFormText = "layoutControlGroupContourImage";
            this.layoutControlGroupContourImage.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3});
            this.layoutControlGroupContourImage.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupContourImage.Name = "layoutControlGroupContourImage";
            this.layoutControlGroupContourImage.Size = new System.Drawing.Size(467, 153);
            this.layoutControlGroupContourImage.Text = "layoutControlGroupContourImage";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.pictureEditContourImage;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(443, 109);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlMapControl
            // 
            this.layoutControlMapControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlMapControl.Location = new System.Drawing.Point(0, 0);
            this.layoutControlMapControl.Name = "layoutControlMapControl";
            this.layoutControlMapControl.Root = this.layoutControlGroup1;
            this.layoutControlMapControl.Size = new System.Drawing.Size(1264, 411);
            this.layoutControlMapControl.TabIndex = 10;
            this.layoutControlMapControl.Text = "layoutControlMapControl";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1264, 411);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1244, 391);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // AlarmLprControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControlMapControl);
            this.Controls.Add(this.dockPanelLprControls);
            this.Name = "AlarmLprControl";
            this.Size = new System.Drawing.Size(1264, 611);
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.dockPanelLprControls.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureEditPlateImage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEditContourImage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlEditPlate)).EndInit();
            this.groupControlEditPlate.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEditEditPlate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlFindPlateResult)).EndInit();
            this.groupControlFindPlateResult.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureEditFindPlateResult.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlReadPlate)).EndInit();
            this.groupControlReadPlate.ResumeLayout(false);
            this.groupControlReadPlate.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupPlateImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupContourImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlMapControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelLprControls;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private AlarmDemoTelemetryStartIncidentControl alarmDemoTelemetryStartIncidentControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControlMapControl;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraEditors.GroupControl groupControlEditPlate;
        private DevExpress.XtraEditors.GroupControl groupControlFindPlateResult;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.LabelControl labelControlReadPlate;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupPlateImage;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupContourImage;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        public AlarmDemoTelemetryStartIncidentControl alarmDemoTelemetryStartIncidentControl2;
        internal DevExpress.XtraEditors.GroupControl groupControlReadPlate;
        internal DevExpress.XtraEditors.PictureEdit pictureEditFindPlateResult;
        internal DevExpress.XtraEditors.TextEdit textEditEditPlate;
        internal DevExpress.XtraEditors.PictureEdit pictureEditContourImage;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        internal DevExpress.XtraEditors.PictureEdit pictureEditPlateImage;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;

    }
}
