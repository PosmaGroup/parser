using SmartCadControls.Controls;
namespace SmartCadAlarm.Controls
{
    partial class CallUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CallUserControl));
            this.buttonTelephone = new System.Windows.Forms.Button();
            this.buttonCreateNewIncident = new System.Windows.Forms.Button();
            this.buttonAddToIncident = new System.Windows.Forms.Button();
            this.buttonFinalize = new System.Windows.Forms.Button();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.chronometerEx = new ChronometerEx();
            this.progressBarEx = new ProgressBarEx();
            this.SuspendLayout();
            // 
            // buttonTelephone
            // 
            this.buttonTelephone.FlatAppearance.BorderSize = 0;
            this.buttonTelephone.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonTelephone.Image = ((System.Drawing.Image)(resources.GetObject("buttonTelephone.Image")));
            this.buttonTelephone.Location = new System.Drawing.Point(2, 2);
            this.buttonTelephone.Name = "buttonTelephone";
            this.buttonTelephone.Size = new System.Drawing.Size(32, 32);
            this.buttonTelephone.TabIndex = 0;
            this.toolTip.SetToolTip(this.buttonTelephone, "Atender");
            this.buttonTelephone.UseVisualStyleBackColor = true;
            this.buttonTelephone.MouseLeave += new System.EventHandler(this.CallUserButton_MouseLeave);
            // 
            // buttonCreateNewIncident
            // 
            this.buttonCreateNewIncident.FlatAppearance.BorderSize = 0;
            this.buttonCreateNewIncident.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCreateNewIncident.Image = ((System.Drawing.Image)(resources.GetObject("buttonCreateNewIncident.Image")));
            this.buttonCreateNewIncident.Location = new System.Drawing.Point(399, 2);
            this.buttonCreateNewIncident.Name = "buttonCreateNewIncident";
            this.buttonCreateNewIncident.Size = new System.Drawing.Size(32, 32);
            this.buttonCreateNewIncident.TabIndex = 3;
            this.toolTip.SetToolTip(this.buttonCreateNewIncident, "Registrar");
            this.buttonCreateNewIncident.UseVisualStyleBackColor = true;
            this.buttonCreateNewIncident.MouseHover += new System.EventHandler(this.buttonCreateNewIncident_MouseHover);
            this.buttonCreateNewIncident.MouseLeave += new System.EventHandler(this.CallUserButton_MouseLeave);
            // 
            // buttonAddToIncident
            // 
            this.buttonAddToIncident.FlatAppearance.BorderSize = 0;
            this.buttonAddToIncident.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAddToIncident.Image = ((System.Drawing.Image)(resources.GetObject("buttonAddToIncident.Image")));
            this.buttonAddToIncident.Location = new System.Drawing.Point(438, 2);
            this.buttonAddToIncident.Name = "buttonAddToIncident";
            this.buttonAddToIncident.Size = new System.Drawing.Size(32, 32);
            this.buttonAddToIncident.TabIndex = 4;
            this.toolTip.SetToolTip(this.buttonAddToIncident, "Asociar");
            this.buttonAddToIncident.UseVisualStyleBackColor = true;
            this.buttonAddToIncident.MouseLeave += new System.EventHandler(this.CallUserButton_MouseLeave);
            // 
            // buttonFinalize
            // 
            this.buttonFinalize.FlatAppearance.BorderSize = 0;
            this.buttonFinalize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonFinalize.Image = ((System.Drawing.Image)(resources.GetObject("buttonFinalize.Image")));
            this.buttonFinalize.Location = new System.Drawing.Point(477, 2);
            this.buttonFinalize.Name = "buttonFinalize";
            this.buttonFinalize.Size = new System.Drawing.Size(32, 32);
            this.buttonFinalize.TabIndex = 5;
            this.toolTip.SetToolTip(this.buttonFinalize, "Finalizar");
            this.buttonFinalize.UseVisualStyleBackColor = true;
            this.buttonFinalize.MouseLeave += new System.EventHandler(this.CallUserButton_MouseLeave);
            // 
            // chronometerEx
            // 
            this.chronometerEx.ColorBackGround = System.Drawing.Color.White;
            this.chronometerEx.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold);
            this.chronometerEx.Location = new System.Drawing.Point(306, 8);
            this.chronometerEx.MaximumSize = new System.Drawing.Size(80, 20);
            this.chronometerEx.MinimumSize = new System.Drawing.Size(80, 20);
            this.chronometerEx.Name = "chronometerEx";
            this.chronometerEx.Size = new System.Drawing.Size(80, 20);
            this.chronometerEx.TabIndex = 2;
            // 
            // progressBarEx
            // 
            this.progressBarEx.ColorBackGround = System.Drawing.Color.White;
            this.progressBarEx.ColorBarBorder = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(240)))), ((int)(((byte)(170)))));
            this.progressBarEx.ColorBarCenter = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(150)))), ((int)(((byte)(10)))));
            this.progressBarEx.ColorText = System.Drawing.Color.Black;
            this.progressBarEx.Location = new System.Drawing.Point(47, 8);
            this.progressBarEx.Name = "progressBarEx";
            this.progressBarEx.Position = 50;
            this.progressBarEx.PositionMax = 100;
            this.progressBarEx.PositionMin = 0;
            this.progressBarEx.Size = new System.Drawing.Size(246, 20);
            this.progressBarEx.SteepDistance = ((byte)(0));
            this.progressBarEx.TabIndex = 1;
            // 
            // CallUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.buttonFinalize);
            this.Controls.Add(this.buttonAddToIncident);
            this.Controls.Add(this.buttonCreateNewIncident);
            this.Controls.Add(this.chronometerEx);
            this.Controls.Add(this.progressBarEx);
            this.Controls.Add(this.buttonTelephone);
            this.MinimumSize = new System.Drawing.Size(518, 33);
            this.Name = "CallUserControl";
            this.Size = new System.Drawing.Size(518, 33);
            this.ResumeLayout(false);

        }

        #endregion

        private ProgressBarEx progressBarEx;
        private ChronometerEx chronometerEx;
        private System.Windows.Forms.Button buttonTelephone;
        private System.Windows.Forms.Button buttonCreateNewIncident;
        private System.Windows.Forms.Button buttonAddToIncident;
        private System.Windows.Forms.Button buttonFinalize;
        private System.Windows.Forms.ToolTip toolTip;
    }
}
