using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using SmartCadCore.Common;
using SmartCadCore.Core;
using SmartCadAlarm.Gui;
using SmartCadAlarm.Enums;
using SmartCadCore.Enums;

namespace SmartCadAlarm.Controls
{
    public partial class CallUserControl : UserControl
    {
        #region Fields

        private event EventHandler limitReached;

        private Color buttonOverBorderColor;
        private Color buttonClickBorderColor;

        private Color beginColor;
        private Color middleColor;
        private Color endColor;

        private int beginLimit;
        private int limit;

        private Color beginColorBackGround;
        private Color endColorBackGround;
        private Color borderColor;

        private TelephoneStatusType telephoneStatus;

        private Dictionary<Button, bool> buttonStatus;

        private bool limitReachedCalled;

        private FrontClientStateEnum frontClientState;
        #endregion

        public CallUserControl()
        {
            InitializeComponent();

            buttonOverBorderColor = Color.Gray;
            buttonClickBorderColor = Color.Black;

            beginColor = Color.Green;
            middleColor = Color.Yellow;
            endColor = Color.Red;

            beginColorBackGround = Color.White;
            endColorBackGround = ControlPaint.Dark(Color.Blue);
            borderColor = ControlPaint.Dark(Color.Blue);

            buttonStatus = new Dictionary<Button, bool>(4);
        }

        public Color BeginColorBackGround
        {
            get
            {
                return beginColorBackGround;
            }
            set
            {
                beginColorBackGround = value;
            }
        }

        public Color EndColorBackGround
        {
            get
            {
                return endColorBackGround;
            }
            set
            {
                endColorBackGround = value;
            }
        }

        public Color BorderColor
        {
            get
            {
                return borderColor;
            }
            set
            {
                borderColor = value;
            }
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            base.OnPaintBackground(e);

            e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;

            GraphicsPath graphPath = GetPath();

            Pen borderPen = new Pen(borderColor, 1);

            e.Graphics.DrawPath(borderPen, graphPath);

            borderPen.Dispose();
            graphPath.Dispose();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            Rectangle rect;
            LinearGradientBrush brush;
            
            rect = new Rectangle(1, 1, this.Width - 2, this.Height - 2);
            brush = new LinearGradientBrush(rect, beginColorBackGround, endColorBackGround, LinearGradientMode.Vertical);
            
            e.Graphics.FillRectangle(brush, rect);

            brush.Dispose();

            ControlPaint.DrawBorder3D(e.Graphics, 39, 2, 2, 32, Border3DStyle.RaisedInner);

            ControlPaint.DrawBorder3D(e.Graphics, 298, 2, 2, 32, Border3DStyle.RaisedInner);

            ControlPaint.DrawBorder3D(e.Graphics, 392, 2, 2, 32, Border3DStyle.RaisedInner);
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            SetupButton(this.buttonTelephone);
            SetupButton(this.buttonCreateNewIncident);
            SetupButton(this.buttonAddToIncident);
            SetupButton(this.buttonFinalize);

            this.chronometerEx.SecondAdded += new EventHandler(chronometerEx_SecondAdded);

            this.progressBarEx.PositionMin = 0;
            if (limit > 0)
            {
                this.progressBarEx.PositionMax = limit;
            }
            else
            {
                this.progressBarEx.PositionMax = 1;
            }
            this.progressBarEx.SteepWidth = 1;
            this.progressBarEx.Position = 0;
            this.progressBarEx.TextShadow = false;

            this.buttonTelephone.Click += new EventHandler(buttonTelephone_Click);
            this.buttonCreateNewIncident.Click += new EventHandler(buttonCreateNewIncident_Click);
            this.buttonAddToIncident.Click += new EventHandler(buttonAddToIncident_Click);
            this.buttonFinalize.Click += new EventHandler(buttonFinalize_Click);

            this.TelephoneStatus = TelephoneStatusType.None;
        }

        public event EventHandler Telephone;

        protected virtual void OnTelephone(EventArgs e)
        {
            if (Telephone != null)
            {
                Telephone(this, e);
            }
        }

        private void buttonTelephone_Click(object sender, EventArgs e)
        {
            if (buttonStatus[buttonTelephone])
                OnTelephone(EventArgs.Empty);
        }

        public event EventHandler CreateNewIncident;

        protected virtual void OnCreateNewIncident(EventArgs e)
        {
            if (CreateNewIncident != null)
            {
                CreateNewIncident(this, e);
            }
        }

        private void buttonCreateNewIncident_Click(object sender, EventArgs e)
        {
            OnCreateNewIncident(EventArgs.Empty);
        }

        public event EventHandler AddToIncident;

        protected virtual void OnAddToIncident(EventArgs e)
        {
            if (AddToIncident != null)
            {
                AddToIncident(this, e);
            }
        }

        private void buttonAddToIncident_Click(object sender, EventArgs e)
        {
            OnAddToIncident(EventArgs.Empty);
        }

        public event EventHandler Finalize;

        protected virtual void OnFinalize(EventArgs e)
        {
            
            if (Finalize != null)
            {
                Finalize(this, e);
            }
            //Reset();
        }

        private void buttonFinalize_Click(object sender, EventArgs e)
        {
            OnFinalize(EventArgs.Empty);
        }

        private void chronometerEx_SecondAdded(object sender, EventArgs e)
        {
            this.progressBarEx.Position = chronometerEx.Seconds;

            if (chronometerEx.Seconds > limit)
            {
                OnLimitReached(EventArgs.Empty);

                progressBarEx.Text = ResourceLoader.GetString2("TimeSpent").ToUpper();
                this.chronometerEx.SecondAdded -= new EventHandler(chronometerEx_SecondAdded);
                limitReachedCalled = true;
            }

            if (0 <= chronometerEx.Seconds && chronometerEx.Seconds <= beginLimit)
            {
                progressBarEx.ColorBarBorder = ControlPaint.LightLight(beginColor);
                Color nextColor = FormUtil.MakeColor(beginColor, - 25);
                progressBarEx.ColorBarCenter = nextColor;
            }
            else if (beginLimit < chronometerEx.Seconds && chronometerEx.Seconds <= Limit)
            {
                progressBarEx.ColorBarBorder = ControlPaint.LightLight(middleColor);
                Color nextColor = FormUtil.MakeColor(middleColor, -25);
                progressBarEx.ColorBarCenter = nextColor;
            }
            else
            {
                progressBarEx.ColorBarBorder = ControlPaint.LightLight(endColor);
                Color nextColor = FormUtil.MakeColor(endColor, -25);
                progressBarEx.ColorBarCenter = nextColor;
            }
        }

        #region Properties

        public event EventHandler LimitReached
        {
            add
            {
                limitReached += value;
            }
            remove
            {
                limitReached -= value;
            }
        }

        public Color ButtonOverBorderColor
        {
            get
            {
                return buttonOverBorderColor;
            }
            set
            {
                buttonOverBorderColor = value;
            }
        }

        public Color ButtonClickBorderColor
        {
            get
            {
                return buttonClickBorderColor;
            }
            set
            {
                buttonClickBorderColor = value;
            }
        }

        public int Limit
        {
            get
            {
                return limit;
            }
            set
            {
                limit = value;
                progressBarEx.PositionMax = limit;
                beginLimit = Convert.ToInt32(limit / 2);
            }
        }

        #endregion

        protected virtual GraphicsPath GetPath()
        {
            GraphicsPath graphPath = new GraphicsPath();

            try
            {
                Rectangle rect = this.ClientRectangle;
                int offset = 0;

                int rectWidth = rect.Width - 1 - offset;
                int rectHeight = rect.Height - 1 - offset;
                int curveWidth = 4;

                graphPath.AddArc(rectWidth - curveWidth, offset, curveWidth, curveWidth, 270, 90);
                graphPath.AddArc(rectWidth - curveWidth, rectHeight - curveWidth, curveWidth, curveWidth, 0, 90);
                graphPath.AddArc(offset, rectHeight - curveWidth, curveWidth, curveWidth, 90, 90);
                graphPath.AddArc(offset, offset, curveWidth, curveWidth, 180, 90);

                graphPath.CloseFigure();
            }
            catch
            {
                graphPath.AddRectangle(this.ClientRectangle);
            }

            return graphPath;
        }

        private void button_MouseEnter(object sender, EventArgs e)
        {
            Button button = sender as Button;

            if (buttonStatus[button])
            {
                button.FlatAppearance.BorderSize = 1;
                button.FlatAppearance.BorderColor = buttonOverBorderColor;
            }
        }

        private void button_MouseLeave(object sender, EventArgs e)
        {
            Button button = sender as Button;

            if (buttonStatus[button])
            {
                button.FlatAppearance.BorderSize = 0;
            }
        }

        private void button_MouseDown(object sender, MouseEventArgs e)
        {
            Button button = sender as Button;

            if (buttonStatus[button])
            {
                button.FlatAppearance.BorderSize = 1;
                button.FlatAppearance.BorderColor = buttonClickBorderColor;
            }
        }

        private void button_MouseUp(object sender, MouseEventArgs e)
        {
            Button button = sender as Button;

            if (buttonStatus[button])
            {
                button.FlatAppearance.BorderSize = 1;
                button.FlatAppearance.BorderColor = buttonOverBorderColor;
            }
        }

        private void SetupButton(Button button)
        {
            button.MouseEnter += new EventHandler(button_MouseEnter);
            button.MouseLeave += new EventHandler(button_MouseLeave);
            button.MouseDown += new MouseEventHandler(button_MouseDown);
            button.MouseUp += new MouseEventHandler(button_MouseUp);

            buttonStatus.Add(button, true);
        }

        public void Stop()
        {
            //this.progressBarEx.Text = "";
            this.chronometerEx.Stop();
        }

        public void Reset()
        {
            this.progressBarEx.Position = this.progressBarEx.PositionMin;
            this.progressBarEx.Text = "";
            this.chronometerEx.Reset();
            if (limitReachedCalled == true)
            {
                this.chronometerEx.SecondAdded += new EventHandler(chronometerEx_SecondAdded);
            }
            limitReachedCalled = false;
        }

        public void Start()
        {
            this.progressBarEx.Text = "";
            this.chronometerEx.Start();
        }

        protected virtual void OnLimitReached(EventArgs e)
        {
            if (limitReached != null)
                limitReached(this, e);
        }

        public bool TelephoneActive
        {
            get
            {
                return buttonStatus[buttonTelephone];
            }
            set
            {
                buttonStatus[this.buttonTelephone] = value;

                if (value == false)
                    this.buttonTelephone.FlatAppearance.BorderSize = 0;
            }
        }

        public bool TelephoneEnabled
        {
            get
            {
                return this.buttonTelephone.Enabled;
            }
            set
            {
                this.buttonTelephone.Enabled = value;
            }
        }

        public bool CreateNewIncidentEnabled
        {
            get
            {
                return this.buttonCreateNewIncident.Enabled;
            }
            set
            {
                this.buttonCreateNewIncident.Enabled = value;
            }
        }

        public bool AddToIncidentEnabled
        {
            get
            {
                return this.buttonAddToIncident.Enabled;
            }
            set
            {
                this.buttonAddToIncident.Enabled = value;
            }
        }

        public bool FinalizeEnabled
        {
            get
            {
                return this.buttonFinalize.Enabled;
            }
            set
            {
                this.buttonFinalize.Enabled = value;
            }
        }

        public TelephoneStatusType TelephoneStatus
        {
            get
            {
                return telephoneStatus;
            }
            set
            {
                telephoneStatus = value;

                switch (telephoneStatus)
                {
                    case TelephoneStatusType.None:
                        this.buttonTelephone.Image = ResourceLoader.GetImage("TelephoneNone");
                        toolTip.SetToolTip(buttonTelephone, "");
                        this.TelephoneEnabled = false;
                        break;
                    case TelephoneStatusType.Incoming:
                        buttonTelephone.Image = ResourceLoader.GetImage("TelephoneIncoming");
                        toolTip.SetToolTip(buttonTelephone, ResourceLoader.GetString2("AnswerPhone"));
                        this.TelephoneEnabled = true;
                        this.TelephoneActive = true;
                        break;
                    case TelephoneStatusType.Active:
                        buttonTelephone.Image = ResourceLoader.GetImage("TelephoneActive");
                        toolTip.SetToolTip(buttonTelephone, ResourceLoader.GetString2("HangUp"));
                        this.TelephoneEnabled = true;
                        this.TelephoneActive = true;
                        break;
                    case TelephoneStatusType.Dropped:
                        buttonTelephone.Image = ResourceLoader.GetImage("TelephoneDropped");
                        toolTip.SetToolTip(buttonTelephone, "");
                        this.TelephoneEnabled = true;
                        this.TelephoneActive = false;
                        break;
                    case TelephoneStatusType.Ended:
                        buttonTelephone.Image = ResourceLoader.GetImage("TelephoneEnded");
                        toolTip.SetToolTip(buttonTelephone, "");
                        this.TelephoneEnabled = true;
                        this.TelephoneActive = false;
                        break;
                }
            }
        }

        public FrontClientStateEnum FrontClientState
        {
            get
            {
                return this.frontClientState;
            }
            set
            {
                this.frontClientState = value;
            }
        }

        private void buttonCreateNewIncident_MouseHover(object sender, EventArgs e)
        {
            if (this.frontClientState == FrontClientStateEnum.RegisteringIncident)
                toolTip.SetToolTip(buttonCreateNewIncident, ResourceLoader.GetString2("ToolTipButtonCreateNewIncidentRegister"));
            else if(this.frontClientState == FrontClientStateEnum.UpdatingIncident)
                toolTip.SetToolTip(buttonCreateNewIncident, ResourceLoader.GetString2("ToolTipButtonCreateNewIncidentUpdate"));
            else
                toolTip.SetToolTip(buttonCreateNewIncident, "");
        }

        private void CallUserButton_MouseLeave(object sender, EventArgs e)
        {
            this.toolTip.Hide((Button)sender);
        }
    }

    //public enum TelephoneStatusType
    //{
    //    None,
    //    Connected,
    //    Incoming,
    //    Outgoing,
    //    Active,
    //    Dropped,
    //    Ended
    //}
}
