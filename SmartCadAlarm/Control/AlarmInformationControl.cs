using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Collections;

using SmartCadAlarm.Controls;
using SmartCadCore.Common;
using SmartCadCore.Core;
using SmartCadAlarm.Gui;
using SmartCadCore.ClientData;
using SmartCadAlarm.Enums;
using SmartCadControls.Controls;


namespace SmartCadAlarm.Controls
{

    public partial class AlarmInformationControl: HeaderPanelControl
    {
        #region Fields

       
        private AddressClientData addressData;
        private AlarmClientStateEnum alarmClientState = AlarmClientStateEnum.WaitingForAlarmIncident;
        protected CallRingingEventArgs incomingCallEventArgs;
        private StructClientData structClientData;

        #endregion

        public AlarmInformationControl()
        {
            InitializeComponent();
            this.groupControlBody.Text = ResourceLoader.GetString2("AlarmDataHeaderText");
			LoadLanguage();
        }

		private void LoadLanguage()
		{
            layoutControlItemCity.Text = ResourceLoader.GetString2("City") + ":";
            layoutControlItemStructName.Text = ResourceLoader.GetString2("ZoneName") + ":";
            layoutControlItemStreet.Text = ResourceLoader.GetString2("StreetName") + ":";
            layoutControlItemTown.Text = ResourceLoader.GetString2("Town") + ":";
            layoutControlItemSensor.Text = ResourceLoader.GetString2("Sensor") + ":";
            layoutControlItemDate.Text = ResourceLoader.GetString2("Date") + ":";
            layoutControlDatalogger.Text = ResourceLoader.GetString2("Datalogger") + ":";
            layoutControlEvent.Text = ResourceLoader.GetString2("Variable") + ":";
            this.Text = ResourceLoader.GetString2("AlarmDataHeaderText");
		}

        #region Properties

        public AlarmClientStateEnum AlarmClientState
        {
            get
            {
                return this.alarmClientState;
            }
            set
            {
                this.alarmClientState = value;
                if (value == AlarmClientStateEnum.WaitingForAlarmIncident)
                    SetFrontClientWaitingForCallState();
                else if (value == AlarmClientStateEnum.RegisteringAlarmIncident)
                    SetFrontClientRegisteringCallState();
            }
        }
        public string SensorName
        {
            get
            {
                return this.textBoxExSensor.Text;
            }

             set
            {
                this.textBoxExSensor.Text = value;
            }

        }

        public string DataloggerAlarm
        {
            get
            {
                return this.textBoxExDatalogger.Text;
            }

            set
            {
                this.textBoxExDatalogger.Text = value;
            }

        }

        public string SensorCurrentEvent
        {
            get
            {
                return this.textBoxExEvent.Text;
            }

            set
            {
                this.textBoxExEvent.Text = value;
            }

        }

        public string SensorDateAlarm
        {
            get
            {
                return this.textBoxExDate.Text;
            }

            set
            {
                this.textBoxExDate.Text = value;
            }

        }

        public string SensorStreet
        {
            get
            {
                return this.textBoxExStreet.Text;
            }

            set
            {
                this.textBoxExStreet.Text = value;
            }

        }

        public string SensorState
        {
            get
            {
                return this.textBoxExCity.Text;
            }

            set
            {
                this.textBoxExCity.Text = value;
            }

        }

        public string SensorTown
        {
            get
            {
                return this.textBoxExTown.Text;
            }

            set
            {
                this.textBoxExTown.Text = value;
            }

        }     

        public StructClientData StructClientData
        {
            get
            {
                return structClientData;
            }

             set
            {
                if (value != null)
                {
                    this.textBoxExStruct.Text = (value as StructClientData).Name;
                }
                this.structClientData = value;
            }

        }
       

        [RefreshProperties(RefreshProperties.All),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
        Browsable(false)]
        public AddressClientData CallerAddress
        {
            get
            {
                if (null != this.addressData)
                {
                    this.addressData.Zone = this.textBoxExStruct.Text;
                    this.addressData.Street = this.textBoxExStreet.Text;
                    this.addressData.Reference = this.textBoxExCity.Text;
                    this.addressData.More = this.textBoxExTown.Text;                   
                }
                else
                {
                    this.addressData = new AddressClientData(
                        this.textBoxExStruct.Text,
                        this.textBoxExStreet.Text,
                        this.textBoxExCity.Text,
                        this.textBoxExTown.Text,                        
                        false);
                }

                return addressData;
            }
            set
            {
                addressData = value;

                if (null != this.addressData)
                {
                    this.textBoxExStruct.Text = this.addressData.Zone;
                    this.textBoxExStreet.Text = this.addressData.Street;
                    this.textBoxExCity.Text = this.addressData.Reference;
                    this.textBoxExTown.Text = this.addressData.More;               
                }
            }
        }

		public override bool Active
		{
			get
			{
				return base.Active;
			}
			set
			{
				base.Active = value;
				Image img;

				img = pictureBoxNumber.Image;

				pictureBoxNumber.Image = ResourceLoader.GetImage(GetNumberImageName());

				if (img != null)
					img.Dispose();
				if (Active)
				{                 
					this.pictureBoxNumber.LookAndFeel.Style = GetStyle(ActiveColor);
					this.groupControlBody.LookAndFeel.Style = GetStyle(ActiveColor);
					this.layoutControl1.LookAndFeel.Style = GetStyle(ActiveColor);
				}
				else
				{
					this.pictureBoxNumber.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Skin;
					this.groupControlBody.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Skin;
					this.layoutControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Skin;
					this.pictureBoxNumber.LookAndFeel.SkinName = InactiveColor;
					this.groupControlBody.LookAndFeel.SkinName = InactiveColor;
					this.layoutControl1.LookAndFeel.SkinName = InactiveColor;
				}
			}
		}

        #endregion        
                     
        private void ResetAddresControls()
        {           
            this.textBoxExCity.Text = string.Empty;
            this.textBoxExTown.Text = string.Empty;
            this.textBoxExStruct.Text = string.Empty;
            this.textBoxExStreet.Text = string.Empty;
        }

        private void SetFrontClientUpdatingCallState()
        {
            this.textBoxExStreet.Enabled = false;
            this.textBoxExStruct.Enabled = false;
        }

        private void SetFrontClientRegisteringCallState()
        {
            //this.comboBoxExCcvtZone.Enabled = true;
            //this.comboBoxExStructType.Enabled = true;
            //this.comboBoxExStruct.Enabled = true;
            //this.comboBoxExCamera.Enabled = true;
            this.pictureBoxNumber.Enabled = true;
        }

        private void SetFrontClientWaitingForCallState()
        {
            //this.comboBoxExCcvtZone.Enabled = false;
            //this.comboBoxExStructType.Enabled = false;
            //this.comboBoxExStruct.Enabled = false;
            //this.comboBoxExCamera.Enabled = false;
            this.pictureBoxNumber.Enabled = false;
        }

        public void CleanControl()
        {
            FormUtil.InvokeRequired(this, new MethodInvoker(delegate
            {
                this.textBoxExDatalogger.Clear();
                this.textBoxExSensor.Clear();
                this.textBoxExEvent.Clear();
                this.textBoxExDate.Clear();
                this.textBoxExTown.Clear();
                this.textBoxExCity.Clear();
                this.textBoxExStreet.Clear();
                this.textBoxExStruct.Clear();
            }));
        }
                            
        //[Browsable(true)]
        //public event EventHandler<GeoPointEventArgs> SendGeoPointStruct;

  
    }
}
