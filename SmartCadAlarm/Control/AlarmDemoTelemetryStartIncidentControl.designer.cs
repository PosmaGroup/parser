using Smartmatic.SmartCad.Map;
namespace SmartCadAlarm.Controls
{
    partial class AlarmDemoTelemetryStartIncidentControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mapControlEx1 = new MapControlEx();
            this.toolTipController1 = new DevExpress.Utils.ToolTipController(this.components);
            this.SuspendLayout();
            // 
            // mapControlEx1
            // 
            this.mapControlEx1.Distance = 0;
            this.mapControlEx1.DistanceUnit = null;
            this.mapControlEx1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mapControlEx1.Location = new System.Drawing.Point(0, 0);
            this.mapControlEx1.Margin = new System.Windows.Forms.Padding(4);
            this.mapControlEx1.Name = "mapControlEx1";
            this.mapControlEx1.PostFixTables = null;
            this.mapControlEx1.ShowDistance = false;
            this.mapControlEx1.Size = new System.Drawing.Size(660, 642);
            this.mapControlEx1.TabIndex = 0;
            this.mapControlEx1.ToolInUse = null;
            // 
            // AlarmDemoTelemetryStartIncidentControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.mapControlEx1);
            this.Name = "AlarmDemoTelemetryStartIncidentControl";
            this.Size = new System.Drawing.Size(660, 642);
            this.Load += new System.EventHandler(this.AlarmDemoTelemetryStartIncidentControl_Load);
            this.ResumeLayout(false);

        }

        #endregion

        public MapControlEx mapControlEx1;
        private DevExpress.Utils.ToolTipController toolTipController1;


    }
}
