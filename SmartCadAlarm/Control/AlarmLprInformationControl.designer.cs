
using SmartCadControls.Controls;
namespace SmartCadAlarm.Controls
{
    partial class AlarmLprInformationControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupControlBody = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.textBoxExReason = new TextBoxEx();
            this.pictureBoxNumber = new DevExpress.XtraEditors.PictureEdit();
            this.textBoxExReference = new TextBoxEx();
            this.textBoxExZone = new TextBoxEx();
            this.textBoxExStreet = new TextBoxEx();
            this.textBoxExPlate = new TextBoxEx();
            this.textBoxExStruct = new TextBoxEx();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlReason = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlPlate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemStructName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemStreet = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemZone = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemReference = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlBody)).BeginInit();
            this.groupControlBody.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlPlate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemStructName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemStreet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemZone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemReference)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControlBody
            // 
            this.groupControlBody.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(252)))), ((int)(((byte)(226)))));
            this.groupControlBody.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(252)))), ((int)(((byte)(226)))));
            this.groupControlBody.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(94)))), ((int)(((byte)(50)))));
            this.groupControlBody.Appearance.Options.UseBackColor = true;
            this.groupControlBody.Appearance.Options.UseBorderColor = true;
            this.groupControlBody.AppearanceCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(115)))), ((int)(((byte)(172)))), ((int)(((byte)(68)))));
            this.groupControlBody.AppearanceCaption.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(94)))), ((int)(((byte)(50)))));
            this.groupControlBody.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControlBody.AppearanceCaption.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.groupControlBody.AppearanceCaption.Options.UseBackColor = true;
            this.groupControlBody.AppearanceCaption.Options.UseFont = true;
            this.groupControlBody.Controls.Add(this.layoutControl1);
            this.groupControlBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControlBody.Location = new System.Drawing.Point(0, 0);
            this.groupControlBody.LookAndFeel.SkinName = "Blue";
            this.groupControlBody.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.groupControlBody.LookAndFeel.UseDefaultLookAndFeel = false;
            this.groupControlBody.Name = "groupControlBody";
            this.groupControlBody.Size = new System.Drawing.Size(466, 155);
            this.groupControlBody.TabIndex = 41;
            this.groupControlBody.Text = "groupControlBody";
            // 
            // layoutControl1
            // 
            this.layoutControl1.AllowCustomizationMenu = false;
            this.layoutControl1.BackColor = System.Drawing.Color.White;
            this.layoutControl1.Controls.Add(this.textBoxExReason);
            this.layoutControl1.Controls.Add(this.pictureBoxNumber);
            this.layoutControl1.Controls.Add(this.textBoxExReference);
            this.layoutControl1.Controls.Add(this.textBoxExZone);
            this.layoutControl1.Controls.Add(this.textBoxExStreet);
            this.layoutControl1.Controls.Add(this.textBoxExPlate);
            this.layoutControl1.Controls.Add(this.textBoxExStruct);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 19);
            this.layoutControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.layoutControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(46, 177, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(462, 134);
            this.layoutControl1.TabIndex = 49;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // textBoxExReason
            // 
            this.textBoxExReason.AllowsLetters = true;
            this.textBoxExReason.AllowsNumbers = true;
            this.textBoxExReason.AllowsPunctuation = true;
            this.textBoxExReason.AllowsSeparators = true;
            this.textBoxExReason.AllowsSymbols = true;
            this.textBoxExReason.AllowsWhiteSpaces = true;
            this.textBoxExReason.Enabled = false;
            this.textBoxExReason.ExtraAllowedChars = "";
            this.textBoxExReason.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExReason.Location = new System.Drawing.Point(118, 36);
            this.textBoxExReason.MaxLength = 40;
            this.textBoxExReason.Name = "textBoxExReason";
            this.textBoxExReason.NonAllowedCharacters = "";
            this.textBoxExReason.RegularExpresion = "";
            this.textBoxExReason.Size = new System.Drawing.Size(107, 20);
            this.textBoxExReason.TabIndex = 50;
            // 
            // pictureBoxNumber
            // 
            this.pictureBoxNumber.Location = new System.Drawing.Point(6, 6);
            this.pictureBoxNumber.Name = "pictureBoxNumber";
            this.pictureBoxNumber.Properties.ReadOnly = true;
            this.pictureBoxNumber.Properties.ShowMenu = false;
            this.pictureBoxNumber.Size = new System.Drawing.Size(41, 41);
            this.pictureBoxNumber.StyleController = this.layoutControl1;
            this.pictureBoxNumber.TabIndex = 0;
            // 
            // textBoxExReference
            // 
            this.textBoxExReference.AllowsLetters = true;
            this.textBoxExReference.AllowsNumbers = true;
            this.textBoxExReference.AllowsPunctuation = true;
            this.textBoxExReference.AllowsSeparators = true;
            this.textBoxExReference.AllowsSymbols = true;
            this.textBoxExReference.AllowsWhiteSpaces = true;
            this.textBoxExReference.Enabled = false;
            this.textBoxExReference.ExtraAllowedChars = "";
            this.textBoxExReference.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExReference.Location = new System.Drawing.Point(296, 36);
            this.textBoxExReference.MaxLength = 100;
            this.textBoxExReference.Name = "textBoxExReference";
            this.textBoxExReference.NonAllowedCharacters = "";
            this.textBoxExReference.RegularExpresion = "";
            this.textBoxExReference.Size = new System.Drawing.Size(160, 20);
            this.textBoxExReference.TabIndex = 46;
            // 
            // textBoxExZone
            // 
            this.textBoxExZone.AllowsLetters = true;
            this.textBoxExZone.AllowsNumbers = true;
            this.textBoxExZone.AllowsPunctuation = true;
            this.textBoxExZone.AllowsSeparators = true;
            this.textBoxExZone.AllowsSymbols = true;
            this.textBoxExZone.AllowsWhiteSpaces = true;
            this.textBoxExZone.Enabled = false;
            this.textBoxExZone.ExtraAllowedChars = "";
            this.textBoxExZone.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExZone.Location = new System.Drawing.Point(296, 6);
            this.textBoxExZone.MaxLength = 40;
            this.textBoxExZone.Name = "textBoxExZone";
            this.textBoxExZone.NonAllowedCharacters = "";
            this.textBoxExZone.RegularExpresion = "";
            this.textBoxExZone.Size = new System.Drawing.Size(160, 20);
            this.textBoxExZone.TabIndex = 45;
            // 
            // textBoxExStreet
            // 
            this.textBoxExStreet.AllowsLetters = true;
            this.textBoxExStreet.AllowsNumbers = true;
            this.textBoxExStreet.AllowsPunctuation = true;
            this.textBoxExStreet.AllowsSeparators = true;
            this.textBoxExStreet.AllowsSymbols = true;
            this.textBoxExStreet.AllowsWhiteSpaces = true;
            this.textBoxExStreet.Enabled = false;
            this.textBoxExStreet.ExtraAllowedChars = "";
            this.textBoxExStreet.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExStreet.Location = new System.Drawing.Point(296, 66);
            this.textBoxExStreet.MaxLength = 40;
            this.textBoxExStreet.Name = "textBoxExStreet";
            this.textBoxExStreet.NonAllowedCharacters = "";
            this.textBoxExStreet.RegularExpresion = "";
            this.textBoxExStreet.Size = new System.Drawing.Size(160, 20);
            this.textBoxExStreet.TabIndex = 48;
            // 
            // textBoxExPlate
            // 
            this.textBoxExPlate.AllowsLetters = true;
            this.textBoxExPlate.AllowsNumbers = true;
            this.textBoxExPlate.AllowsPunctuation = true;
            this.textBoxExPlate.AllowsSeparators = true;
            this.textBoxExPlate.AllowsSymbols = true;
            this.textBoxExPlate.AllowsWhiteSpaces = true;
            this.textBoxExPlate.Enabled = false;
            this.textBoxExPlate.ExtraAllowedChars = "";
            this.textBoxExPlate.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExPlate.Location = new System.Drawing.Point(118, 6);
            this.textBoxExPlate.MaxLength = 40;
            this.textBoxExPlate.Name = "textBoxExPlate";
            this.textBoxExPlate.NonAllowedCharacters = "";
            this.textBoxExPlate.RegularExpresion = "";
            this.textBoxExPlate.Size = new System.Drawing.Size(107, 20);
            this.textBoxExPlate.TabIndex = 49;
            // 
            // textBoxExStruct
            // 
            this.textBoxExStruct.AllowsLetters = true;
            this.textBoxExStruct.AllowsNumbers = true;
            this.textBoxExStruct.AllowsPunctuation = true;
            this.textBoxExStruct.AllowsSeparators = true;
            this.textBoxExStruct.AllowsSymbols = true;
            this.textBoxExStruct.AllowsWhiteSpaces = true;
            this.textBoxExStruct.Enabled = false;
            this.textBoxExStruct.ExtraAllowedChars = "";
            this.textBoxExStruct.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExStruct.Location = new System.Drawing.Point(118, 66);
            this.textBoxExStruct.MaxLength = 40;
            this.textBoxExStruct.Name = "textBoxExStruct";
            this.textBoxExStruct.NonAllowedCharacters = "";
            this.textBoxExStruct.RegularExpresion = "";
            this.textBoxExStruct.Size = new System.Drawing.Size(107, 20);
            this.textBoxExStruct.TabIndex = 47;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(252)))), ((int)(((byte)(226)))));
            this.layoutControlGroup1.AppearanceGroup.Options.UseBackColor = true;
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem9,
            this.emptySpaceItem1,
            this.layoutControlReason,
            this.layoutControlPlate,
            this.layoutControlItemStructName,
            this.layoutControlItemStreet,
            this.layoutControlItemZone,
            this.layoutControlItemReference});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.OptionsItemText.TextToControlDistance = 5;
            this.layoutControlGroup1.Size = new System.Drawing.Size(462, 134);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.pictureBoxNumber;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(51, 51);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(51, 51);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(51, 51);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Text = "layoutControlItem9";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextToControlDistance = 0;
            this.layoutControlItem9.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 51);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(51, 81);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlReason
            // 
            this.layoutControlReason.Control = this.textBoxExReason;
            this.layoutControlReason.CustomizationFormText = "layoutControlLpr";
            this.layoutControlReason.Location = new System.Drawing.Point(51, 30);
            this.layoutControlReason.Name = "layoutControlReason";
            this.layoutControlReason.Size = new System.Drawing.Size(178, 30);
            this.layoutControlReason.Text = "Reason";
            this.layoutControlReason.TextSize = new System.Drawing.Size(56, 13);
            this.layoutControlReason.TextToControlDistance = 5;
            // 
            // layoutControlPlate
            // 
            this.layoutControlPlate.Control = this.textBoxExPlate;
            this.layoutControlPlate.CustomizationFormText = "layoutControlItemPlate";
            this.layoutControlPlate.Location = new System.Drawing.Point(51, 0);
            this.layoutControlPlate.Name = "layoutControlPlate";
            this.layoutControlPlate.Size = new System.Drawing.Size(178, 30);
            this.layoutControlPlate.Text = "Plate";
            this.layoutControlPlate.TextSize = new System.Drawing.Size(56, 13);
            this.layoutControlPlate.TextToControlDistance = 5;
            // 
            // layoutControlItemStructName
            // 
            this.layoutControlItemStructName.Control = this.textBoxExStruct;
            this.layoutControlItemStructName.CustomizationFormText = "layoutControlItemStructName";
            this.layoutControlItemStructName.Location = new System.Drawing.Point(51, 60);
            this.layoutControlItemStructName.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItemStructName.MinSize = new System.Drawing.Size(105, 30);
            this.layoutControlItemStructName.Name = "layoutControlItemStructName";
            this.layoutControlItemStructName.Size = new System.Drawing.Size(178, 72);
            this.layoutControlItemStructName.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemStructName.Text = "StructName";
            this.layoutControlItemStructName.TextSize = new System.Drawing.Size(56, 13);
            this.layoutControlItemStructName.TextToControlDistance = 5;
            // 
            // layoutControlItemStreet
            // 
            this.layoutControlItemStreet.Control = this.textBoxExStreet;
            this.layoutControlItemStreet.CustomizationFormText = "Street";
            this.layoutControlItemStreet.Location = new System.Drawing.Point(229, 60);
            this.layoutControlItemStreet.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItemStreet.MinSize = new System.Drawing.Size(100, 30);
            this.layoutControlItemStreet.Name = "layoutControlItemStreet";
            this.layoutControlItemStreet.Size = new System.Drawing.Size(231, 72);
            this.layoutControlItemStreet.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemStreet.Text = "Street";
            this.layoutControlItemStreet.TextSize = new System.Drawing.Size(56, 13);
            this.layoutControlItemStreet.TextToControlDistance = 5;
            // 
            // layoutControlItemZone
            // 
            this.layoutControlItemZone.Control = this.textBoxExZone;
            this.layoutControlItemZone.CustomizationFormText = "layoutControlItemCity";
            this.layoutControlItemZone.Location = new System.Drawing.Point(229, 0);
            this.layoutControlItemZone.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItemZone.MinSize = new System.Drawing.Size(100, 30);
            this.layoutControlItemZone.Name = "layoutControlItemZone";
            this.layoutControlItemZone.Size = new System.Drawing.Size(231, 30);
            this.layoutControlItemZone.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemZone.Text = "Zone";
            this.layoutControlItemZone.TextSize = new System.Drawing.Size(56, 13);
            this.layoutControlItemZone.TextToControlDistance = 5;
            // 
            // layoutControlItemReference
            // 
            this.layoutControlItemReference.Control = this.textBoxExReference;
            this.layoutControlItemReference.CustomizationFormText = "layoutControlItemReference";
            this.layoutControlItemReference.Location = new System.Drawing.Point(229, 30);
            this.layoutControlItemReference.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItemReference.MinSize = new System.Drawing.Size(100, 30);
            this.layoutControlItemReference.Name = "layoutControlItemReference";
            this.layoutControlItemReference.Size = new System.Drawing.Size(231, 30);
            this.layoutControlItemReference.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemReference.Text = "Reference";
            this.layoutControlItemReference.TextSize = new System.Drawing.Size(56, 13);
            this.layoutControlItemReference.TextToControlDistance = 5;
            // 
            // AlarmLprInformationControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupControlBody);
            this.MinimumSize = new System.Drawing.Size(466, 155);
            this.Name = "AlarmLprInformationControl";
            this.Size = new System.Drawing.Size(466, 155);
            ((System.ComponentModel.ISupportInitialize)(this.groupControlBody)).EndInit();
            this.groupControlBody.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlPlate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemStructName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemStreet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemZone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemReference)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TextBoxEx textBoxExStruct;
        private TextBoxEx textBoxExStreet;
        private TextBoxEx textBoxExZone;
		private DevExpress.XtraEditors.GroupControl groupControlBody;
		private DevExpress.XtraEditors.PictureEdit pictureBoxNumber;
		private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemZone;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemStructName;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemStreet;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private TextBoxEx textBoxExReason;
        private TextBoxEx textBoxExPlate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlPlate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlReason;
        private TextBoxEx textBoxExReference;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemReference;
		
    }
}
