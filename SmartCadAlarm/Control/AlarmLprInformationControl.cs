using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using System.Collections;
using SmartCadCore.Common;
using SmartCadCore.ClientData;
using SmartCadAlarm.Enums;
using SmartCadCore.Core;
using SmartCadAlarm.Gui;
using SmartCadAlarm.Util;
using SmartCadCore.Enums;
using SmartCadControls.Controls;



namespace SmartCadAlarm.Controls
{

    public partial class AlarmLprInformationControl: HeaderPanelControl
    {
        #region Fields

       
        private AddressClientData addressData;
        protected CallRingingEventArgs incomingCallEventArgs;
        private StructClientData structClientData;
        private FrontClientStateEnum frontClientState;

        #endregion

        public AlarmLprInformationControl()
        {
            InitializeComponent();
            this.groupControlBody.Text = ResourceLoader.GetString2("LprDataHeaderText");
			LoadLanguage();
        }

		private void LoadLanguage()
		{
            layoutControlItemZone.Text = ResourceLoader.GetString2("City") + ":";
            layoutControlItemStructName.Text = ResourceLoader.GetString2("Struct") + ":";
            layoutControlItemStreet.Text = ResourceLoader.GetString2("StreetName") + ":";
            layoutControlItemReference.Text = ResourceLoader.GetString2("Town") + ":";
            layoutControlPlate.Text = ResourceLoader.GetString2("Plate") + ":";
            layoutControlReason.Text = ResourceLoader.GetString2("RequestReason") + ":";            
            this.Text = ResourceLoader.GetString2("AlarmDataHeaderText");
		}

        #region Properties

        public FrontClientStateEnum FrontClientState
        {
            get
            {
                return this.frontClientState;
            }
            set
            {
                this.frontClientState = value;
                if (value == FrontClientStateEnum.WaitingForIncident)
                    SetFrontClientWaitingForCallState();
                else if (value == FrontClientStateEnum.RegisteringIncident)
                    SetFrontClientRegisteringCallState();
                else if (value == FrontClientStateEnum.UpdatingIncident)
                    SetFrontClientUpdatingCallState();
            }
        }

        public string Plate
        {
            get
            {
                return this.textBoxExPlate.Text;
            }

             set
            {
                this.textBoxExPlate.Text = value;
            }

        }

        public string Reason
        {
            get
            {
                return this.textBoxExReason.Text;
            }

            set
            {
                this.textBoxExReason.Text = value;
            }

        }

        public string Street
        {
            get
            {
                return this.textBoxExStreet.Text;
            }

            set
            {
                this.textBoxExStreet.Text = value;
            }

        }

        public string State
        {
            get
            {
                return this.textBoxExZone.Text;
            }

            set
            {
                this.textBoxExZone.Text = value;
            }

        }

        public string Town
        {
            get
            {
                return this.textBoxExReference.Text;
            }

            set
            {
                this.textBoxExReference.Text = value;
            }

        }  

        public StructClientData StructClientData
        {
            get
            {
                return structClientData;
            }

             set
            {
                if (value != null)
                {
                    this.textBoxExStruct.Text = (value as StructClientData).Name;
                }
                this.structClientData = value;
            }

        }
       

        [RefreshProperties(RefreshProperties.All),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
        Browsable(false)]
        public AddressClientData CallerAddress
        {
            get
            {
                if (null != this.addressData)
                {
                    this.addressData.Zone = this.textBoxExZone.Text;
                    this.addressData.Street = this.textBoxExStreet.Text;
                    this.addressData.Reference = this.textBoxExReference.Text;      
                }
                else
                {
                    this.addressData = new AddressClientData(
                        this.textBoxExZone.Text,
                        this.textBoxExStreet.Text,
                        this.textBoxExReference.Text,    
                        "",
                        false);
                }

                return addressData;
            }
            set
            {
                addressData = value;

                if (null != this.addressData)
                {
                    this.textBoxExZone.Text = this.addressData.Zone;
                    this.textBoxExStreet.Text = this.addressData.Street;
                    this.textBoxExReference.Text = this.addressData.Reference;      
                }
            }
        }

		public override bool Active
		{
			get
			{
				return base.Active;
			}
			set
			{
				base.Active = value;
				Image img;

				img = pictureBoxNumber.Image;

				pictureBoxNumber.Image = ResourceLoader.GetImage(GetNumberImageName());

				if (img != null)
					img.Dispose();
				if (Active)
				{                 
					this.pictureBoxNumber.LookAndFeel.Style = GetStyle(ActiveColor);
					this.groupControlBody.LookAndFeel.Style = GetStyle(ActiveColor);
					this.layoutControl1.LookAndFeel.Style = GetStyle(ActiveColor);
				}
				else
				{
					this.pictureBoxNumber.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Skin;
					this.groupControlBody.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Skin;
					this.layoutControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Skin;
					this.pictureBoxNumber.LookAndFeel.SkinName = InactiveColor;
					this.groupControlBody.LookAndFeel.SkinName = InactiveColor;
					this.layoutControl1.LookAndFeel.SkinName = InactiveColor;
				}
			}
		}

        #endregion        
                  
        public void ChangeSelectedAlarm(LprAlarmGridData alarm)
        {
            Plate = alarm.Plate;
            Reason = alarm.alarmClientData.VehicleRequest.RequestFor;
            State = alarm.StructClientData.State;
            Street = alarm.StructClientData.Street;
            Town = alarm.StructClientData.Town;
            StructClientData = alarm.StructClientData;
        }

        public void CleanControls()
        {
            this.textBoxExPlate.Text = string.Empty;
            this.textBoxExReason.Text = string.Empty;
            this.textBoxExZone.Text = string.Empty;
            this.textBoxExStreet.Text = string.Empty;
            this.textBoxExReference.Text = string.Empty;
            this.textBoxExStruct.Text = string.Empty;
            
        }

        private void SetFrontClientUpdatingCallState()
        {
            this.textBoxExStreet.Enabled = false;
            this.textBoxExStruct.Enabled = false;
        }

        private void SetFrontClientRegisteringCallState()
        {
            //this.textBoxExPlate.Enabled = true;
            //this.textBoxExReason.Enabled = true;
            //this.textBoxExZone.Enabled = true;
            //this.textBoxExStreet.Enabled = true;
            //this.textBoxExReference.Enabled = true;
            //this.textBoxExStruct.Enabled = true;
            this.pictureBoxNumber.Enabled = true;
        }

        private void SetFrontClientWaitingForCallState()
        {
            this.textBoxExPlate.Enabled = false;
            this.textBoxExReason.Enabled = false;
            this.textBoxExZone.Enabled = false;
            this.textBoxExStreet.Enabled = false;
            this.textBoxExReference.Enabled = false;
            this.textBoxExStruct.Enabled = false;
            this.pictureBoxNumber.Enabled = false;
        }                 
  
    }
}
