using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Diagnostics;

using System.Collections;
using SmartCadCore.Common;
using SmartCadCore.ClientData;

using SmartCadCore.Core;
using SmartCadAlarm.Gui;
using Smartmatic.SmartCad.Map;
using Smartmatic.SmartCad.Service;
using SmartCadGuiCommon;
using Smartmatic.SmartCad.Vms;

namespace SmartCadAlarm.Controls
{
    //public delegate void GroupBoxVideoEventHandler(object sender,EventArgs e);
    public partial class AlarmDemoTelemetryStartIncidentControl : DevExpress.XtraEditors.XtraUserControl
    {
        //public MessageBalloon messageBallon;
        private CamerasRibbonForm camerasRibbonForm;

        public AlarmDemoTelemetryStartIncidentControl()
        {
            InitializeComponent();
        }

        private void AlarmDemoTelemetryStartIncidentControl_Load(object sender, EventArgs e)
        {
            InitializeMaps();
            this.mapControlEx1.ShowBallonEvent += new MapControlEx.ShowBallonEventHandler(ShowBallonEvent);
            this.mapControlEx1.FeatureSelected += new MapControlEx.FeatureSelectedEventHandler(Tools_FeatureSelected);
        }

        private void InitializeMaps()
        {
            try
            {
               
                Controls.Remove(mapControlEx1);
                mapControlEx1 = MapControlEx.GetInstance(SmartCadConfiguration.SmartCadSection.ServerElement.Map.Dll);
                Controls.Add(mapControlEx1);
                mapControlEx1.Dock = DockStyle.Fill;

                mapControlEx1.InitializeMaps();
               mapControlEx1.Initialized += new EventHandler(mapControlEx1_Initialized);
            }
            catch (Exception ex)
            {
                MessageForm.Show(ResourceLoader.GetString2("ErrorBeginingMaps"), ex);               
                Process.GetCurrentProcess().Kill();
            }
        }

        void mapControlEx1_Initialized(object sender, EventArgs e)
        {
            mapControlEx1.CreateTable(new MapObjectAlarm(), mapControlEx1.DynTableTelemetryAlarm, true, true, 11, 10);
            mapControlEx1.CreateTable(new MapObjectStruct(), mapControlEx1.DynTablePostName, true, true, 11, 10);
           
            IList list = ServerServiceClient.GetInstance().SearchClientObjects(typeof(StructClientData));
            foreach (StructClientData post in list)
            {
                if (post.CctvZone != null)
                    mapControlEx1.InsertObject(new MapObjectStruct(post));
            }
            mapControlEx1.MakeLayerSelectable(mapControlEx1.DynTablePostName);
            mapControlEx1.EnableLayer(mapControlEx1.DynTableTelemetryAlarm, true);
            mapControlEx1.EnableLayer(mapControlEx1.DynTablePostName, true);

            mapControlEx1.SetLeftButtonTool("Pan");
            mapControlEx1.ruler.Visible = false;
            mapControlEx1.CenterMap(5120);
        }

        private List<StructClientData> GetPostList()
        {
            IList list =
               ServerServiceClient.GetInstance().SearchClientObjects(typeof(StructClientData));
            List<StructClientData> retval = new List<StructClientData>();
            foreach (StructClientData var in list)
            {
                retval.Add(var);
            }

            return retval;
        }

        /// <summary>
        /// Metodo importante...
        /// Cada vez que una persona selecciona algo con la herramienta de seleccionar
        /// entra en este metodo. Es importante que usen el codigo que esta aqui
        /// para iterar o buscar el elemento seleccionado.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Tools_FeatureSelected(object sender, FeatureSelectedEventArgs e)
        {
            //para cambiar el estilo de color de la figura seleccionanda en el mapa.
            //if (messageBallon != null)
            //{
            //    messageBallon.Dispose();
            //    messageBallon.SetVisible = false;

            //}

            //Se redirecciona el evento al mapControlEx pero se le pasa el handler para manejarlo aqui
            mapControlEx1.ToolFeatureSelected(new EventHandler(toolstripmenuitem_Click), e, ref toolTipController1);
        }

        void toolstripmenuitem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;
            ShowBallon((BallonData)item.Tag);
        }

        private void ShowBallonEvent(object sender, BallonData ballon)
        {
            ShowBallon(ballon);
        }

        /// <summary>
        /// Aqui es donde se muestra la informacion.
        /// </summary>
        /// <param name="ballon"></param>
        private void ShowBallon(BallonData ballon)
        {
            if (ballon.Type == mapControlEx1.DynTablePostName)
            {
                #region Structs
                StructClientData structClientData = null;

                IList list = ServerServiceClient.GetInstance().SearchClientObjects(
                    SmartCadHqls.GetCustomHql(SmartCadHqls.GetStructsByCode, ballon.Code));

                if (list != null && list.Count > 0)
                {
                    structClientData = (StructClientData)list[0];
                    System.Drawing.Point point = ballon.Position;
                    System.Windows.Forms.ContextMenu menu = new ContextMenu();

                    IList cameraList = ServerServiceClient.GetInstance().SearchClientObjects(
                    SmartCadHqls.GetCustomHql(SmartCadHqls.GetCamerasByStructName, structClientData.Name));
                    IList sensorList = ServerServiceClient.GetInstance().SearchClientObjects(
                   SmartCadHqls.GetCustomHql(SmartCadHqls.GetSensorTelemetrysByStructName, structClientData.Name));
                    int index;
                    foreach (CameraClientData camera in cameraList)
                    {
                        MenuItem item = new MenuItem();
                        item.Text = camera.Name;
                        index = menu.MenuItems.Add(item);
                        menu.MenuItems[index].Click += new EventHandler(StructCamera_Click);
                    }
                    foreach (DataloggerClientData sensor in sensorList)
                    {
                        MenuItem item = new MenuItem();
                        item.Text = sensor.Name;
                        index = menu.MenuItems.Add(item);
                        //menu.MenuItems[index].Click += new EventHandler(StructCamera_Click);
                    }
                    //menu.Show(mapControl, point);
                    menu.Show(mapControlEx1, point);
                }
                #endregion
            }
        }

        private void StructCamera_Click(object sender, EventArgs e)
        {
            if (camerasRibbonForm == null || camerasRibbonForm.IsDisposed == true)
                camerasRibbonForm = new CamerasRibbonForm();

            camerasRibbonForm.Activate();

            CameraClientData camera = ServerServiceClient.GetInstance().SearchClientObjects(
                      SmartCadHqls.GetCustomHql(
                      SmartCadHqls.GetCamerasByCustomId,
                      (sender as MenuItem).Name)) as CameraClientData;

            CameraCctvAppForm cameraCctvAppForm = new CameraCctvAppForm(camera, PlayModes.PlayBack_SingleSequence);
            cameraCctvAppForm.Text = camera.Name;
            cameraCctvAppForm.CameraType = camera.Type.Name;
            cameraCctvAppForm.Ip = camera.Ip;
            cameraCctvAppForm.Login = camera.Login;
            cameraCctvAppForm.Password = camera.Password;
            cameraCctvAppForm.StreamType = camera.StreamType;
            cameraCctvAppForm.FrameRate = camera.FrameRate;
            cameraCctvAppForm.Resolution = camera.Resolution;
            cameraCctvAppForm.MdiParent = camerasRibbonForm;
            cameraCctvAppForm.MaximizeBox = false;

            camerasRibbonForm.Show();

            if (cameraCctvAppForm.IsDisposed == false)
                cameraCctvAppForm.Show();
            else
                camerasRibbonForm.Close();
        }  

        internal void AddAlarm(int code, double lon, double lat, string name, int severity)
        {
            mapControlEx1.InsertObject(new MapObjectAlarm(code, lon, lat, name, severity));
        }

        internal void UpdateAlarm(int code, double lon, double lat, string name, int severity)
        {
            mapControlEx1.UpdateObject(new MapObjectAlarm(code, lon, lat, name, severity));
        }

        internal void RemoveAlarm(int code)
        {
            mapControlEx1.DeleteObject(new MapObjectAlarm(code,0,0,"",0));
        }

        internal void CenterMap(double lon, double lat)
        {
           
            mapControlEx1.CenterMapInPoint(new GeoPoint(lon,lat));
            mapControlEx1.RefreshMap();
        }

    }
}
