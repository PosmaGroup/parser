﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace SmartCadAlarm.Enums
{
    public enum AlarmClientStateEnum
    {
        WaitingForAlarmIncident,
        RegisteringAlarmIncident
    }
}
