﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadCore.ClientData;

namespace SmartCadAlarm.Gui
{
    public class SensorTelemetryGridData
    {
        public SensorTelemetryGridData(SensorTelemetryThresholdClientData data, string name, string variable, double? value, string low, string high, int dataloggerCustomCode)
        {
            Data = data;
            Name = name;
            Variable = variable;
            Value = value;
            Low = low;
            High = high;
            DataloggerCustomCode = dataloggerCustomCode;
        }

        public SensorTelemetryThresholdClientData Data;

        public string Name { get; set; }

        public string Variable;
        
        public double? Value { get; set; }

        public string Low { get; set; }

        public string High { get; set; }

        public int DataloggerCustomCode;

        public override bool Equals(object obj)
        {
            if (obj is SensorTelemetryGridData)
            {
                SensorTelemetryGridData device = (obj as SensorTelemetryGridData);

                if (device.Name == Name &&
                    device.Variable == Variable &&
                    device.Low == Low &&
                    device.High == High)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }
}
