using Microsoft.Win32;
using SmartCadAlarm.Controls;
using SmartCadAlarm.Enums;
using SmartCadControls;
using SmartCadControls.Controls;
using SmartCadCore.Enums;
using SmartCadGuiCommon.Controls;
using System.IO;

namespace SmartCadAlarm.Gui
{
	partial class AlarmRegisterFormDevX
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private new System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();

                RegistryKey regkey;
               
                try
                {
                    DirectoryInfo source = new DirectoryInfo(SourceDirectory);

                    //Determine whether the source directory exists.
                    if (!source.Exists)
                        regkey = Registry.Users;
                    else
                        regkey = Registry.CurrentUser;

                    // Restore the Print Page Setup properties
                    regkey = regkey.OpenSubKey(@SourceDirectory, true);
                    if (regkey != null)
                    {
                        // Restore the footer values.			
                        regkey.SetValue("footer", IEFooter.ToString());
                    }
                }
                catch
                { }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AlarmRegisterFormDevX));
            this.toolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.saveFileDialogMain = new System.Windows.Forms.SaveFileDialog();
            this.printDialogMain = new System.Windows.Forms.PrintDialog();
            this.contextMenuStripIncidents = new ContextMenuStripEx(this.components);
            this.imageListTabs = new System.Windows.Forms.ImageList(this.components);
            this.departmentsInvolvedControl = new DepartmentsInvolvedControl();
            this.textBoxExIncidentdetails = new SearchableWebBrowser();
            this.questionsCctvControl1 = new QuestionsControlDevX();
            this.layoutControlDefaultCCTVFrontClient = new DevExpress.XtraLayout.LayoutControl();
            this.alarmLprInformationControl1 = new AlarmLprInformationControl();
            this.alarmInformationControl1 = new AlarmInformationControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemQuestionControl = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemDepartmentsInvolved = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            this.layoutControlItemDatalogger = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemLPr = new DevExpress.XtraLayout.LayoutControlItem();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanel1 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.layoutControlDockPanel = new DevExpress.XtraLayout.LayoutControl();
            this.gridControlExIncidentList = new GridControlEx();
            this.gridViewExIncidentList = new GridViewEx();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemIncidentList = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemIncidentDetails = new DevExpress.XtraLayout.LayoutControlItem();
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barButtonItemPrint = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemSave = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemStartRegIncident = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemRegisterIncident = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemCancelIncident = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroupCallInfo = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupGeneralOptions = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlDefaultCCTVFrontClient)).BeginInit();
            this.layoutControlDefaultCCTVFrontClient.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemQuestionControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDepartmentsInvolved)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDatalogger)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLPr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.dockPanel1.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlDockPanel)).BeginInit();
            this.layoutControlDockPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExIncidentList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExIncidentList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemIncidentList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemIncidentDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            this.SuspendLayout();
            // 
            // printDialogMain
            // 
            this.printDialogMain.UseEXDialog = true;
            // 
            // contextMenuStripIncidents
            // 
            this.contextMenuStripIncidents.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.contextMenuStripIncidents.Name = "contextMenuStripIncidents";
            this.contextMenuStripIncidents.Size = new System.Drawing.Size(61, 4);
            this.contextMenuStripIncidents.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStripIncidents_Opening);
            // 
            // imageListTabs
            // 
            this.imageListTabs.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListTabs.ImageStream")));
            this.imageListTabs.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListTabs.Images.SetKeyName(0, "atender_.gif");
            this.imageListTabs.Images.SetKeyName(1, "despachos_.gif");
            this.imageListTabs.Images.SetKeyName(2, "procedimientos_de_primer_ni.gif");
            this.imageListTabs.Images.SetKeyName(3, "procedimientos_por respuesta.gif");
            this.imageListTabs.Images.SetKeyName(4, "procedimientos-genericos.gif");
            // 
            // departmentsInvolvedControl
            // 
            this.departmentsInvolvedControl.FrontClientState = FrontClientStateEnum.WaitingForIncident;
            this.departmentsInvolvedControl.Location = new System.Drawing.Point(12, 632);
            this.departmentsInvolvedControl.Name = "departmentsInvolvedControl";
            this.departmentsInvolvedControl.Size = new System.Drawing.Size(583, 109);
            this.departmentsInvolvedControl.TabIndex = 4;
            // 
            // textBoxExIncidentdetails
            // 
            this.textBoxExIncidentdetails.AllowWebBrowserDrop = false;
            this.textBoxExIncidentdetails.DocumentText = "<HTML></HTML>\0";
            this.textBoxExIncidentdetails.IsWebBrowserContextMenuEnabled = true;
            this.textBoxExIncidentdetails.Location = new System.Drawing.Point(24, 327);
            this.textBoxExIncidentdetails.Margin = new System.Windows.Forms.Padding(0);
            this.textBoxExIncidentdetails.Name = "textBoxExIncidentdetails";
            this.textBoxExIncidentdetails.Size = new System.Drawing.Size(305, 396);
            this.textBoxExIncidentdetails.TabIndex = 5;
            this.textBoxExIncidentdetails.WebBrowserShortcutsEnabled = false;
            this.textBoxExIncidentdetails.XmlText = "<HTML></HTML>\0";
            this.textBoxExIncidentdetails.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.textBoxExIncidentdetails_PreviewKeyDown);
            // 
            // questionsCctvControl1
            // 
            this.questionsCctvControl1.ActiveAnswerTextColor = System.Drawing.Color.Empty;
            this.questionsCctvControl1.ActiveQuestionTextColor = System.Drawing.Color.Empty;
            this.questionsCctvControl1.AskedStatusColor = System.Drawing.Color.Empty;
            this.questionsCctvControl1.AskingStatusColor = System.Drawing.Color.Empty;
            this.questionsCctvControl1.CctvClientState = CctvClientStateEnum.WaitingForCctvIncident;
            this.questionsCctvControl1.DisabledButtonColor = System.Drawing.Color.Empty;
            this.questionsCctvControl1.FrontClientState = FrontClientStateEnum.WaitingForIncident;
            this.questionsCctvControl1.GlobalIncidentTypes = null;
            this.questionsCctvControl1.InactiveAnswerTextColor = System.Drawing.Color.Empty;
            this.questionsCctvControl1.InactiveQuestionTextColor = System.Drawing.Color.Empty;
            this.questionsCctvControl1.IncidentTypeLabelBorderColor = System.Drawing.Color.Empty;
            this.questionsCctvControl1.Location = new System.Drawing.Point(12, 336);
            this.questionsCctvControl1.MinimumSize = new System.Drawing.Size(583, 286);
            this.questionsCctvControl1.Name = "questionsCctvControl1";
            this.questionsCctvControl1.NamePressedButton = null;
            this.questionsCctvControl1.NotAskedStatusColor = System.Drawing.Color.Empty;
            this.questionsCctvControl1.QuestionsWithAnswers = ((System.Collections.IList)(resources.GetObject("questionsCctvControl1.QuestionsWithAnswers")));
            this.questionsCctvControl1.SelectedIncidentTypes = null;
            this.questionsCctvControl1.Size = new System.Drawing.Size(583, 286);
            this.questionsCctvControl1.TabIndex = 13;
            // 
            // layoutControlDefaultCCTVFrontClient
            // 
            this.layoutControlDefaultCCTVFrontClient.AllowCustomizationMenu = false;
            this.layoutControlDefaultCCTVFrontClient.Controls.Add(this.alarmLprInformationControl1);
            this.layoutControlDefaultCCTVFrontClient.Controls.Add(this.questionsCctvControl1);
            this.layoutControlDefaultCCTVFrontClient.Controls.Add(this.departmentsInvolvedControl);
            this.layoutControlDefaultCCTVFrontClient.Controls.Add(this.alarmInformationControl1);
            this.layoutControlDefaultCCTVFrontClient.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlDefaultCCTVFrontClient.Location = new System.Drawing.Point(0, 0);
            this.layoutControlDefaultCCTVFrontClient.LookAndFeel.SkinName = "Blue";
            this.layoutControlDefaultCCTVFrontClient.LookAndFeel.UseDefaultLookAndFeel = false;
            this.layoutControlDefaultCCTVFrontClient.MinimumSize = new System.Drawing.Size(1, 1);
            this.layoutControlDefaultCCTVFrontClient.Name = "layoutControlDefaultCCTVFrontClient";
            this.layoutControlDefaultCCTVFrontClient.Root = this.layoutControlGroup1;
            this.layoutControlDefaultCCTVFrontClient.Size = new System.Drawing.Size(402, 770);
            this.layoutControlDefaultCCTVFrontClient.TabIndex = 43;
            this.layoutControlDefaultCCTVFrontClient.Text = "layoutControlLpr";
            // 
            // alarmLprInformationControl1
            // 
            this.alarmLprInformationControl1.FrontClientState = FrontClientStateEnum.WaitingForIncident;
            this.alarmLprInformationControl1.Location = new System.Drawing.Point(12, 174);
            this.alarmLprInformationControl1.MinimumSize = new System.Drawing.Size(466, 155);
            this.alarmLprInformationControl1.Name = "alarmLprInformationControl1";
            this.alarmLprInformationControl1.Plate = "";
            this.alarmLprInformationControl1.Reason = "";
            this.alarmLprInformationControl1.Size = new System.Drawing.Size(583, 158);
            this.alarmLprInformationControl1.State = "";
            this.alarmLprInformationControl1.Street = "";
            this.alarmLprInformationControl1.StructClientData = null;
            this.alarmLprInformationControl1.TabIndex = 15;
            this.alarmLprInformationControl1.Town = "";
            // 
            // alarmInformationControl1
            // 
            this.alarmInformationControl1.AlarmClientState = AlarmClientStateEnum.WaitingForAlarmIncident;
            this.alarmInformationControl1.DataloggerAlarm = "";
            this.alarmInformationControl1.Location = new System.Drawing.Point(12, 12);
            this.alarmInformationControl1.MinimumSize = new System.Drawing.Size(466, 155);
            this.alarmInformationControl1.Name = "alarmInformationControl1";
            this.alarmInformationControl1.SensorCurrentEvent = "";
            this.alarmInformationControl1.SensorDateAlarm = "";
            this.alarmInformationControl1.SensorName = "";
            this.alarmInformationControl1.SensorState = "";
            this.alarmInformationControl1.SensorStreet = "";
            this.alarmInformationControl1.SensorTown = "";
            this.alarmInformationControl1.Size = new System.Drawing.Size(583, 158);
            this.alarmInformationControl1.StructClientData = null;
            this.alarmInformationControl1.TabIndex = 14;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemQuestionControl,
            this.layoutControlItemDepartmentsInvolved,
            this.splitterItem1,
            this.layoutControlItemDatalogger,
            this.layoutControlItemLPr});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(607, 753);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItemQuestionControl
            // 
            this.layoutControlItemQuestionControl.Control = this.questionsCctvControl1;
            this.layoutControlItemQuestionControl.CustomizationFormText = "layoutControlItemQuestionControl";
            this.layoutControlItemQuestionControl.Location = new System.Drawing.Point(0, 324);
            this.layoutControlItemQuestionControl.MinSize = new System.Drawing.Size(587, 290);
            this.layoutControlItemQuestionControl.Name = "layoutControlItemQuestionControl";
            this.layoutControlItemQuestionControl.Size = new System.Drawing.Size(587, 290);
            this.layoutControlItemQuestionControl.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemQuestionControl.Text = "layoutControlItemQuestionControl";
            this.layoutControlItemQuestionControl.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemQuestionControl.TextToControlDistance = 0;
            this.layoutControlItemQuestionControl.TextVisible = false;
            // 
            // layoutControlItemDepartmentsInvolved
            // 
            this.layoutControlItemDepartmentsInvolved.Control = this.departmentsInvolvedControl;
            this.layoutControlItemDepartmentsInvolved.CustomizationFormText = "layoutControlItemDepartmentsInvolved";
            this.layoutControlItemDepartmentsInvolved.Location = new System.Drawing.Point(0, 620);
            this.layoutControlItemDepartmentsInvolved.Name = "layoutControlItemDepartmentsInvolved";
            this.layoutControlItemDepartmentsInvolved.Size = new System.Drawing.Size(587, 113);
            this.layoutControlItemDepartmentsInvolved.Text = "layoutControlItemDepartmentsInvolved";
            this.layoutControlItemDepartmentsInvolved.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemDepartmentsInvolved.TextToControlDistance = 0;
            this.layoutControlItemDepartmentsInvolved.TextVisible = false;
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            this.splitterItem1.CustomizationFormText = "splitterItem1";
            this.splitterItem1.Location = new System.Drawing.Point(0, 614);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(587, 6);
            // 
            // layoutControlItemDatalogger
            // 
            this.layoutControlItemDatalogger.Control = this.alarmInformationControl1;
            this.layoutControlItemDatalogger.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItemDatalogger.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemDatalogger.Name = "layoutControlItem1";
            this.layoutControlItemDatalogger.Size = new System.Drawing.Size(587, 162);
            this.layoutControlItemDatalogger.Text = "layoutControlItem1";
            this.layoutControlItemDatalogger.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemDatalogger.TextToControlDistance = 0;
            this.layoutControlItemDatalogger.TextVisible = false;
            // 
            // layoutControlItemLPr
            // 
            this.layoutControlItemLPr.Control = this.alarmLprInformationControl1;
            this.layoutControlItemLPr.CustomizationFormText = "layoutControlItemLPr";
            this.layoutControlItemLPr.Location = new System.Drawing.Point(0, 162);
            this.layoutControlItemLPr.Name = "layoutControlItemLPr";
            this.layoutControlItemLPr.Size = new System.Drawing.Size(587, 162);
            this.layoutControlItemLPr.Text = "layoutControlItemLPr";
            this.layoutControlItemLPr.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemLPr.TextToControlDistance = 0;
            this.layoutControlItemLPr.TextVisible = false;
            this.layoutControlItemLPr.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // dockManager1
            // 
            this.dockManager1.Form = this;
            this.dockManager1.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanel1});
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "System.Windows.Forms.StatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            // 
            // dockPanel1
            // 
            this.dockPanel1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.dockPanel1.Appearance.Options.UseBackColor = true;
            this.dockPanel1.Controls.Add(this.dockPanel1_Container);
            this.dockPanel1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Right;
            this.dockPanel1.ID = new System.Guid("dcc1b68c-a8dd-4c08-822e-42893f1d3a8a");
            this.dockPanel1.Location = new System.Drawing.Point(402, 0);
            this.dockPanel1.Name = "dockPanel1";
            this.dockPanel1.Options.AllowDockBottom = false;
            this.dockPanel1.Options.AllowDockFill = false;
            this.dockPanel1.Options.AllowDockLeft = false;
            this.dockPanel1.Options.AllowDockTop = false;
            this.dockPanel1.Options.AllowFloating = false;
            this.dockPanel1.Options.FloatOnDblClick = false;
            this.dockPanel1.Options.ShowCloseButton = false;
            this.dockPanel1.Options.ShowMaximizeButton = false;
            this.dockPanel1.OriginalSize = new System.Drawing.Size(350, 200);
            this.dockPanel1.Size = new System.Drawing.Size(350, 770);
            this.dockPanel1.Text = "dockPanel1";
            this.dockPanel1.DockChanged += new System.EventHandler(this.dockPanel1_DockChanged);
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.layoutControlDockPanel);
            this.dockPanel1_Container.Location = new System.Drawing.Point(3, 29);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(344, 738);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // layoutControlDockPanel
            // 
            this.layoutControlDockPanel.AllowCustomizationMenu = false;
            this.layoutControlDockPanel.Controls.Add(this.gridControlExIncidentList);
            this.layoutControlDockPanel.Controls.Add(this.textBoxExIncidentdetails);
            this.layoutControlDockPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlDockPanel.Location = new System.Drawing.Point(0, 0);
            this.layoutControlDockPanel.Name = "layoutControlDockPanel";
            this.layoutControlDockPanel.Root = this.layoutControlGroup2;
            this.layoutControlDockPanel.Size = new System.Drawing.Size(344, 738);
            this.layoutControlDockPanel.TabIndex = 7;
            this.layoutControlDockPanel.Text = "layoutControl2";
            // 
            // gridControlExIncidentList
            // 
            this.gridControlExIncidentList.EnableAutoFilter = false;
            this.gridControlExIncidentList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExIncidentList.Location = new System.Drawing.Point(24, 45);
            this.gridControlExIncidentList.MainView = this.gridViewExIncidentList;
            this.gridControlExIncidentList.Name = "gridControlExIncidentList";
            this.gridControlExIncidentList.Size = new System.Drawing.Size(296, 233);
            this.gridControlExIncidentList.TabIndex = 19;
            this.gridControlExIncidentList.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExIncidentList});
            this.gridControlExIncidentList.ViewTotalRows = false;
            // 
            // gridViewExIncidentList
            // 
            this.gridViewExIncidentList.AllowFocusedRowChanged = true;
            this.gridViewExIncidentList.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewExIncidentList.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExIncidentList.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewExIncidentList.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewExIncidentList.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewExIncidentList.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExIncidentList.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewExIncidentList.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewExIncidentList.EnablePreviewLineForFocusedRow = false;
            this.gridViewExIncidentList.GridControl = this.gridControlExIncidentList;
            this.gridViewExIncidentList.Name = "gridViewExIncidentList";
            this.gridViewExIncidentList.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExIncidentList.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewExIncidentList.ViewTotalRows = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3,
            this.layoutControlGroup4});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.Size = new System.Drawing.Size(344, 738);
            this.layoutControlGroup2.Text = "Root";
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroup3.ExpandButtonMode = DevExpress.Utils.Controls.ExpandButtonMode.Inverted;
            this.layoutControlGroup3.ExpandButtonVisible = true;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemIncidentList});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(324, 282);
            this.layoutControlGroup3.Text = "layoutControlGroup3";
            // 
            // layoutControlItemIncidentList
            // 
            this.layoutControlItemIncidentList.Control = this.gridControlExIncidentList;
            this.layoutControlItemIncidentList.CustomizationFormText = "layoutControlItemIncidentList";
            this.layoutControlItemIncidentList.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemIncidentList.Name = "layoutControlItemIncidentList";
            this.layoutControlItemIncidentList.Size = new System.Drawing.Size(300, 237);
            this.layoutControlItemIncidentList.Text = "layoutControlItemIncidentList";
            this.layoutControlItemIncidentList.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemIncidentList.TextToControlDistance = 0;
            this.layoutControlItemIncidentList.TextVisible = false;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "layoutControlGroup4";
            this.layoutControlGroup4.ExpandButtonMode = DevExpress.Utils.Controls.ExpandButtonMode.Inverted;
            this.layoutControlGroup4.ExpandButtonVisible = true;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemIncidentDetails});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 282);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Padding = new DevExpress.XtraLayout.Utils.Padding(9, 0, 9, 0);
            this.layoutControlGroup4.Size = new System.Drawing.Size(324, 436);
            this.layoutControlGroup4.Text = "layoutControlGroup4";
            // 
            // layoutControlItemIncidentDetails
            // 
            this.layoutControlItemIncidentDetails.Control = this.textBoxExIncidentdetails;
            this.layoutControlItemIncidentDetails.CustomizationFormText = "layoutControlItemIncidentDetails";
            this.layoutControlItemIncidentDetails.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemIncidentDetails.Name = "layoutControlItemIncidentDetails";
            this.layoutControlItemIncidentDetails.Size = new System.Drawing.Size(309, 400);
            this.layoutControlItemIncidentDetails.Text = "layoutControlItemIncidentDetails";
            this.layoutControlItemIncidentDetails.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemIncidentDetails.TextToControlDistance = 0;
            this.layoutControlItemIncidentDetails.TextVisible = false;
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.Dock = System.Windows.Forms.DockStyle.None;
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.ExpandCollapseItem.Name = "";
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.barButtonItemPrint,
            this.barButtonItemSave,
            this.barButtonItemRefresh,
            this.barButtonItemStartRegIncident,
            this.barButtonItemRegisterIncident,
            this.barButtonItemCancelIncident});
            this.ribbonControl1.Location = new System.Drawing.Point(12, 44);
            this.ribbonControl1.MaxItemId = 18;
            this.ribbonControl1.MdiMergeStyle = DevExpress.XtraBars.Ribbon.RibbonMdiMergeStyle.Always;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbonControl1.ShowToolbarCustomizeItem = false;
            this.ribbonControl1.Size = new System.Drawing.Size(521, 119);
            this.ribbonControl1.Toolbar.ShowCustomizeItem = false;
            this.ribbonControl1.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            // 
            // barButtonItemPrint
            // 
            this.barButtonItemPrint.Caption = "ItemPrint";
            this.barButtonItemPrint.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPrint.Glyph")));
            this.barButtonItemPrint.Id = 6;
            this.barButtonItemPrint.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemPrint.Name = "barButtonItemPrint";
            this.barButtonItemPrint.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemPrint_ItemClick);
            // 
            // barButtonItemSave
            // 
            this.barButtonItemSave.Caption = "ItemSave";
            this.barButtonItemSave.Glyph = global::SmartCadAlarm.Properties.ResourcesGui.SaveIcon;
            this.barButtonItemSave.Id = 7;
            this.barButtonItemSave.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemSave.Name = "barButtonItemSave";
            this.barButtonItemSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemSave_ItemClick);
            // 
            // barButtonItemRefresh
            // 
            this.barButtonItemRefresh.Caption = "ItemRefresh";
            this.barButtonItemRefresh.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemRefresh.Glyph")));
            this.barButtonItemRefresh.Id = 8;
            this.barButtonItemRefresh.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemRefresh.Name = "barButtonItemRefresh";
            this.barButtonItemRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemRefresh_ItemClick);
            // 
            // barButtonItemStartRegIncident
            // 
            this.barButtonItemStartRegIncident.Caption = "ItemStartRegIncident";
            this.barButtonItemStartRegIncident.Enabled = false;
            this.barButtonItemStartRegIncident.Id = 12;
            this.barButtonItemStartRegIncident.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemStartRegIncident.Name = "barButtonItemStartRegIncident";
            this.barButtonItemStartRegIncident.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItemStartRegIncident.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemStartRegIncident_ItemClick);
            // 
            // barButtonItemRegisterIncident
            // 
            this.barButtonItemRegisterIncident.Caption = "ItemCreateNewIncident";
            this.barButtonItemRegisterIncident.Enabled = false;
            this.barButtonItemRegisterIncident.Id = 13;
            this.barButtonItemRegisterIncident.LargeGlyph = global::SmartCadAlarm.Properties.ResourcesGui.Registrar;
            this.barButtonItemRegisterIncident.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemRegisterIncident.Name = "barButtonItemRegisterIncident";
            this.barButtonItemRegisterIncident.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItemRegisterIncident.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemRegisterIncident_ItemClick);
            // 
            // barButtonItemCancelIncident
            // 
            this.barButtonItemCancelIncident.Caption = "ItemCancelIncident";
            this.barButtonItemCancelIncident.Id = 15;
            this.barButtonItemCancelIncident.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemCancelIncident.LargeGlyph")));
            this.barButtonItemCancelIncident.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemCancelIncident.Name = "barButtonItemCancelIncident";
            this.barButtonItemCancelIncident.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItemCancelIncident.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemCancelIncident_ItemClick);
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupCallInfo,
            this.ribbonPageGroupGeneralOptions});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "ribbonPage1";
            // 
            // ribbonPageGroupCallInfo
            // 
            this.ribbonPageGroupCallInfo.AllowMinimize = false;
            this.ribbonPageGroupCallInfo.AllowTextClipping = false;
            this.ribbonPageGroupCallInfo.ItemLinks.Add(this.barButtonItemStartRegIncident);
            this.ribbonPageGroupCallInfo.ItemLinks.Add(this.barButtonItemRegisterIncident, true);
            this.ribbonPageGroupCallInfo.ItemLinks.Add(this.barButtonItemCancelIncident);
            this.ribbonPageGroupCallInfo.Name = "ribbonPageGroupCallInfo";
            this.ribbonPageGroupCallInfo.ShowCaptionButton = false;
            this.ribbonPageGroupCallInfo.Text = "ribbonPageGroupCallInfo";
            // 
            // ribbonPageGroupGeneralOptions
            // 
            this.ribbonPageGroupGeneralOptions.AllowMinimize = false;
            this.ribbonPageGroupGeneralOptions.AllowTextClipping = false;
            this.ribbonPageGroupGeneralOptions.ItemLinks.Add(this.barButtonItemPrint);
            this.ribbonPageGroupGeneralOptions.ItemLinks.Add(this.barButtonItemSave);
            this.ribbonPageGroupGeneralOptions.ItemLinks.Add(this.barButtonItemRefresh);
            this.ribbonPageGroupGeneralOptions.Name = "ribbonPageGroupGeneralOptions";
            this.ribbonPageGroupGeneralOptions.ShowCaptionButton = false;
            this.ribbonPageGroupGeneralOptions.Text = "ribbonPageGroupGeneralOptions";
            // 
            // AlarmRegisterFormDevX
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(752, 770);
            this.ControlBox = false;
            this.Controls.Add(this.layoutControlDefaultCCTVFrontClient);
            this.Controls.Add(this.dockPanel1);
            this.Controls.Add(this.ribbonControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.LookAndFeel.SkinName = "Blue";
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AlarmRegisterFormDevX";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AlarmRegisterFormDevX_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.AlarmFrontClientForm_FormClosed);
            this.Load += new System.EventHandler(this.AlarmFrontClientForm_Load);
            this.Enter += new System.EventHandler(this.AlarmRegisterFormDevX_Enter);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlDefaultCCTVFrontClient)).EndInit();
            this.layoutControlDefaultCCTVFrontClient.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemQuestionControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDepartmentsInvolved)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDatalogger)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLPr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.dockPanel1.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlDockPanel)).EndInit();
            this.layoutControlDockPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExIncidentList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExIncidentList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemIncidentList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemIncidentDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            this.ResumeLayout(false);

        }

  
        #endregion

        private System.Windows.Forms.ToolTip toolTipMain;
        private System.Windows.Forms.SaveFileDialog saveFileDialogMain;
        private System.Windows.Forms.PrintDialog printDialogMain;
        private System.Windows.Forms.ImageList imageListTabs;
        private DepartmentsInvolvedControl departmentsInvolvedControl;
        private ContextMenuStripEx contextMenuStripIncidents;
        private SearchableWebBrowser textBoxExIncidentdetails;
        private QuestionsControlDevX questionsCctvControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControlDefaultCCTVFrontClient;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
		private DevExpress.XtraBars.Docking.DockManager dockManager1;
		private DevExpress.XtraBars.Docking.DockPanel dockPanel1;
		private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
		private DevExpress.XtraLayout.LayoutControl layoutControlDockPanel;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemIncidentDetails;
		private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
		private DevExpress.XtraBars.BarButtonItem barButtonItemPrint;
		private DevExpress.XtraBars.BarButtonItem barButtonItemSave;
		private DevExpress.XtraBars.BarButtonItem barButtonItemRefresh;
		public DevExpress.XtraBars.BarButtonItem barButtonItemStartRegIncident;
		public DevExpress.XtraBars.BarButtonItem barButtonItemRegisterIncident;
		private DevExpress.XtraBars.BarButtonItem barButtonItemCancelIncident;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemQuestionControl;
		private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupCallInfo;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupGeneralOptions;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDepartmentsInvolved;
        private DevExpress.XtraLayout.SplitterItem splitterItem1;
		private GridControlEx gridControlExIncidentList;
		private GridViewEx gridViewExIncidentList;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemIncidentList;
        internal AlarmInformationControl alarmInformationControl1;
        internal DevExpress.XtraLayout.LayoutControlItem layoutControlItemDatalogger;
        internal AlarmLprInformationControl alarmLprInformationControl1;
        internal DevExpress.XtraLayout.LayoutControlItem layoutControlItemLPr; 
    }
}
