using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Globalization;
using System.Drawing.Printing;
using System.Threading;
using System.Diagnostics;
using System.Reflection;

using System.Xml;
using System.Xml.Xsl;
using System.Net;
using System.ServiceModel;
using Microsoft.Win32;
using DevExpress.XtraBars;
using SmartCadCore.ClientData;
using SmartCadCore.Core;
using SmartCadCore.Common;

using SmartCadAlarm.Controls;

using SmartCadAlarm.Util;
using SmartCadAlarm.Enums;
using SmartCadAlarm.Util.SyncBoxes;
using SmartCadAlarm.Util.SyncBoxes.Alarm;
using Smartmatic.SmartCad.Service;
using SmartCadControls.Controls;
using SmartCadCore.Enums;
using SmartCadControls;
using SmartCadGuiCommon.Controls;
using SmartCadGuiCommon.Enums;
using SmartCadControls.SyncBoxes;
//using SmartCadGuiCommon.Services;
using SmartCadControls.Filters;
using SmartCadGuiCommon.Util;
using SmartCadGuiCommon.Services;

namespace SmartCadAlarm.Gui
{

   

    

    public partial class AlarmRegisterFormDevX : DevExpress.XtraEditors.XtraForm
    {
        #region Fields
        private ServerServiceClient serverServiceClient;
        private AlarmReportClientData globalDataloggerAlarmReport;
        private AlarmLprReportClientData globalLprAlarmReport;
        private bool isActiveCall = false;
        private bool incidentAdded = false;
        private bool callEntering = false;
        private ImageList activeStepImages;
        private ImageList inactiveStepImages;
        private FilterBase incidentsCurrentFilter;
        private ToolStripMenuItem toolStripMenuItem;
        private SkinEngine skinEngine;
        private System.Threading.Timer answersUpdatedTimer;
        private EventArgs answersUpdatedTimerEventArgs;
        private Dictionary<int, IncidentTypeClientData> globalIncidentTypes;
        private Dictionary<int, DepartmentTypeClientData> globalDepartmentTypes;
        private Dictionary<string, IncidentNotificationPriorityClientData> globalNotificationPriorities;
        private IList globalIncidents;
        private Dictionary<string, ApplicationPreferenceClientData> globalApplicationPreference;
        private object syncAssociatedInfo;
        private IList updatedAnswers = new ArrayList();
        private XslCompiledTransform xslCompiledTransform;
        private ConfigurationClientData configurationClient;
        private GridControlSynBox incidentSyncBox;
        private NetworkCredential networkCredential;
        private int selectedIncidentCode = -1;
        private int selectedAlarmSensorTelemetryCode = -1;
        private System.Threading.Timer refreshIncidentReport;
        public string password;
        private object IEFooter;
        private string objectValue;
        public string ExtNumber;
        private const string SourceDirectory = "Software\\Microsoft\\Internet Explorer\\PageSetup";
        private bool searchByText = false;
        private bool popUpShow = true;
        private object sync = new object();
        private AlarmTypes alarmType = AlarmTypes.Datalogger;
        private ArrayList IncidentChanges = new ArrayList();
        private ArrayList QuestionChanges = new ArrayList();
        private ArrayList DepartmentTypeChanges = new ArrayList();
        private AlarmClientStateEnum alarmClientState;
        private Dictionary<int, IncidentTypeClientData> incidentTypeActionsUpdate;
        private Dictionary<int, IncidentTypeClientData> incidentTypeActionDelete;
        private Dictionary<int, IncidentTypeClientData> incidentTypeActionAdd;
        private Dictionary<int, QuestionClientData> questionActionsUpdate;
        private Dictionary<int, QuestionClientData> questionActionDelete;
        private Dictionary<int, QuestionClientData> questionActionAdd;
        #endregion

        #region Properties

        public AlarmClientStateEnum AlarmClientState
        {
            get
            {
                return this.alarmClientState;
            }
            set
            {
                this.alarmClientState = value;
                if (value == AlarmClientStateEnum.WaitingForAlarmIncident)
                    SetFrontClientWaitingForCallState();
                else if (value == AlarmClientStateEnum.RegisteringAlarmIncident)
                    SetFrontClientRegisteringCallState();
            }
        }

        public bool IsActiveCall
        {
            get
            {
                return this.isActiveCall;
            }
            set
            {
                this.isActiveCall = value;
            }
        }

        public IList GlobalIncidents
        {
            get
            {
                return globalIncidents;
            }
            set
            {
                globalIncidents = value;
            }
        }

        public AlarmReportClientData GlobalDataloggerAlarmReport
        {
            get
            {
                return globalDataloggerAlarmReport;
            }
            set
            {
                globalDataloggerAlarmReport = value;
            }
        }


        public AlarmLprReportClientData GlobalLprAlarmReport
        {
            get
            {
                return globalLprAlarmReport;
            }
            set
            {
                globalLprAlarmReport = value;
            }
        }


        object syncGlobalIncidentTypes = new object();
        public Dictionary<int, IncidentTypeClientData> GlobalIncidentTypes
        {
            get
            {
                lock (syncGlobalIncidentTypes)
                {
                    return globalIncidentTypes;
                }
            }
            set
            {
                lock (sync)
                {
                    globalIncidentTypes = value;

                    foreach (IncidentTypeClientData itcd in incidentTypeActionAdd.Values)
                    {
                        if (globalIncidentTypes.ContainsKey(itcd.Code) == false)
                            globalIncidentTypes.Add(itcd.Code, itcd);
                    }
                    incidentTypeActionAdd.Clear();

                    foreach (IncidentTypeClientData itcd in incidentTypeActionsUpdate.Values)
                    {
                        if (globalIncidentTypes.ContainsKey(itcd.Code) == true)
                            globalIncidentTypes[itcd.Code] = itcd;
                    }
                    incidentTypeActionsUpdate.Clear();

                    foreach (IncidentTypeClientData itcd in incidentTypeActionDelete.Values)
                    {
                        if (globalIncidentTypes.ContainsKey(itcd.Code) == true)
                            globalIncidentTypes.Remove(itcd.Code);
                    }
                    incidentTypeActionDelete.Clear();

                    foreach (QuestionClientData qcd in questionActionAdd.Values)
                    {
                        AddQuestion(qcd);
                    }
                    questionActionAdd.Clear();

                    foreach (QuestionClientData qcd in questionActionsUpdate.Values)
                    {
                        UpdateQuestion(qcd);
                    }
                    questionActionsUpdate.Clear();

                    foreach (QuestionClientData qcd in questionActionDelete.Values)
                    {
                        DeleteQuestion(qcd);
                    }
                    questionActionDelete.Clear();

                    questionsCctvControl1.GlobalIncidentTypes = globalIncidentTypes;
                }
            }
        }

        public Dictionary<int, DepartmentTypeClientData> GlobalDepartmentTypes
        {
            get
            {
                return this.globalDepartmentTypes;
            }
            set
            {
                this.globalDepartmentTypes = value;
                this.departmentsInvolvedControl.departmentTypesControl.GlobalDepartmentTypes = globalDepartmentTypes;
            }
        }

        public Dictionary<string, IncidentNotificationPriorityClientData> GlobalNotificationPriorities
        {
            get
            {
                return this.globalNotificationPriorities;
            }
            set
            {

                this.globalNotificationPriorities = value;
                this.departmentsInvolvedControl.departmentTypesControl.GlobalNotificationPriorities = globalNotificationPriorities;
            }
        }

        public Dictionary<string, ApplicationPreferenceClientData> GlobalApplicationPreference
        {
            get
            {
                return globalApplicationPreference;
            }
            set
            {
                globalApplicationPreference = value;
            }
        }

        public ServerServiceClient ServerServiceClient
        {
            get
            {
                return serverServiceClient;
            }
            set
            {
                serverServiceClient = value;
            }
        }


        internal QuestionsControlDevX QuestionsCctvControl1
        {
            get
            {
                return this.questionsCctvControl1;
            }
        }

        public NetworkCredential NetworkCredential
        {
            get
            {
                return networkCredential;
            }

            set
            {
                networkCredential = value;
            }
        }

        public ConfigurationClientData ConfigurationClient
        {
            get
            {
                return configurationClient;
            }
            set
            {
                configurationClient = value;
            }
        }

        public void SetPassword(string passwd)
        {
            this.password = passwd;
        }

        public AlarmTypes AlarmType 
        {
            get
            {
                return alarmType;
            }
        }

        public AlarmSensorTelemetryClientData Alarm { get; set; }

        #endregion

        #region Constructors

        public AlarmRegisterFormDevX()
        {
            InitializeComponent();
            globalIncidents = new ArrayList();
            globalDepartmentTypes = new Dictionary<int, DepartmentTypeClientData>();
            globalNotificationPriorities = new Dictionary<string, IncidentNotificationPriorityClientData>();
            globalIncidentTypes = new Dictionary<int, IncidentTypeClientData>();
            syncAssociatedInfo = new object();
            refreshIncidentReport = new System.Threading.Timer(new TimerCallback(OnRefreshIncidentReportTimer));

            incidentTypeActionsUpdate = new Dictionary<int, IncidentTypeClientData>();
            incidentTypeActionDelete = new Dictionary<int, IncidentTypeClientData>();
            incidentTypeActionAdd = new Dictionary<int, IncidentTypeClientData>();
            questionActionsUpdate = new Dictionary<int, QuestionClientData>();
            questionActionDelete = new Dictionary<int, QuestionClientData>();
            questionActionAdd = new Dictionary<int, QuestionClientData>();

            ServerServiceClient.GetInstance().CommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(serverService_CommittedChanges);
            configurationClient = ServerServiceClient.GetInstance().GetConfiguration();
            LoadLanguage();
        }

        #endregion

        public void LoadInitialData(IList incidentList)
        {
            #region Loading Incident Types and questions
            IList clientIncidentTypes = ServerServiceClient.GetInstance().SearchClientObjectsMaxRows(
                SmartCadHqls.GetCustomHql(
                SmartCadHqls.IncidentTypesWithDepartmentTypesQuestions, UserApplicationClientData.Alarm.Code), true);
            foreach (IncidentTypeClientData incidentTypeClient in clientIncidentTypes)
            {
                if (globalIncidentTypes.ContainsKey(incidentTypeClient.Code))
                {
                    if (globalIncidentTypes[incidentTypeClient.Code].Version < incidentTypeClient.Version)
                    {
                        globalIncidentTypes[incidentTypeClient.Code] = incidentTypeClient;
                    }
                }
                else
                {
                    globalIncidentTypes.Add(incidentTypeClient.Code, incidentTypeClient);
                }
            }
            this.GlobalIncidentTypes = globalIncidentTypes;
            #endregion

            #region Loading DepartmentTypes
            IList tempDepartmentTypes = ServerServiceClient.GetInstance().SearchClientObjects(
                     SmartCadHqls.GetDepartmentsTypeWithStationsAssociated, true);

            foreach (DepartmentTypeClientData departmentType in tempDepartmentTypes)
            {
                if (globalDepartmentTypes.ContainsKey(departmentType.Code))
                {
                    if (globalDepartmentTypes[departmentType.Code].Version < departmentType.Version)
                    {
                        globalDepartmentTypes[departmentType.Code] = departmentType;
                    }
                }
                else
                {
                    globalDepartmentTypes.Add(departmentType.Code, departmentType);
                    
                }
            }
            this.GlobalDepartmentTypes = globalDepartmentTypes;
            #endregion

            #region Loading IncidentNotificationPriorities
            IList tempNotificationPriorities = ServerServiceClient.GetInstance().SearchClientObjectsMaxRows(SmartCadHqls.GetIncidentNotificationPriorities, true);
            foreach (IncidentNotificationPriorityClientData notificationPriority in tempNotificationPriorities)
            {
                if (globalNotificationPriorities.ContainsKey(notificationPriority.CustomCode))
                {
                    if (globalNotificationPriorities[notificationPriority.CustomCode].Version < notificationPriority.Version)
                    {
                        globalNotificationPriorities[notificationPriority.CustomCode] = notificationPriority;
                    }
                }
                else
                {
                    globalNotificationPriorities.Add(notificationPriority.CustomCode, notificationPriority);
                }
            }
            GlobalNotificationPriorities = globalNotificationPriorities;
            #endregion

            #region Loading ApplicationPreferences
            GlobalApplicationPreference = new Dictionary<string, ApplicationPreferenceClientData>();
            foreach (ApplicationPreferenceClientData preference in
                ServerServiceClient.GetInstance().SearchClientObjectsMaxRows(
                SmartCadHqls.GetAllApplicationPreferences, true))
            {
                GlobalApplicationPreference.Add(preference.Name, preference);
            }
            #endregion

            #region Loading Incidents

            int index = -1;
            foreach (IncidentClientData incidentClientData in incidentList)
            {
                if (incidentClientData.SourceIncidentApp == SourceIncidentAppEnum.Alarm)
                {
                    index = globalIncidents.IndexOf(incidentClientData);
                    if (index != -1 && (globalIncidents[index] as IncidentClientData).Version < incidentClientData.Version)
                    {
                        globalIncidents[index] = incidentClientData;
                    }
                    else if (index == -1)
                    {
                        if (IsLpr() && incidentClientData.AlarmReports[0] is AlarmLprReportClientData)
                        {
                            globalIncidents.Add(incidentClientData);
                        }
                        else if (IsDatalogger() && incidentClientData.AlarmReports[0] is AlarmReportClientData)
                        {
                            globalIncidents.Add(incidentClientData);
                        }
                    }
                }
            }
            this.GlobalIncidents = globalIncidents;

            #endregion
        }

        private void UpdateIncidentListData()
        {
            IList incidentList = ServerServiceClient.GetInstance().SearchClientObjectsMaxRows(
                SmartCadHqls.GetCustomHql(
                SmartCadHqls.NewIncidentRule, UserApplicationClientData.Alarm.Code), true);

            int index = -1;
            globalIncidents = new ArrayList();
            foreach (IncidentClientData incidentClientData in incidentList)
            {
                if (incidentClientData.SourceIncidentApp == SourceIncidentAppEnum.Alarm)
                {
                    index = globalIncidents.IndexOf(incidentClientData);
                    if (index != -1 && (globalIncidents[index] as IncidentClientData).Version < incidentClientData.Version)
                    {
                        globalIncidents[index] = incidentClientData;
                    }
                    else if (index == -1)
                    {
                        if (IsLpr() && incidentClientData.AlarmReports[0] is AlarmLprReportClientData)
                        {
                            globalIncidents.Add(incidentClientData);
                        }
                        else if (IsDatalogger() && incidentClientData.AlarmReports[0] is AlarmReportClientData)
                        {
                            globalIncidents.Add(incidentClientData);
                        }
                    }
                }
            }
            gridControlExIncidentList.DataSource = globalIncidents;
            incidentSyncBox.Sync(globalIncidents);
        }

        public void LoadDataloggerAlarmData(DefaultDataloggerFrontClientXtraForm.AlarmSensorTelemetryGridData rowSelected)
        {
            Alarm = rowSelected.Alarm;
            alarmInformationControl1.SensorName = rowSelected.Sensor.Name;
            alarmInformationControl1.SensorDateAlarm = rowSelected.Date.ToString();
            alarmInformationControl1.DataloggerAlarm = rowSelected.Sensor.Datalogger.Name;
            alarmInformationControl1.SensorState = rowSelected.Sensor.Datalogger.StructClientData.State;
            alarmInformationControl1.SensorStreet = rowSelected.Sensor.Datalogger.StructClientData.Street;
            alarmInformationControl1.SensorTown = rowSelected.Sensor.Datalogger.StructClientData.Town;
            alarmInformationControl1.StructClientData = rowSelected.Sensor.Datalogger.StructClientData;
            alarmInformationControl1.SensorCurrentEvent = rowSelected.Sensor.Variable;
            selectedAlarmSensorTelemetryCode = rowSelected.Tag.Code;
        }


        public void LoadLprAlarmData(LprAlarmGridData rowSelected)
        {
            alarmLprInformationControl1.Reason = rowSelected.Lpr;
            alarmLprInformationControl1.Plate = rowSelected.Plate;
            alarmLprInformationControl1.State = rowSelected.State;
            alarmLprInformationControl1.Street = rowSelected.Street;
            alarmLprInformationControl1.Town = rowSelected.Town;
            alarmLprInformationControl1.StructClientData = rowSelected.StructClientData;
        }

        private void AlarmFrontClientForm_Load(object sender, EventArgs e)
        {
            SetSkin();

            XmlReaderSettings xmlReaderSettings = new XmlReaderSettings();
            xmlReaderSettings.ProhibitDtd = false;

            xslCompiledTransform = new XslCompiledTransform();
            xslCompiledTransform.Load(XmlReader.Create(SmartCadConfiguration.AlarmIncidentXslt, xmlReaderSettings));

            departmentsInvolvedControl.departmentTypesControl.Left =
                (departmentsInvolvedControl.Size.Width - departmentsInvolvedControl.departmentTypesControl.Size.Width) / 2;

            //Subscribing to events needed.
            questionsCctvControl1.AnswerUpdated += new EventHandler<EventArgs>(questionsControl_AnswerUpdated);

            questionsCctvControl1.IncidentTypesSelected += new EventHandler<EventArgs>(questionsCctvControl1_IncidentTypeSelected);
            this.departmentsInvolvedControl.departmentTypesControl.DepartmentTypesSelected += new EventHandler<EventArgs>(departmentTypesControl2_DepartmentTypesSelected);
            questionsCctvControl1.Enter += new EventHandler(questionsCctvControl1_Enter);
            this.departmentsInvolvedControl.Enter += new EventHandler(departmentsInvolvedControl_Enter);
            alarmInformationControl1.Enter += new EventHandler(callReceiverControl1_Enter);
            alarmLprInformationControl1.Enter += new EventHandler(alarmLprInformationControl1_Enter);
            KeyDown += new KeyEventHandler(AlarmFrontClientForm_KeyDown);


            //Filling needed information...
            LoadStepImages();

            //Setting up the FrontClient and Nortel Configuration...
            this.AlarmClientState = AlarmClientStateEnum.RegisteringAlarmIncident;

            ArrayList parameters = new ArrayList();
            parameters.Add("OPEN");

            configurationClient = ServerServiceClient.GetInstance().GetConfiguration();
            answersUpdatedTimer = new System.Threading.Timer(new TimerCallback(OnAnswerUpdateTimer));
            CleanForm(false);
            questionsCctvControl1.removeIncident += new QuestionsControlDevX.RemoveIncident(questionsCctvControl1_removeIncident);
            FillAlarmZones();
            departmentsInvolvedControl.departmentTypesControl.gridViewExDepartmentType.ClearSelection();
            LoadLanguage();
            questionsCctvControl1.clientMode = FrontClientMode.Alarm;

            gridControlExIncidentList.Type = typeof(GridIncidentAlarmData);
            incidentSyncBox = new GridControlSynBox(gridControlExIncidentList);
            incidentSyncBox.Sync(globalIncidents);
            gridControlExIncidentList.ViewTotalRows = true;
            gridControlExIncidentList.EnableAutoFilter = true;
            gridViewExIncidentList.OptionsView.ShowAutoFilterRow = true;
            gridViewExIncidentList.ViewTotalRows = true;
            gridControlExIncidentList.AllowDrop = false;
            gridControlExIncidentList.CellToolTipNeeded += new DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventHandler(gridControlExIncidentList_CellToolTipNeeded);
            gridViewExIncidentList.SelectionWillChange += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(gridViewEx1_SelectionWillChange);
            gridViewExIncidentList.SingleSelectionChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(gridViewExIncidentList_SingleSelectionChanged);


            if (alarmType == AlarmTypes.LPR)
            {
                gridViewExIncidentList.Columns["Datalogger"].Visible = false;
                gridViewExIncidentList.Columns["Sensor"].Visible = false;
                gridViewExIncidentList.Columns["Variable"].Visible = false;
            }
            else if (alarmType == AlarmTypes.Datalogger)
            {
                gridViewExIncidentList.Columns["Lpr"].Visible = false;
            }

            //barButtonItemStartRegIncident.LargeGlyph = ResourceLoader.GetImage("StartRegistry");

            AlarmClientState = AlarmClientStateEnum.WaitingForAlarmIncident;
        }

        void alarmLprInformationControl1_Enter(object sender, EventArgs e)
        {
            if (this.AlarmClientState != AlarmClientStateEnum.WaitingForAlarmIncident)
            {
                ChangeCurrentStep(1, false);
            }
        }

        void departmentsInvolvedControl_Enter(object sender, EventArgs e)
        {
            if (this.AlarmClientState != AlarmClientStateEnum.WaitingForAlarmIncident)
            {
                ChangeCurrentStep(3, false);
            }
        }


        void gridControlExIncidentList_CellToolTipNeeded(object sender, DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventArgs e)
        {
            if (e.Info == null)
            {
                DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo info = ((GridViewEx)gridControlExIncidentList.MainView).CalcHitInfo(e.ControlMousePosition);
                if (info.RowHandle > -1)
                {
                    e.Info = new DevExpress.Utils.ToolTipControlInfo();
                    e.Info.Object = gridControlExIncidentList;

                    if (this.gridViewExIncidentList.GetRow(info.RowHandle) != null)
                    {
                        IncidentClientData incidentData = (IncidentClientData)((GridIncidentAlarmData)((IList)gridControlExIncidentList.DataSource)[info.RowHandle]).Tag;

                        string toolTipText = "";

                        if (incidentData != null)
                        {
                            foreach (IncidentTypeClientData incidentType in incidentData.IncidentTypes)
                            {
                                if (incidentType.NoEmergency != true)
                                    toolTipText = toolTipText + incidentType.FriendlyName + ", ";
                            }
                        }
                        if (toolTipText.Trim() != "")
                        {
                            toolTipText = toolTipText.Substring(0, toolTipText.Length - 2);
                        }
                        e.Info.Text = toolTipText;
                    }
                }
            }
        }

        private void LoadLanguage()
        {
            barButtonItemPrint.Caption = ResourceLoader.GetString2("Print");
            barButtonItemRefresh.Caption = ResourceLoader.GetString2("Refresh");
            barButtonItemSave.Caption = ResourceLoader.GetString2("Save");
            barButtonItemCancelIncident.Caption = ResourceLoader.GetString2("Cancel");
            barButtonItemRegisterIncident.Caption = ResourceLoader.GetString2("Register");
           // barButtonItemStartRegIncident.Caption = ResourceLoader.GetString2("New");
            barButtonItemStartRegIncident.Visibility = BarItemVisibility.Never;
            ribbonPageGroupCallInfo.Text = ResourceLoader.GetString2("Incidents");
            ribbonPageGroupGeneralOptions.Text = ResourceLoader.GetString2("GeneralOptions");
            ribbonPage1.Text = ResourceLoader.GetString2("Alarm");
            layoutControlGroup3.Text = ResourceLoader.GetString2("IncidentList");
            layoutControlGroup4.Text = ResourceLoader.GetString2("IncidentDetails");
            dockPanel1.Text = ResourceLoader.GetString2("HeaderRegIncidents");
            this.Text = ResourceLoader.GetString2("Incidents");
        }
        private void FillAlarmZones()
        {
            //foreach (AlarmZoneClientData alarmZone in
            //    ServerServiceClient.GetInstance().SearchClientObjects(typeof(AlarmZoneClientData)))           
            //{
            //    this.alarmInformationControl1.CcvtZone = alarmZone;
            //}
        }



        private void questionsCctvControl1_removeIncident(IList selectedIncidentTypes)
        {

            IncidentTypeDepartmentTypeClientData department;
            IncidentTypeClientData incidentType;
            ArrayList updateIncident = new ArrayList();

            if (IsDatalogger())
            {
                foreach (IncidentTypeClientData incident in selectedIncidentTypes)
                {
                    if (GlobalDataloggerAlarmReport.IncidentTypesCodes != null)
                        GlobalDataloggerAlarmReport.IncidentTypesCodes.Remove(incident.Code);

                }
                for (int j = 0; j < departmentsInvolvedControl.departmentTypesControl.gridViewExDepartmentType.RowCount; j++)
                {
                    if (departmentsInvolvedControl.departmentTypesControl.gridViewExDepartmentType.GetRowCellValue(j, "Prioridad") == null)
                        departmentsInvolvedControl.departmentTypesControl.gridViewExDepartmentType.SetRowCellValue(j, " ", false);
                }
                if (globalDataloggerAlarmReport.IncidentTypesCodes != null)
                {
                    foreach (int incidentCode in GlobalDataloggerAlarmReport.IncidentTypesCodes)
                    {
                        for (int k = 0; k < this.questionsCctvControl1.SelectedIncidentTypes.Count; k++)
                        {
                            incidentType = (IncidentTypeClientData)this.questionsCctvControl1.SelectedIncidentTypes[k];
                            if (incidentType.Code == incidentCode)
                                updateIncident.Add(incidentType);
                        }
                    }
                }
            }
            else if (IsLpr())
            {
                foreach (IncidentTypeClientData incident in selectedIncidentTypes)
                {
                    if (GlobalLprAlarmReport.IncidentTypesCodes != null)
                        GlobalLprAlarmReport.IncidentTypesCodes.Remove(incident.Code);

                }
                for (int j = 0; j < departmentsInvolvedControl.departmentTypesControl.gridViewExDepartmentType.RowCount; j++)
                {
                    if (departmentsInvolvedControl.departmentTypesControl.gridViewExDepartmentType.GetRowCellValue(j, "Prioridad") == null)
                        departmentsInvolvedControl.departmentTypesControl.gridViewExDepartmentType.SetRowCellValue(j, " ", false);
                }
                if (globalLprAlarmReport.IncidentTypesCodes != null)
                {
                    foreach (int incidentCode in GlobalLprAlarmReport.IncidentTypesCodes)
                    {
                        for (int k = 0; k < this.questionsCctvControl1.SelectedIncidentTypes.Count; k++)
                        {
                            incidentType = (IncidentTypeClientData)this.questionsCctvControl1.SelectedIncidentTypes[k];
                            if (incidentType.Code == incidentCode)
                                updateIncident.Add(incidentType);
                        }
                    }
                }
            }
            foreach (IncidentTypeClientData incident in updateIncident)
            {
                for (int i = 0; i < incident.IncidentTypeDepartmentTypeClients.Count; i++)
                {
                    department = (IncidentTypeDepartmentTypeClientData)incident.IncidentTypeDepartmentTypeClients[i];
                    for (int j = 0; j < departmentsInvolvedControl.departmentTypesControl.gridViewExDepartmentType.RowCount; j++)
                    {
                        if (department.DepartmentType.Code == (((GridDepartmentTypeData)((IList)departmentsInvolvedControl.departmentTypesControl.gridControlExDepartmentType.DataSource)[j]).Tag as DepartmentTypeClientData).Code)
                        {
                            departmentsInvolvedControl.departmentTypesControl.gridViewExDepartmentType.SetRowCellValue(j, " ", true);
                        }
                    }
                }
            }
            this.questionsCctvControl1.UpdateTextBoxSelectedIncidentTypes(updateIncident);
        }
        private void gridViewEx1_SelectionWillChange(object sender, EventArgs e)
        {

            barButtonItemSave.Enabled = false;
            barButtonItemPrint.Enabled = false;
            barButtonItemRefresh.Enabled = false;

            if (textBoxExIncidentdetails.IsDisposed == false)
                textBoxExIncidentdetails.DocumentText = "<span style='HEIGHT:100%;FONT-SIZE: 10pt; FONT-FAMILY: Sans-Serif'>" + ResourceLoader.GetString2("NoIncidentSelected") + "</span>";

            FormUtil.InvokeRequired(this,
               delegate
               {
                   this.layoutControlGroup4.Text = ResourceLoader.GetString2("IncidentDetails");
               });
        }
        private void OnRefreshIncidentReport()
        {
            refreshIncidentReport.Change(1000, Timeout.Infinite);
        }
        private void OnRefreshIncidentReportTimer(object state)
        {
            refreshIncidentReport.Change(Timeout.Infinite, Timeout.Infinite);

            ThreadPool.QueueUserWorkItem(delegate
            {
                try
                {
                    RefreshIncidentReport();
                }
                catch (Exception ex)
                {
                    SmartLogger.Print(ex);
                }
            });
        }
        private void OnAnswerUpdateTimer(object state)
        {
            answersUpdatedTimer.Change(Timeout.Infinite, Timeout.Infinite);
        }

        private void AlarmFrontClientForm_KeyDown(object sender, KeyEventArgs e)
        {
            //SHORTCUTS...
            if (e.Control == true && !(e.Modifiers == (Keys.Control | Keys.Alt)))
            {
                if ((e.KeyValue == 49 || e.KeyCode == Keys.NumPad1) && this.alarmClientState != AlarmClientStateEnum.WaitingForAlarmIncident)//GOES TO STEP ONE(1)...
                {
                    ChangeCurrentStep(1, true);
                }
                else if ((e.KeyValue == 50 || e.KeyCode == Keys.NumPad2) && this.alarmClientState != AlarmClientStateEnum.WaitingForAlarmIncident)//GOES TO STEP TWO(2)...
                {
                    ChangeCurrentStep(2, true);
                }
                else if ((e.KeyValue == 51 || e.KeyCode == Keys.NumPad3) && this.alarmClientState != AlarmClientStateEnum.WaitingForAlarmIncident)//GOES TO STEP THREE(3)...
                {
                    ChangeCurrentStep(3, true);
                }
                else if ((e.KeyValue == 52 || e.KeyCode == Keys.NumPad4) && this.alarmClientState != AlarmClientStateEnum.WaitingForAlarmIncident)//GOES TO STEP FOUR(4)...
                {
                    ChangeCurrentStep(4, true);
                }
                else if (e.KeyCode == Keys.A && this.alarmClientState == AlarmClientStateEnum.RegisteringAlarmIncident)//TO CHECK OR UNCHECK THE IS ANONIMOUS CALL CHECBOX
                {

                }
                else if ((e.KeyCode == Keys.Enter || e.KeyCode == Keys.Space))
                {
                    e.SuppressKeyPress = true;
                }
            }

            else if (e.KeyCode == Keys.F1)//CALLS ONLINE HELP...
            {
                ((AlarmFrontClientFormDevX)ParentForm).barButtonItemHelp_ItemClick(null, null);
            }

            else if (e.KeyCode == Keys.F6)//SETS THE SIMILAR INCIDENT FILTER...
            {

            }
            else if (e.KeyCode == Keys.F7)//SETS THE SAME NUMBER INCIDENT FILTER...
            {

            }
            else if (e.KeyCode == Keys.Enter && e.Shift == false && e.Control == false && e.Alt == false)
            {
                if (this.questionsCctvControl1.ContainsFocus && this.questionsCctvControl1.Active && !this.questionsCctvControl1.IsPanelIncidentTypeActive)
                {
                    this.questionsCctvControl1.SetNextQuestion();
                }
            }
            else if (e.KeyCode == Keys.F8)//SETS THE OPEN INCIDENTS FILTER...
            {

            }
            else if (e.KeyCode == Keys.F9)//CREATE A NEW INCIDENT WITH A NEW PHONE REPORTS...
            {
                if (AlarmClientState != AlarmClientStateEnum.WaitingForAlarmIncident)
                    barButtonItemRegisterIncident_ItemClick(null, null);
            }
            else if (sender == null && e.Alt == true && !(e.Modifiers == (Keys.Control | Keys.Alt))
               && e.KeyCode == Keys.F4 && this.Disposing == false)
            {
                this.Close();
            }
        }
        public void callReceiverControl1_Enter(object sender, EventArgs e)
        {
            if (this.AlarmClientState != AlarmClientStateEnum.WaitingForAlarmIncident)
            {
                ChangeCurrentStep(1, false);
            }
        }

        private void questionsCctvControl1_Enter(object sender, EventArgs e)
        {
            if (this.AlarmClientState != AlarmClientStateEnum.WaitingForAlarmIncident)
            {
                ChangeCurrentStep(2, false);
            }
        }
        private void LoadStepImages()
        {
            this.inactiveStepImages = new ImageList();
            this.activeStepImages = new ImageList();
            string imageName = "$Image.";
            for (int i = 1; i < 5; i++)
            {
                Image image = ResourceLoader.GetImage(imageName + i + "Encendido");
                if (activeStepImages.ImageSize.Height != image.Size.Height ||
                    activeStepImages.ImageSize.Width != image.Size.Width)
                {
                    activeStepImages.ImageSize = image.Size;
                }
                activeStepImages.Images.Add(image);
            }
            for (int i = 1; i < 5; i++)
            {
                Image image = ResourceLoader.GetImage(imageName + i + "Apagado");
                if (inactiveStepImages.ImageSize.Height != image.Size.Height ||
                    inactiveStepImages.ImageSize.Width != image.Size.Width)
                {
                    inactiveStepImages.ImageSize = image.Size;
                }
                inactiveStepImages.Images.Add(image);
            }
        }
        private void departmentTypesControl2_DepartmentTypesSelected(object sender, EventArgs e)
        {
            DepartmentTypesControl control = (DepartmentTypesControl)sender;
            if (IsDatalogger())
            {
                if (this.GlobalDataloggerAlarmReport != null)
                    this.GlobalDataloggerAlarmReport.ReportBaseDepartmentTypesClient = control.SelectedDepartmentTypesWithPriority;
            }
            else if (IsLpr())
            {
                if (this.GlobalLprAlarmReport != null)
                    this.GlobalLprAlarmReport.ReportBaseDepartmentTypesClient = control.SelectedDepartmentTypesWithPriority;
            }
        }
        private bool ValidateQuestions(RequirementTypeClientEnum requirementType)
        {
            //returns true if there are invalid questions...
            bool toReturn = false;

            if (IsDatalogger())
            {
                if (GlobalDataloggerAlarmReport.IncidentTypesCodes != null)
                {
                    int index = 0;
                    while (toReturn != true && index < GlobalDataloggerAlarmReport.IncidentTypesCodes.Count)
                    {
                        IncidentTypeClientData incidentType = globalIncidentTypes[(int)GlobalDataloggerAlarmReport.IncidentTypesCodes[index]];
                        foreach (IncidentTypeQuestionClientData incidentQuestion in incidentType.IncidentTypeQuestions)
                        {
                            if (incidentQuestion.Question != null)
                            {
                                if (incidentQuestion.Question.RequirementType == requirementType)
                                {
                                    int questionIndex = 0;
                                    bool found = false;
                                    while ((questionIndex < this.questionsCctvControl1.QuestionsWithAnswers.Count) && (found != true))
                                    {
                                        if (IsDatalogger())
                                        {
                                            if (((ReportAnswerClientData)this.questionsCctvControl1.QuestionsWithAnswers[questionIndex]).QuestionCode.Equals(incidentQuestion.Question.Code) == true)
                                            {
                                                found = true;
                                            }
                                        }
                                        else if (IsLpr())
                                        {
                                            if (((AlarmLprReportAnswerClientData)this.questionsCctvControl1.QuestionsWithAnswers[questionIndex]).QuestionCode.Equals(incidentQuestion.Question.Code) == true)
                                            {
                                                found = true;
                                            }
                                        }
                                        questionIndex++;
                                    }
                                    if (found != true)
                                    {
                                        // toReturn = true;
                                    }
                                }
                            }
                        }
                        index++;
                    }
                }
            }
            else if (IsLpr())
            {
                if (GlobalLprAlarmReport.IncidentTypesCodes != null)
                {
                    int index = 0;
                    while (toReturn != true && index < GlobalLprAlarmReport.IncidentTypesCodes.Count)
                    {
                        IncidentTypeClientData incidentType = globalIncidentTypes[(int)GlobalLprAlarmReport.IncidentTypesCodes[index]];
                        foreach (IncidentTypeQuestionClientData incidentQuestion in incidentType.IncidentTypeQuestions)
                        {
                            if (incidentQuestion.Question != null)
                            {
                                if (incidentQuestion.Question.RequirementType == requirementType)
                                {
                                    int questionIndex = 0;
                                    bool found = false;
                                    while ((questionIndex < this.questionsCctvControl1.QuestionsWithAnswers.Count) && (found != true))
                                    {
                                        if (IsDatalogger())
                                        {
                                            if (((ReportAnswerClientData)this.questionsCctvControl1.QuestionsWithAnswers[questionIndex]).QuestionCode.Equals(incidentQuestion.Question.Code) == true)
                                            {
                                                found = true;
                                            }
                                        }
                                        else if (IsLpr())
                                        {
                                            if (((AlarmLprReportAnswerClientData)this.questionsCctvControl1.QuestionsWithAnswers[questionIndex]).QuestionCode.Equals(incidentQuestion.Question.Code) == true)
                                            {
                                                found = true;
                                            }
                                        }
                                        questionIndex++;

                                    }
                                    if (found != true)
                                    {
                                        // toReturn = true;
                                    }
                                }
                            }
                        }
                        index++;
                    }
                }
            }
            return toReturn;
        }
        private bool ValidateRequiredQuestions()
        {
            return ValidateQuestions(RequirementTypeClientEnum.Required);
        }
        private bool ValidateIncidentTypes()
        {
            if (IsDatalogger())
            {
                if (GlobalDataloggerAlarmReport.IncidentTypesCodes != null && GlobalDataloggerAlarmReport.IncidentTypesCodes.Count > 0)
                    return false;
                else
                    return true;
            }
            else if (IsLpr())
            {
                if (GlobalLprAlarmReport.IncidentTypesCodes != null && GlobalLprAlarmReport.IncidentTypesCodes.Count > 0)
                    return false;
                else
                    return true;
            }
            else
            {

                return false;
            }
        }
        private bool ValidateDepartmentTypes()
        {
            bool toReturn = false;
            if (IsDatalogger())
            {
                if (GlobalDataloggerAlarmReport.ReportBaseDepartmentTypesClient != null)
                {
                    int index = 0;
                    while (toReturn != true && index < GlobalDataloggerAlarmReport.ReportBaseDepartmentTypesClient.Count)
                    {
                        ReportBaseDepartmentTypeClientData alarmReportDepartmentType =
                            GlobalDataloggerAlarmReport.ReportBaseDepartmentTypesClient[index] as ReportBaseDepartmentTypeClientData;
                        if (alarmReportDepartmentType.PriorityCode == 0)
                        {
                            toReturn = true;
                        }
                        index++;
                    }
                }
                else
                {
                    GlobalDataloggerAlarmReport.ReportBaseDepartmentTypesClient = new ArrayList();
                }
            }
            else if (IsLpr())
            {
                if (GlobalLprAlarmReport.ReportBaseDepartmentTypesClient != null)
                {
                    int index = 0;
                    while (toReturn != true && index < GlobalLprAlarmReport.ReportBaseDepartmentTypesClient.Count)
                    {
                        ReportBaseDepartmentTypeClientData alarmReportDepartmentType =
                            GlobalLprAlarmReport.ReportBaseDepartmentTypesClient[index] as ReportBaseDepartmentTypeClientData;
                        if (alarmReportDepartmentType.PriorityCode == 0)
                        {
                            toReturn = true;
                        }
                        index++;
                    }
                }
                else
                {
                    GlobalLprAlarmReport.ReportBaseDepartmentTypesClient = new ArrayList();
                }
            }
            return toReturn;
        }
        private void UpdateQuestionsCctvControl1(ArrayList Changes)
        {
            if ((Changes != null) && (Changes.Count > 0))
            {
                if (Changes[0] is IncidentTypeClientData)
                {
                    this.questionsCctvControl1.UpdateIncidentTClientData(Changes);
                }
                else if (Changes[0] is QuestionClientData)
                {
                    this.questionsCctvControl1.UpdateIncidentQuestions(Changes);
                }
            }
        }
        private void UpdatedDepartmentInvolvedControl()
        {
            for (int i = 0; i < this.departmentsInvolvedControl.departmentTypesControl.gridViewExDepartmentType.RowCount; i++)
                if ((this.departmentsInvolvedControl.departmentTypesControl.gridViewExDepartmentType.GetRowCellValue(i, "Prioridad") == null) ||
                    (this.departmentsInvolvedControl.departmentTypesControl.gridViewExDepartmentType.GetRowCellValue(i, "Prioridad").ToString() == string.Empty))
                    this.departmentsInvolvedControl.departmentTypesControl.gridViewExDepartmentType.SetRowCellValue(i, " ", false);
        }
        private bool ValidateInterface()
        {
            bool toReturn = true;

            if (ValidateChangedIncidentype() == false)
            {
                if (ValidateIncidentTypes() == false)
                {
                    DialogResult result = MessageForm.Show(ResourceLoader.GetString2("DeletedOrModifiedIncidentTipes"), MessageFormType.Question);
                    if (result == DialogResult.No)
                    {
                        popUpShow = false;
                        this.UpdateQuestionsCctvControl1(IncidentChanges);
                        UpdatedDepartmentInvolvedControl();
                        this.departmentsInvolvedControl.departmentTypesControl.IncidentTypes = IncidentChanges;
                        IncidentChanges.Clear();
                        toReturn = false;
                        this.questionsCctvControl1.SetFrontClientChangedIncidentTypeState();
                        this.questionsCctvControl1.Focus();
                    }
                }
                else
                {
                    MessageForm.Show(ResourceLoader.GetString2("DeletedIncidentTypes"), MessageFormType.Information);
                    toReturn = false;
                    this.questionsCctvControl1.SetFrontClientChangedIncidentTypeState();
                    this.questionsCctvControl1.Focus();
                }
            }
            else if (ValidateChangedQuestions() == false)
            {
                DialogResult result = MessageForm.Show(ResourceLoader.GetString2("UpdatedRequiredQuestionsWishToContinue"), MessageFormType.Question);
                if (result == DialogResult.No)
                {
                    this.UpdateQuestionsCctvControl1(QuestionChanges);
                    QuestionChanges.Clear();
                    toReturn = false;
                    this.questionsCctvControl1.Focus();
                }
            }
            else if (ValidateRecommendedChangedQuestions() == false)
            {
                DialogResult result = MessageForm.Show(ResourceLoader.GetString2("UpdatedRequiredQuestions"), MessageFormType.Information);
                toReturn = false;
                this.questionsCctvControl1.PressedButtonOk();
                this.questionsCctvControl1.Focus();
            }
            else if (ValidateAddedQuestions() == false)
            {
                DialogResult result = MessageForm.Show(ResourceLoader.GetString2("NewRequiredQuestions"), MessageFormType.Information);
                toReturn = false;
                this.questionsCctvControl1.PressedButtonOk();
                this.questionsCctvControl1.Focus();
            }

            else if (ValidateDepartmentTypeChanges() == false)
            {
                if (ValidateDepartmentTypes() == false)
                {
                    DialogResult dialog = MessageForm.Show(ResourceLoader.GetString2("DepartmentTypeUpdated"), MessageFormType.Question);
                    if (dialog == DialogResult.No)
                    {
                        popUpShow = false;
                        toReturn = false;
                    }
                    else if (GetSelectedDepartment().Count() == 0)
                    {
                        MessageForm.Show(ResourceLoader.GetString2("DepartmentTypeUpdatedCheckSelection"), MessageFormType.Error);
                        popUpShow = false;
                        toReturn = false;
                    }
                }
            }
            if (toReturn == true)
            {
                if (ValidateIncidentTypes() == true)
                {
                    toReturn = false;
                    MessageForm.Show(ResourceLoader.GetString2("AlarmReportWithoutIncidentType"), MessageFormType.Error);
                    this.questionsCctvControl1.SetFrontClientChangedIncidentTypeState();
                    this.questionsCctvControl1.Focus();

                }
                else if (ValidateRequiredQuestions() == true)
                {
                    toReturn = false;
                    MessageForm.Show(ResourceLoader.GetString2("AlarmRemainingNotAnsweredRequiredQuestions"), MessageFormType.Error);
                    this.questionsCctvControl1.Focus();
                    this.questionsCctvControl1.SelectNextNotAskedQuestion(new VistaButtonEx());
                }
                else if (ValidateDepartmentTypes() == true)
                {
                    toReturn = false;
                    MessageForm.Show(ResourceLoader.GetString2("AlarmDepartmentsWithoutPriority"), MessageFormType.Error);

                    this.departmentsInvolvedControl.departmentTypesControl.SetFocus("radioButtonExPriority1");
                }

            }
            if ((questionActionDelete.Count > 0) && (toReturn == true))
            {
                questionsCctvControl1.ShowPanelSelected();
            }
            return toReturn;
        }

        private bool ValidateDepartmentTypeChanges()
        {
            bool result = true;
            foreach (DepartmentTypeClientData item in DepartmentTypeChanges)
            {
                departmentsInvolvedControl.departmentTypesControl.DeleteDepartmentWithPriority(item);
                departmentsInvolvedControl.departmentTypesControl.gridControlExDepartmentType.BeginInit();
                departmentsInvolvedControl.departmentTypesControl.gridControlExDepartmentType.DeleteItem(new GridDepartmentTypeData(item));
                departmentsInvolvedControl.departmentTypesControl.gridControlExDepartmentType.EndInit();
                result = false;
            }
            DepartmentTypeChanges.Clear();
            return result;
        }



        private bool ValidateChangedQuestions()
        {
            bool validate = true;
            if (IsDatalogger())
            {
                if (GlobalDataloggerAlarmReport.Answers != null)
                {
                    for (int i = 0; i < GlobalDataloggerAlarmReport.Answers.Count; i++)
                    {
                        ReportAnswerClientData pracd = GlobalDataloggerAlarmReport.Answers[i] as ReportAnswerClientData;
                        if (questionActionDelete.ContainsKey(pracd.QuestionCode) == true)
                        {
                            if (validate == true)
                                validate = false;

                            QuestionClientData deletedQuestion = questionActionDelete[pracd.QuestionCode];
                            GlobalDataloggerAlarmReport.Answers.RemoveAt(i);
                            questionsCctvControl1.DeleteQuestion(deletedQuestion);
                            i--;
                        }
                        else
                        {
                            if (questionActionsUpdate.ContainsKey(pracd.QuestionCode) == true)
                            {
                                QuestionClientData updatedQuestion = questionActionsUpdate[pracd.QuestionCode];

                                if (validate == true)
                                    validate = false;

                                questionsCctvControl1.UpdateQuestion(updatedQuestion);
                            }
                        }
                    }
                }
            }
            else if (IsLpr())
            {
                if (GlobalLprAlarmReport.Answers != null)
                {
                    for (int i = 0; i < GlobalLprAlarmReport.Answers.Count; i++)
                    {
                        AlarmLprReportAnswerClientData pracd = GlobalLprAlarmReport.Answers[i] as AlarmLprReportAnswerClientData;
                        if (questionActionDelete.ContainsKey(pracd.QuestionCode) == true)
                        {
                            if (validate == true)
                                validate = false;

                            QuestionClientData deletedQuestion = questionActionDelete[pracd.QuestionCode];
                            GlobalDataloggerAlarmReport.Answers.RemoveAt(i);
                            questionsCctvControl1.DeleteQuestion(deletedQuestion);
                            i--;
                        }
                        else
                        {
                            if (questionActionsUpdate.ContainsKey(pracd.QuestionCode) == true)
                            {
                                QuestionClientData updatedQuestion = questionActionsUpdate[pracd.QuestionCode];

                                if (validate == true)
                                    validate = false;

                                questionsCctvControl1.UpdateQuestion(updatedQuestion);
                            }
                        }
                    }
                }
            }
            return validate;
        }

        private bool ValidateRecommendedChangedQuestions()
        {
            bool validate = true;
            foreach (QuestionClientData qcd in questionActionsUpdate.Values)
            {
                foreach (IncidentTypeQuestionClientData itqcd in qcd.IncidentTypes)
                {
                    for (int i = 0; i < questionsCctvControl1.SelectedIncidentTypes.Count; i++)
                    {
                        IncidentTypeClientData incidentTypeClientData = questionsCctvControl1.SelectedIncidentTypes[i] as IncidentTypeClientData;
                        if (incidentTypeClientData.Code == itqcd.IncidentType.Code)
                        {
                            if (validate == true)
                            {
                                validate = false;
                                break;
                            }

                            questionsCctvControl1.UpdateQuestion(qcd);
                        }
                    }
                    if (validate == false)
                        break;
                }
                if (validate == false)
                    break;
            }
            if (validate == false)
            {
                questionActionsUpdate.Clear();
            }
            return validate;
        }

        private bool ValidateAddedQuestions()
        {
            bool validate = true;
            foreach (QuestionClientData qcd in questionActionAdd.Values)
            {
                foreach (IncidentTypeQuestionClientData itqcd in qcd.IncidentTypes)
                {
                    for (int i = 0; i < questionsCctvControl1.SelectedIncidentTypes.Count; i++)
                    {
                        IncidentTypeClientData incidentTypeClientData = questionsCctvControl1.SelectedIncidentTypes[i] as IncidentTypeClientData;
                        if (incidentTypeClientData.Code == itqcd.IncidentType.Code)
                        {
                            if (validate == true)
                            {
                                validate = false;
                                break;
                            }
                        }
                    }
                    if (validate == false)
                        break;
                }
                if (validate == false)
                    break;
            }
            if (validate == false)
                questionActionAdd.Clear();
            return validate;
        }

        private bool ValidateChangedIncidentype()
        {
            bool validate = true;
            for (int i = 0; i < questionsCctvControl1.SelectedIncidentTypes.Count; i++)
            {
                IncidentTypeClientData incidentTypeClientData = questionsCctvControl1.SelectedIncidentTypes[i] as IncidentTypeClientData;
                if (GlobalIncidentTypes.ContainsKey(incidentTypeClientData.Code) == false)
                {
                    if (validate == true)
                        validate = false;

                    questionsCctvControl1.DeleteIncidentType(incidentTypeClientData);
                    CleanIncidentTypeInformationFromGlobalAlarmReport(incidentTypeClientData);
                }
                else
                {
                    IncidentTypeClientData updatedIncidentTypeClientData = GlobalIncidentTypes[incidentTypeClientData.Code] as IncidentTypeClientData;
                    if ((updatedIncidentTypeClientData.Version > incidentTypeClientData.Version) && (popUpShow == true))
                    {
                        if (validate == true)
                            validate = false;

                        questionsCctvControl1.UpdateIncidentType(updatedIncidentTypeClientData);
                    }
                }
            }
            return validate;
        }

        private void CleanIncidentTypeInformationFromGlobalAlarmReport(IncidentTypeClientData incidentTypeClientData)
        {
            //If the IncidentType has been deleted while using it. Before we save the phone report we need to 
            // delete the incident report and its answers from it.

            if (IsDatalogger())
            {
                GlobalDataloggerAlarmReport.IncidentTypesCodes.Remove(incidentTypeClientData.Code);
                for (int k = 0; GlobalDataloggerAlarmReport.Answers != null && k < GlobalDataloggerAlarmReport.Answers.Count; k++)
                {
                    ReportAnswerClientData pracd = GlobalDataloggerAlarmReport.Answers[k] as ReportAnswerClientData;
                    bool deleteAnswer = true;

                    foreach (IncidentTypeClientData itcd in questionsCctvControl1.SelectedIncidentTypes)
                    {
                        foreach (IncidentTypeQuestionClientData prqt in itcd.IncidentTypeQuestions)
                        {
                            if (prqt.Question.Code == pracd.QuestionCode)
                            {
                                deleteAnswer = false;
                                break;
                            }
                        }
                        if (deleteAnswer == false)
                            break;
                    }

                    if (deleteAnswer == true)
                    {
                        GlobalDataloggerAlarmReport.Answers.RemoveAt(k);
                        k--;
                    }
                }
            }
            else if (IsLpr())
            {
                GlobalLprAlarmReport.IncidentTypesCodes.Remove(incidentTypeClientData.Code);
                for (int k = 0; GlobalLprAlarmReport.Answers != null && k < GlobalLprAlarmReport.Answers.Count; k++)
                {
                    AlarmLprReportAnswerClientData pracd = GlobalLprAlarmReport.Answers[k] as AlarmLprReportAnswerClientData;
                    bool deleteAnswer = true;

                    foreach (IncidentTypeClientData itcd in questionsCctvControl1.SelectedIncidentTypes)
                    {
                        foreach (IncidentTypeQuestionClientData prqt in itcd.IncidentTypeQuestions)
                        {
                            if (prqt.Question.Code == pracd.QuestionCode)
                            {
                                deleteAnswer = false;
                                break;
                            }
                        }
                        if (deleteAnswer == false)
                            break;
                    }

                    if (deleteAnswer == true)
                    {
                        GlobalLprAlarmReport.Answers.RemoveAt(k);
                        k--;
                    }
                }
            }
        }


        private void ChangeCurrentStep(int stepNumber, bool needFocus)
        {
            switch (stepNumber)
            {
                case 0:
                    if (layoutControlItemLPr.Visibility == DevExpress.XtraLayout.Utils.LayoutVisibility.Never)
                    {
                        this.alarmInformationControl1.Active = false;
                    }
                    else if (layoutControlItemDatalogger.Visibility == DevExpress.XtraLayout.Utils.LayoutVisibility.Never)
                    {
                        this.alarmLprInformationControl1.Active = false;
                    }
                    this.questionsCctvControl1.Active = false;
                    this.departmentsInvolvedControl.Active = false;
                    break;
                case 1:

                    if (layoutControlItemLPr.Visibility == DevExpress.XtraLayout.Utils.LayoutVisibility.Never)
                    {
                        this.alarmInformationControl1.Active = true;
                        if (needFocus == true)
                            this.alarmInformationControl1.Focus();
                    }
                    else if (layoutControlItemDatalogger.Visibility == DevExpress.XtraLayout.Utils.LayoutVisibility.Never)
                    {
                        this.alarmLprInformationControl1.Active = true;
                        if (needFocus == true)
                            this.alarmLprInformationControl1.Focus();
                    }
                    this.questionsCctvControl1.Active = false;
                    this.departmentsInvolvedControl.Active = false;
                    break;
                case 2:
                    this.alarmInformationControl1.Active = false;
                    this.questionsCctvControl1.Active = true;
                    if (needFocus == true)
                        this.questionsCctvControl1.Focus();
                    this.departmentsInvolvedControl.Active = false;
                    break;
                case 3:
                    if (layoutControlItemLPr.Visibility == DevExpress.XtraLayout.Utils.LayoutVisibility.Never)
                    {
                        this.alarmInformationControl1.Active = false;
                    }
                    else if (layoutControlItemDatalogger.Visibility == DevExpress.XtraLayout.Utils.LayoutVisibility.Never)
                    {
                        this.alarmLprInformationControl1.Active = false;
                    }
                    this.questionsCctvControl1.Active = false;
                    this.departmentsInvolvedControl.Active = true;
                    if (needFocus == true)
                        this.departmentsInvolvedControl.Focus();
                    break;
                case 4:
                    if (layoutControlItemLPr.Visibility == DevExpress.XtraLayout.Utils.LayoutVisibility.Never)
                    {
                        this.alarmInformationControl1.Active = false;
                    }
                    else if (layoutControlItemDatalogger.Visibility == DevExpress.XtraLayout.Utils.LayoutVisibility.Never)
                    {
                        this.alarmLprInformationControl1.Active = false;
                    }
                    this.questionsCctvControl1.Active = false;
                    this.departmentsInvolvedControl.Active = false;
                    break;
            }
        }

        private void GenerateNewAlarmReport()
        {
            if (IsDatalogger())
            {
                if (this.globalDataloggerAlarmReport == null)
                {
                    this.GlobalDataloggerAlarmReport = new AlarmReportClientData();
                }
                
                
                //AlarmSensorTelemetryClientData alarmSensorTelemetry = new AlarmSensorTelemetryClientData();
                
                //alarmSensorTelemetry = (AlarmSensorTelemetryClientData)ServerServiceClient.GetInstance().SearchClientObject(
                //    SmartCadHqls.GetCustomHql(SmartCadHqls.GetAlarmSensorTelemetryByCode,
                //        selectedAlarmSensorTelemetryCode));

                //alarmSensorTelemetry.EndAlarm = ServerServiceClient.GetInstance().GetTime();
                //alarmSensorTelemetry.Operator = ServerServiceClient.GetInstance().OperatorClient;

                this.GlobalDataloggerAlarmReport.AlarmSensorTelemetryClient = Alarm;
                this.GlobalDataloggerAlarmReport.CustomCode = Guid.NewGuid().ToString();
                this.GlobalDataloggerAlarmReport.OperatorLogin = ServerServiceClient.OperatorClient.Login;
            }
            else if (IsLpr())
            {
                if (this.globalLprAlarmReport == null)
                {
                    this.GlobalLprAlarmReport = new AlarmLprReportClientData();
                }
                AlarmLprReportLprClientData lpr = new AlarmLprReportLprClientData();
                lpr.Address = new AddressClientData();
                //this.GlobalLprAlarmReport.AlarmLprReportLprClient = lpr;
                this.GlobalLprAlarmReport.CustomCode = Guid.NewGuid().ToString();
                this.GlobalLprAlarmReport.OperatorLogin = ServerServiceClient.OperatorClient.Login;
            }
        }

        private void ReleaseAlarm()
        {
            AlarmSensorTelemetryClientData astcd = this.GlobalDataloggerAlarmReport.AlarmSensorTelemetryClient;
            astcd.Operator = null;
            ServerServiceClient.GetInstance().SaveOrUpdateClientData(astcd);
        }


        private void questionsCctvControl1_IncidentTypeSelected(object sender, EventArgs e)
        {
            QuestionsControlDevX questionCtrl = (QuestionsControlDevX)sender;
            IList selectedIncidentTypes = questionCtrl.SelectedIncidentTypes;
            if (HasIncidentTypesListChanged(selectedIncidentTypes) == true)
            {
                if (IsDatalogger())
                {
                    this.GlobalDataloggerAlarmReport.IncidentTypesCodes = new ArrayList();
                    foreach (IncidentTypeClientData incidentType in selectedIncidentTypes)
                        this.GlobalDataloggerAlarmReport.IncidentTypesCodes.Add(incidentType.Code);
                    if (selectedIncidentTypes.Count == 1 && (selectedIncidentTypes[0] as IncidentTypeClientData) != null &&
                        (selectedIncidentTypes[0] as IncidentTypeClientData).NoEmergency == true)
                        this.departmentsInvolvedControl.departmentTypesControl.CleanControl();
                    else
                        this.departmentsInvolvedControl.departmentTypesControl.IncidentTypes = selectedIncidentTypes;
                }
                else if (IsLpr())
                {
                    this.GlobalLprAlarmReport.IncidentTypesCodes = new ArrayList();
                    foreach (IncidentTypeClientData incidentType in selectedIncidentTypes)
                        this.GlobalLprAlarmReport.IncidentTypesCodes.Add(incidentType.Code);
                    if (selectedIncidentTypes.Count == 1 && (selectedIncidentTypes[0] as IncidentTypeClientData) != null &&
                        (selectedIncidentTypes[0] as IncidentTypeClientData).NoEmergency == true)
                        this.departmentsInvolvedControl.departmentTypesControl.CleanControl();
                    else
                        this.departmentsInvolvedControl.departmentTypesControl.IncidentTypes = selectedIncidentTypes;
                }
            }
            questionActionAdd.Clear();
            questionActionsUpdate.Clear();
            questionActionDelete.Clear();
        }

        private bool HasIncidentTypesListChanged(IList newList)
        {
            bool result = false;
            if (IsDatalogger())
            {
                if (GlobalDataloggerAlarmReport.IncidentTypesCodes != null)
                {
                    if (newList.Count != globalDataloggerAlarmReport.IncidentTypesCodes.Count)
                        result = true;
                    else if (result == false)
                    {
                        for (int i = 0; i < newList.Count && result == false; i++)
                        {
                            IncidentTypeClientData newIncidentType = newList[i] as IncidentTypeClientData;
                            if (GlobalDataloggerAlarmReport.IncidentTypesCodes.Contains(newIncidentType.Code) == false)
                                result = true;
                        }
                    }
                }
                else
                    result = true;
            }
            else if (IsLpr())
            {
                if (GlobalLprAlarmReport.IncidentTypesCodes != null)
                {
                    if (newList.Count != globalLprAlarmReport.IncidentTypesCodes.Count)
                        result = true;
                    else if (result == false)
                    {
                        for (int i = 0; i < newList.Count && result == false; i++)
                        {
                            IncidentTypeClientData newIncidentType = newList[i] as IncidentTypeClientData;
                            if (GlobalLprAlarmReport.IncidentTypesCodes.Contains(newIncidentType.Code) == false)
                                result = true;
                        }
                    }
                }
                else
                    result = true;
            }
            return result;
        }

        public void serverService_CommittedChanges(object sender, CommittedObjectDataCollectionEventArgs args)
        {
            try
            {
                if (args != null && args.Objects != null && args.Objects.Count > 0)
                {
                    if (args.Objects[0] is IncidentClientData)//if it's an incident...
                    {
                        if (((IncidentClientData)args.Objects[0]).Status.Name.Equals(IncidentStatusClientData.Closed.Name) == false
                            && args.Action != CommittedDataAction.Delete)
                        {
                            SmartCadContext context = args.Context as SmartCadContext;
                            bool select = false;
                            if (context != null)
                            {
                                if (ServerServiceClient.GetInstance().OperatorClient != null &&
                                    ServerServiceClient.GetInstance().OperatorClient.Login == context.Operator)
                                {
                                    select = true;
                                }
                            }
                            gridControlExIncidentList.AddOrUpdateItem(new GridIncidentAlarmData(args.Objects[0] as IncidentClientData), select);

                            this.GlobalIncidents.Add(args.Objects[0] as IncidentClientData);
                        }
                        else if (((IncidentClientData)args.Objects[0]).Status.Name.Equals(IncidentStatusClientData.Closed.Name))
                        {
                            gridControlExIncidentList.DeleteItem(new GridIncidentAlarmData(args.Objects[0] as IncidentClientData));
                            globalIncidents.Remove(args.Objects[0] as IncidentClientData);
                        }
                    }
                    else if (args.Objects[0] is AlarmReportClientData)// if it's a phone report... gotta check if it is already added...
                    {
                        IncidentClientData incidentData = new IncidentClientData();//null;
                        AlarmReportClientData alarmReport = args.Objects[0] as AlarmReportClientData;
                        int index = ((IList)gridControlExIncidentList.DataSource).IndexOf(new GridIncidentAlarmData(incidentData));

                        if (index >= 0)
                        {
                            incidentData = (IncidentClientData)((GridIncidentAlarmData)((IList)gridControlExIncidentList.DataSource)[index]).Tag;
                            bool found = false;
                            for (int i = 0; i < incidentData.AlarmReports.Count && found != true; i++)
                            {
                                AlarmReportClientData alarmReportTemp = incidentData.AlarmReports[i] as AlarmReportClientData;
                                if (alarmReportTemp.CustomCode == alarmReport.CustomCode)
                                {
                                    found = true;
                                    alarmReportTemp = alarmReport;
                                }
                            }
                            if (found == false)
                            {
                                foreach (int incidentTypeCode in alarmReport.IncidentTypesCodes)
                                {
                                    bool alreadyAdded = false;
                                    IncidentTypeClientData itcd = GlobalIncidentTypes[incidentTypeCode];
                                    foreach (IncidentTypeClientData incidentType in incidentData.IncidentTypes)
                                    {
                                        if (incidentType.Code == itcd.Code)
                                            alreadyAdded = true;
                                    }

                                    if (alreadyAdded == false)
                                        incidentData.IncidentTypes.Add(itcd);
                                }
                                incidentData.AlarmReports.Add(alarmReport);
                                incidentSyncBox.Sync(new GridIncidentAlarmData(incidentData), args.Action);
                            }
                        }
                    }
                    else if (args.Objects[0] is IncidentTypeClientData)
                    {
                        IncidentTypeClientData incidentTypeClientData = args.Objects[0] as IncidentTypeClientData;
                        if (args.Action == CommittedDataAction.Save)
                        {
                            AddIncindetType(incidentTypeClientData);
                        }
                        else if (args.Action == CommittedDataAction.Update)
                        {
                            popUpShow = true;

                            UpdateIncidentType(incidentTypeClientData);
                            for (int i = 0; i < IncidentChanges.Count; i++)
                            {
                                if ((IncidentChanges[i] as IncidentTypeClientData).Code == incidentTypeClientData.Code)
                                    IncidentChanges.RemoveAt(i);
                            }
                            IncidentChanges.Add(incidentTypeClientData);
                        }
                        else if (args.Action == CommittedDataAction.Delete)
                        {
                            DeleteIncidentType(incidentTypeClientData);
                        }
                        //Actualizar tipod de incident en datagrid.
                        FormUtil.InvokeRequired(this, delegate
                        {
                            gridControlExIncidentList.BeginUpdate();

                            if (gridControlExIncidentList.DataSource != null)
                            {
                                ArrayList list = new ArrayList(gridControlExIncidentList.DataSource as IList);
                                foreach (GridIncidentAlarmData item in list)
                                {
                                    IncidentClientData icd = item.Tag as IncidentClientData;
                                    for (int i = 0; i < icd.IncidentTypes.Count; i++)
                                    {
                                        IncidentTypeClientData itcd = icd.IncidentTypes[i] as IncidentTypeClientData;
                                        if (itcd.Code == incidentTypeClientData.Code)
                                        {
                                            icd.IncidentTypes[i] = incidentTypeClientData;
                                            int index = ((IList)gridControlExIncidentList.DataSource).IndexOf(new GridIncidentAlarmData(icd));
                                            if (index >= 0)
                                                ((IList)gridControlExIncidentList.DataSource)[index] = new GridIncidentAlarmData(icd);
                                            break;
                                        }
                                    }
                                }
                            }

                            gridControlExIncidentList.EndUpdate();

                        });
                    }
                    else if (args.Objects[0] is QuestionClientData)
                    {
                        QuestionClientData questionClientDara = args.Objects[0] as QuestionClientData;
                        if (args.Action == CommittedDataAction.Save)
                        {
                            AddQuestion(questionClientDara);
                        }
                        else if (args.Action == CommittedDataAction.Update)
                        {
                            UpdateQuestion(questionClientDara);
                            for (int i = 0; i < QuestionChanges.Count; i++)
                            {
                                if ((QuestionChanges[i] as QuestionClientData).Code == questionClientDara.Code)
                                    QuestionChanges.RemoveAt(i);
                            }
                            QuestionChanges.Add(questionClientDara);
                        }
                        else if (args.Action == CommittedDataAction.Delete)
                        {
                            DeleteQuestion(questionClientDara);
                        }
                    }
                    else if (args.Objects[0] is DataloggerClientData)
                    {
                        //Perform Online Changes for this type
                    }
                    else if (args.Objects[0] is AlarmSensorTelemetryClientData)
                    {
                        AlarmSensorTelemetryClientData alarmClientData =
                                (args.Objects[0] as AlarmSensorTelemetryClientData);

                        if (args.Action == CommittedDataAction.Update)
                        {
                            if (alarmClientData.Code == Alarm.Code) { }
                                //Alarm = alarmClientData;
                        }
                    }
                    else if (args.Objects[0] is ApplicationPreferenceClientData)
                    {
                        ApplicationPreferenceClientData preference = args.Objects[0] as ApplicationPreferenceClientData;
                        if (args.Action == CommittedDataAction.Update)
                        {
                            if (globalApplicationPreference.ContainsKey(preference.Name))
                            {
                                globalApplicationPreference[preference.Name] = preference;
                            }
                            else
                            {
                                globalApplicationPreference.Add(preference.Name, preference);
                            }
                        }
                    }
                    else if (args.Objects[0] is DepartmentTypeClientData)
                    {
                        #region  DepartmentTypeClientData
                        if (args.Action == CommittedDataAction.Delete)
                        {
                            DepartmentTypeClientData data = (DepartmentTypeClientData)args.Objects[0];
                            GridDepartmentTypeData itemData = new GridDepartmentTypeData(data);
                            if (GetSelectedDepartment().Contains(itemData))
                                DepartmentTypeChanges.Add(data);
                            else
                            {
                                FormUtil.InvokeRequired(this,
                                      delegate
                                      {
                                          departmentsInvolvedControl.departmentTypesControl.gridControlExDepartmentType.BeginInit();
                                          departmentsInvolvedControl.departmentTypesControl.gridControlExDepartmentType.DeleteItem(itemData);
                                          departmentsInvolvedControl.departmentTypesControl.gridControlExDepartmentType.EndInit();
                                      });
                            }
                        }
                        else if (args.Action == CommittedDataAction.Save || args.Action == CommittedDataAction.Update)
                        {
                            GridDepartmentTypeData data = new GridDepartmentTypeData((DepartmentTypeClientData)args.Objects[0]);
                            FormUtil.InvokeRequired(this,
                                delegate
                                {
                                    departmentsInvolvedControl.departmentTypesControl.gridControlExDepartmentType.BeginInit();
                                    departmentsInvolvedControl.departmentTypesControl.gridControlExDepartmentType.AddOrUpdateItem(data);
                                    departmentsInvolvedControl.departmentTypesControl.gridControlExDepartmentType.EndInit();
                                });
                        }
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        private IEnumerable<GridDepartmentTypeData> GetSelectedDepartment()
        {
            return departmentsInvolvedControl.departmentTypesControl.gridControlExDepartmentType.Items.Cast<GridDepartmentTypeData>().Where(
                                    dep => dep.DepartmentTypeCheck);
        }

        #region IncidentType Actions
        private void DeleteIncidentType(IncidentTypeClientData incidentTypeClientData)
        {
            if (GlobalIncidents != null && GlobalIncidentTypes.ContainsKey(incidentTypeClientData.Code) == true)
            {
                GlobalIncidentTypes.Remove(incidentTypeClientData.Code);
                if (alarmClientState == AlarmClientStateEnum.WaitingForAlarmIncident)
                {
                    FormUtil.InvokeRequired(this, delegate
                    {
                        CleanForm(true);
                    });
                }
            }
            else if (incidentTypeActionDelete.ContainsKey(incidentTypeClientData.Code) == false)
            {
                incidentTypeActionDelete.Add(incidentTypeClientData.Code, incidentTypeClientData);
            }
        }
        private void UpdateIncidentType(IncidentTypeClientData incidentTypeClientData)
        {
            if (GlobalIncidents != null && GlobalIncidentTypes.ContainsKey(incidentTypeClientData.Code) == true)
            {
                GlobalIncidentTypes[incidentTypeClientData.Code] = incidentTypeClientData;
                if (alarmClientState == AlarmClientStateEnum.WaitingForAlarmIncident && callEntering == false)
                {
                    FormUtil.InvokeRequired(this, delegate
                    {
                        CleanForm(true);
                    });
                }
            }
            else
            {
                if (incidentTypeActionsUpdate.ContainsKey(incidentTypeClientData.Code) == true)
                {
                    incidentTypeActionsUpdate[incidentTypeClientData.Code] = incidentTypeClientData;
                }
                else
                {
                    incidentTypeActionsUpdate.Add(incidentTypeClientData.Code, incidentTypeClientData);
                }
            }
        }
        private void AddIncindetType(IncidentTypeClientData incidentTypeClientData)
        {
            if (GlobalIncidents != null)
                GlobalIncidentTypes.Add(incidentTypeClientData.Code, incidentTypeClientData);
            if (alarmClientState == AlarmClientStateEnum.WaitingForAlarmIncident && callEntering == false)
            {
                FormUtil.InvokeRequired(this, delegate
                {
                    CleanForm(true);
                });
            }
            else
            {
                if (incidentTypeActionAdd.ContainsKey(incidentTypeClientData.Code) == false)
                {
                    incidentTypeActionAdd.Add(incidentTypeClientData.Code, incidentTypeClientData);
                }
            }
        }
        #endregion

        #region Question Actions
        private void DeleteQuestion(QuestionClientData questionClientData)
        {
            if (GlobalIncidents != null)
            {
                foreach (IncidentTypeQuestionClientData itqcd in questionClientData.IncidentTypes)
                {
                    if (GlobalIncidentTypes.ContainsKey(itqcd.IncidentType.Code) == true)
                    {
                        IncidentTypeClientData itcd = GlobalIncidentTypes[itqcd.IncidentType.Code] as IncidentTypeClientData;
                        for (int i = 0; i < itcd.IncidentTypeQuestions.Count; i++)
                        {
                            IncidentTypeQuestionClientData itqcd2 = itcd.IncidentTypeQuestions[i] as IncidentTypeQuestionClientData;
                            if (itqcd2.Code == itqcd.Code)
                            {
                                itcd.IncidentTypeQuestions.RemoveAt(i);
                                i--;
                            }
                        }
                    }
                }
            }
            if (alarmClientState == AlarmClientStateEnum.WaitingForAlarmIncident)
            {
                FormUtil.InvokeRequired(this, delegate
                {
                    CleanForm(true);
                });
            }
            else
            {
                if (questionActionDelete.ContainsKey(questionClientData.Code) == false)
                {
                    questionActionDelete.Add(questionClientData.Code, questionClientData);
                }
            }
        }
        private void UpdateQuestion(QuestionClientData questionClientData)
        {
            if (GlobalIncidents != null)
            {
                foreach (IncidentTypeClientData itcd in GlobalIncidentTypes.Values)
                {
                    for (int i = 0; i < itcd.IncidentTypeQuestions.Count; i++)
                    {
                        IncidentTypeQuestionClientData itqcd = itcd.IncidentTypeQuestions[i] as IncidentTypeQuestionClientData;
                        if (itqcd.Question.Code == questionClientData.Code)
                        {
                            itcd.IncidentTypeQuestions.RemoveAt(i);
                            i--;
                        }
                    }
                }
                foreach (IncidentTypeQuestionClientData itqcd in questionClientData.IncidentTypes)
                {
                    if (GlobalIncidentTypes.ContainsKey(itqcd.IncidentType.Code) == true)
                    {
                        IncidentTypeClientData itcd = GlobalIncidentTypes[itqcd.IncidentType.Code] as IncidentTypeClientData;
                        itqcd.Question = questionClientData;
                        itcd.IncidentTypeQuestions.Add(itqcd);
                    }
                }
            }
            if (alarmClientState == AlarmClientStateEnum.WaitingForAlarmIncident && callEntering == false)
            {
                FormUtil.InvokeRequired(this, delegate
                {
                    CleanForm(true);
                });
            }
            else
            {
                if (questionClientData.RequirementType == RequirementTypeClientEnum.Recommended &&
                    questionActionsUpdate.ContainsKey(questionClientData.Code) == false)
                {
                    questionActionsUpdate.Add(questionClientData.Code, questionClientData);
                }
                else
                {
                    if (questionClientData.RequirementType == RequirementTypeClientEnum.Recommended &&
                        questionActionsUpdate[questionClientData.Code] != null)
                        questionActionsUpdate[questionClientData.Code] = questionClientData;
                }
            }

            if (questionActionAdd.ContainsKey(questionClientData.Code))
            {
                questionActionAdd[questionClientData.Code] = questionClientData;
                questionActionsUpdate.Remove(questionClientData.Code);
            }
        }
        private void AddQuestion(QuestionClientData questionClientData)
        {
            foreach (IncidentTypeQuestionClientData itqcd in questionClientData.IncidentTypes)
            {
                if (GlobalIncidents != null && GlobalIncidentTypes.ContainsKey(itqcd.IncidentType.Code) == true)
                {
                    IncidentTypeClientData itcd = GlobalIncidentTypes[itqcd.IncidentType.Code] as IncidentTypeClientData;
                    itqcd.Question = questionClientData;
                    itcd.IncidentTypeQuestions.Add(itqcd);
                }
            }
            if (alarmClientState == AlarmClientStateEnum.WaitingForAlarmIncident && callEntering == false)
            {
                FormUtil.InvokeRequired(this, delegate
                {
                    CleanForm(true);
                });
            }
            else
            {
                if (questionClientData.RequirementType == RequirementTypeClientEnum.Recommended &&
                    questionActionAdd.ContainsKey(questionClientData.Code) == false)
                {
                    questionActionAdd.Add(questionClientData.Code, questionClientData);
                }
                else
                {
                    if (questionClientData.RequirementType == RequirementTypeClientEnum.Recommended &&
                        questionActionAdd[questionClientData.Code] != null)
                        questionActionAdd[questionClientData.Code] = questionClientData;
                }
            }
        }

        private void questionsControl_AnswerUpdated(object sender, EventArgs e)
        {

            if (IsDatalogger())
            {
                if (AlarmClientState != AlarmClientStateEnum.RegisteringAlarmIncident)
                {
                    updatedAnswers = new ArrayList(questionsCctvControl1.QuestionsWithAnswers);
                    foreach (ReportAnswerClientData alarmReportAnswer in updatedAnswers)
                    {

                        alarmReportAnswer.ReportCode = GlobalDataloggerAlarmReport.Code;

                    }
                }
                else
                {

                    this.GlobalDataloggerAlarmReport.Answers = new ArrayList(questionsCctvControl1.QuestionsWithAnswers);
                    foreach (ReportAnswerClientData alarmReportAnswer in GlobalDataloggerAlarmReport.Answers)
                    {
                        alarmReportAnswer.ReportCode = GlobalDataloggerAlarmReport.Code;
                    }


                }
            }
            else if (IsLpr())
            {
                if (AlarmClientState != AlarmClientStateEnum.RegisteringAlarmIncident)
                {
                    updatedAnswers = new ArrayList(questionsCctvControl1.QuestionsWithAnswers);
                    foreach (AlarmLprReportAnswerClientData alarmReportAnswer in updatedAnswers)
                    {

                        alarmReportAnswer.ReportCode = GlobalLprAlarmReport.Code;

                    }
                }
                else
                {

                    this.GlobalLprAlarmReport.Answers = new ArrayList(questionsCctvControl1.QuestionsWithAnswers);
                    foreach (AlarmLprReportAnswerClientData alarmReportAnswer in GlobalLprAlarmReport.Answers)
                    {
                        alarmReportAnswer.ReportCode = GlobalLprAlarmReport.Code;
                    }


                }
            }
            answersUpdatedTimerEventArgs = e;
            answersUpdatedTimer.Change(1000, Timeout.Infinite);
        }
        #endregion

        private IList CheckAlarmReportUpdates(IList newAnswers)
        {
            //No borrar, se utilizara despues.
            IList answersToAdd = new ArrayList();
            if (IsDatalogger())
            {
                if (GlobalDataloggerAlarmReport != null)
                {
                    if (newAnswers != null)
                    {
                        foreach (ReportAnswerClientData alarmReportAnswer in newAnswers)
                        {
                            bool found = false;
                            int i = 0;
                            while (found == false && i < GlobalDataloggerAlarmReport.Answers.Count)
                            {
                                ReportAnswerClientData tempAlarmReportAnswer = GlobalDataloggerAlarmReport.Answers[i] as ReportAnswerClientData;
                                if (tempAlarmReportAnswer.QuestionCode == alarmReportAnswer.QuestionCode)
                                {
                                    found = true;
                                }
                                i++;
                            }
                            if (found == false)
                            {
                                alarmReportAnswer.ReportCode = GlobalDataloggerAlarmReport.Code;
                                answersToAdd.Add(alarmReportAnswer);
                            }
                        }
                    }
                }
            }
            else if (IsLpr())
            {
                if (GlobalLprAlarmReport != null)
                {
                    if (newAnswers != null)
                    {
                        foreach (AlarmLprReportAnswerClientData alarmReportAnswer in newAnswers)
                        {
                            bool found = false;
                            int i = 0;
                            while (found == false && i < GlobalLprAlarmReport.Answers.Count)
                            {
                                AlarmLprReportAnswerClientData tempAlarmReportAnswer = GlobalLprAlarmReport.Answers[i] as AlarmLprReportAnswerClientData;
                                if (tempAlarmReportAnswer.QuestionCode == alarmReportAnswer.QuestionCode)
                                {
                                    found = true;
                                }
                                i++;
                            }
                            if (found == false)
                            {
                                alarmReportAnswer.ReportCode = GlobalLprAlarmReport.Code;
                                answersToAdd.Add(alarmReportAnswer);
                            }
                        }
                    }
                }
            }
            return answersToAdd;
        }

        private bool IsEmergencyIncident(AlarmReportClientData newAlarmReport)
        {
            bool result = true;
            if (newAlarmReport.IncidentTypesCodes.Count == 1)
            {
                IncidentTypeClientData incidentType = globalIncidentTypes[(int)newAlarmReport.IncidentTypesCodes[0]];
                if (incidentType.NoEmergency == true)
                    result = false;
            }
            return result;
        }
        private bool IsEmergencyLprIncident(AlarmLprReportClientData newAlarmReport)
        {
            bool result = true;
            if (newAlarmReport.IncidentTypesCodes.Count == 1)
            {
                IncidentTypeClientData incidentType = globalIncidentTypes[(int)newAlarmReport.IncidentTypesCodes[0]];
                if (incidentType.NoEmergency == true)
                    result = false;
            }
            return result;
        }
        private bool IsDatalogger()
        {
            if (layoutControlItemDatalogger.Visibility == DevExpress.XtraLayout.Utils.LayoutVisibility.Always)
            {
                return true;
            }
            return false;
        }

        private bool IsLpr()
        {
            if (layoutControlItemLPr.Visibility == DevExpress.XtraLayout.Utils.LayoutVisibility.Always)
            {
                return true;
            }
            return false;
        }


        private void GenerateNewIncident()
        {
            IncidentClientData incident = new IncidentClientData();
            incident.SourceIncidentApp = SourceIncidentAppEnum.Alarm;
            incident.AlarmReports = new ArrayList();
            ApplicationPreferenceClientData prefix = globalApplicationPreference["ALARMIncidentCodePrefix"];
            ApplicationPreferenceClientData isSuffix = globalApplicationPreference["IsIncidentCodeSuffix"];
            incident.CustomCode = serverServiceClient.OperatorClient.Code + serverServiceClient.GetTime().ToString("yyMMddHHmmss");
            if (isSuffix.Value.Equals(false.ToString()))
                incident.CustomCode = prefix.Value + incident.CustomCode;
            else
                incident.CustomCode += prefix.Value;
            incident.Address = new AddressClientData(
                    alarmInformationControl1.SensorTown,
                    alarmInformationControl1.SensorStreet,
                    null, null, true);

            if (this.IsDatalogger() == true)
            {
                incident.Address.State = alarmInformationControl1.SensorState;
                incident.Address.Town = alarmInformationControl1.SensorTown;
                incident.Address.Lat = (alarmInformationControl1.StructClientData as StructClientData).Lat;
                incident.Address.Lon = (alarmInformationControl1.StructClientData as StructClientData).Lon;
                incident.IsEmergency = IsEmergencyIncident(GlobalDataloggerAlarmReport);
                incident.StartDate = DateTime.Now;
                if (incident.IsEmergency == true)
                    incident.Status = IncidentStatusClientData.Open;
                else
                    incident.Status = IncidentStatusClientData.Closed;

                foreach (ReportBaseDepartmentTypeClientData reportBaseDepartmentType in
              this.GlobalDataloggerAlarmReport.ReportBaseDepartmentTypesClient)
                {
                    reportBaseDepartmentType.ReportBaseCode = this.GlobalDataloggerAlarmReport.Code;
                }
                if (GlobalDataloggerAlarmReport.ReportBaseDepartmentTypesClient == null || GlobalDataloggerAlarmReport.ReportBaseDepartmentTypesClient.Count == 0)
                    incident.Status = IncidentStatusClientData.Closed;
                //Alarm = (AlarmSensorTelemetryClientData) ServerServiceClient.GetInstance().SearchClientObject("select alarm from AlarmSensorTelemetryData alarm where alarm.Code="+Alarm.Code.ToString());
                Alarm.EndAlarm = ServerServiceClient.GetInstance().GetTime();
                Alarm.Operator = ServerServiceClient.GetInstance().OperatorClient;
                Alarm.Version = Alarm.Version + 1;
                //ServerServiceClient.SaveOrUpdateClientData(Alarm);
                incident.EndDate = (DateTime) Alarm.EndAlarm;
                GlobalDataloggerAlarmReport.AlarmSensorTelemetryClient = Alarm;

                this.GlobalDataloggerAlarmReport.IncidentCode = incident.Code;
                if (GlobalDataloggerAlarmReport.Answers == null)
                    GlobalDataloggerAlarmReport.Answers = new ArrayList();
                incident.AlarmReports.Add(this.GlobalDataloggerAlarmReport);
            }
            else if (this.IsLpr() == true)
            {
                incident.Address.State = alarmLprInformationControl1.State;
                incident.Address.Town = alarmLprInformationControl1.Town;
                incident.Address.Lat = (alarmLprInformationControl1.StructClientData as StructClientData).Lat;
                incident.Address.Lon = (alarmLprInformationControl1.StructClientData as StructClientData).Lon;
                incident.IsEmergency = IsEmergencyLprIncident(GlobalLprAlarmReport);
                
                if (incident.IsEmergency == true)
                    incident.Status = IncidentStatusClientData.Open;
                else
                    incident.Status = IncidentStatusClientData.Closed;

                foreach (ReportBaseDepartmentTypeClientData reportBaseDepartmentType in
                this.GlobalLprAlarmReport.ReportBaseDepartmentTypesClient)
                {
                    reportBaseDepartmentType.ReportBaseCode = this.GlobalLprAlarmReport.Code;
                }
                if (GlobalLprAlarmReport.ReportBaseDepartmentTypesClient == null || GlobalLprAlarmReport.ReportBaseDepartmentTypesClient.Count == 0)
                    incident.Status = IncidentStatusClientData.Closed;
                //GlobalLprAlarmReport.AlarmLprReportLprClient.Address = incident.Address;
                //GlobalLprAlarmReport.AlarmLprReportLprClient.Name = alarmLprInformationControl1.LprAlarm;
                GlobalLprAlarmReport.FinishedTime = ServerServiceClient.GetInstance().GetTime();
                this.GlobalLprAlarmReport.IncidentCode = incident.Code;
                if (GlobalLprAlarmReport.Answers == null)
                    GlobalLprAlarmReport.Answers = new ArrayList();
                incident.AlarmReports.Add(this.GlobalLprAlarmReport);

            }
            //ReleaseAlarm();
           
            //if (IsDatalogger())
            //{
            //    ServerServiceClient.GetInstance().SaveOrUpdateClientData(GlobalDataloggerAlarmReport.AlarmSensorTelemetryClient);
            //}
            ServerServiceClient.GetInstance().SaveOrUpdateClientData(incident);
            updatedAnswers.Clear();
            EndRecordNewIncident();

        }

        private void EndRecordNewIncident()
        {
            //CHECKING FOR UNSAVED CHANGES IN PHONEREPORT...
            if (IsDatalogger())
            {
                if (updatedAnswers.Count > 0)
                {
                    if (EnsureUpdateAlarmReport() == true)
                    {
                        GlobalDataloggerAlarmReport.Answers = updatedAnswers;
                    }
                }
                if (ValidateChangedQuestions() == false)
                {
                    DialogResult dialogResult = MessageForm.Show(ResourceLoader.GetString2("QuestionsWereDeleted"), MessageFormType.Information);
                }
                ServerServiceClient.GetInstance().FinalizeAlarmIncident(GlobalDataloggerAlarmReport);
            }
            else if (IsLpr())
            {
                if (updatedAnswers.Count > 0)
                {
                    if (EnsureUpdateAlarmReport() == true)
                    {
                        GlobalLprAlarmReport.Answers = updatedAnswers;
                    }
                }
                if (ValidateChangedQuestions() == false)
                {
                    DialogResult dialogResult = MessageForm.Show(ResourceLoader.GetString2("QuestionsWereDeleted"), MessageFormType.Information);
                }
                ServerServiceClient.GetInstance().FinalizeAlarmLprIncident(GlobalLprAlarmReport);
            }
            this.AlarmClientState = AlarmClientStateEnum.WaitingForAlarmIncident;
            //SEND CLEAR TO MAPS
            ApplicationServiceClient.Current(UserApplicationClientData.Map).InsertAddressDataInGis(
                new AddressClientData(string.Empty, string.Empty, string.Empty, string.Empty, false));
            questionActionsUpdate.Clear();
            questionActionDelete.Clear();
            questionActionAdd.Clear();
            CleanForm(true);
        }

        private void FillIncidentDetails(SelectedIncidentTaskResult selectedIncidentTask, bool sameIncident)
        {
            try
            {
                string strXml = ApplicationUtil.BuildHtml(selectedIncidentTask.Xml, xslCompiledTransform, "Alarmincident");
                strXml = ApplicationUtil.RecoverBlanks(strXml);
                FileStream fs = File.Open("incident_alarm.html", FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite);
                byte[] byteXML = Encoding.Unicode.GetBytes(strXml);
                fs.Write(byteXML, 0, byteXML.Length);
                fs.Flush();
                fs.Close();
                FormUtil.InvokeRequired(this,
               delegate
               {
                   this.layoutControlGroup4.Text = ResourceLoader.GetString2("IncidentDetails");
                   this.layoutControlGroup4.Text += ResourceLoader.GetString2("UpdatedAt");
                   this.layoutControlGroup4.Text += serverServiceClient.GetTime().ToString("G", ApplicationUtil.GetCurrentCulture());
               });

                FormUtil.InvokeRequired(textBoxExIncidentdetails, delegate
                {
                    textBoxExIncidentdetails.Navigate(new FileInfo("incident_alarm.html").FullName, sameIncident);
                });
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        private void FillCallsAndDispatchs(SelectedIncidentTaskResult selectedIncidentTask)
        {
            if (selectedIncidentTask != null)
            {
                foreach (AlarmReportClientData phoneReportLight in selectedIncidentTask.TotalPhoneReports)
                {
                    GridControlDataAssociatedCallData item = new GridControlDataAssociatedCallData(phoneReportLight);
                }
                foreach (DispatchOrderClientData dispatchOrderLight in selectedIncidentTask.TotalDispatchOrders)
                {
                    GridControlDataAssociatedDispatchOrder item = new GridControlDataAssociatedDispatchOrder(dispatchOrderLight);
                }
            }
        }

        private string GetAlarmReportIncidentTypes(AlarmReportClientData alarmReport)
        {
            string incidentTypeStr = "";
            int counter = 0;
            int incTypeLength = alarmReport.IncidentTypesCodes.Count;
            foreach (int incidentTypeCode in alarmReport.IncidentTypesCodes)
            {
                incidentTypeStr += globalIncidentTypes[incidentTypeCode].CustomCode;
                counter++;
                if (counter < incTypeLength)
                    incidentTypeStr += ", ";

            }
            return incidentTypeStr;
        }

        private bool EnsureUpdateAlarmReport()
        {
            DialogResult dialogResult = MessageForm.Show(ResourceLoader.GetString2("ChangesOnReport"), MessageFormType.Question);

            return dialogResult == DialogResult.Yes;
        }

        private void CleanForm(bool clearAlarmInformation)
        {
            this.questionsCctvControl1.CleanControls();
            //this.alarmLprInformationControl1.CleanControl();
            this.departmentsInvolvedControl.departmentTypesControl.CleanControl();
            this.incidentAdded = false;
            callEntering = false;
            IsActiveCall = false;
            this.gridViewExIncidentList.ClearSelection();
            if (clearAlarmInformation)
            {
                this.alarmInformationControl1.CleanControl();
            }
            this.GlobalDataloggerAlarmReport = null;
            this.GlobalLprAlarmReport = null;
            ChangeCurrentStep(0, false);
        }

        private void CheckButtons(ButtonEx pressedButton)
        {
            pressedButton.BackColor = SystemColors.ButtonHighlight;
        }

        private void buttonExOpenIncidentsFilter_Click(object sender, EventArgs e)
        {
            ButtonEx pressedButton = (ButtonEx)sender;
            if (pressedButton.BackColor != SystemColors.ButtonHighlight)
            {
                searchByText = false;
                CheckButtons(pressedButton);
                ArrayList parameters = new ArrayList();
                parameters.Add("OPEN_INCIDENTS");
                incidentsCurrentFilter.Parameters = parameters;
                incidentsCurrentFilter.Filter(true);

                if (gridControlExIncidentList.SelectedItems.Count == 0)
                    ClearTextBox();
            }
        }

        private void ClearTextBox()
        {
            layoutControlGroup4.Text = ResourceLoader.GetString2("IncidentDetails");
            textBoxExIncidentdetails.DocumentText = "<span style='HEIGHT:100%;FONT-SIZE: 10pt; FONT-FAMILY: Sans-Serif'>" + ResourceLoader.GetString2("NoIncidentSelected") + "</span>";
            barButtonItemPrint.Enabled = false;
            barButtonItemSave.Enabled = false;
            barButtonItemRefresh.Enabled = false;
        }

        private void SelectedIncidentHelp(SelectedIncidentTask selectedIncidentTask, GeoPoint point)
        {
            if (point != null)
            {
                ApplicationServiceClient.Current(UserApplicationClientData.Map).DisplayGeoPointInGis(point);
            }
            if (selectedIncidentTask != null)
            {
                ArrayList tasks = new ArrayList();
                tasks.Add(selectedIncidentTask);
                bool sameIncident = false;
                if (selectedIncidentCode == (int)selectedIncidentTask.Parameters[0])
                    sameIncident = true;
                selectedIncidentCode = (int)selectedIncidentTask.Parameters[0];
                try
                {
                    IList result = serverServiceClient.RunTasks(tasks);
                    if (result != null && result.Count > 0 && (result[0] as IList) != null)
                    {
                        IList tempList = (IList)result[0];
                        if (tempList != null && tempList.Count > 0)
                        {
                            SelectedIncidentTaskResult selectedIncidentLightData = tempList[0] as SelectedIncidentTaskResult;
                            if (selectedIncidentLightData != null)
                            {
                                FillIncidentDetails(selectedIncidentLightData, sameIncident);
                                FillCallsAndDispatchs(selectedIncidentLightData);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    SmartLogger.Print(ex);
                }
            }
        }

        private void RefreshIncidentReport()
        {
            if (selectedIncidentCode != -1)
            {
                ArrayList parameters = new ArrayList();
                parameters.Add(selectedIncidentCode);
                SelectedIncidentTask selectedIncidentTask = new SelectedIncidentTask(parameters);
                ArrayList tasks = new ArrayList();
                tasks.Add(selectedIncidentTask);
                try
                {
                    IList result = serverServiceClient.RunTasks(tasks);
                    if (result != null && result.Count > 0 && (result[0] as IList) != null)
                    {
                        IList tempList = (IList)result[0];
                        if (tempList != null && tempList.Count > 0)
                        {
                            SelectedIncidentTaskResult selectedIncidentLightData = tempList[0] as SelectedIncidentTaskResult;
                            if (selectedIncidentLightData != null)
                            {
                                FillIncidentDetails(selectedIncidentLightData, true);
                                FillCallsAndDispatchs(selectedIncidentLightData);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    SmartLogger.Print(ex);
                }
            }
        }

        private void SetSkin()
        {
            try
            {
                string skinFile = SmartCadConfiguration.SkinsFolder + "/Clasic.xml";
                skinEngine = SkinEngine.Load(skinFile);
                ProfessionalColorTable colorTable = new CustomToolStripColors(
                    skinEngine.Colors["ToolStripGradientBegin"],
                    skinEngine.Colors["ToolStripGradientMiddle"],
                    skinEngine.Colors["ToolStripGradientEnd"],
                    skinEngine.Colors["MenuStripGradientBegin"],
                    skinEngine.Colors["MenuStripGradientEnd"],
                    skinEngine.Colors["StatusStripGradientBegin"],
                    skinEngine.Colors["StatusStripGradientEnd"]
                );


                skinEngine.AddElement("Step1Control", this.alarmInformationControl1);
                skinEngine.AddElement("Step1Control", this.alarmLprInformationControl1);
                skinEngine.AddElement("Step2Control", this.questionsCctvControl1);
                skinEngine.AddElement("Step3LeftControl", this.departmentsInvolvedControl);
                skinEngine.AddCompleteElement(this);
                skinEngine.ApplyChanges();
            }
            catch (Exception ex)
            {
                MessageForm.Show(ResourceLoader.GetString2("$Message.Error"), ResourceLoader.GetString2("ErrorLoadingSkin"), ex);
            }
        }

        private void AlarmFrontClientForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            ServerServiceClient.GetInstance().CommittedChanges -= new EventHandler<CommittedObjectDataCollectionEventArgs>(serverService_CommittedChanges);

            ServerServiceClient.GetInstance().CloseSession();
        }

        private void SetFrontClientWaitingForCallState()
        {
            FormUtil.InvokeRequired(this, delegate
            {
                this.questionsCctvControl1.CctvClientState = CctvClientStateEnum.WaitingForCctvIncident;
                this.departmentsInvolvedControl.FrontClientState = FrontClientStateEnum.WaitingForIncident;
                this.barButtonItemRegisterIncident.Enabled = false;
                this.barButtonItemCancelIncident.Enabled = false;
            });
        }

        private void SetFrontClientRegisteringCallState()
        {
            FormUtil.InvokeRequired(this, delegate
            {
                this.questionsCctvControl1.CctvClientState = CctvClientStateEnum.RegisteringCctvIncident;
                this.departmentsInvolvedControl.FrontClientState = FrontClientStateEnum.RegisteringIncident;
                this.barButtonItemRegisterIncident.Enabled = true;
                this.barButtonItemCancelIncident.Enabled = true;
            });
        }

        private void dataGridAlarmPrevIncidents_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                barButtonItemRefresh_ItemClick(null, null);
            }
            else
            {
                AlarmFrontClientForm_KeyDown(null, e);
            }
        }

        private void textBoxExIncidentdetails_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            KeyEventArgs keyEvent = new KeyEventArgs(e.KeyData);
            AlarmFrontClientForm_KeyDown(null, keyEvent);
        }

        private void questionAnswerTextControl_Enter(object sender, EventArgs e)
        {
            if (this.AlarmClientState != AlarmClientStateEnum.WaitingForAlarmIncident)
                ChangeCurrentStep(3, false);
        }

        private void toolStripMenuItemHelpOnLine_Click(object sender, EventArgs e)
        {
            try
            {
                ApplicationUtil.LaunchHelp(SmartCadConfiguration.DistFolder + "/Help/SmartCad.chm");
            }
            catch (Exception ex)
            {
                MessageForm.Show(ResourceLoader.GetString2("$Message.HelpNotInstalled"), ex);
            }
        }

        private void questionAnswerTextControl_QuestionAnswerTextControlPreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            KeyEventArgs keyEvent = new KeyEventArgs(e.KeyData);
            AlarmFrontClientForm_KeyDown(null, keyEvent);
        }

        private void frontClientButton_MouseLeave(object sender, EventArgs e)
        {
            toolTipMain.Hide((Button)sender);
        }

        private void contextMenuStripIncidents_Opening(object sender, CancelEventArgs e)
        {
            if (gridControlExIncidentList.SelectedItems.Count > 0)
                barButtonItemRefresh.Enabled = true;
            else
            {
                e.Cancel = true;
                barButtonItemRefresh.Enabled = false;
            }
        }

        private void alarmInformationControl1_SendGeoPointStruct(object sender, GeoPointEventArgs e)
        {
            ApplicationServiceClient.Current(UserApplicationClientData.Map).DisplayGeoPointInGis(e.Point);
        }

        private void barButtonItemStartRegIncident_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            InitializeNewReport();
        }

        public void InitializeNewReport()
        {
            GenerateNewAlarmReport();
            ChangeCurrentStep(1, true);
            this.AlarmClientState = AlarmClientStateEnum.RegisteringAlarmIncident;
        }

        private void barButtonItemRegisterIncident_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (ValidateInterface() == true)
            {
                this.questionsCctvControl1.Focus();
                GenerateNewIncident();
                ((AlarmFrontClientFormDevX)this.MdiParent).RemoveFromAlarmList(selectedAlarmSensorTelemetryCode);
                CleanForm(true);
                departmentsInvolvedControl.departmentTypesControl.gridViewExDepartmentType.ClearSelection();
                UpdateIncidentListData();
                this.AlarmClientState = AlarmClientStateEnum.WaitingForAlarmIncident;
                //((AlarmFrontClientFormDevX)ParentForm).CloseAlarm(Alarm);
            }
        }

        private void barButtonItemCancelIncident_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DialogResult dialog = MessageForm.Show("CancelIncidentRegistrationCCTV", MessageFormType.WarningQuestion);
            if (dialog.Equals(DialogResult.No))
            {
                return;
            }
            else
            {
                this.barButtonItemRegisterIncident.Enabled = false;
                updatedAnswers.Clear();
                CleanForm(true);
                departmentsInvolvedControl.departmentTypesControl.gridViewExDepartmentType.ClearSelection();
                this.AlarmClientState = AlarmClientStateEnum.WaitingForAlarmIncident;
                ((AlarmFrontClientFormDevX)ParentForm).TakeAlarm(false);
            }
        }

        private void barButtonItemPrint_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            textBoxExIncidentdetails.ShowPrintDialog();
        }

        private void barButtonItemSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            textBoxExIncidentdetails.ShowSaveAsDialog();
        }

        private void barButtonItemRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridControlExIncidentList.SelectedItems.Count > 0)
                OnRefreshIncidentReport();
        }

        private void dockPanel1_DockChanged(object sender, EventArgs e)
        {
            if (dockPanel1.Visible == true && this.Width < this.ParentForm.MinimumSize.Width + dockPanel1.Width)
                this.ParentForm.Width += dockPanel1.Width;
        }

        private void gridViewExIncidentList_SingleSelectionChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            this.selectedIncidentCode = -1;
            if (gridControlExIncidentList.SelectedItems.Count > 0)
            {
                IncidentClientData selectedIncident = ((GridIncidentAlarmData)gridControlExIncidentList.SelectedItems[0]).Tag as IncidentClientData;

                ArrayList parameters = new ArrayList();
                parameters.Add(selectedIncident.Code);

                SelectedIncidentTask selectedIncidentTask = new SelectedIncidentTask(parameters);
                ArrayList tasks = new ArrayList();
                tasks.Add(selectedIncidentTask);

                GeoPoint point = null;
                AddressClientData address = selectedIncident.Address;
                if (address != null && address.Lon != 0 && address.Lat != 0)
                {
                    point = new GeoPoint(address.Lon, address.Lat);
                }
                ThreadPool.QueueUserWorkItem(delegate(object state)
                {
                    try
                    {
                        SelectedIncidentHelp(selectedIncidentTask, point);
                    }
                    catch (Exception ex)
                    {
                        MessageForm.Show(ex.Message, ex);
                    }
                });

                barButtonItemSave.Enabled = true;
                barButtonItemPrint.Enabled = true;
                barButtonItemRefresh.Enabled = true;

            }
            else
            {
                barButtonItemSave.Enabled = false;
                barButtonItemPrint.Enabled = false;
                barButtonItemRefresh.Enabled = false;
                textBoxExIncidentdetails.DocumentText = "<span style='HEIGHT:100%;FONT-SIZE: 10pt; FONT-FAMILY: Sans-Serif'>" + ResourceLoader.GetString2("NoIncidentSelected") + "</span>";
            }
        }

        private void AlarmRegisterFormDevX_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
            }
        }

        private void AlarmRegisterFormDevX_Enter(object sender, EventArgs e)
        {
            this.dockPanel1.Size = new Size(560, this.dockPanel1.Size.Height);
        }
    }
}
