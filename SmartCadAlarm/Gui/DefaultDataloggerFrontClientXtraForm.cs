using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

using DevExpress.XtraLayout;
using DevExpress.XtraGrid.Views.Base;
using System.IO;
using DevExpress.Data;

//using Smartmatic.SmartCad.Map;

using System.Net;
using System.Collections;
using DevExpress.XtraCharts;
using DevExpress.XtraTab;
using System.Threading;
using SmartCadCore.ClientData;
using SmartCadCore.Core;
using SmartCadAlarm.Controls;
using SmartCadCore.Common;
using SmartCadAlarm.Enums;
using Smartmatic.SmartCad.Service;
using SmartCadControls.Controls;
using SmartCadControls;
using SmartCadGuiCommon;
using SmartCadGuiCommon.Services;
//using SmartCadGuiCommon.Services;


namespace SmartCadAlarm.Gui
{
    

    #region Delegates
    public delegate void EnableButtonHandler(object sender, DefaultDataloggerFrontClientXtraForm.EnableButtonsEventArgs e);

    #endregion Delegates

    #region Enums
    public enum Buttons
    {
        barButtonItemStart,
        barButtonItemCancelIncident,
        barButtonItemRegister,
        barButtonItemCancelRegister,
        barButtonItemPlay,
        barButtonItemStop,
        barButtonItemPause,
        barButtonItemPrint,
        barButtonItemSave,
        barButtonItemAso,
        barButtonItemEditPlate,
        barButtonItemConsult

    }

    #endregion Enums

    public partial class DefaultDataloggerFrontClientXtraForm : DevExpress.XtraEditors.XtraForm
    {
        private ConfigurationClientData configurationClient;
        List<ClientData> actualDataloggerData;
        List<ClientData> checkedDataloggerData;
        private NetworkCredential networkCredential;
        private ServerServiceClient serverServiceClient;
        private string password;
        private Dictionary<string, ApplicationPreferenceClientData> globalApplicationPreference;//Falta
        private Dictionary<int, List<DataloggerClientData>> dataloggervalues;
        private object dataloggervaluesLock = new object();
        public XYDiagram diagram;
        public double minValue = 5;
        public double maxValue = 20;

        #region Fields
        private static Color[] chartStripColors = { Color.FromArgb(18,5,250),Color.FromArgb(83,210,51),Color.FromArgb(248,242,16),Color.FromArgb(255,0,0),
												  Color.FromArgb(7,223,248),Color.FromArgb(163,159,168),Color.FromArgb(252,109,11),Color.FromArgb(20,1,20),
												  Color.FromArgb(164,92,40),Color.FromArgb(235,18,219)};
        #endregion Fields

        #region Properties
        public BindingList<AlarmSensorTelemetryGridData> Alarms { get; set; }
        public BindingList<DataloggerGridData> Dataloggers { get; set; }
        public bool ParserDisconnectedMessageShown { get; set; }
        #endregion Properties

        #region Connection
        public ServerServiceClient ServerServiceClient
        {
            get
            {
                return serverServiceClient;
            }
            set
            {
                serverServiceClient = value;
            }
        }
        public NetworkCredential NetworkCredential
        {
            get
            {
                return networkCredential;
            }

            set
            {
                networkCredential = value;
            }
        }
        public ConfigurationClientData ConfigurationClient
        {
            get
            {
                return configurationClient;
            }
            set
            {
                configurationClient = value;
            }
        }
        public String Password
        {
            get
            {
                return password;
            }
            set
            {
                password = value;
            }
        }

        //public void SetPassword(string passwd)
        //{
        //    this.password = passwd;
        //}
        public Dictionary<string, ApplicationPreferenceClientData> GlobalApplicationPreference
        {
            get
            {
                return globalApplicationPreference;
            }
            set
            {
                globalApplicationPreference = value;
            }
        }



        #endregion

        public DefaultDataloggerFrontClientXtraForm()
        {
            InitializeComponent();
            Dataloggers = new BindingList<DataloggerGridData>();
            Alarms = new BindingList<AlarmSensorTelemetryGridData>();
            dataloggervalues = new Dictionary<int, List<DataloggerClientData>>();

            CurrencyDataController.DisableThreadingProblemsDetection = true;

            this.Enter +=
                new EventHandler(DefaultAlarmFrontClientXtraForm_Enter);

            this.Leave += new EventHandler(DefaultDataloggerFrontClientXtraForm_Leave);
            
            ServerServiceClient.GetInstance().CommittedChanges +=
                new EventHandler<CommittedObjectDataCollectionEventArgs>(serverService_CommittedChanges);

           
        }


        

        private void UpdateDataloggerValues(DataloggerClientData data)
        {
            lock (dataloggervaluesLock)
            {
                if (dataloggervalues.ContainsKey(data.CustomCode))
                {
                    dataloggervalues[data.CustomCode].Add(data);
                    UpdateSensorValues(data.CustomCode);
                    //RefreshGraph(tabAlarms.SelectedTab);
                }
            }
        }

        private void UpdateDataloggerValues(int code, List<DataloggerClientData> values)
        {
            lock (dataloggervaluesLock)
            { 
                if (dataloggervalues.ContainsKey(code))
                {
                    dataloggervalues.Remove(code);
                }
                if (values.Count > 0)
                {
                    dataloggervalues.Add(code, values);
                    UpdateSensorValues(code);
                }
            }
        }

        private void UpdateSensorValues(int code)
        {
            List<DataloggerClientData> values = dataloggervalues[code];
            DataloggerClientData newValue = values[values.Count - 1];

            foreach (DataloggerGridData gridData in Dataloggers)
            {
                if (gridData.Data.CustomCode == newValue.CustomCode)
                {
                    foreach (SensorTelemetryClientData gridSensor in gridData.Data.SensorTelemetrys)
                    {
                        foreach (SensorTelemetryClientData sensor in newValue.SensorTelemetrys)
                        {
                            if (gridSensor.Variable == sensor.Variable)
                            {
                                gridSensor.Value = sensor.Value;
                                gridSensor.ValueDate = sensor.ValueDate;
                                break;
                            }
                        }
                    }
                }
            }
            gridControlDatalogger.RefreshDataSource();
            //foreach (AlarmSensorTelemetryGridData gridData in Alarms)
            //{
            //    if (gridData.Sensor.Datalogger.CustomCode == newValue.CustomCode)
            //    {
            //        foreach (SensorTelemetryClientData sensor in newValue.SensorTelemetrys)
            //        {
            //            if (gridData.Alarm.SensorTelemetry.Variable == sensor.Variable)
            //            {
            //                gridData.Alarm.Value = sensor.Value.Value;
            //                gridData.Alarm.StartAlarm = sensor.ValueDate;
            //                break;
            //            }
            //        }
            //    }
            //}
            //gridViewDataloggerAlarms.RefreshData();
        }

        bool dataloggerSensorsGridSelected = false;

        void gridControlDatalogger_FocusedViewChanged(object sender, DevExpress.XtraGrid.ViewFocusEventArgs e)
        {
            
            GridViewEx gridviewTemp = e.View as GridViewEx;
            if (e.View != null)
            {
                dataloggerSensorsGridSelected = (e.View.LevelName == "Sensors");
            }
            else
            {
                dataloggerSensorsGridSelected = false;
            }

            if (dataloggerSensorsGridSelected)
            {
                FormUtil.InvokeRequired(this, delegate
                {
                    if (gridviewTemp.FocusedRowHandle > -1)
                    {
                        SensorTelemetryGridData stg = (SensorTelemetryGridData)gridviewTemp.GetFocusedRow();
                        for (int i = 0; i < gridViewExDatalogger.RowCount; i++)
                        {
                            DataloggerGridData dgd = (DataloggerGridData)gridViewExDatalogger.GetRow(i);
                            if (dgd != null && dgd.Data.CustomCode == stg.DataloggerCustomCode)
                            {
                                gridControlDatalogger.SelectData(dgd);
                                break;
                            }
                        }
                    }
                });
                (this.MdiParent as AlarmFrontClientFormDevX).barButtonItemCreateTelemetryAlarm.Enabled = true;
            }
            else
            {
                //(this.MdiParent as AlarmFrontClientFormDevX).barButtonItemCreateTelemetryAlarm.Enabled = false;
            }
            //tabAlarms_SelectedIndexChanged(null, null);
        }

        void DefaultDataloggerFrontClientXtraForm_Leave(object sender, EventArgs e)
        {
            (this.MdiParent as AlarmFrontClientFormDevX).barButtonItemStartRegIncident.Enabled = false;
            (this.MdiParent as AlarmFrontClientFormDevX).barButtonItemCancelTelemetryAlarm.Enabled = false;
            (this.MdiParent as AlarmFrontClientFormDevX).barButtonItemCreateTelemetryAlarm.Enabled = false;
        }

        private void LoadLanguage()
        {
            this.Text = ResourceLoader.GetString2("Alarms");
            this.tabDataloggers.Text = ResourceLoader.GetString2("Dataloggers");
            this.tabDataloggerAlarms.Text = ResourceLoader.GetString2("Alarms");
            gridViewDataloggerAlarms.Columns["Name"].Caption = ResourceLoader.GetString2("SensorName");

            gridColumnSensor.Caption = ResourceLoader.GetString2("Name");
            gridColumnValue.Caption = ResourceLoader.GetString2("Value");
            gridColumnLow.Caption = ResourceLoader.GetString2("Low");
            gridColumnHigh.Caption = ResourceLoader.GetString2("High");
        }

        #region Style
        private void DefaultAlarmFrontClientXtraForm_Enter(object sender, EventArgs e)
        {
            (this.MdiParent as AlarmFrontClientFormDevX).barButtonItemStartRegIncident.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
            tabAlarms_SelectedIndexChanged(sender, e);
        }
        #endregion Style

        #region Grid
        private void ShowData()
        {
            IList sensors = ServerServiceClient.GetInstance().SearchClientObjects(
                SmartCadHqls.GetAllDataloggers);

            foreach (DataloggerClientData datalogger in sensors)
            {
                DataloggerGridData griddata = CreateDataloggerGridData(datalogger);
                Dataloggers.Add(griddata);
            }

            //FormUtil.InvokeRequired(this, delegate
            //{
                //gridControlDatalogger.BeginUpdate();
                gridControlDatalogger.DataSource = Dataloggers;
                //gridControlDatalogger.EndUpdate();
            //});

            IList alarms = ServerServiceClient.GetInstance().SearchClientObjects(
                SmartCadHqls.GetAllAlarmSensorTelemetrys);

            foreach (AlarmSensorTelemetryClientData alarm in alarms)
            {
                Alarms.Add(new AlarmSensorTelemetryGridData(alarm));
            }

            //FormUtil.InvokeRequired(this, delegate
            //{
                //gridControlAlarmIn.BeginUpdate();
                gridControlAlarmIn.DataSource = Alarms;
                
                //gridControlAlarmIn.EndUpdate();
            //});
        }

        void DefaultDataloggerFrontClientXtraForm_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            GridViewEx gridviewTemp = sender as GridViewEx;
            if (gridviewTemp != null)
            {
                if (gridviewTemp.SelectedRowsCount > 0)
                {
                    if (xtraTabControlChart.TabPages.Count > 0)
                    {
                        FormUtil.InvokeRequired(this, delegate
                        {
                            SensorTelemetryGridData gridData = (SensorTelemetryGridData)gridviewTemp.GetFocusedRow();

                            SelectCurrentChart(gridData.Name);
                        });
                    }
                }
            }
        }

        

        public void SelectCurrentChart(string name)
        {
            foreach (XtraTabPage page in xtraTabControlChart.TabPages)
            {
                if (page.Text == name)
                {
                    xtraTabControlChart.SelectedTabPage = page;
                    break;
                }
            }
        }

        private DataloggerGridData CreateDataloggerGridData(DataloggerClientData datalogger)
        {
            return new DataloggerGridData(datalogger);
        }
        #endregion Grid

        #region UtilClass
        
        #region DataClass
        public class DataloggerGridData : GridControlData
        {
            public DataloggerGridData(DataloggerClientData datalogger)
                :base(datalogger)
            {
            }

            public DataloggerClientData Data
            { 
                get
                {
                    return (DataloggerClientData)Tag;
                }
            }

            [GridControlRowInfoData(Searchable = true)]
            public string Datalogger
            {
                get
                {
                    return Data.Name;
                }
            }

            [GridControlRowInfoData(Searchable = true)]
            public string State
            {
                get { return StructClientData != null ? StructClientData.State : string.Empty; }
            }

            [GridControlRowInfoData(Searchable = true)]
            public string Street
            {
                get { return StructClientData != null ? StructClientData.Street : string.Empty; }
            }

            [GridControlRowInfoData(Searchable = true)]
            public string Town
            {
                get { return StructClientData != null ? StructClientData.Town : string.Empty; ; }
            }

            public StructClientData StructClientData
            {
                get { return Data.StructClientData; }
            }

            [GridControlRowInfoData(Searchable = true)]
            public string Struct
            {
                get
                {
                    if (Data.StructClientData == null)
                    {
                        return string.Empty;
                    }
                    else
                    {
                        return Data.StructClientData.Name;
                    }
                }
            }

            public List<SensorTelemetryGridData> Sensors
            {
                get
                {
                    List<SensorTelemetryGridData> sensors = new List<SensorTelemetryGridData>();

                    foreach (SensorTelemetryClientData clientData in Data.SensorTelemetrys)
                    {
                        foreach (SensorTelemetryThresholdClientData threshold in clientData.Threshold)
                        {
                            SensorTelemetryGridData sensor = new SensorTelemetryGridData(threshold,
                                clientData.Name, 
                                clientData.Variable,
                                clientData.Value == null ? 0 : clientData.Value,
                                threshold.Low == null ? string.Empty : threshold.Low.ToString(), 
                                threshold.High == null ? string.Empty : threshold.High.ToString(),
                                Data.CustomCode);

                            sensors.Add(sensor);
                        }
                    }
                    return sensors;
                }
            }

            public override bool Equals(object obj)
            {
                if (obj is DataloggerGridData)
                {
                    DataloggerGridData device = (obj as DataloggerGridData);

                    if (device.Data.CustomCode == this.Data.CustomCode)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }

        public class AlarmSensorTelemetryGridData : GridControlData
        {
            public AlarmSensorTelemetryGridData(
                AlarmSensorTelemetryClientData alarm)
                :base(alarm)
            {
                Alarm = alarm;
            }

            public AlarmSensorTelemetryClientData Alarm { get; set; }

            public SensorTelemetryClientData Sensor
            {
                get
                {
                    return Alarm.SensorTelemetry;
                }
            }

            [GridControlRowInfoData(Searchable = true)]
            public string Datalogger
            {
                get
                {
                    return Sensor.Datalogger.Name;
                }
            }

            [GridControlRowInfoData(Searchable = true)]
            public string Struct
            {
                get
                {
                    if (Sensor.Datalogger.StructClientData != null)
                        return Sensor.Datalogger.StructClientData.Name;
                    else
                        return string.Empty;
                }
            }

            [GridControlRowInfoData(Searchable = true)]
            public string Name
            {
                get
                {
                    return Sensor.Name;
                }
            }

            [GridControlRowInfoData(Searchable = true)]
            public double Value
            {
                get
                {
                    return Alarm.Value;
                }
            }

            [GridControlRowInfoData(Searchable = true)]
            public DateTime? Date
            {
                get
                {
                    return Alarm.StartAlarm;
                }
            }

            [GridControlRowInfoData(Searchable = true)]
            public string Operator
            {
                get
                {
                    if (Alarm.Operator != null)
                        return Alarm.Operator.ToString();
                    else
                        return string.Empty;
                }
            }

            public override bool Equals(object obj)
            {
                bool result = false;

                if (obj is AlarmSensorTelemetryGridData)
                {
                    if (((AlarmSensorTelemetryGridData)obj).Alarm.Code == this.Alarm.Code)
                        result = true;
                }
                else if (obj is AlarmSensorTelemetryClientData)
                {
                    if (((AlarmSensorTelemetryClientData)obj).Code == this.Alarm.Code)
                        result = true;
                }

                return result;
            }
        }
        #endregion DataClass

        #region EventArgsClass
        public class EnableButtonsEventArgs
        {
            private List<Buttons> buttons = new List<Buttons>();
            private bool enable = true;
            public List<Buttons> Buttons
            {
                get { return buttons; }
                set { buttons = value; }
            }
            public bool Enable
            {
                get { return enable; }
                set { enable = value; }
            }
        }
        #endregion EventArgsClass

        private void DefaultAlarmFrontClientXtraForm_Load(object sender, EventArgs e)
        {
            this.gridControlDatalogger.EnableAutoFilter = true;
            this.gridControlDatalogger.Type = typeof(DataloggerGridData);
            this.gridControlAlarmIn.EnableAutoFilter = true;
            this.gridControlAlarmIn.Type = typeof(AlarmSensorTelemetryGridData);
            ShowData();
            LoadLanguage();
        }

        #endregion UtilClass

        public void LoadInitialData()
        {
            #region Loading ApplicationPreferences
            GlobalApplicationPreference = new Dictionary<string, ApplicationPreferenceClientData>();
            foreach (ApplicationPreferenceClientData preference in
                ServerServiceClient.GetInstance().SearchClientObjectsMaxRows(
                SmartCadHqls.GetAllApplicationPreferences, true))
            {
                GlobalApplicationPreference.Add(preference.Name, preference);
            }
            #endregion
        }


        public void serverService_CommittedChanges(object sender, CommittedObjectDataCollectionEventArgs args)
        {
            try
            {
                if (args != null && args.Objects != null & args.Objects.Count > 0)
                {
                    if (args.Objects[0] is AlarmSensorTelemetryClientData)
                    {
                        AlarmSensorTelemetryClientData alarmClientData =
                                (args.Objects[0] as AlarmSensorTelemetryClientData);
                        
                        if (args.Action == CommittedDataAction.Save)
                        {
                            IList sensor = ServerServiceClient.GetInstance().SearchClientObjects(
                                SmartCadHqls.GetCustomHql(SmartCadHqls.GetSensorTelemetryByCode,
                                    alarmClientData.IdSensorTelemetry));

                            SensorTelemetryClientData sensorTelemetry = (sensor[0] as SensorTelemetryClientData);

                            AlarmSensorTelemetryGridData alarm = new AlarmSensorTelemetryGridData(alarmClientData);
                            FormUtil.InvokeRequired(this, delegate
                            {
                                Alarms.Add(alarm);
                            });
                        }
                        else if (args.Action == CommittedDataAction.Update)
                        {
                            if (alarmClientData.EndAlarm != null)
                            {
                                FormUtil.InvokeRequired(this, delegate
                                {
                                    Alarms.Remove(new AlarmSensorTelemetryGridData(alarmClientData));
                                });
                            }
                            else
                            {
                                FormUtil.InvokeRequired(this, delegate
                                       {
                                           for (int i = 0; i < Alarms.Count; i++)
                                           {
                                               AlarmSensorTelemetryGridData alarm = Alarms[i];

                                               if (alarm.Alarm.Code == alarmClientData.Code)
                                               {
                                                   gridControlAlarmIn.BeginUpdate();
                                                   Alarms[i].Alarm = alarmClientData;
                                                   gridControlAlarmIn.EndUpdate();
                                                   break;
                                               }
                                           }
                                       });
                            }
                        }
                    }
                    else if (args.Objects[0] is DataloggerClientData)
                    {
                        foreach (DataloggerClientData dataloggerClientData in args.Objects)
                        {
                            if (dataloggerClientData.ValuesUpdate)
                            {
                                if (dataloggervalues.ContainsKey(dataloggerClientData.CustomCode))
                                {
                                    
                                    UpdateDataloggerValues(dataloggerClientData);
                                    Thread.Sleep(25000);
                                }
                            }
                            else
                            {
                                DataloggerGridData dataloggerGridData = CreateDataloggerGridData(dataloggerClientData);
                                int index = Dataloggers.IndexOf(dataloggerGridData);

                                FormUtil.InvokeRequired(this, delegate
                                {
                                    gridControlDatalogger.BeginUpdate();
                                    if (index > -1)
                                        Dataloggers[index] = dataloggerGridData;
                                    else
                                        Dataloggers.Add(dataloggerGridData);
                                    gridControlDatalogger.EndUpdate();
                                });
                            }
                        }
                    }
                    else if (args.Objects[0] is SensorTelemetryClientData)
                    {
                        DataloggerGridData dataloggerGridData = null;
                        gridControlDatalogger.BeginUpdate();
                        foreach (SensorTelemetryClientData sensorTelemetryClientData in args.Objects)
                        {
                            if (dataloggerGridData == null)
                            {
                                dataloggerGridData = CreateDataloggerGridData(sensorTelemetryClientData.Datalogger);
                                int index = Dataloggers.IndexOf(dataloggerGridData);

                                if (index > -1)
                                    dataloggerGridData = Dataloggers[index];
                            }

                            int sensorIndex = dataloggerGridData.Data.SensorTelemetrys.IndexOf(sensorTelemetryClientData);

                            dataloggerGridData.Data.SensorTelemetrys[sensorIndex] = sensorTelemetryClientData;
                        }
                        gridControlDatalogger.EndUpdate();
                    }
                    else if (args.Objects[0] is IncidentClientData)
                    {
                        IncidentClientData incidentClientData = (IncidentClientData)args.Objects[0];

                        AlarmSensorTelemetryClientData alarmClientData = ((AlarmReportClientData)incidentClientData.AlarmReports[0]).AlarmSensorTelemetryClient;

                        FormUtil.InvokeRequired(this, delegate
                                {
                                    Alarms.Remove(new AlarmSensorTelemetryGridData(alarmClientData));
                                });
                    }
                } 
                
                FormUtil.InvokeRequired(tabDataloggers, delegate
                {
                    int selectedTabPageIndex = xtraTabControlChart.SelectedTabPageIndex;
                    RefreshGraph(tabAlarms.SelectedTab);
                    xtraTabControlChart.SelectedTabPageIndex = selectedTabPageIndex;
                });
                Thread.Sleep(25000);
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        /// <summary>
        /// Creates the alarm.
        /// </summary>
        public void CreateAlarm()
        {
            if (dataloggerSensorsGridSelected)
            {
                GridViewEx currentView = (GridViewEx)gridControlDatalogger.FocusedView;
                
                object rowData = currentView.GetRow(currentView.FocusedRowHandle);

                SensorTelemetryGridData gridData = (SensorTelemetryGridData)rowData;

                SensorTelemetryClientData stcd = gridData.Data.SensorTelemetry;

                AlarmSensorTelemetryClientData astcd = new AlarmSensorTelemetryClientData();
                //astcd.Operator = ServerServiceClient.OperatorClient;
                astcd.IdSensorTelemetry = stcd.Code;
                astcd.StartAlarm = ServerServiceClient.GetInstance().GetTime();
                astcd.Value = stcd.Value.HasValue ? stcd.Value.Value : 0;

                ServerServiceClient.GetInstance().SaveOrUpdateClientData(astcd);
            }
        }

        public void UpdateAlarm(bool take)
        {
            if (gridControlAlarmIn.SelectedItems.Count > 0)
            {
                AlarmSensorTelemetryGridData tmp = gridControlAlarmIn.SelectedItems[0] as AlarmSensorTelemetryGridData;
                AlarmSensorTelemetryClientData astcd = tmp.Alarm;

                string operatorAttended = tmp.Operator;
                string operatorConnected = ServerServiceClient.GetInstance().OperatorClient.ToString();


                if ((operatorAttended != operatorConnected) && (operatorAttended != ""))
                {
                    ServerServiceClient.GetInstance().SaveOrUpdateClientData(astcd);
                }
                else
                {
                    if (take)
                        astcd.Operator = ServerServiceClient.GetInstance().OperatorClient;
                    else
                        astcd.Operator = null;

                    ServerServiceClient.GetInstance().SaveOrUpdateClientData(astcd);
                }
            }
        }

        public void CancelAlarm()
        {
            if (MessageForm.Show(ResourceLoader.GetString2("CancelTelemetryAlarm"), MessageFormType.Question) == DialogResult.Yes)
            {
                CancelIncidentNotificationForm cancelForm = new CancelIncidentNotificationForm();
                cancelForm.Text = ResourceLoader.GetString2("Alarms");
                if (cancelForm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    if (gridViewDataloggerAlarms.SelectedRowsCount > 0)
                    {
                        AlarmSensorTelemetryGridData tmp = gridControlAlarmIn.SelectedItems[0] as AlarmSensorTelemetryGridData;

                        AlarmSensorTelemetryClientData astcd = tmp.Alarm;
                        astcd.EndAlarm = ServerServiceClient.GetInstance().GetTime();
                        astcd.Operator = ServerServiceClient.GetInstance().OperatorClient;
                        astcd.Observation = cancelForm.Comments;

                        ServerServiceClient.GetInstance().SaveOrUpdateClientData(astcd);
                    }
                }
            }
        }

        internal void ShowDataloggerAlarms()
        {
            tabAlarms.SelectedTab = tabDataloggerAlarms;
        }

        private void tabAlarms_SelectedIndexChanged(object sender, EventArgs e)
        {
            AlarmFrontClientFormDevX parent = (this.MdiParent as AlarmFrontClientFormDevX);
            if (tabAlarms.SelectedTab.Name == "tabDataloggerAlarms")
            {
                if ((parent.MdiChildren[1] as AlarmRegisterFormDevX).AlarmClientState == AlarmClientStateEnum.WaitingForAlarmIncident &&
                gridControlAlarmIn.SelectedItems.Count > 0)
                {
                    parent.barButtonItemStartRegIncident.Enabled = true;
                    parent.barButtonItemCancelTelemetryAlarm.Enabled = true;
                    parent.barButtonItemCreateTelemetryAlarm.Enabled = false;
                }

                if (gridControlAlarmIn.Items.Count > 0)
                {
                    RefreshGraph(tabAlarms.SelectedTab);
                }
            }
            else if (tabAlarms.SelectedTab.Name == "tabDataloggers")
            {
                if (dataloggerSensorsGridSelected)
                {
                    parent.barButtonItemStartRegIncident.Enabled = false;
                    parent.barButtonItemCancelTelemetryAlarm.Enabled = false;
                    parent.barButtonItemCreateTelemetryAlarm.Enabled = true;
                }
                else
                {
                    parent.barButtonItemStartRegIncident.Enabled = false;
                    parent.barButtonItemCancelTelemetryAlarm.Enabled = false;
                    parent.barButtonItemCreateTelemetryAlarm.Enabled = false;
                }

                if (gridControlDatalogger.Items.Count > 0)
                {
                    RefreshGraph(tabAlarms.SelectedTab);
                }
            }
            else
            {
                parent.barButtonItemStartRegIncident.Enabled = false;
                parent.barButtonItemCreateTelemetryAlarm.Enabled = false;
                parent.barButtonItemCancelTelemetryAlarm.Enabled = false;
            }
        }

        private void gridViewExDatalogger_Click(object sender, EventArgs e)
        {
            //CheckForUpdates();
            if (dataloggerSensorsGridSelected)
            {
                (this.MdiParent as AlarmFrontClientFormDevX).barButtonItemCreateTelemetryAlarm.Enabled = true;
            }
            else
            {
                (this.MdiParent as AlarmFrontClientFormDevX).barButtonItemCreateTelemetryAlarm.Enabled = false;
            }
        }

        private void gridViewExDatalogger_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            
            if (e.FocusedRowHandle > -1 && tabAlarms.SelectedTab == tabDataloggers)
            {
                RefreshGraph(tabAlarms.SelectedTab);
            }
        }

        int currentDataloggerChart = 0;

        private void DrawChart(int code)
        {
            //if (code != currentDataloggerChart)
            //{
                currentDataloggerChart = code;

                xtraTabControlChart.TabPages.Clear();

                if (dataloggervalues.ContainsKey(code))
                {
                    Dictionary<string, Series> series = new Dictionary<string, Series>();
                    Dictionary<string, ChartControl> chartControls = new Dictionary<string, ChartControl>();

                    int i = 0;
                    int j = 0;
                    List<Series> chartSeriesPoints = new List<Series>();
                    foreach (DataloggerClientData dcd in dataloggervalues[code].ToArray())
                    {
                        if (i == 0)
                        {
                            foreach (SensorTelemetryClientData stcd in dcd.SensorTelemetrys)
                            {
                                XtraTabPage tabPage = new XtraTabPage();
                                //tabPage.Text = stcd.Variable;

                                ChartControl chartControl = new ChartControl();
                                Point scrollTo = new Point();

                                Series chartSeries = new Series(stcd.Variable, ViewType.Line);
                                chartSeriesPoints.Add(chartSeries);
                                chartSeries.Points.BeginUpdate();
                                chartSeries.Points.Add(new SeriesPoint(stcd.ValueDate, stcd.Value));
                                chartControl.Series.Add(chartSeries);
                                scrollTo = chartControl.AutoScrollOffset;
                                chartControl.RefreshData();
                                chartControl.AutoScrollOffset = scrollTo;

                                // XYDiagram diagram = (XYDiagram)chartControl.Diagram;
                                diagram = (XYDiagram)chartControl.Diagram;


                                diagram.EnableAxisXZooming = true;
                                diagram.EnableAxisYZooming = true;
                                diagram.AxisX.Visible = true;
                                diagram.AxisX.GridLines.Visible = true;
                                diagram.AxisX.Label.Staggered = true;



                                diagram.AxisX.Range.SetInternalMinMaxValues(minValue, maxValue);
                                diagram.AxisX.Range.Auto = false;
                                diagram.EnableAxisXScrolling = true;
                                diagram.EnableAxisYScrolling = true;

                                diagram.ZoomingOptions.UseKeyboard = true;

                                diagram.AxisY.Visible = true;
                                diagram.AxisY.GridLines.Visible = true;
                                diagram.AxisY.Label.Staggered = true;
                                diagram.AxisY.Range.Auto = true;
                                

                                chartControl.Legend.Visible = false;

                                chartControls.Add(stcd.Variable, chartControl);

                                chartControl.Dock = DockStyle.Fill;

                                tabPage.Controls.Add(chartControl);

                               // xtraTabControlChart.TabPages.Add(tabPage);

                                foreach (DataloggerGridData stgd in Dataloggers)
                                {
                                    if (stgd.Data.CustomCode == dcd.CustomCode)
                                    {
                                        foreach (SensorTelemetryGridData sensor in stgd.Sensors)
                                        {
                                            if (sensor.Variable == stcd.Variable)
                                            {
                                                xtraTabControlChart.TabPages.Add(tabPage);
                                                tabPage.Text = sensor.Name;
                                                Strip strip = new Strip();
                                                strip.ShowInLegend = true;
                                                strip.LegendText = sensor.Name;
                                                strip.Color = chartStripColors[j++];

                                                if (j == chartStripColors.Length)
                                                    j = 0;

                                                strip.MinLimit.Enabled = sensor.Data.Low.HasValue;
                                                if (strip.MinLimit.Enabled)
                                                    strip.MinLimit.AxisValueSerializable = sensor.Data.Low.Value.ToString();

                                                strip.MaxLimit.Enabled = sensor.Data.High.HasValue;
                                                if (strip.MaxLimit.Enabled)
                                                    strip.MaxLimit.AxisValueSerializable = sensor.Data.High.Value.ToString();
                                                diagram.AxisY.Strips.Add(strip);
                                            }
                                        }
                                    }
                                }

                                i++;
                            }
                        }

                        foreach (SensorTelemetryClientData stcd in dcd.SensorTelemetrys)
                        {
                            chartControls[stcd.Variable].Series[stcd.Variable].Points.Add(new SeriesPoint(stcd.ValueDate, stcd.Value));
                        }
                    }

                    foreach (Series seriesPoints in chartSeriesPoints)
                    {
                        seriesPoints.Points.EndUpdate();
                    }
                }
            //}
        }

        private void gridControlAlarmIn_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            
            if (e.FocusedRowHandle > -1 && tabAlarms.SelectedTab == tabDataloggerAlarms)
            {
                RefreshGraph(tabAlarms.SelectedTab);
            }
        }

        public void RefreshGraph(TabPage tabPage)
        {

            if (tabPage == tabDataloggerAlarms)
            {
                if (gridControlAlarmIn.SelectedItems.Count > 0)
                {
                    AlarmSensorTelemetryGridData alarmSensorTelemetryGridData = (AlarmSensorTelemetryGridData)gridControlAlarmIn.SelectedItems[0];
                    DataloggerClientData dataloggerGridData = alarmSensorTelemetryGridData.Sensor.Datalogger;
                    if (dataloggerGridData.StructClientData != null)
                        ApplicationServiceClient.Current(UserApplicationClientData.Map).DisplayGeoPointInGis(new GeoPoint(dataloggerGridData.StructClientData.Lon, dataloggerGridData.StructClientData.Lat));
                    if (dataloggervalues.ContainsKey(dataloggerGridData.CustomCode) == false)
                    {
                        List<ClientData> result = serverServiceClient.GetHistory(DateTime.Now.Subtract(TimeSpan.FromHours(1000)), DateTime.Now, "Datalogger", dataloggerGridData.CustomCode.ToString(), 0, 1000);
                        actualDataloggerData = result;
                        //int min = 0;
                        //int max = 1000;
                        //List<ClientData.ClientData> temp = new List<ClientData.ClientData>();
                        //List<ClientData.ClientData> result = null;
                        //while ((temp = ServerServiceClient.GetInstance().GetHistory(DateTime.Now.Subtract(TimeSpan.FromHours(1000)), DateTime.Now, "Datalogger", dataloggerGridData.CustomCode.ToString(), min, max)).Count > 0)
                        //{
                        //    if (result == null)
                        //        result = new List<ClientData.ClientData>();
                        //    result.AddRange(temp);
                        //    min = max;
                        //    max += 1000;
                        //}
                        if (result != null)
                        {
                            List<DataloggerClientData> result2 = result.ConvertAll<DataloggerClientData>(st => (DataloggerClientData)st);
                            UpdateDataloggerValues(dataloggerGridData.CustomCode, result2);
                            DrawChart(dataloggerGridData.CustomCode);
                        }
                        else
                        {
                            if (ParserDisconnectedMessageShown == false)
                            {
                                MessageForm.Show(ResourceLoader.GetString2("NoAvlConnectionStringCofigurated"), MessageFormType.Information);
                                ParserDisconnectedMessageShown = true;
                            }
                        }
                    }
                    else
                    {
                        List<ClientData> result = serverServiceClient.GetHistory(DateTime.Now.Subtract(TimeSpan.FromHours(72)), DateTime.Now, "Datalogger", dataloggerGridData.CustomCode.ToString(), 0, 1000);
                        actualDataloggerData = result;
                        if (result != null)
                        {
                            List<DataloggerClientData> result2 = result.ConvertAll<DataloggerClientData>(st => (DataloggerClientData)st);
                            UpdateDataloggerValues(dataloggerGridData.CustomCode, result2);
                            DrawChart(dataloggerGridData.CustomCode);
                        }
                        //  DrawChart(dataloggerGridData.CustomCode);
                    }
                    SelectCurrentChart(alarmSensorTelemetryGridData.Sensor.Name);
                }
            }
            else if (tabPage == tabDataloggers)
            {
                if (gridControlDatalogger.SelectedItems.Count > 0)
                {
                    DataloggerGridData dataloggerGridData = (DataloggerGridData)gridControlDatalogger.SelectedItems[0];
                    if (dataloggerGridData.StructClientData != null)
                        ApplicationServiceClient.Current(UserApplicationClientData.Map).DisplayGeoPointInGis(new GeoPoint(dataloggerGridData.StructClientData.Lon, dataloggerGridData.StructClientData.Lat));
                    if (dataloggervalues.ContainsKey(dataloggerGridData.Data.CustomCode) == false)
                    {
                        List<ClientData> result = serverServiceClient.GetHistory(DateTime.Now.Subtract(TimeSpan.FromHours(72)), DateTime.Now, "Datalogger", dataloggerGridData.Data.CustomCode.ToString(), 0, 1000);
                        actualDataloggerData = result;
                        //int min = 0;
                        //int max = 1000;
                        //List<ClientData.ClientData> temp = new List<ClientData.ClientData>();
                        //List<ClientData.ClientData> result = null;
                        //while ((temp = ServerServiceClient.GetInstance().GetHistory(DateTime.Now.Subtract(TimeSpan.FromHours(1000)), DateTime.Now, "Datalogger", dataloggerGridData.Data.CustomCode.ToString(), min, max)).Count > 0)
                        //{
                        //    if (result == null)
                        //        result = new List<ClientData.ClientData>();
                        //    result.AddRange(temp);
                        //    min = max;
                        //    max += 1000;
                        //}
                        if (result != null)
                        {
                            List<DataloggerClientData> result2 = result.ConvertAll<DataloggerClientData>(st => (DataloggerClientData)st);
                            UpdateDataloggerValues(dataloggerGridData.Data.CustomCode, result2);
                            DrawChart(dataloggerGridData.Data.CustomCode);
                        }
                        else
                        {
                            if (ParserDisconnectedMessageShown == false)
                            {
                                MessageForm.Show(ResourceLoader.GetString2("NoAvlConnectionStringCofigurated"), MessageFormType.Information);
                                ParserDisconnectedMessageShown = true;
                            }
                        }
                    }
                    else
                    {


                        List<ClientData> result = serverServiceClient.GetHistory(DateTime.Now.Subtract(TimeSpan.FromHours(72)), DateTime.Now, "Datalogger", dataloggerGridData.Data.CustomCode.ToString(), 0, 1000);
                        actualDataloggerData = result;
                        if (result != null)
                        {
                            List<DataloggerClientData> result2 = result.ConvertAll<DataloggerClientData>(st => (DataloggerClientData)st);
                            UpdateDataloggerValues(dataloggerGridData.Data.CustomCode, result2);
                            DrawChart(dataloggerGridData.Data.CustomCode);
                        }


                    }

                //DefaultDataloggerFrontClientXtraForm_FocusedRowChanged(null, null);
               }

            }
            }

            
        }

    }
