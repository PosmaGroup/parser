using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Net;

using System.Collections;
using System.Threading;
using DevExpress.XtraBars;
using DevExpress.XtraCharts;
using SmartCadCore.Core;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadAlarm.Util;
using SmartCadAlarm.Enums;
using Smartmatic.SmartCad.Service;
using SmartCadCore.Enums;

namespace SmartCadAlarm.Gui
{
    public partial class AlarmFrontClientFormDevX : XtraForm
    {
        #region Fields

        DefaultDataloggerFrontClientXtraForm defaultDataloggerFrontClientXtraForm;
        //DefaultLprFrontClientXtraForm defaultLprFrontClientXtraForm;
        AlarmRegisterFormDevX alarmRegisterFormDevX;
        IList dataloggerIncidentList = null;
        IList lprIncidentList = null;
        LprAlarmGridData lprRowSelected;

        private ApplicationMessageClientData alertControlAmcd = null;

        private DateTime syncServerDateTime;
        private TimeSpan sinceLoggedIn;
        private const int GLOBAL_TIMER_PERIOD = 10000;
        private readonly TimeSpan ADD_TIMESPAN;
        public System.Threading.Timer globalTimer;
        private string labelUserCaption = "";
        private string labelConnectedCaption = "";

        ServerServiceClient serverServiceClient = null;

        #endregion

        #region Properties

        public ServerServiceClient ServerServiceClient
        {
            get
            {
                return defaultDataloggerFrontClientXtraForm.ServerServiceClient;
            }

            set
            {
                defaultDataloggerFrontClientXtraForm.ServerServiceClient = value;
            }
        }

        public NetworkCredential NetworkCredential
        {
            get
            {
                return defaultDataloggerFrontClientXtraForm.NetworkCredential;
            }
            set
            {
                defaultDataloggerFrontClientXtraForm.NetworkCredential = value;
            }
        }

        public ConfigurationClientData ConfigurationClient
        {
            set
            {
                defaultDataloggerFrontClientXtraForm.ConfigurationClient = value;
            }
        }

        #endregion

        public AlarmFrontClientFormDevX()
        {
            var applications = ServerServiceClient.GetInstance().GetActiveApplications();
            InitializeComponent();
            if (!applications.Contains(SmartCadApplications.SmartCadMap)) 
            {
                ribbonPageGroupMap.Visible = false;
            }
            CustomInitializeComponent();

            defaultDataloggerFrontClientXtraForm = new DefaultDataloggerFrontClientXtraForm();
            defaultDataloggerFrontClientXtraForm.MdiParent = this;
            //defaultLprFrontClientXtraForm = new DefaultLprFrontClientXtraForm();
            //defaultLprFrontClientXtraForm.MdiParent = this;

            LoadLanguage();

            
            ServerServiceClient.GetInstance().CommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(FrontClientFormDevX_CommittedChanges);
            this.ribbonControl1.MinimizedChanged += new EventHandler(RibbonControl_MinimizedChanged);
            //dataloggerIncidentList = ServerServiceClient.GetInstance().SearchClientObjectsMaxRows(
            //  SmartCadHqls.GetCustomHql(
            //  SmartCadHqls.NewIncidentRule, UserApplicationClientData.Alarm.Code), true);
            //
            //lprIncidentList = ServerServiceClient.GetInstance().SearchClientObjectsMaxRows(
            // SmartCadHqls.GetCustomHql(
            // SmartCadHqls.NewIncidentRule, UserApplicationClientData.Alarm.Code), true);
            
            ADD_TIMESPAN = new TimeSpan(0, 0, 0, 0, GLOBAL_TIMER_PERIOD);

        }

        private void CustomInitializeComponent()
        {
#if DEBUG
            this.barButtonItemSimulateDvrAlarm.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
          //  this.barButtonItemSimulateLprAlarm.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
#else
            this.barButtonItemSimulateDvrAlarm.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
           // this.barButtonItemSimulateLprAlarm.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
# endif
        }

        void RibbonControl_MinimizedChanged(object sender, EventArgs e)
        {
            if (this.ribbonControl1.Minimized == true)
            {
                this.ribbonControl1.Minimized = false;
            }
        }

        private void LoadLanguage()
        {
            barButtonItemOpenMap.Caption = ResourceLoader.GetString2("Open");
            barButtonItemPrint.Caption = ResourceLoader.GetString2("Print");
            barButtonItemRefresh.Caption = ResourceLoader.GetString2("Refresh");
            barButtonItemSave.Caption = ResourceLoader.GetString2("Save");
            barButtonItemCancelIncident.Caption = ResourceLoader.GetString2("Cancel");
            barButtonItemCreateNewIncident.Caption = ResourceLoader.GetString2("Register");
            barButtonItemCreateTelemetryAlarm.Caption = ResourceLoader.GetString2("Generate");
            ribbonPageGroupIncident.Text = ResourceLoader.GetString2("Incidents");
            ribbonPageGroupGeneralOptions.Text = ResourceLoader.GetString2("GeneralOptions");
            barButtonItemStartRegIncident.Caption = ResourceLoader.GetString2("New");
            Text = ResourceLoader.GetString2("AlarmForm");
            ribbonPage1.Text = ResourceLoader.GetString2("Alarm");
            xtraTabbedMdiManager1.Pages[defaultDataloggerFrontClientXtraForm].Text = ResourceLoader.GetString2("Datalogger");
           // barButtonItemSimulateLprAlarm.Caption = ResourceLoader.GetString2("SimulateLprAlarm");
            barButtonItemSimulateDvrAlarm.Caption = ResourceLoader.GetString2("SimulateDvrAlarm");
            barButtonZoomIn.Caption = ResourceLoader.GetString2("ZoomIn");
            barButtonItemZoomOut.Caption = ResourceLoader.GetString2("ZoomOut");
            //barButtonItemOpenMap.Glyph = ResourceLoader.GetImage("$Image.OpenMap");
            //barButtonItemOpenMap.LargeGlyph = ResourceLoader.GetImage("$Image.OpenMap");

            ribbonPageGroupMap.Text = ResourceLoader.GetString2("Maps");
            barButtonItemMap.Caption = ResourceLoader.GetString2("Open");

            barButtonItemCancelTelemetryAlarm.Caption = ResourceLoader.GetString2("Cancel");
            
            

            //barButtonItemCancelTelemetryAlarm.LargeGlyph = ResourceLoader.GetImage("ImageCancelAlarm");

            ribbonPageGroupAlarm.Text = ResourceLoader.GetString2("Alarms");

            #region Footer
            labelUserCaption = ResourceLoader.GetString2("User") + ": ";
            labelConnectedCaption = ResourceLoader.GetString2("Connected") + ": ";
            #endregion
        }

        private void FillStatusStrip()
        {
            syncServerDateTime = ServerServiceClient.GetInstance().GetTime();
            sinceLoggedIn = TimeSpan.Zero;
            this.labelConnected.Caption = "00:00";

            labelDate.Caption = ApplicationUtil.GetFormattedDate(serverServiceClient.GetTime());
            globalTimer = ApplicationUtil.RecurrentExecuteMethod(
                new SimpleDelegate(
                delegate
                {
                    globalTimer_Tick(null, null);
                }), null, null, GLOBAL_TIMER_PERIOD, GLOBAL_TIMER_PERIOD);

            labelUser.Caption = labelUserCaption + serverServiceClient.OperatorClient.FirstName
                + " " + serverServiceClient.OperatorClient.LastName;
            labelConnected.Caption = labelConnectedCaption + "00:00";
        }

        private void globalTimer_Tick(object sender, EventArgs e)
        {
            syncServerDateTime = syncServerDateTime.AddMilliseconds((double)GLOBAL_TIMER_PERIOD);
            sinceLoggedIn = sinceLoggedIn.Add(ADD_TIMESPAN);
            FormUtil.InvokeRequired(this, delegate
            {
                labelDate.Caption = ApplicationUtil.GetFormattedDate(serverServiceClient.GetTime());
                labelConnected.Caption = labelConnectedCaption + Math.Truncate(sinceLoggedIn.TotalHours).ToString().PadLeft(2, '0') + ":" + sinceLoggedIn.Minutes.ToString().PadLeft(2, '0');
            });
        }

        private void FrontClientFormDevX_CommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
        {
            try
            {
                if (e != null && e.Objects != null && e.Objects.Count > 0)
                {
                    if (e.Objects[0] is AlarmSensorTelemetryClientData)
                    {
                    }
                }
            }
            catch
            {
            }
        }

        public void SetPassword(string p)
        {
            defaultDataloggerFrontClientXtraForm.Password = p;
            ServerServiceClient.GetInstance().OperatorClient.Password = p;
        }

        private void FrontClientFormDevX_Load(object sender, EventArgs e)
        {
            CheckAccessToMap();
            xtraTabbedMdiManager1.Pages[defaultDataloggerFrontClientXtraForm].ShowCloseButton = DevExpress.Utils.DefaultBoolean.False;
            //xtraTabbedMdiManager1.Pages[defaultLprFrontClientXtraForm].ShowCloseButton = DevExpress.Utils.DefaultBoolean.True;

            alarmRegisterFormDevX = new AlarmRegisterFormDevX();
            alarmRegisterFormDevX.MdiParent = this;
            alarmRegisterFormDevX.ServerServiceClient = this.ServerServiceClient;
            alarmRegisterFormDevX.LoadInitialData(dataloggerIncidentList);
            alarmRegisterFormDevX.Visible = true;

            defaultDataloggerFrontClientXtraForm.Show();
            //defaultLprFrontClientXtraForm.Show();
            repositoryItemProgressBar1.Step = 1;
            repositoryItemProgressBar1.Maximum = int.Parse(defaultDataloggerFrontClientXtraForm.GlobalApplicationPreference["AlarmTimeLimit"].Value);
            this.defaultDataloggerFrontClientXtraForm.gridViewDataloggerAlarms.Click += new EventHandler(gridView1_Click);
            barButtonItemCreateNewIncident.Enabled = false;
            //barButtonItemStartRegIncident.LargeGlyph = ResourceLoader.GetImage("StartRegistry");
            this.WindowState = FormWindowState.Maximized;
        }

        private void gridView1_Click(object sender, EventArgs e)
        {
            if (defaultDataloggerFrontClientXtraForm.ActiveControl != null)
            {
                if (defaultDataloggerFrontClientXtraForm.gridViewDataloggerAlarms.FocusedRowHandle >= 0)
                {
                    if (alarmRegisterFormDevX.AlarmClientState == AlarmClientStateEnum.WaitingForAlarmIncident) {
                        this.barButtonItemStartRegIncident.Enabled = true;
                    }
                }
            }
            //else if (defaultLprFrontClientXtraForm.ActiveControl != null)
            //{
            //    if (defaultLprFrontClientXtraForm.gridView1.FocusedRowHandle >= 0)
            //    {
            //        lprRowSelected = (defaultLprFrontClientXtraForm.gridView1.GetRow(defaultLprFrontClientXtraForm.gridView1.FocusedRowHandle)
            //           as LprAlarmGridData);
            //        this.barButtonItemStartRegIncident.Enabled = true;


            //        defaultLprFrontClientXtraForm.SetDefaultControl(lprRowSelected);
            //    }
            //}
        }

        private DefaultDataloggerFrontClientXtraForm.AlarmSensorTelemetryGridData GetSelectedAlarm()
        {
            int rowHandle = defaultDataloggerFrontClientXtraForm.gridViewDataloggerAlarms.FocusedRowHandle;
            return (defaultDataloggerFrontClientXtraForm.gridViewDataloggerAlarms.GetRow(rowHandle)
                       as DefaultDataloggerFrontClientXtraForm.AlarmSensorTelemetryGridData);
        }

        private void FrontClientFormDevX_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (EnsureLogout() == false)
            {
                e.Cancel = true;
            }
            else
            {
                alarmRegisterFormDevX.Dispose();
                defaultDataloggerFrontClientXtraForm.Dispose();
            }

        }

        private bool EnsureLogout()
        {
            DialogResult dialogResult = MessageForm.Show(ResourceLoader.GetString2("$Message.ExitApplication"), MessageFormType.Question);

            return dialogResult == DialogResult.Yes;
        }

        private void CheckAccessToMap()
        {
            if (ServerServiceClient.GetInstance().CheckAccessClient(UserResourceClientData.Application,
                UserActionClientData.Basic, UserApplicationClientData.Map, false) == true)
                //this.barButtonItemOpenMap.Enabled = true;
                ribbonPageGroupMap.Ribbon.Enabled = true;
            else
                //this.barButtonItemOpenMap.Enabled = false;
                ribbonPageGroupMap.Ribbon.Enabled = false;
        }




        private void barButtonItemExit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            defaultDataloggerFrontClientXtraForm.Dispose();
            this.Close();
        }

        public void barButtonItemHelp_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                ApplicationUtil.LaunchHelp(SmartCadConfiguration.DistFolder + "/Help/SmartCad.chm");
            }
            catch (Exception ex)
            {
                MessageForm.Show(ResourceLoader.GetString2("$Message.HelpNotInstalled"), ex);
            }
        }

        public void LoadInitialData()
        {
            defaultDataloggerFrontClientXtraForm.LoadInitialData();
            serverServiceClient = ServerServiceClient.GetInstance();
            FillStatusStrip();
        }

        private void barButtonItemOpenMap_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                ApplicationUtil.LaunchApplicationWithArguments(
                        SmartCadConfiguration.SmartCadMapFilename,
                        new object[] { 
                            ServerServiceClient.GetInstance().OperatorClient.Login, 
                            this.defaultDataloggerFrontClientXtraForm.Password, 
                            ApplicationUtil.CheckPrimaryScreenBounds(this.Left),
                            new SendActivateLayerEventArgs(ShapeType.Struct),
                            });
            }
            catch (Exception ex)
            {
                MessageForm.Show(ex.Message, ex);
            }
        }


        private void barButtonItemStartRegIncident_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (alarmRegisterFormDevX.AlarmClientState == AlarmClientStateEnum.RegisteringAlarmIncident)
            {
                MessageForm.Show(ResourceLoader.GetString2("CurrentlyRegistering"), MessageFormType.Warning);
                alarmRegisterFormDevX.Show();
                alarmRegisterFormDevX.Activate();
                return;
            }
            else
            {
                if (defaultDataloggerFrontClientXtraForm.ActiveControl != null)
                {
                    DefaultDataloggerFrontClientXtraForm.AlarmSensorTelemetryGridData alarm = GetSelectedAlarm();
                    if (alarm.Sensor.Datalogger.StructClientData != null)
                    {
                        barButtonItemStartRegIncident.Enabled = false;
                        alarmRegisterFormDevX.LoadDataloggerAlarmData(GetSelectedAlarm());
                        SetDataloggerView();
                    }
                    else
                    {
                        MessageForm.Show(ResourceLoader.GetString2("DataloggerWithoutStruct"), MessageFormType.Warning);
                        return;
                    }
                }
                //else if (defaultLprFrontClientXtraForm.ActiveControl != null)
                //{
                //    alarmRegisterFormDevX.LoadLprAlarmData(lprRowSelected);
                //    SetLprView();
                //}
            }
            try
            {
                TakeAlarm(true);
                alarmRegisterFormDevX.AlarmClientState = AlarmClientStateEnum.RegisteringAlarmIncident;
                alarmRegisterFormDevX.InitializeNewReport();
                alarmRegisterFormDevX.Show();
                alarmRegisterFormDevX.Activate();
            }
            catch (Exception ex)
            {
                MessageForm.Show(ex.Message, ex);
            }
        }

        public void TakeAlarm(bool take)
        {
            defaultDataloggerFrontClientXtraForm.UpdateAlarm(take);
        }

        private void SetDataloggerView()
        {
            alarmRegisterFormDevX.layoutControlItemLPr.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            alarmRegisterFormDevX.layoutControlItemDatalogger.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
        }

        private void SetLprView()
        {
            alarmRegisterFormDevX.layoutControlItemLPr.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            alarmRegisterFormDevX.layoutControlItemDatalogger.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
        }

        private void RemoveSelectedAlarm()
        {
            //if (defaultLprFrontClientXtraForm.ActiveControl != null)
            //{
            //    LprAlarmGridData selected =
            //        (defaultLprFrontClientXtraForm.gridView1.GetRow((defaultLprFrontClientXtraForm.gridView1.GetSelectedRows())[0])
            //        as LprAlarmGridData);

            //    defaultLprFrontClientXtraForm.DeviceList.Remove(selected);
            //}
        }

        #region SimulateAlarm
        //private void barButtonItemSimulateLprAlarm_ItemClick(object sender, ItemClickEventArgs e)
        //{
        //    defaultLprFrontClientXtraForm.Show();
        //    defaultLprFrontClientXtraForm.SimulateAlarm();
        //}

        private void barButtonItemSimulateDvrAlarm_ItemClick(object sender, ItemClickEventArgs e)
        {
            defaultDataloggerFrontClientXtraForm.Show();
            defaultDataloggerFrontClientXtraForm.CreateAlarm();
        }
        #endregion SimulateAlarm

        private void barButtonItemCreateTelemetryAlarm_ItemClick(object sender, ItemClickEventArgs e)
        {
            defaultDataloggerFrontClientXtraForm.Show();
            defaultDataloggerFrontClientXtraForm.CreateAlarm();
            defaultDataloggerFrontClientXtraForm.ShowDataloggerAlarms();
        }

        private void barButtonItemCancelTelemetryAlarm_ItemClick(object sender, ItemClickEventArgs e)
        {
            defaultDataloggerFrontClientXtraForm.CancelAlarm();
        }

        internal void RemoveFromAlarmList(int alarmSensorTelemetryCode)
        {
            foreach (DefaultDataloggerFrontClientXtraForm.AlarmSensorTelemetryGridData gridData in defaultDataloggerFrontClientXtraForm.gridControlAlarmIn.Items) 
            {
                if (gridData.Tag.Code == alarmSensorTelemetryCode)
                {
                    defaultDataloggerFrontClientXtraForm.Alarms.Remove(gridData);
                    return;
                }
            }
        }

        private void barButtonZoomIn_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (defaultDataloggerFrontClientXtraForm.minValue - 1 != 0)
            {
                defaultDataloggerFrontClientXtraForm.minValue = defaultDataloggerFrontClientXtraForm.minValue - 1;
                defaultDataloggerFrontClientXtraForm.maxValue = defaultDataloggerFrontClientXtraForm.maxValue - 4;
                if (defaultDataloggerFrontClientXtraForm.xtraTabControlChart.SelectedTabPage != null) 
                {
                    string namePage = defaultDataloggerFrontClientXtraForm.xtraTabControlChart.SelectedTabPage.Text;

                    defaultDataloggerFrontClientXtraForm.RefreshGraph(defaultDataloggerFrontClientXtraForm.tabAlarms.SelectedTab);

                    defaultDataloggerFrontClientXtraForm.SelectCurrentChart(namePage);
                }
                


            }

        }

        private void barButtonItemZoomOut_ItemClick(object sender, ItemClickEventArgs e)
        {
            defaultDataloggerFrontClientXtraForm.minValue = defaultDataloggerFrontClientXtraForm.minValue + 1;
            defaultDataloggerFrontClientXtraForm.maxValue = defaultDataloggerFrontClientXtraForm.maxValue + 4;
           if (defaultDataloggerFrontClientXtraForm.xtraTabControlChart.SelectedTabPage!=null) 
            {
                string namePage = defaultDataloggerFrontClientXtraForm.xtraTabControlChart.SelectedTabPage.Text;

                defaultDataloggerFrontClientXtraForm.RefreshGraph(defaultDataloggerFrontClientXtraForm.tabAlarms.SelectedTab);

                defaultDataloggerFrontClientXtraForm.SelectCurrentChart(namePage);
            }
            
        }
    }
}