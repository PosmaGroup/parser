using SmartCadAlarm.Controls;
using SmartCadControls;


namespace SmartCadAlarm.Gui
{
    partial class DefaultDataloggerFrontClientXtraForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.gridViewExSensors = new SmartCadControls.GridViewEx();
            this.gridColumnSensor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnLow = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnHigh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControlDatalogger = new SmartCadControls.GridControlEx();
            this.gridViewExDatalogger = new SmartCadControls.GridViewEx();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabAlarms = new System.Windows.Forms.TabControl();
            this.tabDataloggers = new System.Windows.Forms.TabPage();
            this.tabDataloggerAlarms = new System.Windows.Forms.TabPage();
            this.gridControlAlarmIn = new SmartCadControls.GridControlEx();
            this.gridViewDataloggerAlarms = new SmartCadControls.GridViewEx();
            this.layoutControlAlarms = new DevExpress.XtraLayout.LayoutControl();
            this.xtraTabControlChart = new DevExpress.XtraTab.XtraTabControl();
            this.layoutControlGroupAlarms = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemGrids = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemChart = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExSensors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlDatalogger)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExDatalogger)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            this.tabAlarms.SuspendLayout();
            this.tabDataloggers.SuspendLayout();
            this.tabDataloggerAlarms.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlAlarmIn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDataloggerAlarms)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlAlarms)).BeginInit();
            this.layoutControlAlarms.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControlChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupAlarms)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemGrids)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // gridViewExSensors
            // 
            this.gridViewExSensors.AllowFocusedRowChanged = true;
            this.gridViewExSensors.Appearance.EvenRow.BackColor = System.Drawing.Color.White;
            this.gridViewExSensors.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridViewExSensors.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewExSensors.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExSensors.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewExSensors.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewExSensors.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewExSensors.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExSensors.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewExSensors.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewExSensors.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnSensor,
            this.gridColumnValue,
            this.gridColumnLow,
            this.gridColumnHigh});
            this.gridViewExSensors.EnablePreviewLineForFocusedRow = false;
            this.gridViewExSensors.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridViewExSensors.GridControl = this.gridControlDatalogger;
            this.gridViewExSensors.Name = "gridViewExSensors";
            this.gridViewExSensors.OptionsBehavior.Editable = false;
            this.gridViewExSensors.OptionsCustomization.AllowFilter = false;
            this.gridViewExSensors.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExSensors.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewExSensors.OptionsView.ShowGroupPanel = false;
            this.gridViewExSensors.OptionsView.ShowIndicator = false;
            this.gridViewExSensors.ViewTotalRows = false;
            this.gridViewExSensors.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.DefaultDataloggerFrontClientXtraForm_FocusedRowChanged);
            // 
            // gridColumnSensor
            // 
            this.gridColumnSensor.Caption = "gridColumn1";
            this.gridColumnSensor.FieldName = "Name";
            this.gridColumnSensor.Name = "gridColumnSensor";
            this.gridColumnSensor.Visible = true;
            this.gridColumnSensor.VisibleIndex = 0;
            // 
            // gridColumnValue
            // 
            this.gridColumnValue.Caption = "gridColumn2";
            this.gridColumnValue.FieldName = "Value";
            this.gridColumnValue.Name = "gridColumnValue";
            this.gridColumnValue.Visible = true;
            this.gridColumnValue.VisibleIndex = 1;
            // 
            // gridColumnLow
            // 
            this.gridColumnLow.Caption = "gridColumn3";
            this.gridColumnLow.FieldName = "Low";
            this.gridColumnLow.Name = "gridColumnLow";
            this.gridColumnLow.Visible = true;
            this.gridColumnLow.VisibleIndex = 2;
            // 
            // gridColumnHigh
            // 
            this.gridColumnHigh.Caption = "gridColumn4";
            this.gridColumnHigh.FieldName = "High";
            this.gridColumnHigh.Name = "gridColumnHigh";
            this.gridColumnHigh.Visible = true;
            this.gridColumnHigh.VisibleIndex = 3;
            // 
            // gridControlDatalogger
            // 
            this.gridControlDatalogger.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlDatalogger.EnableAutoFilter = false;
            this.gridControlDatalogger.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            gridLevelNode1.LevelTemplate = this.gridViewExSensors;
            gridLevelNode1.RelationName = "Sensors";
            this.gridControlDatalogger.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControlDatalogger.Location = new System.Drawing.Point(2, 2);
            this.gridControlDatalogger.MainView = this.gridViewExDatalogger;
            this.gridControlDatalogger.MinimumSize = new System.Drawing.Size(1248, 299);
            this.gridControlDatalogger.Name = "gridControlDatalogger";
            this.gridControlDatalogger.Size = new System.Drawing.Size(1250, 299);
            this.gridControlDatalogger.TabIndex = 5;
            this.gridControlDatalogger.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExDatalogger,
            this.gridViewExSensors});
            this.gridControlDatalogger.ViewTotalRows = false;
            this.gridControlDatalogger.FocusedViewChanged += new DevExpress.XtraGrid.ViewFocusEventHandler(this.gridControlDatalogger_FocusedViewChanged);
            // 
            // gridViewExDatalogger
            // 
            this.gridViewExDatalogger.AllowFocusedRowChanged = true;
            this.gridViewExDatalogger.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewExDatalogger.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExDatalogger.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewExDatalogger.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewExDatalogger.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewExDatalogger.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExDatalogger.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewExDatalogger.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewExDatalogger.EnablePreviewLineForFocusedRow = false;
            this.gridViewExDatalogger.GridControl = this.gridControlDatalogger;
            this.gridViewExDatalogger.GroupFormat = "[#image]{1} {2}";
            this.gridViewExDatalogger.Name = "gridViewExDatalogger";
            this.gridViewExDatalogger.OptionsBehavior.Editable = false;
            this.gridViewExDatalogger.OptionsCustomization.AllowGroup = false;
            this.gridViewExDatalogger.OptionsMenu.EnableColumnMenu = false;
            this.gridViewExDatalogger.OptionsMenu.EnableFooterMenu = false;
            this.gridViewExDatalogger.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridViewExDatalogger.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExDatalogger.OptionsView.ShowAutoFilterRow = true;
            this.gridViewExDatalogger.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewExDatalogger.OptionsView.ShowGroupPanel = false;
            this.gridViewExDatalogger.OptionsView.ShowIndicator = false;
            this.gridViewExDatalogger.ViewTotalRows = false;
            this.gridViewExDatalogger.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewExDatalogger_FocusedRowChanged);
            this.gridViewExDatalogger.Click += new System.EventHandler(this.gridViewExDatalogger_Click);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.OptionsItemText.TextToControlDistance = 5;
            this.layoutControlGroup2.Size = new System.Drawing.Size(1270, 814);
            this.layoutControlGroup2.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Text = "layoutControlGroup2";
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlItem3.Size = new System.Drawing.Size(1266, 792);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // tabAlarms
            // 
            this.tabAlarms.Controls.Add(this.tabDataloggers);
            this.tabAlarms.Controls.Add(this.tabDataloggerAlarms);
            this.tabAlarms.Location = new System.Drawing.Point(4, 4);
            this.tabAlarms.MinimumSize = new System.Drawing.Size(1262, 300);
            this.tabAlarms.Name = "tabAlarms";
            this.tabAlarms.SelectedIndex = 0;
            this.tabAlarms.Size = new System.Drawing.Size(1262, 323);
            this.tabAlarms.TabIndex = 2;
            this.tabAlarms.SelectedIndexChanged += new System.EventHandler(this.tabAlarms_SelectedIndexChanged);
            // 
            // tabDataloggers
            // 
            this.tabDataloggers.Controls.Add(this.gridControlDatalogger);
            this.tabDataloggers.Location = new System.Drawing.Point(4, 22);
            this.tabDataloggers.Name = "tabDataloggers";
            this.tabDataloggers.Padding = new System.Windows.Forms.Padding(2);
            this.tabDataloggers.Size = new System.Drawing.Size(1254, 297);
            this.tabDataloggers.TabIndex = 0;
            this.tabDataloggers.Text = "Dataloggers";
            this.tabDataloggers.UseVisualStyleBackColor = true;
            // 
            // tabDataloggerAlarms
            // 
            this.tabDataloggerAlarms.Controls.Add(this.gridControlAlarmIn);
            this.tabDataloggerAlarms.Location = new System.Drawing.Point(4, 22);
            this.tabDataloggerAlarms.Name = "tabDataloggerAlarms";
            this.tabDataloggerAlarms.Padding = new System.Windows.Forms.Padding(2);
            this.tabDataloggerAlarms.Size = new System.Drawing.Size(1254, 297);
            this.tabDataloggerAlarms.TabIndex = 1;
            this.tabDataloggerAlarms.Text = "Datalogger Alarms";
            this.tabDataloggerAlarms.UseVisualStyleBackColor = true;
            // 
            // gridControlAlarmIn
            // 
            this.gridControlAlarmIn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlAlarmIn.EnableAutoFilter = false;
            this.gridControlAlarmIn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlAlarmIn.Location = new System.Drawing.Point(2, 2);
            this.gridControlAlarmIn.MainView = this.gridViewDataloggerAlarms;
            this.gridControlAlarmIn.Margin = new System.Windows.Forms.Padding(2);
            this.gridControlAlarmIn.Name = "gridControlAlarmIn";
            this.gridControlAlarmIn.Size = new System.Drawing.Size(1250, 293);
            this.gridControlAlarmIn.TabIndex = 8;
            this.gridControlAlarmIn.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewDataloggerAlarms});
            this.gridControlAlarmIn.ViewTotalRows = false;
            // 
            // gridViewDataloggerAlarms
            // 
            this.gridViewDataloggerAlarms.AllowFocusedRowChanged = true;
            this.gridViewDataloggerAlarms.Appearance.FocusedCell.BackColor = System.Drawing.Color.Transparent;
            this.gridViewDataloggerAlarms.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewDataloggerAlarms.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewDataloggerAlarms.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewDataloggerAlarms.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewDataloggerAlarms.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewDataloggerAlarms.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewDataloggerAlarms.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewDataloggerAlarms.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewDataloggerAlarms.Appearance.Row.BackColor = System.Drawing.Color.Red;
            this.gridViewDataloggerAlarms.Appearance.Row.Options.UseBackColor = true;
            this.gridViewDataloggerAlarms.EnablePreviewLineForFocusedRow = false;
            this.gridViewDataloggerAlarms.GridControl = this.gridControlAlarmIn;
            this.gridViewDataloggerAlarms.GroupFormat = "[#image]{1} {2}";
            this.gridViewDataloggerAlarms.Name = "gridViewDataloggerAlarms";
            this.gridViewDataloggerAlarms.OptionsBehavior.Editable = false;
            this.gridViewDataloggerAlarms.OptionsMenu.EnableColumnMenu = false;
            this.gridViewDataloggerAlarms.OptionsMenu.EnableFooterMenu = false;
            this.gridViewDataloggerAlarms.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridViewDataloggerAlarms.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewDataloggerAlarms.OptionsView.ShowAutoFilterRow = true;
            this.gridViewDataloggerAlarms.OptionsView.ShowDetailButtons = false;
            this.gridViewDataloggerAlarms.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewDataloggerAlarms.OptionsView.ShowGroupPanel = false;
            this.gridViewDataloggerAlarms.OptionsView.ShowIndicator = false;
            this.gridViewDataloggerAlarms.ViewTotalRows = false;
            this.gridViewDataloggerAlarms.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridControlAlarmIn_FocusedRowChanged);
            // 
            // layoutControlAlarms
            // 
            this.layoutControlAlarms.Controls.Add(this.xtraTabControlChart);
            this.layoutControlAlarms.Controls.Add(this.tabAlarms);
            this.layoutControlAlarms.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlAlarms.Location = new System.Drawing.Point(0, 0);
            this.layoutControlAlarms.Margin = new System.Windows.Forms.Padding(2);
            this.layoutControlAlarms.Name = "layoutControlAlarms";
            this.layoutControlAlarms.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(50, 97, 250, 350);
            this.layoutControlAlarms.Padding = new System.Windows.Forms.Padding(2);
            this.layoutControlAlarms.Root = this.layoutControlGroupAlarms;
            this.layoutControlAlarms.Size = new System.Drawing.Size(1270, 750);
            this.layoutControlAlarms.TabIndex = 8;
            this.layoutControlAlarms.Text = "layoutControl1";
            // 
            // xtraTabControlChart
            // 
            this.xtraTabControlChart.Location = new System.Drawing.Point(4, 337);
            this.xtraTabControlChart.MinimumSize = new System.Drawing.Size(1262, 200);
            this.xtraTabControlChart.Name = "xtraTabControlChart";
            this.xtraTabControlChart.Size = new System.Drawing.Size(1262, 409);
            this.xtraTabControlChart.TabIndex = 8;
            // 
            // layoutControlGroupAlarms
            // 
            this.layoutControlGroupAlarms.CustomizationFormText = "layoutControlGroupAlarms";
            this.layoutControlGroupAlarms.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroupAlarms.GroupBordersVisible = false;
            this.layoutControlGroupAlarms.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemGrids,
            this.layoutControlItemChart,
            this.splitterItem1});
            this.layoutControlGroupAlarms.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupAlarms.Name = "Root";
            this.layoutControlGroupAlarms.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupAlarms.Size = new System.Drawing.Size(1270, 750);
            this.layoutControlGroupAlarms.Text = "Root";
            this.layoutControlGroupAlarms.TextVisible = false;
            // 
            // layoutControlItemGrids
            // 
            this.layoutControlItemGrids.Control = this.tabAlarms;
            this.layoutControlItemGrids.CustomizationFormText = "layoutControlItemGrids";
            this.layoutControlItemGrids.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemGrids.Name = "layoutControlItemGrids";
            this.layoutControlItemGrids.Size = new System.Drawing.Size(1266, 327);
            this.layoutControlItemGrids.Text = "layoutControlItemGrids";
            this.layoutControlItemGrids.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemGrids.TextToControlDistance = 0;
            this.layoutControlItemGrids.TextVisible = false;
            // 
            // layoutControlItemChart
            // 
            this.layoutControlItemChart.Control = this.xtraTabControlChart;
            this.layoutControlItemChart.CustomizationFormText = "layoutControlItemChart";
            this.layoutControlItemChart.Location = new System.Drawing.Point(0, 333);
            this.layoutControlItemChart.Name = "layoutControlItemChart";
            this.layoutControlItemChart.Size = new System.Drawing.Size(1266, 413);
            this.layoutControlItemChart.Text = "layoutControlItemChart";
            this.layoutControlItemChart.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemChart.TextToControlDistance = 0;
            this.layoutControlItemChart.TextVisible = false;
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            this.splitterItem1.CustomizationFormText = "splitterItem1";
            this.splitterItem1.Location = new System.Drawing.Point(0, 327);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(1266, 6);
            // 
            // DefaultDataloggerFrontClientXtraForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1270, 750);
            this.ControlBox = false;
            this.Controls.Add(this.layoutControlAlarms);
            this.Name = "DefaultDataloggerFrontClientXtraForm";
            this.Text = "Alarmas";
            this.Load += new System.EventHandler(this.DefaultAlarmFrontClientXtraForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExSensors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlDatalogger)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExDatalogger)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            this.tabAlarms.ResumeLayout(false);
            this.tabDataloggers.ResumeLayout(false);
            this.tabDataloggerAlarms.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlAlarmIn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDataloggerAlarms)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlAlarms)).EndInit();
            this.layoutControlAlarms.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControlChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupAlarms)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemGrids)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        public System.Windows.Forms.TabControl tabAlarms;
        private System.Windows.Forms.TabPage tabDataloggers;
        private GridControlEx gridControlDatalogger;
        private System.Windows.Forms.TabPage tabDataloggerAlarms;
        public GridControlEx gridControlAlarmIn;
        public GridViewEx gridViewDataloggerAlarms;
        private DevExpress.XtraLayout.LayoutControl layoutControlAlarms;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupAlarms;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemGrids;
        private DevExpress.XtraLayout.SplitterItem splitterItem1;
        public GridViewEx gridViewExDatalogger;
        public DevExpress.XtraTab.XtraTabControl xtraTabControlChart;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemChart;
        private GridViewEx gridViewExSensors;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnSensor;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnValue;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnLow;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnHigh;
    }
}