//#define GENERATE
using System;
using System.ComponentModel;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;
using System.Collections;
using System.Resources;
using System.IO;
using System.Diagnostics;
using SmartCadCore.Common;
using SmartCadAlarm.Controls;
using SmartCadControls;
using SmartCadControls.Controls;


namespace SmartCadAlarm.Gui
{
	public class FormBaseEx : Form
	{
       
		/// <summary>
		/// Required designer variable.
		/// </summary>
		protected IContainer components = null;

		public FormBaseEx()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

        #region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.SuspendLayout();
            // 
            // FormBaseEx
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(292, 266);
            this.Name = "FormBaseEx";
            this.TopMost = true;
            this.ResumeLayout(false);

		}

		#endregion
        
        private void AssignResources()
		{
			try
			{
                System.Windows.Forms.Control.ControlCollection col = this.Controls;

				string typeName = this.GetType().FullName + ".";
				
				string text = ResourceLoader.GetString(typeName + "Text");

				if (text != null && text != "")
				{
					this.Text = text;
				}

				Image img = ResourceLoader.GetImage(typeName + "BackgroundImage");

				if (img != null)
				{
					this.BackgroundImage = img;
				}

				Icon icon = ResourceLoader.GetIcon(typeName + "Icon");

				if (icon != null)
				{
					this.Icon = icon;
				}
		
				AssignResourcesMenu(this, typeName);			
				AssignResourcesTooltip(this, typeName);
                AssignResourcesToolStrip(this, typeName);
				AssignResourcesColumnHeaders(this, typeName);
				SearchAllControls(col, typeName);
			}
			catch(Exception ex)
			{
				Console.WriteLine(ex.Message);
			}
		}

		public static void AssignResourcesMenu(object form, string assemblyName)
		{
			Type formtype = form.GetType();
			FieldInfo[] fi = formtype.GetFields(BindingFlags.Public|BindingFlags.NonPublic|BindingFlags.Instance|BindingFlags.Static|BindingFlags.DeclaredOnly);
			Type menutype = typeof(System.Windows.Forms.MenuItem);
			foreach(FieldInfo mi in fi)
			{
				if (menutype.Equals(mi.FieldType))
				{
					MenuItem menu = (MenuItem)formtype.InvokeMember(mi.Name,BindingFlags.DeclaredOnly | 
						BindingFlags.Public | BindingFlags.NonPublic | 
						BindingFlags.Instance | BindingFlags.GetField, null, form, null);
					string text = ResourceLoader.GetString(assemblyName+ mi.Name +".Text");					
					if (text!=null)
						menu.Text = text;
				}
			}
		}

        public static void AssignResourcesToolStrip(object form, string assemblyName)
        {
            Type formtype = form.GetType();
            FieldInfo[] fi = formtype.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static | BindingFlags.DeclaredOnly);
            Type toolStripMenuItem = typeof(ToolStripMenuItem);
            Type toolStripStatusLabel = typeof(ToolStripStatusLabel);
            Type toolStripLabel = typeof(ToolStripLabel);
            Type toolStripButton = typeof(ToolStripButton);
            Type richTextBoxExtended = typeof(RichTextBoxExtended);
            Type openFileDialog = typeof(OpenFileDialog);
            Type toolStripDropButton = typeof(ToolStripDropDownButton);
            Type toolStripComboBox = typeof(ToolStripComboBox);
    
            string text;
            string tooltip;

            foreach (FieldInfo mi in fi)
            {
                if (toolStripMenuItem.Equals(mi.FieldType))
                {
                  ToolStripMenuItem item = (ToolStripMenuItem)formtype.InvokeMember(mi.Name, BindingFlags.DeclaredOnly |
                        BindingFlags.Public | BindingFlags.NonPublic |
                        BindingFlags.Instance | BindingFlags.GetField, null, form, null);
                    text = ResourceLoader.GetString(assemblyName + mi.Name + ".Text");
                    tooltip = ResourceLoader.GetString(assemblyName + mi.Name + ".ToolTipText");
                    
                    if (text != null)
                        item.Text = text;
                    if (tooltip != null)
                        item.ToolTipText = tooltip;
                
                }else if(toolStripStatusLabel.Equals(mi.FieldType)){
                
                    ToolStripStatusLabel item = (ToolStripStatusLabel)formtype.InvokeMember(mi.Name, BindingFlags.DeclaredOnly |
                        BindingFlags.Public | BindingFlags.NonPublic |
                        BindingFlags.Instance | BindingFlags.GetField, null, form, null);
                    text = ResourceLoader.GetString(assemblyName + mi.Name + ".Text");
                             
                    if (text != null)
                        item.Text = text;
                
                }else if(toolStripLabel.Equals(mi.FieldType)){
                
                    ToolStripLabel item = (ToolStripLabel)formtype.InvokeMember(mi.Name, BindingFlags.DeclaredOnly |
                        BindingFlags.Public | BindingFlags.NonPublic |
                        BindingFlags.Instance | BindingFlags.GetField, null, form, null);
                    text = ResourceLoader.GetString(assemblyName + mi.Name + ".Text");
                             
                    if (text != null)
                        item.Text = text;
                
                }else if (toolStripButton.Equals(mi.FieldType)){
                
                    ToolStripButton button = (ToolStripButton)formtype.InvokeMember(mi.Name, BindingFlags.DeclaredOnly |
                        BindingFlags.Public | BindingFlags.NonPublic |
                        BindingFlags.Instance | BindingFlags.GetField, null, form, null);
                    text = ResourceLoader.GetString(assemblyName + mi.Name + ".Text");
                    tooltip = ResourceLoader.GetString(assemblyName + mi.Name + ".ToolTipText");
                    
                    if (text != null)
                        button.Text = text;
                    if (tooltip != null)
                        button.ToolTipText = tooltip;
                
                }else if(toolStripDropButton.Equals(mi.FieldType)){
                     
                     ToolStripDropDownButton button = (ToolStripDropDownButton)formtype.InvokeMember(mi.Name, BindingFlags.DeclaredOnly |
                            BindingFlags.Public | BindingFlags.NonPublic |
                            BindingFlags.Instance | BindingFlags.GetField, null, form, null);
                    
                     text = ResourceLoader.GetString(assemblyName + mi.Name + ".Text");
                     tooltip = ResourceLoader.GetString(assemblyName + mi.Name + ".ToolTipText");
                    
                     if (text != null)
                         button.Text = text;
                     if (tooltip != null)
                         button.ToolTipText = tooltip;

                 } else if (toolStripComboBox.Equals(mi.FieldType))
                 {
                     ToolStripComboBox combo = (ToolStripComboBox)formtype.InvokeMember(mi.Name, BindingFlags.DeclaredOnly |
                                BindingFlags.Public | BindingFlags.NonPublic |
                                BindingFlags.Instance | BindingFlags.GetField, null, form, null);

                     text = ResourceLoader.GetString(assemblyName + mi.Name + ".Text");
                     tooltip = ResourceLoader.GetString(assemblyName + mi.Name + ".ToolTipText");

                     if (text != null)
                         combo.Text = text;
                     if (tooltip != null)
                         combo.ToolTipText = tooltip;
                 
                 }else if (richTextBoxExtended.Equals(mi.FieldType))
                {
                    RichTextBoxExtended rtb = (RichTextBoxExtended)formtype.InvokeMember(mi.Name, BindingFlags.DeclaredOnly |
                           BindingFlags.Public | BindingFlags.NonPublic |
                           BindingFlags.Instance | BindingFlags.GetField, null, form, null);

                    AssignResourcesRichTextBoxExtended(rtb, assemblyName + rtb.Name + ".");

                }
                else if (openFileDialog.Equals(mi.FieldType))
                {
                    OpenFileDialog dialog = (OpenFileDialog)formtype.InvokeMember(mi.Name, BindingFlags.DeclaredOnly |
                               BindingFlags.Public | BindingFlags.NonPublic |
                               BindingFlags.Instance | BindingFlags.GetField, null, form, null);
                    string filter = ResourceLoader.GetString(assemblyName + mi.Name + ".Filter");
                    string title = ResourceLoader.GetString(assemblyName + mi.Name + ".Title");
                    if (filter != null)
                        dialog.Filter = filter;
                    if (title != null)
                        dialog.Title = title;

                }
            }

        }

        private static void AssignResourcesRichTextBoxExtended(RichTextBoxExtended rtb, string assemblyName)
        {

            foreach (ToolStripItem item in rtb.Toolbar.Items)
            {
                if (item is ToolStripButton || item is ToolStripDropDownButton || item is ToolStripComboBox)
                {
                    item.Text = ResourceLoader.GetString(assemblyName + item.Name + ".Text");
                    item.ToolTipText = ResourceLoader.GetString(assemblyName + item.Name + ".ToolTipText");
                   
                }

            }

            foreach (MenuItem item in rtb.RichTextBox.ContextMenu.MenuItems)
            {
                item.Text = ResourceLoader.GetString(assemblyName + item.Name + ".Text");
            }

        }

		public static void AssignResourcesColumnHeaders(object form, string assemblyName)
		{
			Type formtype = form.GetType();
			FieldInfo[] fi = formtype.GetFields(BindingFlags.Public|BindingFlags.NonPublic|BindingFlags.Instance|BindingFlags.Static|BindingFlags.DeclaredOnly);
			Type []columnHeaderType = {typeof(System.Windows.Forms.ColumnHeader), typeof(ColumnHeaderEx)} ;
			foreach(FieldInfo colHeaderInfo in fi)
			{
				if (colHeaderInfo.FieldType.Equals(columnHeaderType[0]))
				{
					string name = colHeaderInfo.Name;
					ColumnHeader columnHeader = (ColumnHeader)formtype.InvokeMember(colHeaderInfo.Name,BindingFlags.DeclaredOnly | 
						BindingFlags.Public | BindingFlags.NonPublic | 
						BindingFlags.Instance | BindingFlags.GetField, null, form, null);
					string text = ResourceLoader.GetString(assemblyName+ colHeaderInfo.Name +".Text");					
					if (text!=null)
						columnHeader.Text = text;
				}
				else if (colHeaderInfo.FieldType.Equals(columnHeaderType[1])) 
				{
					string name = colHeaderInfo.Name;
					IResourceLoadable loadableProp = (IResourceLoadable)formtype.InvokeMember(colHeaderInfo.Name,BindingFlags.DeclaredOnly | 
						BindingFlags.Public | BindingFlags.NonPublic | 
						BindingFlags.Instance | BindingFlags.GetField, null, form, null);
					if (loadableProp.LoadFromResources)
					{
						ColumnHeader columnHeaderEx = (ColumnHeader)formtype.InvokeMember(colHeaderInfo.Name,BindingFlags.DeclaredOnly | 
							BindingFlags.Public | BindingFlags.NonPublic | 
							BindingFlags.Instance | BindingFlags.GetField, null, form, null);
						string text = ResourceLoader.GetString(assemblyName+ colHeaderInfo.Name +".Text");					
						if (text!=null)
							columnHeaderEx.Text = text;
					}
				}
			}						
		}

		private static void AssignResourcesTooltip(object form, string assemblyName)
		{
			Type formtype = form.GetType();
			FieldInfo[] fi = formtype.GetFields(BindingFlags.Public|BindingFlags.NonPublic|BindingFlags.Instance|BindingFlags.Static|BindingFlags.DeclaredOnly);
			Type tttype = typeof(System.Windows.Forms.ToolTip);
			foreach(FieldInfo mi in fi)
			{
				if (tttype.Equals(mi.FieldType))
				{
					ToolTip tt = (ToolTip)formtype.InvokeMember(mi.Name,BindingFlags.DeclaredOnly | 
						BindingFlags.Public | BindingFlags.NonPublic | 
						BindingFlags.Instance | BindingFlags.GetField, null, form, null);
					foreach(FieldInfo allf in fi)
					{
						string tooltip;
						if ((tooltip = ResourceLoader.GetString(assemblyName+mi.Name+"."+allf.Name+".Tooltip"))!=null)
						{
							Object o = formtype.InvokeMember(allf.Name,BindingFlags.DeclaredOnly | 
								BindingFlags.Public | BindingFlags.NonPublic | 
								BindingFlags.Instance | BindingFlags.GetField, null, form, null);
                            tt.SetToolTip((System.Windows.Forms.Control)o, tooltip);
						}
					}
				}
			}
		}

		
		protected override void OnLoad(EventArgs e)
		{	
#if ( GENERATE )
			/*try
			{
				if (!DesignMode)
				{
					if (true)
					{
						ResourceGenerator.Generate(this, this.components != null ? this.components.Components : null, SmartCadConfiguration.ResourceFilename);				
					}
				}
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message+" StackTrace: " + ex.StackTrace, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}*/
#endif
            if (!(this.Site != null && this.Site.DesignMode))
                AssignResources();
			base.OnLoad(e);
			
		}

        private void AssignImage(string imagePropertyName, string controlName, System.Windows.Forms.Control c)
		{
			MemberInfo[] arrayMemberInfo = c.GetType().GetMember(imagePropertyName, MemberTypes.Property, BindingFlags.Public | BindingFlags.Instance);
			if (arrayMemberInfo.Length > 0)
			{
				Image img = ResourceLoader.GetImage(controlName + "." + imagePropertyName);
				if (img != null)
				{
					try
					{
						Type t = c.GetType();
						t.InvokeMember(imagePropertyName,
							BindingFlags.Public | BindingFlags.Instance | BindingFlags.SetProperty,
							null, c, new Object[] {img});
					}
					catch
					{
					}
				}
			}
		}

        private void AssignIcon(string imagePropertyName, string controlName, System.Windows.Forms.Control c)
		{
			MemberInfo[] arrayMemberInfo = c.GetType().GetMember(imagePropertyName, MemberTypes.Property, BindingFlags.Public | BindingFlags.Instance);
			if (arrayMemberInfo.Length > 0)
			{
				Icon icon = ResourceLoader.GetIcon(controlName + "." + imagePropertyName);
				if (icon != null)
				{
					try
					{
						Type t = c.GetType();
						t.InvokeMember(imagePropertyName,
							BindingFlags.Public | BindingFlags.Instance | BindingFlags.SetProperty,
							null, c, new Object[] {icon});
					}
					catch
					{
					}
				}
			}
		}

        private void SearchAllControls(System.Windows.Forms.Control.ControlCollection col, string assemblyName)
		{
            foreach (System.Windows.Forms.Control c in col)
			{
                try
                {
                    IResourceLoadable controlResourceLoadable = c as IResourceLoadable;
                    if (controlResourceLoadable != null)
                    {
                        if (controlResourceLoadable.LoadFromResources)
                        {
                            string controlName = assemblyName + c.Name;
                            string text = ResourceLoader.GetString(controlName + ".Text");
                            c.Text = (text != null && text != "") ? text : c.Text;
                            AssignImage("Image", controlName, c);
                            AssignImage("BackgroundImage", controlName, c);
                        }
                    }
                    else if (!(c is WebBrowser) && !(c is TextBox) && !(c is ComboBox) && !(c is DateTimePicker) && !(c is DataGridView))
                    {
                        string controlName = assemblyName + c.Name;
                        string text = ResourceLoader.GetString(controlName + ".Text");
                        c.Text = (text != null && text != "") ? text : c.Text;
                        AssignImage("Image", controlName, c);
                        AssignImage("BackgroundImage", controlName, c);
                    }
                    else if (c is DataGridView)
                    {
                        DataGridEx dataGrid = c as DataGridEx;

                        foreach (DataGridViewColumn column in dataGrid.Columns)
                        {

                            if (column.Visible)
                            {
                                string headerText = ResourceLoader.GetString(assemblyName + c.Name + ".ColumnHeaderTextAttribute." + column.Name);
                                if (!string.IsNullOrEmpty(headerText))
                                {
                                    column.HeaderText = headerText;
                                    column.ToolTipText = headerText;
                                }
                            }

                        }

                        //PropertyInfo[] properties = dataGrid.Type.GetProperties();
                        //foreach (PropertyInfo var in properties)
                        //{
                        //    object[] atributes = var.GetCustomAttributes(typeof(DataGridExColumnAttribute), false);
                        //    if (atributes.Length > 0)
                        //    {
                        //        DataGridExColumnAttribute tag = atributes[0] as DataGridExColumnAttribute;
                        //        string headerText = ResourceLoader.GetString(assemblyName + c.Name + ".ColumnHeaderTextAttribute." + var.Name);
                        //        tag.HeaderText = headerText;
                        //    }
                        //}
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("{0}", ex);
                }
				if (c.Controls != null)
				{
                    if (c is UserControl)
                        SearchAllControls(c.Controls, assemblyName + c.Name + ".");
				    else
                    SearchAllControls(c.Controls, assemblyName);
				    
                }
			}
		}
	}


   

    public class ResourceGenerator
    {

        private static Hashtable controlTypes = new Hashtable();
        private static Hashtable resxValues = null;
        private static string assemblyName = string.Empty;
        private static Form form = null;

        public static void Generate(Form currentForm, ComponentCollection components, string resourceName)
        {
            try
            {

                form = currentForm;
                Type controlType = form.GetType();

                FileInfo originalFile = new FileInfo(resourceName);
                FileInfo destinationFile = new FileInfo(Path.Combine(originalFile.DirectoryName, "ResourceTemp.resx"));

                assemblyName = controlType.FullName + ".";
                string textForm = form.Text;

                if (!controlTypes.ContainsKey(assemblyName))
                {
                    controlTypes.Add(assemblyName, "");

                    ResXResourceReader reader = new ResXResourceReader(originalFile.FullName);
                    reader.UseResXDataNodes = true;
                    IDictionaryEnumerator dic = reader.GetEnumerator();
                    resxValues = new Hashtable();

                    while (dic.MoveNext())
                    {
                        resxValues.Add(dic.Key.ToString(), dic.Value);
                        //writer.AddResource(dic.Key.ToString(), dic.Value);
                    }

                    reader.Close();

                    GenerateResourcesMenu(/*form, writer, assemblyName*/);
                    GenerateResourcesTooltip(/*form, writer, assemblyName*/);
                    GenerateResourcesColumnHeaders(/*form, writer, assemblyName*/);
                    GenerateResourcesToolStrip(/*form, writer, assemblyName*/);
                    //writer.AddResource(assemblyName + "Text", textForm);
                    AddorUpdateValue(assemblyName + "Text", textForm);
                    //writer.AddResource(assemblyName + "BackgroundImage", "");
                    AddorUpdateValue(assemblyName + "BackgroundImage", "");
                    //writer.AddResource(assemblyName + "Icon", "");
                    AddorUpdateValue(assemblyName + "Icon", "");

                    //Control.ControlCollection collection = form.Controls;
                    //SearchAllControls(collection, assemblyName, writer);
                    SearchAllControls(form.Controls, assemblyName);

                    if (File.Exists(destinationFile.FullName))
                        File.Delete(destinationFile.FullName);

                    ResXResourceWriter writer = new ResXResourceWriter(destinationFile.FullName);
                    dic = resxValues.GetEnumerator();
                    while (dic.MoveNext())
                    {
                        writer.AddResource(dic.Key.ToString(), dic.Value);
                    }
                    writer.Generate();
                    writer.Close();

                    File.SetAttributes(originalFile.FullName, FileAttributes.Normal);
                    File.Copy(destinationFile.FullName, originalFile.FullName, true);
                    File.Delete(destinationFile.FullName);
                    
                    string resGenPath = Environment.GetEnvironmentVariable("ProgramFiles") + "\\Microsoft Visual Studio 8\\SDK\\v2.0\\Bin\\ResGen.exe";

                    Process.Start(resGenPath, resourceName + ".resx /str:cs");
				
                }

                resxValues.Clear();
                resxValues = null;
                assemblyName = string.Empty;
                form = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static void AddorUpdateValue(string key, object value)
        {
            if (resxValues.ContainsKey(key) == false)
                resxValues.Add(key, value);
            else
                resxValues[key] = value;
        }

        private static void GenerateResourcesColumnHeaders(/*object form, ResXResourceWriter writer, string assemblyName*/)
        {
            Type formtype = form.GetType();
            FieldInfo[] fi = formtype.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static | BindingFlags.DeclaredOnly);
            Type columnHeaderType = typeof(System.Windows.Forms.ColumnHeader);
            foreach (FieldInfo colHeaderInfo in fi)
            {
                if (colHeaderInfo != null && colHeaderInfo.FieldType != null)
                {
                    if (colHeaderInfo.FieldType.Equals(columnHeaderType) || (colHeaderInfo.FieldType.BaseType != null && colHeaderInfo.FieldType.BaseType.Equals(columnHeaderType)))
                    {
                        string name = colHeaderInfo.Name;
                        ColumnHeader columnHeader = (ColumnHeader)formtype.InvokeMember(colHeaderInfo.Name, BindingFlags.DeclaredOnly |
                            BindingFlags.Public | BindingFlags.NonPublic |
                            BindingFlags.Instance | BindingFlags.GetField, null, form, null);
                        //writer.AddResource(assemblyName + colHeaderInfo.Name + ".Text", columnHeader.Text);
                        AddorUpdateValue(assemblyName + colHeaderInfo.Name + ".Text", columnHeader.Text);
                    }
                }
            }
        }

        private static void SearchAllControls(System.Windows.Forms.Control.ControlCollection col, string currentAssemblyName/*, ResXResourceWriter writer*/)
        {
            foreach (System.Windows.Forms.Control c in col)
            {

                if (c.Name != null && c.Name != "")
                {
                    string controlName = currentAssemblyName + c.Name;
                    //writer.AddResource(controlName + ".Text", c.Text);
                    AddorUpdateValue(controlName + ".Text", c.Text);
                    MemberInfo[] arrayMemberInfo = c.GetType().GetMember("Image", MemberTypes.Property, BindingFlags.Public | BindingFlags.Instance);
                    if (arrayMemberInfo.Length > 0)
                    {
                        //writer.AddResource(controlName + ".Image", "");
                        AddorUpdateValue(controlName + ".Image", "");
                    }
                    arrayMemberInfo = c.GetType().GetMember("BackgroundImage", MemberTypes.Property, BindingFlags.Public | BindingFlags.Instance);
                    if (arrayMemberInfo.Length > 0)
                    {
                        //writer.AddResource(controlName + ".BackgroundImage", "");
                        AddorUpdateValue(controlName + ".BackgroundImage", "");
                    }
                }
                if (c.Controls != null)
                {
                    if (c is UserControl)
                        SearchAllControls(c.Controls, currentAssemblyName + c.Name + "."/*, writer*/);

                    else
                        SearchAllControls(c.Controls, currentAssemblyName/*, writer*/);
                }

                if (c is DataGridView)
                {
                    DataGridEx dataGrid = c as DataGridEx;
                    PropertyInfo[] properties = dataGrid.Type.GetProperties();
                    foreach (PropertyInfo var in properties)
                    {
                        object[] atributes = var.GetCustomAttributes(typeof(DataGridExColumnAttribute), false);
                        if (atributes.Length > 0)
                        {
                            DataGridExColumnAttribute tag = atributes[0] as DataGridExColumnAttribute;
                            //writer.AddResource(assemblyName + c.Name + ".ColumnHeaderTextAttribute." + var.Name, tag.HeaderText);
                            AddorUpdateValue(currentAssemblyName + c.Name + ".ColumnHeaderTextAttribute." + var.Name, tag.HeaderText);
                        }
                    }
                }

            }
        }

        private static void GenerateResourcesMenu(/*object form, ResXResourceWriter writer, string assemblyName*/)
        {
            Type formtype = form.GetType();
            FieldInfo[] fi = formtype.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static | BindingFlags.DeclaredOnly);
            Type menutype = typeof(System.Windows.Forms.MenuItem);
            foreach (FieldInfo mi in fi)
            {
                if (menutype.Equals(mi.FieldType))
                {
                    MenuItem menu = (MenuItem)formtype.InvokeMember(mi.Name, BindingFlags.DeclaredOnly |
                        BindingFlags.Public | BindingFlags.NonPublic |
                        BindingFlags.Instance | BindingFlags.GetField, null, form, null);
                    //writer.AddResource(assemblyName + mi.Name + ".Text", menu.Text);
                    AddorUpdateValue(assemblyName + mi.Name + ".Text", menu.Text);
                }
            }
        }

        private static void GenerateResourcesToolStrip(/*object form, ResXResourceWriter writer, string assemblyName*/)
        {
            Type formtype = form.GetType();
            FieldInfo[] fi = formtype.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static | BindingFlags.DeclaredOnly);
            Type toolStripItem = typeof(ToolStripMenuItem);
            Type toolStripStatusLabel = typeof(ToolStripStatusLabel);
            Type toolStripLabel = typeof(ToolStripLabel);
            Type toolStripButton = typeof(ToolStripButton);
            Type richTextBoxExtended = typeof(RichTextBoxExtended);
            Type openFileDialog = typeof(OpenFileDialog);
            Type toolStripDropButton = typeof(ToolStripDropDownButton);
            Type toolStripComboBox = typeof(ToolStripComboBox);


            foreach (FieldInfo mi in fi)
            {
                if (toolStripItem.Equals(mi.FieldType))
                {
                    ToolStripMenuItem item = (ToolStripMenuItem)formtype.InvokeMember(mi.Name, BindingFlags.DeclaredOnly |
                           BindingFlags.Public | BindingFlags.NonPublic |
                           BindingFlags.Instance | BindingFlags.GetField, null, form, null);
                    if (item != null)
                    {
                        //writer.AddResource(assemblyName + mi.Name + ".Text", item.Text);
                        //writer.AddResource(assemblyName + mi.Name + ".ToolTipText", item.ToolTipText);
                        //writer.AddResource(assemblyName + mi.Name + ".Image", "");
                        AddorUpdateValue(assemblyName + mi.Name + ".Text", item.Text);
                        AddorUpdateValue(assemblyName + mi.Name + ".ToolTipText", item.ToolTipText);
                        AddorUpdateValue(assemblyName + mi.Name + ".Image", "");
                    }
                }
                else if (toolStripStatusLabel.Equals(mi.FieldType))
                {
                    ToolStripStatusLabel label = (ToolStripStatusLabel)formtype.InvokeMember(mi.Name, BindingFlags.DeclaredOnly |
                           BindingFlags.Public | BindingFlags.NonPublic |
                           BindingFlags.Instance | BindingFlags.GetField, null, form, null);
                    //writer.AddResource(assemblyName + mi.Name + ".Text", label.Text);
                    AddorUpdateValue(assemblyName + mi.Name + ".Text", label.Text);

                }
                else if (toolStripLabel.Equals(mi.FieldType))
                {
                    ToolStripLabel label = (ToolStripLabel)formtype.InvokeMember(mi.Name, BindingFlags.DeclaredOnly |
                           BindingFlags.Public | BindingFlags.NonPublic |
                           BindingFlags.Instance | BindingFlags.GetField, null, form, null);
                    //writer.AddResource(assemblyName + mi.Name + ".Text", label.Text);
                    AddorUpdateValue(assemblyName + mi.Name + ".Text", label.Text);
                }
                else if (toolStripButton.Equals(mi.FieldType))
                {
                    ToolStripButton button = (ToolStripButton)formtype.InvokeMember(mi.Name, BindingFlags.DeclaredOnly |
                           BindingFlags.Public | BindingFlags.NonPublic |
                           BindingFlags.Instance | BindingFlags.GetField, null, form, null);
                    //writer.AddResource(assemblyName + mi.Name + ".Text", button.Text);
                    //writer.AddResource(assemblyName + mi.Name + ".ToolTipText", button.ToolTipText);
                    //writer.AddResource(assemblyName + mi.Name + ".Image", "");
                    AddorUpdateValue(assemblyName + mi.Name + ".Text", button.Text);
                    AddorUpdateValue(assemblyName + mi.Name + ".ToolTipText", button.ToolTipText);
                    AddorUpdateValue(assemblyName + mi.Name + ".Image", "");
                }
                else if (toolStripDropButton.Equals(mi.FieldType))
                {

                    ToolStripDropDownButton button = (ToolStripDropDownButton)formtype.InvokeMember(mi.Name, BindingFlags.DeclaredOnly |
                           BindingFlags.Public | BindingFlags.NonPublic |
                           BindingFlags.Instance | BindingFlags.GetField, null, form, null);
                    //writer.AddResource(assemblyName + mi.Name + ".Text", button.Text);
                    //writer.AddResource(assemblyName + mi.Name + ".ToolTipText", button.ToolTipText);
                    //writer.AddResource(assemblyName + mi.Name + ".Image", "");
                    AddorUpdateValue(assemblyName + mi.Name + ".Text", button.Text);
                    AddorUpdateValue(assemblyName + mi.Name + ".ToolTipText", button.ToolTipText);
                    AddorUpdateValue(assemblyName + mi.Name + ".Image", "");

                }
                else if (toolStripComboBox.Equals(mi.FieldType))
                {
                    ToolStripComboBox combo = (ToolStripComboBox)formtype.InvokeMember(mi.Name, BindingFlags.DeclaredOnly |
                               BindingFlags.Public | BindingFlags.NonPublic |
                               BindingFlags.Instance | BindingFlags.GetField, null, form, null);
                    //writer.AddResource(assemblyName + mi.Name + ".Text", combo.Text);
                    //writer.AddResource(assemblyName + mi.Name + ".ToolTipText", combo.ToolTipText);
                    AddorUpdateValue(assemblyName + mi.Name + ".Text", combo.Text);
                    AddorUpdateValue(assemblyName + mi.Name + ".ToolTipText", combo.ToolTipText);

                }
                else if (richTextBoxExtended.Equals(mi.FieldType))
                {
                    RichTextBoxExtended rtb = (RichTextBoxExtended)formtype.InvokeMember(mi.Name, BindingFlags.DeclaredOnly |
                           BindingFlags.Public | BindingFlags.NonPublic |
                           BindingFlags.Instance | BindingFlags.GetField, null, form, null);

                    GenerateResourcesRichTextBoxExtended(rtb, /*writer,*/ assemblyName + rtb.Name + ".");

                }
                else if (openFileDialog.Equals(mi.FieldType))
                {
                    OpenFileDialog dialog = (OpenFileDialog)formtype.InvokeMember(mi.Name, BindingFlags.DeclaredOnly |
                               BindingFlags.Public | BindingFlags.NonPublic |
                               BindingFlags.Instance | BindingFlags.GetField, null, form, null);
                    //writer.AddResource(assemblyName + mi.Name + ".Title", dialog.Title);
                    //writer.AddResource(assemblyName + mi.Name + ".Filter", dialog.Filter);
                    AddorUpdateValue(assemblyName + mi.Name + ".Title", dialog.Title);
                    AddorUpdateValue(assemblyName + mi.Name + ".Filter", dialog.Filter);
                }
            }
        }

        private static void GenerateResourcesRichTextBoxExtended(RichTextBoxExtended rtb, /*ResXResourceWriter writer,*/ string currentAssemblyName)
        {
            foreach (ToolStripItem item in rtb.Toolbar.Items)
            {
                if (item is ToolStripButton || item is ToolStripDropDownButton)
                {
                    //writer.AddResource(assemblyName + item.Name + ".Text", item.Text);
                    //writer.AddResource(assemblyName + item.Name + ".ToolTipText", item.ToolTipText);
                    //writer.AddResource(assemblyName + item.Name + ".Image", "");
                    AddorUpdateValue(currentAssemblyName + item.Name + ".Text", item.Text);
                    AddorUpdateValue(currentAssemblyName + item.Name + ".ToolTipText", item.ToolTipText);
                    AddorUpdateValue(currentAssemblyName + item.Name + ".Image", "");

                }
                else if (item is ToolStripComboBox)
                {
                    //writer.AddResource(assemblyName + item.Name + ".Text", item.Text);
                    //writer.AddResource(assemblyName + item.Name + ".ToolTipText", item.ToolTipText);
                    AddorUpdateValue(currentAssemblyName + item.Name + ".Text", item.Text);
                    AddorUpdateValue(currentAssemblyName + item.Name + ".ToolTipText", item.ToolTipText);
                }
            }

            foreach (MenuItem item in rtb.RichTextBox.ContextMenu.MenuItems)
            {
                //writer.AddResource(assemblyName + item.Name + ".Text", item.Text);
                AddorUpdateValue(currentAssemblyName + item.Name + ".Text", item.Text);
            }

        }

        private static void GenerateResourcesTooltip(/*object form, ResXResourceWriter writer, string assemblyName*/)
        {
            Type formtype = form.GetType();
            FieldInfo[] fi = formtype.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static | BindingFlags.DeclaredOnly);
            Type tttype = typeof(System.Windows.Forms.ToolTip);
            foreach (FieldInfo mi in fi)
            {
                if (tttype.Equals(mi.FieldType))
                {
                    ToolTip m = (ToolTip)formtype.InvokeMember(mi.Name, BindingFlags.DeclaredOnly |
                        BindingFlags.Public | BindingFlags.NonPublic |
                        BindingFlags.Instance | BindingFlags.GetField, null, form, null);
                    Hashtable m2 = (Hashtable)tttype.InvokeMember("tools", BindingFlags.DeclaredOnly |
                        BindingFlags.Public | BindingFlags.NonPublic |
                        BindingFlags.Instance | BindingFlags.GetField, null, m, null);
                    foreach (FieldInfo allf in fi)
                    {
                        Object field = formtype.InvokeMember(allf.Name, BindingFlags.DeclaredOnly |
                            BindingFlags.Public | BindingFlags.NonPublic |
                            BindingFlags.Instance | BindingFlags.Static | BindingFlags.GetField, null, form, null);
                        if (field != null)
                        {
                            if (m2.Count > 0 && m2.ContainsKey(field))
                            {
                                System.Windows.Forms.Control ctrl = (System.Windows.Forms.Control)field;
                                string toolTip = m.GetToolTip(ctrl);
                                //writer.AddResource(assemblyName + mi.Name + "." + allf.Name + ".Tooltip", toolTip);
                                AddorUpdateValue(assemblyName + mi.Name + "." + allf.Name + ".Tooltip", toolTip);
                            }
                        }
                    }
                }
            }
        }

    }    
}
