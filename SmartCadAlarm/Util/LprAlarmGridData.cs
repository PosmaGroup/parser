﻿using SmartCadAlarm.Controls;
using SmartCadControls;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;


namespace SmartCadAlarm.Util
{
    public class LprAlarmGridData
    {
        public AlarmLprClientData alarmClientData { get; set; }
        public LprClientData lprClientData { get; set; }
        public StructClientData StructClientData { get; set; }
        public Image ContourImage
        {
            get
            {
                return GetImage(alarmClientData.ColorImage);
            }
            set { }
        }
        public Image PlateImage
        {
            get
            {
                return GetImage(alarmClientData.BWImage);
            }
            set { }
        }
        public int Code { get; set; }

        public LprAlarmGridData()
        {
        }

        public LprAlarmGridData(AlarmLprClientData alarmClientData)
        {
            this.alarmClientData = alarmClientData;
            this.lprClientData = alarmClientData.Lpr;
            this.Code = alarmClientData.Code;
            this.Date = alarmClientData.StartDate;
            this.State = alarmClientData.Lpr.StructClientData.State;
            this.Street = alarmClientData.Lpr.StructClientData.Street;
            this.Town = alarmClientData.Lpr.StructClientData.Town;
            this.Plate = alarmClientData.plate;
            this.Lpr = alarmClientData.Lpr.Name;
            this.StructClientData = alarmClientData.Lpr.StructClientData;
            this.Status = ResourceLoader.GetString2(((AlarmLprClientData.AlarmStatus)alarmClientData.Status).ToString());
            //SetContourImage(alarmClientData.ColorImage);
            //SetPlateImage(alarmClientData.BWImage);
        }

        [GridControlRowInfoData(Visible = true, Width = 70, Searchable = true)]
        public string Status
        {
            get;
            set;
        }

        [GridControlRowInfoData(Visible = true, Width = 70, Searchable = true)]
        public string Plate { get; set; }

        [GridControlRowInfoData(Visible = true, Width = 70, Searchable = true)]
        public string Lpr { get; set; }

        [GridControlRowInfoData(Visible = true, Width = 70, Searchable = true)]
        public string Struct
        {
            get
            {

                return StructClientData.Name;
            }

        }

        [GridControlRowInfoData(Visible = true, Width = 70, Searchable = true)]
        public DateTime? Date { get; set; }

        public string DateAndTime
        {
            get { return Date.ToString(); }
        }

        [GridControlRowInfoData(Visible = true, Width = 70, Searchable = true)]
        public string State { get; set; }

        [GridControlRowInfoData(Visible = true, Width = 70, Searchable = true)]
        public string Street { get; set; }

        [GridControlRowInfoData(Visible = true, Width = 70, Searchable = true)]
        public string Town { get; set; }

        public void SetContourImage(byte[] colorImage)
        {
            ContourImage = GetImage(colorImage);
        }

        public void SetPlateImage(byte[] colorImage)
        {
            PlateImage = GetImage(colorImage);
        }

        private Image GetImage(byte[] imageBuffer)
        {
            Image image = null;
            if (imageBuffer != null)
            {
                try
                {
                    MemoryStream ms = new MemoryStream(imageBuffer);
                    image = Image.FromStream(ms);
                }
                catch
                {
                }
            }
            return image;
        }

        public override bool Equals(object obj)
        {
            if (obj is LprAlarmGridData)
            {
                LprAlarmGridData alarm = (obj as LprAlarmGridData);

                if (alarm.Code == this.Code)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

        }


    }
}
