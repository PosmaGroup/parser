using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using System.Collections;
using SmartCadAlarm.Controls;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadControls.Controls;
using SmartCadControls;


namespace SmartCadAlarm.Util.SyncBoxes.Alarm
{
    public class GridIncidentAlarmData : GridControlData
    {
        private SensorTelemetryClientData sensorTelemetry;
        private LprClientData lpr;
        private Color backColor;
        private IncidentClientData incident;
        public GridIncidentAlarmData(IncidentClientData incident)
            : base(incident)
        {
            this.incident = incident;
            backColor = Color.White;
            if (incident.AlarmReports != null)
            {
                if (incident.AlarmReports[0] is AlarmReportClientData)
                {
                    AlarmReportClientData alarmReport = (AlarmReportClientData)incident.AlarmReports[0];

                    this.sensorTelemetry = (alarmReport.AlarmSensorTelemetryClient.SensorTelemetry);
                }
                else if (incident.AlarmReports[0] is AlarmLprReportClientData)
                {
                    //AlarmLprReportClientData alarmReport = (AlarmLprReportClientData)incident.AlarmReports[0];
                    //IList lprs = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(
                    //    SmartCadHqls.GetLprByName, alarmReport.AlarmLprReportLprClient.Name));

                    //if (lprs.Count > 0)
                    //{
                    //    this.lpr = (lprs[0] as LprClientData);
                    //}
                }
               
            }
        }

        
        public string Key
        {
            get
            {
                return incident.CustomCode;
            }
            set
            {
                throw new Exception(ResourceLoader.GetString2("NotImplementedMethod"));
            }
        }

        
        [GridControlRowInfoData(Searchable = true)]        		
        public string State
        {
            get
            {
                if ((sensorTelemetry != null) && (sensorTelemetry.Datalogger.StructClientData != null))
                    return sensorTelemetry.Datalogger.StructClientData.State;
                else if ((lpr != null) && (lpr.StructClientData != null))
                    return lpr.StructClientData.State;
                else
                    return string.Empty;
            }
        }
		[GridControlRowInfoData(Searchable = true)]
        public string Town
        {
            get
            {
                if ((sensorTelemetry != null) && (sensorTelemetry.Datalogger.StructClientData != null))
                    return sensorTelemetry.Datalogger.StructClientData.Town;
                else if ((lpr != null) && (lpr.StructClientData != null))
                    return lpr.StructClientData.Town;
                else
                    return string.Empty;
            }
        }
		[GridControlRowInfoData(Searchable = true)]
        public string Zone
        {
            get
            {
                if ((sensorTelemetry != null) && (sensorTelemetry.Datalogger.StructClientData != null))
                    return sensorTelemetry.Datalogger.StructClientData.ZoneSecRef;
                else if ((lpr != null) && (lpr.StructClientData != null))
                    return lpr.StructClientData.ZoneSecRef;
                else
                    return string.Empty;
            }
        }
		[GridControlRowInfoData(Searchable = true)]
        public string Street
        {
            get
            {
                if ((sensorTelemetry != null) && (sensorTelemetry.Datalogger.StructClientData != null))
                    return sensorTelemetry.Datalogger.StructClientData.Street;
                else if ((lpr != null) && (lpr.StructClientData != null))
                    return lpr.StructClientData.Street;
                else
                    return string.Empty;
            }
        }
		[GridControlRowInfoData(Searchable = true)]        
        public string Struct
        {
            get
            {
                if ((sensorTelemetry != null) && (sensorTelemetry.Datalogger.StructClientData != null))
                    return sensorTelemetry.Datalogger.StructClientData.Name;
                else if ((lpr != null) && (lpr.StructClientData != null))
                    return lpr.StructClientData.Name;
                else
                    return string.Empty;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string Lpr
        {
            get
            {
                if (lpr != null)
                {
                    return lpr.Name;
                }
                else
                    return string.Empty;
            }
        }

        [GridControlRowInfoData(Searchable = true)]
        public string Datalogger
        {
            get
            {
                if (sensorTelemetry != null)
                {
                    return sensorTelemetry.Datalogger.Name;
                }
                else
                    return string.Empty;
            }
        }

		[GridControlRowInfoData(Searchable = true)]
        public string Sensor
        {
            get
            {
                if (sensorTelemetry != null)
                {
                    return sensorTelemetry.Name;
                }
                else
                    return string.Empty;
            }
        }
        [GridControlRowInfoData(Searchable = true)]
        public string Variable
        {
            get
            {
                if (sensorTelemetry != null)
                {
                    return sensorTelemetry.Variable;
                }
                else
                    return string.Empty;
            }
        }
		[GridControlRowInfoData(Searchable = true)]
        public string IncidentTypes
        {
            get
            {
                int count = 0;
                StringBuilder sb = new StringBuilder();
                if (incident.IncidentTypes != null)
                {
                    foreach (IncidentTypeClientData temp in incident.IncidentTypes)
                    {
                        sb.Append(temp.CustomCode);
                        if (++count < incident.IncidentTypes.Count)
                        {
                            sb.Append(", ");
                        }
                        else
                        {
                            sb.Append(".");
                        }
                    }
                }
                return sb.ToString();
            }
        }
		[GridControlRowInfoData(Searchable = true)]
        public string CallDate
        {
            get
            {
                return incident.StartDate.ToString();
            }
        }

		public override bool Equals(object obj)
		{
			if (obj is GridIncidentAlarmData == false)
				return false;
			GridIncidentAlarmData gridData = obj as GridIncidentAlarmData;
			if (gridData.incident.Code == this.incident.Code)
				return true;
			return false;
		}
    }
}