using System;
using System.Collections.Generic;
using System.Text;
using System.ServiceModel;
using System.Net.Security;
using SmartCadCore.ClientData;

namespace SmartCadAlarm.Service
{
    [ServiceContract(CallbackContract = typeof(ITelephonyServiceCallback), ProtectionLevel = ProtectionLevel.None)]
    public interface ITelephonyService
    {
        [OperationContract]
        void Login(string login, string password, string agentId, string agentPassword, bool recordCalls, string extNumber, bool isReady);

        [OperationContract(IsOneWay = true)]
        void Answer();

        [OperationContract(IsOneWay = true)]
        void Disconnect();

        [OperationContract(IsOneWay = true)]
        void SetIsReady(bool isReady);

        [OperationContract(IsOneWay = true)]
        void Logout();

        [OperationContract(IsOneWay = true)]
        void Ping();

        [OperationContract(IsOneWay = true)]
        void GenerateCall(string number);

        [OperationContract(IsOneWay = true)]
        void SetConfiguration(ConfigurationClientData ConfigurationProperties);

        [OperationContract]
        bool IsVirtual();

        [OperationContract]
        void SetVirtualCall(bool virtualCall);

        [OperationContract]
        bool InVirtualCall();

        [OperationContract]
        void ParkCall(string extensionParking, string parkId);

        [OperationContract]
        void Dispose();

        [OperationContract]
        void RecoverConnection();

        [OperationContract]
        void DivertCall();

        [OperationContract]
        void SetPhoneReportCustomCode(string customCode);
    }
}
