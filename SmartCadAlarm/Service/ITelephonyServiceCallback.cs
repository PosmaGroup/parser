using System;
using System.Collections.Generic;
using System.Text;
using System.ServiceModel;
using SmartCadCore.Core;

namespace SmartCadAlarm.Service
{
    public interface ITelephonyServiceCallback
    {
        [OperationContract(IsOneWay = true)]
        void OnTelephonyAction(TelephonyActionEventArgs e);
    }
}
