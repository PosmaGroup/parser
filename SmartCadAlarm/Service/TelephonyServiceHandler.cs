using System;
using System.Collections.Generic;
using System.Text;
using System.ServiceModel;
using SmartCadCore.Core;


namespace SmartCadAlarm.Service
{
    [CallbackBehavior(ConcurrencyMode = ConcurrencyMode.Reentrant, UseSynchronizationContext = false)]
    public class TelephonyServiceHandler : ITelephonyServiceCallback
    {
        public event EventHandler<TelephonyActionEventArgs> TelephonyAction;

        public void OnTelephonyAction(TelephonyActionEventArgs e)
        {
            //System.Threading.ThreadPool.QueueUserWorkItem(delegate(object state)
            //{
            //    try
            //    {
            //        if (this.TelephonyAction != null)
            //        {
            //            this.TelephonyAction(this, e);
            //        }
            //    }
            //    catch (Exception ex)
            //    {
                    
            //    }
            //});

            if (this.TelephonyAction != null)
            {
                this.TelephonyAction(this, e);
            }
        }
    }
}
