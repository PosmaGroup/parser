using System;
using System.Collections.Generic;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.Threading;
using SmartCadCore.Core;
using SmartCadCore.Common;
using SmartCadCore.ClientData;

namespace SmartCadAlarm.Service
{
    public class TelephonyServiceClient : ClientBase<ITelephonyService>, ITelephonyService
    {
        private static TelephonyServiceClient telephonyServiceClient;
        private TelephonyServiceHandler handler;

        public event EventHandler<TelephonyActionEventArgs> TelephonyAction;

        private TelephonyServiceClient(TelephonyServiceHandler handler, Binding binding, EndpointAddress remoteAddress)
            : base(new InstanceContext(handler), binding, remoteAddress)
        {
            this.handler = handler;
            this.handler.TelephonyAction += new EventHandler<TelephonyActionEventArgs>(handler_TelephonyAction);
        }

        private static TelephonyServiceClient GetInstance(Binding binding, EndpointAddress remoteAddress)
        {
            telephonyServiceClient = new TelephonyServiceClient(new TelephonyServiceHandler(), binding, remoteAddress);

            return telephonyServiceClient;
        }

        public static TelephonyServiceClient Instance 
        {
            get
            {
                return telephonyServiceClient;
            }
        }

        public static TelephonyServiceClient GetInstance()
        {
            if (telephonyServiceClient == null)
                throw new Exception(ResourceLoader.GetString2("ClientInstanceNotCreated"));

            return telephonyServiceClient;
        }

        public static TelephonyServiceClient GetServerService()
        {
            string url = ApplicationUtil.BuildServiceUrl(
                "net.tcp",
                "localhost",
                SmartCadConfiguration.TELEPHONY_SERVICE_PORT,
                SmartCadConfiguration.TELEPHONY_SERVICE_ADDRESS);

            EndpointAddress address = new EndpointAddress(url);

            CustomBinding binding = BindingBuilder.GetNetTcpBinding();

            TelephonyServiceClient client = GetInstance(binding, address);

            foreach (OperationDescription op in client.Endpoint.Contract.Operations)
            {
                DataContractSerializerOperationBehavior dataContractBehavior =
                   op.Behaviors.Remove<DataContractSerializerOperationBehavior>()
                        as DataContractSerializerOperationBehavior;

                op.Behaviors.Add(new NetDataContractSerializerOperationBehavior(op));
            }

            client.Ping();

            return client;
        }

        public void SetConfiguration(ConfigurationClientData configurationProperties)
        {
            base.Channel.SetConfiguration(configurationProperties);
        }

        public bool IsVirtual()
        {
            return base.Channel.IsVirtual();
        }

        void handler_TelephonyAction(object sender, TelephonyActionEventArgs e)
        {
            if (this.TelephonyAction != null)
            {
                this.TelephonyAction(this, e);
            }
        }

        #region ITelephonyService Members

        public void Login(string login, string password, string agentId, string agentPassword, bool recordCalls, string extNumber, bool isReady)
        {
            base.Channel.Login(login, password, agentId, agentPassword, recordCalls, extNumber, isReady);
        }

        public void Answer()
        {
            base.Channel.Answer();
        }

        public void Disconnect()
        {
            try
            {
                base.Channel.Disconnect();
            }
            catch { };
        }

        public void DivertCall()
        {
            base.Channel.DivertCall();
        }

        public void SetIsReady(bool isReady)
        {
            base.Channel.SetIsReady(isReady);
        }

        public void Logout()
        {
            base.Channel.Logout();
        }

        public void Dispose()
        {
            base.Channel.Dispose();
        }

        public void Ping()
        {
            base.Channel.Ping();
        }

        public void GenerateCall(string number)
        {
            base.Channel.GenerateCall(number);
        }

        public void SetVirtualCall(bool virtualCall)
        {
            base.Channel.SetVirtualCall(virtualCall);
        }

        public bool InVirtualCall()
        {
            return base.Channel.InVirtualCall();
        }

        public void RecoverConnection()
        {
            base.Channel.RecoverConnection();
        }

        /// <summary>
        /// Puts the current call in parking state, waiting for a dispatcher to re-take it.
        /// </summary>
        /// <param name="extensionParking">Extension number where call will be parked(queue number)</param>
        /// <param name="parkId">call id used to park and unpark the call</param>
        public void ParkCall(string extensionParking, string parkId)
        {
            base.Channel.ParkCall(extensionParking, parkId);
        }
        #endregion

        public void SetPhoneReportCustomCode(string customCode)
        {
            base.Channel.SetPhoneReportCustomCode(customCode);
        }
    }
}