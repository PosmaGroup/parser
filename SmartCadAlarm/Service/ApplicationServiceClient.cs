﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using SmartCadCore.ClientData;
using SmartCadCore.Core;
using SmartCadCore.Common;

using Smartmatic.SmartCad.Service;
using SmartCadCore.Enums;

namespace SmartCadAlarm.Service
{
    public class ApplicationServiceClient : ClientBase<IApplicationService>, IApplicationService
    {
        #region Constructors

        private ApplicationServiceClient(Binding binding, EndpointAddress remoteAddress)
            : base(binding, remoteAddress)
        {
        }

        #endregion

        #region Properties
        /// <summary>
        /// Application that would be connected to this instance.
        /// </summary>
        public UserApplicationClientData Application { get; set; }
        #endregion

        #region Static
        private static Dictionary<string, ApplicationServiceClient> current = new Dictionary<string,ApplicationServiceClient>();

        public static ApplicationServiceClient Current(UserApplicationClientData application)
        {
            if (current.ContainsKey(application.Name) == false)
            {
                current.Add(application.Name, GetService(application));
            }
            else
            {
                if (current[application.Name].State == CommunicationState.Faulted ||
                    current[application.Name].State == CommunicationState.Closed)
                    current[application.Name] = GetService(application);
            }
            return current[application.Name];
        }

        private static ApplicationServiceClient GetService(UserApplicationClientData application)
        {
            EndpointAddress address = new EndpointAddress("net.pipe://localhost/" + application.Name);

            NetNamedPipeBinding binding = new NetNamedPipeBinding(NetNamedPipeSecurityMode.None);

            binding.ReaderQuotas.MaxDepth = 1024;
            binding.ReaderQuotas.MaxArrayLength = 524288;
            binding.MaxReceivedMessageSize = 5242880;
            binding.ReaderQuotas.MaxStringContentLength = 1572864;

            BindingElementCollection bindingElementCollection = binding.CreateBindingElements();

            NamedPipeTransportBindingElement namedPipeTransportBindingElement = 
                (NamedPipeTransportBindingElement)bindingElementCollection[bindingElementCollection.Count - 1];

            namedPipeTransportBindingElement.ConnectionPoolSettings.IdleTimeout = TimeSpan.FromMinutes(60);

            ApplicationServiceClient client = new ApplicationServiceClient(binding, address);
            client.Application = application;

            foreach (OperationDescription op in client.Endpoint.Contract.Operations)
            {
                DataContractSerializerOperationBehavior dataContractBehavior =
                   op.Behaviors.Remove<DataContractSerializerOperationBehavior>()
                        as DataContractSerializerOperationBehavior;

                op.Behaviors.Add(new NetDataContractSerializerOperationBehavior(op));
            }

            client.InnerChannel.OperationTimeout = TimeSpan.FromMinutes(3);

            return client;
        }
        #endregion

        #region To Map
        public void DisplayGeoPointInGis(GeoPoint geoPoint)
        {
            try
            {
                if (geoPoint != null)
                {
                    DisplayGeoPointEventArgs args = new DisplayGeoPointEventArgs(geoPoint);

                    OnGisAction(args);
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        public void HighLightObjectsInMap(List<HLGisObject> objsHL)
        {
            try
            {
                if (objsHL != null)
                {
                    HLObjectsEventArgs args = new HLObjectsEventArgs(objsHL);
                    OnGisAction(args);
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        public void HighLightObjectInMap(HLGisObject objHL)
        {
            HighLightObjectsInMap(new List<HLGisObject>(new HLGisObject[] { objHL }));
        }

        #endregion

        #region FirstLevel To Map
        public void InsertAddressDataInGis(AddressClientData addressData)
        {
            try
            {
                if (addressData != null)
                {
                    SearchGeoAddressEventArgs e = new SearchGeoAddressEventArgs();

                    e.GeoAddress = new GeoAddress(
                        addressData.Zone,
                        addressData.Street,
                        addressData.Reference,
                        addressData.More);

                    OnGisAction(e);
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        public void SendEnableButton(bool enabled, ButtonType button)
        {
            try
            {
                SendEnableButtonEventArgs args = new SendEnableButtonEventArgs(button,enabled);
                OnGisAction(args);
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }
        #endregion

        #region Map To FirsLevel
        
        public void VerifyStateFrontClient()
        {
            try
            {
                VerifyStateFrontClientEventArgs args = new VerifyStateFrontClientEventArgs();

                OnGisAction(args);
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }
        #endregion

        #region From or To Map

        public void SendGeoPoint(GeoPoint geoPoint)
        {
            try
            {
                SendGeoPointEventArgs args = new SendGeoPointEventArgs(geoPoint);
                OnGisAction(args);
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        public void SendGeoPointList(List<GeoPoint> geoPoints, ShapeType type)
        {
            try
            {
                SendGeoPointListEventArgs args = CreateSendGeoPointEventArgs(geoPoints, type);
                OnGisAction(args);
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        public void SendGeoPointList(List<GeoPoint> geoPoints, int depTypeCode, int zoneCode, ShapeType type)
        {
            try
            {
                SendGeoPointListEventArgs args = CreateSendGeoPointEventArgs(geoPoints, type);
                args.DepartmentCode = depTypeCode;
                args.ZoneCode = zoneCode;
                OnGisAction(args);
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        public void SendGeoPointList(List<GeoPoint> geoPoints, int depTypeCode, int zoneCode, string stationName, ShapeType type)
        {
            try
            {
                SendGeoPointListEventArgs args = CreateSendGeoPointEventArgs(geoPoints, type);
                args.DepartmentCode = depTypeCode;
                args.ZoneCode = zoneCode;
                args.StationName = stationName;
                OnGisAction(args);
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        public void SendToActivateLayer(ShapeType type)
        {
            try
            {
                SendActivateLayerEventArgs args = new SendActivateLayerEventArgs(type);
                OnGisAction(args);
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        public void SendToActivateLayer(ShapeType type, int selectedDepartment, int zoneCode)
        {
            try
            {
                SendActivateLayerEventArgs args = new SendActivateLayerEventArgs(type, selectedDepartment, zoneCode);
                OnGisAction(args);
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        public void SendToActivateLayer(ShapeType type, int selectedDepartment, int zoneCode, string station)
        {
            try
            {
                SendActivateLayerEventArgs args = new SendActivateLayerEventArgs(type, selectedDepartment, zoneCode, station);
                OnGisAction(args);
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        public void SendACK(bool connected)
        {
            try
            {
                SendACKEventArgs args = new SendACKEventArgs(connected);
                OnGisAction(args);
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        private static SendGeoPointListEventArgs CreateSendGeoPointEventArgs(List<GeoPoint> geoPoints, ShapeType type)
        {
            SendGeoPointListEventArgs args = null;

            if (geoPoints == null)
            {
                geoPoints = new List<GeoPoint>();
                geoPoints.Add(new GeoPoint(0, 0));
            }
            args = new SendGeoPointListEventArgs(geoPoints, -1, -1, type);

            return args;
        }

        public bool Ping()
        {
            try
            {
                OnPing();
                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion

        #region IApplicationService Members

        public void OnPing()
        {
            base.Channel.OnPing();
        }

        public void OnCommittedChanges(object sender, CommittedObjectDataCollectionEventArgs committedObjectDataCollectionEventArgs)
        {
            base.Channel.OnCommittedChanges(sender, committedObjectDataCollectionEventArgs);
        }

        public void OnGisAction(GisActionEventArgs e)
        {
            if (base.State == CommunicationState.Opened || base.State == CommunicationState.Created)
                base.Channel.OnGisAction(e);
        }

        #endregion
    }
}
