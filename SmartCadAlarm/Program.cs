﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading;
using System.ServiceModel;

using System.Net;
using System.Globalization;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadCore.Core;
using Smartmatic.SmartCad.Service;
using SmartCadGuiCommon;
using SmartCadAlarm.Gui;
using SmartCadGuiCommon.Util;
using System.IO;

//comment
namespace SmartCadAlarm
{
    static class Program
    {
        private static string ApplicationName = "Alarm";

        [STAThread]
        static void Main()
        {
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.ThreadException += new ThreadExceptionEventHandler(ApplicationGuiUtil.Application_ThreadException);
                ApplicationUtil.ActivateProcess(Process.GetCurrentProcess());

                SmartCadConfiguration.Load();
                ResourceLoader.UpdatedLanguage(new CultureInfo(SmartCadConfiguration.SmartCadSection.CultureElement.CultureLanguageElement.Name
                    + "-" + SmartCadConfiguration.SmartCadSection.CultureElement.CultureCountryElement.Name));

                object result = ServerServiceClient.GetServerService();

                if (result == null)
                {
                    MessageForm.Show(ResourceLoader.GetString2("UnableConnectToServer"), MessageFormType.Error);
                    return;
                }

                ConfigurationClientData cfg = ServerServiceClient.GetInstance().GetConfiguration();
                SmartCadConfiguration.SaveConfigurationInClient(cfg);
                if (!ApplicationUtil.VerifyLanguage(cfg))
                {
                    byte[] dll = ServerServiceClient.GetInstance().CheckLanguages(cfg.Language);
                    DirectoryInfo directory = Directory.CreateDirectory(SmartCadConfiguration.LanguageDirectoryClient + "\\" + cfg.Language);
                    using (FileStream fileDll = new FileStream(directory.FullName + "\\Smartmatic.SmartCad.Resources.resources.dll", FileMode.Create, FileAccess.Write))
                    {
                        fileDll.Write(dll, 0, dll.Length);
                    }
                }

                ResourceLoader.UpdatedLanguage(new CultureInfo(cfg.Language));
                ServerServiceClient.GetInstance().SetApplication(UserApplicationClientData.UserApplications.Alarm.ToString());
                SmartCadLoadInitialData.LoadInitialClientData();

                System.Threading.Timer checkServer = ApplicationGuiUtil.StartCheckServer();

                ApplicationGuiUtil.CheckErrorDetailButton();
                ServerServiceClient.GetInstance().ActivateApplication(ApplicationName,ApplicationUtil.GetMACAddress());
                bool success = false;

                do
                {
                    LoginForm login = new LoginForm(UserApplicationClientData.Alarm);

                    AlarmFrontClientFormDevX frontClientForm = new AlarmFrontClientFormDevX();
                  //  CctvFrontClientFormDevX frontClientForm = new CctvFrontClientFormDevX();

                    if (login.ShowDialog() == DialogResult.OK)
                    {
                        SplashForm.ShowSplash();
                        NetworkCredential networkCredential = login.NetworkCredential;

                        ServerServiceClient serverServiceClient = login.ServerService;

                        success = true;

                        frontClientForm.SetPassword(login.Password);
                        frontClientForm.ServerServiceClient = serverServiceClient;
                        frontClientForm.NetworkCredential = networkCredential;
                        configuration = ServerServiceClient.GetInstance().GetConfiguration();
                        frontClientForm.ConfigurationClient = configuration;
                        frontClientForm.LoadInitialData();
                        SplashForm.CloseSplash();
                        Application.Run(frontClientForm);
                    }
                    else
                    {
                        ServerServiceClient.GetInstance().CloseSession();
                        success = true;
                    }
                }
                while (!success);
            }
            catch (EndpointNotFoundException ex)
            {
                SplashForm.CloseSplash();

                MessageForm.Show(ResourceLoader.GetString2("UnableConnectToServer"), ex);
            }
            catch (Exception ex)
            {
                SplashForm.CloseSplash();

                MessageForm.Show(ResourceLoader.GetString2("GenericErrorApplication"), ex);
            }
        }

        private static ConfigurationClientData configuration;

    }
}
