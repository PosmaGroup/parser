﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using Smartmatic.SmartCad.VideoAnalitica;
using Smartmatic.SmartCad.VideoAnalitica.Objects;

namespace Smartmatic.SmartCad.VideoAnalitica.AgentVI
{
    public class AgentViManager: VideoAnaliticaManagerAbstract
    {
        private ViGateway.ViGateway viGateway;
        private string host {get;set;}
        private string user { get; set; }
        private string password { get; set; }
        private string port { get; set; }

        public AgentViManager(string host, string port, string user, string password)
        {
            this.host = host;
            this.port = port;
            this.user = user;
            this.password = password;

            try
            {
                viGateway = new ViGateway.ViGateway
                {
                    Url = string.Format("http://{0}:{1}/", host, port),
                    PreAuthenticate = true,
                    Credentials = new NetworkCredential(user, password)
                };

            }
            catch
            {
                throw;
            }


        }


        public Rule[] getAllRules()
        {
           ViGateway.Rule[] answer = viGateway.RuleGetAll();
           IList<Rule> rules = new List<Rule>();
           foreach (ViGateway.Rule ruleAnswer in answer)
           {
               Rule rule = new Rule();
               rule.ID = ruleAnswer.Id;
               rule.GUID = ruleAnswer.Guid;
               rules.Add(rule);
           }
           return rules.ToArray();
        }

        public Alert[] getAlertsByRule(string[] ruleGUID)
        {
            uint[] ruleIds = null;
            ViGateway.EventPage[] eventsPages;
            uint answer = viGateway.EventGetFilteredPages(ViGateway.EventPageOrder.EventPageOrderTime, true, null,
                                            false, null, true, null, false, null, false, null, true,
                                            null, false, null, false, null, false, ruleGUID, ruleIds, 0, 0, 1000, out eventsPages);
            List<Alert> result = new List<Alert>();
            if (eventsPages.Length > 0)
            {
                ViGateway.Event[] events = eventsPages[0].Events;
                foreach (ViGateway.Event ev in events)
                {
                    Alert aux = new Alert();
                    aux.id = ev.Id;
                    aux.ruleId = ev.RuleId;
                    aux.date = ev.EventTime;
                    aux.sensorId = ev.SensorId;
                    result.Add(aux);
                }
            }
            return result.ToArray(); 
        }
        public Alert[] getAlerts()
        {

            string[] ruleguids = null;
            return getAlertsByRule(ruleguids);
        }

        public VideoSensor getVideoSensor(uint sensor)
        {

            VideoSensor result = new VideoSensor();
            uint[] sensors= new uint[1];
            sensors[0] = sensor;
            ViGateway.VideoSensor[] videoSensors;
            videoSensors= viGateway.VideoSensorGet(sensors);
            if (videoSensors.Length > 0)
            {
                result.ID=videoSensors[0].Id;
                result.VMS_ID = videoSensors[0].SourceStreamInfo;
            }
            return result;
        }

        public Rule getRule(uint ruleId)
        {
            Rule result = new Rule();
            ViGateway.Rule[] rules = new ViGateway.Rule[1];
            uint[] ruleIds = new uint [1];
            ruleIds[0]=ruleId;

            rules = viGateway.RuleGet(ruleIds);
            result.ID = rules[0].Id;
            result.GUID = rules[0].Guid;
            return result;
        }

        public EventImage getImageByEvent(uint eventId)
        {
            EventImage result = new EventImage();
            result.ID_EVENT = eventId;
            result.IMAGE = viGateway.EventGetImage(eventId);
            return result;
        }

        public EventVideo getVideoByEvent(uint eventId)
        {
            uint[] events = { eventId };
            ViGateway.QueryResultEvent[] result = viGateway.EventGetMetadata(events);
            EventVideo eventVideo = new EventVideo();
            eventVideo.EventTimeValue = result[0].EventTime.Value;
            eventVideo.EventTimeMilliseconds = (int)result[0].EventTime.MilliSeconds;
            if (result[0].Objects != null && result[0].Objects.Length > 0)
            {
                eventVideo.ObjectId = (long)result[0].Objects[0].ObjectId;
                eventVideo.TotalObjectInstances = (int)result[0].Objects[0].TotalObjectInstances;

                
                eventVideo.BirthTimeValue = result[0].Objects[0].ObjectBirthTime.Value;
                eventVideo.BirtTimeMilliseconds = (int)result[0].Objects[0].ObjectDeathTime.MilliSeconds;
                eventVideo.DeathTimeValue = result[0].Objects[0].ObjectBirthTime.Value;
                eventVideo.DeathTimeMilliseconds = (int)result[0].Objects[0].ObjectDeathTime.MilliSeconds;               
                
                IList<VideoInstance> videoInstances = new List<VideoInstance>();
                VideoInstance videoInstance;
                ViGateway.QueryResultObjectInstance objectInst;

                #region ObjectInstance
                if (result[0].Objects[0].TotalObjectInstances > 0)
                {
                    foreach (ViGateway.QueryResultObjectInstance objectInstance in result[0].Objects[0].ObjectInstances)
                    {
                        videoInstance = new VideoInstance();
                        videoInstance.ObjectId = eventVideo.ObjectId;
                        videoInstance.TimestampMilliseconds = (int)objectInstance.Timestamp.MilliSeconds;
                        videoInstance.TimestampValue = objectInstance.Timestamp.Value;
                        videoInstance.TypeObject = VideoInstance.TypeObjectEnum.ObjectInstance;
                        videoInstance.BoundingBoxTop = objectInstance.BoundingBox.Top;
                        videoInstance.BoundingBoxButtom = objectInstance.BoundingBox.Bottom;
                        videoInstance.BoundingBoxLeft = objectInstance.BoundingBox.Left;
                        videoInstance.BoundingBoxRight = objectInstance.BoundingBox.Right;
                        videoInstance.FootX = objectInstance.FootForPanoramicImage.x;
                        videoInstance.FootY = objectInstance.FootForPanoramicImage.y;
                        videoInstances.Add(videoInstance);
                    }
                }

                eventVideo.VideoInstances = videoInstances;
                #endregion

                #region ObjectInstanceOnEvent
                objectInst = result[0].Objects[0].ObjectInstanceOnEvent;

                videoInstance = new VideoInstance();
                videoInstance.ObjectId = eventVideo.ObjectId;
                videoInstance.TimestampMilliseconds = (int)objectInst.Timestamp.MilliSeconds;
                videoInstance.TimestampValue = objectInst.Timestamp.Value;
                videoInstance.TypeObject = VideoInstance.TypeObjectEnum.ObjectInstanceOnEvent;
                videoInstance.BoundingBoxTop = objectInst.BoundingBox.Top;
                videoInstance.BoundingBoxButtom = objectInst.BoundingBox.Bottom;
                videoInstance.BoundingBoxLeft = objectInst.BoundingBox.Left;
                videoInstance.BoundingBoxRight = objectInst.BoundingBox.Right;
                videoInstance.FootX = objectInst.FootForPanoramicImage.x;
                videoInstance.FootY = objectInst.FootForPanoramicImage.y;
                videoInstances.Add(videoInstance);

                #endregion

                #region ObjectInstanceOnBestVIew
                objectInst = result[0].Objects[0].ObjectInstanceOnBestView;

                videoInstance = new VideoInstance();
                videoInstance.ObjectId = eventVideo.ObjectId;
                videoInstance.TimestampMilliseconds = (int)objectInst.Timestamp.MilliSeconds;
                videoInstance.TimestampValue = objectInst.Timestamp.Value;
                videoInstance.TypeObject = VideoInstance.TypeObjectEnum.ObjectInstanceOnBestVIew;
                videoInstance.BoundingBoxTop = objectInst.BoundingBox.Top;
                videoInstance.BoundingBoxButtom = objectInst.BoundingBox.Bottom;
                videoInstance.BoundingBoxLeft = objectInst.BoundingBox.Left;
                videoInstance.BoundingBoxRight = objectInst.BoundingBox.Right;
                videoInstance.FootX = objectInst.FootForPanoramicImage.x;
                videoInstance.FootY = objectInst.FootForPanoramicImage.y;
                videoInstances.Add(videoInstance);

                #endregion

                eventVideo.VideoInstances = videoInstances;
            }
             return eventVideo;
        }

        public void deleteAlerts()
        {
            viGateway.EventDelAll();
        }
    }
}
