﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;


using System.Threading;
using mshtml;
using System.Globalization;
using DevExpress.Utils;

using System.Net;
using SmartCadCore.Common;
using Smartmatic.SmartCad.Map.Util;
using SmartCadCore.Core;

namespace Smartmatic.SmartCad.Map.Google
{
    /// <summary>
    /// TODO:
    /// </summary>
    /// <className>GoogleControlEx</className>
    /// <author email="arturo.aguero@smartmatic.com">Aruro Agüero</author>
    /// <date>2011/03/20</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    public partial class GoogleControlEx : MapControlEx
    {
        private double mapDistance = 0;
        private string searchResults = "";
        private bool isInitialized = false;
        BackgroundWorker worker = new BackgroundWorker();
        private GeoPoint actualPoint;
        public bool MapLoaded { get; set; }
        private string homeMessage = 
            @"<div id='content'>" +
                "<h2>" + ResourceLoader.GetString2("Home") +": </h2>" +
		        "<h2 id='firstHeading'> {0} </h2>" +
		        "<div id='bodyContent'> " +
                    "<p>" + ResourceLoader.GetString2("Latitude") + 
                    ": {1}, " + ResourceLoader.GetString2("Longitude") + ": {2}</p>" +
		        "</div>" +
	        "</div>";

        private string structMessage =
            @"<div id='content'>" +
                "<h2>" + ResourceLoader.GetString2("Struct") + ": </h2>" +
                "<h2 id='firstHeading'> {0} </h2>" +
            "</div>";

        private IDictionary<string, GoogleLayer> layers = new Dictionary<string,GoogleLayer>();
        private string MapsIconsFolder = "";

        #region Constructor
        public GoogleControlEx()
        :base()
        {
            InitializeComponent();
            this.ShowDistance = false;
            this.ruler.Visible = false;
            mapControl.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(mapControl_DocumentCompleted);
            MapsIconsFolder = SmartCadConfiguration.MapsNewFolder;// TrimStart('\\');
        }
        #endregion
       
        #region Methods

        public override void InitializeMaps()
        {
            mapControl.Navigate(new Uri(String.Format("file:///{0}/GoogleMap.html", System.IO.Directory.GetCurrentDirectory())));   
        }

        void mapControl_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            if (!isInitialized)
            {

                object[] objs = new object[4];
                objs[0] = CenterLat;
                objs[1] = CenterLon;
                objs[2] = 18;
                objs[3] = "Ups! it seem you do not have internet.";

                mapControl.Document.InvokeScript("initialize", objs);
                mapControl.Dock = DockStyle.None;
                mapControl.Dock = DockStyle.Fill;

                try
                {
                    mapControl.Document.GetElementById("tool").AttachEventHandler("onclick", GoogleMapToolUsed);
                    mapControl.Document.GetElementById("toolEnd").AttachEventHandler("onclick", GoogleMapToolEnding);
                    mapControl.Document.GetElementById("searched").AttachEventHandler("onclick", GoogleMapSearched);
                    mapControl.Document.GetElementById("feature").AttachEventHandler("onclick", GoogleMapFeatureSelected);
                    mapControl.Document.GetElementById("viewChanged").AttachEventHandler("onclick", GoogleMapViewChanged);
                }
                catch { }

                if (Initialized != null)
                    Initialized(this, null);

            isInitialized = true;
        }
    }

        public override void CreateTable(MapObject emptyObject, string tableName, bool objEnable, bool lblEnable, int objZoom, int lblZoom)
        {
            if (layers.ContainsKey(tableName) == false)
            {
                layers.Add(tableName, new GoogleLayer(tableName));
                layers.Add(tableName + "Labels",new GoogleLayer(emptyObject.Fields.Keys.First<string>()));
            }
        }

        

        public override void CreateLineTable(string type)
        {
            //TODO: REVIEW
            string name = DynTableDistance;
            if (type == "RouteTemp")
            {
                name = DynTableRouteTemp;
            }

            if (layers.ContainsKey(name) == false)
            {
                layers.Add(name, new GoogleLayer(name));
            }
        }

        public override void SetLeftButtonTool(string p)
        {
            ToolInUse = p;
            if (ToolInUse == ADD_POLYLINE_CODE || ToolInUse == ADD_POLYGON_CODE)
            {
                if (layers.ContainsKey(DynTableTemp))
                {
                    layers.Remove(DynTableTemp);
                }
            }
            else if (ToolInUse == CENTER_CODE) 
            {
                string lat = CenterLat.ToString();
                string lon = CenterLon.ToString();

                object[] objs = new object[2];
                objs[0] = lat;//actualPoint.Lat.ToString();
                objs[1] = lon;//actualPoint.Lon.ToString();
                mapControl.Document.InvokeScript("CenterMap", objs);
                ToolInUse = SELECT_CODE;
                p = SELECT_CODE;
            }
            else if (ToolInUse == ZOOM_IN_CODE)
            {
                mapControl.Document.InvokeScript("ZoomIn");
            }
            else if (ToolInUse == ZOOM_OUT_CODE) 
            {
                mapControl.Document.InvokeScript("ZoomOut");
            }
            FormUtil.InvokeRequired(this, new MethodInvoker(delegate
            {
                if (mapControl.Document != null)
                {
                    mapControl.Document.InvokeScript("SetToolInUse", new object[] { p });
                }
            }));
        }



        public override void RefreshZoomTrackBar()
        {
            //DONT DO ANYTHING
        }

        public override void BuildPreQuerys(Dictionary<string, IList> layersInfo)
        {
            //TODO: REVIEW
        }

        public override void EnableLayer(string layerName, bool enable)
        {
            //TODO: ENABLE AN SPECIFIC LAYER
        }

        public override void UpdateObject(MapObject obj)
        {
            string tableName = GetTableName(obj);
            if (layers[tableName].Contains(obj))
            {
                if (obj is MapPoint)
                    UpdateObject((MapPoint)obj, tableName);
                else if (obj is MapPolygon)
                    UpdateObject((MapPolygon)obj, tableName);
                else
                    UpdateObject((MapCurve)obj, tableName);

                layers[tableName][layers[tableName].IndexOf(obj)] = obj;
            }
        }

        private void UpdateObject(MapPoint obj, string tableName)
        {
            FormUtil.InvokeRequired(this, new MethodInvoker(delegate
            {
                object[] objs = new object[6];
                objs[0] = obj.Code + tableName;
                objs[1] = obj.Fields.Values.First().ToString();
                objs[2] = obj.IconName == "" ? "" : MapsIconsFolder + obj.IconName.Replace(".BMP", ".png").Replace(".bmp", ".png");
                objs[3] = obj.Position.Lat.ToString().Replace(',', '.');
                objs[4] = obj.Position.Lon.ToString().Replace(',', '.');
                objs[5] = "";//string.Format(structMessage, mos.Fields["Name"]);

                mapControl.Document.InvokeScript("UpdatePoint", objs);
            }));
        }

        private void UpdateObject(MapPolygon obj, string tableName)
        {
            FormUtil.InvokeRequired(this, new MethodInvoker(delegate
            {
                string array = "[";
                foreach (GeoPoint point in obj.Geometry)
                {
                    array += "[" + point.Lat.ToString().Replace(',', '.') + ","
                          + point.Lon.ToString().Replace(',', '.') + "],";
                }
                if (array.Length > 1)
                    array = array.Remove(array.Length - 1);
                array += "]";

                object[] objs = new object[6];
                objs[0] = "true";
                objs[1] = obj.Code + tableName;
                objs[2] = obj.Fields.Values.First().ToString();
                objs[3] = array;
                objs[4] = "#" + ColorToHexString(obj.Color).ToUpper();
                objs[5] = "";//string.Format(structMessage, mos.Fields["Name"]);

                mapControl.Document.InvokeScript("UpdatePolygon", objs);
            }));
        }

        private void UpdateObject(MapCurve obj, string tableName)
        {
           FormUtil.InvokeRequired(this, new MethodInvoker(delegate
           {
               string array = "[";
               foreach (GeoPoint point in obj.Geometry)
               {
                   array += "[" + point.Lat.ToString().Replace(',', '.') + ","
                         + point.Lon.ToString().Replace(',', '.') + "],";
               }
               if (array.Length > 1)
                   array = array.Remove(array.Length - 1);
               array += "]";

               object[] objs = new object[6];
               objs[0] = "false";
               objs[1] = obj.Code + tableName;
               objs[2] = obj.Fields.Values.First().ToString();
               objs[3] = array;
               objs[4] = "#" + ColorToHexString(obj.Color).ToUpper();
               objs[5] = "";//string.Format(structMessage, mos.Fields["Name"]);

               mapControl.Document.InvokeScript("UpdatePolygon", objs);
           }));
        }

        private string GetTableName(MapObject obj)
        {
            string tableName = "";
            if (obj is MapObjectIncident)
                tableName = DynTableIncidentsName;
            else if (obj is MapObjectStruct)
                tableName = DynTablePostName;
            else if (obj is MapObjectZone)
                tableName = DynTablePolygonZoneName;
            else if (obj is MapObjectStation)
                tableName = DynTablePolygonStationName;
            else if (obj is MapObjectTrack)
                tableName = DynTableTracksName;
            else if (obj is MapObjectUnitTrack)
                tableName = DynTableUnitTracksName;
            else if (obj is MapObjectUnitFollow)
                tableName = obj.Fields["CustomCode"].ToString();
            else if (obj is MapObjectRoute || obj is MapObjectBusStop)
                tableName = DynTableRoute;
            else if (obj is MapObjectAlarm)
                tableName = DynTableTelemetryAlarm;
            else if (obj is MapObjectUnit)
                tableName = DynTableUnitsName;
            else
                tableName = DynTableTemp;

            return tableName;
        }

        public override void InsertObject(MapObject obj)
        {
            string tableName = GetTableName(obj);

            if (layers.ContainsKey(tableName) && !layers[tableName].Contains(obj))
            {
                if (obj is MapPoint)
                    InsertObject((MapPoint)obj, tableName);
                else if (obj is MapPolygon)
                    InsertObject((MapPolygon)obj, tableName);
                else
                    InsertObject((MapCurve)obj, tableName);
            }
        }

        private void InsertObject(MapPoint obj, string tableName)
        {
            FormUtil.InvokeRequired(this, new MethodInvoker(delegate
            {
                object[] objs = new object[6];
                objs[0] = obj.Code + tableName;
                if (obj.Fields != null) objs[1] = obj.Fields.Values.First();
                objs[2] = obj.IconName == "" ? "" : MapsIconsFolder + obj.IconName.Replace(".BMP", ".png").Replace(".bmp", ".png");
                objs[3] = obj.Position.Lat.ToString().Replace(',', '.');
                objs[4] = obj.Position.Lon.ToString().Replace(',', '.');
                objs[5] = "";//string.Format(structMessage, mos.Fields["Name"]);

                var response = mapControl.Document.InvokeScript("InsertPoint", objs);
               
            }));

            layers[tableName].Add(obj);
        }

        private void InsertObject(MapPolygon obj, string tableName)
        {
            FormUtil.InvokeRequired(this, new MethodInvoker(delegate
            {
                string array = "[";
                foreach (GeoPoint point in obj.Geometry)
                {
                    array += "[" + point.Lat.ToString().Replace(',', '.') + ","
                          + point.Lon.ToString().Replace(',', '.') + "],";
                }
                if (array.Length > 1)
                    array = array.Remove(array.Length - 1);
                array += "]";

                object[] objs = new object[6];
                objs[0] = "true";
                objs[1] = obj.Code + tableName;
                if(obj.Fields !=null) objs[2] = obj.Fields.Values.First().ToString();
                objs[3] = array;
                objs[4] = "#" + ColorToHexString(obj.Color).ToUpper().Substring(0, 6);
                objs[5] = "";//string.Format(structMessage, mos.Fields["Name"]);

                try
                {
                    mapControl.Document.InvokeScript("InsertPolygon", objs);
                }
                catch {}
            }));

            layers[tableName].Add(obj);
        }

        private void InsertObject(MapCurve obj, string tableName)
        {
            FormUtil.InvokeRequired(this, new MethodInvoker(delegate
            {
                string array = "[";
                foreach (GeoPoint point in obj.Geometry)
                {
                    array += "[" + point.Lat.ToString().Replace(',', '.') + ","
                          + point.Lon.ToString().Replace(',', '.') + "],";
                }
                if (array.Length > 1)
                    array = array.Remove(array.Length - 1);
                array += "]";

                object[] objs = new object[6];
                objs[0] = "false";
                objs[1] = obj.Code + tableName;
                if (obj.Fields != null) objs[2] = obj.Fields.Values.First();
                objs[3] = array;
                objs[4] = "#" + ColorToHexString(obj.Color).ToUpper();
                objs[5] = "";//string.Format(structMessage, mos.Fields["Name"]);

                try
                {
                    mapControl.Document.InvokeScript("InsertPolygon", objs);
                }
                catch {}
            }));

            layers[tableName].Add(obj);
        }

        public override void InsertTempObject(MapObject obj, string tableName)
        {
            if (!layers.ContainsKey(tableName))
            {
                layers.Add(tableName, new GoogleLayer(""));
            }

            obj.Fields = new Dictionary<string, object>();
            obj.Fields.Add("", "");
            if (obj is MapPoint)
                InsertObject((MapPoint)obj,tableName);
            else if (obj is MapPolygon)
                InsertObject((MapPolygon)obj,tableName);
            else
                InsertObject((MapCurve)obj,tableName);
        }

        public override void DeleteObject(MapObject obj)
        {
            DeleteObject(obj, GetTableName(obj));
        }

        private void DeleteObject(MapObject obj, string tableName)
        {
            FormUtil.InvokeRequired(this, new MethodInvoker(delegate
            {
                object[] objs = new object[1];
                objs[0] = obj.Code + tableName;
                mapControl.Document.InvokeScript("DeleteObject", objs);
            }));

            layers[tableName].Remove(obj);
        }


        //TODO: ELIMINAR--> debería usarse InsertTempObject
        public override void InsertTempPolygonInLayer(List<GeoPoint> points, string tableName)
        {
            MapPolygon mp = new MapPolygon(0, Color.Black);
            mp.Geometry = points;
            InsertTempObject(mp, tableName);
        }

        //TODO: ELIMINAR--> debería usarse InsertTempObject
        public override void InsertPointInTempLayer(GeoPoint p)
        {
            MapPoint mp = new MapPoint(0, p, "");
            InsertTempObject(mp, DynTableTemp);
        }

        public override void DrawPing(GeoPoint selectedPosition)
        {
            CenterMapInPoint(selectedPosition);
            MapPoint mp = new MapPoint(0, selectedPosition, "");
            InsertTempObject(mp, DynTableTemp);
        }

        public override bool CheckOverlaps(List<GeoPoint> points, List<GeoPoint> points2, string tableName)
        {
            Polygon newPol = new Polygon(points);
            Polygon pol = new Polygon(points2);

            return Polygon.Intersects(newPol, pol);
        }

        public override bool CheckContains(List<GeoPoint> points, List<GeoPoint> points2, string tableName)
        {
            bool contains = true;
            foreach (GeoPoint point in points2)
            {
                contains &= ApplicationUtil.PointInPolygon(point, points);
                if (!contains) break;
            }
            return contains;
        }

        public override void ClearActions(MapActions clearAction)
        {
            switch (clearAction)
            {
                case MapActions.ClearDistanceTable:
                    DeleteAllInTable(DynTableDistance);
                    break;
                case MapActions.ClearTempTable:
                    DeleteAllInTable(DynTableTemp);
                    break;
                default:
                    break;
            }
        }

        public override void ClearTables()
        {
            DeleteAllInTable(DynTableDistance);
            DeleteAllInTable(DynTableTemp);
        }

        public override void ClearTable(string tableName)
        {
            DeleteAllInTable(tableName);
        }

        private void DeleteAllInTable(string tableName)
        {
            if (layers.ContainsKey(tableName))
            {
                while (layers[tableName].Count > 0)
                {
                    DeleteObject(layers[tableName][0], tableName);
                }
            }
        }

        //CAMBIAR POR METODO MAS GENERICO
        public override void DeleteAllTracksUnless(int unitCode)
        {
            if (layers.ContainsKey(DynTableTracksName))
            {
                foreach (MapObject obj in layers[DynTableTracksName])
                {
                    if(obj.Code != unitCode)
                        DeleteObject(obj, DynTableTracksName);  
                }
            }
        }

        //CAMBIAR POR METODO MAS GENERICO
        public override void RestoreAllTracksUnless(int unitCode)
        {
            if (layers.ContainsKey(DynTableTracksName))
            {
                foreach (MapObject obj in layers[DynTableTracksName])
                {
                    if (obj.Code != unitCode)
                        UpdateObject(obj);
                }
            }
        }

        //CAMBIAR POR METODO MAS GENERICO
        public override void DeleteAllUnitTracks()
        {
            DeleteAllInTable(DynTableUnitTracksName);
        }

        public override bool ReproduceTrack(MapPoint mapPoint)
        {
            if (layers.ContainsKey(DynTableUnitTracksName))
            {
                UpdateObject(mapPoint, DynTableUnitTracksName);
                CenterMapInPoint(mapPoint.Position);
                bool visible = false;
                FormUtil.InvokeRequired(this, new MethodInvoker(delegate
                {
                    if (mapControl.Url != null)
                    {
                        object[] objs = new object[2] { mapPoint.Position.Lat, mapPoint.Position.Lon };
                        visible = (bool)mapControl.Document.InvokeScript("ViewContains", objs);
                    }
                }));
                if (!visible) { CenterMapInPoint(mapPoint.Position); }
                return false;
            }
            return true;
        }

        public override void CenterMap(int viewDistance)
        {
            int mapZoom = (viewDistance / 512) + 5;
            FormUtil.InvokeRequired(this, new MethodInvoker(delegate
            {
                if (mapControl.Url != null)
                {
                    mapControl.Document.InvokeScript("CenterMap", new object[1]{mapZoom});
                }
            }));
        }

        public override void CenterMapInPoint(GeoPoint point)
        {
            FormUtil.InvokeRequired(this, new MethodInvoker(delegate
            {
                if (mapControl.Url != null)
                {
                    object[] objs = new object[2];
                    objs[0] = point.Lat;
                    objs[1] = point.Lon;
                    mapControl.Document.InvokeScript("CenterMap", objs);
                }
            }));
        }

        public override Dictionary<string, string> GetLayersName()
        {
            Dictionary<string, string> names = new Dictionary<string, string>();
 
            foreach (KeyValuePair<string,GoogleLayer> layer in layers)
            {
                names.Add(layer.Key, layer.Value.Label);
            }

            return names;
        }

        public override void ReloadMap()
        {
 
        }

        public override void  RefreshMap()
        {

        }

        public override IList<GeoAddress> Search(GeoAddress geoAddress)
        {
            List<GeoAddress> resultsList = new List<GeoAddress>();
            try
            {
                string searchText = "";

                if (string.IsNullOrEmpty(geoAddress.FullText))
                {
                    searchText = geoAddress.ToString() + ", " + CurrentMap;
                }
                else
                {
                    searchText = geoAddress.FullText + ", " + CurrentMap;
                }

                if (searchText != ", " + CurrentMap)
                {
                    searchResults = "";
                    FormUtil.InvokeRequired(this, new MethodInvoker(delegate
                    {
                        if (mapControl.Url != null)
                        {
                            object[] objs = { searchText, DynTableTemp };
                            mapControl.Document.InvokeScript("SearchAddress", objs);
                        }
                    }));

                    while (searchResults == "")
                    {
                        Thread.Sleep(50);
                    }

                    if (string.IsNullOrEmpty(searchResults) == false && searchResults != "noResults")
                    {
                        string[] results = searchResults.Split(';');
                        foreach (string address in results)
                        {
                            if (address != "")
                            {
                                string[] parts = address.Split('\\');
                                string[] geoAddressParts = parts[0].Split(',');
                                GeoAddress ga = new GeoAddress();//geoAddressParts[0], "", "", "");
                                foreach (string component in geoAddressParts)
                                {
                                    if (component != "" && component.Contains(':'))
                                    {
                                        string[] nameValue = component.Split(':');
                                        if (nameValue[1] == "route" || nameValue[1] == "street_address")
                                        {
                                            ga.Street = nameValue[0];
                                        }
                                        else if (nameValue[1] == "sublocality"
                                            || nameValue[1] == "neighborhood"
                                            || nameValue[1] == "administrative_area_level_2"
                                            || nameValue[1] == "administrative_area_level_3")
                                        {
                                            ga.Zone += nameValue[0] + ", ";
                                        }
                                        else if (nameValue[1] != "locality"
                                            && nameValue[1] != "administrative_area_level_1"
                                            && nameValue[1] != "country")
                                        {
                                            ga.Reference += nameValue[0] + ", ";
                                        }
                                    }
                                }
                                if (ga.Zone != null) ga.Zone = ga.Zone.Remove(ga.Zone.Length - 2, 2);
                                if (ga.Reference != null) ga.Reference = ga.Reference.Remove(ga.Reference.Length - 2, 2);

                                string[] lonlat = parts[1].Split(',');
                                ga.Point = new GeoPoint(
                                    double.Parse(lonlat[1].Substring(0, lonlat[1].Length - 2), new CultureInfo(1033).NumberFormat),
                                    double.Parse(lonlat[0].Substring(1), new CultureInfo(1033).NumberFormat));
                                resultsList.Add(ga);
                            }
                        }
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.ToString()); }
            return resultsList;
        }

        // Searched event
        private void GoogleMapSearched(object sender, EventArgs e)
        {
            searchResults = mapControl.Document.InvokeScript("GetSearchResults").ToString();
        }
        
        public override void ToolFeatureSelected(EventHandler toolstripmenuitem_Click, FeatureSelectedEventArgs e, ref ToolTipController toolTipController1)
        {
            double ratio = mapDistance/1000 * 0.015;

            int count = 0;
            ContextMenuStrip cms = new ContextMenuStrip();
            foreach (string layerName in layers.Keys)
            {
                if (layerName != this.DynTableTemp)
                {
                    ToolStripMenuItem baseToolStripMenu = new ToolStripMenuItem();
                    baseToolStripMenu.Text = ResourceLoader.GetString2(layerName);
                    int count2 = count;
                    foreach (MapPoint obj in layers[layerName])
                    {
                        if (obj.Position.Lat != 0.0 && obj.Position.Lon != 0.0)
                        {
                            if (GisServiceUtil.CalculateDistance(obj.Position, e.Position) < ratio)
                            {
                                ToolStripMenuItem toolstripmenuitem = new ToolStripMenuItem();
                                toolstripmenuitem.Text = obj.Fields.Values.First().ToString();
                                toolstripmenuitem.Tag = CreateBallonData(obj.Code.ToString(),
                                    layerName, obj.Position,
                                    e.Position.Lon, e.Position.Lat);
                                toolstripmenuitem.Click += toolstripmenuitem_Click;
                                baseToolStripMenu.DropDownItems.Add(toolstripmenuitem);
                                count++;
                            }
                        }
                    }
                    if (count > count2) cms.Items.Add(baseToolStripMenu);
                }
            }
            if (count > 1)
                cms.Show(MousePosition);
            else
            {
                ((ToolStripMenuItem)cms.Items[0]).DropDownItems[0].PerformClick();
            }
        }
       
        public override System.Drawing.Point GetRelativePoint(GeoPoint point)
        {
            Point newPoint;
            string obj = "10;10";
            FormUtil.InvokeRequired(this, new MethodInvoker(delegate
            {
                obj = mapControl.Document.InvokeScript("GetPointFromLatLon",
                    new object[] { point.Lat, point.Lon }).ToString();
            }));
            string[] p = obj.Split(';');

            newPoint = new Point(int.Parse(p[0].Split('.')[0]),
                            int.Parse(p[1].Split('.')[0])
                        );

            return newPoint;
        }

        public override bool IsVisible(System.Drawing.Point point)
        {
            bool result = false;
            FormUtil.InvokeRequired(this, new MethodInvoker(delegate
            {
                result = (bool)mapControl.Document.InvokeScript("IsVisible",
                    new object[] { point.X, point.Y });
            }));

            return result;
        }

        public override void RecalculateAll()
        {
            //CenterMap(5120);
        }

        public override void HighLightSingle(GeoPoint point)
        {
            CenterMapInPoint(point);

            //TODO: hacerle alguna animación o high light
            Point clientPoint = GetRelativePoint(point);
            for (int j = 0; j < 2; j++)
            {
                int xcenter = clientPoint.X - 32;
                int ycenter = clientPoint.Y - 32;
                int radio = 76;
                for (int i = 0; i < 4; i++)
                {
                    SolidBrush sb = new SolidBrush(Color.FromArgb(75, 255, 255, 255));
                    Graphics gra = this.mapControl.CreateGraphics();
                    gra.DrawEllipse(new Pen(new SolidBrush(Color.Blue), 2), xcenter, ycenter, radio, radio);
                    gra.FillEllipse(sb, xcenter, ycenter, radio, radio);
                    gra.Dispose();
                    Thread.Sleep(85);
                    FormUtil.InvokeRequired(this, () =>
                    {
                        this.Invalidate(new System.Drawing.Rectangle(xcenter - 1, ycenter - 1, radio + 3, radio + 3));
                        this.Update();
                    });
                    radio -= 18;
                    xcenter += 9;
                    ycenter += 9;
                }
            }
        }

        public override void HighLightUnit(MapObjectUnit unit)
        {
            HighLightSingle(unit.Position);
        }

        public override void HighLightIncident(MapObjectIncident inc)
        {
            HighLightSingle(inc.Position);
        }

        public override void ShowCrimeMap(List<GeoPoint> source, int zoom, int clusterSize)
        {
            FormUtil.InvokeRequired(this, new MethodInvoker(delegate
            {
                if (mapControl.Url != null)
                {
#if DEBUG
                    source.Add(new GeoPoint(-122.394365, 37.785114));
                    source.Add(new GeoPoint(-122.394441, 37.785022));
                    source.Add(new GeoPoint(-122.394635, 37.784823));
                    source.Add(new GeoPoint(-122.394629, 37.784719));
                    source.Add(new GeoPoint(-122.394176, 37.785069));
                    source.Add(new GeoPoint(-122.393650, 37.785500));
                    source.Add(new GeoPoint(-122.393291, 37.785770));
#endif
                    string array = "[";
                    foreach (GeoPoint point in source)
                    {
                        array += "[" + point.Lat.ToString().Replace(',', '.') + ","
                              + point.Lon.ToString().Replace(',', '.') + "],";
                    }
                    if (array.Length > 1)
                        array = array.Remove(array.Length - 1);
                    array += "]";

                    object[] objs = new object[] { array, zoom, clusterSize };
                    mapControl.Document.InvokeScript("ShowCrimeMap", objs);
                }
            }));
        }

        public override void ShowHeatMap(List<GeoPoint> source, int zoom)
        {
            FormUtil.InvokeRequired(this, new MethodInvoker(delegate
            {
                if (mapControl.Url != null)
                {
#if DEBUG
                    source.Add(new GeoPoint(-122.394365, 37.785114));
                    source.Add(new GeoPoint( -122.394441, 37.785022));
                    source.Add(new GeoPoint(-122.394635, 37.784823));
                    source.Add(new GeoPoint(-122.394629, 37.784719));
                    source.Add(new GeoPoint(-122.394176, 37.785069 ));
                    source.Add(new GeoPoint(-122.393650, 37.785500));
                    source.Add(new GeoPoint(-122.393291, 37.785770));
#endif

                    string array = "[";
                    foreach (GeoPoint point in source)
                    {
                        array += "[" + point.Lat.ToString().Replace(',', '.') + ","
                              + point.Lon.ToString().Replace(',', '.') + "],";
                    }
                    if (array.Length > 1)
                        array = array.Remove(array.Length - 1);
                    array += "]";

                    object[] objs = new object[] { array, zoom };
                    mapControl.Document.InvokeScript("ShowHeatMap", objs);
                }
            }));
        }


        static char[] hexDigits = {
         '0', '1', '2', '3', '4', '5', '6', '7',
         '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        private static string ColorToHexString(Color color)
        {
            byte[] bytes = new byte[3];
            bytes[0] = color.R;
            bytes[1] = color.G;
            bytes[2] = color.B;
            char[] chars = new char[bytes.Length * 2];
            for (int i = 0; i < bytes.Length; i++)
            {
                int b = bytes[i];
                chars[i * 2] = hexDigits[b >> 4];
                chars[i * 2 + 1] = hexDigits[b & 0xF];
            }
            return new string(chars);
        }

        #endregion

        #region Events
        
        public override event ToolUsedEventHandler ToolUsed;
        public override event EventHandler Initialized;
        public override event FeatureSelectedEventHandler FeatureSelected;
        public override event ViewChangedEventHandler ViewChanged;

        // mapClick event handler
        public void GoogleMapToolUsed(object sender, EventArgs e)
        {
            ToolStatus status = ToolStatus.Starting;
            GeoPoint point = GetCurrentToolPosition();
            if (ToolInUse == ADD_POSITION_CODE)
            {
                if (ToolUsed != null)
                {
                    ToolUsed(this, new ToolUsedEventArgs(ToolStatus.InProgress, point, ToolInUse));
                }
                status = ToolStatus.Ending;
            }
            else if (ToolInUse == ADD_POLYGON_CODE)
            {
                MapCurve curve = new MapCurve(0, Color.Black);
                if (layers.ContainsKey(DynTableTemp))
                {
                    curve = (MapCurve)layers[DynTableTemp][0];
                }
                curve.Geometry.Add(point);
                InsertTempObject(curve, DynTableTemp);
                status = ToolStatus.InProgress;
            }
            else if (ToolInUse == ADD_POLYLINE_CODE)
            {
                MapPolygon polygon = new MapPolygon(0, Color.Black);
                if (layers.ContainsKey(DynTableTemp))
                {
                    polygon = (MapPolygon)layers[DynTableTemp][0];
                }
                polygon.Geometry.Add(point);
                InsertTempObject(polygon, DynTableTemp);
                status = ToolStatus.InProgress;
                
            }

            else if (ToolInUse == SELECT_CODE) 
            {
                actualPoint = point;
            }
            if (ToolUsed != null)
            {
                ToolUsed(this, new ToolUsedEventArgs(status, point, ToolInUse));
            }
        }

        // mapDoubleClick event handler
        private void GoogleMapToolEnding(object sender, EventArgs e)
        {
            if (ToolUsed != null)
            {
                ToolUsed(this, null);
            }
            SetLeftButtonTool("Pan");
        }

        void GoogleMapFeatureSelected(object sender, EventArgs e)
        {
            if (FeatureSelected != null)
            {
                GeoPoint point = GetCurrentToolPosition();
                FeatureSelected(this, new FeatureSelectedEventArgs(sender, point));
            }
        }

        void GoogleMapViewChanged(object sender, EventArgs e)
        {
            actualPoint = GetCurrentToolPosition();
            mapDistance = double.Parse(mapControl.Document.InvokeScript("GetViewDistance", new object[] { }).ToString());
            if (ViewChanged != null)
            {
                ViewChanged(this, new ViewChangedEventArgs());
            }
        }
        #endregion

        private GeoPoint GetCurrentToolPosition()
        {


            string pos = "0;0";
            FormUtil.InvokeRequired(this, new MethodInvoker(delegate
            {
                pos = mapControl.Document.InvokeScript("GetToolPosition", new object[] { }).ToString();
            }));
            string[] coords = pos.Split(';');
            
            return new GeoPoint(
                double.Parse(coords[1], CultureInfo.InvariantCulture.NumberFormat),
                double.Parse(coords[0], CultureInfo.InvariantCulture.NumberFormat)
            );
        }
    }

    public class GoogleLayer : List<MapObject>
    {
        public string Label;

        public GoogleLayer (string lbl)
        :base()
        {
            Label = lbl;
        }   
    }
}
