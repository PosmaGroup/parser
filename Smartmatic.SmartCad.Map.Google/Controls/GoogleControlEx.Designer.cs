﻿namespace Smartmatic.SmartCad.Map.Google
{
    partial class GoogleControlEx : MapControlEx
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mapControl = new System.Windows.Forms.WebBrowser();
            this.SuspendLayout();
            // 
            // mapControl
            // 
            this.mapControl.Location = new System.Drawing.Point(0, 0);
            this.mapControl.MinimumSize = new System.Drawing.Size(20, 20);
            this.mapControl.Name = "mapControl";
            this.mapControl.ScriptErrorsSuppressed = true;
#if DEBUG
            this.mapControl.ScriptErrorsSuppressed = false;
#endif
            this.mapControl.ScrollBarsEnabled = false;
            this.mapControl.Size = new System.Drawing.Size(650, 640);
            this.mapControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mapControl.TabIndex = 8;
            // 
            // GoogleControlEx
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this.mapControl);
            this.Name = "GoogleControlEx";
            this.Controls.SetChildIndex(this.mapControl, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.WebBrowser mapControl;

    }
}
