﻿/// <reference path="googlemap.html" />
var map = null;
var infowindow = null;
var initZoom = 17;
var markers = [];
var geocoder;
var zindex = 0;
var actualZoom;
var zoomOperation = 1;
var toolInUse = "";
var toolPosition = "0;0";
var markerClusterer = null;
var addressList = "";
var imageUrl = '../Maps/Icons/m1.png';
var styles = [{
    url: '../Maps/Icons/m2.png',
    height: 60,
    width: 60,
    anchor: [32, 0],
    textColor: '#000000',
    opt_textSize: 11
}, {
    url: '../Maps/Icons/m3.png',
    height: 80,
    width: 80,
    opt_anchor: [48, 0],
    opt_textColor: '#000000',
    opt_textSize: 12
}, {
    url: '../Maps/Icons/m3.png',
    width: 100,
    height: 100,
    opt_anchor: [64, 0],
    opt_textColor: '#000000',
    opt_textSize: 13
}];


function initialize(lat, lon, zoom, messageError) {
    try {
        geocoder = new google.maps.Geocoder();

        var latLng = new google.maps.LatLng(lat, lon);
        var myOptions = {
            zoom: zoom,
            center: latLng
        };
        map = new google.maps.Map(document.getElementById('map'),
		    myOptions);

        //var image = './Images/estructuras.png';
        //var beachMarker = new google.maps.Marker({
        //    position: { lat: -33.890, lng: 151.274 },
        //    map: map,
        //    icon: image
        //});

        google.maps.event.addListener(map, 'click', function (event) {
            if (toolInUse == "AddPoint" ||
               toolInUse == "AddPolygon" ||
               toolInUse == "AddPolyline" ||
               toolInUse == "Select" ||
               toolInUse == "Pan"
                ) {            
                toolPosition = event.latLng.lat() + ';' + event.latLng.lng();
                document.getElementById("tool").click();
            }
            //toolPosition = event.latLng.lat() + ';' + event.latLng.lng();
        });

        google.maps.event.addListener(map, 'bounds_changed', function () {
            document.getElementById("viewChanged").click();
        });

    }
    catch (ex) {
        document.write('<div id="content">' +
			'<h2>' + messageError + '</h2>' +
		'</div>');
    };
}

function tool_Click() { }
function toolEnd_Click() { }
function searched_Click() { }
function feature_Click() { }
function viewChanged_Click() { }

function SetToolInUse(p) {
    toolInUse = p;
}

function GetToolPosition() {
    return toolPosition;
}

function GetPointFromLatLon(lat, lon) {
    var scale = Math.pow(2, map.getZoom());
    var nw = new google.maps.LatLng(
        map.getBounds().getNorthEast().lat(),
        map.getBounds().getSouthWest().lng()
    );
    var worldCoordinateNW = map.getProjection().fromLatLngToPoint(nw);
    var worldCoordinate = map.getProjection().fromLatLngToPoint(new google.maps.LatLng(lat, lon));
    var pixelOffset = new google.maps.Point(
        Math.floor((worldCoordinate.x - worldCoordinateNW.x) * scale),
        Math.floor((worldCoordinate.y - worldCoordinateNW.y) * scale)
    );

    return pixelOffset.x + ";" + pixelOffset.y;
}

function IsVisible(x, y) {
    var scale = Math.pow(2, map.getZoom());
    var nw = new google.maps.LatLng(
        map.getBounds().getNorthEast().lat(),
        map.getBounds().getSouthWest().lng()
    );
    var worldCoordinateNW = map.getProjection().fromLatLngToPoint(nw);

    var xWorld = (x / scale) - worldCoordinateNW.x;
    var yWorld = (y / scale) - worldCoordinateNW.y;

    var worldCoordinate = map.getProjection().fromPointToLatLng(new google.maps.Point(xWorld, yWorld));

    var mapBounds = new google.maps.LatLngBounds(this.map.getBounds().getSouthWest(),
      this.map.getBounds().getNorthEast());

    return mapBounds.contains(worldCoordinate);
}

function GetViewDistance(y) {
    var bounds = map.getBounds();

    var distance = google.maps.geometry.spherical.computeDistanceBetween(bounds.getNorthEast(), bounds.getSouthWest());
    return distance;
}

function PointInPolygon(point, polygon) {
    return polygon.containsLatLng(point);
}

function ViewContains(lat, lon) {
    //return map.getBounds().contains( new google.maps.LatLng(lat, lon));

    var mapBounds = new google.maps.LatLngBounds(this.map.getBounds().getSouthWest(),
      this.map.getBounds().getNorthEast());
    return mapBounds.contains(new google.maps.LatLng(lat, lon));
}

function InsertPoint(id, title, image, lat, lon, message) {
    UpdatePoint(id, title, image, lat, lon, message);
}

function UpdatePoint(id, title, image, lat, lon, message) {
    DeleteObject(id);
    markers.push([id, CreatePoint(title, image, lat, lon, message)]);
}

function CreatePoint(title, image, lat, lon, message) {
    var marker = new google.maps.Marker({
        map: map,
        title: title,
        image: image,
        position: new google.maps.LatLng(lat, lon),
        zIndex: ++zindex
    });
    if (image != '') marker.setIcon(image);

    google.maps.event.addListener(marker, 'click', function (event) {
        var latLng = marker.getPosition();
        toolPosition = latLng.lat() + ';' + latLng.lng();
        document.getElementById("feature").click();
    });

   

    return marker;
}

function InsertPolygon(closedPolygon, id, title, array, color, message) {
    UpdatePolygon(closedPolygon, id, title, array, color, message);
}

function UpdatePolygon(closedPolygon, id, title, array, color, message) {
    DeleteObject(id);
    markers.push([id, CreatePolygon(closedPolygon, title, array, color, message)]);
}

function CreatePolygon(closedPolygon, title, array, color, message) {
    var polygon = null;
    var points = eval(array);
    var coordinates = new Array();

    for (var i = 0, point; point = points[i]; i++) {
        coordinates.push(new google.maps.LatLng(point[0], point[1]));
    }

    if (coordinates.length > 0) {
        if (closedPolygon == 'true') {
            polygon = new google.maps.Polygon({
                paths: coordinates,
                fillColor: color,
                fillOpacity: 0.35,
                strokeOpacity: 0.8
            });
        }
        else {
            polygon = new google.maps.Polyline({
                path: coordinates
            });
        }

        //common options
        polygon.setOptions({
            strokeColor: color,
            strokeWeight: 2,
            zIndex: ++zindex
        });
        polygon.setMap(map);

        if (toolInUse == "AddPolygon" || toolInUse == "AddPolyline") {
            google.maps.event.addListener(polygon, 'dblclick', function (event) {
                document.getElementById("toolEnd").click();
                if (event.stopPropagation) {
                    event.stopPropagation();
                } else {
                    event.cancelBubble = true;
                }
            });
        }
        //attachMessage(polygon, message);
    }
    return polygon;
}

function DeleteObject(id) {
    try {
        if (markers) {
            for (var n = 0, marker; marker = markers[n]; n++) {
                if (marker[0] == id) {
                    if (marker[1] != null) marker[1].setMap(null);
                    markers.splice(n, 1);
                }
            }
        }
    }
    catch (e) { alert('error'); }
}

function CenterMap(zoom) {
    map.setZoom(zoom);
}

function GetZoom() {
    return map.getZoom();
}

function CenterMap(lat, lng) {
    map.setCenter(new google.maps.LatLng(lat, lng));
}




function SearchAddress(address, tableName) {
    addressList = "";
    geocoder.geocode({ 'address': address, 'partialmatch': true }, function (results, status) {
        if (status == 'OK' && results.length > 0) {
            for (var n = 0, address; address = results[n]; n++) {
                for (var i = 0, component; component = address.address_components[i]; i++) {
                    addressList = addressList + component.long_name + ":" + component.types[0] + ",";
                }
                addressList = addressList + "\\" + address.geometry.location + ";";
            }
        }
        else {
            addressList = "noResults";
        }

        document.getElementById("searched").click();
    });
}

function ZoomIn() {
    map.setZoom(GetZoom() + zoomOperation);
}


function ZoomOut() {
    map.setZoom(GetZoom() - zoomOperation);
}
function GetSearchResults() {
    return addressList;
}

function attachMessage(marker, message) {
    if (infowindow) {
        infowindow.close();
    }
    infowindow = new google.maps.InfoWindow(
		{
		    content: message
		});
    google.maps.event.addListener(marker, 'click', function () {
        infowindow.open(map, marker);
    });
}

function ShowCrimeMap(array, zoom, size) {
    var points = eval(array);
    if (markerClusterer) {
        markerClusterer.clearMarkers();
    }

    var markers = [];

    var markerImage = new google.maps.MarkerImage(imageUrl);

    for (var n = 0, point; point = points[n]; n++) {
        var latLng = new google.maps.LatLng(point[0], point[1]);
        var marker = new google.maps.Marker({
            position: latLng,
            draggable: true,
            icon: markerImage
        });
        markers.push(marker);
    }

    markerClusterer = new MarkerClusterer(map, markers, {
        maxZoom: zoom,
        gridSize: size,
        styles: styles
    });

    map.setZoom(zoom);
}


function ShowHeatMap(array, zoom) {
    var data = [];
    var heatmap;
    var points = eval(array);
    map.setZoom(zoom);

    for (var n = 0, point; point = points[n]; n++) {
        data.push(new google.maps.LatLng(point[0], point[1]));
    }

    var pointArray = new google.maps.MVCArray(data);

    heatmap = new google.maps.visualization.HeatmapLayer({
        data: pointArray
    });

    heatmap.setMap(map);
}
