﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadParser;

namespace Smartmatic.SmartCad.Parser.Data
{
    public class LPRData : DataFrame
    {
        #region Constructors
     
        public LPRData()
            :base()
        {
        }

        public LPRData(byte[] dataBuffer)
            :base(dataBuffer)
        {
        }

        #endregion

        #region Override
        public override string NameType
        {
            get
            {
                return "LPR";
            }
        }
        #endregion
    }
}
