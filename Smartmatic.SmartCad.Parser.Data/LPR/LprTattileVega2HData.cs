﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using SmartCadParser;

namespace Smartmatic.SmartCad.Parser.Data
{
    public class LprTattileVega2HData : LPRData
    {
        private string[] Parts { get; set; }
        private object obj = new object();

        #region Constructors

        public LprTattileVega2HData()
          : base() 
        {
            NeedClean = false;
        }

        public LprTattileVega2HData(byte[] dataBuffer)
            : base(dataBuffer)
        {
            NeedClean = false;
        }

        public LprTattileVega2HData(string modemId, DateTime date, string data)
            : base()
        {
            ModemId = modemId;
            Date = date;
            Data = data;
            NeedClean = false;
        }

        #endregion

        #region Override Base
        protected override FrameType GetFrameType()
        {
            if(Buffer[0] != 0)
                return FrameType.Alarm;
            else if (Buffer[0] == 10)
                return FrameType.Sync;
            else
                return FrameType.Incomplete;
        }

        protected override string GetModemId()
        {
            return modemId == null ? "": modemId;
        }

        protected override string GetDate()
        {
            try
            {
                return Date.ToString(Utility.DataBaseFormattedDate);
            }
            catch
            {
                Date = DateTime.Now;
                return Date.ToString(Utility.DataBaseFormattedDate);
            }
        }

        private string GetValue()
        {
            string values = "";
            for (int i = 2; i < Parts.Length; i++)
            {
                values+= Parts[i].TrimStart();
                if (i + 1 < Parts.Length)
                    values+= Separator;
            }
            return values; 
        }

        private string convertToWord(string data)
        {
            if (data.Length < 8)
            {
                data = data.Insert(0, "0");
            }
            if (data.Length < 8)
            {
               data = convertToWord(data);
            }
            return data;
        }

        public override void Decode()
        {
            try
            {
                /*byte[] totalLprTattileBytes = new byte[100000];//The arry with all the data
                int totalLprTattilesize = 0;//The total array length
                int currentLprTattileArrayPosition = 0;//The Lasr position in writer in the array


                int currentSize = TotalLprTattileSize(buffer);
                if (currentSize > 0)//Header
                {
                    totalLprTattileBytes = new byte[150000];
                    totalLprTattilesize = currentSize;
                    currentLprTattileArrayPosition = 0;
                }

                for (int i = 0; i < buffer.Length; i++)
                {
                    if ((buffer[i] == 0) && (buffer[i + 1] == 0) && (buffer[i + 2] == 0) && (buffer[i + 3] == 0) && (buffer[i + 4] == 0) &&
                        (buffer[i + 5] == 0) && (buffer[i + 6] == 0) && (buffer[i + 7] == 0) && (buffer[i + 8] == 0) && (buffer[i + 9] == 0) &&
                        (buffer[i + 10] == 0) && (buffer[i + 11] == 0) && (buffer[i + 12] == 0) && (buffer[i + 13] == 0) && (buffer[i + 14] == 0) &&
                        (buffer[i + 15] == 0) && (buffer[i + 16] == 0) && (buffer[i + 17] == 0) && (buffer[i + 18] == 0) && (buffer[i + 19] == 0) &&
                        (buffer[i + 20] == 0) && (buffer[i + 21] == 0) && (buffer[i + 22] == 0) && (buffer[i + 23] == 0) && (buffer[i + 24] == 0))
                    {
                        //currentLprTattileArrayPosition++;
                        break;
                    }
                    totalLprTattileBytes[currentLprTattileArrayPosition] = buffer[i];
                    currentLprTattileArrayPosition++;
                }

                if (currentLprTattileArrayPosition >= totalLprTattilesize)
                {

                    buffer = totalLprTattileBytes;*/

                    byte[] code = new byte[4];
                    byte[] size = new byte[4];
                    string colorImage = "";
                    string bwImage = "";
                    int lengthColorImage = 0;
                    string plate = "";

                    string frame = ASCIIEncoding.ASCII.GetString(buffer);

                    //DATE TIME
                    int startDate = frame.IndexOf("S_DATE=");
                    Date = DateTime.ParseExact(frame.Substring(startDate + 7, 10) + " " + frame.Substring(startDate + 19 + 7, 12),
                        "yyyy-MM-dd HH-mm-ss-fff", null);

                    //ID
                    int begin = frame.IndexOf("I_SN=") + 5;
                    int end = frame.IndexOf("I_BOARD_FPGA_VER") - 2; // -2 x q esta precedido por \r\n
                    modemId = frame.Substring(begin, end - begin);

                    //PLATE
                    begin = frame.IndexOf("S_PLATE=") + 8;
                    end = frame.IndexOf("I_NREAD") - 2;
                    plate = frame.Substring(begin, end - begin);


                    for (int i = 0; i < buffer.Length; i++)
                    {
                        //Seek the Color image code 14021 --> in bytes: 197 54 0 0 
                        if (buffer[i] == 197 && buffer[i + 1] == 54 && buffer[i + 2] == 0 && buffer[i + 3] == 0)
                        {
                            #region COLOR IMAGE
                            i = i + 4;//Code//i = 48;
                            string n4 = Convert.ToString(buffer[i], 2);
                            n4 = convertToWord(n4);
                            string n3 = Convert.ToString(buffer[i + 1], 2);
                            n3 = convertToWord(n3);
                            string n2 = Convert.ToString(buffer[i + 2], 2);
                            n2 = convertToWord(n2);
                            string n1 = Convert.ToString(buffer[i + 3], 2);
                            n1 = convertToWord(n1);
                            i = i + 4;//Length
                            string lengthInBinary = n1 + n2 + n3 + n4;
                            lengthColorImage = Convert.ToInt32(lengthInBinary, 2);
                            int endColorImagePosition = lengthColorImage + i;
                            if (endColorImagePosition % 4 > 0)
                            {
                                endColorImagePosition += (endColorImagePosition % 4);
                            }

                            for (int j = i; j < endColorImagePosition; j++)
                            {
                                if (colorImage == "")
                                {
                                    colorImage = buffer[j].ToString();
                                }
                                else
                                {
                                    colorImage = colorImage + "," + buffer[j];
                                }
                            }
                            i = endColorImagePosition-1;
                            #endregion
                        }

                        //Seek the Color image code 14020 --> in bytes: 19 54 0 0 
                        if (buffer[i] == 196 && buffer[i + 1] == 54 && buffer[i + 2] == 0 && buffer[i + 3] == 0)
                        {
                            #region BW IMAGE
                            i = i + 4;//Code
                            string n4 = Convert.ToString(buffer[i], 2);
                            n4 = convertToWord(n4);
                            string n3 = Convert.ToString(buffer[i + 1], 2);
                            n3 = convertToWord(n3);
                            string n2 = Convert.ToString(buffer[i + 2], 2);
                            n2 = convertToWord(n2);
                            string n1 = Convert.ToString(buffer[i + 3], 2);
                            n1 = convertToWord(n1);
                            i = i + 4;//Length
                            string lengthInBinary = n1 + n2 + n3 + n4;
                            int length = Convert.ToInt32(lengthInBinary, 2);
                            int endPosition = length + i;
                            if (endPosition % 4 > 0)
                            {
                                endPosition = endPosition + (endPosition % 4);
                            }

                            for (int j = i; j < endPosition; j++)
                            {
                                if (bwImage == "")
                                {
                                    bwImage = buffer[j].ToString(); ;
                                }
                                else
                                {
                                    bwImage = bwImage + "," + buffer[j];
                                }
                            }
                            i = endPosition;
                            #endregion
                        }
                    }

                    if ((colorImage != "" || bwImage != "") && plate != "" && modemId != "")
                    {
                        Data = NameType + Separator + modemId + Separator + Date.ToString(Utility.DataBaseFormattedDate) + Separator + colorImage + Separator + bwImage + Separator + plate;
                        Data = Data.Replace("\0", "");
                    }
                    else
                    {
                        type = FrameType.Incomplete;
                    }
               // }

            }
            catch
            {
            }
        
        }

        public override IList<DataFrame> GetCleanFrames()
        {
            IList<DataFrame> frames = new List<DataFrame>();
            string[] values = this.GetValue().Split(Separator[0]);

            for (int i = 0; i < values.Length; i++)
            {
                string value = values[i];
                string[] valueParts = value.Split('=');

                if (valueParts.Length > 1)
                {
                    frames.Add(new LprTattileVega2HData(ASCIIEncoding.ASCII.GetBytes(
                        ModemId + "," + Parts[1] + "," + valueParts[0] + "," + valueParts[1]
                    )));
                }
            }
            return frames;
        }
        #endregion



        /*private int TotalLprTattileSize(byte[] bytes)
        {
            int size = 0;
            if ((bytes[0] == 24) && (bytes[1] == 0) && (bytes[2] == 0) && (bytes[3] == 0) &&//24
                (bytes[4] == 64) && (bytes[5] == 156) && (bytes[6] == 0) && (bytes[7] == 0) &&//40000
                (bytes[8] == 0) && (bytes[9] == 0) && (bytes[10] == 0) && (bytes[11] == 0)//0
                && (bytes[12] == 0) && (bytes[13] == 0) && (bytes[14] == 0) && (bytes[15] == 0)//0
                && (bytes[16] == 0) && (bytes[17] == 0) && (bytes[18] == 0) && (bytes[19] == 0))//0
            {

                string n4 = Convert.ToString(bytes[20], 2);
                n4 = convertToWord(n4);
                string n3 = Convert.ToString(bytes[21], 2);
                n3 = convertToWord(n3);
                string n2 = Convert.ToString(bytes[22], 2);
                n2 = convertToWord(n2);
                string n1 = Convert.ToString(bytes[23], 2);
                n1 = convertToWord(n1);

                string totalSizeInBinary = n1 + n2 + n3 + n4;

                size = Convert.ToInt32(totalSizeInBinary, 2);

            }
            return size;
        }*/
    }
}
