using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Xml;
using SmartCadParser;

namespace Smartmatic.SmartCad.Parser.Data
{
    abstract class IntellitrackData : GPSData
    {
        internal string[] Parts { get; set; }

        #region Constructors
     
        public IntellitrackData()
            :base()
        {
            respondBytes = 8;
            Respond = true;
            NeedClean = true;
        }

        public IntellitrackData(byte[] dataBuffer)
            :base(dataBuffer)
        {
            respondBytes = 8;
            Respond = true;
            NeedClean = true;
        }
        #endregion

        #region Methods

        protected override string GetModemId()
        {
            modemId = Parts[0];
            return modemId;
        }

        protected string GetLatitud() 
        {
            return Parts[3].Replace(".",",");
        }

        protected string GetLongitud()
        {
            return Parts[2].Replace(".", ",");
        }

        protected string GetAltitud()
        {
            return Parts[6];
        }

        protected override string GetDate()
        {
            string dStr = Parts[1];
            Date = new DateTime(int.Parse(dStr.Substring(0, 4)), 
                                int.Parse(dStr.Substring(4, 2)), 
                                int.Parse(dStr.Substring(6, 2)), 
                                int.Parse(dStr.Substring(8, 2)), 
                                int.Parse(dStr.Substring(10, 2)), 
                                int.Parse(dStr.Substring(12, 2)), 
                                DateTimeKind.Utc).ToLocalTime();
            return Date.ToString(Utility.DataBaseFormattedDate);
        }

        protected string GetSpeed()
        {
            try
            {
                return string.IsNullOrEmpty(Parts[4]) ? "0" : Parts[4];
            }
            catch { return "0"; }
        }

        protected string GetHeading()
        {
            return Parts[5];
        }

        protected string GetSatellites()
        {
            return Parts[7];
        }

        protected string GetInputs()
        {
            int i = int.Parse(Parts[9]);
            if (i == 1)
                return "1";
            if (i > 1)
                return ((int)Math.Sqrt((double)i+1)).ToString();
            return "0";
        }

        protected string GetOutputs()
        {
            int i = int.Parse(Parts[10]);
            if (i == 1)
                return "1";
            if (i > 1)
                return ((int)Math.Sqrt((double)i + 1)).ToString();
            return "0";
        }

        protected override FrameType GetFrameType()
        {
            if (buffer.Length > 0)
            {
                if (Utility.ToHex(buffer).Substring(0, 4) == "FAF8")
                    return FrameType.Sync;
                else
                    return FrameType.Position;
            }
            else
                return FrameType.Incomplete;
        }

        public override IList<DataFrame> GetCleanFrames()
        {
            IList<DataFrame> datas = new List<DataFrame>();
            string[] frames = this.Frame.Replace("\r", "").Split('\n');
            
            foreach (string frame in frames)
            {
                string[] parts = frame.Split(',');
                if (parts.Length == 11)
                {
                    datas.Add(new X1Data(ASCIIEncoding.ASCII.GetBytes(frame)));
                }
                if (parts.Length == 15)//Is A1Data 
                {
                    datas.Add(new A1Data(ASCIIEncoding.ASCII.GetBytes(frame)));
                }
            }
            return datas;
        }

        #endregion

        public override void Decode()
        {
            Parts = Frame.Split(',');

            Data += NameType + Separator;
            Data += GetModemId() + Separator;
            Data += GetDate() + Separator;
            Data += GetLongitud() + Separator;
            Data += GetLatitud() + Separator;
            Data += GetAltitud() + Separator;
            Data += GetSpeed() + Separator;
            Data += GetHeading() + Separator;
            Data += GetSatellites() + Separator;
            Data += GetInputs() + Separator;
            Data += GetOutputs();
        }
    }
}
