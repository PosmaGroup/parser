﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadParser;

namespace Smartmatic.SmartCad.Parser.Data
{
    public class GPSData : DataFrame
    {
        #region Constructors
     
        public GPSData()
            :base()
        {
        }

        public GPSData(byte[] dataBuffer)
            :base(dataBuffer)
        {
        }

        #endregion

        #region Override
        public override string NameType
        {
            get
            {
                return "GPS";
            }
        } 
        #endregion
    }
}
