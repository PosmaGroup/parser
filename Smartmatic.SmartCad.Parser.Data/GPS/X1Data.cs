﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Smartmatic.SmartCad.Parser.Data
{
    class X1Data : IntellitrackData
    {
        #region Constructors
     
        public X1Data()
            :base()
        {
        }

        public X1Data(byte[] dataBuffer)
            :base(dataBuffer)
        {
        }
        #endregion

        public override byte[] GetCommand(Command command, params object[] args)
        {
            switch (command)
            {
                case Command.GetPosition:
                    return ASCIIEncoding.ASCII.GetBytes("$ST+GETPOSITION=0000");
                case Command.SetOutputs:  // arg[0] = pin, arg[1] = on/off, arg[2]= seconds, arg[3] = times
                    return ASCIIEncoding.ASCII.GetBytes("$ST+OUTS=0000," + args[0] + "," + args[1] + "," + args[2] + "," +args[2]);
                default:
                    break;
            }
            return null;
        }
    }
}
