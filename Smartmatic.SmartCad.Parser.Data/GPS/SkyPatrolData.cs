using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using SmartCadParser;

namespace Smartmatic.SmartCad.Parser.Data
{
    class SkyPatrolData : GPSData
    {
        #region Constructors
     
        public SkyPatrolData()
            :base()
        {
            Respond = false;
            NeedClean = false;
        }

        public SkyPatrolData(byte[] dataBuffer)
            :base(dataBuffer)
        {
            Respond = false;
            NeedClean = false;
        }

        #endregion

        #region Methods

        protected override string GetModemId()
        {
            if (Type == FrameType.Alarm)
            {
                byte[] buffID = new byte[17];
                for (int u = 20; u <= 36; u++)
                {
                    buffID[u - 20] = buffer[u];
                }
                modemId = ASCIIEncoding.ASCII.GetString(buffID).Replace("", "").Replace("\0","").Replace(" ", "");
            }
            else
            {
                byte[] buffID = new byte[17];
                for (int u = 13; u <= 29; u++)
                {
                    buffID[u - 13] = buffer[u];
                }

                modemId = ASCIIEncoding.ASCII.GetString(buffID).Replace("", "").Replace("\0","").Replace(" ", "");
            }

            if (modemId.Trim() == string.Empty)
            {
                throw new Exception();
            }

            return modemId;
        }

        protected string GetLatitud() 
        {
            int decValue = Utility.ToDec(Utility.ToHex(buffer,41, 43));
            return Utility.ToCoordinates(decValue).ToString();
        }

        protected string GetLongitud()
        {
            int decValue = Utility.ToDec(Utility.ToHex(buffer, 44, 47));
            return Utility.ToCoordinates(decValue).ToString();
        }

        protected string GetAltitud()
        {
            return Utility.ToDec(Utility.ToHex(buffer, 55, 57)).ToString();
        }

        protected string GetInputs()
        {
            int i = Utility.ToDec(Utility.ToHex(buffer[36]));
            return i.ToString();
        }

        protected string GetOutputs()
        {
            return GetInputs();
        }

        protected override string GetDate()
        {
            int decValue = Utility.ToDec(Utility.ToHex(buffer, 37, 39));
            string str = decValue.ToString().PadLeft(6, '0');

            int day = Convert.ToInt32(str.Substring(0, 2));
            int month = Convert.ToInt32(str.Substring(2, 2));
            int year = Convert.ToInt32("20" + str.Substring(4, 2));

            decValue = Utility.ToDec(Utility.ToHex(buffer, 52, 54));
            str = decValue.ToString().PadLeft(6, '0');

            int hour = Convert.ToInt32(str.Substring(0, 2));
            int minutes = Convert.ToInt32(str.Substring(2, 2));
            int seconds = Convert.ToInt32(str.Substring(4, 2));

            Date = new DateTime(year, month, day, hour, minutes, seconds, DateTimeKind.Utc);
            return Date.ToLocalTime().ToString(Utility.DataBaseFormattedDate);
        }

        protected string GetSpeed()
        {
            return Utility.ToDouble(Utility.ToHex(buffer, 48, 49)).ToString();
        }

        protected string GetHeading()
        {
            return Utility.ToDouble(Utility.ToHex(buffer, 50, 51)).ToString();
        }

        protected string GetSatellites()
        {
            return Utility.ToDec(Utility.ToHex(buffer, 58, 58)).ToString();
        }

        protected override FrameType GetFrameType()
        {
            if (buffer.Length >= 59)
            {
                type = FrameType.Position;
            }
            else
            {
                type = FrameType.Sync;
            }
            return type;
        }

        public override byte[] GetCommand(Command command, params object[] args)
        {
            switch (command)
            {
                case Command.GetPosition:
                    return ASCIIEncoding.ASCII.GetBytes("AT$GPSRD=10");
                case Command.SetOutputs:  // arg[0] = pin, arg[1] = on/off, arg[2]= seconds , arg[3] = times
                    return ASCIIEncoding.ASCII.GetBytes("$AT+OUT=" + args[0] + "," + args[1] + "," + args[2] + ",," + args[3]);
                default:
                    break;
            }
            return null;
        }

        #endregion

        public override void Decode()
        {
            Data += NameType + Separator;
            Data += GetModemId() + Separator;
            Data += GetDate() + Separator;
            Data += GetLongitud() + Separator;
            Data += GetLatitud() + Separator;
            Data += GetAltitud() + Separator;
            Data += GetSpeed() + Separator;
            Data += GetHeading() + Separator;
            Data += GetSatellites() + Separator;
            Data += GetInputs() + Separator;
            Data += GetOutputs();
        }

    }
}
