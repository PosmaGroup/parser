using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using SmartCadParser;

namespace Smartmatic.SmartCad.Parser.Data
{
    class DctData : GPSData
    {
        #region Constructors
     
        public DctData()
            :base()
        {
            Respond = false;
            NeedClean = false;
        }

        public DctData(byte[] dataBuffer)
            :base(dataBuffer)
        {
            Respond = false;
            NeedClean = false;
        }

        #endregion

        #region Methods

        //listo
        protected override string GetModemId()
        {
            int start = Frame.IndexOf("ID=");
            modemId = Frame.Substring(index + start + 2, 15);
            return modemId;
        }

        //listo
        protected string GetLatitud()
        {
            string data = Frame.Substring(index + 15, 8);
            data = data.Insert(3, ",");
            return data;
        }

        //listo
        protected string GetLongitud()
        {
            string data = Frame.Substring(index + 23, 10);

            data = data.Insert(4, ",");

            return data;
        }

        protected string GetAltitud()
        {
            int start = Frame.IndexOf("AL=") + 3;
            int end = Frame.IndexOf(";", start);
            return Frame.Substring(start + 1, 4);
        }

        protected string GetInputs()
        {
            int start = Frame.IndexOf("IO=") + 5;
            int i = Utility.ToDec(Frame.Substring(start, 1));
            if (i == 1)
                return "3";
            else if (i == 2)
                return "2";
            else if (i == 3)
            {
                return "[2" + ListSeparator + "3]";
            }
            else if (i == 4)
            {
                return "1";
            }
            else if (i == 5)
            {
                return "[1" + ListSeparator + "3]";
            }
            else if (i == 6)
            {
                return "[1" + ListSeparator + "2]";
            }
            else if (i == 7)
            {
                return "[1" + ListSeparator + "2" + ListSeparator + "3]";
            }
            return "0";
        }

        protected string GetOutputs()
        {
            int start = Frame.IndexOf("IO=") + 4;
            int i = Utility.ToDec(Frame.Substring(start, 1));
            if (i == 1)
                return "2";
            else if (i == 2)
                return "1";
            else if (i == 3)
            {
                return "[1" + ListSeparator + "2]";
            }
            return "0";
        }

        //listo
        protected override string GetDate()
        {
            // Number of weeks since 00:00 AM January 6, 1980.
            int weeks = 0;
            int dayOfWeek = 0;
            int seconds = 0;

            int.TryParse(Frame.Substring(index + 5, 4), out weeks);
            int.TryParse(Frame.Substring(index + 9, 1), out dayOfWeek);
            int.TryParse(Frame.Substring(index + 10, 5), out seconds);

            Date = new DateTime(1980, 1, 6, 0, 0, 0);
            Date = Date.AddDays((weeks * 7) + dayOfWeek);
            Date  = Date.AddSeconds(seconds);
            return Date.ToString(Utility.DataBaseFormattedDate);
        }

       //listo
        protected string GetSpeed()
        {
            return Frame.Substring(index + 33, 3);
        }

        //listo
        protected string GetHeading()
        {
            return Frame.Substring(index + 36, 3);
        }
        //Probar
        protected string GetSatellites()
        {
            int start = Frame.IndexOf("SV=") + 3;
            int end = Frame.IndexOf(";", start);
            return Frame.Substring(start, end - start);
        }

        int index = 0;
        protected override FrameType GetFrameType()
        {
            index = Frame.IndexOf(">REV") + Frame.IndexOf(">RPV");
            if (index >= 0)
            {
                type = FrameType.Position;
            }
            else
            {
                type = FrameType.Sync;
            }
            return type;
        }

        public override byte[] GetCommand(Command command, params object[] args)
        {
            switch (command)
            {
                case Command.GetPosition:
                    return ASCIIEncoding.ASCII.GetBytes(">QPV<");
                case Command.SetOutputs:  // arg[0] = pin, arg[1] = on/off, arg[2]= seconds , arg[3] = times
                    return ASCIIEncoding.ASCII.GetBytes(">SSSXP" + args[0] + args[1] + "<");
                default:
                    break;
            }
            return null;
        }

        #endregion

        public override void Decode()
        {
            Data += NameType + Separator;
            Data += GetModemId() + Separator;
            Data += GetDate() + Separator;
            Data += GetLongitud() + Separator;
            Data += GetLatitud() + Separator;
            Data += GetAltitud() + Separator;
            Data += GetSpeed() + Separator;
            Data += GetHeading() + Separator;
            Data += GetSatellites() + Separator;
            Data += GetInputs() + Separator;
            Data += GetOutputs();
        }
    }
}
