using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Xml;
using SmartCadParser;

namespace Smartmatic.SmartCad.Parser.Data
{
    class TramigoData : GPSData
    {
        //example data:
        // +CMGL: 3,
        //\"REC READ\",
        //\"+639475835089\",
        //,
        //\"15/04/16
        //,10:42:03+32\"\r\nTramigo: Status,
        //GPS: 0%,
        //GSM: 100%,
        //battery: 20%,
        //reports: Status (1,
        //-,
        //-,
        //-),
        //14.55310,
        //121.04447,
        //10:41 Apr 16\r\n

        internal string[] Parts { get; set; }

        #region Constructors
     
        public TramigoData()
            :base()
        {
            Respond = false;
            NeedClean = false;
        }

        public TramigoData(string data)
            :base()
        {
            Respond = false;
            NeedClean = false;

            frame = data;
        }
        #endregion

        #region Methods

        protected override string GetModemId()
        {
            modemId = Parts[2].Replace("\"","");
            return modemId;
        }

        protected string GetLatitud()
        {
            Logger.Print("12-" + Parts[12] + "13-" + Parts[13] + "14-" + Parts[14]);
            if( Parts[13].Contains(")"))
                return Parts[14].Replace(".", ",").TrimStart();
            else
                return Parts[13].Replace(".", ",").TrimStart();
        }

        protected string GetLongitud()
        {
            if (Parts[13].Contains(")"))
                return Parts[15].Replace(".", ",").TrimStart();
            else
                return Parts[14].Replace(".", ",").TrimStart();
        }

        protected string GetAltitud()
        {
            return "0";
        }

        protected override string GetDate()
        {
            string date = Parts[4].Replace("\"", "");
            string time = Parts[5].Remove(Parts[5].IndexOf('+'));
            TimeSpan timespan = TimeSpan.Parse(time);
            //\"15/04/16,
            //10:42:03+32\"\r\nTramigo: Status,

            Date = new DateTime(2000 + int.Parse(date.Substring(0, 2)),
                                 int.Parse(date.Substring(3, 2)),
                                 int.Parse(date.Substring(6, 2)),
                                 timespan.Hours,
                                 timespan.Minutes,
                                 timespan.Seconds,
                                 DateTimeKind.Local);
            return Date.ToString(Utility.DataBaseFormattedDate);
        }

        protected string GetSpeed()
        {
            return "0"; 
        }

        protected string GetHeading()
        {
            return "0";
        }

        protected string GetSatellites()
        {
            return "0";
        }

        protected string GetInputs()
        {
            //Not yet implemmented
            return "0";
        }

        protected string GetOutputs()
        {
            //Not yet implemmented
            return "0";
        }

        protected override FrameType GetFrameType()
        {
            if (frame.ToLower().Contains("status"))
            {
                return FrameType.Position;
            }
            else
                return FrameType.None;
        }

        #endregion

        public override void Decode()
        {
            if (GetFrameType() != FrameType.Position)
                return;
            Parts = Frame.Split(',');

            Data += NameType + Separator;
            Data += GetModemId() + Separator;
            Data += GetDate() + Separator;
            Data += GetLongitud() + Separator;
            Data += GetLatitud() + Separator;
            Data += GetAltitud() + Separator;
            Data += GetSpeed() + Separator;
            Data += GetHeading() + Separator;
            Data += GetSatellites() + Separator;
            Data += GetInputs() + Separator;
            Data += GetOutputs();

            Logger.Print("fullframe: " + Data);
        }
    }
}
