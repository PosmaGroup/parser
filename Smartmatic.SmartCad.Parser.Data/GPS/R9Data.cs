﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadParser;

namespace Smartmatic.SmartCad.Parser.Data
{
    class R9Data : GPSData
    {
        private string[] Parts { get; set; }

        #region Constructors
     
        public R9Data()
            :base()
        {
            Respond = false;
            NeedClean = false;
        }

        public R9Data(byte[] dataBuffer)
            :base(dataBuffer)
        {
            Respond = false;
            NeedClean = false;
        }

        #endregion

        #region Methods

        protected override string GetModemId()
        {
            modemId = Parts[0];
            return modemId;
        }

        protected string GetLatitud()
        {
            string strLat = Parts[5].Replace(".", ",");
            string cardinal_indicator = Parts[6];

            return Utility.ToCoordinates(strLat, cardinal_indicator, 2).ToString();
        }

        protected string GetLongitud()
        {
            string strLon = Parts[7].Replace(".", ",");
            string cardinal_indicator = Parts[8];

            return Utility.ToCoordinates(strLon, cardinal_indicator,3).ToString();
        }

        protected string GetAltitud()
        {
            return "0";
        }

        protected override string GetDate()
        {
            string[] dateTimeStr = Parts[12].Split(' ');
            string timeStr = dateTimeStr[0]; 
            string[] tparts = timeStr.Split(':');

            string dateStr = dateTimeStr[1];
            string[] dparts = dateStr.Split('-');
            
            Date = new DateTime(2000+int.Parse(dparts[2]),
                                int.Parse(dparts[1]),
                                int.Parse(dparts[0]),
                                int.Parse(tparts[0]),
                                int.Parse(tparts[1]),
                                int.Parse(tparts[2]),
                                DateTimeKind.Utc);
            return Date.ToLocalTime().ToString(Utility.DataBaseFormattedDate);
        }

        protected string GetSpeed()
        {
            return Parts[9];
        }

        protected string GetHeading()
        {
            return Parts[10];
        }

        protected string GetSatellites()
        {
            return "0";
        }

        protected override FrameType GetFrameType()
        {
            type = FrameType.Position;
            return type;
        }

        protected string GetInputs()
        {
            byte[] statusCode = ASCIIEncoding.ASCII.GetBytes(Parts[2]);

            if ((statusCode[0] - 48 == 8) || (statusCode[0] - 48 -8) > 0) 
                return "2";
            if ((statusCode[1] - 48 == 1) || (statusCode[1] - 48 - 1) > 0)
                return "0";
            if ((statusCode[2] - 48 == 8) || (statusCode[2] - 48 - 8) > 0)
                return "1";
            return "-1";
        }

        protected string GetOutputs()
        {
            byte[] statusCode = ASCIIEncoding.ASCII.GetBytes(Parts[2]);

            if ((statusCode[1] - 48 == 2) || (statusCode[1] - 48 - 2) > 0)
                return "0";
            if ((statusCode[2] - 48 == 2) || (statusCode[2] - 48 - 2) > 0)
                return "2";
            if ((statusCode[2] - 48 == 1) || (statusCode[2] - 48 - 1) > 0)
                return "1";
            return "-1";
        }

        public override byte[] GetCommand(Command command, params object[] args)
        {
            /*
             * 4xx, ( first x = command code =1 Output 1 activate - DOOR
                                             =2 Output 2 activate - SOS
                                             =3 Both outputs activate
                                             =5 Output 1 deactivate
                                             =6 Output 2 deactivate
                                             =7 Both outputs deactivate
                     ( second x = time       =0 leave activated
                                             =1 to 8 time(sec) active in seconds
                                             =9 activate for 30 seconds
             Example: 05*827,429,0 - Activate SOS output for 30 seconds!
             */

            switch (command)
            {
                case Command.GetPosition:
                    return ASCIIEncoding.ASCII.GetBytes("05*827,11,0");
                case Command.SetOutputs:  // arg[0] = pin, arg[1] = on/off, arg[2]= seconds 
                    return ASCIIEncoding.ASCII.GetBytes("05*827,4" + ((int)args[0] + (4*(1-(int)args[1]))) + "" + ((int)args[2] > 9 ? 9: (int)args[2]) + ",0");
                default:
                    break;
            }
            return null;
        }

        #endregion

        public override void Decode()
        {
            Parts = Frame.Split(',');
            Data += NameType + Separator;
            Data += GetModemId() + Separator;
            Data += GetDate() + Separator;
            Data += GetLongitud() + Separator;
            Data += GetLatitud() + Separator;
            Data += GetAltitud() + Separator;
            Data += GetSpeed() + Separator;
            Data += GetHeading() + Separator;
            Data += GetSatellites() + Separator;
            Data += GetInputs() + Separator;
            Data += GetOutputs();
        }
    }
}
