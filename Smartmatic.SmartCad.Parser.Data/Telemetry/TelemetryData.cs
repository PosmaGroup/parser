﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartCadParser;

namespace Smartmatic.SmartCad.Parser.Data
{
    public class TelemetryData : DataFrame
    {
        #region Constructors
     
        public TelemetryData()
            :base()
        {
        }

        public TelemetryData(byte[] dataBuffer)
            :base(dataBuffer)
        {
        }

        #endregion

        #region Override
        public override string NameType
        {
            get
            {
                return "Datalogger";
            }
        }
        #endregion
    }
}
