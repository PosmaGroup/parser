﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using SmartCadParser;

namespace Smartmatic.SmartCad.Parser.Data
{
    public class DataloggerData : TelemetryData
    {
        private string[] Parts { get; set; }
        private object obj = new object();

        #region Constructors

        public DataloggerData()
          : base() 
        {
            NeedClean = true;
        }

        public DataloggerData(byte[] dataBuffer)
            : base(dataBuffer)
        {
            NeedClean = true;
        }

        public DataloggerData(string modemId, DateTime date, string data)
            : base()
        {
            ModemId = modemId;
            Date = date;
            Data = data;
            NeedClean = true;
        }

        #endregion

        #region Override Base
        protected override FrameType GetFrameType()
        {
            if(Buffer[0] != 0)
                return FrameType.Alarm;
            else if (Buffer[0] == 10)
                return FrameType.Sync;
            else
                return FrameType.Incomplete;
        }

        protected override string GetModemId()
        {
            if (Parts == null)
                return null;

            modemId = Parts[0];
            int o = 0;
            if (int.TryParse(modemId, out o) == false)
                type = FrameType.Incomplete;

            return modemId;
        }

        protected override string GetDate()
        {
            try
            {
                Parts[1] = Parts[1].TrimStart();
                Date = DateTime.ParseExact(Parts[1], Utility.DataBaseFormattedDate, System.Globalization.CultureInfo.InvariantCulture);
                return Date.ToString(Utility.DataBaseFormattedDate);
            }
            catch
            {
                Date = DateTime.Now;
                return Date.ToString(Utility.DataBaseFormattedDate);
            }
        }

        private string GetValue()
        {
            string values = "";
            for (int i = 2; i < Parts.Length; i++)
            {
                string value = Parts[i].TrimStart();
                int equalsPos = value.IndexOf("=");

                if (equalsPos >= -1)
                {
                    if (value.ToLower().Contains("nan") || equalsPos == value.Length - 1)
                        value = value.Substring(0, equalsPos + 1) + "0";

                    values += value;
                    if (i < Parts.Length -1)
                        values += Separator;
                }
            }
            return values; 
        }

        public override void Decode()
        {
            byte[] code = new byte[4];
            byte[] size = new byte[4];

            frame = Frame.Replace("\0", "");
            frame = Frame.Replace("#", "");
            Parts = Frame.Split(',');
            if (Parts.Length > 2)
            {
                Data += NameType + Separator;
                Data += GetModemId() + Separator;
                Data += GetDate() + Separator;
                Data += GetValue();

                type = FrameType.Alarm;
            }
            else
            {
                type = FrameType.Incomplete;
#if DEBUG
                Console.WriteLine("incomplete:" + Frame.Replace("\0",""));
#endif
            }
        }

        /// <summary>
        /// Separates the multiples frames that came together.
        /// </summary>
        /// <returns>List of each frame separated and decoded.</returns>
        public override IList<DataFrame> GetCleanFrames()
        {
            IList<DataFrame> frames = new List<DataFrame>();

            string[] values = this.Frame.Split(ArrivingSeparator[0]);

            for (int i = 0; i < values.Length; i++)
            {
                if (values[i].Length > 0)
                {
                    DataloggerData data = new DataloggerData();
                    data.frame = values[i];
                    data.Decode();
                    frames.Add(data);
                }
            }
            return frames;
        }
        #endregion
    }
}
