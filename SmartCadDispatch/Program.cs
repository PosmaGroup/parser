﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;


using SmartCadCore.Model;
using System.ServiceModel;
using System.Collections;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using System.Globalization;
using System.Net;
using SmartCadCore.Core;
using Smartmatic.SmartCad.Service;
using SmartCadGuiCommon;
using SmartCadDispatch.Gui;
using SmartCadGuiCommon.Util;
using System.IO;

namespace SmartCadDispatch
{
    static class Program
    {
        private static TelephonyConsoleManager telephonyConsoleManager;
        private static ConfigurationClientData configuration;
        private static string ApplicationName = "Dispatch";
        [STAThread]
        static void Main()
        {
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.ThreadException += new ThreadExceptionEventHandler(ApplicationGuiUtil.Application_ThreadException);                
                ApplicationUtil.ActivateProcess(Process.GetCurrentProcess());
                
                SmartCadConfiguration.Load();
                ResourceLoader.UpdatedLanguage(new CultureInfo(SmartCadConfiguration.SmartCadSection.CultureElement.CultureLanguageElement.Name
                    + "-" + SmartCadConfiguration.SmartCadSection.CultureElement.CultureCountryElement.Name));

                object result = ServerServiceClient.GetServerService();
                if (result == null)
                {
                    MessageForm.Show(ResourceLoader.GetString2("UnableConnectToServer"), MessageFormType.Error);
                    return;
                }

                configuration = ServerServiceClient.GetInstance().GetConfiguration();

                ConfigurationClientData cfg = ServerServiceClient.GetInstance().GetConfiguration();
                SmartCadConfiguration.SaveConfigurationInClient(cfg);
                if (!ApplicationUtil.VerifyLanguage(cfg))
                {
                    byte[] dll = ServerServiceClient.GetInstance().CheckLanguages(cfg.Language);
                    DirectoryInfo directory = Directory.CreateDirectory(SmartCadConfiguration.LanguageDirectoryClient + "\\" + cfg.Language);
                    using (FileStream fileDll = new FileStream(directory.FullName + "\\Smartmatic.SmartCad.Resources.resources.dll", FileMode.Create, FileAccess.Write))
                    {
                        fileDll.Write(dll, 0, dll.Length);
                    }
                }

                ResourceLoader.UpdatedLanguage(new CultureInfo(configuration.Language));

                telephonyConsoleManager = new TelephonyConsoleManager();
                telephonyConsoleManager.Run();
                telephonyConsoleManager.WaitForService(3);

                ServerServiceClient.GetInstance().SetApplication(UserApplicationClientData.UserApplications.Dispatch.ToString());
                SmartCadLoadInitialData.LoadInitialClientData();

                System.Threading.Timer checkServer = ApplicationGuiUtil.StartCheckServer();

                ApplicationGuiUtil.CheckErrorDetailButton();
                ServerServiceClient.GetInstance().ActivateApplication(ApplicationName,ApplicationUtil.GetMACAddress());
                ApplicationPreferenceClientData recordCalls =
                    (ApplicationPreferenceClientData)ServerServiceClient.GetInstance().SearchClientObject(SmartCadHqls.GetCustomHql(SmartCadHqls.GetApplicationPreferenceByName, "RecordCalls"));
                
                bool success = false;

                do
                {
                    LoginForm login = new LoginForm(UserApplicationClientData.Dispatch);//Borrar este parametro. Con el setApplication no debería hacer falta pasarlo de nuevo

                    DispatchFormDevX dispatchForm = new DispatchFormDevX();

                    if (login.ShowDialog() == DialogResult.OK)
                    {
#if !DEBUG
                        SplashForm.ShowSplash();
#endif
                        ServerServiceClient.GetInstance().NetworkCredential = login.NetworkCredential;
                        
                        TelephonyServiceClient.GetServerService();
                        TelephonyServiceClient.GetInstance().SetConfiguration(configuration);

                        dispatchForm.SetPassword(login.Password);
                        ServerServiceClient serverServiceClient = login.ServerService;

                        success = true;

                        dispatchForm.ServerServiceClient = serverServiceClient;
						dispatchForm.LoadInitialData();

                        RegisterPhone(recordCalls, login.NetworkCredential);
#if !DEBUG
                        SplashForm.CloseSplash();
#endif
                        Application.Run(dispatchForm);

                        //UnregisterPhone();
                    }
                    else
                    {
                        ServerServiceClient.GetInstance().CloseSession();
                        success = true;
                    }
                }
                while (!success);
            }
            catch (EndpointNotFoundException ex)
            {
                MessageForm.Show(ResourceLoader.GetString2("UnableConnectToServer"), ex);
            }
            catch (Exception ex)
            {
                //MessageForm.Show(ResourceLoader.GetString2("GenericErrorApplication"), ex);
                MessageForm.Show(ResourceLoader.GetString2(ex.StackTrace), ex);
            }
            finally
            {
                if (telephonyConsoleManager != null)
                    telephonyConsoleManager.Stop();
            } 
        }

        private static void UnregisterPhone()
        {
            try
            {
                TelephonyServiceClient.GetInstance().Logout();
                Thread.Sleep(1000);
            }
            catch { }
        }

        private static void RegisterPhone(ApplicationPreferenceClientData recordCalls, NetworkCredential networkCredential)
        {
            try
            {
                OperatorClientData oper = ServerServiceClient.GetInstance().OperatorClient;
                TelephonyServiceClient.GetInstance().Login(
                    networkCredential.UserName,
                    networkCredential.Password,
                    "",
                    "",
                    recordCalls == null ? false : bool.Parse(recordCalls.Value),
                    configuration.Extension, false);
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }
    }
}
