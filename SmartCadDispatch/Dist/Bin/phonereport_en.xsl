<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:dt="http://xsltsl.org/date-time">
  <xsl:import href="xsltsl/stdlib.xsl"/>
  <xsl:output method="html"/>
  <!-- Global Parameters and Variables -->
  <!-- date format-->
  <xsl:param name="format" select="'%m/%d/%Y %H:%M:%S'"/>
  <!-- Section footer-->
  <xsl:variable name="section-footer">
    <tr>
      <td>&#160;</td>
      <td>
        <br/>
        <a class="enabled-link" onclick="goToObject('top');">Back to top</a>
        <br/>
        <br/>
      </td>
      <td>&#160;</td>
      <td>&#160;</td>
    </tr>
  </xsl:variable>

  <!-- BEGIN Base document-->
  <xsl:template match="/">
    <html>
      <head>
        <!-- 
          <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        -->
        <LINK REL="StyleSheet" HREF="style.css" TYPE="text/css"/>
        <script type="text/javascript" src="script.js" language="javascript"/>
        <title>SmartCAD</title>
      </head>
      <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <xsl:apply-templates/>
        </table>
      </body>
    </html>
  </xsl:template>
  <!-- END Base document-->

  <!-- BEGIN Phone report-->
  <xsl:template match="phone-report">
    <tr>
      <div id="top"/>
    </tr>
    <tr bgcolor="C6D3D9">
      <td width="5" />
      <td width="875">
        <span class="header1">
          Call # <xsl:value-of select="@call-order"/>
      </span>
        &#160;<xsl:call-template name="dt:format-date-time">
          <xsl:with-param name="xsd-date-time" select="@call-start"/>
          <xsl:with-param name="format" select="$format"/>
        </xsl:call-template>
      </td>
      <xsl:choose>
        <xsl:when test="@complete='0'">
          <td width="143">
            <span class="status-complete">Call finished</span>
          </td>
        </xsl:when>
        <xsl:otherwise>
          <td width="143">
            <span class="status-incomplete">Call unfinished</span>
          </td>
        </xsl:otherwise>
      </xsl:choose>
      <td width="5">&#160;</td>
    </tr>
    <tr bgcolor="E4EAED">
      <td width="5">&#160;</td>
      <td colSpan="2">
        <br/>
        <span class="prop-name">Attended by:</span>&#160;<xsl:value-of select="@attended-by"/><br/>
        <xsl:for-each select="caller">
          <span class="prop-name">Caller:&#160;</span>
          <xsl:choose>
            <xsl:when test="string-length(@name)>0">
              <xsl:value-of select="@name"/>
            </xsl:when>
            <xsl:otherwise>Anonymous</xsl:otherwise>
          </xsl:choose>
          <br/>
          <span class="prop-name">Phone</span>:&#160;<xsl:value-of select="@phone"/>
          <xsl:if test="string-length(@additional-phone)>0">
            /&#160;<xsl:value-of select="@additional-phone"/>
          </xsl:if>
          <br/>
        </xsl:for-each>
        <xsl:for-each select="address">
          <span class="prop-name">Caller address:</span>
          <br/>
          <xsl:if test ="string-length(@zone)>0">
            Zone:&#160;<xsl:value-of select="@zone"/>.
            <br/>
          </xsl:if>
          <xsl:if test ="string-length(@street)>0">
            St/Ave/Cnr:&#160;<xsl:value-of select="@street"/>.
            <br/>
          </xsl:if>
          <xsl:if test ="string-length(@reference)>0">
            House/Building:&#160;<xsl:value-of select="@reference"/>.
            <br/>
          </xsl:if>
          <xsl:if test ="string-length(@more)>0">
            Floor/Apt:&#160;<xsl:value-of select="@more"/>.
            <br/>
          </xsl:if>
        </xsl:for-each>
        <span class="prop-name">Incident types:</span>
        <xsl:for-each select="incident-types/incident-type">
          &#160;
          <xsl:variable name="incident-type-string">
            <xsl:value-of select="@friendly-name"/>&#160;(<xsl:value-of select="@custom-code"/>)
          </xsl:variable>
          <xsl:copy-of select ="normalize-space($incident-type-string)"/>
          <xsl:choose>
            <xsl:when test="position()=last()">.</xsl:when>
            <xsl:otherwise>,</xsl:otherwise>
          </xsl:choose>
        </xsl:for-each>
        <br/>
        <br/>
      </td>
      <td width="5">&#160;</td>
    </tr>
    <tr>
      <td width="5">&#160;</td>
      <td colSpan="2">
        <br/>
        <span class="header2">Questions</span>
        <br/>
        <br/>
        <xsl:for-each select="question-set/question">
          <span class="prop-name">
            <xsl:value-of select="q"/>
          </span>
          <br/>
          <xsl:for-each select="a">
            <xsl:value-of select="@text"/>
            <xsl:if test="position()!=last()">,&#160;</xsl:if>
          </xsl:for-each>
          <br/>
        </xsl:for-each>
        <br/>
      </td>
      <td width="5">&#160;</td>
    </tr>

    <tr bgcolor="E4EAED">
      <td width="5">&#160;</td>
      <td colSpan="2">
        <strong>
          <br/>
          <span class="header2">Requests for dispatch:</span>
          <br/>
          <br/>
        </strong>
        <xsl:for-each select="notifications/notification">
          &#160;<xsl:value-of
    select="@department-type-name"/>&#160;(<xsl:value-of
                select="@priority-custom-code"/>)<br/>
        </xsl:for-each>
        <br/>
      </td>
      <td width="5">&#160;</td>
    </tr>
    <tr>
      <td>
        <br/>
      </td>
    </tr>
    <xsl:copy-of select="$section-footer"/>
  </xsl:template>
  <!-- END Phone reports -->

</xsl:stylesheet>
