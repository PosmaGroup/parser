<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:dt="http://xsltsl.org/date-time">
  <xsl:import href="xsltsl/stdlib.xsl"/>
  <xsl:output method="html"/>
  <!-- Global Parameters and Variables -->
  <!-- date format-->
  <xsl:param name="format" select="'%m/%d/%Y %H:%M:%S'"/>
  <!-- Section footer-->
  <xsl:variable name="section-footer">
    <tr>
      <td>&#160;</td>
      <td>
        <br/>
        <a class="enabled-link" onclick="goToObject('top');">Back to top</a>
        <br/>
        <br/>
      </td>
      <td>&#160;</td>
      <td>&#160;</td>
    </tr>
  </xsl:variable>

  <!-- BEGIN Base document-->
  <xsl:template match="/">
    <html>
      <head>
        <!-- 
          <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        -->
        <LINK REL="StyleSheet" HREF="style.css" TYPE="text/css"/>
        <script type="text/javascript" src="script.js" language="javascript"/>
        <title>SmartCAD</title>
      </head>
      <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <xsl:apply-templates/>
        </table>
      </body>
    </html>
  </xsl:template>
  <!-- END Base document-->

  <!-- BEGIN Incident-->
  <xsl:template match="incident">
    <tr>
      <div id="top"></div>
    </tr>
    <tr>
      <td width="5"/>
      <td width="875">
        <span class="header1">
          Incident # <xsl:value-of select="@custom-code"/>
        </span>
        <a name="up" id="up"/>
        <br/>
        <xsl:choose>
          <xsl:when test="//phone-report">
            <a class="enabled-link" onclick="goToObject('phone-reports');">Calls</a>
            <br/>
          </xsl:when>
          <xsl:otherwise>
            <a class="disabled-link">No calls recorded</a>
            <br/>
          </xsl:otherwise>
        </xsl:choose>
        <xsl:choose>
          <xsl:when test="//incident-notification">
            <a class="enabled-link" onclick="goToObject('dispatch-orders');">Requests for dispatch</a>
            <br/>
          </xsl:when>
          <xsl:otherwise>
            <a class="disabled-link">No requests for dispatch recorded</a>
            <br/>
          </xsl:otherwise>
        </xsl:choose>
        <xsl:choose>
          <xsl:when test="//support-requests">
            <a class="enabled-link" onclick="goToObject('support');">Support requests</a>
            <br/>
          </xsl:when>
          <xsl:otherwise>
            <a class="disabled-link">No support requests recorded</a>
            <br/>
          </xsl:otherwise>
        </xsl:choose>
        <xsl:choose>
          <xsl:when test="//incident-notes">
            <a class="enabled-link" onclick="goToObject('inc-notes');">Incident notes</a>
            <br/>
          </xsl:when>
          <xsl:otherwise>
            <a class="disabled-link">No incident notes recorded</a>
            <br/>
          </xsl:otherwise>
        </xsl:choose>
        <xsl:choose>
          <xsl:when test="//summary">
            <a class="enabled-link" onclick="goToObject('summary');">Summary of incident</a>
            <br/>
          </xsl:when>
          <xsl:otherwise>
            <a class="disabled-link">No summary of incident recorded</a>
            <br/>
          </xsl:otherwise>
        </xsl:choose>
        <br/>
        <br/>
      </td>
      <td>&#160;</td>
      <td width="5">&#160;</td>
    </tr>
    <xsl:apply-templates/>
  </xsl:template>
  <!-- END Incident-->

  <!-- BEGIN Phone reports-->
  <xsl:template match="phone-reports">
    <tr>
      <div id="phone-reports"/>
    </tr>
    <xsl:for-each select="phone-report">
      <tr bgcolor="C6D3D9">
        <td width="5" />
        <td>
          <span class="header1">
            Call # <xsl:number value="position()"/>
          </span>
          &#160;<xsl:call-template name="dt:format-date-time">
            <xsl:with-param name="xsd-date-time" select="@call-start"/>
            <xsl:with-param name="format" select="$format"/>
          </xsl:call-template>
        </td>
        <xsl:choose>
          <xsl:when test="@complete='0'">
            <td width="143">
              <span class="status-complete">Call finished</span>
            </td>
          </xsl:when>
          <xsl:otherwise>
            <td width="143">
              <span class="status-incomplete">Call unfinished</span>
            </td>
          </xsl:otherwise>
        </xsl:choose>
        <td width="5">&#160;</td>
      </tr>
      <tr bgcolor="E4EAED">
        <td width="5">&#160;</td>
        <td>
          <br/>
          <span class="prop-name">Attended by:</span>&#160;<xsl:value-of select="@attended-by"/><br/>
          <xsl:for-each select="caller">
            <span class="prop-name">Caller:&#160;</span>
            <xsl:choose>
              <xsl:when test="string-length(@name)>0">
                <xsl:value-of select="@name"/>
              </xsl:when>
              <xsl:otherwise>Anonymous</xsl:otherwise>
            </xsl:choose>
            <br/>
            <span class="prop-name">Phone</span>:&#160;<xsl:value-of select="@phone"/>
            <xsl:if test="string-length(@additional-phone)>0">
              /&#160;<xsl:value-of select="@additional-phone"/>
            </xsl:if>
            <br/>
          </xsl:for-each>
          <xsl:for-each select="address">
            <span class="prop-name">Caller address:</span>
            <br/>
            <xsl:if test ="string-length(@zone)>0">
              Zone:&#160;<xsl:value-of select="@zone"/>.
              <br/>
            </xsl:if>
            <xsl:if test ="string-length(@street)>0">
              Street:&#160;<xsl:value-of select="@street"/>.
              <br/>
            </xsl:if>
            <xsl:if test ="string-length(@reference)>0">
              House/Bldg:&#160;<xsl:value-of select="@reference"/>.
              <br/>
            </xsl:if>
            <xsl:if test ="string-length(@more)>0">
              More:&#160;<xsl:value-of select="@more"/>.
              <br/>
            </xsl:if>
          </xsl:for-each>          <span class="prop-name">Incident types:</span>
          <xsl:for-each select="incident-types/incident-type">
            &#160;
            <xsl:variable name="incident-type-string">
              <xsl:value-of select="@friendly-name"/>&#160;(<xsl:value-of select="@custom-code"/>)
            </xsl:variable>
            <xsl:copy-of select ="normalize-space($incident-type-string)"/>
            <xsl:choose>
              <xsl:when test="position()=last()">.</xsl:when>
              <xsl:otherwise>,</xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
          <br/>
          <br/>
        </td>
        <td width="143">&#160;</td>
        <td width="5">&#160;</td>
      </tr>
      <tr>
        <td width="5">&#160;</td>
        <td>
          <br/>
          <span class="header2">Questions</span>
          <br/>
          <br/>
          <xsl:for-each select="question-set/question">
            <span class="prop-name">
              <xsl:value-of select="q"/>
            </span>
            <br/>
            <xsl:for-each select="a">
              <xsl:value-of select="@text"/>
              <xsl:if test="position()!=last()">,&#160;</xsl:if>
            </xsl:for-each>
            <br/>
          </xsl:for-each>
          <br/>
        </td>
        <td width="143">&#160;</td>
        <td width="5">&#160;</td>
      </tr>
      <tr bgcolor="E4EAED">
        <td width="5">&#160;</td>
        <td>
          <strong>
            <br/>
            <span class="header2">Requests for dispatch:</span>
            <br/>
            <br/>
          </strong>
          <xsl:for-each select="notifications/notification">
            &#160;<xsl:value-of
    select="@department-type-name"/>&#160;(<xsl:value-of
                select="@priority-custom-code"/>)<br/>
          </xsl:for-each>
          <br/>
        </td>
        <td width="143">&#160;</td>
        <td width="5">&#160;</td>
      </tr>
      <tr>
        <td>
          <br/>
        </td>
      </tr>
    </xsl:for-each>
    <xsl:copy-of select="$section-footer"/>
  </xsl:template>
  <!-- END Phone reports -->

  <!-- BEGIN Incident notifications-->
  <xsl:template match="incident-notifications">
    <tr>
      <div id="dispatch-orders"/>
    </tr>
    <xsl:for-each select="incident-notification">
      <tr bgcolor="C6D3D9">
        <td>&#160;</td>
        <td>
          <span class="header1">
            Request for dispatch # <xsl:number value="position()"/>
          </span>
        </td>
        <td>&#160;</td>
        <td>&#160;</td>
      </tr>
      <tr bgcolor="E4EAED">
        <td>&#160;</td>
        <td>
          <br/>
          <span class="prop-name">
            <xsl:value-of select="@department-type-name"/>
          </span>
          &#160;(<xsl:value-of select="@priority-custom-code"/>)&#160; <xsl:call-template name="dt:format-date-time">
            <xsl:with-param name="xsd-date-time" select="@creation-date"/>
            <xsl:with-param name="format" select="$format"/>
          </xsl:call-template>
          <br/>
          <xsl:for-each select="real-status">
           	<span class="prop-name">Status (actual):</span>&#160;<xsl:value-of select="@friendly-name"/>&#160;
            &#160; <xsl:call-template name="dt:format-date-time">
                <xsl:with-param name="xsd-date-time" select="@start"/>
                <xsl:with-param name="format" select="$format"/>               
              </xsl:call-template>&#160;
          <xsl:if test="@custom_code='CANCELLED'">
              <br/>
              <span class="prop-name">Reason for cancellation:</span>&#160;
              <xsl:value-of select="@status_detail"/>
              <br/>
            </xsl:if>
         </xsl:for-each>
          <xsl:choose>
            <xsl:when test="status-set/status[2]">
              <a onclick="switchMenu('exp{position()}');" title="Mostrar historia">
                <span class="enabled-link">(Historical)</span>
              </a><br/>
              <div style="display:none" id="exp{position()}">
                <br/>

                <table width="60%" border="0" cellspacing="0" cellpadding="0">
                  <tr bgcolor="C6D3D9">
                    <td width="20"/>
                    <td class="prop-name">Status</td>
					<td class="prop-name">Operator</td>
                    <td class="prop-name">Date</td>
                  </tr>
                  <xsl:for-each select="status-set/status">
                    <xsl:if test="position()!=last()">
                      <tr>
                        <xsl:attribute name="class">
                          <xsl:if test="position() mod 2">even-sub-row</xsl:if>
                        </xsl:attribute>
                        <td/>
                        <td>
                          &#160;<xsl:value-of select="@status"/>&#160;
                        </td>
						<td>
                          &#160;<xsl:value-of select="@complete-name"/>&#160;
                        </td>
                        <td>
                          &#160; <xsl:call-template name="dt:format-date-time">
                            <xsl:with-param name="xsd-date-time" select="@start-date"/>
                            <xsl:with-param name="format" select="$format"/>
                          </xsl:call-template>
                        </td>
                      </tr>
                    </xsl:if>
                  </xsl:for-each>
                </table>
                <br/>
              </div>
            </xsl:when>
            <xsl:otherwise>
              <br/>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:for-each select="units">
            <span class="prop-name">Number of mobile units assigned:</span>&#160;<xsl:value-of select="@total-units"/>
            <br/>
          </xsl:for-each>
          <br/>
        </td>
        <td>&#160;</td>
        <td>&#160;</td>
      </tr>
      <xsl:for-each select="units/dispatch-order">
        <tr>
          <td>&#160;</td>
          <td>
            <br/>
            <span class="header2">
              Mobile unit # <xsl:number value="position()"/>
            </span>
            <br/>
            <br/>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="30">&#160;</td>
                <td>
                  <span class="prop-name">
                    <xsl:value-of select="@unit"/><br/> Dispatch:
                  </span>&#160; <xsl:call-template
  name="dt:format-date-time">
                    <xsl:with-param name="xsd-date-time" select="@start-date"/>
                    <xsl:with-param name="format" select="$format"/>
                  </xsl:call-template>
                  <span class="prop-name">
                    <br/> Arrival:
                  </span>&#160; <xsl:call-template
  name="dt:format-date-time">
                    <xsl:with-param name="xsd-date-time" select="@arrival-date"/>
                    <xsl:with-param name="format" select="$format"/>
                  </xsl:call-template>
                  <span class="prop-name">
                    <br/> Ending:
                  </span>
                  <xsl:if test="@end-date">
                    &#160; <xsl:call-template name="dt:format-date-time">
                      <xsl:with-param name="xsd-date-time" select="@end-date"/>
                      <xsl:with-param name="format" select="$format"/>
                    </xsl:call-template>
                  </xsl:if>
                  <span class="prop-name">
                    <br/> Officers:
                  </span>
                  <xsl:for-each select="officers/officer">
                    &#160;<xsl:value-of select="@badge"
                  />&#160;<xsl:value-of select="@complete-name"/>.
                  </xsl:for-each>
                  <xsl:for-each select="notes/note">
                    <br/>
                    <br/>
                    <span class="prop-name">
                      Note&#160;<xsl:number value="position()"/>
                    </span>
                    &#160; <xsl:call-template name="dt:format-date-time">
                      <xsl:with-param name="xsd-date-time" select="@creation-date"/>
                      <xsl:with-param name="format" select="$format"/>
                    </xsl:call-template>
                    <br/>
                    <span class="prop-name">Operator:</span>&#160;<xsl:value-of select="@operator"/><br/>
                    <span class="prop-name">Detail:</span>&#160;<xsl:value-of select="@detail"/>
                  </xsl:for-each>
                  <br/><br/>
                </td>
              </tr>
              <xsl:for-each select="ending-report">                
                <xsl:if test="question-set">
                  <tr>
                    <td>&#160;</td>
                    <td>
                      <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="E4EAED">
                        <tr>
                          <td width="30">&#160;</td>
                          <td align="center">
                            <br/>
                            <span class="prop-name">End report</span>
                            <br/>
                            <br/>
                          </td>
                        </tr>
                        <tr>
                          <td width="30">&#160;</td>
                          <td>
                            <span class="prop-name">Officer lead:</span>&#160;<xsl:value-of select="@officerFullName"/><br/><br/>
                          </td>
                        </tr>
                        <tr>
                          <td width="30">&#160;</td>
                          <td>
                            <span class="prop-name">Incident information</span>
                            <br/>
                            <br/>
                          </td>
                        </tr>
                        <tr>
                          <td width="30">&#160;</td>
                          <td>
                            <xsl:for-each select="question-set/question">
                              <span class="prop-name">
                                &#160;&#160;&#160;<xsl:value-of select="@text"/>:&#160;
                              </span>
                              <br/>
                              <xsl:for-each select="answer">
                                &#160;&#160;&#160;<xsl:value-of select="@text"/>
                                <xsl:if test="position()!=last()">,&#160;</xsl:if>
                              </xsl:for-each>
                              <br/>
                            </xsl:for-each>
                            <br/>
                            </td>
                        </tr>
                      </table>
                    </td>
                    <td>&#160;</td>
                    <td>&#160;</td>
                  </tr>
                </xsl:if>             
              </xsl:for-each>              
            </table>
          </td>
        </tr>
      </xsl:for-each>
        <xsl:for-each select="notif-ending-report">
      <tr>        
        <td>&#160;</td>
        <td>
          <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="E4EAED">
            <tr>
              <td width="30">&#160;</td>
              <td align="center">
                <br/>
                  <span class="prop-name">Ending report</span>
                <br/>
                <br/>
              </td>
            </tr>
            <tr>
              <td width="30">&#160;</td>
              <td>
                <span class="prop-name">Officer lead:</span>&#160;<xsl:value-of select="@officerFullName"/><br/><br/>
              </td>
            </tr>
            <tr>
              <td width="30">&#160;</td>
              <td>
                <span class="prop-name">Incident information</span>
                <br/>
                <br/>
              </td>
            </tr>
            <tr>
              <td width="30">&#160;</td>
              <td>
                <xsl:for-each select="question-set/question">
                  <span class="prop-name">
                    &#160;&#160;&#160;<xsl:value-of select="@text"/>:&#160;
                  </span>
                  <br/>
                  <xsl:for-each select="answer">
                    &#160;&#160;&#160;<xsl:value-of select="@text"/>
                    <xsl:if test="position()!=last()">,&#160;</xsl:if>
                  </xsl:for-each>
                  <br/>
                </xsl:for-each>
                <br/>
              </td>              
            </tr>
          </table>
        </td>       
      </tr>
      <tr>
        <td>&#160;</td>
        <td>&#160;</td>
      </tr>
      </xsl:for-each>
      
      <tr>
        <td>&#160;</td>
        <td>
          <span class="prop-name">
            Realized by:&#160;<xsl:value-of select="@firstly-assigned"/>
          </span>
          <br/>
          <span class="prop-name">
            Closed by:&#160;<xsl:value-of select="@notification-closed-by"/>
          </span>
          <br/>
        </td>
        <td>&#160;</td>
        <td>&#160;</td>
      </tr>
      <xsl:copy-of select="$section-footer"/>
    </xsl:for-each>
  </xsl:template>
  <!--END Incident notifications-->

  <!-- BEGIN Incident notes-->
  <xsl:template match="incident-notes">
    <tr>
      <div id="inc-notes"/>
    </tr>
    <xsl:for-each select="incident-note">
      <tr bgcolor="C6D3D9">
        <td>&#160;</td>
        <td>
          <span class="header1">
            Incident note # <xsl:number value="position()"/>
          </span>
        </td>
        <td>&#160;</td>
        <td>&#160;</td>
      </tr>
      <tr bgcolor="E4EAED">
        <td>&#160;</td>
        <td>
          <span class="prop-name">
            <br/>Note&#160;
          </span>
          <xsl:call-template name="dt:format-date-time">
            <xsl:with-param name="xsd-date-time" select="@creation-date"/>
            <xsl:with-param name="format" select="$format"/>
          </xsl:call-template><br/>
          <span class="prop-name">Operator:</span>&#160;<xsl:value-of select="@user-login"/><br/>
          <br/>
        </td>
        <td>&#160;</td>
        <td>&#160;</td>
      </tr>
      <tr>
        <td>&#160;</td>
        <td>
          <br/>
          <span class="prop-name">Detail:</span>&#160;<xsl:value-of select="@detail"/>
          <br/>
          <br/>
        </td>
        <td>&#160;</td>
        <td>&#160;</td>
      </tr>
    </xsl:for-each>
    <xsl:copy-of select="$section-footer"/>
  </xsl:template>
  <!-- END Incident notes-->

  <!-- BEGIN Support requests-->
  <xsl:template match="support-requests">
    <tr>
      <div id="support"/>
    </tr>
    <xsl:for-each select="support-request">
      <tr bgcolor="C6D3D9">
        <td>&#160;</td>
        <td>
          <span class="header1">
            Support request # <xsl:number value="position()"/>
          </span>
        </td>
        <td>&#160;</td>
        <td>&#160;</td>
      </tr>
      <tr bgcolor="E4EAED">
        <td>&#160;</td>
        <td>
          <br/>
          <span class="prop-name">Support request&#160;</span>
          <xsl:call-template name="dt:format-date-time">
            <xsl:with-param name="xsd-date-time" select="@creation-date"/>
            <xsl:with-param name="format" select="$format"/>
          </xsl:call-template>
          <br/>
          <span class="prop-name">Operator:</span>&#160;<xsl:value-of select="@attended-by"/>
          <br/>
          <xsl:for-each select="unit-petitioneer">
            <span class="prop-name">Required by:&#160;</span>
            <xsl:value-of select="@department"/>&#160;-&#160;
            <xsl:value-of select="@custom-code"/>&#160;
            <xsl:value-of select="@unit-type"/>
          </xsl:for-each>
          <br/>
          <span class="prop-name">Department(s) requested:&#160;</span>
          <br/>
          <xsl:for-each select="incident-notifications/incident-notification">
            &#160;<xsl:value-of select="@department-type-name"/>
            &#160;(<xsl:value-of select="@priority"/>)<br/>
          </xsl:for-each>
        </td>
        <td>&#160;</td>
        <td>&#160;</td>
      </tr>
      <tr>
        <td>&#160;</td>
        <td>
          <br/>
          <span class="prop-name">Comments:</span>:&#160;<xsl:value-of select="@details"/>
          <br/>
          <br/>
        </td>
        <td>&#160;</td>
        <td>&#160;</td>
      </tr>
    </xsl:for-each>
    <xsl:copy-of select="$section-footer"/>
  </xsl:template>
  <!-- END Support requests-->

  <!-- BEGIN Summary-->
  <xsl:template match="summary">
    <tr>
      <div id="summary"/>
    </tr>
    <tr bgcolor="C6D3D9">
      <td>&#160;</td>
      <td>
        <span class="header1">Summary of incident</span>
      </td>
      <td>&#160;</td>
      <td>&#160;</td>
    </tr>
    <tr bgcolor="E4EAED">
      <td>&#160;</td>
      <td>
        <br/>
        <xsl:for-each select="attention-time">
          <xsl:choose>
            <xsl:when test="@value=-1">
              The incident has not been closed.
            </xsl:when>
            <xsl:otherwise>
              <span class="prop-name">Call time:</span>&#160;
              <xsl:value-of select="@value"/>&#160;minute(s).
            </xsl:otherwise>
          </xsl:choose>
        </xsl:for-each>
        <br/>
        <span class="prop-name">Departments involved:</span>
        <xsl:for-each select="involved-departments/department">
          &#160;<xsl:value-of select="@name"/>
          <xsl:choose>
            <xsl:when test="position()=last()">.</xsl:when>
            <xsl:otherwise>,</xsl:otherwise>
          </xsl:choose>
        </xsl:for-each>
        <br/>
        <span class="prop-name">Number of mobile units assigned:</span>
        &#160;<xsl:value-of select="total-assigned-units"/>
        <br/>
        <span class="prop-name">Types of mobile units:</span>
        <xsl:for-each select="unit-types/unit-type">
          &#160;<xsl:value-of select="@name"/>
          <xsl:choose>
            <xsl:when test="position()=last()">.</xsl:when>
            <xsl:otherwise>,</xsl:otherwise>
          </xsl:choose>
        </xsl:for-each>
        <br/>
      </td>
      <td>&#160;</td>
      <td>&#160;</td>
    </tr>
    <xsl:copy-of select="$section-footer"/>
    <tr>
      <td>&#160;</td>
      <td>&#160;</td>
      <td>&#160;</td>
    </tr>
  </xsl:template>
  <!-- END Summary-->

</xsl:stylesheet>
