using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using SmartCadCore.ClientData;
using System.Xml;
using System.IO;
using System.Xml.Xsl;
using SmartCadCore.Core;
using SmartCadCore.Common;
using SmartCadControls.SyncBoxes;

namespace SmartCadDispatch.Controls
{
	public partial class CallsAndDispatchsControl : DevExpress.XtraEditors.XtraUserControl
	{
		private XslCompiledTransform xslPhoneReportCompiledTransform;
		private XslCompiledTransform xslCompiledTransform;
		public CallsAndDispatchsControl()
		{
			InitializeComponent();
            LoadLanguage();
            gridControlExTotalCalls.Type = typeof(GridControlDataAssociatedCallData);
            gridViewExTotalCalls.ViewTotalRows = true;
            gridControlExTotalCalls.ViewTotalRows = true;
            gridControlExTotalCalls.EnableAutoFilter = true;
            gridControlExTotalDispatchs.Type = typeof(GridControlDataAssociatedDispatchOrder);
            gridControlExTotalDispatchs.ViewTotalRows = true;
            gridControlExTotalDispatchs.ViewTotalRows = true;
            gridControlExTotalDispatchs.EnableAutoFilter = true;
        }


        private void LoadLanguage() 
        {
            xtraTabPageCalls.Text = ResourceLoader.GetString2("Calls");
            xtraTabPageDispatchs.Text = ResourceLoader.GetString2("Dispatches");
                   
        }

		private void CallsAndDispatchsControl_Load(object sender, EventArgs e)
		{
			if (this.DesignMode == false)
			{
				XmlReaderSettings xmlReaderSettings = new XmlReaderSettings();
				xmlReaderSettings.ProhibitDtd = false;
				xslCompiledTransform = new XslCompiledTransform();
				xslCompiledTransform.Load(XmlReader.Create(SmartCadConfiguration.IncidentXslt, xmlReaderSettings));
				xslPhoneReportCompiledTransform = new XslCompiledTransform();
				xslPhoneReportCompiledTransform.Load(XmlReader.Create(SmartCadConfiguration.PhoneReportXslt, xmlReaderSettings));
			}				
        }

        private void gridViewExTotalCalls_SelectionChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
		{
			if (gridControlExTotalCalls.SelectedItems.Count > 0)
            {
                try
                {
					PhoneReportClientData phoneReportLight = ((GridControlDataAssociatedCallData)gridControlExTotalCalls.SelectedItems[0]).Tag as PhoneReportClientData;
                    string xml = BuildPhoneReportHtml(phoneReportLight.Xml);                    
                    xml = xml.Replace("language=\"javascript\" />", "language=\"javascript\"></script>");
                    File.WriteAllText("phonereport_firstlevel.html", xml, Encoding.Unicode);

                    FormUtil.InvokeRequired(richTextBoxPhoneReportDetails, delegate
                    {
                        richTextBoxPhoneReportDetails.Navigate(new FileInfo("phonereport_firstlevel.html").FullName);
                    });
                }
                catch (Exception ex)
                {
                    SmartLogger.Print(ex);
                }
            }
            else
            {
               // richTextBoxPhoneReportDetails.Clear();
                richTextBoxPhoneReportDetails.DocumentText = "<span style='HEIGHT:100%;FONT-SIZE: 10pt; FONT-FAMILY: Sans-Serif'>" + ResourceLoader.GetString2("NoPhoneReportSelected") + "</span>";
            }
		}

		private string BuildPhoneReportHtml(string xml)
		{
			try
			{
				StringBuilder sb = new StringBuilder();

				XmlReader input = XmlReader.Create(new StringReader(xml));
#if DEBUG
				File.WriteAllText("phonereport.xml", xml);
#endif
				XmlWriter output = XmlWriter.Create(sb);

				xslPhoneReportCompiledTransform.Transform(input, output);

				return sb.ToString();
			}
			catch (Exception ex)
			{
				SmartLogger.Print(ex);
				return "";
			}
		}

		private void gridControlExTotalDispatchs_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.Control && e.KeyCode == Keys.Tab)
                xtraTabControlCallDispatch.SelectedTabPage = this.xtraTabPageCalls;
            else if (e.KeyCode == Keys.Tab)
            {
                if (e.Shift)
                    this.xtraTabControlCallDispatch.Focus();
                //REMEMBER WHEN ITS INSTALLED FIRST LEVEL
                //else if(this.ParentForm is DefaultFrontClientFormDevX)
				//	(this.ParentForm as DefaultFrontClientFormDevX).callInformationControl.Focus();
                //REMEMBER WHEN ITS INSTALLED FIRST LEVEL
            }
		}

		private void gridControlExTotalCalls_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.Control && e.KeyCode == Keys.Tab)
				xtraTabControlCallDispatch.SelectedTabPage = this.xtraTabPageDispatchs;
            else if (e.KeyCode == Keys.Tab)
            {
                if (e.Shift)
                    this.xtraTabControlCallDispatch.Focus();
                else
                    this.richTextBoxPhoneReportDetails.Focus();
            }
		}

        public void ChangeTabPage(int tabPageIndex)
        {
            xtraTabControlCallDispatch.SelectedTabPageIndex = tabPageIndex;
        }
	}
}
