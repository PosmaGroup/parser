
using SmartCadControls;
using SmartCadControls.Controls;
namespace SmartCadDispatch.Controls
{
	partial class CallsAndDispatchsControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            DatagGridDefaultGroup datagGridDefaultGroup1 = new DatagGridDefaultGroup();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            DatagGridDefaultGroup datagGridDefaultGroup2 = new DatagGridDefaultGroup();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CallsAndDispatchsControl));
            this.richTextBoxPhoneReportDetails = new WebBrowserEx();
            this.dataGridTotalCalls = new DataGridEx();
            this.dataGridTotalDispatches = new DataGridEx();
            this.xtraTabControlCallDispatch = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageCalls = new DevExpress.XtraTab.XtraTabPage();
            this.layoutControlCalls = new DevExpress.XtraLayout.LayoutControl();
            this.gridControlExTotalCalls = new GridControlEx();
            this.gridViewExTotalCalls = new GridViewEx();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemGridControlCalls = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemPhoneReport = new DevExpress.XtraLayout.LayoutControlItem();
            this.xtraTabPageDispatchs = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlExTotalDispatchs = new GridControlEx();
            this.gridViewExTotalDispatchs = new GridViewEx();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridTotalCalls)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridTotalDispatches)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControlCallDispatch)).BeginInit();
            this.xtraTabControlCallDispatch.SuspendLayout();
            this.xtraTabPageCalls.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCalls)).BeginInit();
            this.layoutControlCalls.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExTotalCalls)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExTotalCalls)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemGridControlCalls)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPhoneReport)).BeginInit();
            this.xtraTabPageDispatchs.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExTotalDispatchs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExTotalDispatchs)).BeginInit();
            this.SuspendLayout();
            // 
            // richTextBoxPhoneReportDetails
            // 
            this.richTextBoxPhoneReportDetails.AllowWebBrowserDrop = false;
            this.richTextBoxPhoneReportDetails.Location = new System.Drawing.Point(216, 1);
            this.richTextBoxPhoneReportDetails.Name = "richTextBoxPhoneReportDetails";
            this.richTextBoxPhoneReportDetails.Size = new System.Drawing.Size(234, 229);
            this.richTextBoxPhoneReportDetails.TabIndex = 1;
            this.richTextBoxPhoneReportDetails.WebBrowserShortcutsEnabled = false;
            this.richTextBoxPhoneReportDetails.XmlText = "<HTML></HTML>\0";
            // 
            // dataGridTotalCalls
            // 
            this.dataGridTotalCalls.AllowDrop = true;
            this.dataGridTotalCalls.AllowEditing = false;
            this.dataGridTotalCalls.AllowUserToAddRows = false;
            this.dataGridTotalCalls.AllowUserToDeleteRows = false;
            this.dataGridTotalCalls.AllowUserToOrderColumns = true;
            this.dataGridTotalCalls.AllowUserToResizeRows = false;
            this.dataGridTotalCalls.AllowUserToSortColumns = true;
            this.dataGridTotalCalls.BackgroundColor = System.Drawing.Color.White;
            this.dataGridTotalCalls.ColumnHeadersBackColor = System.Drawing.Color.Empty;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridTotalCalls.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridTotalCalls.ColumnHeadersHeight = 25;
            this.dataGridTotalCalls.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridTotalCalls.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridTotalCalls.Editing = false;
            this.dataGridTotalCalls.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dataGridTotalCalls.EnabledSelectionHandler = true;
            this.dataGridTotalCalls.EnableHeadersVisualStyles = false;
            this.dataGridTotalCalls.EnableSystemShortCuts = false;
            this.dataGridTotalCalls.Grouping = false;
            datagGridDefaultGroup1.Collapsed = false;
            datagGridDefaultGroup1.Column = null;
            datagGridDefaultGroup1.Height = 34;
            datagGridDefaultGroup1.ItemCount = 0;
            datagGridDefaultGroup1.Text = "";
            datagGridDefaultGroup1.Value = null;
            this.dataGridTotalCalls.GroupTemplate = datagGridDefaultGroup1;
            this.dataGridTotalCalls.Location = new System.Drawing.Point(7, 7);
            this.dataGridTotalCalls.MultiSelect = false;
            this.dataGridTotalCalls.Name = "dataGridTotalCalls";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridTotalCalls.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridTotalCalls.SelectedItemBackColor = System.Drawing.Color.Empty;
            this.dataGridTotalCalls.SelectedItemForeColor = System.Drawing.Color.Empty;
            this.dataGridTotalCalls.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridTotalCalls.Size = new System.Drawing.Size(345, 149);
            this.dataGridTotalCalls.SortedColumnColor = System.Drawing.Color.Empty;
            this.dataGridTotalCalls.TabIndex = 0;
            this.dataGridTotalCalls.Type = null;
            this.dataGridTotalCalls.VirtualMode = true;
            // 
            // dataGridTotalDispatches
            // 
            this.dataGridTotalDispatches.AllowDrop = true;
            this.dataGridTotalDispatches.AllowEditing = false;
            this.dataGridTotalDispatches.AllowUserToAddRows = false;
            this.dataGridTotalDispatches.AllowUserToDeleteRows = false;
            this.dataGridTotalDispatches.AllowUserToOrderColumns = true;
            this.dataGridTotalDispatches.AllowUserToResizeRows = false;
            this.dataGridTotalDispatches.AllowUserToSortColumns = true;
            this.dataGridTotalDispatches.BackgroundColor = System.Drawing.Color.White;
            this.dataGridTotalDispatches.ColumnHeadersBackColor = System.Drawing.Color.Empty;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridTotalDispatches.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridTotalDispatches.ColumnHeadersHeight = 25;
            this.dataGridTotalDispatches.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridTotalDispatches.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridTotalDispatches.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridTotalDispatches.Editing = false;
            this.dataGridTotalDispatches.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dataGridTotalDispatches.EnabledSelectionHandler = true;
            this.dataGridTotalDispatches.EnableHeadersVisualStyles = false;
            this.dataGridTotalDispatches.EnableSystemShortCuts = false;
            this.dataGridTotalDispatches.Grouping = false;
            datagGridDefaultGroup2.Collapsed = false;
            datagGridDefaultGroup2.Column = null;
            datagGridDefaultGroup2.Height = 34;
            datagGridDefaultGroup2.ItemCount = 0;
            datagGridDefaultGroup2.Text = "";
            datagGridDefaultGroup2.Value = null;
            this.dataGridTotalDispatches.GroupTemplate = datagGridDefaultGroup2;
            this.dataGridTotalDispatches.Location = new System.Drawing.Point(3, 3);
            this.dataGridTotalDispatches.MultiSelect = false;
            this.dataGridTotalDispatches.Name = "dataGridTotalDispatches";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridTotalDispatches.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridTotalDispatches.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;
            this.dataGridTotalDispatches.SelectedItemBackColor = System.Drawing.Color.Empty;
            this.dataGridTotalDispatches.SelectedItemForeColor = System.Drawing.Color.Empty;
            this.dataGridTotalDispatches.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridTotalDispatches.Size = new System.Drawing.Size(358, 162);
            this.dataGridTotalDispatches.SortedColumnColor = System.Drawing.Color.Empty;
            this.dataGridTotalDispatches.TabIndex = 1;
            this.dataGridTotalDispatches.Type = null;
            this.dataGridTotalDispatches.VirtualMode = true;
            // 
            // xtraTabControlCallDispatch
            // 
            this.xtraTabControlCallDispatch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControlCallDispatch.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControlCallDispatch.Name = "xtraTabControlCallDispatch";
            this.xtraTabControlCallDispatch.SelectedTabPage = this.xtraTabPageCalls;
            this.xtraTabControlCallDispatch.Size = new System.Drawing.Size(456, 259);
            this.xtraTabControlCallDispatch.TabIndex = 8;
            this.xtraTabControlCallDispatch.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageCalls,
            this.xtraTabPageDispatchs});
            // 
            // xtraTabPageCalls
            // 
            this.xtraTabPageCalls.Controls.Add(this.layoutControlCalls);
            this.xtraTabPageCalls.Image = ((System.Drawing.Image)(resources.GetObject("xtraTabPageCalls.Image")));
            this.xtraTabPageCalls.Name = "xtraTabPageCalls";
            this.xtraTabPageCalls.Size = new System.Drawing.Size(451, 231);
            this.xtraTabPageCalls.Text = "xtraTabPageCalls";
            // 
            // layoutControlCalls
            // 
            this.layoutControlCalls.AllowCustomizationMenu = false;
            this.layoutControlCalls.Controls.Add(this.gridControlExTotalCalls);
            this.layoutControlCalls.Controls.Add(this.richTextBoxPhoneReportDetails);
            this.layoutControlCalls.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlCalls.Location = new System.Drawing.Point(0, 0);
            this.layoutControlCalls.Name = "layoutControlCalls";
            this.layoutControlCalls.Root = this.layoutControlGroup1;
            this.layoutControlCalls.Size = new System.Drawing.Size(451, 231);
            this.layoutControlCalls.TabIndex = 1;
            this.layoutControlCalls.Text = "layoutControl1";
            // 
            // gridControlExTotalCalls
            // 
            this.gridControlExTotalCalls.EnableAutoFilter = false;
            this.gridControlExTotalCalls.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExTotalCalls.Location = new System.Drawing.Point(1, 1);
            this.gridControlExTotalCalls.MainView = this.gridViewExTotalCalls;
            this.gridControlExTotalCalls.Name = "gridControlExTotalCalls";
            this.gridControlExTotalCalls.Size = new System.Drawing.Size(213, 229);
            this.gridControlExTotalCalls.TabIndex = 0;
            this.gridControlExTotalCalls.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExTotalCalls});
            this.gridControlExTotalCalls.ViewTotalRows = false;
            this.gridControlExTotalCalls.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridControlExTotalCalls_KeyDown);
            // 
            // gridViewExTotalCalls
            // 
            this.gridViewExTotalCalls.AllowFocusedRowChanged = true;
            this.gridViewExTotalCalls.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewExTotalCalls.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExTotalCalls.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewExTotalCalls.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewExTotalCalls.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewExTotalCalls.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExTotalCalls.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewExTotalCalls.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewExTotalCalls.EnablePreviewLineForFocusedRow = false;
            this.gridViewExTotalCalls.GridControl = this.gridControlExTotalCalls;
            this.gridViewExTotalCalls.Name = "gridViewExTotalCalls";
            this.gridViewExTotalCalls.OptionsNavigation.UseTabKey = false;
            this.gridViewExTotalCalls.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExTotalCalls.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewExTotalCalls.ViewTotalRows = false;
            this.gridViewExTotalCalls.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewExTotalCalls_SelectionChanged);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemGridControlCalls,
            this.layoutControlItemPhoneReport});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(451, 231);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItemGridControlCalls
            // 
            this.layoutControlItemGridControlCalls.Control = this.gridControlExTotalCalls;
            this.layoutControlItemGridControlCalls.CustomizationFormText = "layoutControlItemGridControlCalls";
            this.layoutControlItemGridControlCalls.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemGridControlCalls.Name = "layoutControlItemGridControlCalls";
            this.layoutControlItemGridControlCalls.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItemGridControlCalls.Size = new System.Drawing.Size(215, 231);
            this.layoutControlItemGridControlCalls.Text = "layoutControlItemGridControlCalls";
            this.layoutControlItemGridControlCalls.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemGridControlCalls.TextToControlDistance = 0;
            this.layoutControlItemGridControlCalls.TextVisible = false;
            // 
            // layoutControlItemPhoneReport
            // 
            this.layoutControlItemPhoneReport.Control = this.richTextBoxPhoneReportDetails;
            this.layoutControlItemPhoneReport.CustomizationFormText = "layoutControlItemPhoneReport";
            this.layoutControlItemPhoneReport.Location = new System.Drawing.Point(215, 0);
            this.layoutControlItemPhoneReport.Name = "layoutControlItemPhoneReport";
            this.layoutControlItemPhoneReport.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItemPhoneReport.Size = new System.Drawing.Size(236, 231);
            this.layoutControlItemPhoneReport.Text = "layoutControlItemPhoneReport";
            this.layoutControlItemPhoneReport.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemPhoneReport.TextToControlDistance = 0;
            this.layoutControlItemPhoneReport.TextVisible = false;
            // 
            // xtraTabPageDispatchs
            // 
            this.xtraTabPageDispatchs.Controls.Add(this.gridControlExTotalDispatchs);
            this.xtraTabPageDispatchs.Image = ((System.Drawing.Image)(resources.GetObject("xtraTabPageDispatchs.Image")));
            this.xtraTabPageDispatchs.Name = "xtraTabPageDispatchs";
            this.xtraTabPageDispatchs.Size = new System.Drawing.Size(451, 231);
            this.xtraTabPageDispatchs.Text = "xtraTabPageDispatchs";
            // 
            // gridControlExTotalDispatchs
            // 
            this.gridControlExTotalDispatchs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlExTotalDispatchs.EnableAutoFilter = false;
            this.gridControlExTotalDispatchs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExTotalDispatchs.Location = new System.Drawing.Point(0, 0);
            this.gridControlExTotalDispatchs.MainView = this.gridViewExTotalDispatchs;
            this.gridControlExTotalDispatchs.Name = "gridControlExTotalDispatchs";
            this.gridControlExTotalDispatchs.Size = new System.Drawing.Size(451, 231);
            this.gridControlExTotalDispatchs.TabIndex = 0;
            this.gridControlExTotalDispatchs.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExTotalDispatchs});
            this.gridControlExTotalDispatchs.ViewTotalRows = false;
            this.gridControlExTotalDispatchs.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridControlExTotalDispatchs_KeyDown);
            // 
            // gridViewExTotalDispatchs
            // 
            this.gridViewExTotalDispatchs.AllowFocusedRowChanged = true;
            this.gridViewExTotalDispatchs.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewExTotalDispatchs.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExTotalDispatchs.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewExTotalDispatchs.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewExTotalDispatchs.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewExTotalDispatchs.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExTotalDispatchs.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewExTotalDispatchs.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewExTotalDispatchs.EnablePreviewLineForFocusedRow = false;
            this.gridViewExTotalDispatchs.GridControl = this.gridControlExTotalDispatchs;
            this.gridViewExTotalDispatchs.Name = "gridViewExTotalDispatchs";
            this.gridViewExTotalDispatchs.OptionsNavigation.UseTabKey = false;
            this.gridViewExTotalDispatchs.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExTotalDispatchs.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewExTotalDispatchs.ViewTotalRows = false;
            // 
            // CallsAndDispatchsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.xtraTabControlCallDispatch);
            this.Name = "CallsAndDispatchsControl";
            this.Size = new System.Drawing.Size(456, 259);
            this.Load += new System.EventHandler(this.CallsAndDispatchsControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridTotalCalls)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridTotalDispatches)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControlCallDispatch)).EndInit();
            this.xtraTabControlCallDispatch.ResumeLayout(false);
            this.xtraTabPageCalls.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlCalls)).EndInit();
            this.layoutControlCalls.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExTotalCalls)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExTotalCalls)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemGridControlCalls)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPhoneReport)).EndInit();
            this.xtraTabPageDispatchs.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExTotalDispatchs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExTotalDispatchs)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

		private DataGridEx dataGridTotalCalls;
		private DataGridEx dataGridTotalDispatches;
		private DevExpress.XtraTab.XtraTabControl xtraTabControlCallDispatch;
		private DevExpress.XtraTab.XtraTabPage xtraTabPageCalls;
		private DevExpress.XtraTab.XtraTabPage xtraTabPageDispatchs;
		private DevExpress.XtraLayout.LayoutControl layoutControlCalls;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemGridControlCalls;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemPhoneReport;
		public WebBrowserEx richTextBoxPhoneReportDetails;
		public GridControlEx gridControlExTotalCalls;
		public GridViewEx gridViewExTotalCalls;
		public GridControlEx gridControlExTotalDispatchs;
		public GridViewEx gridViewExTotalDispatchs;
	}
}
