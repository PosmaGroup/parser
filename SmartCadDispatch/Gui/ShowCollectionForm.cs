using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using SmartCadCore.Core;
using SmartCadCore.ClientData;
using SmartCadCore.Common;
using SmartCadControls;
using SmartCadGuiCommon.Util;

namespace SmartCadDispatch.Gui
{
    public partial class ShowCollectionForm : DevExpress.XtraEditors.XtraForm
    {
        private Dictionary<UnitClientData, Exception> units;
        private SkinEngine skinEngine;
        private bool processingDispatches;

        public Dictionary<UnitClientData, Exception> Units
        {
            get
            {
                return units;
            }
            set
            {
                units = value;
                dataGridExUnits.BeginChanges();
                foreach (KeyValuePair<UnitClientData, Exception> pair in units)
                {
                    GridUnitCollectionData item = new GridUnitCollectionData(pair.Key);
                    item.Exception = pair.Value;
                    item.BackColor = Color.White;
                    FormUtil.InvokeRequired(dataGridExUnits, delegate
                    {
                        if (dataGridExUnits.Hash.ContainsKey(pair.Key.CustomCode) == true)
                        {
                            dataGridExUnits.UpdateData(item);
                        }
                        else
                        {
                            dataGridExUnits.AddData(item);
                        }
                    });
                }
                dataGridExUnits.EndChanges();
            }
        }
        public ShowCollectionForm(bool dispatches)
        {
            InitializeComponent();
            this.processingDispatches = dispatches;
            this.dataGridExUnits.Type = typeof(GridUnitCollectionData);
        }

        private void ShowCollectionForm_Load(object sender, EventArgs e)
        {
            SetSkin();
            if (this.processingDispatches == true)
            {
                this.Text = ResourceLoader.GetString2("CancelledDispatchs");
            }
            else
            {
                this.Text = ResourceLoader.GetString2("NonUpdatedUnits");
            }
        }

        private void SetSkin()
        {
            try
            {
                string skinFile = SmartCadConfiguration.SkinsFolder + "/Clasic.xml";

                skinEngine = SkinEngine.Load(skinFile);

                ProfessionalColorTable colorTable = new CustomToolStripColors(
                    skinEngine.Colors["ToolStripGradientBegin"],
                    skinEngine.Colors["ToolStripGradientMiddle"],
                    skinEngine.Colors["ToolStripGradientEnd"],

                    skinEngine.Colors["MenuStripGradientBegin"],
                    skinEngine.Colors["MenuStripGradientEnd"],

                    skinEngine.Colors["StatusStripGradientBegin"],
                    skinEngine.Colors["StatusStripGradientEnd"]
                );

                skinEngine.AddElement("DataGrid", this.dataGridExUnits);

                skinEngine.AddCompleteElement(this);

                skinEngine.ApplyChanges();
            }
            catch (Exception ex)
            {
                MessageForm.Show(ResourceLoader.GetString2("$Message.Error"), ResourceLoader.GetString2("ErrorLoadingSkin."), ex);
            }
        }

        private class GridUnitCollectionData : DataGridExData
        {
            private Exception exception;
            private Color backColor;

            public Exception Exception
            {
                get { return exception; }
                set { exception = value; }
            }
	
            public GridUnitCollectionData(object tag)
                : base(tag)
            { }

            public override Color BackColor
            {
                get
                {
                    return backColor;
                }
                set
                {
                    backColor = value;
                }
            }

            public override string Key
            {
                get
                {
                    return (this.Tag as UnitClientData).CustomCode;
                }
                set
                { }
            }

            [DataGridExColumn(HeaderText = "Placa", Width = 80)]
            public string CustomCode
            {
                get
                {
                    return (this.Tag as UnitClientData).CustomCode;
                }
            }

            [DataGridExColumn(HeaderText = "Organismo", Width = 90)]
            public string DepartmentType
            {
                get
                {
                    return (this.Tag as UnitClientData).DepartmentStation.DepartmentZone.DepartmentType.Name;
                }
            }



            [DataGridExColumn(HeaderText = "Tipo", Width = 80)]
            public string UnitType
            {
                get
                {
                    return (this.Tag as UnitClientData).Type.Name;
                }
            }

            [DataGridExColumn(HeaderText = "Motivo de la cancelacion", Width = 258)]
            public string Status
            {
                get
                {
                    return this.exception.Message;
                }
            }
        }
    }    
}