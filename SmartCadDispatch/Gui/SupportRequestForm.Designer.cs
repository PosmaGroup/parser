using SmartCadControls.Controls;
namespace SmartCadDispatch.Gui
{
    partial class SupportRequestForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private new System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SupportRequestForm));
            this.buttonExAccept = new SimpleButtonEx();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.textBoxComment = new TextBoxEx();
            this.buttonExCancel = new SimpleButtonEx();
            this.departmentTypesControl = new DepartmentTypesControl();
            this.comboBoxUnit = new System.Windows.Forms.ComboBox();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroupRequestedBy = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemMobileUnit = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupDepartments = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemDepartments = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupObservations = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupRequestedBy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemMobileUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupDepartments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDepartments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupObservations)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonExAccept
            // 
            this.buttonExAccept.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonExAccept.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.buttonExAccept.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonExAccept.Appearance.Options.UseFont = true;
            this.buttonExAccept.Appearance.Options.UseForeColor = true;
            this.buttonExAccept.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonExAccept.Enabled = false;
            this.buttonExAccept.FontFormat = FontFormat.GetFormat("ButtonFormat");
            this.buttonExAccept.Location = new System.Drawing.Point(269, 458);
            this.buttonExAccept.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonExAccept.Name = "buttonExAccept";
            this.buttonExAccept.Size = new System.Drawing.Size(101, 26);
            this.buttonExAccept.StyleController = this.layoutControl1;
            this.buttonExAccept.TabIndex = 3;
            this.buttonExAccept.Text = "Accept";
            this.buttonExAccept.Click += new System.EventHandler(this.buttonExAccept_Click);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.textBoxComment);
            this.layoutControl1.Controls.Add(this.buttonExCancel);
            this.layoutControl1.Controls.Add(this.departmentTypesControl);
            this.layoutControl1.Controls.Add(this.buttonExAccept);
            this.layoutControl1.Controls.Add(this.comboBoxUnit);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsFocus.EnableAutoTabOrder = false;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(482, 491);
            this.layoutControl1.TabIndex = 5;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // textBoxComment
            // 
            this.textBoxComment.AcceptsReturn = true;
            this.textBoxComment.AllowsLetters = true;
            this.textBoxComment.AllowsNumbers = true;
            this.textBoxComment.AllowsPunctuation = true;
            this.textBoxComment.AllowsSeparators = true;
            this.textBoxComment.AllowsSymbols = true;
            this.textBoxComment.AllowsWhiteSpaces = true;
            this.textBoxComment.ExtraAllowedChars = "";
            this.textBoxComment.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxComment.Location = new System.Drawing.Point(7, 276);
            this.textBoxComment.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxComment.MaxLength = 1000;
            this.textBoxComment.Multiline = true;
            this.textBoxComment.Name = "textBoxComment";
            this.textBoxComment.NonAllowedCharacters = "";
            this.textBoxComment.RegularExpresion = "";
            this.textBoxComment.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxComment.Size = new System.Drawing.Size(468, 178);
            this.textBoxComment.TabIndex = 2;
            this.textBoxComment.TextChanged += new System.EventHandler(this.EnableAcceptButton);
            // 
            // buttonExCancel
            // 
            this.buttonExCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonExCancel.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.buttonExCancel.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonExCancel.Appearance.Options.UseFont = true;
            this.buttonExCancel.Appearance.Options.UseForeColor = true;
            this.buttonExCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonExCancel.FontFormat = FontFormat.GetFormat("ButtonFormat");
            this.buttonExCancel.Location = new System.Drawing.Point(374, 458);
            this.buttonExCancel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonExCancel.Name = "buttonExCancel";
            this.buttonExCancel.Size = new System.Drawing.Size(101, 26);
            this.buttonExCancel.StyleController = this.layoutControl1;
            this.buttonExCancel.TabIndex = 4;
            this.buttonExCancel.Text = "Cancelar";
            // 
            // departmentTypesControl
            // 
            this.departmentTypesControl.BackColor = System.Drawing.Color.Transparent;
            this.departmentTypesControl.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.departmentTypesControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.departmentTypesControl.GlobalDepartmentTypes = null;
            this.departmentTypesControl.GlobalNotificationPriorities = null;
            this.departmentTypesControl.Location = new System.Drawing.Point(7, 90);
            this.departmentTypesControl.Margin = new System.Windows.Forms.Padding(4);
            this.departmentTypesControl.Name = "departmentTypesControl";
            this.departmentTypesControl.SelectedDepartmentTypesWithPriority = ((System.Collections.IList)(resources.GetObject("departmentTypesControl.SelectedDepartmentTypesWithPriority")));
            this.departmentTypesControl.Size = new System.Drawing.Size(468, 152);
            this.departmentTypesControl.TabIndex = 1;
            // 
            // comboBoxUnit
            // 
            this.comboBoxUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxUnit.FormattingEnabled = true;
            this.comboBoxUnit.Location = new System.Drawing.Point(151, 31);
            this.comboBoxUnit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBoxUnit.Name = "comboBoxUnit";
            this.comboBoxUnit.Size = new System.Drawing.Size(320, 24);
            this.comboBoxUnit.TabIndex = 0;
            this.comboBoxUnit.SelectedIndexChanged += new System.EventHandler(this.EnableAcceptButton);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupRequestedBy,
            this.layoutControlGroupDepartments,
            this.layoutControlGroupObservations});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(482, 491);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroupRequestedBy
            // 
            this.layoutControlGroupRequestedBy.CustomizationFormText = "layoutControlGroupRequestedBy";
            this.layoutControlGroupRequestedBy.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemMobileUnit});
            this.layoutControlGroupRequestedBy.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupRequestedBy.Name = "layoutControlGroupRequestedBy";
            this.layoutControlGroupRequestedBy.Padding = new DevExpress.XtraLayout.Utils.Padding(6, 6, 6, 6);
            this.layoutControlGroupRequestedBy.Size = new System.Drawing.Size(482, 63);
            this.layoutControlGroupRequestedBy.Text = "layoutControlGroupRequestedBy";
            // 
            // layoutControlItemMobileUnit
            // 
            this.layoutControlItemMobileUnit.Control = this.comboBoxUnit;
            this.layoutControlItemMobileUnit.CustomizationFormText = "layoutControlItemMobileUnit";
            this.layoutControlItemMobileUnit.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemMobileUnit.Name = "layoutControlItemMobileUnit";
            this.layoutControlItemMobileUnit.Size = new System.Drawing.Size(464, 25);
            this.layoutControlItemMobileUnit.Text = "layoutControlItemMobileUnit";
            this.layoutControlItemMobileUnit.TextSize = new System.Drawing.Size(136, 13);
            // 
            // layoutControlGroupDepartments
            // 
            this.layoutControlGroupDepartments.CustomizationFormText = "layoutControlGroupDepartments";
            this.layoutControlGroupDepartments.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemDepartments});
            this.layoutControlGroupDepartments.Location = new System.Drawing.Point(0, 63);
            this.layoutControlGroupDepartments.Name = "layoutControlGroupDepartments";
            this.layoutControlGroupDepartments.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupDepartments.Size = new System.Drawing.Size(482, 186);
            this.layoutControlGroupDepartments.Text = "layoutControlGroupDepartments";
            // 
            // layoutControlItemDepartments
            // 
            this.layoutControlItemDepartments.Control = this.departmentTypesControl;
            this.layoutControlItemDepartments.CustomizationFormText = "layoutControlItemDepartmentTypesControl";
            this.layoutControlItemDepartments.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemDepartments.Name = "layoutControlItemDepartments";
            this.layoutControlItemDepartments.Size = new System.Drawing.Size(472, 156);
            this.layoutControlItemDepartments.Text = "layoutControlItemDepartmentTypesControl";
            this.layoutControlItemDepartments.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemDepartments.TextToControlDistance = 0;
            this.layoutControlItemDepartments.TextVisible = false;
            // 
            // layoutControlGroupObservations
            // 
            this.layoutControlGroupObservations.CustomizationFormText = "layoutControlGroupObservations";
            this.layoutControlGroupObservations.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem1,
            this.layoutControlItem3,
            this.emptySpaceItem1});
            this.layoutControlGroupObservations.Location = new System.Drawing.Point(0, 249);
            this.layoutControlGroupObservations.Name = "layoutControlGroupObservations";
            this.layoutControlGroupObservations.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupObservations.Size = new System.Drawing.Size(482, 242);
            this.layoutControlGroupObservations.Text = "layoutControlGroupObservations";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.textBoxComment;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItemComment";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(31, 31);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(472, 182);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "layoutControlItemComment";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.buttonExAccept;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(262, 182);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(105, 30);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(105, 30);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(105, 30);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.buttonExCancel;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(367, 182);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(105, 30);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(105, 30);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(105, 30);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 182);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(262, 30);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // defaultLookAndFeel1
            // 
            this.defaultLookAndFeel1.LookAndFeel.SkinName = "Blue";
            // 
            // SupportRequestForm
            // 
            this.AcceptButton = this.buttonExAccept;
            this.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonExCancel;
            this.ClientSize = new System.Drawing.Size(482, 491);
            this.Controls.Add(this.layoutControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SupportRequestForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Solicitud de apoyo";
            this.Load += new System.EventHandler(this.SupportRequestForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupRequestedBy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemMobileUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupDepartments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDepartments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupObservations)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private SimpleButtonEx buttonExAccept;
		private SimpleButtonEx buttonExCancel;
        private System.Windows.Forms.ComboBox comboBoxUnit;
		private DepartmentTypesControl departmentTypesControl;
        private TextBoxEx textBoxComment;
		private DevExpress.XtraLayout.LayoutControl layoutControl1;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupRequestedBy;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemMobileUnit;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupDepartments;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDepartments;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupObservations;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
		private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
    }
}