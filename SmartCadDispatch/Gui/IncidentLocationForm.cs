using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Smartmatic.SmartCad.Service;
using SmartCadCore.Core;
using SmartCadCore.ClientData;
using System.Collections;
using SmartCadCore.Common;
using SmartCadDispatch.Gui;

namespace SmartCadDispatch.Gui
{
    public partial class IncidentLocationForm : DevExpress.XtraEditors.XtraForm
    {
        private IncidentNotificationClientData incidentNotification;
        private DepartmentStationClientData selectedDeptStation;
        private DepartmentZoneClientData selectedDeptZone;
        private IList departmentZoneList;
        private DefaultDispatchFormDevX parentForm;
        private object syncCommited = new object();

        public IncidentLocationForm(DefaultDispatchFormDevX dispatchForm, IncidentNotificationClientData incidentNotification, IList departmentZones)
        {
            InitializeComponent();
            this.parentForm = dispatchForm;
            this.incidentNotification = incidentNotification;
            this.departmentZoneList = departmentZones;
        }

        private void buttonExAccept_Click(object sender, EventArgs e)
        {
            this.incidentNotification.DepartmentStation = (this.comboBoxStation.SelectedItem as DepartmentStationClientData);
            this.incidentNotification.DepartmentStation.DepartmentZone = (this.comboBoxZone.SelectedItem as DepartmentZoneClientData);
            this.DialogResult = DialogResult.OK;
        }

        private void IncidentLocationForm_Load(object sender, EventArgs e)
        {
            LoadLanguage();
            FillCombos();
            comboBoxZone.Select();
            comboBoxZone.Focus();
            if (selectedDeptStation != null)
            {
                this.comboBoxZone.SelectedItem = selectedDeptZone;
                this.comboBoxStation.SelectedItem = selectedDeptStation;
            }
            FillAddressTextBox();
            if (this.parentForm != null)
            {
                this.parentForm.DispatchCommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(IncidentLocationForm_DispatchCommittedChanges);
            }
        }

        void IncidentLocationForm_DispatchCommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
        {
            try
            {
                if (e.Objects != null && e.Objects.Count > 0)
                {
                    lock (syncCommited)
                    {
                        try
                        {
                            if (ServerServiceClient.GetInstance().OperatorClient == null)
                            { }
                        }
                        catch
                        { }
                        if (e.Objects[0] is DepartmentZoneClientData)
                        {
                            DepartmentZoneClientData zone = e.Objects[0] as DepartmentZoneClientData;
                            if (e.Action == CommittedDataAction.Update)
                            {
                                bool selected = false;
                                FormUtil.InvokeRequired(this.comboBoxZone,
                                delegate
                                {
                                    selected = (this.comboBoxZone.SelectedItem == null ? false : this.comboBoxZone.SelectedItem.Equals(zone));
                                    int index = this.comboBoxZone.Items.IndexOf(zone);
                                    if (index != -1)
                                    {
                                        this.comboBoxZone.Items[index] = zone;
                                        if (selected)
                                            this.comboBoxZone.SelectedIndex = index;
                                    }
                                });
                            }
                            else if (e.Action == CommittedDataAction.Save)
                            {
                                FormUtil.InvokeRequired(this.comboBoxZone,
                                delegate
                                {
                                    this.comboBoxZone.Items.Add(zone);
                                });
                            }
                            else if (e.Action == CommittedDataAction.Delete)
                            {
                                bool selected = false;
                                FormUtil.InvokeRequired(this.comboBoxZone,
                                    delegate
                                    {
                                        selected = this.comboBoxZone.SelectedItem.Equals(zone);
                                        int index = this.comboBoxZone.Items.IndexOf(zone);
                                        if (index != -1)
                                        {
                                            this.comboBoxZone.Items.Remove(zone);
                                            if (selected)
                                                this.comboBoxZone.SelectedIndex = -1;
                                        }
                                    });
                            }
                        }
                        else if (e.Objects[0] is DepartmentStationClientData)
                        {
                            DepartmentStationClientData station = e.Objects[0] as DepartmentStationClientData;
                            if (e.Action == CommittedDataAction.Update)
                            {
                                bool selected = false;
                                FormUtil.InvokeRequired(this.comboBoxStation,
                                    delegate
                                    {
                                        selected = this.comboBoxStation.SelectedItem.Equals(station);
                                        int index = this.comboBoxStation.Items.IndexOf(station);
                                        if (index != -1)
                                        {
                                            this.comboBoxStation.Items[index] = station;
                                            if (selected)
                                                this.comboBoxStation.SelectedIndex = index;
                                        }
                                    });
                            }
                            else if (e.Action == CommittedDataAction.Save)
                            {
                                FormUtil.InvokeRequired(this.comboBoxZone,
                                delegate
                                {
                                    this.comboBoxStation.Items.Add(station);
                                });
                            }
                            else if (e.Action == CommittedDataAction.Delete)
                            {
                                bool selected = false;
                                FormUtil.InvokeRequired(this.comboBoxStation,
                                    delegate
                                    {
                                        selected = this.comboBoxStation.SelectedItem.Equals(station);
                                        int index = this.comboBoxStation.Items.IndexOf(station);
                                        if (index != -1)
                                        {
                                            this.comboBoxStation.Items.Remove(station);
                                            if (selected)
                                                this.comboBoxStation.SelectedIndex = -1;
                                        }
                                    });
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        private void LoadLanguage()
        {
            this.Text = ResourceLoader.GetString2("IncidentLocationForm.Text");
            this.groupBoxAddress.Text = ResourceLoader.GetString2("IncidentLocationForm.groupBoxAddress.Text");
            this.groupBoxZoneStation.Text = ResourceLoader.GetString2("IncidentLocationForm.groupBoxZoneStation.Text");
            this.labelExZone.Text = ResourceLoader.GetString2("IncidentLocationForm.labelExZone.Text") + ":";
            this.labelExStation.Text = ResourceLoader.GetString2("IncidentLocationForm.labelExStation.Text") + ":";
            this.simpleButtonAccept.Text = ResourceLoader.GetString2("$Message.Accept");
            this.simpleButtonCancel.Text = ResourceLoader.GetString2("$Message.Cancel");
            
        }

        private void FillAddressTextBox()
        {
            if (incidentNotification.IncidentAddress.Zone != null &&
                incidentNotification.IncidentAddress.Zone.Trim().Length > 0)
            {
                this.labelExAddress.Text += ResourceLoader.GetString2("UrbSector");
            }
            this.labelExAddress.Text += incidentNotification.IncidentAddress.Zone;
            if (incidentNotification.IncidentAddress.Zone != null &&
                incidentNotification.IncidentAddress.Zone.Trim().Length > 0)
            {
                this.labelExAddress.Text += ".";
            }

            if (this.labelExAddress.Text.Trim().Length > 0 &&
                incidentNotification.IncidentAddress.Street != null &&
                incidentNotification.IncidentAddress.Street.Trim().Length > 0)
            {
                this.labelExAddress.Text += "\n";
                this.labelExAddress.Text += ResourceLoader.GetString2("StreetAvCorner");
            }
            this.labelExAddress.Text += incidentNotification.IncidentAddress.Street;
            if (incidentNotification.IncidentAddress.Street.Trim().Length > 0)
            {
                this.labelExAddress.Text += ".";
            }

            if (incidentNotification.IncidentAddress.Reference != null)
            {
                if (this.labelExAddress.Text.Trim().Length > 0 &&
                    incidentNotification.IncidentAddress.Reference.Trim().Length > 0)
                {
                    this.labelExAddress.Text += "\n";
                    this.labelExAddress.Text += ResourceLoader.GetString2("HouseBuilding");
                }
                this.labelExAddress.Text += incidentNotification.IncidentAddress.Reference;
                if (incidentNotification.IncidentAddress.Reference.Trim().Length > 0)
                {
                    this.labelExAddress.Text += ".";
                }
            }
            else if (incidentNotification.IncidentAddress.State != null)
            {
                if (this.labelExAddress.Text.Trim().Length > 0 &&
                      incidentNotification.IncidentAddress.State.Trim().Length > 0)
                {
                    this.labelExAddress.Text += "\n";
                    this.labelExAddress.Text += ResourceLoader.GetString2("State");
                }
                this.labelExAddress.Text += incidentNotification.IncidentAddress.State;
                if (incidentNotification.IncidentAddress.State.Trim().Length > 0)
                {
                    this.labelExAddress.Text += ".";
                }
            }
            if (incidentNotification.IncidentAddress.More != null)
            {
                if (this.labelExAddress.Text.Trim().Length > 0 &&
                    incidentNotification.IncidentAddress.More.Trim().Length > 0)
                {
                    this.labelExAddress.Text += "\n";
                    this.labelExAddress.Text += ResourceLoader.GetString2("FloorApt");
                }
                this.labelExAddress.Text += incidentNotification.IncidentAddress.More;
                if (incidentNotification.IncidentAddress.More.Trim().Length > 0)
                {
                    this.labelExAddress.Text += ".";
                }
            }
            else if (incidentNotification.IncidentAddress.Town != null)
            {
                if (this.labelExAddress.Text.Trim().Length > 0 &&
                        incidentNotification.IncidentAddress.Town.Trim().Length > 0)
                {
                    this.labelExAddress.Text += "\n";
                    this.labelExAddress.Text += ResourceLoader.GetString2("Town");
                }
                this.labelExAddress.Text += incidentNotification.IncidentAddress.Town;
                if (incidentNotification.IncidentAddress.Town.Trim().Length > 0)
                {
                    this.labelExAddress.Text += ".";
                }
            }
        }

        private IList GetDepartmentZonesByDepartmentTypeCode(int departmentTypeCode)
        {
            IList result = new ArrayList();

            foreach (DepartmentZoneClientData departmentZone in departmentZoneList)
            {
                if (departmentZone.DepartmentType.Code == departmentTypeCode)
                    result.Add(departmentZone);
            }

            return result;
        }

        private void FillCombos()
        {
            if (this.incidentNotification != null)
            {
                IList departmentZones = GetDepartmentZonesByDepartmentTypeCode(incidentNotification.DepartmentType.Code);

                foreach (DepartmentZoneClientData zone in departmentZones)
                {
                    comboBoxZone.Items.Add(zone);
                    if (this.incidentNotification.DepartmentStation != null)
                    {
                        bool found = false;
                        for (int i = 0; i < zone.DepartmentStations.Count && found == false; i++)
                        {
                            DepartmentStationClientData station =
                                zone.DepartmentStations[i] as DepartmentStationClientData;
                            if (station.Code == this.incidentNotification.DepartmentStation.Code)
                            {
                                selectedDeptStation = station;
                                selectedDeptZone = zone;
                                found = true;
                            }
                        }
                    }
                }
            }
        }

        private void EnableAcceptButton()
        {
            if (this.comboBoxZone.SelectedIndex != -1 && this.comboBoxStation.SelectedIndex != -1)
            {
                this.simpleButtonAccept.Enabled = true;
            }
            else
            {
                this.simpleButtonAccept.Enabled = false;
            }
        }

        private void comboBoxZone_Click(object sender, EventArgs e)
        {
            if (comboBoxZone.SelectedItem != null)
            {
                toolTip.SetToolTip(comboBoxZone, comboBoxZone.SelectedItem.ToString());
                toolTip.SetToolTip(comboBoxStation, string.Empty);

                DepartmentZoneClientData selectedZone = comboBoxZone.SelectedItem as DepartmentZoneClientData;
                
                comboBoxStation.Items.Clear();
                
                foreach (DepartmentStationClientData station in selectedZone.DepartmentStations)
                {
                    comboBoxStation.Items.Add(station);
                }
                comboBoxStation.Sorted = true;
            }

            EnableAcceptButton();
        }

        private void comboBoxStation_Click(object sender, EventArgs e)
        {
            if (comboBoxStation.SelectedItem != null)
            {
                toolTip.SetToolTip(comboBoxStation, comboBoxStation.SelectedItem.ToString());                
            }

            EnableAcceptButton();
        }

        private void IncidentLocationForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.parentForm != null)
            {
                this.parentForm.DispatchCommittedChanges -= new EventHandler<CommittedObjectDataCollectionEventArgs>(IncidentLocationForm_DispatchCommittedChanges);
            }
        }
    }
}