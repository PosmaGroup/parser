using SmartCadControls;
using SmartCadControls.Controls;
using System.Windows.Forms;
namespace SmartCadDispatch.Gui
{
    partial class ChangeDispatchStatusForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChangeDispatchStatusForm));
            this.groupBoxUnit = new DevExpress.XtraEditors.GroupControl();
            this.gridControlExDispatchOrder = new GridControlEx();
            this.gridViewExDispatchOrder = new GridViewEx();
            this.groupBoxDispatch = new DevExpress.XtraEditors.GroupControl();
            this.comboBoxStatus = new System.Windows.Forms.ComboBox();
            this.labelChangeStatus = new System.Windows.Forms.Label();
            this.buttonEstimateTime = new System.Windows.Forms.Button();
            this.buttonCloseReport = new System.Windows.Forms.Button();
            this.timeSelectorControl1 = new TimeSelectorControl();
            this.labelArrivalDateText = new System.Windows.Forms.Label();
            this.labelArrivalDate = new System.Windows.Forms.Label();
            this.labelStatusText = new System.Windows.Forms.Label();
            this.labelStatus = new System.Windows.Forms.Label();
            this.labelExpectedTimeText = new System.Windows.Forms.Label();
            this.labelExpectedTime = new System.Windows.Forms.Label();
            this.textBoxComment = new TextBoxEx();
            this.labelComment = new System.Windows.Forms.Label();
            this.labelDepartureDateText = new System.Windows.Forms.Label();
            this.labelDepartureDate = new System.Windows.Forms.Label();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.simpleButtonAccept = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.groupBoxUnit)).BeginInit();
            this.groupBoxUnit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExDispatchOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExDispatchOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBoxDispatch)).BeginInit();
            this.groupBoxDispatch.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxUnit
            // 
            this.groupBoxUnit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxUnit.AppearanceCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxUnit.AppearanceCaption.Options.UseFont = true;
            this.groupBoxUnit.Controls.Add(this.gridControlExDispatchOrder);
            this.groupBoxUnit.Location = new System.Drawing.Point(6, 9);
            this.groupBoxUnit.Name = "groupBoxUnit";
            this.groupBoxUnit.Padding = new System.Windows.Forms.Padding(3);
            this.groupBoxUnit.Size = new System.Drawing.Size(491, 146);
            this.groupBoxUnit.TabIndex = 0;
            this.groupBoxUnit.Text = "Datos de la unidad";
            // 
            // gridControlExDispatchOrder
            // 
            this.gridControlExDispatchOrder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlExDispatchOrder.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(2);
            this.gridControlExDispatchOrder.EnableAutoFilter = false;
            this.gridControlExDispatchOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExDispatchOrder.Location = new System.Drawing.Point(5, 24);
            this.gridControlExDispatchOrder.MainView = this.gridViewExDispatchOrder;
            this.gridControlExDispatchOrder.Margin = new System.Windows.Forms.Padding(2);
            this.gridControlExDispatchOrder.Name = "gridControlExDispatchOrder";
            this.gridControlExDispatchOrder.Size = new System.Drawing.Size(481, 117);
            this.gridControlExDispatchOrder.TabIndex = 1;
            this.gridControlExDispatchOrder.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExDispatchOrder});
            this.gridControlExDispatchOrder.ViewTotalRows = false;
            this.gridControlExDispatchOrder.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridControlExDispatchOrder_KeyDown);
            // 
            // gridViewExDispatchOrder
            // 
            this.gridViewExDispatchOrder.AllowFocusedRowChanged = true;
            this.gridViewExDispatchOrder.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewExDispatchOrder.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExDispatchOrder.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewExDispatchOrder.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewExDispatchOrder.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewExDispatchOrder.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExDispatchOrder.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewExDispatchOrder.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewExDispatchOrder.EnablePreviewLineForFocusedRow = false;
            this.gridViewExDispatchOrder.GridControl = this.gridControlExDispatchOrder;
            this.gridViewExDispatchOrder.Name = "gridViewExDispatchOrder";
            this.gridViewExDispatchOrder.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExDispatchOrder.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewExDispatchOrder.ViewTotalRows = false;
            this.gridViewExDispatchOrder.SelectionWillChange += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewExDispatchOrder_SelectionWillChange);
            // 
            // groupBoxDispatch
            // 
            this.groupBoxDispatch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxDispatch.AppearanceCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxDispatch.AppearanceCaption.Options.UseFont = true;
            this.groupBoxDispatch.Controls.Add(this.comboBoxStatus);
            this.groupBoxDispatch.Controls.Add(this.labelChangeStatus);
            this.groupBoxDispatch.Controls.Add(this.buttonEstimateTime);
            this.groupBoxDispatch.Controls.Add(this.buttonCloseReport);
            this.groupBoxDispatch.Controls.Add(this.timeSelectorControl1);
            this.groupBoxDispatch.Controls.Add(this.labelArrivalDateText);
            this.groupBoxDispatch.Controls.Add(this.labelArrivalDate);
            this.groupBoxDispatch.Controls.Add(this.labelStatusText);
            this.groupBoxDispatch.Controls.Add(this.labelStatus);
            this.groupBoxDispatch.Controls.Add(this.labelExpectedTimeText);
            this.groupBoxDispatch.Controls.Add(this.labelExpectedTime);
            this.groupBoxDispatch.Controls.Add(this.textBoxComment);
            this.groupBoxDispatch.Controls.Add(this.labelComment);
            this.groupBoxDispatch.Controls.Add(this.labelDepartureDateText);
            this.groupBoxDispatch.Controls.Add(this.labelDepartureDate);
            this.groupBoxDispatch.Location = new System.Drawing.Point(6, 162);
            this.groupBoxDispatch.Name = "groupBoxDispatch";
            this.groupBoxDispatch.Padding = new System.Windows.Forms.Padding(3);
            this.groupBoxDispatch.Size = new System.Drawing.Size(491, 112);
            this.groupBoxDispatch.TabIndex = 1;
            this.groupBoxDispatch.Text = "Datos de la orden de despacho";
            // 
            // comboBoxStatus
            // 
            this.comboBoxStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxStatus.FormattingEnabled = true;
            this.comboBoxStatus.Location = new System.Drawing.Point(235, 60);
            this.comboBoxStatus.Margin = new System.Windows.Forms.Padding(2);
            this.comboBoxStatus.Name = "comboBoxStatus";
            this.comboBoxStatus.Size = new System.Drawing.Size(116, 21);
            this.comboBoxStatus.TabIndex = 5;
            this.comboBoxStatus.SelectedIndexChanged += new System.EventHandler(this.comboBoxStatus_SelectedIndexChanged);
            // 
            // labelChangeStatus
            // 
            this.labelChangeStatus.AutoSize = true;
            this.labelChangeStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelChangeStatus.Location = new System.Drawing.Point(6, 63);
            this.labelChangeStatus.Name = "labelChangeStatus";
            this.labelChangeStatus.Size = new System.Drawing.Size(94, 13);
            this.labelChangeStatus.TabIndex = 4;
            this.labelChangeStatus.Text = "Cambiar estatus a:";
            // 
            // buttonEstimateTime
            // 
            this.buttonEstimateTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonEstimateTime.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonEstimateTime.Image = ((System.Drawing.Image)(resources.GetObject("buttonEstimateTime.Image")));
            this.buttonEstimateTime.Location = new System.Drawing.Point(449, 63);
            this.buttonEstimateTime.Name = "buttonEstimateTime";
            this.buttonEstimateTime.Size = new System.Drawing.Size(32, 32);
            this.buttonEstimateTime.TabIndex = 12;
            this.buttonEstimateTime.UseVisualStyleBackColor = true;
            this.buttonEstimateTime.Click += new System.EventHandler(this.buttonEstimateTime_Click);
            this.buttonEstimateTime.MouseEnter += new System.EventHandler(this.buttonEstimateTime_MouseEnter);
            this.buttonEstimateTime.MouseLeave += new System.EventHandler(this.buttonEstimateTime_MouseLeave);
            // 
            // buttonCloseReport
            // 
            this.buttonCloseReport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCloseReport.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonCloseReport.Image = ((System.Drawing.Image)(resources.GetObject("buttonCloseReport.Image")));
            this.buttonCloseReport.Location = new System.Drawing.Point(449, 52);
            this.buttonCloseReport.Name = "buttonCloseReport";
            this.buttonCloseReport.Size = new System.Drawing.Size(32, 32);
            this.buttonCloseReport.TabIndex = 7;
            this.buttonCloseReport.UseVisualStyleBackColor = true;
            this.buttonCloseReport.Click += new System.EventHandler(this.buttonCloseReport_Click);
            this.buttonCloseReport.MouseEnter += new System.EventHandler(this.buttonCloseReport_MouseEnter);
            this.buttonCloseReport.MouseLeave += new System.EventHandler(this.buttonCloseReport_MouseLeave);
            // 
            // timeSelectorControl1
            // 
            this.timeSelectorControl1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.timeSelectorControl1.BackColor = System.Drawing.SystemColors.Control;
            this.timeSelectorControl1.Enabled = false;
            this.timeSelectorControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeSelectorControl1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.timeSelectorControl1.Location = new System.Drawing.Point(135, 84);
            this.timeSelectorControl1.Margin = new System.Windows.Forms.Padding(4);
            this.timeSelectorControl1.Name = "timeSelectorControl1";
            this.timeSelectorControl1.ShowSeconds = false;
            this.timeSelectorControl1.Size = new System.Drawing.Size(122, 24);
            this.timeSelectorControl1.TabIndex = 11;
            this.timeSelectorControl1.TimeSpan = System.TimeSpan.Parse("00:00:00");
            this.timeSelectorControl1.Visible = false;
            this.timeSelectorControl1.TimeChanged += new System.EventHandler(this.timeSelectorControl1_TimeChanged);
            // 
            // labelArrivalDateText
            // 
            this.labelArrivalDateText.AutoSize = true;
            this.labelArrivalDateText.Location = new System.Drawing.Point(105, 63);
            this.labelArrivalDateText.Name = "labelArrivalDateText";
            this.labelArrivalDateText.Size = new System.Drawing.Size(0, 13);
            this.labelArrivalDateText.TabIndex = 6;
            // 
            // labelArrivalDate
            // 
            this.labelArrivalDate.AutoSize = true;
            this.labelArrivalDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelArrivalDate.Location = new System.Drawing.Point(6, 63);
            this.labelArrivalDate.Name = "labelArrivalDate";
            this.labelArrivalDate.Size = new System.Drawing.Size(224, 13);
            this.labelArrivalDate.TabIndex = 12;
            this.labelArrivalDate.Text = "Hora de llegada        (mm/dd/aaaa hh:mm:ss):";
            // 
            // labelStatusText
            // 
            this.labelStatusText.AutoSize = true;
            this.labelStatusText.Location = new System.Drawing.Point(105, 18);
            this.labelStatusText.Name = "labelStatusText";
            this.labelStatusText.Size = new System.Drawing.Size(0, 13);
            this.labelStatusText.TabIndex = 1;
            // 
            // labelStatus
            // 
            this.labelStatus.AutoSize = true;
            this.labelStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStatus.Location = new System.Drawing.Point(6, 20);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(45, 13);
            this.labelStatus.TabIndex = 0;
            this.labelStatus.Text = "Estatus:";
            // 
            // labelExpectedTimeText
            // 
            this.labelExpectedTimeText.AutoSize = true;
            this.labelExpectedTimeText.Location = new System.Drawing.Point(105, 84);
            this.labelExpectedTimeText.Name = "labelExpectedTimeText";
            this.labelExpectedTimeText.Size = new System.Drawing.Size(0, 13);
            this.labelExpectedTimeText.TabIndex = 9;
            // 
            // labelExpectedTime
            // 
            this.labelExpectedTime.AutoSize = true;
            this.labelExpectedTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExpectedTime.Location = new System.Drawing.Point(6, 88);
            this.labelExpectedTime.Name = "labelExpectedTime";
            this.labelExpectedTime.Size = new System.Drawing.Size(137, 13);
            this.labelExpectedTime.TabIndex = 8;
            this.labelExpectedTime.Text = "Tiempo estimado (hh:mm): *";
            // 
            // textBoxComment
            // 
            this.textBoxComment.AcceptsReturn = true;
            this.textBoxComment.AllowsLetters = true;
            this.textBoxComment.AllowsNumbers = true;
            this.textBoxComment.AllowsPunctuation = true;
            this.textBoxComment.AllowsSeparators = true;
            this.textBoxComment.AllowsSymbols = true;
            this.textBoxComment.AllowsWhiteSpaces = true;
            this.textBoxComment.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxComment.Enabled = false;
            this.textBoxComment.ExtraAllowedChars = "";
            this.textBoxComment.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxComment.Location = new System.Drawing.Point(9, 133);
            this.textBoxComment.MaxLength = 1000;
            this.textBoxComment.Multiline = true;
            this.textBoxComment.Name = "textBoxComment";
            this.textBoxComment.NonAllowedCharacters = "";
            this.textBoxComment.RegularExpresion = "";
            this.textBoxComment.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxComment.Size = new System.Drawing.Size(476, 76);
            this.textBoxComment.TabIndex = 14;
            this.textBoxComment.TextChanged += new System.EventHandler(this.textBoxComment_TextChanged);
            // 
            // labelComment
            // 
            this.labelComment.AutoSize = true;
            this.labelComment.Enabled = false;
            this.labelComment.Location = new System.Drawing.Point(7, 112);
            this.labelComment.Name = "labelComment";
            this.labelComment.Size = new System.Drawing.Size(125, 13);
            this.labelComment.TabIndex = 13;
            this.labelComment.Text = "Motivo de cancelacion: *";
            // 
            // labelDepartureDateText
            // 
            this.labelDepartureDateText.AutoSize = true;
            this.labelDepartureDateText.Location = new System.Drawing.Point(105, 40);
            this.labelDepartureDateText.Name = "labelDepartureDateText";
            this.labelDepartureDateText.Size = new System.Drawing.Size(0, 13);
            this.labelDepartureDateText.TabIndex = 3;
            // 
            // labelDepartureDate
            // 
            this.labelDepartureDate.AutoSize = true;
            this.labelDepartureDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDepartureDate.Location = new System.Drawing.Point(6, 40);
            this.labelDepartureDate.Name = "labelDepartureDate";
            this.labelDepartureDate.Size = new System.Drawing.Size(264, 13);
            this.labelDepartureDate.TabIndex = 2;
            this.labelDepartureDate.Text = "Dispatch Date Time               (mm/dd/aaaa hh:mm:ss):";
            // 
            // simpleButtonAccept
            // 
            this.simpleButtonAccept.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonAccept.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButtonAccept.Appearance.Options.UseFont = true;
            this.simpleButtonAccept.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.simpleButtonAccept.Location = new System.Drawing.Point(337, 295);
            this.simpleButtonAccept.Margin = new System.Windows.Forms.Padding(2);
            this.simpleButtonAccept.Name = "simpleButtonAccept";
            this.simpleButtonAccept.Size = new System.Drawing.Size(75, 25);
            this.simpleButtonAccept.TabIndex = 4;
            this.simpleButtonAccept.Text = "Accept";
            this.simpleButtonAccept.Click += new System.EventHandler(this.buttonAccept_Click);
            // 
            // simpleButtonCancel
            // 
            this.simpleButtonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonCancel.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButtonCancel.Appearance.Options.UseFont = true;
            this.simpleButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonCancel.Location = new System.Drawing.Point(416, 295);
            this.simpleButtonCancel.Margin = new System.Windows.Forms.Padding(2);
            this.simpleButtonCancel.Name = "simpleButtonCancel";
            this.simpleButtonCancel.Size = new System.Drawing.Size(75, 25);
            this.simpleButtonCancel.TabIndex = 5;
            this.simpleButtonCancel.Text = "Cancelar";
            // 
            // ChangeDispatchStatusForm
            // 
            this.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(502, 324);
            this.Controls.Add(this.simpleButtonCancel);
            this.Controls.Add(this.simpleButtonAccept);
            this.Controls.Add(this.groupBoxDispatch);
            this.Controls.Add(this.groupBoxUnit);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ChangeDispatchStatusForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cancelar solicitud de despacho";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ChangeDispatchStatusForm_FormClosing);
            this.Load += new System.EventHandler(this.ChangeDispatchStatusForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupBoxUnit)).EndInit();
            this.groupBoxUnit.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExDispatchOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExDispatchOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBoxDispatch)).EndInit();
            this.groupBoxDispatch.ResumeLayout(false);
            this.groupBoxDispatch.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupBoxUnit;
        private DevExpress.XtraEditors.GroupControl groupBoxDispatch;
        private TextBoxEx textBoxComment;
        private System.Windows.Forms.Label labelComment;
        private System.Windows.Forms.Label labelDepartureDateText;
        private System.Windows.Forms.Label labelDepartureDate;
        private System.Windows.Forms.Label labelExpectedTime;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.Label labelExpectedTimeText;
        private System.Windows.Forms.Label labelStatusText;
        private System.Windows.Forms.Label labelStatus;
        private System.Windows.Forms.Label labelArrivalDateText;
        private System.Windows.Forms.Label labelArrivalDate;
        private TimeSelectorControl timeSelectorControl1;
        private System.Windows.Forms.Button buttonCloseReport;
        private System.Windows.Forms.Button buttonEstimateTime;
        private System.Windows.Forms.Label labelChangeStatus;
        private System.Windows.Forms.ComboBox comboBoxStatus;
        private GridControlEx gridControlExDispatchOrder;
        private GridViewEx gridViewExDispatchOrder;
        private DevExpress.XtraEditors.SimpleButton simpleButtonAccept;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCancel;
    }
}
