using SmartCadControls;
namespace SmartCadDispatch.Gui
{
    partial class ShowCollectionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            DatagGridDefaultGroup datagGridDefaultGroup1 = new DatagGridDefaultGroup();
            this.buttonAccept = new DevExpress.XtraEditors.SimpleButton();
            this.dataGridExUnits = new DataGridEx();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridExUnits)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonAccept
            // 
            this.buttonAccept.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonAccept.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonAccept.Location = new System.Drawing.Point(231, 145);
            this.buttonAccept.Name = "buttonAccept";
            this.buttonAccept.Size = new System.Drawing.Size(75, 23);
            this.buttonAccept.TabIndex = 1;
            this.buttonAccept.Text = "Accept";
            // 
            // dataGridExUnits
            // 
            this.dataGridExUnits.AllowDrop = true;
            this.dataGridExUnits.AllowEditing = false;
            this.dataGridExUnits.AllowUserToAddRows = false;
            this.dataGridExUnits.AllowUserToDeleteRows = false;
            this.dataGridExUnits.AllowUserToOrderColumns = true;
            this.dataGridExUnits.AllowUserToResizeRows = false;
            this.dataGridExUnits.AllowUserToSortColumns = true;
            this.dataGridExUnits.BackgroundColor = System.Drawing.Color.White;
            this.dataGridExUnits.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridExUnits.ColumnHeadersBackColor = System.Drawing.Color.Empty;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Tahoma", 8.25F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridExUnits.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridExUnits.ColumnHeadersHeight = 25;
            this.dataGridExUnits.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridExUnits.Dock = System.Windows.Forms.DockStyle.Top;
            this.dataGridExUnits.Editing = false;
            this.dataGridExUnits.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dataGridExUnits.EnabledSelectionHandler = true;
            this.dataGridExUnits.EnableHeadersVisualStyles = false;
            this.dataGridExUnits.EnableSystemShortCuts = false;
            this.dataGridExUnits.Grouping = false;
            datagGridDefaultGroup1.Collapsed = false;
            datagGridDefaultGroup1.Column = null;
            datagGridDefaultGroup1.Height = 34;
            datagGridDefaultGroup1.ItemCount = 0;
            datagGridDefaultGroup1.Text = "";
            datagGridDefaultGroup1.Value = null;
            this.dataGridExUnits.GroupTemplate = datagGridDefaultGroup1;
            this.dataGridExUnits.Location = new System.Drawing.Point(4, 4);
            this.dataGridExUnits.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dataGridExUnits.MultiSelect = false;
            this.dataGridExUnits.Name = "dataGridExUnits";
            this.dataGridExUnits.ReadOnly = true;
            this.dataGridExUnits.SelectedItemBackColor = System.Drawing.Color.Empty;
            this.dataGridExUnits.SelectedItemForeColor = System.Drawing.Color.Empty;
            this.dataGridExUnits.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridExUnits.Size = new System.Drawing.Size(509, 136);
            this.dataGridExUnits.SortedColumnColor = System.Drawing.Color.Empty;
            this.dataGridExUnits.TabIndex = 0;
            this.dataGridExUnits.Type = null;
            this.dataGridExUnits.VirtualMode = true;
            // 
            // ShowCollectionForm
            // 
            this.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(517, 174);
            this.Controls.Add(this.dataGridExUnits);
            this.Controls.Add(this.buttonAccept);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.LookAndFeel.SkinName = "Blue";
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ShowCollectionForm";
            this.Padding = new System.Windows.Forms.Padding(4);
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Despachos cancelados";
            this.Load += new System.EventHandler(this.ShowCollectionForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridExUnits)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton buttonAccept;
        private DataGridEx dataGridExUnits;
    }
}