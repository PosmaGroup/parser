using Microsoft.Win32;
using SmartCadControls;
using SmartCadControls.Controls;
using SmartCadDispatch.Controls;
using System.IO;

namespace SmartCadDispatch.Gui
{
	partial class DefaultDispatchFormDevX
    {
        private new System.ComponentModel.IContainer components = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();

                RegistryKey regkey;

                try
                {
                    DirectoryInfo source = new DirectoryInfo(SourceDirectory);

                    //Determine whether the source directory exists.
                    if (!source.Exists)
                        regkey = Registry.Users;
                    else
                        regkey = Registry.CurrentUser;

                    // Restore the Print Page Setup properties
                    regkey = regkey.OpenSubKey(@SourceDirectory, true);
                    if (regkey != null)
                    {
                        // Restore the footer values.			
                        regkey.SetValue("footer", IEFooter.ToString());
                    }
                }
                catch
                { }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DefaultDispatchFormDevX));
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.toolTipGeneral = new System.Windows.Forms.ToolTip(this.components);
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.radioButtonAll = new System.Windows.Forms.RadioButton();
            this.contextMenuStripUnits = new ContextMenuStripEx(this.components);
            this.toolStripMenuItemAssignUnit = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemAvailableUnit = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemOutOfOrderUnit = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemNotAvailableUnit = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemOnSceneUnit = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemDelayUnit = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemAddNote = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemCloseUnit = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemLocateUnit = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemCallUnit = new System.Windows.Forms.ToolStripMenuItem();
            this.labelExShowBy = new LabelEx();
            this.radioButtonZone = new System.Windows.Forms.RadioButton();
            this.buttonReqAttention = new SimpleButtonEx();
            this.layoutControlDispatch = new DevExpress.XtraLayout.LayoutControl();
            this.callsAndDispatch = new CallsAndDispatchsControl();
            this.buttonAddNotes = new SimpleButtonEx();
            this.textBoxIncidentNotes = new TextBoxEx();
            this.gridControlDispatchOrder = new GridControlEx();
            this.contextMenuDispatchOrder = new ContextMenuStripEx(this.components);
            this.enEscenaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modifyTimeOrderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemAddNoteToUnit = new System.Windows.Forms.ToolStripMenuItem();
            this.cerrarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemLocateUnitDispatch = new System.Windows.Forms.ToolStripMenuItem();
            this.gridViewDispatchOrder = new GridViewEx();
            this.blinkingButtonExPendingDispatch = new BlinkingSimpleButtonEx();
            this.textBoxIncidentDetails = new SearchableWebBrowser();
            this.gridControlAssignedIncidentNotifications = new GridControlEx();
            this.contextMenuStripAssignedIncidentNotif = new ContextMenuStripEx(this.components);
            this.toolStripMenuItemRefreshIncidentNotitficationDetails = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemPending = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemModifyZone = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemSupportRequest = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemCloseIncNotif = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemLocateAssignedIncNot = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemRecommendedUnits = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemAssignedUnits = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemCallStation = new System.Windows.Forms.ToolStripMenuItem();
            this.gridViewAssignedIncidentNotifications = new GridViewEx();
            this.blinkingButtonExDispReqAttention = new BlinkingSimpleButtonEx();
            this.buttonClosedDispatch = new SimpleButtonEx();
            this.buttonExAssign = new SimpleButtonEx();
            this.buttonActiveDispatch = new SimpleButtonEx();
            this.radioButtonDepartment = new System.Windows.Forms.RadioButton();
            this.gridControlUnits = new GridControlEx();
            this.gridViewUnits = new GridViewEx();
            this.buttonExAddNotification = new SimpleButtonEx();
            this.buttonAssignedUnits = new SimpleButtonEx();
            this.gridControlNewIncidentNotifications = new GridControlEx();
            this.contextMenuStripNewIncidentNotification = new ContextMenuStripEx(this.components);
            this.toolStripMenuItemAssign = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemRefreshNewIncidentNotificationDetails = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemLocateNewIncNot = new System.Windows.Forms.ToolStripMenuItem();
            this.gridViewNewIncidentNotifications = new GridViewEx();
            this.gridViewEx2 = new GridViewEx();
            this.buttonAllUnits = new SimpleButtonEx();
            this.buttonAvailableUnits = new SimpleButtonEx();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroupIncidentDetails = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabbedControlGroupNotes = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroupNotes = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            this.layoutControlGroupRight = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroupUnits = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupDispatchOrder = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupLeft = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroupAssignedIncidentNotifications = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupNewIncidentNotifications = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.timerSendImage = new System.Windows.Forms.Timer(this.components);
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barButtonItemPending = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemModifyZone = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemClose = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemAvailable = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemOutOfOrder = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemUnavailable = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemOnScene = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemAssignedOnScene = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemModifyTime = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemEnd = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemStatus = new DevExpress.XtraBars.BarButtonItem();
            this.popupMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barButtonItemSupportRequest = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemAddNote = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemRefreshOrder = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemSave = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemPrint = new DevExpress.XtraBars.BarButtonItem();
            this.barItemRecommendedUnits = new DevExpress.XtraBars.BarButtonItem();
            this.barItemAsignedUnits = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemLocateObject = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemUnitsFollow = new DevExpress.XtraBars.BarButtonItem();
            this.barEditItemDepartment = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemComboBoxDepartment = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.barButtonItemCall = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemTalk = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemRestartTelephone = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroupDispatchOrder = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupUnits = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupAssignUnits = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupUser = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupRadio = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupGeneralOptions = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupTools = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.contextMenuStripUnits.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlDispatch)).BeginInit();
            this.layoutControlDispatch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlDispatchOrder)).BeginInit();
            this.contextMenuDispatchOrder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDispatchOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlAssignedIncidentNotifications)).BeginInit();
            this.contextMenuStripAssignedIncidentNotif.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewAssignedIncidentNotifications)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlUnits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewUnits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlNewIncidentNotifications)).BeginInit();
            this.contextMenuStripNewIncidentNotification.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewNewIncidentNotifications)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEx2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupIncidentDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroupNotes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupNotes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupUnits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupDispatchOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupAssignedIncidentNotifications)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupNewIncidentNotifications)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBoxDepartment)).BeginInit();
            this.SuspendLayout();
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "MultDepartments.gif");
            this.imageList.Images.SetKeyName(1, "Recomendada.gif");
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 39);
            // 
            // radioButtonAll
            // 
            this.radioButtonAll.Checked = true;
            this.radioButtonAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonAll.Location = new System.Drawing.Point(872, 30);
            this.radioButtonAll.Name = "radioButtonAll";
            this.radioButtonAll.Size = new System.Drawing.Size(404, 21);
            this.radioButtonAll.TabIndex = 4;
            this.radioButtonAll.TabStop = true;
            this.radioButtonAll.Text = "Todas";
            this.radioButtonAll.UseVisualStyleBackColor = true;
            this.radioButtonAll.CheckedChanged += new System.EventHandler(this.radioButtons_CheckedChanged);
            // 
            // contextMenuStripUnits
            // 
            this.contextMenuStripUnits.Enabled = false;
            this.contextMenuStripUnits.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.contextMenuStripUnits.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemAssignUnit,
            this.toolStripMenuItemAvailableUnit,
            this.toolStripMenuItemOutOfOrderUnit,
            this.toolStripMenuItemNotAvailableUnit,
            this.toolStripMenuItemOnSceneUnit,
            this.toolStripMenuItemDelayUnit,
            this.toolStripMenuItemAddNote,
            this.toolStripMenuItemCloseUnit,
            this.toolStripMenuItemLocateUnit,
            this.toolStripMenuItemCallUnit});
            this.contextMenuStripUnits.Name = "contextMenuStripUnits";
            this.contextMenuStripUnits.Size = new System.Drawing.Size(248, 224);
            this.contextMenuStripUnits.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStripUnits_Opening);
            // 
            // toolStripMenuItemAssignUnit
            // 
            this.toolStripMenuItemAssignUnit.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripMenuItemAssignUnit.AutoSize = false;
            this.toolStripMenuItemAssignUnit.Enabled = false;
            this.toolStripMenuItemAssignUnit.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItemAssignUnit.Image")));
            this.toolStripMenuItemAssignUnit.Name = "toolStripMenuItemAssignUnit";
            this.toolStripMenuItemAssignUnit.ShortcutKeyDisplayString = "Ctrl+A";
            this.toolStripMenuItemAssignUnit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.toolStripMenuItemAssignUnit.Size = new System.Drawing.Size(218, 22);
            this.toolStripMenuItemAssignUnit.Click += new System.EventHandler(this.asignarToolStripMenuItem_Click);
            // 
            // toolStripMenuItemAvailableUnit
            // 
            this.toolStripMenuItemAvailableUnit.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripMenuItemAvailableUnit.AutoSize = false;
            this.toolStripMenuItemAvailableUnit.Enabled = false;
            this.toolStripMenuItemAvailableUnit.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItemAvailableUnit.Image")));
            this.toolStripMenuItemAvailableUnit.Name = "toolStripMenuItemAvailableUnit";
            this.toolStripMenuItemAvailableUnit.ShortcutKeyDisplayString = "Ctrl+C";
            this.toolStripMenuItemAvailableUnit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.toolStripMenuItemAvailableUnit.Size = new System.Drawing.Size(218, 22);
            this.toolStripMenuItemAvailableUnit.Click += new System.EventHandler(this.UnitsToolStripMenuItem_Click);
            // 
            // toolStripMenuItemOutOfOrderUnit
            // 
            this.toolStripMenuItemOutOfOrderUnit.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripMenuItemOutOfOrderUnit.AutoSize = false;
            this.toolStripMenuItemOutOfOrderUnit.Enabled = false;
            this.toolStripMenuItemOutOfOrderUnit.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItemOutOfOrderUnit.Image")));
            this.toolStripMenuItemOutOfOrderUnit.Name = "toolStripMenuItemOutOfOrderUnit";
            this.toolStripMenuItemOutOfOrderUnit.ShortcutKeyDisplayString = "Ctrl+X ";
            this.toolStripMenuItemOutOfOrderUnit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.toolStripMenuItemOutOfOrderUnit.Size = new System.Drawing.Size(218, 22);
            this.toolStripMenuItemOutOfOrderUnit.Click += new System.EventHandler(this.UnitsToolStripMenuItem_Click);
            // 
            // toolStripMenuItemNotAvailableUnit
            // 
            this.toolStripMenuItemNotAvailableUnit.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripMenuItemNotAvailableUnit.AutoSize = false;
            this.toolStripMenuItemNotAvailableUnit.Enabled = false;
            this.toolStripMenuItemNotAvailableUnit.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItemNotAvailableUnit.Image")));
            this.toolStripMenuItemNotAvailableUnit.Name = "toolStripMenuItemNotAvailableUnit";
            this.toolStripMenuItemNotAvailableUnit.ShortcutKeyDisplayString = "Ctrl+N";
            this.toolStripMenuItemNotAvailableUnit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.toolStripMenuItemNotAvailableUnit.Size = new System.Drawing.Size(218, 22);
            this.toolStripMenuItemNotAvailableUnit.Click += new System.EventHandler(this.UnitsToolStripMenuItem_Click);
            // 
            // toolStripMenuItemOnSceneUnit
            // 
            this.toolStripMenuItemOnSceneUnit.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripMenuItemOnSceneUnit.AutoSize = false;
            this.toolStripMenuItemOnSceneUnit.Enabled = false;
            this.toolStripMenuItemOnSceneUnit.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItemOnSceneUnit.Image")));
            this.toolStripMenuItemOnSceneUnit.Name = "toolStripMenuItemOnSceneUnit";
            this.toolStripMenuItemOnSceneUnit.ShortcutKeyDisplayString = "Ctrl+E";
            this.toolStripMenuItemOnSceneUnit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.toolStripMenuItemOnSceneUnit.Size = new System.Drawing.Size(218, 22);
            this.toolStripMenuItemOnSceneUnit.Click += new System.EventHandler(this.toolStripMenuItemOnSceneUnit_Click);
            // 
            // toolStripMenuItemDelayUnit
            // 
            this.toolStripMenuItemDelayUnit.AutoSize = false;
            this.toolStripMenuItemDelayUnit.Enabled = false;
            this.toolStripMenuItemDelayUnit.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItemDelayUnit.Image")));
            this.toolStripMenuItemDelayUnit.Name = "toolStripMenuItemDelayUnit";
            this.toolStripMenuItemDelayUnit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.T)));
            this.toolStripMenuItemDelayUnit.Size = new System.Drawing.Size(218, 22);
            this.toolStripMenuItemDelayUnit.Visible = false;
            // 
            // toolStripMenuItemAddNote
            // 
            this.toolStripMenuItemAddNote.AutoSize = false;
            this.toolStripMenuItemAddNote.Enabled = false;
            this.toolStripMenuItemAddNote.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItemAddNote.Image")));
            this.toolStripMenuItemAddNote.Name = "toolStripMenuItemAddNote";
            this.toolStripMenuItemAddNote.ShortcutKeyDisplayString = "Ctrl+I  ";
            this.toolStripMenuItemAddNote.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.I)));
            this.toolStripMenuItemAddNote.Size = new System.Drawing.Size(218, 22);
            this.toolStripMenuItemAddNote.Visible = false;
            // 
            // toolStripMenuItemCloseUnit
            // 
            this.toolStripMenuItemCloseUnit.AutoSize = false;
            this.toolStripMenuItemCloseUnit.Enabled = false;
            this.toolStripMenuItemCloseUnit.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItemCloseUnit.Image")));
            this.toolStripMenuItemCloseUnit.Name = "toolStripMenuItemCloseUnit";
            this.toolStripMenuItemCloseUnit.ShortcutKeyDisplayString = "Ctrl+F";
            this.toolStripMenuItemCloseUnit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this.toolStripMenuItemCloseUnit.Size = new System.Drawing.Size(218, 22);
            this.toolStripMenuItemCloseUnit.Visible = false;
            // 
            // toolStripMenuItemLocateUnit
            // 
            this.toolStripMenuItemLocateUnit.Enabled = false;
            this.toolStripMenuItemLocateUnit.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItemLocateUnit.Image")));
            this.toolStripMenuItemLocateUnit.Name = "toolStripMenuItemLocateUnit";
            this.toolStripMenuItemLocateUnit.ShortcutKeyDisplayString = "Ctrl+U";
            this.toolStripMenuItemLocateUnit.Size = new System.Drawing.Size(247, 22);
            this.toolStripMenuItemLocateUnit.Text = "toolStripMenuItemLocateUnit";
            this.toolStripMenuItemLocateUnit.Click += new System.EventHandler(this.toolStripMenuItemLocateObject_Click);
            // 
            // toolStripMenuItemCallUnit
            // 
            this.toolStripMenuItemCallUnit.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripMenuItemCallUnit.AutoSize = false;
            this.toolStripMenuItemCallUnit.Enabled = false;
            this.toolStripMenuItemCallUnit.Image = global::SmartCadDispatch.Properties.Resources.Call;
            this.toolStripMenuItemCallUnit.Name = "toolStripMenuItemCallUnit";
            this.toolStripMenuItemCallUnit.ShortcutKeyDisplayString = "Ctrl+L";
            this.toolStripMenuItemCallUnit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L)));
            this.toolStripMenuItemCallUnit.Size = new System.Drawing.Size(218, 22);
            this.toolStripMenuItemCallUnit.Click += toolStripMenuItemCallUnit_Click;
            // 
            // labelExShowBy
            // 
            this.labelExShowBy.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExShowBy.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExShowBy.Location = new System.Drawing.Point(648, 30);
            this.labelExShowBy.Name = "labelExShowBy";
            this.labelExShowBy.Size = new System.Drawing.Size(76, 21);
            this.labelExShowBy.TabIndex = 1;
            this.labelExShowBy.Text = "Mostrar por:";
            this.labelExShowBy.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radioButtonZone
            // 
            this.radioButtonZone.Enabled = false;
            this.radioButtonZone.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonZone.Location = new System.Drawing.Point(728, 30);
            this.radioButtonZone.Name = "radioButtonZone";
            this.radioButtonZone.Size = new System.Drawing.Size(61, 21);
            this.radioButtonZone.TabIndex = 2;
            this.radioButtonZone.TabStop = true;
            this.radioButtonZone.Text = "Zona";
            this.radioButtonZone.UseVisualStyleBackColor = true;
            this.radioButtonZone.CheckedChanged += new System.EventHandler(this.radioButtons_CheckedChanged);
            // 
            // buttonReqAttention
            // 
            this.buttonReqAttention.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.buttonReqAttention.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonReqAttention.Appearance.Options.UseFont = true;
            this.buttonReqAttention.Appearance.Options.UseForeColor = true;
            this.buttonReqAttention.FontFormat = FontFormat.GetFormat("ButtonFormat");
            this.buttonReqAttention.Image = ((System.Drawing.Image)(resources.GetObject("buttonReqAttention.Image")));
            this.buttonReqAttention.Location = new System.Drawing.Point(947, 55);
            this.buttonReqAttention.Name = "buttonReqAttention";
            this.buttonReqAttention.Size = new System.Drawing.Size(91, 28);
            this.buttonReqAttention.StyleController = this.layoutControlDispatch;
            this.buttonReqAttention.TabIndex = 11;
            this.buttonReqAttention.Text = "Atencin";
            this.buttonReqAttention.Click += new System.EventHandler(this.buttonReqAttention_Click);
            this.buttonReqAttention.KeyDown += new System.Windows.Forms.KeyEventHandler(this.UnitFilter_KeyDown);
            // 
            // layoutControlDispatch
            // 
            this.layoutControlDispatch.AllowCustomizationMenu = false;
            this.layoutControlDispatch.Appearance.DisabledLayoutGroupCaption.ForeColor = System.Drawing.Color.White;
            this.layoutControlDispatch.Appearance.DisabledLayoutGroupCaption.Options.UseForeColor = true;
            this.layoutControlDispatch.Controls.Add(this.callsAndDispatch);
            this.layoutControlDispatch.Controls.Add(this.buttonAddNotes);
            this.layoutControlDispatch.Controls.Add(this.textBoxIncidentNotes);
            this.layoutControlDispatch.Controls.Add(this.gridControlDispatchOrder);
            this.layoutControlDispatch.Controls.Add(this.blinkingButtonExPendingDispatch);
            this.layoutControlDispatch.Controls.Add(this.textBoxIncidentDetails);
            this.layoutControlDispatch.Controls.Add(this.gridControlAssignedIncidentNotifications);
            this.layoutControlDispatch.Controls.Add(this.blinkingButtonExDispReqAttention);
            this.layoutControlDispatch.Controls.Add(this.radioButtonAll);
            this.layoutControlDispatch.Controls.Add(this.buttonClosedDispatch);
            this.layoutControlDispatch.Controls.Add(this.buttonExAssign);
            this.layoutControlDispatch.Controls.Add(this.buttonActiveDispatch);
            this.layoutControlDispatch.Controls.Add(this.radioButtonDepartment);
            this.layoutControlDispatch.Controls.Add(this.radioButtonZone);
            this.layoutControlDispatch.Controls.Add(this.labelExShowBy);
            this.layoutControlDispatch.Controls.Add(this.buttonReqAttention);
            this.layoutControlDispatch.Controls.Add(this.gridControlUnits);
            this.layoutControlDispatch.Controls.Add(this.buttonExAddNotification);
            this.layoutControlDispatch.Controls.Add(this.buttonAssignedUnits);
            this.layoutControlDispatch.Controls.Add(this.gridControlNewIncidentNotifications);
            this.layoutControlDispatch.Controls.Add(this.buttonAllUnits);
            this.layoutControlDispatch.Controls.Add(this.buttonAvailableUnits);
            this.layoutControlDispatch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlDispatch.Location = new System.Drawing.Point(0, 0);
            this.layoutControlDispatch.LookAndFeel.SkinName = "Blue";
            this.layoutControlDispatch.LookAndFeel.UseDefaultLookAndFeel = false;
            this.layoutControlDispatch.Name = "layoutControlDispatch";
            this.layoutControlDispatch.Root = this.layoutControlGroup1;
            this.layoutControlDispatch.Size = new System.Drawing.Size(1286, 742);
            this.layoutControlDispatch.TabIndex = 15;
            this.layoutControlDispatch.Text = "layoutControl1";
            // 
            // callsAndDispatch
            // 
            this.callsAndDispatch.Location = new System.Drawing.Point(643, 584);
            this.callsAndDispatch.Name = "callsAndDispatch";
            this.callsAndDispatch.Size = new System.Drawing.Size(634, 149);
            this.callsAndDispatch.TabIndex = 17;
            this.callsAndDispatch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DefaultDispatchFormDevX_KeyDown);
            // 
            // buttonAddNotes
            // 
            this.buttonAddNotes.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAddNotes.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonAddNotes.Appearance.Options.UseFont = true;
            this.buttonAddNotes.Appearance.Options.UseForeColor = true;
            this.buttonAddNotes.Enabled = false;
            this.buttonAddNotes.FontFormat = FontFormat.GetFormat("ButtonFormat");
            this.buttonAddNotes.Image = ((System.Drawing.Image)(resources.GetObject("buttonAddNotes.Image")));
            this.buttonAddNotes.Location = new System.Drawing.Point(1245, 548);
            this.buttonAddNotes.Name = "buttonAddNotes";
            this.buttonAddNotes.Size = new System.Drawing.Size(29, 31);
            this.buttonAddNotes.StyleController = this.layoutControlDispatch;
            this.buttonAddNotes.TabIndex = 16;
            this.buttonAddNotes.Click += new System.EventHandler(this.buttonAddNotes_Click);
            this.buttonAddNotes.MouseEnter += new System.EventHandler(this.buttonAddNotes_MouseEnter);
            this.buttonAddNotes.MouseLeave += new System.EventHandler(this.buttonAddNotes_MouseLeave);
            // 
            // textBoxIncidentNotes
            // 
            this.textBoxIncidentNotes.AcceptsReturn = true;
            this.textBoxIncidentNotes.AllowsLetters = true;
            this.textBoxIncidentNotes.AllowsNumbers = true;
            this.textBoxIncidentNotes.AllowsPunctuation = true;
            this.textBoxIncidentNotes.AllowsSeparators = true;
            this.textBoxIncidentNotes.AllowsSymbols = true;
            this.textBoxIncidentNotes.AllowsWhiteSpaces = true;
            this.textBoxIncidentNotes.BackColor = System.Drawing.Color.White;
            this.textBoxIncidentNotes.Enabled = false;
            this.textBoxIncidentNotes.ExtraAllowedChars = "";
            this.textBoxIncidentNotes.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxIncidentNotes.Location = new System.Drawing.Point(646, 548);
            this.textBoxIncidentNotes.MaxLength = 1000;
            this.textBoxIncidentNotes.Multiline = true;
            this.textBoxIncidentNotes.Name = "textBoxIncidentNotes";
            this.textBoxIncidentNotes.NonAllowedCharacters = "";
            this.textBoxIncidentNotes.RegularExpresion = "";
            this.textBoxIncidentNotes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxIncidentNotes.Size = new System.Drawing.Size(598, 31);
            this.textBoxIncidentNotes.TabIndex = 15;
            this.textBoxIncidentNotes.TextChanged += new System.EventHandler(this.textBoxIncidentNotes_TextChanged);
            // 
            // gridControlDispatchOrder
            // 
            this.gridControlDispatchOrder.ContextMenuStrip = this.contextMenuDispatchOrder;
            this.gridControlDispatchOrder.EnableAutoFilter = true;
            this.gridControlDispatchOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlDispatchOrder.Location = new System.Drawing.Point(647, 305);
            this.gridControlDispatchOrder.MainView = this.gridViewDispatchOrder;
            this.gridControlDispatchOrder.Name = "gridControlDispatchOrder";
            this.gridControlDispatchOrder.Size = new System.Drawing.Size(630, 181);
            this.gridControlDispatchOrder.TabIndex = 14;
            this.gridControlDispatchOrder.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewDispatchOrder});
            this.gridControlDispatchOrder.ViewTotalRows = true;
            this.gridControlDispatchOrder.CellToolTipNeeded += new DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventHandler(this.gridControlDispatchOrder_CellToolTipNeeded);
            this.gridControlDispatchOrder.DragDrop += new System.Windows.Forms.DragEventHandler(this.gridControlDispatchOrder_DragDrop);
            this.gridControlDispatchOrder.DragOver += new System.Windows.Forms.DragEventHandler(this.gridControlDispatchOrder_DragOver);
            this.gridControlDispatchOrder.MouseCaptureChanged += new System.EventHandler(this.gridControlDispatchOrder_MouseCaptureChanged);
            // 
            // contextMenuDispatchOrder
            // 
            this.contextMenuDispatchOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.contextMenuDispatchOrder.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.enEscenaToolStripMenuItem,
            this.modifyTimeOrderToolStripMenuItem,
            this.toolStripMenuItemAddNoteToUnit,
            this.cerrarToolStripMenuItem,
            this.toolStripMenuItemLocateUnitDispatch});
            this.contextMenuDispatchOrder.Name = "contextMenuDispatchOrder";
            this.contextMenuDispatchOrder.Size = new System.Drawing.Size(290, 114);
            this.contextMenuDispatchOrder.Text = "Cerrar";
            this.contextMenuDispatchOrder.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuDispatchOrder_Opening);
            // 
            // enEscenaToolStripMenuItem
            // 
            this.enEscenaToolStripMenuItem.Enabled = false;
            this.enEscenaToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("enEscenaToolStripMenuItem.Image")));
            this.enEscenaToolStripMenuItem.Name = "enEscenaToolStripMenuItem";
            this.enEscenaToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+E";
            this.enEscenaToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.enEscenaToolStripMenuItem.Size = new System.Drawing.Size(289, 22);
            this.enEscenaToolStripMenuItem.Click += new System.EventHandler(this.contextDispOrderMenu);
            // 
            // modifyTimeOrderToolStripMenuItem
            // 
            this.modifyTimeOrderToolStripMenuItem.Enabled = false;
            this.modifyTimeOrderToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("modifyTimeOrderToolStripMenuItem.Image")));
            this.modifyTimeOrderToolStripMenuItem.Name = "modifyTimeOrderToolStripMenuItem";
            this.modifyTimeOrderToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+T";
            this.modifyTimeOrderToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.T)));
            this.modifyTimeOrderToolStripMenuItem.Size = new System.Drawing.Size(289, 22);
            this.modifyTimeOrderToolStripMenuItem.Click += new System.EventHandler(this.contextDispOrderMenu);
            // 
            // toolStripMenuItemAddNoteToUnit
            // 
            this.toolStripMenuItemAddNoteToUnit.Enabled = false;
            this.toolStripMenuItemAddNoteToUnit.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItemAddNoteToUnit.Image")));
            this.toolStripMenuItemAddNoteToUnit.Name = "toolStripMenuItemAddNoteToUnit";
            this.toolStripMenuItemAddNoteToUnit.ShortcutKeyDisplayString = "Ctrl+I  ";
            this.toolStripMenuItemAddNoteToUnit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.I)));
            this.toolStripMenuItemAddNoteToUnit.Size = new System.Drawing.Size(289, 22);
            this.toolStripMenuItemAddNoteToUnit.Click += new System.EventHandler(this.AddNoteToAssignedUnit);
            // 
            // cerrarToolStripMenuItem
            // 
            this.cerrarToolStripMenuItem.Enabled = false;
            this.cerrarToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("cerrarToolStripMenuItem.Image")));
            this.cerrarToolStripMenuItem.Name = "cerrarToolStripMenuItem";
            this.cerrarToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+F";
            this.cerrarToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this.cerrarToolStripMenuItem.Size = new System.Drawing.Size(289, 22);
            this.cerrarToolStripMenuItem.Click += new System.EventHandler(this.contextDispOrderMenu);
            // 
            // toolStripMenuItemLocateUnitDispatch
            // 
            this.toolStripMenuItemLocateUnitDispatch.Enabled = false;
            this.toolStripMenuItemLocateUnitDispatch.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItemLocateUnitDispatch.Image")));
            this.toolStripMenuItemLocateUnitDispatch.Name = "toolStripMenuItemLocateUnitDispatch";
            this.toolStripMenuItemLocateUnitDispatch.ShortcutKeyDisplayString = "Ctrl+U";
            this.toolStripMenuItemLocateUnitDispatch.Size = new System.Drawing.Size(289, 22);
            this.toolStripMenuItemLocateUnitDispatch.Text = "toolStripMenuItemLocateUnitDispatch";
            this.toolStripMenuItemLocateUnitDispatch.Click += new System.EventHandler(this.toolStripMenuItemLocateObject_Click);
            // 
            // gridViewDispatchOrder
            // 
            this.gridViewDispatchOrder.AllowFocusedRowChanged = true;
            this.gridViewDispatchOrder.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewDispatchOrder.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewDispatchOrder.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewDispatchOrder.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewDispatchOrder.Appearance.FooterPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewDispatchOrder.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewDispatchOrder.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewDispatchOrder.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewDispatchOrder.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewDispatchOrder.EnablePreviewLineForFocusedRow = false;
            this.gridViewDispatchOrder.GridControl = this.gridControlDispatchOrder;
            this.gridViewDispatchOrder.Name = "gridViewDispatchOrder";
            this.gridViewDispatchOrder.OptionsHint.ShowCellHints = false;
            this.gridViewDispatchOrder.OptionsMenu.EnableFooterMenu = false;
            this.gridViewDispatchOrder.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewDispatchOrder.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewDispatchOrder.OptionsView.ShowFooter = true;
            this.gridViewDispatchOrder.ViewTotalRows = true;
            this.gridViewDispatchOrder.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.gridView_RowStyle);
            this.gridViewDispatchOrder.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridViewDispatchOrder_SelectionChanged);
            this.gridViewDispatchOrder.ColumnFilterChanged += new System.EventHandler(this.gridViewDispatchOrder_ColumnFilterChanged);
            this.gridViewDispatchOrder.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridViewDispatchOrder_KeyDown);
            this.gridViewDispatchOrder.MouseMove += new System.Windows.Forms.MouseEventHandler(this.gridViewDispatchOrder_MouseMove);
            this.gridViewDispatchOrder.LostFocus += new System.EventHandler(this.GridView_LostFocus);
            this.gridViewDispatchOrder.GotFocus += new System.EventHandler(this.GridView_GotFocus);
            // 
            // blinkingButtonExPendingDispatch
            // 
            this.blinkingButtonExPendingDispatch.Appearance.BackColor = System.Drawing.Color.Gold;
            this.blinkingButtonExPendingDispatch.Appearance.BackColor2 = System.Drawing.Color.Yellow;
            this.blinkingButtonExPendingDispatch.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.blinkingButtonExPendingDispatch.Appearance.Options.UseBackColor = true;
            this.blinkingButtonExPendingDispatch.Appearance.Options.UseFont = true;
            this.blinkingButtonExPendingDispatch.Blinking = false;
            this.blinkingButtonExPendingDispatch.Image = ((System.Drawing.Image)(resources.GetObject("blinkingButtonExPendingDispatch.Image")));
            this.blinkingButtonExPendingDispatch.Location = new System.Drawing.Point(309, 306);
            this.blinkingButtonExPendingDispatch.Name = "blinkingButtonExPendingDispatch";
            this.blinkingButtonExPendingDispatch.Size = new System.Drawing.Size(91, 28);
            this.blinkingButtonExPendingDispatch.StyleController = this.layoutControlDispatch;
            this.blinkingButtonExPendingDispatch.TabIndex = 5;
            this.blinkingButtonExPendingDispatch.Click += new System.EventHandler(this.blinkingButtonExPendingDispatch_Click);
            this.blinkingButtonExPendingDispatch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.AssignedNotificationFilter_KeyDown);
            // 
            // textBoxIncidentDetails
            // 
            this.textBoxIncidentDetails.AllowWebBrowserDrop = true;
            this.textBoxIncidentDetails.DocumentText = "<HTML></HTML>\0";
            this.textBoxIncidentDetails.IsWebBrowserContextMenuEnabled = true;
            this.textBoxIncidentDetails.Location = new System.Drawing.Point(10, 526);
            this.textBoxIncidentDetails.Margin = new System.Windows.Forms.Padding(0);
            this.textBoxIncidentDetails.Name = "textBoxIncidentDetails";
            this.textBoxIncidentDetails.Size = new System.Drawing.Size(630, 206);
            this.textBoxIncidentDetails.TabIndex = 7;
            this.textBoxIncidentDetails.WebBrowserShortcutsEnabled = true;
            this.textBoxIncidentDetails.XmlText = "<HTML></HTML>\0";
            this.textBoxIncidentDetails.KeyDownEx += new System.Windows.Forms.KeyEventHandler(this.textBoxIncidentDetails_KeyDownEx);
            this.textBoxIncidentDetails.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.ControlsTabPressed);
            // 
            // gridControlAssignedIncidentNotifications
            // 
            this.gridControlAssignedIncidentNotifications.ContextMenuStrip = this.contextMenuStripAssignedIncidentNotif;
            this.gridControlAssignedIncidentNotifications.EnableAutoFilter = true;
            this.gridControlAssignedIncidentNotifications.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlAssignedIncidentNotifications.Location = new System.Drawing.Point(9, 337);
            this.gridControlAssignedIncidentNotifications.LookAndFeel.SkinName = "Blue";
            this.gridControlAssignedIncidentNotifications.MainView = this.gridViewAssignedIncidentNotifications;
            this.gridControlAssignedIncidentNotifications.Name = "gridControlAssignedIncidentNotifications";
            this.gridControlAssignedIncidentNotifications.Size = new System.Drawing.Size(626, 149);
            this.gridControlAssignedIncidentNotifications.TabIndex = 6;
            this.gridControlAssignedIncidentNotifications.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewAssignedIncidentNotifications});
            this.gridControlAssignedIncidentNotifications.ViewTotalRows = true;
            this.gridControlAssignedIncidentNotifications.DragDrop += new System.Windows.Forms.DragEventHandler(this.gridControlAssignedIncidentNotifications_DragDrop);
            this.gridControlAssignedIncidentNotifications.DragOver += new System.Windows.Forms.DragEventHandler(this.gridControlAssignedIncidentNotifications_DragOver);
            // 
            // contextMenuStripAssignedIncidentNotif
            // 
            this.contextMenuStripAssignedIncidentNotif.Enabled = false;
            this.contextMenuStripAssignedIncidentNotif.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.contextMenuStripAssignedIncidentNotif.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemRefreshIncidentNotitficationDetails,
            this.toolStripMenuItemPending,
            this.toolStripMenuItemModifyZone,
            this.toolStripMenuItemSupportRequest,
            this.toolStripMenuItemCloseIncNotif,
            this.toolStripMenuItemLocateAssignedIncNot,
            this.toolStripMenuItemRecommendedUnits,
            this.toolStripMenuItemAssignedUnits,
            this.toolStripMenuItemCallStation});
            this.contextMenuStripAssignedIncidentNotif.Name = "contextMenuStrip1";
            this.contextMenuStripAssignedIncidentNotif.Size = new System.Drawing.Size(304, 202);
            this.contextMenuStripAssignedIncidentNotif.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStripAssignedIncidentNotif_Opening);
            // 
            // toolStripMenuItemRefreshIncidentNotitficationDetails
            // 
            this.toolStripMenuItemRefreshIncidentNotitficationDetails.Enabled = false;
            this.toolStripMenuItemRefreshIncidentNotitficationDetails.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItemRefreshIncidentNotitficationDetails.Image")));
            this.toolStripMenuItemRefreshIncidentNotitficationDetails.Name = "toolStripMenuItemRefreshIncidentNotitficationDetails";
            this.toolStripMenuItemRefreshIncidentNotitficationDetails.ShortcutKeyDisplayString = "F5";
            this.toolStripMenuItemRefreshIncidentNotitficationDetails.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.toolStripMenuItemRefreshIncidentNotitficationDetails.Size = new System.Drawing.Size(303, 22);
            this.toolStripMenuItemRefreshIncidentNotitficationDetails.Click += new System.EventHandler(this.toolStripMenuItemRefreshIncidentNotitficationDetails_Click);
            // 
            // toolStripMenuItemPending
            // 
            this.toolStripMenuItemPending.Enabled = false;
            this.toolStripMenuItemPending.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItemPending.Image")));
            this.toolStripMenuItemPending.Name = "toolStripMenuItemPending";
            this.toolStripMenuItemPending.ShortcutKeys = System.Windows.Forms.Keys.F9;
            this.toolStripMenuItemPending.Size = new System.Drawing.Size(303, 22);
            this.toolStripMenuItemPending.Click += new System.EventHandler(this.toolStripMenuItemPending_Click);
            // 
            // toolStripMenuItemModifyZone
            // 
            this.toolStripMenuItemModifyZone.Enabled = false;
            this.toolStripMenuItemModifyZone.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItemModifyZone.Image")));
            this.toolStripMenuItemModifyZone.Name = "toolStripMenuItemModifyZone";
            this.toolStripMenuItemModifyZone.ShortcutKeys = System.Windows.Forms.Keys.F6;
            this.toolStripMenuItemModifyZone.Size = new System.Drawing.Size(303, 22);
            this.toolStripMenuItemModifyZone.Click += new System.EventHandler(this.toolStripMenuItemModifyZone_Click);
            // 
            // toolStripMenuItemSupportRequest
            // 
            this.toolStripMenuItemSupportRequest.Enabled = false;
            this.toolStripMenuItemSupportRequest.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItemSupportRequest.Image")));
            this.toolStripMenuItemSupportRequest.Name = "toolStripMenuItemSupportRequest";
            this.toolStripMenuItemSupportRequest.ShortcutKeys = System.Windows.Forms.Keys.F8;
            this.toolStripMenuItemSupportRequest.Size = new System.Drawing.Size(303, 22);
            this.toolStripMenuItemSupportRequest.Text = "Solicitar apoyo";
            this.toolStripMenuItemSupportRequest.Click += new System.EventHandler(this.toolStripMenuItemSupportRequest_Click);
            // 
            // toolStripMenuItemCloseIncNotif
            // 
            this.toolStripMenuItemCloseIncNotif.Enabled = false;
            this.toolStripMenuItemCloseIncNotif.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItemCloseIncNotif.Image")));
            this.toolStripMenuItemCloseIncNotif.Name = "toolStripMenuItemCloseIncNotif";
            this.toolStripMenuItemCloseIncNotif.ShortcutKeys = System.Windows.Forms.Keys.F12;
            this.toolStripMenuItemCloseIncNotif.Size = new System.Drawing.Size(303, 22);
            this.toolStripMenuItemCloseIncNotif.Text = "Cerrar";
            this.toolStripMenuItemCloseIncNotif.Click += new System.EventHandler(this.toolStripMenuItemCloseIncNotif_Click);
            // 
            // toolStripMenuItemLocateAssignedIncNot
            // 
            this.toolStripMenuItemLocateAssignedIncNot.Enabled = false;
            this.toolStripMenuItemLocateAssignedIncNot.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItemLocateAssignedIncNot.Image")));
            this.toolStripMenuItemLocateAssignedIncNot.Name = "toolStripMenuItemLocateAssignedIncNot";
            this.toolStripMenuItemLocateAssignedIncNot.ShortcutKeyDisplayString = "Ctrl+U";
            this.toolStripMenuItemLocateAssignedIncNot.Size = new System.Drawing.Size(303, 22);
            this.toolStripMenuItemLocateAssignedIncNot.Text = "toolStripMenuItemLocateAssignedIncNot";
            this.toolStripMenuItemLocateAssignedIncNot.Click += new System.EventHandler(this.toolStripMenuItemLocateObject_Click);
            // 
            // toolStripMenuItemRecommendedUnits
            // 
            this.toolStripMenuItemRecommendedUnits.Enabled = false;
            this.toolStripMenuItemRecommendedUnits.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItemRecommendedUnits.Image")));
            this.toolStripMenuItemRecommendedUnits.Name = "toolStripMenuItemRecommendedUnits";
            this.toolStripMenuItemRecommendedUnits.ShortcutKeyDisplayString = "Ctrl+R";
            this.toolStripMenuItemRecommendedUnits.Size = new System.Drawing.Size(303, 22);
            this.toolStripMenuItemRecommendedUnits.Text = "toolStripMenuItemRecommendedUnits";
            this.toolStripMenuItemRecommendedUnits.Click += new System.EventHandler(this.toolStripMenuItemRecommendedUnits_Click);
            // 
            // toolStripMenuItemAssignedUnits
            // 
            this.toolStripMenuItemAssignedUnits.Enabled = false;
            this.toolStripMenuItemAssignedUnits.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItemAssignedUnits.Image")));
            this.toolStripMenuItemAssignedUnits.Name = "toolStripMenuItemAssignedUnits";
            this.toolStripMenuItemAssignedUnits.ShortcutKeyDisplayString = "Ctrl+S";
            this.toolStripMenuItemAssignedUnits.Size = new System.Drawing.Size(303, 22);
            this.toolStripMenuItemAssignedUnits.Text = "toolStripMenuItemAssignedUnits";
            this.toolStripMenuItemAssignedUnits.Click += new System.EventHandler(this.toolStripMenuItemAssignedUnits_Click);
            // 
            // toolStripMenuItemCallStation
            // 
            this.toolStripMenuItemCallStation.Enabled = true;
            this.toolStripMenuItemCallStation.Image = global::SmartCadDispatch.Properties.Resources.Call;
            this.toolStripMenuItemCallStation.Name = "toolStripMenuItemCallStation";
            this.toolStripMenuItemCallStation.ShortcutKeyDisplayString = "Ctrl+L";
            this.toolStripMenuItemCallStation.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L)));
            this.toolStripMenuItemCallStation.Size = new System.Drawing.Size(303, 22);
            this.toolStripMenuItemCallStation.Click += toolStripMenuItemCallStation_Click;
            // 
            // gridViewAssignedIncidentNotifications
            // 
            this.gridViewAssignedIncidentNotifications.AllowFocusedRowChanged = true;
            this.gridViewAssignedIncidentNotifications.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewAssignedIncidentNotifications.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewAssignedIncidentNotifications.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewAssignedIncidentNotifications.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewAssignedIncidentNotifications.Appearance.FooterPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewAssignedIncidentNotifications.Appearance.FooterPanel.Options.UseFont = true;
            this.gridViewAssignedIncidentNotifications.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewAssignedIncidentNotifications.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewAssignedIncidentNotifications.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewAssignedIncidentNotifications.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewAssignedIncidentNotifications.EnablePreviewLineForFocusedRow = false;
            this.gridViewAssignedIncidentNotifications.GridControl = this.gridControlAssignedIncidentNotifications;
            this.gridViewAssignedIncidentNotifications.Name = "gridViewAssignedIncidentNotifications";
            this.gridViewAssignedIncidentNotifications.OptionsMenu.EnableFooterMenu = false;
            this.gridViewAssignedIncidentNotifications.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewAssignedIncidentNotifications.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewAssignedIncidentNotifications.OptionsView.ShowFooter = true;
            this.gridViewAssignedIncidentNotifications.ViewTotalRows = true;
            this.gridViewAssignedIncidentNotifications.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.gridView_RowStyle);
            this.gridViewAssignedIncidentNotifications.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridViewAssignedIncidentNotifications_KeyDown);
            this.gridViewAssignedIncidentNotifications.LostFocus += new System.EventHandler(this.GridView_LostFocus);
            this.gridViewAssignedIncidentNotifications.GotFocus += new System.EventHandler(this.GridView_GotFocus2);
            // 
            // blinkingButtonExDispReqAttention
            // 
            this.blinkingButtonExDispReqAttention.Appearance.BackColor = System.Drawing.Color.Gold;
            this.blinkingButtonExDispReqAttention.Appearance.BackColor2 = System.Drawing.Color.Yellow;
            this.blinkingButtonExDispReqAttention.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.blinkingButtonExDispReqAttention.Appearance.Options.UseBackColor = true;
            this.blinkingButtonExDispReqAttention.Appearance.Options.UseFont = true;
            this.blinkingButtonExDispReqAttention.Blinking = false;
            this.blinkingButtonExDispReqAttention.Image = ((System.Drawing.Image)(resources.GetObject("blinkingButtonExDispReqAttention.Image")));
            this.blinkingButtonExDispReqAttention.Location = new System.Drawing.Point(210, 306);
            this.blinkingButtonExDispReqAttention.Name = "blinkingButtonExDispReqAttention";
            this.blinkingButtonExDispReqAttention.Size = new System.Drawing.Size(91, 28);
            this.blinkingButtonExDispReqAttention.StyleController = this.layoutControlDispatch;
            this.blinkingButtonExDispReqAttention.TabIndex = 4;
            this.blinkingButtonExDispReqAttention.Text = "Atencin";
            this.blinkingButtonExDispReqAttention.Click += new System.EventHandler(this.buttonDispReqAttention_Click);
            this.blinkingButtonExDispReqAttention.KeyDown += new System.Windows.Forms.KeyEventHandler(this.AssignedNotificationFilter_KeyDown);
            // 
            // buttonClosedDispatch
            // 
            this.buttonClosedDispatch.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.buttonClosedDispatch.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonClosedDispatch.Appearance.Options.UseFont = true;
            this.buttonClosedDispatch.Appearance.Options.UseForeColor = true;
            this.buttonClosedDispatch.FontFormat = FontFormat.GetFormat("ButtonFormat");
            this.buttonClosedDispatch.Image = ((System.Drawing.Image)(resources.GetObject("buttonClosedDispatch.Image")));
            this.buttonClosedDispatch.Location = new System.Drawing.Point(111, 306);
            this.buttonClosedDispatch.Name = "buttonClosedDispatch";
            this.buttonClosedDispatch.Size = new System.Drawing.Size(91, 28);
            this.buttonClosedDispatch.StyleController = this.layoutControlDispatch;
            this.buttonClosedDispatch.TabIndex = 3;
            this.buttonClosedDispatch.Text = "Cerradas";
            this.buttonClosedDispatch.Click += new System.EventHandler(this.buttonClosedDispatch_Click);
            this.buttonClosedDispatch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.AssignedNotificationFilter_KeyDown);
            // 
            // buttonExAssign
            // 
            this.buttonExAssign.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.buttonExAssign.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonExAssign.Appearance.Options.UseFont = true;
            this.buttonExAssign.Appearance.Options.UseForeColor = true;
            this.buttonExAssign.Enabled = false;
            this.buttonExAssign.FontFormat = FontFormat.GetFormat("ButtonFormat");
            this.buttonExAssign.Image = ((System.Drawing.Image)(resources.GetObject("buttonExAssign.Image")));
            this.buttonExAssign.Location = new System.Drawing.Point(1190, 244);
            this.buttonExAssign.Name = "buttonExAssign";
            this.buttonExAssign.Padding = new System.Windows.Forms.Padding(2, 0, 4, 0);
            this.buttonExAssign.Size = new System.Drawing.Size(86, 28);
            this.buttonExAssign.StyleController = this.layoutControlDispatch;
            this.buttonExAssign.TabIndex = 13;
            this.buttonExAssign.Text = "Asignar";
            this.buttonExAssign.Click += new System.EventHandler(this.buttonExAssign_Click);
            // 
            // buttonActiveDispatch
            // 
            this.buttonActiveDispatch.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.buttonActiveDispatch.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonActiveDispatch.Appearance.Options.UseFont = true;
            this.buttonActiveDispatch.Appearance.Options.UseForeColor = true;
            this.buttonActiveDispatch.FontFormat = FontFormat.GetFormat("ButtonFormat");
            this.buttonActiveDispatch.Image = ((System.Drawing.Image)(resources.GetObject("buttonActiveDispatch.Image")));
            this.buttonActiveDispatch.Location = new System.Drawing.Point(12, 306);
            this.buttonActiveDispatch.Name = "buttonActiveDispatch";
            this.buttonActiveDispatch.Size = new System.Drawing.Size(91, 28);
            this.buttonActiveDispatch.StyleController = this.layoutControlDispatch;
            this.buttonActiveDispatch.TabIndex = 2;
            this.buttonActiveDispatch.Text = "Activas";
            this.buttonActiveDispatch.Click += new System.EventHandler(this.buttonActiveDispatch_Click);
            this.buttonActiveDispatch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.AssignedNotificationFilter_KeyDown);
            // 
            // radioButtonDepartment
            // 
            this.radioButtonDepartment.Enabled = false;
            this.radioButtonDepartment.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonDepartment.Location = new System.Drawing.Point(793, 30);
            this.radioButtonDepartment.Name = "radioButtonDepartment";
            this.radioButtonDepartment.Size = new System.Drawing.Size(75, 21);
            this.radioButtonDepartment.TabIndex = 3;
            this.radioButtonDepartment.TabStop = true;
            this.radioButtonDepartment.Text = "Estacin";
            this.radioButtonDepartment.UseVisualStyleBackColor = true;
            this.radioButtonDepartment.CheckedChanged += new System.EventHandler(this.radioButtons_CheckedChanged);
            // 
            // gridControlUnits
            // 
            this.gridControlUnits.ContextMenuStrip = this.contextMenuStripUnits;
            this.gridControlUnits.EnableAutoFilter = true;
            this.gridControlUnits.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlUnits.Location = new System.Drawing.Point(647, 86);
            this.gridControlUnits.MainView = this.gridViewUnits;
            this.gridControlUnits.Name = "gridControlUnits";
            this.gridControlUnits.Size = new System.Drawing.Size(630, 155);
            this.gridControlUnits.TabIndex = 12;
            this.gridControlUnits.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewUnits});
            this.gridControlUnits.ViewTotalRows = true;
            this.gridControlUnits.CellToolTipNeeded += new DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventHandler(this.gridControlUnits_CellToolTipNeeded);
            this.gridControlUnits.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.gridControlUnits_MouseDoubleClick);
            // 
            // gridViewUnits
            // 
            this.gridViewUnits.AllowFocusedRowChanged = true;
            this.gridViewUnits.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewUnits.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewUnits.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewUnits.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewUnits.Appearance.FooterPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewUnits.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewUnits.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewUnits.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewUnits.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewUnits.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewUnits.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridViewUnits.EnablePreviewLineForFocusedRow = false;
            this.gridViewUnits.GridControl = this.gridControlUnits;
            this.gridViewUnits.Name = "gridViewUnits";
            this.gridViewUnits.OptionsHint.ShowCellHints = false;
            this.gridViewUnits.OptionsMenu.EnableFooterMenu = false;
            this.gridViewUnits.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewUnits.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewUnits.OptionsView.ShowFooter = true;
            this.gridViewUnits.ViewTotalRows = true;
            this.gridViewUnits.SingleSelectionChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewUnits_FocusedRow);
            this.gridViewUnits.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.gridView_RowStyle);
            this.gridViewUnits.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridViewUnits_SelectionChanged);
            this.gridViewUnits.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewUnits_FocusedRow);
            this.gridViewUnits.ColumnFilterChanged += new System.EventHandler(this.gridViewUnits_ColumnFilterChanged);
            this.gridViewUnits.FocusedRowLoaded += new DevExpress.XtraGrid.Views.Base.RowEventHandler(this.gridViewUnits_FocusedRow);
            this.gridViewUnits.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridViewUnits_KeyDown);
            this.gridViewUnits.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridViewUnits_MouseDown);
            this.gridViewUnits.MouseMove += new System.Windows.Forms.MouseEventHandler(this.gridViewUnits_MouseMove);
            this.gridViewUnits.LostFocus += new System.EventHandler(this.GridView_LostFocus);
            this.gridViewUnits.GotFocus += new System.EventHandler(this.GridView_GotFocus);
            // 
            // buttonExAddNotification
            // 
            this.buttonExAddNotification.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.buttonExAddNotification.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonExAddNotification.Appearance.Options.UseFont = true;
            this.buttonExAddNotification.Appearance.Options.UseForeColor = true;
            this.buttonExAddNotification.Enabled = false;
            this.buttonExAddNotification.FontFormat = FontFormat.GetFormat("ButtonFormat");
            this.buttonExAddNotification.Image = ((System.Drawing.Image)(resources.GetObject("buttonExAddNotification.Image")));
            this.buttonExAddNotification.Location = new System.Drawing.Point(548, 244);
            this.buttonExAddNotification.Name = "buttonExAddNotification";
            this.buttonExAddNotification.Padding = new System.Windows.Forms.Padding(2, 0, 4, 0);
            this.buttonExAddNotification.Size = new System.Drawing.Size(86, 28);
            this.buttonExAddNotification.StyleController = this.layoutControlDispatch;
            this.buttonExAddNotification.TabIndex = 1;
            this.buttonExAddNotification.Text = "Agregar";
            this.buttonExAddNotification.Click += new System.EventHandler(this.buttonExAddNotification_Click);
            // 
            // buttonAssignedUnits
            // 
            this.buttonAssignedUnits.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.buttonAssignedUnits.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonAssignedUnits.Appearance.Options.UseFont = true;
            this.buttonAssignedUnits.Appearance.Options.UseForeColor = true;
            this.buttonAssignedUnits.FontFormat = FontFormat.GetFormat("ButtonFormat");
            this.buttonAssignedUnits.Image = ((System.Drawing.Image)(resources.GetObject("buttonAssignedUnits.Image")));
            this.buttonAssignedUnits.Location = new System.Drawing.Point(848, 55);
            this.buttonAssignedUnits.Name = "buttonAssignedUnits";
            this.buttonAssignedUnits.Size = new System.Drawing.Size(91, 28);
            this.buttonAssignedUnits.StyleController = this.layoutControlDispatch;
            this.buttonAssignedUnits.TabIndex = 10;
            this.buttonAssignedUnits.Text = "Asignadas";
            this.buttonAssignedUnits.Click += new System.EventHandler(this.buttonAssignedUnits_Click);
            this.buttonAssignedUnits.KeyDown += new System.Windows.Forms.KeyEventHandler(this.UnitFilter_KeyDown);
            // 
            // gridControlNewIncidentNotifications
            // 
            this.gridControlNewIncidentNotifications.ContextMenuStrip = this.contextMenuStripNewIncidentNotification;
            this.gridControlNewIncidentNotifications.EnableAutoFilter = true;
            this.gridControlNewIncidentNotifications.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlNewIncidentNotifications.Location = new System.Drawing.Point(8, 28);
            this.gridControlNewIncidentNotifications.MainView = this.gridViewNewIncidentNotifications;
            this.gridControlNewIncidentNotifications.Name = "gridControlNewIncidentNotifications";
            this.gridControlNewIncidentNotifications.Size = new System.Drawing.Size(628, 214);
            this.gridControlNewIncidentNotifications.TabIndex = 0;
            this.gridControlNewIncidentNotifications.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewNewIncidentNotifications,
            this.gridViewEx2});
            this.gridControlNewIncidentNotifications.ViewTotalRows = true;
            this.gridControlNewIncidentNotifications.CellToolTipNeeded += new DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventHandler(this.gridControlNewIncidentNotifications_CellToolTipNeeded);
            this.gridControlNewIncidentNotifications.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.gridControlNewIncidentNotifications_MouseDoubleClick);
            // 
            // contextMenuStripNewIncidentNotification
            // 
            this.contextMenuStripNewIncidentNotification.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.contextMenuStripNewIncidentNotification.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemAssign,
            this.toolStripMenuItemRefreshNewIncidentNotificationDetails,
            this.toolStripMenuItemLocateNewIncNot});
            this.contextMenuStripNewIncidentNotification.Name = "contextMenuStripNewIncidentNotification";
            this.contextMenuStripNewIncidentNotification.Size = new System.Drawing.Size(260, 70);
            this.contextMenuStripNewIncidentNotification.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStripNewIncidentNotification_Opening);
            // 
            // toolStripMenuItemAssign
            // 
            this.toolStripMenuItemAssign.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItemAssign.Image")));
            this.toolStripMenuItemAssign.Name = "toolStripMenuItemAssign";
            this.toolStripMenuItemAssign.ShortcutKeyDisplayString = "F2";
            this.toolStripMenuItemAssign.ShortcutKeys = System.Windows.Forms.Keys.F2;
            this.toolStripMenuItemAssign.Size = new System.Drawing.Size(259, 22);
            this.toolStripMenuItemAssign.Click += new System.EventHandler(this.toolStripMenuItemAssign_Click);
            // 
            // toolStripMenuItemRefreshNewIncidentNotificationDetails
            // 
            this.toolStripMenuItemRefreshNewIncidentNotificationDetails.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItemRefreshNewIncidentNotificationDetails.Image")));
            this.toolStripMenuItemRefreshNewIncidentNotificationDetails.Name = "toolStripMenuItemRefreshNewIncidentNotificationDetails";
            this.toolStripMenuItemRefreshNewIncidentNotificationDetails.ShortcutKeyDisplayString = "F5";
            this.toolStripMenuItemRefreshNewIncidentNotificationDetails.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.toolStripMenuItemRefreshNewIncidentNotificationDetails.Size = new System.Drawing.Size(259, 22);
            this.toolStripMenuItemRefreshNewIncidentNotificationDetails.Click += new System.EventHandler(this.toolStripMenuItemRefreshNewIncidentNotificationDetails_Click);
            // 
            // toolStripMenuItemLocateNewIncNot
            // 
            this.toolStripMenuItemLocateNewIncNot.Enabled = false;
            this.toolStripMenuItemLocateNewIncNot.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItemLocateNewIncNot.Image")));
            this.toolStripMenuItemLocateNewIncNot.Name = "toolStripMenuItemLocateNewIncNot";
            this.toolStripMenuItemLocateNewIncNot.ShortcutKeyDisplayString = "Ctrl+U";
            this.toolStripMenuItemLocateNewIncNot.Size = new System.Drawing.Size(259, 22);
            this.toolStripMenuItemLocateNewIncNot.Text = "toolStripMenuItemLocateObject";
            this.toolStripMenuItemLocateNewIncNot.Click += new System.EventHandler(this.toolStripMenuItemLocateObject_Click);
            // 
            // gridViewNewIncidentNotifications
            // 
            this.gridViewNewIncidentNotifications.AllowFocusedRowChanged = true;
            this.gridViewNewIncidentNotifications.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewNewIncidentNotifications.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewNewIncidentNotifications.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewNewIncidentNotifications.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewNewIncidentNotifications.Appearance.FooterPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridViewNewIncidentNotifications.Appearance.HeaderPanel.Image = ((System.Drawing.Image)(resources.GetObject("gridViewNewIncidentNotifications.Appearance.HeaderPanel.Image")));
            this.gridViewNewIncidentNotifications.Appearance.HeaderPanel.Options.UseImage = true;
            this.gridViewNewIncidentNotifications.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewNewIncidentNotifications.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewNewIncidentNotifications.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewNewIncidentNotifications.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewNewIncidentNotifications.EnablePreviewLineForFocusedRow = false;
            this.gridViewNewIncidentNotifications.GridControl = this.gridControlNewIncidentNotifications;
            this.gridViewNewIncidentNotifications.Images = this.imageList;
            this.gridViewNewIncidentNotifications.Name = "gridViewNewIncidentNotifications";
            this.gridViewNewIncidentNotifications.OptionsHint.ShowCellHints = false;
            this.gridViewNewIncidentNotifications.OptionsMenu.EnableFooterMenu = false;
            this.gridViewNewIncidentNotifications.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewNewIncidentNotifications.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewNewIncidentNotifications.OptionsView.ShowFooter = true;
            this.gridViewNewIncidentNotifications.ViewTotalRows = true;
            this.gridViewNewIncidentNotifications.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.gridViewNewIncidentNotifications_InitNewRow);
            this.gridViewNewIncidentNotifications.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridViewNewIncidentNotifications_KeyDown);
            this.gridViewNewIncidentNotifications.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.gridViewNewIncidentNotifications_KeyPress);
            this.gridViewNewIncidentNotifications.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridViewNewIncidentNotifications_MouseDown);
            this.gridViewNewIncidentNotifications.MouseMove += new System.Windows.Forms.MouseEventHandler(this.gridViewNewIncidentNotifications_MouseMove);
            this.gridViewNewIncidentNotifications.LostFocus += new System.EventHandler(this.GridView_LostFocus);
            this.gridViewNewIncidentNotifications.GotFocus += new System.EventHandler(this.GridView_GotFocus);
            // 
            // gridViewEx2
            // 
            this.gridViewEx2.AllowFocusedRowChanged = true;
            this.gridViewEx2.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewEx2.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewEx2.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewEx2.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewEx2.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewEx2.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewEx2.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewEx2.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewEx2.EnablePreviewLineForFocusedRow = false;
            this.gridViewEx2.GridControl = this.gridControlNewIncidentNotifications;
            this.gridViewEx2.Name = "gridViewEx2";
            this.gridViewEx2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewEx2.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewEx2.ViewTotalRows = false;
            // 
            // buttonAllUnits
            // 
            this.buttonAllUnits.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.buttonAllUnits.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonAllUnits.Appearance.Options.UseFont = true;
            this.buttonAllUnits.Appearance.Options.UseForeColor = true;
            this.buttonAllUnits.FontFormat = FontFormat.GetFormat("ButtonFormat");
            this.buttonAllUnits.Image = ((System.Drawing.Image)(resources.GetObject("buttonAllUnits.Image")));
            this.buttonAllUnits.Location = new System.Drawing.Point(650, 55);
            this.buttonAllUnits.Name = "buttonAllUnits";
            this.buttonAllUnits.Size = new System.Drawing.Size(91, 28);
            this.buttonAllUnits.StyleController = this.layoutControlDispatch;
            this.buttonAllUnits.TabIndex = 8;
            this.buttonAllUnits.Text = "Todas";
            this.buttonAllUnits.Click += new System.EventHandler(this.buttonAllUnits_Click);
            this.buttonAllUnits.KeyDown += new System.Windows.Forms.KeyEventHandler(this.UnitFilter_KeyDown);
            // 
            // buttonAvailableUnits
            // 
            this.buttonAvailableUnits.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.buttonAvailableUnits.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonAvailableUnits.Appearance.Options.UseFont = true;
            this.buttonAvailableUnits.Appearance.Options.UseForeColor = true;
            this.buttonAvailableUnits.FontFormat = FontFormat.GetFormat("ButtonFormat");
            this.buttonAvailableUnits.Image = ((System.Drawing.Image)(resources.GetObject("buttonAvailableUnits.Image")));
            this.buttonAvailableUnits.Location = new System.Drawing.Point(749, 55);
            this.buttonAvailableUnits.Name = "buttonAvailableUnits";
            this.buttonAvailableUnits.Size = new System.Drawing.Size(91, 28);
            this.buttonAvailableUnits.StyleController = this.layoutControlDispatch;
            this.buttonAvailableUnits.TabIndex = 9;
            this.buttonAvailableUnits.Text = "Disponibles";
            this.buttonAvailableUnits.Click += new System.EventHandler(this.buttonAvailableUnits_Click);
            this.buttonAvailableUnits.KeyDown += new System.Windows.Forms.KeyEventHandler(this.UnitFilter_KeyDown);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupIncidentDetails,
            this.splitterItem1,
            this.layoutControlGroupRight,
            this.layoutControlGroupLeft});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(1286, 742);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroupIncidentDetails
            // 
            this.layoutControlGroupIncidentDetails.CustomizationFormText = "layoutControlGroupIncidentDetails";
            this.layoutControlGroupIncidentDetails.ExpandButtonMode = DevExpress.Utils.Controls.ExpandButtonMode.Inverted;
            this.layoutControlGroupIncidentDetails.ExpandButtonVisible = true;
            this.layoutControlGroupIncidentDetails.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem19,
            this.tabbedControlGroupNotes,
            this.layoutControlItem21});
            this.layoutControlGroupIncidentDetails.Location = new System.Drawing.Point(0, 495);
            this.layoutControlGroupIncidentDetails.Name = "layoutControlGroupIncidentDetails";
            this.layoutControlGroupIncidentDetails.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupIncidentDetails.Size = new System.Drawing.Size(1280, 241);
            this.layoutControlGroupIncidentDetails.Text = "layoutControlGroupIncidentDetails";
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.textBoxIncidentDetails;
            this.layoutControlItem19.CustomizationFormText = "layoutControlItem19";
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem19.MinSize = new System.Drawing.Size(105, 25);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(634, 210);
            this.layoutControlItem19.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem19.Text = "layoutControlItem19";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem19.TextToControlDistance = 0;
            this.layoutControlItem19.TextVisible = false;
            // 
            // tabbedControlGroupNotes
            // 
            this.tabbedControlGroupNotes.CustomizationFormText = "tabbedControlGroupNotes";
            this.tabbedControlGroupNotes.Location = new System.Drawing.Point(634, 0);
            this.tabbedControlGroupNotes.Name = "tabbedControlGroupNotes";
            this.tabbedControlGroupNotes.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.tabbedControlGroupNotes.SelectedTabPage = this.layoutControlGroupNotes;
            this.tabbedControlGroupNotes.SelectedTabPageIndex = 0;
            this.tabbedControlGroupNotes.Size = new System.Drawing.Size(636, 59);
            this.tabbedControlGroupNotes.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupNotes});
            this.tabbedControlGroupNotes.Text = "tabbedControlGroupNotes";
            // 
            // layoutControlGroupNotes
            // 
            this.layoutControlGroupNotes.CustomizationFormText = "layoutControlGroupNotes";
            this.layoutControlGroupNotes.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem20,
            this.layoutControlItem22});
            this.layoutControlGroupNotes.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupNotes.Name = "layoutControlGroupNotes";
            this.layoutControlGroupNotes.Size = new System.Drawing.Size(630, 33);
            this.layoutControlGroupNotes.Text = "layoutControlGroupNotes";
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.textBoxIncidentNotes;
            this.layoutControlItem20.CustomizationFormText = "layoutControlItem20";
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem20.MinSize = new System.Drawing.Size(23, 23);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem20.Size = new System.Drawing.Size(600, 33);
            this.layoutControlItem20.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem20.Text = "layoutControlItem20";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem20.TextToControlDistance = 0;
            this.layoutControlItem20.TextVisible = false;
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.buttonAddNotes;
            this.layoutControlItem22.CustomizationFormText = "layoutControlItem22";
            this.layoutControlItem22.Location = new System.Drawing.Point(600, 0);
            this.layoutControlItem22.MaxSize = new System.Drawing.Size(30, 0);
            this.layoutControlItem22.MinSize = new System.Drawing.Size(30, 27);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 1, 1, 1);
            this.layoutControlItem22.Size = new System.Drawing.Size(30, 33);
            this.layoutControlItem22.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem22.Text = "layoutControlItem22";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem22.TextToControlDistance = 0;
            this.layoutControlItem22.TextVisible = false;
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.callsAndDispatch;
            this.layoutControlItem21.CustomizationFormText = "layoutControlItem21";
            this.layoutControlItem21.Location = new System.Drawing.Point(634, 59);
            this.layoutControlItem21.MinSize = new System.Drawing.Size(103, 135);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem21.Size = new System.Drawing.Size(636, 151);
            this.layoutControlItem21.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem21.Text = "layoutControlItem21";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem21.TextToControlDistance = 0;
            this.layoutControlItem21.TextVisible = false;
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            this.splitterItem1.CustomizationFormText = "splitterItem1";
            this.splitterItem1.Location = new System.Drawing.Point(0, 489);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(1280, 6);
            // 
            // layoutControlGroupRight
            // 
            this.layoutControlGroupRight.CustomizationFormText = "layoutControlGroupRight";
            this.layoutControlGroupRight.GroupBordersVisible = false;
            this.layoutControlGroupRight.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupUnits,
            this.layoutControlGroupDispatchOrder});
            this.layoutControlGroupRight.Location = new System.Drawing.Point(638, 0);
            this.layoutControlGroupRight.Name = "layoutControlGroupRight";
            this.layoutControlGroupRight.Size = new System.Drawing.Size(642, 489);
            this.layoutControlGroupRight.Text = "layoutControlGroupRight";
            // 
            // layoutControlGroupUnits
            // 
            this.layoutControlGroupUnits.CustomizationFormText = "layoutControlGroupUnits";
            this.layoutControlGroupUnits.ExpandButtonMode = DevExpress.Utils.Controls.ExpandButtonMode.Inverted;
            this.layoutControlGroupUnits.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem2,
            this.layoutControlItem4,
            this.layoutControlItem3,
            this.emptySpaceItem3,
            this.layoutControlItem8,
            this.layoutControlItem7,
            this.layoutControlItem6,
            this.layoutControlItem5,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem12});
            this.layoutControlGroupUnits.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupUnits.Name = "layoutControlGroupUnits";
            this.layoutControlGroupUnits.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupUnits.Size = new System.Drawing.Size(642, 276);
            this.layoutControlGroupUnits.Text = "layoutControlGroupUnits";
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 214);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(542, 32);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.buttonExAssign;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(542, 214);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(90, 32);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(90, 32);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(90, 32);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.gridControlUnits;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 57);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(103, 130);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem3.Size = new System.Drawing.Size(632, 157);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(396, 25);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(236, 32);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.buttonReqAttention;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(297, 25);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(99, 32);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(99, 32);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 4, 2, 2);
            this.layoutControlItem8.Size = new System.Drawing.Size(99, 32);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "layoutControlItem8";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.buttonAssignedUnits;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(198, 25);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(99, 32);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(99, 32);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 4, 2, 2);
            this.layoutControlItem7.Size = new System.Drawing.Size(99, 32);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.buttonAvailableUnits;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(99, 25);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(99, 32);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(99, 32);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 4, 2, 2);
            this.layoutControlItem6.Size = new System.Drawing.Size(99, 32);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.buttonAllUnits;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 25);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(99, 32);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(99, 32);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 4, 2, 2);
            this.layoutControlItem5.Size = new System.Drawing.Size(99, 32);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.labelExShowBy;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(80, 25);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(80, 25);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(80, 25);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Text = "layoutControlItem9";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextToControlDistance = 0;
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.radioButtonZone;
            this.layoutControlItem10.CustomizationFormText = "layoutControlItem10";
            this.layoutControlItem10.Location = new System.Drawing.Point(80, 0);
            this.layoutControlItem10.MaxSize = new System.Drawing.Size(65, 25);
            this.layoutControlItem10.MinSize = new System.Drawing.Size(65, 25);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(65, 25);
            this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem10.Text = "layoutControlItem10";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextToControlDistance = 0;
            this.layoutControlItem10.TextVisible = false;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.radioButtonDepartment;
            this.layoutControlItem11.CustomizationFormText = "layoutControlItem11";
            this.layoutControlItem11.Location = new System.Drawing.Point(145, 0);
            this.layoutControlItem11.MaxSize = new System.Drawing.Size(79, 25);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(79, 25);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(79, 25);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Text = "layoutControlItem11";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextToControlDistance = 0;
            this.layoutControlItem11.TextVisible = false;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.radioButtonAll;
            this.layoutControlItem12.CustomizationFormText = "layoutControlItem12";
            this.layoutControlItem12.Location = new System.Drawing.Point(224, 0);
            this.layoutControlItem12.MaxSize = new System.Drawing.Size(0, 25);
            this.layoutControlItem12.MinSize = new System.Drawing.Size(1, 25);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(408, 25);
            this.layoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem12.Text = "layoutControlItem12";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextToControlDistance = 0;
            this.layoutControlItem12.TextVisible = false;
            // 
            // layoutControlGroupDispatchOrder
            // 
            this.layoutControlGroupDispatchOrder.CustomizationFormText = "layoutControlGroupDispatchOrder";
            this.layoutControlGroupDispatchOrder.ExpandButtonMode = DevExpress.Utils.Controls.ExpandButtonMode.Inverted;
            this.layoutControlGroupDispatchOrder.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem18});
            this.layoutControlGroupDispatchOrder.Location = new System.Drawing.Point(0, 276);
            this.layoutControlGroupDispatchOrder.Name = "layoutControlGroupDispatchOrder";
            this.layoutControlGroupDispatchOrder.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupDispatchOrder.Size = new System.Drawing.Size(642, 213);
            this.layoutControlGroupDispatchOrder.Text = "layoutControlGroupDispatchOrder";
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.gridControlDispatchOrder;
            this.layoutControlItem18.CustomizationFormText = "layoutControlItem18";
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem18.MinSize = new System.Drawing.Size(103, 162);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem18.Size = new System.Drawing.Size(632, 183);
            this.layoutControlItem18.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem18.Text = "layoutControlItem18";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem18.TextToControlDistance = 0;
            this.layoutControlItem18.TextVisible = false;
            // 
            // layoutControlGroupLeft
            // 
            this.layoutControlGroupLeft.CustomizationFormText = "layoutControlGroupLeft";
            this.layoutControlGroupLeft.GroupBordersVisible = false;
            this.layoutControlGroupLeft.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupAssignedIncidentNotifications,
            this.layoutControlGroupNewIncidentNotifications});
            this.layoutControlGroupLeft.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupLeft.Name = "layoutControlGroupLeft";
            this.layoutControlGroupLeft.Size = new System.Drawing.Size(638, 489);
            this.layoutControlGroupLeft.Text = "layoutControlGroupLeft";
            // 
            // layoutControlGroupAssignedIncidentNotifications
            // 
            this.layoutControlGroupAssignedIncidentNotifications.CustomizationFormText = "layoutControlGroupAssignedIncidentNotifications";
            this.layoutControlGroupAssignedIncidentNotifications.ExpandButtonMode = DevExpress.Utils.Controls.ExpandButtonMode.Inverted;
            this.layoutControlGroupAssignedIncidentNotifications.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem13,
            this.emptySpaceItem4,
            this.layoutControlItem17,
            this.layoutControlItem16,
            this.layoutControlItem15,
            this.layoutControlItem14});
            this.layoutControlGroupAssignedIncidentNotifications.Location = new System.Drawing.Point(0, 276);
            this.layoutControlGroupAssignedIncidentNotifications.Name = "layoutControlGroupAssignedIncidentNotifications";
            this.layoutControlGroupAssignedIncidentNotifications.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupAssignedIncidentNotifications.Size = new System.Drawing.Size(638, 213);
            this.layoutControlGroupAssignedIncidentNotifications.Text = "layoutControlGroupAssignedIncidentNotifications";
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.gridControlAssignedIncidentNotifications;
            this.layoutControlItem13.CustomizationFormText = "layoutControlItem13";
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 32);
            this.layoutControlItem13.MinSize = new System.Drawing.Size(103, 130);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlItem13.Size = new System.Drawing.Size(628, 151);
            this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem13.Text = "layoutControlItem13";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem13.TextToControlDistance = 0;
            this.layoutControlItem13.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(396, 0);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(232, 32);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.blinkingButtonExPendingDispatch;
            this.layoutControlItem17.CustomizationFormText = "layoutControlItem17";
            this.layoutControlItem17.Location = new System.Drawing.Point(297, 0);
            this.layoutControlItem17.MaxSize = new System.Drawing.Size(99, 32);
            this.layoutControlItem17.MinSize = new System.Drawing.Size(99, 32);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 4, 2, 2);
            this.layoutControlItem17.Size = new System.Drawing.Size(99, 32);
            this.layoutControlItem17.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem17.Text = "layoutControlItem17";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem17.TextToControlDistance = 0;
            this.layoutControlItem17.TextVisible = false;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.blinkingButtonExDispReqAttention;
            this.layoutControlItem16.CustomizationFormText = "layoutControlItem16";
            this.layoutControlItem16.Location = new System.Drawing.Point(198, 0);
            this.layoutControlItem16.MaxSize = new System.Drawing.Size(99, 32);
            this.layoutControlItem16.MinSize = new System.Drawing.Size(99, 32);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 4, 2, 2);
            this.layoutControlItem16.Size = new System.Drawing.Size(99, 32);
            this.layoutControlItem16.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem16.Text = "layoutControlItem16";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem16.TextToControlDistance = 0;
            this.layoutControlItem16.TextVisible = false;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.buttonClosedDispatch;
            this.layoutControlItem15.CustomizationFormText = "layoutControlItem15";
            this.layoutControlItem15.Location = new System.Drawing.Point(99, 0);
            this.layoutControlItem15.MaxSize = new System.Drawing.Size(99, 32);
            this.layoutControlItem15.MinSize = new System.Drawing.Size(99, 32);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 4, 2, 2);
            this.layoutControlItem15.Size = new System.Drawing.Size(99, 32);
            this.layoutControlItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem15.Text = "layoutControlItem15";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem15.TextToControlDistance = 0;
            this.layoutControlItem15.TextVisible = false;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.buttonActiveDispatch;
            this.layoutControlItem14.CustomizationFormText = "layoutControlItem14";
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem14.MaxSize = new System.Drawing.Size(99, 32);
            this.layoutControlItem14.MinSize = new System.Drawing.Size(99, 32);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 4, 2, 2);
            this.layoutControlItem14.Size = new System.Drawing.Size(99, 32);
            this.layoutControlItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem14.Text = "layoutControlItem14";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem14.TextToControlDistance = 0;
            this.layoutControlItem14.TextVisible = false;
            // 
            // layoutControlGroupNewIncidentNotifications
            // 
            this.layoutControlGroupNewIncidentNotifications.AppearanceGroup.ForeColor = System.Drawing.Color.Black;
            this.layoutControlGroupNewIncidentNotifications.AppearanceGroup.Options.UseForeColor = true;
            this.layoutControlGroupNewIncidentNotifications.AppearanceItemCaption.ForeColor = System.Drawing.Color.White;
            this.layoutControlGroupNewIncidentNotifications.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlGroupNewIncidentNotifications.CustomizationFormText = "layoutControlGroupNewIncidentNotifications";
            this.layoutControlGroupNewIncidentNotifications.ExpandButtonMode = DevExpress.Utils.Controls.ExpandButtonMode.Inverted;
            this.layoutControlGroupNewIncidentNotifications.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.emptySpaceItem1,
            this.layoutControlItem2});
            this.layoutControlGroupNewIncidentNotifications.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupNewIncidentNotifications.Name = "layoutControlGroupNewIncidentNotifications";
            this.layoutControlGroupNewIncidentNotifications.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroupNewIncidentNotifications.Size = new System.Drawing.Size(638, 276);
            this.layoutControlGroupNewIncidentNotifications.Text = "layoutControlGroupNewIncidentNotifications";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridControlNewIncidentNotifications;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(101, 187);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItem1.Size = new System.Drawing.Size(628, 214);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 214);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(538, 32);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.buttonExAddNotification;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(538, 214);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(90, 32);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(90, 32);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(90, 32);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // timerSendImage
            // 
            this.timerSendImage.Interval = 300;
            this.timerSendImage.Tick += new System.EventHandler(this.timerSendImage_Tick);
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.BackColor = System.Drawing.Color.White;
            this.ribbonControl1.Dock = System.Windows.Forms.DockStyle.None;
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.ExpandCollapseItem.Name = "";
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.barButtonItemPending,
            this.barButtonItemModifyZone,
            this.barButtonItemClose,
            this.barButtonItemAvailable,
            this.barButtonItemOutOfOrder,
            this.barButtonItemUnavailable,
            this.barButtonItemOnScene,
            this.barButtonItemAssignedOnScene,
            this.barButtonItemModifyTime,
            this.barButtonItemEnd,
            this.barButtonItemStatus,
            this.barButtonItemSupportRequest,
            this.barButtonItemAddNote,
            this.barButtonItemRefreshOrder,
            this.barButtonItemSave,
            this.barButtonItemPrint,
            this.barItemRecommendedUnits,
            this.barItemAsignedUnits,
            this.barButtonItemLocateObject,
            this.barButtonItemUnitsFollow,
            this.barEditItemDepartment,
            this.barButtonItemCall,
            this.barButtonItemTalk,
            this.barButtonItemRestartTelephone});
            this.ribbonControl1.ItemsVertAlign = DevExpress.Utils.VertAlignment.Top;
            this.ribbonControl1.Location = new System.Drawing.Point(-95, 204);
            this.ribbonControl1.MaxItemId = 52;
            this.ribbonControl1.MdiMergeStyle = DevExpress.XtraBars.Ribbon.RibbonMdiMergeStyle.Always;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbonControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBoxDepartment});
            this.ribbonControl1.ShowToolbarCustomizeItem = false;
            this.ribbonControl1.Size = new System.Drawing.Size(1350, 119);
            this.ribbonControl1.Toolbar.ShowCustomizeItem = false;
            this.ribbonControl1.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            // 
            // barButtonItemPending
            // 
            this.barButtonItemPending.Caption = "barButtonItemPending";
            this.barButtonItemPending.Id = 17;
            this.barButtonItemPending.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemPending.Name = "barButtonItemPending";
            this.barButtonItemPending.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemPending_ItemClick);
            // 
            // barButtonItemModifyZone
            // 
            this.barButtonItemModifyZone.Caption = "barButtonItemModifyZone";
            this.barButtonItemModifyZone.Id = 18;
            this.barButtonItemModifyZone.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemModifyZone.Name = "barButtonItemModifyZone";
            this.barButtonItemModifyZone.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemModifyZone_ItemClick);
            // 
            // barButtonItemClose
            // 
            this.barButtonItemClose.Caption = "barButtonItemClose";
            this.barButtonItemClose.Id = 19;
            this.barButtonItemClose.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemClose.Name = "barButtonItemClose";
            this.barButtonItemClose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemClose_ItemClick);
            // 
            // barButtonItemAvailable
            // 
            this.barButtonItemAvailable.Caption = "barButtonItemAvailable";
            this.barButtonItemAvailable.Id = 21;
            this.barButtonItemAvailable.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemAvailable.Name = "barButtonItemAvailable";
            this.barButtonItemAvailable.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemAvailable_ItemClick);
            // 
            // barButtonItemOutOfOrder
            // 
            this.barButtonItemOutOfOrder.Caption = "barButtonItemOutOfOrder";
            this.barButtonItemOutOfOrder.Id = 22;
            this.barButtonItemOutOfOrder.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemOutOfOrder.Name = "barButtonItemOutOfOrder";
            this.barButtonItemOutOfOrder.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemOutOfOrder_ItemClick);
            // 
            // barButtonItemUnavailable
            // 
            this.barButtonItemUnavailable.Caption = "ItemUnavailable";
            this.barButtonItemUnavailable.Id = 23;
            this.barButtonItemUnavailable.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemUnavailable.Name = "barButtonItemUnavailable";
            this.barButtonItemUnavailable.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemUnavailable_ItemClick);
            // 
            // barButtonItemOnScene
            // 
            this.barButtonItemOnScene.Caption = "barButtonItemOnScene";
            this.barButtonItemOnScene.Id = 24;
            this.barButtonItemOnScene.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemOnScene.Name = "barButtonItemOnScene";
            this.barButtonItemOnScene.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemOnScene_ItemClick);
            // 
            // barButtonItemAssignedOnScene
            // 
            this.barButtonItemAssignedOnScene.Caption = "ItemAssignedOnScene";
            this.barButtonItemAssignedOnScene.Id = 25;
            this.barButtonItemAssignedOnScene.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemAssignedOnScene.Name = "barButtonItemAssignedOnScene";
            this.barButtonItemAssignedOnScene.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemAssignedOnScene_ItemClick);
            // 
            // barButtonItemModifyTime
            // 
            this.barButtonItemModifyTime.Caption = "barButtonItemModifyTime";
            this.barButtonItemModifyTime.Id = 26;
            this.barButtonItemModifyTime.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemModifyTime.Name = "barButtonItemModifyTime";
            this.barButtonItemModifyTime.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemModifyTime_ItemClick);
            // 
            // barButtonItemEnd
            // 
            this.barButtonItemEnd.Caption = "barButtonItemEnd";
            this.barButtonItemEnd.Id = 27;
            this.barButtonItemEnd.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemEnd.Name = "barButtonItemEnd";
            this.barButtonItemEnd.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemEnd_ItemClick);
            // 
            // barButtonItemStatus
            // 
            this.barButtonItemStatus.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.barButtonItemStatus.Caption = "barButtonItemStatus";
            this.barButtonItemStatus.DropDownControl = this.popupMenu1;
            this.barButtonItemStatus.Id = 34;
            this.barButtonItemStatus.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemStatus.Name = "barButtonItemStatus";
            this.barButtonItemStatus.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItemStatus.ItemPress += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemStatus_ItemPress);
            // 
            // popupMenu1
            // 
            this.popupMenu1.Name = "popupMenu1";
            this.popupMenu1.Ribbon = this.ribbonControl1;
            // 
            // barButtonItemSupportRequest
            // 
            this.barButtonItemSupportRequest.AllowAllUp = true;
            this.barButtonItemSupportRequest.Caption = "ItemSupportRequest";
            this.barButtonItemSupportRequest.Id = 35;
            this.barButtonItemSupportRequest.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemSupportRequest.Name = "barButtonItemSupportRequest";
            this.barButtonItemSupportRequest.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemSupportRequest_ItemClick);
            // 
            // barButtonItemAddNote
            // 
            this.barButtonItemAddNote.Caption = "barButtonItemAddNote";
            this.barButtonItemAddNote.Id = 36;
            this.barButtonItemAddNote.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemAddNote.Name = "barButtonItemAddNote";
            this.barButtonItemAddNote.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemAddNote_ItemClick);
            // 
            // barButtonItemRefreshOrder
            // 
            this.barButtonItemRefreshOrder.Caption = "barButtonItemRefreshOrder";
            this.barButtonItemRefreshOrder.Id = 37;
            this.barButtonItemRefreshOrder.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemRefreshOrder.Name = "barButtonItemRefreshOrder";
            this.barButtonItemRefreshOrder.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemRefreshOrder_ItemClick);
            // 
            // barButtonItemSave
            // 
            this.barButtonItemSave.Caption = "barButtonItemSave";
            this.barButtonItemSave.Glyph = global::SmartCadDispatch.Gui.PrintRibbonControllerResources.RibbonPrintPreview_Save;
            this.barButtonItemSave.Id = 38;
            this.barButtonItemSave.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemSave.Name = "barButtonItemSave";
            this.barButtonItemSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemSave_ItemClick);
            // 
            // barButtonItemPrint
            // 
            this.barButtonItemPrint.Caption = "barButtonItemPrint";
            this.barButtonItemPrint.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPrint.Glyph")));
            this.barButtonItemPrint.Id = 39;
            this.barButtonItemPrint.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemPrint.Name = "barButtonItemPrint";
            this.barButtonItemPrint.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemPrint_ItemClick);
            // 
            // barItemRecommendedUnits
            // 
            this.barItemRecommendedUnits.Caption = "barItemRecommendedUnits";
            this.barItemRecommendedUnits.Glyph = ((System.Drawing.Image)(resources.GetObject("barItemRecommendedUnits.Glyph")));
            this.barItemRecommendedUnits.Id = 42;
            this.barItemRecommendedUnits.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barItemRecommendedUnits.Name = "barItemRecommendedUnits";
            this.barItemRecommendedUnits.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.ItemRecommendedUnits_ItemClick);
            // 
            // barItemAsignedUnits
            // 
            this.barItemAsignedUnits.Caption = "barItemAsignedUnits";
            this.barItemAsignedUnits.Glyph = ((System.Drawing.Image)(resources.GetObject("barItemAsignedUnits.Glyph")));
            this.barItemAsignedUnits.Id = 43;
            this.barItemAsignedUnits.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barItemAsignedUnits.Name = "barItemAsignedUnits";
            this.barItemAsignedUnits.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.ItemAsignedUnits_ItemClick);
            // 
            // barButtonItemLocateObject
            // 
            this.barButtonItemLocateObject.Caption = "barButtonItemLocateObject";
            this.barButtonItemLocateObject.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemLocateObject.Glyph")));
            this.barButtonItemLocateObject.Id = 45;
            this.barButtonItemLocateObject.Name = "barButtonItemLocateObject";
            this.barButtonItemLocateObject.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemLocateObject_ItemClick);
            // 
            // barButtonItemUnitsFollow
            // 
            this.barButtonItemUnitsFollow.Caption = "barButtonItemUnitsFollow";
            this.barButtonItemUnitsFollow.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemUnitsFollow.Glyph")));
            this.barButtonItemUnitsFollow.Id = 46;
            this.barButtonItemUnitsFollow.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemUnitsFollow.Name = "barButtonItemUnitsFollow";
            this.barButtonItemUnitsFollow.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemUnitsFollow_ItemClick);
            // 
            // barEditItemDepartment
            // 
            this.barEditItemDepartment.Caption = "barEditItemDepartment";
            this.barEditItemDepartment.Edit = this.repositoryItemComboBoxDepartment;
            this.barEditItemDepartment.Id = 47;
            this.barEditItemDepartment.MergeType = DevExpress.XtraBars.BarMenuMerge.MergeItems;
            this.barEditItemDepartment.Name = "barEditItemDepartment";
            this.barEditItemDepartment.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barEditItemDepartment_ItemClick);
            // 
            // repositoryItemComboBoxDepartment
            // 
            this.repositoryItemComboBoxDepartment.AutoHeight = false;
            this.repositoryItemComboBoxDepartment.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBoxDepartment.Name = "repositoryItemComboBoxDepartment";
            this.repositoryItemComboBoxDepartment.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.repositoryItemComboBoxDepartment.SelectedIndexChanged += new System.EventHandler(this.repositoryItemComboBoxDepartment_SelectedIndexChanged);
            // 
            // barButtonItemCall
            // 
            this.barButtonItemCall.Caption = "barButtonItemCall";
            this.barButtonItemCall.Id = 48;
            this.barButtonItemCall.Name = "barButtonItemCall";
            this.barButtonItemCall.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonItemTalk
            // 
            this.barButtonItemTalk.Caption = "barButtonItemTalk";
            this.barButtonItemTalk.Id = 49;
            this.barButtonItemTalk.Name = "barButtonItemTalk";
            this.barButtonItemTalk.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonItemRestartTelephone
            // 
            this.barButtonItemRestartTelephone.Caption = "barButtonItemRestartTelephone";
            this.barButtonItemRestartTelephone.Id = 50;
            this.barButtonItemRestartTelephone.Name = "barButtonItemRestartTelephone";
            this.barButtonItemRestartTelephone.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItemRestartTelephone.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemRestartTelephone_ItemClick);
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupDispatchOrder,
            this.ribbonPageGroupUnits,
            this.ribbonPageGroupAssignUnits,
            this.ribbonPageGroupUser,
            this.ribbonPageGroupRadio,
            this.ribbonPageGroupGeneralOptions,
            this.ribbonPageGroupTools});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "ribbonPage1";
            // 
            // ribbonPageGroupDispatchOrder
            // 
            this.ribbonPageGroupDispatchOrder.AllowMinimize = false;
            this.ribbonPageGroupDispatchOrder.AllowTextClipping = false;
            this.ribbonPageGroupDispatchOrder.ItemLinks.Add(this.barButtonItemPending);
            this.ribbonPageGroupDispatchOrder.ItemLinks.Add(this.barButtonItemModifyZone);
            this.ribbonPageGroupDispatchOrder.ItemLinks.Add(this.barButtonItemSupportRequest);
            this.ribbonPageGroupDispatchOrder.ItemLinks.Add(this.barButtonItemClose);
            this.ribbonPageGroupDispatchOrder.Name = "ribbonPageGroupDispatchOrder";
            this.ribbonPageGroupDispatchOrder.ShowCaptionButton = false;
            this.ribbonPageGroupDispatchOrder.Text = "ribbonPageGroupDispatchOrder";
            // 
            // ribbonPageGroupUnits
            // 
            this.ribbonPageGroupUnits.AllowMinimize = false;
            this.ribbonPageGroupUnits.AllowTextClipping = false;
            this.ribbonPageGroupUnits.ItemLinks.Add(this.barButtonItemAvailable);
            this.ribbonPageGroupUnits.ItemLinks.Add(this.barButtonItemOutOfOrder);
            this.ribbonPageGroupUnits.ItemLinks.Add(this.barButtonItemUnavailable);
            this.ribbonPageGroupUnits.ItemLinks.Add(this.barButtonItemOnScene);
            this.ribbonPageGroupUnits.Name = "ribbonPageGroupUnits";
            this.ribbonPageGroupUnits.ShowCaptionButton = false;
            this.ribbonPageGroupUnits.Text = "ribbonPageGroupUnits";
            // 
            // ribbonPageGroupAssignUnits
            // 
            this.ribbonPageGroupAssignUnits.AllowMinimize = false;
            this.ribbonPageGroupAssignUnits.AllowTextClipping = false;
            this.ribbonPageGroupAssignUnits.ItemLinks.Add(this.barButtonItemAssignedOnScene);
            this.ribbonPageGroupAssignUnits.ItemLinks.Add(this.barButtonItemModifyTime);
            this.ribbonPageGroupAssignUnits.ItemLinks.Add(this.barButtonItemAddNote);
            this.ribbonPageGroupAssignUnits.ItemLinks.Add(this.barButtonItemEnd);
            this.ribbonPageGroupAssignUnits.Name = "ribbonPageGroupAssignUnits";
            this.ribbonPageGroupAssignUnits.ShowCaptionButton = false;
            this.ribbonPageGroupAssignUnits.Text = "ribbonPageGroupAssignUnits";
            // 
            // ribbonPageGroupUser
            // 
            this.ribbonPageGroupUser.AllowMinimize = false;
            this.ribbonPageGroupUser.AllowTextClipping = false;
            this.ribbonPageGroupUser.ItemLinks.Add(this.barButtonItemStatus);
            this.ribbonPageGroupUser.Name = "ribbonPageGroupUser";
            this.ribbonPageGroupUser.ShowCaptionButton = false;
            this.ribbonPageGroupUser.Text = "ribbonPageGroupUser";
            // 
            // ribbonPageGroupRadio
            // 
            this.ribbonPageGroupRadio.AllowMinimize = false;
            this.ribbonPageGroupRadio.AllowTextClipping = false;
            this.ribbonPageGroupRadio.ItemLinks.Add(this.barEditItemDepartment, false, "", "", true);
            this.ribbonPageGroupRadio.ItemLinks.Add(this.barButtonItemCall, true);
            this.ribbonPageGroupRadio.ItemLinks.Add(this.barButtonItemTalk);
            this.ribbonPageGroupRadio.ItemLinks.Add(this.barButtonItemRestartTelephone, true);
            this.ribbonPageGroupRadio.MergeOrder = 5;
            this.ribbonPageGroupRadio.Name = "ribbonPageGroupRadio";
            this.ribbonPageGroupRadio.ShowCaptionButton = false;
            this.ribbonPageGroupRadio.Text = "ribbonPageGroupRadio";
            this.ribbonPageGroupRadio.Visible = false;
            // 
            // ribbonPageGroupGeneralOptions
            // 
            this.ribbonPageGroupGeneralOptions.AllowMinimize = false;
            this.ribbonPageGroupGeneralOptions.AllowTextClipping = false;
            this.ribbonPageGroupGeneralOptions.ItemLinks.Add(this.barButtonItemPrint);
            this.ribbonPageGroupGeneralOptions.ItemLinks.Add(this.barButtonItemSave);
            this.ribbonPageGroupGeneralOptions.ItemLinks.Add(this.barButtonItemRefreshOrder);
            this.ribbonPageGroupGeneralOptions.Name = "ribbonPageGroupGeneralOptions";
            this.ribbonPageGroupGeneralOptions.ShowCaptionButton = false;
            this.ribbonPageGroupGeneralOptions.Text = "ribbonPageGroupGeneralOptions";
            // 
            // ribbonPageGroupTools
            // 
            this.ribbonPageGroupTools.AllowMinimize = false;
            this.ribbonPageGroupTools.AllowTextClipping = false;
            this.ribbonPageGroupTools.ItemLinks.Add(this.barButtonItemUnitsFollow);
            this.ribbonPageGroupTools.ItemLinks.Add(this.barButtonItemLocateObject);
            this.ribbonPageGroupTools.ItemLinks.Add(this.barItemRecommendedUnits, true);
            this.ribbonPageGroupTools.ItemLinks.Add(this.barItemAsignedUnits);
            this.ribbonPageGroupTools.Name = "ribbonPageGroupTools";
            this.ribbonPageGroupTools.ShowCaptionButton = false;
            this.ribbonPageGroupTools.Text = "ribbonPageGroupTools";
            // 
            // defaultLookAndFeel1
            // 
            this.defaultLookAndFeel1.LookAndFeel.SkinName = "Blue";
            // 
            // DefaultDispatchFormDevX
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1286, 742);
            this.Controls.Add(this.ribbonControl1);
            this.Controls.Add(this.layoutControlDispatch);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DefaultDispatchFormDevX";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SmartCAD - Mdulo de Despacho";
            this.TransparencyKey = System.Drawing.Color.Maroon;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DefaultDispatchFormDevX_FormClosing);
            this.Load += new System.EventHandler(this.DefaultDispatchFormDevX_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DefaultDispatchFormDevX_KeyDown);
            this.contextMenuStripUnits.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlDispatch)).EndInit();
            this.layoutControlDispatch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlDispatchOrder)).EndInit();
            this.contextMenuDispatchOrder.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDispatchOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlAssignedIncidentNotifications)).EndInit();
            this.contextMenuStripAssignedIncidentNotif.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridViewAssignedIncidentNotifications)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlUnits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewUnits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlNewIncidentNotifications)).EndInit();
            this.contextMenuStripNewIncidentNotification.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridViewNewIncidentNotifications)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEx2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupIncidentDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroupNotes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupNotes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupUnits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupDispatchOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupAssignedIncidentNotifications)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupNewIncidentNotifications)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBoxDepartment)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private SimpleButtonEx buttonReqAttention;
		private SimpleButtonEx buttonAssignedUnits;
		private SimpleButtonEx buttonAvailableUnits;
		private SimpleButtonEx buttonAllUnits;
		private BlinkingSimpleButtonEx blinkingButtonExDispReqAttention;
		private SimpleButtonEx buttonClosedDispatch;
        private SimpleButtonEx buttonActiveDispatch;
        private ContextMenuStripEx contextMenuStripAssignedIncidentNotif;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemPending;
        private ContextMenuStripEx contextMenuStripUnits;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemAssignUnit;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemCallUnit;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemDelayUnit;
        private ContextMenuStripEx contextMenuDispatchOrder;
        private System.Windows.Forms.ToolStripMenuItem cerrarToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem modifyTimeOrderToolStripMenuItem;
        private System.Windows.Forms.ToolTip toolTipGeneral;
        private System.Windows.Forms.ToolStripMenuItem enEscenaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemOutOfOrderUnit;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemCloseIncNotif;
        private BlinkingSimpleButtonEx blinkingButtonExPendingDispatch;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemAvailableUnit;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemNotAvailableUnit;
        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemOnSceneUnit;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemCloseUnit;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemSupportRequest;
        private LabelEx labelExShowBy;
        private System.Windows.Forms.RadioButton radioButtonZone;
        private System.Windows.Forms.RadioButton radioButtonAll;
        private System.Windows.Forms.RadioButton radioButtonDepartment;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemModifyZone;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemCallStation;
        private ContextMenuStripEx contextMenuStripNewIncidentNotification;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemAssign;
		private SimpleButtonEx buttonExAddNotification;
        private SimpleButtonEx buttonExAssign;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemAddNote;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemAddNoteToUnit;
		private System.Windows.Forms.PrintDialog printDialogMain;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemRefreshIncidentNotitficationDetails;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemRefreshNewIncidentNotificationDetails;
		private System.Windows.Forms.Timer timerSendImage;
		private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
		private DevExpress.XtraBars.BarButtonItem barButtonItemPending;
		private DevExpress.XtraBars.BarButtonItem barButtonItemModifyZone;
		private DevExpress.XtraBars.BarButtonItem barButtonItemClose;
		private DevExpress.XtraBars.BarButtonItem barButtonItemAvailable;
		private DevExpress.XtraBars.BarButtonItem barButtonItemOutOfOrder;
		private DevExpress.XtraBars.BarButtonItem barButtonItemUnavailable;
		private DevExpress.XtraBars.BarButtonItem barButtonItemOnScene;
		private DevExpress.XtraBars.BarButtonItem barButtonItemAssignedOnScene;
		private DevExpress.XtraBars.BarButtonItem barButtonItemModifyTime;
		private DevExpress.XtraBars.BarButtonItem barButtonItemEnd;
		private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupUnits;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupAssignUnits;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupUser;
		private DevExpress.XtraBars.BarButtonItem barButtonItemStatus;
		private DevExpress.XtraBars.PopupMenu popupMenu1;
		private DevExpress.XtraBars.BarButtonItem barButtonItemSupportRequest;
		private DevExpress.XtraBars.BarButtonItem barButtonItemAddNote;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupDispatchOrder;
		private DevExpress.XtraBars.BarButtonItem barButtonItemRefreshOrder;
		private DevExpress.XtraBars.BarButtonItem barButtonItemSave;
		private DevExpress.XtraBars.BarButtonItem barButtonItemPrint;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupGeneralOptions;
		private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
        private SearchableWebBrowser textBoxIncidentDetails;
		private SimpleButtonEx buttonAddNotes;
        private TextBoxEx textBoxIncidentNotes;
        private GridControlEx gridControlNewIncidentNotifications;
        private GridViewEx gridViewNewIncidentNotifications;
        private GridViewEx gridViewEx2;
        private GridControlEx gridControlAssignedIncidentNotifications;
        private GridViewEx gridViewAssignedIncidentNotifications;
        private GridControlEx gridControlUnits;
        private GridViewEx gridViewUnits;
        private GridControlEx gridControlDispatchOrder;
        private GridViewEx gridViewDispatchOrder;
        private DevExpress.XtraLayout.LayoutControl layoutControlDispatch;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupIncidentDetails;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.SplitterItem splitterItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupRight;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupUnits;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupDispatchOrder;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroupNotes;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupNotes;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private CallsAndDispatchsControl callsAndDispatch;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupLeft;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupAssignedIncidentNotifications;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupNewIncidentNotifications;
        internal DevExpress.XtraBars.BarButtonItem barItemRecommendedUnits;
        internal DevExpress.XtraBars.BarButtonItem barItemAsignedUnits;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupTools;
        internal DevExpress.XtraBars.BarButtonItem barButtonItemLocateObject;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemLocateNewIncNot;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemLocateAssignedIncNot;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemLocateUnit;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemLocateUnitDispatch;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemRecommendedUnits;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemAssignedUnits;
		private DevExpress.XtraBars.BarButtonItem barButtonItemUnitsFollow;
        private DevExpress.XtraBars.BarEditItem barEditItemDepartment;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBoxDepartment;
        private DevExpress.XtraBars.BarButtonItem barButtonItemCall;
        private DevExpress.XtraBars.BarButtonItem barButtonItemTalk;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupRadio;
        private DevExpress.XtraBars.BarButtonItem barButtonItemRestartTelephone;
        



    }
}

