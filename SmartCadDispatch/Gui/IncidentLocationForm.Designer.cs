using SmartCadControls.Controls;
namespace SmartCadDispatch.Gui
{
    partial class IncidentLocationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private new System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBoxAddress = new DevExpress.XtraEditors.GroupControl();
            this.labelExAddress = new LabelEx();
            this.groupBoxZoneStation = new DevExpress.XtraEditors.GroupControl();
            this.labelExStation = new LabelEx();
            this.labelExZone = new LabelEx();
            this.comboBoxStation = new System.Windows.Forms.ComboBox();
            this.comboBoxZone = new System.Windows.Forms.ComboBox();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.simpleButtonAccept = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.groupBoxAddress)).BeginInit();
            this.groupBoxAddress.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupBoxZoneStation)).BeginInit();
            this.groupBoxZoneStation.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxAddress
            // 
            this.groupBoxAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxAddress.Controls.Add(this.labelExAddress);
            this.groupBoxAddress.Location = new System.Drawing.Point(5, 5);
            this.groupBoxAddress.Margin = new System.Windows.Forms.Padding(2);
            this.groupBoxAddress.Name = "groupBoxAddress";
            this.groupBoxAddress.Padding = new System.Windows.Forms.Padding(2);
            this.groupBoxAddress.Size = new System.Drawing.Size(350, 103);
            this.groupBoxAddress.TabIndex = 0;
            this.groupBoxAddress.Text = "Ubicacion del incidente";
            // 
            // labelExAddress
            // 
            this.labelExAddress.BackColor = System.Drawing.Color.White;
            this.labelExAddress.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelExAddress.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelExAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExAddress.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExAddress.Location = new System.Drawing.Point(4, 24);
            this.labelExAddress.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelExAddress.Name = "labelExAddress";
            this.labelExAddress.Size = new System.Drawing.Size(342, 75);
            this.labelExAddress.TabIndex = 0;
            // 
            // groupBoxZoneStation
            // 
            this.groupBoxZoneStation.Controls.Add(this.labelExStation);
            this.groupBoxZoneStation.Controls.Add(this.labelExZone);
            this.groupBoxZoneStation.Controls.Add(this.comboBoxStation);
            this.groupBoxZoneStation.Controls.Add(this.comboBoxZone);
            this.groupBoxZoneStation.Location = new System.Drawing.Point(5, 112);
            this.groupBoxZoneStation.Margin = new System.Windows.Forms.Padding(2);
            this.groupBoxZoneStation.Name = "groupBoxZoneStation";
            this.groupBoxZoneStation.Padding = new System.Windows.Forms.Padding(4, 2, 2, 2);
            this.groupBoxZoneStation.Size = new System.Drawing.Size(350, 110);
            this.groupBoxZoneStation.TabIndex = 1;
            this.groupBoxZoneStation.Text = "Zona y estacion *";
            // 
            // labelExStation
            // 
            this.labelExStation.AutoSize = true;
            this.labelExStation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExStation.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExStation.Location = new System.Drawing.Point(5, 64);
            this.labelExStation.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelExStation.Name = "labelExStation";
            this.labelExStation.Size = new System.Drawing.Size(329, 13);
            this.labelExStation.TabIndex = 2;
            this.labelExStation.Text = "Seleccione la estacion en la cual se encuentra ubicado el incidente:";
            // 
            // labelExZone
            // 
            this.labelExZone.AutoSize = true;
            this.labelExZone.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExZone.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExZone.Location = new System.Drawing.Point(5, 22);
            this.labelExZone.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelExZone.Name = "labelExZone";
            this.labelExZone.Size = new System.Drawing.Size(312, 13);
            this.labelExZone.TabIndex = 0;
            this.labelExZone.Text = "Seleccione la zona en la cual se encuentra ubicado el incidente:";
            // 
            // comboBoxStation
            // 
            this.comboBoxStation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxStation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxStation.FormattingEnabled = true;
            this.comboBoxStation.Location = new System.Drawing.Point(8, 80);
            this.comboBoxStation.Margin = new System.Windows.Forms.Padding(2);
            this.comboBoxStation.Name = "comboBoxStation";
            this.comboBoxStation.Size = new System.Drawing.Size(336, 21);
            this.comboBoxStation.TabIndex = 3;
            this.comboBoxStation.SelectedIndexChanged += new System.EventHandler(this.comboBoxStation_Click);
            this.comboBoxStation.Click += new System.EventHandler(this.comboBoxStation_Click);
            // 
            // comboBoxZone
            // 
            this.comboBoxZone.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxZone.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxZone.FormattingEnabled = true;
            this.comboBoxZone.Location = new System.Drawing.Point(7, 38);
            this.comboBoxZone.Margin = new System.Windows.Forms.Padding(2);
            this.comboBoxZone.Name = "comboBoxZone";
            this.comboBoxZone.Size = new System.Drawing.Size(337, 21);
            this.comboBoxZone.TabIndex = 1;
            this.comboBoxZone.SelectedIndexChanged += new System.EventHandler(this.comboBoxZone_Click);
            this.comboBoxZone.Click += new System.EventHandler(this.comboBoxZone_Click);
            // 
            // simpleButtonAccept
            // 
            this.simpleButtonAccept.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonAccept.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButtonAccept.Appearance.Options.UseFont = true;
            this.simpleButtonAccept.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.simpleButtonAccept.Enabled = false;
            this.simpleButtonAccept.Location = new System.Drawing.Point(199, 231);
            this.simpleButtonAccept.Name = "simpleButtonAccept";
            this.simpleButtonAccept.Size = new System.Drawing.Size(75, 25);
            this.simpleButtonAccept.TabIndex = 4;
            this.simpleButtonAccept.Click += new System.EventHandler(this.buttonExAccept_Click);
            // 
            // simpleButtonCancel
            // 
            this.simpleButtonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButtonCancel.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButtonCancel.Appearance.Options.UseFont = true;
            this.simpleButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButtonCancel.Location = new System.Drawing.Point(280, 231);
            this.simpleButtonCancel.Name = "simpleButtonCancel";
            this.simpleButtonCancel.Size = new System.Drawing.Size(75, 25);
            this.simpleButtonCancel.TabIndex = 5;
            // 
            // IncidentLocationForm
            // 
            this.AcceptButton = this.simpleButtonAccept;
            this.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.simpleButtonCancel;
            this.ClientSize = new System.Drawing.Size(360, 259);
            this.Controls.Add(this.simpleButtonCancel);
            this.Controls.Add(this.simpleButtonAccept);
            this.Controls.Add(this.groupBoxZoneStation);
            this.Controls.Add(this.groupBoxAddress);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "IncidentLocationForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Zonificacion del incidente";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.IncidentLocationForm_FormClosing);
            this.Load += new System.EventHandler(this.IncidentLocationForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupBoxAddress)).EndInit();
            this.groupBoxAddress.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupBoxZoneStation)).EndInit();
            this.groupBoxZoneStation.ResumeLayout(false);
            this.groupBoxZoneStation.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupBoxAddress;
        private DevExpress.XtraEditors.GroupControl groupBoxZoneStation;
        private System.Windows.Forms.ComboBox comboBoxStation;
        private System.Windows.Forms.ComboBox comboBoxZone;
        private System.Windows.Forms.ToolTip toolTip;
        private LabelEx labelExStation;
        private LabelEx labelExZone;
        private LabelEx labelExAddress;
        private DevExpress.XtraEditors.SimpleButton simpleButtonAccept;
        private DevExpress.XtraEditors.SimpleButton simpleButtonCancel;
    }
}