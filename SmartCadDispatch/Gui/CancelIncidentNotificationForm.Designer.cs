using SmartCadControls.Controls;
namespace SmartCadDispatch.Gui
{
    partial class CancelIncidentNotificationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private new System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonExAccept = new SimpleButtonEx();
            this.buttonExCancel = new SimpleButtonEx();
            this.groupBoxComment = new DevExpress.XtraEditors.GroupControl();
            this.textBoxExComment = new TextBoxEx();
            ((System.ComponentModel.ISupportInitialize)(this.groupBoxComment)).BeginInit();
            this.groupBoxComment.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonExAccept
            // 
            this.buttonExAccept.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonExAccept.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExAccept.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonExAccept.Appearance.Options.UseFont = true;
            this.buttonExAccept.Appearance.Options.UseForeColor = true;
            this.buttonExAccept.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonExAccept.FontFormat = FontFormat.GetFormat("ButtonFormat");
            this.buttonExAccept.Location = new System.Drawing.Point(135, 146);
            this.buttonExAccept.Name = "buttonExAccept";
            this.buttonExAccept.Size = new System.Drawing.Size(75, 23);
            this.buttonExAccept.TabIndex = 1;
            this.buttonExAccept.Text = "Accept";
            this.buttonExAccept.Click += new System.EventHandler(this.buttonExAccept_Click);
            // 
            // buttonExCancel
            // 
            this.buttonExCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonExCancel.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExCancel.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonExCancel.Appearance.Options.UseFont = true;
            this.buttonExCancel.Appearance.Options.UseForeColor = true;
            this.buttonExCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonExCancel.FontFormat = FontFormat.GetFormat("ButtonFormat");
            this.buttonExCancel.Location = new System.Drawing.Point(216, 146);
            this.buttonExCancel.Name = "buttonExCancel";
            this.buttonExCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonExCancel.TabIndex = 2;
            this.buttonExCancel.Text = "Cancelar";
            // 
            // groupBoxComment
            // 
            this.groupBoxComment.Controls.Add(this.textBoxExComment);
            this.groupBoxComment.Location = new System.Drawing.Point(5, 4);
            this.groupBoxComment.Name = "groupBoxComment";
            this.groupBoxComment.Size = new System.Drawing.Size(286, 136);
            this.groupBoxComment.TabIndex = 0;
            this.groupBoxComment.Text = "Motivo de cancelacion *";
            // 
            // textBoxExComment
            // 
            this.textBoxExComment.AcceptsReturn = true;
            this.textBoxExComment.AllowsLetters = true;
            this.textBoxExComment.AllowsNumbers = true;
            this.textBoxExComment.AllowsPunctuation = true;
            this.textBoxExComment.AllowsSeparators = true;
            this.textBoxExComment.AllowsSymbols = true;
            this.textBoxExComment.AllowsWhiteSpaces = true;
            this.textBoxExComment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxExComment.ExtraAllowedChars = "";
            this.textBoxExComment.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExComment.Location = new System.Drawing.Point(2, 20);
            this.textBoxExComment.MaxLength = 1000;
            this.textBoxExComment.Multiline = true;
            this.textBoxExComment.Name = "textBoxExComment";
            this.textBoxExComment.NonAllowedCharacters = "";
            this.textBoxExComment.RegularExpresion = "";
            this.textBoxExComment.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxExComment.Size = new System.Drawing.Size(282, 114);
            this.textBoxExComment.TabIndex = 0;
            this.textBoxExComment.TextChanged += new System.EventHandler(this.textBoxExComment_TextChanged);
            // 
            // CancelIncidentNotificationForm
            // 
            this.AcceptButton = this.buttonExAccept;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonExCancel;
            this.ClientSize = new System.Drawing.Size(296, 177);
            this.Controls.Add(this.groupBoxComment);
            this.Controls.Add(this.buttonExCancel);
            this.Controls.Add(this.buttonExAccept);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CancelIncidentNotificationForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cancelar solicitud de despacho";
            this.Load += new System.EventHandler(this.CancelIncidentNotificationForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupBoxComment)).EndInit();
            this.groupBoxComment.ResumeLayout(false);
            this.groupBoxComment.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private SimpleButtonEx buttonExAccept;
        private SimpleButtonEx buttonExCancel;
        private DevExpress.XtraEditors.GroupControl groupBoxComment;
        private TextBoxEx textBoxExComment;
    }
}