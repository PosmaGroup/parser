using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

using SmartCadCore.Core;
using Smartmatic.SmartCad.Service;
using SmartCadCore.ClientData;
using System.Collections.Generic;
using SmartCadCore.Common;
using SmartCadControls.Controls;

namespace SmartCadDispatch.Gui
{
    public partial class SupportRequestForm : DevExpress.XtraEditors.XtraForm
    {
        SupportRequestReportClientData supportRequestReport;
        private IncidentNotificationClientData incidentNotification;

		private DefaultDispatchFormDevX parentForm; 

		private object syncCommitted = new object();

        public SupportRequestForm(Form form, IncidentNotificationClientData incidentNotification)
        {
            InitializeComponent();

			this.parentForm = (DefaultDispatchFormDevX)form;

			this.incidentNotification = incidentNotification;
            supportRequestReport = new SupportRequestReportClientData();
            FillControls();
            this.parentForm.DispatchCommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(DispatchForm_DispatchCommittedChanges);
            this.FormClosing += new FormClosingEventHandler(SupportRequestForm_FormClosing);
        }

        void SupportRequestForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.parentForm.DispatchCommittedChanges -= new EventHandler<CommittedObjectDataCollectionEventArgs>(DispatchForm_DispatchCommittedChanges);
        }

        void DispatchForm_DispatchCommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
        {
            try
            {
                if (e.Objects != null && e.Objects.Count > 0)
                {
                    lock (syncCommitted)
                    {
                        try
                        {
                            if (ServerServiceClient.GetInstance().OperatorClient == null)
                            { }
                        }
                        catch
                        { }

                        if (e.Objects[0] is DepartmentTypeClientData)
                        {
                            #region DepartmentTypeClientData
                            DepartmentTypeClientData deptType =
                                e.Objects[0] as DepartmentTypeClientData;
                            if (e.Action == CommittedDataAction.Save)
                            {
                                if (departmentTypesControl.GlobalDepartmentTypes.ContainsKey(deptType.Code) == false)
                                    departmentTypesControl.GlobalDepartmentTypes.Add(deptType.Code, deptType);
                            }
                            else if (e.Action == CommittedDataAction.Delete)
                            {
                                if (departmentTypesControl.GlobalDepartmentTypes.ContainsKey(deptType.Code) == true)
                                    departmentTypesControl.GlobalDepartmentTypes.Remove(deptType.Code);
                            }
							else if (e.Action == CommittedDataAction.Update)
							{
                                if (deptType.Dispatch == false)
                                {
                                    if (departmentTypesControl.GlobalDepartmentTypes.ContainsKey(deptType.Code) == true)
                                        departmentTypesControl.GlobalDepartmentTypes.Remove(deptType.Code);
                                }
                                else
                                {
                                    if (departmentTypesControl.GlobalDepartmentTypes.ContainsKey(deptType.Code) == false)
                                        departmentTypesControl.GlobalDepartmentTypes.Add(deptType.Code, deptType);
                                }
							}
							FormUtil.InvokeRequired(this, new MethodInvoker(departmentTypesControl.FillDepartmentTypes));
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        private void SupportRequestForm_Load(object sender, EventArgs e)
        {
            this.departmentTypesControl.DepartmentTypesSelected += new EventHandler<EventArgs>(departmentTypesControl_DepartmentTypesSelected);

            this.departmentTypesControl.Enabled = true;
            
            if (this.departmentTypesControl.gridViewExDepartmentType.RowCount > 0)
                this.departmentTypesControl.gridViewExDepartmentType.SelectRow(0);
			LoadLanguage();
        }

		private void LoadLanguage()
		{
			layoutControlGroupDepartments.Text = ResourceLoader.GetString2("Departments") + " *";
			layoutControlGroupObservations.Text = ResourceLoader.GetString2("Observations") + " *";
			layoutControlGroupRequestedBy.Text = ResourceLoader.GetString2("RequestedBy") + " *";
			layoutControlItemMobileUnit.Text = ResourceLoader.GetString2("MobileUnit");
            this.Text = ResourceLoader.GetString2("TitleSupportRequestForm");
            this.buttonExAccept.Text = ResourceLoader.GetString2("$Message.Accept");
            this.buttonExCancel.Text = ResourceLoader.GetString2("$Message.Cancel");
		}

        void departmentTypesControl_DepartmentTypesSelected(object sender, EventArgs e)
        {
            DepartmentTypesControl control = sender as DepartmentTypesControl;
            if (control != null)
            {
                supportRequestReport.ReportBaseDepartmentTypes = control.SelectedDepartmentTypesWithPriority;
            }
            EnableAcceptButton(null, null);
        }

        private void FillControls()
        {
            FillComboBox();
            FillDepartmentTypeControl();
        }

        private void FillDepartmentTypeControl()
        {
            IList departmentTypes = ServerServiceClient.GetInstance().SearchClientObjects(
                SmartCadHqls.GetCustomHql(
                SmartCadHqls.GetDepartmentsTypeWithStationsAssociatedWithAlreadyNotifiedDepartmentsByIncidentCode, 
                incidentNotification.IncidentCode));
            Dictionary<int, DepartmentTypeClientData> departmentTypesDictionary = new Dictionary<int, DepartmentTypeClientData>();
            foreach (DepartmentTypeClientData dtcd in departmentTypes)
            {
                departmentTypesDictionary.Add(dtcd.Code, dtcd);
            }
            
            this.departmentTypesControl.GlobalDepartmentTypes = new Dictionary<int, DepartmentTypeClientData>();
            foreach (DepartmentTypeClientData departmentTypeClient in departmentTypesDictionary.Values)
                this.departmentTypesControl.GlobalDepartmentTypes.Add(departmentTypeClient.Code, departmentTypeClient);

            IList notificationPriorities = ServerServiceClient.GetInstance().SearchClientObjects(typeof(IncidentNotificationPriorityClientData));
            this.departmentTypesControl.GlobalNotificationPriorities = new Dictionary<string, IncidentNotificationPriorityClientData>();
            foreach (IncidentNotificationPriorityClientData notificationPriority in notificationPriorities)
                this.departmentTypesControl.GlobalNotificationPriorities.Add(notificationPriority.CustomCode, notificationPriority);

            if (departmentTypesDictionary.Count == 0)
            {
                MessageForm.Show(ResourceLoader.GetString2("SupportRequestError"), MessageFormType.Information);
                this.Dispose();
            }
        }

        private void FillComboBox()
        {
            SortedList<string, UnitClientData> sortedUnitList = new SortedList<string, UnitClientData>();
            foreach (DispatchOrderClientData dispatchOrder in incidentNotification.DispatchOrders)
            	if (dispatchOrder.Status.Name == DispatchOrderStatusClientData.UnitOnScene.Name)
                    sortedUnitList.Add(dispatchOrder.Unit.CustomCode, dispatchOrder.Unit);
            
            foreach (UnitClientData unitClient in sortedUnitList.Values)
                this.comboBoxUnit.Items.Add(unitClient);                
        }

        private void buttonExAccept_Click(object sender, EventArgs e)
        {
            this.supportRequestReport.Comment = textBoxComment.Text;
            this.supportRequestReport.UnitCode = (comboBoxUnit.SelectedItem as UnitClientData).Code;
            this.supportRequestReport.CustomCode = Guid.NewGuid().ToString();
            this.supportRequestReport.IncidentCode = this.incidentNotification.IncidentCode;
            this.supportRequestReport.OperatorCode = ServerServiceClient.GetInstance().OperatorClient.Code;
            this.supportRequestReport.SourceReportBaseCode = this.incidentNotification.ReportBaseCode;
            this.supportRequestReport.SourceReportBaseType = this.incidentNotification.ReportBaseType;

            foreach (ReportBaseDepartmentTypeClientData reportBaseDepartmentType in
                this.supportRequestReport.ReportBaseDepartmentTypes)
                reportBaseDepartmentType.ReportBaseCode = this.supportRequestReport.Code;
            ServerServiceClient.GetInstance().SaveOrUpdateClientData(supportRequestReport);
        }

        private void EnableAcceptButton(object sender, EventArgs e)
        {
            if (this.comboBoxUnit.SelectedIndex != -1 &&
                this.textBoxComment.Text.Trim().Length > 0 &&
                CheckDepartmentTypesAndPriorities() == true)
            {
                this.buttonExAccept.Enabled = true;
            }
            else
            {
                this.buttonExAccept.Enabled = false;
            }
        }

        private bool CheckDepartmentTypesAndPriorities()
        {
            bool result = true;
            if (this.departmentTypesControl.SelectedDepartmentTypesWithPriority.Count > 0)
            {
                for (int i = 0; i < this.departmentTypesControl.SelectedDepartmentTypesWithPriority.Count
                    && result == true; i++)
                {
                    ReportBaseDepartmentTypeClientData temp =
                        (ReportBaseDepartmentTypeClientData)this.departmentTypesControl.SelectedDepartmentTypesWithPriority[i];
                    if (temp.DepartmentTypeCode == 0 || temp.PriorityCode == 0)
                    {
                        result = false;
                    }
                }
            }
            else
            {
                result = false;
            }
            return result;
        }
    }
}
