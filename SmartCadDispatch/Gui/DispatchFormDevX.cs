using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Smartmatic.SmartCad.Service;
using System.Net;
using SmartCadCore.ClientData;
using System.Collections;
using System.Diagnostics;
using System.Threading;
using SmartCadCore.Common;
using DevExpress.XtraBars;
using System.Reflection;
using SmartCadCore.Core;
using SmartCadGuiCommon;
using SmartCadCore.Enums;
using SmartCadGuiCommon.Services;


namespace SmartCadDispatch.Gui
{
	public partial class DispatchFormDevX : XtraForm
	{
		#region Fields
		private OperatorChatForm chatForm;
		DefaultDispatchFormDevX defaultDispatchFormDevX;
		private ServerServiceClient serverServiceClient;
		private object syncCommited = new object();
        public event EventHandler<CommittedObjectDataCollectionEventArgs> DispatchFormCommittedChanges;
        public bool accessToMap;
        internal bool mapIsOpen;
        private bool loaded = false;

        public DateTime syncServerDateTime;
        private TimeSpan sinceLoggedIn;
        private const int GLOBAL_TIMER_PERIOD = 10000;
        private readonly TimeSpan ADD_TIMESPAN;
        public System.Threading.Timer globalTimer;
        private string labelUserCaption = "";
        private string labelConnectedCaption = "";
        
		#endregion

		#region Properties

		public ServerServiceClient ServerServiceClient
		{
			get
			{
				return this.serverServiceClient;
			}
			set
			{
				this.serverServiceClient = value;
				CheckAccessToMap();
				//defaultDispatchFormDevX.ServerService = this.serverServiceClient;
			}
		}
        
		#endregion

        #region Telephone Radio
        public TelephonyConsoleManager TelephonyConsoleManager { get; set; }
        #endregion

        public DispatchFormDevX()
		{
			InitializeComponent();
            var applications = ServerServiceClient.GetInstance().GetActiveApplications();
            if (!applications.Contains(SmartCadApplications.SmartCadSupervision))
            {
                ribbonPageGroupTools.Visible = false;
            }
            if (!applications.Contains(SmartCadApplications.SmartCadMap))
            {
                ribbonPageGroupApplications.Visible = false;
            }
			LoadLanguage();
			defaultDispatchFormDevX = new DefaultDispatchFormDevX(this);
			defaultDispatchFormDevX.MdiParent = this;
			ServerServiceClient.GetInstance().CommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(DispatchFormDevX_CommittedChanges);
            this.ribbonControl1.MinimizedChanged += new EventHandler(RibbonControl_MinimizedChanged);
            ADD_TIMESPAN = new TimeSpan(0, 0, 0, 0, GLOBAL_TIMER_PERIOD);
		}

        private void FillStatusStrip()
        {
            syncServerDateTime = serverServiceClient.GetTime();
            sinceLoggedIn = TimeSpan.Zero;
            //this.labelConnected.Caption = "00:00";

            labelDate.Caption = ApplicationUtil.GetFormattedDate(syncServerDateTime);
            globalTimer = ApplicationUtil.RecurrentExecuteMethod(
                new SimpleDelegate(
                delegate
                {
                    globalTimer_Tick(null, null);
                }), null, null, GLOBAL_TIMER_PERIOD, GLOBAL_TIMER_PERIOD);

            labelUser.Caption = labelUserCaption + serverServiceClient.OperatorClient.FirstName
                + " " + serverServiceClient.OperatorClient.LastName;
            labelConnected.Caption = labelConnectedCaption + "00:00";
        }

        private void globalTimer_Tick(object sender, EventArgs e)
        {
            syncServerDateTime = syncServerDateTime.AddMilliseconds((double)GLOBAL_TIMER_PERIOD);
            sinceLoggedIn = sinceLoggedIn.Add(ADD_TIMESPAN);
            FormUtil.InvokeRequired(this, delegate
            {
                ribbonStatusBar1.Invalidate(true);
                labelDate.Caption = ApplicationUtil.GetFormattedDate(syncServerDateTime);
                labelConnected.Caption = labelConnectedCaption + Math.Truncate(sinceLoggedIn.TotalHours).ToString().PadLeft(2, '0') + ":" + sinceLoggedIn.Minutes.ToString().PadLeft(2, '0');
                ribbonStatusBar1.ResumeLayout(true);
                ribbonStatusBar1.LayoutChanged();
            });
        }

        void RibbonControl_MinimizedChanged(object sender, EventArgs e)
        {
            if (this.ribbonControl1.Minimized == true)
            {
                this.ribbonControl1.Minimized = false;
            }
        }

		void DispatchFormDevX_CommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
		{
			try
			{
				if (e.Objects != null && e.Objects.Count > 0)
				{
					lock (syncCommited)
					{
						try
						{
							if (ServerServiceClient.GetInstance().OperatorClient == null)
							{ }
						}
						catch
						{ }
                        if (DispatchFormCommittedChanges != null)
                        {
                            DispatchFormCommittedChanges(null, e);
                        }
						if (e.Objects[0] is ApplicationMessageClientData)
						{
                            if (loaded)
                            {
                                ApplicationMessageClientData amcd = ((ApplicationMessageClientData)e.Objects[0]);
                                if (amcd.ToOperators.Contains(serverServiceClient.OperatorClient))
                                {
                                    if (chatForm != null && chatForm.IsDisposed == false)
                                    {
                                        FormUtil.InvokeRequired(this, delegate
                                        {
                                            chatForm.OpenNewTab(amcd.Operator, amcd);
                                            chatForm.TopMost = false; FlashingWindow.FlashWindowEx(chatForm);
                                        });
                                    }
                                    else
                                    {
                                       FormUtil.InvokeRequired(this, delegate
                                        {
                                            alertControlChat.Show(this, ResourceLoader.GetString2("MessageFrom") + ": " + amcd.Operator.Login, amcd.Message);
                                            chatForm = new OperatorChatForm(UserApplicationClientData.Dispatch.Name, ChatOperatorType.Operator);
                                            chatForm.OpenNewTab(amcd.Operator, amcd);
                                            chatForm.WindowState = FormWindowState.Minimized;
                                            chatForm.Show();
                                            FlashingWindow.FlashWindowEx(chatForm);
                                        }
                                        );
                                    }
                                }
                            }
                        }
						else if (e.Objects[0] is SessionHistoryClientData)
                        {
                            SessionHistoryClientData sess = (SessionHistoryClientData)e.Objects[0];
                            if (sess.UserApplication == UserApplicationClientData.Map.Name &&
                                sess.Operator == ServerServiceClient.GetInstance().OperatorClient.Code)
                            {
                                EnableMapButton(sess);
                                if (defaultDispatchFormDevX != null && defaultDispatchFormDevX.IsDisposed == false)
                                {
                                    defaultDispatchFormDevX.EnableLocationButton(null, EventArgs.Empty);
                                }
                            }
                        }
					}
				}
			}
			catch (Exception ex)
			{
				SmartLogger.Print(ex);
			}
		}

		private void alertControlChat_AlertClick(object sender, DevExpress.XtraBars.Alerter.AlertClickEventArgs e)
		{
			if (chatForm == null || chatForm.IsDisposed == true)
			{
				chatForm = new OperatorChatForm(UserApplicationClientData.Dispatch.Name, ChatOperatorType.Operator);
			}
			chatForm.WindowState = FormWindowState.Normal;
			chatForm.Activate();
			chatForm.Show();
			chatForm.BringToFront();
		}

        private void CheckAccessToMap()
		{
            accessToMap = serverServiceClient.CheckAccessClient(UserResourceClientData.Application,
                UserActionClientData.Basic, UserApplicationClientData.Map, false) == true;
			EnableMapButton(null);

            if (barButtonItemOpenMap.Enabled == true)
            {
                ApplicationServiceClient.Current(UserApplicationClientData.Map).SendToActivateLayer(ShapeType.Unit);
            }
		}

		private bool EnsureLogout()
		{
			DialogResult dialogResult = MessageForm.Show(ResourceLoader.GetString2("$Message.ExitApplication"), MessageFormType.Question);

			return dialogResult == DialogResult.Yes;
		}

		private void DispatchFormDevX_Load(object sender, EventArgs e)
		{
			ServerServiceClient.GetInstance().RequestReassignIncidentNotifications();
            this.Location = new Point(0, 0);
            this.Width = Screen.PrimaryScreen.WorkingArea.Width;
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
			xtraTabbedMdiManager1.Pages[defaultDispatchFormDevX].ShowCloseButton = DevExpress.Utils.DefaultBoolean.False;
			defaultDispatchFormDevX.Show();
			barButtonItemOpenMap.Glyph = ResourceLoader.GetImage("$Image.OpenMap");
            CheckAccessToMap();
            loaded = true;
		}

		private void DispatchFormDevX_FormClosing(object sender, FormClosingEventArgs e)
		{
            ServerServiceClient.GetInstance().CommittedChanges -= new EventHandler<CommittedObjectDataCollectionEventArgs>(DispatchFormDevX_CommittedChanges);
		}

        public void DispatchFormDevX_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e != null)
            {
                if (e.KeyCode == Keys.F1)
                {
                    barButtonItemHelp.PerformClick();
                }
                else if (sender == null && e.Alt == true && !(e.Modifiers == (Keys.Control | Keys.Alt))
                    && e.KeyCode == Keys.F4 && this.Disposing == false)
                {
                    this.Close();
                }
                else if (e.Modifiers == Keys.Control)
                {
                    if (e.KeyCode.Equals(Keys.U) == true)
                    {
                        if (defaultDispatchFormDevX.barButtonItemLocateObject.Enabled)
                        {
                            defaultDispatchFormDevX.barButtonItemLocateObject.PerformClick();
                        }
                    }
                    else if (e.KeyCode.Equals(Keys.R) == true)
                    {
                        if (defaultDispatchFormDevX.barItemRecommendedUnits.Enabled)
                        {
                            defaultDispatchFormDevX.barItemRecommendedUnits.PerformClick();
                        }
                    }
                    else if (e.KeyCode.Equals(Keys.S) == true)
                    {
                        if (defaultDispatchFormDevX.barItemAsignedUnits.Enabled)
                        {
                            defaultDispatchFormDevX.barItemAsignedUnits.PerformClick();
                        }
                    }
                }
            }
        }

		private void LoadLanguage()
		{
			barButtonItemAssignedOnScene.Caption = ResourceLoader.GetString2("OnScene");
			barButtonItemAvailable.Caption = ResourceLoader.GetString2("Available");
			barButtonItemClose.Caption = ResourceLoader.GetString2("Close");
			barButtonItemEnd.Caption = ResourceLoader.GetString2("Finalize");
			barButtonItemModifyTime.Caption = ResourceLoader.GetString2("ModifyTime");
			barButtonItemModifyZone.Caption = ResourceLoader.GetString2("ModifyZone");
			barButtonItemOnScene.Caption = ResourceLoader.GetString2("OnScene");
			barButtonItemOutOfOrder.Caption = ResourceLoader.GetString2("OutOfOrder");
			barButtonItemPending.Caption = ResourceLoader.GetString2("Pending");
			barButtonItemRefreshOrder.Caption = ResourceLoader.GetString2("Refresh");
			barButtonItemStatus.Caption = ResourceLoader.GetString2("Status");
			barButtonItemUnavailable.Caption = ResourceLoader.GetString2("Unavailable");
			barButtonItemAddNote.Caption = ResourceLoader.GetString2("AddNote");
			barButtonItemSupportRequest.Caption = ResourceLoader.GetString2("SupportRequest");
			ribbonPage1.Text = ResourceLoader.GetString2("Dispatch");
			ribbonPageGroupAssignUnits.Text = ResourceLoader.GetString2("DispatchedUnits");
			ribbonPageGroupDispatchOrder.Text = ResourceLoader.GetString2("DispatchOrders");
			ribbonPageGroupUnits.Text = ResourceLoader.GetString2("MobileUnits");
			ribbonPageGroupUser.Text = ResourceLoader.GetString2("Status");
			ribbonPageGroupGeneralOptions.Text = ResourceLoader.GetString2("GeneralOptions");
			ribbonPageGroupApplications.Text = ResourceLoader.GetString2("Maps");
			barButtonItemChat.Caption = ResourceLoader.GetString2("Chat");
			barButtonItemOpenMap.Caption = ResourceLoader.GetString2("Open");
			barButtonItemPrint.Caption = ResourceLoader.GetString2("Print");
			barButtonItemSave.Caption = ResourceLoader.GetString2("Save");
			ribbonPageGroupTools.Text = ResourceLoader.GetString2("Tools");
			Text = ResourceLoader.GetString2("DispatchFormDevX");
            this.Icon = ResourceLoader.GetIcon("DispatchIcon");

            #region Footer
            labelUserCaption = ResourceLoader.GetString2("User") + ": ";
            labelConnectedCaption = ResourceLoader.GetString2("Connected") + ": ";
            #endregion
		}

		public void SetPassword(string p)
		{
			defaultDispatchFormDevX.SetPassword(p);
		}
		
		private void barButtonItemChat_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
			if (chatForm == null || chatForm.IsDisposed == true)
			{
				chatForm = new OperatorChatForm(UserApplicationClientData.Dispatch.Name, ChatOperatorType.Operator);
			}
			chatForm.Activate();
			chatForm.Show();
		}

        public void EnableMapButton(SessionHistoryClientData session)
        {            
            FormUtil.InvokeRequired(this,
                delegate
                {
                    if (accessToMap)
                    {
                        if (session == null)
                        {
                            IList lastSessionHistories = ServerServiceClient.GetInstance().SearchClientObjects(
                                SmartCadHqls.GetCustomHql(SmartCadHqls.GetLastSessionHistory, 
                                ServerServiceClient.GetInstance().OperatorClient.Code, UserApplicationClientData.Map.Name));
                            foreach (SessionHistoryClientData sess in lastSessionHistories)
                            {
                                if (sess.UserApplication == UserApplicationClientData.Map.Name)
                                {
                                    session = sess;
                                    break;
                                }
                            }
                        }
                        if (session != null)
                        {
                            CheckMapIsOpen(session);
                            barButtonItemOpenMap.Enabled = !mapIsOpen;
                        }
                    }
                    else
                    {
                        barButtonItemOpenMap.Enabled = false;
                    }
                });
        }

        public void EnableMapButton(SessionHistoryClientData session, SessionHistoryClientData session1)
        {
            FormUtil.InvokeRequired(this,
                delegate
                {
                    if (accessToMap)
                    {
                        if (session == null)
                        {
                            IList lastSessionHistories = ServerServiceClient.GetInstance().SearchClientObjects(
                                SmartCadHqls.GetCustomHql(SmartCadHqls.GetLastSessionHistory,
                                ServerServiceClient.GetInstance().OperatorClient.Code, UserApplicationClientData.Map.Name));
                            foreach (SessionHistoryClientData sess in lastSessionHistories)
                            {
                                if (sess.UserApplication == UserApplicationClientData.Map.Name)
                                {
                                    session = sess;
                                    mapIsOpen = true;
                                    break;
                                }
                            }                            
                        }
                    }
                    else
                    {
                        barButtonItemOpenMap.Enabled = false;
                    }
                });
        }

        private void CheckMapIsOpen(SessionHistoryClientData session)
        {
            mapIsOpen = false;
            try
            {
                Process[] processArray = Process.GetProcessesByName("SmartCadMap");
                if (processArray.Length > 0)
                {
                    for (int i = 0; i < processArray.Length && !mapIsOpen; i++)
                    {
                        Process p = processArray[i];
                        if (p.StartInfo != null)
                        {
                            mapIsOpen = (session.Operator == ServerServiceClient.GetInstance().OperatorClient.Code &&
                                    session.UserApplication == UserApplicationClientData.Map.Name &&
                                    session.IsLoggedIn == true && session.ComputerName == Environment.MachineName);                                                                                    
                        }
                    }
                }
            }
            catch
            {
            }
        }

        private void barButtonItemOpenMap_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                ApplicationUtil.LaunchApplicationWithArguments(
                            SmartCadConfiguration.SmartCadMapFilename,
                            new object[] { serverServiceClient.OperatorClient.Login, 
                                this.defaultDispatchFormDevX.password, 
                                ApplicationUtil.CheckPrimaryScreenBounds(this.Left),
                                new SendActivateLayerEventArgs(ShapeType.Unit)});
            }
            catch (Exception ex)
            {
                MessageForm.Show(ex.Message, ex);
            }
        }

		private void barButtonItemHelp_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
			try
			{
                ApplicationUtil.LaunchHelp(SmartCadConfiguration.DistFolder + "/Help/DISPATCH Module.chm");
			}
			catch (Exception ex)
			{
				MessageForm.Show(ResourceLoader.GetString2("$Message.HelpNotInstalled"), ex);
			}
		}

		private void barButtonItemExit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
		{
			this.Close();
		}

		public void EnableRibbonButtons(bool enabled)
		{
            barButtonItemChat.Enabled = enabled;
		}

		public void LoadInitialData()
		{
			defaultDispatchFormDevX.LoadInitialData();
            FillStatusStrip();
		}

        private void barButtonItemPending_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }
	}
}