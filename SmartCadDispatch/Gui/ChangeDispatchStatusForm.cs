using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Smartmatic.SmartCad.Service;
using SmartCadCore.Core;
using System.Collections;
using System.Reflection;

using SmartCadCore.ClientData;
using System.Globalization;
using SmartCadCore.Common;
using SmartCadCore.Enums;
using SmartCadControls;
using SmartCadControls.SyncBoxes;
using SmartCadGuiCommon.Util;
using Smartmatic.SmartCad.Map;
using SmartCadGuiCommon;

namespace SmartCadDispatch.Gui
{
    public partial class ChangeDispatchStatusForm : DevExpress.XtraEditors.XtraForm
    {
        private readonly Color UNANSWERED = Color.Blue;
        private readonly Color SELECTED = Color.Red;
        private readonly Color ANSWERED = Color.Green;
        private readonly string EXPECTED_TIME_ZERO = "0:00";
        private IList selectedDispatchOrders;
        private DispatchOrderStatusClientData status;
        private DispatchOrderClientData currentDispatchOrder;
        private GridControlDataCreateOrChangeDispatchOrder currentDataGridData;
        private UnitStatusClientData receivedUnitStatus;
        private Dictionary<DispatchOrderClientData, DispatchOrderNoteClientData> noteByDispatchOrder;
        private Dictionary<string, TimeSpan> originalTimeByDispatchOrder;
        private Dictionary<string, DateTime> originalArrivalDate;
        private Dictionary<string, UnitStatusClientData> originalUnitStatus;
        private Dictionary<string, UnitStatusClientData> finalUnitStatus;
        private readonly CultureInfo CURRENT_CULTURE;
        private bool incidentIsInMap;
        private bool isByIncidentNotification;

        public bool IncidentIsInMap
        {
            get
            {
                return incidentIsInMap;
            }
            set
            {
                incidentIsInMap = value;
            }
        }

        public Dictionary<string, TimeSpan> OriginalTimeByDispatchOrder
        {
            get
            {
                return originalTimeByDispatchOrder;
            }
        }

        public Dictionary<string, DateTime> OriginalArrivalDate
        {
            get
            {
                return originalArrivalDate;
            }
        }

        public Dictionary<string, UnitStatusClientData> OriginalUnitStatus
        {
            get
            {
                return originalUnitStatus;
            }
        }

        public IList SelectedDispatchOrders
        {
            get
            {
                return selectedDispatchOrders;
            }
            set
            {
                string expectedTime = EXPECTED_TIME_ZERO;
                selectedDispatchOrders = value;
                FormUtil.InvokeRequired(this,
                    delegate
                    {
                        gridControlExDispatchOrder.ClearData();
                        gridViewExDispatchOrder_SelectionWillChange(null, null);
                        currentDataGridData = null;
                        //this.noteByDispatchOrder.Clear();                        
                        this.currentDispatchOrder = null;
                        this.labelArrivalDateText.Text = "";
                        this.labelArrivalDateText.Tag = "";
                        this.labelDepartureDateText.Text = "";
                        this.labelDepartureDateText.Tag = null;
                        this.labelExpectedTimeText.Text = "";
                        this.labelStatusText.Text = "";
                        this.timeSelectorControl1.Reset();
                        this.textBoxComment.Clear();
                        bool incidentIsSynchronized = false;
                        bool unitsAndIncidentAreSynchronized = false;
                        bool creatingDispatch = this.status.Name.Equals(DispatchOrderStatusClientData.Dispatched.Name);
                        foreach (DispatchOrderClientData dispatchOrder in selectedDispatchOrders)
                        {
                            DispatchOrderNoteClientData note = GetNote(dispatchOrder.Unit.Status);
                            TimeSpan expectedTimeSpan = dispatchOrder.ExpectedTime;
                            if (this.noteByDispatchOrder.ContainsKey(dispatchOrder) == true)
                            {
                                if (this.status.Name != DispatchOrderStatusClientData.Cancelled.Name ||
                                    this.noteByDispatchOrder[dispatchOrder] == null)
                                {
                                    this.noteByDispatchOrder[dispatchOrder] = note;
                                }
                            }
                            else
                            {
                                this.noteByDispatchOrder.Add(dispatchOrder, note);
                            }

                            if (this.originalTimeByDispatchOrder.ContainsKey(dispatchOrder.CustomCode) == true)
                            {
                                this.originalTimeByDispatchOrder[dispatchOrder.CustomCode] = expectedTimeSpan;
                            }
                            else
                            {
                                this.originalTimeByDispatchOrder.Add(dispatchOrder.CustomCode, expectedTimeSpan);
                            }

                            if (this.originalArrivalDate.ContainsKey(dispatchOrder.CustomCode) == true)
                            {
                                this.originalArrivalDate[dispatchOrder.CustomCode] = dispatchOrder.ArrivalDate;
                            }
                            else
                            {
                                this.originalArrivalDate.Add(dispatchOrder.CustomCode, dispatchOrder.ArrivalDate);
                            }

                            if (this.originalUnitStatus.ContainsKey(dispatchOrder.CustomCode) == false)
                            {
                                this.originalUnitStatus.Add(dispatchOrder.CustomCode, dispatchOrder.Unit.Status);
                            }
                            else
                            {
                                this.originalUnitStatus[dispatchOrder.CustomCode] = dispatchOrder.Unit.Status;
                            }
                        }
                        foreach (DispatchOrderClientData dispatchOrder in selectedDispatchOrders)
                        {
                            if (dispatchOrder != null)
                            {
                                if (creatingDispatch == true)
                                {
                                    if (incidentIsSynchronized == false)
                                        incidentIsSynchronized = (dispatchOrder.Address.IsSynchronized == true);

                                    unitsAndIncidentAreSynchronized = incidentIsSynchronized
                                        && (unitsAndIncidentAreSynchronized || (dispatchOrder.Unit.Lon != 0 && dispatchOrder.Unit.Lat != 0));
                                }
                                DispatchOrderNoteClientData note = GetNote(dispatchOrder.Unit.Status);
                                TimeSpan expectedTimeSpan = dispatchOrder.ExpectedTime;
                                expectedTime = EXPECTED_TIME_ZERO;
                                GridControlDataCreateOrChangeDispatchOrder item = null;
                                dispatchOrder.TempArrivalDate = dispatchOrder.ArrivalDate;
                                if (dispatchOrder.Unit.Status.Name.Equals("Delayed"))
                                {
                                    dispatchOrder.TemporalExpectedTime = TimeSpan.Zero;
                                    if (this.status.Name == DispatchOrderStatusClientData.Cancelled.Name)
                                    {
                                        dispatchOrder.NoteDetail = this.noteByDispatchOrder[dispatchOrder].Detail;
                                        item = new GridControlDataCreateOrChangeDispatchOrder(dispatchOrder, GridControlDataCreateOrChangeDispatchOrder.ActionOnData.Cancelled);
                                        if (this.finalUnitStatus.ContainsKey(dispatchOrder.CustomCode) == false)
                                        {
                                            this.finalUnitStatus.Add(dispatchOrder.CustomCode, comboBoxStatus.SelectedItem as UnitStatusClientData);
                                        }
                                        item.FinalUnitStatus = ((UnitStatusClientData)comboBoxStatus.SelectedItem).FriendlyName;
                                    }
                                    else
                                    {
                                        item = new GridControlDataCreateOrChangeDispatchOrder(dispatchOrder, GridControlDataCreateOrChangeDispatchOrder.ActionOnData.TimeChangedInDelayedUnit);
                                    }
                                }
                                else if (this.status.Name == DispatchOrderStatusClientData.Cancelled.Name ||
                                    this.status.Name == DispatchOrderStatusClientData.Closed.Name)
                                {
                                    if (this.status.Name == DispatchOrderStatusClientData.Cancelled.Name)
                                    {
                                        item = new GridControlDataCreateOrChangeDispatchOrder(dispatchOrder, GridControlDataCreateOrChangeDispatchOrder.ActionOnData.Cancelled);
                                    }
                                    else if (this.status.Name == DispatchOrderStatusClientData.Closed.Name)
                                    {
                                        item = new GridControlDataCreateOrChangeDispatchOrder(dispatchOrder, GridControlDataCreateOrChangeDispatchOrder.ActionOnData.Closed);
                                    }

                                    if (this.finalUnitStatus.ContainsKey(dispatchOrder.CustomCode) == false)
                                    {
                                        this.finalUnitStatus.Add(dispatchOrder.CustomCode, comboBoxStatus.SelectedItem as UnitStatusClientData);
                                    }
                                    item.FinalUnitStatus = ((UnitStatusClientData)comboBoxStatus.SelectedItem).FriendlyName;
                                }
                                else if (dispatchOrder.Unit.Status.Name.Equals("Assigned") &&
                                    this.status.Name.Equals("Delayed"))
                                {
                                    item = new GridControlDataCreateOrChangeDispatchOrder(dispatchOrder, GridControlDataCreateOrChangeDispatchOrder.ActionOnData.TimeChangedInAssignedUnit);
                                }
                                else
                                {
                                    item = new GridControlDataCreateOrChangeDispatchOrder(dispatchOrder, GridControlDataCreateOrChangeDispatchOrder.ActionOnData.ToBeCreated);
                                    item.IncidentIsSynchronized = incidentIsSynchronized;
                                }

                                this.labelArrivalDateText.Text = dispatchOrder.TempArrivalDate.ToString("MM/dd/yyyy HH:mm:ss", CURRENT_CULTURE);
                                if (dispatchOrder.Unit.Status.Name.Equals("Assigned"))
                                {
                                    TimeSpan difference = dispatchOrder.TempArrivalDate - ServerServiceClient.GetInstance().GetTime();
                                    if (difference < TimeSpan.Zero)
                                        difference = TimeSpan.Zero;

                                    int hours = (int)Math.Truncate(difference.TotalHours);
                                    int minutes = difference.Minutes;

                                    if (difference.Seconds > 29)
                                    {
                                        if (minutes < 59)
                                            minutes++;
                                        else
                                        {
                                            minutes = 0;
                                            hours++;
                                        }
                                    }
                                    expectedTime = hours.ToString() +
                                                          ":" + minutes.ToString().PadLeft(2, '0');
                                    item.ExpectedTime = expectedTime;
                                    TimeSpan expectedTimeSpan1;
                                    TimeSpan.TryParse(expectedTime, out expectedTimeSpan1);
                                    dispatchOrder.TemporalExpectedTime = expectedTimeSpan1;
                                }
                                else if (dispatchOrder.Unit.Status.Name.Equals("Available"))
                                {
                                    if (dispatchOrder.TemporalExpectedTime.TotalHours > maxTime.TotalHours)
                                        dispatchOrder.TemporalExpectedTime = maxTime;
                                    expectedTime = Math.Floor(dispatchOrder.TemporalExpectedTime.TotalHours).ToString() +
                                                          ":" + dispatchOrder.TemporalExpectedTime.Minutes.ToString().PadLeft(2, '0');
                                    item.ExpectedTime = expectedTime;
                                }


                                if (this.status.Name.Equals(DispatchOrderStatusClientData.Closed.Name))
                                {
                                    item.ExpectedTime = dispatchOrder.TimeInAttention(ServerServiceClient.GetInstance().GetTime());
                                }
                                else
                                {
                                    item.ExpectedTime = expectedTime;
                                }
                                //currentDispatchOrder = dispatchOrder;
                                gridControlExDispatchOrder.AddOrUpdateItem(item, true);
                            }
                        }
                        if (selectedDispatchOrders.Count > 0)
                            gridViewExDispatchOrder.FocusedRowHandle = 0;
                        if (unitsAndIncidentAreSynchronized == true)
                        {
                            this.buttonEstimateTime.Enabled = true;
                        }
                        else
                        {
                            this.buttonEstimateTime.Enabled = false;
                        }

                        if (gridControlExDispatchOrder.Items.Count > 0)
                        {
                            gridControlExDispatchOrder.Focus();
                            gridViewExDispatchOrder.FocusedRowHandle = 0;
                        }
                    });
            }
        }

        private DispatchOrderNoteClientData GetNote(UnitStatusClientData unitStatus)
        {
            DispatchOrderNoteClientData note = new DispatchOrderNoteClientData();
            note.CompleteUserName = ServerServiceClient.GetInstance().OperatorClient.FirstName
                + " " + ServerServiceClient.GetInstance().OperatorClient.LastName;
            note.CreationDate = ServerServiceClient.GetInstance().GetTime();
            note.UserLogin = ServerServiceClient.GetInstance().OperatorClient.Login;
            if (this.status.Name == DispatchOrderStatusClientData.Dispatched.Name)
            {
                note.Type = DispatchOrderNoteTypeClientData.TimeChanged;
                note.Detail = ResourceLoader.GetString2("UnitArrivalTimeChanged");
            }
            else if (this.status.Name == DispatchOrderStatusClientData.Cancelled.Name)
            {
                note.Type = DispatchOrderNoteTypeClientData.Cancelled;
            }
            else if (this.status.Name == DispatchOrderStatusClientData.Closed.Name)
            {
                note.Type = DispatchOrderNoteTypeClientData.Closed;
                note.Detail = ResourceLoader.GetString2("DispatchRequestClosed");
            }
            else if (this.status.Name.Equals("Delayed") && unitStatus.Name.Equals("Assigned"))
            {
                note.Type = DispatchOrderNoteTypeClientData.TimeChanged;
                note.Detail = ResourceLoader.GetString2("UnitArrivalTimeChanged");
            }
            return note;
        }

        private void FillComboBoxStatus()
        {
            if (this.receivedUnitStatus != null)
            {
                this.comboBoxStatus.Items.Add(this.receivedUnitStatus);
            }
            else
            {
                this.comboBoxStatus.Items.Add(UnitStatusClientData.Available);
                this.comboBoxStatus.Items.Add(UnitStatusClientData.NotAvailable);
                this.comboBoxStatus.Items.Add(UnitStatusClientData.OutOfOrder);            
            }
            this.comboBoxStatus.SelectedIndex = 0;
        }

        public ChangeDispatchStatusForm(DispatchOrderStatusClientData newStatus, UnitStatusClientData newUnitStatus, bool isByIncidentNotification)
        {
            InitializeComponent();
            CURRENT_CULTURE = ApplicationUtil.GetCurrentCulture();
            this.status = newStatus;
            this.receivedUnitStatus = newUnitStatus;
            this.isByIncidentNotification = isByIncidentNotification;
        
            if (this.status.Name == DispatchOrderStatusClientData.Cancelled.Name ||
                this.status.Name == DispatchOrderStatusClientData.Closed.Name)
            {
                FillComboBoxStatus();
            }
            this.simpleButtonAccept.Enabled = false;
            this.originalTimeByDispatchOrder = new Dictionary<string, TimeSpan>();
            this.originalArrivalDate = new Dictionary<string, DateTime>();
            this.originalUnitStatus = new Dictionary<string, UnitStatusClientData>();
            this.finalUnitStatus = new Dictionary<string, UnitStatusClientData>();
            this.noteByDispatchOrder = new Dictionary<DispatchOrderClientData, DispatchOrderNoteClientData>();
            this.gridControlExDispatchOrder.EnableAutoFilter = false;
            this.gridControlExDispatchOrder.Type = typeof(GridControlDataCreateOrChangeDispatchOrder);

        

            }

        private void textBoxComment_TextChanged(object sender, EventArgs e)
        {
            if (currentDispatchOrder != null)
            {
                if (this.textBoxComment.Text.Trim() != string.Empty)
                {
                    if (string.IsNullOrEmpty(this.noteByDispatchOrder[currentDispatchOrder].Detail) == true)
                    {
                        FormUtil.InvokeRequired(this.gridControlExDispatchOrder,
                        delegate
                        {
                            if (this.gridControlExDispatchOrder.SelectedItems.Count > 0)
                            {
                                GridControlDataCreateOrChangeDispatchOrder gridData = this.gridControlExDispatchOrder.SelectedItems[0] as GridControlDataCreateOrChangeDispatchOrder;
                                gridData.DispatchOrder.NoteDetail = this.textBoxComment.Text;
                                gridControlExDispatchOrder.AddOrUpdateItem(gridData);                                
                            }
                        });
                    }
                    this.noteByDispatchOrder[currentDispatchOrder].Detail = this.textBoxComment.Text;                    
                }
                else
                {
                    if (string.IsNullOrEmpty(this.noteByDispatchOrder[currentDispatchOrder].Detail) == false)
                    {
                        FormUtil.InvokeRequired(this.gridControlExDispatchOrder,
                        delegate
                        {
                            if (this.gridControlExDispatchOrder.SelectedItems.Count > 0)
                            {
                                GridControlDataCreateOrChangeDispatchOrder gridData = this.gridControlExDispatchOrder.SelectedItems[0] as GridControlDataCreateOrChangeDispatchOrder;
                                gridData.DispatchOrder.NoteDetail = string.Empty;
                                gridControlExDispatchOrder.AddOrUpdateItem(gridData);
                            }                            
                        });
                    }                    
                    this.noteByDispatchOrder[currentDispatchOrder].Detail = string.Empty;
                }
            }
            EnableButtons();
        }

        private void timeSelectorControl1_TimeChanged(object sender, EventArgs e)
        {
            if (currentDispatchOrder != null)
            {
                labelDepartureDateText.Tag = ServerServiceClient.GetInstance().GetTime();
                labelDepartureDateText.Text = ServerServiceClient.GetInstance().GetTime().ToString("MM/dd/yyyy HH:mm:ss", CURRENT_CULTURE);
                if (timeSelectorControl1.TimeSpan != TimeSpan.Zero)
                    currentDispatchOrder.TemporalExpectedTime = timeSelectorControl1.TimeSpan;
                else if (timeSelectorControl1.TimeSpan == TimeSpan.Zero)
                    currentDispatchOrder.TemporalExpectedTime = TimeSpan.Zero;
                DateTime arrivalDate = ((DateTime)this.labelDepartureDateText.Tag).Add(currentDispatchOrder.TemporalExpectedTime);
                currentDispatchOrder.TempArrivalDate = arrivalDate;
                this.labelArrivalDateText.Text = arrivalDate.ToString("MM/dd/yyyy HH:mm:ss", CURRENT_CULTURE);
                this.labelArrivalDateText.Tag = arrivalDate;
                if (this.gridControlExDispatchOrder.SelectedItems.Count > 0)
                {
                    GridControlDataCreateOrChangeDispatchOrder item = this.gridControlExDispatchOrder.SelectedItems[0] as GridControlDataCreateOrChangeDispatchOrder;
                    if (currentDispatchOrder.TemporalExpectedTime.TotalHours > maxTime.TotalHours)
                        currentDispatchOrder.TemporalExpectedTime = maxTime;
                    item.ExpectedTime = Math.Floor(currentDispatchOrder.TemporalExpectedTime.TotalHours) + ":" +
                        currentDispatchOrder.TemporalExpectedTime.Minutes.ToString().PadLeft(2, '0');
                    if (currentDispatchOrder.Unit.Status.Name.Equals("Assigned") &&
                        this.status.Name.Equals("Delayed"))
                    {
                        currentDispatchOrder.NoteDetail = this.noteByDispatchOrder[currentDispatchOrder].Detail;
                    }
                    gridControlExDispatchOrder.AddOrUpdateItem(item);
                }
            }
            EnableButtons();
        }


        private Dictionary<int, Dictionary<string, string>> reportControlsAnswersByUnit = new Dictionary<int, Dictionary<string, string>>();
        private Dictionary<string, string> reportControlsAnswers = new Dictionary<string, string>();

        private void buttonCloseReport_Click(object sender, EventArgs e)
        {        
            if (isByIncidentNotification == false)
            {
                if (currentDispatchOrder != null)
                {
                    EndDispatchReportForm endingReportForm  = new EndDispatchReportForm(FormBehavior.Edit, currentDispatchOrder);

                    if (reportControlsAnswersByUnit.ContainsKey(currentDispatchOrder.Code) == true)
                        endingReportForm.ControlAnswers = reportControlsAnswersByUnit[currentDispatchOrder.Code];

                    bool reportWasNull = true;

                    if (currentDispatchOrder.EndingReport == null)
                        currentDispatchOrder.EndingReport = new EndingReportClientData();
                    else
                        reportWasNull = false;

                    DialogResult result = endingReportForm.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        if (reportControlsAnswersByUnit.ContainsKey(currentDispatchOrder.Code) == false)
                            reportControlsAnswersByUnit.Add(currentDispatchOrder.Code, endingReportForm.ControlAnswers);
                        else
                            reportControlsAnswersByUnit[currentDispatchOrder.Code] = endingReportForm.ControlAnswers;

                        currentDispatchOrder.EndingReport = endingReportForm.EndingReport;
                        currentDispatchOrder.EndingReport.DispatchOrderCode = currentDispatchOrder.Code;
                        currentDispatchOrder.EndingReport.ByUnit = !isByIncidentNotification;

                        FormUtil.InvokeRequired(this.gridControlExDispatchOrder,
                            delegate
                            {
                                if (this.gridControlExDispatchOrder.SelectedItems.Count > 0)
                                {
                                    GridControlDataCreateOrChangeDispatchOrder gridData = this.gridControlExDispatchOrder.SelectedItems[0] as GridControlDataCreateOrChangeDispatchOrder;
                                    gridData.HasEndingReport = true;
                                    this.gridControlExDispatchOrder.AddOrUpdateItem(gridData);
                                }
                            });
                    }
                    else if (result == DialogResult.Cancel)
                    {
                        if (reportWasNull == true)
                            currentDispatchOrder.EndingReport = null;

                    }
                    DialogResult = DialogResult.None;
                }
            }
            else
            {
                if (selectedDispatchOrders != null && selectedDispatchOrders.Count > 0) 
                {
                    EndDispatchReportForm endingReportForm = new EndDispatchReportForm(FormBehavior.Edit, selectedDispatchOrders);

                    if (reportControlsAnswers.Count > 0)
                        endingReportForm.ControlAnswers  = reportControlsAnswers;
          
                    bool reportWasNull = true;

                    if (((DispatchOrderClientData)selectedDispatchOrders[0]).EndingReport == null)
                    {
                        foreach (DispatchOrderClientData dispatchOrder in selectedDispatchOrders)
                            dispatchOrder.EndingReport = new EndingReportClientData();
                        
                    }
                    else
                        reportWasNull = false;


                    DialogResult result = endingReportForm.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        reportControlsAnswers = endingReportForm.ControlAnswers;
                        foreach (DispatchOrderClientData dispatchOrder in selectedDispatchOrders)
                        {                           
                            dispatchOrder.EndingReport = endingReportForm.EndingReport;
                            dispatchOrder.EndingReport.DispatchOrderCode = dispatchOrder.Code;
                            dispatchOrder.EndingReport.ByUnit = !isByIncidentNotification;
                        }
                       
                        FormUtil.InvokeRequired(this.gridControlExDispatchOrder,
                            delegate
                            {
                                if (this.gridControlExDispatchOrder.SelectedItems.Count > 0)
                                {
                                    for (int i = 0; i < gridControlExDispatchOrder.Items.Count; i++)
                                    {
                                        GridControlDataCreateOrChangeDispatchOrder gridData = gridControlExDispatchOrder.Items[i] as GridControlDataCreateOrChangeDispatchOrder;
                                        gridData.HasEndingReport = true;
                                        this.gridControlExDispatchOrder.AddOrUpdateItem(gridData);
                                    }
                                }
                            });
                    }
                    else if (result == DialogResult.Cancel)
                    {
                        if (reportWasNull == true)
                        {
                            foreach (DispatchOrderClientData dispatchOrder in selectedDispatchOrders)
                                   dispatchOrder.EndingReport = null;
                             
                        }
                    }
                    DialogResult = DialogResult.None;
                
                
                }

            }
            EnableButtons();
        }

        private void EnableButtons()
        {
            this.simpleButtonAccept.Enabled = CheckEnabledButtons();            
        }

        private bool CheckEnabledButtons()
        {
            bool result = true;
            for (int i = 0; i < selectedDispatchOrders.Count && result == true; i++)
            {
                DispatchOrderClientData dispatchOrder = (DispatchOrderClientData)selectedDispatchOrders[i];
                result = result && CheckDispatchedOrderAnswered(dispatchOrder);
            }
            return result;
        }

        private bool CheckDispatchedOrderAnswered(DispatchOrderClientData dispatchOrder)
        {
            bool result = false;
            if (status.Name == DispatchOrderStatusClientData.Cancelled.Name)
            {
                if (string.IsNullOrEmpty(this.noteByDispatchOrder[dispatchOrder].Detail) == false)
                    result = true;
            }
            else if (status.Name == DispatchOrderStatusClientData.Delayed.Name)
            {
                if (string.IsNullOrEmpty(this.noteByDispatchOrder[dispatchOrder].Detail) == false &&
                    dispatchOrder.TemporalExpectedTime != TimeSpan.Zero &&
                    this.timeSelectorControl1.TimeSpan != TimeSpan.Zero)
                    result = true;
                else if (dispatchOrder.Unit.Status.Name.Equals("Assigned") &&
                    dispatchOrder.TemporalExpectedTime != TimeSpan.Zero &&
                    this.timeSelectorControl1.TimeSpan != TimeSpan.Zero)
                    result = true;

            }
            else if (status.Name == DispatchOrderStatusClientData.Dispatched.Name)
            {
                if (dispatchOrder.TemporalExpectedTime != TimeSpan.Zero/*dispatchOrder.ExpectedTime != TimeSpan.Zero*/)
                    result = true;
            }
            else if (status.Name == DispatchOrderStatusClientData.Closed.Name)
            {
                if (dispatchOrder.EndingReport != null)
                    result = true;
            }

            if (status.Name == DispatchOrderStatusClientData.Cancelled.Name || 
                status.Name == DispatchOrderStatusClientData.Closed.Name)
            {
                if (finalUnitStatus.ContainsKey(dispatchOrder.CustomCode) == false)
                {
                    result = false;
                }
            }
            return result;
        }

        private void buttonEstimateTime_Click(object sender, EventArgs e)
        {
            BackgroundProcessForm processForm = new BackgroundProcessForm(this,
                new MethodInfo[1] 
                { 
                    GetType().GetMethod("EstimateTime_Click_Helper", BindingFlags.NonPublic | BindingFlags.Instance) 
                },
                new object[1][] { new object[0] });

            processForm.ShowDialog();
            this.DialogResult = DialogResult.None;   
        }

        private void EstimateTime_Click_Helper()
        {
            if (selectedDispatchOrders.Count > 0)
            {
                AddressClientData address = ((DispatchOrderClientData)selectedDispatchOrders[0]).Address;
                SelectedDispatchOrders = CalculateArrivalDate(selectedDispatchOrders, address);
                EnableButtons();
            }
        }

        private void ChangeDispatchStatusForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult == DialogResult.Cancel)
            {
                foreach (DispatchOrderClientData dispatchOrder in selectedDispatchOrders)
                {
                    dispatchOrder.ExpectedTime = this.originalTimeByDispatchOrder[dispatchOrder.CustomCode];
                    dispatchOrder.TemporalExpectedTime = TimeSpan.Zero;
                    dispatchOrder.TempArrivalDate = DateTime.MinValue;
                    dispatchOrder.ArrivalDate = this.originalArrivalDate[dispatchOrder.CustomCode];
                    dispatchOrder.Unit.Status = this.originalUnitStatus[dispatchOrder.CustomCode];
                    dispatchOrder.NoteDetail = null;
                    dispatchOrder.EndingReport = null;
                }
            }
        }

        private void buttonAccept_Click(object sender, EventArgs e)
        {
            foreach (DispatchOrderClientData dispatchOrder in selectedDispatchOrders)
            {
                if (this.status.Name == DispatchOrderStatusClientData.Dispatched.Name)
                {
                    dispatchOrder.ExpectedTime = dispatchOrder.TemporalExpectedTime;
                    dispatchOrder.ArrivalDate = dispatchOrder.TempArrivalDate;
                    dispatchOrder.Status = this.status;
                }
                else if (this.status.Name == DispatchOrderStatusClientData.Cancelled.Name ||
                    this.status.Name == DispatchOrderStatusClientData.Closed.Name)
                {
                    dispatchOrder.Status = this.status;
                    dispatchOrder.Unit.Status = this.finalUnitStatus[dispatchOrder.CustomCode];
                    dispatchOrder.NoteDetail = this.noteByDispatchOrder[dispatchOrder].Detail;
                }
                else if (this.status.Name == DispatchOrderStatusClientData.Delayed.Name)
                {
                    dispatchOrder.ExpectedTime = dispatchOrder.TemporalExpectedTime;
                    dispatchOrder.ArrivalDate = dispatchOrder.TempArrivalDate;
                    dispatchOrder.NoteDetail = this.noteByDispatchOrder[dispatchOrder].Detail;
                    dispatchOrder.Status = DispatchOrderStatusClientData.Dispatched;
                }
                dispatchOrder.TemporalExpectedTime = TimeSpan.Zero;
                dispatchOrder.TempArrivalDate = DateTime.MinValue;
            }
        }

        private void LoadLanguage()
        {
            this.simpleButtonAccept.Text = ResourceLoader.GetString2("Accept");
            this.simpleButtonCancel.Text = ResourceLoader.GetString2("$Message.Cancel");
            this.groupBoxUnit.Text = ResourceLoader.GetString2("ChangeDispatchStatusForm.UnitInformation");
            this.groupBoxDispatch.Text = ResourceLoader.GetString2("ChangeDispatchStatusForm.groupBoxDispatchText");
            this.labelStatus.Text = ResourceLoader.GetString2("Status") + ":";
            this.labelComment.Text = ResourceLoader.GetString2("ReasonCancellation");
            this.labelChangeStatus.Text = ResourceLoader.GetString2("ChangeDispatchStatusForm.labelChangeStatusText");
            this.labelArrivalDate.Text = ResourceLoader.GetString2("ArrivalDate") + ":";
        }

        private void ChangeDispatchStatusForm_Load(object sender, EventArgs e)
        {
            SetSkin();
            LoadLanguage();

            if (this.status.Name.Equals(DispatchOrderStatusClientData.Cancelled.Name))
            {
                this.labelChangeStatus.Visible = true;
                this.comboBoxStatus.Visible = true;
                this.labelArrivalDate.Visible = false;
                this.labelArrivalDateText.Visible = false;
                this.labelExpectedTime.Visible = false;
                this.labelExpectedTimeText.Visible = false;
                this.timeSelectorControl1.Visible = false;
                this.timeSelectorControl1.Enabled = false;
                this.timeSelectorControl1.TabStop = false;
                this.buttonEstimateTime.Visible = false;
                this.buttonCloseReport.Visible = false;
                this.buttonCloseReport.TabStop = false;

                this.Size = new Size(520, 485);
                this.groupBoxDispatch.Size = new Size(499, 238);
                this.Text = ResourceLoader.GetString2("CancelDispatchOrder");
                this.labelComment.Text = ResourceLoader.GetString2("ReasonCancellation");
                this.labelComment.Location = new Point(this.labelComment.Location.X, this.labelExpectedTime.Location.Y);
                this.textBoxComment.Location = new Point(this.textBoxComment.Location.X, this.textBoxComment.Location.Y - 30);
                this.textBoxComment.Size = new Size(480, this.textBoxComment.Height);

                this.labelStatus.Location = new Point(6, 26);
                this.labelStatusText.Location = new Point(this.labelStatus.Location.X + 115, this.labelStatus.Location.Y);

                this.labelDepartureDate.Location = new Point(this.labelStatus.Location.X, this.labelStatus.Location.Y + 30);
                this.labelDepartureDateText.Location = new Point(this.labelDepartureDate.Location.X + 270, this.labelDepartureDate.Location.Y);
                this.labelDepartureDate.Text = ResourceLoader.GetString2("DepartureDate") + ":";

                this.labelChangeStatus.Text = ResourceLoader.GetString2("ChangeDispatchStatusForm.labelChangeStatusText");
                this.labelChangeStatus.Location = new Point(this.labelDepartureDate.Location.X, this.labelDepartureDate.Location.Y + 30);
                this.comboBoxStatus.Location = new Point(this.labelChangeStatus.Location.X + 150, this.labelChangeStatus.Location.Y - 4);

                this.labelComment.Location = new Point(this.labelChangeStatus.Location.X, this.labelChangeStatus.Location.Y + 30);
                this.textBoxComment.Location = new Point(this.labelComment.Location.X, this.labelComment.Location.Y + 20);
                this.textBoxComment.Size = new Size(480, this.textBoxComment.Height);

                this.gridViewExDispatchOrder.Columns["ExpectedTime"].Visible = false;
                this.gridViewExDispatchOrder.Columns["FinalUnitStatus"].Visible = true;
                this.gridViewExDispatchOrder.Columns["Finished"].Visible = true;
                this.gridViewExDispatchOrder.Columns["Finished"].Caption = ResourceLoader.GetString2("Canceled");
                this.gridViewExDispatchOrder.Columns["Calculable"].Visible = false;

            }
            else if (this.status.Name.Equals(DispatchOrderStatusClientData.Delayed.Name))
            {
                this.Size = new Size(520, 515);
                this.groupBoxDispatch.Size = new Size(499, 268);
                this.Text = ResourceLoader.GetString2("ChangeArrivalTime");
                this.labelComment.Text = ResourceLoader.GetString2("ReasonTimeChange");
                this.labelDepartureDate.Text = ResourceLoader.GetString2("CurrentTime") + ":";

                this.labelStatus.Location = new Point(6, 26);
                this.labelStatusText.Location = new Point(this.labelStatus.Location.X + 114, this.labelStatus.Location.Y);

                this.labelDepartureDate.Location = new Point(this.labelStatus.Location.X, this.labelStatus.Location.Y + 30);
                this.labelDepartureDateText.Location = new Point(this.labelDepartureDate.Location.X + 270, this.labelDepartureDate.Location.Y);
                this.labelDepartureDate.Text = ResourceLoader.GetString2("DepartureDate") + ":";

                this.labelArrivalDate.Location = new Point(this.labelDepartureDate.Location.X, this.labelDepartureDate.Location.Y + 30);
                this.labelArrivalDateText.Location = new Point(this.labelArrivalDate.Location.X + 270, this.labelArrivalDate.Location.Y);

                this.labelExpectedTime.Text = ResourceLoader.GetString2("EstimatedTime");
                this.labelExpectedTime.Location = new Point(this.labelArrivalDate.Location.X, this.labelArrivalDate.Location.Y + 30);
                this.timeSelectorControl1.Location = new Point(this.labelExpectedTime.Location.X + 185, this.labelExpectedTime.Location.Y - 2);

                this.labelComment.Location = new Point(this.labelExpectedTime.Location.X, this.labelExpectedTime.Location.Y + 30);
                this.textBoxComment.Location = new Point(this.labelComment.Location.X, this.labelComment.Location.Y + 20);
                this.textBoxComment.Size = new Size(480, this.textBoxComment.Height);

                this.buttonCloseReport.Visible = false;
                this.buttonCloseReport.Enabled = false;
                this.buttonCloseReport.TabStop = false;
                this.buttonEstimateTime.Visible = false;
                this.buttonEstimateTime.Enabled = false;
                this.buttonEstimateTime.TabStop = false;
                this.timeSelectorControl1.Visible = true;
                this.labelExpectedTimeText.Visible = false;
                this.labelChangeStatus.Visible = false;
                this.comboBoxStatus.Visible = false;
                this.comboBoxStatus.Enabled = false;

                this.gridViewExDispatchOrder.Columns["FinalUnitStatus"].Visible = false;
                this.gridViewExDispatchOrder.Columns["Finished"].Visible = true;
                this.gridViewExDispatchOrder.Columns["Finished"].Caption = ResourceLoader.GetString2("Changed");
                this.gridViewExDispatchOrder.Columns["Calculable"].Visible = false;

            }
            else if (this.status.Name.Equals(DispatchOrderStatusClientData.Dispatched.Name))
            {
                this.Size = new Size(520, 360);
                this.groupBoxDispatch.Size = new Size(499, 120);
                this.Text = ResourceLoader.GetString2("AssignUnits");
                this.buttonCloseReport.Visible = false;
                this.buttonCloseReport.Enabled = false;
                this.buttonCloseReport.TabStop = false;

                this.buttonEstimateTime.Visible = true;

                this.timeSelectorControl1.Visible = true;
                this.labelStatus.Visible = false;
                this.labelStatusText.Visible = false;

                this.labelStatus.Location = new Point(6, 26);

                this.labelDepartureDate.Visible = true;
                this.labelDepartureDate.Location = new Point(this.labelStatus.Location.X, this.labelStatus.Location.Y);
                this.labelDepartureDateText.Location = new Point(this.labelDepartureDate.Location.X + 270, this.labelStatus.Location.Y);
                this.labelDepartureDateText.Visible = true;
                this.labelDepartureDate.Text = ResourceLoader.GetString2("DepartureDate") + ":";

                this.labelArrivalDate.Visible = true;
                this.labelArrivalDate.Location = new Point(this.labelDepartureDate.Location.X, this.labelDepartureDate.Location.Y + 30);
                this.labelArrivalDateText.Location = new Point(this.labelArrivalDate.Location.X + 270, this.labelDepartureDateText.Location.Y + 30);
                this.labelArrivalDateText.Visible = true;

                this.labelExpectedTimeText.Visible = false;
                this.labelExpectedTime.Text = ResourceLoader.GetString2("EstimatedTime");
                this.labelExpectedTime.Visible = true;
                this.labelExpectedTime.Location = new Point(this.labelArrivalDate.Location.X, this.labelArrivalDate.Location.Y + 30);

                this.timeSelectorControl1.Visible = true;
                this.timeSelectorControl1.Location = new Point(this.labelExpectedTime.Location.X + 188, this.labelExpectedTime.Location.Y - 2);

                this.buttonEstimateTime.Location = new Point(this.buttonEstimateTime.Location.X - 6, this.timeSelectorControl1.Location.Y - 2);

                this.labelChangeStatus.Visible = false;
                this.comboBoxStatus.Visible = false;
                this.comboBoxStatus.Enabled = false;
                this.comboBoxStatus.TabStop = false;
                this.labelComment.Visible = false;
                this.textBoxComment.Hide();
                this.textBoxComment.Visible = false;

                this.gridViewExDispatchOrder.Columns["FinalUnitStatus"].Visible = false;
                this.gridViewExDispatchOrder.Columns["Finished"].Visible = false;
                this.gridViewExDispatchOrder.Columns["Calculable"].Visible = true;                
            }
            else if (this.status.Name.Equals(DispatchOrderStatusClientData.Closed.Name))
            {
                this.Size = new Size(520, 358);
                this.groupBoxDispatch.Size = new Size(499, 114);
                this.Text = ResourceLoader.GetString2("EndDispatchOrder");
                this.buttonCloseReport.Visible = true;
                this.labelDepartureDate.Visible = true;
                this.labelDepartureDateText.Visible = true;
                this.buttonEstimateTime.Visible = false;
                this.buttonEstimateTime.Enabled = false;
                this.buttonEstimateTime.TabStop = false;
                this.textBoxComment.Hide();
                this.timeSelectorControl1.Visible = false;
                this.timeSelectorControl1.Enabled = false;
                this.timeSelectorControl1.TabStop = false;
                this.labelExpectedTime.Visible = false;
                this.labelExpectedTimeText.Visible = false;
                this.labelArrivalDate.Visible = true;
                this.labelArrivalDateText.Visible = true;
                this.labelStatus.Visible = false;
                this.labelStatusText.Visible = false;

                this.labelStatus.Location = new Point(6, 26);
                this.labelDepartureDate.Location = this.labelStatus.Location;
                this.labelDepartureDateText.Location = new Point(this.labelDepartureDate.Location.X + 270, this.labelStatus.Location.Y);
                this.labelDepartureDate.Text = ResourceLoader.GetString2("DepartureDate") + ":";

                this.labelArrivalDate.Location = new Point(this.labelDepartureDate.Location.X, this.labelDepartureDate.Location.Y + 30);
                this.labelArrivalDateText.Location = new Point(this.labelArrivalDate.Location.X + 270, this.labelArrivalDate.Location.Y);

                this.labelChangeStatus.Location = new Point(this.labelArrivalDate.Location.X, this.labelArrivalDate.Location.Y + 30);
                this.comboBoxStatus.Location = new Point(this.labelChangeStatus.Location.X + 150, this.labelChangeStatus.Location.Y - 4);

                this.buttonCloseReport.Location = new Point(this.comboBoxStatus.Location.X + 285, this.comboBoxStatus.Location.Y - 6);

                this.gridViewExDispatchOrder.Columns["ExpectedTime"].Visible = true;
                this.gridViewExDispatchOrder.Columns["ExpectedTime"].Caption = ResourceLoader.GetString2("AttentionTime");
                this.gridViewExDispatchOrder.Columns["FinalUnitStatus"].Visible = true;
                if (isByIncidentNotification == true)
                    gridViewExDispatchOrder.Columns["Finished"].Visible = false;
                else
                    gridViewExDispatchOrder.Columns["Finished"].Visible = true;
               
                this.gridViewExDispatchOrder.Columns["Calculable"].Visible = false;
                this.gridControlExDispatchOrder.KeyDown += new KeyEventHandler(OpenCloseReport);
                this.gridControlExDispatchOrder.MouseDoubleClick += new MouseEventHandler(gridControlExDispatchOrder_MouseDoubleClick);                
            }
            gridControlExDispatchOrder.AutoAdjustColumnWidth();
        }

        void gridControlExDispatchOrder_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            GridControlEx gridControl = sender as GridControlEx;
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo info = ((GridViewEx)gridControl.MainView).CalcHitInfo(e.X, e.Y);
            if (info.InRow)
            {
                buttonCloseReport_Click(null, null);
            }
        }

        private void comboBoxStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.currentDispatchOrder != null)
            {
                if (this.finalUnitStatus.ContainsKey(this.currentDispatchOrder.CustomCode) == true)
                {
                    this.finalUnitStatus[currentDispatchOrder.CustomCode] = comboBoxStatus.SelectedItem as UnitStatusClientData;
                }
                else
                {
                    this.finalUnitStatus.Add(currentDispatchOrder.CustomCode, comboBoxStatus.SelectedItem as UnitStatusClientData);
                }
                if (currentDataGridData != null && comboBoxStatus.SelectedItem != null)
                {
                    ((GridControlDataCreateOrChangeDispatchOrder)currentDataGridData).FinalUnitStatus = ((UnitStatusClientData)comboBoxStatus.SelectedItem).FriendlyName;
                    gridControlExDispatchOrder.AddOrUpdateItem(currentDataGridData);                    
                }
            }
        }

        private readonly TimeSpan maxTime = new TimeSpan(99, 59, 59);
        private readonly double minTime = 0.009d;
        
        private IList CalculateArrivalDate(IList dispatchOrders, AddressClientData incidentAdress)
        {
            if (dispatchOrders != null && incidentAdress != null && incidentAdress.IsSynchronized == true)
            {
                GeoPoint addressPoint = new GeoPoint(incidentAdress.Lon, incidentAdress.Lat);
                List<GeoPoint> dispatchOrderPoints = new List<GeoPoint>(dispatchOrders.Count);

                for (int i = 0; i < dispatchOrders.Count; i++)
                {
                    GeoPoint dispatchOrderPoint;

                    DispatchOrderClientData dispatchOrder = (DispatchOrderClientData)dispatchOrders[i];

                    if (dispatchOrder.Unit.Lon != 0 && dispatchOrder.Unit.Lat != 0)
                    {
                        dispatchOrderPoint = new GeoPoint(dispatchOrder.Unit.Lon, dispatchOrder.Unit.Lat);
                    }
                    else
                    {
                        dispatchOrderPoint = new GeoPoint(0, 0);
                    }

                    dispatchOrderPoints.Add(dispatchOrderPoint);
                }

                //This line was changed because the method was commented. AA
                //IList distances = GisServiceUtil.Distance(addressPoint, dispatchOrderPoints, DistanceUnit.Kilometers);
                IList<double> distances = GisServiceUtil.GetDistances(addressPoint, dispatchOrderPoints);

                for (int i = 0; i < dispatchOrders.Count; i++)
                {
                    double distance = distances[i];
                    DispatchOrderClientData dispatchOrder = (DispatchOrderClientData)dispatchOrders[i];

                    double time = distance / dispatchOrder.Unit.Velocity;
                    if (time <= minTime && dispatchOrder.Unit.Lon != 0 && dispatchOrder.Unit.Lat != 0)
                    {
                        time = minTime;
                    }
                    TimeSpan timeSpan = ApplicationUtil.TimeSpanFromDouble(time);
                    if (timeSpan.TotalHours > maxTime.TotalHours)
                    {
                        timeSpan = maxTime;
                    }
                    dispatchOrder.TemporalExpectedTime = timeSpan;
                    dispatchOrder.TempArrivalDate = ServerServiceClient.GetInstance().GetTime().Add(dispatchOrder.TemporalExpectedTime);
                }
            }

            return dispatchOrders;
        }

        private void buttonEstimateTime_MouseEnter(object sender, EventArgs e)
        {
            this.toolTip.SetToolTip(this.buttonEstimateTime, ResourceLoader.GetString2("CalculateEstimatedTime"));
        }        

        private void buttonCloseReport_MouseEnter(object sender, EventArgs e)
        {
            this.toolTip.SetToolTip(this.buttonCloseReport, ResourceLoader.GetString2("EndDispatchReport"));
        }

        private void buttonEstimateTime_MouseLeave(object sender, EventArgs e)
        {
            this.toolTip.Hide(this.buttonEstimateTime);
        }

        private void buttonCloseReport_MouseLeave(object sender, EventArgs e)
        {
            this.toolTip.Hide(this.buttonCloseReport);
        }

        private object syncHash = new object();
        public void SynchronizeList(DispatchOrderClientData dispatchOrder)
        {
            try
            {
                DispatchOrderClientData previousDispatch = null;
                lock (syncHash)
                {
                    if (dispatchOrder != null)
                    {
                        bool found = false;
                        int index = -1;
                        for (int i = 0; i < this.SelectedDispatchOrders.Count && !found; i++)
                        {
                            DispatchOrderClientData dispatch = this.SelectedDispatchOrders[i] as DispatchOrderClientData;
                            if (dispatch.CustomCode.Equals(dispatchOrder.CustomCode))
                            {
                                found = true;
                                index = i;
                            }
                        }
                        if (index != -1)
                        {
                            previousDispatch = this.SelectedDispatchOrders[index] as DispatchOrderClientData;                            
                            this.SelectedDispatchOrders[index] = dispatchOrder;
                            this.SelectedDispatchOrders = this.SelectedDispatchOrders;
                        }
                    }

                    
                }
            }
            catch
            {
            }
        }

        private void SetSkin()
        {
            try
            {
                string skinFile = SmartCadConfiguration.SkinsFolder + "/Clasic.xml";

                SkinEngine skinEngine = SkinEngine.Load(skinFile);

                ProfessionalColorTable colorTable = new CustomToolStripColors(
                    skinEngine.Colors["ToolStripGradientBegin"],
                    skinEngine.Colors["ToolStripGradientMiddle"],
                    skinEngine.Colors["ToolStripGradientEnd"],

                    skinEngine.Colors["MenuStripGradientBegin"],
                    skinEngine.Colors["MenuStripGradientEnd"],

                    skinEngine.Colors["StatusStripGradientBegin"],
                    skinEngine.Colors["StatusStripGradientEnd"]
                );

                skinEngine.ApplyChanges();
            }
            catch (Exception ex)
            {
                MessageForm.Show(ResourceLoader.GetString2("$Message.Error"), ResourceLoader.GetString2("ErrorLoadingSkin"), ex);
            }
        }

        private void OpenCloseReport(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                buttonCloseReport_Click(null, null);
            }
        }

        private void gridControlExDispatchOrder_KeyDown(object sender, KeyEventArgs e)
        {
            EnableButtons();
            if (e.KeyValue == (int)Keys.Enter && this.simpleButtonAccept.Enabled == true)
            {
                buttonAccept_Click(null, null);
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void gridViewExDispatchOrder_SelectionWillChange(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (this.gridControlExDispatchOrder.SelectedItems.Count > 0)
            {
                buttonCloseReport.Enabled = true;
                comboBoxStatus.Enabled = true;
                currentDataGridData = (GridControlDataCreateOrChangeDispatchOrder)this.gridControlExDispatchOrder.SelectedItems[0];
                currentDispatchOrder = ((GridControlDataCreateOrChangeDispatchOrder)this.gridControlExDispatchOrder.SelectedItems[0]).DispatchOrder;
                labelStatusText.Text = currentDispatchOrder.Unit.Status.FriendlyName;
                labelDepartureDateText.Text = ServerServiceClient.GetInstance().GetTime().ToString("MM/dd/yyyy HH:mm:ss", CURRENT_CULTURE);
                labelDepartureDateText.Tag = ServerServiceClient.GetInstance().GetTime();
                timeSelectorControl1.Enabled = true;
                textBoxComment.Enabled = true;
                labelComment.Enabled = true;
                if (this.status.Name == DispatchOrderStatusClientData.Dispatched.Name)
                {
                    timeSelectorControl1.TimeChanged -= new EventHandler(timeSelectorControl1_TimeChanged);
                    timeSelectorControl1.TimeSpan = currentDispatchOrder.TemporalExpectedTime;
                    timeSelectorControl1.TimeChanged += new EventHandler(timeSelectorControl1_TimeChanged);
                }
                else if (this.status.Name == DispatchOrderStatusClientData.Closed.Name ||
                    this.status.Name == DispatchOrderStatusClientData.Cancelled.Name)
                {
                    labelDepartureDateText.Text = currentDispatchOrder.StartDate.ToString("MM/dd/yyyy HH:mm:ss", CURRENT_CULTURE);
                    labelDepartureDateText.Tag = currentDispatchOrder.StartDate;
                    if (finalUnitStatus.ContainsKey(currentDispatchOrder.CustomCode) == true)
                        comboBoxStatus.SelectedItem = finalUnitStatus[currentDispatchOrder.CustomCode];
                    else
                        comboBoxStatus.SelectedIndex = 0;
                    ((GridControlDataCreateOrChangeDispatchOrder)currentDataGridData).FinalUnitStatus = ((UnitStatusClientData)comboBoxStatus.SelectedItem).FriendlyName;
                }
                if (currentDispatchOrder.TempArrivalDate != DateTime.MinValue)
                {
                    if (this.status.Name != DispatchOrderStatusClientData.Delayed.Name)
                    {
                        labelArrivalDateText.Text = currentDispatchOrder.TempArrivalDate.ToString("MM/dd/yyyy HH:mm:ss", CURRENT_CULTURE);
                        labelArrivalDateText.Tag = currentDispatchOrder.TempArrivalDate;
                    }
                    else
                    {
                        if (currentDispatchOrder.Unit.Status.Name.Equals("Assigned"))
                        {
                            textBoxComment.Enabled = false;
                            labelComment.Enabled = false;
                        }
                        else
                        {
                            this.textBoxComment.TextChanged -= new EventHandler(textBoxComment_TextChanged);
                            this.textBoxComment.Clear();
                            this.textBoxComment.TextChanged += new EventHandler(textBoxComment_TextChanged);
                            labelArrivalDateText.Text = ServerServiceClient.GetInstance().GetTime().ToString("MM/dd/yyyy HH:mm:ss", CURRENT_CULTURE);
                            labelArrivalDateText.Tag = ServerServiceClient.GetInstance().GetTime();
                        }
                        timeSelectorControl1.TimeChanged -= new EventHandler(timeSelectorControl1_TimeChanged);
                        timeSelectorControl1.TimeSpan = currentDispatchOrder.TemporalExpectedTime;
                        timeSelectorControl1.TimeChanged += new EventHandler(timeSelectorControl1_TimeChanged);
                    }
                }
                else
                {
                    labelArrivalDateText.Text = ServerServiceClient.GetInstance().GetTime().ToString("MM/dd/yyyy HH:mm:ss", CURRENT_CULTURE);
                    labelArrivalDateText.Tag = ServerServiceClient.GetInstance().GetTime();
                }
                if (currentDispatchOrder.TemporalExpectedTime != TimeSpan.Zero)
                    labelExpectedTimeText.Text = currentDispatchOrder.TemporalExpectedTime.Hours
                        + ":" + currentDispatchOrder.TemporalExpectedTime.Minutes;

                if (string.IsNullOrEmpty(this.noteByDispatchOrder[currentDispatchOrder].Detail) == false)
                {
                    if (currentDispatchOrder.Unit.Status.Name.Equals("Assigned") == false &&
                        this.status.Name.Equals("Delayed") == false)
                    {
                        textBoxComment.Text = this.noteByDispatchOrder[currentDispatchOrder].Detail;
                    }
                    else if (currentDispatchOrder.Unit.Status.Name.Equals("Delayed") == true &&
                        this.status.Name.Equals("Delayed") == true)
                    {
                        textBoxComment.Text = this.noteByDispatchOrder[currentDispatchOrder].Detail;
                    }
                    else if (this.status.Name.Equals("Cancelled") == true)
                    {
                        textBoxComment.Text = this.noteByDispatchOrder[currentDispatchOrder].Detail;
                    }
                }
                else
                {
                    textBoxComment.Text = string.Empty;
                }
                EnableButtons();
            }
            else
            {
                currentDataGridData = null;
                currentDispatchOrder = null;
                labelStatusText.Text = string.Empty;
                labelDepartureDateText.Text = string.Empty;
                labelArrivalDateText.Text = string.Empty;
                labelExpectedTimeText.Text = string.Empty;
                timeSelectorControl1.Reset();
                timeSelectorControl1.Enabled = false;
                buttonCloseReport.Enabled = false;
                comboBoxStatus.Enabled = false;
                textBoxComment.Clear();
                textBoxComment.Enabled = false;
                labelComment.Enabled = false;
            }
        }
    }
}
