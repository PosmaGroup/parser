using DevExpress.XtraEditors;
namespace SmartCadDispatch.Gui
{
	partial class DispatchFormDevX : XtraForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DispatchFormDevX));
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barButtonItemHelp = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemRefreshOrder = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemPending = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemModifyZone = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemClose = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemAvailable = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemOutOfOrder = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemUnavailable = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemOnScene = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemAssignedOnScene = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemModifyTime = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemEnd = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemChat = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemOpenMap = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemStatus = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemAddNote = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemSupportRequest = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemPrint = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemSave = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemLogo = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroupDispatchOrder = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupUnits = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupAssignUnits = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupTools = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupUser = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupGeneralOptions = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupApplications = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.repositoryItemPopupContainerEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.ribbonStatusBar1 = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.labelUser = new DevExpress.XtraBars.BarStaticItem();
            this.labelConnected = new DevExpress.XtraBars.BarStaticItem();
            this.labelDate = new DevExpress.XtraBars.BarStaticItem();
            this.alertControlChat = new DevExpress.XtraBars.Alerter.AlertControl(this.components);
            this.xtraTabbedMdiManager1 = new DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(this.components);
            this.labelLogo = new System.Windows.Forms.Label();
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.ribbonPage2 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroupCallInfo = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.barButtonItemStartRegIncident = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemCreateNewIncident = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemCancelIncident = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemHistory = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem5 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem6 = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.BackColor = System.Drawing.Color.White;
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.ExpandCollapseItem.Name = "";
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.barButtonItemHelp,
            this.barButtonItemRefreshOrder,
            this.barButtonItemPending,
            this.barButtonItemModifyZone,
            this.barButtonItemClose,
            this.barButtonItemAvailable,
            this.barButtonItemOutOfOrder,
            this.barButtonItemUnavailable,
            this.barButtonItemOnScene,
            this.barButtonItemAssignedOnScene,
            this.barButtonItemModifyTime,
            this.barButtonItemEnd,
            this.barButtonItemChat,
            this.barButtonItemOpenMap,
            this.barButtonItemStatus,
            this.barButtonItemAddNote,
            this.barButtonItemSupportRequest,
            this.barButtonItemPrint,
            this.barButtonItemSave,
            this.barButtonItemLogo});
            this.ribbonControl1.ItemsVertAlign = DevExpress.Utils.VertAlignment.Top;
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 45;
            this.ribbonControl1.MdiMergeStyle = DevExpress.XtraBars.Ribbon.RibbonMdiMergeStyle.Always;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.PageHeaderItemLinks.Add(this.barButtonItemHelp);
            this.ribbonControl1.PageHeaderItemLinks.Add(this.barButtonItemLogo);
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbonControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPopupContainerEdit1});
            this.ribbonControl1.ShowToolbarCustomizeItem = false;
            this.ribbonControl1.Size = new System.Drawing.Size(1280, 119);
            this.ribbonControl1.StatusBar = this.ribbonStatusBar1;
            this.ribbonControl1.Toolbar.ShowCustomizeItem = false;
            this.ribbonControl1.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            // 
            // barButtonItemHelp
            // 
            this.barButtonItemHelp.Caption = "Help";
            this.barButtonItemHelp.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemHelp.Glyph")));
            this.barButtonItemHelp.Id = 8;
            this.barButtonItemHelp.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F1);
            this.barButtonItemHelp.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemHelp.Name = "barButtonItemHelp";
            this.barButtonItemHelp.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemHelp_ItemClick);
            // 
            // barButtonItemRefreshOrder
            // 
            this.barButtonItemRefreshOrder.Caption = "ItemRefreshOrder";
            this.barButtonItemRefreshOrder.Id = 16;
            this.barButtonItemRefreshOrder.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemRefreshOrder.Name = "barButtonItemRefreshOrder";
            // 
            // barButtonItemPending
            // 
            this.barButtonItemPending.Caption = "ItemPending";
            this.barButtonItemPending.Id = 17;
            this.barButtonItemPending.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemPending.Name = "barButtonItemPending";
            this.barButtonItemPending.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemPending_ItemClick);
            // 
            // barButtonItemModifyZone
            // 
            this.barButtonItemModifyZone.Caption = "ItemModifyZone";
            this.barButtonItemModifyZone.Id = 18;
            this.barButtonItemModifyZone.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemModifyZone.Name = "barButtonItemModifyZone";
            // 
            // barButtonItemClose
            // 
            this.barButtonItemClose.Caption = "ItemClose";
            this.barButtonItemClose.Id = 19;
            this.barButtonItemClose.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemClose.Name = "barButtonItemClose";
            // 
            // barButtonItemAvailable
            // 
            this.barButtonItemAvailable.Caption = "ItemAvailable";
            this.barButtonItemAvailable.Id = 21;
            this.barButtonItemAvailable.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemAvailable.Name = "barButtonItemAvailable";
            // 
            // barButtonItemOutOfOrder
            // 
            this.barButtonItemOutOfOrder.Caption = "ItemOutOfOrder";
            this.barButtonItemOutOfOrder.Id = 22;
            this.barButtonItemOutOfOrder.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemOutOfOrder.Name = "barButtonItemOutOfOrder";
            // 
            // barButtonItemUnavailable
            // 
            this.barButtonItemUnavailable.Caption = "ItemUnavailable";
            this.barButtonItemUnavailable.Id = 23;
            this.barButtonItemUnavailable.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemUnavailable.Name = "barButtonItemUnavailable";
            // 
            // barButtonItemOnScene
            // 
            this.barButtonItemOnScene.Caption = "ItemOnScene";
            this.barButtonItemOnScene.Id = 24;
            this.barButtonItemOnScene.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemOnScene.Name = "barButtonItemOnScene";
            // 
            // barButtonItemAssignedOnScene
            // 
            this.barButtonItemAssignedOnScene.Caption = "ItemAssignedOnScene";
            this.barButtonItemAssignedOnScene.Id = 25;
            this.barButtonItemAssignedOnScene.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemAssignedOnScene.Name = "barButtonItemAssignedOnScene";
            // 
            // barButtonItemModifyTime
            // 
            this.barButtonItemModifyTime.Caption = "ItemModifyTime";
            this.barButtonItemModifyTime.Id = 26;
            this.barButtonItemModifyTime.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemModifyTime.Name = "barButtonItemModifyTime";
            // 
            // barButtonItemEnd
            // 
            this.barButtonItemEnd.Caption = "ItemEnd";
            this.barButtonItemEnd.Id = 27;
            this.barButtonItemEnd.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemEnd.Name = "barButtonItemEnd";
            // 
            // barButtonItemChat
            // 
            this.barButtonItemChat.Caption = "ItemChat";
            this.barButtonItemChat.Glyph = global::SmartCadDispatch.Properties.Resources.chat;
            this.barButtonItemChat.Id = 28;
            this.barButtonItemChat.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemChat.Name = "barButtonItemChat";
            this.barButtonItemChat.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItemChat.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemChat_ItemClick);
            // 
            // barButtonItemOpenMap
            // 
            this.barButtonItemOpenMap.Caption = "ItemOpenMap";
            this.barButtonItemOpenMap.Id = 29;
            this.barButtonItemOpenMap.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemOpenMap.Name = "barButtonItemOpenMap";
            this.barButtonItemOpenMap.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItemOpenMap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemOpenMap_ItemClick);
            // 
            // barButtonItemStatus
            // 
            this.barButtonItemStatus.ActAsDropDown = true;
            this.barButtonItemStatus.AllowAllUp = true;
            this.barButtonItemStatus.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.barButtonItemStatus.Caption = "ItemStatus";
            this.barButtonItemStatus.Id = 35;
            this.barButtonItemStatus.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemStatus.Name = "barButtonItemStatus";
            this.barButtonItemStatus.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonItemAddNote
            // 
            this.barButtonItemAddNote.Caption = "ItemAddNote";
            this.barButtonItemAddNote.Id = 36;
            this.barButtonItemAddNote.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemAddNote.Name = "barButtonItemAddNote";
            // 
            // barButtonItemSupportRequest
            // 
            this.barButtonItemSupportRequest.Caption = "ItemSupportRequest";
            this.barButtonItemSupportRequest.Id = 37;
            this.barButtonItemSupportRequest.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemSupportRequest.Name = "barButtonItemSupportRequest";
            // 
            // barButtonItemPrint
            // 
            this.barButtonItemPrint.Caption = "ItemPrint";
            this.barButtonItemPrint.Id = 40;
            this.barButtonItemPrint.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemPrint.Name = "barButtonItemPrint";
            // 
            // barButtonItemSave
            // 
            this.barButtonItemSave.Caption = "ItemSave";
            this.barButtonItemSave.Id = 41;
            this.barButtonItemSave.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemSave.Name = "barButtonItemSave";
            // 
            // barButtonItemLogo
            // 
            this.barButtonItemLogo.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemLogo.Glyph")));
            this.barButtonItemLogo.Id = 44;
            this.barButtonItemLogo.Name = "barButtonItemLogo";
            this.barButtonItemLogo.SmallWithoutTextWidth = 57;
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupDispatchOrder,
            this.ribbonPageGroupUnits,
            this.ribbonPageGroupAssignUnits,
            this.ribbonPageGroupTools,
            this.ribbonPageGroupUser,
            this.ribbonPageGroupGeneralOptions,
            this.ribbonPageGroupApplications});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "ribbonPage1";
            // 
            // ribbonPageGroupDispatchOrder
            // 
            this.ribbonPageGroupDispatchOrder.AllowMinimize = false;
            this.ribbonPageGroupDispatchOrder.AllowTextClipping = false;
            this.ribbonPageGroupDispatchOrder.ItemLinks.Add(this.barButtonItemPending);
            this.ribbonPageGroupDispatchOrder.ItemLinks.Add(this.barButtonItemModifyZone);
            this.ribbonPageGroupDispatchOrder.ItemLinks.Add(this.barButtonItemSupportRequest);
            this.ribbonPageGroupDispatchOrder.ItemLinks.Add(this.barButtonItemClose);
            this.ribbonPageGroupDispatchOrder.Name = "ribbonPageGroupDispatchOrder";
            this.ribbonPageGroupDispatchOrder.ShowCaptionButton = false;
            this.ribbonPageGroupDispatchOrder.Text = "ribbonPageGroupDispatchOrder";
            // 
            // ribbonPageGroupUnits
            // 
            this.ribbonPageGroupUnits.AllowMinimize = false;
            this.ribbonPageGroupUnits.AllowTextClipping = false;
            this.ribbonPageGroupUnits.ItemLinks.Add(this.barButtonItemAvailable);
            this.ribbonPageGroupUnits.ItemLinks.Add(this.barButtonItemOutOfOrder);
            this.ribbonPageGroupUnits.ItemLinks.Add(this.barButtonItemUnavailable);
            this.ribbonPageGroupUnits.ItemLinks.Add(this.barButtonItemOnScene);
            this.ribbonPageGroupUnits.Name = "ribbonPageGroupUnits";
            this.ribbonPageGroupUnits.ShowCaptionButton = false;
            this.ribbonPageGroupUnits.Text = "ribbonPageGroupUnits";
            // 
            // ribbonPageGroupAssignUnits
            // 
            this.ribbonPageGroupAssignUnits.AllowMinimize = false;
            this.ribbonPageGroupAssignUnits.AllowTextClipping = false;
            this.ribbonPageGroupAssignUnits.ItemLinks.Add(this.barButtonItemAssignedOnScene);
            this.ribbonPageGroupAssignUnits.ItemLinks.Add(this.barButtonItemModifyTime);
            this.ribbonPageGroupAssignUnits.ItemLinks.Add(this.barButtonItemAddNote);
            this.ribbonPageGroupAssignUnits.ItemLinks.Add(this.barButtonItemEnd);
            this.ribbonPageGroupAssignUnits.Name = "ribbonPageGroupAssignUnits";
            this.ribbonPageGroupAssignUnits.ShowCaptionButton = false;
            this.ribbonPageGroupAssignUnits.Text = "ribbonPageGroupAssignUnits";
            // 
            // ribbonPageGroupTools
            // 
            this.ribbonPageGroupTools.ItemLinks.Add(this.barButtonItemChat);
            this.ribbonPageGroupTools.Name = "ribbonPageGroupTools";
            this.ribbonPageGroupTools.ShowCaptionButton = false;
            this.ribbonPageGroupTools.Text = "ribbonPageGroupTools";
            // 
            // ribbonPageGroupUser
            // 
            this.ribbonPageGroupUser.AllowMinimize = false;
            this.ribbonPageGroupUser.AllowTextClipping = false;
            this.ribbonPageGroupUser.ItemLinks.Add(this.barButtonItemStatus, true, "", "", true);
            this.ribbonPageGroupUser.Name = "ribbonPageGroupUser";
            this.ribbonPageGroupUser.ShowCaptionButton = false;
            this.ribbonPageGroupUser.Text = "ribbonPageGroupUser";
            // 
            // ribbonPageGroupGeneralOptions
            // 
            this.ribbonPageGroupGeneralOptions.AllowMinimize = false;
            this.ribbonPageGroupGeneralOptions.AllowTextClipping = false;
            this.ribbonPageGroupGeneralOptions.ItemLinks.Add(this.barButtonItemPrint);
            this.ribbonPageGroupGeneralOptions.ItemLinks.Add(this.barButtonItemSave);
            this.ribbonPageGroupGeneralOptions.ItemLinks.Add(this.barButtonItemRefreshOrder);
            this.ribbonPageGroupGeneralOptions.Name = "ribbonPageGroupGeneralOptions";
            this.ribbonPageGroupGeneralOptions.ShowCaptionButton = false;
            this.ribbonPageGroupGeneralOptions.Text = "ribbonPageGroupGeneralOptions";
            // 
            // ribbonPageGroupApplications
            // 
            this.ribbonPageGroupApplications.AllowMinimize = false;
            this.ribbonPageGroupApplications.AllowTextClipping = false;
            this.ribbonPageGroupApplications.ItemLinks.Add(this.barButtonItemOpenMap);
            this.ribbonPageGroupApplications.Name = "ribbonPageGroupApplications";
            this.ribbonPageGroupApplications.ShowCaptionButton = false;
            this.ribbonPageGroupApplications.Text = "ribbonPageGroupApplications";
            // 
            // repositoryItemPopupContainerEdit1
            // 
            this.repositoryItemPopupContainerEdit1.AutoHeight = false;
            this.repositoryItemPopupContainerEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit1.Name = "repositoryItemPopupContainerEdit1";
            // 
            // ribbonStatusBar1
            // 
            this.ribbonStatusBar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ribbonStatusBar1.ItemLinks.Add(this.labelUser);
            this.ribbonStatusBar1.ItemLinks.Add(this.labelConnected);
            this.ribbonStatusBar1.ItemLinks.Add(this.labelDate);
            this.ribbonStatusBar1.Location = new System.Drawing.Point(0, 727);
            this.ribbonStatusBar1.Name = "ribbonStatusBar1";
            this.ribbonStatusBar1.Ribbon = this.ribbonControl1;
            this.ribbonStatusBar1.Size = new System.Drawing.Size(1280, 23);
            // 
            // labelUser
            // 
            this.labelUser.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUser.Appearance.Options.UseFont = true;
            this.labelUser.Caption = "Usuario";
            this.labelUser.Id = 26;
            this.labelUser.Name = "labelUser";
            this.labelUser.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // labelConnected
            // 
            this.labelConnected.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelConnected.Appearance.Options.UseFont = true;
            this.labelConnected.Caption = "Connected";
            this.labelConnected.Id = 25;
            this.labelConnected.Name = "labelConnected";
            this.labelConnected.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // labelDate
            // 
            this.labelDate.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.labelDate.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDate.Appearance.Options.UseFont = true;
            this.labelDate.Caption = "Date";
            this.labelDate.Id = 27;
            this.labelDate.Name = "labelDate";
            this.labelDate.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // alertControlChat
            // 
            this.alertControlChat.LookAndFeel.SkinName = "Blue";
            this.alertControlChat.LookAndFeel.UseDefaultLookAndFeel = false;
            this.alertControlChat.AlertClick += new DevExpress.XtraBars.Alerter.AlertClickEventHandler(this.alertControlChat_AlertClick);
            // 
            // xtraTabbedMdiManager1
            // 
            this.xtraTabbedMdiManager1.MdiParent = this;
            // 
            // labelLogo
            // 
            this.labelLogo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLogo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.labelLogo.Image = ((System.Drawing.Image)(resources.GetObject("labelLogo.Image")));
            this.labelLogo.Location = new System.Drawing.Point(1223, 2);
            this.labelLogo.Name = "labelLogo";
            this.labelLogo.Size = new System.Drawing.Size(57, 22);
            this.labelLogo.TabIndex = 2;
            // 
            // defaultLookAndFeel1
            // 
            this.defaultLookAndFeel1.LookAndFeel.SkinName = "Blue";
            // 
            // ribbonPage2
            // 
            this.ribbonPage2.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupCallInfo,
            this.ribbonPageGroup1,
            this.ribbonPageGroup2,
            this.ribbonPageGroup3});
            this.ribbonPage2.Name = "ribbonPage2";
            this.ribbonPage2.Text = "ribbonPage1";
            // 
            // ribbonPageGroupCallInfo
            // 
            this.ribbonPageGroupCallInfo.AllowMinimize = false;
            this.ribbonPageGroupCallInfo.AllowTextClipping = false;
            this.ribbonPageGroupCallInfo.ItemLinks.Add(this.barButtonItemStartRegIncident);
            this.ribbonPageGroupCallInfo.ItemLinks.Add(this.barButtonItemCreateNewIncident, true);
            this.ribbonPageGroupCallInfo.ItemLinks.Add(this.barButtonItemCancelIncident);
            this.ribbonPageGroupCallInfo.Name = "ribbonPageGroupCallInfo";
            this.ribbonPageGroupCallInfo.ShowCaptionButton = false;
            this.ribbonPageGroupCallInfo.Text = "ribbonPageGroupCallInfo";
            // 
            // barButtonItemStartRegIncident
            // 
            this.barButtonItemStartRegIncident.Caption = "ItemStartRegIncident";
            this.barButtonItemStartRegIncident.Enabled = false;
            this.barButtonItemStartRegIncident.Id = 12;
            this.barButtonItemStartRegIncident.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemStartRegIncident.LargeGlyph")));
            this.barButtonItemStartRegIncident.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemStartRegIncident.Name = "barButtonItemStartRegIncident";
            this.barButtonItemStartRegIncident.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonItemCreateNewIncident
            // 
            this.barButtonItemCreateNewIncident.Caption = "ItemCreateNewIncident";
            this.barButtonItemCreateNewIncident.Enabled = false;
            this.barButtonItemCreateNewIncident.Id = 13;
            this.barButtonItemCreateNewIncident.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemCreateNewIncident.LargeGlyph")));
            this.barButtonItemCreateNewIncident.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemCreateNewIncident.Name = "barButtonItemCreateNewIncident";
            this.barButtonItemCreateNewIncident.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonItemCancelIncident
            // 
            this.barButtonItemCancelIncident.Caption = "ItemCancelIncident";
            this.barButtonItemCancelIncident.Enabled = false;
            this.barButtonItemCancelIncident.Id = 15;
            this.barButtonItemCancelIncident.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemCancelIncident.LargeGlyph")));
            this.barButtonItemCancelIncident.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemCancelIncident.Name = "barButtonItemCancelIncident";
            this.barButtonItemCancelIncident.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.AllowMinimize = false;
            this.ribbonPageGroup1.AllowTextClipping = false;
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItem1);
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItemHistory);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.ShowCaptionButton = false;
            this.ribbonPageGroup1.Text = "Applications";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "ItemChat";
            this.barButtonItem1.Id = 5;
            this.barButtonItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.LargeGlyph")));
            this.barButtonItem1.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItem1.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barButtonItemHistory
            // 
            this.barButtonItemHistory.Caption = "History";
            this.barButtonItemHistory.Id = 21;
            this.barButtonItemHistory.LargeGlyph = global::SmartCadDispatch.Properties.Resources.Historial;
            this.barButtonItemHistory.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemHistory.Name = "barButtonItemHistory";
            this.barButtonItemHistory.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.AllowMinimize = false;
            this.ribbonPageGroup2.AllowTextClipping = false;
            this.ribbonPageGroup2.ItemLinks.Add(this.barButtonItem2);
            this.ribbonPageGroup2.ItemLinks.Add(this.barButtonItem3);
            this.ribbonPageGroup2.ItemLinks.Add(this.barButtonItemRefresh);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.ShowCaptionButton = false;
            this.ribbonPageGroup2.Text = "ribbonPageGroupGeneralOptions";
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "ItemPrint";
            this.barButtonItem2.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem2.Glyph")));
            this.barButtonItem2.Id = 6;
            this.barButtonItem2.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "ItemSave";
            this.barButtonItem3.Glyph = global::SmartCadDispatch.Gui.PrintRibbonControllerResources.RibbonPrintPreview_Save;
            this.barButtonItem3.Id = 7;
            this.barButtonItem3.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // barButtonItemRefresh
            // 
            this.barButtonItemRefresh.Caption = "ItemRefresh";
            this.barButtonItemRefresh.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemRefresh.Glyph")));
            this.barButtonItemRefresh.Id = 8;
            this.barButtonItemRefresh.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemRefresh.Name = "barButtonItemRefresh";
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.AllowMinimize = false;
            this.ribbonPageGroup3.AllowTextClipping = false;
            this.ribbonPageGroup3.ItemLinks.Add(this.barButtonItem4);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.ShowCaptionButton = false;
            this.ribbonPageGroup3.Text = "ribbonPageGroupTools";
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "ItemOpenMap";
            this.barButtonItem4.Id = 4;
            this.barButtonItem4.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem4.LargeGlyph")));
            this.barButtonItem4.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItem4.Name = "barButtonItem4";
            this.barButtonItem4.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonItem5
            // 
            this.barButtonItem5.Caption = "barButtonItemHelp";
            this.barButtonItem5.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem5.Glyph")));
            this.barButtonItem5.Id = 0;
            this.barButtonItem5.Name = "barButtonItem5";
            // 
            // barButtonItem6
            // 
            this.barButtonItem6.Caption = "barButtonItem4";
            this.barButtonItem6.Id = 24;
            this.barButtonItem6.Name = "barButtonItem6";
            this.barButtonItem6.SmallWithoutTextWidth = 70;
            // 
            // DispatchFormDevX
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1280, 750);
            this.Controls.Add(this.ribbonStatusBar1);
            this.Controls.Add(this.labelLogo);
            this.Controls.Add(this.ribbonControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(1278, 726);
            this.Name = "DispatchFormDevX";
            this.Text = "DispatchFormDevX";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DispatchFormDevX_FormClosing);
            this.Load += new System.EventHandler(this.DispatchFormDevX_Load);
            this.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.DispatchFormDevX_PreviewKeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

		private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
		private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
		private DevExpress.XtraTabbedMdi.XtraTabbedMdiManager xtraTabbedMdiManager1;
        public DevExpress.XtraBars.BarButtonItem barButtonItemHelp;
		private System.Windows.Forms.Label labelLogo;
		private DevExpress.XtraBars.BarButtonItem barButtonItemRefreshOrder;
		private DevExpress.XtraBars.BarButtonItem barButtonItemPending;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupDispatchOrder;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupUnits;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupAssignUnits;
		private DevExpress.XtraBars.BarButtonItem barButtonItemModifyZone;
		private DevExpress.XtraBars.BarButtonItem barButtonItemClose;
		private DevExpress.XtraBars.BarButtonItem barButtonItemAvailable;
		private DevExpress.XtraBars.BarButtonItem barButtonItemOutOfOrder;
		private DevExpress.XtraBars.BarButtonItem barButtonItemUnavailable;
		private DevExpress.XtraBars.BarButtonItem barButtonItemOnScene;
		private DevExpress.XtraBars.BarButtonItem barButtonItemAssignedOnScene;
		private DevExpress.XtraBars.BarButtonItem barButtonItemModifyTime;
		private DevExpress.XtraBars.BarButtonItem barButtonItemEnd;
		private DevExpress.XtraBars.BarButtonItem barButtonItemChat;
		internal DevExpress.XtraBars.BarButtonItem barButtonItemOpenMap;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupUser;
		private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit1;
		public DevExpress.XtraBars.BarButtonItem barButtonItemStatus;
		private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
		private DevExpress.XtraBars.BarButtonItem barButtonItemAddNote;
		private DevExpress.XtraBars.BarButtonItem barButtonItemSupportRequest;
        private DevExpress.XtraBars.Alerter.AlertControl alertControlChat;
		private DevExpress.XtraBars.BarButtonItem barButtonItemPrint;
		private DevExpress.XtraBars.BarButtonItem barButtonItemSave;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupGeneralOptions;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupApplications;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupTools;
        private DevExpress.XtraBars.BarButtonItem barButtonItemLogo;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar1;
        private DevExpress.XtraBars.BarStaticItem labelUser;
        private DevExpress.XtraBars.BarStaticItem labelConnected;
        private DevExpress.XtraBars.BarStaticItem labelDate;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage2;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupCallInfo;
        public DevExpress.XtraBars.BarButtonItem barButtonItemStartRegIncident;
        public DevExpress.XtraBars.BarButtonItem barButtonItemCreateNewIncident;
        private DevExpress.XtraBars.BarButtonItem barButtonItemCancelIncident;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItemHistory;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.BarButtonItem barButtonItemRefresh;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        public DevExpress.XtraBars.BarButtonItem barButtonItem5;
        private DevExpress.XtraBars.BarButtonItem barButtonItem6;
	}
}
