using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Linq;
using Smartmatic.SmartCad.Service;

using MyTimer = System.Threading.Timer;
using System.Globalization;
using System.Threading;
using System.Diagnostics;
using System.Reflection;
using System.ServiceModel;


using SmartCadCore.ClientData;
using System.Xml.Xsl;
using System.Xml;
using System.IO;

using Microsoft.Win32;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraEditors;
using DevExpress.Data.Filtering;
using DevExpress.XtraGrid.Columns;
using DevExpress.Data;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using SmartCadCore.Common;
using System.Net;
using DevExpress.XtraEditors.Repository;
using SmartCadControls.SyncBoxes;
using SmartCadCore.Core;
using SmartCadGuiCommon;
using SmartCadCore.Enums;
using SmartCadControls;
using SmartCadControls.Controls;
using SmartCadControls.Filters;
using SmartCadGuiCommon.Util;
using Smartmatic.SmartCad.Map;
using SmartCadGuiCommon.Services;

namespace SmartCadDispatch.Gui
{
    public partial class DefaultDispatchFormDevX : DevExpress.XtraEditors.XtraForm
    {
		#region Fields
		private const int SINGLE_SELECTION = 1;
        private int GLOBAL_TIMER_PERIOD;
        private readonly Color RECOMMENDED_UNIT = Color.Yellow;
        private const int RECOMMENDED_COLUMN = 5;
        private const int DISTANCE_UNIT = 6;
		const int MAX_UNITS_TO_FOLLOW = 16;
		private int TIME_WITHOUT_ATTENTION;
        private const float FONT_SIZE = 9.75f;
        private bool ActiveCall = false;
        private OperatorStatusClientData defaultReadyStatus = null;
        private OperatorStatusClientData defaultBusyStatus = null;        
        private UnitRadioButtonFilter unitRadioButtonFilter = UnitRadioButtonFilter.ALL;
        private UnitButtonFilter unitButtonFilter = UnitButtonFilter.ALL;
        private IncidentNotificationButtonFilter incidentNotificationButtonFilter = IncidentNotificationButtonFilter.ACTIVE;
        private System.Threading.Timer globalTimer;
        private bool assignUnitsToIncidentNotification = false;
        private GridControlSynBox assignedIncidentNotificationSyncBox;
        private GridControlSynBox dispatchOrderSyncBox;
        private GridControlSynBox newIncidentNotificationSyncBox;
		private GridControlSynBox unitSyncBox;

        private SkinEngine skinEngine;
        private ConfigurationClientData configurationClient;
        private XslCompiledTransform xslCompiledTransform;
        private XslCompiledTransform xslCctvCompiledTransform;
        private XslCompiledTransform xslAlarmCompiledTransform;
        private XslCompiledTransform xslPhoneReportCompiledTransform;
        private Dictionary<string, ApplicationPreferenceClientData> globalApplicationPreferences;
        private int selectedIncidentCode = -1;
        private System.Threading.Timer refreshIncidentReport;
        private object syncCalledFromDataGridUnits;
        private bool calledFromDataGridUnits;
        private ChangeDispatchStatusForm changeDispatchStatusForm;
        public string password;
        private readonly string UNIT_AVAILABLE = "Available";
        private string incNotAssigningCode;
        private object IEFooter;
        private string objectValue;
        private const string SourceDirectory = "Software\\Microsoft\\Internet Explorer\\PageSetup";
        private string selectedIncidentNotificationCustomCode = null;
        private int counterToCompleteMinute;
        private const int ABSENCE_CHECK_CYCLE = 6;
        private double availableTimeInStatusWithPause = 0;
        private IList globalDepartmentZones;
        private List<int> remoteControlOperators = new List<int>();
		private int rowIndexAdded = -1;
		private string closingNotificationCustomCode = null;
		private object syncClosingNotification = new object();
		public event EventHandler<CommittedObjectDataCollectionEventArgs> DispatchCommittedChanges;
        private CriteriaOperator showByFilterExpression;
        private CriteriaOperator unitStatusFilterExpression;
        private bool refreshIncidentFilter = false;
		private bool rightClick = false;
        private DispatchFormDevX parentForm;
        bool enablingLocationButton = false;
        private bool closeIncidentNotificationWithoutMessage = false;
		private const string DynTableTracksName = "Tracks";
		private const string DynTableTempName = "Temp2";
        UnitFollowRibbonForm unitFollowRibbonForm;
        SoftPhoneForm popUpDialer;
        
#endregion

        #region Telephone Radio

        public enum TelephonyClientStateEnum
        {
            None,
            WaitingToCall,
            CallInProcess,
        }

        public enum RadioClientStatusEnum
        { 
            None,
            Talk,
            Listen
        }

        public bool ErrorMessageRadioShown { get; set; }
        private object syncTelephonyAction = new object();
        private TelephoneStatusType telephoneStatusType = TelephoneStatusType.None;
        private RadioClientStatusEnum radioStatusType = RadioClientStatusEnum.None;
        public TelephonyConsoleManager TelephonyConsoleManager { get { return parentForm.TelephonyConsoleManager; } }
        public NetworkCredential NetworkCredential { get; set; }
        public bool DoNotDisturbMessageShown { get; set; }
        public bool CallEntering { get; set; }
        public IList Radios { get; set; }

        public TelephoneStatusType TelephoneStatus
        {
            get
            {
                return telephoneStatusType;
            }
            set
            {
                telephoneStatusType = value;
                CheckTelephoneStatus();
            }
        }

        public RadioClientStatusEnum RadioStatus
        {
            get
            {
                return radioStatusType;
            }
            set
            {
                radioStatusType = value;
                CheckRadioStatus();
            }
        }

        public void CheckRadioStatus()
        {
            switch (RadioStatus)
            {
                case RadioClientStatusEnum.Talk:
                    barButtonItemTalk.LargeGlyph = ResourceLoader.GetImage("MicrophoneMutedImage");
                    barButtonItemTalk.Caption = ResourceLoader.GetString2("Mute");
                    break;
                case RadioClientStatusEnum.None:
                    barButtonItemTalk.LargeGlyph = ResourceLoader.GetImage("MicrophoneDisabledImage");
                    barButtonItemTalk.Caption = ResourceLoader.GetString2("Talk");
                    break;
                case RadioClientStatusEnum.Listen:
                    barButtonItemTalk.LargeGlyph = ResourceLoader.GetImage("MicrophoneEnabledImage");
                    barButtonItemTalk.Caption = ResourceLoader.GetString2("Talk");
                    break;
            }
        }

        public void CheckTelephoneStatus()
        {
            switch (TelephoneStatus)
            {
                case TelephoneStatusType.None:
                case TelephoneStatusType.Connected:
                case TelephoneStatusType.Ended:
                    //this.barButtonItemCall.LargeGlyph = ResourceLoader.GetImage("TelephoneNone");
                    barButtonItemCall.LargeGlyph = ResourceLoader.GetImage("TelephoneActive");
                    barButtonItemCall.Caption = ResourceLoader.GetString2("Call");
                    break;
                case TelephoneStatusType.Active:
                    //barButtonItemCall.LargeGlyph = ResourceLoader.GetImage("TelephoneActive");
                    barButtonItemCall.LargeGlyph = ResourceLoader.GetImage("TelephoneEnded");
                    barButtonItemCall.Caption = ResourceLoader.GetString2("HangUp");
                    break;
            }
        }

        private void barButtonItemPhoneReboot_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                BackgroundProcessForm processForm = new BackgroundProcessForm(ResourceLoader.GetString2("RestartPhone"), this, new MethodInfo[] { this.GetType().GetMethod("ReInitTelephone", BindingFlags.Instance | BindingFlags.NonPublic) }, new object[][] { new object[] { } });
                processForm.CanThrowError = true;
                processForm.ShowDialog();
                DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        private void ReInitTelephone()
        {
            FormUtil.InvokeRequired(this, delegate
            {
                TelephoneStatus = TelephoneStatusType.None;
                RadioStatus = RadioClientStatusEnum.None;
            });

            try
            {
                TelephonyServiceClient.GetInstance().TelephonyAction -= new EventHandler<TelephonyActionEventArgs>(Form_TelephonyAction);
                TelephonyServiceClient.GetInstance().Logout();
                Thread.Sleep(5000);
                TelephonyServiceClient.GetInstance().Close();
            }
            catch { }

            TelephonyConsoleManager.Stop();
            TelephonyConsoleManager.Run();
            TelephonyConsoleManager.WaitForService(3);

            FormUtil.InvokeRequired(this, delegate
            {
                this.Focus();
                Win32.SetForegroundWindow(this.Handle);
            });

            //ApplicationPreferenceClientData recordCalls = (ApplicationPreferenceClientData)ServerServiceClient.GetInstance().SearchClientObject(SmartCadHqls.GetCustomHql(SmartCadHqls.GetApplicationPreferenceByName, "RecordCalls"));

            TelephonyConnection();
        }

        private void TelephonyConnection()
        {
            try
            {
                TelephonyServiceClient.GetServerService();
                TelephonyServiceClient.GetInstance().SetConfiguration(ServerServiceClient.GetInstance().Configuration);
                ApplicationPreferenceClientData recordCalls = globalApplicationPreferences["RecordCalls"];
                Thread.Sleep(1000);
                TelephonyServiceClient.GetInstance().Login(
                    ServerServiceClient.GetInstance().NetworkCredential.UserName,
                    ServerServiceClient.GetInstance().NetworkCredential.Password,
                    ServerServiceClient.GetInstance().OperatorClient.AgentId,
                    ServerServiceClient.GetInstance().OperatorClient.AgentPassword,
                    recordCalls == null ? false : bool.Parse(recordCalls.Value),
                    ServerServiceClient.GetInstance().Configuration.Extension, true);
                TelephonyServiceClient.GetInstance().TelephonyAction += new EventHandler<TelephonyActionEventArgs>(Form_TelephonyAction);

                DoNotDisturbMessageShown = false;

                TelephoneStatus = TelephoneStatusType.Connected;
                barEditItemDepartment.Enabled = true;
                ErrorMessageRadioShown = false;
            }
            catch (Exception ex)
            {
                MessageForm.Show(ex.Message, ex);
            }
        }

        public void Form_TelephonyAction(object sender, TelephonyActionEventArgs e)
        {
            lock (syncTelephonyAction)
            {
                FormUtil.InvokeRequired(this, delegate
                {
                    if (e is TelephonyServerConnectionEventArgs)
                    {
                        TelephonyServerConnectionEventArgs eventArgs = e as TelephonyServerConnectionEventArgs;

                        TelephoneStatus = TelephoneStatusType.Connected;
                        RadioStatus = RadioClientStatusEnum.Talk;
                        ErrorMessageRadioShown = false;
                        //MessageForm.Show(ResourceLoader.GetString2("RadioConnectionEstablish"), MessageFormType.Information);
                    }
                    else if (e is CallMadeEventArgs)
                    {
                        if (TelephoneStatus == TelephoneStatusType.Connected || TelephoneStatus == TelephoneStatusType.Ended)
                        {
                            TelephoneStatus = TelephoneStatusType.Active;
                            RadioStatus = RadioClientStatusEnum.Talk;
                            DepartmentRadio dr = (DepartmentRadio)barEditItemDepartment.EditValue;
                            Thread.Sleep(2000);
                            TelephonyServiceClient.GetInstance().GenerateCall(dr.Radio.FrecuencyCode);
                            barEditItemDepartment.Enabled = false;
                            barButtonItemRestartTelephone.Enabled = false;
                        }
                        else
                            WaitAction = false;
                    }
                    else if (e is DigitsReceivedEventArgs)
                    { 
                        //Generate digits arrived correctly
                        if (TelephoneStatus == TelephoneStatusType.Active && barButtonItemTalk.Enabled == true)
                        {
                            if (RadioStatus == RadioClientStatusEnum.Talk)
                                RadioStatus = RadioClientStatusEnum.Listen;
                            else
                                RadioStatus = RadioClientStatusEnum.Talk;
                        }
                        else if (barButtonItemTalk.Enabled == false)
                        {
                            barButtonItemTalk.Enabled = true;
                        }
                        WaitAction = false;
                    }
                    else if (e is CallReleasedEventArgs || e is CallAbandonedEventArgs)
                    {
                        barButtonItemTalk.Enabled = false;
                        TelephoneStatus = TelephoneStatusType.Ended;
                        barEditItemDepartment.Enabled = true;
                        barButtonItemRestartTelephone.Enabled = true;
                        WaitAction = false;
                    }
                    else if (e is TelephonyErrorEventArgs)
                    {
                        if (ErrorMessageRadioShown == false)
                        {
                            ErrorMessageRadioShown = true;
                            MessageForm.Show(((TelephonyErrorEventArgs)e).ErrorMessage, new Exception());
                            MessageForm.Show(ResourceLoader.GetString2("RadioConnectionLost"),MessageFormType.Warning);
                        }
                        TelephoneStatus = TelephoneStatusType.None;
                        RadioStatus = RadioClientStatusEnum.None;
                        barButtonItemTalk.Enabled = false;
                    }
                    else if (e is ReadyStatusChangedEventArgs)
                    {
                        //if (operatorStatusFromApp == false)
                        //{
                        //    ReadyStatusChangedEventArgs args = (ReadyStatusChangedEventArgs)e;
                        //    OperatorStatusClientData operatorStatus = null;

                        //    OperatorStatusClientData currentOperatorStatus = (OperatorStatusClientData)barButtonItemStatus.Tag;

                        //    if ((args.IsReady == false && FrontClientState == FrontClientStateEnum.WaitingForCall && currentOperatorStatus.Equals(defaultAbsentStatus)) == false)
                        //    {
                        //        if (args.IsReady == true && FrontClientState != FrontClientStateEnum.WaitingForCall)
                        //        {
                        //            TelephonyServiceClient.GetInstance().SetIsReady(false);
                        //            return;
                        //        }
                        //        if ((e as ReadyStatusChangedEventArgs).NotReadyReasonCode == ReadyStatusChangedEventArgs.CallNotAnswered)
                        //        {
                        //            callEntering = true;
                        //        }

                        //        if (args.IsReady == false && FrontClientState == FrontClientStateEnum.WaitingForCall && callEntering == false)
                        //        {
                        //            TelephonyServiceClient.GetInstance().SetIsReady(true);
                        //            return;
                        //        }

                        //        if (FrontClientState == FrontClientStateEnum.WaitingForCall)
                        //        {
                        //            if (args.IsReady == false && FrontClientState == FrontClientStateEnum.WaitingForCall && callEntering == true)
                        //                operatorStatus = defaultAbsentStatus;
                        //            else if (args.IsReady == false)
                        //                operatorStatus = defaultBusyStatus;
                        //            else
                        //                operatorStatus = defaultReadyStatus;

                        //            SetToolStripOperatorStatus(operatorStatus);
                        //            ServerServiceClient.GetInstance().SetOperatorStatus(operatorStatus);
                        //        }
                        //    }
                        //}
                        //else
                        //    operatorStatusFromApp = false;
                    }
                });
            }
        }

        public void OpenTelephony()
        {
            FillDepartmentsRadio();

            ribbonPageGroupRadio.Text = ResourceLoader.GetString2("Radio");

            barEditItemDepartment.Caption = ResourceLoader.GetString2("Departments");

            //barButtonItemCall.Enabled = true;
            barButtonItemCall.Caption = ResourceLoader.GetString2("Call");
            barButtonItemCall.ItemClick += barButtonItemCall_ItemClick;

            barButtonItemTalk.Caption = ResourceLoader.GetString2("Talk");
            barButtonItemTalk.ItemClick += barButtonItemTalk_ItemClick;

            barButtonItemRestartTelephone.Enabled = true;
            barButtonItemRestartTelephone.Caption = ResourceLoader.GetString2("RestartPhone");
            barButtonItemRestartTelephone.LargeGlyph = ResourceLoader.GetImage("RestartPhoneImage");

            TelephoneStatus = TelephoneStatusType.None;
            RadioStatus = RadioClientStatusEnum.None;

            TelephonyConnection();
        }

        void barButtonItemTalk_ItemClick(object sender, ItemClickEventArgs e)
        {
            DepartmentRadio dr = (DepartmentRadio)barEditItemDepartment.EditValue;
            TelephonyServiceClient.GetInstance().GenerateCall(dr.Radio.TalkCode);
            WaitForTelephoneActions();

            if (ServerServiceClient.GetInstance().Configuration.VirtualMode)
            {
                if (RadioStatus == RadioClientStatusEnum.Talk)
                    RadioStatus = RadioClientStatusEnum.Listen;
                else
                    RadioStatus = RadioClientStatusEnum.Talk;
            }
        }

        void barButtonItemCall_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (TelephoneStatus == TelephoneStatusType.Connected || TelephoneStatus == TelephoneStatusType.Ended)
            {
                DepartmentRadio dr = (DepartmentRadio)barEditItemDepartment.EditValue;
                TelephonyServiceClient.GetInstance().GenerateCall(dr.Radio.Extension.ToString());

                if (ServerServiceClient.GetInstance().Configuration.VirtualMode)
                {
                    barButtonItemTalk.Enabled = true;
                }
            }
            else if (TelephoneStatus == TelephoneStatusType.Active)
            {
                TelephonyServiceClient.GetInstance().Disconnect();

                if (ServerServiceClient.GetInstance().Configuration.VirtualMode)
                {
                    barButtonItemTalk.Enabled = false;
                }
            }
            WaitForTelephoneActions();

            if (ServerServiceClient.GetInstance().Configuration.VirtualMode)
            {
                barButtonItemTalk.Enabled = true;
            }
        }

        public void CloseTelephony()
        {
            barButtonItemCall.Enabled = false;
            try
            {
                TelephonyServiceClient.GetInstance().TelephonyAction -= new EventHandler<TelephonyActionEventArgs>(Form_TelephonyAction);
                TelephonyServiceClient.GetInstance().Logout();
                Thread.Sleep(1000);
                TelephonyServiceClient.GetInstance().Close();
            }
            catch
            { }
        }

        public void FillDepartmentsRadio()
        {
            ((RepositoryItemComboBox)barEditItemDepartment.Edit).Items.Clear();
            foreach (RadioConfigurationClientData radio in Radios)
            {
                if (radio.Department != null && ServerServiceClient.GetInstance().OperatorClient.DepartmentTypes.Contains(radio.Department))
                {
                    ((RepositoryItemComboBox)barEditItemDepartment.Edit).Items.Add(new DepartmentRadio() { Radio = radio });
                }
            }
        }

        private class DepartmentRadio
        {
            public RadioConfigurationClientData Radio { get; set; }

            public override string ToString()
            {
                return Radio.Department.Name;
            }
        }

        private void barEditItemDepartment_ItemClick(object sender, ItemClickEventArgs e)
        {
            barButtonItemCall.Enabled = true;
        }
        public bool RadioDepartmentSelected { get; set; }
        private void repositoryItemComboBoxDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            RadioDepartmentSelected = true;
            ActivateRadioButtons();
        }

        private void ActivateRadioButtons()
        {
            if ((TelephoneStatus == TelephoneStatusType.Connected ||  TelephoneStatus == TelephoneStatusType.Ended) && RadioDepartmentSelected)
                barButtonItemCall.Enabled = true;
            barButtonItemRestartTelephone.Enabled = true;
        }

        private void barButtonItemRestartTelephone_ItemClick(object sender, ItemClickEventArgs e)
        {
            //ReInitTelephone();
        }

        public bool WaitAction { get; set; }
        private void WaitForTelephoneActions()
        {
            try
            {
                BackgroundProcessForm processForm = new BackgroundProcessForm(this, new MethodInfo[] { this.GetType().GetMethod("WaitActions", BindingFlags.Instance | BindingFlags.NonPublic) }, new object[][] { new object[] { } });
                processForm.CanThrowError = true;
                processForm.ShowDialog();
                DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        private void WaitActions()
        { 
            WaitAction = true;
            while(WaitAction)
            {
                Thread.Sleep(1000);
            }
        }
        #endregion

        public void SetPassword(string passwd)
        {
            this.password = passwd;
        }

        public IList GlobalUnits
        {
            set
            {
                if (value != null)
                {
                    IList deparmentTypes = ServerServiceClient.GetInstance().OperatorClient.DepartmentTypes;
                    IList units = new ArrayList();
                    foreach (UnitClientData unit in value)
                    {
                        if (deparmentTypes.Contains(unit.DepartmentStation.DepartmentZone.DepartmentType) == true)
                        {
                            if (unit.DispatchOrder == null ||
                                (unit.DispatchOrder != null && unit.DispatchOrder.DispatchOperatorCode == ServerServiceClient.GetInstance().OperatorClient.Code))
                            {
                                gridControlUnits.AddOrUpdateItem(new GridControlDataUnit(unit));
                            }
                        }
                    }
                }
            }
        }

        public IList GlobalIncidentNotifications
        {
            set
            {
                if (value != null)
                {
                    IList deparmentTypes = ServerServiceClient.GetInstance().OperatorClient.DepartmentTypes;
                    IList notifications = new ArrayList();
                    foreach (IncidentNotificationClientData incidentNotification in value)
                    {
                        if (deparmentTypes.Contains(incidentNotification.DepartmentType) == true)
                        {
                            notifications.Add(incidentNotification);
                        }
                    }
                    newIncidentNotificationSyncBox.Sync(notifications);
                }
            }
        }

        public IList GlobalDepartmentZones
        {
            get
            {
                return globalDepartmentZones;
            }
            set
            {
                globalDepartmentZones = value;
            }
        }

        #region Synchronization objects
        
        private object syncCommited = new object();

        #endregion
        
        public ConfigurationClientData ConfigurationClient
        {
            get
            {
                return configurationClient;
            }
            set
            {
                configurationClient = value;
            }
        }

        public DefaultDispatchFormDevX(DispatchFormDevX parentForm)
        {
            this.parentForm = parentForm;            
            CheckForIllegalCrossThreadCalls = false;
            InitializeComponent();
            InitializeDataGrids();
            calledFromDataGridUnits = false;
            syncCalledFromDataGridUnits = new object();

            XmlReaderSettings xmlReaderSettings = new XmlReaderSettings();
            xmlReaderSettings.ProhibitDtd = false;

            xslCompiledTransform = new XslCompiledTransform();
            xslCompiledTransform.Load(XmlReader.Create(SmartCadConfiguration.IncidentXslt, xmlReaderSettings));
            xslCctvCompiledTransform = new XslCompiledTransform();
            xslCctvCompiledTransform.Load(XmlReader.Create(SmartCadConfiguration.CctvIncidentXslt, xmlReaderSettings));
            xslAlarmCompiledTransform = new XslCompiledTransform();
            xslAlarmCompiledTransform.Load(XmlReader.Create(SmartCadConfiguration.AlarmIncidentXslt, xmlReaderSettings));
            xslPhoneReportCompiledTransform = new XslCompiledTransform();
            xslPhoneReportCompiledTransform.Load(XmlReader.Create(SmartCadConfiguration.PhoneReportXslt, xmlReaderSettings));

            this.textBoxIncidentNotes.Enabled = false;
            this.textBoxIncidentNotes.BackColor = Color.White;

            refreshIncidentReport = new System.Threading.Timer(new TimerCallback(OnRefreshIncidentReportTimer));

            DoSubscriptionToEvents();

			LoadLanguage();
			LoadRibbonButtonGlyph();
      
        }

        private void InitializeDataGrids() 
        {
            newIncidentNotificationSyncBox = new GridControlSynBox(gridControlNewIncidentNotifications);
            gridControlNewIncidentNotifications.Type = typeof(GridControlDataNewIncidentNotifications);
            gridControlNewIncidentNotifications.EnableAutoFilter = true;
            gridControlNewIncidentNotifications.ViewTotalRows = true;
            gridViewNewIncidentNotifications.Images = imageList;
            
            gridControlNewIncidentNotifications.AllowDrop = false;
            gridViewNewIncidentNotifications.Columns["Priority"].SortIndex = 0;
            gridViewNewIncidentNotifications.Columns["Priority"].SortOrder = ColumnSortOrder.Ascending;
            gridViewNewIncidentNotifications.Columns["CreationDate"].SortIndex = 1;
            gridViewNewIncidentNotifications.Columns["CreationDate"].SortOrder = ColumnSortOrder.Ascending;

            gridViewNewIncidentNotifications.Click += new EventHandler(gridViewNewIncidentNotifications_Click);
        

            assignedIncidentNotificationSyncBox = new GridControlSynBox(gridControlAssignedIncidentNotifications);
            gridControlAssignedIncidentNotifications.Type = typeof(GridControlDataAssignedIncidentNotifications);
            gridControlAssignedIncidentNotifications.EnableAutoFilter = true;
            gridControlAssignedIncidentNotifications.ViewTotalRows = true;
            gridControlAssignedIncidentNotifications.AllowDrop = true;

            gridViewAssignedIncidentNotifications.Columns["Priority"].SortIndex = 0;
            gridViewNewIncidentNotifications.Columns["Priority"].SortOrder = ColumnSortOrder.Ascending;
     
            unitSyncBox = new GridControlSynBox(gridControlUnits);
            gridControlUnits.Type = typeof(GridControlDataUnit); 
            gridViewUnits.OptionsSelection.MultiSelect = true;
            gridControlUnits.EnableAutoFilter = true;
            gridControlUnits.ViewTotalRows = true;
            gridViewUnits.Images = imageList;
        
            dispatchOrderSyncBox = new GridControlSynBox(gridControlDispatchOrder);

            gridControlDispatchOrder.Type = typeof(GridControlDataDispatchOrder);
            gridViewDispatchOrder.OptionsSelection.MultiSelect = true;
            gridControlDispatchOrder.EnableAutoFilter = true;
            gridControlDispatchOrder.ViewTotalRows = true;
            gridControlDispatchOrder.AllowDrop = true;
            gridViewDispatchOrder.Columns["SendDate"].SortIndex = 0;
        }

        void gridViewNewIncidentNotifications_Click(object sender, EventArgs e)
        {
            DevExpress.Utils.DXMouseEventArgs pointXY = e as DevExpress.Utils.DXMouseEventArgs;
            if (pointXY != null)
            {
                if (gridViewNewIncidentNotifications.CalcHitInfo(pointXY.Location).InColumn == true)
                {
                    gridViewNewIncidentNotifications.SortInfo.Add(gridViewNewIncidentNotifications.Columns["CreationDate"], ColumnSortOrder.Ascending);
                }
            }
        }

		private void LoadRibbonButtonGlyph()
		{
			barButtonItemPending.Glyph = toolStripMenuItemPending.Image;
			barButtonItemModifyZone.Glyph = toolStripMenuItemModifyZone.Image;
			barButtonItemClose.Glyph = toolStripMenuItemCloseIncNotif.Image;
			barButtonItemRefreshOrder.Glyph = toolStripMenuItemRefreshNewIncidentNotificationDetails.Image;
			barButtonItemAvailable.Glyph = toolStripMenuItemAvailableUnit.Image;
			barButtonItemOutOfOrder.Glyph = toolStripMenuItemOutOfOrderUnit.Image;
			barButtonItemUnavailable.Glyph = toolStripMenuItemNotAvailableUnit.Image;
			barButtonItemOnScene.Glyph = toolStripMenuItemOnSceneUnit.Image;
			barButtonItemAssignedOnScene.Glyph = enEscenaToolStripMenuItem.Image;
			barButtonItemModifyTime.Glyph = modifyTimeOrderToolStripMenuItem.Image;
			barButtonItemEnd.Glyph = cerrarToolStripMenuItem.Image;
			barButtonItemAddNote.Glyph = toolStripMenuItemAddNoteToUnit.Image;
			barButtonItemSupportRequest.Glyph = toolStripMenuItemSupportRequest.Image;			
		}

		private void LoadLanguage()
		{
			barButtonItemAssignedOnScene.Caption = ResourceLoader.GetString2("OnScene");
			barButtonItemAvailable.Caption = ResourceLoader.GetString2("Available");
			barButtonItemClose.Caption = ResourceLoader.GetString2("Close");
			barButtonItemEnd.Caption = ResourceLoader.GetString2("Finalize");
			barButtonItemModifyTime.Caption = ResourceLoader.GetString2("ModifyTime");
			barButtonItemModifyZone.Caption = ResourceLoader.GetString2("ModifyZone");
			barButtonItemOnScene.Caption = ResourceLoader.GetString2("OnScene");
			barButtonItemOutOfOrder.Caption = ResourceLoader.GetString2("OutOfOrder");
			barButtonItemPending.Caption = ResourceLoader.GetString2("Pending");
			barButtonItemRefreshOrder.Caption = ResourceLoader.GetString2("Refresh");
			barButtonItemStatus.Caption = ResourceLoader.GetString2("Status");
			barButtonItemUnavailable.Caption = ResourceLoader.GetString2("Unavailable");
			barButtonItemAddNote.Caption = ResourceLoader.GetString2("AddNote");
			barButtonItemSupportRequest.Caption = ResourceLoader.GetString2("SupportRequest");
			ribbonPage1.Text = ResourceLoader.GetString2("Dispatch");
			ribbonPageGroupAssignUnits.Text = ResourceLoader.GetString2("DispatchedUnits");
			ribbonPageGroupDispatchOrder.Text = ResourceLoader.GetString2("DispatchOrders");
			ribbonPageGroupUnits.Text = ResourceLoader.GetString2("MobileUnits");
			ribbonPageGroupUser.Text = ResourceLoader.GetString2("Status");
			ribbonPageGroupGeneralOptions.Text = ResourceLoader.GetString2("GeneralOptions");
			barButtonItemPrint.Caption = ResourceLoader.GetString2("Print");
			barButtonItemSave.Caption = ResourceLoader.GetString2("Save");
			this.layoutControlGroupDispatchOrder.Text = ResourceLoader.GetString2("DispatchedUnits");
		    this.layoutControlGroupIncidentDetails.Text = ResourceLoader.GetString2("IncidentDetails");
			this.layoutControlGroupAssignedIncidentNotifications.Text = ResourceLoader.GetString2("DispatchOrders");
		    this.layoutControlGroupNewIncidentNotifications.Text = ResourceLoader.GetString2("NewIncidentNotifications");
			this.layoutControlGroupUnits.Text = ResourceLoader.GetString2("MobileUnits");
            this.layoutControlGroupNotes.Text = ResourceLoader.GetString2("Notes");
            this.buttonActiveDispatch.Text = ResourceLoader.GetString2("ActivesNotifications");
            this.buttonActiveDispatch.ToolTip = ResourceLoader.GetString2("ActivesNotificationTooltip");
            this.buttonAllUnits.Text = ResourceLoader.GetString2("AllUnits");
            this.buttonAllUnits.ToolTip = ResourceLoader.GetString2("AllUnitsTooltip");
            this.buttonAssignedUnits.Text = ResourceLoader.GetString2("bAssignedUnits");
            this.buttonAssignedUnits.ToolTip = ResourceLoader.GetString2("bAssignedUnitsTooltip");
            this.buttonAvailableUnits.Text = ResourceLoader.GetString2("AvailablesUnits");
            this.buttonAvailableUnits.ToolTip = ResourceLoader.GetString2("AvailablesUnitsTooltip");
            this.buttonClosedDispatch.Text = ResourceLoader.GetString2("ClosedNotifications");
            this.buttonClosedDispatch.ToolTip = ResourceLoader.GetString2("ClosedNotificationTooltip");
            this.blinkingButtonExDispReqAttention.Text = ResourceLoader.GetString2("Attention");
            this.blinkingButtonExDispReqAttention.ToolTip = ResourceLoader.GetString2("AttentionTooltip");
            this.blinkingButtonExPendingDispatch.Text = ResourceLoader.GetString2("PendingNotifications");
            this.blinkingButtonExPendingDispatch.ToolTip = ResourceLoader.GetString2("PendingNotificationTooltip");
            this.buttonReqAttention.Text = ResourceLoader.GetString2("Attention");
            this.buttonReqAttention.ToolTip = ResourceLoader.GetString2("AttentionRTooltip");
            //Estos tres elementos no tienen propiedad text
            this.barItemAsignedUnits.Caption = ResourceLoader.GetString2("BarItemAsignedUnitsLabel");
            this.barItemRecommendedUnits.Caption = ResourceLoader.GetString2("BarItemRecommendedUnitsLabel");
            this.barButtonItemUnitsFollow.Caption = ResourceLoader.GetString2("Follow"); 
			this.barButtonItemLocateObject.Caption = ResourceLoader.GetString2("LocateObject");
            this.toolStripMenuItemLocateNewIncNot.Text = ResourceLoader.GetString2("LocateObject");
            this.toolStripMenuItemLocateAssignedIncNot.Text = ResourceLoader.GetString2("LocateObject");
            this.toolStripMenuItemLocateUnitDispatch.Text = ResourceLoader.GetString2("LocateObject");
            this.toolStripMenuItemLocateUnit.Text = ResourceLoader.GetString2("LocateObject");
            this.toolStripMenuItemRecommendedUnits.Text = ResourceLoader.GetString2("BarItemRecommendedUnitsLabel");
            this.toolStripMenuItemAssignedUnits.Text = ResourceLoader.GetString2("BarItemAsignedUnitsLabel");
            this.ribbonPageGroupTools.Text = ResourceLoader.GetString2("Maps");

            radioButtonAll.Text = ResourceLoader.GetString2("AllUnits");
            radioButtonDepartment.Text = ResourceLoader.GetString2("Station");
            radioButtonZone.Text = ResourceLoader.GetString2("Zone");
            buttonExAssign.Text = ResourceLoader.GetString2("Assign");
            buttonExAddNotification.Text = ResourceLoader.GetString2("Add");
            labelExShowBy.Text = ResourceLoader.GetString2("ShowBy") + ":";

            this.Text = ResourceLoader.GetString2("DefaultDispatchFormDevX");
            this.toolStripMenuItemOutOfOrderUnit.Text = ResourceLoader.GetString2("OutOfOrder");
            this.toolStripMenuItemNotAvailableUnit.Text = ResourceLoader.GetString2("Unavailable");
            this.toolStripMenuItemOnSceneUnit.Text = ResourceLoader.GetString2("OnScene");
            this.toolStripMenuItemDelayUnit.Text = ResourceLoader.GetString2("ModifyTime");
            this.toolStripMenuItemAddNote.Text = ResourceLoader.GetString2("AddNote");
            this.toolStripMenuItemCloseUnit.Text = ResourceLoader.GetString2("Finalize");
            this.toolStripMenuItemAssignUnit.Text = ResourceLoader.GetString2("Assign");
            this.toolStripMenuItemCallUnit.Text = ResourceLoader.GetString2("Call");
            this.toolStripMenuItemCallStation.Text = ResourceLoader.GetString2("Call");
            this.toolStripMenuItemAvailableUnit.Text = ResourceLoader.GetString2("Available");
            this.toolStripMenuItemAssign.Text = ResourceLoader.GetString2("TakeOrder");
            this.toolStripMenuItemRefreshNewIncidentNotificationDetails.Text = ResourceLoader.GetString2("RefreshDispatchOrder");
            this.toolStripMenuItemModifyZone.Text = ResourceLoader.GetString2("ModifyZone");
            this.toolStripMenuItemSupportRequest.Text = ResourceLoader.GetString2("SupportRequest");
            this.toolStripMenuItemCloseIncNotif.Text = ResourceLoader.GetString2("Close");
            this.enEscenaToolStripMenuItem.Text = ResourceLoader.GetString2("OnScene");
            this.modifyTimeOrderToolStripMenuItem.Text = ResourceLoader.GetString2("ModifyTime");
            this.toolStripMenuItemAddNoteToUnit.Text = ResourceLoader.GetString2("AddNote");
            this.cerrarToolStripMenuItem.Text = ResourceLoader.GetString2("Finalize");
            this.toolStripMenuItemRefreshIncidentNotitficationDetails.Text = ResourceLoader.GetString2("RefreshDispatchOrder");
            this.blinkingButtonExPendingDispatch.Text = ResourceLoader.GetString2("PendingNotifications");
            this.toolStripMenuItemPending.Text = ResourceLoader.GetString2("PendingNotifications");



		}
        
        private void gridViewNewIncidentNotifications_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            rowIndexAdded = e.RowHandle;
        }

        private void LoadGlobalApplicationPreferences(IList apps)
        {
            globalApplicationPreferences = new Dictionary<string, ApplicationPreferenceClientData>();

			foreach (ApplicationPreferenceClientData applicationPreference in apps)
                this.globalApplicationPreferences.Add(applicationPreference.Name, applicationPreference);

			#region ReadOnly values from ApplicationPreferences
			TIME_WITHOUT_ATTENTION = int.Parse(globalApplicationPreferences["TimeWithoutBeAttended"].Value);
			GLOBAL_TIMER_PERIOD = ApplicationGuiUtil.GLOBAL_TIME_PERIOD;
			#endregion
        }

        public void LoadInitialData()
        {
			Dictionary<string, IList> querys = new Dictionary<string,IList>();
			querys.Add("Units",new ArrayList());
			querys["Units"].Add(SmartCadHqls.GetCustomHql(SmartCadHqls.UnitWithIncidentType));
			querys["Units"].Add(SmartCadHqls.GetCustomHql(SmartCadHqls.UnitWithOfficerAssign));
			querys["Units"].Add(SmartCadHqls.GetCustomHql(SmartCadHqls.UnitWithDepartmentTypeOfOperator, ServerServiceClient.GetInstance().OperatorClient.Code));
			
			querys = ServerServiceClient.GetInstance().SearchClientObjectsMaxRowsMultiQuery(true, querys);
			this.GlobalUnits = querys["Units"];
			
			querys = new Dictionary<string, IList>();
			querys["IncNotifications"] = new ArrayList();
			querys["IncNotifications"].Add(SmartCadHqls.GetCustomHql(SmartCadHqls.IncidentNotificationByStatusByOperatorDepartmentType, IncidentNotificationStatusClientData.New.CustomCode, ServerServiceClient.GetInstance().OperatorClient.Code));
			querys["DepartmentZones"] = new ArrayList();
			querys["DepartmentZones"].Add(SmartCadHqls.GetCustomHql(SmartCadHqls.DepartmentZonesWithDepartmentStationsByOperatorDepartmentType, ServerServiceClient.GetInstance().OperatorClient.Code));
			querys["DepartmentZones"].Add(SmartCadHqls.GetCustomHql(SmartCadHqls.DepartmentZonesByOperatorDepartmentType, ServerServiceClient.GetInstance().OperatorClient.Code));
			querys["OpStatus"] = new ArrayList();
			querys["OpStatus"].Add(SmartCadHqls.GetOperatorStatus);
			querys["apps"] = new ArrayList();
			querys["apps"].Add(SmartCadHqls.GetAllApplicationPreferences);

            //Search for all radios.
            querys["Radios"] = new ArrayList();
            querys["Radios"].Add(SmartCadHqls.GetAllRadioConfiguration);
			
            querys = ServerServiceClient.GetInstance().SearchClientObjectsMaxRowsMultiQuery(true, querys);
			
			this.GlobalIncidentNotifications = querys["IncNotifications"];
			this.GlobalDepartmentZones = querys["DepartmentZones"];
			CreateOperatorStatusMenu(querys["OpStatus"]);
			LoadGlobalApplicationPreferences(querys["apps"]);
            
            // Set radios into variables.
            Radios = querys["Radios"];
            
            PerformFillRecommendedUnits(null, false);
            previewRowLines();
        }

        private void DefaultDispatchFormDevX_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(ServerServiceClient.GetInstance().Configuration.Extension) == true)
            {
                MessageForm.Show(ResourceLoader.GetString2("ExtensionNotProperlyConfigured"), MessageFormType.Warning);
            }

			SetSkin();
            UnableRibbonButtons(new RibbonPageGroup());    
            
           // dataGridExNewIncidentNotifications.ColumnHeaderMouseClick += new DataGridViewCellMouseEventHandler(dataGridExNewIncidentNotifications_ColumnHeaderMouseClick);
            //COMMENTED ON VERSION 3.2.8. NOT REQUIRED FOR BATAAN
            //OpenTelephony();
  
            this.blinkingButtonExPendingDispatch.LookAndFeel.UseDefaultLookAndFeel = false;
            this.blinkingButtonExDispReqAttention.LookAndFeel.UseDefaultLookAndFeel = false;
            this.blinkingButtonExPendingDispatch.LookAndFeel.SkinName = "Blue";
            this.blinkingButtonExDispReqAttention.LookAndFeel.SkinName = "Blue";
            this.gridViewNewIncidentNotifications.SelectionWillChange += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewNewIncidentNotifications_SelectionWillChange);
            this.gridViewNewIncidentNotifications.SingleSelectionChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewNewIncidentNotifications_SelectionChanged);
            this.gridViewAssignedIncidentNotifications.SingleSelectionChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewAssignedIncidentNotifications_SingleSelectionChanged);
            this.gridViewAssignedIncidentNotifications.SelectionWillChange += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewAssignedIncidentNotifications_SelectionWillChange);
            this.gridViewNewIncidentNotifications.FocusedRowHandle = GridControlEx.AutoFilterRowHandle;
            this.gridViewAssignedIncidentNotifications.FocusedRowHandle = GridControlEx.AutoFilterRowHandle;
            this.gridViewUnits.Click += new EventHandler(gridViewUnits_Click);
            this.gridViewDispatchOrder.Click += new EventHandler(gridViewUnits_Click);
            this.gridViewUnits.EnablePreviewLineForFocusedRow = true;
            this.gridViewUnits.OptionsView.ShowPreview = true;
            this.gridViewUnits.SingleSelectionChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewUnits_FocusedRow);
            SetAvailableOperatorStatus();
            RefreshFilter();
            globalTimer = ApplicationUtil.RecurrentExecuteMethod(
                new SimpleDelegate(
                delegate
                {
                    timer_Tick(null, null);
                }), null, null, GLOBAL_TIMER_PERIOD, GLOBAL_TIMER_PERIOD);
            contextMenuStripAssignedIncidentNotif_Opening(null, new CancelEventArgs());
            contextMenuStripNewIncidentNotification_Opening(null, new CancelEventArgs());
            contextMenuStripUnits_Opening(null, new CancelEventArgs());
            contextMenuDispatchOrder_Opening(null, new CancelEventArgs());
        }

        void gridViewUnits_Click(object sender, EventArgs e)
        {
            MouseEventArgs mea = e as MouseEventArgs;
            if (mea != null)
            {
                GridViewEx gridViewEx = sender as GridViewEx;
                DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo gridViewInfo = gridViewEx.CalcHitInfo(mea.X, mea.Y);
				if (mea.Button.Equals(MouseButtons.Right))
					rightClick = true; 
				if (gridViewInfo.InRow == true)
                {
                    gridViewEx.SelectRow(gridViewInfo.RowHandle);
                }
				
            }
        }

        private void SetAvailableOperatorStatus()
        {
            FormUtil.InvokeRequired(this, delegate
            {
                if (barButtonItemStatus.Tag != null && defaultReadyStatus != null && defaultBusyStatus != null)
                {
                    BarItem busy = null;
                    BarItem ready = null;
                    foreach (BarItemLink link in popupMenu1.ItemLinks)
                    {
                        if (link.Item.Tag.Equals(defaultBusyStatus))
                            busy = link.Item;
                        else if (link.Item.Tag.Equals(defaultReadyStatus))
                            ready = link.Item;
                    }
                    if ((checkStillActiveIncidentNotification() == true) && (((OperatorStatusClientData)barButtonItemStatus.Tag).Name.Equals(defaultReadyStatus.Name)) == false)
                    {
                        if (ready != null)
                            ready.Enabled = false;
                        if (busy != null)
                            busy.Enabled = true;
                    }
                    else if ((checkStillActiveIncidentNotification() == false) && (((OperatorStatusClientData)barButtonItemStatus.Tag).Name.Equals(defaultBusyStatus.Name)) == false)
                    {
                        if (ready != null)
                            ready.Enabled = true;
                        if (busy != null)
                            busy.Enabled = false;
                    }
                    else if ((checkStillActiveIncidentNotification() == true) && (((OperatorStatusClientData)barButtonItemStatus.Tag).Name.Equals(defaultReadyStatus.Name)) == true)
                    {
                        if (ready != null)
                        {
                            ready.Enabled = false;                            
                        }
                        FormUtil.InvokeRequired(this, delegate
                        {
                            busy.PerformClick();
                        });
                    }
                    else if ((checkStillActiveIncidentNotification() == false) && (((OperatorStatusClientData)barButtonItemStatus.Tag).Name.Equals(defaultBusyStatus.Name)) == true)
                    {
                        if (busy != null)
                            busy.Enabled = false;
                        FormUtil.InvokeRequired(this, delegate
                        {
                            ready.PerformClick();
                        });
                    }
                }
            });
        }

        private bool CheckUnitsInStatus(IList items, params string[] statusNameList)
        {
            bool result = true;            
            for (int i = 0; i < items.Count && result == true; i++)
            {
                UnitClientData unit = (UnitClientData)((GridControlDataUnit)items[i]).Tag;
                for (int j = 0; j < statusNameList.Length && result == true; j++)
                {
                    if (unit.Status.Name != statusNameList[j])
                        result = false;
                }
            }
            return result;
        }

        private void DoSubscriptionToEvents()
        {
            if (this.parentForm != null)
            {
                this.parentForm.DispatchFormCommittedChanges += new EventHandler<CommittedObjectDataCollectionEventArgs>(serverServiceClient_CommittedChanges);
            }
        }

        private void ClearDataGridIncidentNotificationsInfo()
        {
            ClearSelectionAssignedIncidentNotificationGrid();

            if (gridViewUnits.DataSource != null)
            {

                List<GridControlDataUnit> unitValues = ((BindingList<GridControlDataUnit>)gridViewUnits.DataSource).Where(unit => unit.Recommended == "Si").ToList();

                if (unitValues != null)
                {
                    foreach (GridControlDataUnit gridData in unitValues)
                    {
                        UnitClientData unit = gridData.Tag as UnitClientData;
                        unit.Distance = 0;
                        gridData.BackColor = unit.Status.Color;
                        gridData.Recommended = string.Empty;
                        gridData.Unit = unit;
                        gridData.PreviewFieldValue = "Capacity: " + unit.Capacity + "\tPortable ID: " + unit.IdRadio + "\tDescription: " + unit.Description;
                        unitSyncBox.Sync(gridData, CommittedDataAction.Update);
                    }
                }

                unitValues = ((BindingList<GridControlDataUnit>)gridViewUnits.DataSource).Where(unit => unit.Recommended == "No").ToList();

                if (unitValues != null)
                {
                    foreach (GridControlDataUnit gridData in unitValues)
                    {
                        UnitClientData unit = gridData.Tag as UnitClientData;
                        unit.Distance = 0;
                        gridData.BackColor = unit.Status.Color;
                        gridData.Recommended = string.Empty;
                        gridData.Unit = unit;
                        gridData.PreviewFieldValue = "Capacity: " + unit.Capacity + "\tPortable ID: " + unit.IdRadio + "\tDescription: " + unit.Description;
                        unitSyncBox.Sync(gridData, CommittedDataAction.Update);
                    }
                }
            }

            FormUtil.InvokeRequired(this, delegate
            {
                this.gridControlDispatchOrder.ClearData();
            });
            FormUtil.InvokeRequired(this, delegate
            {
                this.callsAndDispatch.gridControlExTotalCalls.ClearData();
                this.callsAndDispatch.gridControlExTotalDispatchs.ClearData();
            });
            FormUtil.InvokeRequired(this, delegate
            {
                this.radioButtonZone.Enabled = false;
                this.radioButtonDepartment.Enabled = false;
                this.radioButtonAll.Checked = true;

            });
        }

        private void gridViewNewIncidentNotifications_SelectionWillChange(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (this.Disposing == false)
            {
                //FormUtil.InvokeRequired(dataGridExIncidentNotifications,
                //    delegate
                //    {
                //        dataGridExIncidentNotifications.DelayTimerToInfinite();
                //    });

                ClearDataGridIncidentNotificationsInfo();
                FormUtil.InvokeRequired(this,
                    delegate
                    {
                        this.buttonExAddNotification.Enabled = false;
                        this.buttonAddNotes.Enabled = false;
                        this.textBoxIncidentNotes.Enabled = false;
                        this.textBoxIncidentNotes.Clear();
                        this.layoutControlGroupIncidentDetails.Text = ResourceLoader.GetString2("IncidentDetails");
                    });
                FormUtil.InvokeRequired(this.textBoxIncidentDetails, delegate
                {
                    textBoxIncidentDetails.DocumentText = "<span style='HEIGHT:100%;FONT-SIZE: 10pt; FONT-FAMILY: Sans-Serif'>" + ResourceLoader.GetString2("SelectedDispatchIncidentDetails") + "</span>";
                });
                FormUtil.InvokeRequired(this, delegate
                {
                    this.callsAndDispatch.richTextBoxPhoneReportDetails.DocumentText = "<span style='HEIGHT:100%;FONT-SIZE: 10pt; FONT-FAMILY: Sans-Serif'>" + ResourceLoader.GetString2("SelectedCallIncidentPhoneReport") + "</span>";
                });

                FormUtil.InvokeRequired(this, delegate
                {
                    barButtonItemLocateObject.Enabled = true;
                    barButtonItemSave.Enabled = false;
                    barButtonItemPrint.Enabled = false;                             
                    barButtonItemRefreshOrder.Enabled = false;
                });
                this.gridViewNewIncidentNotifications.Focus();
            }
            contextMenuStripNewIncidentNotification_Opening(null, new CancelEventArgs());
            contextMenuStripAssignedIncidentNotif_Opening(null, new CancelEventArgs());
        }


        private void ClearSelectionNewIncidentNotificationGrid()
        {
            FormUtil.InvokeRequired(this,
                delegate
                {
                    if (gridViewNewIncidentNotifications.SelectedRowsCount > 0)
                    {
                        gridViewNewIncidentNotifications.SelectionWillChange -=new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(gridViewNewIncidentNotifications_SelectionWillChange);
                        gridViewNewIncidentNotifications.SingleSelectionChanged -=new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(gridViewNewIncidentNotifications_SelectionChanged); 

                        gridViewNewIncidentNotifications.FocusedRowHandle = GridControlEx.AutoFilterRowHandle;

                        gridViewNewIncidentNotifications.SelectionWillChange += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(gridViewNewIncidentNotifications_SelectionWillChange);
                        gridViewNewIncidentNotifications.SingleSelectionChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(gridViewNewIncidentNotifications_SelectionChanged); 
                    }
                });
        }

        private void ClearSelectionAssignedIncidentNotificationGrid()
        {
            FormUtil.InvokeRequired(this,
                delegate
                {
                    if (gridViewAssignedIncidentNotifications.SelectedRowsCount > 0)
                    {
                        gridViewAssignedIncidentNotifications.SelectionWillChange -= new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(gridViewAssignedIncidentNotifications_SelectionWillChange);
                        gridViewAssignedIncidentNotifications.SingleSelectionChanged -= new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(gridViewAssignedIncidentNotifications_SingleSelectionChanged);
                                        
                        gridViewAssignedIncidentNotifications.FocusedRowHandle = GridControlEx.AutoFilterRowHandle;

                        gridViewAssignedIncidentNotifications.SelectionWillChange += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(gridViewAssignedIncidentNotifications_SelectionWillChange);
                        gridViewAssignedIncidentNotifications.SingleSelectionChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(gridViewAssignedIncidentNotifications_SingleSelectionChanged);

                    }
                });
        }

        private void ClearSelectionMultipleGrid(GridViewEx gridView)
        {
            FormUtil.InvokeRequired(this,
                delegate
                {
                    if (gridView.SelectedRowsCount > 0)
                    {
                        gridView.ClearSelection();
                       
                    }
                });
        }

       
        private void gridViewAssignedIncidentNotifications_SelectionWillChange(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (this.Disposing == false)
            {
                //FormUtil.InvokeRequired(dataGridExNewIncidentNotifications,
                //    delegate
                //    {
                //        dataGridExNewIncidentNotifications.DelayTimerToInfinite();
                //    });

                if (sender == null || (sender != null && sender.Equals(buttonActiveDispatch) == false))
                    this.selectedIncidentNotificationCustomCode = null;

                ClearSelectionNewIncidentNotificationGrid();
                FormUtil.InvokeRequired(this, delegate
                {
                    barButtonItemLocateObject.Enabled = false;
                    barItemAsignedUnits.Enabled = false;
                    barItemRecommendedUnits.Enabled = false;
                    if (gridViewUnits.DataSource != null)
                    {

                        List<GridControlDataUnit> unitValues = ((BindingList<GridControlDataUnit>)gridViewUnits.DataSource).Where(unit => unit.Recommended == "Si").ToList();

                        foreach (GridControlDataUnit gridData in unitValues)
                        {
                            UnitClientData unit = gridData.Tag as UnitClientData;
                            unit.Distance = 0;
                            gridData.BackColor = unit.Status.Color;
                            gridData.Recommended = string.Empty;
                            gridData.Unit = unit;
                            gridData.PreviewFieldValue = "Capacity: " + unit.Capacity + "\tPortable ID: " + unit.IdRadio + "\tDescription: " + unit.Description;
                            unitSyncBox.Sync(gridData, CommittedDataAction.Update);
                        }

                        unitValues = ((BindingList<GridControlDataUnit>)gridViewUnits.DataSource).Where(unit => unit.Recommended == "No").ToList();

                        foreach (GridControlDataUnit gridData in unitValues)
                        {
                            UnitClientData unit = gridData.Tag as UnitClientData;
                            unit.Distance = 0;
                            gridData.BackColor = unit.Status.Color;
                            gridData.Recommended = string.Empty;
                            gridData.Unit = unit;
                            gridData.PreviewFieldValue = "Capacity: " + unit.Capacity + "\tPortable ID: " + unit.IdRadio + "\tDescription: " + unit.Description;
                            unitSyncBox.Sync(gridData, CommittedDataAction.Update);
                        }

                    }
                });
                FormUtil.InvokeRequired(this, delegate
                {
                    this.buttonExAddNotification.Enabled = false;
                    this.buttonExAssign.Enabled = false;
                    this.buttonAddNotes.Enabled = false;
                    this.textBoxIncidentNotes.Enabled = false;
                    this.textBoxIncidentNotes.Clear();
                    barButtonItemLocateObject.Enabled = true;
                });
                IncidentNotificationClientData selectedIncidentNotification = null;
                FormUtil.InvokeRequired(this, delegate
                {
                    if (this.gridControlAssignedIncidentNotifications.SelectedItems.Count > 0)
                    {
                        selectedIncidentNotification =
                            ((GridControlDataAssignedIncidentNotifications)this.gridControlAssignedIncidentNotifications.SelectedItems[0]).Tag as IncidentNotificationClientData;                        
                    }
                });

                FormUtil.InvokeRequired(this, delegate
                {
                    this.gridControlDispatchOrder.ClearData();
                });
                FormUtil.InvokeRequired(this, delegate
                {
                    this.callsAndDispatch.gridControlExTotalCalls.ClearData();
                    this.callsAndDispatch.gridControlExTotalDispatchs.ClearData();
                });
                FormUtil.InvokeRequired(this,
                delegate
                {
                    this.layoutControlGroupIncidentDetails.Text = ResourceLoader.GetString2("IncidentDetails");
                });
                FormUtil.InvokeRequired(this.textBoxIncidentDetails, delegate
                {
                    textBoxIncidentDetails.DocumentText = "<span style='HEIGHT:100%;FONT-SIZE: 10pt; FONT-FAMILY: Sans-Serif'>" + ResourceLoader.GetString2("SelectedDispatchIncidentDetails") + "</span>";

                });
                FormUtil.InvokeRequired(this, delegate
                {
                    this.callsAndDispatch.richTextBoxPhoneReportDetails.DocumentText = "<span style='HEIGHT:100%;FONT-SIZE: 10pt; FONT-FAMILY: Sans-Serif'>" + ResourceLoader.GetString2("SelectedCallIncidentPhoneReport") + "</span>";
               
                    barButtonItemSave.Enabled = false;
                    barButtonItemPrint.Enabled = false;
                    barButtonItemRefreshOrder.Enabled = false;
                });

                FormUtil.InvokeRequired(this.radioButtonZone, delegate
                {
                    this.radioButtonZone.Enabled = true;
                });

                FormUtil.InvokeRequired(this.radioButtonDepartment, delegate
                {
                    this.radioButtonDepartment.Enabled = true;
                });
               
                FormUtil.InvokeRequired(this, delegate
                {
                    if (this.gridViewAssignedIncidentNotifications.SelectedRowsCount == 0)
                    {
                        this.radioButtonZone.Enabled = false;
                        this.radioButtonDepartment.Enabled = false;
                        this.radioButtonAll.Checked = true;

                    }
                });
                FormUtil.InvokeRequired(this, delegate
                {
                    this.gridViewAssignedIncidentNotifications.Focus();
                });
                if (barButtonItemStatus.Tag != null && defaultReadyStatus != null && defaultBusyStatus != null)
                {
                    BarItem busy = null;
                    BarItem ready = null;
                    foreach (BarItemLink link in popupMenu1.ItemLinks)
                    {
                        if (link.Item.Tag.Equals(defaultBusyStatus))
                            busy = link.Item;
                        else if (link.Item.Tag.Equals(defaultReadyStatus))
                            ready = link.Item;
                    }
                    if ((checkStillActiveIncidentNotification() == true) && (((OperatorStatusClientData)barButtonItemStatus.Tag).Name.Equals(defaultReadyStatus.Name)) == false)
                    {
                        if (ready != null)
                            ready.Enabled = false;
                        if (busy != null)
                            busy.Enabled = true;
                    }
                    else if ((checkStillActiveIncidentNotification() == false) && (((OperatorStatusClientData)barButtonItemStatus.Tag).Name.Equals(defaultBusyStatus.Name)) == false)
                    {
                        if (ready != null)
                            ready.Enabled = true;
                        if (busy != null)
                            busy.Enabled = false;
                    }
                    else if ((checkStillActiveIncidentNotification() == true) && (((OperatorStatusClientData)barButtonItemStatus.Tag).Name.Equals(defaultReadyStatus.Name)) == true)
                    {
                        if (ready != null)
                            ready.Enabled = false;
                        FormUtil.InvokeRequired(this, delegate
                        {
                            busy.PerformClick();
                        });
                    }
                    else if ((checkStillActiveIncidentNotification() == false) && (((OperatorStatusClientData)barButtonItemStatus.Tag).Name.Equals(defaultBusyStatus.Name)) == true)
                    {
                        if (busy != null)
                            busy.Enabled = false;
                        FormUtil.InvokeRequired(this, delegate
                        {
                            ready.PerformClick();
                        });
                    }
                }
                EnableSupportRequest(selectedIncidentNotification);
            }
            contextMenuStripNewIncidentNotification_Opening(null, new CancelEventArgs());
            contextMenuStripAssignedIncidentNotif_Opening(null, new CancelEventArgs());
        }


   

        void serverServiceClient_CommittedChanges(object sender, CommittedObjectDataCollectionEventArgs e)
        {
            try
            {
                if (e.Objects != null && e.Objects.Count > 0)
                {
                    lock (syncCommited)
                    {
                        try
                        {
                            if (ServerServiceClient.GetInstance().OperatorClient == null)
                            { }
                        }
                        catch
                        { }
                        if (DispatchCommittedChanges != null)
                        {
                            DispatchCommittedChanges(null, e);
                        }
                        if (e.Objects[0] is IncidentNotificationClientData)
                        {
                            #region IncidentNotificationClientData
                            IncidentNotificationClientData incidentNotificationClient = e.Objects[0] as IncidentNotificationClientData;

                            if (ServerServiceClient.GetInstance().OperatorClient.DepartmentTypes.Contains(incidentNotificationClient.DepartmentType) == true)
                            {
                                if (incidentNotificationClient.Status.Active == true || incidentNotificationClient.Status.Name == IncidentNotificationStatusClientData.ManualSupervisor.Name)
                                {
                                    if (incidentNotificationClient.Status.Name == IncidentNotificationStatusClientData.ManualSupervisor.Name)
                                    {
                                          FormUtil.InvokeRequired(this,
                                            delegate
                                            {    
                                                GridControlDataAssignedIncidentNotifications incidentNotifData = new GridControlDataAssignedIncidentNotifications(incidentNotificationClient);
                                                if (gridControlAssignedIncidentNotifications.Items.Contains(incidentNotifData))
                                                {
                                                    assignedIncidentNotificationSyncBox.Sync(incidentNotifData, CommittedDataAction.Delete);
                                                }
                                            });
                                    }
                                    else if (incidentNotificationClient.DispatchOperatorCode == ServerServiceClient.GetInstance().OperatorClient.Code)
                                    {
                                        TimeSpan diff = ServerServiceClient.GetInstance().GetTime() - incidentNotificationClient.StartDate;

										if (incidentNotificationClient.Status.Name == IncidentNotificationStatusClientData.Assigned.Name && 
                                            diff.TotalSeconds >= TIME_WITHOUT_ATTENTION)
                                        {
                                            incidentNotificationClient.InAttention = true;
                                            
                                        }
                                        if (incidentNotificationClient.Status.Name == IncidentNotificationStatusClientData.Delayed.Name)
                                        {
                                            //restart the counter
                                            counterToCompleteMinute = 0;                                            
                                        }
                                        assignedIncidentNotificationSyncBox.Sync(new GridControlDataAssignedIncidentNotifications(incidentNotificationClient), e.Action);
                                                                               
                                        IncidentNotificationClientData selectedNotification = null;
                                        FormUtil.InvokeRequired(this,
                                            delegate
                                            {                                                
                                                if (this.gridViewAssignedIncidentNotifications.SelectedRowsCount > 0 && this.gridViewAssignedIncidentNotifications.FocusedRowHandle != GridControlEx.AutoFilterRowHandle)
                                                {
                                                    selectedNotification = ((GridControlDataAssignedIncidentNotifications)this.gridControlAssignedIncidentNotifications.SelectedItems[0]).Tag as IncidentNotificationClientData;
                                                }
                                            });

                                        if (selectedNotification != null && selectedNotification.Code == incidentNotificationClient.Code)
                                        {
                                            PerformFillRecommendedUnits(incidentNotificationClient, false);
                                        }
                                      

										if (incidentNotificationClient.Status.Name == IncidentNotificationStatusClientData.Assigned.Name && diff.TotalSeconds < TIME_WITHOUT_ATTENTION)
										{
                                            incidentNotificationButtonFilter = IncidentNotificationButtonFilter.TEXT;
                                            buttonActiveDispatch_Click(buttonActiveDispatch, null);
                                        }
                                    }
                                    foreach (DispatchOrderClientData dispatchOrder in incidentNotificationClient.DispatchOrders)
                                    {
                                        dispatchOrder.Unit.DispatchOrder = dispatchOrder;
                                        unitSyncBox.Sync(new GridControlDataUnit(dispatchOrder.Unit), CommittedDataAction.Update);
                                    }

									if (incidentNotificationClient.Status.Name == IncidentNotificationStatusClientData.Assigned.Name ||
										incidentNotificationClient.Status.Name != IncidentNotificationStatusClientData.Reassigned.Name)
                                    {
                                        IList selectdNewIncident = this.gridControlNewIncidentNotifications.SelectedItems;
                                        
                                        newIncidentNotificationSyncBox.Sync(new GridControlDataNewIncidentNotifications(incidentNotificationClient), CommittedDataAction.Delete);
                                        if (selectdNewIncident.Count == 0)
                                        {
                                            ClearSelectionNewIncidentNotificationGrid();
                                        }
                                        else if ((((GridControlDataNewIncidentNotifications)selectdNewIncident[0]).Tag as 
                                            IncidentNotificationClientData).CustomCode == incidentNotificationClient.CustomCode)
                                        {
                                            ClearSelectionNewIncidentNotificationGrid();
											FormUtil.InvokeRequired(this,
												delegate
												{
													textBoxIncidentDetails.Clear();
													buttonExAddNotification.Enabled = false;
													this.callsAndDispatch.gridControlExTotalCalls.ClearData();
												    this.callsAndDispatch.gridControlExTotalDispatchs.ClearData();
                                                    this.layoutControlGroupIncidentDetails.Text = ResourceLoader.GetString2("IncidentDetails");
													textBoxIncidentDetails.DocumentText = "<span style='HEIGHT:100%;FONT-SIZE: 10pt; FONT-FAMILY: Sans-Serif'>Seleccione una solicitud de despacho para ver los detalles del incidente.</span>";
													this.callsAndDispatch.richTextBoxPhoneReportDetails.DocumentText = "<span style='HEIGHT:100%;FONT-SIZE: 10pt; FONT-FAMILY: Sans-Serif'>Seleccione una llamada del incidente para ver su reporte telefnico.</span>";
													barButtonItemSave.Enabled = false;
													barButtonItemPrint.Enabled = false;
													barButtonItemRefreshOrder.Enabled = false;
												});
                                        }              
                                       
                                        
                                    }
                                    FormUtil.InvokeRequired(gridControlAssignedIncidentNotifications , delegate
                                    {
                                        if (incidentNotificationClient.DispatchOperatorCode == ServerServiceClient.GetInstance().OperatorClient.Code &&
                                            incidentNotificationClient.Status.Name == IncidentNotificationStatusClientData.Assigned.Name &&
                                            incidentNotificationClient.CustomCode == incNotAssigningCode)
                                        {
                                            gridControlAssignedIncidentNotifications.SelectData(new GridControlDataAssignedIncidentNotifications(incidentNotificationClient));
                                            incNotAssigningCode = null;
                                        }
                                    });
                                }
                                else
                                {
									if (incidentNotificationClient.Status.Name == IncidentNotificationStatusClientData.New.Name &&
                                        ServerServiceClient.GetInstance().OperatorClient.DepartmentTypes.Contains(incidentNotificationClient.DepartmentType) == true)
                                    {
                                        CheckConditionsToResetCounter();
                                        gridControlNewIncidentNotifications.AddOrUpdateItem(new GridControlDataNewIncidentNotifications(incidentNotificationClient),false);
                                    }
                                    else if ((incidentNotificationClient.Status.Name == IncidentNotificationStatusClientData.Cancelled.Name ||
                                        incidentNotificationClient.Status.Name == IncidentNotificationStatusClientData.Closed.Name) &&
                                        incidentNotificationClient.DispatchOperatorCode == ServerServiceClient.GetInstance().OperatorClient.Code)
                                    {
                                        assignedIncidentNotificationSyncBox.Sync(new GridControlDataAssignedIncidentNotifications(incidentNotificationClient), CommittedDataAction.Update);
                                        FormUtil.InvokeRequired(gridControlAssignedIncidentNotifications,
                                        delegate
                                        {
                                            gridViewAssignedIncidentNotifications.FocusedRowHandle = GridControlEx.AutoFilterRowHandle;
                                        });                                    
                                    }
                                }


								if (incidentNotificationClient.Status.Name != IncidentNotificationStatusClientData.New.Name &&
                                    incidentNotificationClient.DispatchOperatorCode == ServerServiceClient.GetInstance().OperatorClient.Code)
                                {
                                    CheckIncidentNotificationInAttentionOrPending();
                                }
                            }
                            SetAvailableOperatorStatus();
                            //RefreshFilter();

                            #endregion
                        }
                        else if (e.Objects[0] is UnitClientData)
                        {
							#region UnitClientData
                            UnitClientData unit = e.Objects[0] as UnitClientData;
                            //Actualizacin de las coordenadas de la unidad.
                            bool synchronized = false;
                            FormUtil.InvokeRequired(this,
                            delegate
                            {
                                if (this.gridControlUnits.Items.Contains(new GridControlDataUnit(unit)))
                                {
                                    UnitClientData existingUnit = gridControlUnits.GetGridControlData(unit).Tag as UnitClientData;
                                    if (unit.CoordinatesDate > existingUnit.CoordinatesDate)
                                    {
                                        gridControlUnits.GetGridControlData(unit).Tag = unit;
                                        synchronized = true;
                                    }
                                }
                            });
                            if (synchronized == false)
                            {                                                                
                                if (ServerServiceClient.GetInstance().OperatorClient.DepartmentTypes.Contains(unit.DepartmentStation.DepartmentZone.DepartmentType) == true)
                                {
                                    if ((unit.Status.Name == UnitStatusClientData.NotAvailable.Name ||
                                        unit.Status.Name == UnitStatusClientData.OutOfOrder.Name ||
                                        unit.Status.Name == UnitStatusClientData.Available.Name) &&
                                        unit.DispatchOrder != null)
                                    {
                                        unit.DispatchOrder = null;
                                    }
                                    //bool synchronized = false;

                                    bool foundUnit = false;
                                    //Actualizacin de la unidad si esta visible en el datagrid de despachos

                                    foreach (GridControlDataDispatchOrder item in gridControlDispatchOrder.Items)
                                    {
                                        DispatchOrderClientData docd = item.Tag as DispatchOrderClientData;
                                        if (unit.Code == docd.Unit.Code)
                                        {
                                            if (docd.Unit.Version < unit.Version)
                                            {
                                                docd.Unit = unit;
                                                dispatchOrderSyncBox.Sync(item, CommittedDataAction.Update);
                                                foundUnit = true;
                                            }
                                            break;
                                        }
                                    }


                                    if (foundUnit == false)//Si la unidad no esta visible en el datagrid de despachos, se busca en las notificaciones
                                    {
                                        foreach (GridControlData item in gridControlAssignedIncidentNotifications.Items)
                                        {
                                            IncidentNotificationClientData notification = item.Tag as IncidentNotificationClientData;
                                            if (notification.DispatchOrders != null && notification.DispatchOrders.Count > 0)
                                            {
                                                foreach (DispatchOrderClientData disp in notification.DispatchOrders)
                                                {
                                                    if (disp.Unit.Code == unit.Code)
                                                    {
                                                        disp.Unit = unit;
                                                        foundUnit = true;
                                                        break;
                                                    }
                                                }
                                            }
                                            if (foundUnit == true)
                                            {
                                                break;
                                            }
                                        }


                                        foreach (GridControlData item in gridControlAssignedIncidentNotifications.Items)
                                        {
                                            IncidentNotificationClientData notification = item.Tag as IncidentNotificationClientData;
                                            if (notification.DispatchOrders != null && notification.DispatchOrders.Count > 0)
                                            {
                                                foreach (DispatchOrderClientData disp in notification.DispatchOrders)
                                                {
                                                    if (disp.Unit.Code == unit.Code)
                                                    {
                                                        disp.Unit = unit;
                                                        foundUnit = true;
                                                        break;
                                                    }
                                                }
                                            }
                                            if (foundUnit == true)
                                            {
                                                break;
                                            }
                                        }
                                    }

                                    unitSyncBox.Sync(new GridControlDataUnit(unit), e.Action);
                                    //Actualizar el menu contextual si esta abierto.
                                    contextMenuStripUnits_Opening(null, new CancelEventArgs(false));
                                    CheckRecommendedUnit(unit);
                                    EnableAssignButton();
                                    CheckIncidentNotificationInAttentionOrPending();
                                }
                                else
                                {
                                    unitSyncBox.Sync(new GridControlDataUnit(unit), CommittedDataAction.Delete);
                                    contextMenuStripUnits_Opening(null, new CancelEventArgs());
                                }
                            }
                            #endregion
                        }
                        else if (e.Objects[0] is GPSClientData)
                        {
                            #region GPSClientData
                            FormUtil.InvokeRequired(this,
                            delegate
                            {
                                foreach (GPSClientData gps in e.Objects)
                                {
                                    UnitClientData unitClient = new UnitClientData();
                                    unitClient.Code = gps.UnitCode;
                                    if (this.gridControlUnits.Items.Contains(new GridControlDataUnit(unitClient)))
                                    {
                                        UnitClientData existingUnit = this.gridControlUnits.GetGridControlData(unitClient).Tag as UnitClientData;
                                        if (gps.CoordinatesDate > existingUnit.CoordinatesDate)
                                        {
                                            existingUnit.Lon = gps.Lon;
                                            existingUnit.Lat = gps.Lat;
                                            existingUnit.Speed = gps.Speed;
                                            existingUnit.CoordinatesDate = gps.CoordinatesDate;

                                            this.gridControlUnits.GetGridControlData(unitClient).Tag = existingUnit;
                                        }
                                    }
                                }
                            });
                            #endregion
                        }
                        else if (e.Objects[0] is DispatchOrderClientData)
                        {
                            #region DispatchOrderClientData
                            DispatchOrderClientData dispatchOrder = e.Objects[0] as DispatchOrderClientData;
                            IncidentNotificationClientData selectedNotification = null;
                            FormUtil.InvokeRequired(this,
                                delegate
                                {
                                    if (this.gridControlAssignedIncidentNotifications.SelectedItems.Count > 0)
                                    {
                                        selectedNotification = ((GridControlDataAssignedIncidentNotifications)this.gridControlAssignedIncidentNotifications.SelectedItems[0]).Tag as IncidentNotificationClientData;
                                        if (selectedNotification.Code == dispatchOrder.IncidentNotificationCode &&
                                            selectedNotification.Version != dispatchOrder.IncidentNotificationVersion)
                                        {
                                            ThreadPool.QueueUserWorkItem(delegate(object state)
                                                {
                                                    try
                                                    {
                                                        AskUpdatedObjectToServer(selectedNotification);
                                                    }
                                                    catch
                                                    { }
                                                });
                                        }
                                    }
                                });

                            if (changeDispatchStatusForm != null)
                            {
                                FormUtil.InvokeRequired(changeDispatchStatusForm,
                                delegate
                                {
                                    if (changeDispatchStatusForm.IsDisposed == false && changeDispatchStatusForm.Visible == true)
                                    {
                                        if (selectedNotification != null && selectedNotification.Code == dispatchOrder.IncidentNotificationCode)
                                        {
                                            changeDispatchStatusForm.SynchronizeList(dispatchOrder);
                                        }
                                        else if (changeDispatchStatusForm.SelectedDispatchOrders.Count > 0)
                                        {
                                            DispatchOrderClientData disp = changeDispatchStatusForm.SelectedDispatchOrders[0] as DispatchOrderClientData;
                                            if (disp.Unit.Status.Name == "Assigned" || disp.Unit.Status.Name == "OnScene" || disp.Unit.Status.Name == "Delayed")
                                            {
                                                changeDispatchStatusForm.SynchronizeList(dispatchOrder);
                                            }
                                        }
                                    }
                                });
                            }
                            try
                            {
                                if (dispatchOrder.DispatchOperatorCode == ServerServiceClient.GetInstance().OperatorClient.Code)
                                {
                                    IncidentNotificationClientData incidentNotification = e.Objects[1] as IncidentNotificationClientData;
                                    assignedIncidentNotificationSyncBox.Sync(new GridControlDataAssignedIncidentNotifications(incidentNotification), CommittedDataAction.Update);

                                    FormUtil.InvokeRequired(this,
                                        delegate
                                        {
                                            if (this.gridControlAssignedIncidentNotifications.SelectedItems.Count == SINGLE_SELECTION)
                                            {
                                                IncidentNotificationClientData selectedIncidentNotification = ((GridControlDataAssignedIncidentNotifications)gridControlAssignedIncidentNotifications.SelectedItems[0]).Tag as IncidentNotificationClientData;
                                                if (incidentNotification.Code == selectedIncidentNotification.Code)
                                                {
                                                    if (dispatchOrder.Status.Name == DispatchOrderStatusClientData.Closed.Name ||
                                                        dispatchOrder.Status.Name == DispatchOrderStatusClientData.Cancelled.Name)
                                                    {
                                                        dispatchOrderSyncBox.Sync(new GridControlDataDispatchOrder(dispatchOrder), CommittedDataAction.Delete);
                                                    }
                                                    else
                                                    {
                                                        dispatchOrderSyncBox.Sync(new GridControlDataDispatchOrder(dispatchOrder), e.Action);
                                                    }
                                                }
                                            }

                                            UpdateDispatchOrderInGrids(dispatchOrder);

                                        });


                                    /*Si llega un dispatchOrder que se haya finalizado y la notificacin de incidente asociada resulta
                                     * no tener mas ordenes de despacho, hay que preguntar si se cancela o se cierra.
                                     */
                                    FormUtil.InvokeRequired(this, delegate
                                    {
                                        closingNotificationCustomCode = incidentNotification.CustomCode;
                                        AskForEndIncidentNotification(incidentNotification, false);
                                    });

                                    //TODO: Mejorar este parche. Si el despacho se cancel, la unidad ya no deberia hacer referencia al despacho
                                    if ((dispatchOrder.Unit.Status.Name == UnitStatusClientData.NotAvailable.Name ||
                                         dispatchOrder.Unit.Status.Name == UnitStatusClientData.OutOfOrder.Name ||
                                         dispatchOrder.Unit.Status.Name == UnitStatusClientData.Available.Name) &&
                                        dispatchOrder.Unit.DispatchOrder != null)
                                    {
                                        dispatchOrder.Unit.DispatchOrder = null;
                                    }
                                    unitSyncBox.Sync(new GridControlDataUnit(dispatchOrder.Unit), CommittedDataAction.Update);
                                    CheckRecommendedUnit(dispatchOrder.Unit);
                                    EnableAssignButton();
                                    CheckIncidentNotificationInAttentionOrPending();
                                    if (e.Action == CommittedDataAction.Save
                                        && selectedNotification != null
                                        && dispatchOrder.IncidentNotificationCode == selectedNotification.Code)
                                    {
                                        FormUtil.InvokeRequired(this,
                                            delegate
                                            {
                                                if (this.gridControlDispatchOrder.Items.Contains(new GridControlDataDispatchOrder(dispatchOrder)) == true)
                                                {
                                                    GridControlDataDispatchOrder gridData = this.gridControlDispatchOrder.GetGridControlData(dispatchOrder) as GridControlDataDispatchOrder;
                                                    this.gridControlDispatchOrder.SelectData(gridData);
                                                }
                                            });
                                    }
                                    contextMenuStripUnits_Opening(null, new CancelEventArgs());
                                }
                                else
                                {
                                    UnitClientData unit = dispatchOrder.Unit;
                                    if (ServerServiceClient.GetInstance().OperatorClient.DepartmentTypes.Contains(unit.DepartmentStation.DepartmentZone.DepartmentType) == true)
                                    {
                                        if ((unit.Status.Name == UnitStatusClientData.NotAvailable.Name ||
                                            unit.Status.Name == UnitStatusClientData.OutOfOrder.Name ||
                                            unit.Status.Name == UnitStatusClientData.Available.Name) &&
                                            unit.DispatchOrder != null)
                                        {
                                            unit.DispatchOrder = null;
                                        }

                                        unitSyncBox.Sync(new GridControlDataUnit(unit), CommittedDataAction.Update);
                                        EnableAssignButton();
                                        CheckIncidentNotificationInAttentionOrPending();
                                        contextMenuStripUnits_Opening(null, new CancelEventArgs());
                                    }
                                }
                            }
                            catch
                            {
                                throw;
                            }
                            if (contextMenuStripUnits != null && contextMenuStripUnits.Visible)
                            {
                                FormUtil.InvokeRequired(this,
                                delegate
                                {
                                    contextMenuStripUnits_Opening(null, new CancelEventArgs(false));
                                });
                            }
                            #endregion
                        }
                        else if (e.Objects[0] is IncidentClientData)
                        {
                            #region IncidentClientData
                            IncidentClientData incident = e.Objects[0] as IncidentClientData;
                            ArrayList newNotifications = new ArrayList();
                            ArrayList updatedNotifications = new ArrayList();
                            if (incident.IncidentNotifications == null || incident.IncidentNotifications.Count == 0)
                                incident.IncidentNotifications = ServerServiceClient.GetInstance().SearchClientObjects(SmartCadHqls.GetCustomHql(SmartCadHqls.GetIncidentNotificationsByIncidentCustomCode, incident.CustomCode));
                            GridControlData selectedInc = null;
                            GridControlData selectedUnit = null;
                            if (gridControlAssignedIncidentNotifications.SelectedItems.Count > 0)
                            {
                                selectedInc = (GridControlData)gridControlAssignedIncidentNotifications.SelectedItems[0];
                                if (gridControlUnits.SelectedItems.Count > 0)
                                    selectedUnit = (GridControlData)gridControlUnits.SelectedItems[0];
                            }
                            foreach (IncidentNotificationClientData notification in incident.IncidentNotifications)
                            {
                                if (notification.Status.Name == IncidentNotificationStatusClientData.Updated.Name &&
                                    notification.DispatchOperatorCode == ServerServiceClient.GetInstance().OperatorClient.Code)
                                {
                                    assignedIncidentNotificationSyncBox.Sync(new GridControlDataAssignedIncidentNotifications(notification), e.Action);
                                }
                                else
                                {
                                    if (notification.Status.Name == IncidentNotificationStatusClientData.New.Name &&
                                        ServerServiceClient.GetInstance().OperatorClient.DepartmentTypes.Contains(notification.DepartmentType) == true)
                                    {
                                        CheckConditionsToResetCounter();
                                        gridControlNewIncidentNotifications.AddOrUpdateItem(new GridControlDataNewIncidentNotifications(notification), false);
                                        gridControlNewIncidentNotifications.Refresh();
                                        gridViewNewIncidentNotifications.RefreshData();
                                    }
                                }
                            }
                            CheckIncidentNotificationInAttentionOrPending();
                            if (selectedInc != null)
                            {
                                gridControlAssignedIncidentNotifications.SelectData(selectedInc);
                                if (selectedUnit != null)
                                    gridControlUnits.SelectData(selectedUnit);
                            }
                            #endregion
                        }
                        else if (e.Objects[0] is PhoneReportClientData)
                        {
                            #region PhoneReportClientData
                            PhoneReportClientData phoneReport = e.Objects[0] as PhoneReportClientData;
                            foreach (IncidentNotificationClientData notification in phoneReport.IncidentNotifications)
                            {
                                if (ServerServiceClient.GetInstance().OperatorClient.DepartmentTypes.Contains(notification.DepartmentType) == true)
                                {
                                    CheckConditionsToResetCounter();
                                    newIncidentNotificationSyncBox.Sync(new GridControlDataNewIncidentNotifications(notification), e.Action);
                                }
                            }
                            #endregion
                        }
                        else if (e.Objects[0] is SupportRequestReportClientData)
                        {
                            #region SupportRequestReportClientData
                            SupportRequestReportClientData suppporRequestClientData = e.Objects[0] as SupportRequestReportClientData;
                            foreach (IncidentNotificationClientData notification in suppporRequestClientData.IncidentNotifications)
                            {
                                if (notification.Status.Name == IncidentNotificationStatusClientData.New.Name &&
                                    ServerServiceClient.GetInstance().OperatorClient.DepartmentTypes.Contains(notification.DepartmentType) == true)
                                {
                                    CheckConditionsToResetCounter();
                                    newIncidentNotificationSyncBox.Sync(new GridControlDataNewIncidentNotifications(notification), e.Action);
                                }
                            }
                            #endregion
                        }
                        else if (e.Objects[0] is OperatorStatusClientData)
                        {
                            #region OperatorStatusClientData
                            OperatorStatusClientData operatorStatus = e.Objects[0] as OperatorStatusClientData;
                            if (e.Action == CommittedDataAction.Save)
                            {
                                CreateOperatorStatusItem(operatorStatus);
                            }
                            else if (e.Action == CommittedDataAction.Update)
                            {
                                UpdateOperatorStatusItem(operatorStatus);
                            }
                            else if (e.Action == CommittedDataAction.Delete)
                            {
                                DeleteOperatorStatusItem(operatorStatus);
                            }
                            #endregion
                        }
                        else if (e.Objects[0] is IncidentTypeClientData)
                        {
                            #region IncidentTypeClientData
                            IncidentTypeClientData incidentTypeClientData = e.Objects[0] as IncidentTypeClientData;

                            foreach (GridControlData item in gridControlNewIncidentNotifications.Items)
                            {
                                IncidentNotificationClientData incd = item.Tag as IncidentNotificationClientData;
                                int codeIndex = incd.IncidentTypeCodes.IndexOf(incidentTypeClientData.Code);
                                if (codeIndex > -1)
                                {
                                    incd.IncidentTypeCustomCodes[codeIndex] = incidentTypeClientData.CustomCode;
                                    incd.IncidentTypeNames[codeIndex] = incidentTypeClientData.Name;
                                    newIncidentNotificationSyncBox.Sync(item, CommittedDataAction.Update);
                                }
                            }


                            foreach (GridControlData item in this.gridControlAssignedIncidentNotifications.Items)
                            {
                                IncidentNotificationClientData incd = item.Tag as IncidentNotificationClientData;
                                int codeIndex = incd.IncidentTypeCodes.IndexOf(incidentTypeClientData.Code);
                                if (codeIndex > -1)
                                {
                                    incd.IncidentTypeCustomCodes[codeIndex] = incidentTypeClientData.CustomCode;
                                    incd.IncidentTypeNames[codeIndex] = incidentTypeClientData.Name;
                                    assignedIncidentNotificationSyncBox.Sync(item, CommittedDataAction.Update);
                                }
                            }

                            #endregion
                        }
                        else if (e.Objects[0] is UnitTypeClientData)
                        {
                            #region UnitTypeClientData
                            UnitTypeClientData unitTypeClientData = e.Objects[0] as UnitTypeClientData;
                            foreach (GridControlDataUnit item in this.gridControlUnits.Items)
                            {
                                UnitClientData ucd = item.Tag as UnitClientData;
                                foreach (int code in unitTypeClientData.Units)
                                {
                                    if (code == ucd.Code)
                                    {
                                        ucd.Type = unitTypeClientData;
                                        unitSyncBox.Sync(item, CommittedDataAction.Update);
                                    }
                                }
                            }

                            foreach (GridControlDataDispatchOrder item in gridControlDispatchOrder.Items)
                            {
                                DispatchOrderClientData docd = item.Tag as DispatchOrderClientData;
                                foreach (int code in unitTypeClientData.Units)
                                {
                                    if (code == docd.Unit.Code)
                                    {
                                        docd.Unit.Type = unitTypeClientData;
                                        this.dispatchOrderSyncBox.Sync(item, CommittedDataAction.Update);
                                    }
                                }
                            }

                            #endregion
                        }
                        else if (e.Objects[0] is OfficerClientData)
                        {
                            #region OfficerClientData
                            OfficerClientData officerClientData = e.Objects[0] as OfficerClientData;

                            bool found = false;

                            foreach (GridControlDataUnit item in gridControlUnits.Items)
                            {
                                UnitClientData ucd = item.Tag as UnitClientData;
                                for (int i = 0; i < ucd.OfficerAssigns.Count && found == false; i++)
                                {
                                    OfficerClientData ocd = ucd.OfficerAssigns[i] as OfficerClientData;
                                    if (ocd.Code == officerClientData.Code)
                                    {
                                        ucd.OfficerAssigns.RemoveAt(i);
                                        i--;
                                        unitSyncBox.Sync(item, CommittedDataAction.Update);
                                        found = true;
                                    }
                                }
                                if (found == true)
                                    break;
                            }

                            if (officerClientData.UnitAssignCode != 0)
                            {
                                foreach (GridControlDataUnit item in gridControlUnits.Items)
                                {
                                    UnitClientData ucd = item.Tag as UnitClientData;
                                    if (officerClientData.UnitAssignCode == ucd.Code)
                                    {
                                        ucd.OfficerAssigns.Add(officerClientData);
                                        unitSyncBox.Sync(item, CommittedDataAction.Update);
                                    }
                                }
                            }
                            #endregion
                        }
                        else if (e.Objects[0] is UserProfileClientData)
                        {
                            UserProfileClientData userProfile = e.Objects[0] as UserProfileClientData;
                            userProfile.Description = "";
                        }
                        else if (e.Objects[0] is DepartmentStationAssignHistoryClientData)
                        {
                            #region DepartmentStationAssignHistoryClientData
                            DepartmentStationAssignHistoryClientData departmentStationAssignHistoryClientData = e.Objects[0] as DepartmentStationAssignHistoryClientData;


                            foreach (KeyValuePair<string, List<OfficerClientData>> item in departmentStationAssignHistoryClientData.Units)
                            {
                                UnitClientData unit = new UnitClientData();
                                unit.CustomCode = item.Key;

                                int index = gridControlUnits.Items.IndexOf(new GridControlDataUnit(unit));

                                if (index > -1)
                                {
                                    GridControlDataUnit data = gridControlUnits.Items[index] as GridControlDataUnit;
                                    UnitClientData ucd = data.Tag as UnitClientData;
                                    ucd.OfficerAssigns = new ArrayList(item.Value);

                                    unitSyncBox.Sync(new GridControlDataUnit(ucd), CommittedDataAction.Update);
                                }
                            }
                            #endregion
                        }
                        else if (e.Objects[0] is ApplicationClientData)
                        {
                            #region ApplicationClientData
                            ApplicationClientData acd = ((ApplicationClientData)e.Objects[0]);
                            if (acd.ToOperators.Contains(ServerServiceClient.GetInstance().OperatorClient.Code))
                            {
                                if (acd.Message == "true")
                                {
                                    if (remoteControlOperators.Contains(acd.Operator.Code) == false)
                                        remoteControlOperators.Add(acd.Operator.Code);
                                    FormUtil.InvokeRequired(this,
                                        delegate
                                        {
                                            if (timerSendImage.Enabled == false) { timerSendImage.Enabled = true; }
                                        });
                                }
                                else if (acd.Message == "false")
                                {
                                    FormUtil.InvokeRequired(this,
                                        delegate
                                        {
                                            if (timerSendImage.Enabled == true) { timerSendImage.Enabled = false; }
                                        });
                                    remoteControlOperators.Remove(acd.Operator.Code);
                                }
                            }
                            #endregion
                        }
                        else if (e.Objects[0] is ApplicationPreferenceClientData)
                        {
                            #region ApplicationPreferenceClientData
                            ApplicationPreferenceClientData preference = e.Objects[0] as ApplicationPreferenceClientData;
                            if (e.Action == CommittedDataAction.Update)
                            {
                                MessageForm.CheckPreference = (preference.Value.ToString().ToUpper() == "TRUE" ? true : false);
                                if (globalApplicationPreferences.ContainsKey(preference.Name))
                                {
                                    globalApplicationPreferences[preference.Name] = preference;
                                }
                                else
                                {
                                    globalApplicationPreferences.Add(preference.Name, preference);
                                }
                                try
                                {
                                    if (preference.Name.Equals("TimeWithoutBeAttended"))
                                    {
                                        TIME_WITHOUT_ATTENTION = int.Parse(globalApplicationPreferences["TimeWithoutBeAttended"].Value);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    SmartLogger.Print(ex);
                                }
                            }
                            #endregion
                        }
                        else if (e.Objects[0] is DepartmentTypeClientData)
                        {
                            #region DepartmentTypeClientData
                            if (e.Action == CommittedDataAction.Update)
                            {
                                DepartmentTypeClientData departmentType = (DepartmentTypeClientData)e.Objects[0];

                                //Refresh Operator
                                if (ServerServiceClient.GetInstance().OperatorClient.DepartmentTypes.Contains(departmentType) == true)
                                {
                                    ServerServiceClient.GetInstance().OperatorClient.DepartmentTypes.Remove(departmentType);
                                    ServerServiceClient.GetInstance().OperatorClient.DepartmentTypes.Add(departmentType);
                                }

                                //Refresh notifications

                                foreach (GridControlData data in gridControlAssignedIncidentNotifications.Items)
                                {
                                    IncidentNotificationClientData incidentNotification = data.Tag as IncidentNotificationClientData;
                                    if (departmentType.Code == incidentNotification.DepartmentType.Code)
                                    {
                                        incidentNotification.DepartmentType = departmentType;
                                        assignedIncidentNotificationSyncBox.Sync(new GridControlDataAssignedIncidentNotifications(incidentNotification), CommittedDataAction.Update);
                                    }
                                }

                                foreach (GridControlData item in gridControlNewIncidentNotifications.Items)
                                {
                                    IncidentNotificationClientData incidentNotification = item.Tag as IncidentNotificationClientData;
                                    if (departmentType.Code == incidentNotification.DepartmentType.Code)
                                    {
                                        incidentNotification.DepartmentType = departmentType;
                                        newIncidentNotificationSyncBox.Sync(new GridControlDataNewIncidentNotifications(incidentNotification), CommittedDataAction.Update);
                                    }
                                }

                                //Refresh units
                                IList units = new ArrayList();

                                foreach (GridControlDataUnit data in gridControlUnits.Items)
                                {
                                    UnitClientData unit = data.Tag as UnitClientData;
                                    if (departmentType.Code == unit.DepartmentStation.DepartmentZone.DepartmentType.Code)
                                    {
                                        unit.DepartmentStation.DepartmentZone.DepartmentType = departmentType;
                                        units.Add(unit);
                                    }
                                }
                                unitSyncBox.Sync(units);
                                IList dispatches = new ArrayList();

                                foreach (GridControlDataDispatchOrder data in gridControlDispatchOrder.Items)
                                {
                                    DispatchOrderClientData disp = data.Tag as DispatchOrderClientData;
                                    if (departmentType.Code == disp.DepartmentType.Code)
                                    {
                                        disp.DepartmentType = departmentType;
                                        dispatches.Add(disp);
                                    }
                                }
                                dispatchOrderSyncBox.Sync(dispatches);

                                foreach (GridControlDataAssociatedDispatchOrder data in this.callsAndDispatch.gridControlExTotalDispatchs.Items)
                                {
                                    DispatchOrderClientData disp = data.Tag as DispatchOrderClientData;
                                    if (departmentType.Code == disp.DepartmentType.Code)
                                    {
                                        disp.DepartmentType = departmentType;
                                        this.callsAndDispatch.gridControlExTotalDispatchs.AddOrUpdateItem(new GridControlDataAssociatedDispatchOrder(disp));
                                    }
                                }

                                #region Radio
                                //Refresh Department Types that the operator can talk.
                                FillDepartmentsRadio();
                                #endregion
                            }
                            #endregion
                        }
                        else if (e.Objects[0] is DepartmentZoneClientData)
                        {
                            #region DepartmentZoneClientData
                            DepartmentZoneClientData departmentZone = (DepartmentZoneClientData)e.Objects[0];
                            if (e.Action == CommittedDataAction.Update)
                            {
                                //Refresh notifications
                                ArrayList notifications = new ArrayList();

                                foreach (GridControlData data in gridControlAssignedIncidentNotifications.Items)
                                {
                                    IncidentNotificationClientData incidentNotification = data.Tag as IncidentNotificationClientData;
                                    if (departmentZone.Code == incidentNotification.DepartmentStation.DepartmentZone.Code)
                                    {
                                        incidentNotification.DepartmentStation.DepartmentZone = departmentZone;
                                        notifications.Add(incidentNotification);
                                    }
                                }
                                assignedIncidentNotificationSyncBox.Sync(notifications);
                                //Refresh units
                                IList units = new ArrayList();

                                foreach (GridControlDataUnit data in gridControlUnits.Items)
                                {
                                    UnitClientData unit = data.Tag as UnitClientData;
                                    if (departmentZone.Code == unit.DepartmentStation.DepartmentZone.Code)
                                    {
                                        unit.DepartmentStation.DepartmentZone = departmentZone;
                                        units.Add(unit);
                                    }
                                }
                                unitSyncBox.Sync(units);
                                IList dispatches = new ArrayList();

                                foreach (GridControlDataDispatchOrder data in gridControlDispatchOrder.Items)
                                {
                                    DispatchOrderClientData disp = data.Tag as DispatchOrderClientData;
                                    if (departmentZone.Code == disp.Unit.DepartmentStation.DepartmentZone.Code)
                                    {
                                        disp.Unit.DepartmentStation.DepartmentZone = departmentZone;
                                        dispatches.Add(disp);
                                    }
                                }
                                dispatchOrderSyncBox.Sync(dispatches);

                                //Refresh GlobalDepartmentZone List
                                if (GlobalDepartmentZones.Contains(departmentZone))
                                {
                                    GlobalDepartmentZones[globalDepartmentZones.IndexOf(departmentZone)] = departmentZone;
                                }
                            }
                            else if (e.Action == CommittedDataAction.Save)
                            {
                                GlobalDepartmentZones.Add(departmentZone);
                            }
                            else if (e.Action == CommittedDataAction.Delete)
                            {
                                GlobalDepartmentZones.Remove(departmentZone);
                            }
                            #endregion
                        }
                        else if (e.Objects[0] is DepartmentStationClientData)
                        {
                            #region DepartmentStationClientData
                            DepartmentStationClientData departmentStation = (DepartmentStationClientData)e.Objects[0];
                            if (e.Action == CommittedDataAction.Update)
                            {
                                //Refresh notifications
                                IList notifications = new ArrayList();

                                foreach (GridControlData data in this.gridControlAssignedIncidentNotifications.Items)
                                {
                                    IncidentNotificationClientData incidentNotification = data.Tag as IncidentNotificationClientData;
                                    if (departmentStation.Code == incidentNotification.DepartmentStation.Code)
                                    {
                                        incidentNotification.DepartmentStation = departmentStation;
                                        notifications.Add(incidentNotification);
                                    }
                                }
                                assignedIncidentNotificationSyncBox.Sync(notifications);

                                //Refresh units
                                IList units = new ArrayList();
                                foreach (GridControlDataUnit data in this.gridControlUnits.Items)
                                {
                                    UnitClientData unit = data.Tag as UnitClientData;
                                    if (departmentStation.Code == unit.DepartmentStation.Code)
                                    {
                                        unit.DepartmentStation = departmentStation;
                                        units.Add(unit);
                                    }
                                }
                                unitSyncBox.Sync(units);

                                //Refresh GlobalDepartmentStations
                                foreach (DepartmentZoneClientData zone in GlobalDepartmentZones)
                                {
                                    if (zone.Code == departmentStation.DepartmentZone.Code)
                                    {
                                        for (int i = 0; i < zone.DepartmentStations.Count; i++)
                                        {
                                            DepartmentStationClientData station = (DepartmentStationClientData)zone.DepartmentStations[i];
                                            if (station.Code == departmentStation.Code)
                                            {
                                                station = departmentStation;
                                                break;
                                            }
                                        }
                                        break;
                                    }
                                }
                            }
                            else if (e.Action == CommittedDataAction.Save)
                            {
                                //Refresh GlobalDepartmentStations
                                foreach (DepartmentZoneClientData zone in GlobalDepartmentZones)
                                {
                                    if (zone.Code == departmentStation.DepartmentZone.Code)
                                    {
                                        zone.DepartmentStations.Add(departmentStation);
                                        break;
                                    }
                                }
                            }
                            else if (e.Action == CommittedDataAction.Delete)
                            { 
                                //Refresh GlobalDepartmentStations
                                foreach (DepartmentZoneClientData zone in GlobalDepartmentZones)
                                {
                                    if (zone.Code == departmentStation.DepartmentZone.Code)
                                    {
                                        zone.DepartmentStations.Remove(departmentStation);
                                        break;
                                    }
                                }
                            }
                            #endregion
                        }
                       
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        /// <summary>
        /// This methods checks if there are not notifications unassigned or assigned but no closed.
        /// If there are not notifications, then counterToCompleteMinute is set to 0
        /// </summary>
        private void CheckConditionsToResetCounter()
        {
            try
            {
                List<GridControlDataAssignedIncidentNotifications> notificationList = new List<GridControlDataAssignedIncidentNotifications>(gridControlAssignedIncidentNotifications.Items.Cast<GridControlDataAssignedIncidentNotifications>());
                if (gridControlNewIncidentNotifications.Items.Count == 0 &&
                    notificationList.Count<GridControlDataAssignedIncidentNotifications>(data => data.IncidentNotification.Status.Active == true) == 0)
                    counterToCompleteMinute = 0;
            }
            catch(Exception ex)
            {
                SmartLogger.Info(ex);
            }
        }

        private void RefreshFilter() 
        {
            refreshIncidentFilter = true;
            switch (incidentNotificationButtonFilter)
            {
                case IncidentNotificationButtonFilter.ACTIVE:
                    buttonActiveDispatch_Click(buttonActiveDispatch, null);
                    break;
                case IncidentNotificationButtonFilter.INACTIVE:
                    buttonClosedDispatch_Click(buttonClosedDispatch, null);
                    break;
                case IncidentNotificationButtonFilter.ATTENTION:
                    this.buttonDispReqAttention_Click(blinkingButtonExDispReqAttention, null);
                    break;
                case IncidentNotificationButtonFilter.PENDING:
                    blinkingButtonExPendingDispatch_Click(blinkingButtonExPendingDispatch, null);
                    break;
                default:
                    break;
            }
            refreshIncidentFilter = false;
        
        }

        private void CheckRecommendedUnit(UnitClientData unit)
        {
            if (unit != null && unit.Status.Name == UnitStatusClientData.Available.Name &&
                this.gridControlAssignedIncidentNotifications.SelectedItems.Count > 0)
            {
                IncidentNotificationClientData incidentNotification = (IncidentNotificationClientData)((GridControlDataAssignedIncidentNotifications)gridControlAssignedIncidentNotifications.SelectedItems[0]).Tag;
                GridControlDataUnit gridData = (GridControlDataUnit)this.gridControlUnits.GetGridControlData(unit);
                bool isRecommended = false;
                if (unit.DepartmentStation.DepartmentZone.DepartmentType.Code == incidentNotification.DepartmentType.Code)
                {
                    foreach (int code in incidentNotification.IncidentTypeCodes)
                    {
                        IncidentTypeClientData incidentType = new IncidentTypeClientData();
                        incidentType.Code = code;
                        int index = unit.IncidentTypes.IndexOf(incidentType);
                        if (index != -1)
                        {
                            gridData.BackColor = RECOMMENDED_UNIT;

                            //barItemRecommendedUnits.Enabled = true;
                            
                            gridData.Recommended = ResourceLoader.GetString2("$Message.Yes");
                            if (incidentNotification.IncidentAddress.IsSynchronized == true &&
                                unit.IsSynchronized == true)
                            {
                                GeoPoint point = new GeoPoint(incidentNotification.IncidentAddress.Lon, incidentNotification.IncidentAddress.Lat);
                                GeoPoint unitPoint = new GeoPoint(unit.Lon, unit.Lat);
                                unit.Distance = GisServiceUtil.CalculateDistance(point, unitPoint);
                            }
                            isRecommended = true;
                            break;
                        }
                    }
                }
                if (isRecommended == false)
                {
                    unit.Distance = 0;
                    gridData.BackColor = unit.Status.Color;
                    gridData.Recommended = string.Empty;
                }
                gridData.Unit = unit;
                gridData.Tag = unit;
                gridData.PreviewFieldValue = "Capacity: " + unit.Capacity + "\tPortable ID: " + unit.IdRadio + "\tDescription: " + unit.Description;
                FormUtil.InvokeRequired(this, delegate
                {
                    unitSyncBox.Sync(gridData, CommittedDataAction.Update);
                });
            }
        }

        private void UpdateDispatchOrderInGrids(DispatchOrderClientData dispatchOrder)
        {
            IncidentNotificationClientData incidentNotification = new IncidentNotificationClientData();
            incidentNotification.Code = dispatchOrder.IncidentNotificationCode;
            if (this.gridControlAssignedIncidentNotifications.Items.Contains(new GridControlDataAssignedIncidentNotifications(incidentNotification)) == true)
            {
                GridControlDataAssignedIncidentNotifications data = gridControlAssignedIncidentNotifications.GetGridControlData(incidentNotification) as GridControlDataAssignedIncidentNotifications;
                IncidentNotificationClientData notification = data.Tag as IncidentNotificationClientData;
                for (int i = 0; i < notification.DispatchOrders.Count; i++)
                {
                    DispatchOrderClientData docd = notification.DispatchOrders[i] as DispatchOrderClientData;
                    if (docd.Code == dispatchOrder.Code)
                    {
                        notification.DispatchOrders[i] = dispatchOrder;
                        assignedIncidentNotificationSyncBox.Sync(data, CommittedDataAction.Update);
                        break;
                    }
                }
            }
                     
        }

        private void FillRecommendedUnits(IncidentNotificationClientData incidentNotification)
        {
            bool recommended = false;
            List<UnitClientData> recommendedUnits = new List<UnitClientData>();

            try
            {
                IList unitValues = ((IList)gridViewUnits.DataSource);
                
                if (unitValues != null)
                {
                    for (int i = 0; i < unitValues.Count; i++)
                    {
                        GridControlDataUnit gridData = unitValues[i] as GridControlDataUnit;
                        UnitClientData unit = gridData.Tag as UnitClientData;
                        unit.Distance = 0;
                        gridData.BackColor = unit.Status.Color;
                        gridData.Recommended = string.Empty;
                        gridData.Unit = unit;
                        gridData.PreviewFieldValue = "Capacity: " + unit.Capacity + "\tPortable ID: " + unit.IdRadio + "\tDescription: " + unit.Description;
                        unitSyncBox.Sync(gridData, CommittedDataAction.Update);
                    }

                    if (incidentNotification != null &&
                        incidentNotification.Status.Name != IncidentNotificationStatusClientData.Closed.Name &&
                        incidentNotification.Status.Name != IncidentNotificationStatusClientData.Cancelled.Name)
                    {
                        if (gridViewUnits.DataSource != null)
                        {
                            FormUtil.InvokeRequired(this, delegate
                            {
                                //Unidades filtradas por departamento y por disponibilidad
                                List<GridControlDataUnit> unitListByDepartment = ((BindingList<GridControlDataUnit>)gridViewUnits.DataSource).Where(unit => unit.Status == UnitStatusClientData.Available.FriendlyName && unit.DepartmentType == incidentNotification.DepartmentType.Name).ToList();

                                if (unitListByDepartment.Count > 0)
                                {
                                    IList<GeoPoint> unitPoints = new List<GeoPoint>();
                                    foreach (GridControlDataUnit gridData in unitListByDepartment)
                                    {
                                        UnitClientData unitClient = gridData.Tag as UnitClientData;
                                        GeoPoint unitPoint = new GeoPoint(unitClient.Lon, unitClient.Lat);
                                        unitPoints.Add(unitPoint);
                                    }
                                    GeoPoint point = new GeoPoint(incidentNotification.IncidentAddress.Lon, incidentNotification.IncidentAddress.Lat);
                                    IList<double> unitDistances = GisServiceUtil.GetDistances(point, unitPoints);

                                    int index = -1;
                                    foreach (GridControlDataUnit gridData in unitListByDepartment)
                                    {
                                        UnitClientData unitClient = gridData.Tag as UnitClientData;
                                        string customCode = unitClient.CustomCode;
                                        double distance = (double)unitDistances[++index];
                                        
                                        if (ApplicationUtil.IsRecommendedUnitByIncidentTypes(unitClient, incidentNotification.IncidentTypeNames) == true)
                                        {
                                            gridData.BackColor = RECOMMENDED_UNIT;
                                            gridData.Recommended = ResourceLoader.GetString2("$Message.Yes");
                                            if (incidentNotification.IncidentAddress.IsSynchronized == true &&
                                                unitClient.IsSynchronized == true)
                                            {
                                                unitClient.Distance = distance;
                                                gridData.Unit = unitClient;
                                            }
                                            recommended = true;
                                            recommendedUnits.Add(unitClient);
                                        }
                                        else
                                        {
                                            gridData.BackColor = unitClient.Status.Color;
                                            gridData.Recommended = string.Empty;
                                            unitClient.Distance = distance;
                                            gridData.Unit = unitClient;
                                        }

                                        gridData.PreviewFieldValue = "Capacity: " + unitClient.Capacity + "\tPortable ID: " + unitClient.IdRadio + "\tDescription: " + unitClient.Description;
                                        unitSyncBox.Sync(gridData, CommittedDataAction.Update);
                                    }

                                    if (recommendedUnits.Count > 0)
                                    {
                                        //barItemRecommendedUnits.Enabled = true;
                                        //barItemAsignedUnits.Enabled = true;
                                        this.toolStripMenuItemRecommendedUnits.Enabled = false;
                                        this.toolStripMenuItemAssignedUnits.Enabled = false;
                                    }
                                    else {
                                        barItemRecommendedUnits.Enabled = false;
                                        barItemAsignedUnits.Enabled = false;
                                        this.toolStripMenuItemRecommendedUnits.Enabled = false;
                                        this.toolStripMenuItemAssignedUnits.Enabled = false;
                                    }
                                }
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
            finally
            {
                FormUtil.InvokeRequired(gridControlUnits, delegate
                {
                    if (recommended)
                    {
                        
                        gridViewUnits.SortInfo.ClearAndAddRange(
                           new GridColumnSortInfo[] { 
                                new GridColumnSortInfo(gridViewUnits.Columns["Recommended"], ColumnSortOrder.Descending), 
                                new GridColumnSortInfo(gridViewUnits.Columns["Distance"], ColumnSortOrder.Ascending)});
                    }
                   
                       
                });
            }            
        }

        private string SendUnitsToHighLight(List<UnitClientData> units, HLGisObjectType hlType, AddressClientData address, int incidentCode)
        {
            List<HLGisObject> toSend = new List<HLGisObject>();
            if (address.IsSynchronized)
            {
                IncidentClientData inc = new IncidentClientData();
                inc.Code = incidentCode;
                inc.Address = address;
                inc = (IncidentClientData)ServerServiceClient.GetInstance().RefreshClient(inc);
                HLGisObject hlinc = new HLGisObject();
                hlinc.Data = inc;
                hlinc.HLType = hlType;
                hlinc.Lat = address.Lat;
                hlinc.Lon = address.Lon;
                toSend.Add(hlinc);
            }
            int count = 0;
            foreach (UnitClientData unit in units)
            {
                HLGisObject hlunit = new HLGisObject();
                hlunit.Data = unit;
                hlunit.HLType = HLGisObjectType.Unit;
                hlunit.Lat = unit.Lat;
                hlunit.Lon = unit.Lon;
                if (unit.IsSynchronized == false && unit.Lat == 0 && unit.Lon == 0)
                {
                    count++;
                }
                else
                {
                    toSend.Add(hlunit);
                }
            }
            ApplicationServiceClient.Current(UserApplicationClientData.Map).HighLightObjectsInMap(toSend);
            string message = string.Empty;
            if (units.Count > 0)
            {
                if (count == units.Count)
                {
                    if (hlType == HLGisObjectType.UnitsAsigned)
                    {
                        message = ResourceLoader.GetString2("NoUnitsAssignedHaveLocation");
                    }
                    else if (hlType == HLGisObjectType.UnitsRecommended)
                    {
                        message = ResourceLoader.GetString2("NoUnitsHaveLocation");
                    }
                }
                else if (count == 1)
                {
                    if (hlType == HLGisObjectType.UnitsAsigned)
                    {
                        message = ResourceLoader.GetString2("UnitAssignedWithoutLocation");
                    }
                    else if (hlType == HLGisObjectType.UnitsRecommended)
                    {
                        message = ResourceLoader.GetString2("OneUnitDoesntHaveLocation");
                    }
                }
                else if (count > 1)
                {
                    if (hlType == HLGisObjectType.UnitsAsigned)
                    {
                        message = string.Format(ResourceLoader.GetString2("SomeUnitsAssignedDoesntHaveLocation"), count);
                    }
                    else if (hlType == HLGisObjectType.UnitsRecommended)
                    {
                        message = string.Format(ResourceLoader.GetString2("SomeUnitsDoesntHaveLocation"), count);
                    }
                }
            }
            return message;
        }

        private void assignIncidentNotification(IncidentNotificationClientData incidentNotification) 
        {
            if (incidentNotification.DispatchOperatorCode != 0 &&
                incidentNotification.DispatchOperatorCode != ServerServiceClient.GetInstance().OperatorClient.Code)
            {
               
                ThreadPool.QueueUserWorkItem(delegate(object state)
                {
                    try
                    {
                        AskUpdatedObjectToServer(incidentNotification);
                    }
                    catch
                    { }
                });
                MessageForm.Show(ResourceLoader.GetString2("RequestAssignedAnotherOperator"), MessageFormType.Warning);
            }
            else if (incidentNotification.DispatchOperatorCode == 0)
            {
                if (incidentNotification.IncidentAddress.Lon != 0.0 && incidentNotification.IncidentAddress.Lat != 0.0)
                {
                    incidentNotification.DepartmentStation = ServerServiceClient.GetInstance().GetDepartmentStationForIncidentNotification(incidentNotification);
                }
				IncidentLocationForm incidentLocationForm = new IncidentLocationForm(this, incidentNotification, GlobalDepartmentZones);

				if (incidentLocationForm.ShowDialog() == DialogResult.OK)
                {
                    BackgroundProcessForm processForm = new BackgroundProcessForm(this,
                    new MethodInfo[1]
                    { 
                        GetType().GetMethod("gridControlAssignedIncidentNotifications_DragDrop_Help", 
                        BindingFlags.NonPublic | BindingFlags.Instance) 
                    },
                        new object[1][] 
                    { 
                        new object[1] { incidentNotification } 
                    });
                    processForm.CanThrowError = true;
                    processForm.ShowDialog();

                    //gridControlAssignedIncidentNotifications_DragDrop_Help(incidentNotification);
                }
            }
            else
            {
               
                ThreadPool.QueueUserWorkItem(delegate(object state)
                {
                    try
                    {
                        AskUpdatedObjectToServer(incidentNotification);
                    }
                    catch
                    { }
                });
            }

            gridViewAssignedIncidentNotifications.MoveLastVisible();
        }

        private void gridControlAssignedIncidentNotifications_DragDrop(object sender, DragEventArgs e)
        {
            object[] obj = e.Data.GetData(typeof(object[])) as object[];

            GridControlData item = ((GridViewEx)((GridControlEx)obj[1]).MainView).GetRow((int)obj[0]) as GridControlData;

            IncidentNotificationClientData incidentNotification = item.Tag as IncidentNotificationClientData;

            assignIncidentNotification(incidentNotification);

            e.Effect = DragDropEffects.None;

        
        }
       
        
      

        private void gridControlAssignedIncidentNotifications_DragDrop_Help(IncidentNotificationClientData incidentNotification)
        {
            try
            {
                int c1 = IncidentNotificationStatusClientData.Cancelled.Code;
                int c2 = IncidentNotificationStatusClientData.Closed.Code;
                int count = 0;
                if (gridControlAssignedIncidentNotifications.DataSource != null)
                    count = ((BindingList<GridControlDataAssignedIncidentNotifications>)gridControlAssignedIncidentNotifications.DataSource).Count(gcd => ((IncidentNotificationClientData)gcd.Tag).Status.Code != c1 && ((IncidentNotificationClientData)gcd.Tag).Status.Code != c2);

                if (int.Parse(globalApplicationPreferences["MaxNotificationsToReassign"].Value) > count)
				{
					incidentNotification.DispatchOperatorCode = ServerServiceClient.GetInstance().OperatorClient.Code;
					incidentNotification.Status = IncidentNotificationStatusClientData.Assigned;
					incidentNotification.LastStatusUpdate = ServerServiceClient.GetInstance().GetTime();
					incidentNotification.StartDate = ServerServiceClient.GetInstance().GetTime();

					ServerServiceClient.GetInstance().SaveOrUpdateClientData(incidentNotification);
					incNotAssigningCode = incidentNotification.CustomCode;
				}
				else
				{
					MessageForm.Show(ResourceLoader.GetString2("UserReachAssignedNotificationMaxLimit"), MessageFormType.Information);
				}
            }
            catch (CommunicationObjectFaultedException)
            {
                throw;
            }
            catch (FaultException ex)
            {
                if (ex.Code != null && ex.Code.Name != null
                    && ex.Code.Name.Equals(typeof(DatabaseServerDownException).Name))
                {
                    throw new CommunicationObjectFaultedException(ex.Message, ex);
                }
                else
                {
                    incidentNotification.DispatchOperatorCode = 0;
					incidentNotification.Status = IncidentNotificationStatusClientData.New;
                    incidentNotification.LastStatusUpdate = DateTime.MaxValue;
                    incidentNotification.StartDate = DateTime.MaxValue;
                    ThreadPool.QueueUserWorkItem(delegate(object state)
                    {
                        try
                        {
                            AskUpdatedObjectToServer(incidentNotification);
                        }
                        catch
                        { }
                    });
                    MessageForm.Show(ResourceLoader.GetString2("RequestAssignedAnotherOperator"), MessageFormType.Warning);
                }
            }
            catch (Exception ex)
            {
                MessageForm.Show(ex.Message, ex);
            }
        }

        private void CheckIncidentNotificationInAttentionOrPending()
        {
            int countPending = 0;
            int countAttention = 0;
                   
            foreach (GridControlData data in gridControlAssignedIncidentNotifications.Items)
            {
                IncidentNotificationClientData incidentNotification = data.Tag as IncidentNotificationClientData;
				if (incidentNotification.Status.Name == IncidentNotificationStatusClientData.Pending.Name &&
                    ExistAvailableUnit(incidentNotification.DepartmentType.Name) == true)
                {
                    countPending++;
                }
				else if (incidentNotification.Status.Name == IncidentNotificationStatusClientData.Delayed.Name ||
					incidentNotification.Status.Name == IncidentNotificationStatusClientData.Updated.Name ||
					incidentNotification.Status.Name == IncidentNotificationStatusClientData.Reassigned.Name ||
					(incidentNotification.Status.Name == IncidentNotificationStatusClientData.Assigned.Name &&
                     incidentNotification.InAttention == true ))                    
                {
                    countAttention++;
                }
				else if (incidentNotification.Status.Name == IncidentNotificationStatusClientData.Assigned.Name &&
                      incidentNotification.InAttention == false)
                {
                    TimeSpan diff = ServerServiceClient.GetInstance().GetTime() - incidentNotification.StartDate;
                    if (diff.TotalSeconds >= TIME_WITHOUT_ATTENTION)
                    {
                        GridControlDataAssignedIncidentNotifications gridAssignedIncidentNotificationData = data as GridControlDataAssignedIncidentNotifications;
                        gridAssignedIncidentNotificationData.InAttention = true;
                        countAttention++;
                    }
                }
            }

            if (countPending == 0)
            {
                this.blinkingButtonExPendingDispatch.Blinking = false;
            }
            else
            {
                if (this.blinkingButtonExPendingDispatch.Blinking == false)
                    this.blinkingButtonExPendingDispatch.Blinking = true;
            }

            if (countAttention == 0)
            {
                this.blinkingButtonExDispReqAttention.Blinking = false;
            }
            else
            {
                if (this.blinkingButtonExDispReqAttention.Blinking == false)
                    this.blinkingButtonExDispReqAttention.Blinking = true;
            }
        }
        
        private bool IsGridUnitDataType(DragEventArgs e)
        {
            bool result = false;
            if (e.Data.GetDataPresent(typeof(object[])) == true)
            {
                IList list = (IList)((object[])e.Data.GetData(typeof(object[])))[0];
                
                if (list != null && list.Count > 0)
                {
                    GridControlDataUnit gridData = list[0] as GridControlDataUnit;
                    if (gridData != null)
                    {
                        result = true;
                    }
                }                
            }
            return result;
        }

      
        private void OpenFormToMakeDispatches(IList<DispatchOrderClientData> dispatchOrderList)
        {
            changeDispatchStatusForm =
                new ChangeDispatchStatusForm(DispatchOrderStatusClientData.Dispatched, null, false);            
            changeDispatchStatusForm.SelectedDispatchOrders = (IList)dispatchOrderList;

            DialogResult result = changeDispatchStatusForm.ShowDialog();
            if (result == DialogResult.OK)
            {
                dispatchOrderList = (IList<DispatchOrderClientData>)changeDispatchStatusForm.SelectedDispatchOrders;
                ProcessDispatchOrderList(dispatchOrderList);
            }
            changeDispatchStatusForm = null;
        }

        private void CalculateUnitsByIncidentNotificationZone(IncidentNotificationClientData selectedIncidentNotification,
            IList<DispatchOrderClientData> dispatchOrderList,
            ref int countAvailableUnitsByZone, ref int countUnitsOtherZones)
        {
            if (gridViewUnits.DataSource != null)
            {
                countAvailableUnitsByZone = ((BindingList<GridControlDataUnit>)gridViewUnits.DataSource).Where(unit => unit.Status == UnitStatusClientData.Available.FriendlyName
                                                                                                   && unit.DepartmentType == selectedIncidentNotification.DepartmentType.Name
                                                                                                   && unit.DepartmentZone == selectedIncidentNotification.DepartmentStation.DepartmentZone.Name).ToList().Count;
            }
            foreach (DispatchOrderClientData dispatchOrder in dispatchOrderList)
            {
                if (dispatchOrder.Unit.DepartmentStation.DepartmentZone.Code != selectedIncidentNotification.DepartmentStation.DepartmentZone.Code)
                {
                    countUnitsOtherZones++;
                }
            }
        }

        private void ProcessDispatchOrderList(IList<DispatchOrderClientData> dispatchOrderList)
        {
            try
            {
                BackgroundProcessForm processForm = new BackgroundProcessForm(this,
                    new MethodInfo[1]
                { 
                    GetType().GetMethod("SaveAndUpdateDispatchOrderList", BindingFlags.NonPublic | BindingFlags.Instance) 
                },
                    new object[1][] 
                { 
                    new object[1] { dispatchOrderList } 
                });
                processForm.CanThrowError = true;
                processForm.ShowDialog();

                Dictionary<UnitClientData, Exception> cancelledDispatchOrders = processForm.Results[0] as Dictionary<UnitClientData, Exception>;

                FormUtil.InvokeRequired(this,
                delegate
                {
                    PaintUnsuccessfulActionOnUnits(cancelledDispatchOrders, true);

                    foreach (GridControlData data in
                        this.gridControlAssignedIncidentNotifications.SelectedItems)
                    {
                        IncidentNotificationClientData incidentNotification = data.Tag
                            as IncidentNotificationClientData;

                        data.BackColor = incidentNotification.Status.Color;
                    }
                });
            }
            catch (CommunicationObjectFaultedException)
            {
                throw;
            }
            catch (FaultException ex)
            {
                if (ex.Code != null && ex.Code.Name != null
                    && ex.Code.Name.Equals(typeof(DatabaseServerDownException).Name))
                {
                    throw new CommunicationObjectFaultedException(ex.Message, ex);
                }
                else
                {
                    MessageForm.Show(ex.Message, ex);
                }
            }
            catch (Exception ex)
            {
                MessageForm.Show(ex.Message, ex);
            }
        }

        private Dictionary<UnitClientData, Exception> SaveAndUpdateDispatchOrderList(IList<DispatchOrderClientData> dispatchOrderList)
        {
            Dictionary<UnitClientData, Exception> cancelledDispatchOrders = new Dictionary<UnitClientData, Exception>();
            for (int i = 0; i < dispatchOrderList.Count; i++)
            {
                DispatchOrderClientData dispatchOrder = dispatchOrderList[i];
                dispatchOrder.DispatchOperatorCode = ServerServiceClient.GetInstance().OperatorClient.Code;
                try
                {
                    ServerServiceClient.GetInstance().SaveOrUpdateClientData(dispatchOrder);
                    Thread.Sleep(100);
                }
                catch (CommunicationObjectFaultedException)
                {
                    throw;
                }
                catch (FaultException ex)
                {
                    if (ex.Code != null && ex.Code.Name != null
                        && ex.Code.Name.Equals(typeof(DatabaseServerDownException).Name))
                    {
                        throw new CommunicationObjectFaultedException(ex.Message, ex);
                    }
                    else
                    {
                        ThreadPool.QueueUserWorkItem(delegate(object state)
                        {
                            try
                            {
                                AskUpdatedObjectToServer(dispatchOrder.Unit);
                            }
                            catch
                            { }
                        });
                        cancelledDispatchOrders.Add(dispatchOrder.Unit, ex);
                    }
                }
                catch (Exception ex)
                {
                    cancelledDispatchOrders.Add(dispatchOrder.Unit, ex);
                }
                Thread.Sleep(250);
            }
            
            return cancelledDispatchOrders;
        }

        private void ProcessUnitStatusChangesList(IList<UnitClientData> unitList)
        {
            try
            {
                BackgroundProcessForm processForm = new BackgroundProcessForm(this,
                    new MethodInfo[1]
                { 
                    GetType().GetMethod("ChangeStatusUnitList", BindingFlags.NonPublic | BindingFlags.Instance) 
                },
                    new object[1][] 
                { 
                    new object[1] { unitList } 
                });
                processForm.CanThrowError = true;
                processForm.ShowDialog();

                Dictionary<UnitClientData, Exception> cancelledUnits = processForm.Results[0] as Dictionary<UnitClientData, Exception>;

                PaintUnsuccessfulActionOnUnits(cancelledUnits, false);
            }
            catch (CommunicationObjectFaultedException)
            {
                throw;
            }
            catch (FaultException ex)
            {
                if (ex.Code != null && ex.Code.Name != null
                    && ex.Code.Name.Equals(typeof(DatabaseServerDownException).Name))
                {
                    throw new CommunicationObjectFaultedException(ex.Message, ex);
                }
                else
                {
                    MessageForm.Show(ex.Message, ex);
                }
            }
            catch (Exception ex)
            {
                MessageForm.Show(ex.Message, ex);
            }
        }

        private Dictionary<UnitClientData, Exception> ChangeStatusUnitList(IList<UnitClientData> unitList)
        {
            Dictionary<UnitClientData, Exception> unitsNotChanged = new Dictionary<UnitClientData, Exception>();
            for (int i = 0; i < unitList.Count; i++)
            {
                UnitClientData unit = unitList[i];
                try
                {
                    ServerServiceClient.GetInstance().SaveOrUpdateClientData(unit);
                    Thread.Sleep(100);
                }
                catch (CommunicationObjectFaultedException)
                {
                    throw;
                }
                catch (FaultException ex)
                {
                    if (ex.Code != null && ex.Code.Name != null
                        && ex.Code.Name.Equals(typeof(DatabaseServerDownException).Name))
                    {
                        throw new CommunicationObjectFaultedException(ex.Message, ex);
                    }
                    else
                    {
                        ThreadPool.QueueUserWorkItem(delegate(object state)
                        {
                            try
                            {
                                AskUpdatedObjectToServer(unit);
                            }
                            catch
                            { }
                        });
                        unitsNotChanged.Add(unit, ex);
                    }
                }
                catch (Exception ex)
                {
                    unitsNotChanged.Add(unit, ex);
                }
                Thread.Sleep(250);
            }

            return unitsNotChanged;
        }

        private void PaintUnsuccessfulActionOnUnits(Dictionary<UnitClientData, Exception> unitsNotChanged, bool dispatches)
        {
            if (unitsNotChanged.Count > 0)
            {
                ShowCollectionForm collectionForm = new ShowCollectionForm(dispatches);
                collectionForm.Units = unitsNotChanged;
                collectionForm.ShowDialog();
            }
        }

        private IList<DispatchOrderClientData> BuildNewDispatchOrderList(DragEventArgs e, 
            ref int countAvailableUnitsBySameZone, ref int countUnitsOtherZones)
        {                
            IList<DispatchOrderClientData> dispatchOrderList = new List<DispatchOrderClientData>();

            object[] data = (object[])e.Data.GetData(typeof(object[]));
            
            IList itemList = (IList)data[0];
            IncidentNotificationClientData selectedIncidentNotification = null;
            
            FormUtil.InvokeRequired(this, delegate
            {
                selectedIncidentNotification = ((GridControlDataAssignedIncidentNotifications)((GridControlDataAssignedIncidentNotifications)this.gridControlAssignedIncidentNotifications.SelectedItems[0])).Tag as IncidentNotificationClientData;
            });

            if (gridViewUnits.DataSource != null)
            {
                countAvailableUnitsBySameZone = ((BindingList<GridControlDataUnit>)gridViewUnits.DataSource).Where(unit => unit.Status == UnitStatusClientData.Available.FriendlyName
                                                                                                       && unit.DepartmentType == selectedIncidentNotification.DepartmentType.Name
                                                                                                       && unit.DepartmentZone == selectedIncidentNotification.DepartmentStation.DepartmentZone.Name).ToList().Count;
            }

			try
			{
				foreach (GridControlDataUnit dataItem in itemList)
				{
					UnitClientData unit = dataItem.Tag as UnitClientData;
					if (unit.DepartmentStation.Code != selectedIncidentNotification.DepartmentStation.Code &&
						globalApplicationPreferences["CanAssignUnitsFromOtherStation"].Value.Equals(false.ToString()))
						throw new Exception();
					if (unit.DepartmentStation.DepartmentZone.DepartmentType.Code == selectedIncidentNotification.DepartmentType.Code)
					{
						if (unit.DepartmentStation.DepartmentZone.Code != selectedIncidentNotification.DepartmentStation.DepartmentZone.Code)
						{
							countUnitsOtherZones++;
						}
						DispatchOrderClientData dispatch = new DispatchOrderClientData();
						dispatch.Address = selectedIncidentNotification.IncidentAddress;
						dispatch.DispatchOperatorCode = ServerServiceClient.GetInstance().OperatorClient.Code;
						dispatch.StartDate = ServerServiceClient.GetInstance().GetTime();
						dispatch.IncidentNotificationCode = selectedIncidentNotification.Code;
						dispatch.IncidentNotificationCustomCode = selectedIncidentNotification.CustomCode;
						dispatch.IncidentNotificationVersion = selectedIncidentNotification.Version;
						dispatch.DepartmentType = unit.DepartmentStation.DepartmentZone.DepartmentType;
						dispatch.Unit = unit;
						dispatch.CustomCode = Guid.NewGuid().ToString();
						dispatchOrderList.Add(dispatch);
					}
				}
			}
			catch
			{
                MessageForm.Show(ResourceLoader.GetString2("NotAllowedToAssignedUnitsFromAnotherZoneOrStation"), MessageFormType.Information);
				countAvailableUnitsBySameZone = 0;
				countUnitsOtherZones = 0;
				dispatchOrderList.Clear();
			}
            return dispatchOrderList;
        }

        private void buttonAssignedUnits_Click(object sender, EventArgs e)
        {
            FormUtil.InvokeRequired(this,
                delegate
                {
                    if (unitButtonFilter != UnitButtonFilter.ASSIGNED)
                    {
                        (sender as SimpleButtonEx).LookAndFeel.UseDefaultLookAndFeel = false;
                        (sender as SimpleButtonEx).LookAndFeel.SkinName = "Black";
                        UnHighlightUnitsButtons(sender as SimpleButtonEx);                       
                        unitButtonFilter = UnitButtonFilter.ASSIGNED;

                        gridViewUnits.ClearColumnsFilter();
                        CriteriaOperator expr1 = new BinaryOperator("Status", UnitStatusClientData.Assigned.FriendlyName, BinaryOperatorType.Equal);
                        expr1 = GroupOperator.Or(new DevExpress.Data.Filtering.CriteriaOperator[] { expr1,  new BinaryOperator("Status", UnitStatusClientData.Delayed.FriendlyName, BinaryOperatorType.Equal)});
                        expr1 = GroupOperator.Or(new DevExpress.Data.Filtering.CriteriaOperator[] { expr1, new BinaryOperator("Status", UnitStatusClientData.OnScene.FriendlyName, BinaryOperatorType.Equal)});

                        unitStatusFilterExpression = expr1;
                        gridViewUnits.ActiveFilterCriteria = showByFilterExpression & unitStatusFilterExpression;

                        EnableAssignButton();                   
                    }
                    this.gridViewUnits.Focus();
                });
        }

        private void buttonAllUnits_Click(object sender, EventArgs e)
        {

            FormUtil.InvokeRequired(this, delegate
            {
                if (unitButtonFilter != UnitButtonFilter.ALL) 
                {
                    (sender as SimpleButtonEx).LookAndFeel.UseDefaultLookAndFeel = false;
                    (sender as SimpleButtonEx).LookAndFeel.SkinName = "Black";
                     UnHighlightUnitsButtons(sender as SimpleButtonEx);
                     unitButtonFilter = UnitButtonFilter.ALL;

                     unitStatusFilterExpression = null;
                     gridViewUnits.ActiveFilterCriteria = showByFilterExpression & unitStatusFilterExpression;
                   
                     EnableAssignButton();
                }

                gridViewUnits.Focus();

            });
        }

		private void UnHighlightUnitsButtons(SimpleButtonEx button)
		{
            if (button.Name == this.buttonAllUnits.Name)
            {
                buttonAssignedUnits.LookAndFeel.SkinName = "Blue";
                buttonAvailableUnits.LookAndFeel.SkinName = "Blue";
                buttonReqAttention.LookAndFeel.SkinName = "Blue";
            }
            else if (button.Name == this.buttonAssignedUnits.Name)
            {
                buttonAllUnits.LookAndFeel.SkinName = "Blue";
                buttonAvailableUnits.LookAndFeel.SkinName = "Blue";
                buttonReqAttention.LookAndFeel.SkinName = "Blue";
            }
            else if (button.Name == this.buttonAvailableUnits.Name)
            {
                buttonAllUnits.LookAndFeel.SkinName = "Blue";
                buttonAssignedUnits.LookAndFeel.SkinName = "Blue";
                buttonReqAttention.LookAndFeel.SkinName = "Blue";
            }
            else if (button.Name == this.buttonReqAttention.Name)
            {
                buttonAllUnits.LookAndFeel.SkinName = "Blue";
                buttonAssignedUnits.LookAndFeel.SkinName = "Blue";
                buttonAvailableUnits.LookAndFeel.SkinName = "Blue";
            }
		}

        private void buttonClosedDispatch_Click(object sender, EventArgs e)
        {          
            try
            {
                if (incidentNotificationButtonFilter != IncidentNotificationButtonFilter.INACTIVE || refreshIncidentFilter == true)
                {
                    (sender as SimpleButtonEx).LookAndFeel.UseDefaultLookAndFeel = false;
                    (sender as SimpleButtonEx).LookAndFeel.SkinName = "Black";
                    UnHighlightDispatchOrdersButtons(sender as SimpleButtonEx);
                 
                    incidentNotificationButtonFilter = IncidentNotificationButtonFilter.INACTIVE;
                    this.layoutControlGroupAssignedIncidentNotifications.Text = (sender as SimpleButtonEx).Text.Trim() + " " +  ResourceLoader.GetString2("DispatchOrders");

                    this.gridViewAssignedIncidentNotifications.ClearColumnsFilter();
                    CriteriaOperator expr = new BinaryOperator("Status", IncidentNotificationStatusClientData.Closed.FriendlyName, BinaryOperatorType.Equal);
                    expr = GroupOperator.Or(new DevExpress.Data.Filtering.CriteriaOperator[] { expr, new BinaryOperator("Status", IncidentNotificationStatusClientData.Cancelled.FriendlyName, BinaryOperatorType.Equal) });

                    this.gridViewAssignedIncidentNotifications.ActiveFilterCriteria = expr;                 
                }
                this.gridViewAssignedIncidentNotifications.Focus();
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        private void buttonActiveDispatch_Click(object sender, EventArgs e)
        {  
            try
            {
                FormUtil.InvokeRequired(this,
                    delegate
                    {

                        if (incidentNotificationButtonFilter != IncidentNotificationButtonFilter.ACTIVE || refreshIncidentFilter == true)
                        {
                            (sender as SimpleButtonEx).LookAndFeel.UseDefaultLookAndFeel = false;
                           (sender as SimpleButtonEx).LookAndFeel.SkinName = "Black";
                            UnHighlightDispatchOrdersButtons(sender as SimpleButtonEx);
                            incidentNotificationButtonFilter = IncidentNotificationButtonFilter.ACTIVE;
                            this.layoutControlGroupAssignedIncidentNotifications.Text = (sender as SimpleButtonEx).Text.Trim() + " " + ResourceLoader.GetString2("DispatchOrders");

                            this.gridViewAssignedIncidentNotifications.ClearColumnsFilter();
                            CriteriaOperator expr = new BinaryOperator("Status", IncidentNotificationStatusClientData.Assigned.FriendlyName, BinaryOperatorType.Equal);
                            expr = GroupOperator.Or(new DevExpress.Data.Filtering.CriteriaOperator[] { expr, new BinaryOperator("Status", IncidentNotificationStatusClientData.Reassigned.FriendlyName, BinaryOperatorType.Equal) });
                            expr = GroupOperator.Or(new DevExpress.Data.Filtering.CriteriaOperator[] { expr, new BinaryOperator("Status", IncidentNotificationStatusClientData.Pending.FriendlyName, BinaryOperatorType.Equal) });
                            expr = GroupOperator.Or(new DevExpress.Data.Filtering.CriteriaOperator[] { expr, new BinaryOperator("Status", IncidentNotificationStatusClientData.InProgress.FriendlyName, BinaryOperatorType.Equal) });
                            expr = GroupOperator.Or(new DevExpress.Data.Filtering.CriteriaOperator[] { expr, new BinaryOperator("Status", IncidentNotificationStatusClientData.Updated.FriendlyName, BinaryOperatorType.Equal) });
                            expr = GroupOperator.Or(new DevExpress.Data.Filtering.CriteriaOperator[] { expr, new BinaryOperator("Status", IncidentNotificationStatusClientData.Delayed.FriendlyName, BinaryOperatorType.Equal) });
                                                        
                            gridViewAssignedIncidentNotifications.ActiveFilterCriteria = expr;

                        }
                        this.gridViewAssignedIncidentNotifications.Focus();
                    });
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

		private void UnHighlightDispatchOrdersButtons(SimpleButton button)
		{
            if (button.Name == buttonActiveDispatch.Name)
            {
                buttonClosedDispatch.LookAndFeel.SkinName = "Blue";
                blinkingButtonExPendingDispatch.LookAndFeel.SkinName = "Blue";
                blinkingButtonExDispReqAttention.LookAndFeel.SkinName = "Blue";
            }
            else if (button.Name == buttonClosedDispatch.Name)
            {
                buttonActiveDispatch.LookAndFeel.SkinName = "Blue";
                blinkingButtonExPendingDispatch.LookAndFeel.SkinName = "Blue";
                blinkingButtonExDispReqAttention.LookAndFeel.SkinName = "Blue";
            }
            else if (button.Name == blinkingButtonExPendingDispatch.Name)
            {
                buttonActiveDispatch.LookAndFeel.SkinName = "Blue";
                buttonClosedDispatch.LookAndFeel.SkinName = "Blue";
                blinkingButtonExDispReqAttention.LookAndFeel.SkinName = "Blue";
            }
            else if (button.Name == blinkingButtonExDispReqAttention.Name)
            {
                buttonClosedDispatch.LookAndFeel.SkinName = "Blue";
                buttonActiveDispatch.LookAndFeel.SkinName = "Blue";
                blinkingButtonExPendingDispatch.LookAndFeel.SkinName = "Blue";
            }
		}

        private void VerifySelectedUnit(UnitFilter filter)
        {
            if (filter != null && filter.Parameters.Count > 0)
            {
                UnitButtonFilter selectedfilter = (UnitButtonFilter)filter.Parameters[0];
                IList<UnitClientData> selectedUnits = new List<UnitClientData>();
                FormUtil.InvokeRequired(this,
                    delegate
                    {
                        if (this.gridControlUnits.SelectedItems.Count > 0)
                        {
                            selectedUnits.Add(((GridControlDataUnit)gridControlUnits.SelectedItems[0]).Tag as UnitClientData);
                        }
                    });
                if (selectedUnits.Count > 0)
                {
                    if (filter.VisibleAfterFilter(selectedUnits, selectedfilter) == false)
                    {
                        FormUtil.InvokeRequired(this,
                        delegate
                        {
                            this.gridViewUnits.ClearSelection();
                        });
                    }
                }                
            }
        }

        private void toolStripMenuItemAbout_Click(object sender, EventArgs e)
        {
            AboutForm aboutForm = new AboutForm();
            aboutForm.ShowDialog();
        }

        private void buttonAvailableUnits_Click(object sender, EventArgs e)
        {
            FormUtil.InvokeRequired(this,
                delegate
                {
                    if (unitButtonFilter != UnitButtonFilter.AVAILABLE)
                    {
                        (sender as SimpleButtonEx).LookAndFeel.UseDefaultLookAndFeel = false;
                        (sender as SimpleButtonEx).LookAndFeel.SkinName = "Black";
                        UnHighlightUnitsButtons(sender as SimpleButtonEx);
                      
                        unitButtonFilter = UnitButtonFilter.AVAILABLE;

                        gridViewUnits.ClearColumnsFilter();
                        CriteriaOperator expr = new BinaryOperator("Status", UnitStatusClientData.Available.FriendlyName, BinaryOperatorType.Equal);

                        unitStatusFilterExpression = expr;
                        gridViewUnits.ActiveFilterCriteria = showByFilterExpression & unitStatusFilterExpression;
               
                        EnableAssignButton();

                    }
                    gridViewUnits.Focus();

                });



            //FormUtil.InvokeRequired(this,
            //    delegate
            //    {
            //        if (unitButtonFilter != UnitButtonFilter.AVAILABLE)
            //        {
            //            (sender as SimpleButtonEx).LookAndFeel.SkinName = "Black";
            //            UnHighlightUnitsButtons(sender as SimpleButtonEx);
            //            IList parameters = new ArrayList();
            //            unitButtonFilter = UnitButtonFilter.AVAILABLE;
            //            parameters.Add(unitButtonFilter);
            //            parameters.Add(unitRadioButtonFilter);
            //            parameters.Add(ServerServiceClient.GetInstance().OperatorClient);

            //            UnitFilter filter = new UnitFilter(dataGridExUnits, parameters);

            //            if (selectedUnitFilter != null
            //                && ((UnitFilter)selectedUnitFilter).SelectedIncidentNotification != null)
            //            {
            //                filter.SelectedIncidentNotification = ((UnitFilter)selectedUnitFilter).SelectedIncidentNotification;
            //            }
            //            VerifySelectedUnit(filter);
            //            filter.Filter(false);
            //            EnableAssignButton();
            //            selectedUnitFilter = filter;
                    
            //        }
            //        gridViewUnits.Focus();
             
            //    });
        }

        private void ResetColorControlsInPanel(Panel panel)
        {
            foreach (Control control in panel.Controls)
            {
                if (control is BlinkingButtonEx)
                    (control as BlinkingButtonEx).SelectedColor = SystemColors.Control;
                else if (control is Button)
                    control.BackColor = SystemColors.Control;
            }
        }

        private void buttonDispReqAttention_Click(object sender, EventArgs e)
        {
            try
            {
                if (incidentNotificationButtonFilter != IncidentNotificationButtonFilter.ATTENTION || refreshIncidentFilter == true)
                {
                    (sender as BlinkingSimpleButtonEx).LookAndFeel.UseDefaultLookAndFeel = false;
                    (sender as BlinkingSimpleButtonEx).LookAndFeel.SkinName = "Black";
                    UnHighlightDispatchOrdersButtons(sender as BlinkingSimpleButtonEx);
                    incidentNotificationButtonFilter = IncidentNotificationButtonFilter.ATTENTION;

                    layoutControlGroupAssignedIncidentNotifications.Text = (sender as BlinkingSimpleButtonEx).Text.Trim() + " " + ResourceLoader.GetString2("DispatchOrders");

                    this.gridViewAssignedIncidentNotifications.ClearColumnsFilter();
                    CriteriaOperator exprA = new BinaryOperator("Status", IncidentNotificationStatusClientData.Reassigned.FriendlyName, BinaryOperatorType.Equal);
                    exprA = GroupOperator.Or(new DevExpress.Data.Filtering.CriteriaOperator[] { exprA, new BinaryOperator("Status", IncidentNotificationStatusClientData.Updated.FriendlyName, BinaryOperatorType.Equal) });
                    exprA = GroupOperator.Or(new DevExpress.Data.Filtering.CriteriaOperator[] { exprA, new BinaryOperator("Status", IncidentNotificationStatusClientData.Delayed.FriendlyName, BinaryOperatorType.Equal) });
                    CriteriaOperator exprB = new BinaryOperator("Status", IncidentNotificationStatusClientData.Assigned.FriendlyName, BinaryOperatorType.Equal);
                    exprB = GroupOperator.And(new DevExpress.Data.Filtering.CriteriaOperator[] { exprB, new BinaryOperator("InAttention", true)});

                    gridViewAssignedIncidentNotifications.ActiveFilterCriteria = exprA | exprB;


                    if (this.gridControlAssignedIncidentNotifications.SelectedItems.Count == 0)
                    {
                        gridViewAssignedIncidentNotifications_SingleSelectionChanged(this.gridViewAssignedIncidentNotifications, null);
                    }

                }
                FormUtil.InvokeRequired(this, delegate
                {
                    this.gridViewAssignedIncidentNotifications.Focus();
                });



                //if (incidentNotificationButtonFilter != IncidentNotificationButtonFilter.ATTENTION)
                //{
                //    (sender as BlinkingSimpleButtonEx).LookAndFeel.SkinName = "Black";
                //    UnHighlightDispatchOrdersButtons(sender as BlinkingSimpleButtonEx);                            
                //    UpdateFilter(IncidentNotificationButtonFilter.ATTENTION);
                //    groupControlIncidentNotifications.Text = ResourceLoader.GetString2("DispatchOrders") + " - " + (sender as BlinkingSimpleButtonEx).Text.Trim();
					
                //    if (this.gridControlAssignedIncidentNotifications.SelectedItems.Count == 0)
                //    {
                //        gridViewAssignedIncidentNotifications_SelectionChanged(this.gridViewAssignedIncidentNotifications, null);
                //    }
                 
                //}
                //FormUtil.InvokeRequired(this, delegate
                //{
                //    this.gridViewAssignedIncidentNotifications.Focus();
                //});
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        //private void UpdateFilter(IncidentNotificationButtonFilter filterType)
        //{
        //    ArrayList parameters = new ArrayList();
        //    incidentNotificationButtonFilter = filterType;
        //    parameters.Add(incidentNotificationButtonFilter);
        //    parameters.Add(this.ServerServiceClient.GetInstance().OperatorClient);
        //    parameters.Add(this.gridViewUnits.GetVisibleRows("[Status] = '" + UnitStatusClientData.Available.FriendlyName + "'"));

        //    selectedAssignedIncidentNotificationFilter.Parameters = parameters;            

        //    VerifySelectedNotification(selectedAssignedIncidentNotificationFilter);
        //    selectedAssignedIncidentNotificationFilter.Filter(false);
        //}                   

        private void EnableAssignedIncNotifMenu(bool enabled, IncidentNotificationClientData selectedIncidentNotification)
        {
            if (selectedIncidentNotification != null)
            {
                this.contextMenuStripAssignedIncidentNotif.Enabled = true;
                this.toolStripMenuItemRefreshIncidentNotitficationDetails.Enabled = true;
				if (selectedIncidentNotification.Status.Name.Equals(IncidentNotificationStatusClientData.Assigned.Name))
                {
                    this.toolStripMenuItemPending.Enabled = selectedIncidentNotification.DispatchOrders.Count == 0;
					barButtonItemPending.Enabled = selectedIncidentNotification.DispatchOrders.Count == 0;
                    this.toolStripMenuItemCloseIncNotif.Enabled = true;
					barButtonItemClose.Enabled = true;
                    this.toolStripMenuItemSupportRequest.Enabled = false;
					barButtonItemSupportRequest.Enabled = false;
                    this.toolStripMenuItemModifyZone.Enabled = selectedIncidentNotification.CanBeChangedZoneAndStation;
					barButtonItemModifyZone.Enabled = selectedIncidentNotification.CanBeChangedZoneAndStation;
                    //barItemRecommendedUnits.Enabled = true;
                    //barItemAsignedUnits.Enabled = true;
                }
                else if (selectedIncidentNotification.InAttention == true)
                {
                    this.toolStripMenuItemPending.Enabled = selectedIncidentNotification.DispatchOrders.Count == 0;
					barButtonItemPending.Enabled = selectedIncidentNotification.DispatchOrders.Count == 0;
					this.toolStripMenuItemCloseIncNotif.Enabled = true;
					barButtonItemClose.Enabled = true;
                    
                    //this.toolStripMenuItemSupportRequest.Enabled = HasActiveDispatchedUnit(selectedIncidentNotification);
                    this.toolStripMenuItemSupportRequest.Enabled = HaveMoreUnitsToRequestSupport(selectedIncidentNotification);

                    barButtonItemSupportRequest.Enabled = HasActiveDispatchedUnit(selectedIncidentNotification);
                    IList statusListNames = new ArrayList();
                    statusListNames.Add(DispatchOrderStatusClientData.Closed.Name);
                    statusListNames.Add(DispatchOrderStatusClientData.Delayed.Name);
                    statusListNames.Add(DispatchOrderStatusClientData.Dispatched.Name);
                    statusListNames.Add(DispatchOrderStatusClientData.UnitOnScene.Name);
                    this.toolStripMenuItemModifyZone.Enabled = selectedIncidentNotification.CanBeChangedZoneAndStation;
					this.barButtonItemModifyZone.Enabled = selectedIncidentNotification.CanBeChangedZoneAndStation;

                    //barItemRecommendedUnits.Enabled = true;
                    //barItemAsignedUnits.Enabled = true;
                }
				else if (selectedIncidentNotification.Status.Name.Equals(IncidentNotificationStatusClientData.Cancelled.Name) ||
					selectedIncidentNotification.Status.Name.Equals(IncidentNotificationStatusClientData.Closed.Name))
                {
                    this.toolStripMenuItemPending.Enabled = false;
					barButtonItemPending.Enabled = false;
					this.toolStripMenuItemCloseIncNotif.Enabled = false;
					barButtonItemClose.Enabled = false;
                    this.toolStripMenuItemSupportRequest.Enabled = false;
					barButtonItemSupportRequest.Enabled = false;
                    this.toolStripMenuItemModifyZone.Enabled = false;
					barButtonItemModifyZone.Enabled = false;
                    //barItemRecommendedUnits.Enabled = false;
                    //barItemAsignedUnits.Enabled = false;
                }
				else if (selectedIncidentNotification.Status.Name.Equals(IncidentNotificationStatusClientData.InProgress.Name))
                {
                    //TODO: Si tuvo unidades pero todas estn canceladas y se desea Cerrar la solicitud, 
                    //el estatus final debe ser cancelada(previa justificacin) 
                    this.toolStripMenuItemPending.Enabled = selectedIncidentNotification.DispatchOrders.Count == 0;
					barButtonItemPending.Enabled = selectedIncidentNotification.DispatchOrders.Count == 0;
					this.toolStripMenuItemCloseIncNotif.Enabled = true;
                    barButtonItemClose.Enabled = true;
                    
                    //this.toolStripMenuItemSupportRequest.Enabled = HasActiveDispatchedUnit(selectedIncidentNotification);
                    this.toolStripMenuItemSupportRequest.Enabled = HasActiveDispatchedUnit(selectedIncidentNotification);// && HaveMoreUnitsToRequestSupport(selectedIncidentNotification);//HaveMoreUnitsToRequestSupport(selectedIncidentNotification);

                    barButtonItemSupportRequest.Enabled = HasActiveDispatchedUnit(selectedIncidentNotification);
                    this.toolStripMenuItemModifyZone.Enabled = false;
					barButtonItemModifyZone.Enabled = false;
                    //barItemRecommendedUnits.Enabled = true;
                    //barItemAsignedUnits.Enabled = true;
                }
				else if (selectedIncidentNotification.Status.Name.Equals(IncidentNotificationStatusClientData.Pending.Name))
                {
                    this.toolStripMenuItemPending.Enabled = false;
					barButtonItemPending.Enabled = false;
					this.toolStripMenuItemCloseIncNotif.Enabled = true;
					barButtonItemClose.Enabled = true;
                    bool hasActiveDispatchedUnit = HasActiveDispatchedUnit(selectedIncidentNotification);
                    
                    this.toolStripMenuItemSupportRequest.Enabled = hasActiveDispatchedUnit;
                    this.toolStripMenuItemSupportRequest.Enabled = HaveMoreUnitsToRequestSupport(selectedIncidentNotification);

                    barButtonItemSupportRequest.Enabled = hasActiveDispatchedUnit;
                    IList statusListNames = new ArrayList();
                    statusListNames.Add(DispatchOrderStatusClientData.Closed.Name);
                    statusListNames.Add(DispatchOrderStatusClientData.Delayed.Name);
                    statusListNames.Add(DispatchOrderStatusClientData.Dispatched.Name);
                    statusListNames.Add(DispatchOrderStatusClientData.UnitOnScene.Name);
                    this.toolStripMenuItemModifyZone.Enabled = selectedIncidentNotification.CanBeChangedZoneAndStation;
					this.barButtonItemModifyZone.Enabled = selectedIncidentNotification.CanBeChangedZoneAndStation;
                    //barItemRecommendedUnits.Enabled = true;
                    //barItemAsignedUnits.Enabled = true;
                }
                if (selectedIncidentNotification.CanBeEnded == true)
                {
                    toolStripMenuItemCloseIncNotif.Enabled = true;
                    barButtonItemClose.Enabled = true;
                }
                else if (selectedIncidentNotification.DispatchOrders != null && selectedIncidentNotification.DispatchOrders.Count > 0)
				{
					toolStripMenuItemCloseIncNotif.Enabled = false;
					barButtonItemClose.Enabled = false;
                }


            }
            else
            {
                this.contextMenuStripAssignedIncidentNotif.Enabled = false;
                this.toolStripMenuItemPending.Enabled = false;
				barButtonItemPending.Enabled = false;
                this.toolStripMenuItemCloseIncNotif.Enabled = false;
				barButtonItemClose.Enabled = false;
                this.toolStripMenuItemSupportRequest.Enabled = false;
				barButtonItemSupportRequest.Enabled = false;
                this.toolStripMenuItemModifyZone.Enabled = false;
				barButtonItemModifyZone.Enabled = false;
                this.toolStripMenuItemRefreshIncidentNotitficationDetails.Enabled = false;
                //barItemRecommendedUnits.Enabled = false;
                //barItemAsignedUnits.Enabled = false;
              
            }            
        }

        private bool HasActiveDispatchedUnit(IncidentNotificationClientData incidentNotification)
        {
            bool result = false;
            for (int i = 0; i < incidentNotification.DispatchOrders.Count && result == false; i++)
            {
                DispatchOrderClientData dispatchOrder = incidentNotification.DispatchOrders[i] as DispatchOrderClientData;
                if (dispatchOrder.Status.Name == DispatchOrderStatusClientData.UnitOnScene.Name)
                {
                    result = true;
                }
            }
            return result;

        }

        private bool HaveMoreUnitsToRequestSupport(IncidentNotificationClientData incidentNotification)
        {
            bool result = true;
            IList departmentTypes = ServerServiceClient.GetInstance().SearchClientObjects(
                SmartCadHqls.GetCustomHql(
                SmartCadHqls.GetDepartmentsTypeWithStationsAssociatedWithAlreadyNotifiedDepartmentsByIncidentCode,
                incidentNotification.IncidentCode));
            Dictionary<int, DepartmentTypeClientData> departmentTypesDictionary = new Dictionary<int, DepartmentTypeClientData>();
            foreach (DepartmentTypeClientData dtcd in departmentTypes)
            {
                departmentTypesDictionary.Add(dtcd.Code, dtcd);
            }

            if (departmentTypesDictionary.Count == 0)
            {
                result = false;
            }

            return result;
        }

        private bool HasDispatchedUnitInStatus(IncidentNotificationClientData incidentNotification, 
            IList statusNameList)
        {
            bool result = false;
            for (int i = 0; i < incidentNotification.DispatchOrders.Count && result == false; i++)
            {
                DispatchOrderClientData dispatchOrder = incidentNotification.DispatchOrders[i] 
                    as DispatchOrderClientData;
                if (statusNameList.Count > 0 && statusNameList.Contains(dispatchOrder.Status.Name))
                {
                    result = true;
                }
            }
            return result;
        }
        
        private bool HasOpenDispatchesIncidentNotification(IncidentNotificationClientData selectedIncidentNotification)
        {
            bool isOpen = false;
            for (int i = 0; i < selectedIncidentNotification.DispatchOrders.Count && isOpen == false; i++)
            {
                DispatchOrderClientData dispOrder = selectedIncidentNotification.DispatchOrders[i] as DispatchOrderClientData;
                if (!dispOrder.Status.Name.Equals(DispatchOrderStatusClientData.Closed.Name) &&
                    !dispOrder.Status.Name.Equals(DispatchOrderStatusClientData.Cancelled.Name))
                {
                    isOpen = true;
                }
            }
            return isOpen;
        }
    
       private void buttonReqAttention_Click(object sender, EventArgs e)
        {
            FormUtil.InvokeRequired(this,
                delegate
                {
                    if (unitButtonFilter != UnitButtonFilter.ATTENTION)
                    {
                        (sender as SimpleButtonEx).LookAndFeel.UseDefaultLookAndFeel = false;
                        (sender as SimpleButtonEx).LookAndFeel.SkinName = "Black";
                        UnHighlightUnitsButtons(sender as SimpleButtonEx);
                       
                        unitButtonFilter = UnitButtonFilter.ATTENTION;

                        gridViewUnits.ClearColumnsFilter();
                        CriteriaOperator expr = new BinaryOperator("Status", UnitStatusClientData.Delayed.FriendlyName, BinaryOperatorType.Equal);
                        unitStatusFilterExpression = expr;

                        gridViewUnits.ActiveFilterCriteria = showByFilterExpression & unitStatusFilterExpression;
                                               
                        EnableAssignButton();
                    }
                    this.gridViewUnits.Focus();
                });


           

            //FormUtil.InvokeRequired(this,
            //    delegate
            //    {
            //        if (unitButtonFilter != UnitButtonFilter.ATTENTION)
            //        {
            //            (sender as SimpleButtonEx).LookAndFeel.SkinName = "Black";
            //            UnHighlightUnitsButtons(sender as SimpleButtonEx);
            //            IList parameters = new ArrayList();
            //            unitButtonFilter = UnitButtonFilter.ATTENTION;
            //            parameters.Add(unitButtonFilter);//Status custom code
            //            parameters.Add(unitRadioButtonFilter);
            //            parameters.Add(ServerServiceClient.GetInstance().OperatorClient);

            //            UnitFilter filter = new UnitFilter(dataGridExUnits, parameters);

            //            if (selectedUnitFilter != null
            //                && ((UnitFilter)selectedUnitFilter).SelectedIncidentNotification != null)
            //            {
            //                filter.SelectedIncidentNotification = ((UnitFilter)selectedUnitFilter).SelectedIncidentNotification;
            //            }
            //            VerifySelectedUnit(filter);
            //            filter.Filter(false);
            //            EnableAssignButton();
            //            selectedUnitFilter = filter;
                      
            //        }
            //        this.gridViewUnits.Focus();
            //    });
        }

        private void FillCallsAndDispatchs(SelectedIncidentTaskResult selectedIncidentTask, bool sameIncident)
        {
            if (selectedIncidentTask != null)            
            {
                FormUtil.InvokeRequired(this, delegate
                {
                    this.callsAndDispatch.gridControlExTotalCalls.SetDataSource(selectedIncidentTask.TotalPhoneReports);
                    this.callsAndDispatch.gridControlExTotalDispatchs.SetDataSource(selectedIncidentTask.TotalDispatchOrders);
                });                               
            }
        }

        private void toolStripMenuItemPending_Click(object sender, EventArgs e)
        {
            IncidentNotificationClientData selectedIncidentNotification = null;
            try
            {

                if (this.gridControlAssignedIncidentNotifications.SelectedItems.Count == SINGLE_SELECTION)
                {
                    selectedIncidentNotification =
                            ((GridControlDataAssignedIncidentNotifications)gridControlAssignedIncidentNotifications.SelectedItems[0]).Tag as IncidentNotificationClientData;
                    if (HasAssignedUnit(selectedIncidentNotification) == false &&
                        selectedIncidentNotification.Status.Active == true &&
						selectedIncidentNotification.Status.Name.Equals(IncidentNotificationStatusClientData.Pending.Name) == false)
                    {
                        if (ExistAvailableUnit(selectedIncidentNotification.DepartmentType.Name) == true)
                        {
                            DialogResult result = MessageForm.Show(ResourceLoader.GetString2("ExistAvailablesUnisPendingIncident"), MessageFormType.Question);
                            if (result == DialogResult.Yes)
                            {
								selectedIncidentNotification.Status = IncidentNotificationStatusClientData.Pending;
                                selectedIncidentNotification.LastStatusUpdate = ServerServiceClient.GetInstance().GetTime();
                                ServerServiceClient.GetInstance().SaveOrUpdateClientData(selectedIncidentNotification);
                            }
                        }
                        else
                        {
							selectedIncidentNotification.Status = IncidentNotificationStatusClientData.Pending;
                            selectedIncidentNotification.LastStatusUpdate = ServerServiceClient.GetInstance().GetTime();
                            ServerServiceClient.GetInstance().SaveOrUpdateClientData(selectedIncidentNotification);
                        }
                    }
                }
            }
            catch (CommunicationObjectFaultedException)
            {
                throw;
            }
            catch (FaultException ex)
            {
                if (ex.Code != null && ex.Code.Name != null
                    && ex.Code.Name.Equals(typeof(DatabaseServerDownException).Name))
                {
                    throw new CommunicationObjectFaultedException(ex.Message, ex);
                }
                else
                {
                    ThreadPool.QueueUserWorkItem(delegate(object state)
                    {
                        try
                        {
                            AskUpdatedObjectToServer(selectedIncidentNotification);
                        }
                        catch
                        { }
                    });
                    MessageForm.Show(ex.Message, ex);
                }
            }
            catch (Exception ex)
            {
                MessageForm.Show(ex.Message, ex);
            }
        }

        private bool ExistAvailableUnit(string departmentName)
        {
            bool exist = false;
            foreach (GridControlData gridUnit in gridControlUnits.Items)
            {
                UnitClientData unitClient = gridUnit.Tag as UnitClientData;
                if (unitClient.DepartmentStation.DepartmentZone.DepartmentType.Name == departmentName &&
                    unitClient.Status.Name == UnitStatusClientData.Available.Name)
                {
                    exist = true;
                    break;
                }
            }
            return exist;
        }

        private bool EnsureLogout()
        {
            DialogResult dialogResult = MessageForm.Show(ResourceLoader.GetString2("$Message.ExitApplication"), MessageFormType.Question);

            return dialogResult == DialogResult.Yes;
        }

        private void DefaultDispatchFormDevX_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
            }
            else
            {
                if (!IsActiveCall() && EnsureLogout())
                {
                    this.blinkingButtonExDispReqAttention.Blinking = false;
                    this.blinkingButtonExPendingDispatch.Blinking = false;
                    this.parentForm.globalTimer.Dispose();
                    if (this.parentForm != null)
                    {
                        this.parentForm.DispatchFormCommittedChanges -= new EventHandler<CommittedObjectDataCollectionEventArgs>(serverServiceClient_CommittedChanges);
                    }
                    CloseTelephony();
                    ServerServiceClient.GetInstance().CloseSession();
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private bool IsActiveCall()
        {
            if (popUpDialer == null)
                return false;
            else
                return popUpDialer.ActiveCall;
        }

        private bool CloseOrCancelOrDelayDispatch(
            DispatchOrderClientData selectedOrder,
            DispatchOrderStatusClientData newStatus,
            bool checkCloseIncidentNotification,
            UnitStatusClientData newUnitStatus, bool isByIncidentNotif)
        {
            bool result = false;
            ArrayList selectedOrders = new ArrayList();
            selectedOrders.Add(selectedOrder);
            result = CloseOrCancelOrDelayDispatch(selectedOrders, newStatus,
                checkCloseIncidentNotification, newUnitStatus, isByIncidentNotif);
            return result;
        }

        private bool CloseOrCancelOrDelayDispatch(IList selectedOrders,
            DispatchOrderStatusClientData newStatus, 
            bool checkCloseIncidentNotification,
            UnitStatusClientData newUnitStatus, bool isByIncidentNotif)
        {
            changeDispatchStatusForm = new ChangeDispatchStatusForm(newStatus, newUnitStatus, isByIncidentNotif);
            changeDispatchStatusForm.SelectedDispatchOrders = selectedOrders;

            DialogResult result = changeDispatchStatusForm.ShowDialog();
            if (result == DialogResult.OK)
            {
                try
                {
                    BackgroundProcessForm processForm = new BackgroundProcessForm(this,
                    new MethodInfo[1]
                { 
                    GetType().GetMethod("CloseOrCancelOrDelayDispatch_Help", BindingFlags.NonPublic | BindingFlags.Instance) 
                },
                    new object[1][] 
                { 
                    new object[3] { changeDispatchStatusForm.SelectedDispatchOrders, 
                        changeDispatchStatusForm.OriginalTimeByDispatchOrder,
                        changeDispatchStatusForm.OriginalUnitStatus} 
                });
                    processForm.CanThrowError = true;
                    processForm.ShowDialog();
                }
                catch (CommunicationObjectFaultedException)
                {
                    throw;
                }
                catch (FaultException ex)
                {
                    if (ex.Code != null && ex.Code.Name != null
                        && ex.Code.Name.Equals(typeof(DatabaseServerDownException).Name))
                    {
                        throw new CommunicationObjectFaultedException(ex.Message, ex);
                    }
                    else
                    {
                        MessageForm.Show(ex.Message, ex);
                    }
                }
                catch (Exception ex)
                {
                    MessageForm.Show(ex.Message, ex);
                }
            }
            else
            {
                for (int i = 0; i < changeDispatchStatusForm.SelectedDispatchOrders.Count; i++)
                {
                    DispatchOrderClientData dispatchOrder = changeDispatchStatusForm.SelectedDispatchOrders[i]
                        as DispatchOrderClientData;
                    dispatchOrder.ExpectedTime = changeDispatchStatusForm.OriginalTimeByDispatchOrder[dispatchOrder.CustomCode];                    
                }
            }
            changeDispatchStatusForm = null;
            return result == DialogResult.OK;
        }

        private void CloseOrCancelOrDelayDispatch_Help(IList selectedDispatchOrders,
            Dictionary<string, TimeSpan> originalTimeByDispatchOrder,
            Dictionary<string, UnitStatusClientData> originalUnitStatus)
        {
            for (int i = 0; i < selectedDispatchOrders.Count; i++)
            {
                DispatchOrderClientData dispatchOrder = selectedDispatchOrders[i]
                    as DispatchOrderClientData;
                try
                {
                    ServerServiceClient.GetInstance().SaveOrUpdateClientData(dispatchOrder);
                }
                catch (Exception ex)
                {
                    if (ex is FaultException)
                    {
                        FaultException fault = ex as FaultException;
                        if (fault.Code != null && fault.Code.Name != null
                        && fault.Code.Name.Equals(typeof(DatabaseServerDownException).Name) == false)
                        {
                            ThreadPool.QueueUserWorkItem(delegate(object state)
                            {
                                try
                                {
                                    AskUpdatedObjectToServer(dispatchOrder);
                                }
                                catch
                                { }
                            });
                        }
                        else
                        {
                            dispatchOrder.ExpectedTime = originalTimeByDispatchOrder[dispatchOrder.CustomCode];
                            dispatchOrder.Unit.Status = originalUnitStatus[dispatchOrder.CustomCode];
                            throw;
                        }
                    }
                }
            }  

            /*try
            {
                ServerServiceClient.GetInstance().SaveOrUpdateClientData(selectedDispatchOrders);
            }
            catch (Exception ex)
            {
                if (ex is FaultException)
                {
                    FaultException fault = ex as FaultException;
                    if (fault.Code != null && fault.Code.Name != null
                    && fault.Code.Name.Equals(typeof(DatabaseServerDownException).Name) == false)
                    {
                        for (int i = 0; i < selectedDispatchOrders.Count; i++)
                        {
                            DispatchOrderClientData dispatchOrder = selectedDispatchOrders[i]
                                as DispatchOrderClientData;
                            ThreadPool.QueueUserWorkItem(delegate(object state)
                            {
                                try
                                {
                                    AskUpdatedObjectToServer(dispatchOrder);
                                }
                                catch
                                { }
                            });
                        }
                    }
                    else
                    {
                        for (int i = 0; i < selectedDispatchOrders.Count; i++)
                        {
                            DispatchOrderClientData dispatchOrder = selectedDispatchOrders[i]
                                as DispatchOrderClientData;
                            dispatchOrder.ExpectedTime = originalTimeByDispatchOrder[dispatchOrder.CustomCode];
                            dispatchOrder.Unit.Status = originalUnitStatus[dispatchOrder.CustomCode];
                        }
                        throw;
                    }
                }
            } */
        }

        private void contextDispOrderMenu(object sender, EventArgs e)
        {
            ToolStripMenuItem contextMenu = sender as ToolStripMenuItem;
            ArrayList selectedItems = new ArrayList(this.gridControlDispatchOrder.SelectedItems);
            if (selectedItems.Count > 0)
            {
                //Status associated specifically to dispatch order
                if (contextMenu.Equals(cerrarToolStripMenuItem))
                {
					DispatchOrderStatusClientData dispatchOrderStatus = null;
						UnitStatusClientData unitStatus = null;
						IList selectedOrders = new ArrayList();
						foreach (GridControlDataDispatchOrder item in selectedItems)
						{
							if (unitStatus == null)
								unitStatus = ((DispatchOrderClientData)item.Tag).Unit.Status;
							selectedOrders.Add(item.Tag);
						}

						try
						{
							if (unitStatus.Name == UnitStatusClientData.OnScene.Name)
							{
								dispatchOrderStatus = DispatchOrderStatusClientData.Closed;
							}
							else
							{
								if (globalApplicationPreferences["CanCancelDispatchUnit"].Value.Equals(true.ToString()))
								{
									dispatchOrderStatus = DispatchOrderStatusClientData.Cancelled;
								}
								else
								{
									throw new Exception();

								}
							}
							CloseOrCancelOrDelayDispatch(selectedOrders, dispatchOrderStatus, true, null, false);
						}
						catch
						{
							MessageForm.Show(ResourceLoader.GetString2("UserCanCancelDispatchOrder"), MessageFormType.Information);
						}
					
                }
                //Status associated specifically to dispatchOrder
                else if (contextMenu.Equals(modifyTimeOrderToolStripMenuItem))
                {
                    IList selectedOrders = new ArrayList();
                    foreach (GridControlDataDispatchOrder item in selectedItems)
                    {
                        selectedOrders.Add(item.Tag);
                    }
                    CloseOrCancelOrDelayDispatch(selectedOrders, DispatchOrderStatusClientData.Delayed, false, null, false);
                }
                //Status associated specifically to unit
                else if (contextMenu.Equals(enEscenaToolStripMenuItem))
                {
                    try
                    {
                        foreach (GridControlDataDispatchOrder item in selectedItems)
                        {
                            DispatchOrderClientData selectedOrder = (DispatchOrderClientData)item.Tag;
                            selectedOrder.Status = DispatchOrderStatusClientData.UnitOnScene;
                            ServerServiceClient.GetInstance().SaveOrUpdateClientData(selectedOrder);
                        }
                    }
                    catch (CommunicationObjectFaultedException)
                    {
                        throw;
                    }
                    catch (FaultException ex)
                    {
                        if (ex.Code != null && ex.Code.Name != null
                            && ex.Code.Name.Equals(typeof(DatabaseServerDownException).Name))
                        {
                            throw new CommunicationObjectFaultedException(ex.Message, ex);
                        }
                        else
                        {
                            MessageForm.Show(ex.Message, ex);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageForm.Show(ex.Message, ex);
                    }
                }
            }
        }

        private void AskForEndIncidentNotification(IncidentNotificationClientData incidentNotification,
            bool showMessageForOpenDispatches)
        {
            try
            {
                Monitor.Enter(syncClosingNotification);
                
                if (this.gridViewDispatchOrder.RowCount == 0 && incidentNotification.CanBeCancelled == true && closingNotificationCustomCode == incidentNotification.CustomCode)
                {
                    closingNotificationCustomCode = incidentNotification.CustomCode;
                    DialogResult result = MessageForm.Show(ResourceLoader.GetString2("CloseDispatchNotExistAssignedUnits"), MessageFormType.Question);
                    if (result == DialogResult.Yes)
                    {
                        FormUtil.InvokeRequired(this, delegate
                        {
                            CancelIncidentNotificationForm form = new CancelIncidentNotificationForm(incidentNotification);
                            DialogResult dialogResult = form.ShowDialog();
                        });
                    }
                    else
                    {
                        closingNotificationCustomCode = null;
                    }
                }
                else if (gridViewDispatchOrder.RowCount == 0 && incidentNotification.CanBeClosed == true && closingNotificationCustomCode == incidentNotification.CustomCode)
                {
                    closingNotificationCustomCode = incidentNotification.CustomCode;
                    //Si la cerramos desde la notificacion, solamente se debe mandar a cerrar y no realizar ninguna pregunta.
                    if (closeIncidentNotificationWithoutMessage == true)
                    {
                        incidentNotification.Status = IncidentNotificationStatusClientData.Closed;
                        incidentNotification.LastStatusUpdate = ServerServiceClient.GetInstance().GetTime();
                        ServerServiceClient.GetInstance().SaveOrUpdateClientData(incidentNotification);
                    }
                    else
                    {
                        DialogResult result = MessageForm.Show(ResourceLoader.GetString2("CloseDispatchNotExistAssignedUnits"), MessageFormType.Question);
                        if (result == DialogResult.Yes)
                        {
                            incidentNotification.Status = IncidentNotificationStatusClientData.Closed;
                            incidentNotification.LastStatusUpdate = ServerServiceClient.GetInstance().GetTime();
                            ServerServiceClient.GetInstance().SaveOrUpdateClientData(incidentNotification);
                            //Set el status de CLOSED a la notificacion de incidente y guardarlo
                        }
                        else
                        {
                            closingNotificationCustomCode = null;
                        }
                    }
                    closeIncidentNotificationWithoutMessage = false;
                }
                else if (showMessageForOpenDispatches == true && incidentNotification.CanBeEnded == true && closingNotificationCustomCode == incidentNotification.CustomCode)
                {
                    closeIncidentNotificationWithoutMessage = true;
                    CloseOrCancelOrDelayDispatch(incidentNotification.DispatchOrders, DispatchOrderStatusClientData.Closed, true, null, true);                  

                }
                else if (showMessageForOpenDispatches == true)
                {
                    MessageForm.Show(ResourceLoader.GetString2("NotCloseNotifyActiveUnitsAssigned"), MessageFormType.Warning);
                }
            }
            catch (CommunicationObjectFaultedException)
            {
                throw;
            }
            catch (FaultException ex)
            {
                if (ex.Code != null && ex.Code.Name != null
                    && ex.Code.Name.Equals(typeof(DatabaseServerDownException).Name))
                {
                    throw new CommunicationObjectFaultedException(ex.Message, ex);
                }
                else
                {
                    ThreadPool.QueueUserWorkItem(delegate(object state)
                    {
                        try
                        {
                            AskUpdatedObjectToServer(incidentNotification);
                        }
                        catch
                        { }
                    });
                    MessageForm.Show(ex.Message, ex);
                }
            }
            catch (Exception ex)
            {
                MessageForm.Show(ex.Message, ex);
            }
            finally
            {
                Monitor.Exit(syncClosingNotification);
            }
        }        

        private string BuildPhoneReportHtml(string xml)
        {
            try
            {
                StringBuilder sb = new StringBuilder();

                XmlReader input = XmlReader.Create(new StringReader(xml));

                XmlWriter output = XmlWriter.Create(sb);

                xslPhoneReportCompiledTransform.Transform(input, output);

                return sb.ToString();
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
                return "";
            }
        }

        private void FillIncidentDetails(SelectedIncidentTaskResult selectedIncidentLightData, bool sameIncident)
        {
            try
            {
                string strXml;
                if (selectedIncidentLightData.SourceApplication == UserApplicationClientData.FirstLevel.Name)
                {
                    strXml = ApplicationUtil.BuildHtml(selectedIncidentLightData.Xml, xslCompiledTransform, "dispatch_incident");
                }
                else if (selectedIncidentLightData.SourceApplication == UserApplicationClientData.Cctv.Name)
                {
                    strXml = ApplicationUtil.BuildHtml(selectedIncidentLightData.Xml, xslCctvCompiledTransform, "dispatch_incident");
                }
                else
                {
                    strXml = ApplicationUtil.BuildHtml(selectedIncidentLightData.Xml, xslAlarmCompiledTransform, "dispatch_incident");
                }
                strXml = ApplicationUtil.RecoverBlanks(strXml);
             
                FileStream fs = File.Open("incident_dispatch.html", FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite);
                byte[] byteXML = Encoding.Unicode.GetBytes(strXml);
                fs.Write(byteXML, 0, byteXML.Length);
                
                fs.Flush();
                fs.Close();
                FormUtil.InvokeRequired(this,
                delegate
                {
                    this.layoutControlGroupIncidentDetails.Text = ResourceLoader.GetString2("IncidentDetails");
                    this.layoutControlGroupIncidentDetails.Text += ResourceLoader.GetString2("UpdatedAt");
                    this.layoutControlGroupIncidentDetails.Text += ServerServiceClient.GetInstance().GetTime().ToString("MM/dd/yyyy HH:mm", ApplicationUtil.GetCurrentCulture());
                });
               // File.WriteAllText("incident_dispatch.html", xml, Encoding.Unicode);

                FormUtil.InvokeRequired(textBoxIncidentDetails, delegate
                {
                    textBoxIncidentDetails.Navigate(new FileInfo("incident_dispatch.html").FullName, sameIncident);
                });
                FormUtil.InvokeRequired(this, delegate
                {
                    barButtonItemSave.Enabled = true;
                });
				FormUtil.InvokeRequired(this, delegate
                {
                    barButtonItemPrint.Enabled = true;
                });
				FormUtil.InvokeRequired(this, delegate
                {
                    barButtonItemRefreshOrder.Enabled = true;
                });
            }
            catch(Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

     

        private void asignarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DataObject data = new DataObject(new object[] {gridControlUnits.SelectedItems });
            DragEventArgs args = new DragEventArgs(data, 0, 0, 0, DragDropEffects.None, DragDropEffects.None);
            gridControlDispatchOrder_DragDrop(gridControlUnits, args);            
        }

        #region Operator status

        private int FindIndexOperatorStatusMenu(OperatorStatusClientData operatorStatus)
        {
            if (operatorStatus.Name == OperatorStatusClientData.Ready.Name)
                return 0;
            if (operatorStatus.Name == OperatorStatusClientData.Busy.Name)
                return 1;

            int index = -1;
			FormUtil.InvokeRequired(this, delegate
			{
				index = popupMenu1.ItemLinks.Count;
				for (int i = 3; i < popupMenu1.ItemLinks.Count; i++)
				{
					BarButtonItem barButtonItem = popupMenu1.ItemLinks[i].Item as BarButtonItem;
					OperatorStatusClientData operatorStatusAux = barButtonItem.Tag as OperatorStatusClientData;
					if (operatorStatusAux != null)
					{
						int res = Comparer<string>.Default.Compare(operatorStatus.FriendlyName, operatorStatusAux.FriendlyName);
						if (res < 0)
						{
							index = i;
							break;
						}
					}
				}
			});
            return index;
        }

        private int GetIndexToDelete(OperatorStatusClientData operatorStatus)
        {
            bool found = false;
			for (int i = 0; i < popupMenu1.ItemLinks.Count && found == false; i++)
			{
				BarButtonItem barButtonItem = popupMenu1.ItemLinks[i].Item as BarButtonItem;
				OperatorStatusClientData operatorStatusAux = barButtonItem.Tag as OperatorStatusClientData;
				if (operatorStatusAux != null && operatorStatusAux.Code == operatorStatus.Code)
				{
					return i;
				}
			}
			return -1;
        }

        private void CreateOperatorStatusItem(OperatorStatusClientData operatorStatus)
        {
			BarButtonItem barButtonItem = new BarButtonItem(ribbonControl1.Manager,operatorStatus.FriendlyName) ;

			barButtonItem.Name = operatorStatus.Name;
			barButtonItem.Tag = operatorStatus;
			barButtonItem.Glyph = FormUtil.ImageFromArray(operatorStatus.Image);
			barButtonItem.ItemClick += new ItemClickEventHandler(barButtomItem_Click);
            //insert separator if theres no one.

			int indexToinsert = FindIndexOperatorStatusMenu(operatorStatus);
			if (operatorStatus.Name.Equals(OperatorStatusClientData.Training.Name) == false)
			{
				FormUtil.InvokeRequired(this, delegate
				{
					if (popupMenu1.ItemLinks.Count == 2)
						popupMenu1.ItemLinks.Add(barButtonItem, true);
					else
						popupMenu1.ItemLinks.Insert(indexToinsert, barButtonItem);
				});
			}
            if (operatorStatus.Name.Equals(OperatorStatusClientData.Busy.Name) || operatorStatus.Name.Equals(OperatorStatusClientData.Absent.Name))
                barButtonItem.Enabled = false;
        }

        private void UpdateOperatorStatusItem(OperatorStatusClientData operatorStatus)
        {
            OperatorStatusClientData operatorStatusAux = barButtonItemStatus.Tag as OperatorStatusClientData;
            if (operatorStatus.Name == operatorStatusAux.Name)
            {
                SetToolStripOperatorStatus(operatorStatus);
            }
            if (operatorStatus.Name == OperatorStatusClientData.Ready.Name || operatorStatus.Name == OperatorStatusClientData.Busy.Name)
            {
                int index = 0;

                if (operatorStatus.Name == OperatorStatusClientData.Busy.Name)
                {
                    defaultBusyStatus = operatorStatus;
                    index = 1;
                }
                else 
                {
                    defaultReadyStatus = operatorStatus;
                }
				BarButtonItem barButtonItem = popupMenu1.ItemLinks[index].Item as BarButtonItem;
                if (operatorStatusAux != null)
                {
                    FormUtil.InvokeRequired(this, delegate
                    {

						barButtonItem.Name = operatorStatus.Name;
						barButtonItem.Caption = operatorStatus.FriendlyName;
						barButtonItem.Tag = operatorStatus;

                    });
                }
            }
            else
            {
                int inToDel = GetIndexToDelete(operatorStatus);
                FormUtil.InvokeRequired(this, delegate
                    {
						popupMenu1.ItemLinks.RemoveAt(inToDel);
                    });
                CreateOperatorStatusItem(operatorStatus);
            }
        }

        private void DeleteOperatorStatusItem(OperatorStatusClientData operatorStatus)
        {
            bool found = false;
			for (int i = 3; i < popupMenu1.ItemLinks.Count && found == false; i++)
			{
				BarButtonItem barButtonItem = popupMenu1.ItemLinks[i].Item as BarButtonItem;
				OperatorStatusClientData operatorStatusAux = barButtonItem.Tag as OperatorStatusClientData;
				if (operatorStatusAux != null && operatorStatusAux.Code == operatorStatus.Code)
				{
					FormUtil.InvokeRequired(this, delegate
					{
						//check if is selected.
						if (barButtonItem.Name == operatorStatusAux.Name)
						{
							if (operatorStatusAux.NotReady == true)
							{
								SetToolStripOperatorStatus(defaultBusyStatus);
							}
							else
							{
								SetToolStripOperatorStatus(defaultReadyStatus);
							}
						}
						popupMenu1.ItemLinks.RemoveAt(i);
						//Delete separator
						if (popupMenu1.ItemLinks.Count <= 3)
						{
							popupMenu1.ItemLinks.RemoveAt(2);
						}
					});
					found = true;
				}
			}
        }

        private void SetToolStripOperatorStatus(OperatorStatusClientData operatorStatus)
        {
            DispatchFormDevX form = this.MdiParent as DispatchFormDevX;
            barButtonItemStatus.Caption = operatorStatus.FriendlyName;
            if (form != null) form.barButtonItemStatus.Caption = operatorStatus.FriendlyName;
            barButtonItemStatus.Tag = operatorStatus;
            barButtonItemStatus.Glyph = FormUtil.ImageFromArray(operatorStatus.Image);
            if (form != null)
            {
                form.barButtonItemStatus.Caption = operatorStatus.FriendlyName;
                form.barButtonItemStatus.Tag = operatorStatus;
                form.barButtonItemStatus.Glyph = FormUtil.ImageFromArray(operatorStatus.Image);
            }
            counterToCompleteMinute = 0;
        }

        private void CreateOperatorStatusMenu(IList operatorStatuses)
        {
            defaultReadyStatus = operatorStatuses[0] as OperatorStatusClientData;
            defaultBusyStatus = operatorStatuses[1] as OperatorStatusClientData;

            foreach (OperatorStatusClientData operatorStatus in operatorStatuses)
            {
                CreateOperatorStatusItem(operatorStatus);
            }

            SetToolStripOperatorStatus(defaultReadyStatus);
        }

        private void barButtomItem_Click(object sender, ItemClickEventArgs e)
        {
            BarItem item = e.Item;

            OperatorStatusClientData operatorStatus = item.Tag as OperatorStatusClientData;

            if (operatorStatus.NotReady.Value == true && (operatorStatus.Name.Equals(defaultBusyStatus.Name) == false))
            {
                EnableIncidentNotificationAssignment(false);
            }
            else
            {
                EnableIncidentNotificationAssignment(true);
            }

            SetToolStripOperatorStatus(operatorStatus);

            if (IsNewStatusAllow())
            {
                ServerServiceClient.GetInstance().SetOperatorStatus(operatorStatus);
            }
        }

        private void EnableIncidentNotificationAssignment(bool enable)
        {
            FormUtil.InvokeRequired(this, delegate
            {
                Enabled = enable;

                if (enable == false)
                {
                    foreach (BarItem var in ribbonControl1.Items)
                    {
                        if (var is BarButtonItem)
                        {
                            BarButtonItem item = var as BarButtonItem;
                            if (item != null && item.Name.Equals("barButtonItemStatus") == false && item.Tag == null)
                                item.Enabled = Enabled;
                        }
                    }
                }
                else
                {
                    contextMenuStripAssignedIncidentNotif_Opening(null, new CancelEventArgs());
                    contextMenuStripNewIncidentNotification_Opening(null, new CancelEventArgs());
                    contextMenuStripUnits_Opening(null, new CancelEventArgs());
                    contextMenuDispatchOrder_Opening(null, new CancelEventArgs());
                    ActivateRadioButtons();
                }
				DispatchFormDevX form = MdiParent as DispatchFormDevX;
				if(form!=null)
					form.EnableRibbonButtons(enable);
            });
        }

        #endregion

        private void gridViewUnits_SelectionChanged_Help(UnitClientData unit)
        {
            if (unit.Status.Name == UnitStatusClientData.Assigned.Name ||
                unit.Status.Name == UnitStatusClientData.Delayed.Name ||
                unit.Status.Name == UnitStatusClientData.OnScene.Name)
            {
                GridControlData gridData = null;
                foreach (GridControlData item in gridControlAssignedIncidentNotifications.Items)
                {
                    IncidentNotificationClientData notification = (IncidentNotificationClientData)item.Tag;
                    if (unit.DispatchOrder != null && 
                        unit.DispatchOrder.IncidentNotificationCode == notification.Code)
                    {
                        FormUtil.InvokeRequired(this, delegate
                        {
                           
                            lock (syncCalledFromDataGridUnits)
                            {
                                calledFromDataGridUnits = true;
                                gridData = item;
                            }                            
                        });
                        break;
                    }
                }
                if (gridData != null)
                {
                    FormUtil.InvokeRequired(this, delegate
                        {
                            IncidentNotificationClientData selectedNotification = null;
                            if (gridControlAssignedIncidentNotifications.SelectedItems.Count > 0)
                            {
                                selectedNotification = ((GridControlDataAssignedIncidentNotifications)gridControlAssignedIncidentNotifications.SelectedItems[0]).Tag as IncidentNotificationClientData;
                            }
                            if (selectedNotification == null ||
                                (selectedNotification.Code != (gridData.Tag as IncidentNotificationClientData).Code))
                            {
                                gridControlAssignedIncidentNotifications.SelectData(gridData);
                            }
                        });
                }
            }
        }

       

        private void EnableAssignButton()
        {
            FormUtil.InvokeRequired(this.buttonExAssign,
                delegate
                {
                    this.buttonExAssign.Enabled = false;
                    IncidentNotificationClientData notification = null;
                    if (this.gridControlAssignedIncidentNotifications.SelectedItems.Count == SINGLE_SELECTION)
                    {
                        notification =
                            (IncidentNotificationClientData)((GridControlDataAssignedIncidentNotifications)this.gridControlAssignedIncidentNotifications.SelectedItems[0]).Tag;

                        if (notification.Status.Active == true &&
                            gridControlUnits.SelectedItems.Count > 0 &&
                            CheckOneUnitStatusSelected(gridControlUnits.SelectedItems) == true &&
                            CheckUnitsSameDepartment(gridControlUnits.SelectedItems, notification.DepartmentType.Name) == true &&
                            CheckUserCanChangeStatus(gridControlUnits.SelectedItems) == true)
                        {
                            GridControlDataUnit data = (GridControlDataUnit)this.gridControlUnits.SelectedItems[0];
                            UnitClientData unit = data.Tag as UnitClientData;
                            if (unit != null && unit.Status.Name == UnitStatusClientData.Available.Name &&
                                unit.DepartmentStation.DepartmentZone.DepartmentType.Name == notification.DepartmentType.Name)
                            {
                                this.buttonExAssign.Enabled = true;
                            }
                        }
                    }
                });
        }

        private void buttonAddNotes_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridControlAssignedIncidentNotifications.SelectedItems.Count == SINGLE_SELECTION &&
                    this.textBoxIncidentNotes.Text.Trim().Length > 0)
                {
                    int incidentCode = (((GridControlDataAssignedIncidentNotifications)gridControlAssignedIncidentNotifications.SelectedItems[0]).Tag
                        as IncidentNotificationClientData).IncidentCode;
                    IncidentNoteClientData note = new IncidentNoteClientData();
                    note.CompleteUserName = ServerServiceClient.GetInstance().OperatorClient.FirstName + " " +
                        ServerServiceClient.GetInstance().OperatorClient.LastName;
                    note.CreationDate = ServerServiceClient.GetInstance().GetTime();
                    note.UserLogin = ServerServiceClient.GetInstance().OperatorClient.Login;
                    note.Detail = ApplicationUtil.ReplaceBlanks(this.textBoxIncidentNotes.Text);
                    note.IncidentCode = incidentCode;
                    ServerServiceClient.GetInstance().SaveOrUpdateClientData(note);
                    this.textBoxIncidentNotes.Clear();
                }
            }
            catch (Exception ex)
            {
                MessageForm.Show(ex.Message, ex);
            }
        }

        private void textBoxIncidentNotes_TextChanged(object sender, EventArgs e)
        {
            if (this.textBoxIncidentNotes.Text.Trim().Length > 0 &&
                this.gridControlAssignedIncidentNotifications.SelectedItems.Count == SINGLE_SELECTION)
            {
                this.buttonAddNotes.Enabled = true;
            }
            else
            {
                this.buttonAddNotes.Enabled = false;
            }
        }

        private void toolStripMenuItemCloseIncNotif_Click(object sender, EventArgs e)
        {
            if (gridControlAssignedIncidentNotifications.SelectedItems.Count == SINGLE_SELECTION)
            {
                IncidentNotificationClientData selectedIncidentNotification =
                    ((GridControlDataAssignedIncidentNotifications)gridControlAssignedIncidentNotifications.SelectedItems[0]).Tag as IncidentNotificationClientData;
                if (selectedIncidentNotification.Status.Active == true)
                {
                    closingNotificationCustomCode = selectedIncidentNotification.CustomCode;
                    AskForEndIncidentNotification(selectedIncidentNotification, true);
                }
            }
        }

        private void contextMenuStripAssignedIncidentNotif_Opening(object sender, CancelEventArgs e)
        {
            if (sender != null)
            {
                FormUtil.InvokeRequired(gridControlAssignedIncidentNotifications,
                    delegate
                    {
                        gridControlAssignedIncidentNotifications.Focus();
                    });
            }
            IList list = gridControlAssignedIncidentNotifications.SelectedItems;
            if (this.gridControlAssignedIncidentNotifications.SelectedItems.Count == SINGLE_SELECTION ||
                    this.gridControlNewIncidentNotifications.SelectedItems.Count == SINGLE_SELECTION)
            {
                barButtonItemSave.Enabled = true;
                barButtonItemPrint.Enabled = true;
                barButtonItemRefreshOrder.Enabled = true;
            }
            else
            {
                barButtonItemSave.Enabled = false;
                barButtonItemPrint.Enabled = false;
                barButtonItemRefreshOrder.Enabled = false;
            }
            if (list.Count > 0)
            {
                EnableAssignedIncNotifMenu(true, (IncidentNotificationClientData)((GridControlDataAssignedIncidentNotifications)list[0]).Tag);                
            }
            else
            {
                e.Cancel = true;
                EnableAssignedIncNotifMenu(false, null);                
            }
            
            EnableLocationButton(null, EventArgs.Empty);
        }


        private void gridViewNewIncidentNotifications_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                assignSelectedIncidentNotification();
            } 
        }

        private void assignSelectedIncidentNotification()
        {
            if (this.gridControlNewIncidentNotifications.SelectedItems.Count == SINGLE_SELECTION)
            {
                assignIncidentNotification(((GridControlDataNewIncidentNotifications)gridControlNewIncidentNotifications.SelectedItems[0]).Tag as IncidentNotificationClientData);
            }
            radioButtonDepartment.Checked = true;
            gridViewAssignedIncidentNotifications.MoveLastVisible();
        }

        private void blinkingButtonExPendingDispatch_Click(object sender, EventArgs e)
        {
            try
            {
                if (incidentNotificationButtonFilter != IncidentNotificationButtonFilter.PENDING || refreshIncidentFilter == true)
                {
                    (sender as BlinkingSimpleButtonEx).LookAndFeel.UseDefaultLookAndFeel = false;
                    (sender as BlinkingSimpleButtonEx).LookAndFeel.SkinName = "Black";
                    UnHighlightDispatchOrdersButtons(sender as BlinkingSimpleButtonEx);
                    incidentNotificationButtonFilter = IncidentNotificationButtonFilter.PENDING;
                    this.layoutControlGroupAssignedIncidentNotifications.Text = (sender as BlinkingSimpleButtonEx).Text.Trim() + " " + ResourceLoader.GetString2("DispatchOrders");

                    gridViewAssignedIncidentNotifications.ClearColumnsFilter();
                    CriteriaOperator expr = new BinaryOperator("Status", IncidentNotificationStatusClientData.Pending.FriendlyName, BinaryOperatorType.Equal);

                    gridViewAssignedIncidentNotifications.ActiveFilterCriteria = expr;

                    if (this.gridControlAssignedIncidentNotifications.SelectedItems.Count == 0)
                    {
                        gridViewAssignedIncidentNotifications_SingleSelectionChanged(this.gridViewAssignedIncidentNotifications, null);
                    }

                }
                this.gridViewAssignedIncidentNotifications.Focus();

                //if (incidentNotificationButtonFilter != IncidentNotificationButtonFilter.PENDING)
                //{
                //    (sender as BlinkingSimpleButtonEx).LookAndFeel.SkinName = "Black";
                //    UnHighlightDispatchOrdersButtons(sender as BlinkingSimpleButtonEx);
                //    ArrayList parameters = new ArrayList();
                //    incidentNotificationButtonFilter = IncidentNotificationButtonFilter.PENDING;
                //    groupControlIncidentNotifications.Text = ResourceLoader.GetString2("DispatchOrders") + " - " + (sender as BlinkingSimpleButtonEx).Text.Trim();
                //    parameters.Add(incidentNotificationButtonFilter);
                //    parameters.Add(this.ServerServiceClient.GetInstance().OperatorClient);
                //    parameters.Add(gridViewUnits.GetVisibleRows("[Status] = '" + UnitStatusClientData.Available.FriendlyName + "'"));

                //    selectedAssignedIncidentNotificationFilter.Parameters = parameters;

                //    VerifySelectedNotification(selectedAssignedIncidentNotificationFilter);                    
                //    selectedAssignedIncidentNotificationFilter.Filter(false);
                //    if (this.gridControlAssignedIncidentNotifications.SelectedItems.Count == 0)
                //    {
                //        gridViewAssignedIncidentNotifications_SelectionChanged(gridViewAssignedIncidentNotifications, null);
                //    }
                 
                //}
               
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

            

        private void contextMenuStripUnits_Opening(object sender, CancelEventArgs e)
        {
            FormUtil.InvokeRequired(this,
            delegate
            {
                gridControlUnits.Focus();
                IncidentNotificationClientData notification = null;
                if (this.gridControlAssignedIncidentNotifications.SelectedItems.Count == SINGLE_SELECTION)
                {
                    notification =
                        (IncidentNotificationClientData)((GridControlDataAssignedIncidentNotifications)this.gridControlAssignedIncidentNotifications.SelectedItems[0]).Tag;

                    if (gridControlUnits.SelectedItems.Count > 0 &&
                        CheckOneUnitStatusSelected(gridControlUnits.SelectedItems) == true &&
                        CheckUnitsSameDepartment(gridControlUnits.SelectedItems, notification.DepartmentType.Name) == true &&
                        CheckUserCanChangeStatus(gridControlUnits.SelectedItems) == true)
                    {
                        GridControlDataUnit data = (GridControlDataUnit)gridControlUnits.SelectedItems[0];
                        UnitClientData unit = data.Tag as UnitClientData;
                        if (unit != null)
                        {
                            EnableUnitMenuItems(unit, notification);
                        }
                    }
                    else
                    {
                        toolStripMenuItemAssignUnit.Enabled = false;
                        toolStripMenuItemCallUnit.Enabled = false;
                        toolStripMenuItemDelayUnit.Enabled = false;
                        toolStripMenuItemOnSceneUnit.Enabled = false;
                        barButtonItemOnScene.Enabled = false;
                        toolStripMenuItemCloseUnit.Enabled = false;
                        toolStripMenuItemAddNote.Enabled = false;
                        toolStripMenuItemOutOfOrderUnit.Enabled = false;
                        barButtonItemOutOfOrder.Enabled = false;
                        toolStripMenuItemAvailableUnit.Enabled = false;
                        barButtonItemAvailable.Enabled = false;
                        toolStripMenuItemNotAvailableUnit.Enabled = false;
                        barButtonItemUnavailable.Enabled = false;
                        if (gridControlUnits.SelectedItems.Count > 0 &&
                            CheckOneUnitStatusSelected(gridControlUnits.SelectedItems) == true &&
                            CheckUserCanChangeStatus(gridControlUnits.SelectedItems) == true)
                        {
                            GridControlDataUnit data = (GridControlDataUnit)gridControlUnits.SelectedItems[0];
                            UnitClientData unit = data.Tag as UnitClientData;
                            if (unit != null)
                            {
                                EnableUnitMenuItems(unit, null);
                            }
                        }
                    }
                    if (gridControlUnits.SelectedItems.Count == 0)
                    {
                        e.Cancel = true;
                    }
                }
                else
                {
                    if (gridControlUnits.SelectedItems.Count > 0)
                    {
                        if (CheckOneUnitStatusSelected(gridControlUnits.SelectedItems) == true &&
                            CheckUserCanChangeStatus(gridControlUnits.SelectedItems) == true)
                        {
                            GridControlDataUnit data = (GridControlDataUnit)gridControlUnits.SelectedItems[0];
                            UnitClientData unit = data.Tag as UnitClientData;
                            if (unit != null)
                            {
                                EnableUnitMenuItems(unit, null);
                            }
                        }
                        else
                        {
                            toolStripMenuItemAssignUnit.Enabled = false;
                            toolStripMenuItemCallUnit.Enabled = false;
                            toolStripMenuItemDelayUnit.Enabled = false;
                            toolStripMenuItemOutOfOrderUnit.Enabled = false;
                            barButtonItemOutOfOrder.Enabled = false;
                            toolStripMenuItemAvailableUnit.Enabled = false;
                            barButtonItemAvailable.Enabled = false;
                            toolStripMenuItemNotAvailableUnit.Enabled = false;
                            barButtonItemUnavailable.Enabled = false;
                            toolStripMenuItemOnSceneUnit.Enabled = false;
                            barButtonItemOnScene.Enabled = false;
                            toolStripMenuItemCloseUnit.Enabled = false;
                            toolStripMenuItemAddNote.Enabled = false;
                        }
                    }
                    else
                    {
                        e.Cancel = true;
                        toolStripMenuItemAssignUnit.Enabled = false;
                        toolStripMenuItemCallUnit.Enabled = false;
                        toolStripMenuItemDelayUnit.Enabled = false;
                        toolStripMenuItemOutOfOrderUnit.Enabled = false;
                        barButtonItemOutOfOrder.Enabled = false;
                        toolStripMenuItemAvailableUnit.Enabled = false;
                        barButtonItemAvailable.Enabled = false;
                        toolStripMenuItemNotAvailableUnit.Enabled = false;
                        barButtonItemUnavailable.Enabled = false;
                        toolStripMenuItemOnSceneUnit.Enabled = false;
                        barButtonItemOnScene.Enabled = false;
                        toolStripMenuItemCloseUnit.Enabled = false;
                        toolStripMenuItemAddNote.Enabled = false;
                    }
                }
                EnableLocationButton(null, EventArgs.Empty);
            });
        }

        private void EnableUnitMenuItems(UnitClientData unit,
            IncidentNotificationClientData incidentNotification)
        {
            this.contextMenuStripUnits.Enabled = true;
            switch (unit.Status.CustomCode)
            {
                case "AVAILABLE":
                    if (incidentNotification != null &&
                        incidentNotification.Status.Active == true &&
                        incidentNotification.DepartmentType.Name == unit.DepartmentStation.DepartmentZone.DepartmentType.Name)
                    {
                        toolStripMenuItemAssignUnit.Enabled = true;
                        toolStripMenuItemCallUnit.Enabled = true;
						toolStripMenuItemOnSceneUnit.Enabled = true;
						barButtonItemOnScene.Enabled = true;
                    }
                    else
                    {
                        toolStripMenuItemAssignUnit.Enabled = false;
                        toolStripMenuItemCallUnit.Enabled = false;
						toolStripMenuItemOnSceneUnit.Enabled = false; 
						barButtonItemOnScene.Enabled = false;
                    }
                    toolStripMenuItemDelayUnit.Enabled = false;
                    toolStripMenuItemOutOfOrderUnit.Enabled = true;
					barButtonItemOutOfOrder.Enabled = true;
                    toolStripMenuItemAvailableUnit.Enabled = false;
					barButtonItemAvailable.Enabled = false;
                    toolStripMenuItemNotAvailableUnit.Enabled = true;
					barButtonItemUnavailable.Enabled = true;
                    toolStripMenuItemCloseUnit.Enabled = false;
                    toolStripMenuItemAddNote.Enabled = false;
                    break;
                case "OUTOFORDER":
                    toolStripMenuItemAssignUnit.Enabled = false;
                    toolStripMenuItemCallUnit.Enabled = false;
					toolStripMenuItemDelayUnit.Enabled = false;
                    toolStripMenuItemOutOfOrderUnit.Enabled = false;
					barButtonItemOutOfOrder.Enabled = false;
                    toolStripMenuItemAvailableUnit.Enabled = true;
					barButtonItemAvailable.Enabled = true;
					toolStripMenuItemNotAvailableUnit.Enabled = true;
					barButtonItemUnavailable.Enabled = true;
                    toolStripMenuItemOnSceneUnit.Enabled = false;
					barButtonItemOnScene.Enabled = false;
                    toolStripMenuItemCloseUnit.Enabled = false;
                    toolStripMenuItemAddNote.Enabled = false;
                    break;
                case "ASSIGNED":
                case "ONSCENE":
                case "DELAYED":
					toolStripMenuItemAssignUnit.Enabled = false;
                    toolStripMenuItemCallUnit.Enabled = gridControlAssignedIncidentNotifications.SelectedItems.Count > 0; 
					toolStripMenuItemDelayUnit.Enabled = false;
                    toolStripMenuItemOutOfOrderUnit.Enabled = false;
					barButtonItemOutOfOrder.Enabled = false;
                    toolStripMenuItemAvailableUnit.Enabled = false;
					barButtonItemAvailable.Enabled = false;
					toolStripMenuItemNotAvailableUnit.Enabled = false;
					barButtonItemUnavailable.Enabled = false;
                    toolStripMenuItemOnSceneUnit.Enabled = false;
					barButtonItemOnScene.Enabled = false;
                    toolStripMenuItemCloseUnit.Enabled = false;
                    toolStripMenuItemAddNote.Enabled = false;
                    break;
                case "NOTAVAILABLE":

                    toolStripMenuItemAssignUnit.Enabled = false;
                    toolStripMenuItemCallUnit.Enabled = false;
					toolStripMenuItemDelayUnit.Enabled = false;
                    toolStripMenuItemOutOfOrderUnit.Enabled = true;
					barButtonItemOutOfOrder.Enabled = true;
                    toolStripMenuItemAvailableUnit.Enabled = true;
					barButtonItemAvailable.Enabled = true;
					toolStripMenuItemNotAvailableUnit.Enabled = false;
					barButtonItemUnavailable.Enabled = false;
                    toolStripMenuItemOnSceneUnit.Enabled = false;
					barButtonItemOnScene.Enabled = false;
                    toolStripMenuItemCloseUnit.Enabled = false;
                    toolStripMenuItemAddNote.Enabled = false;
                    break;
                default:
                    break;
            }
        }

        private bool CheckUserCanChangeStatus(IList selectedItemCollection)
        {
            bool result = true;
            
            for (int i = 0; i < selectedItemCollection.Count && result == true; i++)
			{
                GridControlDataUnit data = selectedItemCollection[i] as GridControlDataUnit;
                DispatchOrderClientData dispatchOrder = (data.Tag as UnitClientData).DispatchOrder;
                if (dispatchOrder != null)
                {
                    if (ServerServiceClient.GetInstance().OperatorClient.Code.Equals(dispatchOrder.DispatchOperatorCode) == false)
                    {
                        result = false;
                    }
                }
			}
            return result;
        }

        private bool CheckOneUnitStatusSelected(IList items)
        {
            bool result = true;
            UnitClientData original = null;
            if (items.Count > 0)
                original = (UnitClientData) ((GridControlDataUnit)items[0]).Tag;
            for (int i = 1; i < items.Count && result == true; i++)
            {
                UnitClientData unit = (UnitClientData)((GridControlDataUnit)items[i]).Tag;
                if (unit.Status.Name != original.Status.Name)
                    result = false;
            }
            return result;
        }

        /// <summary>
        /// items must be a non empty collection
        /// </summary>
        /// <param name="items"></param>
        /// <param name="departmentType"></param>
        /// <returns></returns>
        private bool CheckUnitsSameDepartment(IList items, string departmentType)
        {
            bool result = true;
            for (int i = 0; i < items.Count && result == true; i++)
            {
                UnitClientData unit = (UnitClientData)((GridControlData)items[i]).Tag;
                if (unit.DepartmentStation.DepartmentZone.DepartmentType.Name != departmentType)
                    result = false;
            }            
            return result;
        }

        private bool CheckSameIncidentNotification(IList items)
        {
            bool result = true;
            UnitClientData original = null;
            if (items.Count > 0)
                original = (UnitClientData) ((GridControlDataUnit)items[0]).Tag;
            for (int i = 1; i < items.Count && result == true; i++)
            {
                UnitClientData unit = (UnitClientData)((GridControlDataUnit)items[i]).Tag;
                if (unit.DispatchOrder.IncidentNotificationCode != original.DispatchOrder.IncidentNotificationCode)
                    result = false;
            }
            return result;
        }

        private void gridViewAssignedIncidentNotifications_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control == true && e.KeyCode == Keys.Tab)
            {
                if (incidentNotificationButtonFilter == IncidentNotificationButtonFilter.ACTIVE || buttonActiveDispatch.BackColor == SystemColors.Highlight)
                    buttonClosedDispatch_Click(buttonClosedDispatch, null);
                else if (incidentNotificationButtonFilter == IncidentNotificationButtonFilter.INACTIVE || buttonClosedDispatch.BackColor == SystemColors.Highlight)
                    buttonDispReqAttention_Click(blinkingButtonExDispReqAttention, null);
                else if (incidentNotificationButtonFilter == IncidentNotificationButtonFilter.ATTENTION || blinkingButtonExDispReqAttention.BackColor == SystemColors.Highlight)
                    blinkingButtonExPendingDispatch_Click(blinkingButtonExPendingDispatch, null);
                else if (incidentNotificationButtonFilter == IncidentNotificationButtonFilter.PENDING || blinkingButtonExPendingDispatch.BackColor == SystemColors.Highlight)
                    buttonActiveDispatch_Click(buttonActiveDispatch, null);
                else
                    buttonActiveDispatch_Click(buttonActiveDispatch, null);
                this.gridViewAssignedIncidentNotifications.Focus();
            }
            else if (e.KeyCode == Keys.F5)
            {
                toolStripMenuItemRefreshIncidentNotitficationDetails_Click(null, null);
            }
            else if (e.KeyCode == Keys.F6)
            {
                toolStripMenuItemModifyZone_Click(null, null);
            }
            else if (e.KeyCode == Keys.F8)
            {
                toolStripMenuItemSupportRequest_Click(null, null);
            }
            else if (e.KeyCode == Keys.F9)
            {
                toolStripMenuItemPending_Click(null, null);
            }
            else if (e.KeyCode == Keys.F12)
            {
                if (this.gridControlAssignedIncidentNotifications.SelectedItems.Count > 0)
                {
                    IncidentNotificationClientData selectedIncidentNotification = ((IncidentNotificationClientData)((GridControlDataAssignedIncidentNotifications)gridControlAssignedIncidentNotifications.SelectedItems[0]).Tag);
                    if (selectedIncidentNotification.DispatchOrders != null && selectedIncidentNotification.DispatchOrders.Count == 0)
                    {
                        toolStripMenuItemCloseIncNotif_Click(null, null);
                    }
                }
            }
            DispatchFormDevX form = (this.MdiParent as DispatchFormDevX);
            PreviewKeyDownEventArgs e1 = new PreviewKeyDownEventArgs(e.KeyData);
            if (form != null)
                form.DispatchFormDevX_PreviewKeyDown(null, e1);
        }



        private bool HasAssignedUnit(IncidentNotificationClientData incidentNotification)
        {
            bool result = false;
            for (int i = 0; i < incidentNotification.DispatchOrders.Count && result == false; i++)
            {
                DispatchOrderClientData dispatchOrder = incidentNotification.DispatchOrders[i] as DispatchOrderClientData;
                if (dispatchOrder.Status.Name != DispatchOrderStatusClientData.Cancelled.Name)
                {
                    result = true;
                }
            }
            return result;
        }

        void toolStripMenuItemCallUnit_Click(object sender, System.EventArgs e)
        {
            if (this.gridControlUnits.SelectedItems.Count > 0)
            {
                string phonesToCall = "";
                UnitClientData ucd = ((GridControlDataUnit)gridControlUnits.SelectedItems[0]).Tag as UnitClientData;
                for (int i = 0; i < ucd.OfficerAssigns.Count; i++)
                {
                    OfficerClientData ocd = ucd.OfficerAssigns[i] as OfficerClientData;
                    phonesToCall += ocd.Telephone + "/";
                }

                string customCode = ((GridControlDataAssignedIncidentNotifications)gridControlAssignedIncidentNotifications.SelectedItems[0]).IncidentNotification.CustomCode;

                if (popUpDialer == null)
                {
                    popUpDialer = new SoftPhoneForm();
                }
                popUpDialer.Initialize("Call unit " + ((GridControlDataUnit)gridControlUnits.SelectedItems[0]).UnitCustomCode, customCode, phonesToCall);
                popUpDialer.Show();
            }
        }

        void toolStripMenuItemCallStation_Click(object sender, System.EventArgs e)
        {
            if (this.gridControlAssignedIncidentNotifications.SelectedItems.Count > 0)
            {
                IncidentNotificationClientData notif = ((GridControlDataAssignedIncidentNotifications)gridControlAssignedIncidentNotifications.SelectedItems[0]).IncidentNotification;
                if (notif != null)
                {
                    string customCode = notif.CustomCode;
                    string phoneNumber = notif.DepartmentStation.Telephone;

                    if (popUpDialer == null)
                    {
                        popUpDialer = new SoftPhoneForm();
                    }
                    popUpDialer.Initialize("Call " + notif.DepartmentStation.Name, customCode, phoneNumber);
                    popUpDialer.Show();
                }
            }
        }
                
        private void UnitsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ArrayList selectedItems = new ArrayList(this.gridControlUnits.SelectedItems);
            if (selectedItems.Count > 0)
            {
				if (globalApplicationPreferences["CanChangeUnitStatus"].Value.Equals(true.ToString()))
				{
					UnitStatusClientData status = null;
					if (sender.Equals(toolStripMenuItemNotAvailableUnit) == true)
					{
						status = UnitStatusClientData.NotAvailable;
					}
					else if (sender.Equals(toolStripMenuItemAvailableUnit) == true)
					{
						status = UnitStatusClientData.Available;
					}
					else if (sender.Equals(toolStripMenuItemOutOfOrderUnit) == true)
					{
						status = UnitStatusClientData.OutOfOrder;
					}

					IList<UnitClientData> selectedUnits = new List<UnitClientData>();

					foreach (GridControlDataUnit data in selectedItems)
					{
						UnitClientData unit = (UnitClientData)data.Tag;
						unit.Status = status;
						selectedUnits.Add(unit);
					}
					ProcessUnitStatusChangesList(selectedUnits);
				}
				else
					MessageForm.Show("UserCantChangeUnitStatus", MessageFormType.Information);
            }
            else
            {
                SmartLogger.Print(ResourceLoader.GetString2("NullReference"));
                MessageForm.Show(ResourceLoader.GetString2("OperationNotDone"), MessageFormType.Information);
            }
        }
        
        private void timer_Tick(object sender, EventArgs e)
        {
            GridControlData[] gridItems = null;
            gridItems = new GridControlData[gridControlAssignedIncidentNotifications.Items.Count];
            gridControlAssignedIncidentNotifications.Items.CopyTo(gridItems, 0);
        
            bool newInAttentionNotification = false;
            bool isAbsent = true;
            bool thereIsAtLeastOneNotification = false;
            availableTimeInStatusWithPause = 0;
            for (int i = 0; i < gridItems.Length; i++)
            {
                GridControlDataAssignedIncidentNotifications gridData = gridItems[i] as GridControlDataAssignedIncidentNotifications;
                if (gridData != null && 
                    gridData.IncidentNotification.Status.Name != IncidentNotificationStatusClientData.Cancelled.Name &&
                    gridData.IncidentNotification.Status.Name != IncidentNotificationStatusClientData.Closed.Name)
                {
                    if (!thereIsAtLeastOneNotification)
                    {
                        thereIsAtLeastOneNotification = true;
                    }
                    IncidentNotificationClientData incidentNotification = gridData.Tag as IncidentNotificationClientData;
                    if (incidentNotification.Status.Name == IncidentNotificationStatusClientData.Assigned.Name ||
                        (incidentNotification.Status.Name == IncidentNotificationStatusClientData.Updated.Name &&
                         incidentNotification.LastOperationalStatus != null &&
                         incidentNotification.LastOperationalStatus.Name == IncidentNotificationStatusClientData.Assigned.Name))
                    {
                        TimeSpan diff = parentForm.syncServerDateTime - incidentNotification.StartDate;

                        if (diff.TotalSeconds >= TIME_WITHOUT_ATTENTION)
                        {                            
                            if (gridData.InAttention == false)
                            {
                                gridData.InAttention = true;

                                FormUtil.InvokeRequired(this, delegate
                                {
                                    assignedIncidentNotificationSyncBox.Sync(gridData, CommittedDataAction.Update);
                                });

                                blinkingButtonExDispReqAttention.Blinking = true;
                                newInAttentionNotification = true;
                                counterToCompleteMinute = 0;
                            }
                            else//Notification is already in attention
                            {
                                isAbsent = isAbsent && true;
                            }
                        }
                        else
                        {
                            isAbsent = isAbsent && false;
                        }
                    }
                    else if (incidentNotification.Status.Name == IncidentNotificationStatusClientData.Delayed.Name)
                    {
                        isAbsent = isAbsent && true;
                    }
                    else if (incidentNotification.Status.Name == IncidentNotificationStatusClientData.InProgress.Name ||
                        incidentNotification.Status.Name == IncidentNotificationStatusClientData.Reassigned.Name ||
                        incidentNotification.Status.Name == IncidentNotificationStatusClientData.Pending.Name ||
                        (incidentNotification.Status.Name == IncidentNotificationStatusClientData.Updated.Name &&
                         incidentNotification.LastOperationalStatus != null &&
                         incidentNotification.LastOperationalStatus.Name != IncidentNotificationStatusClientData.Assigned.Name))
                    {
                        isAbsent = isAbsent && false;
                    }
                }
            }

            counterToCompleteMinute++;
            OperatorStatusClientData selectedOperatorStatus = barButtonItemStatus.Tag as OperatorStatusClientData;
            if (selectedOperatorStatus.IsPause())
            {
                availableTimeInStatusWithPause = ServerServiceClient.GetInstance().CalculateAmountTimeAvailableByStatusWithPause(selectedOperatorStatus.CustomCode);
            }
            //Change to absent if there is not time available to be in pause(one minute later occurs the change)
            //If the operator is talking over the radio, the application should not go into absent status.
            if (TelephoneStatus != TelephoneStatusType.Active)
            {
                if (availableTimeInStatusWithPause < 0
                    && selectedOperatorStatus.IsPause() == true
                    //&& counterToCompleteMinute >= ABSENCE_CHECK_CYCLE
                    && bool.Parse(globalApplicationPreferences["CheckLogInMode"].Value) == true)
                {
                    if (counterToCompleteMinute >= ABSENCE_CHECK_CYCLE)
                        counterToCompleteMinute = 0;
                    availableTimeInStatusWithPause = 0;
                    OperatorStatusClientData operatorStatus = OperatorStatusClientData.Absent;
                    SetToolStripOperatorStatus(operatorStatus);
                    ServerServiceClient.GetInstance().SetOperatorStatus(operatorStatus);
                }
                else if (isAbsent == true &&
                    availableTimeInStatusWithPause <= 0 &&
                    counterToCompleteMinute >= ABSENCE_CHECK_CYCLE * int.Parse(globalApplicationPreferences["DispatchAutomaticAbsent"].Value) &&
                    selectedOperatorStatus.Name.Equals(OperatorStatusClientData.Absent.Name) == false)
                {
                    //If there is at least one assigned notification or there are available unassigned notifications then
                    //the operator has to be absent
                    if (thereIsAtLeastOneNotification == true || gridControlNewIncidentNotifications.Items.Count > 0)
                    {
                        counterToCompleteMinute = 0;
                        EnableIncidentNotificationAssignment(false);
                        OperatorStatusClientData operatorStatus = OperatorStatusClientData.Absent;
                        SetToolStripOperatorStatus(operatorStatus);
                        ServerServiceClient.GetInstance().SetOperatorStatus(operatorStatus);
                    }
                }
            }

            if (incidentNotificationButtonFilter.Equals(IncidentNotificationButtonFilter.ATTENTION) && newInAttentionNotification)
            {
                RefreshFilter();
            }
         
            try
            {
                gridItems = new GridControlData[gridControlUnits.Items.Count];
                gridControlUnits.Items.CopyTo(gridItems, 0);
                foreach (GridControlDataUnit gridData in gridItems)
                {
                    UnitClientData unitClient = gridData.Tag as UnitClientData;
                    DispatchOrderClientData dispatchOrderClient =
                        unitClient.DispatchOrder as DispatchOrderClientData;
                    if (dispatchOrderClient != null &&
                        dispatchOrderClient.Status.Name == DispatchOrderStatusClientData.Dispatched.Name &&
                        dispatchOrderClient.DispatchOperatorCode == ServerServiceClient.GetInstance().OperatorClient.Code)
                    {
                        TimeSpan diff = parentForm.syncServerDateTime - dispatchOrderClient.ArrivalDate;
                        if (diff.TotalHours >= 0)
                        {
                            FormUtil.InvokeRequired(this, delegate
                            {
                                  unitSyncBox.Sync(gridData, CommittedDataAction.Update);
                            });
 
                            dispatchOrderClient.Status = DispatchOrderStatusClientData.Delayed;
                            ServerServiceClient.GetInstance().SaveOrUpdateClientData(dispatchOrderClient);
                        }
                    }
                }
            }
            catch (CommunicationObjectFaultedException)
            {
                throw;
            }
            catch (FaultException ex)
            {
                if (ex.Code != null && ex.Code.Name != null
                    && ex.Code.Name.Equals(typeof(DatabaseServerDownException).Name))
                {
                    throw new CommunicationObjectFaultedException(ex.Message, ex);
                }
                else
                {
                    MessageForm.Show(ex.Message, ex);
                }
            }
            catch (Exception ex)
            {
                MessageForm.Show(ex.Message, ex);
            }
            finally
            {
                //FormUtil.InvokeRequired(this, delegate
                //{
                //    this.gridViewUnits.SelectionChanged -= new DevExpress.Data.SelectionChangedEventHandler(gridViewUnits_SelectionChanged);
                //    foreach (int index in selectedRows)
                //    {
                //        if (gridViewUnits.GetRow(index) != null)
                //            gridViewUnits.SelectRow(index);
                //    }
                //    this.gridViewUnits.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(gridViewUnits_SelectionChanged);
                 
                //});
            }		
        }

        private bool IsNewStatusAllow()
        {
            if (bool.Parse(globalApplicationPreferences["CheckLogInMode"].Value) == true)
            {
                OperatorStatusClientData selectedOperatorStatus = barButtonItemStatus.Tag as OperatorStatusClientData;
                if (selectedOperatorStatus.IsPause())
                {
                    availableTimeInStatusWithPause = ServerServiceClient.GetInstance().CalculateAmountTimeAvailableByStatusWithPause(selectedOperatorStatus.CustomCode);

                    if (availableTimeInStatusWithPause < 0)
                    {
                        OperatorStatusClientData operatorStatus = OperatorStatusClientData.Absent;
                        SetToolStripOperatorStatus(operatorStatus);
                        ServerServiceClient.GetInstance().SetOperatorStatus(operatorStatus);
                        return false;
                    }
                }
            }

            return true;
        }
        private void contextMenuDispatchOrder_Opening(object sender, CancelEventArgs e)
        {
            FormUtil.InvokeRequired(this,
                delegate
                {
                    gridControlDispatchOrder.Focus();
                    if (this.gridControlDispatchOrder.SelectedItems.Count > 0 &&
                        CheckOneDispatchOrderStatusSelected(gridControlDispatchOrder.SelectedItems) == true)
                    {
                        DispatchOrderClientData dispatchOrder =
                            ((GridControlDataDispatchOrder)gridControlDispatchOrder.SelectedItems[0]).Tag as DispatchOrderClientData;
                        if (dispatchOrder.Unit.Status.Name.Equals(UnitStatusClientData.Assigned.Name))
                        {
                            enEscenaToolStripMenuItem.Enabled = true;
                            barButtonItemAssignedOnScene.Enabled = true;
                            modifyTimeOrderToolStripMenuItem.Enabled = true;
                            barButtonItemModifyTime.Enabled = true;
                            cerrarToolStripMenuItem.Enabled = true;
                            barButtonItemEnd.Enabled = true;
                        }
                        else if (dispatchOrder.Unit.Status.Name.Equals(UnitStatusClientData.OnScene.Name))
                        {
                            enEscenaToolStripMenuItem.Enabled = false;
                            barButtonItemAssignedOnScene.Enabled = false;
                            modifyTimeOrderToolStripMenuItem.Enabled = false;
                            barButtonItemModifyTime.Enabled = false;
                            cerrarToolStripMenuItem.Enabled = true;
                            barButtonItemEnd.Enabled = true;
                        }
                        else if (dispatchOrder.Unit.Status.Name.Equals(UnitStatusClientData.Delayed.Name))
                        {
                            enEscenaToolStripMenuItem.Enabled = true;
                            barButtonItemAssignedOnScene.Enabled = true;
                            modifyTimeOrderToolStripMenuItem.Enabled = true;
                            barButtonItemModifyTime.Enabled = true;
                            cerrarToolStripMenuItem.Enabled = true;
                            barButtonItemEnd.Enabled = true;
                        }
                        //It is active if this.dataGridAssignedUnits.SelectedItems.Count == SINGLE_SELECTION
                        if (this.gridControlDispatchOrder.SelectedItems.Count == SINGLE_SELECTION)
                        {
                            barButtonItemAddNote.Enabled = true;
                            toolStripMenuItemAddNoteToUnit.Enabled = true;
                        }
                        else
                        {
                            barButtonItemAddNote.Enabled = false;
                            toolStripMenuItemAddNoteToUnit.Enabled = false;
                        }
                    }
                    else
                    {
                        if (this.gridControlDispatchOrder.SelectedItems.Count == 0)
                        {
                            e.Cancel = true;
                        }
                        enEscenaToolStripMenuItem.Enabled = false;
                        barButtonItemAssignedOnScene.Enabled = false;
                        modifyTimeOrderToolStripMenuItem.Enabled = false;
                        barButtonItemModifyTime.Enabled = false;
                        cerrarToolStripMenuItem.Enabled = false;
                        barButtonItemEnd.Enabled = false;
                        toolStripMenuItemAddNoteToUnit.Enabled = false;
                        barButtonItemAddNote.Enabled = false;
                    }
                    EnableLocationButton(null, EventArgs.Empty);
                });
        }

        private bool CheckOneDispatchOrderStatusSelected(IList items)
        {
            bool result = true;
            DispatchOrderClientData original = null;
            if (items.Count > 0)
               original = (DispatchOrderClientData)((GridControlDataDispatchOrder)items[0]).Tag;
            for (int i = 1; i < items.Count && result == true; i++)
            {
                DispatchOrderClientData dispatchOrder = (DispatchOrderClientData)((GridControlDataDispatchOrder)items[i]).Tag;
                if (original.Status.Name != dispatchOrder.Status.Name)
                    result = false;
            }
            return result;
        }

        private void SelectedIncidentHelp(SelectedIncidentTask selectedIncidentTask,
            IncidentNotificationClientData selectedIncidentNotification, bool calledFromUnit)
        {
            if (selectedIncidentTask != null)
            {
                ArrayList tasks = new ArrayList();
                tasks.Add(selectedIncidentTask);
                bool sameIncident = false;
                if (selectedIncidentCode == (int)selectedIncidentTask.Parameters[0])
                    sameIncident = true;
                selectedIncidentCode = (int)selectedIncidentTask.Parameters[0];
                try
                {                    
                    IList result = ServerServiceClient.GetInstance().RunTasks(tasks);
                    if (result != null && result.Count > 0 && (result[0] as IList) != null)
                    {
                        IList tempList = (IList)result[0];
                        if (tempList != null && tempList.Count > 0)
                        {
                            SelectedIncidentTaskResult selectedIncidentLightData = tempList[0] as SelectedIncidentTaskResult;
                            if (selectedIncidentLightData != null)
                            {
                                FillIncidentDetails(selectedIncidentLightData, sameIncident);
                                FillCallsAndDispatchs(selectedIncidentLightData, false);
                            }
                        }
                    }
                }
                catch (CommunicationObjectFaultedException)
                {
                    throw;
                }
                catch (FaultException ex)
                {
                    if (ex.Code != null && ex.Code.Name != null
                        && ex.Code.Name.Equals(typeof(DatabaseServerDownException).Name))
                    {
                        throw new CommunicationObjectFaultedException(ex.Message, ex);
                    }
                    else
                    {
                        SmartLogger.Print(ex);
                    }
                }
                catch (Exception ex)
                {
                    SmartLogger.Print(ex);
                }
            }
            PerformFillRecommendedUnits(selectedIncidentNotification, calledFromUnit);
            if (selectedIncidentNotification != null &&
                (selectedIncidentNotification.Status.Name == IncidentNotificationStatusClientData.Reassigned.Name ||
                selectedIncidentNotification.Status.Name == IncidentNotificationStatusClientData.Updated.Name))
            {
                selectedIncidentNotification.RecalculateStatus = true;
                ServerServiceClient.GetInstance().SaveOrUpdateClientData(selectedIncidentNotification);
            }
        }

        private void PerformFillRecommendedUnits(IncidentNotificationClientData selectedIncidentNotification, bool calledFromUnit)
        {
            FormUtil.InvokeRequired(this, delegate
            {
                this.gridViewUnits.SelectionChanged -= new DevExpress.Data.SelectionChangedEventHandler(gridViewUnits_SelectionChanged);
       
            });
            try
            {
                /* Only recommended units is calculated if the notification was not selected from 
                 * dataGridExNewIncidentNotifications
                 * */
                if (selectedIncidentNotification != null && calledFromUnit == false)
                {
                    FillRecommendedUnits(selectedIncidentNotification);
                }
                else
                {
                    IList unitValues = ((IList)gridViewUnits.DataSource);

                    if (unitValues != null)
                        for (int i = 0; i < unitValues.Count; i++)
                        {
                            GridControlDataUnit gridData = unitValues[i] as GridControlDataUnit;
                            UnitClientData unit = gridData.Tag as UnitClientData;
                            unit.Distance = 0;
                            gridData.BackColor = unit.Status.Color;
                            gridData.Recommended = string.Empty;
                            gridData.Unit = unit;
                            //gridData.PreviewFieldValue = "Capacity: " + unit.Capacity + "\tDescription: " + unit.Description;
                            gridData.PreviewFieldValue = "Capacity: " + unit.Capacity + "\tPortable ID: " + unit.IdRadio + "\tDescription: " + unit.Description;
                            unitSyncBox.Sync(gridData, CommittedDataAction.Update);
                        }
                }
            }
            catch (CommunicationObjectFaultedException)
            {
                throw;
            }
            catch (FaultException ex)
            {
                if (ex.Code != null && ex.Code.Name != null
                    && ex.Code.Name.Equals(typeof(DatabaseServerDownException).Name))
                {
                    throw new CommunicationObjectFaultedException(ex.Message, ex);
                }
                else
                {
                    MessageForm.Show(ex.Message, ex);
                }
            }
            catch (Exception) { }
            finally
            {
                FormUtil.InvokeRequired(this, delegate
                {
                  this.gridViewUnits.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(gridViewUnits_SelectionChanged);

              });
            }
        }

        private void OnRefreshIncidentReport()
        {
            refreshIncidentReport.Change(1000, Timeout.Infinite);
        }

        private void OnRefreshIncidentReportTimer(object state)
        {
            refreshIncidentReport.Change(Timeout.Infinite, Timeout.Infinite);

            ThreadPool.QueueUserWorkItem(delegate
            {
                try
                {
                    RefreshIncidentReport();
                }
                catch (Exception ex)
                {
                    SmartLogger.Print(ex);
                }
            });
        }

        private void RefreshIncidentReport()
        {
            if (selectedIncidentCode != -1)
            {
                ArrayList parameters = new ArrayList();
                parameters.Add(selectedIncidentCode);
                SelectedIncidentTask selectedIncidentTask = new SelectedIncidentTask(parameters);
                ArrayList tasks = new ArrayList();
                tasks.Add(selectedIncidentTask);
                try
                {
                    IList result = ServerServiceClient.GetInstance().RunTasks(tasks);
                    if (result != null && result.Count > 0 && (result[0] as IList) != null)
                    {
                        IList tempList = (IList)result[0];
                        if (tempList != null && tempList.Count > 0)
                        {
                            SelectedIncidentTaskResult selectedIncidentLightData = tempList[0] as SelectedIncidentTaskResult;
                            if (selectedIncidentLightData != null)
                            {
                                FillIncidentDetails(selectedIncidentLightData, true);
                                FillCallsAndDispatchs(selectedIncidentLightData, true);
								FormUtil.InvokeRequired(this,
                                    delegate
                                    {
                                        this.layoutControlGroupIncidentDetails.Text = ResourceLoader.GetString2("IncidentDetails");
                                        this.layoutControlGroupIncidentDetails.Text += ResourceLoader.GetString2("UpdatedAt");
                                        this.layoutControlGroupIncidentDetails.Text += ServerServiceClient.GetInstance().GetTime().ToString("MM/dd/yyyy HH:mm", ApplicationUtil.GetCurrentCulture());
                                    });
                            }
                        }
                    }
                }
                catch (CommunicationObjectFaultedException)
                {
                    throw;
                }
                catch (FaultException ex)
                {
                    if (ex.Code != null && ex.Code.Name != null
                        && ex.Code.Name.Equals(typeof(DatabaseServerDownException).Name))
                    {
                        throw new CommunicationObjectFaultedException(ex.Message, ex);
                    }
                    else
                    {
                        SmartLogger.Print(ex);
                    }
                }
                catch (Exception ex)
                {
                    SmartLogger.Print(ex);
                }
            }
        }


        private void gridViewNewIncidentNotifications_SelectionChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            this.selectedIncidentCode = -1;
            GridViewEx gridViewNewIncidentNotifications = sender as GridViewEx;

            if (e.FocusedRowHandle > -1 && gridViewNewIncidentNotifications != null && gridViewNewIncidentNotifications.SelectedRowsCount > 0)
            {
                FormUtil.InvokeRequired(this, delegate
                {
                    this.buttonExAddNotification.Enabled = true;
                });
                
                IncidentNotificationClientData selectedIncidentNotification = ((GridControlDataNewIncidentNotifications)gridViewNewIncidentNotifications.GetRow(e.FocusedRowHandle)).Tag as IncidentNotificationClientData;
                ArrayList parameters = new ArrayList();
                parameters.Add(selectedIncidentNotification.IncidentCode);

                SelectedIncidentTask selectedIncidentTask = new SelectedIncidentTask(parameters);

                ThreadPool.QueueUserWorkItem(delegate(object state)
                {
                    try
                    {
                        SelectedIncidentHelp(selectedIncidentTask, null, false);
                    }
                    catch (Exception ex)
                    {
                        SmartLogger.Print(ex);
                    }
                });                
            }
            else
            {
                this.textBoxIncidentNotes.Enabled = false;
                this.textBoxIncidentNotes.BackColor = Color.White;
                this.buttonExAddNotification.Enabled = false;
            }
            FormUtil.InvokeRequired(this, delegate
            {
                this.gridViewNewIncidentNotifications.Focus();
            });
            //EnableLocationButton(null, EventArgs.Empty);
        }

        private void gridViewUnits_SelectionChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            this.selectedIncidentCode = -1;
            GridViewEx gridViewNewIncidentNotifications = sender as GridViewEx;

            if (e.FocusedRowHandle > -1 && gridViewNewIncidentNotifications != null && gridViewNewIncidentNotifications.SelectedRowsCount > 0)
            {
                this.buttonExAddNotification.Enabled = true;

                IncidentNotificationClientData selectedIncidentNotification = ((GridControlDataNewIncidentNotifications)gridViewNewIncidentNotifications.GetRow(e.FocusedRowHandle)).Tag as IncidentNotificationClientData;
                ArrayList parameters = new ArrayList();
                parameters.Add(selectedIncidentNotification.IncidentCode);

                SelectedIncidentTask selectedIncidentTask = new SelectedIncidentTask(parameters);

                ThreadPool.QueueUserWorkItem(delegate(object state)
                {
                    try
                    {
                        SelectedIncidentHelp(selectedIncidentTask, null, false);
                    }
                    catch (Exception ex)
                    {
                        SmartLogger.Print(ex);
                    }
                });
            }
            else
            {
                this.textBoxIncidentNotes.Enabled = false;
                this.textBoxIncidentNotes.BackColor = Color.White;
                this.buttonExAddNotification.Enabled = false;
            }
            FormUtil.InvokeRequired(this, delegate
            {
                this.gridViewNewIncidentNotifications.Focus();
            });            
            //EnableLocationButton(null, EventArgs.Empty);
            previewRowLines();
        }

        private void previewRowLines()
        {
            //gridViewUnits.OptionsView.ShowPreview = false;
            gridViewUnits.OptionsView.ShowPreviewLines = false;
            gridViewUnits.EnablePreviewLineForFocusedRow = false;
            if (gridViewUnits.FocusedRowHandle >= 0 && gridViewUnits.SelectedRowsCount == 1 && gridControlUnits.SelectedItems.Count == 1)
            {
                //gridViewUnits.OptionsView.ShowPreview = true;
                gridViewUnits.EnablePreviewLineForFocusedRow = true;
                gridViewUnits.OptionsView.ShowPreviewLines = true;
            }
        }


        private void gridViewUnits_FocusedRow(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            previewRowLines();      
        }

    
		private void UnableRibbonButtons(RibbonPageGroup pageGroup)
		{
			foreach (RibbonPageGroup group in ribbonPage1.Groups)
			{
				foreach (BarItemLink item in group.ItemLinks)
				{
					if (group.Name.Equals(pageGroup.Name) == false && 
						group.Name.Equals(ribbonPageGroupGeneralOptions.Name)==false &&
						group.Name.Equals(ribbonPageGroupUser.Name)==false)
						item.Item.Enabled = false;
					else
						item.Item.Enabled = true;
				}
			}
		}

    
        private void SetSkin()
        {
            try
            {
                this.textBoxIncidentNotes.Font = new Font(this.textBoxIncidentNotes.Font.FontFamily, FONT_SIZE);

                string skinFile = SmartCadConfiguration.SkinsFolder + "Clasic.xml";

                skinEngine = SkinEngine.Load(skinFile);

                ProfessionalColorTable colorTable = new CustomToolStripColors(
                    skinEngine.Colors["ToolStripGradientBegin"],
                    skinEngine.Colors["ToolStripGradientMiddle"],
                    skinEngine.Colors["ToolStripGradientEnd"],

                    skinEngine.Colors["MenuStripGradientBegin"],
                    skinEngine.Colors["MenuStripGradientEnd"],

                    skinEngine.Colors["StatusStripGradientBegin"],
                    skinEngine.Colors["StatusStripGradientEnd"]
                );

                skinEngine.AddElement("background", this);
                
                skinEngine.AddElement("GridControl", this.gridControlNewIncidentNotifications);
                skinEngine.AddElement("GridControl", this.gridControlAssignedIncidentNotifications);
                skinEngine.AddElement("GridControl", this.gridControlUnits);
                skinEngine.AddElement("GridControl", this.gridControlDispatchOrder);
                skinEngine.AddElement("buttonAssignStyle", this.buttonExAddNotification);
                skinEngine.AddElement("buttonAssignStyle", this.buttonExAssign);

                skinEngine.ApplyChanges();                
            }
            catch (Exception ex)
            {
                MessageForm.Show(ResourceLoader.GetString2("$Message.Error"), ResourceLoader.GetString2("ErrorLoadingSkin"), ex);
            }
        }

        private void toolStripMenuItemOnSceneUnit_Click(object sender, EventArgs e)
        {
            if (gridControlUnits.SelectedItems.Count > 0)
            {
                ArrayList selectedItems = new ArrayList(gridControlUnits.SelectedItems);

                UnitClientData selectedUnit = ((GridControlDataUnit)selectedItems[0]).Tag as UnitClientData;
                if (selectedUnit != null && selectedUnit.Status.Name.Equals(UnitStatusClientData.Available.Name) == true)
                {
                    if (this.gridControlAssignedIncidentNotifications.SelectedItems.Count > 0)
                    {
                        IncidentNotificationClientData incidentNotification =
                                ((GridControlDataAssignedIncidentNotifications)this.gridControlAssignedIncidentNotifications.SelectedItems[0]).Tag as IncidentNotificationClientData;
                        if (incidentNotification != null && incidentNotification.Status.CustomCode != IncidentNotificationStatusClientData.Cancelled.CustomCode &&
                            incidentNotification.Status.CustomCode != IncidentNotificationStatusClientData.Closed.CustomCode &&
                            CheckUnitsSameDepartment(selectedItems, incidentNotification.DepartmentType.Name) == true)
                        {
                            IList<DispatchOrderClientData> dispatchOrderList = new List<DispatchOrderClientData>();
                            foreach (GridControlDataUnit data in selectedItems)
                            {
                                UnitClientData unit = (UnitClientData)data.Tag;
                                if (unit.Status.Name.Equals(selectedUnit.Status.Name))
                                {
                                    //Despachar una unidad al procedimiento en curso
                                    DispatchOrderClientData dispatch = new DispatchOrderClientData();
                                    dispatch.DispatchOperatorCode = ServerServiceClient.GetInstance().OperatorClient.Code;
                                    dispatch.StartDate = ServerServiceClient.GetInstance().GetTime();
                                    dispatch.IncidentNotificationCode = incidentNotification.Code;
                                    dispatch.IncidentNotificationCustomCode = incidentNotification.CustomCode;
                                    dispatch.IncidentNotificationVersion = incidentNotification.Version;
                                    dispatch.Unit = unit;
                                    dispatch.CustomCode = Guid.NewGuid().ToString();
                                    dispatch.Status = DispatchOrderStatusClientData.UnitOnScene;
                                    dispatchOrderList.Add(dispatch);
                                }
                            }
                            if (dispatchOrderList.Count > 0)//Slo ocurre en el caso que las unidades estn disponibles. Se necesita una notificacion seleccionada
                            {
                                ProcessDispatchOrderList(dispatchOrderList);
                            }
                        }
                    }
                }
                else if (selectedUnit != null &&
                         (selectedUnit.Status.Name.Equals(UnitStatusClientData.Assigned.Name) == true ||
                          selectedUnit.Status.Name.Equals(UnitStatusClientData.Delayed.Name) == true))
                {
                    foreach (GridControlDataUnit data in this.gridControlUnits.SelectedItems)
                    {
                        UnitClientData unit = (UnitClientData)data.Tag;
                        if (unit.Status.Name.Equals(selectedUnit.Status.Name))
                        {
                            unit.DispatchOrder.Status = DispatchOrderStatusClientData.UnitOnScene;
                            ServerServiceClient.GetInstance().SaveOrUpdateClientData(unit.DispatchOrder);
                        }
                    }
                }
                else if (selectedUnit == null)
                {
                    SmartLogger.Print(ResourceLoader.GetString2("NullReference"));
                    MessageForm.Show(ResourceLoader.GetString2("OperationNotDone"), MessageFormType.Information);
                }
            }
        }

        private void toolStripMenuItemSupportRequest_Click(object sender, EventArgs e)
        {
            if (this.gridControlAssignedIncidentNotifications.SelectedItems.Count == SINGLE_SELECTION)
            {
                IncidentNotificationClientData selectedIncidentNotification =
                    ((GridControlDataAssignedIncidentNotifications)this.gridControlAssignedIncidentNotifications.SelectedItems[0]).Tag as IncidentNotificationClientData;

                if (HasActiveDispatchedUnit(selectedIncidentNotification) == true &&
                    selectedIncidentNotification.Status.Active == true)
                {
                    OpenSupportRequestForm(selectedIncidentNotification);
                }
            }
        }

        private void OpenSupportRequestForm(IncidentNotificationClientData incidentNotification)
        {
            try
            { 
                SupportRequestForm form = new SupportRequestForm(this,incidentNotification);
                if (!form.IsDisposed)
                {
                    DialogResult result = form.ShowDialog();
                }
            }
            catch (CommunicationObjectFaultedException)
            {
                throw;
            }
            catch (FaultException ex)
            {
                if (ex.Code != null && ex.Code.Name != null
                    && ex.Code.Name.Equals(typeof(DatabaseServerDownException).Name))
                {
                    throw new CommunicationObjectFaultedException(ex.Message, ex);
                }
                else
                {
                    MessageForm.Show(ex.Message, ex);
                }
            }
            catch (Exception ex)
            {
                MessageForm.Show(ex.Message, ex);
            }
        }

        private void radioButtons_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton radioButton = sender as RadioButton;
            if (radioButton.Checked == true)
            {
                if (radioButton.Equals(this.radioButtonZone))
                {
                    unitRadioButtonFilter = UnitRadioButtonFilter.ZONE;
                }
                else if (radioButton.Equals(this.radioButtonDepartment))
                {
                    unitRadioButtonFilter = UnitRadioButtonFilter.STATION;
                }
                else if (radioButton.Equals(this.radioButtonAll))
                {
                    unitRadioButtonFilter = UnitRadioButtonFilter.ALL;
                }

                FilterUnits(unitRadioButtonFilter);

                EnableAssignButton();
                radioButtonDepartment.TabStop = true;
                radioButtonAll.TabStop = true;
                radioButtonZone.TabStop = true;
            }
        }

        private void FilterUnits(UnitRadioButtonFilter radioButtonFilter)
        {

            if (radioButtonFilter != UnitRadioButtonFilter.ALL)
            {
                if (gridControlAssignedIncidentNotifications.SelectedItems.Count > 0)
                {
                    IncidentNotificationClientData selectedNotification = ((GridControlDataAssignedIncidentNotifications)gridControlAssignedIncidentNotifications.SelectedItems[0]).Tag as IncidentNotificationClientData;
                    CriteriaOperator expr = null;

                    if (radioButtonFilter == UnitRadioButtonFilter.ZONE)
                    {
                        expr = new BinaryOperator("DepartmentZone", selectedNotification.DepartmentStation.DepartmentZone.Name, BinaryOperatorType.Equal);
                        showByFilterExpression = expr;
                        gridViewUnits.ActiveFilterCriteria = showByFilterExpression & unitStatusFilterExpression;
                    }
                    else if (radioButtonFilter == UnitRadioButtonFilter.STATION)
                    {
                        expr = new BinaryOperator("DepartmentStation", selectedNotification.DepartmentStation.Name, BinaryOperatorType.Equal);
                        showByFilterExpression = expr;
                        gridViewUnits.ActiveFilterCriteria = showByFilterExpression & unitStatusFilterExpression;
                    }
                }
            }
            else
            {
                showByFilterExpression = null;
                gridViewUnits.ActiveFilterCriteria = showByFilterExpression & unitStatusFilterExpression;
            }
            previewRowLines();
        }

        private void FilterUnits()
        {
            UnitRadioButtonFilter radioButtonFilter = UnitRadioButtonFilter.NONE;
            if (radioButtonAll.Checked) radioButtonFilter = UnitRadioButtonFilter.ALL;
            if (radioButtonZone.Checked) radioButtonFilter = UnitRadioButtonFilter.ZONE;
            if (radioButtonDepartment.Checked) radioButtonFilter = UnitRadioButtonFilter.STATION;

            if (radioButtonFilter != UnitRadioButtonFilter.NONE)
            {
                if (radioButtonFilter != UnitRadioButtonFilter.ALL)
                {
                    if (gridControlAssignedIncidentNotifications.SelectedItems.Count > 0)
                    {
                        IncidentNotificationClientData selectedNotification = ((GridControlDataAssignedIncidentNotifications)gridControlAssignedIncidentNotifications.SelectedItems[0]).Tag as IncidentNotificationClientData;
                        CriteriaOperator expr = null;

                        if (radioButtonFilter == UnitRadioButtonFilter.ZONE)
                        {
                            expr = new BinaryOperator("DepartmentZone", selectedNotification.DepartmentStation.DepartmentZone.Name, BinaryOperatorType.Equal);
                            showByFilterExpression = expr;
                            gridViewUnits.ActiveFilterCriteria = showByFilterExpression & unitStatusFilterExpression;
                        }
                        else if (radioButtonFilter == UnitRadioButtonFilter.STATION)
                        {
                            expr = new BinaryOperator("DepartmentStation", selectedNotification.DepartmentStation.Name, BinaryOperatorType.Equal);
                            showByFilterExpression = expr;
                            gridViewUnits.ActiveFilterCriteria = showByFilterExpression & unitStatusFilterExpression;
                        }
                    }
                }
                else
                {
                    showByFilterExpression = null;
                    gridViewUnits.ActiveFilterCriteria = showByFilterExpression & unitStatusFilterExpression;
                }
            }
            previewRowLines();
        }

        private void toolStripMenuItemModifyZone_Click(object sender, EventArgs e)
        {
            if (this.gridControlAssignedIncidentNotifications.SelectedItems.Count == SINGLE_SELECTION)
            {
				if (globalApplicationPreferences["CanChangeIncidentNotificationStation"].Value.Equals(true.ToString()))
				{
					IncidentNotificationClientData selectedIncidentNotification =
					   ((GridControlDataAssignedIncidentNotifications)gridControlAssignedIncidentNotifications.SelectedItems[0]).Tag as IncidentNotificationClientData;
					if (selectedIncidentNotification.Status.Name.Equals(IncidentNotificationStatusClientData.Assigned.Name) && selectedIncidentNotification.CanBeChangedZoneAndStation)
					{
						openIncidentLocationForm(selectedIncidentNotification);
					}
					else if (selectedIncidentNotification.Status.Name.Equals(IncidentNotificationStatusClientData.Delayed.Name) ||
						selectedIncidentNotification.Status.Name.Equals(IncidentNotificationStatusClientData.Pending.Name))
					{
						if ((HasActiveDispatchedUnit(selectedIncidentNotification) == false) && selectedIncidentNotification.CanBeChangedZoneAndStation)
						{
							openIncidentLocationForm(selectedIncidentNotification);
						}
					}
				}
				else
                    MessageForm.Show(ResourceLoader.GetString2("UnableZoneStationChange"), MessageFormType.Information);
            }
        }

        private void openIncidentLocationForm(IncidentNotificationClientData selectedIncidentNotification)
        {
            try
            {
                IncidentLocationForm incidentLocationForm = new IncidentLocationForm(this, selectedIncidentNotification, GlobalDepartmentZones);

                if (incidentLocationForm.ShowDialog() == DialogResult.OK)
                {
                    ServerServiceClient.GetInstance().SaveOrUpdateClientData(selectedIncidentNotification);
                    FilterUnits();
                }
            }
            catch (CommunicationObjectFaultedException)
            {
                throw;
            }
            catch (FaultException ex)
            {
                if (ex.Code != null && ex.Code.Name != null
                    && ex.Code.Name.Equals(typeof(DatabaseServerDownException).Name))
                {
                    throw new CommunicationObjectFaultedException(ex.Message, ex);
                }
                else
                {
                    ThreadPool.QueueUserWorkItem(delegate(object state)
                    {
                        try
                        {
                            AskUpdatedObjectToServer(selectedIncidentNotification);
                        }
                        catch
                        { }
                    });
                    MessageForm.Show(ex.Message, ex);
                }
            }
            catch (Exception ex)
            {
                MessageForm.Show(ex.Message, ex);
            }
        }

        private void contextMenuStripNewIncidentNotification_Opening(object sender, CancelEventArgs e)
        {
            FormUtil.InvokeRequired(this,
            delegate
            {
                gridControlNewIncidentNotifications.Focus();
                if (this.gridControlAssignedIncidentNotifications.SelectedItems.Count == SINGLE_SELECTION ||
                    this.gridControlNewIncidentNotifications.SelectedItems.Count == SINGLE_SELECTION)
                {
                    barButtonItemSave.Enabled = true;
                    barButtonItemPrint.Enabled = true;
                    barButtonItemRefreshOrder.Enabled = true;
                }
                else
                {
                    barButtonItemSave.Enabled = false;
                    barButtonItemPrint.Enabled = false;
                    barButtonItemRefreshOrder.Enabled = false;
                }
                if (this.gridControlNewIncidentNotifications.SelectedItems.Count == SINGLE_SELECTION)
                {
                    this.contextMenuStripNewIncidentNotification.Enabled = true;                                        
                }
                else
                {
                    e.Cancel = true;
                    this.contextMenuStripNewIncidentNotification.Enabled = false;                      
                }
                
                EnableLocationButton(null, EventArgs.Empty);
            });
        }

        private void toolStripMenuItemAssign_Click(object sender, EventArgs e)
        {
            assignSelectedIncidentNotification();
        }

        private void gridControlNewIncidentNotifications_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo info = ((GridViewEx)gridControlNewIncidentNotifications.MainView).CalcHitInfo(e.X, e.Y);
                if (info.InRow == true && info.RowHandle != -1)
                    assignSelectedIncidentNotification();
            }
        }

        private void buttonExAddNotification_Click(object sender, EventArgs e)
        {
            assignSelectedIncidentNotification();
        }

        private void buttonExAssign_Click(object sender, EventArgs e)
        {
            asignarToolStripMenuItem_Click(null, null);
        }

        private void AddNoteToAssignedUnit(object sender, EventArgs e)
        {
            UnitClientData selectedUnit = null;
            if (sender.Equals(toolStripMenuItemAddNote) == true)
            {
                if (gridControlUnits.SelectedItems.Count > 0)
                {
                    selectedUnit = ((GridControlDataUnit)this.gridControlUnits.SelectedItems[0]).Tag as UnitClientData;
                }
            }
            else if (sender.Equals(toolStripMenuItemAddNoteToUnit) == true)
            {
                if (gridControlDispatchOrder.SelectedItems.Count > 0)
                {
                    selectedUnit = (((GridControlDataDispatchOrder)gridControlDispatchOrder.SelectedItems[0]).Tag as DispatchOrderClientData).Unit;
                }
            }
            if (selectedUnit != null)
            {
                NoteForm<UnitClientData> noteForm = new NoteForm<UnitClientData>(selectedUnit);
                noteForm.ShowDialog();
            }
        }

        private void buttonExSave_Click(object sender, EventArgs e)
        {
            textBoxIncidentDetails.ShowSaveAsDialog();
        }

        private void Page_Load(object sender, System.EventArgs e) 		
        {
            RegistryKey regkey;

            try
            {
                DirectoryInfo source = new DirectoryInfo(SourceDirectory);

                //Determine whether the source directory exists.
                if (!source.Exists)
                    regkey = Registry.CurrentUser;
                else
                    regkey = Registry.Users;

               regkey = regkey.OpenSubKey(@SourceDirectory, true);

                if (regkey != null)
                {
                    // Store the existing values.			
                    IEFooter = regkey.GetValue("footer");
                    // Hide the direction (URL) from Web page footer
                    objectValue = (string)IEFooter;
                    regkey.SetValue("footer", objectValue.Replace("&u", ""));
                }
            }
            catch
            { }
        }

        private void buttonExPrint_Click(object sender, EventArgs e) 
        {
            //printDialogMain = new PrintDialog();
            //if (textBoxIncidentDetails.ShowPrintDialog() == DialogResult.OK)
            //{
            textBoxIncidentDetails.ShowPrintDialog();
            //textBoxIncidentDetails.Print();
            //}
            //textBoxIncidentDetails.ShowPrintDialog();
        }

        private void dataGridTotalDispatches_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.Control && e.KeyCode == Keys.Tab)
            //{
            //    this.tabAsociatedInfo.SelectedTab = this.tabPageAsociatedCalls;
            //}
            //else if (e.KeyCode == Keys.Tab)
            //{
            //    if (e.Shift)
            //    {
            //        this.tabAsociatedInfo.Focus();
            //    }
            //    else
            //    {
            //        this.gridViewAssignedIncidentNotifications.Focus();
            //    }
            //}            
        }

        private void dataGridTotalCalls_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.Control && e.KeyCode == Keys.Tab)
            //{
            //    this.tabAsociatedInfo.SelectedTab = this.tabPageAssociatedDisp;
            //}
            //else if (e.KeyCode == Keys.Tab)
            //{
            //    if (e.Shift)
            //    {
            //        this.tabAsociatedInfo.Focus();
            //    }
            //    else
            //    {
            //        this.richTextBoxPhoneReportDetails.Focus();
            //    }
            //}
        }


        private void gridViewNewIncidentNotifications_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Tab)
            {
                if (e.Shift)
                {
                    this.callsAndDispatch.Focus();
                }
                else
                {
                    this.buttonExAddNotification.Focus();
                }
            }
            else if (e.KeyCode == Keys.F5)
            {
                toolStripMenuItemRefreshNewIncidentNotificationDetails_Click(null, null);
            }
            else if (e.Control == false && e.KeyCode == Keys.F2)
            {
                assignSelectedIncidentNotification();
            }
            DispatchFormDevX form = (this.MdiParent as DispatchFormDevX);
            PreviewKeyDownEventArgs e1 = new PreviewKeyDownEventArgs(e.KeyData);
            if (form != null)
                form.DispatchFormDevX_PreviewKeyDown(null, e1);
        }
     
      

        private void toolStripMenuItemHelpOnLine_Click(object sender, EventArgs e)
        {
            try
            {
                ApplicationUtil.LaunchHelp(SmartCadConfiguration.DistFolder + "/Help/DISPATCH Module.chm");
            }
            catch (Exception ex)
            {
                MessageForm.Show(ResourceLoader.GetString2("$Message.HelpNotInstalled"), ex);
            }
        }

        private void buttonExPrint_Leave(object sender, EventArgs e)
        {
         	
        }

        private void buttonExPrint_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {

        }

        private void richTextBoxPhoneReportDetails_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            DispatchFormDevX form = (this.MdiParent as DispatchFormDevX);
			if (form != null)
				form.DispatchFormDevX_PreviewKeyDown(null, e);
        }

        private void textBoxIncidentDetails_KeyDownEx(object sender, KeyEventArgs e)
        {
			DispatchFormDevX form = (this.MdiParent as DispatchFormDevX);
			PreviewKeyDownEventArgs e1 = new PreviewKeyDownEventArgs(e.KeyData);
			if (form != null)
				form.DispatchFormDevX_PreviewKeyDown(null, e1);
        }

        private void richTextBoxPhoneReportDetails_KeyDownEx(object sender, KeyEventArgs e)
        {
			DispatchFormDevX form = (this.MdiParent as DispatchFormDevX);
			PreviewKeyDownEventArgs e1 = new PreviewKeyDownEventArgs(e.KeyData);
			if (form != null)
				form.DispatchFormDevX_PreviewKeyDown(null, e1);
        }

        private void buttonAddNotes_MouseEnter(object sender, EventArgs e)
        {
            this.toolTipGeneral.SetToolTip(buttonAddNotes, ResourceLoader.GetString2("AddNoteToRequest"));
        }

        private void buttonAddNotes_MouseLeave(object sender, EventArgs e)
        {
            this.toolTipGeneral.Hide(buttonAddNotes);
        }

        private void gridControlNewIncidentNotifications_CellToolTipNeeded(object sender, DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventArgs e)
        {
            if (e.Info == null)
            {
                DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo info = ((GridViewEx)gridControlNewIncidentNotifications.MainView).CalcHitInfo(e.ControlMousePosition.X, e.ControlMousePosition.Y);

                if (info.RowHandle > -1 && info.RowHandle != GridControlEx.InvalidRowHandle && info.RowHandle < gridControlNewIncidentNotifications.Items.Count)
                {
                    IncidentNotificationClientData notification =
                        ((GridControlDataNewIncidentNotifications)gridViewNewIncidentNotifications.GetRow(info.RowHandle)).Tag as IncidentNotificationClientData;

                    string infoText = string.Empty;
                    if (notification != null)
                    {
                        infoText = ApplicationUtil.GetFormattedElapsedTime(ResourceLoader.GetString2("WaitTime"), ServerServiceClient.GetInstance().GetTime(), notification.CreationDate);
                    }
                    else
                    {
                        infoText = "";
                    }
                    e.Info = new DevExpress.Utils.ToolTipControlInfo(new DevExpress.XtraGrid.Views.Base.CellToolTipInfo(info.RowHandle, info.Column, "cell"), infoText);
                }
            }
        }

		private void ControlsTabPressed(object sender, PreviewKeyDownEventArgs e)
		{			
			if (sender.Equals(this.gridControlDispatchOrder))
			{
				if (e.KeyCode == Keys.Tab)
				{
					if (e.Shift)
					{
						this.gridViewUnits.Focus();
					}
				}
			}
			else if (sender.Equals(this))
			{
				if (e.KeyCode == Keys.Tab)
				{
					if (e.Shift)
					{
						this.callsAndDispatch.gridViewExTotalCalls.Focus();
					}
					else
					{
                      	gridViewNewIncidentNotifications.Focus();
					}
				}
			}
            else if (sender.Equals(textBoxIncidentDetails))
            {
                DefaultDispatchFormDevX_KeyDown(sender, new KeyEventArgs(e.KeyData));
            }
		}

        private void toolStripMenuItemRefreshIncidentNotitficationDetails_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridControlAssignedIncidentNotifications.SelectedItems.Count > 0)
                    OnRefreshIncidentReport();
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        private void toolStripMenuItemRefreshNewIncidentNotificationDetails_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.gridViewNewIncidentNotifications.SelectedRowsCount > 0)
                    OnRefreshIncidentReport();
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        private void buttonExRefreshIncidentDetails_Click(object sender, EventArgs e)
        {
            try
            {
                OnRefreshIncidentReport();
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        private void AssignedNotificationFilter_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control == true && e.KeyCode == Keys.Tab)
            {
                (sender as Button).SelectNextControl(gridControlAssignedIncidentNotifications, true, true, true, true);
                gridViewAssignedIncidentNotifications_KeyDown(sender, e);
            }
        }

        private void UnitFilter_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control == true && e.KeyCode == Keys.Tab)
            {
                (sender as Button).SelectNextControl(this.gridControlUnits, true, true, true, true);
                gridViewUnits_KeyDown(sender, e);
            }
        }
  
        private void AskUpdatedObjectToServer(ClientData clientObjectData)
        {
            if (clientObjectData != null && clientObjectData.Code > 0)
            {
                try
                {
                    ClientData objectData;
                    ConstructorInfo constructor = clientObjectData.GetType().GetConstructor(Type.EmptyTypes);
                    objectData = constructor.Invoke(null) as ClientData;
                    objectData.Code = clientObjectData.Code;
                    objectData = ServerServiceClient.GetInstance().RefreshClient(objectData);
                    if (objectData != null)
                    {
                        CommittedObjectDataCollectionEventArgs args =
                            new CommittedObjectDataCollectionEventArgs(objectData, CommittedDataAction.Update, null);
                        serverServiceClient_CommittedChanges(null, args);
                    }
                }
                catch (Exception ex)
                {
                    SmartLogger.Print(ex);
                }
            }
        }

        private bool checkStillActiveIncidentNotification()
        {
            foreach (GridControlDataAssignedIncidentNotifications gaind in gridControlAssignedIncidentNotifications.Items)
            {
                IncidentNotificationClientData notif = gaind.Tag as IncidentNotificationClientData;
                if (notif != null && notif.Status.Active == true)
                {
                    return true;
                }
            }
            return false;
        }
		
		private void timerSendImage_Tick(object sender, EventArgs e)
        {
            OperatorClientData oper = new OperatorClientData();
            oper.Code = ServerServiceClient.GetInstance().OperatorClient.Code;

            ApplicationImageClientData aicd = new ApplicationImageClientData();
            aicd.Operator = oper;
            aicd.Application = UserApplicationClientData.Dispatch.Name;
            aicd.ToApplications = new string[] { UserApplicationClientData.Supervision.Name };
            aicd.ToOperators = remoteControlOperators;
            
            byte[] image =ApplicationUtil.GetScreenShot(this.Handle);
            aicd.OriginalLength = image.Length;

            byte[] compressImage;
            MiniLZO.Compress(image, out compressImage);
            aicd.Image = compressImage;

            ServerServiceClient.GetInstance().SendClientData(aicd);
		}

		private void barButtonItemStatus_ItemPress(object sender, ItemClickEventArgs e)
		{
            if (Enabled == false)
			    EnableIncidentNotificationAssignment(true);
			BarButtonItem buttonItem = e.Item as BarButtonItem;      
            
            if (buttonItem.Tag != defaultBusyStatus && buttonItem.Tag != defaultReadyStatus)
            {               
                if (checkStillActiveIncidentNotification() == true)
                {
                    SetToolStripOperatorStatus(defaultBusyStatus);
                    ServerServiceClient.GetInstance().SetOperatorStatus(defaultBusyStatus);
                }
                else
                {
                    SetToolStripOperatorStatus(defaultReadyStatus);
                    ServerServiceClient.GetInstance().SetOperatorStatus(defaultReadyStatus);	
                }                               
            }		
		}

		private void barButtonItemPending_ItemClick(object sender, ItemClickEventArgs e)
		{
			toolStripMenuItemPending.PerformClick();
		}

		private void barButtonItemModifyZone_ItemClick(object sender, ItemClickEventArgs e)
		{
			toolStripMenuItemModifyZone.PerformClick();
		}

		private void barButtonItemClose_ItemClick(object sender, ItemClickEventArgs e)
		{
			toolStripMenuItemCloseIncNotif.PerformClick();
		}

		private void barButtonItemRefreshOrder_ItemClick(object sender, ItemClickEventArgs e)
		{
            int option = -1;
            FormUtil.InvokeRequired(this,
                delegate
                {
                    if (gridControlNewIncidentNotifications.SelectedItems.Count == SINGLE_SELECTION)
                    {
                        option = 0;
                    }
                    else if (gridControlAssignedIncidentNotifications.SelectedItems.Count == SINGLE_SELECTION)
                    {
                        option = 1;
                    }
                    else
                    {
                        option = -1;
                    }
                });
            if (option == 0)
            {
                toolStripMenuItemRefreshNewIncidentNotificationDetails.PerformClick();
            }
            else if (option == 1)
            {
                toolStripMenuItemRefreshIncidentNotitficationDetails.PerformClick();
            }			
		}

        private void barButtonItemSave_ItemClick(object sender, ItemClickEventArgs e)
        {
            buttonExSave_Click(null, null);
        }

        private void barButtonItemPrint_ItemClick(object sender, ItemClickEventArgs e)
        {
            buttonExPrint_Click(null, null);
        }

		private void barButtonItemAvailable_ItemClick(object sender, ItemClickEventArgs e)
		{
			UnitsToolStripMenuItem_Click(toolStripMenuItemAvailableUnit, e);
		}

		private void barButtonItemOutOfOrder_ItemClick(object sender, ItemClickEventArgs e)
		{
			toolStripMenuItemOutOfOrderUnit.PerformClick();
		}

		private void barButtonItemUnavailable_ItemClick(object sender, ItemClickEventArgs e)
		{
			toolStripMenuItemNotAvailableUnit.PerformClick();
		}

		private void barButtonItemOnScene_ItemClick(object sender, ItemClickEventArgs e)
		{
			toolStripMenuItemOnSceneUnit_Click(toolStripMenuItemOnSceneUnit, e);
		}

		private void barButtonItemAssignedOnScene_ItemClick(object sender, ItemClickEventArgs e)
		{
			enEscenaToolStripMenuItem.PerformClick();
		}

		private void barButtonItemModifyTime_ItemClick(object sender, ItemClickEventArgs e)
		{
			modifyTimeOrderToolStripMenuItem.PerformClick();
		}

		private void barButtonItemEnd_ItemClick(object sender, ItemClickEventArgs e)
		{
			cerrarToolStripMenuItem.PerformClick();
		}

		private void barButtonItemAddNote_ItemClick(object sender, ItemClickEventArgs e)
		{
			toolStripMenuItemAddNoteToUnit.PerformClick();
		}

		private void barButtonItemSupportRequest_ItemClick(object sender, ItemClickEventArgs e)
		{
			toolStripMenuItemSupportRequest.PerformClick();
		}

        private void gridControlUnits_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                GridControlEx grid = sender as GridControlEx;

                if (gridControlAssignedIncidentNotifications.SelectedItems.Count == SINGLE_SELECTION
                    && grid.SelectedItems.Count == SINGLE_SELECTION)
                {
                    UnitClientData unit = ((GridControlDataUnit)grid.SelectedItems[0]).Tag as UnitClientData;
                    IncidentNotificationClientData incidentNotification =
                        ((GridControlDataAssignedIncidentNotifications)gridControlAssignedIncidentNotifications.SelectedItems[0]).Tag as IncidentNotificationClientData;
                    if (unit.Status.Name.Equals(UnitStatusClientData.Available.Name) &&
                        incidentNotification.Status.Active == true &&
                        unit.DepartmentStation.DepartmentZone.DepartmentType.Name == incidentNotification.DepartmentType.Name)
                    {

                        DataObject data = new DataObject(new object[] { grid.SelectedItems });
                        DragEventArgs args = new DragEventArgs(data, 0, e.X, e.Y, DragDropEffects.None, DragDropEffects.None);
                        gridControlDispatchOrder_DragDrop(grid, args);      
                    }
                }
            }
        }

        private void gridViewUnits_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Control == true && e.KeyCode == Keys.Tab)
                {
                    if (unitButtonFilter == UnitButtonFilter.ALL || buttonAllUnits.BackColor == SystemColors.ButtonHighlight)
                        buttonAvailableUnits_Click(buttonAvailableUnits, null);
                    else if (unitButtonFilter == UnitButtonFilter.AVAILABLE || buttonAvailableUnits.BackColor == SystemColors.ButtonHighlight)
                        buttonAssignedUnits_Click(buttonAssignedUnits, null);
                    else if (unitButtonFilter == UnitButtonFilter.ASSIGNED || buttonAssignedUnits.BackColor == SystemColors.ButtonHighlight)
                        buttonReqAttention_Click(buttonReqAttention, null);
                    else if (unitButtonFilter == UnitButtonFilter.ATTENTION || buttonReqAttention.BackColor == SystemColors.ButtonHighlight)
                        buttonAllUnits_Click(buttonAllUnits, null);
                    else
                        buttonAllUnits_Click(buttonAllUnits, null);
                }
                else if (e.KeyCode == Keys.Tab)
                {
                    if (e.Shift == false)
                    {
                        this.buttonExAssign.Focus();
                    }
                }
                else if (gridControlUnits.SelectedItems.Count > 0 &&
                    CheckOneUnitStatusSelected(gridControlUnits.SelectedItems) == true &&
                    CheckUserCanChangeStatus(this.gridControlUnits.SelectedItems) == true)
                {
                    UnitClientData unit = ((GridControlDataUnit)this.gridControlUnits.SelectedItems[0]).Tag as UnitClientData;

                    if (e.Control == true)
                    {
                        if (e.KeyCode == Keys.A)
                        {
                            bool single = gridControlAssignedIncidentNotifications.SelectedItems.Count == SINGLE_SELECTION;
                            IncidentNotificationClientData incidentNotification = null;
                            if (single == true)
                            {
                                incidentNotification =
                                    ((GridControlDataAssignedIncidentNotifications)gridControlAssignedIncidentNotifications.SelectedItems[0]).Tag as IncidentNotificationClientData;
                            }

                            if (unit.Status.Name.Equals(UnitStatusClientData.Available.Name) == true &&
                                single == true &&
                                incidentNotification != null &&
                                incidentNotification.DepartmentType.Name == unit.DepartmentStation.DepartmentZone.DepartmentType.Name &&
                                incidentNotification.Status.Name.Equals(IncidentNotificationStatusClientData.Cancelled.Name) == false &&
                                incidentNotification.Status.Name.Equals(IncidentNotificationStatusClientData.Closed.Name) == false)
                            {
                                asignarToolStripMenuItem_Click(null, null);
                            }
                            e.Handled = true;
                        }
                        else if (e.KeyCode == Keys.D)
                        {
                            if (unit.Status.Name.Equals(UnitStatusClientData.NotAvailable.Name) ||
                                unit.Status.Name.Equals(UnitStatusClientData.OutOfOrder.Name))
                            {
                                UnitsToolStripMenuItem_Click(toolStripMenuItemAvailableUnit, EventArgs.Empty);
                            }
                            e.Handled = true;
                        }
                        else if (e.KeyCode == Keys.E && unit.Status.Name.Equals(UNIT_AVAILABLE))
                        {
							if (gridControlAssignedIncidentNotifications.SelectedItems.Count > 0)
							{
								IncidentNotificationClientData incidentNotification = null;
								incidentNotification =
										((GridControlDataAssignedIncidentNotifications)gridControlAssignedIncidentNotifications.SelectedItems[0]).Tag as IncidentNotificationClientData;
								if (incidentNotification.CustomCode != IncidentNotificationStatusClientData.Cancelled.CustomCode &&
									incidentNotification.CustomCode != IncidentNotificationStatusClientData.Closed.CustomCode)
								{
									toolStripMenuItemOnSceneUnit_Click(null, null);
								}
							}
                            e.Handled = true;
                        }
                        else if (e.KeyCode == Keys.N)
                        {
                            if (unit.Status.Name.Equals(UnitStatusClientData.Available.Name) == true ||
                                unit.Status.Name.Equals(UnitStatusClientData.OutOfOrder.Name) == true)
                            {
                                UnitsToolStripMenuItem_Click(toolStripMenuItemNotAvailableUnit, EventArgs.Empty);
                            }
                            e.Handled = true;
                        }
                        else if (e.KeyCode == Keys.X)
                        {
                            if (unit.Status.Name.Equals(UnitStatusClientData.Available.Name) == true ||
                                unit.Status.Name.Equals(UnitStatusClientData.NotAvailable.Name) == true)
                            {
                                UnitsToolStripMenuItem_Click(toolStripMenuItemOutOfOrderUnit, EventArgs.Empty);
                            }
                            e.Handled = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageForm.Show(ex.Message, ex);
            }
            DispatchFormDevX form = (this.MdiParent as DispatchFormDevX);
            PreviewKeyDownEventArgs e1 = new PreviewKeyDownEventArgs(e.KeyData);
            if (form != null)
                form.DispatchFormDevX_PreviewKeyDown(null, e1);
            //previewRowLines();
        }

        private void gridControlUnits_CellToolTipNeeded(object sender, DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventArgs e)
        {
            if (e.Info == null)
            {
                DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo info = ((GridViewEx)gridControlUnits.MainView).CalcHitInfo(e.ControlMousePosition.X, e.ControlMousePosition.Y);

                if (info.RowHandle > -1 && info.RowHandle != GridControlEx.InvalidRowHandle)
                {
                    UnitClientData unit =
                        (UnitClientData)((GridControlDataUnit)gridViewUnits.GetRow(info.RowHandle)).Tag;
                    string infoText = string.Empty;
                    if (unit != null)
                    {
                        string idPortatil = string.IsNullOrEmpty(unit.IdRadio) ? "" : unit.IdRadio;
                        infoText = ResourceLoader.GetString2("PortableID") + ": " + idPortatil;

                    }
                    else
                        infoText = string.Empty;
                    e.Info = new DevExpress.Utils.ToolTipControlInfo(new DevExpress.XtraGrid.Views.Base.CellToolTipInfo(info.RowHandle, info.Column, "cell"), infoText);
                }
            }
        }

        private void gridControlDispatchOrder_MouseCaptureChanged(object sender, EventArgs e)
        {
            if (this.gridControlDispatchOrder.SelectedItems.Count > 0)
            {
                this.contextMenuDispatchOrder.Enabled = true;
            }
            else
            {
                this.contextMenuDispatchOrder.Enabled = false;
            }
        }

        private void gridViewDispatchOrder_MouseMove(object sender, MouseEventArgs e)
        {
            if (gridControlDispatchOrder.SelectedItems.Count == 1)
            {
                this.contextMenuDispatchOrder.Enabled = true;
            }
            else
            {
                this.contextMenuDispatchOrder.Enabled = false;
            }
        }

        private void gridControlDispatchOrder_DragDrop(object sender, DragEventArgs e)
        {
            try
            {
                if (assignUnitsToIncidentNotification == false && IsGridUnitDataType(e) == true)
                {
                    assignUnitsToIncidentNotification = true;
                    int countAvailableUnitsBySameZone = 0;
                    int countUnitsOtherZones = 0;
                    IList<DispatchOrderClientData> dispatchOrderList =
                        BuildNewDispatchOrderList(e, ref countAvailableUnitsBySameZone, ref countUnitsOtherZones);
                    string message = null;
                    if (countAvailableUnitsBySameZone > 0)
                    {
                        if (countUnitsOtherZones == SINGLE_SELECTION)
                        {
                            message = ResourceLoader.GetString2("IncidentZoneUnitAvailables");
                        }
                        else if (countUnitsOtherZones > SINGLE_SELECTION)
                        {
                            message = ResourceLoader.GetString2("IncidentZoneMultipleUnitAvailables");
                        }
                    }
                    if (dispatchOrderList.Count > 0)
                    {
                        if (message != null)
                        {
                            DialogResult dialogResult = MessageForm.Show(message, MessageFormType.Question);
                            if (dialogResult == DialogResult.Yes)
                            {
                                OpenFormToMakeDispatches(dispatchOrderList);
                            }
                        }
                        else
                        {
                            OpenFormToMakeDispatches(dispatchOrderList);
                        }
                    }
                    gridControlDispatchOrder.Focus();
                }
            }
            catch (CommunicationObjectFaultedException)
            {
                throw;
            }
            catch (FaultException ex)
            {
                if (ex.Code != null && ex.Code.Name != null
                    && ex.Code.Name.Equals(typeof(DatabaseServerDownException).Name))
                {
                    throw new CommunicationObjectFaultedException(ex.Message, ex);
                }
                else
                {
                    MessageForm.Show(ex.Message, ex);
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
            finally
            {
                assignUnitsToIncidentNotification = false;
            }
        }


        private void gridControlAssignedIncidentNotifications_DragOver(object sender, DragEventArgs e)
        {
            object[] obj = e.Data.GetData(typeof(object[])) as object[];

            if (obj != null)
            {
                GridControlData item = ((GridViewEx)((GridControlEx)obj[1]).MainView).GetRow((int)obj[0]) as GridControlData;
                if ((item.Tag is IncidentNotificationClientData) == true)
                {
                    if ((e.KeyState & 8) != 0)
                        e.Effect = DragDropEffects.Copy;
                    else
                        e.Effect = DragDropEffects.Move;

                }
                else
                    e.Effect = DragDropEffects.None;

            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        private void gridControlDispatchOrder_DragOver(object sender, DragEventArgs e)
        {
            if (this.gridControlAssignedIncidentNotifications.SelectedItems.Count == 0)
            {
                e.Effect = DragDropEffects.None;
            }
            else
            {
                IList list = (IList)((object[])e.Data.GetData(typeof(object[])))[0];
                
                IncidentNotificationClientData incidentNotification =
                    ((GridControlDataAssignedIncidentNotifications)gridControlAssignedIncidentNotifications.SelectedItems[0]).Tag as IncidentNotificationClientData;
            
                string departmentName = string.Empty;
                if (list.Count > 0 &&
                    IsGridUnitDataType(e) == true &&
                    CheckUnitsSameDepartment(list, incidentNotification.DepartmentType.Name) == true)
                {
                    GridControlDataUnit item = list[0] as GridControlDataUnit;
                    departmentName = (item.Tag as UnitClientData).DepartmentStation.DepartmentZone.DepartmentType.Name;
                }

                if (string.IsNullOrEmpty(departmentName) == true || /*Validado el hecho q el count sea 0 o que no sean del mismo departamento*/
                    CheckUnitsInStatus(list, UnitStatusClientData.Available.Name) == false ||
                    CheckUserCanChangeStatus(list) == false ||
                    incidentNotification.Status.Name.Equals(IncidentNotificationStatusClientData.Closed.Name) ||
                    incidentNotification.Status.Name.Equals(IncidentNotificationStatusClientData.Cancelled.Name))
                {
                    e.Effect = DragDropEffects.None;
                }
                else 
                {
                    e.Effect = DragDropEffects.Move;
                }
            }
        }

        private void gridControlDispatchOrder_CellToolTipNeeded(object sender, DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventArgs e)
        {

            if (e.Info == null)
            {
                DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo info = ((GridViewEx)this.gridControlDispatchOrder.MainView).CalcHitInfo(e.ControlMousePosition.X, e.ControlMousePosition.Y);

                if (info.RowHandle > -1 && info.RowHandle != GridControlEx.InvalidRowHandle)
                {                    
                    if (this.gridViewDispatchOrder.GetRow(info.RowHandle) != null)
                    {
                        DispatchOrderClientData dispatchOrder =
                            (DispatchOrderClientData)((GridControlDataDispatchOrder)this.gridViewDispatchOrder.GetRow(info.RowHandle)).Tag;
                        if (dispatchOrder != null)
                        {
                            if (dispatchOrder.Status.Name == DispatchOrderStatusClientData.Dispatched.Name)
                            {
                                TimeSpan diff = dispatchOrder.ArrivalDate - ServerServiceClient.GetInstance().GetTime();
                                string infoText = string.Empty;
                                if (diff.CompareTo(TimeSpan.Zero) == 1)
                                {
                                    infoText = ResourceLoader.GetString2("RequiredTimeToArrive", Math.Truncate(diff.TotalHours).ToString(), diff.Minutes.ToString(), diff.Seconds.ToString());
                                }
                                else
                                {
                                    infoText = "";
                                }
                                e.Info = new DevExpress.Utils.ToolTipControlInfo(new DevExpress.XtraGrid.Views.Base.CellToolTipInfo(info.RowHandle, info.Column, "cell"), infoText);
                            }
                        }
                    }
                }                
            }
        }

        private void gridViewDispatchOrder_KeyDown(object sender, KeyEventArgs e)
        {
            //if (dataGridAssignedUnits.SelectedItems.Count == SINGLE_SELECTION)
            if (this.gridControlDispatchOrder.SelectedItems.Count > 0 &&
                CheckOneDispatchOrderStatusSelected(gridControlDispatchOrder.SelectedItems) == true)
            {
                if (e.Control == true)
                {
                    DispatchOrderClientData selectedOrder = ((GridControlDataDispatchOrder)gridControlDispatchOrder.SelectedItems[0]).Tag as DispatchOrderClientData;
                    UnitStatusClientData previousUnitStatus = selectedOrder.Unit.Status;

                    if (e.KeyCode == Keys.E)
                    {
                        DispatchOrderStatusClientData status = DispatchOrderStatusClientData.UnitOnScene;
                        //Status associated specifically to dispatch order
						foreach (GridControlDataDispatchOrder gridItem in gridControlDispatchOrder.SelectedItems)
                        {
                            DispatchOrderClientData selectedOrderItem = gridItem.Tag as DispatchOrderClientData;
                            if (selectedOrderItem.Status.Name.Equals(status.Name) == false)
                            {
                                selectedOrderItem.Status = status;
                                ServerServiceClient.GetInstance().SaveOrUpdateClientData(selectedOrderItem);
                            }
                        }
                    }
                    else if (e.KeyCode == Keys.F)
                    {
                        if (selectedOrder.Unit.Status.Name.Equals(UnitStatusClientData.Assigned.Name) == true ||
                            selectedOrder.Unit.Status.Name.Equals(UnitStatusClientData.Delayed.Name) == true ||
                            selectedOrder.Unit.Status.Name.Equals(UnitStatusClientData.OnScene.Name) == true)
                        {
                            this.contextDispOrderMenu(cerrarToolStripMenuItem, null);
                        }
                    }
                    else if (e.KeyCode == Keys.T)
                    {
                        if (selectedOrder.Unit.Status.Name.Equals(UnitStatusClientData.Assigned.Name) == true ||
                            selectedOrder.Unit.Status.Name.Equals(UnitStatusClientData.Delayed.Name) == true)
                        {
                            this.contextDispOrderMenu(modifyTimeOrderToolStripMenuItem, null);
                        }
                    }
                    else if (e.KeyCode == Keys.I)
                    {
                        if (this.gridControlDispatchOrder.SelectedItems.Count == SINGLE_SELECTION &&
                            (selectedOrder.Unit.Status.Name.Equals(UnitStatusClientData.Assigned.Name) == true ||
                            selectedOrder.Unit.Status.Name.Equals(UnitStatusClientData.Delayed.Name) == true ||
                            selectedOrder.Unit.Status.Name.Equals(UnitStatusClientData.OnScene.Name) == true))
                        {
                            AddNoteToAssignedUnit(toolStripMenuItemAddNoteToUnit, null);
                        }
                    }
                }
                else if (e.KeyCode == Keys.Tab)
                {
                    if (e.Shift)
                    {
                        if (this.buttonExAssign.Enabled == true)
                            this.buttonExAssign.Focus();
                        else
                            this.gridViewUnits.Focus();
                    }
                }
            }
            else if (e.KeyCode == Keys.Tab)
            {
                if (e.Shift)
                {
                    if (this.buttonExAssign.Enabled == true)
                        this.buttonExAssign.Focus();
                    else
                        this.gridViewUnits.Focus();
                }
            }


            DispatchFormDevX form = (this.MdiParent as DispatchFormDevX);
            PreviewKeyDownEventArgs e1 = new PreviewKeyDownEventArgs(e.KeyData);
            if (form != null)
                form.DispatchFormDevX_PreviewKeyDown(null, e1);
        }


        DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hitInfo = null;

        private void gridViewNewIncidentNotifications_MouseDown(object sender, MouseEventArgs e)
        {
            hitInfo = gridViewNewIncidentNotifications.CalcHitInfo(new Point(e.X, e.Y));
            if (hitInfo.RowHandle < 0) 
                hitInfo = null;       
        }

        private void gridViewNewIncidentNotifications_MouseMove(object sender, MouseEventArgs e)
        {
            if (hitInfo != null && e.Button == MouseButtons.Left)
            {
                Rectangle dragRect = new Rectangle(new Point(
                    hitInfo.HitPoint.X - SystemInformation.DragSize.Width / 2,
                    hitInfo.HitPoint.Y - SystemInformation.DragSize.Height / 2), SystemInformation.DragSize);
                if (!dragRect.Contains(new Point(e.X, e.Y)))
                {
                    if (hitInfo.InRow)
                    {
                        gridControlNewIncidentNotifications.DoDragDrop(new object[] { hitInfo.RowHandle, gridControlNewIncidentNotifications}, DragDropEffects.All);
                    }
                }
            }
        }

        private void gridViewUnits_MouseDown(object sender, MouseEventArgs e)
        {
            hitInfo = this.gridViewUnits.CalcHitInfo(new Point(e.X, e.Y));
            if (hitInfo.RowHandle < 0)
                hitInfo = null;
            previewRowLines();
        }

        private void gridViewUnits_MouseMove(object sender, MouseEventArgs e)
        {
            if (hitInfo != null && e.Button == MouseButtons.Left)
            {
                Rectangle dragRect = new Rectangle(new Point(
                    hitInfo.HitPoint.X - SystemInformation.DragSize.Width / 2,
                    hitInfo.HitPoint.Y - SystemInformation.DragSize.Height / 2), SystemInformation.DragSize);
                if (!dragRect.Contains(new Point(e.X, e.Y)))
                {
                    if (hitInfo.InRow)
                    {
                        gridControlUnits.DoDragDrop(new object[] {gridControlUnits.SelectedItems}, DragDropEffects.All);
                    }
                }
            }
        }

        private void gridViewAssignedIncidentNotifications_SingleSelectionChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            barButtonItemSupportRequest.Enabled = false;
            toolStripMenuItemSupportRequest.Enabled = false;
            GridViewEx grid = sender as GridViewEx;
            IncidentNotificationClientData selectedIncidentNotification = new IncidentNotificationClientData();
            if (e != null && e.FocusedRowHandle > -1 && gridControlAssignedIncidentNotifications.SelectedItems.Count > 0)
            {
                bool calledFromUnit = false;
                FormUtil.InvokeRequired(this.contextMenuStripUnits, delegate
                {
                    this.contextMenuStripUnits.Enabled = false;
                    this.contextMenuStripUnits.Close();
                });                

                this.selectedIncidentCode = -1;
                if (gridControlAssignedIncidentNotifications.SelectedItems.Count > 0)
                {
                    selectedIncidentNotification = ((GridControlDataAssignedIncidentNotifications)this.gridControlAssignedIncidentNotifications.SelectedItems[0]).Tag as IncidentNotificationClientData;
                    this.selectedIncidentNotificationCustomCode = selectedIncidentNotification.CustomCode;

                    ArrayList parameters = new ArrayList();
                    parameters.Add(selectedIncidentNotification.IncidentCode);

                    SelectedIncidentTask selectedIncidentTask = new SelectedIncidentTask(parameters);

                    ArrayList tasks = new ArrayList();
                    tasks.Add(selectedIncidentTask);


                    if (calledFromDataGridUnits == false)
                    {
                        FormUtil.InvokeRequired(this, delegate
                        {
                            //radioButtonDepartment.Checked = false;
                            if (radioButtonDepartment.Checked == false)
                            {
                                radioButtonDepartment.Checked = true;
                            }
                            else
                            {
                                radioButtons_CheckedChanged(radioButtonDepartment, null);
                            }
                        });
                    }
                    else
                    {
                        lock (syncCalledFromDataGridUnits)
                        {
                            calledFromDataGridUnits = false;
                            calledFromUnit = true;
                        }
                    }


                    FormUtil.InvokeRequired(this.radioButtonZone, delegate
                    {
                        this.radioButtonZone.Enabled = true;
                    });

                    FormUtil.InvokeRequired(this.radioButtonDepartment, delegate
                    {
                        this.radioButtonDepartment.Enabled = true;
                    });

                    FormUtil.InvokeRequired(this, delegate
                    {
                        this.callsAndDispatch.gridControlExTotalDispatchs.ClearData();
                        this.callsAndDispatch.gridControlExTotalCalls.ClearData();
                    });

                    ThreadPool.QueueUserWorkItem(delegate(object state)
                    {
                        try
                        {
                            SelectedIncidentHelp(selectedIncidentTask, selectedIncidentNotification, calledFromUnit);
                        }
                        catch (Exception ex)
                        {
                            SmartLogger.Print(ex);
                        }
                    });

                    if (selectedIncidentNotification.Status.Name == IncidentNotificationStatusClientData.Assigned.Name ||
                        (selectedIncidentNotification.Status.Name == IncidentNotificationStatusClientData.InProgress.Name &&
                         selectedIncidentNotification.DispatchOrders != null && selectedIncidentNotification.DispatchOrders.Count == 0) ||
                         (selectedIncidentNotification.Status.Name == IncidentNotificationStatusClientData.Updated.Name &&
                          selectedIncidentNotification.DispatchOrders != null && selectedIncidentNotification.DispatchOrders.Count == 0))
                    {
                        if (gridControlUnits.DataSource != null)
                        {
                            BindingList<GridControlDataUnit> dataSource = new BindingList<GridControlDataUnit>();
                            foreach (GridControlDataUnit gdu in (BindingList<GridControlDataUnit>)gridControlUnits.DataSource)
                            {
                                dataSource.Add(gdu);
                            }
                            lock (dataSource)
                            {
                                int availableUnits = dataSource.Count(Unit => Unit.Status == UnitStatusClientData.Available.FriendlyName
                                                                                                             && Unit.DepartmentType == selectedIncidentNotification.DepartmentType.Name);
                                if (availableUnits == 0)
                                {
                                    MessageForm.Show(ResourceLoader.GetString2("NotAvailablesUnitsForDispatch"), MessageFormType.Warning);
                                }
                            }
                        }
                    }                    
                    FormUtil.InvokeRequired(this.textBoxIncidentNotes, delegate
                    {
                        this.textBoxIncidentNotes.Enabled = true;
                    });                    
                }
                else
                {                    
                    FormUtil.InvokeRequired(this.textBoxIncidentNotes,
                        delegate
                        {
                            this.textBoxIncidentNotes.Enabled = false;
                            this.textBoxIncidentNotes.BackColor = Color.White;
                        });

                    if (gridViewUnits.DataSource != null)
                    {
                        FormUtil.InvokeRequired(this,
                            delegate
                            {
                                List<GridControlDataUnit> unitValues = ((BindingList<GridControlDataUnit>)gridViewUnits.DataSource).Where(unit => unit.Recommended == "Si").ToList();

                                foreach (GridControlDataUnit gridData in unitValues)
                                {
                                    UnitClientData unit = gridData.Tag as UnitClientData;
                                    unit.Distance = 0;
                                    gridData.BackColor = unit.Status.Color;
                                    gridData.Recommended = string.Empty;
                                    gridData.Unit = unit;
                                    gridData.PreviewFieldValue = "Capacity: " + unit.Capacity + "\tPortable ID: " + unit.IdRadio + "\tDescription: " + unit.Description;
                                    unitSyncBox.Sync(gridData, CommittedDataAction.Update);
                                }

                                unitValues = ((BindingList<GridControlDataUnit>)gridViewUnits.DataSource).Where(unit => unit.Recommended == "No").ToList();

                                foreach (GridControlDataUnit gridData in unitValues)
                                {
                                    UnitClientData unit = gridData.Tag as UnitClientData;
                                    unit.Distance = 0;
                                    gridData.BackColor = unit.Status.Color;
                                    gridData.Recommended = string.Empty;
                                    gridData.Unit = unit;
                                    gridData.PreviewFieldValue = "Capacity: " + unit.Capacity + "\tPortable ID: " + unit.IdRadio + "\tDescription: " + unit.Description;
                                    unitSyncBox.Sync(gridData, CommittedDataAction.Update);
                                }


                            });
                    }


                    FormUtil.InvokeRequired(this, delegate
                    {
                        this.callsAndDispatch.gridControlExTotalDispatchs.ClearData();
                        this.callsAndDispatch.gridControlExTotalCalls.ClearData();
                    });
                    FormUtil.InvokeRequired(this, delegate
                    {
                        this.radioButtonZone.Enabled = false;
                        this.radioButtonDepartment.Enabled = false;
                        this.radioButtonAll.Checked = true;

                    });
                }
                EnableAssignButton();
                FormUtil.InvokeRequired(this, delegate
                {
                    this.gridViewAssignedIncidentNotifications.Focus();
                });
                EnableSupportRequest(selectedIncidentNotification);
            }
            else if (e == null || e.FocusedRowHandle == GridControlEx.AutoFilterRowHandle) 
            {

                FormUtil.InvokeRequired(this, delegate
                {
                    this.radioButtonZone.Enabled = false;
                    this.radioButtonDepartment.Enabled = false;
                    this.radioButtonAll.Checked = true;

                });
            
            }
            //EnableLocationButton(null, EventArgs.Empty);
        }

        private void EnableSupportRequest(IncidentNotificationClientData selectedIncidentNotification)
        {
            if (selectedIncidentNotification != null)
            {
                if (selectedIncidentNotification.DispatchOrders.Count > 0)
                {
                    dispatchOrderSyncBox.Sync(selectedIncidentNotification.DispatchOrders);
                    foreach (DispatchOrderClientData notifData in selectedIncidentNotification.DispatchOrders)
                    {
                        if (notifData.Status.FriendlyName == DispatchOrderStatusClientData.UnitOnScene.FriendlyName && HaveMoreUnitsToRequestSupport(selectedIncidentNotification))
                        {
                            toolStripMenuItemSupportRequest.Enabled = true;
                            barButtonItemSupportRequest.Enabled = true;
                            return;
                        }
                    }
                    barButtonItemSupportRequest.Enabled = false;
                    toolStripMenuItemSupportRequest.Enabled = false;
                }
            }
            else
            {
                barButtonItemSupportRequest.Enabled = false;
                toolStripMenuItemSupportRequest.Enabled = false;
            }
        }

        private void gridView_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            if (e.RowHandle > -1)
            {
                e.Appearance.BackColor = (Color)((GridControlData)(sender as GridViewEx).GetRow(e.RowHandle)).BackColor;
            }
        }

        private void gridViewUnits_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (gridViewUnits.GetSelectedRows().Length > 0)
            {
                GridViewUnitsSelectionChanged();
				if (rightClick == true)
				{
					contextMenuStripUnits.Show(Cursor.Position);
					rightClick = false;
				}
				barButtonItemUnitsFollow.Enabled = true;
            }
			else
				barButtonItemUnitsFollow.Enabled = false;
            contextMenuDispatchOrder_Opening(null, new CancelEventArgs());
            contextMenuStripUnits_Opening(null, new CancelEventArgs());
            previewRowLines();
        }

        private void gridViewDispatchOrder_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (gridViewDispatchOrder.GetSelectedRows().Length > 0)
            {
                ClearSelectionMultipleGrid(this.gridViewUnits);
                FormUtil.InvokeRequired(this,
                    delegate
                    {
                        this.buttonExAssign.Enabled = false;
                    });

                GridViewEx gridView = sender as GridViewEx;

                if (gridView != null && gridControlDispatchOrder.SelectedItems.Count > 0)
                {
                    contextMenuDispatchOrder.Enabled = true;
                    DispatchOrderClientData dispatchOrder = (gridControlDispatchOrder.SelectedItems[0] as GridControlDataDispatchOrder).DispatchOrder;

                    if (rightClick == true)
                    {
                        contextMenuDispatchOrder.Show(Cursor.Position);
                        rightClick = false;
                    }
                }
            }
            contextMenuStripUnits_Opening(null, new CancelEventArgs());
            contextMenuDispatchOrder_Opening(null, new CancelEventArgs());            
        }

        private void GridViewUnitsSelectionChanged()
        {
            previewRowLines();
            ClearSelectionMultipleGrid(this.gridViewDispatchOrder);
            this.buttonExAssign.Enabled = false;

            EnableAssignButton();

            GridViewEx Grid = gridViewUnits;
            if (Grid != null)
            {
                if (gridControlUnits.SelectedItems.Count > 0)
                {
                    this.contextMenuStripUnits.Enabled = true;

                    if (gridControlUnits.SelectedItems.Count == 1)
                    {
                        UnitClientData unit = ((GridControlDataUnit)gridControlUnits.SelectedItems[0]).Tag as UnitClientData;
                        ThreadPool.QueueUserWorkItem(delegate(object state)
                        {
                            try
                            {
                                gridViewUnits_SelectionChanged_Help(unit);
                            }
                            catch (Exception ex)
                            {
                                SmartLogger.Print(ex);
                            }
                        });
                    }
                }
            }
        }

        private void GridView_LostFocus(object sender, EventArgs e)
        {
            FormUtil.InvokeRequired(this,
                delegate
                {
                    if (!gridControlAssignedIncidentNotifications.IsFocused &&
                        !gridControlNewIncidentNotifications.IsFocused &&
                        !gridControlUnits.IsFocused &&
                        !gridControlDispatchOrder.IsFocused)
                    {
                        barButtonItemLocateObject.Enabled = false;
                    }
                    else
                    {
                        barButtonItemLocateObject.Enabled = true;
                    }
                });
        }


        private void GridView_GotFocus(object sender, EventArgs e)
        {
            EnableLocationButton(null, EventArgs.Empty);
        }

        private void GridView_GotFocus2(object sender, EventArgs e)
        {
            EnableLocationButton(null, EventArgs.Empty);
            try
            {
                IncidentNotificationClientData selectedIncidentNotification = null;
                if (this.gridControlAssignedIncidentNotifications.SelectedItems.Count > 0)
                {
                    selectedIncidentNotification =
                        ((GridControlDataAssignedIncidentNotifications)this.gridControlAssignedIncidentNotifications.SelectedItems[0]).Tag as IncidentNotificationClientData;
                }
                EnableSupportRequest(selectedIncidentNotification);
            }
            catch { }
        }
        
        private void ItemRecommendedUnits_ItemClick(object sender, ItemClickEventArgs e)
        {
            string message = string.Empty;
            IncidentNotificationClientData selectedIncidentNotification = null;
            FormUtil.InvokeRequired(this.gridControlAssignedIncidentNotifications,
                delegate
                {
                    if (gridControlAssignedIncidentNotifications.SelectedItems.Count == SINGLE_SELECTION)
                    {
                        selectedIncidentNotification =
                            ((GridControlDataAssignedIncidentNotifications)gridControlAssignedIncidentNotifications.SelectedItems[0]).IncidentNotification;
                    }
                });
            if (selectedIncidentNotification != null)
            {
                if (selectedIncidentNotification.IncidentAddress != null)
                {
                    List<UnitClientData> recommendedUnits = GetRecommendedUnits();
                    if (recommendedUnits.Count > 0)
                    {
                        message = SendUnitsToHighLight(recommendedUnits, HLGisObjectType.UnitsRecommended, selectedIncidentNotification.IncidentAddress, selectedIncidentNotification.IncidentCode);
                    }
                    else
                    {
                        FormUtil.InvokeRequired(this,
                            delegate
                            {
                                if (radioButtonAll.Checked)
                                {
                                    message = ResourceLoader.GetString2("NotRecommendedUnitsAtAll");
                                }
                                else if (radioButtonDepartment.Checked)
                                {
                                    message = ResourceLoader.GetString2("NotRecommendedUnitsByDepartmentStation");
                                }
                                else if (radioButtonZone.Checked)
                                {
                                    message = ResourceLoader.GetString2("NotRecommendedUnitsByZone");
                                }
                            });
                    }
                }
            }
            if (string.IsNullOrEmpty(message) == false)
            {
                MessageForm.Show(message, MessageFormType.Warning);
            }
        }

        private List<UnitClientData> GetRecommendedUnits()
        {
            List<UnitClientData> units = new List<UnitClientData>();
            FormUtil.InvokeRequired(gridControlUnits,
                delegate
                {
                    string yes = ResourceLoader.GetString2("$Message.Yes");
                    for (int i = 0; i < gridViewUnits.RowCount; i++)
                    {
                        GridControlDataUnit gridData = gridViewUnits.GetRow(i) as GridControlDataUnit;
                        if (gridData != null && gridData.Recommended == yes)
                        {
                            units.Add(gridData.Unit);
                        }
                    }                    
                });
            return units;
        }

        private void ItemAsignedUnits_ItemClick(object sender, ItemClickEventArgs e)
        {
            string message = string.Empty;
            IncidentNotificationClientData selectedIncidentNotification = null;
            FormUtil.InvokeRequired(this.gridControlAssignedIncidentNotifications,
                delegate
                {
                    if (gridControlAssignedIncidentNotifications.SelectedItems.Count == SINGLE_SELECTION)
                    {
                        selectedIncidentNotification =
                            ((GridControlDataAssignedIncidentNotifications)gridControlAssignedIncidentNotifications.SelectedItems[0]).IncidentNotification;
                    }
                });
            if (selectedIncidentNotification != null)
            {
                if (selectedIncidentNotification.IncidentAddress != null)
                {
                    if (selectedIncidentNotification.DispatchOrders != null &&
                    selectedIncidentNotification.DispatchOrders.Count > 0)
                    {
                        List<UnitClientData> units = new List<UnitClientData>();
                        foreach (DispatchOrderClientData docd in selectedIncidentNotification.DispatchOrders)
                        {
                            units.Add(docd.Unit);
                        }
                        message = SendUnitsToHighLight(units, HLGisObjectType.UnitsAsigned, selectedIncidentNotification.IncidentAddress,
                            selectedIncidentNotification.IncidentCode);
                    }
                    else
                    {
                        message = ResourceLoader.GetString2("ThereAreNotUnitsAssigned");
                    }
                }                
            }
            if (string.IsNullOrEmpty(message) == false)
            {
                MessageForm.Show(message, MessageFormType.Warning);
            }
        }

        private void HighLightIncidentInMap(IncidentNotificationClientData selectedIncidentNotification)
        {
            if (selectedIncidentNotification != null)
            {
                AddressClientData address = selectedIncidentNotification.IncidentAddress;
                if (address != null && address.Lon != 0 && address.Lat != 0)
                {
                    //From now, we have to inform to map wich object we want
                    //to highlight, because, map have to check if it is visible
                    //or not.
                    HLGisObject hlobj = new HLGisObject();
                    hlobj.HLType = HLGisObjectType.Incident;
                    IncidentClientData incAux = new IncidentClientData();
                    incAux.Code = selectedIncidentNotification.IncidentCode;
                    incAux.Address = new AddressClientData();
                    incAux.Address.Lat = address.Lat;
                    incAux.Address.Lon = address.Lon;
                    incAux.Address.IsSynchronized = true;
                    hlobj.Data = incAux;
                    ApplicationServiceClient.Current(UserApplicationClientData.Map).HighLightObjectInMap(hlobj);
                    
                    
                    //ApplicationServiceClient.Current(UserApplicationClientData.Map).DisplayGeoPointInGis(new GeoPoint(address.Lon, address.Lat));
                }
            }
            
        }

        public void EnableLocationButton(object sender, EventArgs e)
        {
            if (enablingLocationButton == false)
            {
                enablingLocationButton = true;
                bool enableLocationButton = false;
                /*Map must be open, gridcontrol must has focus, gridcontrol mus has one selected item*/
                FormUtil.InvokeRequired(this,
                    delegate
                    {
                        bool mapIsOpen = parentForm.accessToMap;
                        if (mapIsOpen)
                        {                            
                            bool disableAll = true;
                            if (gridControlAssignedIncidentNotifications.SelectedItems.Count == SINGLE_SELECTION && 
                                parentForm.mapIsOpen)
                            {
                                disableAll = false;
                                gridViewDispatchOrder_ColumnFilterChanged(null, null);  // barItemAsignedUnits.Enabled = true;
                                gridViewUnits_ColumnFilterChanged(null, null);          // barItemRecommendedUnits.Enabled = true;
                                
                                if (gridControlAssignedIncidentNotifications.IsFocused)
                                {
                                    enableLocationButton = true;
                                    toolStripMenuItemLocateAssignedIncNot.Enabled = contextMenuStripAssignedIncidentNotif.Enabled;
                                }

                                //toolStripMenuItemAssignedUnits.Enabled = contextMenuStripAssignedIncidentNotif.Enabled;
                                //toolStripMenuItemRecommendedUnits.Enabled = contextMenuStripAssignedIncidentNotif.Enabled;
                            }
                            else
                            {
                                barItemAsignedUnits.Enabled = false;
                                barItemRecommendedUnits.Enabled = false;
                                toolStripMenuItemAssignedUnits.Enabled = false;
                                toolStripMenuItemRecommendedUnits.Enabled = false;
                            }
                            
                            if (gridControlNewIncidentNotifications.SelectedItems.Count == SINGLE_SELECTION && parentForm.mapIsOpen)
                            {
                                disableAll = false;
                                
                                if (gridControlNewIncidentNotifications.IsFocused)
                                {
                                    enableLocationButton = true;
                                    toolStripMenuItemLocateNewIncNot.Enabled = contextMenuStripNewIncidentNotification.Enabled;
                                }
                            }
                            else
                            {
                                toolStripMenuItemLocateNewIncNot.Enabled = false;
                            }

                            if (gridControlUnits.SelectedItems.Count == SINGLE_SELECTION && parentForm.mapIsOpen)
                            {
                                disableAll = false;
                                
                                if (gridControlUnits.IsFocused)
                                {
                                    enableLocationButton = true;
                                }
                                toolStripMenuItemLocateUnit.Enabled = contextMenuStripUnits.Enabled;
                            }
                            else
                            {
                                toolStripMenuItemLocateUnit.Enabled = false;
                            }

                            if (gridControlDispatchOrder.SelectedItems.Count == SINGLE_SELECTION && parentForm.mapIsOpen)
                            {
                                disableAll = false;
                                
                                if (gridControlDispatchOrder.IsFocused)
                                {
                                    enableLocationButton = true;
                                }
                                toolStripMenuItemLocateUnitDispatch.Enabled = contextMenuDispatchOrder.Enabled;
                            }
                            else
                            {
                                toolStripMenuItemLocateUnitDispatch.Enabled = false;
                            }

                            barButtonItemLocateObject.Enabled = enableLocationButton;
                            
                            if (disableAll)
                            {
                                barButtonItemLocateObject.Enabled = false;
                                barItemAsignedUnits.Enabled = false;
                                barItemRecommendedUnits.Enabled = false;
                                toolStripMenuItemLocateNewIncNot.Enabled = false;
                                toolStripMenuItemLocateAssignedIncNot.Enabled = false;
                                toolStripMenuItemLocateUnit.Enabled = false;
                                toolStripMenuItemLocateUnitDispatch.Enabled = false;
                                toolStripMenuItemAssignedUnits.Enabled = false;
                                toolStripMenuItemRecommendedUnits.Enabled = false;
                            }
                        }
                        else
                        {
                            barButtonItemLocateObject.Enabled = false;
                            //barItemAsignedUnits.Enabled = false;
                            //barItemRecommendedUnits.Enabled = true;
                            toolStripMenuItemLocateNewIncNot.Enabled = false;
                            toolStripMenuItemLocateAssignedIncNot.Enabled = false;
                            toolStripMenuItemLocateUnit.Enabled = false;
                            toolStripMenuItemLocateUnitDispatch.Enabled = false;
                            toolStripMenuItemAssignedUnits.Enabled = false;
                            toolStripMenuItemRecommendedUnits.Enabled = false;
                        }
                    });

                enablingLocationButton = false;
            }
        }

        private void barButtonItemLocateObject_ItemClick(object sender, ItemClickEventArgs e)
        {
            FormUtil.InvokeRequired(this,
                delegate
                {
                    try
                    {
                        parentForm.EnableMapButton(null, null);
                        if (parentForm.mapIsOpen == false)
                        { MessageForm.Show(ResourceLoader.GetString2("MapsModuleNotOpen"), MessageFormType.Warning); return; }
                        if (gridControlAssignedIncidentNotifications.IsFocused)
                        {
                            IList list = gridControlAssignedIncidentNotifications.SelectedItems;
                            if (list.Count > 0)
                            {
                                IncidentNotificationClientData selectedIncidentNotification =
                                    (list[0] as GridControlDataAssignedIncidentNotifications).IncidentNotification;
                                if (selectedIncidentNotification.IncidentAddress != null &&
                                    selectedIncidentNotification.IncidentAddress.Lat != 0 &&
                                    selectedIncidentNotification.IncidentAddress.Lon != 0)
                                {
                                    HighLightIncidentInMap(selectedIncidentNotification);
                                }
                                else
                                {
                                    MessageForm.Show(ResourceLoader.GetString2("IncidentCanNotBeFoundInMap"), MessageFormType.Warning);                
                                }
                            }
                        }
                        else if (gridControlNewIncidentNotifications.IsFocused)
                        {
                            IList list = gridControlNewIncidentNotifications.SelectedItems;
                            if (list.Count > 0)
                            {
                                IncidentNotificationClientData selectedIncidentNotification =
                                    (list[0] as GridControlDataNewIncidentNotifications).IncidentNotification;
                                if (selectedIncidentNotification.IncidentAddress != null &&
                                    selectedIncidentNotification.IncidentAddress.Lat != 0 &&
                                    selectedIncidentNotification.IncidentAddress.Lon != 0)
                                {
                                    HighLightIncidentInMap(selectedIncidentNotification);
                                }
                                else
                                {
                                    MessageForm.Show(ResourceLoader.GetString2("IncidentCanNotBeFoundInMap"), MessageFormType.Warning);
                                }
                            }
                        }
                        else if (gridControlUnits.IsFocused)
                        {
                            IList list = gridControlUnits.SelectedItems;
                            if (list.Count == SINGLE_SELECTION)
                            {
                                UnitClientData unit = (list[0] as GridControlDataUnit).Unit;
                                if (unit != null && unit.IsSynchronized)
                                {
                                    ApplicationServiceClient.Current(UserApplicationClientData.Map).DisplayGeoPointInGis(new GeoPoint(unit.Lon, unit.Lat));
                                }
                                else
                                {
                                    MessageForm.Show(ResourceLoader.GetString2("UnitCanNotBeFoundInMap"), MessageFormType.Warning);
                                }
                            }
                        }
                        else if (gridControlDispatchOrder.IsFocused)
                        {
                            IList list = gridControlDispatchOrder.SelectedItems;
                            if (list.Count == SINGLE_SELECTION)
                            {
                                UnitClientData unit = (list[0] as GridControlDataDispatchOrder).DispatchOrder.Unit;
                                if (unit != null && unit.IsSynchronized)
                                {
                                    ApplicationServiceClient.Current(UserApplicationClientData.Map).DisplayGeoPointInGis(new GeoPoint(unit.Lon, unit.Lat));
                                }
                                else
                                {
                                    MessageForm.Show(ResourceLoader.GetString2("UnitCanNotBeFoundInMap"), MessageFormType.Warning);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        SmartLogger.Print(ex);
                    }
                });
        }

        private void toolStripMenuItemLocateObject_Click(object sender, EventArgs e)
        {
            barButtonItemLocateObject_ItemClick(null, null);
        }

        private void toolStripMenuItemRecommendedUnits_Click(object sender, EventArgs e)
        {
            ItemRecommendedUnits_ItemClick(null, null);
        }

        private void toolStripMenuItemAssignedUnits_Click(object sender, EventArgs e)
        {
            ItemAsignedUnits_ItemClick(null, null);
        }

		private void barButtonItemUnitsFollow_ItemClick(object sender, ItemClickEventArgs e)
		{
            bool maxAccept = false;
            if (unitFollowRibbonForm == null || unitFollowRibbonForm.IsDisposed == true)
            {
                if (gridControlUnits.SelectedItems.Count > MAX_UNITS_TO_FOLLOW)
                {
                    DialogResult MaxResult = MessageForm.Show(ResourceLoader.GetString2("OpenWithUnitFollowMax"), MessageFormType.Information);
                    if (MaxResult == DialogResult.OK)
                    {
                        maxAccept = true;
                    }
                    else
                        return;
                }
                 
                unitFollowRibbonForm = new UnitFollowRibbonForm();
            }
            BackgroundProcessForm processForm = new BackgroundProcessForm(this, new MethodInfo[] {
                GetType().GetMethod("OpenUnitFollow_Help", BindingFlags.NonPublic | BindingFlags.Instance) }, new object[][] { new object[0] });
            processForm.CanThrowError = true;
            processForm.ShowDialog();

			
		}
        private void OpenUnitFollow_Help()
        {
            unitFollowRibbonForm.GlobalApplicationPreference = globalApplicationPreferences;
            unitFollowRibbonForm.Activate();
            unitFollowRibbonForm.Show();

            foreach (GridControlDataUnit item in gridControlUnits.SelectedItems)
            {
                if (unitFollowRibbonForm.sendingUnits.Count < MAX_UNITS_TO_FOLLOW)
                {
                                       
                    if (unitFollowRibbonForm.sendingUnits.Contains(item.Unit.Code) == false)
                    {
                        if (item.Unit.Lon == 0.0)
                        {
                            MessageForm.Show(ResourceLoader.GetString2("IncidentCanNotBeFoundInMap"), MessageFormType.Warning);                
                            return;
                        }
                        else
                        {
                            unitFollowRibbonForm.sendingUnits.Add(item.Unit.Code);

                            UnitFollowForm form = new UnitFollowForm(DynTableTempName, DynTableTracksName);
                            form.MdiParent = unitFollowRibbonForm;
                            form.Unit = item.Unit;
                            form.Show();
                        }
                    }
                }
                else
                {
                    MessageForm.Show(ResourceLoader.GetString2("UnitFollowMax"), MessageFormType.Information);
                    break;
                }
            }
        }

        private void gridViewUnits_ColumnFilterChanged(object sender, EventArgs e)
        {
            
            if (parentForm.mapIsOpen)
            {
                if (gridViewUnits.DataSource != null)
                {
                    List<GridControlDataUnit> units = gridViewUnits.GetVisibleRows().Cast<GridControlDataUnit>().Where<GridControlDataUnit>(
                         unit => unit.Recommended == ResourceLoader.GetString2("$Message.Yes")).ToList();
                    
                    if (units.Count > 0)
                    {
                        barItemRecommendedUnits.Enabled = true;
                    }
                    else
                    {
                        barItemRecommendedUnits.Enabled = false;
                    }
                }
            }
        }

        private void gridViewDispatchOrder_ColumnFilterChanged(object sender, EventArgs e)
        {    
            if (parentForm.mapIsOpen)
            {
                IList units = gridViewDispatchOrder.GetVisibleRows();
                if (units.Count > 0)
                {
                    barItemAsignedUnits.Enabled = true;
                }
                else
                {
                    barItemAsignedUnits.Enabled = false;
                }
            }
        }

        private void gridViewUnits_FocusedRow(object sender, DevExpress.XtraGrid.Views.Base.RowEventArgs e)
        {

        }

        private void DefaultDispatchFormDevX_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && !(e.Modifiers == (Keys.Control | Keys.Alt)))
            {
                if (e.KeyValue == 49 || e.KeyCode == Keys.NumPad1)
                {
                    FocusSection(1);
                }
                else if (e.KeyValue == 50 || e.KeyCode == Keys.NumPad2)
                {
                    FocusSection(2);
                }
                else if (e.KeyValue == 51 || e.KeyCode == Keys.NumPad3)
                {
                    FocusSection(3);
                }
                else if (e.KeyValue == 52 || e.KeyCode == Keys.NumPad4)
                {
                    FocusSection(4);
                }
                else if (e.KeyValue == 53 || e.KeyCode == Keys.NumPad5)
                {
                    FocusSection(5);
                }
                else if (e.KeyValue == 54 || e.KeyCode == Keys.NumPad6)
                {
                    FocusSection(6);
                }
                else if (e.KeyValue == 55 || e.KeyCode == Keys.NumPad7)
                {
                    FocusSection(7);
                }
                else if (e.KeyValue == 56 || e.KeyCode == Keys.NumPad8)
                {
                    FocusSection(8);
                }
            } 
            else if (e.KeyCode == Keys.Tab)
            {
                if (gridControlNewIncidentNotifications.Focused)
                {
                    FocusSection(2);
                }
                else if (gridControlAssignedIncidentNotifications.Focused)
                {
                    FocusSection(3);
                }
                else if (this.textBoxIncidentDetails.Focused)
                {
                    FocusSection(4);
                }
                else if (gridControlUnits.Focused)
                {
                    FocusSection(5);
                }
                else if (gridControlDispatchOrder.Focused)
                {
                    FocusSection(6);
                }
                else if (this.textBoxIncidentNotes.Focused)
                {
                    FocusSection(7);
                }
                else if (callsAndDispatch.Focused)
                {
                    FocusSection(1);
                }
                else if (callsAndDispatch.gridControlExTotalCalls.Focused)
                {
                    FocusSection(8);
                }
                else if (callsAndDispatch.gridControlExTotalDispatchs.Focused)
                {
                    FocusSection(1);
                }
                else if (callsAndDispatch.richTextBoxPhoneReportDetails.Focused)
                {
                    FocusSection(1);
                }
                else
                {
                    FocusSection(1);
                }
            }
        }

        private void FocusSection(int section)
        {
            switch (section)
            {
                case 1:
                {
                    gridControlNewIncidentNotifications.Focus();

                    if (gridControlNewIncidentNotifications.SelectedItems.Count == 0 &&
                        gridControlNewIncidentNotifications.Items.Count > 0)
                    {
                        gridControlNewIncidentNotifications.SelectData(
                            gridControlNewIncidentNotifications.Items[0] as GridControlData);
                    }

                    break;
                }

                case 2:
                {
                    gridControlAssignedIncidentNotifications.Focus();

                    if (gridControlAssignedIncidentNotifications.SelectedItems.Count == 0 &&
                        gridControlAssignedIncidentNotifications.Items.Count > 0)
                    {
                        gridControlAssignedIncidentNotifications.SelectData(
                            gridControlAssignedIncidentNotifications.Items[0] as GridControlData);
                    }

                    break;
                }

                case 3:
                {
                    this.textBoxIncidentDetails.Focus();
                    break;
                }

                case 4:
                {
                    gridControlUnits.Focus();

                    if (gridControlUnits.SelectedItems.Count == 0 && gridControlUnits.Items.Count > 0)
                    {
                        gridControlUnits.SelectData(gridControlUnits.Items[0] as GridControlData);
                    }

                    break;
                }

                case 5:
                {
                    gridControlDispatchOrder.Focus();

                    if (gridControlDispatchOrder.SelectedItems.Count == 0 &&
                        gridControlDispatchOrder.Items.Count > 0)
                    {
                        gridControlDispatchOrder.SelectData(gridControlDispatchOrder.Items[0] as GridControlData);
                    }

                    break;
                }

                case 6:
                {
                    this.textBoxIncidentNotes.Focus();
                    break;
                }

                case 7:
                {
                    callsAndDispatch.ChangeTabPage(0);
                    callsAndDispatch.gridControlExTotalCalls.Focus();

                    if (callsAndDispatch.gridControlExTotalCalls.SelectedItems.Count == 0 &&
                        callsAndDispatch.gridControlExTotalCalls.Items.Count > 0)
                    {
                        callsAndDispatch.gridControlExTotalCalls.SelectData(
                            callsAndDispatch.gridControlExTotalCalls.Items[0] as GridControlData);
                    }

                    break;
                }

                case 8:
                {
                    callsAndDispatch.ChangeTabPage(1);
                    callsAndDispatch.gridControlExTotalDispatchs.Focus();

                    if (callsAndDispatch.gridControlExTotalDispatchs.SelectedItems.Count == 0 &&
                        callsAndDispatch.gridControlExTotalDispatchs.Items.Count > 0)
                    {
                        callsAndDispatch.gridControlExTotalDispatchs.SelectData(
                            callsAndDispatch.gridControlExTotalDispatchs.Items[0] as GridControlData);
                    }

                    break;
                }
            }
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == (Keys.Control | Keys.Tab))
            {
                if (gridControlAssignedIncidentNotifications.Focused)
                {
                    if (buttonActiveDispatch.Focused)
                    {
                        buttonClosedDispatch.Focus();
                    }
                    else if (buttonClosedDispatch.Focused)
                    {
                        blinkingButtonExDispReqAttention.Focus();
                    }
                    else if (blinkingButtonExDispReqAttention.Focused)
                    {
                        blinkingButtonExPendingDispatch.Focus();
                    }
                    else if (blinkingButtonExPendingDispatch.Focused)
                    {
                        buttonActiveDispatch.Focus();
                    }
                    else
                    {
                        buttonActiveDispatch.Focus();
                    }
                }
                else if (gridControlUnits.Focused)
                {
                    if (buttonAllUnits.Focused)
                    {
                        buttonAvailableUnits.Focus();
                    }
                    else if (buttonAvailableUnits.Focused)
                    {
                        buttonAssignedUnits.Focus();
                    }
                    else if (buttonAssignedUnits.Focused)
                    {
                        buttonReqAttention.Focus();
                    }
                    else if (buttonReqAttention.Focused)
                    {
                        buttonAllUnits.Focus();
                    }
                    else
                    {
                        buttonAllUnits.Focus();
                    }
                }
                else if (buttonActiveDispatch.Focused)
                {
                    buttonClosedDispatch.Focus();
                }
                else if (buttonClosedDispatch.Focused)
                {
                    blinkingButtonExDispReqAttention.Focus();
                }
                else if (blinkingButtonExDispReqAttention.Focused)
                {
                    blinkingButtonExPendingDispatch.Focus();
                }
                else if (blinkingButtonExPendingDispatch.Focused)
                {
                    buttonActiveDispatch.Focus();
                }
                else if (buttonAllUnits.Focused)
                {
                    buttonAvailableUnits.Focus();
                }
                else if (buttonAvailableUnits.Focused)
                {
                    buttonAssignedUnits.Focus();
                }
                else if (buttonAssignedUnits.Focused)
                {
                    buttonReqAttention.Focus();
                }
                else if (buttonReqAttention.Focused)
                {
                    buttonAllUnits.Focus();
                }

                return true;
            }
            else if (keyData == Keys.Tab)
            {
                if (callsAndDispatch.gridControlExTotalCalls.Focused)
                {
                    FocusSection(8);
                }
                else if (callsAndDispatch.gridControlExTotalDispatchs.Focused)
                {
                    FocusSection(1);
                }
                else if (callsAndDispatch.richTextBoxPhoneReportDetails.Focused)
                {
                    FocusSection(1);
                }

                return base.ProcessCmdKey(ref msg, keyData);
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

       
    }

    


	   
}
