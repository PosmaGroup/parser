﻿using SmartCadControls.Controls;

namespace Smartmatic.SmartCad.Telephony.SIP
{
    partial class SIPConfigurationControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControlSeparatorMain = new DevExpress.XtraEditors.LabelControl();
            this.labelExServerIP = new LabelEx();
            this.textBoxExServerIP = new TextBoxEx();
            this.textBoxExServerPort = new TextBoxEx();
            this.groupControlTelephony = new DevExpress.XtraEditors.GroupControl();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlTelephony)).BeginInit();
            this.groupControlTelephony.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelControlSeparatorMain
            // 
            this.labelControlSeparatorMain.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControlSeparatorMain.Location = new System.Drawing.Point(282, 36);
            this.labelControlSeparatorMain.Name = "labelControlSeparatorMain";
            this.labelControlSeparatorMain.Size = new System.Drawing.Size(5, 16);
            this.labelControlSeparatorMain.TabIndex = 2;
            this.labelControlSeparatorMain.Text = ":";
            // 
            // labelExServerIP
            // 
            this.labelExServerIP.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExServerIP.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExServerIP.Location = new System.Drawing.Point(8, 38);
            this.labelExServerIP.Name = "labelExServerIP";
            this.labelExServerIP.Size = new System.Drawing.Size(64, 16);
            this.labelExServerIP.TabIndex = 0;
            this.labelExServerIP.Text = "ServerIp";
            // 
            // textBoxExServerIP
            // 
            this.textBoxExServerIP.AllowsLetters = true;
            this.textBoxExServerIP.AllowsNumbers = true;
            this.textBoxExServerIP.AllowsPunctuation = true;
            this.textBoxExServerIP.AllowsSeparators = false;
            this.textBoxExServerIP.AllowsSymbols = false;
            this.textBoxExServerIP.AllowsWhiteSpaces = false;
            this.textBoxExServerIP.ExtraAllowedChars = "";
            this.textBoxExServerIP.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExServerIP.Location = new System.Drawing.Point(128, 35);
            this.textBoxExServerIP.MaxLength = 15;
            this.textBoxExServerIP.Name = "textBoxExServerIP";
            this.textBoxExServerIP.NonAllowedCharacters = "\'";
            this.textBoxExServerIP.RegularExpresion = "";
            this.textBoxExServerIP.Size = new System.Drawing.Size(148, 20);
            this.textBoxExServerIP.TabIndex = 1;
            this.textBoxExServerIP.TextChanged += new System.EventHandler(this.textBoxEx_TextChanged);
            // 
            // textBoxExServerPort
            // 
            this.textBoxExServerPort.AllowsLetters = false;
            this.textBoxExServerPort.AllowsNumbers = true;
            this.textBoxExServerPort.AllowsPunctuation = true;
            this.textBoxExServerPort.AllowsSeparators = false;
            this.textBoxExServerPort.AllowsSymbols = false;
            this.textBoxExServerPort.AllowsWhiteSpaces = false;
            this.textBoxExServerPort.ExtraAllowedChars = "";
            this.textBoxExServerPort.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExServerPort.Location = new System.Drawing.Point(292, 35);
            this.textBoxExServerPort.MaxLength = 15;
            this.textBoxExServerPort.Name = "textBoxExServerPort";
            this.textBoxExServerPort.NonAllowedCharacters = "\'";
            this.textBoxExServerPort.RegularExpresion = "";
            this.textBoxExServerPort.Size = new System.Drawing.Size(68, 20);
            this.textBoxExServerPort.TabIndex = 3;
            this.textBoxExServerPort.TextChanged += new System.EventHandler(this.textBoxEx_TextChanged);
            // 
            // groupControlTelephony
            // 
            this.groupControlTelephony.Controls.Add(this.labelControlSeparatorMain);
            this.groupControlTelephony.Controls.Add(this.labelExServerIP);
            this.groupControlTelephony.Controls.Add(this.textBoxExServerPort);
            this.groupControlTelephony.Controls.Add(this.textBoxExServerIP);
            this.groupControlTelephony.Location = new System.Drawing.Point(0, 3);
            this.groupControlTelephony.Name = "groupControlTelephony";
            this.groupControlTelephony.Size = new System.Drawing.Size(375, 73);
            this.groupControlTelephony.TabIndex = 9;
            this.groupControlTelephony.Text = "groupControlTelephony";
            // 
            // SIPConfigurationControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupControlTelephony);
            this.Name = "SIPConfigurationControl";
            this.Size = new System.Drawing.Size(375, 79);
            ((System.ComponentModel.ISupportInitialize)(this.groupControlTelephony)).EndInit();
            this.groupControlTelephony.ResumeLayout(false);
            this.groupControlTelephony.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControlSeparatorMain;
        private LabelEx labelExServerIP;
        internal TextBoxEx textBoxExServerIP;
        internal TextBoxEx textBoxExServerPort;
        private DevExpress.XtraEditors.GroupControl groupControlTelephony;

    }
}
