﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Smartmatic.SmartCad.Telephony.SIP
{
    public static class SIPProperties
    {
        public const string MAIN_SERVER_IP = "SERVER_IP";
        public const string MAIN_SERVER_PORT = "SERVER_PORT";
    }
}
