using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using Smartmatic.SmartCad.Telephony.SIP;


namespace PortSIP
{
    unsafe class PortSIPCore
    {
        private static TelephonyManagerSIP Manager { get; set; }

        public PortSIPCore(Int32 callbackObject, TelephonyManagerSIP manager)
        {
            _callbackHandle = IntPtr.Zero;
            _SIPCoreHandle = IntPtr.Zero;
            _callbackObject = callbackObject;

            Manager = manager;
        }

        //////////////////////////////////////////////////////////////////////////
        //
        //  !!!IMPORTANT!!! DON'T EDIT ALL BELOW SOURCE CODE  
        //
        //////////////////////////////////////////////////////////////////////////


        public Boolean createCallbackHandlers() // This must called before initialize
        {
            if (_callbackHandle != IntPtr.Zero)
            {
                return false;
            }

            if (createSIPCallbackHandle() == false)
            {
                return false;
            }

            setCallbackHandlers();

            return true;

        }

        public void releaseCallbackHandlers() // This must called after unInitialize
        {
            if (_callbackHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.SIPEV_releaseSIPCallbackHandle(_callbackHandle);
            _callbackHandle = IntPtr.Zero;
        }




        public Boolean initialize(TRANSPORT_TYPE transportType,
                                          PORTSIP_LOG_LEVEL appLogLevel,
                                          Int32 maximumLines,
                                          String userName,
                                          String displayName,
                                          String authName,
                                          String password,
                                          String agent,
                                          String localIp,
                                          Int32 localSipPort,
                                          String userDomain,
                                          String proxyServer,
                                          Int32 proxyServerPort,
                                          String outboundServer,
                                          Int32 outboundServerPort,
                                          String STUNSever,
                                          Int32 STUNServerPort)
        {
            if (_callbackHandle == IntPtr.Zero)
            {
                return false;
            }

            if (_SIPCoreHandle != IntPtr.Zero)
            {
                return false;
            }

            _SIPCoreHandle = PortSIP_NativeMethods.PortSIP_initialize(_callbackHandle,
                                                           (Int32)transportType,
                                                           (Int32)appLogLevel,
                                                           maximumLines,
                                                           userName,
                                                           displayName,
                                                           authName,
                                                           password,
                                                           agent,
                                                           localIp,
                                                           localSipPort,
                                                           userDomain,
                                                           proxyServer,
                                                           proxyServerPort,
                                                           outboundServer,
                                                           outboundServerPort,
                                                           STUNSever,
                                                           STUNServerPort);

            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return false;
            }

            return true;
        }



        public void unInitialize()
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_unInitialize(_SIPCoreHandle);
            _SIPCoreHandle = IntPtr.Zero;
        }


        public Boolean setRtpPortRange(Int32 minimumRtpAudioPort, Int32 maximumRtpAudioPort, Int32 minimumRtpVideoPort, Int32 maximumRtpVideoPort)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return false;
            }

            return PortSIP_NativeMethods.PortSIP_setRtpPortRange(_SIPCoreHandle,
                                                         minimumRtpAudioPort,
                                                         maximumRtpAudioPort,
                                                         minimumRtpVideoPort,
                                                         maximumRtpVideoPort);
        }



        public void setSrtpPolicy(SRTP_POLICY srtPolicy)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_setSrtpPolicy(_SIPCoreHandle, (Int32)srtPolicy);
        }


        public Boolean registerServer(Int32 expires)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return false;
            }

            return PortSIP_NativeMethods.PortSIP_registerServer(_SIPCoreHandle, expires);
        }


        public void unRegisterServer()
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_unRegisterServer(_SIPCoreHandle);
        }



        public void setVideoResolution(VIDEO_RESOLUTION resolution)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_setVideoResolution(_SIPCoreHandle, (Int32)resolution);
        }


        public void setVideoImagePos(IntPtr localVideoWindow,
                                             Int32 localVideoAreaLeft,
                                             Int32 localVideoAreaTop,
                                             Int32 localVideoAreaWidth,
                                             Int32 localVideoAreaHeight,
                                             IntPtr remoteVideoWindow,
                                             Int32 remoteVideoAreaLeft,
                                             Int32 remoteVideoAreaTop,
                                             Int32 remoteVideoAreaWidth,
                                             Int32 remoteVideoAreaHeight)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_setVideoImagePos(_SIPCoreHandle,
                                                    localVideoWindow,
                                                    localVideoAreaLeft,
                                                    localVideoAreaTop,
                                                    localVideoAreaWidth,
                                                    localVideoAreaHeight,
                                                    remoteVideoWindow,
                                                    remoteVideoAreaLeft,
                                                    remoteVideoAreaTop,
                                                    remoteVideoAreaWidth,
                                                    remoteVideoAreaHeight);

        }


        public void reverseLocalViewVideo(Boolean state)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_reverseLocalViewVideo(_SIPCoreHandle, state);
        }



        public void reverseReceivedVideo(Boolean state)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_reverseReceivedVideo(_SIPCoreHandle, state);
        }

 
        public void reverseSendingVideo(Boolean state)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_reverseSendingVideo(_SIPCoreHandle, state);
        }


        public void addAudioCodec(AUDIOCODEC_TYPE codecType)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_addAudioCodec(_SIPCoreHandle, (Int32)codecType);
        }


        public void addVideoCodec(VIDEOCODEC_TYPE codecType)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_addVideoCodec(_SIPCoreHandle, (Int32)codecType);
        }


        public void clearAudioCodec()
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_clearAudioCodec(_SIPCoreHandle);
        }


        public void clearVideoCodec()
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_clearVideoCodec(_SIPCoreHandle);
        }




        public Boolean isAudioCodecEmpty()
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return true;
            }

            return PortSIP_NativeMethods.PortSIP_isAudioCodecEmpty(_SIPCoreHandle);
        }



        public Boolean isVideoCodecEmpty()
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return true;
            }

            return PortSIP_NativeMethods.PortSIP_isVideoCodecEmpty(_SIPCoreHandle);
        }


        public Int32 call(String callTo, Boolean sendSdp)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return -1;
            }

            return PortSIP_NativeMethods.PortSIP_call(_SIPCoreHandle, callTo, sendSdp);

        }


        public void rejectCall(Int32 sessionId, int code, String reason)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_rejectCall(_SIPCoreHandle, sessionId, code, reason);
        }



        public Boolean answerCall(Int32 sessionId)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return false;
            }

            return PortSIP_NativeMethods.PortSIP_answerCall(_SIPCoreHandle, sessionId);

        }

 
        public void terminateCall(Int32 sessionId)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_terminateCall(_SIPCoreHandle, sessionId);
        }


        public Boolean forwardCall(Int32 sessionId, String forwardTo)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return false;
            }

            return PortSIP_NativeMethods.PortSIP_forwardCall(_SIPCoreHandle, sessionId, forwardTo);

        }


        public Boolean updateInvite(Int32 sessionId)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return false;
            }

            return PortSIP_NativeMethods.PortSIP_updateInvite(_SIPCoreHandle, sessionId);

        }


        public Boolean hold(Int32 sessionId)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return false;
            }

            return PortSIP_NativeMethods.PortSIP_hold(_SIPCoreHandle, sessionId);           
        }


        public Boolean unHold(Int32 sessionId)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return false;
            }

            return PortSIP_NativeMethods.PortSIP_unHold(_SIPCoreHandle, sessionId);         
        }



        public Boolean refer(Int32 sessionId, String referTo)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return false;
            }

            return PortSIP_NativeMethods.PortSIP_refer(_SIPCoreHandle, sessionId, referTo);  
        }


        public Boolean attendedRefer(Int32 sessionId, Int32 replaceSessionId, String referTo)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return false;
            }

            return PortSIP_NativeMethods.PortSIP_attendedRefer(_SIPCoreHandle, sessionId, replaceSessionId, referTo);  
        }



        public Boolean enableSessionTimer(Int32 timerSeconds)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return false;
            }

            return PortSIP_NativeMethods.PortSIP_enableSessionTimer(_SIPCoreHandle, timerSeconds);  
        }



        public void disableSessionTimer()
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_disableSessionTimer(_SIPCoreHandle);  
        }



        public void setAudioDeviceId(Int32 inputDeviceId, Int32 outputDeviceId)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_setAudioDeviceId(_SIPCoreHandle, inputDeviceId, outputDeviceId);  
        }


        public void setVideoDeviceId(Int32 deviceId)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_setVideoDeviceId(_SIPCoreHandle, deviceId);  
        }



        public void setAudioRecordPathName(String recordFilePath, 
                                            String recordFileName, 
                                            Boolean appendTimestamp, 
                                            AUDIO_RECORDING_FILEFORMAT fileFormat)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_setAudioRecordPathName(_SIPCoreHandle, recordFilePath, recordFileName, appendTimestamp, (Int32)fileFormat);  
        }



        public void setVideoRecordPathName(String recordFilePath, 
                                            String recordFileName, 
                                            Boolean appendTimestamp, 
                                            VIDEO_RECORDING_MODE recordMode)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_setVideoRecordPathName(_SIPCoreHandle, recordFilePath, recordFileName, appendTimestamp, (Int32)recordMode);  
        }



        public void setDtmfSamples(Int32 samples)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_setDTMFSamples(_SIPCoreHandle, samples);  
        }



        public void enableDTMFOfRFC2833(Int32 RTPPayloadOf2833)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_enableDTMFOfRFC2833(_SIPCoreHandle, RTPPayloadOf2833); 
        }

        public void enableDTMFOfInfo()
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_enableDTMFOfInfo(_SIPCoreHandle); 
        }


        public void sendDtmf(Int32 sessionId, Char code)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_sendDTMF(_SIPCoreHandle, sessionId, code); 
        }


        public void startAudioRecording()
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_startAudioRecording(_SIPCoreHandle); 
        }


        public void startAudioRecordingOnLine(Int32 sessionId)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_startAudioRecordingOnLine(_SIPCoreHandle, sessionId);
        }


        public void stopAudioRecording()
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_stopAudioRecording(_SIPCoreHandle); 
        }


        public void stopAudioRecordingOnLine(Int32 sessionId)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_stopAudioRecordingOnLine(_SIPCoreHandle, sessionId);
        }


        public void startVideoRecording()
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_startVideoRecording(_SIPCoreHandle); 
        }


        public void stopVideoRecording()
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_stopVideoRecording(_SIPCoreHandle); 
        }



        
        public void enableCallForwarding(Boolean forBusyOnly, String forwardingTo)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_enableCallForwarding(_SIPCoreHandle, forBusyOnly, forwardingTo); 
        }


        public void disableCallForwarding()
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_disableCallForwarding(_SIPCoreHandle); 
        }


        public void enableAEC(Boolean state)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_enableAEC(_SIPCoreHandle, state); 
        }


        public void enableVAD(Boolean state)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_enableVAD(_SIPCoreHandle, state); 
        }

        public void enableCNG(Boolean state)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_enableCNG(_SIPCoreHandle, state); 
        }

        public void enableAGC(Boolean state)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_enableAGC(_SIPCoreHandle, state); 
        }


        public void viewLocalVideo(Boolean state)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_viewLocalVideo(_SIPCoreHandle, state); 

        }


        public void displayMixVideo(Boolean state)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_displayMixVideo(_SIPCoreHandle, state); 
        }



        public Boolean sendVideoStream(Int32 sessionId, Boolean state)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return false;
            }

            return PortSIP_NativeMethods.PortSIP_sendVideoStream(_SIPCoreHandle, sessionId, state); 
        }


        public void setLicenseKey(String key)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_setLicenseKey(_SIPCoreHandle, key); 
        }


        public void muteMicrophone(Boolean mute)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_muteMicrophone(_SIPCoreHandle, mute); 
        }


        public void muteSpeaker(Boolean mute)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_muteSpeaker(_SIPCoreHandle, mute); 
        }

        /// <summary>
        /// Note: Must special the StringBuilder.Length when call this function.
        /// for example:
        /// StringBuilder value = new StringBuilder();
        /// value.Length = 512;
        /// getExtensionHeaderValue(message, name, value);
        /// </summary>
        public Boolean getExtensionHeaderValue(String sipMessage, String headerName, StringBuilder headerValue, Int32 headerValueLength)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return false;
            }

            return PortSIP_NativeMethods.PortSIP_getExtensionHeaderValue(_SIPCoreHandle, sipMessage, headerName, headerValue, headerValueLength); 
        }



        public void addExtensionHeader(String headerName, String headerValue)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_addExtensionHeader(_SIPCoreHandle, headerName, headerValue); 
        }


        public void clearAddExtensionHeaders()
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_clearAddExtensionHeaders(_SIPCoreHandle); 
        }


        public void modifyHeaderValue(String headerName, String headerValue)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_modifyHeaderValue(_SIPCoreHandle, headerName, headerValue); 
        }



        public void clearModifyHeaders(IntPtr SIPCoreHandle)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_clearModifyHeaders(_SIPCoreHandle); 
        }

        public void setVideoQuality(VIDEO_QUALITY quality)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_setVideoQuality(_SIPCoreHandle, (Int32)quality); 
        }





        public void setVideoFrameRate(Int32 frameRate)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_setVideoFrameRate(_SIPCoreHandle,frameRate); 
        }


        public void enableCabacOfH264(Boolean state)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_enableCabacOfH264(_SIPCoreHandle, state); 
        }



        public void setPlayWaveFileToRemote(String waveFile, Boolean enableState, Int32 playMode, Boolean eventReport)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            if (eventReport == true)
            {
                PortSIP_NativeMethods.PortSIP_setPlayWaveFileToRemote(_SIPCoreHandle, waveFile, enableState, playMode, _SIPCoreHandle, _pwf); 
            }
            else
            {
                PortSIP_NativeMethods.PortSIP_setPlayWaveFileToRemote(_SIPCoreHandle, waveFile, enableState, playMode, _SIPCoreHandle, null); 
            }
        }


        public void setPlayWaveFileToRemoteOnLine(Int32 sessionId, String waveFile, Boolean enableState, Int32 playMode, Boolean eventReport)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            if (eventReport == true)
            {
                PortSIP_NativeMethods.PortSIP_setPlayWaveFileToRemoteOnLine(_SIPCoreHandle, sessionId, waveFile, enableState, playMode, _SIPCoreHandle, _pwf);
            }
            else
            {
                PortSIP_NativeMethods.PortSIP_setPlayWaveFileToRemoteOnLine(_SIPCoreHandle, sessionId, waveFile, enableState, playMode, _SIPCoreHandle, null);
            }
        }



        public void setPlayAVIFileToRemote(String aviFile, Boolean enableState, Int32 playMode, Boolean eventReport)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            if (eventReport == true)
            {
                PortSIP_NativeMethods.PortSIP_setPlayAVIFileToRemote(_SIPCoreHandle, aviFile, enableState, playMode, _SIPCoreHandle, _paf); 
            }
            else
            {
                PortSIP_NativeMethods.PortSIP_setPlayAVIFileToRemote(_SIPCoreHandle, aviFile, enableState, playMode, _SIPCoreHandle, null); 
            }            
        }



        public void playLocalWaveFile(String waveFile)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_playLocalWaveFile(_SIPCoreHandle, waveFile); 
        }



        public void enableAudioStreamCallback(AUDIOSTREAM_CALLBACK_MODE callbackType)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_enableAudioStreamCallback(_SIPCoreHandle, (Int32)callbackType, _SIPCoreHandle, _arc); 
        }



        public void enableVideoStreamCallback(VIDEOSTREAM_CALLBACK_MODE callbackType)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_enableVideoStreamCallback(_SIPCoreHandle, (Int32)callbackType, _SIPCoreHandle, _vrc); 
        }




        public Boolean presenceSubscribeContact(String contact, String subject)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return false;
            }

            return PortSIP_NativeMethods.PortSIP_presenceSubscribeContact(_SIPCoreHandle, contact, subject); 
        }




        public void presenceRejectSubscribe(Int32 subscribeId)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_presenceRejectSubscribe(_SIPCoreHandle, subscribeId); 
        }


        public void presenceAcceptSubscribe(Int32 subscribeId)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_presenceAcceptSubscribe(_SIPCoreHandle, subscribeId); 
        }



        public void presenceOffline(Int32 subscribeId)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_presenceOffline(_SIPCoreHandle, subscribeId); 
        }



        public void presenceOnline(Int32 subscribeId, String stateText)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_presenceOnline(_SIPCoreHandle, subscribeId, stateText); 
        }



        public Boolean sendPagerMessage(String to, String message)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return false;
            }

            return PortSIP_NativeMethods.PortSIP_sendPagerMessage(_SIPCoreHandle, to, message); 
        }



        public void enableStackLog()
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_enableStackLog(_SIPCoreHandle); 
        }




        public Boolean createConference()
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return false;
            }

            return PortSIP_NativeMethods.PortSIP_createConference(_SIPCoreHandle); 
        }




        public void destroyConference()
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_destroyConference(_SIPCoreHandle); 
        }




        public Boolean sendPcmStreamToRemote(Int32 sessionId, byte[] data, Int32 dataLength, Int32 dataSamplesPerSec)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return false;
            }

            return PortSIP_NativeMethods.PortSIP_sendPcmStreamToRemote(_SIPCoreHandle, sessionId, data, dataLength, dataSamplesPerSec); 
        }



        public Boolean setSendAudioStream(Int32 sessionId, Boolean state)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return false;
            }

            return PortSIP_NativeMethods.PortSIP_setSendAudioStream(_SIPCoreHandle, sessionId, state); 
        }




        public void addSupportedMimeType(String methodName, String mimeType, String subMimeType)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_addSupportedMimeType(_SIPCoreHandle, methodName, mimeType, subMimeType); 
        }



        public Boolean sendInfo(Int32 sessionId, String mimeType, String subMimeType, String infoContents)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return false;
            }

            return PortSIP_NativeMethods.PortSIP_sendInfo(_SIPCoreHandle, sessionId, mimeType, subMimeType, infoContents); 
        }



        public Boolean sendOptions(String to, String sdp)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return false;
            }

            return PortSIP_NativeMethods.PortSIP_sendOptions(_SIPCoreHandle, to, sdp); 
        }



        public Boolean setAudioSample(Int32 sample)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return false;
            }

            return PortSIP_NativeMethods.PortSIP_setAudioSample(_SIPCoreHandle, sample); 
        }



        public void enableCheckMwi(Boolean state)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_enableCheckMwi(_SIPCoreHandle, state); 
        }



        public void getVersion(out Int32 majorVersion, out Int32 minorVersion)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                majorVersion = 0;
                minorVersion = 0;

                return;
            }

            PortSIP_NativeMethods.PortSIP_getVersion(_SIPCoreHandle, out majorVersion, out minorVersion);
        }



        public void setRtpKeepAlive(Boolean state)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_setRtpKeepAlive(_SIPCoreHandle, state); 
        }



        public void setDSCPForQos(Int32 DSCPValue)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_setDSCPForQos(_SIPCoreHandle, DSCPValue); 
        }



        public void enableAudioQos(Boolean state)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_enableAudioQos(_SIPCoreHandle, state); 
        }



        public void enableVideoQos(Boolean state)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_enableVideoQos(_SIPCoreHandle, state); 
        }




        public void setDisplayNameOfFrom(String displayName)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_setDisplayNameOfFrom(_SIPCoreHandle, displayName); 
        }


        public Boolean sendMessage(Int32 sessionId, String mimeType, String subMimeType, String message)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return false;
            }

            return PortSIP_NativeMethods.PortSIP_sendMessage(_SIPCoreHandle, sessionId, mimeType, subMimeType, message); 
        }



        public Boolean sendOutOfDialogMessage(String to, String mimeType, String subMimeType, String message)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return false;
            }

            return PortSIP_NativeMethods.PortSIP_sendOutOfDialogMessage(_SIPCoreHandle, to, mimeType, subMimeType, message); 
        }


        public Boolean sendOutOfDialogMessageEx(String to, String mimeType, String subMimeType, byte [] message, Int32 messageLength)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return false;
            }

            return PortSIP_NativeMethods.PortSIP_sendOutOfDialogMessageEx(_SIPCoreHandle, to, mimeType, subMimeType, message, messageLength);
        }



        public Boolean sendMessageEx(Int32 sessionId, String mimeType, String subMimeType, byte[] message, Int32 messageLength)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return false;
            }

            return PortSIP_NativeMethods.PortSIP_sendMessageEx(_SIPCoreHandle, sessionId, mimeType, subMimeType, message, messageLength);
        }


        public void discardAudio(Boolean discardIncomingAudio, Boolean discardOutgoingAudio)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_discardAudio(_SIPCoreHandle, discardIncomingAudio, discardOutgoingAudio);
        }

        public void detectWmi()
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.PortSIP_detectWmi(_SIPCoreHandle);
        }



        public void getDynamicVolumeLevel(out Int32 speakerVolume, out Int32 microphoneVolume)
        {
            if (_SIPCoreHandle == IntPtr.Zero)
            {
                speakerVolume = 0;
                microphoneVolume = 0;

                return;
            }

            PortSIP_NativeMethods.PortSIP_getDynamicVolumeLevel(_SIPCoreHandle, out speakerVolume, out microphoneVolume);
        }



        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// Private members and methods
        /// </summary>



        private static PortSIP_NativeMethods.audioRawCallback _arc = new PortSIP_NativeMethods.audioRawCallback(onAudioRawCallback);
        private static PortSIP_NativeMethods.videoRawCallback _vrc = new PortSIP_NativeMethods.videoRawCallback(onVideoRawCallback);
        private static PortSIP_NativeMethods.playAviFileToRemoteFinished _paf = new PortSIP_NativeMethods.playAviFileToRemoteFinished(onPlayAviFileFinished);
        private static PortSIP_NativeMethods.playWaveFileToRemoteFinished _pwf = new PortSIP_NativeMethods.playWaveFileToRemoteFinished(onPlayWaveFileFinished);



        private static PortSIP_NativeMethods.registerSuccess _rgs = new PortSIP_NativeMethods.registerSuccess(onRegisterSuccess);
        private static PortSIP_NativeMethods.registerFailure _rgf = new PortSIP_NativeMethods.registerFailure(onRegisterFailure);
        private static PortSIP_NativeMethods.inviteIncoming _ivi = new PortSIP_NativeMethods.inviteIncoming(onInviteIncoming);
        private static PortSIP_NativeMethods.inviteTrying _ivt = new PortSIP_NativeMethods.inviteTrying(onInviteTrying);
        private static PortSIP_NativeMethods.inviteRinging _ivr = new PortSIP_NativeMethods.inviteRinging(onInviteRinging);
        private static PortSIP_NativeMethods.inviteAnswered _iva = new PortSIP_NativeMethods.inviteAnswered(onInviteAnswered);
        private static PortSIP_NativeMethods.inviteFailure _ivf = new PortSIP_NativeMethods.inviteFailure(onInviteFailure);
        private static PortSIP_NativeMethods.inviteClosed _ivc = new PortSIP_NativeMethods.inviteClosed(onInviteClosed);
        private static PortSIP_NativeMethods.inviteUpdated _ivu = new PortSIP_NativeMethods.inviteUpdated(onInviteUpdated);
        private static PortSIP_NativeMethods.inviteUASConnected _ivsc = new PortSIP_NativeMethods.inviteUASConnected(onInviteUASConnected);
        private static PortSIP_NativeMethods.inviteUACConnected _ivcc = new PortSIP_NativeMethods.inviteUACConnected(onInviteUACConnected);
        private static PortSIP_NativeMethods.inviteBeginingForward _ivbf = new PortSIP_NativeMethods.inviteBeginingForward(onInviteBeginingForward);
        private static PortSIP_NativeMethods.remoteHold _rth = new PortSIP_NativeMethods.remoteHold(onRemoteHold);
        private static PortSIP_NativeMethods.remoteUnHold _rtu = new PortSIP_NativeMethods.remoteUnHold(onRemoteUnHold);
        private static PortSIP_NativeMethods.transferTrying _tst = new PortSIP_NativeMethods.transferTrying(onTransferTrying);
        private static PortSIP_NativeMethods.transferRinging _tsr = new PortSIP_NativeMethods.transferRinging(onTransferRinging);
        private static PortSIP_NativeMethods.PASVTransferSuccess _pts = new PortSIP_NativeMethods.PASVTransferSuccess(onPASVTransferSuccess);
        private static PortSIP_NativeMethods.PASVTransferFailure _ptf = new PortSIP_NativeMethods.PASVTransferFailure(onPASVTransferFailure);
        private static PortSIP_NativeMethods.ACTVTransferSuccess _ats = new PortSIP_NativeMethods.ACTVTransferSuccess(onACTVTransferSuccess);
        private static PortSIP_NativeMethods.ACTVTransferFailure _atf = new PortSIP_NativeMethods.ACTVTransferFailure(onACTVTransferFailure);
        private static PortSIP_NativeMethods.recvPagerMessage _rpm = new PortSIP_NativeMethods.recvPagerMessage(onRecvPagerMessage);
        private static PortSIP_NativeMethods.sendPagerMessageSuccess _spms = new PortSIP_NativeMethods.sendPagerMessageSuccess(onSendPagerMessageSuccess);
        private static PortSIP_NativeMethods.sendPagerMessageFailure _spmf = new PortSIP_NativeMethods.sendPagerMessageFailure(onSendPagerMessageFailure);
        private static PortSIP_NativeMethods.arrivedSignaling _asg = new PortSIP_NativeMethods.arrivedSignaling(onArrivedSignaling);
        private static PortSIP_NativeMethods.waitingVoiceMessage _wvm = new PortSIP_NativeMethods.waitingVoiceMessage(onWaitingVoiceMessage);
        private static PortSIP_NativeMethods.waitingFaxMessage _wfm = new PortSIP_NativeMethods.waitingFaxMessage(onWaitingFaxMessage);
        private static PortSIP_NativeMethods.recvDtmfTone _rdt = new PortSIP_NativeMethods.recvDtmfTone(onRecvDtmfTone);
        private static PortSIP_NativeMethods.presenceRecvSubscribe _prs = new PortSIP_NativeMethods.presenceRecvSubscribe(onPresenceRecvSubscribe);
        private static PortSIP_NativeMethods.presenceOnline _pon = new PortSIP_NativeMethods.presenceOnline(onPresenceOnline);
        private static PortSIP_NativeMethods.presenceOffline _pof = new PortSIP_NativeMethods.presenceOffline(onPresenceOffline);
        private static PortSIP_NativeMethods.recvOptions _ro = new PortSIP_NativeMethods.recvOptions(onRecvOptions);
        private static PortSIP_NativeMethods.recvInfo _ri = new PortSIP_NativeMethods.recvInfo(onRecvInfo);
        private static PortSIP_NativeMethods.recvMessage _rms = new PortSIP_NativeMethods.recvMessage(onRecvMessage);
        private static PortSIP_NativeMethods.recvBinaryMessage _rbm = new PortSIP_NativeMethods.recvBinaryMessage(onRecvBinaryMessage);
        private static PortSIP_NativeMethods.recvBinaryPagerMessage _rbpm = new PortSIP_NativeMethods.recvBinaryPagerMessage(onRecvBinaryPagerMessage);




        // SIP message callback events

        private unsafe static Int32 onRegisterSuccess(Int32 callbackObject, Int32 statusCode, String statusText)
        {
            Manager.onRegisterSuccess(callbackObject, statusCode, statusText);

            return 0;
        }


        private unsafe static Int32 onRegisterFailure(Int32 callbackObject, Int32 statusCode, String statusText)
        {
            Manager.onRegisterFailure(callbackObject, statusCode, statusText);

            return 0;
        }

        private unsafe static Int32 onInviteIncoming(Int32 callbackObject,
                                             Int32 sessionId,
                                             String caller,
                                             String callerDisplayName,
                                             String callee,
                                             String calleeDisplayName,
                                             String audioCodecName,
                                             String videoCodecName,
                                             Boolean hasVideo)
        {
            Manager.onInviteIncoming(callbackObject,
                                              sessionId,
                                              caller,
                                              callerDisplayName,
                                              callee,
                                              calleeDisplayName,
                                              audioCodecName,
                                              videoCodecName,
                                              hasVideo);

            return 0;
        }


        private unsafe static Int32 onInviteTrying(Int32 callbackObject, Int32 sessionId, String caller, String callee)
        {
            Manager.onInviteTrying(callbackObject, sessionId, caller, callee);

            return 0;
        }

        private unsafe static Int32 onInviteRinging(Int32 callbackObject,
                                            Int32 sessionId,
                                            Boolean hasEarlyMedia,
                                            Boolean hasVideo,
                                            String audioCodecName,
                                            String videoCodecName)
        {
            Manager.onInviteRinging(callbackObject,
                                      sessionId,
                                      hasEarlyMedia,
                                      hasVideo,
                                      audioCodecName,
                                      videoCodecName);

            return 0;
        }



        private unsafe static Int32 onInviteAnswered(Int32 callbackObject,
                                             Int32 sessionId,
                                             Boolean hasVideo,
                                             Int32 statusCode,
                                             String statusText,
                                             String audioCodecName,
                                             String videoCodecName
                                            )
        {
            Manager.onInviteAnswered(callbackObject,
                                       sessionId,
                                       hasVideo,
                                       statusCode,
                                       statusText,
                                       audioCodecName,
                                       videoCodecName);

            return 0;
        }


        private unsafe static Int32 onInviteFailure(Int32 callbackObject, Int32 sessionId, Int32 statusCode, String statusText)
        {
            Manager.onInviteFailure(callbackObject, sessionId, statusCode, statusText);
            return 0;
        }


        private unsafe static Int32 onInviteClosed(Int32 callbackObject, Int32 sessionId)
        {
            Manager.onInviteClosed(callbackObject, sessionId);
            return 0;
        }


        private unsafe static Int32 onInviteUpdated(Int32 callbackObject,
                                            Int32 sessionId,
                                            Boolean hasVideo,
                                            String audioCodecName,
                                            String videoCodecName
                                            )
        {
            Manager.onInviteUpdated(callbackObject,
                                      sessionId,
                                      hasVideo,
                                      audioCodecName,
                                      videoCodecName);

            return 0;
        }


        private unsafe static Int32 onInviteUASConnected(Int32 callbackObject, Int32 sessionId, Int32 statusCode, String statusText)
        {
            Manager.onInviteUASConnected(callbackObject, sessionId, statusCode, statusText);

            return 0;
        }


        private unsafe static Int32 onInviteUACConnected(Int32 callbackObject, Int32 sessionId, Int32 statusCode, String statusText)
        {
            Manager.onInviteUACConnected(callbackObject, sessionId, statusCode, statusText);

            return 0;
        }


        private unsafe static Int32 onInviteBeginingForward(Int32 callbackObject, String forwardingTo)
        {
            Manager.onInviteBeginingForward(callbackObject, forwardingTo);

            return 0;
        }


        private unsafe static Int32 onRemoteHold(Int32 callbackObject, Int32 sessionId)
        {
            Manager.onRemoteHold(callbackObject, sessionId);

            return 0;
        }

        private unsafe static Int32 onRemoteUnHold(Int32 callbackObject, Int32 sessionId)
        {
            Manager.onRemoteUnHold(callbackObject, sessionId);

            return 0;
        }


        private unsafe static Int32 onTransferTrying(Int32 callbackObject, Int32 sessionId, String referTo)
        {
            Manager.onTransferTrying(callbackObject, sessionId, referTo);

            return 0;
        }

        private unsafe static Int32 onTransferRinging(Int32 callbackObject, Int32 sessionId, Boolean hasVideo)
        {
            Manager.onTransferRinging(callbackObject, sessionId, hasVideo);
            return 0;
        }


        private unsafe static Int32 onPASVTransferSuccess(Int32 callbackObject, Int32 sessionId, Boolean hasVideo)
        {
            Manager.onPASVTransferSuccess(callbackObject, sessionId, hasVideo);
            return 0;
        }

        private unsafe static Int32 onPASVTransferFailure(Int32 callbackObject, Int32 sessionId, Int32 statusCode, String statusText)
        {
            Manager.onPASVTransferFailure(callbackObject, sessionId, statusCode, statusText);
            return 0;
        }



        private unsafe static Int32 onACTVTransferSuccess(Int32 callbackObject, Int32 sessionId)
        {
            Manager.onACTVTransferSuccess(callbackObject, sessionId);
            return 0;
        }

        private unsafe static Int32 onACTVTransferFailure(Int32 callbackObject, Int32 sessionId, Int32 statusCode, String statusText)
        {
            Manager.onACTVTransferFailure(callbackObject, sessionId, statusCode, statusText);
            return 0;
        }


        private unsafe static Int32 onRecvPagerMessage(Int32 callbackObject, String from, String fromDisplayName, StringBuilder message)
        {
            Manager.onRecvPagerMessage(callbackObject, from, fromDisplayName, message);

            return 0;
        }

        private unsafe static Int32 onSendPagerMessageSuccess(Int32 callbackObject,
                                                      String caller,
                                                      String callerDisplayName,
                                                      String callee,
                                                      String calleeDisplayName
                                                     )
        {
            Manager.onSendPagerMessageSuccess(callbackObject,
                                                caller,
                                                callerDisplayName,
                                                callee,
                                                calleeDisplayName);

            return 0;
        }



        private unsafe static Int32 onSendPagerMessageFailure(Int32 callbackObject,
                                                       String caller,
                                                      String callerDisplayName,
                                                      String callee,
                                                      String calleeDisplayName,
                                                      Int32 statusCode,
                                                      String statusText
                                                     )
        {
            Manager.onSendPagerMessageFailure(callbackObject,
                                                caller,
                                                callerDisplayName,
                                                callee,
                                                calleeDisplayName,
                                                statusCode,
                                                 statusText);
            return 0;
        }



        private unsafe static Int32 onArrivedSignaling(Int32 callbackObject, Int32 sessionId, StringBuilder signaling)
        {
            Manager.onArrivedSignaling(callbackObject, sessionId, signaling);
            return 0;
        }

        private unsafe static Int32 onWaitingVoiceMessage(Int32 callbackObject,
                                                  String messageAccount,
                                                  Int32 urgentNewMessageCount,
                                                  Int32 urgentOldMessageCount,
                                                  Int32 newMessageCount,
                                                  Int32 oldMessageCount)
        {
            Manager.onWaitingVoiceMessage(callbackObject, messageAccount, urgentNewMessageCount, urgentOldMessageCount, newMessageCount, oldMessageCount);

            return 0;
        }


        private unsafe static Int32 onWaitingFaxMessage(Int32 callbackObject,
                                                  String messageAccount,
                                                  Int32 urgentNewMessageCount,
                                                  Int32 urgentOldMessageCount,
                                                  Int32 newMessageCount,
                                                  Int32 oldMessageCount)
        {
            Manager.onWaitingFaxMessage(callbackObject, messageAccount, urgentNewMessageCount, urgentOldMessageCount, newMessageCount, oldMessageCount);


            return 0;
        }



        private unsafe static Int32 onRecvDtmfTone(Int32 callbackObject, Int32 sessionId, Int32 tone)
        {
            Manager.onRecvDtmfTone(callbackObject, sessionId, tone);
            return 0;
        }


        private unsafe static Int32 onPresenceRecvSubscribe(Int32 callbackObject,
                                                    Int32 subscribeId,
                                                    String from,
                                                    String fromDisplayName,
                                                    String subject)
        {
            Manager.onPresenceRecvSubscribe(callbackObject,
                                               subscribeId,
                                               from,
                                               fromDisplayName,
                                               subject);

            return 0;
        }


        private unsafe static Int32 onPresenceOnline(Int32 callbackObject, String from, String fromDisplayName, String stateText)
        {
            Manager.onPresenceOnline(callbackObject, from, fromDisplayName, stateText);
            return 0;
        }

        private unsafe static Int32 onPresenceOffline(Int32 callbackObject, String from, String fromDisplayName)
        {
            Manager.onPresenceOffline(callbackObject, from, fromDisplayName);

            return 0;
        }


        private unsafe static Int32 onRecvOptions(Int32 callbackObject, StringBuilder optionsMessage)
        {
            Manager.onRecvOptions(callbackObject, optionsMessage);
            return 0;
        }

        private unsafe static Int32 onRecvInfo(Int32 callbackObject, Int32 sessionId, StringBuilder infoMessage)
        {
            Manager.onRecvInfo(callbackObject, sessionId, infoMessage);
            return 0;
        }


        private unsafe static Int32 onRecvMessage(Int32 callbackObject, Int32 sessionId, StringBuilder message)
        {
            Manager.onRecvMessage(callbackObject, sessionId, message);
            return 0;
        }


        private unsafe static Int32 onRecvBinaryMessage(Int32 callbackObject, 
                                                        Int32 sessionId, 
                                                        StringBuilder message,
                                                        [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 4)] byte[] messageBody,
                                                        Int32 length)
        {
            Manager.onRecvBinaryMessage(callbackObject, sessionId, message, messageBody, length);
            return 0;
        }


        private unsafe static Int32 onRecvBinaryPagerMessage(Int32 callbackObject,
                                                         StringBuilder from,
                                                         StringBuilder fromDisplayName,
                                                         [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 4)] byte[] messageBody,
                                                         Int32 length)
        {
            Manager.onRecvBinaryPagerMessage(callbackObject, from, fromDisplayName, messageBody, length);
            return 0;
        }




        private Boolean createSIPCallbackHandle()
        {
            _callbackHandle = PortSIP_NativeMethods.SIPEV_createSIPCallbackHandle();
            if (_callbackHandle == IntPtr.Zero)
            {
                return false;
            }

            return true;
        }


        private void setCallbackHandlers()
        {
            PortSIP_NativeMethods.SIPEV_setRegisterSuccessHandler(_callbackHandle, _rgs, _callbackObject);
            PortSIP_NativeMethods.SIPEV_setRegisterFailureHandler(_callbackHandle, _rgf, _callbackObject);
            PortSIP_NativeMethods.SIPEV_setInviteIncomingHandler(_callbackHandle, _ivi, _callbackObject);
            PortSIP_NativeMethods.SIPEV_setInviteTryingHandler(_callbackHandle, _ivt, _callbackObject);
            PortSIP_NativeMethods.SIPEV_setInviteRingingHandler(_callbackHandle, _ivr, _callbackObject);
            PortSIP_NativeMethods.SIPEV_setInviteAnsweredHandler(_callbackHandle, _iva, _callbackObject);
            PortSIP_NativeMethods.SIPEV_setInviteFailureHandler(_callbackHandle, _ivf, _callbackObject);
            PortSIP_NativeMethods.SIPEV_setInviteClosedHandler(_callbackHandle, _ivc, _callbackObject);
            PortSIP_NativeMethods.SIPEV_setInviteUpdatedHandler(_callbackHandle, _ivu, _callbackObject);
            PortSIP_NativeMethods.SIPEV_setInviteUASConnectedHandler(_callbackHandle, _ivsc, _callbackObject);
            PortSIP_NativeMethods.SIPEV_setInviteUACConnectedHandler(_callbackHandle, _ivcc, _callbackObject);
            PortSIP_NativeMethods.SIPEV_setInviteBeginingForwardHandler(_callbackHandle, _ivbf, _callbackObject);
            PortSIP_NativeMethods.SIPEV_setRemoteHoldHandler(_callbackHandle, _rth, _callbackObject);
            PortSIP_NativeMethods.SIPEV_setRemoteUnHoldHandler(_callbackHandle, _rtu, _callbackObject);
            PortSIP_NativeMethods.SIPEV_setTransferTryingHandler(_callbackHandle, _tst, _callbackObject);
            PortSIP_NativeMethods.SIPEV_setTransferRingingHandler(_callbackHandle, _tsr, _callbackObject);
            PortSIP_NativeMethods.SIPEV_setPASVTransferSuccessHandler(_callbackHandle, _pts, _callbackObject);
            PortSIP_NativeMethods.SIPEV_setACTVTransferSuccessHandler(_callbackHandle, _ats, _callbackObject);
            PortSIP_NativeMethods.SIPEV_setPASVTransferFailureHandler(_callbackHandle, _ptf, _callbackObject);
            PortSIP_NativeMethods.SIPEV_setACTVTransferFailureHandler(_callbackHandle, _atf, _callbackObject);
            PortSIP_NativeMethods.SIPEV_setRecvPagerMessageHandler(_callbackHandle, _rpm, _callbackObject);
            PortSIP_NativeMethods.SIPEV_setSendPagerMessageSuccessHandler(_callbackHandle, _spms, _callbackObject);
            PortSIP_NativeMethods.SIPEV_setSendPagerMessageFailureHandler(_callbackHandle, _spmf, _callbackObject);
            PortSIP_NativeMethods.SIPEV_setArrivedSignalingHandler(_callbackHandle, _asg, _callbackObject);
            PortSIP_NativeMethods.SIPEV_setWaitingVoiceMessageHandler(_callbackHandle, _wvm, _callbackObject);
            PortSIP_NativeMethods.SIPEV_setWaitingFaxMessageHandler(_callbackHandle, _wfm, _callbackObject);
            PortSIP_NativeMethods.SIPEV_setRecvDtmfToneHandler(_callbackHandle, _rdt, _callbackObject);
            PortSIP_NativeMethods.SIPEV_setPresenceRecvSubscribeHandler(_callbackHandle, _prs, _callbackObject);
            PortSIP_NativeMethods.SIPEV_setPresenceOnlineHandler(_callbackHandle, _pon, _callbackObject);
            PortSIP_NativeMethods.SIPEV_setPresenceOfflineHandler(_callbackHandle, _pof, _callbackObject);
            PortSIP_NativeMethods.SIPEV_setRecvOptionsHandler(_callbackHandle, _ro, _callbackObject);
            PortSIP_NativeMethods.SIPEV_setRecvInfoHandler(_callbackHandle, _ri, _callbackObject);
            PortSIP_NativeMethods.SIPEV_setRecvMessageHandler(_callbackHandle, _rms, _callbackObject);
            PortSIP_NativeMethods.SIPEV_setRecvBinaryMessageHandler(_callbackHandle, _rbm, _callbackObject);
            PortSIP_NativeMethods.SIPEV_setRecvBinaryPagerMessageHandler(_callbackHandle, _rbpm, _callbackObject);
        }




        //////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////


        private unsafe static Int32 onAudioRawCallback(IntPtr callbackObject,
                                               Int32 streamType,
                                               [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 3)] byte[] data,
                                               Int32 dataLength)
        {
            Manager.onAudioRawCallback(callbackObject,
                                         streamType,
                                         data,
                                         dataLength);
            return 0;
        }

        private unsafe static Int32 onVideoRawCallback(IntPtr callbackObject,
                                               Int32 sessionId,
                                               Int32 width,
                                               Int32 height,
                                               [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 5)] byte[] data,
                                               Int32 dataLength)
        {
            Manager.onVideoRawCallback(callbackObject,
                                         sessionId,
                                         width,
                                         height,
                                         data,
                                         dataLength);

            return 0;

        }


        private unsafe static Int32 onPlayAviFileFinished(IntPtr callbackObject, String fileName)
        {
            Manager.onPlayAviFileFinished(callbackObject, fileName);
            return 0;
        }


        private unsafe static Int32 onPlayWaveFileFinished(IntPtr callbackObject, Int32 sessionId, String fileName)
        {
            Manager.onPlayWaveFileFinished(callbackObject, sessionId, fileName);
            return 0;
        }


        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        private static IntPtr _callbackHandle;
        private Int32 _callbackObject;
        private static IntPtr _SIPCoreHandle;


    }
}
