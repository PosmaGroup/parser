//////////////////////////////////////////////////////////////////////////
//
// IMPORTANT: DON'T EDIT THIS FILE
//
//////////////////////////////////////////////////////////////////////////



using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;


namespace PortSIP
{
    unsafe class PortSIP_NativeMethods
    {

        // The delegate methods for callback functions of SIPEventEX.dll

        public unsafe delegate Int32 registerSuccess(Int32 callbackObject, Int32 statusCode, String statusText);
        public unsafe delegate Int32 registerFailure(Int32 callbackObject, Int32 statusCode, String statusText);

        public unsafe delegate Int32 inviteIncoming(Int32 callbackObject,
                                             Int32 sessionId,
                                             String caller,
                                             String callerDisplayName,
                                             String callee,
                                             String calleeDisplayName,
                                             String audioCodecName,
                                             String videoCodecName,
                                             [MarshalAs(UnmanagedType.I1)] Boolean hasVideo);

        public unsafe delegate Int32 inviteTrying(Int32 callbackObject, Int32 sessionId, String caller, String callee);
        public unsafe delegate Int32 inviteRinging(Int32 callbackObject,
                                            Int32 sessionId,
                                            [MarshalAs(UnmanagedType.I1)] Boolean hasEarlyMedia,
                                            [MarshalAs(UnmanagedType.I1)] Boolean hasVideo,
                                            String audioCodecName,
                                            String videoCodecName);


        public unsafe delegate Int32 inviteAnswered(Int32 callbackObject,
                                             Int32 sessionId,
                                             [MarshalAs(UnmanagedType.I1)] Boolean hasVideo,
                                             Int32 statusCode,
                                             String statusText,
                                             String audioCodecName,
                                             String videoCodecName
                                            );

        public unsafe delegate Int32 inviteFailure(Int32 callbackObject, Int32 sessionId, Int32 statusCode, String statusText);
        public unsafe delegate Int32 inviteClosed(Int32 callbackObject, Int32 sessionId);


        public unsafe delegate Int32 inviteUpdated(Int32 callbackObject,
                                            Int32 sessionId,
                                            [MarshalAs(UnmanagedType.I1)] Boolean hasVideo,
                                            String audioCodecName,
                                            String videoCodecName
                                            );

        public unsafe delegate Int32 inviteUASConnected(Int32 callbackObject, Int32 sessionId, Int32 statusCode, String statusText);

        public unsafe delegate Int32 inviteUACConnected(Int32 callbackObject, Int32 sessionId, Int32 statusCode, String statusText);

        public unsafe delegate Int32 inviteBeginingForward(Int32 callbackObject, String forwardingTo);

        public unsafe delegate Int32 remoteHold(Int32 callbackObject, Int32 sessionId);
        public unsafe delegate Int32 remoteUnHold(Int32 callbackObject, Int32 sessionId);



        public unsafe delegate Int32 transferTrying(Int32 callbackObject, Int32 sessionId, String referTo);
        public unsafe delegate Int32 transferRinging(Int32 callbackObject, Int32 sessionId, [MarshalAs(UnmanagedType.I1)] Boolean hasVideo);

        public unsafe delegate Int32 PASVTransferSuccess(Int32 callbackObject, Int32 sessionId, [MarshalAs(UnmanagedType.I1)] Boolean hasVideo);
        public unsafe delegate Int32 PASVTransferFailure(Int32 callbackObject, Int32 sessionId, Int32 statusCode, String statusText);


        public unsafe delegate Int32 ACTVTransferSuccess(Int32 callbackObject, Int32 sessionId);
        public unsafe delegate Int32 ACTVTransferFailure(Int32 callbackObject, Int32 sessionId, Int32 statusCode, String statusText);

        public unsafe delegate Int32 recvPagerMessage(Int32 callbackObject, String from, String fromDisplayName, StringBuilder message);
        public unsafe delegate Int32 sendPagerMessageSuccess(Int32 callbackObject,
                                                      String caller,
                                                      String callerDisplayName,
                                                      String callee,
                                                      String calleeDisplayName
                                                     );


        public unsafe delegate Int32 sendPagerMessageFailure(Int32 callbackObject,
                                                       String caller,
                                                      String callerDisplayName,
                                                      String callee,
                                                      String calleeDisplayName,
                                                      Int32 statusCode,
                                                      String statusText
                                                     );


        public unsafe delegate Int32 arrivedSignaling(Int32 callbackObject, Int32 sessionId, StringBuilder signaling);
        public unsafe delegate Int32 waitingVoiceMessage(Int32 callbackObject,
                                                  String messageAccount,
                                                  Int32 urgentNewMessageCount,
                                                  Int32 urgentOldMessageCount,
                                                  Int32 newMessageCount,
                                                  Int32 oldMessageCount);

        public unsafe delegate Int32 waitingFaxMessage(Int32 callbackObject,
                                                  String messageAccount,
                                                  Int32 urgentNewMessageCount,
                                                  Int32 urgentOldMessageCount,
                                                  Int32 newMessageCount,
                                                  Int32 oldMessageCount);


        public unsafe delegate Int32 recvDtmfTone(Int32 callbackObject, Int32 sessionId, Int32 tone);

        public unsafe delegate Int32 presenceRecvSubscribe(Int32 callbackObject,
                                                    Int32 subscribeId,
                                                    String from,
                                                    String fromDisplayName,
                                                    String subject);

        public unsafe delegate Int32 presenceOnline(Int32 callbackObject, String from, String fromDisplayName, String stateText);
        public unsafe delegate Int32 presenceOffline(Int32 callbackObject, String from, String fromDisplayName);

        public unsafe delegate Int32 recvOptions(Int32 callbackObject, StringBuilder optionsMessage);
        public unsafe delegate Int32 recvInfo(Int32 callbackObject, Int32 sessionId, StringBuilder infoMessage);
        public unsafe delegate Int32 recvMessage(Int32 callbackObject, Int32 sessionId, StringBuilder message);

        public unsafe delegate Int32 recvBinaryMessage(Int32 callbackObject, 
                                                        Int32 sessionId,
                                                        StringBuilder message,
                                                        [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 4)] byte[] messageBody,
                                                       Int32 length);

        public unsafe delegate Int32 recvBinaryPagerMessage(Int32 callbackObject,
                                                        StringBuilder from,
                                                        StringBuilder fromDisplayName,
                                                        [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 4)] byte[] messageBody,
                                                       Int32 length);
 

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



        // The functions of SIPEventEX.dll
        [DllImport("SIPEventEX.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern IntPtr SIPEV_createSIPCallbackHandle();

        [DllImport("SIPEventEX.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void SIPEV_releaseSIPCallbackHandle(IntPtr callbackHandle);


        [DllImport("SIPEventEX.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void SIPEV_setRegisterSuccessHandler(IntPtr callbackHandle, registerSuccess eventHandler, Int32 callbackObject);

        [DllImport("SIPEventEX.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void SIPEV_setRegisterFailureHandler(IntPtr callbackHandle, registerFailure eventHandler, Int32 callbackObject);


        [DllImport("SIPEventEX.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void SIPEV_setInviteIncomingHandler(IntPtr callbackHandle, inviteIncoming eventHandler, Int32 callbackObject);

        [DllImport("SIPEventEX.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void SIPEV_setInviteTryingHandler(IntPtr callbackHandle, inviteTrying eventHandler, Int32 callbackObject);


        [DllImport("SIPEventEX.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void SIPEV_setInviteRingingHandler(IntPtr callbackHandle, inviteRinging eventHandler, Int32 callbackObject);

        [DllImport("SIPEventEX.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void SIPEV_setInviteAnsweredHandler(IntPtr callbackHandle, inviteAnswered eventHandler, Int32 callbackObject);

        [DllImport("SIPEventEX.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void SIPEV_setInviteFailureHandler(IntPtr callbackHandle, inviteFailure eventHandler, Int32 callbackObject);

        [DllImport("SIPEventEX.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void SIPEV_setInviteClosedHandler(IntPtr callbackHandle, inviteClosed eventHandler, Int32 callbackObject);

        [DllImport("SIPEventEX.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void SIPEV_setInviteUpdatedHandler(IntPtr callbackHandle, inviteUpdated eventHandler, Int32 callbackObject);

        [DllImport("SIPEventEX.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void SIPEV_setInviteUASConnectedHandler(IntPtr callbackHandle, inviteUASConnected eventHandler, Int32 callbackObject);

        [DllImport("SIPEventEX.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void SIPEV_setInviteUACConnectedHandler(IntPtr callbackHandle, inviteUACConnected eventHandler, Int32 callbackObject);

        [DllImport("SIPEventEX.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void SIPEV_setInviteBeginingForwardHandler(IntPtr callbackHandle, inviteBeginingForward eventHandler, Int32 callbackObject);

        [DllImport("SIPEventEX.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void SIPEV_setRemoteHoldHandler(IntPtr callbackHandle, remoteHold eventHandler, Int32 callbackObject);

        [DllImport("SIPEventEX.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void SIPEV_setRemoteUnHoldHandler(IntPtr callbackHandle, remoteUnHold eventHandler, Int32 callbackObject);

        [DllImport("SIPEventEX.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void SIPEV_setTransferTryingHandler(IntPtr callbackHandle, transferTrying eventHandler, Int32 callbackObject);

        [DllImport("SIPEventEX.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void SIPEV_setTransferRingingHandler(IntPtr callbackHandle, transferRinging eventHandler, Int32 callbackObject);

        [DllImport("SIPEventEX.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void SIPEV_setPASVTransferSuccessHandler(IntPtr callbackHandle, PASVTransferSuccess eventHandler, Int32 callbackObject);

        [DllImport("SIPEventEX.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void SIPEV_setACTVTransferSuccessHandler(IntPtr callbackHandle, ACTVTransferSuccess eventHandler, Int32 callbackObject);

        [DllImport("SIPEventEX.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void SIPEV_setPASVTransferFailureHandler(IntPtr callbackHandle, PASVTransferFailure eventHandler, Int32 callbackObject);

        [DllImport("SIPEventEX.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void SIPEV_setACTVTransferFailureHandler(IntPtr callbackHandle, ACTVTransferFailure eventHandler, Int32 callbackObject);

        [DllImport("SIPEventEX.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void SIPEV_setRecvPagerMessageHandler(IntPtr callbackHandle, recvPagerMessage eventHandler, Int32 callbackObject);

        [DllImport("SIPEventEX.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void SIPEV_setSendPagerMessageSuccessHandler(IntPtr callbackHandle, sendPagerMessageSuccess eventHandler, Int32 callbackObject);

        [DllImport("SIPEventEX.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void SIPEV_setSendPagerMessageFailureHandler(IntPtr callbackHandle, sendPagerMessageFailure eventHandler, Int32 callbackObject);

        [DllImport("SIPEventEX.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void SIPEV_setArrivedSignalingHandler(IntPtr callbackHandle, arrivedSignaling eventHandler, Int32 callbackObject);

        [DllImport("SIPEventEX.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void SIPEV_setWaitingVoiceMessageHandler(IntPtr callbackHandle, waitingVoiceMessage eventHandler, Int32 callbackObject);

        [DllImport("SIPEventEX.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void SIPEV_setWaitingFaxMessageHandler(IntPtr callbackHandle, waitingFaxMessage eventHandler, Int32 callbackObject);

        [DllImport("SIPEventEX.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void SIPEV_setRecvDtmfToneHandler(IntPtr callbackHandle, recvDtmfTone eventHandler, Int32 callbackObject);

        [DllImport("SIPEventEX.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void SIPEV_setPresenceRecvSubscribeHandler(IntPtr callbackHandle, presenceRecvSubscribe eventHandler, Int32 callbackObject);

        [DllImport("SIPEventEX.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void SIPEV_setPresenceOnlineHandler(IntPtr callbackHandle, presenceOnline eventHandler, Int32 callbackObject);

        [DllImport("SIPEventEX.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void SIPEV_setPresenceOfflineHandler(IntPtr callbackHandle, presenceOffline eventHandler, Int32 callbackObject);

        [DllImport("SIPEventEX.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void SIPEV_setRecvOptionsHandler(IntPtr callbackHandle, recvOptions eventHandler, Int32 callbackObject);

        [DllImport("SIPEventEX.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void SIPEV_setRecvInfoHandler(IntPtr callbackHandle, recvInfo eventHandler, Int32 callbackObject);

        [DllImport("SIPEventEX.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void SIPEV_setRecvMessageHandler(IntPtr callbackHandle, recvMessage eventHandler, Int32 callbackObject);

        [DllImport("SIPEventEX.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void SIPEV_setRecvBinaryMessageHandler(IntPtr callbackHandle, recvBinaryMessage eventHandler, Int32 callbackObject);

        [DllImport("SIPEventEX.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void SIPEV_setRecvBinaryPagerMessageHandler(IntPtr callbackHandle, recvBinaryPagerMessage eventHandler, Int32 callbackObject);


        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        // The delegate methods for callback functions of PortSIPCore.dll
        // These callback functions allows you access the raw audio and video data.


        public unsafe delegate Int32 audioRawCallback(IntPtr callbackObject, 
                                               Int32 streamType,
                                               [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 3)] byte[] data, 
                                               Int32 dataLength);


        public unsafe delegate Int32 videoRawCallback(IntPtr callbackObject,
                                               Int32 sessionId,
                                               Int32 width,
                                               Int32 height,
                                               [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 5)] byte[] data,
                                               Int32 dataLength);


        // The delegate methods for callback functions of PortSIPCore.dll
        // These callback functions will be fired when the play the wave file and AVI file to remote side finished.

        public unsafe delegate Int32 playWaveFileToRemoteFinished(IntPtr callbackObject, Int32 sessionId, String fileName);
        public unsafe delegate Int32 playAviFileToRemoteFinished(IntPtr callbackObject, String fileName);


        //////////////////////////////////////////////////////////////////////////
        // The functions of PortSIPCore.dll
        //////////////////////////////////////////////////////////////////////////

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern IntPtr PortSIP_initialize(IntPtr callbackEvent,
                                                   Int32 transportType,
                                                   Int32 appLogLevel,
                                                   Int32 maximumLines,
                                                   String userName,
                                                   String displayName,
                                                   String authName,
                                                   String password,
                                                   String agent,
                                                   String localIp,
                                                   Int32 localSipPort,
                                                   String userDomain,
                                                   String proxyServer,
                                                   Int32 proxyServerPort,
                                                   String outboundServer,
                                                   Int32 outboundServerPort,
                                                   String STUNSever,
                                                   Int32 STUNServerPort);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_unInitialize(IntPtr SIPCoreHandle);


        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static unsafe extern Boolean PortSIP_setRtpPortRange(IntPtr SIPCoreHandle,
                                                   Int32 minimumRtpAudioPort,
                                                   Int32 maximumRtpAudioPort,
                                                   Int32 minimumRtpVideoPort,
                                                   Int32 maximumRtpVideoPort);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_setSrtpPolicy(IntPtr SIPCoreHandle, Int32 srtPolicy);


        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static unsafe extern Boolean PortSIP_registerServer(IntPtr SIPCoreHandle, Int32 expires);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_unRegisterServer(IntPtr SIPCoreHandle);



        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_setVideoResolution(IntPtr SIPCoreHandle, Int32 resolution);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_setVideoImagePos(IntPtr SIPCoreHandle,
                                                   IntPtr localVideoWindow,
                                                   Int32 localVideoAreaLeft,
                                                   Int32 localVideoAreaTop,
                                                   Int32 localVideoAreaWidth,
                                                   Int32 localVideoAreaHeight,
                                                   IntPtr remoteVideoWindow,
                                                   Int32 remoteVideoAreaLeft,
                                                   Int32 remoteVideoAreaTop,
                                                   Int32 remoteVideoAreaWidth,
                                                   Int32 remoteVideoAreaHeight);
        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_reverseLocalViewVideo(IntPtr SIPCoreHandle, [MarshalAs(UnmanagedType.I1)] Boolean state);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_reverseReceivedVideo(IntPtr SIPCoreHandle, [MarshalAs(UnmanagedType.I1)] Boolean state);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_reverseSendingVideo(IntPtr SIPCoreHandle, [MarshalAs(UnmanagedType.I1)] Boolean state);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_addAudioCodec(IntPtr SIPCoreHandle, Int32 codecType);


        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_addVideoCodec(IntPtr SIPCoreHandle, Int32 codecType);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_clearAudioCodec(IntPtr SIPCoreHandle);
       
        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_clearVideoCodec(IntPtr SIPCoreHandle);


        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static unsafe extern Boolean PortSIP_isAudioCodecEmpty(IntPtr SIPCoreHandle);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static unsafe extern Boolean PortSIP_isVideoCodecEmpty(IntPtr SIPCoreHandle);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern Int32 PortSIP_call(IntPtr SIPCoreHandle, String callTo, [MarshalAs(UnmanagedType.I1)] Boolean sendSdp);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_rejectCall(IntPtr SIPCoreHandle, Int32 sessionId, int code, String reason);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static unsafe extern Boolean PortSIP_answerCall(IntPtr SIPCoreHandle, Int32 sessionId);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_terminateCall(IntPtr SIPCoreHandle, Int32 sessionId);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static unsafe extern Boolean PortSIP_forwardCall(IntPtr SIPCoreHandle, Int32 sessionId, String forwardTo);

       [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static unsafe extern Boolean PortSIP_updateInvite(IntPtr SIPCoreHandle, Int32 sessionId);


       [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static unsafe extern Boolean PortSIP_hold(IntPtr SIPCoreHandle, Int32 sessionId);

       [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static unsafe extern Boolean PortSIP_unHold(IntPtr SIPCoreHandle, Int32 sessionId);

       [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static unsafe extern Boolean PortSIP_refer(IntPtr SIPCoreHandle, Int32 sessionId, String referTo);

       [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static unsafe extern Boolean PortSIP_attendedRefer(IntPtr SIPCoreHandle, Int32 sessionId, Int32 replaceSessionId, String referTo);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static unsafe extern Boolean PortSIP_enableSessionTimer(IntPtr SIPCoreHandle, Int32 timerSeconds);


        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_disableSessionTimer(IntPtr SIPCoreHandle);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_setAudioDeviceId(IntPtr SIPCoreHandle, Int32 inputDeviceId, Int32 outputDeviceId);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_setVideoDeviceId(IntPtr SIPCoreHandle, Int32 deviceId);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_setAudioRecordPathName(IntPtr SIPCoreHandle, 
                                                                String recordFilePath, 
                                                                String recordFileName,
                                                                [MarshalAs(UnmanagedType.I1)] Boolean appendTimestamp,
                                                                Int32 fileFormat);


        
        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_setVideoRecordPathName(IntPtr SIPCoreHandle, 
                                                                String recordFilePath, 
                                                                String recordFileName,
                                                                [MarshalAs(UnmanagedType.I1)] Boolean appendTimestamp,
                                                                Int32 recordMode);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_setDTMFSamples(IntPtr SIPCoreHandle, Int32 samples);


        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_enableDTMFOfRFC2833(IntPtr SIPCoreHandle, Int32 RTPPayloadOf2833); // Usually the payload is 101

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_enableDTMFOfInfo(IntPtr SIPCoreHandle);


        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_sendDTMF(IntPtr SIPCoreHandle, Int32 sessionId, Char code);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_startAudioRecording(IntPtr SIPCoreHandle);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_startAudioRecordingOnLine(IntPtr SIPCoreHandle, Int32 sessionId);


        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_stopAudioRecording(IntPtr SIPCoreHandle);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_stopAudioRecordingOnLine(IntPtr SIPCoreHandle, Int32 sessionId);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_startVideoRecording(IntPtr SIPCoreHandle);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_stopVideoRecording(IntPtr SIPCoreHandle);


        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_enableCallForwarding(IntPtr SIPCoreHandle, [MarshalAs(UnmanagedType.I1)] Boolean forBusyOnly, String forwardingTo);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_disableCallForwarding(IntPtr SIPCoreHandle);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_enableAEC(IntPtr SIPCoreHandle, [MarshalAs(UnmanagedType.I1)] Boolean state);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_enableVAD(IntPtr SIPCoreHandle, [MarshalAs(UnmanagedType.I1)] Boolean state);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_enableCNG(IntPtr SIPCoreHandle, [MarshalAs(UnmanagedType.I1)] Boolean state);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_enableAGC(IntPtr SIPCoreHandle, [MarshalAs(UnmanagedType.I1)] Boolean state);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_viewLocalVideo(IntPtr SIPCoreHandle, [MarshalAs(UnmanagedType.I1)] Boolean state);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_displayMixVideo(IntPtr SIPCoreHandle, [MarshalAs(UnmanagedType.I1)] Boolean state);


        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static unsafe extern Boolean PortSIP_sendVideoStream(IntPtr SIPCoreHandle, Int32 sessionId, [MarshalAs(UnmanagedType.I1)] Boolean state);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_setLicenseKey(IntPtr SIPCoreHandle, String key);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_muteMicrophone(IntPtr SIPCoreHandle, [MarshalAs(UnmanagedType.I1)] Boolean mute);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_muteSpeaker(IntPtr SIPCoreHandle, [MarshalAs(UnmanagedType.I1)] Boolean mute);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static unsafe extern Boolean PortSIP_getExtensionHeaderValue(IntPtr SIPCoreHandle, 
                                                                     String sipMessage,
                                                                     String headerName,
                                                                     StringBuilder headerValue,
                                                                     Int32 headerValueLength);



        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_addExtensionHeader(IntPtr SIPCoreHandle, String headerName, String headerValue);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_clearAddExtensionHeaders(IntPtr SIPCoreHandle);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_modifyHeaderValue(IntPtr SIPCoreHandle, String headerName, String headerValue);


        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_clearModifyHeaders(IntPtr SIPCoreHandle);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_setVideoQuality(IntPtr SIPCoreHandle, Int32 quality);


        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_setVideoFrameRate(IntPtr SIPCoreHandle, Int32 frameRate);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_enableCabacOfH264(IntPtr SIPCoreHandle, [MarshalAs(UnmanagedType.I1)] Boolean state);



        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_setPlayWaveFileToRemote(IntPtr SIPCoreHandle, 
                                                                  String waveFile, 
                                                                  [MarshalAs(UnmanagedType.I1)] Boolean enableState, 
                                                                  Int32 playMode,
                                                                  IntPtr reserve,
                                                                  playWaveFileToRemoteFinished callbackHandler);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_setPlayWaveFileToRemoteOnLine(IntPtr SIPCoreHandle,
                                                                  Int32 sessionId,
                                                                  String waveFile,
                                                                  [MarshalAs(UnmanagedType.I1)] Boolean enableState,
                                                                  Int32 playMode,
                                                                  IntPtr reserve,
                                                                  playWaveFileToRemoteFinished callbackHandler);


        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_setPlayAVIFileToRemote(IntPtr SIPCoreHandle, 
                                                                 String aviFile, 
                                                                 [MarshalAs(UnmanagedType.I1)] Boolean enableState,
                                                                 Int32 playMode,
                                                                 IntPtr reserve,
                                                                 playAviFileToRemoteFinished callbackHandler);
    
        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_playLocalWaveFile(IntPtr SIPCoreHandle, String waveFile);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_enableAudioStreamCallback(IntPtr SIPCoreHandle, Int32 callbackType, IntPtr reserve, audioRawCallback callbackHandler);


        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_enableVideoStreamCallback(IntPtr SIPCoreHandle, Int32 callbackType, IntPtr reserve, videoRawCallback callbackHandler);


        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static unsafe extern Boolean PortSIP_presenceSubscribeContact(IntPtr SIPCoreHandle, String contact, String subject);


        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_presenceRejectSubscribe(IntPtr SIPCoreHandle, Int32 subscribeId);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_presenceAcceptSubscribe(IntPtr SIPCoreHandle, Int32 subscribeId);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_presenceOffline(IntPtr SIPCoreHandle, Int32 subscribeId);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_presenceOnline(IntPtr SIPCoreHandle, Int32 subscribeId, String stateText);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static unsafe extern Boolean PortSIP_sendPagerMessage(IntPtr SIPCoreHandle, String to, String message);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_enableStackLog(IntPtr SIPCoreHandle);


        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static unsafe extern Boolean PortSIP_createConference(IntPtr SIPCoreHandle);


        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_destroyConference(IntPtr SIPCoreHandle);


        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static unsafe extern Boolean PortSIP_sendPcmStreamToRemote(IntPtr SIPCoreHandle, 
                                                                    Int32 sessionId,
                                                                    [MarshalAs(UnmanagedType.LPArray)] byte[] data, 
                                                                    Int32 dataLength, 
                                                                    Int32 dataSamplesPerSec);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static unsafe extern Boolean PortSIP_setSendAudioStream(IntPtr SIPCoreHandle, Int32 sessionId,  [MarshalAs(UnmanagedType.I1)] Boolean state);


        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_addSupportedMimeType(IntPtr SIPCoreHandle, 
                                                               String methodName,
                                                               String mimeType,
                                                               String subMimeType);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static unsafe extern Boolean PortSIP_sendInfo(IntPtr SIPCoreHandle, 
                                                      Int32 sessionId,
                                                      String mimeType,
                                                      String subMimeType,
                                                      String infoContents);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static unsafe extern Boolean PortSIP_sendOptions(IntPtr SIPCoreHandle,  String to, String sdp);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static unsafe extern Boolean PortSIP_setAudioSample(IntPtr SIPCoreHandle,  Int32 sample);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_enableCheckMwi(IntPtr SIPCoreHandle,  [MarshalAs(UnmanagedType.I1)] Boolean state);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_getVersion(IntPtr SIPCoreHandle,  out Int32 majorVersion, out Int32 minorVersion);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_setRtpKeepAlive(IntPtr SIPCoreHandle,  [MarshalAs(UnmanagedType.I1)] Boolean state);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_setDSCPForQos(IntPtr SIPCoreHandle,  Int32 DSCPValue);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_enableAudioQos(IntPtr SIPCoreHandle,  [MarshalAs(UnmanagedType.I1)] Boolean state);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_enableVideoQos(IntPtr SIPCoreHandle,  [MarshalAs(UnmanagedType.I1)] Boolean state);


        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_setDisplayNameOfFrom(IntPtr SIPCoreHandle,  String  displayName);


        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static unsafe extern Boolean PortSIP_sendMessage(IntPtr SIPCoreHandle, Int32 sessionId, String mimeType, String subMimeType, String message);


        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static unsafe extern Boolean PortSIP_sendOutOfDialogMessage(IntPtr SIPCoreHandle, String to, String mimeType, String subMimeType, String message);

        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_discardAudio(IntPtr SIPCoreHandle, [MarshalAs(UnmanagedType.I1)] Boolean discardIncomingAudio, [MarshalAs(UnmanagedType.I1)] Boolean discardOutgoingAudio);


        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_getDynamicVolumeLevel(IntPtr SIPCoreHandle, out Int32 speakerVolume, out Int32 microphoneVolume);


        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void PortSIP_detectWmi(IntPtr SIPCoreHandle);


        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static unsafe extern Boolean PortSIP_sendOutOfDialogMessageEx(IntPtr SIPCoreHandle, String to, String mimeType, String subMimeType, [MarshalAs(UnmanagedType.LPArray)] byte[] message, Int32 messageLength);


        [DllImport("PortSIPCore.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static unsafe extern Boolean PortSIP_sendMessageEx(IntPtr SIPCoreHandle, Int32 sessionId, String mimeType, String subMimeType, [MarshalAs(UnmanagedType.LPArray)] byte[] message, Int32 messageLength);

       

        //////////////////////////////////////////////////////////////////////////
        // The functions of DeviceManager.dll
        //////////////////////////////////////////////////////////////////////////

        [DllImport("DeviceManager.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern IntPtr Device_initialize();


        [DllImport("DeviceManager.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void Device_unInitialize(IntPtr deviceManagerHandle);

        [DllImport("DeviceManager.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern Int32 Device_getVideoDeviceNums(IntPtr deviceManagerHandle);

        [DllImport("DeviceManager.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static unsafe extern Boolean Device_getVideoDeviceName(IntPtr deviceManagerHandle, 
                                                             Int32 deviceId, 
                                                             StringBuilder deviceName,
                                                             Int32 deviceNameLength);

                
        [DllImport("DeviceManager.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void Device_showCameraPropertyWindow(IntPtr deviceManagerHandle, IntPtr parentWindow, Int32 deviceId);

                
        [DllImport("DeviceManager.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static unsafe extern Boolean Device_startVideoPreview(IntPtr deviceManagerHandle, 
                                                              IntPtr hwnd, 
                                                              Int32 deviceId,
                                                              Int32 x,
                                                              Int32 y,
                                                              Int32 width,
                                                              Int32 height);

     
        [DllImport("DeviceManager.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void Device_stopVideoPreview(IntPtr deviceManagerHandle);


        [DllImport("DeviceManager.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern Int32 Device_getAudioInputDeviceNums(IntPtr deviceManagerHandle);

        [DllImport("DeviceManager.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern Int32 Device_getAudioOutputDeviceNums(IntPtr deviceManagerHandle);

        [DllImport("DeviceManager.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static unsafe extern Boolean Device_getAudioInputDeviceName(IntPtr deviceManagerHandle, 
                                                                           Int32 deviceId, 
                                                                           StringBuilder deviceName,
                                                                           Int32 deviceNameLength);

        [DllImport("DeviceManager.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static unsafe extern Boolean Device_getAudioOutputDeviceName(IntPtr deviceManagerHandle,
                                                                           Int32 deviceId,
                                                                           StringBuilder deviceName,
                                                                           Int32 deviceNameLength);

        [DllImport("DeviceManager.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern Int32 Device_getAudioInputVolume(IntPtr deviceManagerHandle, Int32 deviceId);

        [DllImport("DeviceManager.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern Int32 Device_getAudioOutputVolume(IntPtr deviceManagerHandle, Int32 deviceId);
       
        [DllImport("DeviceManager.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void Device_setAudioInputVolume(IntPtr deviceManagerHandle, Int32 deviceId, Int32 volume);

        [DllImport("DeviceManager.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void Device_setAudioOutputVolume(IntPtr deviceManagerHandle, Int32 deviceId, Int32 volume);


        [DllImport("DeviceManager.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void Device_startAudioPlayLoopbackTest(IntPtr deviceManagerHandle, Int32 inputDeviceID, Int32 outputDeviceId);

        [DllImport("DeviceManager.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void Device_stopAudioPlayLoopbackTest(IntPtr deviceManagerHandle);


        [DllImport("DeviceManager.dll", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.I1)]
        public static unsafe extern Boolean Device_getLocalIP(IntPtr deviceManagerHandle, Int32 index, StringBuilder ip, Int32 ipLength);

        [DllImport("DeviceManager.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void Device_getVersion(IntPtr deviceManagerHandle, out Int32 majorVersion, out Int32 minorVersion);

        [DllImport("DeviceManager.dll", CallingConvention = CallingConvention.Cdecl)]
        public static unsafe extern void Device_playLocalWaveFile(IntPtr deviceManagerHandle,
                                                            Int32 playDeviceId, 
                                                            String waveFile,
                                                            [MarshalAs(UnmanagedType.I1)] Boolean playAsSync,
                                                            [MarshalAs(UnmanagedType.I1)] Boolean loop,
                                                            Int32 loopInterval);

    }
}
