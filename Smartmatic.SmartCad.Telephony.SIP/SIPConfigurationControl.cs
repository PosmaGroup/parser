﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SmartCadCore.Common;

namespace Smartmatic.SmartCad.Telephony.SIP
{
    public partial class SIPConfigurationControl : UserControl, IConfigurationControl
    {
        public SIPConfigurationControl()
        {
            InitializeComponent();
            LoadLanguage();
        }
       

        void LoadLanguage()
        {
            groupControlTelephony.Text = ResourceLoader.GetString2("Telephony");
            labelExServerIP.Text = ResourceLoader.GetString2("Server") + ":*";
        }

        #region IConfigurationControl Members

        public event Parameters_ChangeEvent FulFilled_Change;

        public void SetProperties(Dictionary<string, string> properties)
        {
            foreach (string propName in properties.Keys)
            {
                switch (propName)
                {
                    case SIPProperties.MAIN_SERVER_IP:
                        textBoxExServerIP.Text = properties[propName];
                        break;
                    case SIPProperties.MAIN_SERVER_PORT:
                        textBoxExServerPort.Text = properties[propName];
                        break;
                    default:
                        break;
                }
            }
        }

        public Dictionary<string, string> GetProperties()
        {
            Dictionary<string, string> props = new Dictionary<string, string>();

            props.Add(SIPProperties.MAIN_SERVER_IP, textBoxExServerIP.Text);
            props.Add(SIPProperties.MAIN_SERVER_PORT, textBoxExServerPort.Text);

            return props;
        }

        public void SetStatisticProperties(string[] statisticProperties)
        {
            //TODO: HACER
            //textBoxExServerIpStatGen.Text = statisticProperties[0];
            //textBoxExUserStatGen.Text = statisticProperties[1];
            //textBoxExPswStatGen.Text = statisticProperties[2];
            ////Ignore [3]
            //textBoxExServerPortStatGen.Text = statisticProperties[4];
            //textBoxExServiceStatGen.Text = statisticProperties[5];
        }

        public string[] GetStatisticProperties()
        {
            string[] props = new string[8];

            //TODO: HACER
            //props[0] = textBoxExServerIpStatGen.Text;
            //props[1] = textBoxExUserStatGen.Text;
            //props[2] = textBoxExPswStatGen.Text;
            //props[3] = ""; //IGNORE [3]
            //props[4] = textBoxExServerPortStatGen.Text;
            //props[5] = textBoxExServiceStatGen.Text;

            return props;
        }
        #endregion

        bool isFullFilled = false;
        private void textBoxEx_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBoxExServerIP.Text.Trim()) ||
                string.IsNullOrEmpty(textBoxExServerPort.Text.Trim()))
            {
                if (isFullFilled && FulFilled_Change != null)
                {
                    FulFilled_Change(false);
                }
                isFullFilled = false;
            }
            else
            {
                if (!isFullFilled && FulFilled_Change != null)
                {
                    FulFilled_Change(true);
                }
                isFullFilled = true;
            }
        }
    }
}
