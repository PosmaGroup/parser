//////////////////////////////////////////////////////////////////////////
//
// IMPORTANT: DON'T EDIT THIS FILE
//
//////////////////////////////////////////////////////////////////////////


using System;
using System.Collections.Generic;
using System.Text;

namespace PortSIP
{
    public enum AUDIOCODEC_TYPE:int
    {
 	    AUDIOCODEC_G723		= 4, //8KHZ
	    AUDIOCODEC_G729		= 18, //8KHZ
	    AUDIOCODEC_PCMA		= 8, //8KHZ
	    AUDIOCODEC_PCMU		= 0, //8KHZ
	    AUDIOCODEC_GSM		= 3, //8KHZ
	    AUDIOCODEC_G722		= 9, // 16KHZ
	    AUDIOCODEC_ILBC		= 97, //8KHZ
	    AUDIOCODEC_AMRWB	= 98,	//16KHZ
	    AUDIOCODEC_SPEEX	= 99,	//8KHZ
	    AUDIOCODEC_SPEEXWB	= 100,	//16KHZ
	    AUDIOCODEC_G7221	= 121,	//16KHZ
	    AUDIOCODEC_DTMF		= 101
    }

    public enum VIDEOCODEC_TYPE:int
    {
        VIDEOCODEC_H263 = 34,
        VIDEOCODEC_H263_1998 = 115,
        VIDEOCODEC_H264 = 125
    }

    

    public enum VIDEO_RESOLUTION:int
    {
	    VIDEO_QCIF	=	1,		//	176X144		- for H263, H263-1998, H264
	    VIDEO_CIF	=	2,		//	352X288		- for H263, H263-1998, H264
	    VIDEO_VGA	=	3,		//	640X480		- for H264 only
	    VIDEO_SVGA	=	4,		//	800X600		- for H264 only
	    VIDEO_XVGA	=	5,		//	1024X768	- for H264 only
	    VIDEO_720P	=	6,		//	1280X720	- for H264 only
        VIDEO_QVGA	=	7		//	320X240 	- for H264 only
    }



    public enum VIDEO_QUALITY:int
    {
	    VIDEO_QUALITY_LEVEL1	=	1,	// 2Mbps   Best
	    VIDEO_QUALITY_LEVEL2	=	2,	// 1Mbps 
	    VIDEO_QUALITY_LEVEL3	=	3,	// 600kbps
	    VIDEO_QUALITY_LEVEL4	=	4,	// 320kbps
	    VIDEO_QUALITY_LEVEL5	=	5,	// 120kbps
	    VIDEO_QUALITY_LEVEL6	=	6	//  60kbps worst
    }




    public enum AUDIO_RECORDING_FILEFORMAT:int
    {
	    FILEFORMAT_WAVE = 1,
	    FILEFORMAT_OGG,
        FILEFORMAT_MP3
    }



    public enum VIDEO_RECORDING_MODE:int
    {
	    VIDEOINFO_NONE = 0,		//	Disable video record
	    VIDEOINFO_ONLY_LOCAL,	//	Record local video only
	    VIDEOINFO_ONLY_REMOTE,	//	Record remote video only
	    VIDEOINFO_BOTH,			//	Record both of local and remote video
	    VIDEOINFO_MULTI			//  Record multi video, conference mode
    }



    public enum AUDIOSTREAM_CALLBACK_MODE:int
    {
	    AUDIOSTREAM_NONE = 0,				//	Disable audio stream callback
	    AUDIOSTREAM_LOCAL,					//	Local audio stream callback
	    AUDIOSTREAM_REMOTE,					//	Remote audio stream callback
	    AUDIOSTREAM_LOCAL_REMOTE_MIX,		//	The mixed stream of local sound card captured audio and received remote audio  
	    AUDIOSTREAM_LOCAL_REMOTE_SEPARATE,	//	Local sound card captured audio stream and received remote audio stream
    }


    public enum VIDEOSTREAM_CALLBACK_MODE:int
    {
	    VIDEOSTREAM_NONE = 0,	// Disable video stream callback
	    VIDEOSTREAM_LOCAL,		// Local video stream callback
	    VIDEOSTREAM_REMOTE,		// Remote video stream callback
	    VIDEOSTREAM_BOTH,		// Both of local and remote video stream callback
    }



    public enum PRESENCE_MODE:int
    {
	    PRESENCE_P2P,
	    PRESENCE_AGENT
    }



    // Log level
    public enum PORTSIP_LOG_LEVEL : int
    {
        PORTSIP_LOG_NONE = -1,
        PORTSIP_LOG_DEBUG = 0,
        PORTSIP_LOG_ERROR = 1,
        PORTSIP_LOG_WARNING = 2,
        PORTSIP_LOG_INFO = 3
    }






    // SRTP Policy
    public enum SRTP_POLICY:int
    {
	    SRTP_POLICY_NONE = 0,	// No use SRTP
	    SRTP_POLICY_FORCE,		// All calls must use SRTP
	    SRTP_POLICY_PREFER		// Top priority to use SRTP
    }



    // Transport
    public enum TRANSPORT_TYPE:int
    {
	    TRANSPORT_UDP = 0,
	    TRANSPORT_TLS,
	    TRANSPORT_TCP
    }


    // virtual device id
    public enum VIRTUAL_DEVICE:int
    {
        PORTSIP_VIRTUAL_CAMERA_ID = 5000,
        PORTSIP_VIRTUAL_AUDIO_ID = 6000
    }


}
