﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using PortSIP;
using System.Runtime.InteropServices;
using SmartCadCore.Common;
using SmartCadCore.Core;

namespace Smartmatic.SmartCad.Telephony.SIP
{
    public class TelephonyManagerSIP : TelephonyManagerAbstract
    {
        #region Fields
        /// <summary>
        /// Number of lines to hold
        /// </summary>
        private const int MAX_LINES = 9; // Maximum lines
        /// <summary>
        /// Number of extensions connected in the softphone.
        /// </summary>
        private const int LINE_BASE = 1;
        /// <summary>
        /// Status of each line.
        /// </summary>
        private Session[] callSessions = new Session[MAX_LINES];
        /// <summary>
        /// Class that manage the Sip Core Methods
        /// </summary>
        private PortSIPCore core;
        /// <summary>
        /// Class that manage the Devices (cameras, microphone, speakers)
        /// </summary>
        private DeviceManager deviceManager;
        /// <summary>
        /// Transport type to be used.
        /// </summary>
        private TRANSPORT_TYPE transport = TRANSPORT_TYPE.TRANSPORT_UDP;
        /// <summary>
        /// SRTP type to be used.
        /// </summary>
        private SRTP_POLICY srtp = SRTP_POLICY.SRTP_POLICY_NONE;
        #endregion

        #region Properties
        /// <summary>
        /// Status of the SIP
        /// </summary>
        private bool Initied { get; set; }
        /// <summary>
        /// Status of the connection of the SIP
        /// </summary>
        private bool Logged { get; set; }
        /// <summary>
        /// Current line.
        /// </summary>
        private int CurrentLine { get; set; }
        /// <summary>
        /// Indicate if the caller is in the ready state or busy state
        /// </summary>
        private bool IsReady { get; set; }
        /// <summary>
        /// Indicate if the call can be automatically answer
        /// </summary>
        private bool AutoAnswer { get; set; }
        /// <summary>
        /// Status of each line.
        /// </summary>
        private Session[] CallSessions
        {
            get 
            {
                return callSessions;
            }
        }
        /// <summary>
        /// Indicates if the call has to be recorded.
        /// </summary>
        private bool AutoRecord { get; set; }

        protected string Domain
        {
            get { return configurationProperties[CommonProperties.Domain].ToString(); }
        }

        protected string AgentId
        {
            get { return configurationProperties[CommonProperties.AgentId].ToString(); }
        }

        protected string Queue
        {
            get { return configurationProperties[CommonProperties.Queue].ToString(); }
        }

        protected string MainServerIP
        {
            get { return configurationProperties[SIPProperties.MAIN_SERVER_IP].ToString(); }
        }

        protected string MainServerPort
        {
            get { return configurationProperties[SIPProperties.MAIN_SERVER_PORT].ToString(); }
        }

        #endregion

        public TelephonyManagerSIP(Hashtable configProperties)
            : base(configProperties)
        {
            Init();
            AutoRecord = true;
            Login(AgentId, "", true);
        }

        #region TelephonyManagerAbstract

        public override void Call(string number)
        {
            throw new NotImplementedException();
        }

        public override string TestCTIState()
        {
            try
            {
                core.addExtensionHeader("Test-Internally", "Test-Internally");
                core.clearAddExtensionHeaders();
            }
            catch
            {
                throw;
            }
            return "Connection_Success";
        }

        protected override void Init()
        {
            for (int i = 0; i < MAX_LINES; ++i)
            {
                CallSessions[i] = new Session();
                CallSessions[i].Reset();
            }

            Initied = false;
            Logged = false;
            CurrentLine = LINE_BASE;

            deviceManager = new DeviceManager();
            deviceManager.initialize();
        }

        public override void Login(string agentId, string agentPassword, bool isReady)
        {
            if (Initied == true)
            {
                throw new Exception(ResourceLoader.GetString2("AlreadyLogIn"));
            }

            Random rd = new Random();
            int LocalSIPPort = rd.Next(1000, 5000) + 4000; // Generate the random port for SIP

            StringBuilder localIP = new StringBuilder();
            localIP.Length = 64;
            deviceManager.getLocalIP(0, localIP, 64);

            //
            // Create the class instance of PortSIP SDK wrapper 
            //

            core = new PortSIPCore(0, this);

            //
            // Create and set the SIP callback handers, this MUST called before
            // core.initialize();
            //
            core.createCallbackHandlers();


            Boolean state = core.initialize(transport,
                                        PORTSIP_LOG_LEVEL.PORTSIP_LOG_NONE,
                                        MAX_LINES,
                                        Extension,
                                        UserLogin,
                                        Extension,
                                        ExtensionPassword,
                                        agentId,
                                        localIP.ToString(),
                                        LocalSIPPort,
                                        Domain,
                                        MainServerIP,
                                        int.Parse(MainServerPort),
                                        "",
                                        0,
                                        "",
                                        0);

            if (state == false)
            {
                core.releaseCallbackHandlers();
                throw new Exception(ResourceLoader.GetString2("InitializeFailed"));
            }

            Initied = true;

            ConfigureSRTPType();

            string licenseKey = "PORTSIP_TEST_LICENSE";
            core.setLicenseKey(licenseKey);

            core.enableAEC(true);

            UpdateAudioCodecs();

            InitSettings();

            if (core.registerServer(90) == false)
            {
                Initied = false;
                core.unInitialize();

                core.releaseCallbackHandlers();

                throw new Exception(ResourceLoader.GetString2("RegistryFailed"));
            }
            IsReady = isReady;
            Logged = true;
        }

        public override void Logout()
        {
            Unregister();
        }

        public override void Hold()
        {
            if (Initied == false || Logged == false)
            {
                return;
            }

            if (CallSessions[CurrentLine].State == false)
            {
                return;
            }

            if (CallSessions[CurrentLine].HoldState == true)
            {
                return;
            }

            core.hold(CallSessions[CurrentLine].Id);
            CallSessions[CurrentLine].HoldState = true;
        }

        public override void SetReadyStatus(bool ready)
        {
            IsReady = ready;
            if(IsReady)
                OnAgentReady(new AgentReadyEventArgs(Extension, AgentId, Queue, TimeSpan.FromTicks(DateTime.Today.Ticks), ""));
            else
                OnAgentNotReady(new AgentNotReadyEventArgs(Extension, AgentId, Queue, TimeSpan.FromTicks(DateTime.Today.Ticks), ""));
        }

        public override bool GetReadyStatus()
        {
            return IsReady;
        }

        public override void Answer()
        {
            if (Initied == false || Logged == false)
            {
                return;
            }

            //if (CallSessions[CurrentLine].ReceivingCallState == false)
            //{
            //    throw new Exception(ResourceLoader.GetString2("NotIncomingCall"));
            //}

            CallSessions[CurrentLine].ReceivingCallState = false;
            CallSessions[CurrentLine].State = true;
            
            if (core.answerCall(CallSessions[CurrentLine].Id) == true)
            {
                StartRecording();
                OnCallEstablished(new CallEstablishedEventArgs("", AgentId, Queue, TimeSpan.FromTicks(DateTime.Today.Ticks)));
            }
            else
            {
                CallSessions[CurrentLine].Reset();
                //Call Failed
            }
        }

        public override void Release()
        {
            if (Initied == false || Logged == false)
            {
                return;
            }

            if (CallSessions[CurrentLine].ReceivingCallState == true)
            {
                string reason = "Busy here";
                core.rejectCall(CallSessions[CurrentLine].Id, 486, reason);
                CallSessions[CurrentLine].Reset();

                return;
            }

            if (CallSessions[CurrentLine].State == true)
            {
                core.terminateCall(CallSessions[CurrentLine].Id);
                StopRecording();
                CallSessions[CurrentLine].Reset();
                OnCallReleased(new CallReleasedEventArgs("", AgentId, Queue, TimeSpan.FromTicks(DateTime.Today.Ticks)));
            }
        }

        public override void Transfer()
        {
            string number = "0";
            if (Initied == false || Logged == false)
            {
                return;
            }

            if (CallSessions[CurrentLine].State == false)
            {
                throw new Exception(ResourceLoader.GetString2("NotCallActive"));
            }

            if (core.refer(CallSessions[CurrentLine].Id, number) == false)
            {
                //Transfer Complete
            }
            else
            {
                //Transfer failed
            }
        }

        public override void ParkCall(string extensionParking, string parkId)
        {
            throw new NotImplementedException();
        }

        public override void Conference()
        {
            throw new NotImplementedException();
        }

        public override void Close()
        {
            deviceManager.unInitialize();
            Unregister();
        }

        public override void RecoverConnection()
        {
            try
            {
                Close();
                Init();
                Login(AgentId,"", true);
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
                throw ex;
            }
        }
        #endregion

        #region Methods
        private void ConfigureAudioInputs()
        {
            int deviceCount = deviceManager.getAudioInputDeviceNums();

            for (int i = 0; i < deviceCount; ++i)
            {
                StringBuilder deviceName = new StringBuilder();
                deviceName.Length = 1024;

                deviceManager.getAudioInputDeviceName(i, deviceName, 1024);

                deviceName.Length = 0;
            }
        }

        private void ConfigureAudioOutput()
        {
            int deviceCount = deviceManager.getAudioOutputDeviceNums();
            for (int i = 0; i < deviceCount; ++i)
            {
                StringBuilder deviceName = new StringBuilder();
                deviceName.Length = 1024;

                deviceManager.getAudioOutputDeviceName(i, deviceName, 1024);

                deviceName.Length = 0;
            }
        }

        private void ConfigureSRTPType()
        {
            if (Initied == false)
            {
                return;
            }

            core.setSrtpPolicy(srtp);
        }

        private void UpdateAudioCodecs()
        {
            if (Initied == false)
            {
                return;
            }

            core.clearAudioCodec();
            core.addAudioCodec(AUDIOCODEC_TYPE.AUDIOCODEC_G729);
            core.addAudioCodec(AUDIOCODEC_TYPE.AUDIOCODEC_G7221);
            core.addAudioCodec(AUDIOCODEC_TYPE.AUDIOCODEC_PCMU);
            //core.addAudioCodec(AUDIOCODEC_TYPE.AUDIOCODEC_PCMA);
            //core.addAudioCodec(AUDIOCODEC_TYPE.AUDIOCODEC_ILBC);
            //core.addAudioCodec(AUDIOCODEC_TYPE.AUDIOCODEC_GSM);
            //core.addAudioCodec(AUDIOCODEC_TYPE.AUDIOCODEC_G723);
            //core.addAudioCodec(AUDIOCODEC_TYPE.AUDIOCODEC_G722);
            //core.addAudioCodec(AUDIOCODEC_TYPE.AUDIOCODEC_SPEEX);
            //core.addAudioCodec(AUDIOCODEC_TYPE.AUDIOCODEC_AMRWB);
            //core.addAudioCodec(AUDIOCODEC_TYPE.AUDIOCODEC_SPEEXWB);
            core.addAudioCodec(AUDIOCODEC_TYPE.AUDIOCODEC_DTMF);
        }

        private void InitSettings()
        {
            if (Initied == false)
            {
                return;
            }

            core.enableDTMFOfRFC2833(101); // Use DTMF as RTP event - RFC2833
            core.setDtmfSamples(160);

            //core.enableDtmfOfInfo(); // Use DTMF as SIP INFO method

            core.enableAEC(true);

            core.enableVAD(false);

            core.enableCNG(false);

            core.enableAGC(true);
        }

        private void Unregister()
        {
            if (Initied == false)
            {
                return;
            }

            for (int i = LINE_BASE; i < MAX_LINES; ++i)
            {
                if (CallSessions[i].ReceivingCallState == true)
                {
                    string reason = "Busy here";
                    core.rejectCall(CallSessions[i].Id, 486, reason);
                }
                else if (CallSessions[i].State == true)
                {
                    core.terminateCall(CallSessions[i].Id);
                }

                CallSessions[i].Reset();
            }

            if (Logged == true)
            {
                core.unRegisterServer();
                Logged = false;
            }

            if (Initied == true)
            {
                core.unInitialize();
                //
                // MUST called after core.unInitliaze();
                //
                core.releaseCallbackHandlers();
                Initied = false;
            }
            CurrentLine = LINE_BASE;
        }

        private void StartRecording()
        {
            if (AutoRecord)
            {
                string filePath = SmartCadConfiguration.AudioRecordingFolder;
                string fileName = Extension;

                AUDIO_RECORDING_FILEFORMAT recordFileFormat = AUDIO_RECORDING_FILEFORMAT.FILEFORMAT_MP3;

                //  Set record file path and name
                core.setAudioRecordPathName(filePath, fileName, true, recordFileFormat);

                //  Start recording
                core.startAudioRecording();
            }
        }

        private void StopRecording()
        {
            if (AutoRecord)
            {
                core.stopAudioRecording();
            }
        }
        #endregion

        #region SIPEvents
        public Int32 onRegisterSuccess(Int32 callbackObject, Int32 statusCode, String statusText)
        {
            Logged = true;
            OnAgentRegistered(new AgentRegisteredEventArgs(Extension, AgentId, Queue, TimeSpan.FromTicks(DateTime.Today.Ticks)));
            OnAgentLogin(new AgentLoginEventArgs(Extension, AgentId, Queue, TimeSpan.FromTicks(DateTime.Today.Ticks)));
            return 0;
        }


        public Int32 onRegisterFailure(Int32 callbackObject, Int32 statusCode, String statusText)
        {
            Logged = false;
            throw new TelephonyException(ResourceLoader.GetString2("UnableLoginInTerminal"), null);
            return 0;
        }


        public Int32 onInviteIncoming(Int32 callbackObject,
                                             Int32 sessionId,
                                             String caller,
                                             String callerDisplayName,
                                             String callee,
                                             String calleeDisplayName,
                                             String audioCodecName,
                                             String videoCodecName,
                                             Boolean hasVideo)
        {
            int i = 0;
            bool state = false;
            string Text = string.Empty;

            for (i = LINE_BASE; i < MAX_LINES; ++i)
            {
                if (CallSessions[i].State == false && CallSessions[i].ReceivingCallState == false)
                {
                    state = true;
                    CallSessions[i].ReceivingCallState = true;
                    break;
                }
            }

            if (state == false)
            {
                string reason = "Busy here";
                core.rejectCall(sessionId, 486, reason);

                return 0;
            }

            if (IsReady == false)
            {
                string reason = "Busy here";
                core.rejectCall(sessionId, 486, reason);
                CallSessions[i].Reset();
                return 0;
            }

            CallSessions[i].Id = sessionId;

            bool needIgnoreAutoAnswer = false;
            int j = 0;

            for (j = LINE_BASE; j < MAX_LINES; ++j)
            {
                if (CallSessions[j].State == true)
                {
                    needIgnoreAutoAnswer = true;
                    break;
                }
            }

            if (needIgnoreAutoAnswer == false && AutoAnswer == true)
            {
                CallSessions[i].ReceivingCallState = false;
                CallSessions[i].State = true;

                core.answerCall(CallSessions[i].Id);

                return 0;
            }
            int position = caller.IndexOf("@");
            if (position > -1)
                caller = caller.Substring(0, position);
            OnCallRinging(new CallRingingEventArgs(caller, AgentId, "", TimeSpan.FromTicks(DateTime.Now.Ticks)));

            return 0;
        }

        public Int32 onInviteTrying(Int32 callbackObject, Int32 sessionId, String caller, String callee)
        {
            int i = 0;
            bool state = false;

            for (i = LINE_BASE; i < MAX_LINES; ++i)
            {
                if (CallSessions[i].Id == sessionId)
                {
                    state = true;
                    break;
                }
            }

            if (state == false)
            {
                return 0;
            }

            return 0;
        }

        public Int32 onInviteRinging(Int32 callbackObject,
                                            Int32 sessionId,
                                            Boolean hasEarlyMedia,
                                            Boolean hasVideo,
                                            String audioCodecName,
                                            String videoCodecName)
        {
            int i = 0;
            bool state = false;

            for (i = LINE_BASE; i < MAX_LINES; ++i)
            {
                if (CallSessions[i].Id == sessionId)
                {
                    state = true;
                    break;
                }
            }

            if (state == false)
            {
                return 0;
            }

            if (hasEarlyMedia == false)
            {
                // Hasn't the early media, you must play the local WAVE  file for ringing tone
                // play the wav file for ring tone
            }

            return 0;
        }


        public Int32 onInviteAnswered(Int32 callbackObject,
                                             Int32 sessionId,
                                             Boolean hasVideo,
                                             Int32 statusCode,
                                             String statusText,
                                             String audioCodecName,
                                             String videoCodecName)
        {
            if (hasVideo == true)
            {
                // This incoming call has video SDP
            }
            else
            {
                // This incoming call hasn't the video SDP
            }


            int i = 0;
            bool state = false;

            for (i = LINE_BASE; i < MAX_LINES; ++i)
            {
                if (CallSessions[i].Id == sessionId)
                {
                    state = true;
                    break;
                }
            }

            if (state == false)
            {
                return 0;
            }

            CallSessions[i].State = true;
            StartRecording();
            OnCallEstablished(new CallEstablishedEventArgs("", AgentId, Queue, TimeSpan.FromTicks(DateTime.Today.Ticks)));
            return 0;
        }


        public Int32 onInviteFailure(Int32 callbackObject, Int32 sessionId, Int32 statusCode, String statusText)
        {
            int i = 0;
            bool state = false;

            for (i = LINE_BASE; i < MAX_LINES; ++i)
            {
                if (CallSessions[i].Id == sessionId)
                {
                    state = true;
                    break;
                }
            }

            if (state == false)
            {
                return 0;
            }

            CallSessions[i].Reset();

            return 0;
        }


        public Int32 onInviteClosed(Int32 callbackObject, Int32 sessionId)
        {
            int i = 0;
            bool state = false;

            for (i = LINE_BASE; i < MAX_LINES; ++i)
            {
                if (CallSessions[i].Id == sessionId)
                {
                    state = true;
                    break;
                }
            }

            if (state == false)
            {
                return 0;
            }

            StopRecording();

            if (state)
            {
                OnCallReleased(new CallReleasedEventArgs("", AgentId, Queue, TimeSpan.FromTicks(DateTime.Today.Ticks)));
            }
            else
            {
                SetReadyStatus(false);
                OnCallAbandoned(new CallAbandonedEventArgs("", AgentId, Queue, TimeSpan.FromTicks(DateTime.Today.Ticks)));
            }

            CallSessions[i].Reset();
            return 0;
        }

        public Int32 onInviteUpdated(Int32 callbackObject,
                                            Int32 sessionId,
                                            Boolean hasVideo,
                                            String audioCodecName,
                                            String videoCodecName
                                            )
        {
            int i = 0;
            bool state = false;

            for (i = LINE_BASE; i < MAX_LINES; ++i)
            {
                if (CallSessions[i].Id == sessionId)
                {
                    state = true;
                    break;
                }
            }

            if (state == false)
            {
                return 0;
            }

            return 0;
        }


        public Int32 onInviteUASConnected(Int32 callbackObject, Int32 sessionId, Int32 statusCode, String statusText)
        {
            return 0;
        }


        public Int32 onInviteUACConnected(Int32 callbackObject, Int32 sessionId, Int32 statusCode, String statusText)
        {
            return 0;
        }


        public Int32 onInviteBeginingForward(Int32 callbackObject, String forwardingTo)
        {
            return 0;
        }

        public Int32 onRemoteHold(Int32 callbackObject, Int32 sessionId)
        {
            int i = 0;
            bool state = false;

            for (i = LINE_BASE; i < MAX_LINES; ++i)
            {
                if (CallSessions[i].Id == sessionId)
                {
                    state = true;
                    break;
                }
            }

            if (state == false)
            {
                return 0;
            }

            return 0;
        }

        public Int32 onRemoteUnHold(Int32 callbackObject, Int32 sessionId)
        {
            int i = 0;
            bool state = false;

            for (i = LINE_BASE; i < MAX_LINES; ++i)
            {
                if (CallSessions[i].Id == sessionId)
                {
                    state = true;
                    break;
                }
            }

            if (state == false)
            {
                return 0;
            }

            return 0;
        }

        public Int32 onTransferTrying(Int32 callbackObject, Int32 sessionId, String referTo)
        {
            int i = 0;
            bool state = false;

            for (i = LINE_BASE; i < MAX_LINES; ++i)
            {
                if (CallSessions[i].Id == sessionId)
                {
                    state = true;
                    break;
                }
            }

            if (state == false)
            {
                return 0;
            }

            return 0;
        }

        public Int32 onTransferRinging(Int32 callbackObject, Int32 sessionId, Boolean hasVideo)
        {
            int i = 0;
            bool state = false;

            for (i = LINE_BASE; i < MAX_LINES; ++i)
            {
                if (CallSessions[i].Id == sessionId)
                {
                    state = true;
                    break;
                }
            }

            if (state == false)
            {
                return 0;
            }

            return 0;
        }

        public Int32 onPASVTransferSuccess(Int32 callbackObject, Int32 sessionId, Boolean hasVideo)
        {
            int i = 0;
            bool state = false;

            for (i = LINE_BASE; i < MAX_LINES; ++i)
            {
                if (CallSessions[i].Id == sessionId)
                {
                    state = true;
                    break;
                }
            }

            if (state == false)
            {
                return 0;
            }

            return 0;
        }

        public Int32 onPASVTransferFailure(Int32 callbackObject, Int32 sessionId, Int32 statusCode, String statusText)
        {
            int i = 0;
            bool state = false;

            for (i = LINE_BASE; i < MAX_LINES; ++i)
            {
                if (CallSessions[i].Id == sessionId)
                {
                    state = true;
                    break;
                }
            }

            if (state == false)
            {
                return 0;
            }

            return 0;
        }

        public Int32 onACTVTransferSuccess(Int32 callbackObject, Int32 sessionId)
        {
            int i = 0;
            bool state = false;

            for (i = LINE_BASE; i < MAX_LINES; ++i)
            {
                if (CallSessions[i].Id == sessionId)
                {
                    state = true;
                    break;
                }
            }

            if (state == false)
            {
                return 0;
            }

            // The ACTIVE Transfer success, then reset currently call.
            CallSessions[i].Reset();

            return 0;
        }

        public Int32 onACTVTransferFailure(Int32 callbackObject, Int32 sessionId, Int32 statusCode, String statusText)
        {
            int i = 0;
            bool state = false;

            for (i = LINE_BASE; i < MAX_LINES; ++i)
            {
                if (CallSessions[i].Id == sessionId)
                {
                    state = true;
                    break;
                }
            }

            if (state == false)
            {
                return 0;
            }

            return 0;
        }

        public Int32 onRecvPagerMessage(Int32 callbackObject, String from, String fromDisplayName, StringBuilder message)
        {
            return 0;
        }

        public Int32 onSendPagerMessageSuccess(Int32 callbackObject,
                                                      String caller,
                                                      String callerDisplayName,
                                                      String callee,
                                                      String calleeDisplayName
                                                     )
        {
            return 0;
        }



        public Int32 onSendPagerMessageFailure(Int32 callbackObject,
                                                       String caller,
                                                      String callerDisplayName,
                                                      String callee,
                                                      String calleeDisplayName,
                                                      Int32 statusCode,
                                                      String statusText
                                                     )
        {
            return 0;
        }

        public Int32 onArrivedSignaling(Int32 callbackObject, Int32 sessionId, StringBuilder signaling)
        {
            return 0;
        }

        public Int32 onWaitingVoiceMessage(Int32 callbackObject,
                                                  String messageAccount,
                                                  Int32 urgentNewMessageCount,
                                                  Int32 urgentOldMessageCount,
                                                  Int32 newMessageCount,
                                                  Int32 oldMessageCount)
        {
            return 0;
        }


        public Int32 onWaitingFaxMessage(Int32 callbackObject,
                                                  String messageAccount,
                                                  Int32 urgentNewMessageCount,
                                                  Int32 urgentOldMessageCount,
                                                  Int32 newMessageCount,
                                                  Int32 oldMessageCount)
        {
            return 0;
        }


        public Int32 onRecvDtmfTone(Int32 callbackObject, Int32 sessionId, Int32 tone)
        {
            int i = 0;
            bool state = false;

            for (i = LINE_BASE; i < MAX_LINES; ++i)
            {
                if (CallSessions[i].Id == sessionId)
                {
                    state = true;
                    break;
                }
            }

            if (state == false)
            {
                return 0;
            }

            string DTMFTone = tone.ToString();
            if (DTMFTone == "10")
            {
                DTMFTone = "*";
            }
            else if (DTMFTone == "11")
            {
                DTMFTone = "#";
            }

            return 0;
        }


        public Int32 onPresenceRecvSubscribe(Int32 callbackObject,
                                                    Int32 subscribeId,
                                                    String from,
                                                    String fromDisplayName,
                                                    String subject)
        {
            return 0;
        }


        public Int32 onPresenceOnline(Int32 callbackObject, String from, String fromDisplayName, String stateText)
        {
            return 0;
        }

        public Int32 onPresenceOffline(Int32 callbackObject, String from, String fromDisplayName)
        {
            return 0;
        }

        public Int32 onRecvOptions(Int32 callbackObject, StringBuilder optionsMessage)
        {
            return 0;
        }

        public Int32 onRecvInfo(Int32 callbackObject, long sessionId, StringBuilder infoMessage)
        {
            int i = 0;
            bool state = false;
            for (i = LINE_BASE; i < MAX_LINES; ++i)
            {
                if (CallSessions[i].Id == sessionId)
                {
                    state = true;
                    break;
                }
            }

            if (state == false)
            {
                return 0;
            }

            return 0;
        }

        public Int32 onRecvMessage(Int32 callbackObject, Int32 sessionId, StringBuilder message)
        {
            int i = 0;
            bool state = false;
            for (i = LINE_BASE; i < MAX_LINES; ++i)
            {
                if (CallSessions[i].Id == sessionId)
                {
                    state = true;
                    break;
                }
            }

            if (state == false)
            {
                return 0;
            }

            return 0;
        }

        public Int32 onRecvBinaryMessage(Int32 callbackObject,
                                        Int32 sessionId,
                                        StringBuilder message,
                                        byte[] messageBody,
                                        Int32 length)
        {

            int i = 0;
            bool state = false;
            for (i = LINE_BASE; i < MAX_LINES; ++i)
            {
                if (CallSessions[i].Id == sessionId)
                {
                    state = true;
                    break;
                }
            }

            if (state == false)
            {
                return 0;
            }

            return 0;
        }


        public Int32 onRecvBinaryPagerMessage(Int32 callbackObject,
                                              StringBuilder from,
                                              StringBuilder fromDisplayName,
                                              byte[] messageBody,
                                              Int32 length)
        {
            return 0;
        }

        public Int32 onAudioRawCallback(IntPtr callbackObject,
                                               Int32 streamType,
                                               [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 3)] byte[] data,
                                               Int32 dataLength)
        {
            // If called enableAudioStreamCallback, then this event will be fired if received  each audio RTP packet

            // data; // Audio stream data, byte array type, it's PCM data, 16000Hz, 16bit, Mono
            // streamType; // streamType is the audio stream type


            //
            // IMPORTANT: the data length is stored in dataLength parameter!!!
            //

            if (streamType == (Int32)AUDIOSTREAM_CALLBACK_MODE.AUDIOSTREAM_LOCAL)
            {
                // This is the audio stream from local sound card captured.
            }
            else if (streamType == (Int32)AUDIOSTREAM_CALLBACK_MODE.AUDIOSTREAM_REMOTE)
            {
                // This is the audio stream which received from remote side
            }
            else if (streamType == (Int32)AUDIOSTREAM_CALLBACK_MODE.AUDIOSTREAM_LOCAL_REMOTE_MIX)
            {
                // This is audio stream which mixed of local sound card captured audio and received remote audio  
            }


            return 0;
        }


        public Int32 onVideoRawCallback(IntPtr callbackObject,
                                               Int32 sessionId,
                                               Int32 width,
                                               Int32 height,
                                               [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 5)] byte[] data,
                                               Int32 dataLength)
        {

            // If called the enableVideoStreamCallback, then this event will be fired if received each video RTP packet
            // or captured local video from camera.


            // sessionId;  // The session id
            // height; // Video image height
            // width; // Video image width
            // videoStream; // Video stream data, byte array type, RGB888 format.


            //
            // IMPORTANT: the data length is stored in dataLength parameter!!!
            //

            int index = LINE_BASE;
            int i = 0;

            if (sessionId == 0)
            {
                // This is the local video stream
            }
            else
            {
                for (i = LINE_BASE; i < MAX_LINES; ++i)
                {
                    if (CallSessions[i].Id == sessionId)
                    {
                        // The video callback stream is for this session
                        index = i;
                        break;
                    }
                }
            }

            return 0;

        }



        public Int32 onPlayAviFileFinished(IntPtr callbackObject, String fileName)
        {
            return 0;
        }

        public Int32 onPlayWaveFileFinished(IntPtr callbackObject, Int32 sessionId, String fileName)
        {
            return 0;
        }

        public override void SetPhoneReport(string customCode)
        {
            throw new NotImplementedException();
        }
        #endregion
    }

    public class Session
    {
        public Session()
        {
            Reset();
        }

        public void Reset()
        {
            Id = 0;
            HoldState = false;
            State = false;
            ReceivingCallState = false;
        }

        public int Id { get; set; }
        public bool HoldState { get; set; }
        public bool State { get; set; }
        //public bool ReceivingCallState { get; set; }
        private bool receivingCallState;
        public bool ReceivingCallState
        {
            get
            {
                return receivingCallState;
            }
            set
            {
                receivingCallState = value;
            }
        }

        public override string ToString()
        {
            return "ID=" + Id.ToString() + " St=" + State.ToString() + " RCS=" + ReceivingCallState.ToString();
        }
    }
}
