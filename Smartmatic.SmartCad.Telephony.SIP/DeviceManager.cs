//////////////////////////////////////////////////////////////////////////
//
// IMPORTANT: DON'T EDIT THIS FILE
//
//////////////////////////////////////////////////////////////////////////


using System;
using System.Collections.Generic;
using System.Text;

namespace PortSIP
{
    unsafe class DeviceManager
    {

        public DeviceManager()
        {
            _deviceManagerHandle = IntPtr.Zero;
        }


        public Boolean initialize()
        {
            if (_deviceManagerHandle != IntPtr.Zero)
            {
                return false;
            }

            _deviceManagerHandle = PortSIP_NativeMethods.Device_initialize();
            if (_deviceManagerHandle == IntPtr.Zero)
            {
                return false;
            }

            return true;
        }

        public void unInitialize()
        {
            if (_deviceManagerHandle != IntPtr.Zero)
            {
                PortSIP_NativeMethods.Device_unInitialize(_deviceManagerHandle);
                _deviceManagerHandle = IntPtr.Zero;
            }
        }


 
        public Int32 getVideoDeviceNums()
        {
            if (_deviceManagerHandle == IntPtr.Zero)
            {
                return 0;
            }

            return PortSIP_NativeMethods.Device_getVideoDeviceNums(_deviceManagerHandle);
        }


        /// <summary>
        /// Note: Must special the StringBuilder.Length when call this function.
        /// for example:
        /// StringBuilder name = new StringBuilder();
        /// name.Length = 512;
        /// getVideoDeviceName(id, name);
        /// </summary>
        public  Boolean getVideoDeviceName(Int32 deviceId, StringBuilder deviceName, Int32 deviceLength)
        {
            if (_deviceManagerHandle == IntPtr.Zero)
            {
                return false;
            }

            return PortSIP_NativeMethods.Device_getVideoDeviceName(_deviceManagerHandle, deviceId, deviceName, deviceLength);
        }


        public void showCameraPropertyWindow(IntPtr parentWindow, Int32 deviceId)
        {
            if (_deviceManagerHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.Device_showCameraPropertyWindow(_deviceManagerHandle, parentWindow, deviceId);
        }



        public Boolean startVideoPreview(IntPtr hwnd,
                                                Int32 deviceId,
                                                Int32 x,
                                                Int32 y,
                                                Int32 width,
                                                Int32 height)
        {
            if (_deviceManagerHandle == IntPtr.Zero)
            {
                return false;
            }

            return PortSIP_NativeMethods.Device_startVideoPreview(_deviceManagerHandle, hwnd, deviceId, x, y, width, height);
        }



        public void stopVideoPreview()
        {
            if (_deviceManagerHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.Device_stopVideoPreview(_deviceManagerHandle);
        }


        public  Int32 getAudioInputDeviceNums()
        {
            if (_deviceManagerHandle == IntPtr.Zero)
            {
                return 0;
            }

            return PortSIP_NativeMethods.Device_getAudioInputDeviceNums(_deviceManagerHandle);
        }


        public Int32 getAudioOutputDeviceNums()
        {
            if (_deviceManagerHandle == IntPtr.Zero)
            {
                return 0;
            }

            return PortSIP_NativeMethods.Device_getAudioOutputDeviceNums(_deviceManagerHandle);
        }



        /// <summary>
        /// Note: Must special the StringBuilder.Length when call this function.
        /// for example:
        /// StringBuilder name = new StringBuilder();
        /// name.Length = 512;
        /// getAudioInputDeviceName(id, name);
        /// </summary>
        public Boolean getAudioInputDeviceName(Int32 deviceId, StringBuilder deviceName, Int32 deviceLength)
        {
            if (_deviceManagerHandle == IntPtr.Zero)
            {
                return false;
            }

            return PortSIP_NativeMethods.Device_getAudioInputDeviceName(_deviceManagerHandle, deviceId, deviceName, deviceLength);
        }


        /// <summary>
        /// Note: Must special the StringBuilder.Length when call this function.
        /// for example:
        /// StringBuilder name = new StringBuilder();
        /// name.Length = 512;
        /// getAudioOutputDeviceName(id, name);
        /// </summary>
        public Boolean getAudioOutputDeviceName(Int32 deviceId, StringBuilder deviceName, Int32 deviceLength)
        {
            if (_deviceManagerHandle == IntPtr.Zero)
            {
                return false;
            }

            return PortSIP_NativeMethods.Device_getAudioOutputDeviceName(_deviceManagerHandle, deviceId, deviceName, deviceLength);
        }


        public Int32 getAudioInputVolume(Int32 deviceId)
        {
            if (_deviceManagerHandle == IntPtr.Zero)
            {
                return 0;
            }

            return PortSIP_NativeMethods.Device_getAudioInputVolume(_deviceManagerHandle, deviceId);
        }


        public Int32 getAudioOutputVolume(Int32 deviceId)
        {
            if (_deviceManagerHandle == IntPtr.Zero)
            {
                return 0;
            }

            return PortSIP_NativeMethods.Device_getAudioOutputVolume(_deviceManagerHandle, deviceId);
        }


        public void setAudioInputVolume(Int32 deviceId, Int32 volume)
        {
            if (_deviceManagerHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.Device_setAudioInputVolume(_deviceManagerHandle, deviceId, volume);
        }


        public void setAudioOutputVolume(Int32 deviceId, Int32 volume)
        {
            if (_deviceManagerHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.Device_setAudioOutputVolume(_deviceManagerHandle, deviceId, volume);
        }



        public void startAudioPlayLoopbackTest(Int32 inputDeviceID, Int32 outputDeviceId)
        {
            if (_deviceManagerHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.Device_startAudioPlayLoopbackTest(_deviceManagerHandle, inputDeviceID, outputDeviceId);
        }


        public void stopAudioPlayLoopbackTest()
        {
            if (_deviceManagerHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.Device_stopAudioPlayLoopbackTest(_deviceManagerHandle);
        }



        /// <summary>
        /// Note: Must special the StringBuilder.Length when call this function.
        /// for example:
        /// StringBuilder ip = new StringBuilder();
        /// ip.Length = 512;
        /// getAudioOutputDeviceName(index, ip);
        /// </summary>
        public Boolean getLocalIP(Int32 index, StringBuilder ip, Int32 ipLength)
        {
            if (_deviceManagerHandle == IntPtr.Zero)
            {
                return false;
            }

            return PortSIP_NativeMethods.Device_getLocalIP(_deviceManagerHandle, index, ip, ipLength);
        }


        public void getVersion(out Int32 majorVersion, out Int32 minorVersion)
        {
            if (_deviceManagerHandle == IntPtr.Zero)
            {
                majorVersion = 0;
                minorVersion = 0;
                return;
            }

            PortSIP_NativeMethods.Device_getVersion(_deviceManagerHandle, out majorVersion, out minorVersion);
        }


        public void playLocalWaveFile(Int32 playDeviceId,
                                             String waveFile,
                                             Boolean playAsSync,
                                             Boolean loop,
                                             Int32 loopInterval)
        {
            if (_deviceManagerHandle == IntPtr.Zero)
            {
                return;
            }

            PortSIP_NativeMethods.Device_playLocalWaveFile(_deviceManagerHandle, playDeviceId, waveFile, playAsSync, loop, loopInterval);
        }





        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        private static IntPtr _deviceManagerHandle;

    }

}
