using System.Collections.Generic;
using System.Windows.Forms;
using System.Runtime.Serialization;
using System.Drawing;




using MapInfo.Windows.Controls;
using MapInfo.Mapping;
using MapInfo.Geometry;
using MapInfo.Tools;
using SmartCadCore.Common;


namespace Smartmatic.SmartCad.Map.MapXtreme
{
    public class AddPositionToolData
    {
        private Image icon;
        public delegate void FlagChangeEventHandler(object sender, System.Drawing.Point e);
        public event FlagChangeEventHandler FlagChange;
        public AddPositionToolData(MapControl mapControl)
        {
            this.mapControl = mapControl;
            this.mapControl.Map.DrawEvent += new MapDrawEventHandler(Map_DrawEvent);
            this.icon = ResourceLoader.GetImage("$Icon.LocateIncident");
        }

        void Map_DrawEvent(object sender, MapDrawEventArgs e)
        {
            if (state == AddPositionToolState.Normal)
            {
                Graphics graphics = this.mapControl.CreateGraphics();

                System.Drawing.Point clientPoint;

                this.mapControl.Map.DisplayTransform.ToDisplay(point, out clientPoint);

                if (this.mapControl.DisplayRectangle.Contains(clientPoint))
                    DrawPosition(graphics, clientPoint);
                
                graphics.Dispose();
            }
            else if (state != AddPositionToolState.Moving && state != AddPositionToolState.Deleted) 
            {
                state = AddPositionToolState.Normal;
            }
        }

        public void repaint() 
        {
            if (enabled)
            {
                Graphics graphics = this.mapControl.CreateGraphics();

                System.Drawing.Point clientPoint;

                this.mapControl.Map.DisplayTransform.ToDisplay(point, out clientPoint);

                if (this.mapControl.DisplayRectangle.Contains(clientPoint))
                    DrawPosition(graphics, clientPoint);

                graphics.Dispose();

            }
        }

        private void DrawPosition(Graphics graphics, System.Drawing.Point clientPoint)
        {
            graphics.DrawImage(icon, clientPoint.X, clientPoint.Y);
            if (FlagChange != null)
            {                
                FlagChange(this,  clientPoint);
            }
        }

        private MapControl mapControl;

        public MapControl MapControl
        {
            get
            {
                return mapControl;
            }
        }

        private DPoint point;

        public DPoint Point
        {
            get
            {
                return point;
            }
            set
            {
                point = value;
            }
        }

        private AddPositionToolState state = AddPositionToolState.NotPainted;

        public AddPositionToolState State
        {
            get { return state ; }
            set { state = value; }
        }
	

        private bool enabled;

        public bool Enabled
        {
            get
            {
                return enabled;
            }
            set
            {
                enabled = value;
            }
        }
    }

    public class AddPositionTool : MapTool
    {
        public AddPositionTool(
            AddPositionToolData addPositionToolData,
            FeatureViewer featureViewer,
            IMouseToolProperties defaultMouseToolProperties,
            IMapToolProperties defaultMapToolProperties
            )
            : base(featureViewer, defaultMouseToolProperties, defaultMapToolProperties)
        {
            this.addPositionToolData = addPositionToolData;
        }

        protected AddPositionTool(
            SerializationInfo info,
            StreamingContext context
            )
            : base(info, context)
        {
        }

        public override void OnMouseDown(object sender, MouseEventArgs mea)
        {
            if (this.addPositionToolData.State == AddPositionToolState.Normal || this.addPositionToolData.State == AddPositionToolState.NotPainted)
            {
                base.OnMouseDown(sender, mea);

                DPoint point = new DPoint();
                addPositionToolData.MapControl.Map.DisplayTransform.FromDisplay(mea.Location, out point);
                addPositionToolData.Point = point;
                addPositionToolData.Enabled = true;
                GeoPoint geoPoint = new GeoPoint(point.x, point.y);

                addPositionToolData.MapControl.Refresh();
            }
        }

        private AddPositionToolData addPositionToolData;

        public AddPositionToolData AddPositionToolData
        {
            get
            {
                return addPositionToolData;
            }
        }
    }
}
