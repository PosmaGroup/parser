using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading;
using System.Collections;
using System.Xml;

using MapInfo.Data;
using MapInfo.Mapping;
using MapInfo.Engine;
using MapInfo.Persistence;
using SmartCadCore.Common;
using SmartCadCore.Core;


namespace Smartmatic.SmartCad.Map.MapXtreme
{
    public class MXSession : IDisposable
    {
        public Dictionary<string, IList> layersInfo;
        public Dictionary<string, string> layersQuery;

        #region Delegates

        #region General Delegates

        public delegate void SimpleDelegate();

        public delegate IList SimpleListDelegate();

        #endregion

        #region Geo Delegates

        public delegate IList<GeoAddress> SearchDelegate(GeoAddress address);

        #endregion

        #endregion

        #region Fields

        private MapInfo.Mapping.Map map;

        private DelegateThreadMap delegateThread;

        #endregion

        #region Constructors


        public MXSession(Stream workspaceStream, string tabPrefix)
        {
            this.InitializeComponents();

            try
            {

                WorkSpaceLoader workspaceLoader = new WorkSpaceLoader(workspaceStream);

                workspaceLoader = MXSession.FormatWorkSpace(workspaceLoader, tabPrefix);

                this.delegateThread.CallDelegate(new SimpleDelegate(delegate
                {
                    workspaceLoader.Load(this.map);
                }), null);

                Thread.Sleep(100);
            }
            catch (Exception ex)
            {
                throw new GisException(ex.Message, ex);
            }
        }

        public static WorkSpaceLoader FormatWorkSpace(WorkSpaceLoader loader, string tabPrefix)
        { 
            if (loader != null)
            {
                XmlElement element = loader.WorkSpaceDoc["WorkSpace"]["DataSourceDefinitionSet"];

                if (element != null && element.HasChildNodes)
                {
                    foreach (XmlNode node in element.ChildNodes)
                    {
                        if (node.Name.Equals("TABFileDataSourceDefinition"))
                        {
                            node["FileName"].InnerText = tabPrefix + node["FileName"].InnerText;
                        }
                    }
                }

            }

            return loader;
        }

        #endregion

        #region IDisposable

        private bool disposed = false;

        public void Dispose()
        {
            Dispose(true);
            
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (delegateThread != null)
                        delegateThread.Stop();
                }

                // Call the appropriate methods to clean up
                // unmanaged resources here.
                
                disposed = true;
            }
        }

        ~MXSession()
        {
            Dispose(false);
        }

        #endregion

        #region Properties

        public MapInfo.Mapping.Map Map
        {
            get
            {
                return map;
            }
        }

        #endregion

        #region MXMethods

        private void InitializeComponents()
        {
            this.map = Session.Current.MapFactory.CreateEmptyMap(new System.Drawing.Size(100, 100));
            this.delegateThread = new DelegateThreadMap();
            this.delegateThread.Start();
        }
        
        #endregion

        #region IGisSession Members
        
        public new IList<GeoAddress> Search(GeoAddress address)
        {
            return (IList<GeoAddress>)this.delegateThread.CallDelegate(new SearchDelegate(SearchHelp), new object[] { address });
        }

        #endregion

        #region Search Methods
        private IList<GeoAddress> SearchHelp(GeoAddress address)
        {
            IList<GeoAddress> result = new List<GeoAddress>();
            if (CheckAddressEmpty(address))
                return result;
            address = StandarizeAddres(address);
            if (string.IsNullOrEmpty(address.Zone) && string.IsNullOrEmpty(address.Street) && string.IsNullOrEmpty(address.Reference))
                result = SearchForAllLayers(address);
            else
                result = SearchBySection(address);

            return result;
        }
        /// <summary>
        /// Convert the address to an standart format
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        private GeoAddress StandarizeAddres(GeoAddress address)
        {
            GeoAddress retval = new GeoAddress();
            retval.FullText = TransformSingleString(address.FullText);
            retval.More = TransformSingleString(address.More);
            retval.Reference = TransformSingleString(address.Reference);
            retval.Street = TransformSingleString(address.Street);
            retval.Zone = TransformSingleString(address.Zone);
            return retval;
        }

        /// <summary>
        /// Remove accents and extra-spaces.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        private string TransformSingleString(string text)
        {
            if (!string.IsNullOrEmpty(text))
            {
                String[] arr = ApplicationUtil.GetStringWithoutAccent(text).Split(' ');
                return string.Join(" ", arr);
            }
            else
                return text;
        }

        /// <summary>
        /// Check if GeoAddress is empty
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        private bool CheckAddressEmpty(GeoAddress address)
        {
            return string.IsNullOrEmpty(address.Zone) && string.IsNullOrEmpty(address.Street) && string.IsNullOrEmpty(address.Reference) && string.IsNullOrEmpty(address.FullText);
        }

        /// <summary>
        /// Search By Section 
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        private IList<GeoAddress> SearchBySection(GeoAddress address)
        {
            List<GeoAddress> result = new List<GeoAddress>();
            if (string.IsNullOrEmpty(address.Zone) == false)
                result.AddRange(FindFeaturesByZone(address));
            if (string.IsNullOrEmpty(address.Street) == false)
                result.AddRange(FindFeaturesByStreet(address));
            if (string.IsNullOrEmpty(address.Reference) == false)
                result.AddRange(FindFeatureByReference(address));
            return result;
        }

        private List<GeoAddress> FindFeatureByReference(GeoAddress address)
        {
            List<GeoAddress> result = new List<GeoAddress>();
            foreach (string layer in layersInfo.Keys)
            {
                foreach (MapColumnElement col in layersInfo[layer])
                {
                    if (!IsZoneColumn(col) && !IsStreetColumn(col) && col.Search.ToLower() == bool.TrueString.ToLower())
                    {
                        IResultSetFeatureCollection resultSet = GetResultSetFeatureFromColumn(layer, col, address.Reference);
                        result.AddRange(TransformFeatures(resultSet, layer));
                    }
                }
            }
            return result;
        }

        private List<GeoAddress> FindFeaturesByStreet(GeoAddress address)
        {
            List<GeoAddress> result = new List<GeoAddress>();
            foreach (string layer in layersInfo.Keys)
            {
                foreach (MapColumnElement col in layersInfo[layer])
                {
                    if (IsStreetColumn(col))
                    {
                        IResultSetFeatureCollection resultSet = GetResultSetFeatureFromColumn(layer, col, address.Street);
                        result.AddRange(TransformFeatures(resultSet, layer));
                    }
                }
            }
            return result;
        }

        private static bool IsStreetColumn(MapColumnElement col)
        {
            return !string.IsNullOrEmpty(col.IsStreet) && col.IsStreet.ToLower() == bool.TrueString.ToLower();
        }

        private List<GeoAddress> FindFeaturesByZone(GeoAddress address)
        {
            List<GeoAddress> result = new List<GeoAddress>();
            foreach (string layer in layersInfo.Keys)
            {
                foreach (MapColumnElement col in layersInfo[layer])
                {
                    if (IsZoneColumn(col))
                    {
                        IResultSetFeatureCollection resultSet = GetResultSetFeatureFromColumn(layer, col, address.Zone);
                        result.AddRange(TransformFeatures(resultSet, layer));
                    }
                }
            }
            return result;
        }

        private static bool IsZoneColumn(MapColumnElement col)
        {
            return !string.IsNullOrEmpty(col.IsZone) && col.IsZone.ToLower() == bool.TrueString.ToLower();
        }

        private IResultSetFeatureCollection GetResultSetFeatureFromColumn(string layer, MapColumnElement col, string text)
        {
            IResultSetFeatureCollection ret = null;
            FeatureLayer ftrlayer = this.map.Layers[layer] as FeatureLayer;
            Column colum = ftrlayer.Table.TableInfo.Columns[col.Name];
            ftrlayer.Table.BeginAccess(TableAccessMode.Read);
            MIConnection connection = new MIConnection();
            MICommand command = connection.CreateCommand();
            command.CommandText = "SELECT " + ftrlayer.Alias + ".* FROM " + ftrlayer.Alias + " WHERE " + ftrlayer.Table.Alias + "." + colum.Alias + " LIKE '%" + text + "%'";
            connection.Open();
            ret = command.ExecuteFeatureCollection();
            connection.Close();
            ftrlayer.Table.EndAccess();
            return ret;
        }

        private IList<GeoAddress> SearchForAllLayers(GeoAddress address)
        {
            List<GeoAddress> result = new List<GeoAddress>();
            SearchSimple(address, result);
            if (result.Count == 0)
            {
                address = RemoveConectors(address);
                SearchSimple(address, result);
                if (result.Count == 0) 
                {
                    SearchComplex(address, result);
                }
            }
            return result;
        }

        private void SearchComplex(GeoAddress address, List<GeoAddress> result)
        {
            //check if has various words
            if (HasVariousWords(address))
            {
                SearchWithMethod(address, result, DoAliasSearchForText);
               // if (result.Count == 0)
               //     SearchWithMethod(address, result, DoCombinedSearchForText);
                //Do not delete..
                //if (result.Count == 0)
                  //  SearchWithMethod(address, result, DoSimpleSearchForWords);
            }
        }

        public delegate void SearchMethodDelegate(string text, List<GeoAddress> result);

        private void SearchWithMethod(GeoAddress address, List<GeoAddress> result, SearchMethodDelegate method)
        {
            method(address.Zone, result);
            method(address.Street, result);
            method(address.More, result);
            method(address.Reference, result);
            method(address.FullText, result);
        }

        private void DoAliasSearchForText(string text, List<GeoAddress> result)
        {
            if (!string.IsNullOrEmpty(text) && result.Count <= 10000)
            {
                ReplaceAliasSearch(text, result);
            }
        }

        private void ReplaceAliasSearch(string text, List<GeoAddress> result)
        {
            string[] array = text.Split(' ');
            List<string> searchs = new List<string>();
            for (int i = 0; i < array.Length; i++)
            {
                string[] replace = ResourceMapLoader.GetString2(array[i]).Split(',');
                if (replace.Length > 1)
                {
                    for (int j = 0; j < replace.Length; j++)
                    {
                        List<string> sb = new List<string>();
                        for (int k = 0; k < array.Length; k++)
                        {
                            if (k == i)
                            {
                                sb.Add(replace[j]);
                            }
                            else
                            {
                                sb.Add(array[k]);
                            }
                        }
                        string toseek = string.Join(" ", sb.ToArray());
                        searchs.Add(toseek);
                    }
                }
            }
            SearchSimpleAvanced(searchs, result);
        }

        private void DoCombinedSearchForText(string text, List<GeoAddress> result)
        {
            if (!string.IsNullOrEmpty(text) && result.Count <= 10000)
            {
                CombinedSearchForWords(text, result);
            }
        }

        private void CombinedSearchForWords(string text, List<GeoAddress> result)
        {
            string[] array = text.Split(' ');
            for (int i = 0; i < array.Length; i++)
            {
                string bs = array[i];
                if (array[i].Trim() != "")
                {
                    SearchSimpleText(result, bs);
                    for (int j = i + 1; j < array.Length; j++)
                    {
                        if (array[j].Trim() != "")
                        {
                            bs = bs + array[j];
                            SearchSimpleText(result, bs);
                        }
                    }
                }
            }
        }

        private void SearchWithAllWordCombined(GeoAddress address, List<GeoAddress> result)
        {

        }

        private void DoSimpleSearchForWords(string text, List<GeoAddress> result)
        {
            if (!string.IsNullOrEmpty(text) && result.Count <= 10000)
            {
                SimpleSearchForWords(text, result);
            }
        }

        private void SimpleSearchForWords(string text, List<GeoAddress> result)
        {
            string[] array = text.Split(' ');
            for (int i = 0; i < array.Length; i++)
            {
                SearchSimpleText(result, array[i]);
            }
        }

        private void SearchSimpleText(List<GeoAddress> result, string text)
        {
            GeoAddress aux = new GeoAddress();
            aux.FullText = text;
            if (result.Count <= 10000)
                SearchSimple(aux, result);
        }

        private bool HasVariousWords(GeoAddress address)
        {
            bool retval = false;
            if (string.IsNullOrEmpty(address.Zone) == false)
                retval |= address.Zone.Split(' ').Length > 0;
            if (string.IsNullOrEmpty(address.Street) == false)
                retval |= address.Street.Split(' ').Length > 0;
            if (string.IsNullOrEmpty(address.Reference) == false)
                retval |= address.Reference.Split(' ').Length > 0;
            if (string.IsNullOrEmpty(address.FullText) == false)
                retval |= address.FullText.Split(' ').Length > 0;
            if (string.IsNullOrEmpty(address.More) == false)
                retval |= address.More.Split(' ').Length > 0;
            return retval;
        }

        private GeoAddress RemoveConectors(GeoAddress address)
        {
            string conectorsString = ResourceMapLoader.GetString2("$Conectors");
            string[] conectors = conectorsString.Split(',');
            string[] words = address.FullText.Split(' ');
            address.FullText = "";
            foreach (string word in words)
            {
                if(conectors.Contains(word)==false){
                    address.FullText += word + " " ;
                }
            }
            address.FullText.Remove(address.FullText.Length - 2);
            return address;
        }

        private string replaceConnectorsString(string text,List<string> conectors)
        {
            string retval = text;
            foreach (string s in conectors)
            {
                retval = retval.Replace(" "+s+" ", " ");
            }
            return retval;
        }

        private void SearchSimple(GeoAddress address, List<GeoAddress> result)
        {
            MIConnection connection = new MIConnection();
            MICommand command = connection.CreateCommand();
            connection.Open();
            foreach (string layer in layersInfo.Keys)
            {
                List<MapColumnElement> l = new List<MapColumnElement>(layersInfo[layer].Cast<MapColumnElement>());
                string sql = BuildQuery(l, layer, address.FullText);
                if (string.IsNullOrEmpty(sql) == false)
                {
                    IResultSetFeatureCollection ret = null;
                    FeatureLayer ftrlayer = this.map.Layers[layer] as FeatureLayer;

                    ftrlayer.Table.BeginAccess(TableAccessMode.Read);

                    command.CommandText = "SELECT  " + ftrlayer.Alias + ".* FROM " + ftrlayer.Alias + " WHERE " + sql;
                    
                    ret = command.ExecuteFeatureCollection();

                    ftrlayer.Table.EndAccess();
                    result.AddRange(TransformFeatures(ret, layer));
                }
                if (result.Count > 1000)
                    break;
            }
            connection.Close();
        }

        private void SearchSimpleAvanced(List<string> searchTexts, List<GeoAddress> result) 
        {

            foreach (string layer in layersInfo.Keys)
            {
                string sql = BuildQueryForAliases(new List<MapColumnElement>(layersInfo[layer].Cast<MapColumnElement>()), layer, searchTexts);
                if (string.IsNullOrEmpty(sql) == false)
                {
                    IResultSetFeatureCollection ret = null;
                    FeatureLayer ftrlayer = this.map.Layers[layer] as FeatureLayer;

                    ftrlayer.Table.BeginAccess(TableAccessMode.Read);
                    MIConnection connection = new MIConnection();
                    MICommand command = connection.CreateCommand();
                    command.CommandText = "SELECT " + ftrlayer.Alias + ".* FROM " + ftrlayer.Alias + " WHERE " + sql;
                    connection.Open();
                    ret = command.ExecuteFeatureCollection();
                    connection.Close();
                    ftrlayer.Table.EndAccess();
                    result.AddRange(TransformFeatures(ret, layer));
                }
            }
        }

        private string BuildQueryForAliases(List<MapColumnElement> list, string layerName, List<string> searchTexts)
        {
            FeatureLayer layer = this.map.Layers[layerName] as FeatureLayer;
            List<string> sb = new List<string>();
            foreach (string text in searchTexts)
            {
                string query = string.Join(" OR ", list.ConvertAll<string>(delegate(MapColumnElement col)
                {
                    Column colum = layer.Table.TableInfo.Columns[col.Name];
                    if (layer.Table.TableInfo.Columns.Contains(colum))
                    {
                        return layer.Table.Alias + "." + colum.Alias + " LIKE '%" + text + "%'";
                    }
                    return "";
                }).ToArray());
                sb.Add(query);
            }

            return string.Join(" OR ", sb.ToArray());
        }

        private List<GeoAddress> TransformFeatures(IResultSetFeatureCollection list, string layer)
        {
            List<GeoAddress> result = new List<GeoAddress>();
            GeoPoint geoPoint = null;
            foreach (Feature feature in list)
            {
                geoPoint = new GeoPoint(feature.Geometry.Centroid.x, feature.Geometry.Centroid.y);
                GeoAddress geoAddress = GetFullStringSearch(feature, layer);
                geoAddress.Point = geoPoint;
                result.Add(geoAddress);
            }
            return result;
        }

        private GeoAddress GetFullStringSearch(Feature feature, string layer)
        {
            GeoAddress ret = new GeoAddress();
            List<string> lfull = new List<string>();
            foreach (MapColumnElement col in layersInfo[layer])
            {
                string ftrString = feature[col.Name] as string;
                if (IsZoneColumn(col))
                {
                    ret.Zone = ftrString;
                }
                else if (IsStreetColumn(col))
                {
                    ret.Street = ftrString;
                }
                else
                {
                    lfull.Add(ftrString);
                }
            }
            ret.FullText = string.Join(", ", lfull.ToArray());
            return ret;
        }

        private string BuildQuery(List<MapColumnElement> list, string layerName, string text)
        {
            FeatureLayer layer = this.map.Layers[layerName] as FeatureLayer;

            return string.Join(" OR ", list.ConvertAll<string>(delegate(MapColumnElement col)
            {
                Column colum = layer.Table.TableInfo.Columns[col.Name];
                if (layer.Table.TableInfo.Columns.Contains(colum))
                {
                    return layer.Table.Alias + "." + colum.Alias + " LIKE '%" + text + "%'";
                }
                return "";
            }).ToArray());
        }

        public static IResultSetFeatureCollection SearchFeatures(FeatureLayer layer, Column column, string name)
        {
            IResultSetFeatureCollection resultSet = null;

            layer.Table.BeginAccess(TableAccessMode.Read);

            if (layer.Table.TableInfo.Columns.Contains(column) && name.Trim().Length > 0)
            {
                Catalog catalog = Session.Current.Catalog;
                SearchInfo searchInfo = MapInfo.Data.SearchInfoFactory.SearchWhere(column.Alias + " like '%" + name + "%'");
                resultSet = catalog.Search(layer.Table, searchInfo);
            }

            layer.Table.EndAccess();
            return resultSet;
        }

        public static IResultSetFeatureCollection SearchFeatures(FeatureLayer layer, Column column, string name, Column layerKey, FeatureLayer filterLayer, Column filterColumn, string filterName, Column filterLayerKey)
        {
            IResultSetFeatureCollection resultSet = null;

            layer.Table.BeginAccess(TableAccessMode.Read);

            if (layer.Table.TableInfo.Columns.Contains(column) && filterLayer.Table.TableInfo.Columns.Contains(filterColumn))
            {
                MIConnection connection = new MIConnection();
                MICommand command = connection.CreateCommand();
                command.CommandText = "SELECT " + layer.Alias + ".* FROM " + layer.Alias + ", " + filterLayer.Alias + " WHERE " + layer.Table.Alias + "." + column.Alias + " LIKE '%" + name + "%' AND " + filterLayer.Table.Alias + "." + filterColumn.Alias + " LIKE '%" + filterName + "%' AND  " + layer.Alias + "." + layerKey.Alias + " = " + filterLayer + "." + filterLayerKey.Alias;

                connection.Open();

                resultSet = command.ExecuteFeatureCollection();

                connection.Close();
            }

            layer.Table.EndAccess();

            return resultSet;
        }

        #endregion
    }
}
