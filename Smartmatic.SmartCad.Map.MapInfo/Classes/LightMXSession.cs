using System;
using System.Collections;
using System.Text;
using System.Collections.Generic;
using System.Data.SqlClient;
using MapInfo.Engine;
using MapInfo.Geometry;
using SmartCadCore.Common;

namespace Smartmatic.SmartCad.Map.MapXtreme
{
    public class LightMXSession
    {
        #region Constructors

        public LightMXSession()
        {
        }


        #endregion

        #region Fields



        #endregion

        #region Properties


        #endregion

        #region IGisSession Members

        public static double Distance(double lonA, double latA, double lonB, double latB, SmartCadCore.Common.DistanceUnit distanceUnit)
        {
            DPoint pointA = new DPoint(lonA, latA);
            DPoint pointB = new DPoint(lonB, latB);

            CoordSys coordSys = Session.Current.CoordSysFactory.CreateLongLat(DatumID.WGS84);

            return coordSys.Distance(DistanceType.Cartesian, (MapInfo.Geometry.DistanceUnit)distanceUnit, pointA, pointB);
        }

        public IList Distance(GeoPoint centralPoint, IList points, SmartCadCore.Common.DistanceUnit distanceUnit)
        {
            IList result = new ArrayList(points.Count);

            CoordSys coorSys = Session.Current.CoordSysFactory.CreateLongLat(DatumID.WGS84);

            DPoint centralDPoint = new DPoint(centralPoint.Lon, centralPoint.Lat);

            foreach (GeoPoint point in points)
            {
                DPoint dpoint = new DPoint(point.Lon, point.Lat);

                result.Add(coorSys.Distance(DistanceType.Cartesian, (MapInfo.Geometry.DistanceUnit)distanceUnit, centralDPoint, dpoint));
            }

            return result;
        }

        public IList<GeoAddress> Search(GeoAddress address)
        {
            throw new GisException("Operacion no disponible en la version Light.");
        }

        public IList<double> Distance(GeoPoint centralPoint, IList<GeoPoint> points)
        {
            List<double> result = new List<double>(points.Count);

            CoordSys coorSys = Session.Current.CoordSysFactory.CreateLongLat(DatumID.WGS84);

            DPoint centralDPoint = new DPoint(centralPoint.Lon, centralPoint.Lat);

            foreach (GeoPoint point in points)
            {
                DPoint dpoint = new DPoint(point.Lon, point.Lat);

                result.Add(coorSys.Distance(DistanceType.Cartesian, MapInfo.Geometry.DistanceUnit.Kilometer, centralDPoint, dpoint));
            }

            return result;
        }

        #endregion

        private GisException HandleException(Exception ex)
        {
            string message = ex.Message;

            if (ex.Message.StartsWith("Login failed for user"))
            {
                int init = ex.Message.IndexOf('\'') + 1;
                int end = ex.Message.IndexOf('\'', init);

                string name = ex.Message.Substring(init, end - init);

                message = "El usuario " + name  + " no tiene permisos en la base de datos geo-espacial.";
                
            }
            else if (ex.Message.StartsWith("Cannot open database"))
            {
                int init = ex.Message.IndexOf('\'') + 1;
                int end = ex.Message.IndexOf('\'', init);

                string name = ex.Message.Substring(init, end - init);

                init = ex.Message.IndexOf('\"') + 1;
                end = ex.Message.IndexOf('\"', init);

                string database = ex.Message.Substring(init, end - init);

                message = "El usuario " + name + " no tiene permisos en la base de datos " + database + ".";
            }

            return new GisException(message, ex);
        }
    }
}
