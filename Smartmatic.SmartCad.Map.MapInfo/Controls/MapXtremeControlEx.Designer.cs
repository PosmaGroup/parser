﻿namespace Smartmatic.SmartCad.Map.MapXtreme
{
    partial class MapXtremeControlEx : MapControlEx
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mapControl = new MapInfo.Windows.Controls.MapControl();
            this.SuspendLayout();
            // 
            // mapControl
            // 
            this.mapControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mapControl.IgnoreLostFocusEvent = false;
            this.mapControl.Location = new System.Drawing.Point(0, 0);
            this.mapControl.Name = "mapControl";
            this.mapControl.Size = new System.Drawing.Size(642, 633);
            this.mapControl.TabIndex = 3;
            this.mapControl.TabStop = false;
            this.mapControl.Text = "mapControl";
            this.mapControl.MouseMove += new System.Windows.Forms.MouseEventHandler(this.mapControl_MouseMove);
            this.mapControl.Tools.LeftButtonTool = null;
            this.mapControl.Tools.MiddleButtonTool = null;
            this.mapControl.Tools.RightButtonTool = null;
            //
            // zoomTrackBarControl1
            //
            this.zoomTrackBarControl1.EditValueChanged += new System.EventHandler(this.zoomTrackBarControl1_EditValueChanged);
            // 
            // MapInfoControlEx
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ruler);
            this.Controls.Add(this.zoomTrackBarControl1);
            this.Controls.Add(this.mapControl);
            this.Name = "MapInfoControlEx";
            this.Size = new System.Drawing.Size(642, 633);
            this.Load += new System.EventHandler(this.MapInfoControlEx_Load);
            ((System.ComponentModel.ISupportInitialize)(this.zoomTrackBarControl1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.zoomTrackBarControl1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public MapInfo.Windows.Controls.MapControl mapControl;
    }
}
