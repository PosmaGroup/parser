﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Windows.Forms;
using System.Collections;
using System.IO;
using System.Threading;
using System.Reflection;



using System.Data;
using DevExpress.Utils;
using MapInfo.Persistence;
using MapInfo.Data;
using MapInfo.Geometry;
using MapInfo.Mapping;
using MapInfo.Engine;
using MapInfo.Styles;
using SmartCadCore.Core;
using SmartCadCore.Common;

namespace Smartmatic.SmartCad.Map.MapXtreme
{
    public partial class MapXtremeControlEx : MapControlEx
    {
        #region Constants
        private double CENTER_X = SmartCadConfiguration.SmartCadSection.DefaultMapLon;
        private double CENTER_Y = SmartCadConfiguration.SmartCadSection.DefaultMapLat;

        private int MAXUPDATES = 60;// Numeros ideales

        private int MAXSLEEPTIME = 20;// Numeros ideales

        private int UNITSMAXUPDATES = 100;// Numeros ideales

        private int UNITSMAXSLEEPTIME = 100;// Numeros ideales

        #endregion

        #region Fields
        private WorkSpaceLoader loaderAux;
        public Key CurrentFeatureKey { get; set; }
        public double MapDistance { get; set; }
        public CoordSys CoordGlobal { get; set; }
        private Queue<CommittedDataAction> queueActionUpdates;
        private Queue<object> queueObjectsUpdates;

        private Dictionary<int, MapObjectUnit> unitsQueue = new Dictionary<int, MapObjectUnit>();

        private System.Windows.Forms.Timer timerRefreshUnits;
        private System.Windows.Forms.Timer timerRefreshObjects;
        private int SLEEP_TIME_BETWEEN_HIGHLIGHT = 400;
        private int SLEEP_TIME_HIGHLIGHT = 1250;
        private Key followedUnitKey = null;
        private object queue = new object();
        private object catalogSync = new object();
        private bool isCenteringMap = false;

        private double viewChangeOldValue;
        //taza de cambio de los iconos cuando crecen o decrecen
        public const double RATE_SIZE_ICON_CHANGE = 1.1;
        public bool closeBackGroundDialog = true;
        private object Layer = new object();

        private Dictionary<int, Feature> tracks;

        //variable que contiene la clase que realiza las busquedas.
        private MXSession gisSession;

        private byte[] fileBytes = null;

        #endregion

        #region Constructor
        public MapXtremeControlEx()
        :base()
        {
            InitializeComponent();

            MapInfo.Engine.Session.Current.StyleRepository.BitmapPointStyleRepository.Reload(
               Application.StartupPath + SmartCadConfiguration.MapsIconsFolder);

            timerRefreshUnits = new System.Windows.Forms.Timer();
            timerRefreshObjects = new System.Windows.Forms.Timer();

            UNITSMAXSLEEPTIME = SmartCadConfiguration.SmartCadSection.ServerElement.SpatialwareElement.MaxSleepTime;
            UNITSMAXUPDATES = SmartCadConfiguration.SmartCadSection.ServerElement.SpatialwareElement.MaxUpdates;

            queueActionUpdates = new Queue<CommittedDataAction>();
            queueObjectsUpdates = new Queue<object>();

            Layer = new object();

            this.zoomTrackBarControl1.Parent = this.mapControl;
            this.ShowDistance = false;

            this.mapControl.Tools.FeatureAdding += new MapInfo.Tools.FeatureAddingEventHandler(MapInfoFeatureAdding);
            this.mapControl.Tools.Used += new MapInfo.Tools.ToolUsedEventHandler(MapInfoToolUsed);
            this.mapControl.Tools.Ending += new MapInfo.Tools.ToolEndingEventHandler(MapInfoToolEnding);
            this.mapControl.Tools.Activated += new MapInfo.Tools.ToolActivatedEventHandler(MapInfoToolActivated);
            this.mapControl.Tools.FeatureSelected += new MapInfo.Tools.FeatureSelectedEventHandler(MapInfoFeatureSelected);
            this.mapControl.Tools.FeatureAdding += new MapInfo.Tools.FeatureAddingEventHandler(MapInfoFeatureAdding);
            this.mapControl.Map.DrawEvent += new MapInfo.Mapping.MapDrawEventHandler(MapInfoDrawEvent);
        }
        #endregion

        #region Windows Methods

        private void MapInfoControlEx_Load(object sender, EventArgs e)
        {
            tracks = new Dictionary<int, Feature>();
            this.mapControl.Map.ViewChangedEvent += new MapInfo.Mapping.ViewChangedEventHandler(Map_ViewChangedEvent);

            timerRefreshObjects.Interval = 100;
            timerRefreshObjects.Tick += new EventHandler(this.SimulatorTimer);
            timerRefreshObjects.Enabled = true;
        }

        private void RaiseBackGroundProcess()
        {
            Thread th = new Thread(new ThreadStart(CallBackGroundProcess));
            th.Start();
        }

        private void CallBackGroundProcess()
        {
            try
            {
                BackgroundProcessForm processForm = new BackgroundProcessForm(this, new MethodInfo[1] { GetType().GetMethod("BackGroundProcess", BindingFlags.NonPublic | BindingFlags.Instance) }, new object[1][] { new object[0] });
                processForm.CanThrowError = true;
                processForm.ShowDialog();
            }
            catch
            {
            }
        }

        private void BackGroundProcess()
        {
            do
                Thread.Sleep(250);
            while (!closeBackGroundDialog);
        }
        #endregion


        #region Override Methods

        /// <summary>
        /// Este metodo chequea que la estacion este dentro de su zona.
        /// </summary>
        public override bool CheckContains(List<GeoPoint> fatherPoints, List<GeoPoint> childPoints, string tableName)
        {
            if (fatherPoints.Count > 0 && childPoints.Count > 0)
            {
                Feature father = CreateTempPolygonInLayer(fatherPoints, tableName);
                //Feature child = CreateTempPolygonInLayer(childPoints, tableName);

                //return father.Geometry.Contains(child.Geometry);
            
                return CheckObjectInFeature(childPoints, father);
            }
            return false;
        }

        /// <summary>
        /// Este metodo chequea si dos poligonos se solapan
        /// </summary>
        public override bool CheckOverlaps(List<GeoPoint> points, List<GeoPoint> points2, string tableName)
        {
            Feature temp = CreateTempPolygonInLayer(points, tableName);
            Feature res = CreateTempPolygonInLayer(points2, tableName);
            if (res.Geometry.Intersects(temp.Geometry) == true)
            {
                return true;
            }
            return false;
        }

        public override void UpdateObject(MapObject obj)
        {
            if (obj is MapObjectUnit)
            {
                InsertOrUpdateUnit((MapObjectUnit)obj);
            }
            else
            {
                queueActionUpdates.Enqueue(CommittedDataAction.Update);
                queueObjectsUpdates.Enqueue(obj);
                FormUtil.InvokeRequired(this, delegate()
                {
                    if (!timerRefreshObjects.Enabled)
                    {
                        this.timerRefreshObjects.Interval = 10;
                        this.timerRefreshObjects.Start();
                    }
                });
            }
        }

        public override void DeleteObject(MapObject obj)
        {
            queueActionUpdates.Enqueue(CommittedDataAction.Delete);
            queueObjectsUpdates.Enqueue(obj);
            FormUtil.InvokeRequired(this, delegate()
            {
                if (!timerRefreshObjects.Enabled)
                {
                    this.timerRefreshObjects.Interval = 10;
                    this.timerRefreshObjects.Start();
                }
            });
        }

        /// <summary>
        /// Create the objects tables
        /// </summary>
        /// <param name="name"></param>
        public override void CreateTable(MapObject emptyObject, string tableName, bool objEnable, bool lblEnable, int objZoom, int lblZoom)
        {
            try
            {
                Catalog cat = MapInfo.Engine.Session.Current.Catalog;
                TableInfoMemTable tblInfoTemp = new TableInfoMemTable(tableName);
                Table tblTemp = cat.GetTable(tableName);

                if (tblTemp != null)//if exists, close!
                    cat.CloseTable(tableName);

                //Aqui se colocan los campos que queremos lleve la tabla
                tblInfoTemp.Columns.Add(ColumnFactory.CreateFeatureGeometryColumn(this.mapControl.Map.GetDisplayCoordSys()));
                tblInfoTemp.Columns.Add(ColumnFactory.CreateStyleColumn());
                tblInfoTemp.Columns.Add(ColumnFactory.CreateIndexedIntColumn("Code"));

                foreach (KeyValuePair<string, object> field in emptyObject.Fields)
                {
                    if (field.Value.GetType() != typeof(int))
                    {
                        tblInfoTemp.Columns.Add(ColumnFactory.CreateStringColumn(field.Key, 50));
                    }
                    else
                    {
                        tblInfoTemp.Columns.Add(ColumnFactory.CreateIntColumn(field.Key));
                    }
                }

                //creates table in the catalog
                try
                {
                    tblTemp = cat.CreateTable(tblInfoTemp);
                }
                catch(Exception ex)
                {
                    if (tblTemp == null)
                        throw ex;
                }

                //adds table to layers group
                FeatureLayer lyr = new FeatureLayer(tblTemp);
                CoordGlobal = lyr.CoordSys;
                lyr.VolatilityHint = LayerVolatilityHint.Animate | LayerVolatilityHint.CacheIfPossible;
                lyr.VisibleRange = new VisibleRange(0, objZoom, MapInfo.Geometry.DistanceUnit.Kilometer); ;
                lyr.VisibleRangeEnabled = true;
                int k = this.mapControl.Map.Layers.Add(lyr);
                lyr.Enabled = objEnable;


                LabelLayer lblyr = new LabelLayer(tableName + "Labels", tableName + "Labels");
                lblyr.VolatilityHint = LayerVolatilityHint.Animate | LayerVolatilityHint.CacheIfPossible;
                lblyr.VisibleRange = new MapInfo.Mapping.VisibleRange(0, lblZoom, MapInfo.Geometry.DistanceUnit.Kilometer); ;
                lblyr.VisibleRangeEnabled = true;
                this.mapControl.Map.Layers.Insert(k, lblyr);
                lblyr.Enabled = lblEnable;

                if (emptyObject.Fields.Keys.Count() > 0)
                {
                    LabelSource src = new LabelSource(tblTemp);
                    src.DefaultLabelProperties.Layout.Alignment = MapInfo.Text.Alignment.BottomCenter;
                    src.DefaultLabelProperties.Style.Font = new MapInfo.Styles.Font("Microsoft Sans Serif", 10);
                    src.DefaultLabelProperties.Layout.Offset = 5;
                    src.DefaultLabelProperties.Layout.UseRelativeOrientation = false;
                    src.DefaultLabelProperties.Caption = emptyObject.Fields.Keys.First<string>(); // Always use the first field like caption
                    lblyr.Sources.Append(src);
                }

                ToolFilter toolFilter = (ToolFilter)mapControl.Tools.SelectMapToolProperties.SelectableLayerFilter;
                if (toolFilter != null && !toolFilter.IncludeLayer(lyr))
                    toolFilter.SetExplicitInclude(lyr, true);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        /// <summary>
        /// Create the distance lines table
        /// </summary>
        /// <param name="name"></param>
        public override void CreateLineTable(string type)
        {
            string name = DynTableDistance;
            if (type == "RouteTemp")
            {
                name = DynTableRouteTemp;
            }
            Catalog Cat = MapInfo.Engine.Session.Current.Catalog;
            TableInfoMemTable tblInfoTemp = new TableInfoMemTable(name);
            Table tblTemp = Cat.GetTable(name);
            if (tblTemp != null) //si la tabla existe cerrar
                Cat.CloseTable(name);

            tblInfoTemp.Columns.Add(ColumnFactory.CreateFeatureGeometryColumn(this.mapControl.Map.GetDisplayCoordSys()));
            tblInfoTemp.Columns.Add(ColumnFactory.CreateStyleColumn());
            tblInfoTemp.Columns.Add(ColumnFactory.CreateStringColumn("Lenght", 50));
            tblInfoTemp.Columns.Add(ColumnFactory.CreateStringColumn("Code", 100));
            //tblInfoTemp.Columns.Add(ColumnFactory.CreateStringColumn("Name", 10));

            //creamos la tabla en el catalogo
            tblTemp = Cat.CreateTable(tblInfoTemp);

            //agregamos la tabla al grupo de layers.
            FeatureLayer lyr = new FeatureLayer(tblTemp);
            LabelLayer lblyr = new LabelLayer("DistanceLabels");

            lyr.VolatilityHint =
                LayerVolatilityHint.Animate | LayerVolatilityHint.CacheIfPossible;
            lblyr.VolatilityHint =
                LayerVolatilityHint.Animate | LayerVolatilityHint.CacheIfPossible;

            int k = this.mapControl.Map.Layers.Add(lyr);
            this.mapControl.Map.Layers.Insert(k, lblyr);

            LabelSource src = new LabelSource(tblTemp);
            src.DefaultLabelProperties.Layout.Alignment = MapInfo.Text.Alignment.BottomCenter;
            src.DefaultLabelProperties.Style.Font = new MapInfo.Styles.Font("Microsoft Sans Serif", 10);
            src.DefaultLabelProperties.Layout.Offset = 5;
            src.DefaultLabelProperties.Layout.UseRelativeOrientation = false;
            src.DefaultLabelProperties.Caption = "Lenght";
            lblyr.Sources.Append(src);
            lblyr.Enabled = true;
        }

        public override void InsertObject(MapObject obj)
        {
            queueActionUpdates.Enqueue(CommittedDataAction.Save);
            queueObjectsUpdates.Enqueue(obj);
            FormUtil.InvokeRequired(this, delegate()
            {
                if (!timerRefreshObjects.Enabled)
                {
                    this.timerRefreshObjects.Interval = 10;
                    this.timerRefreshObjects.Start();
                };

            });
        }

        /// <summary>
        /// Agrega un segmento para distancia
        /// </summary>
        public override void InsertDistanceSegment(List<GeoPoint> points, bool isDistance)
        {
            lock (Layer)
            {
                DPoint[] _lastSegmentPoints = new DPoint[2];
                _lastSegmentPoints[0] = new DPoint(points[points.Count - 2].Lon, points[points.Count - 2].Lat);
                _lastSegmentPoints[1] = new DPoint(points[points.Count - 1].Lon, points[points.Count - 1].Lat);
                bool showDis = false;
                string number = "";
                string unit = "";
                double length = 0;
                if (isDistance == true)
                {
                    showDis = true;
                    Curve curve = new Curve(CoordGlobal, MapInfo.Geometry.CurveSegmentType.Linear, _lastSegmentPoints);
                    length = curve.Length(MapInfo.Geometry.DistanceUnit.Kilometer, DistanceType.Spherical);

                    unit = "Km";
                    if (length > 1)
                    {
                        number = string.Format("{0:0.000}", length);
                    }
                    else
                    {
                        number = ((int)(length * 1000)) + "";
                        unit = "mts";
                    }
                }
                string code = String.Format("{0}-{1}", (points.Count - 2), (points.Count - 1));
                InsertLineInTempLayer(_lastSegmentPoints.ToList(), DynTableDistance, number, code, unit);
                totalDistance += length;
                ShowDistance = showDis;
                Distance = totalDistance;
                RecalculateAll();
            }
        }

        /// <summary>
        /// Este metodo crea un poligono en una tabla temporal, es importante
        /// que se mantenga la distincion entre esta tabla y las demas, esta
        /// tabla solo se utiliza para editar una zona, una estacion, etc.
        /// luego se envian los puntos a administracion y luego que venga por commited change
        /// es que se guarda en la tabla real que esta conectada con el treeview.
        /// </summary>
        /// <param name="e"></param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public override void InsertTempPolygonInLayer(List<GeoPoint> e, string tableName)
        {
            lock (Layer)
            {
                Catalog Cat = MapInfo.Engine.Session.Current.Catalog;
                Table tbl = Cat.GetTable(tableName);
                Feature f = CreateTempPolygonInLayer(e, tableName);

                CurrentFeatureKey = tbl.InsertFeature(f);
            }
        }

        /// <summary>
        /// Agrega un punto en la tabla temporal, hasta ahora solo lo us primer nivel.
        /// </summary>
        /// <param name="p"></param>
        public override void InsertPointInTempLayer(GeoPoint p)
        {
            lock (Layer)
            {
                ClearTempTable();
                Catalog Cat = MapInfo.Engine.Session.Current.Catalog;
                Table tbl = Cat.GetTable(DynTableTemp);

                if (tbl != null)
                {
                    FeatureLayer lyr = new FeatureLayer(tbl);
                    FeatureGeometry pt = new MapInfo.Geometry.Point(lyr.CoordSys, new DPoint(p.Lon, p.Lat)) as FeatureGeometry;
                    CompositeStyle cs = new CompositeStyle(new BitmapPointStyle("Marca-para-punto.bmp", BitmapStyles.None, Color.White, 20));
                    Feature ftr = new Feature(tbl.TableInfo.Columns);
                    ftr.Geometry = pt;
                    ftr.Style = cs;
                    CurrentFeatureKey = tbl.InsertFeature(ftr);
                    Refresh();
                }
            }
        }
       
        public override void RecalculateAll()
        {
            Map_ViewChangedEvent(null, null);
        }

        public override void MakeLayerSelectable(string layerName)
        {
            IMapLayer[] layers = new IMapLayer[1];
            layers[0] = mapControl.Map.Layers[layerName];
            IMapLayerFilter a = MapLayerFilterFactory.FilterAnd(MapLayerFilterFactory.FilterVisibleLayers(true),
               MapLayerFilterFactory.FilterSpecificLayers(layers));
            mapControl.Tools.SelectMapToolProperties.SelectableLayerFilter = a;
        }

        public override void ReloadMap()
        {
            List<IMapLayer> layers = new List<IMapLayer>();
            //add static selectable layers  to selectable layers
            foreach (LayerElement layer in SmartCadConfiguration.SmartCadSection.Maps[CurrentMap].Layers)
            {
                if (layer.Selectable.ToLower() == bool.TrueString.ToLower())
                {
                    layers.Add(mapControl.Map.Layers[layer.Name]);
                }
            }
            //add all dynamics layers to selectable layers
            layers.Add(mapControl.Map.Layers[DynTableIncidentsName]);
            layers.Add(mapControl.Map.Layers[DynTablePolygonStationName]);
            layers.Add(mapControl.Map.Layers[DynTablePolygonZoneName]);
            layers.Add(mapControl.Map.Layers[DynTablePostName]);
            layers.Add(mapControl.Map.Layers[DynTableRoute]);
            layers.Add(mapControl.Map.Layers[DynTableUnitsName]);

            IMapLayerFilter a = MapLayerFilterFactory.FilterAnd(MapLayerFilterFactory.FilterVisibleLayers(true),
                MapLayerFilterFactory.FilterSpecificLayers(layers.ToArray()));
            mapControl.Tools.SelectMapToolProperties.SelectableLayerFilter = a;

            MapInfo.Geometry.DPoint ctr = mapControl.Map.Center;
            MapInfo.Geometry.Distance z = mapControl.Map.Zoom;

            /// TEMP TABLE

            // first close the temporary table -- an easy way to clean it up
            // (otherwise the table will remain open after the map is cleared)
            MapInfo.Data.Table tblTemp = Session.Current.Catalog.GetTable(DynTableTemp);
            if (tblTemp != null)
                tblTemp.Close();
            // create a temporary table and add a featurelayer for it
            FeatureLayer layerTemp = new FeatureLayer(
                Session.Current.Catalog.CreateTable(
                TableInfoFactory.CreateTemp(DynTableTemp), new TableSessionInfo()));
            mapControl.Map.Layers.Add(layerTemp);

            // set the insertion and edit filters to allow all tools to work on Temp
            ToolFilter toolFilter =
                (ToolFilter)mapControl.Tools.AddMapToolProperties.InsertionLayerFilter;
            if (toolFilter != null && !toolFilter.IncludeLayer(layerTemp))
                toolFilter.SetExplicitInclude(layerTemp, true);
            toolFilter = (ToolFilter)mapControl.Tools.SelectMapToolProperties.EditableLayerFilter;
            if (toolFilter != null && !toolFilter.IncludeLayer(layerTemp))
                toolFilter.SetExplicitInclude(layerTemp, true);


            /// END TEMP TABLE
        

            tblTemp = Session.Current.Catalog.GetTable(DynTableIconsTemp);
            if (tblTemp != null)
                tblTemp.Close();

            // create a temporary table and add a featurelayer for it
            layerTemp = new FeatureLayer(
                Session.Current.Catalog.CreateTable(
                TableInfoFactory.CreateTemp(DynTableIconsTemp), new TableSessionInfo()));
            layerTemp.VisibleRange = new VisibleRange(0, true, 13, true, MapInfo.Geometry.DistanceUnit.Mile);
            layerTemp.VisibleRangeEnabled = true;
            mapControl.Map.Layers.Add(layerTemp);

            if (tracks != null)
                tracks.Clear();

            timerRefreshUnits.Interval = 10000; //Start each 10 secs until an update comes
            timerRefreshUnits.Tick += new EventHandler(this.UnitsTimer);
            timerRefreshUnits.Enabled = true;
            timerRefreshUnits.Start();
        }

        public override void ClearTables()
        {
            queueObjectsUpdates.Enqueue(new MapObjectZone());
            queueActionUpdates.Enqueue(CommittedDataAction.Delete);

            queueObjectsUpdates.Enqueue(new MapObjectStation());
            queueActionUpdates.Enqueue(CommittedDataAction.Delete);

            FormUtil.InvokeRequired(this, delegate()
            {
                if (!timerRefreshObjects.Enabled)
                {
                    this.timerRefreshObjects.Interval = 10;
                    this.timerRefreshObjects.Start();
                }
            });
        }

        public override void ClearTable(string tableName)
        {
            queueObjectsUpdates.Enqueue(tableName);
            queueActionUpdates.Enqueue(CommittedDataAction.Delete);
            
            FormUtil.InvokeRequired(this, delegate()
            {
                if (!timerRefreshObjects.Enabled)
                {
                    this.timerRefreshObjects.Interval = 10;
                    this.timerRefreshObjects.Start();
                }
            });
        }

        public override void ClearActions(MapActions clearAction)
        {
            queueObjectsUpdates.Enqueue(clearAction);
            queueActionUpdates.Enqueue(CommittedDataAction.Delete);
            FormUtil.InvokeRequired(this, delegate()
            {
                if (!timerRefreshObjects.Enabled)
                {
                    this.timerRefreshObjects.Interval = 10;
                    this.timerRefreshObjects.Start();
                }
            });
        }

        public override void EnableLayer(string layerName, bool enable)
        {
            foreach (MapLayer layer in mapControl.Map.Layers)
                if (layer.Name == layerName)
                {
                    layer.Enabled = enable;
                    break;
                }
        }

        public override bool IsLayerEnable(string layerName)
        {
            foreach (MapLayer layer in mapControl.Map.Layers)
                if (layer.Name == layerName)
                    return layer.Enabled;
            return false;
        }

        public override Dictionary<string, string> GetLayersName()
        {
            Dictionary<string, string> layers = new Dictionary<string, string>();
            foreach (MapLayer layer in mapControl.Map.Layers)
            {
                LayerElement le = SmartCadConfiguration.SmartCadSection.Maps[CurrentMap].Layers[layer.Name] as LayerElement;
                if (le != null)
                {
                    layers.Add(layer.Name, le.Alias);
                }
                else if (layer.Name.EndsWith("Labels") == true)
                {
                    if(layers.ContainsKey(layer.Name) == false)
                        layers.Add(layer.Name, ResourceLoader.GetString2("Labels"));
                }
                else
                {
                }
            }
            return layers;
        }

        public override void CenterMap(int viewDistance)
        {
            queueObjectsUpdates.Enqueue(viewDistance);
            //queueActionUpdates.Enqueue(CommittedDataAction.None);
            FormUtil.InvokeRequired(this, delegate()
            {
                if (!timerRefreshObjects.Enabled)
                {
                    this.timerRefreshObjects.Interval = 10;
                    this.timerRefreshObjects.Start();
                }
            });
        }

        /// <summary>
        /// Este metodo centra el mapa en el punto indicado, generalmente se 
        /// utiliza este metodo para centrar unidades, incidentes etc.
        /// </summary>
        /// <param name="dPoint"></param>
        public override void CenterMapInPoint(GeoPoint point)
        {
            queueObjectsUpdates.Enqueue(point);
            //queueActionUpdates.Enqueue(CommittedDataAction.None);
            FormUtil.InvokeRequired(this, delegate()
            {
                if (!timerRefreshObjects.Enabled)
                {
                    this.timerRefreshObjects.Interval = 10;
                    this.timerRefreshObjects.Start();
                }
            });
        }

        public override void CenterMap()
        {
            queueObjectsUpdates.Enqueue(new GeoPoint(CENTER_X, CENTER_Y));
            //queueActionUpdates.Enqueue(CommittedDataAction.None);
            FormUtil.InvokeRequired(this, delegate()
            {
                if (!timerRefreshObjects.Enabled)
                {
                    this.timerRefreshObjects.Interval = 10;
                    this.timerRefreshObjects.Start();
                }
            });
        }

        public override void RefreshZoomTrackBar()
        {
            zoomTrackBarControl1.Refresh();
        }

        public override void InitializeMouseWheelSupport()
        {
            mapControl.MouseWheelSupport = new MouseWheelSupport(MouseWheelBehavior.None, 0, 0);
        }

        public override void SetToolUseDefaultCursor(bool useDefaultCursor, Cursor cursor)
        {
            mapControl.Tools.CurrentTool.UseDefaultCursor = useDefaultCursor;
            if (useDefaultCursor == false)
                mapControl.Tools.CurrentTool.Cursor = cursor;
        }

        public override void RefreshMap()
        {
            lock (Layer)
            {
                FormUtil.InvokeRequired(this, delegate
                {
                    mapControl.Refresh();
                });
            }
        }

        //public override void DeleteAllTracksUnless(int unitCode)
        //{
        //    Catalog Cat = MapInfo.Engine.Session.Current.Catalog;
        //    Table tbl = Cat.GetTable(DynTableTracksName);
        //    foreach (int code in tracks.Keys)
        //    {
        //        if (code != unitCode)
        //        {
        //            SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchWhere("Code = " + code);
        //            (tbl as ITableFeatureCollection).Remove(Cat.Search(tbl, si));
        //            tbl.Pack(MapInfo.Data.PackType.All);
        //        }
        //    }
        //}

        //public override void DeleteAllUnitTracks()
        //{
        //    Catalog Cat = MapInfo.Engine.Session.Current.Catalog;
        //    Table tbl = Cat.GetTable(DynTableUnitTracksName);
        //    SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchAll();
        //    (tbl as ITableFeatureCollection).Remove(Cat.Search(tbl, si));
        //    tbl.Pack(MapInfo.Data.PackType.All);
        //}

        public override void ChangeAddPositionToolState(AddPositionToolState newState)
        {
            AddPositionToolData addPositionToolData = ((AddPositionTool)this.mapControl.Tools["AddPosition"]).AddPositionToolData;
            if (addPositionToolData.State != AddPositionToolState.Deleted)
                addPositionToolData.State = newState;
        }

        public override void EnableAddPositionTool(bool enable)
        {
            AddPositionToolData addPositionToolData = ((AddPositionTool)this.mapControl.Tools["AddPosition"]).AddPositionToolData;
            addPositionToolData.Enabled = enable;
        }

        public override void SetLeftButtonTool(string p)
        {
            mapControl.Tools.LeftButtonTool = p;
            ToolInUse = p;
        }

        public override void MapDraw(GeoPoint geoPoint, ImageSearchState setVisibleImageSearch)
        {
            Graphics graphics = this.mapControl.CreateGraphics();

            DPoint point = new DPoint(geoPoint.Lon, geoPoint.Lat);
            System.Drawing.Point clientPoint;

            Image image = ResourceLoader.GetImage("Image.SearchResult");

            mapControl.Map.DisplayTransform.ToDisplay(point, out clientPoint);

            if ((this.mapControl.DisplayRectangle.Contains(clientPoint)) &&
                (setVisibleImageSearch == ImageSearchState.Normal) && isCenteringMap == false)
                graphics.DrawImage(image, clientPoint.X - image.Width, clientPoint.Y - image.Height);

            graphics.Dispose();
        }
        
        /// <summary>
        ///  Sirve para buscar la zona donde han hecho click,
        ///  la herramienta de select debe estar activa. Gustavo Fandiño
        /// </summary>
        /// <param name="radius"></param>
        /// <param name="toolstripmenuitem_Click"></param>
        /// <returns></returns>
        
        public override void ToolFeatureSelected(EventHandler toolstripmenuitem_Click, FeatureSelectedEventArgs e, ref ToolTipController toolTipController1)
        {
            //Obtener todos los puntos cercanos para hacer una lista cuando no se puedan diferenciar.

            SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchWithinDistance(new DPoint(e.Position.Lon, e.Position.Lat), mapControl.Map.GetDisplayCoordSys(), new Distance(MapDistance * 0.2, MapInfo.Geometry.DistanceUnit.Kilometer), ContainsType.Centroid);
            List<IResultSetFeatureCollection> result = new List<IResultSetFeatureCollection>();
            if(MapInfo.Engine.Session.Current.Catalog.GetTable(this.DynTableUnitsName)!=null)
                result.Add(MapInfo.Engine.Session.Current.Catalog.Search(this.DynTableUnitsName, si));

            if (MapInfo.Engine.Session.Current.Catalog.GetTable(this.DynTableIncidentsName) != null)
                result.Add(MapInfo.Engine.Session.Current.Catalog.Search(this.DynTableIncidentsName, si));

            if (MapInfo.Engine.Session.Current.Catalog.GetTable(this.DynTablePostName) != null)
                result.Add(MapInfo.Engine.Session.Current.Catalog.Search(this.DynTablePostName, si));

            //The Route do not have information to show
            //if (MapInfo.Engine.Session.Current.Catalog.GetTable(this.DynTableRoute) != null)
            //    result.Add(MapInfo.Engine.Session.Current.Catalog.Search(this.DynTableRoute, si));

            int total = 0;
            foreach (IResultSetFeatureCollection col in result)
            {
                total += col.Count;
            }

            if (total > 1)
            {
                ContextMenuStrip cms = new ContextMenuStrip();
                foreach (IResultSetFeatureCollection col in result)
                {
                    if (col.Count > 0)
                    {
                        ToolStripMenuItem baseToolStripMenu = new ToolStripMenuItem();
                        baseToolStripMenu.Text = ResourceLoader.GetString2(col.BaseTable.Alias);
                        cms.Items.Add(baseToolStripMenu);
                        foreach (Feature feature in col)
                        {
                            Feature realFeature = MapInfo.Engine.Session.Current.Catalog.SearchForFeature(col.BaseTable.Alias, MapInfo.Data.SearchInfoFactory.SearchWhere("MI_Key = '" + feature.Key.Value + "'"));

                            ToolStripMenuItem toolstripmenuitem = new ToolStripMenuItem();
                            if (col.BaseTable.Alias == this.DynTablePostName )//|| col.BaseTable.Alias == this.DynTableRoute)
                                toolstripmenuitem.Text = realFeature["Name"].ToString();
                            else
                                toolstripmenuitem.Text = realFeature["CustomCode"].ToString();

                            toolstripmenuitem.Tag = CreateBallonData(realFeature["Code"].ToString(),
                                col.BaseTable.Alias, new GeoPoint(realFeature.Geometry.Centroid.x, realFeature.Geometry.Centroid.y),
                                realFeature.Geometry.Bounds.x1, realFeature.Geometry.Bounds.y1);
                            toolstripmenuitem.Click += toolstripmenuitem_Click;
                            baseToolStripMenu.DropDownItems.Add(toolstripmenuitem);
                        }
                    }
                }
                cms.Show(MousePosition);
            }
            else
            {
                MultiResultSetFeatureCollection featureEnumerator = e.Selection as MultiResultSetFeatureCollection;
                IEnumerator enumerator = featureEnumerator.GetEnumerator();

                while (enumerator.MoveNext())
                {
                    IResultSetFeatureCollection featureCollection = enumerator.Current as IResultSetFeatureCollection;

                    if (featureCollection.Table.Alias.StartsWith(this.DynTableUnitsName + "_s") &&
                        featureCollection.Count == 1)
                    {
                        #region DynTableUnitsName
                        if (ShowBallonEvent != null)
                        {
                            Feature f = (Feature)featureCollection[0];
                            ShowBallonEvent(this, CreateBallonData(
                                f["Code"].ToString(), featureCollection.BaseTable.Alias, new GeoPoint(f.Geometry.Centroid.x, f.Geometry.Centroid.y),
                                f.Geometry.Bounds.x1, f.Geometry.Bounds.y1));
                        }
                        #endregion
                    }
                    else if (featureCollection.Table.Alias.StartsWith(this.DynTableIncidentsName + "_s") &&
                        featureCollection.Count == 1)
                    {
                        #region DynTableIncidentsName
                        if (ShowBallonEvent != null)
                        {
                            Feature f = (Feature)featureCollection[0];
                            ShowBallonEvent(this, CreateBallonData(
                                f["Code"].ToString(), featureCollection.BaseTable.Alias, new GeoPoint(f.Geometry.Centroid.x, f.Geometry.Centroid.y),
                                f.Geometry.Bounds.x1, f.Geometry.Bounds.y1));
                        }
                        #endregion
                    }
                    else if (featureCollection.Table.Alias.StartsWith(this.DynTablePostName + "_s") &&
                        featureCollection.Count == 1)
                    {
                        #region DynTablePostName
                        if (ShowBallonEvent != null)
                        {
                            Feature f = (Feature)featureCollection[0];
                            ShowBallonEvent(this, CreateBallonData(
                                f["Code"].ToString(), featureCollection.BaseTable.Alias, new GeoPoint(f.Geometry.Centroid.x, f.Geometry.Centroid.y),
                                f.Geometry.Bounds.x1, f.Geometry.Bounds.y1));
                        }
                        #endregion
                    }
                    else if (featureCollection.Table.Alias.StartsWith(DynTablePolygonZoneName + "_s"))
                    {
                        #region  DynTablePolygonZone
                        if (ShowSuperToolTipEvent != null)
                        {
                            ShowSuperToolTipEvent(this, new ShowSuperToolTipEventArgs(typeof(MapObjectZone),
                                (int)featureCollection[0]["Code"], e.Position));
                        }
                        #endregion
                    }
                    else if (featureCollection.Table.Alias.StartsWith(DynTablePolygonStationName + "_s"))
                    {
                        #region DynTablePolygonStation
                        if (ShowSuperToolTipEvent != null)
                        {
                            ShowSuperToolTipEvent(this, new ShowSuperToolTipEventArgs(typeof(MapObjectStation),
                                (int)featureCollection[0]["Code"], e.Position));
                        }
                        #endregion
                    }
                    else
                    {
                        string layername = featureCollection.Table.Alias.Remove(featureCollection.Table.Alias.IndexOf("_"));
                        LayerElement layer = SmartCadConfiguration.SmartCadSection.Maps[CurrentMap].Layers[layername] as LayerElement;
                        if (layer != null && layer.Selectable.ToLower() == bool.TrueString.ToLower())
                        {
                            #region Selectable_Layers

                            List<MapColumnElement> columns = new List<MapColumnElement>();
                            foreach (MapColumnElement col in layer.Columns)
                            {
                                if (col.Selectable.ToLower() == bool.TrueString.ToLower() && string.IsNullOrEmpty(col.Alias) == false)
                                    columns.Add(col);
                            }
                            if (columns.Count > 0)
                            {
                                List<String> fields = new List<string>();
                                fields.Add((string)featureCollection[0][columns[0].Alias]);

                                for (int i = 1; i < columns.Count; i++)
                                {
                                    MapColumnElement col = columns[i];

                                    fields.Add("<b>" + col.Alias + " :" + "</b> " + featureCollection[0][col.Name]);
                                }

                                if (ShowSuperToolTipEvent != null)
                                {
                                    ShowSuperToolTipEvent(this, new ShowSuperToolTipEventArgs(typeof(MapColumnElement),
                                        0, e.Position, fields));
                                }
                            }
                            #endregion
                        }
                    }
                }
            }
        }

        public override System.Drawing.Point GetRelativePoint(GeoPoint point)
        {
            System.Drawing.Point newPoint;
            mapControl.Map.DisplayTransform.ToDisplay(new DPoint(point.Lon, point.Lat), out newPoint);
            return newPoint;
        }

        public override bool IsVisible(System.Drawing.Point point)
        {
            return this.mapControl.DisplayRectangle.Contains(point.X, point.Y);
        }

        public override void InsertTempObject(MapObject obj, string tableName)
        {
            //TODO: fill
        }
        
        public override void DrawPing(GeoPoint selectedPosition)
        {
            Catalog Cat = MapInfo.Engine.Session.Current.Catalog;
            Table tbl = Cat.GetTable(DynTableIconsTemp);
            if (tbl != null)
                tbl.Close();
            // create a temporary table and add a featurelayer for it
            FeatureLayer layerTemp = new FeatureLayer(
                Session.Current.Catalog.CreateTable(
                TableInfoFactory.CreateTemp(DynTableIconsTemp), new TableSessionInfo()));
            layerTemp.VisibleRange = new VisibleRange(0, true, 13, true, MapInfo.Geometry.DistanceUnit.Mile);
            layerTemp.VisibleRangeEnabled = true;
            mapControl.Map.Layers.Add(layerTemp);
            tbl = Cat.GetTable(DynTableIconsTemp);

            if (tbl != null)
            {
                FeatureGeometry pt = new MapInfo.Geometry.Point(layerTemp.CoordSys, new DPoint(selectedPosition.Lon, selectedPosition.Lat)) as FeatureGeometry;
                CompositeStyle cs = new CompositeStyle(new BitmapPointStyle("PIN5-32.BMP", BitmapStyles.None, Color.White, 20));
                Feature ftr = new Feature(tbl.TableInfo.Columns);
                ftr.Geometry = pt;
                ftr.Style = cs;
                Key currentFeatureKey = tbl.InsertFeature(ftr);
                mapControl.Map.SetView(new DPoint(selectedPosition.Lon, selectedPosition.Lat),
                    mapControl.Map.GetDisplayCoordSys(),
                    mapControl.Map.Zoom);
            }
        }

        //public override bool ReproduceTrack(MapPoint mapPoint)
        //{
        //    Catalog Cat = MapInfo.Engine.Session.Current.Catalog;
        //    Table tbl = Cat.GetTable(DynTableUnitTracksName);

        //    SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchWhere("Code = " + mapPoint.Code);
        //    Feature ftr = Cat.SearchForFeature(tbl, si);
        //    if (ftr != null)
        //    {
        //        MIConnection miConnection = new MIConnection();
        //        miConnection.Open();
        //        MICommand miCommand = miConnection.CreateCommand();
        //        UpdateObject(mapPoint);
        //        miCommand.Dispose();
        //        miConnection.Close();
        //        miConnection.Dispose();

        //        DPoint point = new DPoint(mapPoint.Position.Lon, mapPoint.Position.Lat);
        //        System.Drawing.Point clientPoint;
        //        mapControl.Map.DisplayTransform.ToDisplay(point, out clientPoint);

        //        if (mapControl.DisplayRectangle.Contains(clientPoint))
        //            mapControl.Invalidate();
        //        else
        //            mapControl.Map.SetView(point, mapControl.Map.GetDisplayCoordSys(), mapControl.Map.Zoom);
        //        return false;
        //    }
        //    return true;
        //}

        //public override void RestoreAllTracksUnless(int unitCode)
        //{
        //    Catalog Cat = MapInfo.Engine.Session.Current.Catalog;
        //    Table tbl = Cat.GetTable(DynTableTracksName);
        //    foreach (int code in tracks.Keys)
        //        if (code != unitCode)
        //            tbl.InsertFeature(tracks[code]);
        //}

        /// <summary>
        /// Metodo que precalcula los querys sobre los layers estaticos.
        /// </summary>
        public override void BuildPreQuerys(Dictionary<string, IList> layersInfo)
        {
            gisSession.layersInfo = layersInfo;

            Dictionary<string, string> layersQuery = new Dictionary<string, string>();
            foreach (string layerName in layersInfo.Keys)
            {
                FeatureLayer layer = mapControl.Map.Layers[layerName] as FeatureLayer;

                List<string> colums = new List<string>();
                foreach (MapColumnElement column in layersInfo[layerName])
                {
                    Column colum = layer.Table.TableInfo.Columns[column.Name];
                    if (layer.Table.TableInfo.Columns.Contains(colum))
                    {
                        colums.Add(layer.Table.Alias + "." + colum.Alias + " LIKE '%{0}%'");
                    }
                }
                string ql = string.Join(" OR ", colums.ToArray());
                layersQuery.Add(layerName, ql);
            }
            gisSession.layersQuery = layersQuery;
        }

        public override IList<GeoAddress> Search(GeoAddress geoAddress)
        {
            return this.gisSession.Search(geoAddress);
        }

        public override void InitializeMaps()
        {          
            if (fileBytes == null) fileBytes = File.ReadAllBytes(fileName);

            WorkSpaceLoader loader = new WorkSpaceLoader(new MemoryStream(fileBytes));
            loader = MXSession.FormatWorkSpace(loader, mapsFolder);
            loader.Load(mapControl.Map);
            mapControl.Map.SetView(new DPoint(CenterLon, CenterLat), mapControl.Map.GetDisplayCoordSys(), new Distance(5120, MapInfo.Geometry.DistanceUnit.Meter));
            gisSession = new MXSession(new MemoryStream(fileBytes), mapsFolder);

            MapInfo.Engine.Session.Current.StyleRepository.BitmapPointStyleRepository.Reload(
               Application.StartupPath + SmartCadConfiguration.MapsIconsFolder);

            if (Initialized != null)
                Initialized(this, null);
        }


        #endregion

        #region Private Methods
        /// <summary>
        /// Este metodo crea la tabla temporal de mapas, en la cual se guarda
        /// la edicion de las cosas de mapas.
        /// </summary>
        private Feature CreateTempPolygonInLayer(List<GeoPoint> geoPoints, string tableName)
        {
            List<DPoint> pnts = geoPoints.ConvertAll<DPoint>(delegate(GeoPoint obj)
            {
                return new DPoint(obj.Lon, obj.Lat);
            });
            return CreateTempPolygonInLayer(pnts, tableName);
        }

        /// <summary>
        /// Este metodo crea la tabla temporal de mapas, en la cual se guarda
        /// la edicion de las cosas de mapas.
        /// </summary>
        private Feature CreateTempPolygonInLayer(List<DPoint> e, string tableName)
        {
            lock (Layer)
            {
                Catalog Cat = MapInfo.Engine.Session.Current.Catalog;
                Table tbl = Cat.GetTable(tableName);
                MapInfo.Geometry.DPoint[] dPoints = new MapInfo.Geometry.DPoint[e.Count];
                for (int i = 0; i < e.Count; i++)
                {
                    dPoints[i] = new MapInfo.Geometry.DPoint(e[i].x, e[i].y);
                }
                MapInfo.Geometry.FeatureGeometry g =
                    new MapInfo.Geometry.MultiPolygon(this.mapControl.Map.GetDisplayCoordSys(),
                        MapInfo.Geometry.CurveSegmentType.Linear, dPoints);
                MapInfo.Styles.SimpleInterior sis = new MapInfo.Styles.SimpleInterior(1, Color.Transparent);
                MapInfo.Styles.LineWidth lw = new MapInfo.Styles.LineWidth(3, MapInfo.Styles.LineWidthUnit.Pixel);
                MapInfo.Styles.SimpleLineStyle sl = new MapInfo.Styles.SimpleLineStyle(lw, 3);
                MapInfo.Styles.AreaStyle ar = new MapInfo.Styles.AreaStyle(sl, sis);

                MapInfo.Styles.CompositeStyle cs = new MapInfo.Styles.CompositeStyle(ar, sl, null, null);
                return new Feature(g, cs);
            }
        }

        private void CreateUnitTempTable(string customCode)
        {
            Catalog Cat = MapInfo.Engine.Session.Current.Catalog;
            Table tbl = Cat.GetTable(customCode);
            if (tbl != null)
                tbl.Close();
            // create a temporary table and add a featurelayer for it
            FeatureLayer layerTemp = new FeatureLayer(
                Session.Current.Catalog.CreateTable(
                TableInfoFactory.CreateTemp(customCode), new TableSessionInfo()));
            mapControl.Map.Layers.Add(layerTemp);
        }

        private void LoadMap()
        {
            loaderAux.Load(mapControl.Map);
        }

        /// <summary>
        /// Este método chequea si un objeto (ejem. estacion) 
        /// está contenido en otro objeto (ejem. zona)
        /// </summary>
        /// <param name="points"></param>
        /// <param name="ftr"></param>
        /// <returns></returns>
        private bool CheckObjectInFeature(List<GeoPoint> points, Feature ftr)
        {
            int i = 0;
            foreach (GeoPoint point in points)
            {
                if (ftr.Geometry.ContainsPoint(new DPoint(point.Lon, point.Lat)))
                {
                    i++;
                }
            }
            if (i != points.Count)
            {
                return false;
            }
            return true;
        }
        #endregion
                      

        #region PreTimer Methods
        
        private void InsertOrUpdateUnit(MapObjectUnit data)
        {
            lock (unitsQueue)
            {
                if (unitsQueue.ContainsKey(data.Code))
                    unitsQueue[data.Code] = data;
                else
                    unitsQueue.Add(data.Code, data);
            }
        }

        #endregion

        #region Timer Methods

        private void SimulatorTimer(object sender, System.EventArgs e)
        {
            lock (queue)
            {
                timerRefreshObjects.Interval = int.MaxValue;
                int nUpdates = 0;
                while (queueObjectsUpdates.Count > 0 && nUpdates++ < MAXUPDATES)
                {
                    CommittedDataAction action = CommittedDataAction.None;
                    if (queueActionUpdates.Count > 0) action = queueActionUpdates.Peek();

                    if (queueObjectsUpdates.Peek() is int)
                    {
                        #region Int
                        int distance = (int)queueObjectsUpdates.Dequeue();
                        mapControl.Map.SetView(this.mapControl.Map.Center, mapControl.Map.GetDisplayCoordSys(), new Distance(distance, MapInfo.Geometry.DistanceUnit.Meter));
                        #endregion
                    }
                    else if (queueObjectsUpdates.Peek() is GeoPoint)
                    {
                        #region GeoPoint
                        GeoPoint point = queueObjectsUpdates.Dequeue() as GeoPoint;
                        if (action == CommittedDataAction.HighLight)
                        {
                            HighLightSingle_Helper(point);
                            queueActionUpdates.Dequeue();
                        }
                        else
                        {
                            mapControl.Map.SetView(new DPoint(point.Lon, point.Lat), mapControl.Map.GetDisplayCoordSys(), mapControl.Map.Zoom);
                        }
                        #endregion
                    }
                    else if (queueObjectsUpdates.Peek() is string)
                    {
                        #region String
                        DeleteAllInTable(queueObjectsUpdates.Dequeue() as string);
                        queueActionUpdates.Dequeue();
                        #endregion
                    }
                    else if (queueObjectsUpdates.Peek() is MapObject)
                    {
                        #region MapObject
                        MapObject obj = queueObjectsUpdates.Peek() as MapObject;

                        string tableName = "";
                        if (obj is MapObjectIncident)
                            tableName = DynTableIncidentsName;
                        else if (obj is MapObjectStruct)
                            tableName = DynTablePostName;
                        else if (obj is MapObjectZone)
                            tableName = DynTablePolygonZoneName;
                        else if (obj is MapObjectStation)
                            tableName = DynTablePolygonStationName;
                        else if (obj is MapObjectTrack)
                            tableName = DynTableTracksName;
                        else if (obj is MapObjectUnitTrack)
                            tableName = DynTableUnitTracksName;
                        else if (obj is MapObjectUnitFollow)
                            tableName = obj.Fields["CustomCode"].ToString();
                        else if (obj is MapObjectRoute || obj is MapObjectBusStop)
                            tableName = DynTableRoute;
                        else if (obj is MapObjectAlarm)
                            tableName = DynTableTelemetryAlarm;
                        else if (obj is MapObjectUnit)
                            tableName = DynTableUnitsName;
                        else
                            tableName = DynTableTemp;

                        Table table = Session.Current.Catalog.GetTable(tableName);

                        switch (action)
                        {
                            case CommittedDataAction.Save:
                                if (obj is MapPoint)
                                    InsertObject((MapPoint)obj, table);
                                else if (obj is MapPolygon)
                                    InsertObject((MapPolygon)obj, table);
                                else
                                    InsertObject((MapCurve)obj, table);
                                queueActionUpdates.Dequeue();
                                queueObjectsUpdates.Dequeue();
                                break;
                            case CommittedDataAction.Update:
                                if (obj is MapPoint)
                                    UpdateObject((MapPoint)obj, table);
                                else if (obj is MapPolygon)
                                    UpdateObject((MapPolygon)obj, table);
                                else
                                    UpdateObject((MapCurve)obj, table);
                                queueActionUpdates.Dequeue();
                                queueObjectsUpdates.Dequeue();
                                break;
                            case CommittedDataAction.Delete:
                                DeleteObject(obj, table);
                                queueActionUpdates.Dequeue();
                                queueObjectsUpdates.Dequeue();
                                break;
                            default:
                                queueActionUpdates.Dequeue();
                                queueObjectsUpdates.Dequeue();
                                break;
                        }
                        #endregion
                    }
                    else if (queueObjectsUpdates.Peek() is MapActions)
                    {
                        #region MapLayersActions
                        MapActions clearAction = (MapActions)queueObjectsUpdates.Peek();
                        switch (clearAction)
                        {
                            case MapActions.ClearDistanceTable:
                                DeleteAllInTable(DynTableDistance);
                                break;
                            case MapActions.ClearTempTable:
                                ClearTempTable();
                                break;
                            default:
                                break;
                        }
                        queueActionUpdates.Dequeue();
                        queueObjectsUpdates.Dequeue();
                        #endregion
                    }
                }
                if (queueObjectsUpdates.Count > 0)
                    timerRefreshObjects.Interval = MAXSLEEPTIME;
                else
                    timerRefreshObjects.Enabled = false;
            }
        }

        private void UnitsTimer(object sender, System.EventArgs e)
        {
            lock (unitsQueue)
            {
                if (unitsQueue.Count > 0)
                {
                    timerRefreshUnits.Interval = int.MaxValue;
                    Table table = Session.Current.Catalog.GetTable(DynTableUnitsName);
                    if (table != null)
                    {
                        int nUpdates = 0;
                        while (unitsQueue.Count > 0 && nUpdates++ < UNITSMAXUPDATES)
                        {
                            MapObjectUnit unit = unitsQueue.First().Value;
                            UpdateObject(unit, table);
                            unitsQueue.Remove(unit.Code);
                        }
                    }
                    if (unitsQueue.Count == 0)
                        timerRefreshUnits.Interval = 1000;
                    else
                        timerRefreshUnits.Interval = MAXSLEEPTIME;
                }
            }
        }

        #endregion

        #region PostTimerMethods

        #region -- Insert --
        private void InsertObject(MapPoint obj, Table table)
        {
            if (table != null)
            {
                MIConnection miConnection = CreateConnection();
                MICommand miCommand = miConnection.CreateCommand();

                FeatureGeometry pt = new MapInfo.Geometry.Point(
                    new FeatureLayer(table).CoordSys, new DPoint(obj.Position.Lon, obj.Position.Lat)) as FeatureGeometry;
                CompositeStyle cs = new CompositeStyle(new BitmapPointStyle(
                    obj.IconName, BitmapStyles.None, Color.Empty, MapControlEx.BitmapIconSize));

                string text = "insert into \"" + table.Alias + "\" values (@obj, @MI_Style";
                if (obj.Code > -1) text += ", @Code";

                foreach (string field in obj.Fields.Keys)
                {
                    text += ", @" + field;
                }
                miCommand.CommandText = text + ")";

                miCommand.Parameters.Add("@obj", pt);
                miCommand.Parameters.Add("@MI_Style", cs);
                if (obj.Code > -1) miCommand.Parameters.Add("@Code", obj.Code);

                foreach (string field in obj.Fields.Keys)
                {
                    miCommand.Parameters.Add("@" + field, obj.Fields[field]);
                }

                miCommand.ExecuteNonQuery();
                miCommand.Dispose();
                miConnection.Close();
                miConnection.Dispose();
            }
            
        }

        private void InsertObject(MapPolygon obj, Table table)
        {
            if (obj.Geometry.Count() > 0)
            {
                MIConnection miConnection = CreateConnection();
                MICommand miCommand = miConnection.CreateCommand();

                DPoint[] dPoints = obj.Geometry.ConvertAll<DPoint>(delegate(GeoPoint toConv)
                {
                    return new DPoint(toConv.Lon, toConv.Lat);
                }).ToArray();

                FeatureGeometry g = new MultiPolygon(CoordGlobal, CurveSegmentType.Linear, dPoints);
                SimpleInterior sis = new SimpleInterior(obj.FillPattern, obj.Color, obj.Color, true);
                LineWidth lw = new LineWidth(3, LineWidthUnit.Pixel);
                SimpleLineStyle sl = new SimpleLineStyle(lw, obj.LinePattern, obj.Color);
                AreaStyle ar = new AreaStyle(sl, sis);
                CompositeStyle cs = new CompositeStyle(ar, null, null, null);

                string text = "insert into \"" + table.Alias + "\" values (@obj, @MI_Style, @Code";
                foreach (string field in obj.Fields.Keys)
                {
                    text += ", @" + field;
                }
                miCommand.CommandText = text + ")";
                miCommand.Parameters.Add("@obj", g);
                miCommand.Parameters.Add("@MI_Style", cs);
                miCommand.Parameters.Add("@Code", obj.Code);
                foreach (string field in obj.Fields.Keys)
                {
                    miCommand.Parameters.Add("@" + field, obj.Fields[field].ToString());
                }

                miCommand.ExecuteNonQuery();
                miCommand.Dispose();
                miConnection.Close();
                miConnection.Dispose();
            }
        }

        private void InsertObject(MapCurve obj, Table table)
        {
            if (obj.Geometry.Count() > 0)
            {
                MIConnection miConnection = CreateConnection();
                MICommand miCommand = miConnection.CreateCommand();

                DPoint[] dPoints = obj.Geometry.ConvertAll<DPoint>(delegate(GeoPoint toConv)
                {
                    return new DPoint(toConv.Lon, toConv.Lat);
                }).ToArray();

                FeatureGeometry g = new MultiCurve(CoordGlobal, CurveSegmentType.Linear, dPoints);

                SimpleInterior sis = new SimpleInterior(1, Color.Transparent, obj.Color);
                LineWidth lw = new LineWidth(2, LineWidthUnit.Pixel);
                SimpleLineStyle sl = new SimpleLineStyle(lw, 54, obj.Color);
                CompositeStyle cs = new CompositeStyle(sl);

                string text = "insert into \"" + table.Alias + "\" values (@obj, @MI_Style, @Code";
                foreach (string field in obj.Fields.Keys)
                {
                    text += ", @" + field;
                }
                miCommand.CommandText = text + ")";
                miCommand.Parameters.Add("@obj", g);
                miCommand.Parameters.Add("@MI_Style", cs);
                miCommand.Parameters.Add("@Code", obj.Code);
                foreach (string field in obj.Fields.Keys)
                {
                    miCommand.Parameters.Add("@" + field, obj.Fields[field].ToString());
                }

                miCommand.ExecuteNonQuery();
                miCommand.Dispose();
                miConnection.Close();
                miConnection.Dispose();
            }
        }

        private double InsertLinesHelp(List<GeoPoint> points)
        {
            double totalDistance = 0;
            for (int i = 0; i < points.Count - 1; i++)
            {
                lock (Layer)
                {
                    DPoint[] _lastSegmentPoints = new DPoint[2];
                    _lastSegmentPoints[0] = new DPoint(points[i].Lon, points[i].Lat);
                    _lastSegmentPoints[1] = new DPoint(points[i + 1].Lon, points[i + 1].Lat);
                    Curve curve = new Curve(mapControl.Map.GetDisplayCoordSys(), MapInfo.Geometry.CurveSegmentType.Linear, _lastSegmentPoints);
                    double length = curve.Length(MapInfo.Geometry.DistanceUnit.Kilometer, DistanceType.Spherical);

                    string number = "";
                    string unit = "Km";
                    if (length > 1)
                    {
                        number = string.Format("{0:0.000}", length);
                    }
                    else
                    {
                        number = ((int)(length * 1000)) + "";
                        unit = "mts";
                    }
                    string code = (points.Count - 2) + "-" + (points.Count - 1);
                    InsertLineInTempLayer(_lastSegmentPoints.ToList(), DynTableRouteTemp, number, code, unit);
                    totalDistance += length;
                }
            }
            return totalDistance;
        }

        private void InsertLineInTempLayer(List<DPoint> list, string tableName, string number, string code, string unit)
        {
            lock (Layer)
            {
                MIConnection miConnection = CreateConnection();
                MICommand miCommand = miConnection.CreateCommand();
                DPoint[] dPoints = list.ToArray();

                FeatureGeometry g = new MultiPolygon(CoordGlobal, CurveSegmentType.Linear, dPoints);

                SimpleInterior sis = new MapInfo.Styles.SimpleInterior(1, Color.Yellow);
                LineWidth lw = new MapInfo.Styles.LineWidth(2, MapInfo.Styles.LineWidthUnit.Pixel);
                SimpleLineStyle sl = new MapInfo.Styles.SimpleLineStyle(lw);
                AreaStyle ar = new MapInfo.Styles.AreaStyle(sl, sis);
                CompositeStyle cs = new MapInfo.Styles.CompositeStyle(ar, null, null, null);

                miCommand.CommandText = String.Format("insert into \"{0}\" values (@obj, @MI_Style, @Lenght, @Code)", tableName);
                miCommand.Parameters.Add("@obj", g);
                miCommand.Parameters.Add("@MI_Style", cs);
                miCommand.Parameters.Add("@Lenght", String.Format("{0} {1}", number, unit));
                miCommand.Parameters.Add("@Code", code);

                miCommand.ExecuteNonQuery();
                miCommand.Dispose();
                miConnection.Close();
                miConnection.Dispose();
            }
        }

        private void InsertLineInTempLayer(List<DPoint> e, string tableName)
        {
            lock (Layer)
            {
                Catalog Cat = MapInfo.Engine.Session.Current.Catalog;
                Table tbl = Cat.GetTable(tableName);
                MapInfo.Geometry.DPoint[] dPoints = new MapInfo.Geometry.DPoint[e.Count];
                for (int i = 0; i < e.Count; i++)
                {
                    dPoints[i] = new MapInfo.Geometry.DPoint(e[i].x, e[i].y);
                }
                MapInfo.Geometry.FeatureGeometry g =
                    new MapInfo.Geometry.MultiCurve(mapControl.Map.GetDisplayCoordSys(), CurveSegmentType.Linear, dPoints);
                MapInfo.Styles.SimpleInterior sis = new MapInfo.Styles.SimpleInterior(1, Color.Transparent);
                MapInfo.Styles.LineWidth lw = new MapInfo.Styles.LineWidth(3, MapInfo.Styles.LineWidthUnit.Pixel);
                MapInfo.Styles.SimpleLineStyle sl = new MapInfo.Styles.SimpleLineStyle(lw);
                MapInfo.Styles.AreaStyle ar = new MapInfo.Styles.AreaStyle(sl, sis);

                MapInfo.Styles.CompositeStyle cs = new MapInfo.Styles.CompositeStyle(ar, null, null, null);

                Feature f = new Feature(g, cs);

                CurrentFeatureKey = tbl.InsertFeature(f);
            }
        }
        #endregion

        #region -- Update --
        public void UpdateObject(MapPoint obj, Table table)
        {
            MIConnection miConnection = CreateConnection();
            MICommand miCommand = miConnection.CreateCommand();

            FeatureGeometry pt = new MapInfo.Geometry.Point(new FeatureLayer(table).CoordSys, new DPoint(obj.Position.Lon, obj.Position.Lat)) as FeatureGeometry;
            CompositeStyle cs = new CompositeStyle(new BitmapPointStyle(obj.IconName, BitmapStyles.None, Color.Empty, MapControlEx.BitmapIconSize));

            string text = "update \"" + table.Alias + "\" set obj = @obj, MI_Style = @MI_Style";
            foreach (string field in obj.Fields.Keys)
            {
                text += ", " + field + " = @" + field;
            }

            text += " where Code = " + obj.Code;

            miCommand.CommandText = text;
            miCommand.Parameters.Add("@obj", pt);
            miCommand.Parameters.Add("@MI_Style", cs);
           // miCommand.Parameters.Add("@Code", obj.Code);
            foreach (string field in obj.Fields.Keys)
            {
                miCommand.Parameters.Add("@" + field, obj.Fields[field].ToString());
            }

            miCommand.ExecuteNonQuery();
            miCommand.Dispose();
            miConnection.Close();
            miConnection.Dispose();
        }

        public void UpdateObject(MapPolygon obj, Table table)
        {
            if (obj.Geometry.Count() > 0)
            {
                MIConnection miConnection = CreateConnection();
                MICommand miCommand = miConnection.CreateCommand();

                DPoint[] dPoints = obj.Geometry.ConvertAll<DPoint>(delegate(GeoPoint toConv)
                {
                    return new DPoint(toConv.Lon, toConv.Lat);
                }).ToArray();

                FeatureGeometry g = new MultiPolygon(CoordGlobal, CurveSegmentType.Linear, dPoints);
                SimpleInterior sis = new SimpleInterior(obj.FillPattern, obj.Color, obj.Color, true);
                LineWidth lw = new LineWidth(3, LineWidthUnit.Pixel);
                SimpleLineStyle sl = new SimpleLineStyle(lw, obj.LinePattern, obj.Color);
                AreaStyle ar = new AreaStyle(sl, sis);
                CompositeStyle cs = new CompositeStyle(ar, null, null, null);


                string text = "update \"" + table.Alias + "\" set obj = @obj, MI_Style = @MI_Style";
                foreach (string field in obj.Fields.Keys)
                {
                    text += ", " + field + " = @" + field;
                }

                text += " where Code = " + obj.Code;

                miCommand.CommandText = text;
                miCommand.Parameters.Add("@obj", g);
                miCommand.Parameters.Add("@MI_Style", cs);
                //miCommand.Parameters.Add("@Code", obj.Code);
                foreach (string field in obj.Fields.Keys)
                {
                    miCommand.Parameters.Add("@" + field, obj.Fields[field].ToString());
                }

                miCommand.ExecuteNonQuery();
                miCommand.Dispose();
                miConnection.Close();
                miConnection.Dispose();
            }
            else
            {
                DeleteObject(obj,table);
            }
        }

        public void UpdateObject(MapCurve obj, Table table)
        {
            if (obj.Geometry.Count() > 0)
            {
                MIConnection miConnection = CreateConnection();
                MICommand miCommand = miConnection.CreateCommand();

                DPoint[] dPoints = obj.Geometry.ConvertAll<DPoint>(delegate(GeoPoint toConv)
                {
                    return new DPoint(toConv.Lon, toConv.Lat);
                }).ToArray();

                FeatureGeometry g = new MultiCurve(CoordGlobal, CurveSegmentType.Linear, dPoints);
                SimpleInterior sis = new SimpleInterior(1, Color.Transparent, obj.Color);
                LineWidth lw = new LineWidth(2, LineWidthUnit.Pixel);
                SimpleLineStyle sl = new SimpleLineStyle(lw, 54, obj.Color);
                CompositeStyle cs = new CompositeStyle(sl);


                string text = "update \"" + table.Alias + "\" set obj = @obj, MI_Style = @MI_Style";
                foreach (string field in obj.Fields.Keys)
                {
                    text += ", " + field + " = @" + field;
                }

                text += " where Code = " + obj.Code;

                miCommand.CommandText = text;
                miCommand.Parameters.Add("@obj", g);
                miCommand.Parameters.Add("@MI_Style", cs);
                //miCommand.Parameters.Add("@Code", obj.Code);
                foreach (string field in obj.Fields.Keys)
                {
                    miCommand.Parameters.Add("@" + field, obj.Fields[field].ToString());
                }

                miCommand.ExecuteNonQuery();
                miCommand.Dispose();
                miConnection.Close();
                miConnection.Dispose();
            }
            else
            {
                DeleteObject(obj, table);
            }
        }
        #endregion

        #region -- Delete --
        public void DeleteObject(MapObject obj, Table table)
        {
            MIConnection miConnection = CreateConnection();
            MICommand miCommand = miConnection.CreateCommand();

            miCommand.CommandText = "delete from \"" + table.Alias + "\" where Code = " + obj.Code;

            miCommand.ExecuteNonQuery();
            miCommand.Dispose();
            miConnection.Close();
            miConnection.Dispose();
        }

        private void DeleteAllInTable(string name)
        {
            Catalog Cat = MapInfo.Engine.Session.Current.Catalog;
            Table tbl = Cat.GetTable(name);
            if (tbl != null)
            {
                SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchAll();
                IResultSetFeatureCollection setres = Cat.Search(tbl, si);
                if (setres.Count > 0)
                {
                    tbl.BeginAccess(MapInfo.Data.TableAccessMode.Write);
                    (tbl as ITableFeatureCollection).Remove(setres);
                    tbl.EndAccess();
                    tbl.Pack(MapInfo.Data.PackType.All);
                }
            }
        }

        private void ClearTempTable()
        {
            DeleteAllInTable(DynTableTemp);
            CurrentFeatureKey = null;
        }
        #endregion

        #region -- Search --
        public IResultSetFeatureCollection Search(Table table, SearchInfo searchInfo)
        {
            lock (catalogSync)
            {
                return Session.Current.Catalog.Search(table, searchInfo);
            }
        }

        public Feature SearchForFeature(Table table, SearchInfo searchInfo)
        {
            lock (catalogSync)
            {
                return Session.Current.Catalog.SearchForFeature(table, searchInfo);
            }
        }
        #endregion

        private MIConnection CreateConnection()
        {
            MIConnection miConnection = new MapInfo.Data.MIConnection();
            miConnection.Open();
            return miConnection;
        }    
        
        #endregion

                      
        #region Draw Methods

        private void DrawLinesOutsideView(List<DPoint> notVisiblePoints, Color colorRecommended, Graphics gra, System.Drawing.Point incPointClient)
        {
            for (int k = 0; k < notVisiblePoints.Count; k++)
            {
                if (notVisiblePoints[k].x != (double)0.0 && notVisiblePoints[k].y != (double)0.0)
                {
                    System.Drawing.Point clientPoint;
                    this.mapControl.Map.DisplayTransform.ToDisplay(notVisiblePoints[k], out clientPoint);
                    SolidBrush sb = new SolidBrush(colorRecommended);
                    Pen pen = new Pen(sb, 2);

                    pen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dash;
                    gra.DrawLine(pen, incPointClient, clientPoint);
                }
            }
        }

        private void DrawCircleInMapWithPoints(List<DPoint> listPointsAsignedVisible, int radio, int centerDiffx, int centerDiffy, Graphics gra, Color color)
        {
            for (int k = 0; k < listPointsAsignedVisible.Count; k++)
            {
                System.Drawing.Point clientPoint;
                this.mapControl.Map.DisplayTransform.ToDisplay(listPointsAsignedVisible[k], out clientPoint);
                int xcenter = clientPoint.X - 32 + centerDiffx;
                int ycenter = clientPoint.Y - 32 + centerDiffy;

                SolidBrush sb = new SolidBrush(Color.FromArgb(75, color.R, color.G, color.B));
                gra.DrawEllipse(new Pen(new SolidBrush(Color.Black), 2), xcenter, ycenter, radio, radio);
                gra.FillEllipse(sb, xcenter, ycenter, radio, radio);
            }
        }

        #endregion

        #region HighLight Methods

        public override void HighLightSingle(GeoPoint point)
        {
            queueActionUpdates.Enqueue(CommittedDataAction.HighLight);
            queueObjectsUpdates.Enqueue(point);
            FormUtil.InvokeRequired(this, delegate()
            {
                this.timerRefreshObjects.Interval = 10;
                this.timerRefreshObjects.Enabled = true;
                this.timerRefreshObjects.Start();
            });
        }

        private void HighLightSingle_Helper(GeoPoint point)
        {
            lock (Layer)
            {
                System.Drawing.Point clientPoint;
                mapControl.Map.DisplayTransform.ToDisplay(new DPoint(point.Lon, point.Lat), out clientPoint);
                for (int j = 0; j < 2; j++)
                {
                    int xcenter = clientPoint.X - 32;
                    int ycenter = clientPoint.Y - 32;
                    int radio = 76;
                    for (int i = 0; i < 4; i++)
                    {
                        SolidBrush sb = new SolidBrush(Color.FromArgb(75, 255, 255, 255));
                        Graphics gra = this.mapControl.CreateGraphics();
                        gra.DrawEllipse(new Pen(new SolidBrush(Color.Blue), 2), xcenter, ycenter, radio, radio);
                        gra.FillEllipse(sb, xcenter, ycenter, radio, radio);
                        gra.Dispose();
                        Thread.Sleep(85);
                        this.mapControl.Invalidate(new System.Drawing.Rectangle(xcenter - 1, ycenter - 1, radio + 3, radio + 3));
                        this.mapControl.Update();
                        radio -= 18;
                        xcenter += 9;
                        ycenter += 9;
                    }
                }
                Thread.Sleep(85);
                this.mapControl.Invalidate();
                this.mapControl.Update();
            }
        }

        public override void HighLightUnit(MapObjectUnit unit)
        {
            HighLightSingle(unit.Position);
        }

        public override void HighLightIncident(MapObjectIncident inc)
        {
            if (inc.Position != null)
            {
                CenterMapInPoint(inc.Position);
                Thread.Sleep(500);
                FormUtil.InvokeRequired(this, delegate
                {
                    Thread.Sleep(100);
                    HighLightSingle(inc.Position);
                });

            }
        }

        public override void HighLightUnits(MapObjectIncident inc, List<HLGisObject> list, Color color)
        {
            HighLightIncident(inc);
            HighLightWithColor(list, color);
        }

        public override void HighLightUnits(List<HLGisObject> list)
        {
            GeoPoint ipoint = new GeoPoint(list[0].Lon, list[0].Lat);
            list.RemoveAt(0);
            List<DPoint> listPointsAsignedVisible = new List<DPoint>();
            List<DPoint> listPointsAsignedNotVisible = new List<DPoint>();
            List<HLGisObject> listAsigned = list.Where(hl => hl.HLType == HLGisObjectType.UnitAsigned).ToList();
            GetListPointsVisible(listAsigned, listPointsAsignedVisible, listPointsAsignedNotVisible);
            List<DPoint> listPointsRecommendedVisible = new List<DPoint>();
            List<DPoint> listPointsRecommendedNotVisible = new List<DPoint>();
            List<HLGisObject> listRecommended = list.Where(hl => hl.HLType == HLGisObjectType.UnitsRecommended).ToList();
            GetListPointsVisible(listRecommended, listPointsRecommendedVisible, listPointsRecommendedNotVisible);

            FormUtil.InvokeRequired(this, delegate
            {
                HighLightUnitsTotalFromDispatch(listPointsAsignedVisible, listPointsAsignedNotVisible, listPointsRecommendedVisible, listPointsRecommendedNotVisible, ipoint, Color.BlueViolet, Color.Yellow);
            });
        }

        private void HighLightUnitsTotalFromDispatch(List<DPoint> listPointsAsignedVisible, List<DPoint> listPointsAsignedNotVisible, List<DPoint> listPointsRecommendedVisible, List<DPoint> listPointsRecommendedNotVisible, GeoPoint ipoint, Color colorAsigned, Color colorRecommended)
        {
            for (int j = 0; j < 3; j++)
            {
                int radio = 64;
                int centerDiffx = 0;
                int centerDiffy = 0;
                for (int i = 0; i < 3; i++)
                {
                    lock (Layer)
                    {
                        Graphics gra = this.mapControl.CreateGraphics();
                        DrawCircleInMapWithPoints(listPointsAsignedVisible, radio, centerDiffx, centerDiffy, gra, colorAsigned);
                        DrawCircleInMapWithPoints(listPointsRecommendedVisible, radio, centerDiffx, centerDiffy, gra, colorRecommended);
                        if (i == 2)
                        {
                            System.Drawing.Point incPointClient;
                            mapControl.Map.DisplayTransform.ToDisplay(new DPoint(ipoint.Lon, ipoint.Lat), out incPointClient);
                            DrawLinesOutsideView(listPointsAsignedNotVisible, colorAsigned, gra, incPointClient);
                            DrawLinesOutsideView(listPointsRecommendedNotVisible, colorRecommended, gra, incPointClient);
                        }
                        gra.Dispose();
                        Thread.Sleep(255);
                        this.mapControl.Invalidate(this.mapControl.Region);
                        this.mapControl.Update();
                        radio -= 30;
                        centerDiffx += 15;
                        centerDiffy += 15;
                    }
                }
            }
        }
          
        private void HighLightUnitsFromDispatch(List<DPoint> points, List<DPoint> notVisiblePoints, GeoPoint incPoint, Color color)
        {
            lock (Layer)
            {
                Thread.Sleep(SLEEP_TIME_HIGHLIGHT);
                for (int j = 0; j < 3; j++)
                {
                    int radio = 64;
                    int centerDiffx = 0;
                    int centerDiffy = 0;

                    for (int i = 0; i < 3; i++)
                    {
                        Graphics gra = this.mapControl.CreateGraphics();
                        for (int k = 0; k < points.Count; k++)
                        {
                            System.Drawing.Point clientPoint;
                            this.mapControl.Map.DisplayTransform.ToDisplay(points[k], out clientPoint);
                            int xcenter = clientPoint.X - 32 + centerDiffx;
                            int ycenter = clientPoint.Y - 32 + centerDiffy;

                            SolidBrush sb = new SolidBrush(Color.FromArgb(75, color.R, color.G, color.B));
                            gra.DrawEllipse(new Pen(new SolidBrush(Color.Black), 2), xcenter, ycenter, radio, radio);
                            gra.FillEllipse(sb, xcenter, ycenter, radio, radio);
                        }
                        if (i == 2)
                        {
                            System.Drawing.Point incPointClient;
                            this.mapControl.Map.DisplayTransform.ToDisplay(new DPoint(incPoint.Lon, incPoint.Lat), out incPointClient);
                            DrawLinesOutsideView(notVisiblePoints, color, gra, incPointClient);
                        }
                        gra.Dispose();
                        Thread.Sleep(SLEEP_TIME_BETWEEN_HIGHLIGHT);
                        this.mapControl.Invalidate(this.mapControl.Region);
                        this.mapControl.Update();
                        radio -= 30;
                        centerDiffx += 15;
                        centerDiffy += 15;
                    }
                }
            }
        }

        private void HighLightWithColor(List<HLGisObject> list, Color color)
        {
            GeoPoint ipoint = new GeoPoint(list[0].Lon, list[0].Lat);
            list.RemoveAt(0);
            List<DPoint> listPointsVisible = new List<DPoint>();
            List<DPoint> listPointsNotVisible = new List<DPoint>();
            GetListPointsVisible(list, listPointsVisible, listPointsNotVisible);
            FormUtil.InvokeRequired(this, delegate
            {
                HighLightUnitsFromDispatch(listPointsVisible, listPointsNotVisible, ipoint, color);
            });
        }

        private void GetListPointsVisible(List<HLGisObject> list, List<DPoint> listPointsVisible, List<DPoint> listPointsNotVisible)
        {
            for (int i = 0; i < list.Count; i++)
            {
                //MapPoint unit = list[i].Data as MapPoint;
                //if (InternalTableUnits.CheckObjectVisible(unit))
                //{
                    DPoint upoint = new DPoint(list[i].Lon, list[i].Lat);
                    System.Drawing.Point clientPoint;
                    bool isVisibleinRec = false;
                    lock (Layer)
                    {
                        FormUtil.InvokeRequired(this, delegate
                        {
                            this.mapControl.Map.DisplayTransform.ToDisplay(upoint, out clientPoint);
                            isVisibleinRec = this.mapControl.DisplayRectangle.Contains(clientPoint);
                        });
                    }
                    if (isVisibleinRec)
                        listPointsVisible.Add(upoint);
                    else
                        listPointsNotVisible.Add(upoint);
                //}
            }
        }

        #endregion


        #region Events

        public override event ToolUsedEventHandler ToolUsed;
        public override event ToolEndingEventHandler ToolEnding;
        public override event ToolActivatedEventHandler ToolActivated;
        public override event ViewChangedEventHandler ViewChanged;
        public override event FeatureAddingEventHandler FeatureAdding;
        public override event FeatureSelectedEventHandler FeatureSelected;
        public override event DrawEventHandler DrawEvent;
        public override event ShowBallonEventHandler ShowBallonEvent;
        public override event ShowSuperToolTipEventHandler ShowSuperToolTipEvent;
        public override event EventHandler Initialized;

        void MapInfoToolUsed(object sender, MapInfo.Tools.ToolUsedEventArgs e)
        {
            ToolStatus status = ToolStatus.Starting;
            if (ToolUsed != null)
            {
                ToolInUse = e.ToolName;
                switch (e.ToolStatus)
                {
                    case MapInfo.Tools.ToolStatus.End:
                        status = ToolStatus.Ending;
                        break;
                    case MapInfo.Tools.ToolStatus.InProgress:
                        status = ToolStatus.InProgress;
                        break;
                    case MapInfo.Tools.ToolStatus.Start:
                        status = ToolStatus.Starting;
                        break;
                    default:
                        break;
                }
                ToolUsed(this, new ToolUsedEventArgs(status, new GeoPoint(e.MapCoordinate.x, e.MapCoordinate.y), e.ToolName));
            }
        }

        void MapInfoToolEnding(object sender, MapInfo.Tools.ToolEndingEventArgs e)
        {
            if (ToolEnding != null)
            {
                ToolEnding(this, new ToolEndingEventArgs(e.ToolName));
            }
        }

        void MapInfoToolActivated(object sender, MapInfo.Tools.ToolActivatedEventArgs e)
        {
            if (ToolActivated != null)
            {
                ToolActivated(this, new ToolActivatedEventArgs(e.ToolName));
            }
        }

        void MapInfoFeatureAdding(object sender, MapInfo.Tools.FeatureAddingEventArgs e)
        {
            if (FeatureAdding != null)
            {
                e.Cancel = true;
                FeatureAdding(this, new FeatureAddingEventArgs(e.ToolName));
            }
        }

        void MapInfoFeatureSelected(object sender, MapInfo.Tools.FeatureSelectedEventArgs e)
        {
            if (FeatureSelected != null)
            {
                FeatureSelected(this, new FeatureSelectedEventArgs(e.Selection, new GeoPoint(e.MapCoordinate.x, e.MapCoordinate.y)));
            }
        }

        void MapInfoViewChanged(object sender, MapInfo.Mapping.ViewChangedEventArgs e)
        {
            if (ViewChanged != null)
            {
                ViewChanged(this, new ViewChangedEventArgs());
            }
        }

        void MapInfoDrawEvent(object sender, MapInfo.Mapping.MapDrawEventArgs e)
        {
            if (DrawEvent != null)
            {
                DrawEvent(this, new DrawEventArgs());
            }

            if (ShowingHeatMap && isCenteringMap == false)
            {
                HeatMap heatmap = new HeatMap();
                Bitmap bMap = new Bitmap(this.Width, this.Height);

                int intensity = 100;
                if (mapControl.Map.Zoom.Value < 10000 & mapControl.Map.Zoom.Value > 1000)
                    intensity -= (int)mapControl.Map.Zoom.Value / 200;
                else if(mapControl.Map.Zoom.Value > 10000 )
                    intensity -= (int)mapControl.Map.Zoom.Value / 500;

                foreach (GeoPoint point in HeatPoints)
                {
                    if (point.Lat != 0 && point.Lon != 0)
                    {
                        System.Drawing.Point displayPoint = GetRelativePoint(point);
                        heatmap.HeatPoints.Add(new HeatPoint(displayPoint.X, displayPoint.Y, (byte)(intensity)));
                    }
                }

                bMap = heatmap.CreateIntensityMask(bMap);
                Graphics gra = this.mapControl.CreateGraphics();
                gra.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                gra.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                gra.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                gra.DrawImage(heatmap.Colorize(bMap, 255), new PointF(0, 0));
                gra.Dispose();
            }
        }

        DPoint PhillipinesCenter = new DPoint(7.536764, 124.453125);
      
        
        private void AddShapeFile()
        {                      
            TableInfoShapefile ti = new TableInfoShapefile("EsriShape");            
            ti.TablePath = @"C:\Epi2000\rp.shp";         
            MapInfo.Geometry.CoordSys coordSys = mapControl.Map.GetDisplayCoordSys();                                   
            ti.DefaultStyle = new MapInfo.Styles.AreaStyle(
            new MapInfo.Styles.SimpleLineStyle(new MapInfo.Styles.LineWidth(2.0, MapInfo.Styles.LineWidthUnit.Pixel),
                (int)MapInfo.Styles.PatternStyle.Solid, Color.Red),
            new MapInfo.Styles.SimpleInterior((int)MapInfo.Styles.PatternStyle.Cross, Color.Red, Color.White));            
            string pathName = @"C:\Epi2000";           
            string fileName = @"rp.dbf";          
            MapInfo.Data.TableInfoAdoNet NativeAdoTableInfo = default(MapInfo.Data.TableInfoAdoNet);
            MapInfo.Data.Table NativeAdoTable = default(MapInfo.Data.Table);
            System.Data.OleDb.OleDbConnection DbConnection = null;
            System.Data.OleDb.OleDbCommand DbCommand = null;
            System.Data.OleDb.OleDbDataAdapter DbAdapter = null;
            DataTable DbDataTable = new DataTable();
            DbConnection = 
                new System.Data.OleDb.OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" 
                    + pathName + ";Extended Properties=DBASE III;");
            DbCommand = new System.Data.OleDb.OleDbCommand("SELECT * FROM " + fileName, DbConnection);
            DbAdapter = new System.Data.OleDb.OleDbDataAdapter(DbCommand);
            try
            {
                DbConnection.Open();
                DbAdapter.Fill(DbDataTable);
                NativeAdoTableInfo = 
                    new MapInfo.Data.TableInfoAdoNet(System.IO.Path.GetFileNameWithoutExtension(fileName 
                        + "Db"), DbDataTable);
                NativeAdoTable = MapInfo.Engine.Session.Current.Catalog.OpenTable(NativeAdoTableInfo);                
                ti.Columns.Add(MapInfo.Data.ColumnFactory.CreateFeatureGeometryColumn(coordSys));
                ti.Columns.Add(MapInfo.Data.ColumnFactory.CreateStyleColumn());
                foreach (MapInfo.Data.Column col in NativeAdoTableInfo.Columns)
                {
                    Column column = ColumnFactory.CreateStringColumn(col.Alias, 100);
                    ti.Columns.Add(column);
                }

                DbConnection.Close();    
            
            }
            catch (Exception ex)
            {
              
            }
            Table tblTemp = MapInfo.Engine.Session.Current.Catalog.CreateTable(ti);/*OpenTable(ti);*/
            MapTableLoader tl = new MapTableLoader(tblTemp);
            mapControl.Map.Load(tl);
            FeatureLayer lyr = new FeatureLayer(tblTemp);

            CoordGlobal = lyr.CoordSys;
            LabelLayer lblyr = new LabelLayer(ti.Alias + "Labels", ti.Alias + "Labels");
            lyr.VolatilityHint = LayerVolatilityHint.Animate | LayerVolatilityHint.CacheIfPossible;
            lblyr.VolatilityHint = LayerVolatilityHint.Animate | LayerVolatilityHint.CacheIfPossible;
            int k = this.mapControl.Map.Layers.Add(lyr);
            this.mapControl.Map.Layers.Insert(k, lblyr);
            DPoint center = new DPoint(7.536764, 124.453125);
            mapControl.Map.Center = center;
            EnableLayer(ti.Alias, true);        
        }
        
        #endregion

        #region Event Handlers

        private void Map_ViewChangedEvent(object sender, MapInfo.Mapping.ViewChangedEventArgs e)
        {
            isCenteringMap = true;

            int initval = 40;
            int min = Int32.MaxValue;
            int zoomNow = (int)this.mapControl.Map.Zoom.Value;
            DPoint dorigin;
            DPoint dfinal;

            this.mapControl.Map.DisplayTransform.FromDisplay(new System.Drawing.Point(0, 0), out dorigin);
            this.mapControl.Map.DisplayTransform.FromDisplay(new System.Drawing.Point(0, 92), out dfinal);

            GeoPoint gp1 = new GeoPoint(dorigin.x, dorigin.y);
            GeoPoint gp2 = new GeoPoint(dfinal.x, dfinal.y);
            MapDistance = GisServiceUtil.CalculateDistance(gp1, gp2);
            string formatHigh = "";
            if (MapDistance < 1)
            {
                double mapDistanceMeters = (double)((int)(MapDistance * 1000));
                formatHigh = mapDistanceMeters + " mts";
            }
            else
            {
                formatHigh = string.Format("{0:0.000}", MapDistance) + " Km";
            }

            int pow = 0;
            for (int i = 0; i < 11; i++)
            {
                if (Math.Abs(zoomNow - initval) < min)
                {
                    pow = i;
                    min = Math.Abs(zoomNow - initval);
                }
                initval = initval * 2;
            }


            double ab = 40 * (1 << pow);
            int valueZoomBar = (int)(Math.Log(40960, 2.0) - Math.Log(ab, 2.0) - 5);
            this.zoomTrackBarControl1.Value = valueZoomBar;

            string number = string.Format("{0:0.000}", (ab / 1000.0)) + " Km";
            string textFinal = ResourceLoader.GetString2("MapZoomHight") + ":  " + number;

            if (ruler.Visible == true)
            {
                this.ruler.labelControlTotalHigh.Text = "";
                if (this.ShowDistance)
                {
                    string totalDis = string.Format("{0:0.000}", this.Distance);
                    if (this.DistanceUnit != "Km")
                        totalDis = (int)(this.Distance) + "";
                    this.ruler.labelControlTotalHigh.Text = ResourceLoader.GetString2("TotalDistance") + ": " + totalDis + " " + this.DistanceUnit;
                }
                this.ruler.labelControlZoom.Text = ResourceLoader.GetString2("Zoom") + ":  " + ((100) - (pow * 10)) + "%";
                this.ruler.labelControlHigh.Text = textFinal;
                this.ruler.rulerText.Text = formatHigh;
                this.ruler.Refresh();
            }
            this.zoomTrackBarControl1.Refresh();

            if (mapControl.Map.Zoom.Value > 40960)
            {
                mapControl.Map.ViewChangedEvent -= new MapInfo.Mapping.ViewChangedEventHandler(Map_ViewChangedEvent);
                this.mapControl.Map.SetView(this.mapControl.Map.Center, mapControl.Map.GetDisplayCoordSys(), new Distance(40960, MapInfo.Geometry.DistanceUnit.Meter));
                mapControl.Map.ViewChangedEvent += new MapInfo.Mapping.ViewChangedEventHandler(Map_ViewChangedEvent);
            }
            if (mapControl.Map.Zoom.Value < 40)
            {
                mapControl.Map.ViewChangedEvent -= new MapInfo.Mapping.ViewChangedEventHandler(Map_ViewChangedEvent);
                this.mapControl.Map.SetView(this.mapControl.Map.Center, mapControl.Map.GetDisplayCoordSys(), new Distance(40, MapInfo.Geometry.DistanceUnit.Meter));
                mapControl.Map.ViewChangedEvent += new MapInfo.Mapping.ViewChangedEventHandler(Map_ViewChangedEvent);
            }

            //Esta parte del metodo hace que los iconos se actualizen junto con el zoom para que crescan
            //en concordancia con el mapa.
            double roundZoom = Math.Round(mapControl.Map.Zoom.Value);

            if (viewChangeOldValue > 0)
            {
                if (viewChangeOldValue > roundZoom)
                    BitmapIconSize += 5;
                else if (viewChangeOldValue < roundZoom)
                    BitmapIconSize -= 5;
            }
            if (roundZoom != viewChangeOldValue)
            {
                viewChangeOldValue = roundZoom;
                SetFeatureOverrideStyleModifier(mapControl.Map.Layers[DynTableUnitsName] as FeatureLayer);
                SetFeatureOverrideStyleModifier(mapControl.Map.Layers[DynTableIncidentsName] as FeatureLayer);
                SetFeatureOverrideStyleModifier(mapControl.Map.Layers[DynTablePostName] as FeatureLayer);
                SetFeatureOverrideStyleModifier(mapControl.Map.Layers[DynTableRoute] as FeatureLayer);
                SetFeatureOverrideStyleModifier(mapControl.Map.Layers[DynTableUnitTracksName] as FeatureLayer);
            }

            if (ViewChanged != null)
                ViewChanged(this, new ViewChangedEventArgs());
            isCenteringMap = false;
        }
        /// <summary>
        /// MapViewChanged helper Method
        /// </summary>
        /// <param name="_lyr"></param>
        private void SetFeatureOverrideStyleModifier(FeatureLayer _lyr)
        {
            MapInfo.Styles.BitmapPointStyle vs = new BitmapPointStyle();
            vs.Attributes = StyleAttributes.PointAttributes.Size;
            vs.PointSize = (zoomTrackBarControl1.Value + 16) *RATE_SIZE_ICON_CHANGE;
            BitmapIconSize = (zoomTrackBarControl1.Value + 16) *RATE_SIZE_ICON_CHANGE;
            FeatureOverrideStyleModifier fsm = new FeatureOverrideStyleModifier(null, new MapInfo.Styles.CompositeStyle(vs));
            if (_lyr != null)
            {
                _lyr.Modifiers.Clear();
                _lyr.Modifiers.Append(fsm);
            }
        }

        private void zoomTrackBarControl1_EditValueChanged(object sender, EventArgs e)
        {
            int v = this.zoomTrackBarControl1.Value;
            int high = 40960 / (1 << (v + 5));
            //string number = string.Format("{0:0.000}", (high / 1000.0)) + " Km";

            if (high != this.mapControl.Map.Zoom.Value && (zoomTrackBarControl1.OldEditValue == null || v != -int.Parse(zoomTrackBarControl1.OldEditValue.ToString())))
            {
                this.mapControl.Map.SetView(this.mapControl.Map.Center, mapControl.Map.GetDisplayCoordSys(), new Distance(high, MapInfo.Geometry.DistanceUnit.Meter));
            }
        }

        private void mapControl_MouseMove(object sender, MouseEventArgs e)
        {
            DPoint latlon;
            this.mapControl.Map.DisplayTransform.FromDisplay(new PointF((float)e.X, (float)e.Y), out latlon);
            if (ruler.Visible == true)
            {
                if (!this.ruler.labelLatLon.Visible)
                    this.ruler.labelLatLon.Visible = true;
                this.ruler.labelLatLon.Text = "Lon: " + latlon.y + " Lat: " +latlon.x ;
            }
        }

        #endregion

    }
}