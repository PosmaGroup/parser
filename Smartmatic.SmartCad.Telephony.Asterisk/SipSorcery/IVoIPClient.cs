﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smartmatic.SmartCad.Telephony.Asterisk
{
    public interface IVoIPClient
    {

        void Call(MediaManager mediaManager, string destination);
        void Cancel();
        void Answer(MediaManager mediaManager);
        void Reject();
        void Redirect(string destination);
        void Hangup();
        void Shutdown();
    }
}
