﻿using AsterNET.Manager;
using AsterNET.Manager.Action;
using AsterNET.Manager.Response;
using System;
using SmartCadCore.Core;
using SmartCadCore.Common;
using System.Threading;
using System.IO;
using Codecs;
using System.Collections;
using NAudio.Wave;

namespace Smartmatic.SmartCad.Telephony.Asterisk
{
    public class TelephonyManagerAsterisk : TelephonyManagerAbstract
    {

    


       
        private MediaManager _mediaManager;
		private WaveIn waveInStream;
        private WaveFileWriter writer;

      

        string currentDisplay = "";
        public string Context = "from-internal";
        public string Priority = "1";
        public string CallerId = "Id";
        public int Timeout = 3000000;
        private string currentDeviceId;
        private bool shuttingDown = false;
        private bool answeringCall = false;
        bool recordCalls = false;
        private INetworkChatCodec codec = new MuLawChatCodec();
        string waitingCallId = null;
        string waitingDeviceId = null;
        string currentCallId = "";
        string callingDeviceId = "";
        string incommingAudioFileName;


        object locker = new object();
        object lockState = new object();
        object lockAnswer = new object();
        int callReceivedSession;
     


        public int waitingRtpPort_ToSend { get; set; }
        public string waitingRtpIp_ToSend { get; set; }
        public string PhoneReportCustomCode { get; set; }

        private SIPClient _sipClient;
        private IVoIPClient _activeClient;
        public bool IsReady { get; set; }

        public ManagerConnection Manager;

        protected string Queue
        {
            get { return configurationProperties[CommonProperties.Queue].ToString(); }
        }
        protected string AESServerIp
        {
            get { return configurationProperties[AsteriskProperties.AES_IP].ToString(); }
        }
        protected string AESServerPort
        {
            get { return configurationProperties[AsteriskProperties.AES_PORT].ToString(); }
        }
        protected string AESLogin
        {
            get { return configurationProperties[AsteriskProperties.AES_LOGIN].ToString(); }
        }
        
        string aesPassword = "";
        protected string AESPassword
        {
            get
            {
                return configurationProperties[AsteriskProperties.AES_PASSWORD].ToString();
            }
        }
        public enum TelephonyCallState
        {
            NoCall = 0,
            Originated,
            Alerting,
            Established,
            Held,
            Conferenced,
            Failed,
            LoggingIn,
            LoggingOut,
            AutoIn,
            DoNotDisturb,
            Initializing,
            DisableAutoIn
        }

        private TelephonyCallState currentCallState;
        public TelephonyCallState CurrentCallState
        {
            get { return currentCallState; }
            set
            {
                if (value == TelephonyCallState.NoCall)
                    currentDisplay = "";
                currentCallState = value;
            }
        }

        


        public TelephonyManagerAsterisk(Hashtable properties) : base (properties)
        {
            try 
            {
                
                this.configurationProperties = properties;
                recordCalls = (bool)properties[CommonProperties.RecordCalls];
            }
            catch (Exception ex) 
            {
                HandleException(new TelephonyException(ResourceLoader.GetString2("TelphonyServerNotConfigured"), ex));
            }
            Init();
            Login(AgentId, AgentPassword, true);
        }

        public override string TestCTIState()
        {
            return "";
        }
        /// <summary>
        /// Initializes all the Manager used Events
        /// </summary>
        public void InitializeCallBacks() 
        {
           
        }


        #region Events



       

      

        
     
        #endregion


    

        /// <summary>
        /// Initializes the Sip client with the extension and extension password configured for the machine in the administration module.
        /// </summary>
        protected override void Init() 
        {
            try
            {
                Console.WriteLine("Initializing...");
                _mediaManager = new MediaManager();
                _mediaManager.OnCallRecordedEvent += OnCallRecordedHandler;
                _sipClient = new SIPClient()
                {
                    SIPUSERNAME = Extension,
                    SIPPASSWORD = ExtensionPassword,
                    SIPSERVER = AESServerIp

                };
                _sipClient.RecordCall = recordCalls;
                string password = CryptoUtil.DecryptWithRijndael(AESPassword).TrimEnd('\0');
                Manager = new ManagerConnection(AESServerIp, int.Parse(AESServerPort), AESLogin, password);
                Manager.Login();
                InitializeCallBacks();


                Console.WriteLine("Started.");
            }
            catch (System.Net.Sockets.SocketException)
            {
                HandleException(new ApplicationException(ResourceLoader.GetString2("UnavailableTelephonyServer")));
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
                throw ex;
            }
        }

        private void OnCallRecordedHandler()
        {
            OnCallRecorded(new CallRecordedEventArgs(PhoneReportCustomCode));
        }

        /// <summary>
        /// Login a channel to a queue, the channel is composed by the extension obtained from the configurationProperties
        /// and the prefix 'PJSIP/' that defines the protocol.
        /// </summary>
        /// <param name="agentId">Id of the agent defined when the user is created</param>
        /// <param name="agentPassword">Password of the agent defined when the user is created</param>
        /// <param name="isReady">Defines if the user is ready or not</param>
        public override void Login(string agentId, string agentPassword, bool isReady) 
        {
            if (!string.IsNullOrEmpty(agentId) && !string.IsNullOrEmpty(agentPassword))
            {
                //while (CurrentCallState == TelephonyCallState.Initializing)
                //{
                //    Thread.Sleep(50);
                //}

                if (CurrentCallState == TelephonyCallState.Failed)
                {
                    Logout();
                    Close();
                    HandleException(new ApplicationException(ResourceLoader.GetString2("ExtensionNotProperlyConfigured")));
                }
                try
                {
                    var channel = "PJSIP/" + configurationProperties[CommonProperties.Extension];
                    SmartLogger.Print("Adding interface: "+ channel+" to Queue: " + Queue);
                    QueueAddAction action = new QueueAddAction(Queue, channel);
                    ManagerResponse response = Manager.SendAction(action);
                    SmartLogger.Print("Response: "+ response.Message);
                    SmartLogger.Print("Unpausing interface: " + channel + " in Queue: " + Queue);
                    QueuePauseAction pauseAction = new QueuePauseAction(channel, Queue, false);
                    ManagerResponse responsePause = Manager.SendAction(pauseAction);
                    SmartLogger.Print("Response: " + responsePause.Message);
                    _sipClient.IncomingCall += SIPCallIncoming;
                    _sipClient.OutgoingCallAnswered += SIPCallAnswered;
                    _sipClient.CallEnded += ResetToCallStartState;
                    _sipClient.StatusMessage += StatusChange;
                    _sipClient.CallCancelled += CancelCall;
                    _sipClient.IncomingCallAnswered += IncomingCAllAnswered;
                    _sipClient.Register();
                    IsReady = isReady;



                    while (CurrentCallState == TelephonyCallState.LoggingIn)
                    {
                        Thread.Sleep(10);
                    }
                    if (CurrentCallState == TelephonyCallState.Failed) throw new Exception();
                }
                catch (Exception ex) 
                {
                    Logout();
                    Close();
                    HandleException(new TelephonyException(ResourceLoader.GetString2("UnableLoginInTerminal"), ex));
                }
                
            
            }           
        }

        /// <summary>
        /// Event triggered when an incoming call is answered, stops ringing and activates UI to register incident
        /// </summary>
        private void IncomingCAllAnswered()
        {
            waitingCallId = null;
            CurrentCallState = TelephonyCallState.Established;
            answeringCall = false;
            Console.WriteLine("Call Answered");
            OnCallMade(new CallMadeEventArgs());
            OnCallEstablished(new CallEstablishedEventArgs("", AgentId, Queue, TimeSpan.FromTicks(DateTime.Today.Ticks)));
        }

        /// <summary>
        /// When a call is cancelled, stops ringing
        /// </summary>
        private void CancelCall()
        {
            waitingCallId = null;
            waitingDeviceId = null;
            waitingRtpIp_ToSend = null;
            waitingRtpPort_ToSend = -1;

            OnCallAbandoned(new CallAbandonedEventArgs("", AgentId, Queue, TimeSpan.FromTicks(DateTime.Today.Ticks)));
            answeringCall = false;
            DropCall();
            Console.WriteLine("Call Cancelled");
            CurrentCallState = TelephonyCallState.NoCall;
            //IsReady = true;
            _sipClient.UnlockQueueCall();
        }

        /// <summary>
        /// Informational event, displays in console a change of status or messages incoming from the PBX
        /// </summary>
        /// <param name="obj"></param>
        private void StatusChange(string obj)
        {
            Console.WriteLine("Status: " + obj);
        }

        /// <summary>
        /// Event triggered when a call is finished, restores all values to initial values to accept or make other calls
        /// </summary>
        private void ResetToCallStartState()
        {
            currentCallState = TelephonyCallState.NoCall;
            answeringCall = false;
         
            OnCallReleased(new CallReleasedEventArgs("", AgentId, Queue, TimeSpan.FromTicks(DateTime.Today.Ticks)));
            _sipClient.UnlockQueueCall();
            _activeClient = null;
            //waveInStream.Dispose();
            
        }

        /// <summary>
        /// Event triggered when an outgoing call is answered.
        /// </summary>
        private void SIPCallAnswered()
        {
            waitingCallId = null;
            CurrentCallState = TelephonyCallState.Established;
            answeringCall = false;
            Console.WriteLine("Call Answered");
            OnCallMade(new CallMadeEventArgs());
            OnCallEstablished(new CallEstablishedEventArgs("", AgentId, Queue, TimeSpan.FromTicks(DateTime.Today.Ticks)));

        }

        /// <summary>
        /// Event triggered when an incoming call is received, starts ringing if the user is available.
        /// </summary>
        private void SIPCallIncoming(string callingDeviceId)
        {
            if (IsReady && currentCallState == TelephonyCallState.NoCall)
            {
                Console.WriteLine("Incoming Call... ");
                _activeClient = _sipClient;
                currentCallState = TelephonyCallState.Alerting;
                OnCallRinging(new CallRingingEventArgs(callingDeviceId, AgentId, "", TimeSpan.FromTicks(DateTime.Now.Ticks)));
            }
        }


        /// <summary>
        /// Logout a channel of the queue, the channel is formed by the extension obtained from de configurationProperties
        /// and the prefix 'PJSIP/' that defines the protocol.
        /// </summary>
        public override void Logout()
        {
            var channel = "PJSIP/" + configurationProperties[CommonProperties.Extension];
            SmartLogger.Print("Removing interface: " + channel + " from Queue: " + Queue);
            QueueRemoveAction action = new QueueRemoveAction(Queue, channel);
            ManagerResponse responsePause = Manager.SendAction(action);
            SmartLogger.Print("Response: " + responsePause.Message);
        }

        public override void Hold()
        {
            try 
            {
                if (!string.IsNullOrEmpty(currentCallId)) 
                {
                    
                    CurrentCallState = TelephonyCallState.Held;
                }
                if (recordCalls)
                {
                    waveInStream.StopRecording();
                    waveInStream.Dispose();
                    waveInStream = null;
                    writer.Close();
                    writer = null;
                }
            }
            catch (Exception ex) 
            {
            
            }
        }
        private void EnableAutoIn()
        {
            try
            {
                //This feature should be changed in version 6.1
                //MakeCall("*20");
            }
            catch (Exception e)
            {
            }
        }
        private void DisableAutoIn()
        {
            try
            {
                //This feature should be changed in version 6.1
                //MakeCall("*21");
                //StopMediaThreads();
            }
            catch (Exception e)
            {
            }
        }
        public override void SetReadyStatus(bool ready)
        {
            if (ready)
            {
                
                if (waitingCallId != null)
                {
                    Console.WriteLine("Se puso en RINGING");
                    CurrentCallState = TelephonyCallState.Alerting;
                    currentCallId = waitingCallId;
                    callingDeviceId = waitingDeviceId;
                    OnCallRinging(new CallRingingEventArgs(callingDeviceId, AgentId, "",
                        TimeSpan.FromTicks(DateTime.Now.Ticks)));

                    waitingCallId = null;
                }
                else
                {
                    var channel = "PJSIP/" + configurationProperties[CommonProperties.Extension];
                    //_sipClient.Register();
                    //QueueAddAction action = new QueueAddAction(Queue, channel);
                    //ManagerResponse response = Manager.SendAction(action);
                    SmartLogger.Print("Unpausing interface: " + channel + " in Queue: " + Queue);
                    QueuePauseAction pauseAction = new QueuePauseAction(channel, Queue, false);
                    ManagerResponse responsePause = Manager.SendAction(pauseAction);
                    SmartLogger.Print("Response: " + responsePause.Message);
                    CurrentCallState = TelephonyCallState.NoCall;
                    EnableAutoIn();
                    IsReady = true;
                    Console.WriteLine("Se puso en Ready");
                }
            }
            else
            {
                //if (waitingCallId == null || IsReady)
                //{
                //    CurrentCallState = TelephonyCallState.DisableAutoIn;
                //    DisableAutoIn();
                //}
                    IsReady = false;
                    //_sipClient.Unregister();
                    var channel = "PJSIP/" + configurationProperties[CommonProperties.Extension];
                //QueueRemoveAction action = new QueueRemoveAction(Queue, channel);
                //ManagerResponse response = Manager.SendAction(action);
                SmartLogger.Print("Pausing interface: " + channel + " in Queue: " + Queue);
                QueuePauseAction pauseAction = new QueuePauseAction(channel, Queue, true);

                ManagerResponse responsePause = Manager.SendAction(pauseAction);
                SmartLogger.Print("Response: " + responsePause.Message);

                Console.WriteLine("Se puso en NO Ready");

            }
        }

        public override bool GetReadyStatus()
        {
            return IsReady;
        }

        public override void Answer()
        {
            try
            {
                if (!answeringCall) 
                {
                    lock (lockAnswer)
                    {

                        currentCallId = waitingCallId;
                        callingDeviceId = waitingDeviceId;
                        waitingCallId = null;
                        callingDeviceId = null;
                    }
                    answeringCall = true;
                    Console.WriteLine("Answering call...");
                    _activeClient.Answer(_mediaManager);
                    //var channel = "PJSIP/" + configurationProperties[CommonProperties.Extension];
                    //QueuePauseAction pauseAction = new QueuePauseAction(channel, Queue, true);
                    //ManagerResponse response = Manager.SendAction(pauseAction);
                    Console.WriteLine("Answered call...");
                

                }              
            }
            catch (Exception ex)
            {
                answeringCall = false;
                SmartLogger.Print(ex);
            }
        }


        public void DropCall() 
        {
            try 
            {
                Console.WriteLine("Dropping Call");
                if(_activeClient != null)
                {
                    _activeClient.Hangup();
                   
                }
                else
                {
                    _mediaManager.EndCall();
                }
            }
            catch (Exception ex) 
            {
            
            }
        }

        public override void Release()
        {
            DropCall();
            Console.WriteLine("Entrando en release");
        }

        public override void Transfer()
        {
            throw new NotImplementedException();
        }

        private void DeleteAudioFiles()
        {
            try
            {
                lock (locker)
                {
                    Console.WriteLine("Deleting Audio Files " + incommingAudioFileName);

                    File.Delete(incommingAudioFileName + ".wav");
                    File.Delete(incommingAudioFileName + "_mic.wav");
                }
            }
            catch { /*Keep going*/}
        }
       
      

        



        private void MakeCall(string number)
        {
            try
            {
                
                CallDialingEventArgs args = new CallDialingEventArgs(Extension, number, "", number, new TimeSpan());
                OnCallDialing(args);
                _sipClient.OutgoingCallAnswered += SIPCallAnswered;
                _sipClient.CallEnded += ResetToCallStartState;
                _activeClient = _sipClient;
                _activeClient.Call(_mediaManager, number);
            }
            catch (Exception ex)
            {

                throw ex;
            }


        }

        public override void Call(string number)
        {
            try
            {
                MakeCall(number);   
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Puts the current call in parking state, waiting for a dispatcher to re-take it.
        /// </summary>
        /// <param name="extensionParking">Extension number where call will be parked(queue number)</param>
        /// <param name="parkId">call id used to park and unpark the call</param>
        public override void ParkCall(string extensionParking, string parkId)
        {
            throw new NotImplementedException();
        }

        public override void Conference()
        {
            throw new NotImplementedException();
        }

        public override void Close()
        {
            try
            {
                Manager.Logoff();
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        public override void RecoverConnection()
        {
            throw new NotImplementedException();
        }

        public override void SetPhoneReport(string customCode)
        {
            PhoneReportCustomCode = customCode;
        }

        private void HandleException(Exception ex)
        {
            SmartLogger.Print(ex);
            currentCallState = TelephonyCallState.Failed;
            throw ApplicationUtil.NewFaultException(ex);
        }

        private void HandleExceptionEvent(Exception ex)
        {
            SmartLogger.Print(ex);
            currentCallState = TelephonyCallState.Failed;
            OnTelephonyError(new TelephonyErrorEventArgs(ex.Message, AgentId, Extension, Queue, DateTime.Now.TimeOfDay));
        }
    }
}
