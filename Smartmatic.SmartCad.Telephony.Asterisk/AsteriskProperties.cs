﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Smartmatic.SmartCad.Telephony.Asterisk
{
    public static class AsteriskProperties
    {
        public const string AES_IP = "AES_IP";
        public const string AES_PORT = "AES_PORT";
        public const string AES_LOGIN = "AES_LOGIN";
        public const string AES_PASSWORD = "AES_PASSWORD";
        public const string CM_IP = "CM_IP";
        public const string CM_NAME = "CM_NAME";
        public const string SECURE_SOCKET = "SECURE_SOCKET";
        public const string MEDIA_GATEWAY = "MEDIA_GATEWAY";
        public const string DEVICE_PORT = "DEVICE_PORT";
        public const string QUEUE = "QUEUE";
    }
}
