﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading;
using System.Net;
using SmartCadCore.Core;
using SmartCadGuiCommon.Util;
using SmartCadCore.Common;
using SmartCadCore.ClientData;
using SmartCadGuiCommon;
using System.Globalization;
using System.ServiceModel;
using Smartmatic.SmartCad.Gui;
using Smartmatic.SmartCad.Service;
using SmartCadFirstLevel.Gui;
using System.IO;

namespace SmartCadLPR
{
    static class Program
    {
        private static string ApplicationName = "Lpr";

        [STAThread]
        static void Main()
        {
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.ThreadException += new ThreadExceptionEventHandler(ApplicationGuiUtil.Application_ThreadException);
                ApplicationUtil.ActivateProcess(Process.GetCurrentProcess());

                SmartCadConfiguration.Load();
                ResourceLoader.UpdatedLanguage(new CultureInfo(SmartCadConfiguration.SmartCadSection.CultureElement.CultureLanguageElement.Name
                    + "-" + SmartCadConfiguration.SmartCadSection.CultureElement.CultureCountryElement.Name));

                object result = ServerServiceClient.GetServerService();

                if (result == null)
                {
                    MessageForm.Show(ResourceLoader.GetString2("UnableConnectToServer"), MessageFormType.Error);
                    return;
                }

                ConfigurationClientData cfg = ServerServiceClient.GetInstance().GetConfiguration();

                if (!ApplicationUtil.VerifyLanguage(cfg))
                {
                    byte[] dll = ServerServiceClient.GetInstance().CheckLanguages(cfg.Language);
                    DirectoryInfo directory = Directory.CreateDirectory(SmartCadConfiguration.LanguageDirectoryClient + "\\" + cfg.Language);
                    using (FileStream fileDll = new FileStream(directory.FullName + "\\Smartmatic.SmartCad.Resources.resources.dll", FileMode.Create, FileAccess.Write))
                    {
                        fileDll.Write(dll, 0, dll.Length);
                    }
                }

                ResourceLoader.UpdatedLanguage(new CultureInfo(cfg.Language));
                ServerServiceClient.GetInstance().SetApplication(UserApplicationClientData.UserApplications.Cctv.ToString());
                SmartCadLoadInitialData.LoadInitialClientData();

                // System.Threading.Timer checkServer = ApplicationGuiUtil.StartCheckServer();
                ServerServiceClient.GetInstance().ActivateApplication(SmartCadApplications.SmartCadLPR, ApplicationUtil.GetMACAddress());
                ApplicationGuiUtil.CheckErrorDetailButton();

                bool success = false;

                do
                {


                    LoginForm login = new LoginForm(UserApplicationClientData.Lpr);
                    LprFrontClient lprClientForm = new LprFrontClient();




                    if (login.ShowDialog() == DialogResult.OK)
                    {
                        SplashForm.ShowSplash();
                        NetworkCredential networkCredential = login.NetworkCredential;

                        ServerServiceClient serverServiceClient = login.ServerService;

                        success = true;

                        lprClientForm.SetPassword(login.Password);
                        lprClientForm.ServerServiceClient = serverServiceClient;
                        lprClientForm.NetworkCredential = networkCredential;
                        configuration = ServerServiceClient.GetInstance().GetConfiguration();
                        lprClientForm.ConfigurationClient = configuration;
                        lprClientForm.LoadInitialData();
                        SplashForm.CloseSplash();
                        Application.Run(lprClientForm);
                    }
                    else
                    {
                        ServerServiceClient.GetInstance().CloseSession();
                        success = true;
                    }
                }
                while (!success);
            }
            catch (EndpointNotFoundException ex)
            {
                SplashForm.CloseSplash();

                MessageForm.Show(ResourceLoader.GetString2("UnableConnectToServer"), ex);
            }
            catch (Exception ex)
            {
                SplashForm.CloseSplash();

                MessageForm.Show(ResourceLoader.GetString2("GenericErrorApplication"), ex);
            }
        }

        private static ConfigurationClientData configuration;
    }
}
