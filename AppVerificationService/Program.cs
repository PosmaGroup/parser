﻿using AppVerificationService.Service;
using SmartCadCore.Common;
using SmartCadCore.Core;
using Smartmatic.SmartCad.Service;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.ServiceProcess;
using System.Text;

namespace AppVerificationService
{
    class Program 
    {
        private static object ServerService = null;

        static void Main(string[] args)
        {
            if (IsFromServiceManager())
            {
                StartService();
            }
            else
            {
               
                StartProgram();
            }           
        }

        private static void StartService()
        {
            ServiceBase[] servicesToRun = new ServiceBase[] { new AppVerification() };

            ServiceBase.Run(servicesToRun);
            Process.GetCurrentProcess().Kill();
        }

        private static bool IsFromServiceManager()
        {
            Uri uri = new Uri(Assembly.GetExecutingAssembly().EscapedCodeBase);
            FileInfo fi = new FileInfo(uri.LocalPath);

            return fi.DirectoryName != Environment.CurrentDirectory;
        }

        public static void StartProgram() 
        {
            SmartCadConfiguration.Load();
            SmartLogger.Print("Initializing service");


            try 
            {
                while (true)
                {
                    if (GetServerService() != null)
                    {
                        SmartLogger.Print("Connecting to server... ");
                        var applications = ServerServiceClient.GetInstance().GetInstalledApplicationsForWorkStation(ApplicationUtil.GetMACAddress());

                        foreach (var application in applications)
                        {
                            SmartLogger.Print(application.ToString());
                            string applicationName = "SmartCad " + application.ToString() + " " + SmartCadConfiguration.Version;
                            

                            if (!ApplicationUtil.IsInstalled(applicationName))
                            {
                                ServerServiceClient.GetInstance().RemoveInstalledApplication(application.ToString(), ApplicationUtil.GetMACAddress());
                                SmartLogger.Print("Remove " + applicationName);
                                break;
                            }
                        }
                    }
                    System.Threading.Thread.Sleep(60000);

                }
            }
            catch (Exception ex) 
            {
                SmartLogger.Print(ex);
            }
            
        }


        public static object GetServerService()
        {
            if (ServerService == null)
                ServerService = ServerServiceClient.GetServerService();          
            return ServerService;
            
        }
    }
}
