﻿        var credentials = "AoLvJrAnVFoaeE4ZaCL-Z97r7ZqEx9nFnPEwA-6wHSkomU0lWQn3jhklfW9zkfEL";
        var map = null;
        var initLat="10.48168";
        var initLon="-67368";
        var initZoom = 16;
        var markers = [];
        function initialize(lat, lon, zoom, messageError)
        {   
            try {
                map = new Microsoft.Maps.Map(document.getElementById("map"), 
                   {credentials: credentials,
                    center: new Microsoft.Maps.Location(lat, lon),
                    mapTypeId: Microsoft.Maps.MapTypeId.road,
                    zoom: zoom
                });
            }
            catch (ex)
            {
                document.write('<div id="content">'+
			        '<h2>' + messageError + '</h2>'+
		        '</div>');
            };
        }
        
        function InsertPoint(id, title, image, lat, lon)
        {
            // Add a pin to the map
            var pin = new Microsoft.Maps.Pushpin(new Microsoft.Maps.Location(lat, lon), 
            {
                text: title, 
                width: 50, 
                height: 50, 
                draggable: true
            });  
            
            if(image!= '') pin.setOptions({icon:image});
                         
            // Add the ping to the map
            map.entities.push(polygon);
            markers.push[id, map.entities.getLength()-1];
        }
        
        function UpdatePoint(id, title, image, lat, lon)
        {
            for (var n = 0, marker; marker = markers[n]; n++) 
	        {
	            if(marker[0] == id)
	            {
	                 var point = map.entities.get(marker[1]);
	                 point.setOptions({ text: title });
                     point.setLocation(coordinates);
                     
                     if(image!= '') pin.setOptions({icon:image});
                     break;
                }
            }       
        }
        
        function InsertPolygon(id, array, color)
        {
            var points = eval(array);
            var coordinates = new Array();
            for (var i = 0, point; point = points[i]; i++) 
            {
                coordinates.push(new Microsoft.Maps.Location(point[0],point[1]));
            }
            var color = eval(rgbColor); //RGB & opacity
            var polygoncolor = new Microsoft.Maps.Color(color[0],color[1],color[2],color[3]);
            
            var polygon = new Microsoft.Maps.Polygon(coordinates,{
                fillColor: polygoncolor, 
                strokeColor: polygoncolor
            });
            
            // Add the polygon to the map
            map.entities.push(polygon);
            markers.push[id,map.entities.getLength()-1];
        }
        
        function UpdatePolygon(id, array, color)
        {
            for (var n = 0, marker; marker = markers[n]; n++) 
	        {
	            if(marker[0] == id)
	            {
	                 var polygon = map.entities.get(marker[1]);
	                 polygon.setOptions({ 
	                    fillColor: polygoncolor, 
                        strokeColor: polygoncolor});
                     polygon.setLocations(coordinates);
                     break;
                }
            }            
        }
        
        function InsertPolyline(id, array, color)
        {
            var points = eval(array);
            var coordinates = new Array();
            for (var i = 0, point; point = points[i]; i++) 
            {
                coordinates.push(new Microsoft.Maps.Location(point[0],point[1]));
            }
            var color = eval(rgbColor); //RGB & opacity
            var polylinecolor = new Microsoft.Maps.Color(color[0],color[1],color[2],color[3]);
            
            var polyline = new Microsoft.Maps.Polyline(coordinates,{
                strokeColor: polylinecolor,
                strokeThickness: 2
            });
            
            // Add the polygon to the map
            map.entities.push(polyline);
            markers.push[id,map.entities.getLength()-1];
        }
        
        function UpdatePolyline(id, array, color)
        {
            for (var n = 0, marker; marker = markers[n]; n++) 
	        {
	            if(marker[0] == id)
	            {
	                 var polygon = map.entities.get(marker[1]);
	                 polygon.setOptions({ strokeColor: polygoncolor });
                     polygon.setLocations(coordinates);
                     break;
                }
            }            
        }
        
        function DeleteObject(id)
        {
            try{
                 if(markers)
                 {
	                for (var n = 0, marker; marker = markers[n]; n++) 
	                {
	                    if(marker[0] == id)
	                    {
                             map.entities.removeAt(marker[1]);
                             markers.splice(n,1);
                        }
                    }
	            }
	        }
	        catch (e){ alert('errorr'); }
        }
        
        function EnableObject(id, status)
        {
            for (var n = 0, marker; marker = markers[n]; n++) 
	        {
	            if(marker[0] == id)
	            {
	                 var mark = map.entities.get(marker[1]);
	                 mark.setOptions({ Visible: status });
                     break;
                }
            }           
        }

        function CenterMap(lat, lng, zoom)
        {
	        map.setView({ center: new Microsoft.Maps.Location(lat,lng), zoom: zoom });
        }

        function CenterMap(lat, lng)
        {
	        map.setView({ center: new Microsoft.Maps.Location(lat,lng) });
        }
        
        
        function SearchAddress(address, tableName)
        {
            map.getCredentials(function()
            {
                var geocodeRequest = "http://dev.virtualearth.net/REST/v1/Locations/" 
                + address
                + "?output=json&jsonp=GeocodeCallback&key=" 
                + credentials;
            });
        }

        function GeocodeCallback(result) 
        {   
            if (result &&
                   result.resourceSets &&
                   result.resourceSets.length > 0 &&
                   result.resourceSets[0].resources &&
                   result.resourceSets[0].resources.length > 0) 
            {
               // Set the map view using the returned bounding box
               var bbox = result.resourceSets[0].resources[0].bbox;
               var viewBoundaries = Microsoft.Maps.LocationRect.fromLocations(new Microsoft.Maps.Location(bbox[0], bbox[1]), new Microsoft.Maps.Location(bbox[2], bbox[3]));
               map.setView({ bounds: viewBoundaries});

               // Add a pushpin at the found location
               var location = new Microsoft.Maps.Location(result.resourceSets[0].resources[0].point.coordinates[0], result.resourceSets[0].resources[0].point.coordinates[1]);
               var pushpin = new Microsoft.Maps.Pushpin(location);
               map.entities.push(pushpin);
            }

        }