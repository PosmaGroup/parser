SELECT incident.CUSTOM_CODE AS 'custom-code', 
	(
														
			SELECT cctvReport.PARENT_CODE AS 'parent-code', 	
					incident.START_DATE AS 'incident-Time', 		
			(
			SELECT rb.CUSTOM_CODE
			) AS 'custom-code',
				(
				SELECT op.FIRSTNAME + ' ' + op.LASTNAME FROM OPERATOR op WHERE op.CODE = rb.OPERATOR_CODE
				) AS 'attended-by',
					(
					
				SELECT camera.NAME AS 'CameraName', 
					structType.NAME as 'StructType', struct.NAME as 'StructName', cctvZone.NAME as 'cctvZoneName', device.NAME as 'deviceName' 				    												
					FROM ALARM_REPORT cctvReportForCamera, ALARM_SENSOR_TELEMETRY alarm, STRUCT struct, STRUCT_TYPE structType, 
					SENSOR_TELEMETRY camera, VIDEO_DEVICE video, DEVICE device, CCTV_ZONE cctvZone
					WHERE cctvReportForCamera.PARENT_CODE = cctvReport.PARENT_CODE and 
						  video.PARENT_CODE = camera.DATALOGGER_CODE  and
						  video.PARENT_CODE = device.CODE and
						  cctvReportForCamera.ALARM_SENSOR_TELEMETRY_CODE = alarm.CODE and
						  alarm.SENSOR_TELEMETRY_CODE = camera.CODE and 
						  struct.STRUCT_TYPE = structType.CODE and  
						  struct.CODE = video.STRUCT_CODE and 
						  struct.ZONE_CODE = cctvZone.CODE
					FOR XML RAW('cctvAddres'), TYPE
					),
					 
					(
					SELECT struct.STATE AS 'state',
					struct.TOWN AS 'town',
					struct.ZONE AS 'zone',
					struct.CCTV_STREET AS 'street'
					FROM ALARM_REPORT address, ALARM_SENSOR_TELEMETRY alarm, SENSOR_TELEMETRY sensor, DATA_LOGGER datalogger,
					VIDEO_DEVICE videoDevice, STRUCT struct
					WHERE address.PARENT_CODE = cctvReport.PARENT_CODE and
					address.ALARM_SENSOR_TELEMETRY_CODE = alarm.CODE and
					alarm.SENSOR_TELEMETRY_CODE = sensor.CODE and
					sensor.DATALOGGER_CODE = datalogger.PARENT_CODE and
					datalogger.PARENT_CODE = videoDevice.PARENT_CODE and
					videoDevice.STRUCT_CODE = struct.CODE
					FOR XML RAW('address'), TYPE
					),
					(
					SELECT incidentType.FRIENDLY_NAME AS 'friendly-name',
					incidentType.CUSTOM_CODE AS 'custom-code'
					FROM INCIDENT_TYPE incidentType
					INNER JOIN REPORT_BASE_INCIDENT_TYPE rbit ON incidentType.CODE = rbit.INCIDENT_TYPE_CODE
					WHERE rbit.REPORT_BASE_CODE = rb.CODE
					FOR XML RAW('incident-type'), TYPE
					) AS 'incident-types',										
					(
					SELECT question.[TEXT] AS 'q',
					(
						SELECT answer.TEXT_ANSWER as 'text'
						FROM POSSIBLE_ANSWER_ANSWER answer
						WHERE answer.ALARM_REPORT_ANSWER_CODE = pra.CODE
						ORDER BY INCIDENT_TYPE_POSSIBLE_ANSWER_CODE
						FOR XML RAW('a'), TYPE
					)
					FROM QUESTION question
					INNER JOIN ALARM_REPORT_ANSWER pra ON pra.ALARM_REPORT_ANSWER_QUESTION_CODE = question.CODE
					WHERE cctvReport.PARENT_CODE = pra.ALARM_REPORT_CODE
					FOR XML RAW('question'), TYPE, ELEMENTS				
					) AS 'question-set',					
					
				(
				SELECT incidentNotification.CODE AS 'code',
				(
				SELECT dt.[NAME] FROM DEPARTMENT_TYPE dt WHERE dt.CODE = incidentNotification.DEPARTMENT_TYPE_CODE
				) AS 'department-type-name',
				(
				SELECT inp.CUSTOM_CODE FROM INCIDENT_NOTIFICATION_PRIORITY inp WHERE inp.CODE = incidentNotification.INCIDENT_NOTIFICATION_PRIORITY_CODE
				) AS 'priority-custom-code',
				CREATION_DATE as 'creation-date',
				START_DATE as 'start-date'
				FROM INCIDENT_NOTIFICATION incidentNotification 
				WHERE incidentNotification.REPORT_BASE_CODE = cctvReport.PARENT_CODE
				FOR XML RAW('notification'), TYPE
			) AS 'notifications'
		FROM ALARM_REPORT cctvReport
		INNER JOIN REPORT_BASE rb ON rb.CODE = cctvReport.PARENT_CODE
		WHERE rb.INCIDENT_CODE = incident.CODE for xml RAW('alarm-report'), type
	) AS 'alarm-reports',
	(
		SELECT incidentNotification.CODE AS 'code',
			incidentNotification.CREATION_DATE as 'creation-date',
			incidentNotification.start_date as 'start-date',
			(
				SELECT TOP 1 status.CUSTOM_CODE as custom_code, status.FRIENDLY_NAME as [friendly-name], insh.START_DATE as start, insh.DETAIL as status_detail FROM incident_notification_status_history insh
				INNER JOIN incident_notification_status status on status.CODE = insh.INCIDENT_NOTIFICATION_STATUS_CODE, operator ope
				WHERE insh.incident_notification_code = incidentNotification.code AND
					insh.user_account_code = ope.code AND
					status.CUSTOM_CODE NOT IN ('MANUAL_SUPERVISOR', 'AUTOMATIC_SUPERVISOR')
				ORDER BY insh.START_DATE DESC FOR XML RAW('real-status'), TYPE
			),
			(SELECT departmentTypePriority.[NAME]) AS 'department-type-name',
			(SELECT priority.CUSTOM_CODE) AS 'priority-custom-code',
			(SELECT 1 FROM support_request_report srr WHERE incidentNotification.report_base_code = srr.parent_code) AS 'support-request',
			(
				SELECT status.[FRIENDLY_NAME] AS 'status',
					(SELECT insh.start_date) AS 'start-date',
					(SELECT insh.end_date) AS 'end-date',
					(SELECT ope.firstname + ' ' + ope.lastname) AS 'complete-name',
					(SELECT ope.person_id) AS 'person-id'
				FROM incident_notification_status_history insh, incident_notification_status status, operator ope
				WHERE insh.incident_notification_code = incidentNotification.code AND
					insh.incident_notification_status_code = status.code AND
					insh.user_account_code = ope.code ORDER BY insh.start_date ASC
				FOR XML RAW('status'), TYPE
			) AS 'status-set',
			(
				SELECT COUNT(*) AS 'total-units',
					(SELECT dispatch_order.custom_code AS 'unit',
						(SELECT ut.[NAME] FROM UNIT_TYPE ut WHERE ut.CODE = dispatch_order.UNIT_TYPE_CODE) AS 'unit-type',
						(SELECT do.start_date) AS 'start-date',
                        (SELECT do.arrival_date) AS 'arrival-date',
                        (SELECT do.end_date) AS 'end-date',
                        (
							SELECT
								(SELECT officer.badge) AS 'badge',
								(SELECT officer.firstName + ' ' + officer.lastName) AS 'complete-name'
							FROM officer, unit_officer_assign_history uoah
							WHERE uoah.unit_code = do.unit_code AND
								uoah.officer_code = officer.code AND
								do.start_date BETWEEN uoah.start_date AND uoah.end_date
							FOR XML RAW('officer'), TYPE
						) AS 'officers',
                        (
							SELECT note.creation_date AS 'creation-date',
								(SELECT note.complete_user_name) AS 'operator',
								(SELECT note.detail) AS 'detail'
							FROM unit_note note
							WHERE note.dispatch_order_code = do.code
							FOR XML RAW('note'), TYPE
						) AS 'notes',
						(
							SELECT cancelNote.creation_date AS 'creation-date',
								(SELECT cancelNote.complete_user_name) AS 'operator',
								(SELECT cancelNote.detail) AS 'detail'
							FROM dispatch_order_note cancelNote
							INNER JOIN dispatch_order_note_type dont ON dont.code = cancelNote.dispatch_order_note_type
							WHERE cancelNote.dispatch_order_code = do.code AND
								dont.custom_code = 'CANCEL_NOTE'
							FOR XML RAW('cancel-note'), TYPE
						),
						
						(
							SELECT offic.FIRSTNAME + ' ' + offic.LASTNAME  as 'officerFullName', 
							       endingReport.BY_UNIT as 'byUnit',
								( 
									SELECT question.QUESTION_TEXT as 'text',
									(
										SELECT answer.ANSWER_HEADER_TEXT as 'header', ANSWER_TEXT as 'text'
										FROM ENDING_REPORT_ANSWER answer
										WHERE answer.DISPATCH_ORDER_CODE = endingReport.code AND answer.QUESTION_TEXT = question.QUESTION_TEXT
										FOR XML RAW('answer'), TYPE									
									)
									FROM ENDING_REPORT_ANSWER question
									WHERE question.DISPATCH_ORDER_CODE = endingReport.code
									GROUP BY question.QUESTION_TEXT
						     		FOR XML RAW('question'), TYPE
								) AS 'question-set'
							FROM dispatch_order endingReport
							LEFT OUTER JOIN OFFICER offic ON offic.CODE = endingReport.OFFICER_CODE
							WHERE endingReport.code = do.code 								
							FOR XML RAW('ending-report'), TYPE
						)																													
					 FROM dispatch_order do, unit dispatch_order
					 WHERE do.incident_notification_code = incidentNotification.code AND
						dispatch_order.code = do.unit_code
					 FOR XML RAW('dispatch-order'), TYPE
					)
			FROM dispatch_order units
			WHERE units.incident_notification_code = incidentNotification.code
			GROUP BY units.incident_notification_code
			FOR XML RAW('units'), TYPE
		),
		(
			SELECT op.FIRSTNAME + ' ' + op.LASTNAME FROM OPERATOR op 
				WHERE op.CODE = 
				(
					SELECT TOP 1 history.USER_ACCOUNT_CODE 
					FROM INCIDENT_NOTIFICATION_STATUS_HISTORY history
					WHERE history.START_DATE = (SELECT MIN(history2.START_DATE) FROM INCIDENT_NOTIFICATION_STATUS_HISTORY history2
												WHERE history2.INCIDENT_NOTIFICATION_STATUS_CODE IN 
													(SELECT CODE FROM INCIDENT_NOTIFICATION_STATUS 
														WHERE CUSTOM_CODE = 'ASSIGNED'
													) 
													AND history2.INCIDENT_NOTIFICATION_CODE = incidentNotification.CODE
												) AND
						history.INCIDENT_NOTIFICATION_CODE = incidentNotification.CODE
				)
		) AS 'firstly-assigned',
		(
			SELECT op.FIRSTNAME + ' ' + op.LASTNAME FROM OPERATOR op 
				WHERE op.CODE = 
				(
					SELECT TOP 1 history.USER_ACCOUNT_CODE 
					FROM INCIDENT_NOTIFICATION_STATUS_HISTORY history
					WHERE history.START_DATE = (SELECT MAX(history2.START_DATE) FROM INCIDENT_NOTIFICATION_STATUS_HISTORY history2
												WHERE history2.INCIDENT_NOTIFICATION_STATUS_CODE IN 
													(SELECT CODE FROM INCIDENT_NOTIFICATION_STATUS 
														WHERE CUSTOM_CODE IN ('CLOSED', 'CANCELLED')
													)
													AND history2.INCIDENT_NOTIFICATION_CODE = incidentNotification.CODE
												) AND
						history.INCIDENT_NOTIFICATION_CODE = incidentNotification.CODE
				)
		) AS 'notification-closed-by'
		FROM REPORT_BASE rb2, INCIDENT_NOTIFICATION incidentNotification, DEPARTMENT_TYPE departmentTypePriority, INCIDENT_NOTIFICATION_PRIORITY priority
		WHERE rb2.INCIDENT_CODE = incident.code AND
			incidentNotification.REPORT_BASE_CODE = rb2.CODE AND
			departmentTypePriority.CODE = incidentNotification.DEPARTMENT_TYPE_CODE AND
			priority.CODE = incidentNotification.INCIDENT_NOTIFICATION_PRIORITY_CODE
		FOR XML RAW('incident-notification'), TYPE
	) AS 'incident-notifications',
	(
		SELECT incidentNote.CODE AS 'code',
			incidentNote.CREATION_DATE AS 'creation-date',
			incidentNote.COMPLETE_USER_NAME AS 'user-login',
			incidentNote.DETAIL AS 'detail'
		FROM INCIDENT_NOTE incidentNote
		WHERE incidentNote.INCIDENT_CODE = incident.CODE 
		FOR XML RAW('incident-note'), TYPE
	) AS 'incident-notes',
	(
		SELECT supportRequest.PARENT_CODE AS 'parent-code', 
			supportRequest.CREATION_DATE AS 'creation-date',
			supportRequest.COMMENT AS 'details', 					
			(SELECT op.FIRSTNAME + ' ' + op.LASTNAME FROM OPERATOR op WHERE op.CODE = rb.OPERATOR_CODE) AS 'attended-by',
			(
				SELECT unitPetitioner.CUSTOM_CODE AS 'custom-code',
					(SELECT ut.[NAME] FROM UNIT_TYPE ut WHERE ut.CODE = unitPetitioner.UNIT_TYPE_CODE) AS 'unit-type',
					(SELECT dt.[NAME] FROM DEPARTMENT_TYPE dt INNER JOIN DEPARTMENT_ZONE dz ON dz.DEPARTMENT_TYPE_CODE = dt.Code INNER JOIN DEPARTMENT_STATION ds ON ds.DEPARTMENT_ZONE_CODE = dz.CODE WHERE ds.CODE = unitPetitioner.DEPARTMENT_STATION_CODE) AS 'department'
				FROM UNIT unitPetitioner 
				WHERE unitPetitioner.CODE = supportRequest.UNIT_CODE 
				FOR XML RAW('unit-petitioneer'), TYPE
			),
			(
				SELECT incidentNotification.CODE,
					(
						SELECT departmentType.[NAME] 
						FROM DEPARTMENT_TYPE departmentType 
						WHERE departmentType.CODE = incidentNotification.DEPARTMENT_TYPE_CODE
					) AS 'department-type-name',
					(
						SELECT priority.CUSTOM_CODE 
						FROM INCIDENT_NOTIFICATION_PRIORITY priority 
						WHERE priority.CODE = incidentNotification.INCIDENT_NOTIFICATION_PRIORITY_CODE
					) AS 'priority'
				FROM INCIDENT_NOTIFICATION incidentNotification 
				WHERE incidentNotification.REPORT_BASE_CODE = supportRequest.PARENT_CODE 
				FOR XML RAW('incident-notification'), TYPE
			) AS 'incident-notifications'
		FROM SUPPORT_REQUEST_REPORT supportRequest
		INNER JOIN REPORT_BASE rb ON rb.CODE = supportRequest.PARENT_CODE
		WHERE rb.INCIDENT_CODE = incident.code 
		FOR XML RAW('support-request'), TYPE
	) AS 'support-requests',
	(
		SELECT
		(	
			SELECT 'value' = 
				CASE
					WHEN incident.END_DATE IS NULL THEN DATEDIFF(minute, incident.START_DATE, getdate())
					ELSE DATEDIFF(minute, incident.START_DATE, incident.END_DATE) 
				END 	
			FOR XML RAW('attention-time'), TYPE		
		),
		(
			SELECT DISTINCT (dt.[NAME]) AS 'name' FROM DEPARTMENT_TYPE dt
			JOIN INCIDENT_NOTIFICATION incNot ON dt.CODE = incNot.DEPARTMENT_TYPE_CODE
			JOIN REPORT_BASE rb ON rb.CODE = incNot.REPORT_BASE_CODE
			WHERE rb.INCIDENT_CODE = incident.code
			FOR XML RAW('department'), TYPE
		)AS 'involved-departments',			
		(
			SELECT COUNT(*) FROM DISPATCH_ORDER do
			JOIN INCIDENT_NOTIFICATION incNot ON incNot.CODE = do.INCIDENT_NOTIFICATION_CODE
			JOIN REPORT_BASE rb ON rb.CODE = incNot.REPORT_BASE_CODE
			WHERE rb.INCIDENT_CODE = incident.CODE
		) AS 'total-assigned-units',
		(
			SELECT DISTINCT(ut.[NAME]) as 'name'
			FROM UNIT_TYPE ut
			JOIN UNIT unit ON unit.UNIT_TYPE_CODE = ut.CODE
			JOIN DISPATCH_ORDER do ON do.UNIT_CODE = unit.CODE
			JOIN INCIDENT_NOTIFICATION incNot ON incNot.CODE = do.INCIDENT_NOTIFICATION_CODE
			JOIN REPORT_BASE rb ON rb.CODE = incNot.REPORT_BASE_CODE
			WHERE rb.INCIDENT_CODE = incident.CODE
			FOR XML RAW('unit-type'), TYPE
		) AS 'unit-types'
		FOR XML RAW('summary'),ELEMENTS, TYPE		
	)
    FROM INCIDENT incident WHERE incident.CODE = {0}
	FOR XML RAW('incident'), TYPE
	
	--select * from INCIDENT