DECLARE @CRLF CHAR(2)
DECLARE @TAB CHAR(1)
SET @CRLF = CHAR(13) + CHAR(10)
SET @TAB = CHAR(9)

SELECT result.[Text]
from
(
select
'0' as [Name], 0 as [Index],
'DECLARE @RESULT NVARCHAR(MAX)
DECLARE @CRLF CHAR(2)
SET @CRLF = CHAR(13) + CHAR(10) 
SET @RESULT = ''' + '''
' as [Text]

union

select '0' as [Name], 1 as [Index], 
'IF NOT EXISTS(SELECT * FROM SYSOBJECTS SO WHERE SO.XTYPE = ''U'' AND SO.UID = 1)
BEGIN
	SELECT ''0''
END
ELSE
BEGIN
' as [Text]

union

select 
SO.NAME, 2, 
@TAB + '-- TABLE INFORMATION ' + SO.NAME + @CRLF +
@TAB + 'IF NOT EXISTS(SELECT * FROM sysobjects so WHERE so.name = ''' + so.name + ''')' + @CRLF
+ @TAB + 'BEGIN ' + @CRLF 
+ @TAB + @TAB + 'SET @RESULT = @RESULT + ''' +
'MISSING TABLE ' + so.name + ''' + @CRLF' + @CRLF 
+ @TAB + 'END' + @CRLF 
+ @TAB + 'ELSE' + @CRLF 
+ @TAB + 'BEGIN' + @CRLF
from sysobjects so where xtype = 'U' and uid = 1

union

SELECT
SO.NAME, 3, 
@TAB + @TAB + 'IF NOT EXISTS(SELECT * FROM syscolumns sc, sysobjects so WHERE sc.id = so.id and so.name = ''' 
+ so.name
+ ''' and sc.name = ''' + sc.name + ''')' + @CRLF
+ @TAB + @TAB + 'BEGIN' + @CRLF
+ @TAB + @TAB + @TAB + 'SET @RESULT = @RESULT + ''' + 'MISSING COLUMN ' + so.name + '.' + sc.name + ''' + @CRLF' + @CRLF
+ @TAB + @TAB + 'END' + @CRLF
FROM syscolumns sc, sysobjects so where sc.id = so.id and so.xtype = 'U' and so.uid = 1

union

SELECT
parent.name, 4, 
@TAB + @TAB + 'IF NOT EXISTS(SELECT * FROM sysobjects so WHERE so.name = ''' 
+ so.name + ''')' + @CRLF
+ @TAB + @TAB + 'BEGIN' + @CRLF
+ @TAB + @TAB +  @TAB + 'SET @RESULT = @RESULT + ''' + 'MISSING PRIMARY KEY ' + so.name + ''' + @CRLF' + @CRLF
+ @TAB + @TAB + 'END' + @CRLF
FROM sysobjects so, (select name, id from sysobjects where xtype = 'U') parent where so.xtype = 'PK' and so.uid = 1 and parent.id = so.parent_obj

union

SELECT
parent.name, 5, 
@TAB + @TAB + 'IF NOT EXISTS(SELECT * FROM sysobjects so WHERE so.name = ''' 
+ so.name + ''')' + @CRLF
+ @TAB + @TAB + 'BEGIN' + @CRLF
+ @TAB + @TAB +  @TAB + 'SET @RESULT = @RESULT + ''' + 'MISSING UNIQUE KEY ' + so.name + ''' + @CRLF' + @CRLF
+ @TAB + @TAB + 'END' + @CRLF
FROM sysobjects so, (select name, id from sysobjects where xtype = 'U') parent where so.xtype = 'UQ' and so.uid = 1 and parent.id = so.parent_obj

union

SELECT
parent.name, 6, 
@TAB + @TAB + 'IF NOT EXISTS(SELECT * FROM sysobjects so WHERE so.name = ''' 
+ so.name + ''')' + @CRLF
+ @TAB + @TAB + 'BEGIN' + @CRLF
+ @TAB + @TAB + @TAB + 'SET @RESULT = @RESULT + ''' + 'MISSING FOREIGN KEY ' + so.name + ''' + @CRLF' + @CRLF
+ @TAB + @TAB + 'END' + @CRLF
FROM sysobjects so, (select name, id from sysobjects where xtype = 'U') parent where so.xtype = 'F' and so.uid = 1 and parent.id = so.parent_obj

union

SELECT 
SO.NAME, 7,
@TAB + 'END -- TABLE INFORMATION ' + SO.NAME + @CRLF
from sysobjects so where xtype = 'U' and uid = 1

union

select 
'Z', 8,
'END' +
@CRLF + 'SELECT @RESULT'
) as result