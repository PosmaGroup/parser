CREATE TRIGGER [AfterInsertOrUpdateRecord]
ON [dbo].[reports]
AFTER INSERT, UPDATE
AS
BEGIN
	DECLARE @_id varchar(250)
	DECLARE @_date datetime
	DECLARE @_frame varchar(250)

	SET @_id = (SELECT INSERTED.id
					FROM INSERTED)

	SET @_date = (SELECT INSERTED.date
					FROM INSERTED)
	
	SET @_frame = (SELECT INSERTED.frame
					FROM INSERTED)


	INSERT INTO [dbo].[history] (id, date, frame) 
	VALUES (@_id, @_date, @_frame)    

END
