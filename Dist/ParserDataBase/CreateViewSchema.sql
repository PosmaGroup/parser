CREATE VIEW reports 
AS
SELECT     TOP (100) PERCENT id, type, date, frame
FROM         dbo.history AS history
WHERE     (date IN
			  (SELECT     MAX(date) AS Expr1
				FROM          dbo.history AS history_1
				WHERE      (history.id = id AND history.type = type)))
ORDER BY date
