﻿
//using SmartCadServer.DataAccess;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using SmartCadCore.ClientData;
using SmartCadCore.Core;
using SmartCadCore.Model;
using Smartmatic.SmartCad.Service;

namespace SmartCadServer.Init
{
    public class SmartCadLoadInitialData
    {
        private static bool isInit = false;
        internal static readonly string DataAssembly = "SmartCadServer";
        internal static readonly string ClientDataAssembly = "SmartCadServer";
        internal static bool IsInit
        {
            get
            {
                return isInit;
            }
        }

        public static void Load()
        {
            if (IsInit == false)
            {
                LoadInitialData();

                isInit = true;
            }
        }

        private static void LoadInitialData()
        {
            Assembly assembly = Assembly.Load(DataAssembly);

            Type[] types = assembly.GetTypes();

            foreach (Type type in types)
            {
                if (type.IsClass == true
                    && type.IsAbstract == false
                    && type.IsSubclassOf(typeof(ObjectData)))
                {
                    LoadInitialData(type);
                }
            }
        }

        private static void LoadInitialData(Type type)
        {
            object obj;

            try
            {
                ObjectData objectData = (ObjectData)type.GetConstructor(new Type[0] { }).Invoke(new object[0] { });

                foreach (FieldInfo fi in objectData.GetType().GetFields(BindingFlags.Public | BindingFlags.Static))
                {
                    if (fi.IsLiteral == false && fi.GetValue(null) == null)
                    {
                        obj = fi.FieldType.GetConstructor(new Type[0] { }).Invoke(new object[0] { });

                        object[] atttributes = fi.GetCustomAttributes(typeof(InitialDataAttribute), true);
                        InitialDataAttribute initialDataAttribute = null;
                        if (atttributes.Length > 0)
                            initialDataAttribute = (InitialDataAttribute)atttributes[0];

                        if (initialDataAttribute != null)
                        {
                            PropertyInfo property = obj.GetType().GetProperty(initialDataAttribute.PropertyName);
                            if (property != null)
                            {
                                property.SetValue(obj, initialDataAttribute.PropertyValue, null);
                            }
                        }
                        IList list = SmartCadDatabase.SearchObjects((ObjectData)obj);
                        if (list.Count > 0)
                        {
                            obj = list[0];
                            fi.SetValue(objectData, obj);
                        }
                    }
                }
            }
            catch
            {
                return;
            }
        }

        public static void LoadInitialClientData()
        {
            Assembly assembly = Assembly.Load(ClientDataAssembly);

            Type[] types = assembly.GetTypes();

            Dictionary<string, IList> querys = new Dictionary<string, IList>();

            for (int i = 0; i < 2; i++)
            {
                bool createQuery = i % 2 == 0;
                foreach (Type type in types)
                {
                    if (type.IsClass == true
                        && type.IsAbstract == false
                        && type.IsSubclassOf(typeof(ClientData)))
                    {
                        LoadInitialClientData(type, createQuery, ref querys);
                    }
                }
                if (createQuery)
                    querys = ServerServiceClient.GetInstance().SearchClientObjectsMaxRowsMultiQuery(true, querys);
            }
        }

        private static void LoadInitialClientData(Type type, bool createQuery, ref Dictionary<string, IList> querys)
        {
            object obj;

            try
            {
                ClientData clientData = (ClientData)type.GetConstructor(new Type[0] { }).Invoke(new object[0] { });
                string hql = string.Empty;
                foreach (FieldInfo fi in clientData.GetType().GetFields(BindingFlags.Public | BindingFlags.Static))
                {
                    bool hasToGet = false;
                    if (fi.IsLiteral == false && fi.GetValue(null) == null && createQuery == true)
                    {
                        obj = fi.FieldType.GetConstructor(new Type[0] { }).Invoke(new object[0] { });

                        object[] atttributes = fi.GetCustomAttributes(typeof(InitialDataClientAttribute), true);
                        InitialDataClientAttribute initialDataAttribute = null;
                        if (atttributes.Length > 0)
                            initialDataAttribute = (InitialDataClientAttribute)atttributes[0];

                        if (initialDataAttribute != null)
                        {
                            PropertyInfo property = obj.GetType().GetProperty(initialDataAttribute.PropertyName);
                            if (property != null)
                            {
                                property.SetValue(obj, initialDataAttribute.PropertyValue, null);
                            }
                            string data = fi.FieldType.Name.Replace("Client", "");
                            hql = SmartCadHqls.GetCustomHql("Select data From {0} data Where ", data);
                            hql += "data." + initialDataAttribute.PropertyName + " = '" + initialDataAttribute.PropertyValue + "'";
                            hasToGet = true;
                        }
                        if (hasToGet)
                        {
                            querys[type.Name + fi.Name] = new ArrayList();
                            querys[type.Name + fi.Name].Add(hql);
                        }
                    }

                    else if (createQuery == false)
                    {
                        foreach (ClientData item in querys[type.Name + fi.Name])
                        {
                            fi.SetValue(item, item);
                        }
                    }
                }
            }
            catch
            {
                return;
            }
        }
    }
}
