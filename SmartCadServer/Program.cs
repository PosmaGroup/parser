﻿
//using SmartCadDatabase = SmartCadServer.DataAccess.SmartCadDatabase;
//using SmartCadServer.DataAccess;
//using SmartCadServer.Init;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Management;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Security.AccessControl;
using System.ServiceModel;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;


using SmartCadCore.Core;
using SmartCadCore.Common;
using SmartCadServer.SmartCad.Core;
using Smartmatic.SmartCad.Service;


namespace SmartCadServer
{
    #region Class Program Documentation
    /// <summary>
    /// Clase base de data.
    /// </summary>
    /// <className>Program</className>
    /// <author email="fsilva@smartmatic.com">Fernando Silva</author>
    /// <author email="atorres@smartmatic.com">Alden Torres</author>
    /// <date>2007/01/31</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    class Program
    {
        static ServiceHost serverService;

        static void Main(string[] args)
        {

            if (IsFromServiceManager())
            {
                StartService();
            }
            else
            {
                //ApplicationUtil.ActivateProcess(Process.GetCurrentProcess());
                StartProgram();
            }
        }

        public static void PrintMisc()
        {
            Console.WriteLine("----- Misc --------------------------------------------------");

            Color controlColor = FormUtil.ControlColor;
            Console.WriteLine("ControlColor = " + "A(" + controlColor.A + ")," + "R(" + controlColor.R + ")," + "G(" + controlColor.G + ")," + "B(" + controlColor.B + ")");

            Color darkControlColor = ControlPaint.Dark(FormUtil.ControlColor);
            Console.WriteLine("DarkControlColor = " + "A(" + darkControlColor.A + ")," + "R(" + darkControlColor.R + ")," + "G(" + darkControlColor.G + ")," + "B(" + darkControlColor.B + ")");

            Color color1 = ControlPaint.LightLight(Color.FromArgb(231, 231, 233));
            Console.WriteLine("Color1 = " + "A(" + color1.A + ")," + "R(" + color1.R + ")," + "G(" + color1.G + ")," + "B(" + color1.B + ")");

            Font font = new Font(FontFamily.GenericSansSerif, 10);

            TypeConverter typeConverter = TypeDescriptor.GetConverter(typeof(Font));

            if (typeConverter != null)
                Console.WriteLine("Font = " + typeConverter.ConvertToString(font));

            Console.WriteLine("-------------------------------------------------------------");
        }

        private static bool IsFromServiceManager()
        {
            Uri uri = new Uri(Assembly.GetExecutingAssembly().EscapedCodeBase);
            FileInfo fi = new FileInfo(uri.LocalPath);

            return fi.DirectoryName.ToUpper() != Environment.CurrentDirectory.ToUpper();
        }

        private static void StartService()
        {
            ServerConsoleService serverConsoleService = new ServerConsoleService();

            ServiceBase.Run(serverConsoleService);
        }

        public static void StartProgram()
        {
            try
            {
                SmartCadConfiguration.Load();
                
               

                string language = SmartCadConfiguration.SmartCadSection.CultureElement.CultureLanguageElement.Name + "-" +
                SmartCadConfiguration.SmartCadSection.CultureElement.CultureCountryElement.Name;
               
                try 
                {
                    ResourceLoader.UpdatedLanguage(new CultureInfo(language));
                    
                }
                catch (Exception ex) 
                {
                    SmartLogger.Print("Hubo un error cargando el lenguaje");
                }

#if DEBUG
                //PrintMisc();

                string directoryPath = SmartCadConfiguration.DistFolder;

                DirectorySecurity directorySecurity = Directory.GetAccessControl(directoryPath, AccessControlSections.All);

                //       directorySecurity.AddAccessRule(
                //           new FileSystemAccessRule(
                //                @"Everyone",
                //                FileSystemRights.FullControl,
                //                 InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit,
                //                 PropagationFlags.None,
                //                  AccessControlType.Allow));
                //            Directory.SetAccessControl(directoryPath, directorySecurity);
#endif
                SmartLogger.Print(ResourceLoader.GetString2("InitializeServer"));

#if DEBUG
                SmartLogger.Print(ResourceLoader.GetString2("UpdateDataBase"));
                try
                {
                    SmartCadDatabase.UpgradeDatabaseSchema();
                }
                catch (Exception ex)
                {
                    SmartLogger.Print(ex);
                }
#else
               // SmartLogger.Print(ResourceLoader.GetString2("CheckSquemeDataBase"));
                try
                {
                 //   SmartCadDatabase.CheckDatabaseSchema();
                }
                catch(Exception ex)
                {
                    SmartLogger.Print(ex);
                    //System.Threading.Thread.Sleep(1000);
                    //TODO:After fixing CheckDataBaseSchema
                    //ServerConsoleService service = new ServerConsoleService();

                    //ServiceController[] services = ServiceController.GetServices();
                    //for (int i = 0; i < services.Length; i++)
                    //{
                    //    if (services[i].ServiceName == service.ServiceName)
                    //    {
                    //        ServiceController sc = new ServiceController(services[i].DisplayName);

                    //        if (sc.Status == ServiceControllerStatus.Running)
                    //        {
                    //            sc.Stop();
                    //        }
                    //    }
                    //} 
                    return;
                }
#endif

               SmartCadDatabase.Init();
                SmartCadDataValidator.Init();
                SmartCadLoadInitialData.Load();

                if (SmartCadConfiguration.VideoAnaliticaProperties.activate)
                {

                    SmartCadVideoAnalitica.Start();
                }
                
                //Main Server
                serverService = ServerService.Host();
                //File server, this updates the files from the report and map application.
                ServiceHost fileServerService = FileServerService.Host();
                //Device server, would server for communication from the GPS, LPR and DVR service.
                DeviceServerService dss = DeviceServerService.Current;


                serverService.Open();

                fileServerService.Open();

                //dss.OpenHost();

                //Start the indicator engine.
                SmartCadIndicators.Start();

                //Start the social Networks engine.
                //SocialNetworksEngine.Start();

                EventWaitHandle startNLB = new EventWaitHandle(false, EventResetMode.ManualReset, SmartCadConfiguration.ServiceName);
                //Check if the server is running and that the DB is active.
                if (WMI_NlbNode.CheckNlb() == true)
                {
                    Thread threadCheckRunning = new Thread(new ThreadStart(CheckRunning));
                    threadCheckRunning.Start();
                    startNLB.Set();
                }

                SmartLogger.Print(ResourceLoader.GetString2(ResourceLoader.GetString2("ServerActiveIn") + " " + serverService.Description.Endpoints[0].Address.ToString()));

                SmartLogger.Print(ResourceLoader.GetString2("ServerStarted"));

                SmartLogger.Print(ResourceLoader.GetString2("PressAnyKeyFinish"));

                Console.ReadLine();

                SmartCadIndicators.Stop();

                //SocialNetworksEngine.Stop();

                if (ParserServiceClient.Current != null)
                    ParserServiceClient.Current.Close();

                ServerService.helper.Stop();

                dss.Close();

                serverService.Close();

                fileServerService.Close();
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
            }
        }

        public static void CheckRunning()
        {
            TcpListener listener = null;
            try
            {
                WMI_NlbNode node = new WMI_NlbNode();
                listener = new TcpListener(IPAddress.Parse(node.DedicatedIPAddress), SmartCadConfiguration.SERVER_SERVICE_PORT);
                listener.Start();

                SmartLogger.Print(ResourceLoader.GetString2("NodeStarted"));

                while (Thread.CurrentThread.IsAlive)
                {
                    if (listener.Pending() == true)
                    {
                        TcpClient client = listener.AcceptTcpClient();

                        string ip = ((IPEndPoint)client.Client.RemoteEndPoint).Address.ToString();

                        try
                        {
                            if (WMI_NlbNode.AllDedicatedIPsNodes().IndexOf(ip) > -1)
                            {
                                if (serverService.State == CommunicationState.Opened)
                                {
                                    client.GetStream().WriteByte(0);
                                    client.GetStream().Close();
                                }
                                else
                                {
                                    client.GetStream().WriteByte(1);
                                    client.GetStream().Close();
                                    client.Close();
                                }
                            }
                            else
                            {
                                client.Client.Disconnect(false);
                            }
                            client.Client.Close();
                            client.Close();
                        }
                        catch (Exception e)
                        {
                            SmartLogger.Print(e.ToString());

                            try
                            {
                                client.Client.Close();
                                client.Close();
                            }
                            catch { }
                        }
                    }
                    Thread.Sleep(1000);
                }

                listener.Stop();
            }
            catch (ManagementException me)
            {
                SmartLogger.Print(me);
                SmartLogger.Print(ResourceLoader.GetString2("UnavailableNodeInformation"));
                return;
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
                SmartLogger.Print(ResourceLoader.GetString2("PortOpenException"));
                return;
            }
        }
    }
}
