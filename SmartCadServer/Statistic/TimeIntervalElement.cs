﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadServer.Statistic
{
    public class TimeIntervalElement : ConfigurationElement
    {
        [ConfigurationProperty("time-interval-type", IsRequired = true)]
        public timeIntervalType TimeIntervalType
        {
            get
            {
                return (timeIntervalType)this["time-interval-type"];
            }
            set
            {
                this["time-interval-type"] = value;
            }
        }

        [ConfigurationProperty("length", IsRequired = false)]
        public int Length
        {
            get
            {
                return (int)this["length"];
            }
            set
            {
                this["length"] = value;
            }
        }

        [ConfigurationProperty("slide-length", IsRequired = false)]
        public int SlideLength
        {
            get
            {
                return (int)this["slide-length"];
            }
            set
            {
                this["slide-length"] = value;
            }
        }

        [ConfigurationProperty("time-profile-name", IsRequired = false)]
        public string TimeProfileName
        {
            get
            {
                return (string)this["time-profile-name"];
            }
            set
            {
                this["time-profile-name"] = value;
            }
        }
    }
}
