﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadServer.Statistic
{
    public class MetricElement : ConfigurationElement
    {
        [ConfigurationProperty("metric-type-name", IsRequired = true, IsKey = true)]
        public string MetricTypeName
        {
            get
            {
                return this["metric-type-name"] as string;
            }
            set
            {
                this["metric-type-name"] = value;
            }
        }

        [ConfigurationProperty("filter-name", IsRequired = false)]
        public string FilterName
        {
            get
            {
                return this["filter-name"] as string;
            }
            set
            {
                this["filter-name"] = value;
            }
        }

        [ConfigurationProperty("object-type", IsRequired = true)]
        public objectType ObjectType
        {
            get
            {
                return (objectType)this["object-type"];
            }
            set
            {
                this["object-type"] = value;
            }
        }

        [ConfigurationProperty("time-interval", IsRequired = true)]
        public TimeIntervalElement TimeIntervalElement
        {
            get
            {
                return this["time-interval"] as TimeIntervalElement;
            }
            set
            {
                this["time-interval"] = value;
            }
        }
    }
}
