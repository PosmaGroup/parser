﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadServer.Statistic
{
    /// <remarks/>
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name = "StatServiceSoapBinding", Namespace = "http://www.genesyslab.com/services/statservice/wsdl/2002/03")]
    [System.Xml.Serialization.SoapIncludeAttribute(typeof(timeProfile))]
    [System.Xml.Serialization.SoapIncludeAttribute(typeof(statisticTypeInfoType))]
    [System.Xml.Serialization.SoapIncludeAttribute(typeof(parameter))]
    [System.Xml.Serialization.SoapIncludeAttribute(typeof(keyValue))]
    [System.Xml.Serialization.SoapIncludeAttribute(typeof(stateDNAction))]
    [System.Xml.Serialization.SoapIncludeAttribute(typeof(eventValue))]
    [System.Xml.Serialization.SoapIncludeAttribute(typeof(statisticSubscription))]
    public class StatServiceService : System.Web.Services.Protocols.SoapHttpClientProtocol
    {

        /// <remarks/>
        public StatServiceService()
        {
            this.Url = "http://servername:8080/gis/services/StatService";
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("", RequestNamespace = "http://www.genesyslab.com/services/statservice/wsdl/2002/03", ResponseNamespace = "http://www.genesyslab.com/services/statservice/wsdl/2002/03")]
        public void subscribeStatistic(statistic statistic, string resource, unsolicitedNotification remoting)
        {
            this.Invoke("subscribeStatistic", new object[] {
                        statistic,
                        resource,
                        remoting});
        }

        /// <remarks/>
        public System.IAsyncResult BeginsubscribeStatistic(statistic statistic, string resource, unsolicitedNotification remoting, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("subscribeStatistic", new object[] {
                        statistic,
                        resource,
                        remoting}, callback, asyncState);
        }

        /// <remarks/>
        public void EndsubscribeStatistic(System.IAsyncResult asyncResult)
        {
            this.EndInvoke(asyncResult);
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("", RequestNamespace = "http://www.genesyslab.com/services/statservice/wsdl/2002/03", ResponseNamespace = "http://www.genesyslab.com/services/statservice/wsdl/2002/03")]
        public void unsubscribeStatistic(string statisticId)
        {
            this.Invoke("unsubscribeStatistic", new object[] {
                        statisticId});
        }

        /// <remarks/>
        public System.IAsyncResult BeginunsubscribeStatistic(string statisticId, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("unsubscribeStatistic", new object[] {
                        statisticId}, callback, asyncState);
        }

        /// <remarks/>
        public void EndunsubscribeStatistic(System.IAsyncResult asyncResult)
        {
            this.EndInvoke(asyncResult);
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("", RequestNamespace = "http://www.genesyslab.com/services/statservice/wsdl/2002/03", ResponseNamespace = "http://www.genesyslab.com/services/statservice/wsdl/2002/03")]
        public retrieveSubscribedStatisticsResponse retrieveSubscribedStatistics(statisticSubscriptions subscriptions, notification notification)
        {
            object[] results = this.Invoke("retrieveSubscribedStatistics", new object[] {
                        subscriptions,
                        notification});
            return ((retrieveSubscribedStatisticsResponse)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginretrieveSubscribedStatistics(statisticSubscriptions subscriptions, notification notification, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("retrieveSubscribedStatistics", new object[] {
                        subscriptions,
                        notification}, callback, asyncState);
        }

        /// <remarks/>
        public retrieveSubscribedStatisticsResponse EndretrieveSubscribedStatistics(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((retrieveSubscribedStatisticsResponse)(results[0]));
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("", RequestNamespace = "http://www.genesyslab.com/services/statservice/wsdl/2002/03", ResponseNamespace = "http://www.genesyslab.com/services/statservice/wsdl/2002/03")]
        public retrieveStatisticResponse retrieveStatistic(statistic statistic, string resource)
        {
            object[] results = this.Invoke("retrieveStatistic", new object[] {
                        statistic,
                        resource});
            return ((retrieveStatisticResponse)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginretrieveStatistic(statistic statistic, string resource, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("retrieveStatistic", new object[] {
                        statistic,
                        resource}, callback, asyncState);
        }

        /// <remarks/>
        public retrieveStatisticResponse EndretrieveStatistic(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((retrieveStatisticResponse)(results[0]));
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("", RequestNamespace = "http://www.genesyslab.com/services/statservice/wsdl/2002/03", ResponseNamespace = "http://www.genesyslab.com/services/statservice/wsdl/2002/03")]
        public retrieveStatisticalProfileResponse retrieveStatisticalProfile(string resource, statisticalProfileType profileType)
        {
            object[] results = this.Invoke("retrieveStatisticalProfile", new object[] {
                        resource,
                        profileType});
            return ((retrieveStatisticalProfileResponse)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginretrieveStatisticalProfile(string resource, statisticalProfileType profileType, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("retrieveStatisticalProfile", new object[] {
                        resource,
                        profileType}, callback, asyncState);
        }

        /// <remarks/>
        public retrieveStatisticalProfileResponse EndretrieveStatisticalProfile(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((retrieveStatisticalProfileResponse)(results[0]));
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.SoapTypeAttribute("statistic", "http://www.genesyslab.com/services/statservice/types/2002/03")]
    public class statistic
    {

        /// <remarks/>
        public metric metric;

        /// <remarks/>
        public objectIdType objectId;

        /// <remarks/>
        public schedule schedule;

        /// <remarks/>
        public string statisticId;
    }

    /// <remarks/>
    [System.Xml.Serialization.SoapTypeAttribute("metric", "http://www.genesyslab.com/services/statservice/types/2002/03")]
    public class metric
    {

        /// <remarks/>
        public string filterName;

        /// <remarks/>
        public timeInterval interval;

        /// <remarks/>
        public statisticType statisticType;

        /// <remarks/>
        public timeRangeType timeRange;

        /// <remarks/>
        public string timeRangeName;

        /// <remarks/>
        public string typeName;
    }

    /// <remarks/>
    [System.Xml.Serialization.SoapTypeAttribute("timeInterval", "http://www.genesyslab.com/services/statservice/types/2002/03")]
    public class timeInterval
    {

        /// <remarks/>
        public timeIntervalType intervalType;

        /// <remarks/>
        public int length;

        /// <remarks/>
        public int slideLength;

        /// <remarks/>
        public string timeProfileName;
    }

    /// <remarks/>
    [System.Xml.Serialization.SoapTypeAttribute("timeIntervalType", "http://www.genesyslab.com/services/statservice/types/2002/03")]
    public enum timeIntervalType
    {

        /// <remarks/>
        GrowingWindow,

        /// <remarks/>
        SlidingWindow,

        /// <remarks/>
        SlidingSelectionWindow,
    }

    /// <remarks/>
    [System.Xml.Serialization.SoapTypeAttribute("timeProfile", "http://www.genesyslab.com/services/statservice/types/2002/03")]
    public class timeProfile
    {

        /// <remarks/>
        public timeIntervalType intervalType;

        /// <remarks/>
        public string key;

        /// <remarks/>
        public string value;
    }

    /// <remarks/>
    [System.Xml.Serialization.SoapTypeAttribute("statisticTypeInfoType", "http://www.genesyslab.com/services/statservice/types/2002/03")]
    public class statisticTypeInfoType
    {

        /// <remarks/>
        public objectType[] objectTypes;

        /// <remarks/>
        public eventValueTypeType type;

        /// <remarks/>
        public string typeName;
    }

    /// <remarks/>
    [System.Xml.Serialization.SoapTypeAttribute("objectType", "http://www.genesyslab.com/services/statservice/types/2002/03")]
    public enum objectType
    {

        /// <remarks/>
        Agent,

        /// <remarks/>
        Place,

        /// <remarks/>
        RegDN,

        /// <remarks/>
        Queue,

        /// <remarks/>
        RoutePoint,

        /// <remarks/>
        GroupAgents,

        /// <remarks/>
        GroupPlaces,

        /// <remarks/>
        GroupQueues,

        /// <remarks/>
        Tenant,

        /// <remarks/>
        Strategy,

        /// <remarks/>
        Campaign,

        /// <remarks/>
        CallingList,

        /// <remarks/>
        StagingArea,

        /// <remarks/>
        CampaignGroup,

        /// <remarks/>
        CampaignCallingList,
    }

    /// <remarks/>
    [System.Xml.Serialization.SoapTypeAttribute("eventValueTypeType", "http://www.genesyslab.com/services/statservice/types/2002/03")]
    public enum eventValueTypeType
    {

        /// <remarks/>
        fValue,

        /// <remarks/>
        lValue,

        /// <remarks/>
        unknownValue,
    }

    /// <remarks/>
    [System.Xml.Serialization.SoapTypeAttribute("profileInfo", "http://www.genesyslab.com/services/statservice/types/2002/03")]
    public class profileInfo
    {

        /// <remarks/>
        public parameter[] filters;

        /// <remarks/>
        public statisticalProfileType profileType;

        /// <remarks/>
        public statisticTypeInfoType[] statisticInfos;

        /// <remarks/>
        public timeProfile[] timeProfiles;

        /// <remarks/>
        public parameter[] timeRanges;
    }

    /// <remarks/>
    [System.Xml.Serialization.SoapTypeAttribute("parameter", "http://www.genesyslab.com/services/statservice/types/2002/03")]
    public class parameter
    {

        /// <remarks/>
        public string key;

        /// <remarks/>
        public string value;
    }

    /// <remarks/>
    [System.Xml.Serialization.SoapTypeAttribute("statisticalProfileType", "http://www.genesyslab.com/services/statservice/types/2002/03")]
    public enum statisticalProfileType
    {

        /// <remarks/>
        statisticalProfile,

        /// <remarks/>
        timeProfile,

        /// <remarks/>
        filterProfile,

        /// <remarks/>
        timeRangeProfile,
    }

    /// <remarks/>
    [System.Xml.Serialization.SoapTypeAttribute("retrieveStatisticalProfileResponse", "http://www.genesyslab.com/services/statservice/types/2002/03")]
    public class retrieveStatisticalProfileResponse
    {

        /// <remarks/>
        public profileInfo statisticalProfileInfo;
    }

    /// <remarks/>
    [System.Xml.Serialization.SoapTypeAttribute("retrieveStatisticResponse", "http://www.genesyslab.com/services/statservice/types/2002/03")]
    public class retrieveStatisticResponse
    {

        /// <remarks/>
        public statisticValue statisticValue;
    }

    /// <remarks/>
    [System.Xml.Serialization.SoapTypeAttribute("statisticValue", "http://www.genesyslab.com/services/statservice/types/2002/03")]
    public class statisticValue
    {

        /// <remarks/>
        public eventValue[] eventValues;

        /// <remarks/>
        public string statisticId;
    }

    /// <remarks/>
    [System.Xml.Serialization.SoapTypeAttribute("eventValue", "http://www.genesyslab.com/services/statservice/types/2002/03")]
    public class eventValue
    {

        /// <remarks/>
        public System.Single FValue;

        /// <remarks/>
        public long LValue;

        /// <remarks/>
        public string SValue;

        /// <remarks/>
        public agentStatus agentStatus;

        /// <remarks/>
        public long date;

        /// <remarks/>
        public dnStatus dnStatus;

        /// <remarks/>
        public groupStatus groupStatus;

        /// <remarks/>
        public long intervalLength;

        /// <remarks/>
        public statisticState stateValue;
    }

    /// <remarks/>
    [System.Xml.Serialization.SoapTypeAttribute("agentStatus", "http://www.genesyslab.com/services/statservice/types/2005/10")]
    public class agentStatus
    {

        /// <remarks/>
        public string agentId;

        /// <remarks/>
        public dnStatus[] dns;

        /// <remarks/>
        public string loginId;

        /// <remarks/>
        public string placeId;

        /// <remarks/>
        public string status;

        /// <remarks/>
        public long tmStart;
    }

    /// <remarks/>
    [System.Xml.Serialization.SoapTypeAttribute("dnStatus", "http://www.genesyslab.com/services/statservice/types/2005/10")]
    public class dnStatus
    {

        /// <remarks/>
        public stateDNAction[] dnAction;

        /// <remarks/>
        public string dnId;

        /// <remarks/>
        public string status;

        /// <remarks/>
        public string switchId;

        /// <remarks/>
        public long tmStart;

        /// <remarks/>
        public string type;
    }

    /// <remarks/>
    [System.Xml.Serialization.SoapTypeAttribute("stateDNAction", "http://www.genesyslab.com/services/statservice/types/2005/10")]
    public class stateDNAction
    {

        /// <remarks/>
        public string action;

        /// <remarks/>
        public dataCall data;

        /// <remarks/>
        public long tmStart;
    }

    /// <remarks/>
    [System.Xml.Serialization.SoapTypeAttribute("dataCall", "http://www.genesyslab.com/services/statservice/types/2005/10")]
    public class dataCall
    {

        /// <remarks/>
        public connID connId;

        /// <remarks/>
        public string sani;

        /// <remarks/>
        public string sdnis;

        /// <remarks/>
        public keyValue[] userData;
    }

    /// <remarks/>
    [System.Xml.Serialization.SoapTypeAttribute("connID", "http://www.genesyslab.com/services/statservice/types/2005/10")]
    public class connID
    {

        /// <remarks/>
        [System.Xml.Serialization.SoapElementAttribute(DataType = "base64Binary")]
        public System.Byte[] conn_id;
    }

    /// <remarks/>
    [System.Xml.Serialization.SoapTypeAttribute("keyValue", "http://www.genesyslab.com/services/statservice/types/2005/10")]
    public class keyValue
    {

        /// <remarks/>
        [System.Xml.Serialization.SoapElementAttribute(DataType = "base64Binary")]
        public System.Byte[] binaryValue;

        /// <remarks/>
        public int intValue;

        /// <remarks/>
        public string key;

        /// <remarks/>
        public keyValue[] kvListValue;

        /// <remarks/>
        public string stringValue;
    }

    /// <remarks/>
    [System.Xml.Serialization.SoapTypeAttribute("groupStatus", "http://www.genesyslab.com/services/statservice/types/2005/10")]
    public class groupStatus
    {

        /// <remarks/>
        public agentStatus[] agents;

        /// <remarks/>
        public string groupId;

        /// <remarks/>
        public string status;

        /// <remarks/>
        public long tmStart;
    }

    /// <remarks/>
    [System.Xml.Serialization.SoapTypeAttribute("statisticState", "http://www.genesyslab.com/services/statservice/types/2002/03")]
    public class statisticState
    {

        /// <remarks/>
        public long date;

        /// <remarks/>
        public statisticStateData extendedData;

        /// <remarks/>
        public string id;

        /// <remarks/>
        public statisticState[] items;

        /// <remarks/>
        public string state;

        /// <remarks/>
        public statisticStateType type;
    }

    /// <remarks/>
    [System.Xml.Serialization.SoapTypeAttribute("statisticStateData", "http://www.genesyslab.com/services/statservice/types/2002/03")]
    public class statisticStateData
    {

        /// <remarks/>
        public string ANI;

        /// <remarks/>
        public string DNIS;

        /// <remarks/>
        public string agent;

        /// <remarks/>
        public string dnType;

        /// <remarks/>
        public parameter[] list;

        /// <remarks/>
        public string loginID;

        /// <remarks/>
        public string place;

        /// <remarks/>
        public string switchID;
    }

    /// <remarks/>
    [System.Xml.Serialization.SoapTypeAttribute("statisticStateType", "http://www.genesyslab.com/services/statservice/types/2002/03")]
    public enum statisticStateType
    {

        /// <remarks/>
        GroupStatus,

        /// <remarks/>
        AgentStatus,

        /// <remarks/>
        DNStatus,

        /// <remarks/>
        DNAction,
    }

    /// <remarks/>
    [System.Xml.Serialization.SoapTypeAttribute("retrieveSubscribedStatisticsResponse", "http://www.genesyslab.com/services/statservice/types/2002/03")]
    public class retrieveSubscribedStatisticsResponse
    {

        /// <remarks/>
        public statisticValue[] statisticValues;
    }

    /// <remarks/>
    [System.Xml.Serialization.SoapTypeAttribute("notification", "http://www.genesyslab.com/services/statservice/types/2002/03")]
    public class notification
    {

        /// <remarks/>
        public notificationMode mode;

        /// <remarks/>
        [System.Xml.Serialization.SoapElementAttribute(DataType = "integer")]
        public string timeout;
    }

    /// <remarks/>
    [System.Xml.Serialization.SoapTypeAttribute("notificationMode", "http://www.genesyslab.com/services/statservice/types/2002/03")]
    public enum notificationMode
    {

        /// <remarks/>
        Polling,

        /// <remarks/>
        Blocked,
    }

    /// <remarks/>
    [System.Xml.Serialization.SoapTypeAttribute("statisticSubscription", "http://www.genesyslab.com/services/statservice/types/2002/03")]
    public class statisticSubscription
    {

        /// <remarks/>
        public string scope;

        /// <remarks/>
        public string statisticId;
    }

    /// <remarks/>
    [System.Xml.Serialization.SoapTypeAttribute("statisticSubscriptions", "http://www.genesyslab.com/services/statservice/types/2002/03")]
    public class statisticSubscriptions
    {

        /// <remarks/>
        public statisticSubscription[] statisticSubscription;
    }

    /// <remarks/>
    [System.Xml.Serialization.SoapTypeAttribute("unsolicitedNotification", "http://www.genesyslab.com/services/statservice/types/2002/03")]
    public class unsolicitedNotification
    {

        /// <remarks/>
        public string url;
    }

    /// <remarks/>
    [System.Xml.Serialization.SoapTypeAttribute("schedule", "http://www.genesyslab.com/services/statservice/types/2002/03")]
    public class schedule
    {

        /// <remarks/>
        public int insensitivity;

        /// <remarks/>
        public scheduleMode notificationMode;

        /// <remarks/>
        public int timeout;
    }

    /// <remarks/>
    [System.Xml.Serialization.SoapTypeAttribute("scheduleMode", "http://www.genesyslab.com/services/statservice/types/2002/03")]
    public enum scheduleMode
    {

        /// <remarks/>
        ChangesBased,

        /// <remarks/>
        TimeBased,

        /// <remarks/>
        ResetBased,
    }

    /// <remarks/>
    [System.Xml.Serialization.SoapTypeAttribute("objectIdType", "http://www.genesyslab.com/services/statservice/types/2002/03")]
    public class objectIdType
    {

        /// <remarks/>
        public string id;

        /// <remarks/>
        public string tenantName;
    }

    /// <remarks/>
    [System.Xml.Serialization.SoapTypeAttribute("timeRangeType", "http://www.genesyslab.com/services/statservice/types/2002/03")]
    public class timeRangeType
    {

        /// <remarks/>
        public int leftTime;

        /// <remarks/>
        public int rightTime;
    }

    /// <remarks/>
    [System.Xml.Serialization.SoapTypeAttribute("actions", "http://www.genesyslab.com/services/statservice/types/2002/03")]
    public class actions
    {

        /// <remarks/>
        public string[] action;
    }

    /// <remarks/>
    [System.Xml.Serialization.SoapTypeAttribute("statisticType", "http://www.genesyslab.com/services/statservice/types/2002/03")]
    public class statisticType
    {

        /// <remarks/>
        public string category;

        /// <remarks/>
        public actions mainActions;

        /// <remarks/>
        public objectType[] objectType;

        /// <remarks/>
        public actions relativeActions;

        /// <remarks/>
        public string subject;
    }
}
