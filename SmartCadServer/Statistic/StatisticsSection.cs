﻿using SmartCadCore.Core;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadServer.Statistic
{
    #region Class StatisticsSection Documentation
    /// <summary>
    /// It refers to all statistic configuration section. Each statistic asked to server can be
    /// enabled or disabled and all the setting required to make the request is available in this section.
    /// </summary>
    /// <className>StatisticSection</className>
    /// <author email="jynojosa@smartmatic.com">Julio Ynojosa</author>
    /// <date>2009/06/29</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    public class StatisticsSection : ConfigurationSection
    {
        public StatisticsSection()
        { }

        [ConfigurationProperty("statistic-list", IsRequired = false)]
        [ConfigurationCollection(typeof(PropertiesCollection), AddItemName = "statistic")]
        public new PropertiesCollection Properties
        {
            get
            {
                return this["statistic-list"] as PropertiesCollection;
            }
            set
            {
                this["statistic-list"] = value;
            }
        }

    }
}
