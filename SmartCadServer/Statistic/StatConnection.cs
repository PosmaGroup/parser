﻿
using Genesyslab.Desktop.ApplicationBlocks.StatConnection;
using Genesyslab.Desktop.ApplicationBlocks.StatConnection.StatProxy;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadServer.Statistic
{
    public interface IConnectionNotification
    {
        /// <summary>
        /// This method is called when a subscribed statistic changes.
        /// </summary>
        /// <param name="eventType">The string that identifies the service.</param>
        /// <param name="keyValue">The key-value pair that gives the statistic result</param>
        void OnStatisticEvent(String eventType, String[][] keyValue);
    }

    /// <summary>
    /// The <c>Connection</c> component establishes the connection with the GIS.
    /// </summary>
    /// <remarks>
    /// <para>Before you call the <see cref="Connect"/> method to initialize, set the <see cref="PrimaryURL"/> property, as shown here:
    /// <code>Connection mConnection = new Connection();
    /// mConnection.PrimaryURL ="http://[server host]:[server port]/gis";
    /// mConnection.connect();
    /// </code>
    /// </para>
    /// </remarks>
    [ToolboxItem(true)]
    public class Connection : IDisposable
    {
        #region Connection implementation variables
        private bool mIsConnected;
        private bool mDisposed;
        private bool mUsingBackup;
        private string mPrimaryURL;
        private string mBackupURL;
        private string mUserName;
        private string mPassword;
        private string mTenant;
        private string mRemotingPort;
        #endregion

        #region Miscellaneous variables
        private string mTargetURL = "";
        private SessionServiceService mSessionService;
        private string mSid = "";
        private StatServiceService mStatService;
        private unsolicitedNotification _unsolicitedNotification;
        private System.Runtime.Remoting.Channels.Http.HttpServerChannel mChannel;
        // Table of IConnectionNotification object used to dispatch unsolicited events
        private static Hashtable mNotifications = new Hashtable();
        private static Hashtable mRemoteChannels = new Hashtable();
        private static ArrayList mChannelNames = new ArrayList();
        #endregion

        #region Contructors

        /// <summary>
        /// Connection constructor.
        /// </summary>
        public Connection()
        {
        }

        /// <summary>
        /// Connection destructor.
        /// </summary>
        ~Connection()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal in terms of
            // readability and maintainability.
            Dispose(false);
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            // This object is cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        // Dispose(bool disposing) executes in two distinct scenarios.
        // If disposing is true, it means that the method is directly
        // or indirectly called by a user's code. Managed and unmanaged resources
        // can be disposed.
        // If disposing is false, the method is called by the
        // runtime from inside the finalizer and you should not reference
        // other objects. Only unmanaged resources can be disposed.
        private void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this.mDisposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                    if (mStatService != null)
                        mStatService.Dispose();
                    if (mSessionService != null)
                        mSessionService.Dispose();
                }
            }
            mDisposed = true;
        }

        internal static void OnNotification(String eventType, String[][] keyValue)
        {
            if (mNotifications != null && mNotifications.Count > 0)
            {
                if (keyValue.Length >= 2 && keyValue[1].Length == 2 && keyValue[1][0] == "sessionId")
                {
                    IConnectionNotification cn = (IConnectionNotification)mNotifications[keyValue[1][1]];
                    if (cn != null)
                        cn.OnStatisticEvent(eventType, keyValue);
                }
            }
        }

        #endregion

        #region IConnection implementation

        // Properties

        /// <summary>
        /// true if successfully connected.
        /// </summary>
        [Browsable(false)]
        public bool IsConnected { get { return mIsConnected; } }

        // Methods

        /// <summary>
        /// Establishes the connection to the GIS server application. Before you call this method, fill the <see cref="PrimaryURL"/>, <see cref="UserName"/>, and <see cref="Password"/> properties.
        /// <para>If a backup server exists, fill in the property <see cref="BackupURL"/> to be able to switch hosts in case of disconnection. If the connection fails with the primary URL, the Connection class retries with the backup URL.
        /// </para>
        /// <para>This method opens a session on the GIS server by holding back a token. This token is released when you call the <see cref="Disconnect"/> method.
        /// </para>
        /// </summary>
        /// <returns>Return true if the connection succeeded.</returns>
        [Browsable(false)]
        public bool Connect()
        {
            if (mIsConnected)
                Disconnect();
            // Check for host and port properties
            if (mPrimaryURL == null || mPrimaryURL.Length == 0)
            {
                if (mBackupURL == null || mBackupURL.Length == 0)
                    throw ((new StatConnectionException("", "Connection::Connect() : No URL specified!!!")));
                mUsingBackup = true;
                mTargetURL = mBackupURL;
            }
            else
            {
                mUsingBackup = false;
                mTargetURL = mPrimaryURL;
            }
            //create a new proxy instance.
            mSessionService = new SessionServiceService();
            // Check the url connection
            try
            {
                mSessionService.Url = mTargetURL + "/gis/services/SessionService";
            }
            catch (Exception e)
            {
                if (mUsingBackup == true || mBackupURL == null || mBackupURL.Length == 0)
                    throw ((new StatConnectionException("", e.Message, e)));
                // If failed try backup url
                mUsingBackup = true;
                mTargetURL = mBackupURL;
                try
                {
                    mSessionService.Url = mTargetURL + "/services/SessionService";
                }
                catch (Exception e2)
                {
                    throw ((new StatConnectionException("", e2.Message, e2)));
                }
            }
            //login request
            Identity identity = new Identity();
            identity.principal = mUserName;
            identity.credentials = mPassword;
            identity.tenant = mTenant;
            try
            {
                mSid = mSessionService.login(identity);
                mIsConnected = true;
            }
            catch (System.Net.WebException e)
            {
                if (mUsingBackup == true || mBackupURL == null || mBackupURL.Length == 0)
                {
                    mSid = "";
                    throw ((new StatConnectionException("", e.Message, e)));
                }
                // If login failed try with backup
                mUsingBackup = true;
                mTargetURL = mBackupURL;
                try
                {
                    mSessionService.Url = mTargetURL + "/services/SessionService";
                }
                catch (Exception e2)
                {
                    mSid = "";
                    throw ((new StatConnectionException("", e2.Message, e2)));
                }
                try
                {
                    mSid = mSessionService.login(identity);
                    mIsConnected = true;
                }
                catch (Exception e3)
                {
                    mSid = "";
                    throw ((new StatConnectionException("", e3.Message, e3)));
                }
            }
            catch (Exception e)
            {
                mSid = "";
                throw ((new StatConnectionException("", e.Message, e)));
            }
            mSessionService.Url = mTargetURL + "/gis/services/SessionService?GISsessionId=" + mSid;
            return true;
        }

        /// <summary>
        /// Disconnects from the server application.
        /// <para>This method closes the GIS session opened by the <see cref="Connect"/> method. The associated token is released.
        /// </para>
        /// </summary>
        public void Disconnect()
        {
            if (mSessionService != null && mSid.Length > 0)
            {
                if (mChannel != null)
                {
                    if (mChannelNames.Contains(mChannel.ChannelName))
                        mChannelNames.Remove(mChannel.ChannelName);
                    if (!mChannelNames.Contains(mChannel.ChannelName))
                    {
                        mRemoteChannels.Remove(mChannel.ChannelName);
                        System.Runtime.Remoting.Channels.ChannelServices.UnregisterChannel(mChannel);
                        mChannel = null;
                        _unsolicitedNotification = null;
                    }
                }
                mIsConnected = false;
                mStatService = null;
                try
                {
                    mSessionService.logout(mSid);
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Trace.WriteLineIf(true, ex.Message);
                }
                mSessionService = null;
                if (mNotifications != null && mNotifications.Contains(mSid))
                    mNotifications.Remove(mSid);
                mSid = "";
            }
        }

        /// <summary>
        /// Switches from the active host to the backup host.
        /// </summary>
        private void SwitchSessionServiceURL()
        {
            if (mUsingBackup == false)
            {
                if (mBackupURL == null || mBackupURL.Length == 0)
                    throw ((new StatConnectionException("", "Connection::SwitchSessionServiceURL() : No backup URL specified!!!")));
                mUsingBackup = true;
                mTargetURL = mBackupURL;
                try
                {
                    mSessionService.Url = mTargetURL + "/services/SessionService?GISsessionId=" + mSid;
                }
                catch (Exception e)
                {
                    throw ((new StatConnectionException("", e.Message, e)));
                }
            }
            else
            {
                if (mPrimaryURL == null || mPrimaryURL.Length == 0)
                    throw ((new StatConnectionException("", "Connection::SwitchSessionServiceURL() : No primary URL specified!!!")));
                mUsingBackup = false;
                mTargetURL = mPrimaryURL;
                try
                {
                    mSessionService.Url = mTargetURL + "/services/SessionService?GISsessionId=" + mSid;
                }
                catch (Exception e)
                {
                    throw ((new StatConnectionException("", e.Message, e)));
                }
            }
        }

        // Events

        /// <summary>
        /// Primary URL to establish the connection with the server application. Mandatory to call the <see cref="Connect"/> method.
        /// </summary>
        [Browsable(true)]
        [Category("CTI"), Description("Primary URL to establish the connection with the server application. Mandatory to call the Connect method. ")]
        public string PrimaryURL { get { return mPrimaryURL; } set { mPrimaryURL = value; } }

        /// <summary>
        /// Backup URL (optional) used if the connection to the primary server failed.
        /// </summary>
        [Browsable(true)]
        [Category("CTI"), Description("Backup URL (optional) used if the connection to the primary server failed.")]
        public string BackupURL { get { return mBackupURL; } set { mBackupURL = value; } }

        /// <summary>
        /// Username to establish the connection to the server application. Mandatory to call the <see cref="Connect"/> method.
        /// </summary>
        [Browsable(true)]
        [Category("CTI"), Description("Username to establish the connection to the server application. Mandatory to call the Connect method. ")]
        public string UserName { get { return mUserName; } set { mUserName = value; } }

        /// <summary>
        /// Password to establish the connection to the server application. Mandatory to call the <see cref="Connect"/> method.
        /// </summary>
        [Browsable(true)]
        [Category("CTI"), Description("Password must be set in order to establish the connection with the server application.")]
        public string Password { get { return mPassword; } set { mPassword = value; } }

        /// <summary>
        /// Tenant to establish the connection with the server application.
        /// </summary>
        [Browsable(true)]
        [Category("CTI"), Description("Tenant to establish the connection with the server application.")]
        public string Tenant { get { return mTenant; } set { mTenant = value; } }

        /// <summary>
        /// RemotingPort to receive unsolicited events for statistics. See <see cref="GetStatService"/>
        /// </summary>
        [Browsable(true)]
        [Category("CTI"), Description("RemotingPort to receive unsolicited events for statistics.")]
        public string RemotingPort { get { return mRemotingPort; } set { mRemotingPort = value; } }

        /// <summary>
        /// Returns the unsolicited notification object to be used when your application subscribes to a statistic request if the notification is needed.
        /// This object must be passed to the subscribeStatistic method of the StatServiceService object returned by <see cref="GetStatService"/>.
        /// </summary>
        [Browsable(false)]
        [Category("CTI"), Description("Returns the unsolicited notification object to be used when your application subscribes to a statistic request if the notification is needed.")]
        public unsolicitedNotification UnsolicitedNotification { get { return _unsolicitedNotification; } }

        /// <summary>
        /// Switches from the active host to the backup host.<para> Call this method when, on a request for statistics, you detect a connection loss from the active server.</para>
        /// <para>If your client application uses the StatServiceService object (returned by <see cref ="GetStatService"/>) to perform statistic requests, it detects a connection loss when it gets an exception.
        /// If you call this method, the Connection Application Block switches to the backup host. Then, you can again perform requests on the
        /// <code>StatServiceService</code> object, which is still valid. </para>
        /// </summary>
        [Browsable(false)]
        [Category("CTI"), Description("Switches from the active host to the backup host. Call this method when you detect a disconnection from the active server on a statistic request.")]
        public void SwitchStatServiceURL()
        {
            // if current host is primary
            if (mUsingBackup == false)
            {
                // Check backup properties
                if (mBackupURL == null || mBackupURL.Length == 0)
                    throw ((new StatConnectionException("", "Connection::SwitchStatServiceURL() : No backup URL specified!!!")));
                // Check backup url
                mUsingBackup = true;
                mTargetURL = mBackupURL;
                try
                {
                    mStatService.Url = mTargetURL + "/services/StatService?GISsessionId=" + mSid;
                }
                catch (Exception e)
                {
                    throw ((new StatConnectionException("", e.Message, e)));
                }
            }
            else
            {
                // Check primary properties
                if (mPrimaryURL == null || mPrimaryURL.Length == 0)
                    throw ((new StatConnectionException("", "Connection::SwitchStatServiceURL() : No primary URL specified!!!")));
                // Check primary url
                mUsingBackup = false;
                mTargetURL = mPrimaryURL;
                try
                {
                    mStatService.Url = mTargetURL + "/services/StatService?GISsessionId=" + mSid;
                }
                catch (Exception e)
                {
                    throw ((new StatConnectionException("", e.Message, e)));
                }
            }
        }

        /// <summary>
        /// Returns the statistic service to be used to request statistics to the GIS. Call this method if you already perform a successfull <see cref="Connect"/> method call.
        /// <para>If your application requires notification for statistic events, set the <see cref="RemotingPort"/> property before you call this method.</para>para>
        /// </summary>
        /// <param name="notification"> is of type IConnectionNotification. Set this parameter to receive unsolicited notifications; otherwise set to null.</param>
        /// <returns>The StatServiceService object returned by this method is a WSDL generated object.</returns>
        [Browsable(false)]
        public StatServiceService GetStatService(object notification)
        {
            if (!IsConnected)
                return null;
            if (mStatService == null)
            {
                // Create statistic service
                try
                {
                    mStatService = new StatServiceService();
                }
                catch (Exception e)
                {
                    mStatService = null;
                    throw ((new StatConnectionException("", e.Message, e)));
                }
                // Try to get service from GIS
                try
                {
                    mSessionService.getServices(new string[] { "GIS_STATSERVICE" });
                }
                catch (System.Net.WebException e)
                {
                    // If connection failed try to switch to backup server
                    try
                    {
                        SwitchSessionServiceURL();
                    }
                    catch (Exception)
                    {
                        mStatService = null;
                        throw ((new StatConnectionException("", e.Message, e)));
                    }
                    try
                    {
                        mSessionService.getServices(new string[] { "GIS_STATSERVICE" });
                    }
                    catch (Exception e2)
                    {
                        mStatService = null;
                        throw ((new StatConnectionException("", e2.Message, e2)));
                    }
                }
                catch (Exception e)
                {
                    mStatService = null;
                    throw ((new StatConnectionException("", e.Message, e)));
                }
                mStatService.Url = mTargetURL + "/gis/services/StatService?GISsessionId=" + mSid;
                // Register to unsolited notifications if needed
                if (mRemotingPort != null && mRemotingPort.Length > 0)
                {
                    _unsolicitedNotification = new unsolicitedNotification();
                    string remotingHost = System.Net.Dns.GetHostName();
                    _unsolicitedNotification.url = "http://" + remotingHost + ":" + mRemotingPort + "/Notification";
                    try
                    {
                        string channelName = "channel" + mRemotingPort;
                        // Create a new HttpServerChannel
                        if (mRemoteChannels.Contains(channelName))
                            mChannel = (System.Runtime.Remoting.Channels.Http.HttpServerChannel)mRemoteChannels[channelName];
                        else
                        {
                            mChannel = new System.Runtime.Remoting.Channels.Http.HttpServerChannel("channel" + mRemotingPort, Int32.Parse(mRemotingPort));
                            mRemoteChannels[channelName] = mChannel;
                            System.Runtime.Remoting.Channels.ChannelServices.RegisterChannel(mChannel);
                        }
                        mChannelNames.Add(channelName);
                        // Now register the channel
                        System.Runtime.Remoting.RemotingConfiguration.RegisterWellKnownServiceType(System.Type.GetType("Genesyslab.Desktop.ApplicationBlocks.StatConnection.Notification"), "Notification", System.Runtime.Remoting.WellKnownObjectMode.Singleton);
                        remotingHost = "";
                    }
                    catch (Exception e)
                    {
                        mChannel = null;
                        _unsolicitedNotification = null;
                        throw ((new StatConnectionException("", e.Message, e)));
                    }
                }
            }
            if (_unsolicitedNotification != null && mChannel != null && notification != null)
            {
                IConnectionNotification cn = notification as IConnectionNotification;
                if (cn != null)
                {
                    mNotifications[mSid] = cn;
                }
            }
            return mStatService;
        }
        #endregion

    }
}
