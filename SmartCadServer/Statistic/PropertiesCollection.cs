﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadServer.Statistic
{
    #region Class PropertiesCollection Documentation
    /// <summary>
    /// TODO:
    /// </summary>
    /// <className>PropertiesCollection</className>
    /// <author email="jynojosa@smartmatic.com">Julio Ynojosa</author>
    /// <date>2006/03/31</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    public class PropertiesCollection : ConfigurationElementCollection
    {
        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((StatisticElement)element).Name;
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new StatisticElement();
        }

        public new StatisticElement this[string name]
        {
            get
            {
                return BaseGet(name) as StatisticElement;
            }
        }
    }
}
