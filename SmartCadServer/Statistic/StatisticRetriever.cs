﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadServer.Statistic
{
    public class StatisticRetriever : IConnectionNotification
    {
        public enum StatisticNames
        {
            L02TotalCallsAbandoned,
            L05CurrentCallsInQueue,
            L06CurrMaxCallWaitingTime,
            L09TotalCallsEntered,
            L10TotalAnsweredCalls,
            L11TotalCallsAbandonedWR,
            L12ServiceLevel,
            CallsAbandonedInThresholdSubscription,
            CallsAnsweredInThresholdSubscription
        }

        private Connection connection;

        /// <summary>
        /// Server name or ip
        /// </summary>
        private string host;
        /// <summary>
        /// Port where GIS is working
        /// </summary>
        private string portGis;
        /// <summary>
        /// Server user name
        /// </summary>
        private string hostUsername;
        /// <summary>
        /// Server user password
        /// </summary>
        private string passwordHost;
        /// <summary>
        /// Stat service name
        /// </summary>
        private string statServiceName;

        public static event EventHandler<StatisticReceivedEventArgs> StatisticReceived;
        // <summary>
        /// Statistics service proxy class
        /// </summary>
        private StatServiceService statService;
        /// <summary>
        /// Session Id for the current session
        /// </summary>
        private static Dictionary<StatisticNames, double> statisticResults;

        /// <summary>
        /// Password to connect to host where GIS is running
        /// </summary>
        public string PasswordHost
        {
            get
            {
                return passwordHost;
            }
            set
            {
                passwordHost = value;
            }
        }

        public event EventHandler ConnectionLost;

        /// <summary>
        /// User name required to login in host where GIS is running
        /// </summary>
        public string HostUserName
        {
            get
            {
                return hostUsername;
            }
            set
            {
                hostUsername = value;
            }
        }

        /// <summary>
        /// GIS port
        /// </summary>
        public string PortGis
        {
            get
            {
                return portGis;
            }
            set
            {
                portGis = value;
            }
        }

        /// <summary>
        /// Host name or ip host where GIS is running
        /// </summary>
        public string Host
        {
            get
            {
                return host;
            }
            set
            {
                host = value;
            }
        }

        /// <summary>
        /// Stat service name
        /// </summary>
        public string StatServiceName
        {
            get
            {
                return statServiceName;
            }
            set
            {
                statServiceName = value;
            }
        }

        private static StatisticRetriever statisticRetriever;
        /// <summary>
        /// Initializes the GIS connection
        /// </summary>
        /// <param name="host">Host of GIS</param>
        /// <param name="port">Port of GIS</param>
        /// <param name="userhost">CME username for GIS connection</param>
        /// <param name="paswHost">CME password for GIS connection</param>
        /// <param name="statService">stat service name</param>
        private StatisticRetriever(string host, string port, string userhost, string paswHost,
            string statService)
        {
            statisticResults = new Dictionary<StatisticNames, double>();
            this.Host = host;
            this.PortGis = port;
            this.HostUserName = userhost;
            this.PasswordHost = paswHost;
            this.StatServiceName = statService;
            statisticResults.Add(StatisticNames.L02TotalCallsAbandoned, 0);
            statisticResults.Add(StatisticNames.L05CurrentCallsInQueue, 0);
            statisticResults.Add(StatisticNames.L06CurrMaxCallWaitingTime, 0);
            statisticResults.Add(StatisticNames.L09TotalCallsEntered, 0);
            statisticResults.Add(StatisticNames.L10TotalAnsweredCalls, 0);
            statisticResults.Add(StatisticNames.L11TotalCallsAbandonedWR, 0);
            statisticResults.Add(StatisticNames.L12ServiceLevel, 0);
            statisticResults.Add(StatisticNames.CallsAbandonedInThresholdSubscription, 0);
            statisticResults.Add(StatisticNames.CallsAnsweredInThresholdSubscription, 0);
            connection = new Connection();
        }

        public static void Init(string host, string port, string userhost, string paswHost,
            string statService)
        {
            statisticRetriever = new StatisticRetriever(host, port, userhost, paswHost, statService);
            statisticRetriever.Connect();
        }

        public static StatisticRetriever GetStatisticRetriever()
        {
            return statisticRetriever;
        }

        /// <summary>
        /// Makes the connection to stat server and creates the subscription to configured statistics.
        /// </summary>
        /// <returns>Message error if any occurs</returns>
        public void Connect()
        {
            try
            {
                connection.Password = this.PasswordHost;
                connection.PrimaryURL = "http://" + this.Host + ":" + this.PortGis;
                connection.RemotingPort = "8080";
                connection.Tenant = "Resources";
                connection.UserName = this.HostUserName;
                connection.Password = this.PasswordHost;
                connection.Connect();
                statService = connection.GetStatService(this);
                StatisticElement statElement = StatisticsConfiguration.StatisticsSection.Properties[StatisticNames.L02TotalCallsAbandoned.ToString()];
                if (statElement.Enabled)
                {
                    statistic statisticRequestObj = BuildStatistic(statElement);
                    try
                    {
                        statService.subscribeStatistic(statisticRequestObj, StatServiceName, connection.UnsolicitedNotification);
                    }
                    catch (Exception ex)
                    {
                        if (ConnectionLost != null)
                            ConnectionLost(null, EventArgs.Empty);
                    }
                }
            }
            catch
            {
                try
                {
                    if (connection != null)
                    {
                        connection.Disconnect();
                        connection = null;
                        statService = null;
                    }
                }
                catch
                {
                    throw;
                }
                throw;
            }
        }

        private double GetStatValue(StatisticNames statId, retrieveStatisticResponse response)
        {
            double result = 0;
            eventValue evntValue = response.statisticValue.eventValues[0];
            switch (statId)
            {
                case StatisticNames.L02TotalCallsAbandoned:
                case StatisticNames.L05CurrentCallsInQueue:
                case StatisticNames.L06CurrMaxCallWaitingTime:
                case StatisticNames.L09TotalCallsEntered:
                case StatisticNames.L10TotalAnsweredCalls:
                case StatisticNames.L11TotalCallsAbandonedWR:
                case StatisticNames.CallsAbandonedInThresholdSubscription:
                case StatisticNames.CallsAnsweredInThresholdSubscription:
                    result = (double)evntValue.LValue;
                    break;
                case StatisticNames.L12ServiceLevel:
                    result = serviceLevel;
                    break;
            }
            return result;
        }

        /// <summary>
        /// Release subscription to stat server
        /// </summary>
        /// <param name="exceptionMsg">Message of any exception ocurred during stat unregistering</param>
        /// <returns>true if unregister was successfully done</returns>
        public bool Disconnect(ref string exceptionMsg)
        {
            bool result = false;
            try
            {
                //Borrar si la subscripción a los eventos no funciona definitivamente
                //statService.unsubscribeStatistic(StatisticNames.L02TotalCallsAbandoned.ToString());
                //statService.unsubscribeStatistic(StatisticNames.L05CurrentCallsInQueue.ToString());
                //statService.unsubscribeStatistic(StatisticNames.L06CurrMaxCallWaitingTime.ToString());
                //statService.unsubscribeStatistic(StatisticNames.L09TotalCallsEntered.ToString());
                //statService.unsubscribeStatistic(StatisticNames.L10TotalAnsweredCalls.ToString());
                //statService.unsubscribeStatistic(StatisticNames.L11TotalCallsAbandonedWR.ToString());
                //statService.unsubscribeStatistic(StatisticNames.L12ServiceFactor.ToString());
                //statService.unsubscribeStatistic(StatisticNames.CallsAbandonedInThresholdSubscription.ToString());
                //statService.unsubscribeStatistic(StatisticNames.CallsAnsweredInThresholdSubscription.ToString());
                result = true;
            }
            catch (Exception ex)
            {
                exceptionMsg = ex.Message;
            }
            finally
            {
                try
                {
                    connection.Disconnect();

                }
                catch (Exception ex)
                {
                    exceptionMsg = ex.Message;
                }
            }
            return result;
        }

        /// <summary>
        /// Gets the last statistic value received given statID
        /// </summary>
        /// <param name="statId">Statistical id</param>
        /// <returns>Value received from stat served for this statistic</returns>
        public double GetStatisticValue(StatisticNames statId)
        {
            return statisticResults[statId];

        }

        /// <summary>
        /// Build an statistic object with data suministered in statElement
        /// </summary>
        /// <param name="statElement"></param>
        /// <returns></returns>
        private statistic BuildStatistic(
            StatisticElement statElement)
        {
            //Construct statistic definition
            statistic stat = new statistic();

            stat.metric = new metric();
            stat.metric.statisticType = new statisticType();

            stat.schedule = new schedule();
            stat.metric.statisticType.objectType = new objectType[] { statElement.MetricElement.ObjectType }; //stat object type

            timeInterval interval = new timeInterval();
            interval.intervalType = statElement.MetricElement.TimeIntervalElement.TimeIntervalType;
            interval.length = statElement.MetricElement.TimeIntervalElement.Length;
            interval.slideLength = statElement.MetricElement.TimeIntervalElement.SlideLength;
            interval.timeProfileName = statElement.MetricElement.TimeIntervalElement.TimeProfileName;
            stat.metric.interval = interval;
            stat.metric.typeName = statElement.MetricElement.MetricTypeName;

            stat.schedule.notificationMode = statElement.ScheduleElement.NotificationMode;
            stat.schedule.insensitivity = statElement.ScheduleElement.Insensitivity;
            stat.schedule.timeout = statElement.ScheduleElement.Timeout;

            stat.objectId = new objectIdType();
            stat.objectId.id = statElement.ObjectIdTypeElement.ObjectId;
            stat.objectId.tenantName = statElement.ObjectIdTypeElement.TenantName;

            stat.statisticId = statElement.Name;
            return stat;
        }

        #region L02 - Abandon calls
        /// <summary>
        /// Cantidad de llamadas abandonadas
        /// </summary>
        /// <returns></returns>        
        public void L02TotalCallsAbandoned()
        {
            StatisticElement statElement = StatisticsConfiguration.StatisticsSection.Properties[StatisticNames.L02TotalCallsAbandoned.ToString()];
            if (statElement.Enabled)
            {
                statistic statisticRequestObj = BuildStatistic(statElement);
                try
                {
                    retrieveStatisticResponse resp = statService.retrieveStatistic(statisticRequestObj, StatServiceName);
                    statisticResults[StatisticNames.L02TotalCallsAbandoned] = GetStatValue(StatisticNames.L02TotalCallsAbandoned, resp);
                    //Borrar si la subscripción a los eventos no funciona definitivamente
                    //RegisterSubscription(statisticRequestObj);
                }
                catch (Exception ex)
                {
                    if (ConnectionLost != null)
                        ConnectionLost(null, EventArgs.Empty);
                }
            }
        }

        private void RegisterSubscription(statistic statisticRequestObj)
        {
            retrieveStatisticResponse resp = statService.retrieveStatistic(statisticRequestObj, StatServiceName);

            //statService.subscribeStatistic(statisticRequestObj, StatServiceName, connection.UnsolicitedNotification);
        }

        #endregion

        private static double serviceLevel = 0d;

        private static double GetServiceLevel()
        {
            //Revisar estadisticas
            double res = 0d;
            double l10 = statisticResults[StatisticNames.L10TotalAnsweredCalls];
            double l02 = statisticResults[StatisticNames.L02TotalCallsAbandoned];
            double callsAb = statisticResults[StatisticNames.CallsAbandonedInThresholdSubscription];
            double callsAn = statisticResults[StatisticNames.CallsAnsweredInThresholdSubscription];
            if ((l10 + l02) > 0)
            {
                res = (callsAb + callsAn) / (l10 + l02);
            }
            return res;
        }


        #region L05 - Calls in Queue
        /// <summary>
        /// Cantidad de llamadas en cola
        /// </summary>
        /// <returns></returns>
        public void L05CurrentCallsInQueue()
        {
            StatisticElement statElement = StatisticsConfiguration.StatisticsSection.Properties[StatisticNames.L05CurrentCallsInQueue.ToString()];
            if (statElement.Enabled)
            {
                statistic statisticRequestObj = BuildStatistic(statElement);
                try
                {
                    retrieveStatisticResponse resp = statService.retrieveStatistic(statisticRequestObj, StatServiceName);
                    statisticResults[StatisticNames.L05CurrentCallsInQueue] = GetStatValue(StatisticNames.L05CurrentCallsInQueue, resp);
                    //Borrar si la subscripción a los eventos no funciona definitivamente
                    //RegisterSubscription(statisticRequestObj);
                }
                catch (Exception ex)
                {
                    if (ConnectionLost != null)
                        ConnectionLost(null, EventArgs.Empty);
                }
            }
        }
        #endregion

        #region L06 - Max call waiting time in queue
        /// <summary>
        /// Duración máxima de una llamada en cola
        /// </summary>
        /// <returns></returns>

        private DateTime L06FirstDateTime = DateTime.MinValue;

        public void L06CurrMaxCallWaitingTime(DateTime date)
        {
            StatisticElement statElement = StatisticsConfiguration.StatisticsSection.Properties[StatisticNames.L06CurrMaxCallWaitingTime.ToString()];
            if (statElement.Enabled)
            {
                statistic statisticRequestObj = BuildStatistic(statElement);
                try
                {
                    if (L06FirstDateTime == DateTime.MinValue || L06FirstDateTime.Day != date.Day)
                    {
                        L06FirstDateTime = date;
                        statisticResults[StatisticNames.L06CurrMaxCallWaitingTime] = 0d;
                    }
                    retrieveStatisticResponse resp = statService.retrieveStatistic(statisticRequestObj, StatServiceName);
                    double value = GetStatValue(StatisticNames.L06CurrMaxCallWaitingTime, resp);
                    if (statisticResults[StatisticNames.L06CurrMaxCallWaitingTime] < value)
                    {
                        statisticResults[StatisticNames.L06CurrMaxCallWaitingTime] = value;
                    }
                    //Borrar si la subscripción a los eventos no funciona definitivamente
                    //RegisterSubscription(statisticRequestObj);
                }
                catch (Exception ex)
                {
                    if (ConnectionLost != null)
                        ConnectionLost(null, EventArgs.Empty);
                }
            }
        }
        #endregion

        #region L09 - Received calls
        /// <summary>
        /// Cantidad de llamadas recibidas
        /// </summary>
        /// <returns></returns>
        public void L09TotalCallsEntered()
        {
            StatisticElement statElement = StatisticsConfiguration.StatisticsSection.Properties[StatisticNames.L09TotalCallsEntered.ToString()];
            if (statElement.Enabled)
            {
                statistic statisticRequestObj = BuildStatistic(statElement);
                try
                {
                    retrieveStatisticResponse resp = statService.retrieveStatistic(statisticRequestObj, StatServiceName);
                    statisticResults[StatisticNames.L09TotalCallsEntered] = GetStatValue(StatisticNames.L09TotalCallsEntered, resp);
                    //Borrar si la subscripción a los eventos no funciona definitivamente
                    //RegisterSubscription(statisticRequestObj);
                }
                catch (Exception ex)
                {
                    if (ConnectionLost != null)
                        ConnectionLost(null, EventArgs.Empty);
                }
            }
        }
        #endregion

        #region L10 - Answered call number
        /// <summary>
        /// Cantidad de llamadas atendidas (contestadas)
        /// </summary>
        /// <returns></returns>
        public void L10TotalAnsweredCalls()
        {
            StatisticElement statElement = StatisticsConfiguration.StatisticsSection.Properties[StatisticNames.L10TotalAnsweredCalls.ToString()];
            if (statElement.Enabled)
            {
                statistic statisticRequestObj = BuildStatistic(statElement);
                try
                {
                    retrieveStatisticResponse resp = statService.retrieveStatistic(statisticRequestObj, StatServiceName);
                    statisticResults[StatisticNames.L10TotalAnsweredCalls] = GetStatValue(StatisticNames.L10TotalAnsweredCalls, resp);
                    //Borrar si la subscripción a los eventos no funciona definitivamente
                    //RegisterSubscription(statisticRequestObj);
                }
                catch (Exception ex)
                {
                    if (ConnectionLost != null)
                        ConnectionLost(null, EventArgs.Empty);
                }
            }
        }
        #endregion

        #region L11 - Received calls not answered
        /// <summary>
        /// Cantidad de llamadas recibidas no contestadas
        /// </summary>
        /// <returns></returns>
        public void L11TotalCallsAbandonedWR()
        {
            StatisticElement statElement = StatisticsConfiguration.StatisticsSection.Properties[StatisticNames.L11TotalCallsAbandonedWR.ToString()];
            if (statElement.Enabled)
            {
                statistic statisticRequestObj = BuildStatistic(statElement);
                try
                {
                    retrieveStatisticResponse resp = statService.retrieveStatistic(statisticRequestObj, StatServiceName);
                    statisticResults[StatisticNames.L11TotalCallsAbandonedWR] = GetStatValue(StatisticNames.L11TotalCallsAbandonedWR, resp);
                    //Borrar si la subscripción a los eventos no funciona definitivamente
                    //RegisterSubscription(statisticRequestObj);
                }
                catch (Exception ex)
                {
                    if (ConnectionLost != null)
                        ConnectionLost(null, EventArgs.Empty);
                }
            }
        }
        #endregion

        #region L12 - Nivel de servicio
        /// <summary>
        /// Nivel de servicio
        /// </summary>
        /// <returns></returns>
        public void L12ServiceFactor()
        {
            StatisticElement statElement = StatisticsConfiguration.StatisticsSection.Properties[StatisticNames.L12ServiceLevel.ToString()];
            if (statElement.Enabled)
            {
                //statistic statisticRequestObj = BuildStatistic(statElement);
                try
                {
                    //L10TotalAnsweredCalls();
                    //L11TotalCallsAbandonedWR();
                    //LCallsAbandonedInThresholdCalculate();
                    //LCallsAnsweredInThresholdCalculate();
                    serviceLevel = GetServiceLevel();
                    statisticResults[StatisticNames.L12ServiceLevel] = serviceLevel;
                    //Borrar si la subscripción a los eventos no funciona definitivamente
                    //RegisterSubscription(statisticRequestObj);
                }
                catch (Exception ex)
                {
                    if (ConnectionLost != null)
                        ConnectionLost(null, EventArgs.Empty);
                }
            }
        }
        #endregion

        public void LCallsAbandonedInThresholdCalculate()
        {
            StatisticElement statElement = StatisticsConfiguration.StatisticsSection.Properties[StatisticNames.CallsAbandonedInThresholdSubscription.ToString()];
            if (statElement.Enabled)
            {
                statistic statisticRequestObj = BuildStatistic(statElement);
                try
                {
                    retrieveStatisticResponse resp = statService.retrieveStatistic(statisticRequestObj, StatServiceName);
                    statisticResults[StatisticNames.CallsAbandonedInThresholdSubscription] = GetStatValue(StatisticNames.CallsAbandonedInThresholdSubscription, resp);
                    //Borrar si la subscripción a los eventos no funciona definitivamente
                    //RegisterSubscription(statisticRequestObj);
                }
                catch (Exception ex)
                {
                    if (ConnectionLost != null)
                        ConnectionLost(null, EventArgs.Empty);
                }
            }
        }

        public void LCallsAnsweredInThresholdCalculate()
        {
            StatisticElement statElement =  StatisticsConfiguration.StatisticsSection.Properties[StatisticNames.CallsAnsweredInThresholdSubscription.ToString()];
            if (statElement.Enabled)
            {
                statistic statisticRequestObj = BuildStatistic(statElement);
                try
                {
                    retrieveStatisticResponse resp = statService.retrieveStatistic(statisticRequestObj, StatServiceName);
                    statisticResults[StatisticNames.CallsAnsweredInThresholdSubscription] = GetStatValue(StatisticNames.CallsAnsweredInThresholdSubscription, resp);
                    //Borrar si la subscripción a los eventos no funciona definitivamente
                    //RegisterSubscription(statisticRequestObj);
                }
                catch (Exception ex)
                {
                    if (ConnectionLost != null)
                        ConnectionLost(null, EventArgs.Empty);
                }
            }
        }

        #region IConnectionNotification Members

        public void OnStatisticEvent(string eventType, string[][] keyValue)
        {
            if (StatisticReceived != null)
            {
                StatisticReceived(null, new StatisticReceivedEventArgs(eventType, keyValue));
            }
        }

        #endregion
    }        
}
