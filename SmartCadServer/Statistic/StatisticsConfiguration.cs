﻿using log4net.Config;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadServer.Statistic
{
    #region Class StatisticsConfiguration Documentation
    /// <summary>
    /// Clase estatica para leer la configuracion y datos de las estadísticas.
    /// </summary>
    /// <className>StatisticsConfiguration</className>
    /// <author email="jynojosa@smartmatic.com">Julio Ynojosa</author>
    /// <date>2009/06/29</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    public class StatisticsConfiguration
    {
#if (DEBUG)
        public const string ConfigFilename = "../../Dist/Statistics/Configuration.xml";
#else
            public const string ConfigFilename = "../Statistics/Configuration.xml";
#endif

        private static System.Configuration.Configuration Configuration;

        public StatisticsConfiguration()
        { }

        public static void Load()
        {
            Load(ConfigFilename);
        }

        public static void Load(string path)
        {
            ExeConfigurationFileMap exeConfigurationFileMap = new ExeConfigurationFileMap();

            exeConfigurationFileMap.ExeConfigFilename = path;

            Configuration =
                ConfigurationManager.OpenMappedExeConfiguration(exeConfigurationFileMap,
                ConfigurationUserLevel.None);

            //log4net configuration
            XmlConfigurator.ConfigureAndWatch(new FileInfo(Configuration.FilePath));
            Save();
        }

        public static void Save()
        {
            Configuration.Save();
        }

        public static StatisticsSection StatisticsSection
        {
            get
            {
                StatisticsSection statisticConfigurationSection = Configuration.GetSection("statistics")
                    as StatisticsSection;

                return statisticConfigurationSection;
            }
        }

    }
}
