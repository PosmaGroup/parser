﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadServer.Statistic
{
    public class ObjectIdTypeElement : ConfigurationElement
    {
        private objectIdType objectIdType;

        [ConfigurationProperty("object-id", IsRequired = true)]
        public string ObjectId
        {
            get
            {
                return (string)this["object-id"];
            }
            set
            {
                this["object-id"] = value;
            }
        }

        [ConfigurationProperty("tenant-name", IsRequired = true)]
        public string TenantName
        {
            get
            {
                return (string)this["tenant-name"];
            }
            set
            {
                this["tenant-name"] = value;
            }
        }

        public objectIdType ObjectIdType
        {
            get
            {
                if (objectIdType == null)
                {
                    objectIdType = new objectIdType();
                    objectIdType.id = this.ObjectId;
                    objectIdType.tenantName = this.TenantName;
                }
                return objectIdType;
            }
        }
    }
}
