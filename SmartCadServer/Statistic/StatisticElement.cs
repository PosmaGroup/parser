﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadServer.Statistic
{
    public class StatisticElement : ConfigurationElement
    {
        public StatisticElement()
        { }

        [ConfigurationProperty("name", IsRequired = true, IsKey = true)]
        public string Name
        {
            get
            {
                return this["name"] as string;
            }
            set
            {
                this["name"] = value;
            }
        }

        [ConfigurationProperty("enabled", IsRequired = true)]
        public bool Enabled
        {
            get
            {
                return (bool)this["enabled"];
            }
            set
            {
                this["enabled"] = value;
            }
        }

        [ConfigurationProperty("metric", IsRequired = true)]
        public MetricElement MetricElement
        {
            get
            {
                return this["metric"] as MetricElement;
            }
            set
            {
                this["metric"] = value;
            }
        }

        [ConfigurationProperty("object-id-type", IsRequired = true)]
        public ObjectIdTypeElement ObjectIdTypeElement
        {
            get
            {
                return this["object-id-type"] as ObjectIdTypeElement;
            }
            set
            {
                this["object-id-type"] = value;
            }
        }

        [ConfigurationProperty("schedule", IsRequired = true)]
        public ScheduleElement ScheduleElement
        {
            get
            {
                return this["schedule"] as ScheduleElement;
            }
            set
            {
                this["schedule"] = value;
            }
        }
    }
}
