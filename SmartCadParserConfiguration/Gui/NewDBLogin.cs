using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Reflection;
using System.Data.SqlClient;
using SmartCadCore.Common;
using SmartCadParserConfiguration.Controls;

namespace SmartCadParserConfiguration.Gui
{
	/// <summary>
	/// Summary description for NewDBLogin.
	/// </summary>
	public class NewDBLogin : DevExpress.XtraEditors.XtraForm
	{
        private DevExpress.XtraEditors.TextEdit textBoxPassword;
        private DevExpress.XtraEditors.TextEdit textBoxLogin;
        private DevExpress.XtraEditors.LabelControl labelPassword;
        private DevExpress.XtraEditors.LabelControl labelExLogin;
        private SimpleButtonEx buttonOk;
        private SimpleButtonEx buttonCancel;
        private Label label1;
        private Separator separator1;
        private DevExpress.XtraEditors.TextEdit textBoxServer;
		private DevExpress.XtraEditors.LabelControl labelServer;
        private DevExpress.XtraEditors.CheckEdit checkBoxExWindowsAuthentication;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private new System.ComponentModel.Container components = null;

		public NewDBLogin()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			
            //
			// TODO: Add any constructor code after InitializeComponent call
			//
            LoadLanguage();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.textBoxPassword = new DevExpress.XtraEditors.TextEdit();
            this.textBoxLogin = new DevExpress.XtraEditors.TextEdit();
            this.labelPassword = new DevExpress.XtraEditors.LabelControl();
            this.labelExLogin = new DevExpress.XtraEditors.LabelControl();
            this.buttonOk = new SimpleButtonEx();
            this.buttonCancel = new SimpleButtonEx();
            this.label1 = new System.Windows.Forms.Label();
            this.separator1 = new Separator();
            this.textBoxServer = new DevExpress.XtraEditors.TextEdit();
            this.labelServer = new DevExpress.XtraEditors.LabelControl();
            this.checkBoxExWindowsAuthentication = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxPassword.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxLogin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxServer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxExWindowsAuthentication.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.AllowDrop = true;
            this.textBoxPassword.Location = new System.Drawing.Point(92, 131);
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBoxPassword.Properties.PasswordChar = '*';
            this.textBoxPassword.Size = new System.Drawing.Size(176, 20);
            this.textBoxPassword.TabIndex = 4;
            // 
            // textBoxLogin
            // 
            this.textBoxLogin.Location = new System.Drawing.Point(92, 97);
            this.textBoxLogin.Name = "textBoxLogin";
            this.textBoxLogin.Size = new System.Drawing.Size(176, 20);
            this.textBoxLogin.TabIndex = 3;
            this.textBoxLogin.TextChanged += new System.EventHandler(this.ButtonAcceptActivation);
            // 
            // labelPassword
            // 
            this.labelPassword.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPassword.Location = new System.Drawing.Point(12, 131);
            this.labelPassword.Name = "labelPassword";
            this.labelPassword.Size = new System.Drawing.Size(0, 13);
            this.labelPassword.TabIndex = 11;
            // 
            // labelExLogin
            // 
            this.labelExLogin.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExLogin.Location = new System.Drawing.Point(12, 97);
            this.labelExLogin.Name = "labelExLogin";
            this.labelExLogin.Size = new System.Drawing.Size(0, 13);
            this.labelExLogin.TabIndex = 10;
            // 
            // buttonOk
            // 
            this.buttonOk.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOk.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonOk.Appearance.Options.UseFont = true;
            this.buttonOk.Appearance.Options.UseForeColor = true;
            this.buttonOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOk.Enabled = false;
            this.buttonOk.FontFormat = FontFormat.GetFormat("ButtonFormat");
            this.buttonOk.Location = new System.Drawing.Point(92, 160);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(80, 25);
            this.buttonOk.TabIndex = 5;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCancel.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonCancel.Appearance.Options.UseFont = true;
            this.buttonCancel.Appearance.Options.UseForeColor = true;
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.FontFormat = FontFormat.GetFormat("ButtonFormat");
            this.buttonCancel.Location = new System.Drawing.Point(190, 160);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(78, 25);
            this.buttonCancel.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(292, 34);
            this.label1.TabIndex = 15;
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // separator1
            // 
            this.separator1.BackColor = System.Drawing.Color.DarkGray;
            this.separator1.Dock = System.Windows.Forms.DockStyle.Top;
            this.separator1.Location = new System.Drawing.Point(0, 34);
            this.separator1.Name = "separator1";
            this.separator1.Size = new System.Drawing.Size(292, 2);
            this.separator1.TabIndex = 16;
            this.separator1.TransparentColor = System.Drawing.Color.WhiteSmoke;
            // 
            // textBoxServer
            // 
            this.textBoxServer.Location = new System.Drawing.Point(92, 43);
            this.textBoxServer.Name = "textBoxServer";
            this.textBoxServer.Size = new System.Drawing.Size(176, 20);
            this.textBoxServer.TabIndex = 1;
            this.textBoxServer.TextChanged += new System.EventHandler(this.ButtonAcceptActivation);
            // 
            // labelServer
            // 
            this.labelServer.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelServer.Location = new System.Drawing.Point(12, 45);
            this.labelServer.Name = "labelServer";
            this.labelServer.Size = new System.Drawing.Size(0, 13);
            this.labelServer.TabIndex = 20;
            // 
            // checkBoxExWindowsAuthentication
            // 
            this.checkBoxExWindowsAuthentication.Location = new System.Drawing.Point(92, 71);
            this.checkBoxExWindowsAuthentication.Name = "checkBoxExWindowsAuthentication";
            this.checkBoxExWindowsAuthentication.Properties.Caption = "";
            this.checkBoxExWindowsAuthentication.Size = new System.Drawing.Size(137, 19);
            this.checkBoxExWindowsAuthentication.TabIndex = 2;
            this.checkBoxExWindowsAuthentication.CheckedChanged += new System.EventHandler(this.checkBoxExWindowsAuthentication_CheckedChanged);
            // 
            // NewDBLogin
            // 
            this.AcceptButton = this.buttonOk;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.ClientSize = new System.Drawing.Size(292, 194);
            this.Controls.Add(this.checkBoxExWindowsAuthentication);
            this.Controls.Add(this.textBoxServer);
            this.Controls.Add(this.textBoxPassword);
            this.Controls.Add(this.textBoxLogin);
            this.Controls.Add(this.labelServer);
            this.Controls.Add(this.separator1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.labelPassword);
            this.Controls.Add(this.labelExLogin);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.LookAndFeel.SkinName = "Blue";
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "NewDBLogin";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)(this.textBoxPassword.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxLogin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxServer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxExWindowsAuthentication.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion
        
        private void LoadLanguage()
        {
            this.Text = ResourceLoader.GetString2("DatabaseWizardCreation");
            this.label1.Text = ResourceLoader.GetString2("DatabaseCreationStepOne");
            this.labelServer.Text = ResourceLoader.GetString2("ServerTwoPoints");
            this.checkBoxExWindowsAuthentication.Properties.Caption = 
                ResourceLoader.GetString2("WindowsAuthentication");
            this.labelExLogin.Text = ResourceLoader.GetString2("LoginTwoPoints");
            this.labelPassword.Text = ResourceLoader.GetString2("PasswordTwoPoints");
            this.buttonOk.Text = ResourceLoader.GetString2("Next");
            this.buttonCancel.Text = ResourceLoader.GetString2("ProfileFormEditButtonCancelText");
        }

		private void ButtonAcceptActivation (object sender, System.EventArgs e)
		{
			if (textBoxServer.Text == "" || 
                (textBoxLogin.Text == "" && checkBoxExWindowsAuthentication.Checked == false))
				buttonOk.Enabled =  false;
			else
				buttonOk.Enabled = true;
		}

		private void buttonOk_Click(object sender, System.EventArgs e)
		{
			this.server = this.textBoxServer.Text.Trim();
            this.login = this.textBoxLogin.Text.Trim();
            this.password = "Abcd1234"; //this.textBoxPassword.Text.Trim();
    
			try
			{
				BackgroundProcessForm processForm = new BackgroundProcessForm(this,new MethodInfo[1]{GetType().GetMethod("testConnection",BindingFlags.NonPublic | BindingFlags.Instance)},new object[1][]{new object[0]});
				processForm.CanThrowError = true;
				processForm.ShowDialog();	
			}
			catch(Exception ex)
			{
                MessageForm.Show("CantEstablishConnectionWihtDB", ex);
				DialogResult = DialogResult.None;
			}
		}

		private void testConnection()
		{			
			string connectionString = this.GetConnectionString();	
			connection = new SqlConnection(connectionString);
			connection.Open();
		}

		private string GetConnectionString()
		{
			string server = "Data Source=";
			string catalog = ";Initial Catalog=";			
			string user = ";uid=";
			string pwd = ";pwd=";
            string windows = ";Trusted_Connection=True;";
			
			server       += this.server;
			catalog      += "master";
			user         += this.login;
			pwd          += this.password;

            string result = "";

            if (checkBoxExWindowsAuthentication.Checked == true)
                result = server + catalog + windows;
            else
                result = server + catalog + user + pwd;

            return result;
		}

        private string server = "";
        private string login = "";
        private string password = "";
        public SqlConnection connection = null;

        public string Server
        {
            get
            {
                return textBoxServer.Text.Trim();
            }
        }

        public string Login
        {
            get
            {
                return textBoxLogin.Text.Trim();
            }
        }

        public string Password
        {
            get
            {
                return textBoxPassword.Text.Trim();
            }
        }

        private void checkBoxExWindowsAuthentication_CheckedChanged(object sender, EventArgs e)
        {
            labelExLogin.Enabled = !checkBoxExWindowsAuthentication.Checked;
            labelPassword.Enabled = !checkBoxExWindowsAuthentication.Checked;
            textBoxLogin.Enabled = !checkBoxExWindowsAuthentication.Checked;
            textBoxPassword.Enabled = !checkBoxExWindowsAuthentication.Checked;
            ButtonAcceptActivation(sender, e);
        }
	}
}
