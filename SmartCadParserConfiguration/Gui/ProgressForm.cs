using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Reflection;
using System.Threading;
using SmartCadCore.Core;
using SmartCadCore.Common;
using SmartCadParserConfiguration.Controls;

namespace SmartCadParserConfiguration.Gui
{
	/// <summary>
	/// Summary description for ProgressForm.
	/// </summary>
	public class ProgressForm : DevExpress.XtraEditors.XtraForm
	{
		private bool canThrowError = false;
		private bool canShowError = true;
		private bool showError = false;
		private Thread backgroundThread;
		private Exception error = null;
		private object[] arguments = null;
		private LabelEx labelCurrentTask;
		private System.Windows.Forms.ProgressBar progressBar;
		private TaskFunctionsDelegate myFunction;
		private DevExpress.XtraEditors.SimpleButton simpleCancel;
		private DevExpress.XtraLayout.LayoutControl layoutControl1;
		private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
		private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
		private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private new System.ComponentModel.Container components = null;		

		public ProgressForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		private void simpleCancel_Click(object sender, EventArgs e)
		{
			backgroundThread.Suspend();
			BeginInvoke(new MethodInvoker(Close));
		}

		public DevExpress.XtraLayout.Utils.LayoutVisibility ButtonCancelVisibility
		{
			get
			{
				return layoutControlItem3.Visibility;
			}
			set
			{
				layoutControlItem3.Visibility = value;
				emptySpaceItem1.Visibility = value;
				emptySpaceItem2.Visibility = value;
			}
		}

		public int Max
		{
			set
			{
				progressBar.Maximum = value;
			}
			get
			{
				return progressBar.Maximum;
			}
		}

		public int Min
		{
			set
			{
				progressBar.Minimum = value;
			}
			get
			{
				return progressBar.Minimum;
			}
		}

		public int CurrentValue
		{
			set
			{
				progressBar.Value = value;
			}
			get
			{
				return progressBar.Value;
			}

		}

		public string CurrentTask
		{
			set
			{
				labelCurrentTask.Text = value;
			}
			get
			{
				return labelCurrentTask.Text;
			}
		}

		public ProgressForm (TaskFunctionsDelegate myFunctionI, object [] argumentsI, string windowsText)
		{
			InitializeComponent();
			arguments = argumentsI;
			myFunction = myFunctionI;
			Text = windowsText;
		}

		private void ProgressForm_Load(object sender, System.EventArgs e)
		{
			LoadLanguage();
		}

		private void LoadLanguage()
		{
			simpleCancel.Text = ResourceLoader.GetString2("Cancel");
		}

		protected override void OnVisibleChanged( EventArgs e )
		{
			base.OnVisibleChanged( e );
			if(Visible==true)
			{
				backgroundThread = new Thread(new ThreadStart(backgroundThreadMethod));
				backgroundThread.IsBackground = true;
				backgroundThread.Start();
			}
		}

		private void backgroundThreadMethod()
		{
			try 
			{
			 ChangeTaskDelegate TaskFunction = new ChangeTaskDelegate (ChangeText);
			 ChangeValueDelegate ValueFunction = new ChangeValueDelegate (ChangeValue);

			 myFunction (TaskFunction, ValueFunction, arguments);			
			}
			catch(Exception ex)
			{
				if(ex.InnerException!=null)
					Error = ex.InnerException;
				else
					Error = ex;
			}
			finally
			{
				BeginInvoke(new MethodInvoker(Close));
			}
		}

		private bool validClose = false;
		private new void Close()
		{
			validClose = true;
			base.Close();
		}
		protected override void OnClosing( CancelEventArgs e )
		{
			e.Cancel = (!validClose);
			base.OnClosing(e);
		}
		protected override void OnClosed( EventArgs e )
		{
			base.OnClosed(e);
		}
		public new void ShowDialog()
		{
			base.ShowDialog();
			if(showError && canShowError)
			{
				Opacity = 0;
				MessageForm.Show(error.Message,error);
			}
			else if(showError && canThrowError)
			{
				Opacity = 0;
				throw error;
			}
		}
	
		private Exception Error
		{
			set
			{
				error = value;
				showError = true;
			}
		}
		public bool WasAnError
		{
			get{ return showError;}
		}

		public bool CanShowError
		{
			get{return canShowError;}
			set
			{
				canShowError=value;
				if(canShowError)
					canThrowError = false;
			}
		}
		public bool CanThrowError
		{
			get{return canThrowError;}
			set
			{
				canThrowError=value;
				if(canThrowError)
					canShowError = false;
			}
		}
		
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		private void Change_Text (string Task)
		{
			CurrentTask = Task;
		}

		public void ChangeText (string Task)
		{
			if (InvokeRequired)
				BeginInvoke (new  ChangeTaskDelegate (Change_Text), new object[1]{Task});
			else
				Change_Text (Task);
		}

		private void Change_Value (int Value)
		{
			CurrentValue = Value;
		}

		public void ChangeValue (int Value)
		{
			if (InvokeRequired)
				BeginInvoke (new ChangeValueDelegate (Change_Value), new object[1]{Value});
			else
				Change_Value (Value);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.labelCurrentTask = new LabelEx();
            this.simpleCancel = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(12, 12);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(275, 21);
            this.progressBar.TabIndex = 0;
            // 
            // labelCurrentTask
            // 
            this.labelCurrentTask.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCurrentTask.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelCurrentTask.Location = new System.Drawing.Point(12, 37);
            this.labelCurrentTask.Name = "labelCurrentTask";
            this.labelCurrentTask.Size = new System.Drawing.Size(275, 22);
            this.labelCurrentTask.TabIndex = 1;
            // 
            // simpleCancel
            // 
            this.simpleCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleCancel.Location = new System.Drawing.Point(118, 63);
            this.simpleCancel.Name = "simpleCancel";
            this.simpleCancel.Size = new System.Drawing.Size(76, 22);
            this.simpleCancel.StyleController = this.layoutControl1;
            this.simpleCancel.TabIndex = 2;
            this.simpleCancel.Text = "simpleCancel";
            this.simpleCancel.Visible = false;
            this.simpleCancel.Click += new System.EventHandler(this.simpleCancel_Click);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.progressBar);
            this.layoutControl1.Controls.Add(this.simpleCancel);
            this.layoutControl1.Controls.Add(this.labelCurrentTask);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(299, 97);
            this.layoutControl1.TabIndex = 3;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.emptySpaceItem2,
            this.emptySpaceItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(299, 97);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.progressBar;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(279, 25);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.labelCurrentTask;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 25);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(279, 26);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.simpleCancel;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(106, 51);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(80, 26);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            this.layoutControlItem3.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 51);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(106, 26);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            this.emptySpaceItem2.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(186, 51);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(93, 26);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            this.emptySpaceItem1.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // ProgressForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.ClientSize = new System.Drawing.Size(299, 97);
            this.Controls.Add(this.layoutControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.LookAndFeel.SkinName = "Blue";
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ProgressForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.TopMost = true;
            this.Load += new System.EventHandler(this.ProgressForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion			
	}

    public delegate void TaskFunctionsDelegate(ChangeTaskDelegate ChangeTaskFunction, ChangeValueDelegate ChangeValueFunction, object[] arguments);
}
