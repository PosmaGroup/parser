using SmartCadParserConfiguration.Controls;
namespace SmartCadParserConfiguration
{
    partial class ConnectionParserConfigurationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private new System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConnectionParserConfigurationForm));
            this.buttonExCancel = new SimpleButtonEx();
            this.tabControl = new DevExpress.XtraTab.XtraTabControl();
            this.tabPageConnection = new DevExpress.XtraTab.XtraTabPage();
            this.devicesConfigurationControl1 = new SmartCadParserConfiguration.Controls.DevicesConfigurationControl();
            this.groupBox1 = new DevExpress.XtraEditors.GroupControl();
            this.textBoxExRate = new Smartmatic.SmartCad.Controls.TextBoxEx();
            this.labelEx2 = new DevExpress.XtraEditors.LabelControl();
            this.textBoxExSendPort = new Smartmatic.SmartCad.Controls.TextBoxEx();
            this.labelEx1 = new DevExpress.XtraEditors.LabelControl();
            this.textBoxExAppServer = new Smartmatic.SmartCad.Controls.TextBoxEx();
            this.label1 = new DevExpress.XtraEditors.LabelControl();
            this.dbConnectionControl = new SmartCadParserConfiguration.GPSParserDbConnectionControl();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.toolTipBoton = new System.Windows.Forms.ToolTip(this.components);
            this.buttonExAccept = new SimpleButtonEx();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl)).BeginInit();
            this.tabControl.SuspendLayout();
            this.tabPageConnection.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonExCancel
            // 
            this.buttonExCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.buttonExCancel.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExCancel.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonExCancel.Appearance.Options.UseFont = true;
            this.buttonExCancel.Appearance.Options.UseForeColor = true;
            this.buttonExCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonExCancel.FontFormat = FontFormat.GetFormat("ButtonFormat");
            this.buttonExCancel.Location = new System.Drawing.Point(319, 473);
            this.buttonExCancel.Name = "buttonExCancel";
            this.buttonExCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonExCancel.TabIndex = 1;
            this.buttonExCancel.Text = "Cancelar";
            // 
            // tabControl
            // 
            this.tabControl.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabControl.Location = new System.Drawing.Point(5, 5);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedTabPage = this.tabPageConnection;
            this.tabControl.Size = new System.Drawing.Size(392, 462);
            this.tabControl.TabIndex = 0;
            this.tabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tabPageConnection});
            this.tabControl.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tabControl_KeyUp);
            this.tabControl.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tabControl_KeyDown);
            // 
            // tabPageConnection
            // 
            this.tabPageConnection.Controls.Add(this.devicesConfigurationControl1);
            this.tabPageConnection.Controls.Add(this.groupBox1);
            this.tabPageConnection.Controls.Add(this.dbConnectionControl);
            this.tabPageConnection.Name = "tabPageConnection";
            this.tabPageConnection.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageConnection.Size = new System.Drawing.Size(383, 432);
            this.tabPageConnection.Text = "Conexi�n";
            // 
            // devicesConfigurationControl1
            // 
            this.devicesConfigurationControl1.HasNeededFields = false;
            this.devicesConfigurationControl1.Location = new System.Drawing.Point(7, 93);
            this.devicesConfigurationControl1.Name = "devicesConfigurationControl1";
            this.devicesConfigurationControl1.Size = new System.Drawing.Size(369, 237);
            this.devicesConfigurationControl1.TabIndex = 3;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBoxExRate);
            this.groupBox1.Controls.Add(this.labelEx2);
            this.groupBox1.Controls.Add(this.textBoxExSendPort);
            this.groupBox1.Controls.Add(this.labelEx1);
            this.groupBox1.Controls.Add(this.textBoxExAppServer);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(7, 336);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(369, 87);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.Text = "Transmisi�n de tramas";
            // 
            // textBoxExRate
            // 
            this.textBoxExRate.AllowsLetters = false;
            this.textBoxExRate.AllowsNumbers = true;
            this.textBoxExRate.AllowsPunctuation = false;
            this.textBoxExRate.AllowsSeparators = false;
            this.textBoxExRate.AllowsSymbols = false;
            this.textBoxExRate.AllowsWhiteSpaces = false;
            this.textBoxExRate.ExtraAllowedChars = "";
            this.textBoxExRate.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExRate.Location = new System.Drawing.Point(97, 58);
            this.textBoxExRate.MaxLength = 3;
            this.textBoxExRate.Name = "textBoxExRate";
            this.textBoxExRate.NonAllowedCharacters = " \"qwertyuiopasdfghjklzxcvbnm,.-�{�+�\'|*_:[�*�/!QWERTYUIOPASDFGHJKLZXCVBNM";
            this.textBoxExRate.RegularExpresion = "";
            this.textBoxExRate.Size = new System.Drawing.Size(60, 20);
            this.textBoxExRate.TabIndex = 3;
            this.textBoxExRate.TextChanged += new System.EventHandler(this.textBoxExRate_TextChanged);
            // 
            // labelEx2
            // 
            this.labelEx2.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEx2.Appearance.Options.UseFont = true;
            this.labelEx2.Location = new System.Drawing.Point(8, 61);
            this.labelEx2.Name = "labelEx2";
            this.labelEx2.Size = new System.Drawing.Size(78, 13);
            this.labelEx2.TabIndex = 2;
            this.labelEx2.Text = "Frecuencia [ms]:";
            // 
            // textBoxExSendPort
            // 
            this.textBoxExSendPort.AllowsLetters = false;
            this.textBoxExSendPort.AllowsNumbers = true;
            this.textBoxExSendPort.AllowsPunctuation = false;
            this.textBoxExSendPort.AllowsSeparators = false;
            this.textBoxExSendPort.AllowsSymbols = false;
            this.textBoxExSendPort.AllowsWhiteSpaces = false;
            this.textBoxExSendPort.ExtraAllowedChars = "";
            this.textBoxExSendPort.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExSendPort.Location = new System.Drawing.Point(296, 58);
            this.textBoxExSendPort.MaxLength = 6;
            this.textBoxExSendPort.Name = "textBoxExSendPort";
            this.textBoxExSendPort.NonAllowedCharacters = " \"qwertyuiopasdfghjklzxcvbnm,.-�{�+�\'|*_:[�*�/!QWERTYUIOPASDFGHJKLZXCVBNM";
            this.textBoxExSendPort.RegularExpresion = "";
            this.textBoxExSendPort.Size = new System.Drawing.Size(61, 20);
            this.textBoxExSendPort.TabIndex = 5;
            // 
            // labelEx1
            // 
            this.labelEx1.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEx1.Appearance.Options.UseFont = true;
            this.labelEx1.Location = new System.Drawing.Point(239, 61);
            this.labelEx1.Name = "labelEx1";
            this.labelEx1.Size = new System.Drawing.Size(34, 13);
            this.labelEx1.TabIndex = 4;
            this.labelEx1.Text = "Puerto:";
            // 
            // textBoxExAppServer
            // 
            this.textBoxExAppServer.AllowsLetters = true;
            this.textBoxExAppServer.AllowsNumbers = true;
            this.textBoxExAppServer.AllowsPunctuation = true;
            this.textBoxExAppServer.AllowsSeparators = true;
            this.textBoxExAppServer.AllowsSymbols = true;
            this.textBoxExAppServer.AllowsWhiteSpaces = true;
            this.textBoxExAppServer.ExtraAllowedChars = "";
            this.textBoxExAppServer.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxExAppServer.Location = new System.Drawing.Point(97, 28);
            this.textBoxExAppServer.MaxLength = 63;
            this.textBoxExAppServer.Name = "textBoxExAppServer";
            this.textBoxExAppServer.NonAllowedCharacters = "\'";
            this.textBoxExAppServer.RegularExpresion = "";
            this.textBoxExAppServer.Size = new System.Drawing.Size(260, 20);
            this.textBoxExAppServer.TabIndex = 1;
            this.textBoxExAppServer.TextChanged += new System.EventHandler(this.textBoxExAppServer_TextChanged);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(8, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Servidor: ";
            // 
            // dbConnectionControl
            // 
            this.dbConnectionControl.DataBaseName = "";
            this.dbConnectionControl.DataBaseServer = "";
            this.dbConnectionControl.Location = new System.Drawing.Point(7, 6);
            this.dbConnectionControl.Name = "dbConnectionControl";
            this.dbConnectionControl.Size = new System.Drawing.Size(370, 81);
            this.dbConnectionControl.TabIndex = 0;
            // 
            // buttonExAccept
            // 
            this.buttonExAccept.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExAccept.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonExAccept.Appearance.Options.UseFont = true;
            this.buttonExAccept.Appearance.Options.UseForeColor = true;
            this.buttonExAccept.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonExAccept.FontFormat = FontFormat.GetFormat("ButtonFormat");
            this.buttonExAccept.Location = new System.Drawing.Point(238, 473);
            this.buttonExAccept.Name = "buttonExAccept";
            this.buttonExAccept.Size = new System.Drawing.Size(75, 23);
            this.buttonExAccept.TabIndex = 0;
            this.buttonExAccept.Text = "Accept";
            this.buttonExAccept.Click += new System.EventHandler(this.buttonExAccept_Click);
            // 
            // ConnectionParserConfigurationForm
            // 
            this.AcceptButton = this.buttonExAccept;
            this.TopMost = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonExCancel;
            this.ClientSize = new System.Drawing.Size(402, 503);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.buttonExCancel);
            this.Controls.Add(this.buttonExAccept);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.LookAndFeel.SkinName = "Blue";
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ConnectionParserConfigurationForm";
            this.Padding = new System.Windows.Forms.Padding(5);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.ConnectionParserConfigurationForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tabControl)).EndInit();
            this.tabControl.ResumeLayout(false);
            this.tabPageConnection.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private SimpleButtonEx buttonExCancel;
        private DevExpress.XtraTab.XtraTabControl tabControl;
        private DevExpress.XtraTab.XtraTabPage tabPageConnection;
        private GPSParserDbConnectionControl dbConnectionControl;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private DevExpress.XtraEditors.GroupControl groupBox1;
        private Smartmatic.SmartCad.Controls.TextBoxEx textBoxExAppServer;
        private DevExpress.XtraEditors.LabelControl label1;
        private Smartmatic.SmartCad.Controls.TextBoxEx textBoxExRate;
        private DevExpress.XtraEditors.LabelControl labelEx2;
        private Smartmatic.SmartCad.Controls.TextBoxEx textBoxExSendPort;
        private DevExpress.XtraEditors.LabelControl labelEx1;
        private System.Windows.Forms.ToolTip toolTipBoton;
        private SimpleButtonEx buttonExAccept;
        private SmartCadParserConfiguration.Controls.DevicesConfigurationControl devicesConfigurationControl1;
    }
    }
