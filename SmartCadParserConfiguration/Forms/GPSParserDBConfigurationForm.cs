using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Diagnostics;
using System.Reflection;
using System.Xml;
using System.Xml.Schema;
using System.Xml.XPath;
using SmartCadCore.Core;
using SmartCadParser;
using SmartCadCore.Common;
using SmartCadParserConfiguration.Controls;
using SmartCadParserConfiguration.Gui;

namespace SmartCadParserConfiguration
{
    public class GPSParserDBConfigurationForm : DevExpress.XtraEditors.XtraForm
    {
        private SimpleButtonEx buttonNewDB;
        private SimpleButtonEx buttonDeleteDB;
        private Separator separator1;
        private SimpleButtonEx buttonCancel;
        private LabelEx labelNewDB;
        private LabelEx labelDeleteDB;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private new System.ComponentModel.Container components = null;

#if (DEBUG)
        public const string CreateTableFilename = "C:/Users/Oswaldo Zarate/Documents/Proyectos/SmartCadDist/ParserDataBase/CreateTableSchema.sql";
        public const string CreateViewFilename = "C:/Users/Oswaldo Zarate/Documents/Proyectos/SmartCadDist/ParserDataBase/CreateViewSchema.sql";
        public const string DropFilename = "C:/Users/Oswaldo Zarate/Documents/Proyectos/SmartCadDist/ParserDataBase/DropSchema.sql";
#else
        public const string CreateTableFilename = "../DataBase/CreateTableSchema.sql";
        public const string CreateViewFilename = "../DataBase/CreateViewSchema.sql";
        public const string DropFilename = "../DataBase/DropSchema.sql";
#endif

        public GPSParserDBConfigurationForm(SqlConnection myConnection)
        {
            InitializeComponent();
            this.Icon = ResourceLoader.GetIcon("appIcon");
            this.labelNewDB.Text = ResourceLoader.GetString2("CreateDataBase");
            this.labelDeleteDB.Text = ResourceLoader.GetString2("DeleteDataBase");
            this.Text = ResourceLoader.GetString2("ConfigureDataBase");

            connection = myConnection;
            bool hasSBObjects = hasDataObjects();

            if (!hasSBObjects)
            {
                buttonNewDB.Enabled = true;
                buttonDeleteDB.Enabled = false;
            }
            else
            {
                buttonNewDB.Enabled = false;
                buttonDeleteDB.Enabled = true;
            }


        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GPSParserDBConfigurationForm));
            this.buttonNewDB = new SimpleButtonEx();
            this.buttonDeleteDB = new SimpleButtonEx();
            this.separator1 = new Separator();
            this.labelNewDB = new LabelEx();
            this.labelDeleteDB = new LabelEx();
            this.buttonCancel = new SimpleButtonEx();
            this.SuspendLayout();
            // 
            // buttonNewDB
            // 
            this.buttonNewDB.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNewDB.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonNewDB.Appearance.Options.UseFont = true;
            this.buttonNewDB.Appearance.Options.UseForeColor = true;
            this.buttonNewDB.FontFormat = FontFormat.GetFormat("ButtonFormat");
            this.buttonNewDB.Image = ((System.Drawing.Image)(resources.GetObject("buttonNewDB.Image")));
            this.buttonNewDB.Location = new System.Drawing.Point(16, 17);
            this.buttonNewDB.Name = "buttonNewDB";
            this.buttonNewDB.Size = new System.Drawing.Size(48, 52);
            this.buttonNewDB.TabIndex = 4;
            this.buttonNewDB.Click += new System.EventHandler(this.buttonNewDB_Click);
            // 
            // buttonDeleteDB
            // 
            this.buttonDeleteDB.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDeleteDB.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonDeleteDB.Appearance.Options.UseFont = true;
            this.buttonDeleteDB.Appearance.Options.UseForeColor = true;
            this.buttonDeleteDB.Enabled = false;
            this.buttonDeleteDB.FontFormat = FontFormat.GetFormat("ButtonFormat");
            this.buttonDeleteDB.Image = ((System.Drawing.Image)(resources.GetObject("buttonDeleteDB.Image")));
            this.buttonDeleteDB.Location = new System.Drawing.Point(16, 103);
            this.buttonDeleteDB.Name = "buttonDeleteDB";
            this.buttonDeleteDB.Size = new System.Drawing.Size(48, 52);
            this.buttonDeleteDB.TabIndex = 5;
            this.buttonDeleteDB.Click += new System.EventHandler(this.buttonDeleteDB_Click);
            // 
            // separator1
            // 
            this.separator1.BackColor = System.Drawing.Color.DarkGray;
            this.separator1.Location = new System.Drawing.Point(7, 86);
            this.separator1.Name = "separator1";
            this.separator1.Size = new System.Drawing.Size(257, 2);
            this.separator1.TabIndex = 6;
            this.separator1.TransparentColor = System.Drawing.Color.WhiteSmoke;
            // 
            // labelNewDB
            // 
            this.labelNewDB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNewDB.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelNewDB.Location = new System.Drawing.Point(80, 22);
            this.labelNewDB.Name = "labelNewDB";
            this.labelNewDB.Size = new System.Drawing.Size(184, 61);
            this.labelNewDB.TabIndex = 7;
            // 
            // labelDeleteDB
            // 
            this.labelDeleteDB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDeleteDB.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelDeleteDB.Location = new System.Drawing.Point(80, 108);
            this.labelDeleteDB.Name = "labelDeleteDB";
            this.labelDeleteDB.Size = new System.Drawing.Size(182, 58);
            this.labelDeleteDB.TabIndex = 8;
            // 
            // buttonCancel
            // 
            this.buttonCancel.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCancel.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonCancel.Appearance.Options.UseFont = true;
            this.buttonCancel.Appearance.Options.UseForeColor = true;
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.FontFormat = FontFormat.GetFormat("ButtonFormat");
            this.buttonCancel.Location = new System.Drawing.Point(192, 178);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(72, 25);
            this.buttonCancel.TabIndex = 30;
            this.buttonCancel.Text = "Cerrar";
            // 
            // GPSParserDBConfigurationForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(274, 212);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.labelDeleteDB);
            this.Controls.Add(this.labelNewDB);
            this.Controls.Add(this.separator1);
            this.Controls.Add(this.buttonNewDB);
            this.Controls.Add(this.buttonDeleteDB);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.LookAndFeel.SkinName = "Blue";
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "GPSParserDBConfigurationForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.TopMost = true;
            this.ResumeLayout(false);

        }
        #endregion


        private void CreateDataObjects(ChangeTaskDelegate changeTaskFunction, ChangeValueDelegate changeValueFunction, object[] arguments)
        {
            try
            {
                changeValueFunction(10);
                changeTaskFunction(ResourceLoader.GetString2("Iniciando servicio ..."));

                SqlConnection SqlConn = new SqlConnection(ParserConfiguration.DataBaseConnectionString);
                SqlConn.Open();

                changeValueFunction(50);
                changeTaskFunction(ResourceLoader.GetString2("Creando esquema de base de datos ..."));

                string SchemaQuery = File.ReadAllText(CreateTableFilename);                
                SqlCommand SqlCom = new SqlCommand(SchemaQuery, SqlConn);
                SqlCom.ExecuteNonQuery();

                changeValueFunction(70);

                try
                {
                    SchemaQuery = File.ReadAllText(CreateViewFilename);
                    SqlCom = new SqlCommand(SchemaQuery, SqlConn);
                    SqlCom.ExecuteNonQuery();
                }
                catch { }

                changeValueFunction(90);

                SqlConn.Close();
                SqlCom.Dispose();

                changeValueFunction(100);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void buttonNewDB_Click(object sender, System.EventArgs e)
        {
            try
            {
                ProgressForm progressForm = new ProgressForm(new TaskFunctionsDelegate(CreateDataObjects),
                    new object[0], ResourceLoader.GetString2("Configurando base de datos"));
                progressForm.CanThrowError = true;
                progressForm.ShowDialog();
                buttonNewDB.Enabled = false;
                buttonDeleteDB.Enabled = true;
                MessageForm.Show(ResourceLoader.GetString2("Repositorio de datos creado exitosamente."), MessageFormType.Information);
            }
            catch (Exception ex)
            {
                MessageForm.Show(ex.Message, ex);
            }
        }

        private void buttonDeleteDB_Click(object sender, System.EventArgs e)
        {
            try
            {
                ProgressForm processForm = new ProgressForm(new TaskFunctionsDelegate(DeleteDB),
                    new object[0], ResourceLoader.GetString2("Eliminando base de datos"));
                processForm.CanThrowError = true;
                processForm.ShowDialog();
                buttonNewDB.Enabled = true;
                buttonDeleteDB.Enabled = false;
                MessageForm.Show(ResourceLoader.GetString2("Repositorio de datos borrado exitosamente."), MessageFormType.Information);
            }
            catch (Exception ex)
            {
                MessageForm.Show(ex.Message, ex);
            }
        }

        private void DeleteDB(ChangeTaskDelegate changeTaskFunction, ChangeValueDelegate changeValueFunction, object[] arguments)
        {
            string DropQuery = File.ReadAllText(DropFilename);

            changeValueFunction(40);
            changeTaskFunction(ResourceLoader.GetString2("Iniciando servicio..."));

            SqlConnection SqlConn = new SqlConnection(ParserConfiguration.DataBaseConnectionString);
            SqlConn.Open();

            changeValueFunction(70);
            changeTaskFunction(ResourceLoader.GetString2("Borrando estructuras..."));

            SqlCommand SqlCom = new SqlCommand(DropQuery, SqlConn);

            SqlCom.ExecuteNonQuery();

            changeValueFunction(100);
        }

        private bool hasDataObjects()
        {
            ArrayList tableNames = new ArrayList();
            bool hasSBObjects = false;
            SqlDataReader myReader = null;

            try
            {
                SqlCommand myCommand = new SqlCommand("EXEC sp_tables", connection);
                myReader = myCommand.ExecuteReader();
                while (myReader.Read())
                {
                    if ("dbo" == myReader.GetString(1))
                    {
                        hasSBObjects = true;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageForm.Show(ex.Message, ex);
            }
            finally
            {
                if (myReader != null)
                    myReader.Close();
            }

            return hasSBObjects;
        }

        private SqlConnection connection = null;

    }
}
