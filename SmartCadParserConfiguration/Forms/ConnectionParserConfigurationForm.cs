using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SmartCadCore.Core;
using SmartCadParser;

using SmartCadCore.Common;
using Smartmatic.SmartCad.Service;

namespace SmartCadParserConfiguration
{
    public partial class ConnectionParserConfigurationForm : DevExpress.XtraEditors.XtraForm
    {
        private static string ApplicationName = "Parser";

        public ConnectionParserConfigurationForm()
        {
            InitializeComponent();
        }

        private void ConnectionParserConfigurationForm_Load(object sender, EventArgs e)
        {
            dbConnectionControl.DataBaseServer = ParserConfiguration.DataBaseServer;
            dbConnectionControl.DataBaseName = ParserConfiguration.DataBaseName;
            devicesConfigurationControl1.ParametersChanged += new SmartCadParserConfiguration.Controls.DevicesConfigurationControl.ParametersChangedEventHandler(devicesConfigurationControl1_ParametersChanged);
            textBoxExAppServer.Text = ParserConfiguration.SyncAppServer;
            if (ParserConfiguration.SyncPort != -1)
            {
                textBoxExSendPort.Text = ParserConfiguration.SyncPort.ToString();
            }
            
            if (ParserConfiguration.SyncRate != -1)
            {
                textBoxExRate.Text = ParserConfiguration.SyncRate.ToString();
            }

            EvalEnableOk();
            LoadLenguage();
        }

        void devicesConfigurationControl1_ParametersChanged(bool hasNeededFields)
        {
            EvalEnableOk();
        }

        private void LoadLenguage()
        {
            buttonExAccept.Text = ResourceLoader.GetString2("$Message.Accept");
            buttonExCancel.Text = ResourceLoader.GetString2("$Message.Cancel");
            tabPageConnection.Text = ResourceLoader.GetString2("Connection");
            this.Text = ResourceLoader.GetString2("ParserServiceName");
            this.groupBox1.Text = ResourceLoader.GetString2("FrameTransmission");
            this.label1.Text = ResourceLoader.GetString2("Server");
            this.labelEx2.Text = ResourceLoader.GetString2("FrequencyMS");
            this.labelEx1.Text = ResourceLoader.GetString2("Port");

        }

        private void buttonExAccept_Click(object sender, EventArgs e)
        {
            if (ApplicationUtil.IsServiceUp("SmartCad_Parser") == false)
            {
                ParserConfiguration.DataBaseServer = dbConnectionControl.DataBaseServer;
                ParserConfiguration.DataBaseName = dbConnectionControl.DataBaseName;
                ParserConfiguration.SyncAppServer = textBoxExAppServer.Text;

                if (string.IsNullOrEmpty(textBoxExSendPort.Text) == false)
                {
                    int syncPort = int.Parse(textBoxExSendPort.Text);
                    if ((syncPort < 1024 || syncPort > 65535) && textBoxExAppServer.Text != "")
                    {
                        MessageForm.Show(ResourceLoader.GetString2("InvalidTransmisionPort"), MessageFormType.Information);
                        DialogResult = DialogResult.None;
                        return;
                    }
                }

                if (string.IsNullOrEmpty(textBoxExRate.Text) == false)
                {
                    int rate = int.Parse(textBoxExRate.Text);
                    if (rate < 50 && textBoxExAppServer.Text != "")
                    {
                        MessageForm.Show(ResourceLoader.GetString2("InvalidTransmisionFrecuency"), MessageFormType.Information);
                        DialogResult = DialogResult.None;
                        return;
                    }
                }

                if (textBoxExRate.Text == "")
                {
                    ParserConfiguration.SyncRate = -1;
                }
                else
                {
                    ParserConfiguration.SyncRate = int.Parse(textBoxExRate.Text);
                }

                if (textBoxExSendPort.Text == "")
                {
                    ParserConfiguration.SyncPort = -1;//Esto desactiva el envio de tramas al servidor
                }
                else
                {
                    ParserConfiguration.SyncPort = int.Parse(textBoxExSendPort.Text);
                }
                try
                {
                    BindingList<DeviceElement> devices = devicesConfigurationControl1.GetDevicesCollection();
                    ParserConfiguration.ParserSection.Devices = new DevicesCollection();
                    foreach (DeviceElement device in devices)
                    {
                        if(device.Port > 0 && device.Protocol!="")
                            ParserConfiguration.ParserSection.Devices.Add(device);
                    }
                    
                    ParserConfiguration.Save();
                    var result = ServerServiceClient.GetServerService();
                    ServerServiceClient.GetInstance().ActivateApplication(ApplicationName, ApplicationUtil.GetMACAddress());
                    MessageForm.Show(ResourceLoader.GetString2("ChangesSavedSuccessfully"), MessageFormType.Information);

                    DialogResult = DialogResult.OK;
                }
                catch (Exception ex)
                {
                    MessageForm.Show(ResourceLoader.GetString2("Error guardando la configuración."), ex);

                    DialogResult = DialogResult.None;
                }

            }
            else
            {
                MessageForm.Show(
                    ResourceLoader.GetString2(
                        "NoChangesWithServiceUp", ResourceLoader.GetString2("ParserServiceTitle")
                        ), MessageFormType.Error);
                DialogResult = DialogResult.None;
            }
        }

        private void tabControl_KeyDown(object sender, KeyEventArgs e)
        {
            EvalEnableOk();
        }

        private void tabControl_KeyUp(object sender, KeyEventArgs e)
        {
            EvalEnableOk();
        }

        private void EvalEnableOk()
        {
            if (textBoxExAppServer.Text != string.Empty &&
                 (textBoxExRate.Text == string.Empty || textBoxExSendPort.Text == string.Empty)
                )
            {
                buttonExAccept.Enabled = false;
            }
            else 
            {
                buttonExAccept.Enabled = dbConnectionControl.HasNeededFields && devicesConfigurationControl1.HasNeededFields;
            }
        }

        private void textBoxExAppServer_TextChanged(object sender, EventArgs e)
        {
            EvalEnableOk();
        }

        private void textBoxExRate_TextChanged(object sender, EventArgs e)
        {
            EvalEnableOk();
        }
    }
}