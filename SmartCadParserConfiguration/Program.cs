using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading;
using SmartCadCore.Core;
using System.Security.Permissions;
using System.Security.Principal;
using SmartCadParser;
using SmartCadCore.Common;
using System.Globalization;
using SmartCadParserConfiguration.Gui;
using Smartmatic.SmartCad.Service;

namespace SmartCadParserConfiguration
{
    static class Program
    {
        

        [STAThread]
        static void Main()
        {
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                ApplicationUtil.ActivateProcess(Process.GetCurrentProcess());

                AppDomain.CurrentDomain.SetPrincipalPolicy(PrincipalPolicy.WindowsPrincipal);
                WindowsPrincipal myPrincipal = (WindowsPrincipal)Thread.CurrentPrincipal;

                ParserConfiguration.Load();

                SmartCadConfiguration.Load();
                var result = ServerServiceClient.GetServerService();
                
                    
                string language =
                    ParserConfiguration.CultureElement.CultureLanguageElement.Name + "-" +
                    ParserConfiguration.CultureElement.CultureCountryElement.Name;
                ResourceLoader.UpdatedLanguage(new CultureInfo(language));


                SplashForm.ShowSplash();

                Thread.Sleep(TimeSpan.FromSeconds(2));

                SplashForm.CloseSplash();

                if (myPrincipal.IsInRole(WindowsBuiltInRole.Administrator) == true)
                {
                    ConnectionParserConfigurationForm form = new ConnectionParserConfigurationForm();
                    form.ShowDialog();
                }
                else
                {
                    MessageForm.Show(ResourceLoader.GetString2("MustHaveAdministratorAccess"), MessageFormType.Error);
                }
            }
            catch (System.Security.SecurityException se)
            {
                MessageForm.Show(ResourceLoader.GetString2("MustHaveAdministratorAccess"), se);
            }
            catch (Exception ex)
            {
                MessageForm.Show(ResourceLoader.GetString2("GenericErrorApplication"), ex);
            }
        }
    }
}