using System.Windows.Forms.Design;
using SelectionRulesEnum = System.Windows.Forms.Design.SelectionRules;

namespace SmartCadParserConfiguration.Controls
{
	/// <summary>
	/// Summary description for SeparatorControlDesigner.
	/// </summary>
	public class SeparatorControlDesigner : ControlDesigner
	{
		public override SelectionRulesEnum SelectionRules
		{
			get { return (base.SelectionRules & (~SelectionRulesEnum.BottomSizeable) & (~SelectionRulesEnum.TopSizeable)); }
		}
	}
}