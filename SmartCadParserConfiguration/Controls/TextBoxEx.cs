using SmartCadParserConfiguration.Controls;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using FontClass = System.Drawing.Font;
using FontFormatClass = SmartCadParserConfiguration.Controls.FontFormat;

namespace Smartmatic.SmartCad.Controls
{
	/// <summary>
	/// Summary description for TextBoxEx.
	/// </summary>
	public class TextBoxEx : TextBox, IResourceLoadable
	{
		private MenuItem menuItemUndo;
		private MenuItem menuItemCut;
		private MenuItem menuItemCopy;
		private MenuItem menuItemPaste;
		private MenuItem menuItemDelete;
		private MenuItem menuItemSelectAll;
		private string nonAllowedCharacters = "";
		private string regularExpresion = "";
		private ContextMenu contextMenu;

		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private Container components = null;

		public TextBoxEx()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitForm call
			menuItemUndo = new MenuItem("Undo", new EventHandler(contextMenu_undoItem_Click), Shortcut.CtrlZ);
			menuItemCut = new MenuItem("Cut", new EventHandler(contextMenu_cutItem_Click), Shortcut.CtrlX);
			menuItemCopy = new MenuItem("Copy", new EventHandler(contextMenu_copyItem_Click), Shortcut.CtrlC);
			menuItemPaste = new MenuItem("Paste", new EventHandler(contextMenu_pasteItem_Click), Shortcut.CtrlV);
			menuItemDelete = new MenuItem("Delete", new EventHandler(contextMenu_deleteItem_Click), Shortcut.Del);
			menuItemSelectAll = new MenuItem("Select all", new EventHandler(contextMenu_selectAllItem_Click));
			contextMenu.MenuItems.Add(menuItemUndo);
			contextMenu.MenuItems.Add(new MenuItem("-"));
			contextMenu.MenuItems.Add(menuItemCut);
			contextMenu.MenuItems.Add(menuItemCopy);
			contextMenu.MenuItems.Add(menuItemPaste);
			contextMenu.MenuItems.Add(menuItemDelete);
			contextMenu.MenuItems.Add(new MenuItem("-"));
			contextMenu.MenuItems.Add(menuItemSelectAll);
		}


        protected override void WndProc(ref Message m)
        {
            const int WM_CONTEXTMENU = 0x007b;
            switch (m.Msg)
            {
                case WM_CONTEXTMENU:
                    break;
                default:
                    base.WndProc(ref m);
                    break;
            }
        }

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.contextMenu = new System.Windows.Forms.ContextMenu();
			// 
			// contextMenu
			// 
			this.contextMenu.Popup += new System.EventHandler(this.contextMenu_Popup);
			// 
			// TextBoxEx
			// 
			this.ContextMenu = this.contextMenu;

		}

		#endregion

		[Description(@"Specifies the characters that the control does not allow like entrance
Note: a character which would be in both NonAllowedCharacter and ExtraAllowedCharacter would not be allowed"),
			Category("Behavior")]
		public string NonAllowedCharacters
		{
			get { return nonAllowedCharacters; }
			set { nonAllowedCharacters = value; }
		}

		[Description(@"Specifies the regular expresion with which the text in the control must match
Ex: for an email, enter \w+@\w+.com
Note: each form who uses this control should catch the Validating event and decide what to do using the Regex.IsMatch method."),
			Category("Behavior")]
		public string RegularExpresion
		{
			get { return regularExpresion; }
			set { regularExpresion = value; }
		}

        /// <summary>
        /// Checks that the conditions given are present in s
        /// </summary>
        /// <param name="s">String to check</param>
        /// <returns>The result of validation</returns>
		public bool IsValid(string s)
		{
			foreach (char c in s)
			{
				if (NonAllowedCharacters.IndexOf(c) >= 0)
				{
					return false;
				}
				else
				{
					if (ExtraAllowedChars.IndexOf(c) < 0)
					{
						if ((Char.IsWhiteSpace(c) && (!AllowsWhiteSpaces || s.IndexOf(c) == 0)) || (Char.IsLetter(c) && !AllowsLetters) || (Char.IsNumber(c) && !AllowsNumbers) || (Char.IsSeparator(c) && !AllowsSeparators) || (Char.IsPunctuation(c) && !AllowsPunctuation) || (Char.IsSymbol(c) && !AllowsSymbols))
						{
							return false;
						}
					}
				}
			}
			return true;
		}

		protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
		{
			IDataObject iData = Clipboard.GetDataObject();
            if ((keyData == (Keys)Shortcut.CtrlV || keyData == (Keys)Shortcut.ShiftIns || keyData == (Keys)Shortcut.CtrlShiftV) 
                && iData.GetDataPresent(DataFormats.Text))
			{
				string newText = base.Text.Substring(0, base.SelectionStart)
					+ (string) iData.GetData(DataFormats.Text)
					+ base.Text.Substring(base.SelectionStart + base.SelectionLength);
				if (!IsValid(newText))
				{
					return true;
				}
			}

            if (keyData == (Keys)Shortcut.CtrlH || keyData == (Keys)Shortcut.CtrlI || keyData == (Keys)Shortcut.CtrlJ || keyData == (Keys)Shortcut.CtrlM)
                return true;

			return base.ProcessCmdKey(ref msg, keyData);
		}

        protected override void OnMouseHover(EventArgs e)
        {
            if (this.Text == "")
            {
                return;
            }
            base.OnMouseHover(e);
          
        }
		protected override void OnKeyPress(KeyPressEventArgs e)
		{
			char c = e.KeyChar;
			if (!Char.IsControl(c))
			{
				string newtext;
				newtext = base.Text.Substring(0, base.SelectionStart)
					+ c.ToString() + base.Text.Substring(base.SelectionStart + base.SelectionLength);
				if (!IsValid(newtext))
				{
					e.Handled = true;
				}
			}
			base.OnKeyPress(e);
		}

		protected override bool IsInputChar(char charCode)
		{
			if (nonAllowedCharacters.IndexOfAny(new char[1] {charCode}) >= 0)
			{
				return false;
			}
			else
			{
				return base.IsInputChar(charCode);
			}
		}

		protected virtual void contextMenu_undoItem_Click(object sender, EventArgs e)
		{
			Undo();
		}

		protected virtual void contextMenu_cutItem_Click(object sender, EventArgs e)
		{
			Cut();
		}

		protected virtual void contextMenu_copyItem_Click(object sender, EventArgs e)
		{
			Copy();
		}

		protected virtual void contextMenu_pasteItem_Click(object sender, EventArgs e)
		{
			IDataObject iData = Clipboard.GetDataObject();
			if (iData.GetDataPresent(DataFormats.Text))
			{
				string newText;
				newText = base.Text.Substring(0, base.SelectionStart)
					+ (string) iData.GetData(DataFormats.Text)
					+ base.Text.Substring(base.SelectionStart + base.SelectionLength);
				if (IsValid(newText))
				{
					Paste();
				}
			}
		}

		protected virtual void contextMenu_deleteItem_Click(object sender, EventArgs e)
		{
			if (SelectionStart < Text.Length && !this.ReadOnly)
			{
				int selectionStarT = SelectionStart;
				int count = (SelectionLength == 0 ? 1 : SelectionLength);
				Text = Text.Remove(SelectionStart, count);
				SelectionStart = selectionStarT;
			}
		}

		protected virtual void contextMenu_selectAllItem_Click(object sender, EventArgs e)
		{
			SelectAll();
		}

		private void contextMenu_Popup(object sender, EventArgs e)
		{
			if (!Focused)
			{
				Focus();
			}
			if (SelectionLength == 0)
			{
				menuItemCopy.Enabled = false;
				menuItemCut.Enabled = false;
				menuItemDelete.Enabled = false;
			}
			else
			{
				menuItemCopy.Enabled = true;
				menuItemCut.Enabled = true;
				menuItemDelete.Enabled = true;
			}
			if (Text.Length == SelectionLength)
			{
				menuItemSelectAll.Enabled = false;
			}
			else
			{
				menuItemSelectAll.Enabled = true;
			}
			if (!Modified)
			{
				menuItemUndo.Enabled = false;
			}
			else
			{
				menuItemUndo.Enabled = true;
			}
			IDataObject iData = Clipboard.GetDataObject();
			if (!iData.GetDataPresent(DataFormats.Text))
			{
				menuItemPaste.Enabled = false;
			}
			else
			{
				string newText;
				newText = base.Text.Substring(0, base.SelectionStart)
					+ (string) iData.GetData(DataFormats.Text)
					+ base.Text.Substring(base.SelectionStart + base.SelectionLength);
				if (!IsValid(newText))
				{
					menuItemPaste.Enabled = false;
				}
				else
				{
					menuItemPaste.Enabled = true;
				}
			}

			if (this.ReadOnly)
			{
				menuItemDelete.Enabled = false;
			}
		}


		[RefreshProperties(RefreshProperties.All),
			DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
			Browsable(false)]
		public override FontClass Font
		{
			get { return base.Font; }
			set { base.Font = value; }
		}

		[RefreshProperties(RefreshProperties.All),
			DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
			Browsable(false)]
		public override Color ForeColor
		{
			get { return base.ForeColor; }
			set { base.ForeColor = value; }
		}

		private FontFormatClass fontFormat = FontFormatClass.EditBoxFormat;

		[EditorBrowsable(EditorBrowsableState.Never),
			Browsable(true),
			Description("Specified The font and foreground color used to display text and graphics in the control"),
			Category("Appearance")]
		public FontFormatClass FontFormat
		{
			get { return fontFormat; }
			set
			{
				fontFormat = value;
				ForeColor = fontFormat.Color;
				Font = fontFormat.Font;
			}
		}

		private bool allowsNumbers = true;

		[Description("If false, the control doesn't accept numbers"),
			Category("Behavior")]
		public bool AllowsNumbers
		{
			get { return allowsNumbers; }
			set { allowsNumbers = value; }
		}

		private bool allowsSeparators = true;

		[Description("If false, the control doesn't accept separators"),
			Category("Behavior")]
		public bool AllowsSeparators
		{
			get { return allowsSeparators; }
			set { allowsSeparators = value; }
		}

		private bool allowsLetters = true;

		[Description("If false, the control doesn't accept letters"),
			Category("Behavior")]
		public bool AllowsLetters
		{
			get { return allowsLetters; }
			set { allowsLetters = value; }
		}

		private bool allowsPunctuation = true;

		[Description("If false, the control doesn't accept punctuation"),
			Category("Behavior")]
		public bool AllowsPunctuation
		{
			get { return allowsPunctuation; }
			set { allowsPunctuation = value; }
		}

		private bool allowsSymbols = true;

		[Description("If false, the control doesn't accept punctuation"),
			Category("Behavior")]
		public bool AllowsSymbols
		{
			get { return allowsSymbols; }
			set { allowsSymbols = value; }
		}

		private bool allowsWhiteSpaces = true;

		[Description("If false, the control doesn't accept spaces"),
			Category("Behavior")]
		public bool AllowsWhiteSpaces
		{
			get { return allowsWhiteSpaces; }
			set { allowsWhiteSpaces = value; }
		}

		private string extraAllowedChars = "";

		[Browsable(true),
			Description(@"Specified the extra allowed characters that aren�t contemplated by the other properties
Note: a character which would be in both NonAllowedCharacter and ExtraAllowedCharacter would not be allowed"),
			Category("Appearance")]
		public string ExtraAllowedChars
		{
			get { return extraAllowedChars; }
			set { extraAllowedChars = value; }
		}

		public bool IsExtraAllowedChar(char c)
		{
			return (extraAllowedChars.IndexOf(c) >= 0);
		}

		private bool loadFromResources = true;
		#region IResourceLoadable Members

		[Description("This property is used to load text and images resources from Smartmatic.Usp.Resources when it is true."
			 +" When false, resources are the same at design time"),
		Category("Behavior"), Browsable(true), DefaultValue(true)]
		public bool LoadFromResources
		{
			get
			{
				// TODO:  Add AlphabeticalTextBox.LoadFromResources getter implementation
				return loadFromResources;
			}
			set
			{
				// TODO:  Add AlphabeticalTextBox.LoadFromResources setter implementation
				loadFromResources = value;
			}
		}

		#endregion

	}
}
