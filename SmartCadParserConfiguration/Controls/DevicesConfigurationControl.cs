﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using SmartCadParser;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using System.IO;
using SmartCadCore.Common;

namespace SmartCadParserConfiguration.Controls
{
    public partial class DevicesConfigurationControl : UserControl
    {
        public bool HasNeededFields { get; set; }
        public event ParametersChangedEventHandler ParametersChanged;
        public delegate void ParametersChangedEventHandler(bool hasNeededFields);
        
        public DevicesConfigurationControl()
        {
            InitializeComponent();
        }

        private void DevicesConfigurationControl_Load(object sender, EventArgs e)
        {
            LoadLanguage();
            LoadDevices();
        }

        private void LoadDevices()
        {
            try
            {
                BindingList<DeviceElement> devices = new BindingList<DeviceElement>();
                List<DeviceElement> storageDevices = ParserConfiguration.Devices;

                byte[] dllBytes = File.ReadAllBytes(DataFrame.DataDllFileName);
                Assembly assembly = Assembly.Load(dllBytes);
                Type[] types = assembly.GetTypes();

                foreach (Type type in types)
                {
                    if (type.IsClass == true
                        && type.IsAbstract == false
                        && type.IsSubclassOf(typeof(DataFrame)))
                    {
                        repositoryItemComboBoxDevices.Items.Add(type.Name.Substring(0, type.Name.Length - 4));
                    }
                }

                foreach (DeviceElement device in storageDevices)
                {
                    foreach (string type in repositoryItemComboBoxDevices.Items)
                    {
                        if (type == device.Type)
                            devices.Add(device);
                    }
                }

                gridControlDevices.BeginUpdate();
                gridControlDevices.DataSource = devices;
                gridControlDevices.EndUpdate();
            }
            catch (Exception e)
            {
                //SmartLogger.Print(e);
            }
            HasNeededFields = true;
        }

        private void LoadLanguage()
        {
            this.gridColumnDevice.Caption = ResourceLoader.GetString2("DeviceType");
            this.gridColumnProtocol.Caption = ResourceLoader.GetString2("Protocol");
            this.gridColumnPort.Caption = ResourceLoader.GetString2("LprPort");
            this.gridColumnDelete.Caption = ResourceLoader.GetString2("Delete");
            this.repositoryItemButtonEdit1.Buttons[0].ToolTip = ResourceLoader.GetString2("Delete");
            this.groupBoxReception.Text = ResourceLoader.GetString2("FrameReception");
        }

        public BindingList<DeviceElement> GetDevicesCollection()
        {
            return gridControlDevices.DataSource as BindingList<DeviceElement>;
        }

        private void gridView1_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            DeviceElement device = gridView1.GetRow(e.RowHandle) as DeviceElement;
            int index = ((BindingList<DeviceElement>)gridControlDevices.DataSource).IndexOf(device);

            if (device != null)
            {
                if (device.Port != 0 && device.Protocol != "")
                {
                    int i = 0;
                    foreach (DeviceElement dev in gridControlDevices.DataSource as BindingList<DeviceElement>)
                    {
                        if (index != i)
                        {
                            if (dev.Port == device.Port && dev.Protocol == device.Protocol)
                            {
                                e.Valid = false;
                                //Set errors with specific descriptions for the columns
                                if (gridView1.FocusedColumn.Name == "gridColumnDelete")
                                {
                                    gridView1.SetColumnError(gridColumnPort, ResourceLoader.GetString2("DeviceProtocolAndPortError"));
                                }
                                else
                                {
                                    gridView1.SetColumnError(gridView1.FocusedColumn, ResourceLoader.GetString2("DeviceProtocolAndPortError"));
                                }
                                break;
                            }
                        }
                        i++;
                    }
                }
                else
                {
                    e.Valid = false;
                }
                HasNeededFields = e.Valid;

                if (ParametersChanged != null)
                    ParametersChanged(HasNeededFields);
            }
        }

        private void gridView1_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
        {
            //Suppress displaying the error message box
            e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction;
        }

        private void repositoryItemButtonEdit1_MouseUp(object sender, MouseEventArgs e)
        {
            gridControlDevices.BeginUpdate();
            ((BindingList<DeviceElement>)gridControlDevices.DataSource).Remove((DeviceElement)gridView1.GetFocusedRow());
            gridControlDevices.EndUpdate();

            HasNeededFields = true;
            if (ParametersChanged != null)
                ParametersChanged(HasNeededFields);
        }
    }
}
