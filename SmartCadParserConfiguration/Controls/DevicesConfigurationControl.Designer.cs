﻿namespace SmartCadParserConfiguration.Controls
{
    partial class DevicesConfigurationControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxReception = new DevExpress.XtraEditors.GroupControl();
            this.gridControlDevices = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnDevice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemComboBoxDevices = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.gridColumnProtocol = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.gridColumnPort = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridColumnDelete = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            ((System.ComponentModel.ISupportInitialize)(this.groupBoxReception)).BeginInit();
            this.groupBoxReception.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlDevices)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBoxDevices)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBoxReception
            // 
            this.groupBoxReception.Controls.Add(this.gridControlDevices);
            this.groupBoxReception.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxReception.Location = new System.Drawing.Point(0, 0);
            this.groupBoxReception.Name = "groupBoxReception";
            this.groupBoxReception.Size = new System.Drawing.Size(374, 265);
            this.groupBoxReception.TabIndex = 2;
            this.groupBoxReception.Text = "Recepción de tramas";
            // 
            // gridControlDevices
            // 
            this.gridControlDevices.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlDevices.Location = new System.Drawing.Point(2, 20);
            this.gridControlDevices.MainView = this.gridView1;
            this.gridControlDevices.Name = "gridControlDevices";
            this.gridControlDevices.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox1,
            this.repositoryItemSpinEdit1,
            this.repositoryItemComboBoxDevices,
            this.repositoryItemButtonEdit1});
            this.gridControlDevices.Size = new System.Drawing.Size(370, 243);
            this.gridControlDevices.TabIndex = 0;
            this.gridControlDevices.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnDevice,
            this.gridColumnProtocol,
            this.gridColumnPort,
            this.gridColumnDelete});
            this.gridView1.GridControl = this.gridControlDevices;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsCustomization.AllowFilter = false;
            this.gridView1.OptionsNavigation.EnterMoveNextColumn = true;
            this.gridView1.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.InvalidRowException += new DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventHandler(this.gridView1_InvalidRowException);
            this.gridView1.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridView1_ValidateRow);
            // 
            // gridColumnDevice
            // 
            this.gridColumnDevice.Caption = "Device";
            this.gridColumnDevice.ColumnEdit = this.repositoryItemComboBoxDevices;
            this.gridColumnDevice.FieldName = "Type";
            this.gridColumnDevice.Name = "gridColumnDevice";
            this.gridColumnDevice.Visible = true;
            this.gridColumnDevice.VisibleIndex = 0;
            this.gridColumnDevice.Width = 149;
            // 
            // repositoryItemComboBoxDevices
            // 
            this.repositoryItemComboBoxDevices.AutoHeight = false;
            this.repositoryItemComboBoxDevices.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBoxDevices.Name = "repositoryItemComboBoxDevices";
            this.repositoryItemComboBoxDevices.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // gridColumnProtocol
            // 
            this.gridColumnProtocol.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumnProtocol.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnProtocol.Caption = "Protocol";
            this.gridColumnProtocol.ColumnEdit = this.repositoryItemComboBox1;
            this.gridColumnProtocol.FieldName = "Protocol";
            this.gridColumnProtocol.Name = "gridColumnProtocol";
            this.gridColumnProtocol.Visible = true;
            this.gridColumnProtocol.VisibleIndex = 1;
            this.gridColumnProtocol.Width = 70;
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Items.AddRange(new object[] {
            "SMS","TCP","UDP"});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            this.repositoryItemComboBox1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // gridColumnPort
            // 
            this.gridColumnPort.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumnPort.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnPort.Caption = "Port";
            this.gridColumnPort.ColumnEdit = this.repositoryItemSpinEdit1;
            this.gridColumnPort.FieldName = "Port";
            this.gridColumnPort.Name = "gridColumnPort";
            this.gridColumnPort.Visible = true;
            this.gridColumnPort.VisibleIndex = 2;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit1.IsFloatValue = false;
            //this.repositoryItemSpinEdit1.Mask.EditMask = "\\d{5}";
            //this.repositoryItemSpinEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.repositoryItemSpinEdit1.MaxValue = new decimal(new int[] {
            60000,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            //this.repositoryItemSpinEdit1.ValidateOnEnterKey = true;
            // 
            // gridColumnDelete
            // 
            this.gridColumnDelete.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumnDelete.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumnDelete.Caption = "gridColumnDelete";
            this.gridColumnDelete.ColumnEdit = this.repositoryItemButtonEdit1;
            this.gridColumnDelete.Name = "gridColumnDelete";
            this.gridColumnDelete.Visible = true;
            this.gridColumnDelete.VisibleIndex = 3;
            this.gridColumnDelete.Width = 50;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, global::SmartCadParserConfiguration.Properties.Resources.delete_icon1, null)});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEdit1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.repositoryItemButtonEdit1_MouseUp);
            // 
            // DevicesConfigurationControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBoxReception);
            this.Name = "DevicesConfigurationControl";
            this.Size = new System.Drawing.Size(374, 265);
            this.Load += new System.EventHandler(this.DevicesConfigurationControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupBoxReception)).EndInit();
            this.groupBoxReception.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlDevices)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBoxDevices)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupBoxReception;
        private DevExpress.XtraGrid.GridControl gridControlDevices;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnDevice;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnProtocol;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnPort;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBoxDevices;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnDelete;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
    }
}
