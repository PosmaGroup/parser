using SmartCadParserConfiguration.Controls;

namespace SmartCadParserConfiguration
{
    partial class GPSParserDbConnectionControl
    {
        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GPSParserDbConnectionControl));
            this.groupBoxDB = new DevExpress.XtraEditors.GroupControl();
            this.textBoxServer = new Smartmatic.SmartCad.Controls.TextBoxEx();
            this.labelDBName = new DevExpress.XtraEditors.LabelControl();
            this.buttonNewDB = new SimpleButtonEx();
            this.textBoxName = new Smartmatic.SmartCad.Controls.TextBoxEx();
            this.labelServer = new DevExpress.XtraEditors.LabelControl();
            this.buttonConfigurateDB = new SimpleButtonEx();
            this.toolTipMain = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.groupBoxDB)).BeginInit();
            this.groupBoxDB.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxDB
            // 
            this.groupBoxDB.Controls.Add(this.textBoxServer);
            this.groupBoxDB.Controls.Add(this.labelDBName);
            this.groupBoxDB.Controls.Add(this.buttonNewDB);
            this.groupBoxDB.Controls.Add(this.textBoxName);
            this.groupBoxDB.Controls.Add(this.labelServer);
            this.groupBoxDB.Controls.Add(this.buttonConfigurateDB);
            this.groupBoxDB.Location = new System.Drawing.Point(1, 1);
            this.groupBoxDB.Name = "groupBoxDB";
            this.groupBoxDB.Size = new System.Drawing.Size(368, 79);
            this.groupBoxDB.TabIndex = 1;
            this.groupBoxDB.Text = "Base de datos";
            // 
            // textBoxServer
            // 
            this.textBoxServer.AllowsLetters = true;
            this.textBoxServer.AllowsNumbers = true;
            this.textBoxServer.AllowsPunctuation = true;
            this.textBoxServer.AllowsSeparators = true;
            this.textBoxServer.AllowsSymbols = true;
            this.textBoxServer.AllowsWhiteSpaces = true;
            this.textBoxServer.ExtraAllowedChars = "";
            this.textBoxServer.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxServer.Location = new System.Drawing.Point(96, 22);
            this.textBoxServer.MaxLength = 63;
            this.textBoxServer.Name = "textBoxServer";
            this.textBoxServer.NonAllowedCharacters = "\'";
            this.textBoxServer.RegularExpresion = "";
            this.textBoxServer.Size = new System.Drawing.Size(259, 20);
            this.textBoxServer.TabIndex = 10;
            this.textBoxServer.TextChanged += new System.EventHandler(this.GPSParserDbConnectionControlParameters_Change);
            // 
            // labelDBName
            // 
            this.labelDBName.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDBName.Appearance.Options.UseFont = true;
            this.labelDBName.Location = new System.Drawing.Point(7, 52);
            this.labelDBName.Name = "labelDBName";
            this.labelDBName.Size = new System.Drawing.Size(81, 13);
            this.labelDBName.TabIndex = 17;
            this.labelDBName.Text = "Base de datos : *";
            // 
            // buttonNewDB
            // 
            this.buttonNewDB.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNewDB.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonNewDB.Appearance.Options.UseFont = true;
            this.buttonNewDB.Appearance.Options.UseForeColor = true;
            this.buttonNewDB.FontFormat = FontFormat.GetFormat("ButtonFormat");
            this.buttonNewDB.Image = ((System.Drawing.Image)(resources.GetObject("buttonNewDB.Image")));
            this.buttonNewDB.Location = new System.Drawing.Point(296, 50);
            this.buttonNewDB.Name = "buttonNewDB";
            this.buttonNewDB.Size = new System.Drawing.Size(24, 24);
            this.buttonNewDB.TabIndex = 12;
            this.toolTipMain.SetToolTip(this.buttonNewDB, "Crear nueva base de datos");
            this.buttonNewDB.Click += new System.EventHandler(this.buttonNewDB_Click);
            // 
            // textBoxName
            // 
            this.textBoxName.AllowsLetters = true;
            this.textBoxName.AllowsNumbers = true;
            this.textBoxName.AllowsPunctuation = true;
            this.textBoxName.AllowsSeparators = true;
            this.textBoxName.AllowsSymbols = true;
            this.textBoxName.AllowsWhiteSpaces = true;
            this.textBoxName.ExtraAllowedChars = "";
            this.textBoxName.FontFormat = FontFormat.GetFormat("EditBoxFormat");
            this.textBoxName.Location = new System.Drawing.Point(96, 52);
            this.textBoxName.MaxLength = 100;
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.NonAllowedCharacters = "\'";
            this.textBoxName.RegularExpresion = "";
            this.textBoxName.Size = new System.Drawing.Size(195, 20);
            this.textBoxName.TabIndex = 11;
            this.textBoxName.TextChanged += new System.EventHandler(this.GPSParserDbConnectionControlParameters_Change);
            // 
            // labelServer
            // 
            this.labelServer.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelServer.Appearance.Options.UseFont = true;
            this.labelServer.Location = new System.Drawing.Point(7, 24);
            this.labelServer.Name = "labelServer";
            this.labelServer.Size = new System.Drawing.Size(52, 13);
            this.labelServer.TabIndex = 16;
            this.labelServer.Text = "Servidor : *";
            // 
            // buttonConfigurateDB
            // 
            this.buttonConfigurateDB.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonConfigurateDB.Appearance.ForeColor = System.Drawing.Color.Black;
            this.buttonConfigurateDB.Appearance.Options.UseFont = true;
            this.buttonConfigurateDB.Appearance.Options.UseForeColor = true;
            this.buttonConfigurateDB.FontFormat = FontFormat.GetFormat("ButtonFormat");
            this.buttonConfigurateDB.Image = ((System.Drawing.Image)(resources.GetObject("buttonConfigurateDB.Image")));
            this.buttonConfigurateDB.Location = new System.Drawing.Point(328, 50);
            this.buttonConfigurateDB.Name = "buttonConfigurateDB";
            this.buttonConfigurateDB.Size = new System.Drawing.Size(24, 24);
            this.buttonConfigurateDB.TabIndex = 13;
            this.toolTipMain.SetToolTip(this.buttonConfigurateDB, "Configurar base de datos");
            this.buttonConfigurateDB.Click += new System.EventHandler(this.buttonConfigurateDB_Click);
            // 
            // GPSParserDbConnectionControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBoxDB);
            this.Name = "GPSParserDbConnectionControl";
            this.Size = new System.Drawing.Size(370, 86);
            this.Load += new System.EventHandler(this.GPSParserDbConnectionControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupBoxDB)).EndInit();
            this.groupBoxDB.ResumeLayout(false);
            this.groupBoxDB.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupBoxDB;
        private Smartmatic.SmartCad.Controls.TextBoxEx textBoxServer;
        private DevExpress.XtraEditors.LabelControl labelDBName;
        private SimpleButtonEx buttonNewDB;
        private Smartmatic.SmartCad.Controls.TextBoxEx textBoxName;
        private DevExpress.XtraEditors.LabelControl labelServer;
        private SimpleButtonEx buttonConfigurateDB;
        private System.Windows.Forms.ToolTip toolTipMain;
        private System.ComponentModel.IContainer components;
    }
}
