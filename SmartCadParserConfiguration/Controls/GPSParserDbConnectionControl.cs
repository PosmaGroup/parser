using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Reflection;
using SmartCadCore.Core;
using SmartCadParser;
using SmartCadCore.Common;
using SmartCadParserConfiguration.Gui;

namespace SmartCadParserConfiguration
{
    public partial class GPSParserDbConnectionControl : DevExpress.XtraEditors.XtraUserControl
    {
        public bool HasNeededFields
        {
            get
            {
                return DataBaseServer.Length != 0 &&
                   DataBaseName.Length != 0;
            }
        }

        public GPSParserDbConnectionControl()
        {
            InitializeComponent();
        }

        private void GPSParserDbConnectionControl_Load(object sender, EventArgs e)
        {
            GPSParserDbConnectionControlParameters_Change(null, null);
            this.toolTipMain.SetToolTip(this.buttonConfigurateDB, ResourceLoader.GetString2("ToolTipbuttonConfigurateDB"));
            this.toolTipMain.SetToolTip(this.buttonNewDB, ResourceLoader.GetString2("ToolTipButtonNewDB"));
            LoadLenguage();
        }

        private void GPSParserDbConnectionControlParameters_Change(object sender, System.EventArgs e)
        {
            buttonConfigurateDB.Enabled = HasNeededFields;
           // buttonNewDB.Enabled = HasNeededFilds;
        }

        private void LoadLenguage()
        {
            groupBoxDB.Text = ResourceLoader.GetString2("DataBase");
            labelDBName.Text = ResourceLoader.GetString2("$Message.Name") + ": *";
            labelServer.Text = ResourceLoader.GetString2("Server") + ": *";
        }

        private void buttonNewDB_Click(object sender, EventArgs e)
        {
            NewDBLogin newDBLogin = new NewDBLogin();
            newDBLogin.ShowDialog();

            if (newDBLogin.DialogResult == DialogResult.OK)
            {
                NewDBForm newDBForm = new NewDBForm(newDBLogin.connection);
                newDBForm.ShowDialog();

                if (newDBForm.DialogResult == DialogResult.OK)
                {
                    DataBaseServer = newDBLogin.Server;
                    DataBaseName = newDBForm.DataBaseName;
                }
            }

            this.toolTipMain.SetToolTip(this.buttonNewDB, ResourceLoader.GetString2("ToolTipButtonNewDB"));
        }

        private void buttonConfigurateDB_Click(object sender, EventArgs e)
        {
            string oldDataBaseServer = ParserConfiguration.DataBaseServer;
            string oldDataBaseName = ParserConfiguration.DataBaseName;

            try
            {
#if (!DEBUG)
                if (ApplicationUtil.IsServiceUp("SmartCad_Parser"))
                {
                    throw new Exception("ServiceUpException");
                }
#endif
                ParserConfiguration.DataBaseServer = DataBaseServer;
                ParserConfiguration.DataBaseName = DataBaseName;

                ParserConfiguration.Save();

                BackgroundProcessForm processForm = new BackgroundProcessForm(this, new MethodInfo[1] {
                    GetType().GetMethod("CreateConnection", BindingFlags.NonPublic | BindingFlags.Instance) }, new object[1][] { new object[0] });
                processForm.CanThrowError = true;
                processForm.ShowDialog();

                SqlConnection connection = (SqlConnection)processForm.Results[0];

                GPSParserDBConfigurationForm ConfigurateDB = new GPSParserDBConfigurationForm(connection);

                if (ConfigurateDB.ShowDialog() == DialogResult.Cancel)
                {
                    ParserConfiguration.DataBaseServer = oldDataBaseServer;
                    ParserConfiguration.DataBaseName = oldDataBaseName;

                    ParserConfiguration.Save();
                }
            }
            catch (SqlException ex)
            {

                MessageForm.Show(ResourceLoader.GetString2("ErrorConnectionDBServer"), ex); 
                ParserConfiguration.DataBaseServer = oldDataBaseServer;
                ParserConfiguration.DataBaseName = oldDataBaseName;

                ParserConfiguration.Save();
            }
            catch
            {
                MessageForm.Show(
                    string.Format(
                        ResourceLoader.GetString2("NoChangesWithServiceUp"),
                        ResourceLoader.GetString2("ParserServiceTitle")
                    ),
                    MessageFormType.Error
                );
            }

            this.toolTipMain.SetToolTip(this.buttonConfigurateDB, ResourceLoader.GetString2("ToolTipbuttonConfigurateDB"));
        }

        public string DataBaseServer
        {
            get
            {
                return textBoxServer.Text.Trim();
            }
            set
            {
                textBoxServer.Text = value.Trim();
            }
        }

        public string DataBaseName
        {
            get
            {
                return textBoxName.Text.Trim();
            }
            set
            {
                textBoxName.Text = value.Trim();
            }
        }

        private SqlConnection CreateConnection()
        {
            string server = "Data Source=";
            string catalog = ";Initial Catalog=";
            string user = ";uid=";
            string pwd = ";pwd=";
            string security = "; Persist Security Info=True";

            server += DataBaseServer;
            catalog += DataBaseName;

            string connectionString = server + catalog + user + pwd + security + ";Pooling = false";

            SqlConnection connection = new SqlConnection(ParserConfiguration.DataBaseConnectionString);
            connection.Open();

            return connection;
        }
    }
}
