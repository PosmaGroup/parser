using System;
using System.ComponentModel;

namespace SmartCadParserConfiguration.Controls
{
	/// <summary>
	/// Interface to expose properties related to the loading of resources associated to
	/// windows form
	/// </summary>
	/// <remarks>Author: Julio Ynojosa, jynojosa@smartmatic.com, september 2005</remarks>
	
	public interface IResourceLoadable
	{	
		/// <summary>
		/// When LoadFromResources is true, this property is used to load text and images resources from Smartmatic.Usp.Resources
		/// </summary>		
		bool LoadFromResources
		{
			get;
			set;
		}
	}
}
