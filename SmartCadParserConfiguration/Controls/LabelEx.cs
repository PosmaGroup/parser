using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using FontClass = System.Drawing.Font;
using FontFormatClass = SmartCadParserConfiguration.Controls.FontFormat;

namespace SmartCadParserConfiguration.Controls
{
	/// <summary>
	/// Summary description for LabelEx.
	/// </summary>
	public class LabelEx : Label, IResourceLoadable
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private Container components = null;

		public LabelEx()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

            ForeColor = fontFormat.Color;
            Font = fontFormat.Font;
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
		}

		#endregion

		[RefreshProperties(RefreshProperties.All),
         DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
         Browsable(true)]
		public override FontClass Font
		{
			get
            {
                return base.Font;
            }
			set
            {
                base.Font = value;
            }
		}

		[RefreshProperties(RefreshProperties.All),
         DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
         Browsable(true)]
		public override Color ForeColor
		{
			get
            {
                return base.ForeColor;
            }
			set
            {
                base.ForeColor = value;
            }
		}

		private FontFormatClass fontFormat = FontFormatClass.LabelFormat;

		[EditorBrowsable(EditorBrowsableState.Always),
			Browsable(true),
			Description("Specified The font and foreground color used to display text and graphics in the control"),
			Category("Appearance")]
		public FontFormatClass FontFormat
		{
			get { return fontFormat; }
			set
			{
				fontFormat = value;
				//ForeColor = fontFormat.Color;
				//Font = fontFormat.Font;
			}
		}
		
		private bool loadFromResources = true;
		#region IResourceLoadable Members

		[Description("This property is used to load text and images resources from Smartmatic.Usp.Resources when it is true."
			 +" When false, resources are the same at design time"),
		Category("Behavior"), Browsable(true), DefaultValue(true)]
		public bool LoadFromResources
		{
			get
			{
				// TODO:  Add AlphabeticalTextBox.LoadFromResources getter implementation
				return loadFromResources;
			}
			set
			{
				// TODO:  Add AlphabeticalTextBox.LoadFromResources setter implementation
				loadFromResources = value;
			}
		}

		#endregion
	}
}