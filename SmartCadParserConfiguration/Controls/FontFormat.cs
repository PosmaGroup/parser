using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using FontClass = System.Drawing.Font;
using ColorClass = System.Drawing.Color;

namespace SmartCadParserConfiguration.Controls
{
	/// <summary>
	/// Summary description for FontFormat.
	/// </summary>
	[Editor(typeof (FontFormatEditor), typeof (UITypeEditor)),
		TypeConverter(typeof (FontFormatConverter))]
	public class FontFormat
	{
		private static Hashtable values = new Hashtable();
		private static StringCollection valueNames = new StringCollection();

		public static StringCollection ValueNames
		{
			get { return valueNames; }
		}

		public static FontFormat GetFormat(string name)
		{
			FontFormat ret = null;
			if (values.ContainsKey(name))
			{
				ret = (FontFormat) values[name];
			}
			return ret;
		}

		private string name = "";
		private FontClass font = null;
		private ColorClass color = ColorClass.Black;

		public string Name
		{
			get { return name; }
		}

		public FontClass Font
		{
			get { return font; }
		}

		public ColorClass Color
		{
			get { return color; }
		}

		private FontFormat(string nameI, FontClass fontI, ColorClass colorI)
		{
			name = nameI;
			font = fontI;
			color = colorI;
			values.Add(name, this);
			valueNames.Add(name);
		}

		public static readonly FontFormat LabelFormat = new FontFormat("LabelFormat",
		                                                               new FontClass(FontFamily.GenericSansSerif, 8.25F, FontStyle.Regular, GraphicsUnit.Point, (byte) 0),
		                                                               ColorClass.Black);

		public static readonly FontFormat EditBoxFormat = new FontFormat("EditBoxFormat",
		                                                                 new FontClass(FontFamily.GenericSansSerif, 8.25F, FontStyle.Regular, GraphicsUnit.Point, (byte) 0),
		                                                                 ColorClass.Black);

		public static readonly FontFormat ButtonFormat = new FontFormat("ButtonFormat",
		                                                                new FontClass(FontFamily.GenericSansSerif, 8.25F, FontStyle.Regular, GraphicsUnit.Point, (byte) 0),
		                                                                ColorClass.Black);

		public static readonly FontFormat TitleFormat = new FontFormat("TitleFormat",
		                                                               new FontClass(FontFamily.GenericSansSerif, 11.25F, FontStyle.Bold, GraphicsUnit.Point, (byte) 0),
		                                                               ColorClass.Black);

		public static readonly FontFormat ReportTitleFormat = new FontFormat("ReportTitleFormat",
		                                                                     new FontClass(FontFamily.GenericSansSerif, 9.25F, FontStyle.Bold, GraphicsUnit.Point, (byte) 0),
		                                                                     ColorClass.White);

		public static readonly FontFormat StationTitleFormat = new FontFormat("StationTitleFormat",
		                                                                      new FontClass("Arial", 12F, FontStyle.Bold, GraphicsUnit.Point, (byte) 0),
		                                                                      ColorClass.White);
	}
}