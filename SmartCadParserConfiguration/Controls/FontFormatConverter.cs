using System;
using System.ComponentModel;
using System.ComponentModel.Design.Serialization;
using System.Globalization;
using System.Reflection;

namespace SmartCadParserConfiguration.Controls
{
	/// <summary>
	/// Summary description for FontFormatConverter.
	/// </summary>
	public class FontFormatConverter : TypeConverter
	{
		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			if (sourceType == typeof (string))
			{
				return false;
			}
			return base.CanConvertFrom(context, sourceType);
		}

		public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
		{
			if (destinationType == typeof (InstanceDescriptor))
			{
				return true;
			}
			else if (destinationType == typeof (string))
			{
				return true;
			}
			return base.CanConvertTo(context, destinationType);
		}

		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
		{
			if (value is FontFormat)
			{
				FontFormat format = (FontFormat) value;
				if (destinationType == typeof (InstanceDescriptor))
				{
					MethodInfo method = typeof (FontFormat).GetMethod("GetFormat", BindingFlags.Static | BindingFlags.Public);
					if (method != null)
					{
						return new InstanceDescriptor(method, new object[1] {format.Name});
					}
				}
				else if (destinationType == typeof (string))
				{
					return format.Name;
				}
			}
			return base.ConvertTo(context, culture, value, destinationType);
		}

		public override bool GetPropertiesSupported(ITypeDescriptorContext context)
		{
			if (context != null)
			{
				return true;
			}
			return base.GetPropertiesSupported(context);
		}

		public override PropertyDescriptorCollection GetProperties(ITypeDescriptorContext context, object value, Attribute[] attributes)
		{
			if (context != null)
			{
				PropertyDescriptorCollection proDcol = TypeDescriptor.GetProperties(typeof (FontFormat));
				PropertyDescriptor[] retProDarray = new PropertyDescriptor[2];
				retProDarray[0] = proDcol["Font"];
				retProDarray[1] = proDcol["Color"];
				return new PropertyDescriptorCollection(retProDarray);
			}
			return base.GetProperties(context, value, attributes);
		}
	}
}