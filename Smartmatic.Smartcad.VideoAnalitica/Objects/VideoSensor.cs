﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smartmatic.SmartCad.VideoAnalitica.Objects
{
        public class VideoSensor
        {
            private uint id;

            public uint ID
            {
                get { return id; }
                set { id = value; }
            }
            private string vmsId;

            public string VMS_ID
            {
                get { return vmsId; }
                set { vmsId = value; }
            }
      }
 }
