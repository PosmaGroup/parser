﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smartmatic.SmartCad.VideoAnalitica.Objects
{
    public class VideoInstance
    {
        #region Fields


        private long objectId;

        public enum TypeObjectEnum
        {
            ObjectInstanceOnEvent,
            ObjectInstanceOnBestVIew,
            ObjectInstance
        }

        

        private TypeObjectEnum typeObject;

        private DateTime timestampValue;

        private int timestampMilliseconds;

        private float boundingBoxLeft;

        private float boundingBoxTop;

        private float boundingBoxRight;

        private float boundingBoxButtom;

        private float footX;

        private float footY;


        #endregion

        #region Properties

        public virtual long ObjectId
        {
            get { return objectId; }
            set { objectId = value; }
        }
       
        public virtual DateTime TimestampValue
        {
            get { return timestampValue; }
            set { timestampValue = value; }
        }

        public virtual int TimestampMilliseconds
        {
            get { return timestampMilliseconds; }
            set { timestampMilliseconds = value; }
        }

        public virtual float BoundingBoxLeft
        {
            get { return boundingBoxLeft; }
            set { boundingBoxLeft = value; }
        }

        public virtual float BoundingBoxTop
        {
            get { return boundingBoxTop; }
            set { boundingBoxTop = value; }
        }

        public virtual float BoundingBoxRight
        {
            get { return boundingBoxRight; }
            set { boundingBoxRight = value; }
        }

        public virtual float BoundingBoxButtom
        {
            get { return boundingBoxButtom; }
            set { boundingBoxButtom = value; }
        }

        public virtual float FootX
        {
            get { return footX; }
            set { footX = value; }
        }

        public virtual float FootY
        {
            get { return footY; }
            set { footY = value; }
        }

        public virtual TypeObjectEnum TypeObject
        {
            get { return typeObject; }
            set { typeObject = value; }
        }

        #endregion
    }
}
