﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Smartmatic.SmartCad.VideoAnalitica.Objects;

namespace Smartmatic.SmartCad.VideoAnalitica
{
    public interface VideoAnaliticaManagerAbstract
    {        
  

        Alert[] getAlerts();

        Alert[] getAlertsByRule(string[] ruleGUID);
        
        VideoSensor getVideoSensor(uint sensor);

        Rule[] getAllRules();
        Rule getRule(uint ruleId);

        EventImage getImageByEvent(uint eventId);

        EventVideo getVideoByEvent(uint eventId);
     
        void deleteAlerts();
    }
}
