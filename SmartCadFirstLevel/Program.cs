using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading;
using System.ServiceModel;
using System.Net;
using System.Globalization;
using SmartCadGuiCommon;
using SmartCadCore.Common;
using SmartCadCore.ClientData;
using SmartCadCore.Core;
using SmartCadGuiCommon.Util;
using Smartmatic.SmartCad.Service;
using SmartCadFirstLevel.Gui;
using System.IO;
//using SmartCadServices;

namespace SmartCadFrontClient
{
    static class Program
    {
        private static TelephonyConsoleManager telephonyConsoleManager;
        private static FrontClientFormDevX frontClientForm;
        private static string ApplicationName = "First Level";

        [STAThread]
        static void Main()
        {
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.ThreadException += new ThreadExceptionEventHandler(ApplicationGuiUtil.Application_ThreadException);                
                ApplicationUtil.ActivateProcess(Process.GetCurrentProcess());

                SmartCadConfiguration.Load();
                ResourceLoader.UpdatedLanguage(new CultureInfo(SmartCadConfiguration.SmartCadSection.CultureElement.CultureLanguageElement.Name
                    + "-" + SmartCadConfiguration.SmartCadSection.CultureElement.CultureCountryElement.Name));

                object result = ServerServiceClient.GetServerService();

                if (result == null)
                {
                    MessageForm.Show(ResourceLoader.GetString2("UnableConnectToServer"), MessageFormType.Error);
                    return;
                }
                
                telephonyConsoleManager = new TelephonyConsoleManager();
                telephonyConsoleManager.Run();
                telephonyConsoleManager.WaitForService(3);

                configuration = ServerServiceClient.GetInstance().GetConfiguration();

                ConfigurationClientData cfg = ServerServiceClient.GetInstance().GetConfiguration();
                SmartCadConfiguration.SaveConfigurationInClient(cfg);
                if (!ApplicationUtil.VerifyLanguage(cfg))
                {
                    byte[] dll = ServerServiceClient.GetInstance().CheckLanguages(cfg.Language);
                    DirectoryInfo directory = Directory.CreateDirectory(SmartCadConfiguration.LanguageDirectoryClient + "\\" + cfg.Language);
                    using (FileStream fileDll = new FileStream(directory.FullName + "\\Smartmatic.SmartCad.Resources.resources.dll", FileMode.Create, FileAccess.Write))
                    {
                        fileDll.Write(dll, 0, dll.Length);
                    }
                }

                ResourceLoader.UpdatedLanguage(new CultureInfo(configuration.Language));
                ServerServiceClient.GetInstance().SetApplication(UserApplicationClientData.UserApplications.FirstLevel.ToString());
                SmartCadLoadInitialData.LoadInitialClientData();
                ApplicationGuiUtil.CheckErrorDetailButton();
                ServerServiceClient.GetInstance().ActivateApplication(ApplicationName,ApplicationUtil.GetMACAddress());
                ApplicationPreferenceClientData recordCalls = 
                    (ApplicationPreferenceClientData)ServerServiceClient.GetInstance().SearchClientObject(SmartCadHqls.GetCustomHql(SmartCadHqls.GetApplicationPreferenceByName, "RecordCalls"));
                
                bool success = false;
                do
                {
                    LoadGlobalData();
                    LoginForm login = new LoginForm(UserApplicationClientData.FirstLevel);
			
					 frontClientForm = new FrontClientFormDevX();

					if (login.ShowDialog() == DialogResult.OK)
                    {
#if !DEBUG
                        SplashForm.ShowSplash();
#endif
                        ServerServiceClient.GetInstance().NetworkCredential = login.NetworkCredential;
                        frontClientForm.NetworkCredential = login.NetworkCredential;

                        TelephonyServiceClient.GetServerService();
                        TelephonyServiceClient.GetInstance().SetConfiguration(configuration);

                        frontClientForm.RecordCalls = recordCalls == null ? false : bool.Parse(recordCalls.Value);
                        frontClientForm.SetPassword(login.Password);
                        ServerServiceClient serverServiceClient = login.ServerService;                       
                        success = true;
                        frontClientForm.ServerServiceClient = serverServiceClient;     
                        frontClientForm.ConfigurationClient = configuration;
                        frontClientForm.PostFrontClientFormDevXLoad += frontClientForm_PostFrontClientFormDevXLoad;

                        LoadInitialData(frontClientForm);

                        Application.Run(frontClientForm);

                        UnregisterPhone();
                    }
                    else
                    {
                        ServerServiceClient.GetInstance().CloseSession();
                        success = true;
                    }
                }
                while (!success);
            }
            catch (EndpointNotFoundException ex)
            {
                SplashForm.CloseSplash();
                SmartLogger.Print(ex);
                MessageForm.Show(ResourceLoader.GetString2("UnableConnectToServer"), ex);
            }
            catch (Exception ex)
            {
                SplashForm.CloseSplash();
                SmartLogger.Print(ex);
                //MessageForm.Show(ResourceLoader.GetString2("GenericErrorApplication"), ex);
                MessageForm.Show("Error: "+ex.StackTrace +", Telephony :"+ SmartCadConfiguration.TelephonyConsoleFilename+", Configuration: "+SmartCadConfiguration.ConfigFilename, ex);
            }
            finally 
            {
                if (telephonyConsoleManager != null)
                    telephonyConsoleManager.Stop();
            } 
        }

        static void frontClientForm_PostFrontClientFormDevXLoad(object sender, EventArgs e)
        {
            RegisterPhone();
#if !DEBUG
            SplashForm.CloseSplash();
#endif
        }

        private static void UnregisterPhone()
        {
            TelephonyServiceClient.GetInstance().Logout();
            Thread.Sleep(1000);
        }

        static void RegisterPhone()
        {
            try
            {
                OperatorClientData oper = ServerServiceClient.GetInstance().OperatorClient;
                TelephonyServiceClient.GetInstance().Login(
                    frontClientForm.NetworkCredential.UserName, 
                    frontClientForm.NetworkCredential.Password,
                    oper.AgentId,
                    oper.AgentPassword,
                    frontClientForm.RecordCalls,
                    configuration.Extension, false);
                timerCheckServer = StartCheckServer();
                frontClientForm.TimerCheckServer = timerCheckServer;
            }
            catch (Exception ex)
            {
                if(ex.Message !="")
                {
                    MessageForm.Show(ex.Message,ex);
                    frontClientForm.DoNotEnsureLogout = true;
                    frontClientForm.Close();
                }
                SmartLogger.Print(ex);
            }
        }

        private static void LoadInitialData(Form form)
        {
			FrontClientFormDevX frontClientForm = form as FrontClientFormDevX;

            frontClientForm.GlobalIncidentTypes = globalIncidentTypes;
            frontClientForm.GlobalDepartmentTypes = globalDepartmentTypes;
            frontClientForm.GlobalNotificationPriorities = globalNotificationPriorities;
            frontClientForm.GlobalIncidents = globalIncidents;
            frontClientForm.GlobalApplicationPreference = globalApplicationPreference;
        }

        private static Dictionary<int, IncidentTypeClientData> globalIncidentTypes;
        private static Dictionary<int, DepartmentTypeClientData> globalDepartmentTypes;
        private static Dictionary<string, IncidentNotificationPriorityClientData> globalNotificationPriorities;
        private static IList globalIncidents;
        private static Dictionary<string,ApplicationPreferenceClientData> globalApplicationPreference;

        private static void LoadGlobalData()
        {
			Dictionary<string, IList> querys = new Dictionary<string, IList>();
			querys["IncidentTypes"] = new ArrayList();
			querys["IncidentTypes"].Add(SmartCadHqls.GetCustomHql(SmartCadHqls.IncidentTypesWithDepartmentTypes));
			querys["IncidentTypes"].Add(SmartCadHqls.GetCustomHql(SmartCadHqls.QuestionsWithSetPossibleAnswers));
			querys["IncidentTypes"].Add(SmartCadHqls.GetCustomHql(SmartCadHqls.QuestionsWithUserApp));
            querys["IncidentTypes"].Add(SmartCadHqls.GetCustomHql(SmartCadHqls.IncidentTypesForApp, UserApplicationClientData.FirstLevel.Code));
			querys["DepTypes"] = new ArrayList();
			querys["DepTypes"].Add(SmartCadHqls.GetCustomHql(SmartCadHqls.GetDepartmentsTypeWithStationsAssociated));
			querys["Priorities"] = new ArrayList();
			querys["Priorities"].Add(SmartCadHqls.GetCustomHql("SELECT data FROM IncidentNotificationPriorityData data"));
			querys["apps"] = new ArrayList();
			querys["apps"].Add(SmartCadHqls.GetAllApplicationPreferences);
			querys = ServerServiceClient.GetInstance().SearchClientObjectsMaxRowsMultiQuery(true, querys);
			
			globalIncidentTypes = new Dictionary<int,IncidentTypeClientData>();
			foreach (IncidentTypeClientData incidentTypeClient in querys["IncidentTypes"])
                globalIncidentTypes.Add(incidentTypeClient.Code, incidentTypeClient);
           
            globalDepartmentTypes = new Dictionary<int, DepartmentTypeClientData>();
			foreach (DepartmentTypeClientData departmentType in querys["DepTypes"])
                globalDepartmentTypes.Add(departmentType.Code, departmentType);
            
            globalNotificationPriorities = new Dictionary<string, IncidentNotificationPriorityClientData>();
			foreach (IncidentNotificationPriorityClientData notificationPriority in querys["Priorities"])
                globalNotificationPriorities.Add(notificationPriority.CustomCode, notificationPriority);
            
            globalApplicationPreference = new Dictionary<string, ApplicationPreferenceClientData>();
			foreach (ApplicationPreferenceClientData preference in querys["apps"])
                globalApplicationPreference.Add(preference.Name, preference);            
            
			querys = new Dictionary<string, IList>();
			querys["Incidents"] = new ArrayList();
			querys["Incidents"].Add(SmartCadHqls.GetCustomHql(SmartCadHqls.NotCloseIncidentsSetIncidentTypes));
			querys["Incidents"].Add(SmartCadHqls.GetCustomHql(SmartCadHqls.NotCloseIncidents));
			querys = ServerServiceClient.GetInstance().SearchClientObjectsMaxRowsMultiQuery(true, querys);

			globalIncidents = new ArrayList();
			foreach (IncidentClientData incident in querys["Incidents"])
                if (incident.SourceIncidentApp == SourceIncidentAppEnum.FirstLevel)
           		globalIncidents.Add(incident);          
        }

        private static ConfigurationClientData configuration;

        #region Check server

        private static System.Threading.Timer timerCheckServer;

        private static System.Threading.Timer StartCheckServer()
        {
            return ApplicationUtil.RecurrentExecuteMethod(
                new SimpleDelegate(delegate
                {
                    bool successfull = false;
                    int serverTries = 0;
                    while (successfull == false)
                    {
                        try
                        {
                            ServerServiceClient.GetInstance().Ping();
                            successfull = true;
                        }
                        catch
                        {
                            serverTries++;
                            if (serverTries >= 3)
                                throw;
                            Thread.Sleep(300);
                        }
                    }

                    try
                    {
                        if (!TelephonyServiceClient.GetInstance().InVirtualCall())
                        {
                            TelephonyServiceClient.GetInstance().Ping();
                        }
                    }
                    catch (Exception ex)
                    {
                        SmartLogger.Print(ex);
                    }
                }),
                new SimpleDelegate(delegate
                {
                    timerCheckServer.Change(Timeout.Infinite, Timeout.Infinite);
                    timerCheckServer.Dispose();
                    ServerServiceClient.GetInstance().CloseSession(false);
                    DialogResult result = MessageForm.Show(ResourceLoader.GetString2("CommunicationObjectFaultedExceptionMessage"), MessageFormType.Error);
                    Process.GetCurrentProcess().Kill();
                }), null, 15000, 15000);
        }

        #endregion
    }
}
