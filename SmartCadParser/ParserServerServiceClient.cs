﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel;
using SmartCadCore.Core;
//using SmartCadParser.Services;
using Smartmatic.SmartCad.Service;

namespace SmartCadParser
{
    public class ParserServerServiceClient : ClientBase<IDeviceServerService>, IDeviceServerService
    {
        #region Constructors

        private ParserServerServiceClient(Binding binding, EndpointAddress remoteAddress)
            : base(binding, remoteAddress)
        {
        }

        #endregion

        #region Methods

        private static ParserServerServiceClient current = null;

        public static ParserServerServiceClient Current
        {
            get
            {
                return (current == null || current.State == CommunicationState.Faulted) ? GetParserServerService() : current;
            }
        }

        private static ParserServerServiceClient GetParserServerService()
        {
            if (string.IsNullOrEmpty(ParserConfiguration.SyncAppServer) == true)
                return null;

            string url = ApplicationUtil.BuildServiceUrl(
                "net.tcp",
                ParserConfiguration.SyncAppServer,
                ParserConfiguration.SyncPort,
                "Devices");

            EndpointAddress address = new EndpointAddress(url);

            NetTcpBinding binding = new NetTcpBinding(SecurityMode.None);

            //binding.MaxReceivedMessageSize = 1024 * 1024;
            binding.ReaderQuotas.MaxDepth = 1024;
            binding.ReaderQuotas.MaxArrayLength = 524288;
            binding.MaxReceivedMessageSize = 5242880;
            binding.ReaderQuotas.MaxStringContentLength = 1572864;

            ParserServerServiceClient client = new ParserServerServiceClient(binding, address);

            foreach (OperationDescription op in client.Endpoint.Contract.Operations)
            {
                DataContractSerializerOperationBehavior dataContractBehavior =
                   op.Behaviors.Remove<DataContractSerializerOperationBehavior>()
                        as DataContractSerializerOperationBehavior;

                op.Behaviors.Add(new NetDataContractSerializerOperationBehavior(op));
            }

            client.InnerChannel.OperationTimeout = TimeSpan.FromMinutes(3);

            current = client;

            return client;
        }

        #endregion

        #region IDeviceServerService Members

        public void SendData(object obj)
        {
            base.Channel.SendData(obj);
        }

        public void Ping()
        {
            base.Channel.Ping();
        }

        #endregion
    }
}
