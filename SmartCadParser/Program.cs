using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Collections;
using System.ServiceProcess;
using System.Reflection;
using System.Globalization;
using System.Net.Sockets;
using System.Diagnostics;
using System.Net;
using System.Management;
using SmartCadCore.Core;
using SmartCadCore.Common;
using Smartmatic.SmartCad.Service;

namespace SmartCadParser
{
    class Program : IDisposable
    {
        #region Fields
        
        public static bool isQueueManagerActive = true;
        private static ParserService ps = new ParserService();
        

        #endregion

        #region Main
        /// <summary>
        /// Start application as a service or as a process.
        /// </summary>
        static void Main()
        {
            
            File.Delete(Assembly.GetEntryAssembly().GetName().Name + ".txt");
            if (IsFromServiceManager())
            {
                StartService();
            }
            else
            {
                Logger.PrintToConsole = true;
                StartProgram();
            }
        }

        #endregion

        #region Methods 

        public void Dispose()
        {
        }

        /// <summary>
        /// Check if the application was started as a service.
        /// </summary>
        /// <returns></returns>
        private static bool IsFromServiceManager()
        {
            Uri uri = new Uri(Assembly.GetExecutingAssembly().EscapedCodeBase);
            FileInfo fi = new FileInfo(uri.LocalPath);

            return fi.DirectoryName != Environment.CurrentDirectory;
        }

        /// <summary>
        /// Start a service to run the application.
        /// </summary>
        private static void StartService()
        {
            ServiceBase[] servicesToRun = new ServiceBase[] { new GPSParserService() };
            ServiceBase.Run(servicesToRun);
            Process.GetCurrentProcess().Kill();
        }

        /// <summary>
        /// Run application.
        /// </summary>
        public static void StartProgram()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US", true);
            
            ParserConfiguration.Load();
            
            if (ParserConfiguration.ParserSection.Service.Port <= 0 || 
                ParserConfiguration.ParserSection.SyncUnits.AppServer.Trim() == string.Empty)
            {
                System.Windows.Forms.MessageBox.Show(ResourceLoader.GetString2("AddressPortIsNotConfigured"));
                Environment.Exit(1);
            }
            
            try
            {
                Writer.OpenOrCreateQueueFile();
            }
            catch (Exception e)
            {
                if (ParserConfiguration.ShowLog == true) 
                    Logger.Print( e.Message);
                else
                    ResourceLoader.GetString2("QueueFileException");
            }
            var result = ServerServiceClient.GetServerService();

            Synchronizer.StartSyncData();

            Thread threadWriterCheckQueue = new Thread(new ThreadStart(Writer.CheckQueue));
            threadWriterCheckQueue.Start();

            Thread t1 = new Thread(new ThreadStart(Writer.QueueManager));
            t1.Start();

            Thread t2 = new Thread(new ThreadStart(DataFrame.StartDataDllWatcher));
            t2.Start();
            
            Receiver.StartListener();

            //Check if the parser is running the receiving thread and that the DB is active or the
            //Queue is active
            Logger.Print(WMI_NlbNode.CheckNlb().ToString());
            if (WMI_NlbNode.CheckNlb() == true)
            {
                Thread threadCheckRunning = new Thread(new ThreadStart(CheckRunning));
                threadCheckRunning.Start();
            }

            try
            {
                ps.OpenHost();
            }
            catch (System.ServiceModel.AddressAlreadyInUseException ex)
            {
                System.Windows.Forms.MessageBox.Show(ResourceLoader.GetString2("PortIsAlreadyInUse"));
                Environment.Exit(1);
            }                
            catch (Exception ex)
            {
                Logger.Print("Error starting service");
                Logger.Print(ex);        
            }
            Console.ReadLine();
        }

        /// <summary>
        /// Check if all theads (Receiver, Writer & Synchronizer) are alive.
        /// </summary>
        public static void CheckRunning()
        {
            TcpListener listener = null;
            try
            {
                WMI_NlbNode node = new WMI_NlbNode();
                listener = new TcpListener(IPAddress.Parse(node.DedicatedIPAddress), ParserConfiguration.NodesPort);
                listener.Start();

                Logger.Print(ResourceLoader.GetString2("NodeStarted"));

                while (Thread.CurrentThread.IsAlive)
                {
                    if (listener.Pending() == true)
                    {
                        TcpClient client = listener.AcceptTcpClient();
                        
                        string ip = ((IPEndPoint)client.Client.RemoteEndPoint).Address.ToString();

                        try
                        {
                            if (WMI_NlbNode.AllDedicatedIPsNodes().IndexOf(ip) > -1)
                            {
                                if (Receiver.CheckRunning() == true &&
                                    (Writer.CheckRunning() == true ||
                                    Synchronizer.CheckRunning() == true))
                                {
                                    client.GetStream().WriteByte(0);
                                    client.GetStream().Close();
                                }
                                else
                                {
                                    client.GetStream().WriteByte(1);
                                    client.GetStream().Close();
                                    client.Close();
                                }
                            }
                            else
                            {
                                client.Client.Disconnect(false);
                            }
                            client.Client.Close();
                            client.Close();
                        }
                        catch (Exception e)
                        {
                            Logger.Print(e.ToString());

                            try
                            {
                                client.Client.Close();
                                client.Close();
                            }
                            catch { }
                        }
                    }
                    Thread.Sleep(1000);
                }

                listener.Stop();
            }
            catch (ManagementException me)
            {
                Logger.Print(me);
                Logger.Print(ResourceLoader.GetString2("UnavailableNodeInformation"));
                return;
            }
            catch (Exception ex)
            {
                Logger.Print(ex);
                Logger.Print(ResourceLoader.GetString2("PortOpenException"));
                return;
            }
        }

        
        #endregion
    }
}

