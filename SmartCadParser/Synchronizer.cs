using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.ServiceProcess;
using System.Collections;
using System.Collections.Generic;

using System.Data.SqlClient;

using Config = SmartCadParser.ParserConfiguration;
using SmartCadCore.Core;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using SmartCadCore.Common;

namespace SmartCadParser
{
    /// <summary>
    /// Send the processed data to the application server.
    /// </summary>
    /// <className>Synchronizer</className>
    /// <author email="arturo.aguero@smartmatic.com">Arturo Ag�ero</author>
    /// <date>2011</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    public class Synchronizer
    {
        #region Fields
        public static List<DataFrame> toSend = new List<DataFrame>();
        private static object syncToSend = new object();
        private static bool connectedToApp = false;
        private static bool isInit = true;  // TODO: quitar!! es un parche 
        private static ParserServerServiceClient client = null;
        
       
        public enum OperationToSend
        {
            Save,
            Read,
            Delete
        }

        #endregion

        #region Methods

        /// <summary>
        /// Operations to queue.
        /// </summary>
        /// <param name="operation">The operation.</param>
        /// <returns></returns>
        public static DataFrame OperationToQueue(OperationToSend operation)
        {
            return OperationToQueue(null, operation);
        }

        /// <summary>
        /// Operations to queue.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <param name="operation">The operation.</param>
        /// <returns></returns>
        public static DataFrame OperationToQueue(DataFrame data, OperationToSend operation)
        {
            lock (syncToSend)
            {
                if (operation == OperationToSend.Save)
                {
                    InsertOrReplace(data);
                    return data;
                }
                else if (operation == OperationToSend.Read)
                    return (toSend.Count > 0) ? (DataFrame)toSend[0] : null;
                else // if remove
                    data = (DataFrame)toSend[0];
                    toSend.RemoveAt(0);
                    return data;
            }
        }

        /// <summary>
        /// Starts the synchronization  of the data.
        /// </summary>
        public static void StartSyncData()
        {
            try
            {
                if (Config.SyncPort == -1 || Config.SyncAppServer == "")
                {
                    Logger.Print(ResourceLoader.GetString2("ConnectionFailed"));
                }
                else
                {
                    Thread t1 = new Thread(new ThreadStart(TryConnect));
                    t1.Start();
                }
            }
            catch (Exception ex)
            {
                if (ParserConfiguration.ShowLog == true) Logger.Print(ex.Message);
                Logger.Print(ResourceLoader.GetString2("ConnectionFailed"));
            }
        }

        /// <summary>
        /// Checks if the thread is running.
        /// </summary>
        /// <returns></returns>
        public static bool CheckRunning()
        {
            return connectedToApp;
        }

        /// <summary>
        /// Tries to connect to the application server.
        /// </summary>
        private static void TryConnect()
        {
            Logger.Print(ResourceLoader.GetString2("TrayingConnect"));
            while (Thread.CurrentThread.IsAlive)
            {
                try
                {
                    client = ParserServerServiceClient.Current;
                    client.Ping();

                    Logger.Print(ResourceLoader.GetString2("ConnectionSuccessfull"));
                    FillDataQueue();
                    SyncAllData();
                }
                catch (Exception e)
                {
                    if (connectedToApp == true)
                        if (isInit == true)
                        {
                            if (ParserConfiguration.ShowLog == true) Logger.Print(e.Message);
                            Logger.Print(ResourceLoader.GetString2("ConnectionLost"));
                            isInit = false;
                        }
                }
                finally // Siempre intenta reconectarse
                {
                    connectedToApp = false;
                    System.Threading.Thread.Sleep(Config.SyncRate * 2);
                }
            }
        }

        /// <summary>
        /// Synchronizes data in the queue.
        /// </summary>
        private static void SyncAllData() 
        {
            while (Thread.CurrentThread.IsAlive)
            {
                if (toSend.Count > 0)
                {
                    DataFrame data = OperationToQueue(OperationToSend.Read);

                    if (data != null)
                    {
                        SyncData(data);
                    }
                    OperationToQueue(data, OperationToSend.Delete);
                }
                System.Threading.Thread.Sleep(Config.SyncRate);
            }
        }

        /// <summary>
        /// Synchronize one single data.
        /// </summary>
        /// <param name="data">The data.</param>
        public static void SyncData(DataFrame data)
        {
            if (client.State == System.ServiceModel.CommunicationState.Opened)
            {                
                try
                {
                    client.SendData(data.Data);
                    if (ParserConfiguration.ShowLog == true) 
                        Logger.Print("Position sent. ID= " + data.ModemId);
                }
                catch ( Exception ex)
                {
                    if (ParserConfiguration.ShowLog == true) Logger.Print(ex.Message);
                    Logger.Print(ResourceLoader.GetString2("SyncUnitException"));
                    throw;
                }
            }
            else
            {
                throw new Exception();
            }
        }

        /// <summary>
        /// Fills the data queue with the last received data from each device.
        /// </summary>
        private static void FillDataQueue()
        {
            try
            {
                if (Writer.SqlConn == null)
                    return;
                if (Writer.SqlConn.State == System.Data.ConnectionState.Closed)
                    Writer.ConnectDB();

                while (Writer.SqlConn.State == System.Data.ConnectionState.Connecting) { Thread.Sleep(30); }

                lock (Writer.syncToWrite)
                {
                    DataFrame data = new DataFrame();

                    SqlCommand SqlCom = new SqlCommand(data.SelectAllCommand, Writer.SqlConn);

                    SqlDataReader reader = SqlCom.ExecuteReader();

                    while (reader.Read() == true)
                    {
                        data.ModemId = reader.GetString(0);
                        data.Date = reader.GetDateTime(2);
                        data.Data = reader.GetString(3);
                        
                        OperationToQueue(data, OperationToSend.Save);
                    }

                    reader.Dispose();
                }
            }
            catch (SqlException sqle)
            {
                if (Writer.isConnected == true)
                {
                    if (ParserConfiguration.ShowLog == true) Logger.Print(sqle);
                    Writer.isConnected = false;
                }

                try
                {
                    if (Writer.SqlConn != null)
                    {
                        Writer.SqlConn.Close();
                        Writer.SqlConn.Dispose();
                    }
                }
                catch { }
            }
        }

        /// <summary>
        ///Inserts the device data in the queue 
        ///or replaces the previous data if the device
        ///already have one package in the queue.
        /// </summary>
        /// <param name="data">The data.</param>
        public static void InsertOrReplace(DataFrame data) 
        {
            bool wasReplaced = false;
            int i = 0;

            foreach (DataFrame dataTemp in toSend)
            {
                if (data.ModemId == dataTemp.ModemId)
                {
                    if (data.Date > dataTemp.Date)
                    {
                        toSend[i] = data;
                    }
                    
                    wasReplaced = true;
                    break;
                }
                i++;
            }

            if (wasReplaced == false)
                toSend.Add(data);
        }
        
        #endregion
    }
}
