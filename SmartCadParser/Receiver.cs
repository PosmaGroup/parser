using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using System.Diagnostics;
using System.Collections;
using SmartCadCore.Core;
using SmartCadCore.Common;
using System.Globalization;
using System.IO;
using System.Reflection;

namespace SmartCadParser
{
    /// <summary>
    /// Receives and processes data from remote devices.
    /// </summary>
    /// <className>Receiver</className>
    /// <author email="arturo.aguero@smartmatic.com">Arturo Ag�ero</author>
    /// <date>2010</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    public class Receiver
    {
        static IList threads = new ArrayList();
        private static object obj = new object();

        #region Methods
        /// <summary>
        /// Create one listener thread for each device.
        /// </summary>
        public static void StartListener()
        {
            for (int i = 0; i < ParserConfiguration.Devices.Count; i++)
            {
                DeviceElement dev = ParserConfiguration.Devices[i];
                if (dev.Port != -1)
                {
                    Thread thread = null;

                    switch (dev.Protocol)
                    {
                        case "UDP":
                            thread = new Thread(delegate() { ListenerUDP(dev.Port, dev.Type); });
                            break;
                        case "TCP":
                            thread = new Thread(delegate() { ListenerTCP(dev.Port, dev.Type); });
                            break;
                        case "SMS":
                            thread = new Thread(delegate() { ListenerSMS(dev.Port, dev.Type); });
                            break;
                        default:
                            break;
                    }

                    if (thread != null)
                    {
                        threads.Add(thread);
                        thread.Start();
                    }
                    else
                        Logger.Print(ResourceLoader.GetString2("InvalidPortType"));

                    Thread.Sleep(200);
                }

                dev = null;
            }
        }

        /// <summary>
        /// Check if process is alive.
        /// </summary>
        /// <returns>True if process is alive</returns>
        public static bool CheckRunning()
        {
            //bool result = false;
            try
            {
                IList temp = new ArrayList(threads);
                foreach (Thread t in temp)
                {
                    if (t == null || t.IsAlive == false || t.ThreadState != System.Threading.ThreadState.Running)
                        threads.Remove(t);
                    else
                        return true;
                }
            }
            catch (Exception ex)
            {
                if (ParserConfiguration.ShowLog == true) Logger.Print(ex);
            }
            return false;
            /*
            if (treadListener != null)
            {
                result = treadListener.IsAlive == true &&
                    treadListener.ThreadState == System.Threading.ThreadState.Running;
            }
            return result;*/
        }

        /// <summary>
        /// Receives UDP packages from devices of type "type" through port "port".
        /// </summary>
        /// <param name="port">Port to listen</param>
        /// <param name="type">Device type</param>
        public static void ListenerUDP(int port, string type)
        {
            UdpClient listener = null;
            IPEndPoint groupEP = null;
            EventWaitHandle startNLB = new EventWaitHandle(false, EventResetMode.ManualReset, ParserConfiguration.ServiceName);
            try
            {
                listener = new UdpClient(port);
                groupEP = new IPEndPoint(IPAddress.Any, port);
            }
            catch (SocketException se)
            {
                if (ParserConfiguration.ShowLog == true) Logger.Print(se);
                Logger.Print(ResourceLoader.GetString2("PortOpenException"));
                return;
            }
            catch (Exception e)
            {
                if (ParserConfiguration.ShowLog == true) Logger.Print(e);
                return;
            }
            Logger.Print(ResourceLoader.GetString2("AvlServiceUp", type.ToString(), port.ToString()));
            if (WMI_NlbNode.CheckNlb() == true)
            {
                startNLB.Set();
            }
            try
            {
                while (true)
                {
                    Logger.Print("Entry into protocol UDP");
                    byte[] bytes = listener.Receive(ref groupEP);

                    Thread t = new Thread(delegate() { Attend(bytes, "UDP", type, null, groupEP); });
                    t.Start();

                    Thread.Sleep(10);
                }
            }
            catch (Exception e)
            {
                if (ParserConfiguration.ShowLog == true) Logger.Print(e);
            }
            finally
            {
                listener.Close();
            }
        }

        /// <summary>
        /// Receives TCP packages from devices of type "type" through port "port".
        /// </summary>
        /// <param name="port">Port to listen</param>
        /// <param name="type">Device type</param>
        public static void ListenerTCP(int port, string type)
        {
            Socket server = null;
            IPEndPoint groupEP = null;
            EventWaitHandle startNLB = new EventWaitHandle(false, EventResetMode.ManualReset, ParserConfiguration.ServiceName);
            try
            {
                groupEP = new IPEndPoint(IPAddress.Any, port);
                server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                server.Bind(groupEP); 
                server.Listen(10);
            }
            catch (Exception ex)
            {
                if (ParserConfiguration.ShowLog == true) Logger.Print(ex);
                Logger.Print(ResourceLoader.GetString2("PortOpenException"));
                return;
            }
            Logger.Print(ResourceLoader.GetString2("AvlServiceUp", type.ToString(), port.ToString()));
            if (WMI_NlbNode.CheckNlb() == true)
            {
                startNLB.Set();
            }
            try
            {
                while (true)
                {
                    Logger.Print("Entry into protocol TCP");
                    Socket listener = server.Accept();
                    Thread t = new Thread(delegate() { Listen(type, listener); });
                    t.Start();

                    Thread.Sleep(10);
                }
            }
            catch (Exception e)
            {
                if (ParserConfiguration.ShowLog == true) Logger.Print(e);
            }
            finally
            {
                server.Close();
            }
        }

        /// <summary>
        /// Receive SMS pakages from devices of type "type" through port "port".
        /// </summary>
        /// <param name="port">Port to listen</param>
        /// <param name="type">Device type</param>
        public static void ListenerSMS(int port, string type)
        {
            EventWaitHandle startNLB = new EventWaitHandle(false, EventResetMode.ManualReset, ParserConfiguration.ServiceName);

            if (WMI_NlbNode.CheckNlb() == true)
            {
                startNLB.Set();
            }

            while (true)
            {
                try
                {
                    int result = SMSService.Start(port, 9600);
                    if (result == 1)
                    {
                        Logger.Print("SMS Service started successfully on port COM" + port);
                    }
                    else
                    {
                        Logger.Print("The SMS service did not start successfully!");
                        return;
                    }

                    while (true)
                    {
                        List<string> msgList = SMSService.ReadMessages(true);
                        if (msgList != null)
                        {
                            foreach (string message in msgList)
                            {
                                Thread t = new Thread(delegate() { Attend(message, "SMS", type, null, null); });
                                t.Start();

                                Thread.Sleep(10);
                            }
                        }
                        Thread.Sleep(10000);
                    }
                }
                catch (Exception e)
                {
                    if (ParserConfiguration.ShowLog == true) Logger.Print(e);
                }
                finally
                {
                    SMSService.Stop();
                }
            }
        }
        private static string convertToWord(string data)
        {
            if (data.Length < 8)
            {
                data = data.Insert(0, "0");
            }
            if (data.Length < 8)
            {
                data = convertToWord(data);
            }
            return data;
        }


        /// <summary>
        /// Receives and preprocess data from socket "listener".
        /// </summary>
        /// <param name="type">Device type</param>
        /// <param name="listener">Socket with data</param>
        private static void Listen(string type, Socket listener)
        {
            DataFrame data = null;
            try
            {
                while (true)
                {
                    byte[] bytes = new byte[60000];//46808];

                    listener.Receive(bytes);

                    data = DataFrame.GetDataFrame(type, bytes);
                    if (data != null)
                    {
                        if (data.NeedClean &&
                            data.Type != DataFrame.FrameType.Sync &&
                            data.Type != DataFrame.FrameType.Incomplete)
                        {
                            IList<DataFrame> frames = new List<DataFrame>();

                            frames = data.GetCleanFrames();

                            foreach (DataFrame frame in frames)
                            {
                                Thread t = new Thread(delegate() { Attend(frame, "TCP", listener, listener.RemoteEndPoint); });
                                t.Start();
                                Thread.Sleep(5);
                            }
                            frames = null;
                        }
                        else if (data.Type != DataFrame.FrameType.Incomplete)
                        {
                            try
                            {
                                data.Decode();
                                Thread t = new Thread(delegate() { Attend(data, "TCP", listener, listener.RemoteEndPoint); });
                                t.Start();
                            }
                            catch { /*DISCARD FRAME IF ERROR*/ }
                        }
                    }
                    else
                    {
                        if (ParserConfiguration.ShowLog)
                            Logger.Print("No exists the type {0}.", type);
                    }

                    Thread.Sleep(50);
                }
            }
            catch (Exception e)
            {
                if (ParserConfiguration.ShowLog == true) Logger.Print(e.Message);
                listener.Close();
                if (data != null && !string.IsNullOrEmpty(data.ModemId))
                    Sender.RemoveConnection(data.ModemId);
            }
        }

        /// <summary>
        /// Cleans and processes the data received.
        /// </summary>
        /// <param name="bytes">Received Data</param>
        /// <param name="protocol">Protocol</param>
        /// <param name="type">Device Type</param>
        /// <param name="sock">Socket</param>
        /// <param name="remoteEp">Remote end point</param>
        public static void Attend(byte[] bytes, string protocol, string type, Socket sock, EndPoint remoteEp)
        {
            DataFrame data = DataFrame.GetDataFrame(type, bytes);
            data.Decode();
            Attend(data, protocol, sock, remoteEp);
        }

        /// <summary>
        /// Cleans and processes the data received.
        /// </summary>
        /// <param name="message">Received Data in ASCII</param>
        /// <param name="protocol">Protocol</param>
        /// <param name="type">Device Type</param>
        /// <param name="sock">Socket</param>
        /// <param name="remoteEp">Remote end point</param>
        public static void Attend(string message, string protocol, string type, Socket sock, EndPoint remoteEp)
        {
            DataFrame data = DataFrame.GetDataFrame(type, message);
            data.Decode();
            Attend(data, protocol, sock, remoteEp);
        }

        /// <summary>
        /// Cleans and processes the data received.
        /// </summary>
        /// <param name="data">Received Data</param>
        /// <param name="protocol">Protocol</param>
        /// <param name="sock">Socket</param>
        /// <param name="remoteEp">Remote end Point</param>
        public static void Attend(DataFrame data, string protocol, Socket sock, EndPoint remoteEp)
        {
            if (data != null)
            {
                try
                {
                    switch (data.Type)
                    {
                        case DataFrame.FrameType.Sync:
                            if (data.Respond == true)
                                Sender.RespondSync(data.Buffer, protocol, remoteEp, data.respondBytes);

                            if (ParserConfiguration.ShowLog == true)
                                Logger.Print("{0} Sync received", data.GetType().Name);
                            break;
                        case DataFrame.FrameType.Incomplete:
                            break;
                        case DataFrame.FrameType.None:
                            break;
                        default: // Alarm or Position
                            CultureInfo ci = new CultureInfo("es-VE");
                            Thread.CurrentThread.CurrentUICulture = ci;
                            Thread.CurrentThread.CurrentCulture = ci;

                            if (ParserConfiguration.ShowLog == true)
                                Logger.Print(ResourceLoader.GetString2("FrameReceived", data.ModemId));

                            if (data.ModemId != "")
                            {
                                Synchronizer.OperationToQueue(data, Synchronizer.OperationToSend.Save);
                                Writer.OperationToQueue(data, Writer.OperationToSave.Save);
                                if (sock != null && remoteEp != null)
                                    Sender.AddOrUpdateConnection(data.ModemId, data, sock, remoteEp);
                            }
                            break;
                    }
                }
                catch (Exception ex)
                {
                    Logger.Print(ResourceLoader.GetString2("InvalidFrameException"));
                    if (ParserConfiguration.ShowLog == true) Logger.Print(ex);
                }
                data = null;
            }
        }
        #endregion
    }
}
    