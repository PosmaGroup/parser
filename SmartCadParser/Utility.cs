using System;
using System.Collections.Generic;
using System.Text;

namespace SmartCadParser
{
    /// <summary>
    /// Contains a variety of general utilities.
    /// </summary>
    public class Utility
    {
        public static string DataBaseFormattedDate = "yyyyMMdd HH:mm:ss";

        #region Methods
        /// <summary>
        /// Converts the byte "b" into an hexadecimal string.
        /// </summary>
        /// <param name="b">The byte.</param>
        /// <returns></returns>
        public static string ToHex(byte b)
        {
            return b.ToString("X2");
        }

        /// <summary>
        /// Converts the array of bytes "buffer" into an  hexadecimal string.
        /// </summary>
        /// <param name="buffer">The buffer.</param>
        /// <param name="inicio">The start position.</param>
        /// <param name="final">The end position.</param>
        /// <returns></returns>
        public static string ToHex(byte[] buffer, int initialPos, int endPos)
        {
            StringBuilder sb = new StringBuilder();
            for (int u = initialPos; u <= endPos; u++)
            {
                sb.Append(buffer[u].ToString("X2"));
            }

            return sb.ToString();
        }

        /// <summary>
        /// Converts the whole array of bytes "buffer" into an  hexadecimal string.
        /// </summary>
        /// <param name="buffer">The buffer.</param>
        /// <returns></returns>
        public static string ToHex(byte[] buffer)
        {
            StringBuilder sb = new StringBuilder();
            for (int u = 0; u < buffer.Length ; u++)
            {
                sb.Append(buffer[u].ToString("X2"));
            }
            
            return sb.ToString();
        }

        /// <summary>
        /// Converts the decimal "decValue" into an hexadecimal string.
        /// </summary>
        /// <param name="decValue">The decimal value.</param>
        /// <returns></returns>
        public static string ToHex(int decValue)
        {
            return string.Format("{0:X}", decValue);
        }

        /// <summary>
        /// Converts the hexadecimal string "HexString" into an decimal.
        /// </summary>
        /// <param name="HexString">The hexadecimal string.</param>
        /// <returns></returns>
        public static int ToDec(string hexString)
        {
            return int.Parse(hexString, System.Globalization.NumberStyles.HexNumber);
        }

        /// <summary>
        /// Converts the hexadecimal string "HexString" into a double.
        /// </summary>
        /// <param name="HexString">The hex string.</param>
        /// <returns></returns>
        public static double ToDouble(string HexString)
        {
            int intBase = ToDec(HexString);
            return intBase / 10;
        }

        /// <summary>
        /// Converts the integer coordinates into a decimal coordinates.
        /// </summary>
        /// <param name="decValue">The dec value.</param>
        /// <returns></returns>
        public static double ToCoordinates(int decValue)
        {
            int num2 = decValue / 100000;
            double num3 = decValue - (num2 * 100000);
            num3 = num3 / (60 * 1000);
            
            return (num2 + num3);
        }

        /// <summary>
        /// Converts the conventional coordinates into a decimal coordinates.
        /// </summary>
        /// <param name="lonValue">The lon value.</param>
        /// <param name="cardinal_indicator">The cardinal_indicator.</param>
        /// <param name="degreesLon">The degrees lon.</param>
        /// <returns></returns>
        public static double ToCoordinates(string lonValue, string cardinal_indicator, int degreesLon)
        {
            //NOTA: seg�n vi en otro caso... el degreesLon puede ser 2 fijo 
            //y hay que ver donde esta el punto en el dato.
            //Si es 10.2222 entonces el m dese ser calculado desde el punto 2+1
            //Si es 1022.22 entonces el m dese ser calculado desde el punto 2
            //:)
            double d = double.Parse(lonValue.Substring(0, degreesLon));
            double m = double.Parse(lonValue.Substring(degreesLon, 2));
            double s = double.Parse(lonValue.Substring(degreesLon + 3))*60/10000;

            double dec = d + (m / 60) + (s / 3600);

            return (cardinal_indicator.ToUpper() == "N" || cardinal_indicator.ToUpper() == "E") ? dec : -dec;
        }

        /// <summary>
        /// Converts the string "str" into a byte array.
        /// </summary>
        /// <param name="str">The string.</param>
        /// <returns></returns>
        public static byte[] ToByteArray(string str)
        {
            System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
            return encoding.GetBytes(str);
        }

        /// <summary>
        /// Converts the double "d" into a byte array.
        /// </summary>
        /// <param name="d">The double.</param>
        /// <returns></returns>
        public static byte[] ToByteArray(double d)
        {
            return System.BitConverter.GetBytes(d);
        }

        /// <summary>
        /// Converts the integer "i" into a byte array.
        /// </summary>
        /// <param name="d">The integer.</param>
        /// <returns></returns>
        public static byte[] ToByteArray(int i)
        {
            return System.BitConverter.GetBytes(i);
        }
        
        #endregion
    }
}
