﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using SmartCadCore.Core;
using System.Threading;

namespace SmartCadParser
{
    /// <summary>
    /// Sends commands to devices through an existing connection.
    /// </summary>
    /// <className>Sender</className>
    /// <author email="arturo.aguero@smartmatic.com">Arturo Agüero</author>
    /// <date>2010</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    public class Sender
    {
        private static Dictionary<string, Connection> connections = new Dictionary<string,Connection>();
        private static object obj = new object();

        /// <summary>
        /// Sends the synchronization message response.
        /// Sometimes the device send a synchronization message and waits for the response.
        /// </summary>
        /// <param name="buffer">Message to send</param>
        /// <param name="protocol">Protocol to use</param>
        /// <param name="remoteEp">Device end point</param>
        /// <param name="length">Mesasge lenght</param>
        public static void RespondSync(byte[] buffer, string protocol, EndPoint remoteEp, int length)
        {
            Socket sock = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            sock.SendTo(buffer, length, SocketFlags.None, remoteEp);
        }

        /// <summary>
        /// Turn On/Off the device outputs.
        /// </summary>
        /// <param name="idGPS">GPS id</param>
        /// <param name="outputs">List of ouputs to set</param>
        /// <param name="state">True to turn it on and False to turn it off</param>
        /// <param name="seconds">Acction duration in seconds. Infinite = 0</param>
        /// <param name="times">Number of times to apply this change. (It's good to create light indicators)</param>
        /// <returns></returns>
        public static bool SetOutputs(string idGPS, System.Collections.Generic.IList<int> outputs, bool state, int seconds, int times)
        {
            int s = (state == true) ? 1 : 0;
            foreach (int pin in outputs)
            {
                if (SendCommand(idGPS, DataFrame.Command.SetOutputs, pin, s, seconds, times) == false)
                    return false;
            }
            return true;
        }

        /// <summary>
        /// Sends a request to get the device position.
        /// </summary>
        /// <param name="idGPS">GPS id</param>
        /// <returns></returns>
        public static DataFrame GetPosition(string idGPS)
        {
            SendCommand(idGPS, DataFrame.Command.GetPosition);

            /*
             * Thread.Sleep(1000);
             * 
             * 
               NOT IMPLEMENTED
             * 
             * 
             * 
            */
            return null;
        }

        /// <summary>
        /// Base method used to send any kind of message.
        /// </summary>
        /// <param name="idGPS">GPS id</param>
        /// <param name="command">Command to send</param>
        /// <param name="args">Command arguments</param>
        /// <returns></returns>
        private static bool SendCommand(string idGPS, DataFrame.Command command, params object[] args)
        {
            if (connections.ContainsKey(idGPS) == false)
                return false;

            byte[] bytes = connections[idGPS].Data.GetCommand(command, args);
            return SendBytes(bytes, connections[idGPS].Sock, connections[idGPS].RemoteEP);
        }

        /// <summary>
        /// Sends the message bytes.
        /// </summary>
        /// <param name="buffer">The buffer.</param>
        /// <param name="sock">The sock.</param>
        /// <param name="remoteEp">The device end point.</param>
        /// <returns></returns>
        private static bool SendBytes(byte[] buffer, Socket sock, EndPoint remoteEp)
        {
            for (int i = 0; i < 10; i++)
            {
                try
                {
                    sock.SendTo(buffer, remoteEp);
                    return true;
                }
                catch (Exception) { }
                Thread.Sleep(100);
            }
            return false;
        }

        /// <summary>
        /// Add (or update) the current device connection to the list of connections.
        /// </summary>
        /// <param name="modemId">The modem id.</param>
        /// <param name="data">The data.</param>
        /// <param name="sock">The sock.</param>
        /// <param name="remoteEp">The device end point.</param>
        internal static void AddOrUpdateConnection(string modemId, DataFrame data, Socket sock, EndPoint remoteEp)
        {
            lock (obj)
            {
                if (sock == null)
                    sock = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

                if (connections.ContainsKey(modemId) == true)
                {
                    connections[modemId] = new Connection(data, sock, remoteEp);
                }
                else
                {
                    connections.Add(modemId, new Connection(data, sock, remoteEp));
                }
            }
        }

        /// <summary>
        /// Removes the device connection.
        /// </summary>
        /// <param name="modemId">The modem id.</param>
        internal static void RemoveConnection(string modemId)
        {
            if (connections.ContainsKey(modemId) == true)
            {
                connections.Remove(modemId);
            }
        }
    }

    public class Connection
    {
        public Connection(DataFrame data, Socket sock, EndPoint ep)
        {
            this.Data = data;
            this.Sock = sock;
            this.RemoteEP = ep;
        }

        public DataFrame Data { get; set; }
        public Socket Sock { get; set; }
        public EndPoint RemoteEP { get; set; }
    }
}
