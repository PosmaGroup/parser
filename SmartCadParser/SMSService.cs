using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Threading;
using System.Text.RegularExpressions;

namespace SmartCadParser
{
    #region SMS Service class
    public static class SMSService
    {
        public static bool serviceActive = false;
        private static int DFLTDLY = 300, NORMDLY = 500, LNGRDLY = 1000, EXTDDLY = 5000;
        private static SerialPort port = new SerialPort();
        private static clsSMS oSMS = new clsSMS();
        private static String msgStore = "MT";
        private static int iResult, stsFlag;
        private static configMdm cm = new configMdm();

        enum stsCond : int
        {
            Inactive = 0x00, Active = 0x01, ReadMessage = 0x02, ReadFailed = 0x04, DeleteFailed = 0x08,
            InvalidPort = 0x10, StorageError = 0x20, PortSetFailure = 0x40, ReadException=0x80
        };

        private struct configMdm { public String cbPort, cbBaud, cbDBits, tbReadTO, tbWriteTO; };

        //configMdm cm = new configMdm();
        #region SMS Public Methods

        public static int Start(int sPort, int baudrate)
        {
            //open a com port connection
            cm.cbPort = String.Format("COM{0}", sPort);
            cm.cbBaud = Convert.ToString(baudrate);
            cm.cbDBits = "8";
            cm.tbReadTO = "300";
            cm.tbWriteTO = "300";

            clsSMS.msgStore = msgStore;

            return openSPort();
        }


        public static List<string> ReadMessages(bool cleanMemory)
        {
            List<string> messages = new List<string>();

            try
            {
                int mCnt;
                String respns;

                //ReadMessages from modem memory
                if ((respns = oSMS.ExecCommand(port, "AT+CPMS=\"" + msgStore + "\"", NORMDLY, "Storage set error")) == null)
                {
                    statusHandler(stsFlag |= (int)stsCond.ReadFailed);
                }

                if ((mCnt = oSMS.CountSMSmessages(port)) > 0)
                {
                    if ((messages = oSMS.ReadSMS(port, "AT+CMGL=\"ALL\"")) == null) // ALL, REC UNREAD
                    {
                        statusHandler(stsFlag |= (int)stsCond.ReadFailed);
                    }
                }

                //flush memory if cleanMemory == true
                if (cleanMemory && mCnt > 0)
                {
                    Console.WriteLine("Cleaning " + messages.Count + " messages at " + string.Format("{0:HH:mm:ss tt}", DateTime.Now) + "..");

                    foreach (string message in messages)
                    {
                        int ndx = Convert.ToInt32(message.Split(',')[0].Substring(message.IndexOf(':') + 1));
                        if (!oSMS.DeleteMsg(port, "AT+CMGD=" + ndx))
                        {
                            statusHandler(stsFlag |= (int)stsCond.DeleteFailed);
                        }
                    }
                    mCnt = 0;
                    Console.WriteLine("Remaining messages:" + oSMS.CountSMSmessages(port));
                }
                else
                    Console.WriteLine("No message in queue at " + string.Format("{0:HH:mm:ss tt}", DateTime.Now) + "..");

            }
            catch (Exception ex)
            {   statusHandler(stsFlag = (int)stsCond.ReadException);
                Console.WriteLine("Reset the ports..");
                openSPort();
                iResult = 0;
            }
            return messages;
        }
        //respns = oSMS.ExecCommand(port, "AT+CNMI=1,1", NORMDLY, "SMS notify error");

        public static void Stop()
        {   //Close the com port connection.
            oSMS.ClosePort(port);
            serviceActive = false;
        }

        private static int openSPort()
        {
            try //initialize modem
            {
                port = oSMS.OpenPort(cm.cbPort, Convert.ToInt32(cm.cbBaud), Convert.ToInt32(cm.cbDBits), Convert.ToInt32(cm.tbReadTO), Convert.ToInt32(cm.tbWriteTO));

                if (port != null)
                {
                    statusHandler(stsFlag = (int)stsCond.Active);
                }
                else
                {
                    statusHandler(stsFlag = (int)stsCond.InvalidPort);
                    return iResult = 0;
                }
                iResult = 1;
            }
            catch (Exception ex)
            {
                statusHandler(stsFlag = (int)stsCond.PortSetFailure);
                iResult = 0;
            }
            return iResult;
        }

        private static void statusHandler(int StsFlag)
        {
            switch (StsFlag)
            {
                case (int)stsCond.Active:
                    serviceActive = true;
                    break;
                case (int)stsCond.Active
                    | (int)stsCond.ReadFailed:
                    Console.WriteLine("Read message Failed!");
                    break;
                case (int)stsCond.Active
                    | (int)stsCond.StorageError:
                    Console.WriteLine("Message storage error!");
                    break;
                case (int)stsCond.Active
                    | (int)stsCond.InvalidPort:
                case (int)stsCond.InvalidPort:
                    Console.WriteLine("Invalid port setting!");
                    oSMS.ClosePort(port);
                    break;
                case (int)stsCond.PortSetFailure:
                    Console.WriteLine("Port set failure!");
                    serviceActive = false;
                    break;
                case (int)stsCond.Active
                    | (int)stsCond.DeleteFailed:
                    Console.WriteLine("Message delete failed!");
                    break;
                case (int) stsCond.ReadException:
                    Console.WriteLine("Read message exception error!");
                    oSMS.ClosePort(port);
                    break;
            }
        }
        #endregion
    }
    #endregion
    
    #region SMS Class Source
    public class clsSMS
    {
        public int DFLTDLY = 300, NORMDLY = 500, LNGRDLY = 1000, EXTDDLY = 5000;
        public static String msgStore;

        #region Open and Close Ports
        //Open Port
        public SerialPort OpenPort(string p_strPortName, int p_uBaudRate, int p_uDataBits, int p_uReadTimeout, int p_uWriteTimeout)
        {
            receiveNow = new AutoResetEvent(false); // set handler to unsignaled mode
            SerialPort port = new SerialPort();

            try
            {
                port.PortName = p_strPortName;                 //COM1
                port.BaudRate = p_uBaudRate;                   //9600
                port.DataBits = p_uDataBits;                   //8
                port.StopBits = StopBits.One;                  //1
                port.Parity = Parity.None;                     //None
                port.ReadTimeout = p_uReadTimeout;             //300
                port.WriteTimeout = p_uWriteTimeout;           //300
                port.Encoding = Encoding.GetEncoding("iso-8859-1");
                port.DataReceived += new SerialDataReceivedEventHandler(port_DataReceived); // set event handler
                port.Open();
                port.DtrEnable = true;
                port.RtsEnable = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return port;
        }

        //Close Port
        public void ClosePort(SerialPort port)
        {
            try
            {   port.Close();
                port.DataReceived -= new SerialDataReceivedEventHandler(port_DataReceived); // unset event handler
                port = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        //Execute AT Command
        public string ExecCommand(SerialPort port, string command, int responseTimeout, string errorMessage)
        {
            try
            {
                port.DiscardOutBuffer();
                port.DiscardInBuffer();
                receiveNow.Reset();  // set handler to unsignaled mode
                port.Write(command + "\r");

                string input = ReadResponse(port, responseTimeout);
                if ((input.Length == 0) || ((!input.EndsWith("\r\n> ")) && (!input.EndsWith("\r\nOK\r\n"))))
                    throw new ApplicationException("No success message was received.");

                Thread.Sleep(100); // delay to settle till next command
                return input;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Receive data from port
        public void port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                if (e.EventType == SerialData.Chars)
                {
                    receiveNow.Set();  // set handler to signaled mode
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string ReadResponse(SerialPort port, int timeout)
        {
            string buffer = string.Empty;
            try
            {
                do
                {
                    if (receiveNow.WaitOne(timeout, false))
                    {
                        string t = port.ReadExisting();
                        buffer += t;

                        //                      MessageBox.Show("Message received from device!");
                    }
                    else
                    {
                        if (buffer.Length > 0)
                            throw new ApplicationException("Response received is incomplete.");
                        else
                            throw new ApplicationException("No data received from phone.");
                    }
                }
                while (!buffer.EndsWith("\r\nOK\r\n") && !buffer.EndsWith("\r\n> ") && !buffer.EndsWith("\r\nERROR\r\n"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return buffer;
        }

        #region Count SMS
        public int CountSMSmessages(SerialPort port)
        {
            int CountTotalMessages = 0;
            try
            {   
                #region Execute Command
                String receivedData;
                if ((receivedData = ExecCommand(port, "AT", NORMDLY, "No phone connected at ")) == null) return 0;
                if ((receivedData = ExecCommand(port, "AT+CMGF=1", NORMDLY, "Failed to set message format.")) == null) return 0;
                String command = "AT+CPMS?";
                if ((receivedData = ExecCommand(port, command, LNGRDLY, "Failed to count SMS message")) == null) return 0;
                int uReceivedDataLength = receivedData.Length;
                #endregion

                #region If command is executed successfully
                if ((receivedData.Length >= 45) && (receivedData.StartsWith("AT+CPMS?")))
                {
                    #region Parsing SMS
                    string[] strSplit = receivedData.Split(',');
                    string strMessageStorageArea1 = strSplit[0];     //SM
                    string strMessageExist1 = strSplit[1];           //Msgs exist in SM
                    #endregion

                    #region Count Total Number of SMS In SIM
                    CountTotalMessages = Convert.ToInt32(strMessageExist1);
                    #endregion
                }
                #endregion

                #region If command is not executed successfully
                else if (receivedData.Contains("ERROR"))
                {
                    #region Error in Counting total number of SMS
                    string receivedError = receivedData;
                    receivedError = receivedError.Trim();
                    receivedData = "Following error occured while counting the message" + receivedError;
                    #endregion
                }
                #endregion

                return CountTotalMessages;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        #region Read SMS
        public AutoResetEvent receiveNow;

        public List<String> ReadSMS(SerialPort port, string p_strCommand)
        {
            // Set up the phone and read the messages
            List<String> messages = null;

            try
            {
                #region Execute Command
                String input;
                // Check connection
                if ((input = ExecCommand(port, "AT", 300, "No phone connected")) == null) return null;
                // Use message format "Text mode"
                if ((input = ExecCommand(port, "AT+CMGF=1", 300, "Failed to set message format.")) == null) return null;
                // Use character set "PCCP437"
                if ((input = ExecCommand(port, "AT+CSCS=\"GSM\"", 300, "Failed to set character set.")) == null) return null;
                // Select SIM storage
                if ((input = ExecCommand(port, "AT+CPMS=\"" + msgStore + "\"", 300, "Failed to select message storage.")) == null) return null;
                // Read the messages
                if ((input = ExecCommand(port, p_strCommand, 5000, "Failed to read the messages.")) == null) return null;
                #endregion

                #region Parse messages
                messages = ParseMessages(input);
                #endregion
            }
            catch (Exception ex)
            {   // throw ex;
                Console.WriteLine("Read SMS failed!");
                return null;
            }

            if (messages != null)
                return messages;
            else
                return null;
        }
        //ExecCommand(port, "AT+CSCS=\"PCCP437\"", 300, "Failed to set character set.");

        public List<String> ParseMessages(string input)
        {
            List<string> messages = new List<string>();

            try
            {
                Regex r = new Regex(@"\+CMGL:.*\r\n.*\r\n");
                Match m = r.Match(input);
                while (m.Success)
                {
                    messages.Add(m.Groups[0].Value);
                    m = m.NextMatch();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return messages;
        }

        #endregion

        #region Send SMS

        static AutoResetEvent readNow = new AutoResetEvent(false);

        public bool sendMsg(SerialPort port, string PhoneNo, string Message)
        {
            bool isSend = false;

            try
            {
                String recievedData = ExecCommand(port, "AT", 300, "No phone connected");
                recievedData = ExecCommand(port, "AT+CMGF=1", 300, "Failed to set message format.");
                String command = "AT+CMGS=\"" + PhoneNo + "\"";
                recievedData = ExecCommand(port, command, 300, "Failed to accept phoneNo");
                command = Message + char.ConvertFromUtf32(26) + "\r";
                recievedData = ExecCommand(port, command, 3000, "Failed to send message"); //3 seconds
                if (recievedData.EndsWith("\r\nOK\r\n"))
                {
                    isSend = true;
                }
                else if (recievedData.Contains("ERROR"))
                {
                    isSend = false;
                }
                return isSend;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        static void DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                if (e.EventType == SerialData.Chars)
                {
                    readNow.Set();
                    //MessageBox.Show("New message received.");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Delete SMS
        public bool DeleteMsg(SerialPort port, string p_strCommand)
        {
            bool isDeleted = false;
            try
            {
                #region Execute Command
                string recievedData = ExecCommand(port, "AT", NORMDLY, "No phone connected");
                recievedData = ExecCommand(port, "AT+CMGF=1", NORMDLY, "Failed to set message format.");
                String command = p_strCommand;
                recievedData = ExecCommand(port, command, EXTDDLY, "Failed to delete message");
                #endregion

                if (recievedData.EndsWith("\r\nOK\r\n"))
                {
                    isDeleted = true;
                }
                if (recievedData.Contains("ERROR"))
                {
                    isDeleted = false;
                }
                return isDeleted;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion
    }

    #endregion
}
