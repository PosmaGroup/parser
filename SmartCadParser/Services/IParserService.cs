﻿using SmartCadCore.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Security;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadParser.Services
{
    [ServiceContract(ProtectionLevel = ProtectionLevel.None)]
    public interface IParserService
    {
        [OperationContract()]
        bool SetOutputs(string idGPS, IList<int> outputs, bool state);

        [OperationContract(Name = "SetOutputsByTime")]
        bool SetOutputs(string idGPS, IList<int> outputs, bool state, int seconds, int times);

        [OperationContract()]
        bool SendCommand(string idGPS, string command);

        [OperationContract()]
        List<string> GetHistory(DateTime startDate, DateTime endDate, string deviceType, string deviceID, int min, int max);

        [OperationContract()]
        GPSUnitData GetLastStop(string unit, DateTime data, int minTime, int maxTime);

        [OperationContract()]
        GPSUnitData CheckPointForStop(GPSUnitData gpsUnit, int minTime, int maxTime);

        [OperationContract()]
        TimeSpan GetAverageStopTime(string unit, DateTime date, int minTime, int maxTime);

        [OperationContract()]
        IList GetRunTimePoints(string unit, DateTime start, DateTime end);

        [OperationContract()]
        double GetAverageSpeed(string unit, DateTime start, DateTime end);

        [OperationContract()]
        bool Ping();
    }
}
