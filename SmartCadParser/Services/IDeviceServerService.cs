﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Security;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace SmartCadParser.Services
{
    [ServiceContract(ProtectionLevel = ProtectionLevel.None)]
    public interface IDeviceServerService
    {
        [OperationContract()]
        void Ping();

        [OperationContract()]
        void SendData(object obj);
    }
}
