using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Reflection;

using log4net;
using System.IO;

namespace SmartCadParser
{
    #region Class Logger Documentation
    /// <summary>
    /// TODO:
    /// </summary>
    /// <className>Logger</className>
    /// <author email="atorres@smartmatic.com">Alden Torres</author>
    /// <date>2007/01/16</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    public class Logger
    {
        private static readonly ILog log;
        public static bool PrintToConsole = false;

        static Logger()
        {
            string logName;

            if (Assembly.GetEntryAssembly() != null)
                logName = Assembly.GetEntryAssembly().GetName().Name;
            else
                logName = "GenericLog";

            log = LogManager.GetLogger(logName);
        }

        public static void Info(object message)
        {
            log.Info(message);
        }

        public static void Print(string message)
        {
            string methodInfo = "";

            if (log.IsInfoEnabled == true)
            {
                StackTrace stackTrace = new StackTrace();
                StackFrame stackFrame = stackTrace.GetFrame(1);

                MethodBase methodBase = stackFrame.GetMethod();

                string className = methodBase.DeclaringType.Name;
                string methodName = methodBase.Name;

                methodInfo = className + ":-" + methodName + ":-" + message;
                log.Info(methodInfo);
            }

            if (PrintToConsole == true)
            {
                Console.WriteLine("[" + DateTime.Now.ToString("hh:mm:ss.fff") + "] " + methodInfo);
            }
        }

        public static void Print(Exception ex)
        {
            Print(ex.Message);
            Print(ex.StackTrace);
        }

        public static void Print(string message, params object[] args)
        {
            StringWriter sw = new StringWriter();
            sw.WriteLine(message, args);
            Print(sw.ToString());
            sw.Close();
            sw.Dispose();
        }
    }
}

