﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace SmartCadParser
{
    public class ServiceElement : ConfigurationElement
    {
        public ServiceElement()
        {
        }
        
        [ConfigurationProperty("port", IsRequired = true, IsKey = true)]
        public int Port
        {
            get
            {
                try
                {
                    return (int)this["port"];
                }
                catch (ConfigurationErrorsException)
                {
                    Logger.Print(SmartCadCore.Common.ResourceLoader.GetString2("AddressPortIsNotConfigured"));
                    return 0;
                }
            }
            set
            {
                this["port"] = value;
            }
        }
    }
}
