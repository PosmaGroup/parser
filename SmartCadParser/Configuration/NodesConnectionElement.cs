using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace SmartCadParser
{
    public class NodesConnectionElement : ConfigurationElement
    {
        public NodesConnectionElement()
        {
        }

        [ConfigurationProperty("port")]
        public int Port
        {
            get
            {
                return (int)this["port"];
            }
            set
            {
                this["port"] = value;
            }
        }

        [ConfigurationProperty("receiveTimeout")]
        public int ReceiveTimeout
        {
            get
            {
                return (int)this["receiveTimeout"];
            }
            set
            {
                this["receiveTimeout"] = value;
            }
        }

        [ConfigurationProperty("checkTime")]
        public int CheckTime
        {
            get
            {
                return (int)this["checkTime"];
            }
            set
            {
                this["checkTime"] = value;
            }
        }

        [ConfigurationProperty("minRunTime")]
        public int MinRunTime
        {
            get
            {
                return (int)this["minRunTime"];
            }
            set
            {
                this["minRunTime"] = value;
            }
        }

        [ConfigurationProperty("maxRestartTimes")]
        public int MaxRestartTimes
        {
            get
            {
                return (int)this["maxRestartTimes"];
            }
            set
            {
                this["maxRestartTimes"] = value;
            }
        }
    }
}
