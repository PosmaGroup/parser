﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace SmartCadParser
{
    #region Class DevicesCollection Documentation
    /// <summary>
    /// TODO:
    /// </summary>
    /// <className>DevicesCollection</className>
    /// <author email="arturo.aguero@smartmatic.com">Arturo Aguero</author>
    /// <date>2009/07/31</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    public class DevicesCollection : ConfigurationElementCollection
    {
        protected override object GetElementKey(ConfigurationElement element)
        {
            return element.ToString().GetHashCode();//((DeviceElement)element).Type;
        } 

        protected override ConfigurationElement CreateNewElement()
        {
            return new DeviceElement();
        }

        public void Add(DeviceElement device)
        {
            BaseAdd(device);
        }

        /*public new DeviceElement this[string type]
        {
            get
            {
                return BaseGet(type) as DeviceElement;
            }
        } */
    }
}
