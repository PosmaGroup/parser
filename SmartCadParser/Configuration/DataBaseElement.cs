using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace SmartCadParser
{
    public class DataBaseElement : ConfigurationElement
    {
        public DataBaseElement()
        {
        }

        [ConfigurationProperty("connectionString")]
        public string ConnectionString
        {
            get
            {
                return this["connectionString"] as string;
            }
            set
            {
                this["connectionString"] = value;
            }
        }

        [ConfigurationProperty("reconnectTime")]
        public int ReconnectTime
        {
            get
            {
                return (int)this["reconnectTime"];
            }
            set
            {
                this["reconnectTime"] = value;
            }
        }
    }
}
