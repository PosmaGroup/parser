using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace SmartCadParser
{
    public class SyncUnitsElement : ConfigurationElement
    {
        public SyncUnitsElement()
        {
        }

        [ConfigurationProperty("rate")]
        public int Rate
        {
            get
            {
                return (int)this["rate"];
            }
            set
            {
                this["rate"] = value;
            }
        }

        [ConfigurationProperty("port")]
        public int Port
        {
            get
            {
                return (int)this["port"];
            }
            set
            {
                this["port"] = value;
            }
        }

        [ConfigurationProperty("appServer")]
        public string AppServer
        {
            get
            {
                return this["appServer"] as string;
            }
            set
            {
                this["appServer"] = value;
            }
        }

    }
}
