using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;

namespace SmartCadParser
{
    #region Class ParserConfigurationSection Documentation
    /// <summary>
    /// TODO:
    /// </summary>
    /// <className>ParserConfigurationSection</className>
    /// <author email="arturo.aguero@smartmatic.com">Arturo Aguero</author>
    /// <date>2008/01/16</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    public class ParserConfigurationSection : ConfigurationSection
    {
        public ParserConfigurationSection()
        {
        }

        [ConfigurationProperty("devices", IsRequired = true)]
        [ConfigurationCollection(typeof(DevicesCollection), AddItemName = "device")]
        public DevicesCollection Devices
        {
            get
            {
                return this["devices"] as DevicesCollection;
            }
            set
            {
                this["devices"] = value;
            }
        }

        [ConfigurationProperty("culture")]
        public CultureElement CultureElement
        {
            get
            {
                return this["culture"] as CultureElement;
            }
            set
            {
                this["culture"] = value;
            }
        }

        [ConfigurationProperty("dataBase")]
        public DataBaseElement DataBase
        {
            get
            {
                return this["dataBase"] as DataBaseElement;
            }
            set
            {
                this["dataBase"] = value;
            }
        }

        [ConfigurationProperty("syncUnits")]
        public SyncUnitsElement SyncUnits
        {
            get
            {
                return this["syncUnits"] as SyncUnitsElement;
            }
            set
            {
                this["synUnits"] = value;
            }
        }

        [ConfigurationProperty("service")]
        public ServiceElement Service
        {
            get
            {
                return this["service"] as ServiceElement;
            }
            set
            {
                this["service"] = value;
            }
        }

        [ConfigurationProperty("nodesConnection")]
        public NodesConnectionElement NodesConnection
        {
            get
            {
                return this["nodesConnection"] as NodesConnectionElement;
            }
            set
            {
                this["nodesConnection"] = value;
            }
        }

        [ConfigurationProperty("showLog")]
        public BooleanElement ShowLog
        {
            get
            {
                return this["showLog"] as BooleanElement;
            }
            set
            {
                this["showLog"] = value;
            }
        }
    }
}
