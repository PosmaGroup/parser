﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace SmartCadParser
{
    public class DeviceElement : ConfigurationElement
    {
        public DeviceElement()
        {
        }

        public DeviceElement(string name)
        {
            Type = name;
        }

        [ConfigurationProperty("type", IsRequired = true, IsKey = true)]
        public string Type
        {
            get
            {
                return this["type"] as string;
            }
            set
            {
                this["type"] = value;
            }
        }

        [ConfigurationProperty("protocol", IsRequired = true)]
        public string Protocol
        {
            get
            {
                return this["protocol"] as string;
            }
            set
            {
                this["protocol"] = value;
            }
        }

        [ConfigurationProperty("port", IsRequired = true)]
        public int Port
        {
            get
            {
                return (int)this["port"];
            }
            set
            {
                this["port"] = value;
            }
        }

        public override string ToString()
        {
            return Type + Protocol + Port;
        }

        public override bool Equals(object compareTo)
        {
            DeviceElement d = compareTo as DeviceElement;
            if (d != null)
            {
                return (d.Type == this.Type && d.Protocol == this.Protocol && d.Port == this.Port);
            }
            return false;
        }
    }
}
