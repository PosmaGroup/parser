using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace SmartCadGPSParser
{
    public class ConnectionElement: ConfigurationElement
    {
        public ConnectionElement()
        {
        }

        [ConfigurationProperty("protocol")]
        public string Protocol
        {
            get
            {
                return this["protocol"] as string;
            }
            set
            {
                this["protocol"] = value;
            }
        }

        [ConfigurationProperty("port")]
        public int Port
        {
            get
            {
                return (int)this["port"];
            }
            set
            {
                this["port"] = value;
            }
        }
    }
}