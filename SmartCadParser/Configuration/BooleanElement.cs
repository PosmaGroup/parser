﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace SmartCadParser
{
    public class BooleanElement : ConfigurationElement
    {
        public BooleanElement()
        {
        }

        [ConfigurationProperty("value")]
        public bool Value
        {
            get
            {
                return (bool)this["value"];
            }
            set
            {
                this["value"] = value;
            }
        }
    }
}
