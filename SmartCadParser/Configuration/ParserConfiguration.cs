using System;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.IO;
using log4net.Config;
using System.Diagnostics;
using System.Xml;
using SmartCadCore.Core;
using SmartCadCore.Common;

namespace SmartCadParser
{
    #region Class ParserConfiguration Documentation
    /// <summary>
    /// Clase estatica para leer la configuracion y datos del parser.
    /// </summary>
    /// <className>ParserConfiguration</className>
    /// <author email="arturo.aguero@smartmatic.com">Arturo Aguero</author>
    /// <date>2008/01/18</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    #endregion
    public class ParserConfiguration
    {
        public const string Version = "4.0.9";

        public static string QUEUE_FILE_NAME = "AVLQueue.txt";

#if (DEBUG)
        public static string ConfigFilename = Path.Combine(Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.Parent.FullName, "parser\\Dist\\ParserConfiguration\\Configuration.xml");
#else
        public static string ConfigFilename = "../ParserConfiguration/Configuration.xml";
#endif
        public static string ConfigFolderFilename = "Configuration\\Configuration.xml";

        private static System.Configuration.Configuration Configuration;

        public ParserConfiguration()
        {
        }

        public static void Load()
        {
            ExeConfigurationFileMap exeConfigurationFileMap = new ExeConfigurationFileMap();
            
            exeConfigurationFileMap.ExeConfigFilename = ConfigFilename;

            Configuration =
                ConfigurationManager.OpenMappedExeConfiguration(exeConfigurationFileMap,
                ConfigurationUserLevel.None);
            
            //log4net configuration
            XmlConfigurator.Configure(new FileInfo(ConfigFilename));

        }

        public static void Load(string path)
        {
            ExeConfigurationFileMap exeConfigurationFileMap = new ExeConfigurationFileMap();

            exeConfigurationFileMap.ExeConfigFilename = Path.Combine(path, "ParserConfiguration\\Configuration.xml");

            Configuration =
                ConfigurationManager.OpenMappedExeConfiguration(exeConfigurationFileMap,
                ConfigurationUserLevel.None);

            //log4net configuration
            XmlConfigurator.Configure(new FileInfo(Configuration.FilePath));
        }

        public static ParserConfigurationSection ParserSection
        {
            get
            {
                ParserConfigurationSection configurationSection = Configuration.GetSection("parserSettings")
                    as ParserConfigurationSection;

                return configurationSection;
            }
        }

        public static string ServiceName
        {
            get
            {
                return ResourceLoader.GetString2("ParserServiceName") + Version;
            }
        }

        public static string DataBaseConnectionString
        {
            get
            {
                return ParserSection.DataBase.ConnectionString;
            }
            set
            {
                ParserSection.DataBase.ConnectionString = value;
            }
        }

        public static string DataBaseServer
        {
            get
            {
                return GetConnectionStringValue("Server");
            }
            set
            {
                SetConnectionStringValue("Server", value);
            }
        }

        public static string DataBaseName
        {
            get
            {
                return GetConnectionStringValue("Initial Catalog");
            }
            set
            {
                SetConnectionStringValue("Initial Catalog", value);
            }
        }
        
        public static void Save()
        {
            Configuration.Save();
        }
        
        private static string GetConnectionStringValue(string name)
        {
            string[] arr = ParserSection.DataBase.ConnectionString.Split(';');
            NameValueCollection collection = new NameValueCollection();

            foreach (string s in arr)
            {
                string[] temp = s.Split('=');
                if (temp.Length == 2)
                    collection.Add(temp[0], temp[1]);
            }

            return collection[name];
        }

        private static void SetConnectionStringValue(string name, string value)
        {
            string[] arr = ParserSection.DataBase.ConnectionString.Split(';');
            NameValueCollection collection = new NameValueCollection();

            foreach (string s in arr)
            {
                string[] temp = s.Split('=');
                if (temp.Length == 2)
                    collection.Add(temp[0], temp[1]);
            }

            collection[name] = value;

            string connectionString = "";

            foreach (string key in collection.Keys)
            {
                connectionString = connectionString +
                    key + "=" + collection[key] + ";";
            }

            ParserSection.DataBase.ConnectionString = connectionString;
        }

        public static List<DeviceElement> Devices
        {
            get
            {
                ParserConfigurationSection element = ParserConfiguration.ParserSection;

                List<DeviceElement> devices = new List<DeviceElement>();

                foreach (DeviceElement device in element.Devices)
                {
                    devices.Add(device);
                }

                return devices;
            }
        }

        public static DeviceElement GetDevice(string type)
        {
            foreach (DeviceElement device in ParserConfiguration.ParserSection.Devices)
            {
                if (device.Type == type) return device;
            }
            return null;
        }

        public static int SyncRate
        {
            get
            {
                return ParserSection.SyncUnits.Rate;
            }
            set
            {
                ParserSection.SyncUnits.Rate = value;
            }
        }

        public static int SyncPort
        {
            get
            {
                return ParserSection.SyncUnits.Port;
            }
            set
            {
                ParserSection.SyncUnits.Port = value;
            }
        }

        public static string SyncAppServer
        {
            get
            {
                return ParserSection.SyncUnits.AppServer;
            }
            set
            {
                ParserSection.SyncUnits.AppServer = value;
            }
        }
        public static CultureElement CultureElement
        {
            get
            {
                return ParserSection.CultureElement;
            }
            set
            {
                ParserSection.CultureElement = value;
            }
        }

        public static int NodesPort
        {
            get
            {
                return ParserSection.NodesConnection.Port;
            }
            set
            {
                ParserSection.NodesConnection.Port = value;
            }
        }

        public static int ReceiveTimeout
        {
            get
            {
                return ParserSection.NodesConnection.ReceiveTimeout;
            }
            set
            {
                ParserSection.NodesConnection.ReceiveTimeout = value;
            }
        }

        public static int CheckTime
        {
            get
            {
                return ParserSection.NodesConnection.CheckTime;
            }
            set
            {
                ParserSection.NodesConnection.CheckTime = value;
            }
        }

        public static int MinRunTime
        {
            get
            {
                return ParserSection.NodesConnection.MinRunTime;
            }
            set
            {
                ParserSection.NodesConnection.MinRunTime = value;
            }
        }

        public static int MaxRestartTimes
        {
            get
            {
                return ParserSection.NodesConnection.MaxRestartTimes;
            }
            set
            {
                ParserSection.NodesConnection.MaxRestartTimes = value;
            }
        }

        public static bool ShowLog
        {
            get
            {
                return ParserSection.ShowLog.Value;
            }
            set
            {
                ParserSection.ShowLog.Value = value;
            }
        }
    }
}
