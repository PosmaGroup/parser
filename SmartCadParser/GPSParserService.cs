using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Net.Sockets;
using SmartCadCore.Core;
using SmartCadCore.Common;
using Smartmatic.SmartCad.Service;

namespace SmartCadParser
{
    partial class GPSParserService : ServiceBase
    {
        private int restartTimes = 0;
        private EventWaitHandle startNLB = null;
        private bool stoping = false;
        private Process process;
        private WMI_NlbNode localNode = null;
        private Timer timer = null;
        private Dictionary<string, int> errorCount;
        private string ApplicationName = "Parser";

        public GPSParserService()
        {
            InitializeComponent();
            /// ParserConfiguration Configuration = new ParserConfiguration();
            this.ServiceName = ParserConfiguration.ServiceName;
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                Uri uri = new Uri(Assembly.GetExecutingAssembly().EscapedCodeBase);
                FileInfo fi = new FileInfo(uri.LocalPath);
                ParserConfiguration.Load(fi.Directory.Parent.FullName);
                //SmartCadConfiguration.Load("../Configuration/Configuration.xml");
                //var result = ServerServiceClient.GetServerService();
                //ServerServiceClient.GetInstance().ActivateApplication(ApplicationName, ApplicationUtil.GetMACAddress());

                if (WMI_NlbNode.CheckNlb() == true)
                {
                    WMI_NlbNode.AllComputerNameNodes();
                    WMI_NlbNode.AllDedicatedIPsNodes();
                    WMI_NlbNode.AllNodes();
                    localNode = new WMI_NlbNode();
                    errorCount = new Dictionary<string, int>();
                    timer = new Timer(new TimerCallback(CheckAllNodes), null, -1, -1);
                    startNLB = new EventWaitHandle(false, EventResetMode.ManualReset, ParserConfiguration.ServiceName);
                }

                process = new Process();
                process.StartInfo = new ProcessStartInfo();
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.CreateNoWindow = true;
                process.StartInfo.WorkingDirectory = fi.DirectoryName;
                process.StartInfo.FileName = uri.LocalPath;
                process.EnableRaisingEvents = true;
                process.Exited += new System.EventHandler(process_Exited);
                process.Start();

                if (WMI_NlbNode.CheckNlb() == true)
                {
                    if (startNLB.WaitOne(5000, false) == true)
                    {
                        localNode.Start();
                    }
                    timer.Change(0, ParserConfiguration.CheckTime);
                }
            }
            catch (Exception ex)
            {
                Logger.Print(ex.ToString());
            }
        }

        protected override void OnStop()
        {
            try
            {
                if (WMI_NlbNode.CheckNlb() == true)
                {
                    localNode.Stop();
                    timer.Change(-1, -1);
                    timer.Dispose();
                    startNLB.Close();
                }
                stoping = true;
                process.Kill();
            }
            catch
            {
            }
        }

        private void process_Exited(object sender, EventArgs args)
        {
            try
            {
                TimeSpan runTime = process.ExitTime.Subtract(process.StartTime);

                if (runTime.TotalMinutes < ParserConfiguration.MinRunTime)
                    restartTimes++;
                else
                    restartTimes = 0;

                if (restartTimes > ParserConfiguration.MaxRestartTimes)
                {
                    Stop();
                    Logger.Print(ResourceLoader.GetString2("ProcessStop", restartTimes.ToString()));
                }

                if (!stoping)
                {
                    Uri uri = new Uri(Assembly.GetExecutingAssembly().EscapedCodeBase);
                    FileInfo fi = new FileInfo(uri.LocalPath);

                    process = new Process();
                    process.StartInfo = new ProcessStartInfo();
                    process.StartInfo.UseShellExecute = false;
                    process.StartInfo.CreateNoWindow = true;
                    process.StartInfo.WorkingDirectory = fi.DirectoryName;
                    process.StartInfo.FileName = uri.LocalPath;
                    process.EnableRaisingEvents = true;
                    process.Exited += new System.EventHandler(process_Exited);
                    process.Start();
                }
            }
            catch (Exception ex)
            {
                Logger.Print(ex.ToString());
            }
        }

        private void CheckAllNodes(object data)
        {
            Uri uri = new Uri(Assembly.GetExecutingAssembly().EscapedCodeBase);
            FileInfo fi = new FileInfo(uri.LocalPath);
            ParserConfiguration.Load(fi.Directory.Parent.FullName);

            foreach (WMI_NlbNode node in WMI_NlbNode.AllNodes())
            {
                if (errorCount.ContainsKey(node.ComputerName) == false)
                    errorCount.Add(node.ComputerName, 0);
                try
                {
                    TcpClient client = new TcpClient();
                    try
                    {
                        client.Connect(node.ComputerName, ParserConfiguration.NodesPort);
                        client.ReceiveTimeout = ParserConfiguration.ReceiveTimeout;
                        int result = client.GetStream().ReadByte();
                        node.Refresh();
                        if (result == 1)
                        {
                            if (node.StartingOrRunning == true)
                                errorCount[node.ComputerName] += 1;
                        }
                        else if (result == 0)
                        {
                            errorCount[node.ComputerName] = 0;
                            if (node.StartingOrRunning == false)
                            {
                                try
                                {
                                    node.Start();
                                    Logger.Print(ResourceLoader.GetString2("NodeStarted", node.ComputerName));
                                }
                                catch { }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        node.Refresh();
                        if (node.Running == true)
                        {
                            errorCount[node.ComputerName] += 1;
                        }
                        else
                        {
                            errorCount[node.ComputerName] = 0;
                        }

                        if (errorCount[node.ComputerName] > ParserConfiguration.MaxRestartTimes)
                        {
                            if (ex is ArgumentNullException)
                            {
                                Logger.Print(ResourceLoader.GetString2("InvalidMachineName", node.ComputerName));
                                Logger.Print(ex);
                            }
                            else if (ex is ArgumentOutOfRangeException)
                            {
                                Logger.Print(ResourceLoader.GetString2("InvalidPort", ParserConfiguration.NodesPort.ToString()));
                                Logger.Print(ex);
                            }
                            else if (ex is SocketException)
                            {
                                Logger.Print(ResourceLoader.GetString2("SocketException", ((SocketException)ex).SocketErrorCode.ToString()));
                                Logger.Print(ex);
                            }
                            else if (ex is ObjectDisposedException)
                            {
                                Logger.Print(ResourceLoader.GetString2("TcpClientException"));
                                Logger.Print(ex);
                            }
                            else
                            {
                                Logger.Print(ex);
                            }
                        }
                    }
                    finally
                    {
                        try
                        {
                            if (client.Connected)
                            {
                                client.Client.Disconnect(true);
                                client.GetStream().Close();
                            }
                            client.Client.Close();
                            client.Close();
                        }
                        catch { }
                    }
                    if (errorCount[node.ComputerName] > ParserConfiguration.MaxRestartTimes)
                    {
                        errorCount[node.ComputerName] = 0;
                        Logger.Print(ResourceLoader.GetString2("NodeStoped", node.ComputerName));

                        node.Stop();
                    }
                }
                catch
                { }
            }
        }
    }
}
