﻿using System;
using System.ServiceModel;
using System.ServiceModel.Channels;

using SmartCadCore.Core;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using System.Threading;
using System.Globalization;
using System.ServiceModel.Description;
using SmartCadCore.Common;
using Smartmatic.SmartCad.Service;
//using SmartCadParser.Services;

namespace SmartCadParser
{
    /// <summary>
    /// Parser Service
    /// </summary>
    /// <className>ParserService</className>
    /// <author email="arturo.aguero@smartmatic.com">Arturo Agüero</author>
    /// <date>2011</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class ParserService : IParserService
    {
        private ServiceHost host;

        #region Host

        public void OpenHost()
        {
            if (host == null)
            {
                try
                {
                    String address = ApplicationUtil.BuildServiceUrl(
                        "net.tcp",
                        "localhost",
                        ParserConfiguration.ParserSection.Service.Port,
                        "Parser");


                    NetTcpBinding binding = new NetTcpBinding(SecurityMode.None);
					binding.ReaderQuotas.MaxDepth = 1024;
					binding.ReaderQuotas.MaxArrayLength = 524288;
					binding.MaxReceivedMessageSize = 5242880;
					binding.ReaderQuotas.MaxStringContentLength = 1572864;
                    
					host = ServiceHostBuilder.ConfigureSimpleHost(typeof(ParserService), typeof(IParserService), binding, address);

                    ServiceThrottlingBehavior serviceThrottlingBehavior = new ServiceThrottlingBehavior();

                    serviceThrottlingBehavior.MaxConcurrentCalls = 200; 
                    serviceThrottlingBehavior.MaxConcurrentInstances = 200;
                    serviceThrottlingBehavior.MaxConcurrentSessions = 200;

                    host.Description.Behaviors.Add(serviceThrottlingBehavior);
                    host.Open();
                } 
                catch(Exception) 
                {
                    throw;
                }
                
            }
        }

        public void CloseHost()
        {
            try
            {
                if (host != null)
                    host.Close();
            }
            catch { }
        }

        #endregion

        #region IParserService Members

        /// <summary>
        /// Sets the outputs.
        /// </summary>
        /// <param name="idGPS">The id GPS.</param>
        /// <param name="outputs">The outputs.</param>
        /// <param name="state">if set to <c>true</c> [state].</param>
        /// <returns></returns>
        public bool SetOutputs(string idGPS, IList<int> outputs, bool state)
        {
            return SetOutputs(idGPS, outputs, state, 0, 1);
        }

        /// <summary>
        /// Sets the outputs.
        /// </summary>
        /// <param name="idGPS">The id GPS.</param>
        /// <param name="outputs">The outputs.</param>
        /// <param name="state">if set to <c>true</c> [state].</param>
        /// <param name="seconds">The seconds.</param>
        /// <param name="times">The times.</param>
        /// <returns></returns>
        public bool SetOutputs(string idGPS, IList<int> outputs, bool state, int seconds, int times)
        {
            return Sender.SetOutputs(idGPS, outputs, state, seconds, times);
        }

        /// <summary>
        /// Sends commands.
        /// </summary>
        /// <param name="idGPS">The id GPS.</param>
        /// <param name="command">The command.</param>
        /// <returns></returns>
        public bool SendCommand(string idGPS, string command)
        {
            throw new NotImplementedException();
        }



        /// <summary>
        /// Gets the units position history.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="deviceType">The type of the device to look for</param>
        /// <param name="deviceID">The ID of the device to look for</param>
        /// <param name="min">The min.</param>
        /// <param name="max">The max.</param>
        /// <returns></returns>
		public List<string> GetHistory(DateTime startDate, DateTime endDate, string deviceType, string deviceID, int min, int max)
        {
            List<string> positions = new List<string>();
            SqlConnection SqlConn = null;
            SqlCommand SqlCom = null;
            SqlDataReader reader = null;

			try
			{
				SqlConn = new SqlConnection(ParserConfiguration.DataBaseConnectionString + "Connection Timeout=2");
				SqlConn.Open();

				while (SqlConn.State == ConnectionState.Connecting) { Thread.Sleep(30); }
				try
				{
                    string SQL = string.Empty;

                    if (deviceType == "GPS")
                    {
                        SQL = SmartCadSQL.GetCustomSQL(
                                        @"SELECT  frame 
										FROM  ( SELECT ROW_NUMBER() OVER ( ORDER BY date ) AS RowNum, * 
												FROM [history] 	
												WHERE id = '{0}' AND type = '{1}' 
                                                    AND date BETWEEN '{2}' AND '{3}') AS RowConstrainedResult 
										WHERE RowNum BETWEEN {4} AND {5} 
										ORDER BY RowNum",
                                        deviceID,
                                        deviceType,
                                        ApplicationUtil.GetDataBaseFormattedDate(startDate),
                                        ApplicationUtil.GetDataBaseFormattedDate(endDate),
                                        min,
                                        max - 1);
                    }
                    else
                    {
                        SQL = SmartCadSQL.GetCustomSQL(
                                        @"SELECT  frame 
										FROM  ( SELECT ROW_NUMBER() OVER ( ORDER BY date desc) AS RowNum, * 
												FROM [history] 	
												WHERE id = '{0}' AND type = '{1}' 
                                                    AND date BETWEEN '{2}' AND '{3}') AS RowConstrainedResult 
										WHERE RowNum BETWEEN {4} AND {5} 
										ORDER BY RowNum",
                                        deviceID,
                                        deviceType,
                                        ApplicationUtil.GetDataBaseFormattedDate(startDate),
                                        ApplicationUtil.GetDataBaseFormattedDate(endDate),
                                        min,
                                        max - 1);
                    }

					SqlCom = new SqlCommand(SQL, SqlConn);
					reader = SqlCom.ExecuteReader();
					int i = 0;

					while (reader.Read())
					{
                        positions.Add((string)reader.GetValue(0));
					}
				}
				catch (Exception ex)
				{
					SmartLogger.Print(ex);
				}
				finally
				{
					reader.Dispose();
					SqlCom.Dispose();
				}
			}
			catch (Exception e)
			{
				SmartLogger.Print(e);
			}
            finally
            {
                SqlConn.Dispose();
            }
			return positions;
        }

        /// <summary>
        /// Gets the average stop time.
        /// </summary>
        /// <param name="unit">The unit.</param>
        /// <param name="date">The date.</param>
        /// <param name="minTime">The min time.</param>
        /// <param name="maxTime">The max time.</param>
        /// <returns></returns>
        public TimeSpan GetAverageStopTime(string unit, DateTime date, int minTime, int maxTime)
        {
            SqlConnection SqlConn = null;
            SqlCommand SqlCom = null;
            SqlDataReader reader = null;

            TimeSpan result = new TimeSpan();
            try
            {
                SqlConn = new SqlConnection(ParserConfiguration.DataBaseConnectionString + "Connection Timeout=2");

                SqlConn.Open();
                while (SqlConn.State == System.Data.ConnectionState.Connecting) { Thread.Sleep(30); }

                try
                {
                    string SQL = SmartCadSQL.GetCustomSQL(
                                            @"SELECT AVG(datediff(ss,h2.date,h1.date)) 
											FROM history h1, history h2  
											WHERE h1.id = '{0}'  AND 
												h2.id = '{0}' AND 
                                                h1.type = '{1}' AND 
                                                h2.type = '{1}' AND 
												datediff(ss,h2.date,h1.date) BETWEEN {2} AND {3} AND 
												h1.date > h2.date AND h1.date < '{4}'",
                                            unit,
                                            "GPS",
                                            minTime,
                                            maxTime,
                                            ApplicationUtil.GetDataBaseFormattedDate(date));

                    SqlCom = new SqlCommand(SQL, SqlConn);
                    reader = SqlCom.ExecuteReader();

                    while (reader.Read() && reader.GetValue(0) != null)
                        result = new TimeSpan(0, (int)reader.GetValue(0) / 60, (int)reader.GetValue(0) % 60);
                }
                catch (Exception ex)
                {
                    SmartLogger.Print(ex);
                }
                finally
                {
                    reader.Dispose();
                    SqlCom.Dispose();
                }
            }
            catch (Exception e)
            {
                SmartLogger.Print(e);
            }
            finally
            {
                SqlConn.Dispose();
            }
            return result;
        }

        /// <summary>
        /// Gets the last stop.
        /// </summary>
        /// <param name="unit">The unit.</param>
        /// <param name="date">The date.</param>
        /// <param name="minTime">The min time.</param>
        /// <param name="maxTime">The max time.</param>
        /// <returns></returns>
        public GPSUnitData GetLastStop(string unit, DateTime date, int minTime, int maxTime)
		{
			SqlConnection SqlConn = null;
			SqlCommand SqlCom = null;
			SqlDataReader reader = null;

            GPSUnitData result = null;
			try
			{
				SqlConn = new SqlConnection(ParserConfiguration.DataBaseConnectionString + "Connection Timeout=2");

				SqlConn.Open();
				while (SqlConn.State == System.Data.ConnectionState.Connecting) { Thread.Sleep(30); }

				try
				{
					string SQL = SmartCadSQL.GetCustomSQL(
                                            @"SELECT h1.frame, h2.frame, datediff(ss,h2.date,h1.date)  
											FROM history h1, history h2  
											WHERE h1.id = '{0}'  AND 
												h2.id = '{0}' AND 
                                                h1.type = '{1}' AND
                                                h2.type = '{1}' AND
                                                h1.date > h2.date AND h1.date < '{4}' AND 
												datediff(ss,h2.date,h1.date) BETWEEN {2} AND {3} 
											ORDER BY h1.date DESC",
											unit,
                                            "GPS",
											minTime,
											maxTime,
											ApplicationUtil.GetDataBaseFormattedDate(date));

					SqlCom = new SqlCommand(SQL, SqlConn);
					reader = SqlCom.ExecuteReader();

					while (reader.Read())
					{
                        GPSUnitData data1 = new GPSUnitData(reader.GetValue(0).ToString(), (int)reader.GetValue(2));
                        GPSUnitData data2 = new GPSUnitData(reader.GetValue(1).ToString());

                        if (Math.Round(data1.lat, 4) == Math.Round(data2.lat, 4))
                        {
                            if(result == null || result.date < data1.date)
                                result = data1;
                        }
					}
				}
				catch (Exception ex)
				{
					SmartLogger.Print(ex);
				}
				finally
				{
					reader.Dispose();
					SqlCom.Dispose();
				}
			}
			catch (Exception e)
			{
				SmartLogger.Print(e);
			}
			finally
			{
				SqlConn.Dispose();
			}
			return result;
		}

        /// <summary>
        /// Checks the point for stop.
        /// </summary>
        /// <param name="gpsUnit">The GPS unit.</param>
        /// <param name="minTime">The min time.</param>
        /// <param name="maxTime">The max time.</param>
        /// <returns></returns>
        public GPSUnitData CheckPointForStop(GPSUnitData gpsUnit, int minTime, int maxTime)
		{
			SqlConnection SqlConn = null;
			SqlCommand SqlCom = null;
			SqlDataReader reader = null;

            GPSUnitData gps = null;

            try
			{
				SqlConn = new SqlConnection(ParserConfiguration.DataBaseConnectionString + "Connection Timeout=2");

				SqlConn.Open();
				while (SqlConn.State == System.Data.ConnectionState.Connecting) { Thread.Sleep(30); }

				try
				{
                    string SQL = SmartCadSQL.GetCustomSQL(
                                @"SELECT COUNT(*)
                                  FROM
                                (SELECT  COUNT(*) as result
                                    FROM  history                             
                                    WHERE id = '{0}' AND                                     
                                        type = '{1}' AND                                                                 
                                        '{2}' > date AND                                               
                                        frame like '%{3}%' AND                            
                                        frame like '%{4}%' AND                             
                                        datediff(ss,date,'{2}') BETWEEN 0 AND {6}) as SamePosition,
                                (SELECT  COUNT(*) as result
                                    FROM  history                             
                                    WHERE id = '{0}' AND                                     
                                        type = '{1}' AND                                                                 
                                        '{2}' > date AND                                                                         
                                        datediff(ss,date,'{2}') BETWEEN 0 AND {6}) as AllPositions
                                 WHERE
                                 SamePosition.result = AllPositions.result and SamePosition.result > 0",
                                gpsUnit.idGPS,
                                "GPS",
                                ApplicationUtil.GetDataBaseFormattedDate(gpsUnit.date),
                                gpsUnit.lon.ToString(new CultureInfo("es-VE")).Substring(0, 7),
                                gpsUnit.lat.ToString(new CultureInfo("es-VE")).Substring(0, 7),
                                minTime,
                                maxTime);

//                string SQL = SmartCadSQL.GetCustomSQL(
//                                @"SELECT top 1 frame
//	                            FROM  history 
//	                            WHERE id = '{0}' AND 
//                                    type = '{1}' AND 		                            
//                                    '{2}' > date AND                   
//		                            frame like '%{3}%' AND		
//		                            frame like '%{4}%' AND 
//		                            datediff(ss,date,'{2}') BETWEEN {5} AND {6}
//	                            ORDER BY date desc",
//                                gpsUnit.idGPS,
//                                "GPS",
//                                ApplicationUtil.GetDataBaseFormattedDate(gpsUnit.date),
//                                gpsUnit.lon.ToString(new CultureInfo("es-VE")).Substring(0,7),
//                                gpsUnit.lat.ToString(new CultureInfo("es-VE")).Substring(0, 7),
//                                minTime,
//                                maxTime);

					SqlCom = new SqlCommand(SQL, SqlConn);
					reader = SqlCom.ExecuteReader();

                    if (reader.Read())
                    {
                        if ((int)reader.GetValue(0) > 0)
                            gps = gpsUnit;
                    }
				}
				catch (Exception ex)
				{
					SmartLogger.Print(ex);
				}
				finally
				{
					reader.Dispose();
					SqlCom.Dispose();
				}
			}
			catch (Exception e)
			{
				SmartLogger.Print(e);
			}
			finally
			{
				SqlConn.Dispose();
			}

			return gps;
		}

        /// <summary>
        /// Gets the run time points.
        /// </summary>
        /// <param name="unit">The unit.</param>
        /// <param name="start">The start.</param>
        /// <param name="end">The end.</param>
        /// <returns></returns>
		public IList GetRunTimePoints(string unit, DateTime start, DateTime end)
		{
			SqlConnection SqlConn = null;
			SqlCommand SqlCom = null;
			SqlDataReader reader = null;

			IList result = new ArrayList();
			try
			{
				SqlConn = new SqlConnection(ParserConfiguration.DataBaseConnectionString + "Connection Timeout=2");

				SqlConn.Open();
				while (SqlConn.State == System.Data.ConnectionState.Connecting) { Thread.Sleep(30); }

				try
				{
					string SQL = SmartCadSQL.GetCustomSQL(
                                            @"SELECT frame 
												FROM [history]
												WHERE type = 'GPS' AND id = '{0}'  AND 
												date BETWEEN '{1}' AND '{2}'",
											unit,
											ApplicationUtil.GetDataBaseFormattedDate(start),
											ApplicationUtil.GetDataBaseFormattedDate(end));

					SqlCom = new SqlCommand(SQL, SqlConn);
					reader = SqlCom.ExecuteReader();

					while (reader.Read())
					{
						GPSUnitData gpsUnit = new GPSUnitData((string)reader.GetValue(0));

						result.Add(gpsUnit);
					}
				}
				catch (Exception ex)
				{
					SmartLogger.Print(ex);
				}
				finally
				{
					reader.Dispose();
					SqlCom.Dispose();
				}
			}
			catch (Exception e)
			{
				SmartLogger.Print(e);
			}
			finally
			{
				SqlConn.Dispose();
			}
			return result;
		}

        /// <summary>
        /// Gets the average speed.
        /// </summary>
        /// <param name="unit">The unit.</param>
        /// <param name="start">The start.</param>
        /// <param name="end">The end.</param>
        /// <returns></returns>
		public double GetAverageSpeed(string unit, DateTime start, DateTime end)
		{
			SqlConnection SqlConn = null;
			SqlCommand SqlCom = null;
			SqlDataReader reader = null;

			double result = 0.0;
			try
			{
				SqlConn = new SqlConnection(ParserConfiguration.DataBaseConnectionString + "Connection Timeout=2");

				SqlConn.Open();
				while (SqlConn.State == System.Data.ConnectionState.Connecting) { Thread.Sleep(30); }

				try
				{
					string SQL = SmartCadSQL.GetCustomSQL(
											@"SELECT frame 
											FROM history   
											WHERE type = 'GPS' AND id = '{0}'  AND 
												date BETWEEN '{1}' AND '{2}'",
											unit,
											ApplicationUtil.GetDataBaseFormattedDate(start),
											ApplicationUtil.GetDataBaseFormattedDate(end));

					SqlCom = new SqlCommand(SQL, SqlConn);
					reader = SqlCom.ExecuteReader();

                    int i = 0;
                    while (reader.Read())
                    {
                        result += double.Parse(reader.GetValue(0).ToString().Split('|')[6]);
                        i++;
                    }
                    result /= i;
				}
				catch (Exception ex)
				{
					SmartLogger.Print(ex);
				}
				finally
				{
					reader.Dispose();
					SqlCom.Dispose();
				}
			}
			catch (Exception e)
			{
				SmartLogger.Print(e);
			}
			finally
			{
				SqlConn.Dispose();
			}
			return result;
		}

        /// <summary>
        /// Pings this instance.
        /// </summary>
        /// <returns></returns>
		public bool Ping()
        {
            return true;
        }

        #endregion
    }
}
