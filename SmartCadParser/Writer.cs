using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Collections;

using System.Data.SqlClient;
using SmartCadCore.Core;
using SmartCadCore.Common;

namespace SmartCadParser
{
    /// <summary>
    /// Writes the processed data in the DataBase
    /// or in a temporal file while the DataBase connection is recovered.
    /// </summary>
    /// <className>Synchronizer</className>
    /// <author email="arturo.aguero@smartmatic.com">Arturo Ag�ero</author>
    /// <date>2011</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    public class Writer
    {
        #region Fields
        public static bool isConnected = true;
        public static SqlConnection SqlConn = null;
        private static SqlCommand SqlCom = null;
        public static Queue toSave = new Queue();
        private static object syncToSave = new object();
        public static object syncToWrite = new object();
        private static object syncToWriteFile = new object();
        private static Mutex fileMut = new Mutex();
        private static Timer idle = new Timer(new TimerCallback(Writer.ConnectionIdle));
        private static string QueueFileName = ParserConfiguration.QUEUE_FILE_NAME;

        public enum OperationToSave
        {
            Save,
            Read,
            Delete
        }
        #endregion

        #region Methods
        /// <summary>
        /// Checks if the thread is running.
        /// </summary>
        /// <returns></returns>
        public static bool CheckRunning()
        { 
            FileStream stream = null;
            bool running = false;
            
            try
            {
                if (isConnected == true || ConnectDB() == true)
                {
                    running = true;
                    if (ParserConfiguration.ShowLog == true) Logger.Print("running = true");
                }
                else 
                {                    
                    try
                    {
                        OpenOrCreateQueueFile();
                        lock (syncToWriteFile)
                        {
                            stream = File.OpenWrite(ParserConfiguration.QUEUE_FILE_NAME);
                            if (stream.CanWrite == false)
                            {
                                stream.Close();
                                throw new Exception();
                            }
                        }  
                        running = true;
                        if (ParserConfiguration.ShowLog == true) Logger.Print("running = true");
                    }
                    catch
                    {                        
                        Logger.Print(ResourceLoader.GetString2("CanNotOpenQueueFile"));
                        try
                        {
                            lock (syncToWriteFile)
                            {
                                File.Delete(ParserConfiguration.QUEUE_FILE_NAME);
                            }
                            OpenOrCreateQueueFile();
                            running = true;
                        }
                        catch
                        {
                            Logger.Print(ResourceLoader.GetString2("CreatingNewQueueFile"));
                            
                            OpenOrCreateQueueFile(true);
                            
                            Logger.Print(ResourceLoader.GetString2("NewQueueFileCreated"));
                            running = true;
                        }
                    }
                }
            }
            catch 
            {
                if (ParserConfiguration.ShowLog == true) Logger.Print("running = false");
                running = false;
            }
            finally
            {
                try
                {
                    if (stream != null)
                        stream.Close();
                }
                catch { }
            }
            return running;
        }

        /// <summary>
        /// Operations to queue.
        /// </summary>
        /// <param name="operation">The operation.</param>
        /// <returns></returns>
        public static DataFrame OperationToQueue(OperationToSave operation)
        {
            return OperationToQueue(null, operation);   
        }

        /// <summary>
        /// Operations to queue.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <param name="operation">The operation.</param>
        /// <returns></returns>
        public static DataFrame OperationToQueue(DataFrame data, OperationToSave operation)
        {
            lock (syncToSave)
            {
                if (operation == OperationToSave.Save)
                {
                    toSave.Enqueue(data);
                    return data;
                }
                else if (operation == OperationToSave.Read)
                    return (toSave.Count > 0) ? (DataFrame)toSave.Peek() : null;
                else
                    return (toSave.Count > 0) ?(DataFrame)toSave.Dequeue() : null;
            }
        }

        /// <summary>
        /// Close the sql connection.
        /// </summary>
        /// <param name="info">The info.</param>
        private static void ConnectionIdle(object info)
        {
            if (SqlConn != null)
                SqlConn.Close();
        }

        /// <summary>
        /// Checks if exists data to save.
        /// </summary>
        public static void CheckQueue()
        {
            while (true)
            {
                try
                {
                    if (toSave.Count > 0)
                    {

                        if (ConnectDB())
                        {
                            Logger.Print("Entrando a la BD");
                            CopyToDB();
                            Logger.Print("Copio en la base de datos");
                            CloseConnection();
                        }
                        else
                            CopyToFile(OperationToQueue(OperationToSave.Delete));

                       
                        if (SqlConn != null)
                        {
                            SqlConn.Close();
                            SqlConn.Dispose();
                        }
                    }
                    else
                    {
                        Thread.Sleep(100);
                    }
                }
                catch (Exception e){
                    Logger.Print("Exception BD:  " + e);
                }
            }

        }

        /// <summary>
        /// Copies data to a temporal file.
        /// </summary>
        /// <param name="data">The data.</param>
        private static void CopyToFile(DataFrame data)
        {
            try
            {   
                OpenOrCreateQueueFile();
                lock (syncToWriteFile)
                {

                    StreamWriter writer = File.AppendText(ParserConfiguration.QUEUE_FILE_NAME);

                    writer.Write(data.ModemId);
                    writer.Write("\t" + data.NameType);
                    writer.Write("\t" + data.Date.ToString(Utility.DataBaseFormattedDate));
                    writer.WriteLine("\t" + data.Data);

                    writer.Close();
                }
            }
            catch(Exception ex) 
            {
                if (ParserConfiguration.ShowLog == true) Logger.Print(ex);
                Logger.Print(ResourceLoader.GetString2("CanNotOpenQueueFile"));
                throw;
            }
        }

        /// <summary>
        /// Copies data to the data base.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <returns></returns>
        private static bool CopyToDB(params DataFrame[] args)
        {
            bool result = false;
            try
            {
                if (args.Length > 0)
                    ExecuteQuery(args[0]);
                else
                    ExecuteQuery();
                result = true;
            }
            catch (Exception ex)
            {
                if (Program.isQueueManagerActive == false)
                {
                    Program.isQueueManagerActive = true;
                    Logger.Print(ResourceLoader.GetString2("QueueToDiskInit"));

                    Thread t = new Thread(new ThreadStart(QueueManager));
                    t.Start();
                }

                isConnected = false;
                CloseConnection();
            }
            return result;
        }

        /// <summary>
        /// Executes the query.
        /// </summary>
        private static void ExecuteQuery()
        {
            while (SqlConn.State == System.Data.ConnectionState.Connecting) { Thread.Sleep(30); }

            string SQL = "INSERT INTO [history] (id, type, date, frame) VALUES";
            for (int i = 0; i < (toSave.Count > 100 ? (toSave.Count / 10) : toSave.Count > 10 ? 10 : toSave.Count); i++)
            {
                DataFrame data = OperationToQueue(OperationToSave.Delete);
                SQL += string.Format(" ('{0}','{1}','{2}', '{3}'), ", data.ModemId, data.NameType, data.Date.ToString(Utility.DataBaseFormattedDate), data.Data);
            }
            SQL = SQL.Substring(0, SQL.Length - 2);

            lock (syncToWrite)
            {
                SqlCom = new SqlCommand(SQL, SqlConn);
                SqlCom.ExecuteNonQuery();
                SqlCom.Dispose();
            }
        }

        private static void ExecuteQuery(DataFrame data)
        {
            while (SqlConn.State == System.Data.ConnectionState.Connecting) { Thread.Sleep(30); }

            string SQL = "INSERT INTO [history] (id, type, date, frame) VALUES";
            SQL += string.Format(" ('{0}','{1}','{2}', '{3}')", data.ModemId, data.NameType, data.Date.ToString(Utility.DataBaseFormattedDate), data.Data);

            lock (syncToWrite)
            {
                SqlCom = new SqlCommand(SQL, SqlConn);
                SqlCom.ExecuteNonQuery();
                SqlCom.Dispose();
            }
        }


        /// <summary>
        /// Recover the queue in the temporal file.
        /// </summary>
        public static void QueueManager()
        {
            Program.isQueueManagerActive = true;
            try
            {
                string line = "";
                bool res = false;
                while (true)
                {
                    while (isConnected == false)
                    {
                        Thread.Sleep(ParserConfiguration.ParserSection.DataBase.ReconnectTime);
                        ConnectDB();
                    }

                    OpenOrCreateQueueFile();
                    lock (syncToWriteFile)
                    {
                        string[] lines = File.ReadAllLines(ParserConfiguration.QUEUE_FILE_NAME);
                        if (lines.Length == 0) break;
                    }

                    Logger.Print(ResourceLoader.GetString2("BeginRecoverQueueToDisk"));

                    lock (syncToWriteFile)
                    {
                        using (StreamReader srRead = new StreamReader(ParserConfiguration.QUEUE_FILE_NAME))
                        {
                            while ((line = srRead.ReadLine()) != null)
                            {
                                try
                                {
                                    string[] data = line.Split('\t');
                                    DataFrame frame = new DataFrame();
                                    
                                    frame.ModemId = data[0];
                                    frame.NameType = data[1];
                                    frame.Date = DateTime.ParseExact(data[2], Utility.DataBaseFormattedDate, System.Globalization.CultureInfo.InvariantCulture);
                                    frame.Data = data[3];

                                    res = CopyToDB(frame);
                                    if (res == false)
                                    {
                                        break;
                                    }
                                }
                                catch
                                { } // bad line is ignored.
                            }

                            srRead.Close();
                            srRead.Dispose();
                        }
                    }

                    if (res == true)
                    {
                        lock (syncToWriteFile)
                        {
                            File.Delete(ParserConfiguration.QUEUE_FILE_NAME);
                        }
                        OpenOrCreateQueueFile();

                        Logger.Print(ResourceLoader.GetString2("EndRecoverQueueToDisk"));
                    }
                    Thread.Sleep(100);
                }
            }
            catch (Exception e)
            {
                if (ParserConfiguration.ShowLog == true) Logger.Print(e);
            }
            finally
            {
                Program.isQueueManagerActive = false;
            }
        }

        /// <summary>
        /// Connects to the Data Base.
        /// </summary>
        /// <returns></returns>
        public static bool ConnectDB()
        {
            try
            {
                if (string.IsNullOrEmpty(ParserConfiguration.DataBaseName) == true ||
                    string.IsNullOrEmpty(ParserConfiguration.DataBaseServer) == true)
                {
                    if (isConnected == true)
                        Logger.Print(ResourceLoader.GetString2("DataBaseNotConfigurated"));
                    isConnected = false;
                }
                else
                {
                    lock (syncToWrite)
                    {
                        SqlConn = new SqlConnection(ParserConfiguration.DataBaseConnectionString + "Connection Timeout=2");
                        SqlConn.Open();
                    }
                    if(isConnected == false)
                        Logger.Print(ResourceLoader.GetString2("ConnectedDataBase"));
                    isConnected = true;
                }
            }
            catch(Exception e)
            {
                if (isConnected == true)
                    Logger.Print(ResourceLoader.GetString2("ConnectDataBaseException"));
                
                Logger.Print(e);
                isConnected = false;

                CloseConnection();
            }
            return isConnected;
        }

        private static void CloseConnection()
        {
            try
            {
                if (SqlConn != null)
                {
                    SqlConn.Close();
                    SqlConn.Dispose();
                }
            }
            catch { }
        }

        /// <summary>
        /// Tests the data base connection.
        /// </summary>
        /// <returns></returns>
        public static int TestDB()
        {
            int result = 0;
            try
            {
                SqlConnection SqlConn = new SqlConnection(ParserConfiguration.DataBaseConnectionString);
                SqlConn.Open();
                SqlConn.Close();

                result = 1;
            }
            catch (Exception ex)
            {
                if (ex.Message.Substring(0, 12) == "Login failed")
                    result = 2;
                else
                    result = 0;
            }
            return result;
        }
        
        /// <summary>
        /// Opens or create the queue file.
        /// </summary>
        public static void OpenOrCreateQueueFile()
        {
            OpenOrCreateQueueFile(false);
        }

        /// <summary>
        /// Opens or create the queue file.
        /// </summary>
        /// <param name="newFile">if set to <c>true</c> [new file].</param>
        public static void OpenOrCreateQueueFile(bool newFile)
        {
            lock (syncToWriteFile)
            {
                QueueFileName = ParserConfiguration.QUEUE_FILE_NAME;
                if (newFile == true)
                {
                    ParserConfiguration.QUEUE_FILE_NAME =
                        ParserConfiguration.QUEUE_FILE_NAME.Substring(0, ParserConfiguration.QUEUE_FILE_NAME.Length - 4)
                        + DateTime.Now.Day + "-" + DateTime.Now.Month + ".txt";
                }
                try
                {
                    if (File.Exists(ParserConfiguration.QUEUE_FILE_NAME))
                    {
                        File.SetAttributes(ParserConfiguration.QUEUE_FILE_NAME, FileAttributes.Normal);
                    }
                    else
                    {
                        File.Open(ParserConfiguration.QUEUE_FILE_NAME, FileMode.OpenOrCreate).Close();
                    }
#if (!DEBUG)
                    File.SetAttributes(ParserConfiguration.QUEUE_FILE_NAME, FileAttributes.Hidden);
#endif
                    
                }
                catch (Exception e)
                {
                    ParserConfiguration.QUEUE_FILE_NAME = QueueFileName;
                    throw e;
                }
            }
        }
        #endregion
    }
}
