using System;
using System.Collections.Generic;
using System.Text;
using SmartCadCore.Core;
using System.Xml;
using System.IO;
using System.Data.SqlClient;
using System.Reflection;
using System.Threading;

namespace SmartCadParser
{

    /// <summary>
    /// General device data.
    /// </summary>
    /// <className>Synchronizer</className>
    /// <author email="arturo.aguero@smartmatic.com">Arturo Ag�ero</author>
    /// <date>2011</date>
    /// <copyRight>Smartmatic copyright</copyRight>
    public class DataFrame
    {
        #region Consts
        /// <summary>
        /// Indicate the separator for the lists.
        /// </summary>
        protected const string ListSeparator = ";";
        /// <summary>
        /// Indicates the separator use for the data.
        /// </summary>
        protected const string Separator = "|";
        /// <summary>
        /// Separator for arriving data
        /// </summary>
        protected const string ArrivingSeparator = "#";

        public const string DataDllFileName = "Smartmatic.SmartCad.Parser.Data.dll";
        #endregion

        #region Constructors

        public DataFrame()
        {
        }

        protected DataFrame(byte[] dataBuffer)
            :this()
        { 
            this.buffer = dataBuffer;
        }

        #endregion

        #region Enums

        public enum Command
        {
            GetPosition,
            SetOutputs
        }

        public enum FrameType
        {
            Position,
            Sync,
            Alarm,
            Incomplete,
            None
        }
        #endregion

        #region Fields
        protected string modemId = null;
        public int respondBytes;

        protected string frame = null;
        protected byte[] buffer = null;
        protected FrameType type = FrameType.None;
        protected static Dictionary<string, Type> DeviceTypes = null;

        private static FileSystemWatcher watcher = null;
        private static object typesLock = new object();
        #endregion

        #region Properties
        /// <summary>
        /// Represent the data in string format.
        /// </summary>
        public string Data { get; set; }
        /// <summary>
        /// Indicate the buffer in ascii format.
        /// </summary>
        protected string Frame
        {
            get { return (frame != null) ? (frame) : Encoding.ASCII.GetString(buffer); }
        }
        /// <summary>
        /// Indicate the type of frame receive.
        /// </summary>
        public FrameType Type
        {
            get { return (type != FrameType.None) ? (type) : (GetFrameType()); }
        }
        /// <summary>
        /// Id of the device
        /// </summary>
        public string ModemId
        {
            get { return (modemId != null) ? (modemId) : (GetModemId()); }
            set { modemId = value; }
        }
        /// <summary>
        /// Indicates if the device need to respond.
        /// </summary>
        public bool Respond { get; set; }
        /// <summary>
        /// Indicate if the device need to clean up the data.
        /// </summary>
        public bool NeedClean { get; set; }
        /// <summary>
        /// Date of the data.
        /// </summary>
        public DateTime Date { get; set; }
        /// <summary>
        /// Byte array of the data.
        /// </summary>
        public byte[] Buffer
        {
            get { return buffer; }
        }
        /// <summary>
        /// Name of the device type
        /// </summary>
        public virtual string NameType
        {
            get
            {
                return "DataFrame";
            }
            set { }
        }
        #endregion
        
        #region Protected Methods

        protected virtual string GetModemId()       { return modemId; }
        protected virtual string GetDate()        { return Date.ToString(); }
        protected virtual FrameType GetFrameType()  { return FrameType.Sync; }

        public virtual void Decode()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Public Methods

        public virtual byte[] GetCommand(Command command, params object[] args)
        {
            throw new NotImplementedException();
        }

        public virtual IList<DataFrame> GetCleanFrames() { return new List<DataFrame>(); }

        public static DataFrame GetDataFrame(string dataType, byte[] buffer)
        {
            lock (typesLock)
            {
                if (DeviceTypes == null)
                {
                    UpdateDataDll();
                }
            }

            if (DeviceTypes.ContainsKey(dataType))
            {
                return (DataFrame)DeviceTypes[dataType].GetConstructor(new Type[1] { typeof(byte[]) }).Invoke(new object[1] { buffer });
            }
            return null;
        }

        public static DataFrame GetDataFrame(string dataType, string buffer)
        {
            lock (typesLock)
            {
                if (DeviceTypes == null)
                {
                    UpdateDataDll();
                }
            }

            if (DeviceTypes.ContainsKey(dataType))
            {
                return (DataFrame)DeviceTypes[dataType].GetConstructor(new Type[1] { typeof(string) }).Invoke(new object[1] { buffer });
            }
            return null;
        }


        public static void StartDataDllWatcher()
        {
            //File watcher to monitor the file coming from the clients (Audio recordings, etc)
            watcher = new FileSystemWatcher();
            watcher.Path = Environment.CurrentDirectory;
            watcher.NotifyFilter = NotifyFilters.CreationTime | NotifyFilters.LastWrite;
            watcher.Filter = DataDllFileName;
            watcher.Created += OnDllChanged;
            watcher.Changed += OnDllChanged;
            watcher.EnableRaisingEvents = true;
        }

        private static void OnDllChanged(object source, FileSystemEventArgs e)
        {
            watcher.EnableRaisingEvents = false;
            lock (typesLock)
            {
                DeviceTypes = null;
            }
            UpdateDataDll();
            Logger.Print("Data dll was updated successfully.");
            watcher.EnableRaisingEvents = true;
        }

        static DateTime asstime = DateTime.MinValue;
        private static void UpdateDataDll()
        {
            lock (typesLock)
            {
                while (DeviceTypes==null)
                {
                    Thread.Sleep(100);
                    try
                    {

                        DateTime newasstime = new FileInfo(DataDllFileName).CreationTime;

                        // DateTime newasstime = DateTime.Now;
                        if (asstime == DateTime.MinValue || asstime < newasstime)
                        {
                            byte[] dllBytes = File.ReadAllBytes(DataDllFileName);
#if DEBUG
                            Assembly assembly = Assembly.Load("Smartmatic.SmartCad.Parser.Data");//DataDllFileName);
#else                                      
                        Assembly assembly = Assembly.Load(dllBytes);
#endif
                            Type[] types = assembly.GetTypes();

                            DeviceTypes = new Dictionary<string, Type>();
                            foreach (Type type in types)
                            {
                                if (type.IsClass == true
                                    && type.IsAbstract == false
                                    && type.IsSubclassOf(typeof(DataFrame)) == true
                                    && type.BaseType != typeof(DataFrame))
                                {
                                    try
                                    {
                                        DeviceTypes.Add(type.Name.Substring(0, type.Name.Length - 4), type);
                                    }
                                    catch { }
                                }
                            }
                        }
                        break;
                    }
                    catch (Exception e)
                    {
                    }
                    if (DeviceTypes == null)
                    {
                        Logger.Print("Parser.Data.dll doesn't have Data types");
                        break;
                    }

                    Logger.Print("Data dll was updated successfully.");
                }
            }
        }
        #endregion


        #region Database
        public string SelectAllCommand
        {
            get { return "SELECT * FROM [REPORTS]"; }
        }

        public string SelectCommand
        {
            get { return "SELECT * FROM [REPORTS] WHERE id = '" + ModemId + "'"; }
        }

        public string InsertOnHistoryCommand
        {
            get
            {
                return "INSERT INTO [history] (id, date, frame) VALUES ('"
                                      + ModemId + "', '"
                                      + Date.ToString(Utility.DataBaseFormattedDate) + "', '"
                                      + Data + "')";
            }
        }

        #endregion
    }
}
