
using System;
using System.Windows.Forms;
using VideoOS.Platform;
using VideoOS.Platform.Client;
using VideoOS.Platform.SDK.UI;
namespace Smartmatic.SmartCad.Vms.Milestone
{
    partial class MilestoneControlEx
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        //private ImageViewerControl _imageViewerControl1;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            
            this.components = new System.ComponentModel.Container();

            //this.imageViewerControl = ClientControl.Instance.GenerateImageViewerControl();

            

            //this.audioControl = null;// new VideoOS.Sdk.UI.AudioControl();
            this.tmrUpdateAlerts = new System.Windows.Forms.Timer(this.components);
            this.tmrPlaybackDate = new System.Windows.Forms.Timer(this.components);
            //this._playbackTimeControl = new VideoOS.Platform.Client.PlaybackControllerBase();
            this.SuspendLayout();
            // 
            // imageViewerControl
            // 
            //this.imageViewerControl.AllowDrop = true;
            //this.imageViewerControl.EnableMouseControlledPtz = true;
            //this.imageViewerControl.EnableVisibleCameraName = false;
            //this.imageViewerControl.EnableLiveStreamInformation = false;
            //this.imageViewerControl.EnableVisibleTimeStamp = true;
            //this.imageViewerControl.Dock = System.Windows.Forms.DockStyle.Fill;
            //this.imageViewerControl.EnableVisibleHeader = false;
            //this.imageViewerControl.Location = new System.Drawing.Point(0, 0);
            //this.imageViewerControl.MaintainImageAspectRatio = true;
            // = VideoOS.Platform.SDK.UI.SDKImageViewerControl.MulticastMode.Started;
            //this.imageViewerControl.Name = "imageViewerControl";
            ////this.imageViewerControl.PlaybackDirection = VideoOS.Sdk.UI.PlayDirection.Forward;
            ////this.imageViewerControl.PlaybackMode = VideoOS.Sdk.UI.PlayModes.CurrentSequence;
            ////this.imageViewerControl.PlaybackSpeed = 1D;
            ////this.imageViewerControl.PlaybackStartCommand = VideoOS.Sdk.UI.TimeStartCommands.FromStart;
            ////this.imageViewerControl.PlaybackTime = new System.DateTime(2012, 5, 14, 17, 52, 53, 612);
            ////this.imageViewerControl.SelectedDevice = null;
            //this.imageViewerControl.Size = new System.Drawing.Size(647, 552);
            //this.imageViewerControl.TabIndex = 2;
            //this.imageViewerControl.Click += new System.EventHandler(this.imageViewerControl_Click);
            //this.imageViewerControl.DoubleClick += new System.EventHandler(this.imageViewerControl_DoubleClick);
            //this.imageViewerControl.MouseWheel += new System.Windows.Forms.MouseEventHandler(this.imageViewerControl_MouseWheel);
            // 
            // audioControl
            // 
            //this.audioControl.DeviceConnectionState = VideoOS.Sdk.SystemInformation.ConnectStates.NotConnected;
            //this.audioControl.Mute = false;
            //this.audioControl.PlayDirection = VideoOS.Sdk.UI.PlayDirection.Forward;
            //this.audioControl.PlaySpeed = 1D;
            //this.audioControl.SelectedAudioDevice = null;
            // 
            // tmrUpdateAlerts
            // 
            this.tmrUpdateAlerts.Interval = 1000;
            this.tmrUpdateAlerts.Tick += new System.EventHandler(this.tmrUpdateAlerts_Tick);
            // 
            // _playbackTimeControl
            // 

            //this._playbackTimeControl.Location = new System.Drawing.Point(359, 30);
            //this._playbackTimeControl.Name = "_playbackTimeControl";
            ////this._playbackTimeControl.p = VideoOS.Sdk.UI.PlayState.Stop;
            //this._playbackTimeControl.Size = new System.Drawing.Size(285, 227);
            //this._playbackTimeControl.TabIndex = 3;
            //this._playbackTimeControl.SelectionToTime = new System.DateTime(2008, 1, 30, 11, 42, 39, 524);
            //this._playbackTimeControl.Visible = false;

            // 
            // MilestoneControlEx
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
          //  this.Controls.Add(this._playbackTimeControl);
          //   this.Controls.Add(this.imageViewerControl);
            this.Name = "MilestoneControlEx";
            this.ResumeLayout(false);

        }

        #endregion

        //public ImageViewerControl imageViewerControl;
        //public SDKAudioControl audioControl;
        public Timer tmrUpdateAlerts;
        public Timer tmrPlaybackDate;
        //public PlaybackControllerBase _playbackTimeControl;
    }
}
