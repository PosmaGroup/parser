﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Collections;

using VideoOS.Platform.SDK.Config;
using videoos.net._2.XProtectCSRecorderStatus;
using System.Globalization;
using System.Threading;
using SmartCadCore.Common;
using SmartCadGuiCommon;
using VideoOS.Platform.SDK.Proxy.Server;
using VideoOS.Platform.SDK.UI.Speaker;
using VideoOS.Platform.Data;
using VideoOS.Platform;
using VideoOS.Platform.Client;
using VideoOS.Platform.Messaging;
using System.Net;
using VideoOS.Platform.SDK.Platform;
using System.Diagnostics;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Xml;
using SmartCadCore.Core;

namespace Smartmatic.SmartCad.Vms.Milestone
{
    public partial class MilestoneControlEx : VmsControlEx
    {
        private Item _selectItem;
        ProgressBar progressBar = new System.Windows.Forms.ProgressBar();
        public static ImageViewerControl imageViewerControl;
        public static List<ImageViewerControl> imageViewersTemplate = new List<ImageViewerControl>();
        public static List<IExporter> exportersList = new List<IExporter>();
        public static int totalExported = 0;


        public MilestoneControlEx()
            : base()
        {
            InitializeComponent();

        }
        private static bool Connected = false;
        private static void SetLoginResult(bool connected)
        {
            Connected = connected;
        }
        private bool LoginUsingCurrentCredentials(string _url, string _user, string _pass)
        {
            //_url = "http://192.168.100.101:90";
            //_user = "Test";
            //_pass = "Test";
            Uri uri = new UriBuilder(_url).Uri;
            url = _url;
            NetworkCredential nc;

            CredentialCache cc = VideoOS.Platform.Login.Util.BuildCredentialCache(uri, _user, _pass, "Basic");
            VideoOS.Platform.SDK.Environment.AddServer(uri, cc);

            try
            {
                VideoOS.Platform.SDK.Environment.Login(uri);
            }
            catch (ServerNotFoundMIPException snfe)
            {
                //Console.WriteLine("Server not found: " + snfe.Message);
                VideoOS.Platform.SDK.Environment.RemoveServer(uri);
                return false;
            }
            catch (InvalidCredentialsMIPException ice)
            {
                //Console.WriteLine("Invalid credentials for: " + ice.Message);
                VideoOS.Platform.SDK.Environment.RemoveServer(uri);
                return false;
            }
            catch (Exception)
            {
                //Console.WriteLine("Internal error connecting to: " + uri.DnsSafeHost);
                VideoOS.Platform.SDK.Environment.RemoveServer(uri);
                return false;
            }
            //Console.WriteLine("Login succeeded.");
            return true;
        }

        private object PlaybackTimeChangedHandler(VideoOS.Platform.Messaging.Message message, FQID dest, FQID sender)
        {
            DateTime time = ((DateTime)message.Data).ToLocalTime();
            //textBoxTime.Text = time.ToShortDateString() + "  " + time.ToLongTimeString();
            //PlaybackDate = time;
            imageViewerControl.Invalidate();

            //Jorge Maguina
            //Se actualizo imageViewerControl por imageViewerControlforech para evitar referencia cruzada
            foreach (ImageViewerControl imageViewerControlforech in imageViewersTemplate)
            {
                if (imageViewerControlforech.Created && imageViewerControlforech.Parent == this)
                {
                    if (direction == VideoOS.Platform.Client.PlaybackControllerBase.PlaybackModeType.Forward)
                    {
                        if (EndPlayBackDate != new DateTime() && time > EndPlayBackDate)
                        {
                            //PlaybackDate = startDate;
                            EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                                            MessageId.SmartClient.PlaybackCommand,
                                                                            new PlaybackCommandData() { Command = PlaybackData.Goto, DateTime = PlaybackDate, Speed = _speed }));
                        }
                    }

                    if (direction == VideoOS.Platform.Client.PlaybackControllerBase.PlaybackModeType.Reverse)
                    {
                        if (PlaybackDate != new DateTime() && time < PlaybackDate)
                        {
                            EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                    MessageId.SmartClient.PlaybackCommand,
                                                    new PlaybackCommandData() { Command = PlaybackData.Goto, DateTime = EndPlayBackDate, Speed = _speed }));
                        }
                    }
                }
            }


            //if (DateTimeValueChanged != null)
            //    DateTimeValueChanged(PlaybackDate);
            return null;
        }

        private object HistoryPlaybackTimeChangedHandler(VideoOS.Platform.Messaging.Message message, FQID dest, FQID sender)
        {
            DateTime time = ((DateTime)message.Data).ToLocalTime();
            //textBoxTime.Text = time.ToShortDateString() + "  " + time.ToLongTimeString();
            //PlaybackDate = time;
            imageViewerControl.Invalidate();

            if (EndPlayBackDate != new DateTime() && time > EndPlayBackDate)
            {
                //PlaybackDate = startDate;
                EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                                MessageId.SmartClient.PlaybackCommand,
                                                                new PlaybackCommandData() { Command = PlaybackData.Goto, DateTime = PlaybackDate, Speed = _speed }));
            }



            //if (DateTimeValueChanged != null)
            //    DateTimeValueChanged(PlaybackDate);
            return null;
        }


        void ImageViewerActiveX_onConnectionLost()
        {
            Disconnect();
            //ReConnect();
        }

        void ImageViewerActiveX_onExportCompleted(short bSuccess)
        {
            //imageViewerControl.ImageViewerActiveX.SetOnExportCompletedEventStatus(1);
           // imageViewerControl.Goto(PlaybackDate, true);
            //imageViewerControl.StopPlayback();
            Application.DoEvents();
            Thread.Sleep(1000);
            Application.DoEvents();
            MessageForm.Show(imageViewerControl.Name, ResourceLoader.GetString2("VideoExportedSuccessfully"), MessageFormType.Information);
        }

        #region Events
        public override event ChangeInput CInput;

        public override event ChangeOutput COutput;

        public override event DateTimeValueChangedEventHandler DateTimeValueChanged;
        #endregion

        #region fields
        private bool flagGeneral = false;
        private string cameraName;
        private AuthenticationType cameraAuthenticationType;
        private VideoOS.Platform.SDK.Config.ICamera currentCamera;
        private string login;
        private string password;
        private string serverIp;
        private SystemInfo sysinfo;
        private string portrec;
        private string recorder;
        private StartStatusSessionResponse startResponse;
        private StartStatusSessionRequest startRequest;
        private RecorderStatusServiceSoap client;
        private System.Guid sessionId;
        private SubscribeEventStatusRequest subscribeEventRequest;
        private static VideoOS.Platform.SDK.Proxy.Server.ServerCommandService serverClient = new VideoOS.Platform.SDK.Proxy.Server.ServerCommandService();
        private AudioRecorder aRecorder = new AudioRecorder();
        private VideoOS.Platform.Client.PlaybackController.PlaybackModeType direction = VideoOS.Platform.Client.PlaybackController.PlaybackModeType.Forward;
        private double _speed = 0.0;
        private IList inputInfo = new ArrayList();
        private IList outputInfo = new ArrayList();
        PlayModes _mode;
        private Item _item = null;
        VideoOS.Platform.Data.IExporter _exporter;
        private Uri _uri = null;
        private string url;
        private DateTime _date;
        private System.Windows.Forms.Timer _timer = new System.Windows.Forms.Timer() { Interval = 100 };
        JPEGVideoSource _jpegVideoSource = null;
        private FQID PlaybackFQID;
        private PlaybackController _playbackController;
        private int templates = 0;

        #endregion

        #region properties
        public Uri ServerAddress
        {
            get
            {
                Uri uri = null;
                if (!string.IsNullOrEmpty(serverIp))
                {
                    uri = new Uri("http://" + serverIp);
                }
                return uri;
            }
        }
        #endregion

        #region methods override

        public virtual void SetTemplate(int template)
        {
            this.templates = template;
        }

        /// <summary>
        /// Initialize the control
        /// </summary>
        /// <param name="cameraName">Represent the camera name</param>
        /// <param name="login">User Login</param>
        /// <param name="password">User Password</param>
        /// <param name="serverIp">Ip for the VMS server</param>
        public override void Initialize(string cameraName, string login, string password, string serverIp)
        {
            this.login = login;
            this.password = password;
            this.cameraAuthenticationType = AuthenticationType.Basic;
            this.cameraName = cameraName;
            this.serverIp = serverIp;

            Uri uri = new UriBuilder("http://" + serverIp).Uri;

            InitializeComponent();

            //VideoOS.Platform.SDK.Environment.Initialize();
            //VideoOS.Platform.SDK.UI.Environment.Initialize();
            //VideoOS.Platform.SDK.Environment.Properties.ConfigurationRefreshIntervalInMs = 5000;
            //EnvironmentManager.Instance.TraceFunctionCalls = true;
            ////DialogLoginForm loginForm = new DialogLoginForm(SetLoginResult);
            //LoginUsingCurrentCredentials("http://" + serverIp, login, password);
            //EnvironmentManager.Instance.RegisterReceiver(PlaybackTimeChangedHandler,new MessageIdFilter(MessageId.SmartClient.PlaybackCurrentTimeIndication));


            if (object.ReferenceEquals(EnvironmentManager.Instance, null))
            {
                //!VideoOS.Platform.SDK.Environment.IsServerConnected(uri)

            VideoOS.Platform.SDK.Environment.Initialize();
                VideoOS.Platform.SDK.UI.Environment.Initialize();
                VideoOS.Platform.SDK.Environment.Properties.ConfigurationRefreshIntervalInMs = 5000;
                EnvironmentManager.Instance.TraceFunctionCalls = true;
                LoginUsingCurrentCredentials("http://" + serverIp, login, password);
                EnvironmentManager.Instance.RegisterReceiver(PlaybackTimeChangedHandler, new MessageIdFilter(MessageId.SmartClient.PlaybackCurrentTimeIndication));
            }


            else if (!EnvironmentManager.Instance.ApplicationLoggedOn)
            {
                ///VideoOS.Platform.SDK.Environment.IsLoggedIn
                LoginUsingCurrentCredentials("http://" + serverIp, login, password);
                EnvironmentManager.Instance.RegisterReceiver(PlaybackTimeChangedHandler, new MessageIdFilter(MessageId.SmartClient.PlaybackCurrentTimeIndication));
            }

            else if (EnvironmentManager.Instance.ApplicationLoggedOn)
            {
                if (Parent.AccessibleDescription == "IncidentHistory")
                {
                    EnvironmentManager.Instance.RegisterReceiver(HistoryPlaybackTimeChangedHandler, new MessageIdFilter(MessageId.SmartClient.PlaybackCurrentTimeIndication));
                }
                else
                {
                    EnvironmentManager.Instance.RegisterReceiver(PlaybackTimeChangedHandler, new MessageIdFilter(MessageId.SmartClient.PlaybackCurrentTimeIndication));
                }
            }
      
        }

        void _playbackTimeControl_PlaybackCommand(object sender, VideoOS.Platform.Client.PlaybackController.PlaybackModeEventArgs e)
        {
           // imageViewerControl.Goto(e.PlaybackMode,true);
        }

        void _playbackTimeControl_ChangeTime(object sender, VideoOS.Platform.Client.PlaybackController.TimeEventArgs e)
        {
           // imageViewerControl.Goto(e.Time);
            PlaybackDate = e.Time;
            if (DateTimeValueChanged != null)
                DateTimeValueChanged(PlaybackDate);
        }

        public override int Connect(string cameraName)
        {
            this.cameraName = cameraName;
            return Connect(0);
        }
       

        static private void FindCamera(List<Item> items, Guid recorderGuid, Dictionary<Guid, Item> result, string cameraName)
        {
            int count = 0;
            Guid newGuid = Guid.Parse(cameraName);
            foreach (Item item in items)
            {
                if (item.FQID.Kind == Kind.Camera && item.FQID.ParentId == recorderGuid && item.FQID.FolderType == FolderType.No)
                {

                    if (result.ContainsKey(item.FQID.ObjectId) == false && item.FQID.ObjectId == newGuid)
                        result[item.FQID.ObjectId] = item;
                }
                else
                    if (item.FQID.FolderType != FolderType.No)
                {
                    FindCamera(item.GetChildren(), recorderGuid, result, cameraName);
                }
            }
        }


        public override void ReleaseConnectionsTemplate()
        {
            foreach (ImageViewerControl imageViewerControl in imageViewersTemplate)
            {
                if (imageViewerControl.Created)
                {
                    imageViewerControl.Disconnect();
                    imageViewerControl.Close();
                    imageViewerControl.Controls.Clear();
                }
            }
           imageViewersTemplate = new List<ImageViewerControl>();
        }

        /// <summary>
        /// Connect to the camera
        /// </summary>
        /// <returns>0 Not connected, 1 Connected, 2 Error</returns>
        public override int Connect(int multiple)
        {
            Type type;
            try
            {
                if (SmartCadGuiCommon.Controls.CameraPanelControl.layoutGlobal != null)
                {
                    System.Windows.Forms.Control.ControlCollection collection = SmartCadGuiCommon.Controls.CameraPanelControl.layoutGlobal.Controls as System.Windows.Forms.Control.ControlCollection;
                    type = typeof(SmartCadGuiCommon.Controls.CameraPanelControl);
                    if (collection.Count != 0)
                    {
                        foreach (var item in collection)
                        {

                            if (item.GetType().Equals(type))
                                multiple++;
                        }
                    }
                }

                    if (CameraCctvAppForm.controlGlobal != null)
                    {
                        type = typeof(SmartCadGuiCommon.Controls.CameraPanelControl);
                        if (CameraCctvAppForm.controlGlobal.Count != 0)
                        {
                            foreach (var item in CameraCctvAppForm.controlGlobal)
                            {
                                if (item.GetType().Equals(type))
                                    multiple++;
                            }
                        }
                    }


                if (imageViewerControl != null && imageViewerControl.Created )
                {
                    if (multiple==0 && templates == 0)
                    {
                        imageViewerControl.Disconnect();
                        imageViewerControl.Close();
                        imageViewerControl.Dispose();
                        imageViewerControl = null;
                    }
                    else if (multiple >= 1)
                    {
                    //    imageViewersTemplate.Add(imageViewerControl);
                        imageViewerControl = /*new ImageViewerControl();*/ null;
                    }                
                       
                    //else if (multiple > 1)
                    //{
                    //    templates = multiple;
                    //    imageViewerControl = null;
                    //}
                }


                int connected = 0;
                Dictionary<Guid, Item> result = new Dictionary<Guid, Item>();
                Item camera = new Item();
                // Get the root level Items, which are the servers added. Configuration is not loaded implicitly yet.
                List<Item> list = Configuration.Instance.GetItems();
                // For each root level Item, check the children. We are certain, none of the root level Items is a camera
                foreach (Item item in list)
                {
                   
                    Dictionary<Guid, Item> allCameras = new Dictionary<Guid, Item>();
                    List<Item> allItems = item.GetChildren();
                    FindCamera(allItems, item.FQID.ServerId.Id, allCameras, cameraName);
                    result = allCameras;
                }
                foreach (KeyValuePair<Guid, Item> cameraResult in result)
                {
                    camera = cameraResult.Value;
                    _item = camera;
                }
                
                imageViewerControl = ClientControl.Instance.GenerateImageViewerControl();
                imageViewerControl.Dock = DockStyle.Fill;
                imageViewerControl.ClickEvent += new EventHandler(_imageViewerControl1_ClickEvent);

                imageViewerControl.CameraFQID = camera.FQID;

                imageViewerControl.EnableVisibleLiveIndicator = EnvironmentManager.Instance.Mode == VideoOS.Platform.Mode.ClientLive;
                imageViewerControl.EnableMousePtzEmbeddedHandler = true;
                imageViewerControl.MaintainImageAspectRatio = true;
                imageViewerControl.SetVideoQuality(0, 1);

                imageViewerControl.ImageOrPaintInfoChanged += ImageOrPaintChangedHandler;
                imageViewerControl.EnableRecordedImageDisplayedEvent = true;

                imageViewerControl.RecordedImageReceivedEvent += imageViewerControl_RecordedImageReceivedEvent;
                imageViewerControl.EnableVisibleTimeStamp = true;
                string testImageViewer = imageViewerControl.ToString();
                imageViewersTemplate.Add(imageViewerControl);
                //     Thread.Sleep(5000);

                if (!imageViewerControl.IsDisposed)
                {
                    imageViewerControl.Update();
                    imageViewerControl.Refresh();
                    imageViewerControl.Parent = this;
                    imageViewerControl.UpdateStates();
                    imageViewerControl.Initialize();
                    imageViewerControl.Connect();//(serverIp, login, password, AuthenticationType.Basic);
                    imageViewerControl.Selected = true;

                    connected = 1;
                }


                if (imageViewerControl.CameraFQID != null)//&& imageViewerControl.Cameras!=null)
                {

                    if (camera != null)
                    {


                        connected = 1;
                    }
                    else {
                        connected = 0;
                    }
                }
                return connected;

                //else
                //{
                //    return 0;
                //}
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
                EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                        MessageId.SmartClient.PlaybackCommand,
                                                        new PlaybackCommandData() { Command = PlaybackData.End }));
                return 2;
            }
        }


        void _imageViewerControl1_ClickEvent(object sender, EventArgs e)
        {
            this.OnClick(e);
            imageViewerControl.Selected = true;
        }

        public override ImageViewerControl instanceCameraPanelControl()
        {
            imageViewerControl.Dock = DockStyle.Fill;
            return imageViewerControl;
        }


        void ImageOrPaintChangedHandler(object sender, EventArgs e)
        {
            Debug.WriteLine("ImageSize:" + imageViewerControl.ImageSize.Width + "x" + imageViewerControl.ImageSize.Height + ", PaintSIze:" +
                imageViewerControl.PaintSize.Width + "x" + imageViewerControl.PaintSize.Height +
                ", PaintLocation:" + imageViewerControl.PaintLocation.X + "-" + imageViewerControl.PaintLocation.Y);
        }

        void imageViewerControl_RecordedImageReceivedEvent(object sender, RecordedImageReceivedEventArgs e)
        {
        }

        void ImageViewerActiveX_onConnectResponseReceived(short bConnectionGranted)
        {
            if (bConnectionGranted == 0)
            {
                Disconnect();
            }
        }

        public override void Disconnect()
        {
            try
            {
                if (imageViewerControl != null)
                    imageViewerControl.Disconnect();
                this.Dispose();

            }
            catch (Exception ex) { SmartLogger.Print(ex); }
        }

        public void ReConnect()
        {
            try
            {
                //if(imageViewerControl.IsConnected==false)
                if (imageViewerControl.IsDisposed == false)
                    Disconnect();
                Connect(0);
                imageViewerControl.Initialize();//ImageViewerActiveX.LiveStart();
               // PlayLive();
            }
            catch { }
        }

        /// <summary>
        /// Continuos movement of the Camera
        /// </summary>
        /// <param name="X">Axis X</param>
        /// <param name="Y">Axis Y</param>
        /// <param name="Z">Axis Z</param>
        public override void PTZMove(int X, int Y, int Z)
        {
            if (feed)
            {
                bool flag = false;
                VideoOS.Platform.SDK.Config.PTZArgs ptzArgs = new VideoOS.Platform.SDK.Config.PTZArgs();
                Item camera = new Item();
                 camera.FQID   = imageViewerControl.CameraFQID;
                #region AxisX
                if (X > 5000)//right movement
                {
                    double temp = X - 5000;
                    ptzArgs.Pan = 1;
                    ptzArgs.PanSpeed = (temp * 1 / 5000);
                    flag = true;
                }
                else if (X < 5000)//left movement
                {
                    double temp = 5000 - X;
                    ptzArgs.Pan = -1;
                    ptzArgs.PanSpeed = (temp * 1 / 5000);
                    flag = true;
                }

                #endregion
                #region AxisY
                if (Y > 5000)//Up movement
                {
                    double temp = Y - 5000;
                    ptzArgs.Tilt = 1;
                    ptzArgs.TiltSpeed = (temp * 1 / 5000);
                    flag = true;
                }
                else if (Y < 5000)//Down movement
                {
                    double temp = 5000 - Y;
                    ptzArgs.Tilt = -1;
                    ptzArgs.TiltSpeed = (temp * 1 / 5000);
                    flag = true;
                }

                #endregion
                #region AxisZ
                if (Z > 5000)//In movement
                {
                    double temp = Z - 5000;
                    ptzArgs.Zoom = 1;
                    ptzArgs.ZoomSpeed = (temp * 1 / 5000);
                    flag = true;
                }
                else if (Z < 5000)//Out movement
                {
                    double temp = 5000 - Z;
                    ptzArgs.Zoom = -1;
                    ptzArgs.ZoomSpeed = (temp * 1 / 5000);
                    flag = true;
                }
                #endregion
                //if (flag)
                //{
                //    camera.PtzMoveStart(ptzArgs);
                //    flagGeneral = true;
                //}
                //if (flagGeneral && flag == false)
                //{
                //    camera.PtzMoveStop();
                //    flagGeneral = false;
                //}
            }
        }

        /// <summary>
        /// Relative movement of the camera
        /// </summary>
        /// <param name="dir">direction fo the movement</param>
        public override void Ptz_do(PtzMoveRelativeTypes dir)
        {
            if (currentCamera != null)
            {
                double speed = Convert.ToDouble(1);
                double step = Convert.ToDouble(1);
                currentCamera.PtzMoveRelative((VideoOS.Platform.SDK.Config.PtzMoveRelativeTypes)dir, speed, step);
            }
        }

        /// <summary>
        /// Mute the audio in the video
        /// </summary>
        /// <param name="mute">bool, true mute and false unmute</param>
        public override void mute(bool mute)
        {
            //if(audioControl!=null)
            //    audioControl.Mute = mute;
        }

        /// <summary>
        /// Ajust the volume in the control
        /// </summary>
        /// <param name="volume">value for the volume</param>
        public override void volume(int volume)
        {
            //if (audioControl != null)
            //    audioControl.Volume = volume / 10.0;
        }

        /// <summary>
        /// This allow the operator speak with the user
        /// </summary>
        /// <param name="speak">true speaking, false quiet</param>
        //public override void Speaking(bool speak)
        //{
        //    try
        //    {
        //        if (speak)
        //        {
        //            serverClient.BeginSpeak();
        //            aRecorder.StartRecording();
        //        }
        //        else
        //        {
        //            aRecorder.StopRecording();
        //            serverClient.EndSpeak();
        //        }
        //    }
        //    catch (Exception e) { SmartLogger.Print(e); }
        //}

        /// <summary>
        /// Trigger the selected Output
        /// </summary>
        /// <param name="status">True Activate, False Deactivate</param>
        /// <param name="OutputName">Name of the output</param>
        public override void TriggerOutput(bool status, string OutputName)
        {
            try
            {
                string url2 = string.Format("http://{0}:{1}/recordercommandservice/recordercommandservice.asmx", recorder, portrec);
                VideoOS.Platform.SDK.Proxy.RecorderServices.RecorderCommandService c = new VideoOS.Platform.SDK.Proxy.RecorderServices.RecorderCommandService();
                c.Url = url2;
                foreach (OutputInfo outinf in outputInfo)
                {
                    if (outinf.Name.Equals(OutputName))
                    {
                        if (status)
                            c.OutputActivate(sysinfo.Token, outinf.DeviceId);
                        else
                            c.OutputDeactivate(sysinfo.Token, outinf.DeviceId);
                        break;
                    }
                }
            }
            catch (Exception e) { SmartLogger.Print(e); }
        }

        /// <summary>
        /// Get id the current camera is muted
        /// </summary>
        /// <returns>Mute value</returns>
        //public override bool GetMute()
        //{
        //    //if (audioControl != null)
        //    //    return this.audioControl.Mute;
        //    //else
        //    //    return true;
        //}

        /// <summary>
        /// Get the volume of the currente camera
        /// </summary>
        /// <returns>Volume value</returns>
        //public override int GetVolume()
        //{
        //    //if (audioControl != null)
        //    //    return Convert.ToInt32(this.audioControl.Volume * 10.0);
        //    //else
        //    //    return 0;
        //}

        /// <summary>
        /// Change the play mode for teh current
        /// </summary>
        /// <param name="mode">Play mode</param>
        public override void Playmode(PlayModes mode)
        {
            PlaybackController _playbackTimeControl;
            //_mode = mode;
            switch (mode)
            {
                case PlayModes.Live:

                    //if (imageViewerControl != null)
                    //{
                    //    imageViewerControl.EnableVisibleLiveIndicator = true;
                    //    imageViewerControl.EnableMousePtzEmbeddedHandler = true;
                    //}
                        
                    
                    EnvironmentManager.Instance.Mode = Mode.ClientLive;
                    //buttonMode.Text = "Current mode: Live";


                    break;
                case PlayModes.PlayBack_SingleSequence:

                    //if (_speed == 0.0)
                    //    _speed = 1.0;
                    //else
                    //    _speed *= 2;
                    EnvironmentManager.Instance.Mode = Mode.ClientPlayback;
                    //COMMENTED FOR TESTING
                    EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                                MessageId.SmartClient.PlaybackCommand,
                                                                new PlaybackCommandData() { Command = PlaybackData.Goto, DateTime = PlaybackDate, Speed = _speed }));

                    //MultipleSequences;
                    //if(PlaybackFQID == null)
                    //{
                    //    PlaybackFQID = ClientControl.Instance.GeneratePlaybackController();
                    //    _playbackUserControl.Init(_playbackFQID);
                    //}

                    break;
                //    case PlayModes.PlayBack_MultiSequences:

                //        if (_speed == 0.0)
                //            _speed = 1.0;
                //        else
                //            _speed *= 2;
                //        EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                //                                                    MessageId.SmartClient.PlaybackCommand,
                //                                                    new PlaybackCommandData() { Command = PlaybackData.PlayReverse, Speed = _speed }));//MultipleSequences;
                //        break;
                //case PlayModes.PlayBack_RepeatSequence:

                //    EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                //                                        MessageId.SmartClient.PlaybackCommand,
                //                                        new PlaybackCommandData() { Command = PlaybackData.PlayStop }));

                //    //buttonMode.Text = "Current mode: Playback";
                //    _speed = 0.0;
                //    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Get the current play mode for teh camera
        /// </summary>
        /// <returns>True Live, False Playback</returns>
        //public override bool GetPlaymode()
        //{
        //    if (this._playbackTimeControl.Mode == 0)
        //        return true;
        //    else
        //        return false;
        //}


        /// <summary>
        /// Set the direction of the playback
        /// </summary>
        /// <param name="direction">0 forward, 1 backward</param>
        public override void SetPlayDirection(int _direction)
        {
            
            if (_direction == 0)
            {
                direction = VideoOS.Platform.Client.PlaybackControllerBase.PlaybackModeType.Forward;
            }
                
            else
                direction = VideoOS.Platform.Client.PlaybackControllerBase.PlaybackModeType.Reverse;
        }

        /// <summary>
        /// Start the video from startDate
        /// </summary>
        /// <param name="startDate"></param>
        public override void StartPlayBack()
        {
            //if (_speed == 0.0)
            //    _speed = 1.0;
            //else
            //    _speed *= 2;
            //EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
            //                                            MessageId.SmartClient.PlaybackCommand,
            //                                            new PlaybackCommandData() { Command = PlaybackData.PlayForward, Speed = _speed }));
            //EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
            //                                            MessageId.SmartClient.PlaybackCommand,
            //                                            new PlaybackCommandData() { Command = PlaybackData.Goto, DateTime = PlaybackDate }));
            if (direction == VideoOS.Platform.Client.PlaybackControllerBase.PlaybackModeType.Forward)
            {
                if (_speed == 0.0)
                    _speed = 1.0;
                //else
                //    _speed *= 2;
                EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                            MessageId.SmartClient.PlaybackCommand,

                                                            new PlaybackCommandData() { Command = PlaybackData.PlayForward, Speed = _speed, DateTime = PlaybackDate}));

            }
            if (direction == VideoOS.Platform.Client.PlaybackControllerBase.PlaybackModeType.Reverse)
            {

                if (_speed == 0.0)
                    _speed = 1.0;
                else
                    _speed *= 2;
                EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                            MessageId.SmartClient.PlaybackCommand,

                                                            new PlaybackCommandData() { Command = PlaybackData.PlayReverse, Speed = _speed, DateTime = PlaybackDate }));

            }
        }

        /// <summary>
        /// Get the current direction of the playback
        /// </summary>
        /// <returns>0 forward, 1 backward</returns>
        //public override int GetPlayDirection()
        //{
        //    if (_playbackTimeControl.PlaybackMode == VideoOS.Platform.Client.PlaybackControllerBase.PlaybackModeType.Forward)
        //        return 0;
        //    else
        //        return 1;
        //}

        public override void StopLive()
        {

            EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                        MessageId.SmartClient.PlaybackCommand,
                                                        new PlaybackCommandData() { Command = PlaybackData.PlayStop, DateTime = _date }));
            EnvironmentManager.Instance.Mode = Mode.ClientPlayback;
            //buttonMode.Text = "Current mode: Playback";
            _speed = 0.0;
        }

        /// <summary>
        /// Stop the playback
        /// </summary>
        public override void StopPlayback()
        {
            //if (EnvironmentManager.Instance.Mode == Mode.ClientLive)
            //{
            //    EnvironmentManager.Instance.Mode = Mode.ClientPlayback;
            //    //buttonMode.Text = "Current mode: Playback";
            //}
            //else
            //{
            //    EnvironmentManager.Instance.Mode = Mode.ClientLive;
            //    //buttonMode.Text = "Current mode: Live";
            //}

            EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                        MessageId.SmartClient.PlaybackCommand,

                                                        new PlaybackCommandData() { Command = PlaybackData.PlayStop}));
            //FEnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
            //F                                            MessageId.SmartClient.PlaybackCommand,
            //F                                            new PlaybackCommandData() { Command = PlaybackData.Goto, DateTime = PlaybackDate }));
            //F
            //EnvironmentManager.Instance.Mode = Mode.ClientPlayback;
            ////buttonMode.Text = "Current mode: Playback";
            //_speed = 0.0;
        }

        /// <summary>
        /// Change the current speed of the playback
        /// </summary>
        /// <param name="speed">value for the speed</param>
        public override void SetSpeedPlayback(double speed)
        {
            _speed = speed;
            if (direction == VideoOS.Platform.Client.PlaybackControllerBase.PlaybackModeType.Forward)
            {
                //if (_speed == 0.0)
                //    _speed = 1.0;
                //else
                //    _speed *= 2;
                EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                            MessageId.SmartClient.PlaybackCommand,
                                                            new PlaybackCommandData() { Command = PlaybackData.Speed, Speed = _speed }));
            }
            if (direction == VideoOS.Platform.Client.PlaybackControllerBase.PlaybackModeType.Reverse)
            {

                //if (_speed == 0.0)
                //    _speed = 1.0;
                //else
                //    _speed *= 2;
                EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                            MessageId.SmartClient.PlaybackCommand,
                                                            new PlaybackCommandData() { Command = PlaybackData.Speed, Speed = _speed }));
            }

            //_playbackTimeControl.GetType().GetField("_speed", BindingFlags.NonPublic |
            //             BindingFlags.Instance).SetValue(_playbackTimeControl, speed);

            //if (audioControl != null)
            //{
            //    if (Mode == VideoOS.Platform.Client.PlaybackController.PlaybackModeType.Forward)
            //    {
            //        if (speed == 1.0)
            //            this.audioControl.Initialize();//(this.imageViewerControl.PlaybackTime);
            //        else
            //            this.audioControl.Close();// Stop();
            //    }
            //}
        }

        /// <summary>
        /// Get the current speed of the playback
        /// </summary>
        /// <returns></returns>
        public override double GetSpeedPlayback()
        {
            return _speed;
        }


        public override void SetPlaybackEndDate(DateTime date)
        {
            EndPlayBackDate = date;
        }

        /// <summary>
        /// Set the playback time
        /// </summary>
        /// <param name="PlaybackDAte">start time of the playback</param>
        public override void SetPlaybackDate(DateTime date)
        {
            try
            {
                if (date < DateTime.Now)
                {

                    PlaybackDate = date;

                    //COMMENTED FOR TESTING
                    EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                                MessageId.SmartClient.PlaybackCommand,
                                                                new PlaybackCommandData() { Command = PlaybackData.Goto, DateTime = PlaybackDate, Speed = _speed }));



                    //FQID _playbackFQID = null;
                    //_playbackController = ClientControl.Instance.GetPlaybackController(_playbackFQID);
                    //DateTime start = PlaybackDate;
                    //DateTime end = PlaybackDate + TimeSpan.FromMinutes/*FromSeconds*/(5);

                    ////_playbackController.SequenceProgressChanged += new EventHandler<PlaybackController.ProgressChangedEventArgs>(_playbackController_SequenceProgressChanged);
                    //_playbackController.SetSequence(start, end);
                    //imageViewerControl.Goto(PlaybackDate);

                    //_playbackTimeControl.PlaybackTime = date;
                    //imageViewerControl.PlaybackTime = date;
                }
            }
            catch (Exception ex) { SmartLogger.Print(ex); }
        }

        


        /// <summary>
        /// Get the current date of the playback
        /// </summary>
        /// <returns></returns>
        public override DateTime GetPlaybackDate()
        {
            return PlaybackDate;
        }

        /// <summary>
        /// Get the current camera name
        /// </summary>
        /// <returns>String with the camera name</returns>
        public override string GetCameraName()
        {
            return this.cameraName;
        }

        private void buttonStop_Click(object sender, EventArgs e)
        {
            //if (EnvironmentManager.Instance.Mode == Mode.ClientLive)
            //{
            //    EnvironmentManager.Instance.Mode = Mode.ClientPlayback;
            //    //buttonMode.Text = "Current mode: Playback";
            //}
            //else
            //{
            //    EnvironmentManager.Instance.Mode = Mode.ClientLive;
            //    //buttonMode.Text = "Current mode: Live";
            //}
            //buttonReverse.Enabled = EnvironmentManager.Instance.Mode == Mode.ClientPlayback;
            //buttonForward.Enabled = EnvironmentManager.Instance.Mode == Mode.ClientPlayback;
            //buttonStop.Enabled = EnvironmentManager.Instance.Mode == Mode.ClientPlayback;

            EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                        MessageId.SmartClient.PlaybackCommand,
                                                        new PlaybackCommandData() { Command = PlaybackData.PlayStop, DateTime = _date }));
            EnvironmentManager.Instance.Mode = Mode.ClientPlayback;
            //buttonMode.Text = "Current mode: Playback";
            _speed = 0.0;
        }


        public void ShowProgressInForm(ChangeTaskDelegate changeTaskFunction, ChangeValueDelegate changeValueFunction, object[] arguments) 
        {
            AVIExporter _exporter = (AVIExporter)arguments[0];
            changeTaskFunction(ResourceLoader.GetString2("ExportingVideo"));
            int aux = 0;

            while (_exporter.Progress < 100)
            {
                if (_exporter.Progress == 10 || _exporter.Progress == 25
                   || _exporter.Progress == 40 || _exporter.Progress == 50
                   || _exporter.Progress == 65 || _exporter.Progress == 85)
                {
                    changeTaskFunction("Completed : " + _exporter.Progress.ToString());
                    changeValueFunction(_exporter.Progress);

                    aux = 1;
                }

                if ( (_exporter.Progress >= 30) && (aux != 0) )
                {
                    break;
                }

            }
   
        }


        public void ShowProgressJPEG(ChangeTaskDelegate changeTaskFunction, ChangeValueDelegate changeValueFunction, object[] arguments)
        {
            JPEGData jpegData = (JPEGData)arguments[0];
            DateTime beginDate = (DateTime)arguments[1];
            DateTime finalDate = (DateTime)arguments[2];
            int countJPEG = (int)arguments[3];
            string path = (string)arguments[4];
            changeTaskFunction(ResourceLoader.GetString2("ExportingJPEG"));
            changeValueFunction(30);
            for (DateTime date = beginDate; date <= finalDate; date = date.AddMilliseconds(200), countJPEG++)
            {
                jpegData = _jpegVideoSource.GetAtOrBefore(date) as JPEGData;
                jpegData.DateTime = date;
                ShowJPEG(jpegData, path, date);
            }
            changeValueFunction(100);

        }






        /// <summary>
        /// Connect to the server to import the video of the current camera
        /// </summary>
        /// <returns>0 Not connected, 1 Connected</returns>
        public override int ExportVideo(DateTime startDate, DateTime endDate, string path, int format, int timestamp)
        {
            try
            {
                bool timeStampBool = !(timestamp != 0); //Cast int to bool, because timestamp is boolean in the new SDK
                if (format == 0) // AVI format
                {
                    int lastIndexOf = path.LastIndexOf('\\');
                    string directory = path.Substring(0, lastIndexOf);
                    string fileName = path.Substring(lastIndexOf, path.Length - lastIndexOf);
                    path = directory + "\\" + _item.Name;
                    fileName = fileName.Replace("\\", "");

                    //AVIExporter tempExporter = new VideoOS.Platform.Data.AVIExporter() { Width = 720, Height = 480, Filename = fileName, Codec = "Microsoft Video 1", FrameRate = 80 };

                    //tempExporter.Init();
                    //tempExporter.Path = directory;
                    //tempExporter.CameraList = Configuration.Instance.GetItems();
                    //tempExporter.EndExport();


                    try
                    {
                        List<Item> audioSources = new List<Item>();
                        String destPath = path;

                        audioSources = _item.GetRelated(); // Get the defined related Microphones and Speakers for the selected camera
                        if (EnvironmentManager.Instance.MasterSite.ServerId.ServerType == ServerId.EnterpriseServerType)
                        {
                            //Enterprise does not record speaker sound
                            foreach (Item item in audioSources)
                            {
                                if (item.FQID.Kind != Kind.Microphone)
                                {
                                    audioSources.Remove(item);
                                }
                            }
                        }

                        if (_exporter != null)
                        {
                            _exporter.CameraList.Clear();
                            _exporter = null;
                        }
                        _exporter = new VideoOS.Platform.Data.AVIExporter()
                            { Filename = fileName, Codec = "Microsoft Video 1", FrameRate = 60, Timestamp = timeStampBool };


                        _exporter.Init();
                        _exporter.Path = destPath;
                        _exporter.CameraList = new List<Item>() { _item };
                        _exporter.AudioList = audioSources;
                        DateTime startDate1 = startDate;
                        DateTime endDate1 = endDate;
                        bool _startresult = _exporter.StartExport(startDate1, endDate1);
                        exportersList.Add(_exporter);
                     //   ProgressForm processForm = new ProgressForm(ShowProgressInForm, new object[1] { _exporter }, ResourceLoader.GetString2("ExportVideo"));

                     //   processForm.CanThrowError = true;
                    //    processForm.ShowDialog();
                        MessageForm.Show(ResourceLoader.GetString2("VideoExportedSucessfully") + DateTime.Now.ToString(), MessageFormType.Information);

                        //processForm.Close();
                        //processForm.Dispose();
                        
                      //  _exporter.Close();

                       // this._exporter.Close();
                        //if (_startresult)
                        //{
                        //    _timer.Tick += ShowProgress;
                        //    _timer.Start();
                        //}
                        //else
                        //{
                        //    _exporter.EndExport();
                        //}

                  //      _exporter.EndExport();
                  //      _exporter.Close();

                        return 1;
                    }
                    catch (Exception ex)
                    {
                        MessageForm.Show(ex.Message, ex);
                        return 0;
                    }

                   // return 1;
                }
                else if (format == 1) // JPEG Format
                {
                    try
                    {
                            
                        DateTime beginDate = startDate;
                        DateTime finalDate = endDate;


                        _jpegVideoSource = new JPEGVideoSource(_item);
                        _jpegVideoSource.Width = 720;
                        _jpegVideoSource.Height = 480;
                        _jpegVideoSource.Init();
                            
                        JPEGData jpegData =  _jpegVideoSource.GetAtOrBefore(startDate) as JPEGData;

                        int countJPEG = 0;


                        ProgressForm processForm = new ProgressForm(ShowProgressJPEG, new object[5] {jpegData,beginDate,finalDate,countJPEG,path}, 
                                                                    ResourceLoader.GetString2("ExportJPEG"));

                        processForm.CanThrowError = true;
                        processForm.ShowDialog();



                        return 1;
                    }
                    catch (Exception ex)
                    {
                        MessageForm.Show(ex.Message, ex);
                        return 0;
                    }
                    
                }
                return 0;
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ex);
                return 0;
            }

        }



        private void ShowProgress(object sender, EventArgs e)
        {
            if (_exporter != null)
            {
                int progress = _exporter.Progress;
                if (progress >= 0)
                {
                    progressBar.Value = progress;
                    if (progress == 100)
                    {
                        _timer.Stop();
                        _exporter.EndExport();
                        _exporter = null;
                    }
                }
             
            }
        }



        private delegate void ShowJpegDelegate(JPEGData jpegData);

        private void ShowJPEG(JPEGData jpegData, string dir, DateTime date)
        {



            int lastIndexOf = dir.LastIndexOf('\\');
            string directory = dir.Substring(0, lastIndexOf);
            string fileName = dir.Substring(lastIndexOf, dir.Length - lastIndexOf);
            dir = directory;
            dir = Path.Combine(dir, _item.Name);
            
            fileName = fileName.Replace("\\", "");
            String[] substrings = fileName.Split('.');
            fileName = substrings[0];
            dir = Path.Combine(dir, fileName);
            DirectoryInfo dirinfo = Directory.CreateDirectory(dir);

            Byte[] jpeg = jpegData.Bytes;
            string cname = fileName;
           // cname = cname + count +  ".jpg";
            

            // DateTime dt = DateTime.ParseExact(date, "ddMMyyyy",CultureInfo.InvariantCulture);
            string dateTimeFormat = date.ToString("yyyyMMdd");
            string dateHour = date.Hour.ToString();
            string dateMinutes = date.Minute.ToString();
            string dateSeconds = date.Second.ToString();


            cname = dateTimeFormat + dateHour + dateMinutes + dateSeconds + ".jpg";
            
            string fname = Path.Combine(dir, cname.Replace('/', '-'));

            File.WriteAllBytes(fname, jpeg);

            

            
        }






  

        #endregion

        bool doReconnect = true;
        //private void imageViewerControl_ConnectionStateChanged(object sender, ConnectionStateChangedEventArgs e)
        //{
        //    try
        //    {
        //        if (e.ConnectionState == ConnectStates.Connected)
        //        {
        //            PlayLive();
        //            doReconnect = true;
        //        }
        //        else if (doReconnect)
        //        {
        //            ReConnect();
        //            doReconnect = false;
        //        }
        //    }
        //    catch (Exception ex) { SmartLogger.Print(ex); }
        //}

        /// <summary>
        /// Initialize the Speaker
        /// </summary
        /// <param name="speakerinfo">Speaker info</param>
        /// <param name="recorder">Recorder IP</param>
        /// <param name="port">Server port</param>
        //private void SpeakerInit(SpeakerInfo speakerinfo, RecorderInfo recorder, int port)
        //{
        //    List<Guid> guids = new List<Guid>();
        //    guids.Add(speakerinfo.DeviceId);
        //    serverClient = new ServerCommandService();
        //    serverClient.DeviceIds = guids;
        //    serverClient.RecorderName = recorder.Name;
        //    serverClient.ServerIPAddress = recorder.HostName;
        //    serverClient.ServerPort = port;
        //    serverClient.Token = sysinfo.Token;
        //    serverClient.Connect();
        //    aRecorder.Initialize();
        //    aRecorder.AudioDataReadyEvent += new AudioRecorder.AudioDataReadyEventHandler(aRecorder_AudioDataReadyEvent);
        //}

        /// <summary>
        /// Initialize de audio device event
        /// </summary>
        void aRecorder_AudioDataReadyEvent(object sender, AudioRecorder.AudioDataEventArgs e)
        {
          //  serverClient.Speak(e.AudioData);
        }

        /// <summary>
        /// Starts video
        /// </summary>
        private void PlayLive()
        {
            if (imageViewerControl.Selected == false)
                imageViewerControl.CameraFQID = _selectItem.FQID;

            //if (currentCamera != null && imageViewerControl.IsConnected)
            if (currentCamera != null && imageViewerControl.IsDisposed)
            {
                //if (_playbackTimeControl.Mode == VideoOS.Platform.Mode.ClientLive)
                //{
                //    // imageViewerControl.SelectedDevice = currentCamera;
                //    //imageViewerControl.Initialize();// PlayLive();
                //    //if(audioControl!=null)
                //    //    audioControl.StartLive();// PlayLive();
                //    //feed = true;
                //}
                //else
                //{
                //  //  imageViewerControl.Goto(PlaybackDate, true);
                //}
            }
        }

        private void tmrUpdateAlerts_Tick(object sender, EventArgs e)
        {
            GetStatusRequest statusRequest = new GetStatusRequest();
            statusRequest.Body = new GetStatusRequestBody();
            statusRequest.Body.token = sysinfo.Token;
            statusRequest.Body.statusSessionId = sessionId;
            GetStatusResponse statusResponse = client.GetStatus(statusRequest);
            StatusBase[] stats = statusResponse.Body.GetStatusResult;

            String temp = string.Empty;
            if (feed)
                foreach (StatusBase stat in stats)
                {
                    temp = string.Empty;
                    if (stat.GetType() == typeof(EventStatus))
                    {
                        EventStatus eStat = (EventStatus)stat;
                        #region input
                        foreach (InputInfo input in inputInfo)
                        {
                            if (input.DeviceId == eStat.SourceId)
                            {
                                foreach (IOData item in inputs)
                                {
                                    if (input.Name.Equals(item.Name.ToString()))
                                    {
                                        IOArgs InpArgs;
                                        if (eStat.EventId == MilestoneCommon.InputActivated)
                                        {
                                            IOData data = new IOData();
                                            data.Name = item.Name;
                                            data.Status = item.Status = 1;
                                            InpArgs = new IOArgs(data);
                                        }
                                        else
                                        {
                                            IOData data = new IOData();
                                            data.Name = item.Name;
                                            data.Status = item.Status = 0;
                                            InpArgs = new IOArgs(data);
                                        }
                                        if (CInput != null)
                                            CInput(this, InpArgs);
                                        break;
                                    }
                                }
                            }
                        }
                        #endregion
                        #region output
                        foreach (OutputInfo output in outputInfo)
                        {
                            if (output.DeviceId == eStat.SourceId)
                            {
                                foreach (IOData item in outputs)
                                {
                                    if (output.Name.Equals(item.Name.ToString()))
                                    {
                                        IOArgs InpArgs;
                                        if (eStat.EventId == MilestoneCommon.OutputActivated)
                                        {
                                            IOData data = new IOData();
                                            data.Name = item.Name;
                                            data.Status = item.Status = 1;
                                            InpArgs = new IOArgs(data);
                                        }
                                        else
                                        {
                                            IOData data = new IOData();
                                            data.Name = item.Name;
                                            data.Status = item.Status = 0;
                                            InpArgs = new IOArgs(data);
                                        }
                                        if (COutput != null)
                                            COutput(this, InpArgs);
                                        break;
                                    }
                                }
                            }
                        }
                        #endregion
                    }
                }
        }

        private delegate void SimpleDelegate();

        private void LoadLanguageImageControl()
        {
            //this.imageViewerControl.ImageViewerActiveX.ReplaceTextString(4, ResourceLoader.GetString2("VMS.4"));
            //this.imageViewerControl.ImageViewerActiveX.ReplaceTextString(5, ResourceLoader.GetString2("VMS.5"));
            //this.imageViewerControl.ImageViewerActiveX.ReplaceTextString(6, ResourceLoader.GetString2("VMS.6"));
            //this.imageViewerControl.ImageViewerActiveX.ReplaceTextString(7, ResourceLoader.GetString2("VMS.7"));
            //this.imageViewerControl.ImageViewerActiveX.ReplaceTextString(8, ResourceLoader.GetString2("VMS.8"));
            //this.imageViewerControl.ImageViewerActiveX.ReplaceTextString(9, ResourceLoader.GetString2("VMS.9"));
            //this.imageViewerControl.ImageViewerActiveX.ReplaceTextString(10, ResourceLoader.GetString2("VMS.10"));
            //this.imageViewerControl.ImageViewerActiveX.ReplaceTextString(11, ResourceLoader.GetString2("VMS.11"));
            //this.imageViewerControl.ImageViewerActiveX.ReplaceTextString(12, ResourceLoader.GetString2("VMS.12"));
            //this.imageViewerControl.ImageViewerActiveX.ReplaceTextString(13, ResourceLoader.GetString2("VMS.13"));
            //this.imageViewerControl.ImageViewerActiveX.ReplaceTextString(14, ResourceLoader.GetString2("VMS.14"));
            //this.imageViewerControl.ImageViewerActiveX.ReplaceTextString(15, ResourceLoader.GetString2("VMS.15"));
            //this.imageViewerControl.ImageViewerActiveX.ReplaceTextString(17, ResourceLoader.GetString2("VMS.17"));
        }

        private static readonly DateTime _dt1970 = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
        private static double ToDouble(DateTime time)
        {
            if (TimeZone.CurrentTimeZone.IsDaylightSavingTime(time))
            {
                DaylightTime dlt = TimeZone.CurrentTimeZone.GetDaylightChanges(time.Year);
                time = time.AddHours(-dlt.Delta.Hours);
            }
            return (time - _dt1970).Ticks / 10000;
        }

        private void imageViewerControl_DoubleClick(object sender, EventArgs e)
        {
            Point pos = Cursor.Position;
            Point pos2 = this.PointToScreen(this.Location);
            Point center = new Point(pos2.X + (this.Width / 2) - 10, pos2.Y + (this.Height / 2) - 10);
            int deltaX = pos.X - center.X;
            int deltaY = pos.Y - center.Y;

            if (Math.Abs(deltaX) > (this.Width / 20))
            {
                if (deltaX < 0) // mover a la izquierda
                {
                    if (Math.Abs(deltaY) > (this.Height / 20))
                    {
                        if (deltaY > 0) // mover para abajo
                            Ptz_do(PtzMoveRelativeTypes.DownLeft);
                        else
                            Ptz_do(PtzMoveRelativeTypes.UpLeft);
                    }
                    else
                    {
                        Ptz_do(PtzMoveRelativeTypes.Left);
                    }
                }
                else // mover a la derecha
                {
                    if (Math.Abs(deltaY) > (this.Height / 20))
                    {
                        if (deltaY > 0) // mover para abajo
                            Ptz_do(PtzMoveRelativeTypes.DownRight);
                        else
                            Ptz_do(PtzMoveRelativeTypes.UpRight);
                    }
                    else
                    {
                        Ptz_do(PtzMoveRelativeTypes.Right);
                    }
                }
            }
            else
            {
                if (Math.Abs(deltaY) > (this.Height / 20))
                {
                    if (deltaY > 0) // mover para abajo
                        Ptz_do(PtzMoveRelativeTypes.Down);
                    else
                        Ptz_do(PtzMoveRelativeTypes.Up);
                }
            }
        }

        private void imageViewerControl_Click(object sender, EventArgs e)
        {
            this.OnClick(e);

            //if (!imageViewerControl.IsConnected)
            if (!imageViewerControl.IsDisposed)
            {
                ReConnect();
            }
            else
            {
                imageViewerControl.Initialize();// ImageImageViewerActiveX.LiveStart();
            }
        }

        void imageViewerControl_MouseWheel(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (currentCamera != null)
            {
                currentCamera.PtzMoveRelative(VideoOS.Platform.SDK.Config.PtzMoveRelativeTypes.ZoomIn, 1, e.Delta / 120);
            }
        }

        public override void clearVms()
        {
            this.Dispose();
        }


        public override bool ReleaseConnectionExport()
        {
            List<IExporter> tempList = new List<IExporter>();
            tempList = exportersList;
            int count = -1;
            bool returnMethod = true;


            foreach (var exp in exportersList)
            {
              if ((exp.Progress == 0) || (exp.Progress == 100) || (exp.Progress > 98))
                {
                   
                }
                else
                {
                    return false;
                }
              return true;
            }
            return true;
            //if (returnMethod)
            //{
            //    return true;
            //}
            //else
            //{
            //    for (int i = 0; i <= count; i++)
            //    {
            //        exportersList.RemoveAt(i);
            //    }
            //    return false;
            //}

            
        }


        public override bool VerifyConnectionExport()
        {
            List<IExporter> tempList = new List<IExporter>();
            tempList = exportersList;
            int count = -1;
            bool returnMethod = true;
            List <int> position = new List<int>();
            totalExported = 0;

            foreach (var exp in tempList)
            {
                count++;

                if ((exp.Progress == 0) || (exp.Progress == 100) || (exp.Progress > 98))
                {
                    totalExported++;
                    position.Add(count);
                }
               
            }

            if (totalExported != 0)
            {
                bool firstTime = true;

                foreach (int pos in position)
                {
                    int aux = pos;

                    if (firstTime)
                    {
                        exportersList.RemoveAt(aux);
                        firstTime = false;
                    }
                    else
                    {
                        aux = aux - 1;
                        exportersList.RemoveAt(aux);
                    }
                    
                }
                return true;
            
            }
            else { return false; }


        }

        /// <summary>
        /// Stop the playback
        /// </summary>
        public override void InitStopPlayback()
        {
            //if (EnvironmentManager.Instance.Mode == Mode.ClientLive)
            //{
            //    EnvironmentManager.Instance.Mode = Mode.ClientPlayback;
            //    //buttonMode.Text = "Current mode: Playback";
            //}
            //else
            //{
            //    EnvironmentManager.Instance.Mode = Mode.ClientLive;
            //    //buttonMode.Text = "Current mode: Live";
            //}

            EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                        MessageId.SmartClient.PlaybackCommand,

                                                        new PlaybackCommandData() { Command = PlaybackData.PlayStop }));
            EnvironmentManager.Instance.SendMessage(new VideoOS.Platform.Messaging.Message(
                                                        MessageId.SmartClient.PlaybackCommand,
                                                        new PlaybackCommandData() { Command = PlaybackData.Goto, DateTime = PlaybackDate }));

            //EnvironmentManager.Instance.Mode = Mode.ClientPlayback;
            ////buttonMode.Text = "Current mode: Playback";
            //_speed = 0.0;
        }

        public override int TotalExported()
        {
            return totalExported;
        }


    }
}
