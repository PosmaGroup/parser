﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VideoOS.Platform.Proxy.Alarm;
using System.ComponentModel;
using System.ServiceModel;
using System.Threading;

namespace Smartmatic.SmartCad.Vms.Milestone
{
    public class MilestoneAlarmManager : VmsAlarmManager
    {
        private Thread _thread;
        private AlarmCommandClient _alarmCommandClient;
        AlarmFilter _filter = new AlarmFilter();
        OrderBy _orderBy = new OrderBy();
        DateTime _lastUsedTimestamp = DateTime.Now.ToUniversalTime();
        Random _rand = new Random();
        private bool _exit = false;
        public override event AlarmsReceived AlarmsReceived;
        public override event StatsReceived StatsReceived;
        private static AlarmLine[] _alarms = null;
        private Statistic[] _stats = null;
        private BindingList<ObservableAlarmLine> _alarmObserverCollection = new BindingList<ObservableAlarmLine>();
        private string _server;
        private string _alarmName;
        private int _statsNew = -1;
        private int _statsInProgress = -1;
        private int _statsOnHold = -1;
        private int _statsClosed = -1;
        private int _statsP1 = -1;
        private int _statsP2 = -1;
        private int _statsP3 = -1;
        private int _statsTotal = -1;
        private string _musicfile = string.Empty;
#if PLAY_WITH_DOTNET
        private System.Media.SoundPlayer _player = new System.Media.SoundPlayer();
#endif

        public override void  Connect(string serverIp)
        {
            if (_thread != null)
            {
                // This ought not happen, but just in case...
                _thread.Abort();
                _thread = null;
            }

            _server = serverIp;
            _alarmName = "";
            ConfigureFilter();

            // Have a worker thread request alarms in the background
            _thread = new Thread(AlarmThread);
            _thread.Start();
        }

        public override void Disconnect()
        {
            if (_thread != null)
            {
                _thread.Abort();
                _thread = null;
            }
        }

        /// <summary>
        /// Configure which alarms you wish to request
        /// </summary>
        private void ConfigureFilter()
        {
            // Create a new list of conditions
            List<VideoOS.Platform.Proxy.Alarm.Condition> cList = new List<VideoOS.Platform.Proxy.Alarm.Condition>();

            // Create a new condition. 
            VideoOS.Platform.Proxy.Alarm.Condition cond = new VideoOS.Platform.Proxy.Alarm.Condition();

            // In this case we want to select all alarms with a specific message entered in the upper entry field
            cond.Operator = Operator.Equals;
            cond.Target = Target.State;
            cond.Value = 1;
            // Add the condition to the list of conditions
            cList.Add(cond);

            // Filtering on priority 1 will in almost any case give some alarms
            // cond.Operator = Operator.Equals;
            // cond.Target = Target.Priority;
            // cond.Value = 1;
            // Add the condition to the list of conditions
            // cList.Add(cond);

            // You could create more conditions and add them. They would be ANDed.

            // Apply the list of conditions to our alarm filter
            _filter.Conditions = cList.ToArray();

            // We want the newest alarms first
            _orderBy.Order = Order.Descending;
            _orderBy.Target = Target.State;
            _filter.Orders = new OrderBy[] { _orderBy };
        }

        /// <summary>
        /// This is the entry point for the worker thread requesting alarms in the background
        /// </summary>
        private void AlarmThread()
        {
            if (OpenAlarmCommandClient())
            {
                while (!_exit)
                {
                    if (!GetAlarmData())
                        break;
                    System.Threading.Thread.Sleep(1000);
                }
            }
        }


        /// <summary>
        /// Query a list of filtered alarm lines and the statistics for all alarms on this server
        /// </summary>
        /// <returns></returns>
        private bool GetAlarmData()
        {
            // We need an array of alarm lines to receive data to. An alarm line is the "short version" of an alarm
            try
            {
                // We want to have a rolling window always showing the 10 newest alarms matching the filter
                // That is ok for a sample program playing Xmas songs. Your application probably needs to be somewhat smarter.
                AlarmLine[] _newAlarms = _newAlarms = _alarmCommandClient.GetAlarmLines(0, 200, _filter);
               
                List<VmsAlarm> toAdd = new List<VmsAlarm>();
                List<VmsAlarm> toDelete = new List<VmsAlarm>();

                if (_alarms != null)
                {
                    foreach (AlarmLine oldAlarm in _alarms)
                    {
                        bool contains = false;
                        foreach (AlarmLine newAlarm in _newAlarms)
                        {
                            if (oldAlarm.Id == newAlarm.Id)
                            {
                                contains = true;
                                break;
                            }
                        }
                        if (!contains)
                            toDelete.Add(new VmsAlarm() { AlarmId = oldAlarm.Id });
                    }

                    foreach (AlarmLine newAlarm in _newAlarms)
                    {
                        bool contains = false;
                        foreach (AlarmLine oldAlarm in _alarms)
                        {
                            if (oldAlarm.Id == newAlarm.Id)
                            {
                                contains = true;
                                break;
                            }
                        }
                        if (!contains)
                        {
                            toAdd.Add(new VmsAlarm()
                            {
                                AlarmId = newAlarm.Id,
                                CameraId = newAlarm.CameraId.ToString(),
                                Description = newAlarm.CustomTag,
                                Priority = newAlarm.Priority == 3 ? VmsAlarmPriority.Low : newAlarm.Priority == 2 ? VmsAlarmPriority.Medium : VmsAlarmPriority.High,
                                StartDate = newAlarm.Timestamp,
                                Status = newAlarm.State == 11 ? VmsAlarmStatus.Closed : newAlarm.State == 4 ? VmsAlarmStatus.InProgress : VmsAlarmStatus.New,
                            });
                        }
                    }
                }
                else
                {
                    foreach (AlarmLine newAlarm in _newAlarms)
                    {
                        toAdd.Add(new VmsAlarm()
                        {
                            AlarmId = newAlarm.Id,
                            CameraId = newAlarm.CameraId.ToString(),
                            Description = newAlarm.CustomTag,
                            Priority = newAlarm.Priority == 3 ? VmsAlarmPriority.Low : newAlarm.Priority == 2 ? VmsAlarmPriority.Medium : VmsAlarmPriority.High,
                            StartDate = newAlarm.Timestamp,
                            Status = newAlarm.State == 11 ? VmsAlarmStatus.Closed : newAlarm.State == 4 ? VmsAlarmStatus.InProgress : VmsAlarmStatus.New,
                        });
                    }
                }

                //// We need to update the UI in the UI thread. Please note that if you want to access the list view from other threads also, you must provide the thread safety.
                if (AlarmsReceived != null && (toAdd.Count > 0 || toDelete.Count > 0))
                    AlarmsReceived(toAdd, toDelete);

                _alarms = _newAlarms;

                // We also want to get some general statistics
                //_stats = _alarmCommandClient.GetStatistics();

                //// We need to update the UI in the UI thread. Please note that if you want to access the list view from other threads also, you must provide the thread safety.
                //if (_stats != null)
                //   Dispatcher.Invoke(_onStatsReceived);
            }
            catch (Exception e)
            {
                //MessageBox.Show(e.Message, "Exception");
                //ResetUiDelegate resetUiDelegate = new ResetUiDelegate(resetUi);
                //_buttonStop.Dispatcher.Invoke(resetUiDelegate);
                return false;
            }

            // If one of the alarms is all new (newer than latest new alarm), sound the alarm
            if (_alarms != null && _alarms.Length > 0)
            {
                if (_alarms[0].Timestamp > _lastUsedTimestamp)
                {
                    _lastUsedTimestamp = _alarms[0].Timestamp;
                    SoundTheAlarm();
                }
            }
            return true;
        }
        
        /// <summary>
        /// Updates the alarm
        /// </summary>
        /// <param name="vmsAlarm"></param>
        /// <param name="newStatus"> 1 new, 2 InProgress, 3 Hold, 4 Closed</param>
        public override void UpdateAlarm(VmsAlarm vmsAlarm, int newStatus)
        {
            _alarmCommandClient.UpdateAlarm(vmsAlarm.AlarmId, "", newStatus,
                vmsAlarm.Priority == VmsAlarmPriority.Low ? 3 : vmsAlarm.Priority == VmsAlarmPriority.Medium ? 2 : 1,
                vmsAlarm.StartDate, ""); 
        }

        /// <summary>
        /// Play a random .wav file from MyMusic\AlarmSounds
        /// </summary>
        private void SoundTheAlarm()
        {
            try
            {
                // Play a random wav file from subdirectory Xmas in MyMusic, rescan directory every time to allow new music to be dropped in
                string mymusicpath = Environment.GetFolderPath(Environment.SpecialFolder.MyMusic);
                string myXmasmusicpath = System.IO.Path.Combine(mymusicpath, "AlarmSounds");
                System.IO.DirectoryInfo dirInfo = new System.IO.DirectoryInfo(myXmasmusicpath);
                System.IO.FileInfo[] files = dirInfo.GetFiles("*.wav");
                _musicfile = files[_rand.Next(0, files.Length - 1)].Name;

#if PLAY_WITH_DOTNET
                _player.Stop();
                _player.SoundLocation = System.IO.Path.Combine(myXmasmusicpath, _musicfile);
                _player.Play();
#else
                // This allows you to use the installed player, which hopefully plays more file formats than the .NET player
                System.Diagnostics.Process.Start(System.IO.Path.Combine(myXmasmusicpath, _musicfile));
#endif
            }
            catch (Exception e)
            {
                // If the directory MyMusic\AlarmSounds was not created, or if no wav files were put there, we silently continue
            }
        }

        /// <summary>
        /// Create an instance of the WSDL generated client from AlarmCommand.cs
        /// </summary>
        /// <returns></returns>
        private bool OpenAlarmCommandClient()
        {
            try
            {
                BasicHttpBinding binding = new BasicHttpBinding();
                binding.MaxReceivedMessageSize = 2000000;
                binding.MaxBufferPoolSize = 2000000;
                binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
                binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Ntlm;

                // The event server usually runs on the same computer as the management server. Its service will listen on port 22331 by default
                if (!_server.Contains(':'))
                    _server += ":22331";
                string alarmServiceUri = "http://" + _server + "/Central/AlarmService";
                _alarmCommandClient = new AlarmCommandClient(binding, new EndpointAddress(new Uri(alarmServiceUri), EndpointIdentity.CreateSpnIdentity("CentralServerAlarmCommand")));
                _alarmCommandClient.ClientCredentials.Windows.ClientCredential = System.Net.CredentialCache.DefaultNetworkCredentials;
            }
            catch (Exception e)
            {
                //MessageBox.Show("Error opening AlarmCommandClient: " + e.Message + ((e.InnerException != null) ? (" - " + e.InnerException.Message) : ""), "Exception");
                //ResetUiDelegate resetUiDelegate = new ResetUiDelegate(resetUi);
                //_buttonStop.Dispatcher.Invoke(resetUiDelegate);
                return false;
            }

            try
            {
                _alarmCommandClient.Open();
                if (!(_alarmCommandClient.State != System.ServiceModel.CommunicationState.Opening ||
                      _alarmCommandClient.State != System.ServiceModel.CommunicationState.Opened))
                {
                    //ResetUiDelegate resetUiDelegate = new ResetUiDelegate(resetUi);
                    //_buttonStop.Dispatcher.Invoke(resetUiDelegate);
                    return false;
                }
            }

            catch (Exception e)
            {
               // MessageBox.Show("Error opening AlarmCommandClient: " + e.Message + ((e.InnerException != null) ? (" - " + e.InnerException.Message) : ""));
                //ResetUiDelegate resetUiDelegate = new ResetUiDelegate(resetUi);
                //_buttonStop.Dispatcher.Invoke(resetUiDelegate);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Show the statistics data about all alarms
        /// </summary>
        public void onStatsReceivedMethod()
        {
            foreach (Statistic stat in _stats)
            {
                switch (stat.Type)
                {
                    case StatisticType.Priority:
                        switch (stat.Number)
                        {
                            case 1:
                                _statsP1 = stat.Value;
                                NotifyPropertyChanged("StatsP1");
                                break;
                            case 2:
                                _statsP2 = stat.Value;
                                NotifyPropertyChanged("StatsP2");
                                break;
                            case 3:
                                _statsP3 = stat.Value;
                                NotifyPropertyChanged("StatsP3");
                                break;
                            default:
                                break;
                        }
                        break;
                    case StatisticType.State:
                        switch ((AlarmStates)stat.Number)
                        {
                            case AlarmStates.New:
                                _statsNew = stat.Value;
                                NotifyPropertyChanged("StatsNew");
                                break;
                            case AlarmStates.InProgress:
                                _statsInProgress = stat.Value;
                                NotifyPropertyChanged("StatsInProgress");
                                break;
                            case AlarmStates.OnHold:
                                _statsOnHold = stat.Value;
                                NotifyPropertyChanged("StatsOnHold");
                                break;
                            case AlarmStates.Closed:
                                _statsClosed = stat.Value;
                                NotifyPropertyChanged("StatsClosed");
                                break;
                            default:
                                break;
                        }
                        break;
                    case StatisticType.Total:
                        _statsTotal = stat.Value;
                        NotifyPropertyChanged("StatsTotal");
                        break;
                    default:
                        break;
                }
            }
        }


        #region Events and Protected Methods

        /// <summary>
        /// Needed for coordination with WPF controls
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Needed for coordination with WPF controls
        /// </summary>
        /// <param name="propertyName"></param>
        protected void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
    }
}
