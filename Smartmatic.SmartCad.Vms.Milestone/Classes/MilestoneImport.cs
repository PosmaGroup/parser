﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using VideoOS.Platform.SDK.Config;
using System.Net;
using VideoOS.Platform.Login;
using VideoOS.Platform;
using VideoOS.Platform.SDK.Platform;

namespace Smartmatic.SmartCad.Vms.Milestone
{
    class MilestoneImport : ImportManager
    {
        enum Authorizationmodes
        {
            DefaultWindows,
            Windows,
            Basic
        };

        #region fields
        static string _user;
        static string _url;
        static Authorizationmodes _auth;
        static string _pass;
        private string serverIp;
        private AuthenticationType authenticationType;
        private SystemInfo sysinfo;
        IList _list = new ArrayList();
        #endregion

        public MilestoneImport()
        :base()
        {

        }

        /// <summary>
        /// Initialize the class
        /// </summary>
        /// <param name="serverIp">Ip for the VMS server</param>
        /// <param name="login">User Login</param>
        /// <param name="password">User Password</param>
        public override void Initialize(string serverIp, string login, string password)
        {
            _user = login;
            _pass = password;
            this.authenticationType = AuthenticationType.Basic;
            this.serverIp = serverIp;
        }

        /// <summary>
        /// Connect to the server to import the cameras
        /// </summary>
        /// <returns>0 Not connected, 1 Connected</returns>
        public override int Export()
        {
            string urlManagementService = string.Format("http://{0}", serverIp);

            Dictionary<Guid, Item> result = new Dictionary<Guid, Item>();
            try
            {
                VideoOS.Platform.SDK.Environment.Initialize();
                _url = "http://" + serverIp;
                _auth = Authorizationmodes.Basic;

                if (LoginUsingCurrentCredentials())
                {
                    Cameras = Find();

                    //if (Find())
                    //{
                        //GetRes();
                        //GetDefaultStream();
                        //GetStreams();
                        /*return 1;*/
                    //}
                    //Console.WriteLine(Environment.NewLine + "Press any key to exit.");
                }

                //////////////////////////////////
                //CredentialCache credentials = new CredentialCache();
                //String[] serverIpSplit = serverIp.Split(':');
                //credentials.Add(new Uri(urlManagementService), "Basic", new NetworkCredential(login, password));
                ////credentials.Add(serverIpSplit[0], Int32.Parse(serverIpSplit[1]), "Basic", new NetworkCredential(login, password));
                ////LoginSettings loginSettings = new LoginSettings(new Uri(urlManagementService), credentials, new Guid(), urlManagementService);
                //LoginSettings loginSettings = new LoginSettings(new Uri(urlManagementService), new NetworkCredential(login, password), new Guid(), urlManagementService);
                //UserContext userContext = new UserContext();

                //sysinfo = new SystemInfo(loginSettings, userContext);
                ////  sysinfo = new SystemInfo();
                //bool z = sysinfo.Login();// Connect(urlManagementService, login, password, authenticationType);
                //bool x = sysinfo.ServerConnectionCheck();
                //bool y = sysinfo.IsConnected;
                //bool w = sysinfo.IsLoggedIn();
                //CameraCollection cameras = sysinfo.Cameras;
                //if (cameras != null)
                //{
                //    foreach (ICamera cam in cameras)
                //    {
                //        list.Add(cam.Name + "\\" + cam.Id);
                //    }
                //    Cameras = list;
                //}
                //return 1;
                ////////////////////////////////////
                return 1;
            }
            catch (Exception e)
            {
                return 0;
            }

        }

        static private bool LoginUsingCurrentCredentials()
        {
            Uri uri = new UriBuilder(_url).Uri;
            NetworkCredential nc;

            CredentialCache cc = VideoOS.Platform.Login.Util.BuildCredentialCache(uri, _user, _pass, "Basic");
            VideoOS.Platform.SDK.Environment.AddServer(uri, cc);

            try
            {
                
                if (VideoOS.Platform.SDK.Environment.IsLoggedIn(uri)){
                    VideoOS.Platform.SDK.Environment.Logout(uri);
                }


                VideoOS.Platform.SDK.Environment.Login(uri);
            }
            catch (ServerNotFoundMIPException snfe)
            {
                //Console.WriteLine("Server not found: " + snfe.Message);
                VideoOS.Platform.SDK.Environment.RemoveServer(uri);
                return false;
            }
            catch (InvalidCredentialsMIPException ice)
            {
                //Console.WriteLine("Invalid credentials for: " + ice.Message);
                VideoOS.Platform.SDK.Environment.RemoveServer(uri);
                return false;
            }
            catch (Exception)
            {
                //Console.WriteLine("Internal error connecting to: " + uri.DnsSafeHost);
                VideoOS.Platform.SDK.Environment.RemoveServer(uri);
                return false;
            }

            //Console.WriteLine("Login succeeded.");
            return true;
        }

            static private IList Find()
        {
            Dictionary<Guid, Item> result = new Dictionary<Guid, Item>(); 

            // Get the root level Items, which are the servers added. Configuration is not loaded implicitly yet.
            List<Item> list = Configuration.Instance.GetItems();
            IList listCamera = new ArrayList();
            // For each root level Item, check the children. We are certain, none of the root level Items is a camera
            foreach (Item item in list)
            {
                //CheckChildren(item);
                Dictionary<Guid, Item> allCameras = new Dictionary<Guid, Item>();
                List<Item> allItems = item.GetChildren();
                FindAllCameras(allItems, item.FQID.ServerId.Id, allCameras);
                result = allCameras;

                foreach (KeyValuePair<Guid, Item> camera in result)
                {
                    listCamera.Add(camera.Value.Name + "\\" +camera.Value.FQID.ObjectId);
                }
            }

            //if (result == null)
            //    Console.WriteLine("No camera");
            //else
            //    Console.WriteLine("Si Hay camaras: " + result.Count);

            //Console.WriteLine(" ");
            return listCamera;
        }

        static private void FindAllCameras(List<Item> items, Guid recorderGuid, Dictionary<Guid, Item> result)
        {
            int count = 0;
            foreach (Item item in items)
            {
                if (item.FQID.Kind == Kind.Camera && item.FQID.ParentId == recorderGuid && item.FQID.FolderType == FolderType.No)
                {
                    if (result.ContainsKey(item.FQID.ObjectId) == false)
                        result[item.FQID.ObjectId] = item;
                }
                else
                    if (item.FQID.FolderType != FolderType.No)
                {
                    FindAllCameras(item.GetChildren(), recorderGuid, result);
                }

            }
        }

        public override void Disconnect()
        {
            try
            {
                this.sysinfo.Disconnect();
                sysinfo = null;
            }
            catch
            {
            }
        }

    }
}
