﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Threading;
using System.ServiceModel;

using VideoOS.Common.Proxy.Server;
using videoos.net._2.XProtectCSRecorderStatus;
using SmartCadCore.Common;
using VideoOS.Platform.SDK.Proxy.Server;
using VideoOS.Platform.SDK.Config;
using System.Net;
using VideoOS.Platform.Login;
using VideoOS.Platform;

namespace Smartmatic.SmartCad.Vms.Milestone
{
    class MilestoneEventManager : EventManager
    {
        #region fields
        private string login;
        private string password;
        private string serverIp;
        private AuthenticationType authenticationType;
        private SystemInfo sysinfo;
        private string portrec;
        private string recorder;
        private StartStatusSessionResponse startResponse;
        private StartStatusSessionRequest startRequest;
        private System.Guid sessionId;
        private SubscribeEventStatusRequest subscribeEventRequest;
        private IList Inputs = new ArrayList();
        private IList Outputs = new ArrayList();
        private IList Cameras = new ArrayList();
        private IList AlarmManagers = new ArrayList();
        private Thread AlarmThread;
        private bool isStarted = true;

        #endregion

        public MilestoneEventManager()
        :base()
        {

        }

        #region properties
        public Uri ServerAddress
        {
            get
            {
                Uri uri = null;
                if (!string.IsNullOrEmpty(serverIp))
                {
                    uri = new Uri("http://" + serverIp);
                }
                return uri;
            }
        }
        #endregion

        #region methods override

        /// <summary>
        /// Initialize the class
        /// </summary>
        /// <param name="serverIp">Ip for the VMS server</param>
        /// <param name="login">User Login</param>
        /// <param name="password">User Password</param>
        public override void Initialize(string serverIp, string login, string password)
        {
            this.login = login;
            this.password = password;
            this.authenticationType = AuthenticationType.WindowsDefault;
            this.serverIp = serverIp;
        }

        /// <summary>
        /// Connect to the server of events
        /// </summary>
        /// <returns>0 Not connected, 1 Connected</returns>
        public override int Connect()
        {
            string urlManagementService = string.Format("http://{0}", serverIp);
            try
            {
                CredentialCache credentials = new CredentialCache();
                credentials.Add(new Uri(urlManagementService), "Basic", new NetworkCredential(login, password));
                LoginSettings loginSettings = new LoginSettings(new Uri(urlManagementService), credentials, new Guid(), urlManagementService);
                UserContext userContext = new UserContext();


                sysinfo = new SystemInfo(loginSettings, userContext);

             //   sysinfo = new SystemInfo(urlManagementService, login, password, authenticationType);
                #region Initialize Temporal List
                foreach (RecorderInfo rec in sysinfo.ConfigurationInfo.Recorders)
                {
                    #region Cameras
                    foreach (CameraInfo cam in rec.Cameras)
                        Cameras.Add(cam);
                    #endregion
                    #region Input
                    foreach (InputInfo inp in rec.Inputs)
                        if (inp.InputSecurity.ReadInput)
                        {
                            Inputs.Add(inp);
                        }
                    #endregion
                    #region Output
                    foreach (OutputInfo outs in rec.Outputs)
                        if (outs.OutputSecurity.Activate)
                        {
                            Outputs.Add(outs);
                        }
                    #endregion
                }
                #endregion

                #region Alarms
                if (Inputs.Count > 0 || Outputs.Count > 0)
                {
                    foreach (Recorder rec in sysinfo.Recorders)
                    {
                        RecorderStatusServiceSoap client;
                        recorder = rec.HostName.ToString();
                        portrec = rec.Port.ToString();
                        string url = string.Format("http://{0}:{1}/RecorderStatusService/RecorderStatusService.asmx", rec.HostName, rec.Port);
                        EndpointAddress webServiceAddress = new EndpointAddress(url);
                        BasicHttpBinding binding = new BasicHttpBinding();
                        client = new RecorderStatusServiceSoapClient(binding, webServiceAddress);
                        startRequest = new StartStatusSessionRequest();
                        startRequest.Body = new StartStatusSessionRequestBody();
                        startRequest.Body.token = sysinfo.Token;
                        startResponse = client.StartStatusSession(startRequest);
                        sessionId = startResponse.Body.StartStatusSessionResult;
                        ArrayOfGuid subscribeTheseEventIds = new ArrayOfGuid();

                        subscribeTheseEventIds.Add(MilestoneCommon.InputActivated);
                        subscribeTheseEventIds.Add(MilestoneCommon.InputDeactivated);
                        subscribeTheseEventIds.Add(MilestoneCommon.OutputActivated);
                        subscribeTheseEventIds.Add(MilestoneCommon.OutputDeactivated);

                        subscribeEventRequest = new SubscribeEventStatusRequest();
                        subscribeEventRequest.Body = new SubscribeEventStatusRequestBody();
                        subscribeEventRequest.Body.token = sysinfo.Token;
                        subscribeEventRequest.Body.statusSessionId = sessionId;
                        subscribeEventRequest.Body.eventIds = subscribeTheseEventIds;
                        client.SubscribeEventStatus(subscribeEventRequest);
                        AlarmManagers.Add(client);
                    }
                }
                #endregion
                return 1;
            }
            catch
            {
                return 0;
            }
            
        }

        /// <summary>
        /// Start the alarm manager
        /// </summary>
        public override void Start()
        {
            isStarted = true;
            AlarmThread = new Thread(new ThreadStart(UpdateAlerts));
            AlarmThread.Start();
        }

        /// <summary>
        /// Stop the alarm manager
        /// </summary>
        public override void Stop()
        {
            isStarted = false;
            AlarmThread.Abort();
        }

        public override event AlarmEvent AlarmEventManager;
        #endregion

        private void UpdateAlerts()
        {
            while (isStarted)
            {
                GetStatusRequest statusRequest = new GetStatusRequest();
                statusRequest.Body = new GetStatusRequestBody();
                statusRequest.Body.token = sysinfo.Token;
                statusRequest.Body.statusSessionId = sessionId;

                foreach (RecorderStatusServiceSoap client in AlarmManagers)
                {
                    GetStatusResponse statusResponse = client.GetStatus(statusRequest);
                    StatusBase[] stats = statusResponse.Body.GetStatusResult;

                    foreach (StatusBase stat in stats)
                    {
                        if (stat.GetType() == typeof(EventStatus))
                        {
                            EventStatus eStat = (EventStatus)stat;
                            #region input
                            foreach (InputInfo input in Inputs)
                            {
                                if (input.DeviceId == eStat.SourceId && eStat.EventId == MilestoneCommon.InputActivated)
                                {
                                    Alarm data = new Alarm();
                                    data.DeviceEventName = input.Name;
                                    data.EventTime = eStat.Time;
                                    //string[] words = input.Name.Split('(');
                                    //string word = words[1].Substring(0, words[1].Length - 1);

                                    //foreach (CameraInfo cam in Cameras)
                                    //{
                                    //    if (cam.Name.Contains(word))
                                    //    {
                                    //        data.CameraName = cam.;
                                    //        break;
                                    //    }
                                    //}

                                    foreach (CameraInfo cam in Cameras)
                                    {
                                        if (cam.DeviceId == input.DeviceId)
                                        {
                                            data.CameraId = cam.DeviceId.ToString();
                                            break;
                                        }
                                    }


                                    AlarmArgs EvtArgs = new AlarmArgs(data);
                                    if (AlarmEventManager != null)
                                        AlarmEventManager(this, EvtArgs);
                                    break;
                                }
                            }
                            #endregion
                            #region output
                            foreach (OutputInfo output in Outputs)
                            {
                                if (output.DeviceId == eStat.SourceId && eStat.EventId == MilestoneCommon.OutputActivated)
                                {
                                    Alarm data = new Alarm();
                                    data.DeviceEventName = output.Name;
                                    data.EventTime = eStat.Time;
                                    //string[] words = output.Name.Split('(');
                                    //string word = words[1].Substring(0, words[1].Length - 1);

                                    //foreach (CameraInfo cam in Cameras)
                                    //{
                                    //    if (cam.Name.Contains(word))
                                    //    {
                                    //        data.CameraName = cam.Name;
                                    //        break;
                                    //    }
                                    //}

                                    foreach (CameraInfo cam in Cameras)
                                    {
                                        if (cam.DeviceId == output.DeviceId)
                                        {
                                            data.CameraId = cam.DeviceId.ToString();
                                            break;
                                        }
                                    }

                                    AlarmArgs EvtArgs = new AlarmArgs(data);
                                    if (AlarmEventManager != null)
                                        AlarmEventManager(this, EvtArgs);
                                    break;
                                }
                            }
                            #endregion

                            if (eStat.EventId == MilestoneCommon.CommunicationError)
                            {
                                Alarm data = new Alarm();
                                data.DeviceEventName = ResourceLoader.GetString2("communicationError");
                                data.EventTime = eStat.Time;
                                data.CameraId = "";

                                AlarmArgs EvtArgs = new AlarmArgs(data);
                                if (AlarmEventManager != null)
                                    AlarmEventManager(this, EvtArgs);
                                break;
                            }
                            else if (eStat.EventId == MilestoneCommon.CommunicationStarted)
                            {
                                Alarm data = new Alarm();
                                data.DeviceEventName = ResourceLoader.GetString2("communicationStarted");
                                data.EventTime = eStat.Time;
                                data.CameraId = "";

                                AlarmArgs EvtArgs = new AlarmArgs(data);
                                if (AlarmEventManager != null)
                                    AlarmEventManager(this, EvtArgs);
                                break;
                            }
                            else if (eStat.EventId == MilestoneCommon.CommunicationStopped)
                            {
                                Alarm data = new Alarm();
                                data.DeviceEventName = ResourceLoader.GetString2("communicationStopped");
                                data.EventTime = eStat.Time;
                                data.CameraId = "";

                                AlarmArgs EvtArgs = new AlarmArgs(data);
                                if (AlarmEventManager != null)
                                    AlarmEventManager(this, EvtArgs);
                                break;
                            }
                            else if (eStat.EventId == MilestoneCommon.DatabaseDiskFull)
                            {
                                Alarm data = new Alarm();
                                data.DeviceEventName = ResourceLoader.GetString2("DatabaseDiskFull");
                                data.EventTime = eStat.Time;
                                data.CameraId = "";

                                AlarmArgs EvtArgs = new AlarmArgs(data);
                                if (AlarmEventManager != null)
                                    AlarmEventManager(this, EvtArgs);
                                break;
                            }
                            else if (eStat.EventId == MilestoneCommon.DatabaseDiskUnavailable)
                            {
                                Alarm data = new Alarm();
                                data.DeviceEventName = ResourceLoader.GetString2("DatabaseDiskUnavailable");
                                data.EventTime = eStat.Time;
                                data.CameraId = "";

                                AlarmArgs EvtArgs = new AlarmArgs(data);
                                if (AlarmEventManager != null)
                                    AlarmEventManager(this, EvtArgs);
                                break;
                            }
                            else if (eStat.EventId == MilestoneCommon.DatabaseDiskAvailable)
                            {
                                Alarm data = new Alarm();
                                data.DeviceEventName = ResourceLoader.GetString2("DatabaseDiskAvailable");
                                data.EventTime = eStat.Time;
                                data.CameraId = "";

                                AlarmArgs EvtArgs = new AlarmArgs(data);
                                if (AlarmEventManager != null)
                                    AlarmEventManager(this, EvtArgs);
                                break;
                            }
                            else if (eStat.EventId == MilestoneCommon.ArchiveDiskUnavailable)
                            {
                                Alarm data = new Alarm();
                                data.DeviceEventName = ResourceLoader.GetString2("ArchiveDiskUnavailable");
                                data.EventTime = eStat.Time;
                                data.CameraId = "";

                                AlarmArgs EvtArgs = new AlarmArgs(data);
                                if (AlarmEventManager != null)
                                    AlarmEventManager(this, EvtArgs);
                                break;
                            }
                            else if (eStat.EventId == MilestoneCommon.ArchiveDiskAvailable)
                            {
                                Alarm data = new Alarm();
                                data.DeviceEventName = ResourceLoader.GetString2("ArchiveDiskAvailable");
                                data.EventTime = eStat.Time;
                                data.CameraId = "";

                                AlarmArgs EvtArgs = new AlarmArgs(data);
                                if (AlarmEventManager != null)
                                    AlarmEventManager(this, EvtArgs);
                                break;
                            }
                        }
                    }
                }
                Thread.Sleep(2000);
            }
        }
    }
}
