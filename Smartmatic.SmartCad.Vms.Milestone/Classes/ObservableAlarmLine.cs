﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using VideoOS.Platform.Proxy.Alarm;
using milestonesystems;

namespace Smartmatic.SmartCad.Vms.Milestone
{
    public enum AlarmStates
    {
        New = 1,
        InProgress = 4,
        OnHold = 9,
        Closed = 11,
    }

    public class ObservableAlarmLine : INotifyPropertyChanged
    {
        private AlarmLine _alarmLine = null;

        /// <summary>
        /// Constructor for use when alarm data exists at creation time
        /// </summary>
        public ObservableAlarmLine(AlarmLine alarmLine)
        {
            _alarmLine = alarmLine;
        }

        /// <summary>
        /// Gets or sets the AlarmLine data embedded in the ObservableAlarm
        /// </summary>
        public AlarmLine AlarmLine
        {
            get
            {
                return _alarmLine;
            }
            set
            {
                _alarmLine = value;
            }
        }

        /// <summary>
        /// Gets the identier as a number - human readable id
        /// </summary>
        public int LocalId
        {
            get
            {
                if (_alarmLine != null)
                {
                    return _alarmLine.LocalId;
                }
                else
                {
                    return -1;
                }
            }
        }

        /// <summary>
        /// Gets the state as a number
        /// </summary>
        public int State
        {
            get
            {
                if (_alarmLine != null)
                {
                    return _alarmLine.State;
                }
                else
                {
                    return -1;
                }
            }
            set
            {
                if (_alarmLine != null && _alarmLine.State != (ushort)value)
                {
                    _alarmLine.State = (ushort)value;
                    NotifyPropertyChanged("StateString");
                }
            }
        }

        /// <summary>
        /// Gets the state as a string
        /// </summary>
        public string StateString
        {
            get
            {
                if (_alarmLine != null)
                {
                    AlarmStates s = (AlarmStates)_alarmLine.State;
                    switch (s)
                    {
                        case AlarmStates.Closed:
                            return "Closed";
                        case AlarmStates.InProgress:
                            return "In progress";
                        case AlarmStates.New:
                            return "New";
                        case AlarmStates.OnHold:
                            return "On hold";
                        default:
                            return _alarmLine.State.ToString(); // States may have arbitrary values stored if some vendor wants to use that
                    }
                }
                else
                {
                    return "";
                }
            }
        }

        /// <summary>
        /// Gets the priority as a number
        /// </summary>
        public int Priority
        {
            get
            {
                if (_alarmLine != null)
                {
                    return _alarmLine.Priority;
                }
                else
                {
                    return -1;
                }
            }
            set
            {
                if (_alarmLine != null && _alarmLine.Priority != (ushort)value)
                {
                    _alarmLine.Priority = (ushort)value;
                }
            }
        }

        /// <summary>
        /// Gets the message
        /// </summary>
        public string Message
        {
            get
            {
                if (_alarmLine != null && _alarmLine.Message != null)
                {
                    return _alarmLine.Message.Trim();
                }
                else
                {
                    return "";
                }
            }
        }

        /// <summary>
        /// Gets the timestamp as a string
        /// </summary>
        public string Timestamp
        {
            get
            {
                if (_alarmLine != null && _alarmLine.Timestamp != null)
                {
                    DateTime tim = _alarmLine.Timestamp.ToLocalTime();
                    return tim.ToLongTimeString() + " " + tim.ToShortDateString();
                }
                else
                {
                    return "";
                }
            }
        }

        #region Events and Protected Methods

        /// <summary>
        /// Needed for coordination with WPF controls
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Needed for coordination with WPF controls
        /// </summary>
        /// <param name="propertyName"></param>
        protected void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }
}
