﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Smartmatic.SmartCad.Vms.Milestone
{
    class MilestoneCommon
    {
        #region Public GUID
        public static readonly Guid InputActivated = new Guid("836CA458-A833-4742-8EE0-64B2380984BD");
        public static readonly Guid InputDeactivated = new Guid("8666E3DC-57A7-4F38-9611-51D774EE7358");
        public static readonly Guid OutputActivated = new Guid("7A78F5BB-D8C3-4997-89B7-CAE72713B7DB");
        public static readonly Guid OutputDeactivated = new Guid("35742498-BCC5-4F0A-9800-827C9388D1CD");
        public static readonly Guid CommunicationError = new Guid("A334AF1C-4B4B-4957-9E5F-AB8CA07FEAB6");
        public static readonly Guid CommunicationStarted = new Guid("DD3E6464-7DC0-405A-A92F-6150587563E8");
        public static readonly Guid CommunicationStopped = new Guid("0EE90664-2924-42A0-A816-4129D0ECABDC");
        public static readonly Guid DatabaseDiskFull = new Guid("A4935AA3-C4B6-42A3-B4C5-6D4C20B043CF");
        public static readonly Guid DatabaseDiskUnavailable = new Guid("29986AA1-924B-4c8d-AAC7-A876883DC82C");
        public static readonly Guid DatabaseDiskAvailable = new Guid("FAB03C4D-0589-48c8-9328-E28B76C95AF0");
        public static readonly Guid ArchiveDiskUnavailable = new Guid("72C29AAE-DDB2-46df-B6FD-7F728A85F292");
        public static readonly Guid ArchiveDiskAvailable = new Guid("B7F0003B-1392-450e-B6CC-9BBE6E7DF065");
        #endregion
    }
}
