using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading;
using System.IO;
using System.ServiceModel;
using System.Net;
using System.Globalization;
using SmartCadCore.ClientData;
using SmartCadCore.Core;
using SmartCadCore.Common;
using Smartmatic.SmartCad.Service;
using SmartCadGuiCommon.Util;
using SmartCadGuiCommon;
using SmartCadControls.Controls;

namespace SmartCadMap
{
    static class Program
    {
        public static IDictionary<string, ApplicationPreferenceClientData> globalApplicationPreference;
        private static string ApplicationName = "Map";
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
#if ! DEBUG
                DevExpress.Data.CurrencyDataController.DisableThreadingProblemsDetection = true; 
#endif
                Application.ThreadException += new ThreadExceptionEventHandler(ApplicationGuiUtil.Application_ThreadException);                
                ApplicationUtil.ActivateProcess(Process.GetCurrentProcess());

                Process currentProcess = Process.GetCurrentProcess();
                Process[] processTemp = Process.GetProcesses();
                bool exit = false;
                if (processTemp.Length > 1)
                {
                    foreach (Process p in processTemp)
                    {
                        if (p.Id != currentProcess.Id && p.ProcessName.Contains("SmartCadMap"))
                        {
                            WindowsImports.ShowWindow(p.MainWindowHandle, (int)ShowStates.SW_RESTORE);
                            WindowsImports.SetForegroundWindow(p.MainWindowHandle);
                            exit = true;
                            break;
                        }
                    }
                    if (exit)
                        return;
                }

                SmartCadConfiguration.Load();
                ResourceLoader.UpdatedLanguage(new CultureInfo(SmartCadConfiguration.SmartCadSection.CultureElement.CultureLanguageElement.Name
                    + "-" + SmartCadConfiguration.SmartCadSection.CultureElement.CultureCountryElement.Name));

                object result = ServerServiceClient.GetServerService();

                if (result == null)
                {
                    MessageForm.Show(ResourceLoader.GetString2("UnableConnectToServer"), MessageFormType.Error);
                    return;
                }

                ConfigurationClientData cfg = ServerServiceClient.GetInstance().GetConfiguration();
                SmartCadConfiguration.SaveConfigurationInClient(cfg);

                if (!ApplicationUtil.VerifyLanguage(cfg))
                {
                    byte[] dll = ServerServiceClient.GetInstance().CheckLanguages(cfg.Language);
                    DirectoryInfo directory = Directory.CreateDirectory(SmartCadConfiguration.LanguageDirectoryClient + "\\" + cfg.Language);
                    using (FileStream fileDll = new FileStream(directory.FullName + "\\Smartmatic.SmartCad.Resources.resources.dll", FileMode.Create, FileAccess.Write))
                    {
                        fileDll.Write(dll, 0, dll.Length);
                    }
                }

                ResourceLoader.UpdatedLanguage(new CultureInfo(cfg.Language));

                ServerServiceClient.GetInstance().SetApplication(UserApplicationClientData.UserApplications.Map.ToString());
                SmartCadLoadInitialData.LoadInitialClientData();
                
                System.Threading.Timer checkServer = ApplicationGuiUtil.StartCheckServer();
                
                ApplicationGuiUtil.CheckErrorDetailButton();
                ServerServiceClient.GetInstance().ActivateApplication(ApplicationName, ApplicationUtil.GetMACAddress());
                if (args != null && args.Length > 0)
                {
                    try
                    {
                        SplashForm.ShowSplash();

                        object[] objs = ApplicationUtil.RetrieveArguments(args[0]);
                        string loginStr = objs[0] as string;
                        string passwordStr = objs[1] as string;
                        bool mainWindow = (bool)objs[2];

                        LoginForm login = new LoginForm(UserApplicationClientData.Map);
                        login.ValidateUser(loginStr, passwordStr);
                        NetworkCredential networkCredential = login.NetworkCredential;

                        ImpersonateUser impersonateUser = new ImpersonateUser();
                        impersonateUser.Impersonate(
                            networkCredential.Domain,
                            networkCredential.UserName,
                            networkCredential.Password
                        );

                        Environment.SetEnvironmentVariable("TMP", SmartCadConfiguration.DistFolder + "Temp");

                        ServerServiceClient serverServiceClient = login.ServerService;
                        LoadGlobalData();

                        MapFormDevX mapForm = new MapFormDevX();
                        mapForm.UserPassword = passwordStr;
                        foreach (Screen s in Screen.AllScreens)
                        {
                            if (s.Primary == mainWindow)
                            {
                                mapForm.TempBounds = s.Bounds;
                                break;
                            }
                        }
                        mapForm.ServerServiceClient = serverServiceClient;
                        mapForm.NetworkCredential = networkCredential;
                        mapForm.GlobalApplicationPreference = globalApplicationPreference;
                        mapForm.TrustedConnection = false;
                        mapForm.InitializeActions = new List<GisActionEventArgs>();
                        for (int i = 3; i < objs.Length; i++)
                        {
                            mapForm.InitializeActions.Add((GisActionEventArgs)objs[i]);
                        }

                        Application.Run(mapForm);
                    }
                    catch (Exception ex)
                    {
                        MessageForm.Show(ResourceLoader.GetString2("MustHaveAccessToOpenMapModule"), ex);
                    }
                    finally
                    {
                        try
                        {
                            SplashForm.CloseSplash();
                        }
                        catch
                        { }

                    }
                }
                else
                {
                    bool success = false;
                    //ConfigurationClientData configuration = ServerServiceClient.GetInstance().GetConfiguration();

                    do
                    {
                        LoginForm login = new LoginForm(UserApplicationClientData.Map);
                        
                        if (login.ShowDialog() == DialogResult.OK)
                        {
                            SplashForm.ShowSplash();
                            LoadGlobalData();
                            NetworkCredential networkCredential = login.NetworkCredential;

                            ImpersonateUser impersonateUser = new ImpersonateUser();
                            impersonateUser.Impersonate(
                                networkCredential.Domain,
                                networkCredential.UserName,
                                networkCredential.Password
                            );

                            Environment.SetEnvironmentVariable("TMP", SmartCadConfiguration.DistFolder + "Temp");

                            ServerServiceClient serverServiceClient = login.ServerService;

                            success = true;

                            MapFormDevX mapForm = new MapFormDevX();
                            mapForm.UserPassword = login.Password;
                            mapForm.ServerServiceClient = serverServiceClient;
                            mapForm.NetworkCredential = networkCredential;
                            mapForm.GlobalApplicationPreference = globalApplicationPreference;
                            mapForm.TrustedConnection = false;

                           Application.Run(mapForm); 
                        }
                        else
                        {
                            ServerServiceClient.GetInstance().CloseSession();
                            success = true;
                        }
                    }
                    while (!success);
                }

            }
            catch (EndpointNotFoundException ex)
            {
                SplashForm.CloseSplash();

                MessageForm.Show(ResourceLoader.GetString2("UnableConnectToServer"), ex);
            }
            catch (Exception ex)
            {
                SplashForm.CloseSplash();

                //MessageForm.Show(ResourceLoader.GetString2("GenericErrorApplication"), ex);
            }
            finally
            {
                try
                {
                    Process.GetCurrentProcess().Kill();                                            
                }
                catch { }
            }
        }        

        #region ApplicationPreferences

        private static void LoadGlobalData()
        {
            globalApplicationPreference = new Dictionary<string, ApplicationPreferenceClientData>();
            foreach (ApplicationPreferenceClientData preference in ServerServiceClient.GetInstance().SearchClientObjects(typeof(ApplicationPreferenceClientData)))
            {
                globalApplicationPreference.Add(preference.Name, preference);
            }
        }

        #endregion
        
    }
}
