using CrystalDecisions.CrystalReports.Engine;
using SmartCadControls.Controls;
namespace SmartCadReports.Gui
{
    partial class ReportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private new System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportForm));
            this.reportDocumentMain = new ReportDocument();
            this.labelExLogo = new LabelEx();
            this.ribbonControl = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barButtonItemUSPLogo = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemReportList = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemHelp = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemIncidentArchive = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemCCTVArchive = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroupReport = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupIncidents = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupGeneralOptions = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.barButtonItemPrint = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemSave = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.repositoryItemPictureEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.ribbonStatusBar1 = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.labelUser = new DevExpress.XtraBars.BarStaticItem();
            this.labelConnected = new DevExpress.XtraBars.BarStaticItem();
            this.labelDate = new DevExpress.XtraBars.BarStaticItem();
            this.labelLogo = new System.Windows.Forms.Label();
            this.ribbonPageGroupTools = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.barButtonItemOpenMap = new DevExpress.XtraBars.BarButtonItem();
            this.xtraTabbedMdiManager1 = new DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(this.components);
            this.ribbonPageGroupMaps = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.barButtonItemCrimeMap = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // labelExLogo
            // 
            this.labelExLogo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelExLogo.BackColor = System.Drawing.SystemColors.Control;
            this.labelExLogo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExLogo.FontFormat = FontFormat.GetFormat("LabelFormat");
            this.labelExLogo.Image = ((System.Drawing.Image)(resources.GetObject("labelExLogo.Image")));
            this.labelExLogo.Location = new System.Drawing.Point(794, 0);
            this.labelExLogo.Margin = new System.Windows.Forms.Padding(0);
            this.labelExLogo.Name = "labelExLogo";
            this.labelExLogo.Size = new System.Drawing.Size(57, 23);
            this.labelExLogo.TabIndex = 13;
            // 
            // ribbonControl
            // 
            this.ribbonControl.AllowMinimizeRibbon = false;
            this.ribbonControl.ExpandCollapseItem.Id = 0;
            this.ribbonControl.ExpandCollapseItem.Name = "";
            this.ribbonControl.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl.ExpandCollapseItem,
            this.barButtonItemUSPLogo,
            this.barButtonItemReportList,
            this.barButtonItemHelp,
            this.barButtonItemIncidentArchive,
            this.barButtonItemCCTVArchive,
            this.barButtonItemCrimeMap});
            this.ribbonControl.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl.MaxItemId = 9;
            this.ribbonControl.Name = "ribbonControl";
            this.ribbonControl.PageHeaderItemLinks.Add(this.barButtonItemHelp);
            this.ribbonControl.PageHeaderItemLinks.Add(this.barButtonItemUSPLogo);
            this.ribbonControl.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbonControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPictureEdit1});
            this.ribbonControl.ShowApplicationButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonControl.ShowExpandCollapseButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonControl.ShowToolbarCustomizeItem = false;
            this.ribbonControl.Size = new System.Drawing.Size(852, 119);
            this.ribbonControl.StatusBar = this.ribbonStatusBar1;
            this.ribbonControl.Toolbar.ShowCustomizeItem = false;
            this.ribbonControl.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            // 
            // barButtonItemUSPLogo
            // 
            this.barButtonItemUSPLogo.AllowRightClickInMenu = false;
            this.barButtonItemUSPLogo.Caption = "barButtonItemUSPLogo";
            this.barButtonItemUSPLogo.Id = 2;
            this.barButtonItemUSPLogo.Name = "barButtonItemUSPLogo";
            this.barButtonItemUSPLogo.SmallWithoutTextWidth = 57;
            // 
            // barButtonItemReportList
            // 
            this.barButtonItemReportList.Caption = "barButtonItemReportList";
            this.barButtonItemReportList.Id = 4;
            this.barButtonItemReportList.ItemShortcut = new DevExpress.XtraBars.BarShortcut(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
                | System.Windows.Forms.Keys.R));
            this.barButtonItemReportList.Name = "barButtonItemReportList";
            this.barButtonItemReportList.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemReportList_ItemClick);
            // 
            // barButtonItemHelp
            // 
            this.barButtonItemHelp.Caption = "barButtonItemHelp";
            this.barButtonItemHelp.Glyph = global::SmartCadReports.Properties.ResourcesGui.help2;
            this.barButtonItemHelp.Id = 5;
            this.barButtonItemHelp.Name = "barButtonItemHelp";
            this.barButtonItemHelp.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemHelp_ItemClick);
            // 
            // barButtonItemIncidentArchive
            // 
            this.barButtonItemIncidentArchive.Appearance.Options.UseTextOptions = true;
            this.barButtonItemIncidentArchive.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItemIncidentArchive.AppearanceDisabled.Options.UseTextOptions = true;
            this.barButtonItemIncidentArchive.AppearanceDisabled.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItemIncidentArchive.Caption = "First Level";
            this.barButtonItemIncidentArchive.Id = 6;
            this.barButtonItemIncidentArchive.ItemShortcut = new DevExpress.XtraBars.BarShortcut(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
                | System.Windows.Forms.Keys.I));
            this.barButtonItemIncidentArchive.LargeGlyph = global::SmartCadReports.Properties.ResourcesGui.Historial;
            this.barButtonItemIncidentArchive.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemIncidentArchive.Name = "barButtonItemIncidentArchive";
            this.barButtonItemIncidentArchive.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItemIncidentArchive.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemIncidentArchive_ItemClick);
            // 
            // barButtonItemCCTVArchive
            // 
            this.barButtonItemCCTVArchive.Caption = "CCTV";
            this.barButtonItemCCTVArchive.Id = 7;
            this.barButtonItemCCTVArchive.ItemShortcut = new DevExpress.XtraBars.BarShortcut(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
                | System.Windows.Forms.Keys.T));
            this.barButtonItemCCTVArchive.LargeGlyph = global::SmartCadReports.Properties.ResourcesGui.Historial;
            this.barButtonItemCCTVArchive.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemCCTVArchive.Name = "barButtonItemCCTVArchive";
            this.barButtonItemCCTVArchive.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItemCCTVArchive.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemCCTVArchive_ItemClick);
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupReport,
            this.ribbonPageGroupIncidents,
            this.ribbonPageGroupMaps,
            this.ribbonPageGroupGeneralOptions});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "ribbonPage1";
            // 
            // ribbonPageGroupReport
            // 
            this.ribbonPageGroupReport.AllowMinimize = false;
            this.ribbonPageGroupReport.AllowTextClipping = false;
            this.ribbonPageGroupReport.ItemLinks.Add(this.barButtonItemReportList);
            this.ribbonPageGroupReport.Name = "ribbonPageGroupReport";
            this.ribbonPageGroupReport.ShowCaptionButton = false;
            this.ribbonPageGroupReport.Text = "ribbonPageGroup1";
            // 
            // ribbonPageGroupIncidents
            // 
            this.ribbonPageGroupIncidents.AllowMinimize = false;
            this.ribbonPageGroupIncidents.AllowTextClipping = false;
            this.ribbonPageGroupIncidents.ItemLinks.Add(this.barButtonItemIncidentArchive);
            this.ribbonPageGroupIncidents.ItemLinks.Add(this.barButtonItemCCTVArchive, true);
            this.ribbonPageGroupIncidents.Name = "ribbonPageGroupIncidents";
            this.ribbonPageGroupIncidents.ShowCaptionButton = false;
            this.ribbonPageGroupIncidents.Text = "Incidents Archive";
            // 
            // ribbonPageGroupGeneralOptions
            // 
            this.ribbonPageGroupGeneralOptions.AllowMinimize = false;
            this.ribbonPageGroupGeneralOptions.AllowTextClipping = false;
            this.ribbonPageGroupGeneralOptions.ItemLinks.Add(this.barButtonItemPrint);
            this.ribbonPageGroupGeneralOptions.ItemLinks.Add(this.barButtonItemSave);
            this.ribbonPageGroupGeneralOptions.ItemLinks.Add(this.barButtonItemRefresh);
            this.ribbonPageGroupGeneralOptions.Name = "ribbonPageGroupGeneralOptions";
            this.ribbonPageGroupGeneralOptions.ShowCaptionButton = false;
            this.ribbonPageGroupGeneralOptions.Text = "General Options";
            this.ribbonPageGroupGeneralOptions.Visible = false;
            // 
            // barButtonItemPrint
            // 
            this.barButtonItemPrint.Caption = "ItemPrint";
            this.barButtonItemPrint.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPrint.Glyph")));
            this.barButtonItemPrint.Id = 6;
            this.barButtonItemPrint.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemPrint.Name = "barButtonItemPrint";
            // 
            // barButtonItemSave
            // 
            this.barButtonItemSave.Caption = "ItemSave";
            this.barButtonItemSave.Glyph = global::SmartCadReports.Gui.PrintRibbonControllerResources.RibbonPrintPreview_Save;
            this.barButtonItemSave.Id = 7;
            this.barButtonItemSave.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemSave.Name = "barButtonItemSave";
            // 
            // barButtonItemRefresh
            // 
            this.barButtonItemRefresh.Caption = "ItemRefresh";
            this.barButtonItemRefresh.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemRefresh.Glyph")));
            this.barButtonItemRefresh.Id = 8;
            this.barButtonItemRefresh.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemRefresh.Name = "barButtonItemRefresh";
            // 
            // repositoryItemPictureEdit1
            // 
            this.repositoryItemPictureEdit1.Name = "repositoryItemPictureEdit1";
            // 
            // ribbonStatusBar1
            // 
            this.ribbonStatusBar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ribbonStatusBar1.ItemLinks.Add(this.labelUser);
            this.ribbonStatusBar1.ItemLinks.Add(this.labelConnected);
            this.ribbonStatusBar1.ItemLinks.Add(this.labelDate);
            this.ribbonStatusBar1.Location = new System.Drawing.Point(0, 696);
            this.ribbonStatusBar1.Name = "ribbonStatusBar1";
            this.ribbonStatusBar1.Ribbon = this.ribbonControl;
            this.ribbonStatusBar1.Size = new System.Drawing.Size(852, 23);
            // 
            // labelUser
            // 
            this.labelUser.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUser.Appearance.Options.UseFont = true;
            this.labelUser.Caption = "Usuario";
            this.labelUser.Id = 26;
            this.labelUser.Name = "labelUser";
            this.labelUser.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // labelConnected
            // 
            this.labelConnected.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelConnected.Appearance.Options.UseFont = true;
            this.labelConnected.Caption = "Connected";
            this.labelConnected.Id = 25;
            this.labelConnected.Name = "labelConnected";
            this.labelConnected.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // labelDate
            // 
            this.labelDate.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.labelDate.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDate.Appearance.Options.UseFont = true;
            this.labelDate.Caption = "Date";
            this.labelDate.Id = 27;
            this.labelDate.Name = "labelDate";
            this.labelDate.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // labelLogo
            // 
            this.labelLogo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLogo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.labelLogo.Image = ((System.Drawing.Image)(resources.GetObject("labelLogo.Image")));
            this.labelLogo.Location = new System.Drawing.Point(794, 1);
            this.labelLogo.Name = "labelLogo";
            this.labelLogo.Size = new System.Drawing.Size(57, 22);
            this.labelLogo.TabIndex = 22;
            // 
            // ribbonPageGroupTools
            // 
            this.ribbonPageGroupTools.AllowMinimize = false;
            this.ribbonPageGroupTools.AllowTextClipping = false;
            this.ribbonPageGroupTools.ItemLinks.Add(this.barButtonItemOpenMap);
            this.ribbonPageGroupTools.Name = "ribbonPageGroupTools";
            this.ribbonPageGroupTools.ShowCaptionButton = false;
            this.ribbonPageGroupTools.Text = "ribbonPageGroupTools";
            // 
            // barButtonItemOpenMap
            // 
            this.barButtonItemOpenMap.Caption = "ItemOpenMap";
            this.barButtonItemOpenMap.Id = 4;
            this.barButtonItemOpenMap.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemOpenMap.LargeGlyph")));
            this.barButtonItemOpenMap.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemOpenMap.Name = "barButtonItemOpenMap";
            this.barButtonItemOpenMap.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // xtraTabbedMdiManager1
            // 
            this.xtraTabbedMdiManager1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.xtraTabbedMdiManager1.BorderStylePage = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.xtraTabbedMdiManager1.ClosePageButtonShowMode = DevExpress.XtraTab.ClosePageButtonShowMode.InActiveTabPageHeader;
            this.xtraTabbedMdiManager1.HeaderButtons = ((DevExpress.XtraTab.TabButtons)(((DevExpress.XtraTab.TabButtons.Prev | DevExpress.XtraTab.TabButtons.Next) 
            | DevExpress.XtraTab.TabButtons.Default)));
            this.xtraTabbedMdiManager1.HeaderButtonsShowMode = DevExpress.XtraTab.TabButtonShowMode.WhenNeeded;
            this.xtraTabbedMdiManager1.MdiParent = this;
            this.xtraTabbedMdiManager1.SetNextMdiChildMode = DevExpress.XtraTabbedMdi.SetNextMdiChildMode.TabControl;
            // 
            // ribbonPageGroupMaps
            // 
            this.ribbonPageGroupMaps.AllowMinimize = false;
            this.ribbonPageGroupMaps.AllowTextClipping = false;
            this.ribbonPageGroupMaps.ItemLinks.Add(this.barButtonItemCrimeMap);
            this.ribbonPageGroupMaps.Name = "ribbonPageGroupMaps";
            this.ribbonPageGroupMaps.ShowCaptionButton = false;
            this.ribbonPageGroupMaps.Text = "Map Reports";
            // 
            // barButtonItemCrimeMap
            // 
            this.barButtonItemCrimeMap.Appearance.Options.UseTextOptions = true;
            this.barButtonItemCrimeMap.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItemCrimeMap.Caption = "Crime Map";
            this.barButtonItemCrimeMap.Id = 8;
            this.barButtonItemCrimeMap.ItemShortcut = new DevExpress.XtraBars.BarShortcut(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
                | System.Windows.Forms.Keys.C));
            this.barButtonItemCrimeMap.LargeGlyph = global::SmartCadReports.Properties.ResourcesGui.crimeMapIcon;
            this.barButtonItemCrimeMap.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemCrimeMap.Name = "barButtonItemCrimeMap";
            this.barButtonItemCrimeMap.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItemCrimeMap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemCrimeMap_ItemClick);
            // 
            // ReportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(852, 719);
            this.Controls.Add(this.labelLogo);
            this.Controls.Add(this.ribbonControl);
            this.Controls.Add(this.ribbonStatusBar1);
            this.Controls.Add(this.labelExLogo);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.KeyPreview = true;
            this.Name = "ReportForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ReportForm_FormClosing);
            this.Load += new System.EventHandler(this.ReportForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private CrystalDecisions.CrystalReports.Engine.ReportDocument reportDocumentMain;
        private LabelEx labelExLogo;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupReport;
        private DevExpress.XtraBars.BarButtonItem barButtonItemUSPLogo;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit1;
        private DevExpress.XtraBars.BarButtonItem barButtonItemReportList;
        private DevExpress.XtraBars.BarButtonItem barButtonItemHelp;
        private System.Windows.Forms.Label labelLogo;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar1;
        private DevExpress.XtraBars.BarStaticItem labelUser;
        private DevExpress.XtraBars.BarStaticItem labelConnected;
        private DevExpress.XtraBars.BarStaticItem labelDate;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupGeneralOptions;
        private DevExpress.XtraBars.BarButtonItem barButtonItemPrint;
        private DevExpress.XtraBars.BarButtonItem barButtonItemSave;
        private DevExpress.XtraBars.BarButtonItem barButtonItemRefresh;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupTools;
        private DevExpress.XtraBars.BarButtonItem barButtonItemOpenMap;
        private DevExpress.XtraBars.BarButtonItem barButtonItemIncidentArchive;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupIncidents;
        private DevExpress.XtraTabbedMdi.XtraTabbedMdiManager xtraTabbedMdiManager1;
        private DevExpress.XtraBars.BarButtonItem barButtonItemCCTVArchive;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.BarButtonItem barButtonItemCrimeMap;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupMaps;
    }
}

