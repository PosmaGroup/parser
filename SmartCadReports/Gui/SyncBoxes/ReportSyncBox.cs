using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Threading;
using System.Collections;
using SmartCadControls.Controls;
using SmartCadReports.Gui.Classes;
using SmartCadControls;
using SmartCadCore.ClientData;

namespace SmartCadReports.Gui.SyncBoxes
{
    public class GridReportData : GridControlData
    {
        private ReportContainer report;
        public static DataGridEx DataGrid = null;        
        private Color backColor;

        public ReportContainer Report
        {
            get
            {
                return report;
            }
            set
            {
                report = value;
            }
        }

        public GridReportData(GridReportClientData report)
            : base(report)
        {
            this.report = report.Report;
        }

        [GridControlRowInfoData(Visible = true, Searchable = true)]
        public string Name
        {
            get
            {
                return this.report.Name;
            }
        }

        [GridControlRowInfoData(Visible = true, Searchable = true)]
        public string Description
        {
            get
            {
                return this.report.Description;
            }
        }

        public override bool Equals(object obj)
        {
            if (obj is GridReportData)
            {
                if (Name == ((GridReportData)obj).Name)
                    return true;
            }
            return false;
        }
    }

    public class GridReportClientData : ClientData
    {
        public GridReportClientData(ReportContainer report)
        {
            Report = report;
        }

        public ReportContainer Report { get; set; }
    }
}
