using SmartCadGuiCommon;
namespace SmartCadReports.Gui
{
    partial class DefaultReportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private new System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DefaultReportForm));
            this.reportDocumentMain = new CrystalDecisions.CrystalReports.Engine.ReportDocument();
            this.labelUser = new DevExpress.XtraBars.BarStaticItem();
            this.labelConnected = new DevExpress.XtraBars.BarStaticItem();
            this.labelDate = new DevExpress.XtraBars.BarStaticItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroupCallInfo = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.barButtonItemStartRegIncident = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemCreateNewIncident = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemCancelIncident = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPageGroupApplications = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.barButtonItemChat = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemHistory = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPageGroupGeneralOptions = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.barButtonItemPrint = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemSave = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPageGroupTools = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.barButtonItemOpenMap = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.reportViewer = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.SuspendLayout();
            // 
            // labelUser
            // 
            this.labelUser.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUser.Appearance.Options.UseFont = true;
            this.labelUser.Caption = "Usuario";
            this.labelUser.Id = 26;
            this.labelUser.Name = "labelUser";
            this.labelUser.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // labelConnected
            // 
            this.labelConnected.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelConnected.Appearance.Options.UseFont = true;
            this.labelConnected.Caption = "Connected";
            this.labelConnected.Id = 25;
            this.labelConnected.Name = "labelConnected";
            this.labelConnected.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // labelDate
            // 
            this.labelDate.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.labelDate.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDate.Appearance.Options.UseFont = true;
            this.labelDate.Caption = "Date";
            this.labelDate.Id = 27;
            this.labelDate.Name = "labelDate";
            this.labelDate.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupCallInfo,
            this.ribbonPageGroupApplications,
            this.ribbonPageGroupGeneralOptions,
            this.ribbonPageGroupTools});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "ribbonPage1";
            // 
            // ribbonPageGroupCallInfo
            // 
            this.ribbonPageGroupCallInfo.AllowMinimize = false;
            this.ribbonPageGroupCallInfo.AllowTextClipping = false;
            this.ribbonPageGroupCallInfo.ItemLinks.Add(this.barButtonItemStartRegIncident);
            this.ribbonPageGroupCallInfo.ItemLinks.Add(this.barButtonItemCreateNewIncident, true);
            this.ribbonPageGroupCallInfo.ItemLinks.Add(this.barButtonItemCancelIncident);
            this.ribbonPageGroupCallInfo.Name = "ribbonPageGroupCallInfo";
            this.ribbonPageGroupCallInfo.ShowCaptionButton = false;
            this.ribbonPageGroupCallInfo.Text = "ribbonPageGroupCallInfo";
            // 
            // barButtonItemStartRegIncident
            // 
            this.barButtonItemStartRegIncident.Caption = "ItemStartRegIncident";
            this.barButtonItemStartRegIncident.Enabled = false;
            this.barButtonItemStartRegIncident.Id = 12;
            this.barButtonItemStartRegIncident.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemStartRegIncident.LargeGlyph")));
            this.barButtonItemStartRegIncident.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemStartRegIncident.Name = "barButtonItemStartRegIncident";
            this.barButtonItemStartRegIncident.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonItemCreateNewIncident
            // 
            this.barButtonItemCreateNewIncident.Caption = "ItemCreateNewIncident";
            this.barButtonItemCreateNewIncident.Enabled = false;
            this.barButtonItemCreateNewIncident.Id = 13;
            this.barButtonItemCreateNewIncident.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemCreateNewIncident.LargeGlyph")));
            this.barButtonItemCreateNewIncident.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemCreateNewIncident.Name = "barButtonItemCreateNewIncident";
            this.barButtonItemCreateNewIncident.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonItemCancelIncident
            // 
            this.barButtonItemCancelIncident.Caption = "ItemCancelIncident";
            this.barButtonItemCancelIncident.Enabled = false;
            this.barButtonItemCancelIncident.Id = 15;
            this.barButtonItemCancelIncident.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemCancelIncident.LargeGlyph")));
            this.barButtonItemCancelIncident.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemCancelIncident.Name = "barButtonItemCancelIncident";
            this.barButtonItemCancelIncident.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // ribbonPageGroupApplications
            // 
            this.ribbonPageGroupApplications.AllowMinimize = false;
            this.ribbonPageGroupApplications.AllowTextClipping = false;
            this.ribbonPageGroupApplications.ItemLinks.Add(this.barButtonItemChat);
            this.ribbonPageGroupApplications.ItemLinks.Add(this.barButtonItemHistory);
            this.ribbonPageGroupApplications.Name = "ribbonPageGroupApplications";
            this.ribbonPageGroupApplications.ShowCaptionButton = false;
            this.ribbonPageGroupApplications.Text = "Applications";
            // 
            // barButtonItemChat
            // 
            this.barButtonItemChat.Caption = "ItemChat";
            this.barButtonItemChat.Id = 5;
            this.barButtonItemChat.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemChat.LargeGlyph")));
            this.barButtonItemChat.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemChat.Name = "barButtonItemChat";
            this.barButtonItemChat.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItemChat.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barButtonItemHistory
            // 
            this.barButtonItemHistory.Caption = "History";
            this.barButtonItemHistory.Id = 21;
            this.barButtonItemHistory.LargeGlyph = global::SmartCadReports.Properties.ResourcesGui.Historial;
            this.barButtonItemHistory.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemHistory.Name = "barButtonItemHistory";
            this.barButtonItemHistory.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // ribbonPageGroupGeneralOptions
            // 
            this.ribbonPageGroupGeneralOptions.AllowMinimize = false;
            this.ribbonPageGroupGeneralOptions.AllowTextClipping = false;
            this.ribbonPageGroupGeneralOptions.ItemLinks.Add(this.barButtonItemPrint);
            this.ribbonPageGroupGeneralOptions.ItemLinks.Add(this.barButtonItemSave);
            this.ribbonPageGroupGeneralOptions.ItemLinks.Add(this.barButtonItemRefresh);
            this.ribbonPageGroupGeneralOptions.Name = "ribbonPageGroupGeneralOptions";
            this.ribbonPageGroupGeneralOptions.ShowCaptionButton = false;
            this.ribbonPageGroupGeneralOptions.Text = "ribbonPageGroupGeneralOptions";
            // 
            // barButtonItemPrint
            // 
            this.barButtonItemPrint.Caption = "ItemPrint";
            this.barButtonItemPrint.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemPrint.Glyph")));
            this.barButtonItemPrint.Id = 6;
            this.barButtonItemPrint.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemPrint.Name = "barButtonItemPrint";
            // 
            // barButtonItemSave
            // 
            this.barButtonItemSave.Caption = "ItemSave";
            this.barButtonItemSave.Glyph = global::SmartCadReports.Gui.PrintRibbonControllerResources.RibbonPrintPreview_Save;
            this.barButtonItemSave.Id = 7;
            this.barButtonItemSave.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemSave.Name = "barButtonItemSave";
            // 
            // barButtonItemRefresh
            // 
            this.barButtonItemRefresh.Caption = "ItemRefresh";
            this.barButtonItemRefresh.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemRefresh.Glyph")));
            this.barButtonItemRefresh.Id = 8;
            this.barButtonItemRefresh.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemRefresh.Name = "barButtonItemRefresh";
            // 
            // ribbonPageGroupTools
            // 
            this.ribbonPageGroupTools.AllowMinimize = false;
            this.ribbonPageGroupTools.AllowTextClipping = false;
            this.ribbonPageGroupTools.ItemLinks.Add(this.barButtonItemOpenMap);
            this.ribbonPageGroupTools.Name = "ribbonPageGroupTools";
            this.ribbonPageGroupTools.ShowCaptionButton = false;
            this.ribbonPageGroupTools.Text = "ribbonPageGroupTools";
            // 
            // barButtonItemOpenMap
            // 
            this.barButtonItemOpenMap.Caption = "ItemOpenMap";
            this.barButtonItemOpenMap.Id = 4;
            this.barButtonItemOpenMap.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemOpenMap.LargeGlyph")));
            this.barButtonItemOpenMap.MergeType = DevExpress.XtraBars.BarMenuMerge.Replace;
            this.barButtonItemOpenMap.Name = "barButtonItemOpenMap";
            this.barButtonItemOpenMap.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "barButtonItemHelp";
            this.barButtonItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.Glyph")));
            this.barButtonItem1.Id = 0;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "barButtonItem4";
            this.barButtonItem4.Id = 24;
            this.barButtonItem4.Name = "barButtonItem4";
            this.barButtonItem4.SmallWithoutTextWidth = 70;
            // 
            // reportViewer
            // 
            this.reportViewer.ActiveViewIndex = -1;
            this.reportViewer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.reportViewer.Cursor = System.Windows.Forms.Cursors.Default;
            this.reportViewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.reportViewer.Location = new System.Drawing.Point(0, 0);
            this.reportViewer.Name = "reportViewer";
            this.reportViewer.Size = new System.Drawing.Size(852, 719);
            this.reportViewer.TabIndex = 0;
            // 
            // DefaultReportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(852, 719);
            this.Controls.Add(this.reportViewer);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "DefaultReportForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);

        }

        #endregion

        private CrystalDecisions.CrystalReports.Engine.ReportDocument reportDocumentMain;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer reportViewer;
        private DevExpress.XtraBars.BarStaticItem labelUser;
        private DevExpress.XtraBars.BarStaticItem labelConnected;
        private DevExpress.XtraBars.BarStaticItem labelDate;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupCallInfo;
        public DevExpress.XtraBars.BarButtonItem barButtonItemStartRegIncident;
        public DevExpress.XtraBars.BarButtonItem barButtonItemCreateNewIncident;
        private DevExpress.XtraBars.BarButtonItem barButtonItemCancelIncident;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupApplications;
        private DevExpress.XtraBars.BarButtonItem barButtonItemChat;
        private DevExpress.XtraBars.BarButtonItem barButtonItemHistory;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupGeneralOptions;
        private DevExpress.XtraBars.BarButtonItem barButtonItemPrint;
        private DevExpress.XtraBars.BarButtonItem barButtonItemSave;
        private DevExpress.XtraBars.BarButtonItem barButtonItemRefresh;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupTools;
        private DevExpress.XtraBars.BarButtonItem barButtonItemOpenMap;
        public DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
    }
}

