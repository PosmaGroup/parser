using SmartCadControls;
using SmartCadControls.Controls;

namespace SmartCadReports.Gui
{
    partial class ReportListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private new System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonExCancel = new ButtonEx();
            this.buttonExAccept = new ButtonEx();
            this.groupBoxReports = new System.Windows.Forms.GroupBox();
            this.gridControlExReportList = new GridControlEx();
            this.gridViewExReportList = new GridViewEx();
            this.groupBoxReports.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExReportList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExReportList)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonExCancel
            // 
            this.buttonExCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonExCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.buttonExCancel.FontFormat = FontFormat.GetFormat("ButtonFormat");
            this.buttonExCancel.ForeColor = System.Drawing.Color.Black;
            this.buttonExCancel.Location = new System.Drawing.Point(412, 208);
            this.buttonExCancel.Name = "buttonExCancel";
            this.buttonExCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonExCancel.TabIndex = 0;
            this.buttonExCancel.Text = "Cancelar";
            this.buttonExCancel.UseVisualStyleBackColor = true;
            // 
            // buttonExAccept
            // 
            this.buttonExAccept.Enabled = false;
            this.buttonExAccept.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.buttonExAccept.FontFormat = FontFormat.GetFormat("ButtonFormat");
            this.buttonExAccept.ForeColor = System.Drawing.Color.Black;
            this.buttonExAccept.Location = new System.Drawing.Point(328, 208);
            this.buttonExAccept.Name = "buttonExAccept";
            this.buttonExAccept.Size = new System.Drawing.Size(75, 23);
            this.buttonExAccept.TabIndex = 1;
            this.buttonExAccept.Text = "Accept";
            this.buttonExAccept.UseVisualStyleBackColor = true;
            this.buttonExAccept.Click += new System.EventHandler(this.buttonExAccept_Click);
            // 
            // groupBoxReports
            // 
            this.groupBoxReports.Controls.Add(this.gridControlExReportList);
            this.groupBoxReports.Location = new System.Drawing.Point(3, 5);
            this.groupBoxReports.Name = "groupBoxReports";
            this.groupBoxReports.Size = new System.Drawing.Size(484, 197);
            this.groupBoxReports.TabIndex = 2;
            this.groupBoxReports.TabStop = false;
            this.groupBoxReports.Text = "Reportes";
            // 
            // gridControlExReportList
            // 
            this.gridControlExReportList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlExReportList.EnableAutoFilter = false;
            this.gridControlExReportList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridControlExReportList.Location = new System.Drawing.Point(3, 16);
            this.gridControlExReportList.MainView = this.gridViewExReportList;
            this.gridControlExReportList.Name = "gridControlExReportList";
            this.gridControlExReportList.Size = new System.Drawing.Size(478, 178);
            this.gridControlExReportList.TabIndex = 1;
            this.gridControlExReportList.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewExReportList});
            this.gridControlExReportList.ViewTotalRows = false;
            // 
            // gridViewExReportList
            // 
            this.gridViewExReportList.AllowFocusedRowChanged = true;
            this.gridViewExReportList.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(179)))), ((int)(((byte)(225)))));
            this.gridViewExReportList.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExReportList.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridViewExReportList.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridViewExReportList.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightGray;
            this.gridViewExReportList.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gridViewExReportList.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridViewExReportList.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridViewExReportList.EnablePreviewLineForFocusedRow = false;
            this.gridViewExReportList.GridControl = this.gridControlExReportList;
            this.gridViewExReportList.Name = "gridViewExReportList";
            this.gridViewExReportList.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewExReportList.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewExReportList.ViewTotalRows = false;
            this.gridViewExReportList.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewExReportList_FocusedRowChanged);
            this.gridViewExReportList.DoubleClick += new System.EventHandler(this.gridViewExReportList_DoubleClick);
            // 
            // ReportListForm
            // 
            this.AcceptButton = this.buttonExAccept;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonExCancel;
            this.ClientSize = new System.Drawing.Size(495, 237);
            this.Controls.Add(this.groupBoxReports);
            this.Controls.Add(this.buttonExAccept);
            this.Controls.Add(this.buttonExCancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ReportListForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Listado de reportes";
            this.Load += new System.EventHandler(this.ReportListForm_Load);
            this.groupBoxReports.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlExReportList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewExReportList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ButtonEx buttonExCancel;
        private ButtonEx buttonExAccept;
        private System.Windows.Forms.GroupBox groupBoxReports;
        private System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle;
        private GridControlEx gridControlExReportList;
        private GridViewEx gridViewExReportList;
        

        

    }
}