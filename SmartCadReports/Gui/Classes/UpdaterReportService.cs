using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Smartmatic.SmartCad.Service;
using System.Threading;
using System.Xml;
using System.Windows.Forms;
using SmartCadGuiCommon;
using SmartCadCore.Common;
using SmartCadCore.Core;

namespace SmartCadReports.Gui.Classes
{
    /// <summary>
    /// Clase que se comunica con el servidor para 
    /// obtener el xml de los reportes.
    /// Asi como para obtener los reportes.
    /// Actualiza los reportes al iniciar la aplicacion.
    /// </summary>
    public  class UpdaterReportService
    {

        #region Atributes
        
        
        private  Thread waitThread;
        private  WaitForm waitForm;

        #endregion

        #region Constructors
        
        /// <summary>
        /// Constructor por defecto.
        /// </summary>
        public UpdaterReportService()
        {
        }

        #endregion

        #region UpdateOperations

        public void UpdateReports()
        {
            this.ShowWaitDialog(ResourceLoader.GetString2("UpdatingReports"));

            FileServerServiceClient fileServerServiceClient = FileServerServiceClient.CreateInstance();

            Stream stream = fileServerServiceClient.GetFile("Reports/serverfiles.xml");

            SaveStreamToFile(stream, Path.Combine(SmartCadConfiguration.DistFolder, "Reports/newclientfiles.xml"));

            CompareAndUpdateFiles(fileServerServiceClient,
                Path.Combine(SmartCadConfiguration.DistFolder, "Reports/clientfiles.xml"),
                Path.Combine(SmartCadConfiguration.DistFolder, "Reports/newclientfiles.xml"));

            this.CloseWaitDialog();
        }

        #endregion

        #region StreamOperations

        private static void SaveStreamToFile(Stream stream, string filePath)
        {
            FileStream outstream = File.Open(filePath, FileMode.Create, FileAccess.Write);

            CopyStream(stream, outstream);

            outstream.Close();

            stream.Close();
        }

        private static void CopyStream(Stream instream, Stream outstream)
        {
            const int bufferLen = 1024 * 1024;

            byte[] buffer = new byte[bufferLen];

            int count = 0;

            while ((count = instream.Read(buffer, 0, bufferLen)) > 0)
            {
                outstream.Write(buffer, 0, count);
                outstream.Flush();
            }
        }

        /// <summary>
        /// Retorna un diccionario de la forma
        /// Dic["Archivo"] = version
        /// </summary>
        /// <param name="file">Archivo xml que tiene la informacion de los archivos</param>
        /// <returns></returns>
        private Dictionary<string, int> GetFilesVersions(string file)
        {
            Dictionary<string, int> files = new Dictionary<string, int>();
            XmlDocument doc = doc = new XmlDocument();
            
            doc.Load(file);
            XmlElement root;
            root = doc.DocumentElement;

            foreach (XmlNode node in root)
            {
                if (node.Name == "file")
                {
                    string name = node.Attributes["name"].Value;
                    int version = int.Parse(node.Attributes["version"].Value);

                    files.Add(name, version);
                }
            }
            return files;
        }

        private void CompareAndUpdateFiles(FileServerServiceClient fileServerServiceClient, string oldXml, string newXml)
        {

            Dictionary<string, int> oldFiles = GetFilesVersions(oldXml);
            Dictionary<string, int> newFiles = GetFilesVersions(newXml);

            foreach (string name in newFiles.Keys)
            {
                if ((oldFiles.ContainsKey(name) == false) || 
                    (oldFiles[name] < newFiles[name]) ||
                    (File.Exists(Path.Combine(SmartCadConfiguration.DistFolder, "Reports/" + name)) == false))
                {
                    Stream stream = fileServerServiceClient.GetFile("Reports/" + name);

                    if (stream != null)
                    {
#if DEBUG
                        SaveStreamToFile(stream, Path.Combine(SmartCadConfiguration.DistFolder, "Reports/dev" + name));
#else
                        SaveStreamToFile(stream, Path.Combine(SmartCadConfiguration.DistFolder, "Reports/" + name));
#endif
                    }
                }
            }

            File.Delete(Path.Combine(SmartCadConfiguration.DistFolder, "Reports/clientfiles.xml"));

            File.Move(
                Path.Combine(SmartCadConfiguration.DistFolder, "Reports/newclientfiles.xml"),
                Path.Combine(SmartCadConfiguration.DistFolder, "Reports/clientfiles.xml"));

        }


        #endregion

        #region Dialogs Call

        private void ShowWaitDialog(string waitMessage)
        {
            if (this.waitThread == null)
            {
                ParameterizedThreadStart threadStart = new ParameterizedThreadStart(this.RaiseWaitMessage);
                this.waitThread = new Thread(threadStart);
                this.waitThread.Start(waitMessage);
            }
        }

        public void CloseWaitDialog()
        {
            try
            {
                if (this.waitThread != null)
                {
                    this.waitThread.Abort();
                    this.waitThread = null;
                }
            }
            catch
            {
                // do something with ex
                //throw ex;
                this.waitThread = null;
            }
        }

        private void RaiseWaitMessage(object message)
        {
            this.waitForm = new WaitForm();
            this.waitForm.Message = message as string;
            Application.Run(this.waitForm);
        }

        #endregion

    }
}
