using System;
using System.Collections.Generic;
using System.Text;

namespace SmartCadReports.Gui.Classes
{
    public class ReportContainer
    {
        #region Propierties

        private string m_file = String.Empty;
        /// <summary>
        /// String que contiene la ruta del archivo
        /// </summary>
        public string File
        {
            get { return m_file; }
            set { m_file = value; }
        }

        private string m_name = String.Empty;
        /// <summary>
        /// Propiedad contenedora del nombre del reporte
        /// </summary>
        public string Name
        {
            get { return m_name; }
            set { m_name = value; }
        }

        private string m_desc = String.Empty;

        /// <summary>
        /// Propiedad contenedora de la descripcion.
        /// </summary>
        public string Description
        {
            get { return m_desc; }
            set { m_desc = value; }
        }

        private string m_version;
        /// <summary>
        /// Propiedad contenedora de la version.
        /// </summary>
        public string Version
        {
            get { return m_version; }
            set { m_version = value; }
        }

        #endregion

        #region Constructors

        public ReportContainer()
        {

        }
        /// <summary>
        /// Constructor semi basico
        /// </summary>
        /// <param name="file">Nombre del Archivo</param>
        /// <param name="name">Nombre del reporte</param>
        /// <param name="desc">Descripcion del reporte</param>
        public ReportContainer(string file, string name, string desc)
        {
            this.File = file;
            this.Name = name;
            this.Description = desc;
        }


        /// <summary>
        /// Constructor completo, con todos los campos.
        /// </summary>
        /// <param name="file">Nombre del Archivo</param>
        /// <param name="name">Nombre del reporte</param>
        /// <param name="desc">Descripcion del reporte</param>
        /// <param name="version">Version del reporte</param>
        public ReportContainer(string file, string name, string desc,string version)
        {
            this.File = file;
            this.Name = name;
            this.Description = desc;
            this.Version = version;
        }

        #endregion

        #region Overrides
        
        /// <summary>
        /// Sobre Escritura del metodo to string.
        /// </summary>
        /// <returns>Una representacion en string de un ReportContainer</returns>
        public override string ToString()
        {
            return this.Name;
        }

        #endregion

    }
}
