using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.ReportSource;
using CrystalDecisions.Shared;
using CrystalDecisions.Windows.Forms;
using System.Reflection;

using Smartmatic.SmartCad.Service;
using System.Net;
using System.IO;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraTab;
using DevExpress.XtraTab.ViewInfo;
using SmartCadCore.Common;

namespace SmartCadReports.Gui
{
    public partial class DefaultReportForm : DevExpress.XtraEditors.XtraForm
    {
        #region Constructors

        public DefaultReportForm()
        {
            InitializeComponent();
        }

        public DefaultReportForm(ReportDocument document)
            :this()
        {
            reportDocumentMain = document;
            reportViewer.ReportSource = reportDocumentMain;
        }

        #endregion      
        
        private void xtraTabControl_CloseButtonClick(object sender, EventArgs e)
        {
            reportViewer.Dispose();
        }

        void crystalReportViewerMain_ClickPage(object sender, PageMouseEventArgs e)
        {
            ShowReportControls(true);
        }

        void crystalReportViewerMain_ReportRefresh(object source, ViewerEventArgs e)
        {
            UpdateReportTreeView();

        }

        CrystalDecisions.CrystalReports.ViewerObjectModel.TotallerTree groupTree =
              new CrystalDecisions.CrystalReports.ViewerObjectModel.TotallerTree();
        private void UpdateReportTreeView()
        {
            PageView pageView = null;
            ReportDocumentManager reportManager = new ReportDocumentManager(new IntPtr());
            ReportDocuments reportDocuments = new ReportDocuments();
            foreach (Control c in reportViewer.Controls)
            {

                if (c is PageView)
                {
                    pageView = (PageView)c;

                    FieldInfo fireportmanager = pageView.GetType().GetField("docManager", BindingFlags.NonPublic | BindingFlags.Instance);
                    reportManager = (ReportDocumentManager)fireportmanager.GetValue(pageView);
                    if (reportManager != null)
                    {
                        FieldInfo fireports = reportManager.GetType().GetField("reports", BindingFlags.NonPublic | BindingFlags.Instance);
                        reportDocuments = (ReportDocuments)fireports.GetValue(reportManager);

                        if (reportDocuments[0].GroupTree != null)
                        {
                            groupTree = reportDocuments[0].GroupTree;
                        }
                    }
                }
            }
        }

        private void ShowReportControls(bool visible)
        {
            TotallerTreeView totallerTreeView = new TotallerTreeView();
            foreach (Control c in reportViewer.Controls)
            {
                if (c is ToolStrip)
                {
                    ToolStrip strip = (ToolStrip)c;
                    foreach (ToolStripItem item in strip.Items)
                    {
                        if (item is ToolStripButton)
                        {
                            item.Enabled = visible;
                        }
                    }
                }

                else if (c is ReportGroupTree)
                {

                    ReportGroupTree reportGroupTree = (ReportGroupTree)c;
                    FieldInfo fi = reportGroupTree.GetType().GetField("treeView", BindingFlags.NonPublic | BindingFlags.Instance);
                    totallerTreeView = (TotallerTreeView)fi.GetValue(reportGroupTree);
                    totallerTreeView.Update(groupTree);

                }

            }

        }

        private void FixToolTips()
        {
            //TODO: El codigo que sigue es para
            //Colocarles los tooltips a los botones especificos
            //que indicaba el departamento de QA.
            //Se puede presindir del codigo, debido a que
            //es una solucion temporal para el problema de los tooltips.
            //by Gustavo Fandino, 24/08/07.
            foreach (Control c in reportViewer.Controls)
            {
                if (c is ToolStrip)
                {
                    ToolStrip strip = (ToolStrip)c;
                    foreach (ToolStripItem item in strip.Items)
                    {
                        if (item is ToolStripButton)
                        {
                            if (item.AccessibleName == ResourceLoader.GetString2("ExportInform"))
                            {
                                item.ToolTipText = ResourceLoader.GetString2("ExportReport");
                            }
                            if (item.AccessibleName == ResourceLoader.GetString2("PrintInform"))
                            {
                                item.ToolTipText = ResourceLoader.GetString2("PrintReport");
                            }
                        }
                    }
                }
            }
        }
        
    }
}