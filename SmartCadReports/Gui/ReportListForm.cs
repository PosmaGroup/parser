using SmartCadCore.Common;
using SmartCadCore.Core;
using SmartCadReports.Gui.Classes;
using SmartCadReports.Gui.SyncBoxes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace SmartCadReports.Gui
{
    public partial class ReportListForm : DevExpress.XtraEditors.XtraForm
    {
        private ReportContainer reportFile;

        #region constructors
        
        public ReportListForm()
        {
            InitializeComponent();
        }

        #endregion

        #region Properties
        
        public ReportContainer ReportFile
        {
            get
            {
                return reportFile;
            }

            set
            {
                reportFile = value;
            }
        }

        #endregion

        #region Form Delegate calls

        private void buttonExAccept_Click(object sender, EventArgs e)
        {
            if (this.gridControlExReportList.SelectedItems[0] != null)
            {
                GridReportData gridReport = (GridReportData)gridControlExReportList.SelectedItems[0];
                this.ReportFile = (ReportContainer)gridReport.Report;
                this.DialogResult = DialogResult.OK;
            }
        }

        private void ReportListForm_Load(object sender, EventArgs e)
        {
            gridControlExReportList.ViewTotalRows = true;
            gridControlExReportList.EnableAutoFilter = true;
            gridControlExReportList.Type = typeof(GridReportData);
            LoadLanguage();
            loadReportsXml();
        }

        private void LoadLanguage()
        {
            this.Text = ResourceLoader.GetString2(this.Name);
            this.groupBoxReports.Text = ResourceLoader.GetString2("Reports");
            this.buttonExAccept.Text = ResourceLoader.GetString2("Accept");
            this.buttonExCancel.Text = ResourceLoader.GetString2("Cancel");
        }

        #endregion

        #region Control Calls

        private void loadReportsXml()
        {
            List<ReportContainer> list = getReports(SmartCadConfiguration.ReportXMLFilename);

            //cargo en el dataGrid los reportContainer
            for (int i = 0; i < list.Count; i++)
            {
                ReportContainer report = new ReportContainer(list[i].File,list[i].Name,list[i].Description);
                GridReportData gridReport = new GridReportData(new GridReportClientData(report));
                this.gridControlExReportList.AddOrUpdateItem(gridReport);
            }
            gridViewExReportList.BestFitColumns();
        }

        private List<ReportContainer> getReports(string fileName)
        {
            List<ReportContainer> retval = new List<ReportContainer>();
            try
            {
                XmlTextReader reader = new XmlTextReader(fileName);
                while (reader.Read())
                {
                    switch (reader.NodeType)
                    {
                        case XmlNodeType.Element:
                            if (reader.LocalName == "report")
                            {
                                ReportContainer aux = new ReportContainer();
                                aux.Name = reader.GetAttribute("name");
                                aux.File = reader.GetAttribute("file");
                                aux.Description = reader.GetAttribute("description");
                                retval.Add(aux);
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                SmartLogger.Print(ResourceLoader.GetString2("CantConfigurationLoadFile") + ex.ToString());
            }

            return retval;
        }
        
        #endregion

        private void gridViewExReportList_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (this.gridControlExReportList.SelectedItems.Count > 0)
            {
                this.buttonExAccept.Enabled = true;
            }
            else
            {
                this.buttonExAccept.Enabled = false;
            }
        }

        private void gridViewExReportList_DoubleClick(object sender, EventArgs e)
        {
            if (this.gridControlExReportList.SelectedItems.Count > 0)
            {
                this.ReportFile = ((GridReportData)this.gridControlExReportList.SelectedItems[0]).Report;
                this.DialogResult = DialogResult.OK;
            }
        }
    }
}