using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.ReportSource;
using CrystalDecisions.Shared;
using CrystalDecisions.Windows.Forms;
using System.Reflection;

using Smartmatic.SmartCad.Service;
using System.Net;
using System.IO;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraTab;
using DevExpress.XtraTab.ViewInfo;
using SmartCadCore.Core;
using SmartCadCore.Common;
using SmartCadGuiCommon.Util;
using DevExpress.Utils.About;
using SmartCadGuiCommon;
using SmartCadReports.Gui.Classes;
using SmartCadFirstLevel.Gui;
using DevExpress.XtraBars;

namespace SmartCadReports.Gui
{
    public partial class ReportForm : DevExpress.XtraEditors.XtraForm
    {
        #region Fields

        private ServerServiceClient serverServiceClient;
        private System.Threading.Timer timerCheckServer;

        private NetworkCredential networkCredential;
        private DateTime syncServerDateTime;

        private const int GLOBAL_TIMER_PERIOD = 10000;
        private SkinEngine skinEngine;
        private string labelUserCaption = "";

        #endregion

        #region Properties

        public ServerServiceClient ServerServiceClient
        {
            get
            {
                return serverServiceClient;
            }
            set
            {
                serverServiceClient = value;
            }
        }

        public NetworkCredential NetworkCredential
        {
            get
            {
                return networkCredential;
            }

            set
            {
                networkCredential = value;
            }
        }

        private IncidentsHistoryForm incidentsHistoryForm;
        private CctvIncidentsHistoryForm cctvIncidentsHistoryForm;
        #endregion

        #region Constructors

        public ReportForm()
        {
            var applications = ServerServiceClient.GetInstance().GetActiveApplications();

            InitializeComponent();
            if (!applications.Contains(SmartCadApplications.SmartCadMap))
            {
                ribbonPageGroupMaps.Visible = false;
            }
            if (!applications.Contains(SmartCadApplications.SmartCadFirstLevel) && !applications.Contains(SmartCadApplications.SmartCadCCTV)) 
            {
                ribbonPageGroupIncidents.Visible = false;
            }
            if (!applications.Contains(SmartCadApplications.SmartCadFirstLevel)) 
            {
                barButtonItemIncidentArchive.Visibility = BarItemVisibility.Never;
            }
            if (!applications.Contains(SmartCadApplications.SmartCadCCTV))
            {
                barButtonItemCCTVArchive.Visibility = BarItemVisibility.Never;
            }
        }

        #endregion

        #region Form Delegate calls

        private void ReportForm_Load(object sender, EventArgs e)
        {
            SetSkin();


            //FormUtil.InvokeRequired(this, delegate
            //{
            //    UpdateReports();
            //});

            //this.toolStripButtonExit.Image = ResourceLoader.GetImage("$Image.ReporteExit");
            //this.toolStripButtonHelp.Image = ResourceLoader.GetImage("$Image.ReporteHelp");
            

            //Ver comentario en el metodo.
            //FixToolTips();
            LoadLanguage();
            FillStatusSTrip();
            this.barButtonItemReportList.LargeGlyph = ResourceLoader.GetImage("ReportListImage");
        }

        private void LoadLanguage()
        {
            Text = ResourceLoader.GetString2(Name);
            barButtonItemReportList.Caption = ResourceLoader.GetString2("ReportListForm");
            ribbonPage1.Text = ResourceLoader.GetString2("Reports");
            ribbonPageGroupReport.Text = ResourceLoader.GetString2("Reports");
            barButtonItemPrint.Caption = ResourceLoader.GetString2("Print");
            barButtonItemRefresh.Caption = ResourceLoader.GetString2("Refresh");
            barButtonItemSave.Caption = ResourceLoader.GetString2("Save");

            labelUserCaption = ResourceLoader.GetString2("User") + ": ";
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SetSkin()
        {
            try
            {
                string skinFile = SmartCadConfiguration.SkinsFolder + "/Clasic.xml";

                skinEngine = SkinEngine.Load(skinFile);

                ProfessionalColorTable colorTable = new CustomToolStripColors(
                    skinEngine.Colors["ToolStripGradientBegin"],
                    skinEngine.Colors["ToolStripGradientMiddle"],
                    skinEngine.Colors["ToolStripGradientEnd"],

                    skinEngine.Colors["MenuStripGradientBegin"],
                    skinEngine.Colors["MenuStripGradientEnd"],

                    skinEngine.Colors["StatusStripGradientBegin"],
                    skinEngine.Colors["StatusStripGradientEnd"]
                );

                skinEngine.AddElement("background", this);
                //skinEngine.AddElement("background", this.crystalReportViewerMain);

                skinEngine.ApplyChanges();
            }
            catch (Exception ex)
            {
                MessageForm.Show(ResourceLoader.GetString2("Error"), ResourceLoader.GetString2("ErrorLoadingSkin"), ex);
            }
        }

        #endregion

        #region Control Calls

        /// <summary>
        /// Metodo que se encarga de cargar un reporte en la base de datos
        /// </summary>
        /// <param name="fileName"></param>
        private void reportLoad(ReportContainer report)
        {
            if (String.IsNullOrEmpty(report.File))
            {
                MessageForm.Show(ResourceLoader.GetString2("CantLoadFile"), MessageFormType.Error);
            }
            else
            {
                //Realizo la carga normal del archivo
                try
                {
                    //ConfigurationClientData configuration = serverServiceClient.GetConfiguration();

                    //        bool found = false;
                    //        foreach (XtraTabPage page in xtraTabControl.TabPages)
                    //        {
                    //            if (page.Text == report.Name)
                    //            {
                    //                xtraTabControl.SelectedTabPage = page;
                    //                found = true;
                    //                break;
                    //            }
                    //        }

                    //        if (found == false)
                    //        {
                    //            //this.reportDocumentMain.Refresh();
                    //            FormUtil.InvokeRequired(this, delegate
                    //            {
                    //                XtraTabPage tab = new XtraTabPage();
                    //                tab.Text = report.Name;

                    //                ReportDocument reportDocument = new ReportDocument();
                    //                reportDocument.Load(Path.Combine(SmartCadConfiguration.ReportsFolder, report.File));
                    //                SetConnectionIfo(reportDocument, configuration);
                    //                reportDocument.Refresh();
                    //                CrystalReportViewer viewer = new CrystalReportViewer();
                    //                viewer.ShowParameterPanelButton = false;
                    //                viewer.ShowCopyButton = false;
                    //                viewer.Dock = DockStyle.Fill;
                    //                viewer.ShowCloseButton = false;
                    //                viewer.ReportSource = reportDocument;
                    //                viewer.ReportRefresh += new CrystalDecisions.Windows.Forms.RefreshEventHandler(crystalReportViewerMain_ReportRefresh);
                    //                viewer.ClickPage += new PageMouseEventHandler(crystalReportViewerMain_ClickPage);

                    //                tab.Controls.AddRange(new Control[] { viewer });
                    //                xtraTabControl.TabPages.Add(tab);
                    //            });
                    //        }

                    ReportDocument reportDocument = new ReportDocument();
                    reportDocument.Load(Path.Combine(SmartCadConfiguration.ReportsFolder, report.File));
                    SetConnectionIfo(reportDocument);
                    reportDocument.Refresh();

                    DefaultReportForm defaultReportForm = new DefaultReportForm(reportDocument);
                    defaultReportForm.ControlBox = true;
                    defaultReportForm.MdiParent = this;
                    defaultReportForm.Text = report.Name;
                    defaultReportForm.Activate();
                    defaultReportForm.Show();
                }
                catch (Exception ex)
                {
                    MessageForm.Show(ResourceLoader.GetString2("CantLoadFile"), ex);
                }
            }
        }



        /// <summary>
        /// Este Metodo permite cambiar la base de datos
        /// a la cual se conectara el reporte
        /// </summary>
        /// <param name="ReportDoc">ReportDocument al cual se le quiere cambiar la base de datos</param>
        /// <param name="server">Nombre del servidor</param>
        /// <param name="db">Nombre de la base de datos</param>
        /// <param name="usr">Usuario de la base de datos</param>
        /// <param name="pass">Password el usuario</param>
        private void conectarNewDB(ReportDocument ReportDoc, string server, string db)
        {
            ConnectionInfo crConnectionInfo = new ConnectionInfo();
            crConnectionInfo.ServerName = server;
            crConnectionInfo.DatabaseName = db;
            crConnectionInfo.IntegratedSecurity = false;

            Tables crTables = ReportDoc.Database.Tables;
            //Recorremos todas las tablas para
            //indicarles que ha cambiado el repositorio.
            for (int j = 0; j < crTables.Count; j++)
            {
                Table crTable = crTables[j];
                TableLogOnInfo crTableLogOnInfo = crTable.LogOnInfo;

                crTableLogOnInfo.ConnectionInfo = crConnectionInfo;
                crTable.ApplyLogOnInfo(crTableLogOnInfo);
            }
        }

        private void SetConnectionIfo(ReportDocument ReportDoc)
        {
            ConnectionInfo crConnectionInfo = new ConnectionInfo();
            crConnectionInfo.ServerName = SmartCadConfiguration.SmartCadSection.ServerElement.ReportsElement.DatabaseServer;
            crConnectionInfo.DatabaseName = SmartCadConfiguration.SmartCadSection.ServerElement.ReportsElement.DatabaseName;
            
            Tables crTables = ReportDoc.Database.Tables;
            for (int j = 0; j < crTables.Count; j++)
            {
                Table crTable = crTables[j];
                TableLogOnInfo crTableLogOnInfo = crTable.LogOnInfo;
                crTableLogOnInfo.ConnectionInfo = crConnectionInfo;
                crTable.ApplyLogOnInfo(crTableLogOnInfo);
            }
        }


        /// <summary>
        /// Metodo encargado de sincronizar los reportes con el servidor      
        /// </summary>
        public void UpdateReports()
        {
            UpdaterReportService updater = new UpdaterReportService();

            try
            {
                updater.UpdateReports();
            }
            catch (Exception ex)
            {
                try
                {
                    updater.CloseWaitDialog();
                }
                catch
                {
                }

                MessageForm.Show(ResourceLoader.GetString2("GenericError"), ex);
            }
        }

        private bool EnsureLogout()
        {
            DialogResult dialogResult = MessageForm.Show(ResourceLoader.GetString2("$Message.ExitApplication"), MessageFormType.Question);

            return dialogResult == DialogResult.Yes;
        }

        #endregion


        private void FillStatusSTrip()
        {
            labelConnected.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.labelUser.Caption = labelUserCaption + this.serverServiceClient.OperatorClient.FirstName + " " + this.serverServiceClient.OperatorClient.LastName;
            this.labelDate.Caption = ApplicationUtil.GetFormattedDate(ServerServiceClient.GetInstance().GetTime());
            syncServerDateTime = ServerServiceClient.GetInstance().GetTime();
            timerCheckServer = ApplicationUtil.RecurrentExecuteMethod(
                new SimpleDelegate(
                delegate
                {
                    timer_Tick(null, null);
                }), null, null, GLOBAL_TIMER_PERIOD, GLOBAL_TIMER_PERIOD);

        }

        private void ReportForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (EnsureLogout())
            {
                if (this.timerCheckServer != null)
                {
                    this.timerCheckServer.Dispose();
                }
                this.serverServiceClient.CloseSession();
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void toolStripMenuItemAbout_Click(object sender, EventArgs e)
        {
            SmartCadGuiCommon.AboutForm aboutForm = new SmartCadGuiCommon.AboutForm();
            aboutForm.ShowDialog();
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);

            if (e.KeyCode == Keys.F1)
            {
                toolStripButtonOnlineHelp_Click(new object(), EventArgs.Empty);
            }

        }

        private void toolStripButtonOnlineHelp_Click(object sender, EventArgs e)
        {
            try
            {
                ApplicationUtil.LaunchHelp(SmartCadConfiguration.DistFolder + "/Help/REPORTS Module.chm");
            }
            catch (Exception ex)
            {
                MessageForm.Show(ResourceLoader.GetString2("$Message.HelpNotInstalled"), ex);
            }
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            syncServerDateTime = syncServerDateTime.AddMilliseconds((double)GLOBAL_TIMER_PERIOD);
            string date = ApplicationUtil.GetFormattedDate(syncServerDateTime.AddMilliseconds((double)GLOBAL_TIMER_PERIOD));
            //this.toolStripStatusLabelDate.Text = date;

            FormUtil.InvokeRequired(this, delegate
            {
                this.labelDate.Caption = date;
            });
        }

        private void barButtonItemReportList_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ReportListForm reportListForm = new ReportListForm();

            if (reportListForm.ShowDialog(this) == DialogResult.OK)
            {
                //Cargo el reporte
                //BackgroundProcessForm processForm = new BackgroundProcessForm(this, new MethodInfo[] {
                //    this.GetType().GetMethod("reportLoad", BindingFlags.Instance | BindingFlags.NonPublic) },
                //    new object[][] { new object[] { reportListForm.ReportFile } });
                //processForm.CanThrowError = true;
                //processForm.ShowDialog();
                reportLoad(reportListForm.ReportFile);
            }
        }

        private void xtraTabControl_CloseButtonClick(object sender, EventArgs e)
        {
            //CrystalReportViewer viewer = xtraTabControl.SelectedTabPage.Controls[0] as CrystalReportViewer;
            //viewer.Dispose();

            //ClosePageButtonEventArgs arg = e as ClosePageButtonEventArgs;
            //xtraTabControl.TabPages.Remove(xtraTabControl.SelectedTabPage);
        }

        private void barButtonItemHelp_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                ApplicationUtil.LaunchHelp(SmartCadConfiguration.DistFolder + "/Help/REPORTS Module.chm");
            }
            catch (Exception ex)
            {
                MessageForm.Show(ResourceLoader.GetString2("$Message.HelpNotInstalled"), ex);
            }
        }

        private void barButtonItemIncidentArchive_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (incidentsHistoryForm == null || incidentsHistoryForm.IsDisposed == true)
            {
                incidentsHistoryForm = new IncidentsHistoryForm();
            }
            incidentsHistoryForm.ControlBox = true;
            incidentsHistoryForm.MdiParent = this;
            incidentsHistoryForm.Activate();
            incidentsHistoryForm.Show();

            incidentsHistoryForm.Text = "FirstLevel Archive";
        }

        private void barButtonItemCCTVArchive_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (cctvIncidentsHistoryForm == null || cctvIncidentsHistoryForm.IsDisposed == true)
            {
                cctvIncidentsHistoryForm = new CctvIncidentsHistoryForm();
            }

            cctvIncidentsHistoryForm.ControlBox = true;
            cctvIncidentsHistoryForm.MdiParent = this;
            cctvIncidentsHistoryForm.Activate();
            cctvIncidentsHistoryForm.ribbonControl1.Visible = false;
            cctvIncidentsHistoryForm.Show();

            cctvIncidentsHistoryForm.Text = "CCTV Archive";

        }

        CrimeMapForm crimeMapForm;
        private void barButtonItemCrimeMap_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (crimeMapForm == null || crimeMapForm.IsDisposed == true)
            {
                crimeMapForm = new CrimeMapForm(false);
            }
            crimeMapForm.MdiParent = this;
            crimeMapForm.Activate();
            crimeMapForm.Show();
        }
    }
}