using System;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading;

using System.Net;
using System.Globalization;
using SmartCadGuiCommon.Util;
using SmartCadCore.Core;
using SmartCadCore.Common;
using Smartmatic.SmartCad.Service;
using SmartCadCore.ClientData;
using SmartCadGuiCommon;
using SmartCadReports.Gui;
using System.ServiceModel;
using System.IO;

namespace SmartCadReport
{
	
    static class Program
    {
        private static string ApplicationName = "Reports";

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.ThreadException += ApplicationGuiUtil.Application_ThreadException;
                ApplicationUtil.ActivateProcess(Process.GetCurrentProcess());

                SmartCadConfiguration.Load();
                ResourceLoader.UpdatedLanguage(new CultureInfo(SmartCadConfiguration.SmartCadSection.CultureElement.CultureLanguageElement.Name
                    + "-" + SmartCadConfiguration.SmartCadSection.CultureElement.CultureCountryElement.Name));

                object result = ServerServiceClient.GetServerService();

                if (result == null)
                {
                    MessageForm.Show(ResourceLoader.GetString2("UnableConnectToServer"), MessageFormType.Error);
                    return;
                }

                ConfigurationClientData cfg = ServerServiceClient.GetInstance().GetConfiguration();
                SmartCadConfiguration.SaveConfigurationInClient(cfg);
                if (!ApplicationUtil.VerifyLanguage(cfg))
                {
                    byte[] dll = ServerServiceClient.GetInstance().CheckLanguages(cfg.Language);
                    DirectoryInfo directory = Directory.CreateDirectory(SmartCadConfiguration.LanguageDirectoryClient + "\\" + cfg.Language);
                    using (FileStream fileDll = new FileStream(directory.FullName + "\\Smartmatic.SmartCad.Resources.resources.dll", FileMode.Create, FileAccess.Write))
                    {
                        fileDll.Write(dll, 0, dll.Length);
                    }
                }

                ResourceLoader.UpdatedLanguage(new CultureInfo(cfg.Language));

                ServerServiceClient.GetInstance().SetApplication(UserApplicationClientData.UserApplications.Report.ToString());
				SmartCadLoadInitialData.LoadInitialClientData();

                // LoadGlobalData if apply
                //System.Threading.Timer checkServer = ApplicationGuiUtil.StartCheckServer();

                ApplicationGuiUtil.CheckErrorDetailButton();
                ServerServiceClient.GetInstance().ActivateApplication(ApplicationName,ApplicationUtil.GetMACAddress());
                
                bool success = false;
                do
                {
                    LoginForm login = new LoginForm(UserApplicationClientData.Report);

                    if (login.ShowDialog() == DialogResult.OK)
                    {
#if !DEBUG
                        SplashForm.ShowSplash();
#endif
                        NetworkCredential networkCredential = login.NetworkCredential;

                        ImpersonateUser impersonateUser = new ImpersonateUser();
                        impersonateUser.Impersonate(
                            networkCredential.Domain,
                            networkCredential.UserName,
                            networkCredential.Password
                        );

                        Environment.SetEnvironmentVariable("TMP", SmartCadConfiguration.DistFolder + "Temp");

                        ServerServiceClient serverServiceClient = login.ServerService;

                        success = true;

                        ReportForm reportForm = new ReportForm();
                        reportForm.ServerServiceClient = serverServiceClient;
                        reportForm.NetworkCredential = networkCredential;
                       
#if !DEBUG
                        SplashForm.CloseSplash();
#endif

                        Application.Run(reportForm);
                    }
                    else
                    {
                        ServerServiceClient.GetInstance().CloseSession();
                        success = true;
                    }

                }
                while (!success);

            }
            catch (EndpointNotFoundException ex)
            {
                SplashForm.CloseSplash();

                MessageForm.Show(ResourceLoader.GetString2("UnableConnectToServer"), ex);
            }
            catch (Exception ex)
            {
                SplashForm.CloseSplash();

                MessageForm.Show(ResourceLoader.GetString2("GenericErrorApplication"), ex);
            }
         }
    }
}